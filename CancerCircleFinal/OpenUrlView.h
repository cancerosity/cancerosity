//
//  OpenUrlView.h
//  CancerCircleFirst
//
//  Created by Raminder on 17/04/13.
//
//

#import <UIKit/UIKit.h>

@interface OpenUrlView : UIViewController<UIWebViewDelegate, UIAlertViewDelegate>
{
    IBOutlet UIWebView *webView;
    
    UIBarButtonItem *refreshButton;
    UIBarButtonItem *backButton;
    UIBarButtonItem *forwardButton;
    UIToolbar *toolBar;
    UIActivityIndicatorView *loadingActivity;
    BOOL isRefreshing;
    NSString *title;
}

@property (nonatomic, retain) UIWebView *webView;
@property (nonatomic, retain) NSString *getUrl;
@property (nonatomic, retain) NSString *getId;

-(IBAction)refreshWebView;
-(IBAction)goBack2;
-(IBAction)goForward;
-(void)actualizeButtons;
@end
