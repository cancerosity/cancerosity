//
//  ForgotPassword.h
//  CancerCircleFirst
//
//  Created by Raminder on 24/01/13.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ForgotPassword : UIViewController
{
    IBOutlet UITextField *mailText;
    IBOutlet UITextField *contactText;
    NSString *mailStr;
    UIAlertView *alert;   
    IBOutlet UIImageView *bgImage;
}
-(IBAction)sendMail;
@property (retain, nonatomic) IBOutlet UIButton *submitb;
@property (retain, nonatomic) IBOutlet UIImageView *contactfield;
@property (retain, nonatomic) IBOutlet UIImageView *emailfield;
-(IBAction)hidekeyboard:(id)sender;

@end
