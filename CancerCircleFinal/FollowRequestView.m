//
//  FollowRequestView.m
//  CancerCircleFinal
//
//  Created by Raminder on 01/05/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import "FollowRequestView.h"
#import "AppDelegate.h"

@interface FollowRequestView ()

@end

@implementation FollowRequestView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.navigationController.navigationBarHidden = NO;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"640x960bg.png"]];
 
    CGFloat titleWidth2=100;
    CGFloat titleWidth = MIN(titleWidth2, 300);
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 30)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Futura_Light" size:20];
   
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=@"Follow Requests";
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIColor *barColour = [[UIColor alloc] initWithRed:(022.0/255.0) green:(180.0/255.0) blue:(030.0/255.0) alpha:1.0];
    UIView *colourView = [[UIView alloc] initWithFrame:CGRectMake(0.f, -20.f, 320.f, 64.f)];
    colourView.opaque = NO;
    colourView.alpha = 0.6f;
    colourView.backgroundColor = barColour;
    [navBar.layer insertSublayer:colourView.layer atIndex:1];
    [navBar setTintColor:[[UIColor alloc] initWithRed:(0.0/255.0) green:(214.0/255.0) blue:(11.0/255.0) alpha:1.0]];
    self.navigationItem.titleView = label;
    UIImage* image3 = [UIImage imageNamed:@"back22.png"];
    CGRect frameimg = CGRectMake(0, 0,35,35);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    UIBarButtonItem *backbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.leftBarButtonItem=backbutton;
    [self startIndicator];
}
-(void)goBack
{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}
-(void)startIndicator
{
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];//UIActivityIndicatorViewStyleWhiteLarge
    indicator.frame = CGRectMake(self.view.bounds.size.width / 2.0f - indicator.frame.size.width /2.0f, self.view.bounds.size.height / 2.0f - indicator.frame.size.height /2.0f, indicator.frame.size.width, indicator.frame.size.height);
    //indicator.color = [UIColor colorWithRed:1 green:0.8 blue:0.9 alpha:1 ];
    indicator.color = [UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
    CGAffineTransform transform = CGAffineTransformMakeScale(2.2f, 2.2f);
    indicator.transform = transform;
    [indicator startAnimating];
    [self.view addSubview:indicator];
    indicator.hidden = NO;
    [self performSelector:@selector(newsData) withObject:nil afterDelay:0.1];
    
}
-(void)newsData
{
    userNameArray=[[NSMutableArray alloc]initWithCapacity:userNameArray.count];
    userPicArray=[[NSMutableArray alloc]initWithCapacity:userPicArray.count];
    userStatusArray=[[NSMutableArray alloc]initWithCapacity:userStatusArray.count];
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    NSString *url = [NSString stringWithFormat:SERVER_URL@"/followrequest.do?username=%@",delegate.mynameLogin];
    NSURL *urlrequest=[NSURL URLWithString:url];
    NSData * dataofStrig = [NSData dataWithContentsOfURL:urlrequest];
    NSError *error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions                        error:&error];
    NSLog(@"JSOn from url %@",json);
    NSString *param=[json valueForKey:@"param"];
    if ([param isEqualToString:@"false"]) {
        
    }
    else
    {
        int length=[json count];
        for(int i=0;i<length-1;i++)    {
            NSString *indexStr = [NSString stringWithFormat:@"%d",i];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            NSString *infoStr=[[json objectForKey:data]valueForKey:@"request"] ;
            NSArray *infoArray=[infoStr componentsSeparatedByString:@"-END_OF_DATA-LINE-"];
            [userNameArray addObject:[infoArray objectAtIndex:0]];
            [userStatusArray addObject:[infoArray objectAtIndex:2]];
            NSString *userDP=[infoArray objectAtIndex:1];
            
            [userPicArray addObject:userDP];
            
        }
    }
    [myTable reloadData];
    [indicator stopAnimating];
}
-(void)followingButton:(UIButton*)btnClicked
{
    AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
    NSString *followUrl=[NSString stringWithFormat:SERVER_URL@"/followresponse.do?loginuser=%@&following=%@&response=accept",delegate.mynameLogin,[userNameArray objectAtIndex:btnClicked.tag-1]];
    NSURL *furl=[NSURL URLWithString:followUrl];
    NSError* error;
    NSData *receivedData=[NSData dataWithContentsOfURL:furl];
    
    if(!receivedData )
    {
        NSLog(@"errorrrrr no response");
        UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Connection failed" message:@"Check intenet connection" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
        [alert1 show];
        
    }
    
    else{
        
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:receivedData options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSString *resultStr=[json objectForKey:@"param"];
        if([resultStr isEqualToString:@"true"])
        {
            [userNameArray removeObjectAtIndex:btnClicked.tag-1];
            [userPicArray removeObjectAtIndex:btnClicked.tag-1];
            [userStatusArray removeObjectAtIndex:btnClicked.tag-1];
            delegate.requestCount=[NSString stringWithFormat:@"%d",[delegate.requestCount intValue]-1];
            [myTable reloadData];
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [userNameArray count];
    //  return 5;
    
}
- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    myTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [myTable setBackgroundView:[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"307x410.png"]] autorelease]];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.backgroundView = [ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cellWhiteBg.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];
    
    
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        NSLog(@"index path in cell %i",indexPath.row);
        
    }
    
    UIImage *image=[UIImage imageWithData:[userPicArray objectAtIndex:indexPath.row]];
    cell.imageView.layer.cornerRadius = 5;
    cell.imageView.clipsToBounds = YES;
    
    CGSize itemSize = CGSizeMake(40, 40);
    UIGraphicsBeginImageContext(itemSize);
    CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
    [image drawInRect:imageRect];
    cell.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    cell.textLabel.text=[userNameArray objectAtIndex:indexPath.row];
    cell.detailTextLabel.text=[userStatusArray objectAtIndex:indexPath.row];
    UIButton *likeBtn = nil;
    UIImage *plus = [UIImage imageNamed:@"follow.png"];
    likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
    likeBtn.tag = indexPath.row+1;
    [likeBtn addTarget:self action:@selector(followingButton:) forControlEvents:UIControlEventTouchUpInside];
    likeBtn.frame=CGRectMake(260,5,44,30);
    likeBtn.titleLabel.font=[UIFont systemFontOfSize:10.0];
    [cell addSubview:likeBtn];
    
    return cell;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
