
//
//  searchedUserView.m
//  CancerCircleFirst
//
//  Created by Raminder on 25/03/13.
//
//
#import "imageDetailView.h"
#import "storyDetailView.h"
#import "videoDetailView.h"
#import <Social/Social.h>
#import "searchedUserView.h"
#import "ODRefreshControl.h"
#import <Social/Social.h>
#import "AppDelegate.h"
#import "Login.h"
#import "SearchViewController.h"
#import "OpenUrlView.h"
#import "CircleFirst.h"
#import "ProfileFirst.h"
#import "FollowerViewController.h"
#import "FollowingViewController.h"
#import "FXBlurView.h"
#define kNavBarDefaultPosition CGPointMake(160, 42)

@interface searchedUserView ()

@end

@implementation searchedUserView
@synthesize getUserSearched,videoUrl,getImageSearched,getStatusSearched,getfollowers,getfollowing,getId,getDisName,getRowIndex;
UIActivityIndicatorView *indicator;
CGFloat startContentOffset;
CGFloat lastContentOffset;
BOOL hidden;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.navigationController.navigationBarHidden = NO;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleTweetNotification:) name:IFTweetLabelURLNotification object:nil];
    [self startIndicator];
    CALayer *layer = self.navigationController.navigationBar.layer;
    layer.position = kNavBarDefaultPosition;
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    CALayer *layer = self.navigationController.navigationBar.layer;
    layer.position = kNavBarDefaultPosition;
    
    [self refreshProfileData];
    self.imageDownloadingQueue = [[NSOperationQueue alloc] init];
    self.imageDownloadingQueue.maxConcurrentOperationCount = 4;
    self.imageCache = [[NSCache alloc] init];
    
    self.imageDownloadingQueuePic = [[NSOperationQueue alloc] init];
    self.imageDownloadingQueuePic.maxConcurrentOperationCount = 4;
    self.imageCachePic = [[NSCache alloc] init];
    
    self.imageDownloadingQueueProfilePic = [[NSOperationQueue alloc] init];
    self.imageDownloadingQueueProfilePic.maxConcurrentOperationCount = 1;
    self.imageCacheProfilePic = [[NSCache alloc] init];
    followerLabel.layer.borderColor = [[UIColor alloc] initWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:0.34].CGColor;
    followerLabel.layer.borderWidth = 1;
    followingLabel.layer.borderColor = [[UIColor alloc] initWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:0.34].CGColor;
    followingLabel.layer.borderWidth = 1;
    _followreq.layer.borderColor = [[UIColor alloc] initWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:0.34].CGColor;
    _followreq.layer.borderWidth = 1;
  
    CGFloat titleWidth2=100;
    CGFloat titleWidth = MIN(titleWidth2, 300);
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 30)];
    label.backgroundColor = [UIColor clearColor];
    // label.font = [UIFont boldSystemFontOfSize:20.0];
    label.font = [UIFont fontWithName:@"Futura_Light" size:20];
   
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=getUserSearched;    
    
  
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    
    
    navBar.barTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    
    UIImage* image3 = [UIImage imageNamed:@"settngProfile2.png"];
    CGRect frameimg = CGRectMake(0, 0,18,18);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(goSettings)
         forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *backbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.rightBarButtonItem=backbutton;
    UIImage* image4 = [UIImage imageNamed:@"close.png"];
    CGRect frameimg2 = CGRectMake(0, 0,18,18);
    UIButton *someButton2 = [[UIButton alloc] initWithFrame:frameimg2];
    [someButton2 setBackgroundImage:image4 forState:UIControlStateNormal];
    [someButton2 addTarget:self action:@selector(popBack)
         forControlEvents:UIControlEventTouchUpInside];
    [someButton2 setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *backbutton2 =[[UIBarButtonItem alloc] initWithCustomView:someButton2];
    self.navigationItem.leftBarButtonItem=backbutton2;
   
    
    //self.navigationItem.title =@"Sign Up";
    self.navigationItem.titleView = label;
    self.view.backgroundColor = [UIColor colorWithRed:(218.0/255.0) green:(221.0/255.0) blue:(226.0/255.0) alpha:1.0];
   
    
    
    NSLog(@"Searched user view contoller %@",getDisName);
    Login *login=[[Login alloc]init];
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
  
    
    
  
    userLogin=delegate.mynameLogin;
    usernameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:15];
    usernameLabel.text=getUserSearched;
    statuslabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:9];
    
    NSString *picUrlString = getImageSearched;
    UIImage *cachedPic = [self.imageCacheProfilePic objectForKey:picUrlString];
   
    _profileback.image= cachedPic;
    _profileback.contentMode = UIViewContentModeScaleAspectFill;
    _profileback.clipsToBounds = YES;
    if (cachedPic)
    {
        profileImage.image = cachedPic;
        
    }
    else
    {
        profileImage.image = [UIImage imageNamed:@"editPic.png"];
        _profileback.image= [UIImage imageNamed:@"editPic.png"];
        [self.imageDownloadingQueueProfilePic addOperationWithBlock:^{
            NSURL *imageURL   = [NSURL URLWithString:picUrlString];
            NSData *imageData2 = [NSData dataWithContentsOfURL:imageURL];
            UIImage *image    = nil;
            if (imageData2)
                image = [UIImage imageWithData:imageData2];
            if (image)
            {   [self.imageCacheProfilePic setObject:image forKey:picUrlString];
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    profileImage.image = image;
                    _profileback.image = image;
                    
                }];
            }
        }];
    }
    CALayer *imageLayer = profileImage.layer;
    
    
    
    [imageLayer setCornerRadius:30];
    [imageLayer setBorderWidth:1];
    [imageLayer setBorderColor:[[UIColor alloc] initWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:0.8].CGColor];
    [imageLayer setMasksToBounds:YES];
    // profileImage.image=[UIImage imageWithData:imageData];*/
    
    NSMutableAttributedString *attributed=[login atributedString:getStatusSearched];
    statuslabel.attributedText=attributed;
    NSLog(@"getfollowing %@",getfollowing);
    NSLog(@"getfollowers %@",getfollowers);
    searchfollowing=[getfollowing stringValue];
    //followingLabel.text=[searchfollowing stringByAppendingFormat:@" Following"];
    searchfollowers=[getfollowers stringValue];
    // followerLabel.text=[searchfollowers stringByAppendingFormat:@" Followers"];
    
    [followerLabel setTitle:[searchfollowers stringByAppendingFormat:@" Followers"] forState: UIControlStateNormal];
    [followerLabel setTitle:[searchfollowers stringByAppendingFormat:@" Followers"] forState: UIControlStateSelected];
    [followerLabel setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    [followingLabel setTitle:[searchfollowing stringByAppendingFormat:@" Followings"] forState: UIControlStateNormal];
    [followingLabel setTitle:[searchfollowing stringByAppendingFormat:@" Followings"] forState: UIControlStateSelected];
    [followingLabel setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    
    ODRefreshControl *refreshControl = [[ODRefreshControl alloc] initInScrollView:mytable];
    refreshControl.tintColor=[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1];
    refreshControl.backgroundColor = [UIColor clearColor];
    [refreshControl addTarget:self action:@selector(changeSorting:) forControlEvents:UIControlEventValueChanged];
    [self changeSorting:(refreshControl)];
    darkFollow=[UIImage imageNamed:@"followDark.png"];
    if([existFollowing isEqualToString:@"Yes"])
    {
        [followButton setBackgroundImage:darkFollow forState:UIControlStateNormal];
        followButton.userInteractionEnabled=NO;
    }
    [mytable reloadData];
    
}
-(void)expand
{
    
    [_mainScroll bringSubviewToFront:mytable];
    mytable.backgroundColor = [[UIColor alloc] initWithRed:(233.0/255.0) green:(234.0/255.0) blue:(241.0/255.0) alpha:1];
    
    [UIView animateWithDuration:0.5f
                     animations:^
     {
         
         mytable.frame = CGRectMake(0, 0, mytable.frame.size.width, mytable.frame.size.height);
         
         
     }
                     completion:^(BOOL finished)
     {
         
         
     }
     ];
}

-(void)contract
{
    
    [UIView animateWithDuration:0.5f
                     animations:^
     {
         
         mytable.frame = CGRectMake(0, 190, mytable.frame.size.width, mytable.frame.size.height);
         
         
         
     }
                     completion:^(BOOL finished)
     {
         mytable.backgroundColor = [UIColor clearColor];
         [_mainScroll sendSubviewToBack:mytable];
         
     }
     ];
    
    
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    startContentOffset = lastContentOffset = scrollView.contentOffset.y;
    
    
    //NSLog(@"scrollViewWillBeginDragging: %f", scrollView.contentOffset.y);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat differenceFromStart = startContentOffset - currentOffset;
    CGFloat differenceFromLast = lastContentOffset - currentOffset;
    lastContentOffset = currentOffset;
    
    
    
    if((differenceFromStart) < 0)
    {
        // scroll up
        if(scrollView.isTracking && (abs(differenceFromLast)>1))
        {
            
            
            
            
            [self expand];
            
            
        }
    }
    else {
        if(scrollView.isTracking && (abs(differenceFromLast)>1))
        {
            [self contract];
            
        }
    }
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    [self contract];
    return YES;
}

-(void) popBack {
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)getFollowings
{
    NSNumber *yourNumber = [NSNumber numberWithInt:0];
    if ([getfollowing isEqualToNumber:yourNumber]) {
        
    }
    else
    {
        FollowingViewController *following=[self.storyboard instantiateViewControllerWithIdentifier:@"following"];
        following.getName=getUserSearched;
        [self.navigationController pushViewController:following animated:YES];
    }
}
-(IBAction)getFollowers
{
    NSNumber *yourNumber = [NSNumber numberWithInt:0];
    if ([getfollowers isEqualToNumber:yourNumber]) {
        
    }
    else
    {
        FollowerViewController *getfollower=[self.storyboard instantiateViewControllerWithIdentifier:@"follower"];
        getfollower.getName=getUserSearched;
        [self.navigationController pushViewController:getfollower animated:YES];
    }
}
-(IBAction)goBack
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate.tabId isEqualToString:@"circle"]) {
        CircleFirst   *search=[self.storyboard instantiateViewControllerWithIdentifier:@"CircleFirst"];
        [self.navigationController pushViewController:search animated:YES];
    }
    else  if ([delegate.tabId isEqualToString:@"profile"]) {
        ProfileFirst   *search=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileFirst"];
        [self.navigationController pushViewController:search animated:YES];
    }
    else  if (([delegate.tabId isEqualToString:@"search"])&&([delegate.secondTabId isEqualToString:@"profile"])) {
        ProfileFirst   *search=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileFirst"];
        [self.navigationController pushViewController:search animated:YES];
    }
    else  if (([delegate.tabId isEqualToString:@"search"])&&([delegate.secondTabId isEqualToString:@"circle"])) {
        CircleFirst   *search=[self.storyboard instantiateViewControllerWithIdentifier:@"CircleFirst"];
        [self.navigationController pushViewController:search animated:YES];
    }
    /* else  if ([delegate.tabId isEqualToString:@"following"]){
     FollowingViewController *getfollowings=[self.storyboard instantiateViewControllerWithIdentifier:@"following"];
     [self.navigationController pushViewController:getfollowings animated:YES];
     }
     else  if ([delegate.tabId isEqualToString:@"follower"]){
     FollowerViewController *getfollower=[self.storyboard instantiateViewControllerWithIdentifier:@"follower"];
     [self.navigationController pushViewController:getfollower animated:YES];
     }*/
    
    //[self dismissViewControllerAnimated:YES completion:nil];
    
    
}
- (IBAction)refreshData
{
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self refreshProfileData]; // 1
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            [mytable reloadData];// 2
        });
    });
    
}
- (void)changeSorting:(ODRefreshControl *)refreshControl
//- (void)changeSorting:(UIRefreshControl *)refreshControl
{
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self profilePullData]; // 1
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            [refreshControl endRefreshing];// 2
        });
    });
    
}
- (void)handleTweetNotification:(NSNotification *)notification
{
    NSDictionary *dict = (NSDictionary*)notification.object;
    NSString *strDict = [NSString stringWithFormat:@"%@", dict];
    if ([strDict isEqualToString:getUserSearched]) {
        [mytable reloadData];
    }
    else if([strDict hasPrefix:@"http://"])
    {
        OpenUrlView *openUrl=[self.storyboard instantiateViewControllerWithIdentifier:@"OpenUrl"];
        // openUrl.hidesBottomBarWhenPushed = YES;
        openUrl.getUrl=strDict;
        [self.navigationController pushViewController:openUrl animated:YES];
    }
    else
    {
        AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
        delegate.wordToSearch=strDict;
        SearchViewController *SearchView=[self.storyboard instantiateViewControllerWithIdentifier:@"SearchView"];
        [self.navigationController pushViewController:SearchView animated:YES];
    }
}

-(IBAction)followingButton
{
    NSLog(@"exist following %@",existFollowing);
    if([existFollowing isEqualToString:@"Yes"])
    {
        [followButton setBackgroundImage:darkFollow forState:UIControlStateNormal];
        followButton.userInteractionEnabled=NO;
        UIAlertView *alert2=[[UIAlertView alloc]initWithTitle:@"Request Denied" message:@"You have already follow the user" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert2 show];
    }
    else
    {
        NSString *followUrl=[NSString stringWithFormat:SERVER_URL@"/following.do?username=%@&following=%@",userLogin,getUserSearched];
        NSURL *furl=[NSURL URLWithString:followUrl];
        NSError* error;
        NSData *receivedData=[NSData dataWithContentsOfURL:furl];
        
        if(!receivedData )
        {
            NSLog(@"errorrrrr no response");
            UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Connection failed" message:@"Check intenet connection" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
            [alert1 show];
            
        }
        
        else{
            
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:receivedData options:kNilOptions                        error:&error];
            NSLog(@"JSON format:- %@",json);
            NSString *resultStr=[json objectForKey:@"param"];
            NSString *reqStr=[json objectForKey:@"Follow_request"];
            if([resultStr isEqualToString:@"true"])
            {
                if([reqStr isEqualToString:@"true"])
                {
                    existFollowing=@"yes";
                    [followButton setBackgroundImage:darkFollow forState:UIControlStateNormal];
                    followButton.userInteractionEnabled=NO;
                    
                }
                else
                {
                    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
                    getfollowers=[NSNumber numberWithInt:[getfollowers intValue]+1];
                    delegate.following=[NSNumber numberWithInt:[delegate.following intValue]+1];
                    NSLog(@"getfollowers %@",getfollowers);
                    searchfollowers=[getfollowers stringValue];
                    [followerLabel setTitle:[searchfollowers stringByAppendingFormat:@" Followers"] forState: UIControlStateNormal];
                    [followerLabel setTitle:[searchfollowers stringByAppendingFormat:@" Followers"] forState: UIControlStateSelected];
                    [followerLabel setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
                    existFollowing=@"yes";
                    [followButton setBackgroundImage:darkFollow forState:UIControlStateNormal];
                    followButton.userInteractionEnabled=NO;
                }
                
            }
        }
    }
}

-(void)startIndicator
{
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];//UIActivityIndicatorViewStyleWhiteLarge
    indicator.frame = CGRectMake(self.view.bounds.size.width / 2.0f - indicator.frame.size.width /2.0f, self.view.bounds.size.height / 2.0f - indicator.frame.size.height /2.0f, indicator.frame.size.width, indicator.frame.size.height);
    //indicator.color = [UIColor colorWithRed:1 green:0.8 blue:0.9 alpha:1 ];
    indicator.color = [UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
    CGAffineTransform transform = CGAffineTransformMakeScale(2.2f, 2.2f);
    indicator.transform = transform;
    [indicator startAnimating];
    [self.view addSubview:indicator];
    indicator.hidden = NO;
    [self performSelector:@selector(request) withObject:nil afterDelay:0.1];
    
}
-(void)request{
    Login *login=[[Login alloc]init];
    NSString *url = [NSString stringWithFormat:SERVER_URL@"/login.do?method=myAction1&username=%@&password=",getUserSearched];
    NSURL *urlrequest=[NSURL URLWithString:url];
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    NSError* error;
    NSURLRequest *req=[[NSURLRequest alloc]initWithURL:urlrequest];
    NSURLResponse  *response = nil;
    
    [NSURLConnection sendSynchronousRequest: req returningResponse: &response error: &error];
    
    if (!error) {
        [indicator stopAnimating];
        indicator.hidden=YES;
        NSLog(@"errorrrrr no response");
        UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Connection failed" message:@"Check intenet connection" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
        [alert1 show];
    }
    
    NSData *receivedData=[NSData dataWithContentsOfURL:urlrequest];
    
    if(!receivedData )
    {
        [indicator stopAnimating];
        indicator.hidden=YES;
        NSLog(@"errorrrrr no response");
        UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Connection failed" message:@"Check intenet connection" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
        [alert1 show];
        
    }
    
    else{
        
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:receivedData options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        int length=[json count];
        dataUpdate = [[NSMutableArray alloc] initWithCapacity:dataUpdate.count];
        dataUpdate2 = [[NSMutableArray alloc] initWithCapacity:dataUpdate2.count];
        dataType = [[NSMutableArray alloc] initWithCapacity:dataType.count];
        likearray = [[NSMutableArray alloc] initWithCapacity:likearray.count];
        dislikearray = [[NSMutableArray alloc] initWithCapacity:dislikearray.count];
        snoArray=[[NSMutableArray alloc]initWithCapacity:snoArray.count];
        likesNameArray=[[NSMutableArray alloc]initWithCapacity:likesNameArray.count];
        dislikesNameArray=[[NSMutableArray alloc]initWithCapacity:dislikesNameArray.count];
        commentCounts=[[NSMutableArray alloc]initWithCapacity:commentCounts.count];
        locationArray=[[NSMutableArray alloc]initWithCapacity:locationArray.count];
        insertedTimeArray=[[NSMutableArray alloc]initWithCapacity:insertedTimeArray.count];
        timeArray=[[NSMutableArray alloc]initWithCapacity:timeArray.count];
        fourCommentArray=[[NSMutableArray alloc]initWithCapacity:fourCommentArray.count];
        NSLog(@"getfollowing %@",getfollowing);
        NSLog(@"getfollowers %@",getfollowers);
        searchfollowing=[getfollowing stringValue];
        //followingLabel.text=[searchfollowing stringByAppendingFormat:@" Following"];
        searchfollowers=[getfollowers stringValue];
        // followerLabel.text=[searchfollowers stringByAppendingFormat:@" Followers"];
        
        [followerLabel setTitle:[searchfollowers stringByAppendingFormat:@" Followers"] forState: UIControlStateNormal];
        [followerLabel setTitle:[searchfollowers stringByAppendingFormat:@" Followers"] forState: UIControlStateSelected];
        [followerLabel setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
        [followingLabel setTitle:[searchfollowing stringByAppendingFormat:@" Followings"] forState: UIControlStateNormal];
        [followingLabel setTitle:[searchfollowing stringByAppendingFormat:@" Followings"] forState: UIControlStateSelected];
        [followingLabel setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];

        
                PgetInertedTime=[[json objectForKey:@"data0"] valueForKey:@"inserted_at"];
        AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
        NSString *indexStr = [NSString stringWithFormat:@"%d",(length-6)];
        NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
        delegate.SgetLastInseted =[[json objectForKey:data] valueForKey:@"inserted_at"];
        // UIImage *image = [UIImage imageWithData:imageData];
        // profileImage.image=image;
        // profile_image=[[json objectForKey:@"data0"]valueForKey:@"profile_image"];
        if ([[[json objectForKey:@"data0"]valueForKey:@"privacy"] isEqualToString:@"yes"]) {
            mytable.hidden=YES;
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(70, 190,180, 110)];
            imageView.layer.cornerRadius = 10;
            imageView.clipsToBounds = YES;
            [imageView setImage:[UIImage imageNamed:@"userPrivate.png"]];
            [self.view addSubview:imageView];
        }
        else
        {
            NSLog(@"profile_image %@",profile_image);
            for(int i=0;i<length-5;i++)
            {
                NSString *indexStr = [NSString stringWithFormat:@"%d",i];
                NSString *data2=[@"data" stringByAppendingFormat:@"%@",indexStr];
                NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
                PgetLastInertedTime=[[json objectForKey:data] valueForKey:@"inserted_at"];
                NSLog(@"inserted at %@",PgetLastInertedTime);
                [commentCounts addObject:[[json objectForKey:data]valueForKey:@"comment"]];
                NSString *likesName=[[json objectForKey:data] valueForKey:@"likenames"];
                NSString *confrm=[login likesContent:likesName];
                [likesNameArray addObject:confrm];
                NSString *dislikesName=[[json objectForKey:data] valueForKey:@"dislikenames"];
                NSString *disconfrm=[login likesContent:dislikesName];
                [dislikesNameArray addObject:disconfrm];
                NSString *snoString = [[json valueForKey:data2]valueForKey:@"sno"];
                [snoArray addObject:snoString];
                NSString *likeString = [[json valueForKey:data2]valueForKey:@"like"];
                NSLog(@"value of like string is:- %@",likeString);
                [likearray addObject:likeString];
                NSString *dislikeString = [[json valueForKey:data2]valueForKey:@"dislike"];
                NSLog(@"value of like string is:- %@",dislikeString);
                [dislikearray addObject:dislikeString];
                [insertedTimeArray addObject:[[json objectForKey:data] valueForKey:@"inserted_at"]];
                
                if ([[[json objectForKey:data] valueForKey:@"Elapsed"] isEqualToString:@""]) {
                    [timeArray addObject:@"not updated"];
                }
                else
                {
                    [timeArray addObject:[[json objectForKey:data] valueForKey:@"Elapsed"]];
                }
                if([[[json objectForKey:data] valueForKey:@"location"] isEqualToString:@""])
                {
                    [locationArray addObject:@"not updated"];
                }
                else
                {
                    [locationArray addObject:[[json objectForKey:data] valueForKey:@"location"]];
                }
                
                dataStr=[[json valueForKey:data2] valueForKey:@"content1"];
                dataStr=[[json valueForKey:data2] valueForKey:@"content2"];
                NSLog(@"content from url %@",dataStr);
                NSString *type=[[json objectForKey:data]valueForKey:@"type"];
                
                [dataType addObject:type];
                [dataUpdate addObject:dataStr];
                [dataUpdate2 addObject:dataStr2];
                NSString *fourcomment=[[json objectForKey:data]valueForKey:@"commentiphone"];
                NSLog(@"fourcomment %@",fourcomment);
                if ([fourcomment length]==0) {
                    [fourCommentArray addObject:@"No Comments"];
                }
                else{
                    [fourCommentArray addObject:fourcomment];
                }
                
            }
            
        }
    }
    [dataUpdate addObject:@"See More Updates....."];
    [dataType addObject:@"more"];
    [snoArray addObject:@"more"];
    [likearray addObject:@"more"];
    [dislikearray addObject:@"more"];
    [likesNameArray addObject:@"more"];
    [dislikesNameArray addObject:@"more"];
    [commentCounts addObject:@"more"];
    [locationArray addObject:@"more"];
    [timeArray addObject:@"more"];
    [fourCommentArray addObject:@"more"];
    [insertedTimeArray addObject:@"more"];
    [indicator stopAnimating];
    indicator.hidden=YES;
    
    [mytable reloadData];
    
}
-(NSString *)followingNames:(NSString *)allNames
{
    NSLog(@"alll names %@",allNames);
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    NSLog(@"username %@",delegate.mynameLogin);
    NSString *name=[[NSString alloc]init];
    
    if ([allNames rangeOfString:delegate.mynameLogin].location != NSNotFound)
    {
        name=@"Yes";
    }
    
    else
    {
        name=@"No";
    }
    
    return name;
}
-(IBAction)goSettings
{
    self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:3];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataUpdate count];
    //  return 5;
    
}

- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *typeString=[dataType objectAtIndex:indexPath.row];
    int height;
    if([typeString isEqualToString:@"image"])
    {
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)) {
            height= 410;
        }
        else if ([comArray count]==3){
            height= 425;
        }
        else{
            
            height= 455;
        }
        
    }
    else if([typeString isEqualToString:@"video"]){
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)) {
            height= 360;
        }
        else if ([comArray count]==3){
            height= 375;
        }
        else{
            
            height= 400;
        }
        
        
    }
    else if([typeString isEqualToString:@"story"]){
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)) {
            height= 180;
        }
        else if ([comArray count]==3){
            height= 195;
        }
        else{
            
            height=225;
        }
        
    }
    else
    {
        height=30;
    }
    return height;
    
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    Login *login=[[Login alloc]init];
    static NSString *CellIdentifier = @"Cell";
    static NSString *CellIdentifier2 = @"Cell2";
    static NSString *CellIdentifier3 = @"Cell3";
    static NSString *moreCellId = @"more";
    static NSString *image1 = @"image1";
    static NSString *image2 = @"image2";
    static NSString *story1 = @"story1";
    static NSString *story2 = @"story2";
    static NSString *video1 = @"video1";
    static NSString *video2 = @"video2";
    
    UIImage *minus = [UIImage imageNamed:@"dislikebutton2.png"];
    UIImage *plus = [UIImage imageNamed:@"likebutton2.png"];
    UIImage *minusDark = [UIImage imageNamed:@"minusDark.png"];
    UIImage *plusDark = [UIImage imageNamed:@"plusDark.png"];
    UITableViewCell *cell4 = [tableView dequeueReusableCellWithIdentifier:moreCellId];
    cell4.selectionStyle = UITableViewCellSelectionStyleNone;
    mytable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [mytable setBackgroundView:[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"307x410.png"]] autorelease]];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UITableViewCell *ce=[[UITableViewCell alloc]init];
    UITableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
    UITableViewCell *cell3 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier3];
    UITableViewCell *cellstory1 = [tableView dequeueReusableCellWithIdentifier:story1];
    UITableViewCell *cellstory2 = [tableView dequeueReusableCellWithIdentifier:story2];
    UITableViewCell *cellvideo1 = [tableView dequeueReusableCellWithIdentifier:video1];
    UITableViewCell *cellvideo2 = [tableView dequeueReusableCellWithIdentifier:video2];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell2.selectionStyle = UITableViewCellSelectionStyleNone;
    cell3.selectionStyle = UITableViewCellSelectionStyleNone;
    cell4.selectionStyle = UITableViewCellSelectionStyleNone;
    cellstory1.selectionStyle = UITableViewCellSelectionStyleNone;
    cellstory2.selectionStyle = UITableViewCellSelectionStyleNone;
    cellvideo1.selectionStyle = UITableViewCellSelectionStyleNone;
    cellvideo2.selectionStyle = UITableViewCellSelectionStyleNone;
    
    // cell.backgroundView = [ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cellWhiteBg.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];
    
    NSData *imageString =[dataUpdate objectAtIndex:indexPath.row];
    NSString *typeString=[dataType objectAtIndex:indexPath.row];
    int in=indexPath.row;
    introw=indexPath.row;
    int lbltag =indexPath.row+1;
    // NSString *content= [self contentTypeForImageData:imageString];
    if([typeString isEqualToString:@"image"])
        //if(imageString)
    {
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)){
            NSLog(@"hiiiiiiiiii image %@",imageString);
            image2Cell *imgCell=[tableView dequeueReusableCellWithIdentifier:image2];
            if (imgCell == nil)
            {
                imgCell = [[[image2Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
                NSLog(@"index path in cell %i",indexPath.row);
                
            }
            introw=indexPath.row+1;
            //playButton.enabled=FALSE;
            playButton.userInteractionEnabled=NO;
            NSLog(@"value in data update is:- %@",dataUpdate);
            NSString *imageUrlString = [dataUpdate objectAtIndex: indexPath.row];
            
            NSLog(@"image url string iss:- %@",imageUrlString);
            UIImage *cachedImage = [self.imageCache objectForKey:imageUrlString];
            if (cachedImage)
            {
                //imgCell.imageView.image = cachedImage;
                imgCell.imageView.image = cachedImage;
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imgCell.imageView.image = [UIImage imageNamed:@"placholder.png"];
                [self.imageDownloadingQueue addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:imageUrlString];
                    NSData *imageData2 = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData2)
                        image = [UIImage imageWithData:imageData2];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCache setObject:image forKey:imageString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                //imgCell.imageView.image = image;
                                
                                imgCell.imageView.image = image;
                            
                            
                        }];
                    }
                }];
            }
            
            
            //***********************************
            
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,272,40,40)];
            imageView2.layer.cornerRadius = 5;
            imageView2.clipsToBounds = YES;
            [imgCell.contentView addSubview:imageView2];
            
            NSString *picUrlString = getImageSearched;
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"editPic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData2 = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData2)
                        image = [UIImage imageWithData:imageData2];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            
            
            //***********************************
            
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(70,272,100,30)];
            nameLabel.numberOfLines=0;
            nameLabel.font=[UIFont fontWithName:@"Helvetica-Bold" size:12];
            NSString *smile2=getUserSearched;
            [nameLabel setText:smile2];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.clipsToBounds=YES;
            [imgCell.contentView addSubview:nameLabel];
            
            
            NSString *smile;
            IFTweetLabel *descriptionLabel1=[[IFTweetLabel alloc]initWithFrame:CGRectMake(15,318,278,15)];
            descriptionLabel1.font = [UIFont systemFontOfSize:10.0];
            NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
            NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
            if ([comArray count]==2) {
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
            }
            [imgCell.contentView addSubview:descriptionLabel1];
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,270,100,10)];
            placeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            placeLabel.font = [UIFont systemFontOfSize:8.0];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [imgCell.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,280,100,10)];
            timeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            timeLabel.font = [UIFont systemFontOfSize:8.0];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentRight;
            [imgCell.contentView addSubview:timeLabel];
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            [likeBtn addTarget:self action:@selector(likeImage:) forControlEvents:UIControlEventTouchUpInside];
            likeBtn.frame=CGRectMake(26,350,44,30);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }        [imgCell addSubview:likeBtn];
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            [dislikeBtn addTarget:self action:@selector(dislikeImage:) forControlEvents:UIControlEventTouchUpInside];
            dislikeBtn.frame=CGRectMake(80,350,44,30);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            [imgCell addSubview:dislikeBtn];
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"commentbutton2.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(134,350,44,30)];
            [commentButton addTarget:self action:@selector(moreButtonImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:commentButton];
            
            
            UILabel *likeShow = nil;
            likeShow.backgroundColor=[UIColor redColor];
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(26,340,44,10)] autorelease];
            likeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:12.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [imgCell addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            
            
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(80,340,43,10)] autorelease];
            dislikeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:12.0];
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            dislikeShow.tag = lbltag;
            [imgCell addSubview:dislikeShow];
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(134,340,43,10)] autorelease];
            commentShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:12.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [imgCell addSubview:commentShow];
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"share_button2.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(188,350,44,30)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:shareButton];
            
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"bottom-setting2.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(242,350,44,30)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:settingsButton];
            
            UIButton *moreButtonI=nil;
            moreButtonI = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonI.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonI setBackgroundImage:more forState:UIControlStateNormal];
            
            [moreButtonI setFrame:CGRectMake(280,320,30,22)];
            [moreButtonI addTarget:self action:@selector(moreButtonImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:moreButtonI];
            
            //******************************************************************************
            
            //return cell;
            ce=imgCell;
            
            
        }
        
        else if([comArray count]==3) {
            
            NSLog(@"hiiiiiiiiii image %@",imageString);
            image1Cell *imgCell=[tableView dequeueReusableCellWithIdentifier:image1];
            if (imgCell == nil)
            {
                imgCell = [[[image1Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
                NSLog(@"index path in cell %i",indexPath.row);
                
            }
            introw=indexPath.row+1;
            //playButton.enabled=FALSE;
            playButton.userInteractionEnabled=NO;
            NSLog(@"value in data update is:- %@",dataUpdate);
            NSString *imageUrlString = [dataUpdate objectAtIndex: indexPath.row];
            
            NSLog(@"image url string iss:- %@",imageUrlString);
            UIImage *cachedImage = [self.imageCache objectForKey:imageUrlString];
            if (cachedImage)
            {
                //imgCell.imageView.image = cachedImage;
                imgCell.imageView.image = cachedImage;
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imgCell.imageView.image = [UIImage imageNamed:@"placholder.png"];
                [self.imageDownloadingQueue addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:imageUrlString];
                    NSData *imageData2 = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData2)
                        image = [UIImage imageWithData:imageData2];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCache setObject:image forKey:imageString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                //imgCell.imageView.image = image;
                                
                                imgCell.imageView.image = image;
                            
                            
                        }];
                    }
                }];
            }
            
            
            //***********************************
            
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,272,40,40)];
            imageView2.layer.cornerRadius = 5;
            imageView2.clipsToBounds = YES;
            [imgCell.contentView addSubview:imageView2];
            
            NSString *picUrlString = getImageSearched;
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"editPic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData2 = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData2)
                        image = [UIImage imageWithData:imageData2];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            
            
            //***********************************
            
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(70,272,100,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            nameLabel.font=[UIFont fontWithName:@"Helvetica-Bold" size:12];
            NSString *smile2=getUserSearched;//[@"@" stringByAppendingString:[differUser objectAtIndex:in]];
            [nameLabel setText:smile2];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.clipsToBounds=YES;
            [imgCell.contentView addSubview:nameLabel];
            
            
            NSString *smile;
            IFTweetLabel *descriptionLabel1=[[IFTweetLabel alloc]initWithFrame:CGRectMake(15,318,278,15)];
            descriptionLabel1.font = [UIFont systemFontOfSize:10.0];
            IFTweetLabel *descriptionLabel2=[[IFTweetLabel alloc]initWithFrame:CGRectMake(15,333,278,15)];
            descriptionLabel2.font = [UIFont systemFontOfSize:10.0];
            IFTweetLabel *descriptionLabel3=[[IFTweetLabel alloc]initWithFrame:CGRectMake(15,348,278,15)];
            descriptionLabel3.font = [UIFont systemFontOfSize:10.0];
            //[descriptionLabel3 setNumberOfLines:0];
            IFTweetLabel *descriptionLabel4=[[IFTweetLabel alloc]initWithFrame:CGRectMake(15,363,278,15)];
            descriptionLabel4.font = [UIFont systemFontOfSize:10.0];
            //[descriptionLabel4 setNumberOfLines:0];
            NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
            NSMutableAttributedString *attributed;
            NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
            if ([comArray count]==2) {
                //descriptionLabel1.text=@"Only one comment add your comment....";
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
                
            }
            else if ([comArray count]==3) {
                attributed=[login attibutedUsername:[comArray objectAtIndex:0]];
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
                attributed=[login attibutedUsername:[comArray objectAtIndex:1]];
                smile=[login smilyString:[comArray objectAtIndex:1]];
                [descriptionLabel2 setText:smile];
                [descriptionLabel2 setLinksEnabled:TRUE];
            }
            
            
            // }
            
            [imgCell.contentView addSubview:descriptionLabel1];
            [imgCell.contentView addSubview:descriptionLabel2];
            
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,270,100,10)];
            placeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            placeLabel.font = [UIFont systemFontOfSize:8.0];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [imgCell.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,280,100,10)];
            timeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            timeLabel.font = [UIFont systemFontOfSize:8.0];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentRight;
            [imgCell.contentView addSubview:timeLabel];
            
            
            //******************************************************************************
            
            
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            [likeBtn addTarget:self action:@selector(likeImage:) forControlEvents:UIControlEventTouchUpInside];
            likeBtn.frame=CGRectMake(26,365,44,30);
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            NSLog(@"Confimation returned %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }        [imgCell addSubview:likeBtn];
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            [dislikeBtn addTarget:self action:@selector(dislikeImage:) forControlEvents:UIControlEventTouchUpInside];
            dislikeBtn.frame=CGRectMake(80,365,44,30);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [imgCell addSubview:dislikeBtn];
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"commentbutton2.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(134,365,44,30)];
            [commentButton addTarget:self action:@selector(moreButtonImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:commentButton];
            
            
            UILabel *likeShow = nil;
            likeShow.backgroundColor=[UIColor redColor];
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(26,355,43,10)] autorelease];
            likeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:12.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [imgCell addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            
            
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(80,355,43,10)] autorelease];
            dislikeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:12.0];
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            dislikeShow.tag = lbltag;
            [imgCell addSubview:dislikeShow];
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(134,355,43,10)] autorelease];
            commentShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:12.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [imgCell addSubview:commentShow];
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"share_button2.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(188,365,44,30)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:shareButton];
            
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"bottom-setting2.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(242,365,44,30)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:settingsButton];
            
            UIButton *moreButtonI=nil;
            moreButtonI = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonI.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonI setBackgroundImage:more forState:UIControlStateNormal];
            
            [moreButtonI setFrame:CGRectMake(280,335,30,22)];
            [moreButtonI addTarget:self action:@selector(moreButtonImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:moreButtonI];
            
            //******************************************************************************
            
            //return cell;
            ce=imgCell;
            
            
            
        }
        else if(([comArray count]==4)||([comArray count]==5)){
            imageCell *imgCell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (imgCell == nil)
            {
                imgCell = [[[imageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
                NSLog(@"index path in cell %i",indexPath.row);
                
            }
            introw=indexPath.row+1;
            //playButton.enabled=FALSE;
            playButton.userInteractionEnabled=NO;
            NSLog(@"value in data update is:- %@",dataUpdate);
            NSString *imageUrlString = [dataUpdate objectAtIndex: indexPath.row];
            
            NSLog(@"image url string iss:- %@",imageUrlString);
            UIImage *cachedImage = [self.imageCache objectForKey:imageUrlString];
            if (cachedImage)
            {
                //imgCell.imageView.image = cachedImage;
                imgCell.imageView.image = cachedImage;
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imgCell.imageView.image = [UIImage imageNamed:@"placholder.png"];
                [self.imageDownloadingQueue addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:imageUrlString];
                    NSData *imageData2 = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData2)
                        image = [UIImage imageWithData:imageData2];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCache setObject:image forKey:imageString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                //imgCell.imageView.image = image;
                                
                                imgCell.imageView.image = image;
                            
                            
                        }];
                    }
                }];
            }
            
            
            //***********************************
            
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,272,40,40)];
            imageView2.layer.cornerRadius = 5;
            imageView2.clipsToBounds = YES;
            [imgCell.contentView addSubview:imageView2];
            
            NSString *picUrlString = getImageSearched;
            NSLog(@"kdfjkjdksjgk ljklfjgkljdslgj %@",picUrlString);
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"editPic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData2 = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData2)
                        image = [UIImage imageWithData:imageData2];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            
            
            NSLog(@"valid");
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(70,272,100,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            nameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:13];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.text=getUserSearched;
            [imgCell.contentView addSubview:nameLabel];
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,270,100,10)];
            placeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            placeLabel.font = [UIFont systemFontOfSize:8.0];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cell.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,280,100,10)];
            timeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            timeLabel.font = [UIFont systemFontOfSize:8.0];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentRight;
            [imgCell.contentView addSubview:timeLabel];
            
            NSString *smile;
            IFTweetLabel *descriptionLabel1=[[IFTweetLabel alloc]initWithFrame:CGRectMake(15,318,278,15)];
            descriptionLabel1.font = [UIFont systemFontOfSize:10.0];
            IFTweetLabel *descriptionLabel2=[[IFTweetLabel alloc]initWithFrame:CGRectMake(15,333,278,15)];
            descriptionLabel2.font = [UIFont systemFontOfSize:10.0];
            IFTweetLabel *descriptionLabel3=[[IFTweetLabel alloc]initWithFrame:CGRectMake(15,348,278,15)];
            descriptionLabel3.font = [UIFont systemFontOfSize:10.0];
            //[descriptionLabel3 setNumberOfLines:0];
            IFTweetLabel *descriptionLabel4=[[IFTweetLabel alloc]initWithFrame:CGRectMake(15,363,278,15)];
            descriptionLabel4.font = [UIFont systemFontOfSize:10.0];
            
            smile=[login smilyString:[comArray objectAtIndex:0]];
            [descriptionLabel1 setText:smile];
            [descriptionLabel1 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:1]];
            [descriptionLabel2 setText:smile];
            [descriptionLabel2 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:2]];
            [descriptionLabel3 setText:smile];
            [descriptionLabel3 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:3]];
            [descriptionLabel4 setText:smile];
            [descriptionLabel4 setLinksEnabled:TRUE];
            
            [imgCell.contentView addSubview:descriptionLabel1];
            [imgCell.contentView addSubview:descriptionLabel2];
            [imgCell.contentView addSubview:descriptionLabel3];
            [imgCell.contentView addSubview:descriptionLabel4];
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            [likeBtn addTarget:self action:@selector(likeImage:) forControlEvents:UIControlEventTouchUpInside];
            likeBtn.frame=CGRectMake(26,395,44,30);
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            NSLog(@"Confimation returned %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [imgCell addSubview:likeBtn];
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            //likeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            dislikeBtn.tag = indexPath.row+1;
            [dislikeBtn addTarget:self action:@selector(dislikeImage:) forControlEvents:UIControlEventTouchUpInside];
            dislikeBtn.frame=CGRectMake(80,395,44,30);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [imgCell addSubview:dislikeBtn];
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"commentbutton2.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(134,395,44,30)];
            [commentButton addTarget:self action:@selector(moreButtonImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:commentButton];
            
            
            UILabel *likeShow = nil;
            likeShow.backgroundColor=[UIColor redColor];
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(26,385,43,10)] autorelease];
            likeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:12.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [imgCell addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            
            
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(80,385,43,10)] autorelease];
            dislikeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:12.0];
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            dislikeShow.tag = lbltag;
            [imgCell addSubview:dislikeShow];
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(134,385,43,10)] autorelease];
            commentShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:12.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [imgCell addSubview:commentShow];
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"share_button2.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(188,395,44,30)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:shareButton];
            
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"bottom-setting2.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(242,395,44,30)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:settingsButton];
            
            UIButton *moreButtonI=nil;
            moreButtonI = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonI.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonI setBackgroundImage:more forState:UIControlStateNormal];
            
            [moreButtonI setFrame:CGRectMake(280,355,30,22)];
            [moreButtonI addTarget:self action:@selector(moreButtonImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:moreButtonI];
            ce=imgCell;
            
            
        }
        
    }
    else if([typeString isEqualToString:@"story"])
    {
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)){
            NSLog(@"helllloooo");
            
            if (cellstory1 == nil)
            {
                cellstory1 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:story1] autorelease];
                
            }
            cellstory1.backgroundView = [ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cellWhiteBg.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];
            introw=indexPath.row+1;
            tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,12,290,20)];
            tweetLabel.numberOfLines=0;
            tweetLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            tweetLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:13];
            NSString *smile2=[login smilyString:[dataUpdate objectAtIndex:in]];
            [tweetLabel setText:smile2];
            [tweetLabel setLinksEnabled:TRUE];
            tweetLabel.clipsToBounds=YES;
            [cellstory1.contentView addSubview:tweetLabel];
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,40,40,40)];
            imageView2.layer.cornerRadius = 5;
            imageView2.clipsToBounds = YES;
            [cellstory1.contentView addSubview:imageView2];
            
            NSString *picUrlString = getImageSearched;
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"editPic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData2 = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData2)
                        image = [UIImage imageWithData:imageData2];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            
            NSLog(@"valid");
            
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(70,40,100,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            nameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:13];
            NSString *smile3=getUserSearched;//[@"@" stringByAppendingString:[differUser objectAtIndex:in]];
            [nameLabel setText:smile3];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.clipsToBounds=YES;
            
            [cellstory1.contentView addSubview:nameLabel];
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,40,100,10)];
            placeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            placeLabel.font = [UIFont systemFontOfSize:8.0];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cellstory1.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,50,100,10)];
            timeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            timeLabel.font = [UIFont systemFontOfSize:8.0];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentRight;
            [cellstory1.contentView addSubview:timeLabel];
            
            NSString *smile;
            IFTweetLabel *descriptionLabel1 = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,86,60,15)];
            
            descriptionLabel1.font = [UIFont systemFontOfSize:10.0];
            
            NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
            if ([comArray count]==2) {
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
            }
            
            [cellstory1 addSubview:descriptionLabel1];
            
            UILabel *likeShow = nil;
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(26,110,43,10)] autorelease];
            likeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:12.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cellstory1 addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(80,110,43,10)] autorelease];
            dislikeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:12.0];
            dislikeShow.tag = lbltag;
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cellstory1 addSubview:dislikeShow];
            
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(134,110,43,10)] autorelease];
            commentShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:12.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cellstory1 addSubview:commentShow];
            //playButton.enabled=FALSE;
            playButton.userInteractionEnabled=NO;
            //******************************************************************************
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            likeBtn.frame=CGRectMake(26,120,44,30);
            [likeBtn addTarget:self action:@selector(likeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cellstory1 addSubview:likeBtn];
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            dislikeBtn.frame=CGRectMake(80,120,44,30);
            [dislikeBtn addTarget:self action:@selector(dislikeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[dislikesNameArray objectAtIndex:indexPath.row]);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            [cellstory1 addSubview:likeBtn];
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cellstory1 addSubview:dislikeBtn];
            
            
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"commentbutton2.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(134,120,44,30)];
            [commentButton addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory1.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"share_button2.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(188,120,44,30)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory1.contentView addSubview:shareButton];
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"bottom-setting2.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(242,120,44,30)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory1.contentView addSubview:settingsButton];
            
            UIButton *moreButtonS=nil;
            moreButtonS = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonS.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonS setBackgroundImage:more forState:UIControlStateNormal];
            moreButtonS.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [moreButtonS setFrame:CGRectMake(280,90,30,22)];
            [moreButtonS addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory1.contentView addSubview:moreButtonS];
            ce=cellstory1;
            
        }
        else if(([comArray count]==3))
        {
            cellstory2.backgroundView = [ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cellWhiteBg.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];
            NSLog(@"helllloooo");
            
            if (cellstory2 == nil)
            {
                cellstory2 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:story2] autorelease];
                
            }
            introw=indexPath.row+1;
            tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,12,290,20)];
            tweetLabel.numberOfLines=0;
            tweetLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            tweetLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:13];
            NSString *smile2=[login smilyString:[dataUpdate objectAtIndex:in]];
            [tweetLabel setText:smile2];
            [tweetLabel setLinksEnabled:TRUE];
            tweetLabel.clipsToBounds=YES;
            [cellstory2.contentView addSubview:tweetLabel];
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,40,40,40)];
            imageView2.layer.cornerRadius = 5;
            imageView2.clipsToBounds = YES;
            [cellstory2.contentView addSubview:imageView2];
            
            NSString *picUrlString =getImageSearched;
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"editPic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData2 = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData2)
                        image = [UIImage imageWithData:imageData2];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            
            NSLog(@"valid");
            
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(70,40,100,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            nameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:13];
            NSString *smile3=getUserSearched;//[@"@" stringByAppendingString:[differUser objectAtIndex:in]];
            [nameLabel setText:smile3];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.clipsToBounds=YES;
            
            [cellstory2.contentView addSubview:nameLabel];
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,40,100,10)];
            placeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            placeLabel.font = [UIFont systemFontOfSize:8.0];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cellstory2.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,50,100,10)];
            timeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            timeLabel.font = [UIFont systemFontOfSize:8.0];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentRight;
            [cellstory2.contentView addSubview:timeLabel];
            
            NSString *smile;
            IFTweetLabel *descriptionLabel1 = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,86,278,15)];
            
            descriptionLabel1.font = [UIFont systemFontOfSize:10.0];
            IFTweetLabel *descriptionLabel2 = [[IFTweetLabel alloc]initWithFrame:CGRectMake(15,101,260,15)];
            descriptionLabel2.font = [UIFont systemFontOfSize:10.0];
            NSMutableAttributedString *attributed;
            
            if ([comArray count]==3) {
                attributed=[login attibutedUsername:[comArray objectAtIndex:0]];
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
                attributed=[login attibutedUsername:[comArray objectAtIndex:1]];
                smile=[login smilyString:[comArray objectAtIndex:1]];
                [descriptionLabel2 setText:smile];
                [descriptionLabel2 setLinksEnabled:TRUE];
            }
            
            [cellstory2 addSubview:descriptionLabel1];
            [cellstory2 addSubview:descriptionLabel2];
            
            UILabel *likeShow = nil;
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(26,125,43,10)] autorelease];
            likeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:12.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cellstory2 addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(80,125,43,10)] autorelease];
            dislikeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:12.0];
            dislikeShow.tag = lbltag;
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cellstory2 addSubview:dislikeShow];
            
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(134,125,43,10)] autorelease];
            commentShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:12.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cellstory2 addSubview:commentShow];
            //playButton.enabled=FALSE;
            playButton.userInteractionEnabled=NO;
            //******************************************************************************
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            likeBtn.frame=CGRectMake(26,135,44,30);
            [likeBtn addTarget:self action:@selector(likeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cellstory2 addSubview:likeBtn];
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            dislikeBtn.frame=CGRectMake(80,135,44,30);
            [dislikeBtn addTarget:self action:@selector(dislikeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[dislikesNameArray objectAtIndex:indexPath.row]);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            [cellstory2 addSubview:likeBtn];
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cellstory2 addSubview:dislikeBtn];
            
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"commentbutton2.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(134,135,44,30)];
            [commentButton addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory2.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"share_button2.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(188,135,44,30)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory2.contentView addSubview:shareButton];
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"bottom-setting2.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(242,135,44,30)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory2.contentView addSubview:settingsButton];
            
            UIButton *moreButtonS=nil;
            moreButtonS = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonS.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonS setBackgroundImage:more forState:UIControlStateNormal];
            moreButtonS.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [moreButtonS setFrame:CGRectMake(280,105,30,22)];
            [moreButtonS addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory2.contentView addSubview:moreButtonS];
            ce=cellstory2;
        }
        else if(([comArray count]==4)||([comArray count]==5))
        {
            
            cell2.backgroundView = [ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cellWhiteBg.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];
            NSLog(@"helllloooo");
            //imageViewUrl.image=nil;
            
            if (cell2 == nil)
            {
                cell2 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
                
            }
            Login *login=[[Login alloc]init];
            
            tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,12,290,20)];
            tweetLabel.numberOfLines=0;
            tweetLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            tweetLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:13];
            //NSMutableAttributedString *attributed=[login atributedString:[dataUpdate objectAtIndex:in]];
            //storyLabel.attributedText=attributed;
            NSString *smile=[login smilyString:[dataUpdate objectAtIndex:in]];
            [tweetLabel setText:smile];
            [tweetLabel setLinksEnabled:TRUE];
            tweetLabel.clipsToBounds=YES;
            [cell2.contentView addSubview:tweetLabel];
            
            //int lbltag =indexPath.row;
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,40,40,40)];
            imageView2.layer.cornerRadius = 5;
            imageView2.clipsToBounds = YES;
            [cell2.contentView addSubview:imageView2];
            
            NSString *picUrlString = getImageSearched;
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"editPic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData2 = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData2)
                        image = [UIImage imageWithData:imageData2];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(70,40,100,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            nameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:13];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.text=getUserSearched;
            [cell2.contentView addSubview:nameLabel];
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,40,100,10)];
            placeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            placeLabel.font = [UIFont systemFontOfSize:8.0];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cell2.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,50,100,10)];
            timeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            timeLabel.font = [UIFont systemFontOfSize:8.0];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentRight;
            [cell2.contentView addSubview:timeLabel];
            
            IFTweetLabel *descriptionLabel1 = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,86,278,15)];
            
            descriptionLabel1.font = [UIFont systemFontOfSize:10.0];
            IFTweetLabel *descriptionLabel2 = [[IFTweetLabel alloc]initWithFrame:CGRectMake(15,101,278,15)];
            descriptionLabel2.font = [UIFont systemFontOfSize:10.0];
            IFTweetLabel *descriptionLabel3 = [[IFTweetLabel alloc]initWithFrame:CGRectMake(15,116,240,15)];
            descriptionLabel3.font = [UIFont systemFontOfSize:10.0];
            IFTweetLabel *descriptionLabel4 = [[IFTweetLabel alloc]initWithFrame:CGRectMake(15,131,240,15)];
            descriptionLabel4.font = [UIFont systemFontOfSize:10.0];
            smile=[login smilyString:[comArray objectAtIndex:0]];
            [descriptionLabel1 setText:smile];
            [descriptionLabel1 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:1]];
            [descriptionLabel2 setText:smile];
            [descriptionLabel2 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:2]];
            [descriptionLabel3 setText:smile];
            [descriptionLabel3 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:3]];
            [descriptionLabel4 setText:smile];
            [descriptionLabel4 setLinksEnabled:TRUE];
            
            [cell2 addSubview:descriptionLabel1];
            [cell2 addSubview:descriptionLabel2];
            [cell2 addSubview:descriptionLabel3];
            [cell2 addSubview:descriptionLabel4];
            
            
            UILabel *likeShow = nil;
            
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(26,155,43,10)] autorelease];
            likeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:12.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cell2 addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(80,155,30,10)] autorelease];
            dislikeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:12.0];
            dislikeShow.tag = lbltag;
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cell2 addSubview:dislikeShow];
            
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(134,155,43,10)] autorelease];
            commentShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:12.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cell2 addSubview:commentShow];
            //playButton.enabled=FALSE;
            playButton.userInteractionEnabled=NO;
            //******************************************************************************
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            likeBtn.frame=CGRectMake(26,165,44,30);
            [likeBtn addTarget:self action:@selector(likeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cell2 addSubview:likeBtn];
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            dislikeBtn.frame=CGRectMake(80,165,44,30);
            [dislikeBtn addTarget:self action:@selector(dislikeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[dislikesNameArray objectAtIndex:indexPath.row]);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            [cell2 addSubview:likeBtn];
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cell2 addSubview:dislikeBtn];
            
            
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"commentbutton2.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(134,165,44,30)];
            [commentButton addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell2.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"share_button2.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(188,165,44,30)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell2.contentView addSubview:shareButton];
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"bottom-setting2.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(242,165,44,30)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell2.contentView addSubview:settingsButton];
            
            UIButton *moreButtonS=nil;
            moreButtonS = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonS.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonS setBackgroundImage:more forState:UIControlStateNormal];
            moreButtonS.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [moreButtonS setFrame:CGRectMake(280,135,30,22)];
            [moreButtonS addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell2.contentView addSubview:moreButtonS];
            ce=cell2;
        }
    }
    //if([content isEqualToString:@"image"])
    else if([typeString isEqualToString:@"video"])
    {
        
        NSLog(@"hiiiiiiiiii videoooooo");
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)){
            NSLog(@"helllloooo");
            if (cellvideo1 == nil)
            {
                cellvideo1 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:video1] autorelease];
                NSLog(@"index path in cell3 %i",indexPath.row);
            }
            cellvideo1.backgroundView = [ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cellWhiteBg.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];
            introw=indexPath.row+1;
            videoUrl=[dataUpdate objectAtIndex:in];
            playButton.userInteractionEnabled=YES;
            playButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *play = [UIImage imageNamed:@"thumbVideo.png"];
            [playButton setBackgroundImage:play forState:UIControlStateNormal];
            playButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [playButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [playButton setFrame:CGRectMake(15,12,290,190)];
            [playButton addTarget:self action:@selector(playButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellvideo1.contentView addSubview:playButton];
            //int lbltag =indexPath.row;
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,215,40,40)];
            imageView2.layer.cornerRadius = 5;
            imageView2.clipsToBounds = YES;
            [cellvideo1.contentView addSubview:imageView2];
            
            NSString *picUrlString =getImageSearched;
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"editPic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData2 = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData2)
                        image = [UIImage imageWithData:imageData2];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            
            [cellvideo1.contentView addSubview:imageView2];
            NSLog(@"valid");
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(70,215,100,40)];
            nameLabel.numberOfLines=0;
            nameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:13];
            NSString *smile3=getUserSearched;
            [nameLabel setText:smile3];
            [nameLabel setLinksEnabled:TRUE];
            [cellvideo1.contentView addSubview:nameLabel];
            
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,215,100,10)];
            placeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            placeLabel.font = [UIFont systemFontOfSize:8.0];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cellvideo1.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,225,100,10)];
            timeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            timeLabel.font = [UIFont systemFontOfSize:8.0];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentRight;
            [cellvideo1.contentView addSubview:timeLabel];
            
            NSString *smile;
            
            IFTweetLabel *descriptionLabel1=[[IFTweetLabel alloc]initWithFrame:CGRectMake(15,263,278,15)];
            descriptionLabel1.font = [UIFont systemFontOfSize:10.0];
            NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
            NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
            if ([comArray count]==2) {
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
            }
            [cellvideo1.contentView addSubview:descriptionLabel1];
            
            
            UILabel *likeShow = nil;
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(26,288,43,10)] autorelease];
            likeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:12.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cellvideo1 addSubview:likeShow];
            
            
            UILabel *dislikeShow = nil;
            UIButton *dislikeBtn = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(80,288,43,10)] autorelease];
            dislikeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:12.0];
            dislikeShow.tag = lbltag;
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cellvideo1 addSubview:dislikeShow];
            
            UILabel *commentShow = nil;
            NSLog(@"likes from in %@",[likearray objectAtIndex:in]);
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(134,288,43,10)] autorelease];
            commentShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:12.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cellvideo1 addSubview:commentShow];
            //******************************************************************************
            UIButton *likeBtn=nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            [likeBtn addTarget:self action:@selector(likeVideo:) forControlEvents:UIControlEventTouchUpInside];
            likeBtn.frame=CGRectMake(26,300,44,30);
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            NSLog(@"Confimation returned %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cellvideo1 addSubview:likeBtn];
            
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            [dislikeBtn addTarget:self action:@selector(dislikeVideo:) forControlEvents:UIControlEventTouchUpInside];
            dislikeBtn.frame=CGRectMake(80,300,44,30);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cellvideo1 addSubview:dislikeBtn];
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"commentbutton2.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(134,300,44,30)];
            [commentButton addTarget:self action:@selector(moreButtonVideoClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellvideo1.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"share_button2.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(188,300,44,30)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellvideo1.contentView addSubview:shareButton];
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"bottom-setting2.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(242,300,44,30)];
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            settingsButton.tag=lbltag;
            [cellvideo1.contentView addSubview:settingsButton];
            
            
            UIButton *moreButtonV=nil;
            moreButtonV = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonV.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonV setBackgroundImage:more forState:UIControlStateNormal];
            
            [moreButtonV setFrame:CGRectMake(280,260,30,22)];
            [moreButtonV addTarget:self action:@selector(moreButtonVideoClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellvideo1.contentView addSubview:moreButtonV];
            
            ce=cellvideo1;
            
        }
        else if([comArray count]==3)
        {
            
            if (cellvideo2 == nil)
            {
                cellvideo2 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:video2] autorelease];
                NSLog(@"index path in cell3 %i",indexPath.row);
            }
            cellvideo2.backgroundView = [ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cellWhiteBg.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];
            introw=indexPath.row+1;
            videoUrl=[dataUpdate objectAtIndex:in];
            //playButton.enabled=TRUE;
            playButton.userInteractionEnabled=YES;
            playButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *play = [UIImage imageNamed:@"thumbVideo.png"];
            [playButton setBackgroundImage:play forState:UIControlStateNormal];
            playButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [playButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [playButton setFrame:CGRectMake(15,12,290,190)];
            [playButton addTarget:self action:@selector(playButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellvideo2.contentView addSubview:playButton];
            //int lbltag =indexPath.row;
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,215,40,40)];
            imageView2.layer.cornerRadius = 5;
            imageView2.clipsToBounds = YES;
            [cellvideo2.contentView addSubview:imageView2];
            
            NSString *picUrlString =getImageSearched;
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"editPic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData2 = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData2)
                        image = [UIImage imageWithData:imageData2];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            
            [cellvideo2.contentView addSubview:imageView2];
            NSLog(@"valid");
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(70,215,100,40)];
            nameLabel.numberOfLines=0;
            nameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:13];
            NSString *smile3=getUserSearched;
            [nameLabel setText:smile3];
            [nameLabel setLinksEnabled:TRUE];
            [cellvideo2.contentView addSubview:nameLabel];
            
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,215,100,10)];
            placeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            placeLabel.font = [UIFont systemFontOfSize:8.0];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cellvideo2.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,225,100,10)];
            timeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            timeLabel.font = [UIFont systemFontOfSize:8.0];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentRight;
            [cellvideo2.contentView addSubview:timeLabel];
            
            NSString *smile;
            
            IFTweetLabel *descriptionLabel1=[[IFTweetLabel alloc]initWithFrame:CGRectMake(15,263,278,15)];
            descriptionLabel1.font = [UIFont systemFontOfSize:10.0];
            IFTweetLabel *descriptionLabel2=[[IFTweetLabel alloc]initWithFrame:CGRectMake(15,278,278,15)];
            descriptionLabel2.font = [UIFont systemFontOfSize:10.0];
            smile=[login smilyString:[comArray objectAtIndex:0]];
            [descriptionLabel1 setText:smile];
            [descriptionLabel1 setLinksEnabled:TRUE];
            
            smile=[login smilyString:[comArray objectAtIndex:1]];
            [descriptionLabel2 setText:smile];
            [descriptionLabel2 setLinksEnabled:TRUE];
            
            
            [cellvideo2.contentView addSubview:descriptionLabel1];
            [cellvideo2.contentView addSubview:descriptionLabel2];
            
            
            UILabel *likeShow = nil;
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(26,303,43,10)] autorelease];
            likeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:12.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cellvideo2 addSubview:likeShow];
            
            
            UILabel *dislikeShow = nil;
            UIButton *dislikeBtn = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(80,303,43,10)] autorelease];
            dislikeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:12.0];
            dislikeShow.tag = lbltag;
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cellvideo2 addSubview:dislikeShow];
            
            UILabel *commentShow = nil;
            NSLog(@"likes from in %@",[likearray objectAtIndex:in]);
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(134,303,43,10)] autorelease];
            commentShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:12.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cellvideo2 addSubview:commentShow];
            //******************************************************************************
            UIButton *likeBtn=nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            [likeBtn addTarget:self action:@selector(likeVideo:) forControlEvents:UIControlEventTouchUpInside];
            likeBtn.frame=CGRectMake(26,315,44,30);
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            NSLog(@"Confimation returned %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cellvideo2 addSubview:likeBtn];
            
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            [dislikeBtn addTarget:self action:@selector(dislikeVideo:) forControlEvents:UIControlEventTouchUpInside];
            dislikeBtn.frame=CGRectMake(80,315,44,30);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cellvideo2 addSubview:dislikeBtn];
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"commentbutton2.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(134,315,44,30)];
            [commentButton addTarget:self action:@selector(moreButtonVideoClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellvideo2.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"share_button2.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(188,315,44,30)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellvideo2.contentView addSubview:shareButton];
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"bottom-setting2.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(242,315,44,30)];
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            settingsButton.tag=lbltag;
            [cellvideo2.contentView addSubview:settingsButton];
            
            
            UIButton *moreButtonV=nil;
            moreButtonV = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonV.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonV setBackgroundImage:more forState:UIControlStateNormal];
            
            [moreButtonV setFrame:CGRectMake(280,285,30,22)];
            [moreButtonV addTarget:self action:@selector(moreButtonVideoClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell3.contentView addSubview:moreButtonV];
            
            ce=cellvideo2;
            
            
        }
        else if(([comArray count]==4)||([comArray count]==5))
        {
            
            if (cell3 == nil)
            {
                cell3 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier3] autorelease];
                NSLog(@"index path in cell3 %i",indexPath.row);
            }
            cell3.backgroundView = [ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cellWhiteBg.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];
            
            videoUrl=[dataUpdate objectAtIndex:in];
            //playButton.enabled=TRUE;
            playButton.userInteractionEnabled=YES;
            playButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *play = [UIImage imageNamed:@"thumbVideo.png"];
            [playButton setBackgroundImage:play forState:UIControlStateNormal];
            playButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [playButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [playButton setFrame:CGRectMake(15,12,290,190)];
            [playButton addTarget:self action:@selector(playButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell3.contentView addSubview:playButton];
            //int lbltag =indexPath.row;
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,215,40,40)];
            imageView2.layer.cornerRadius = 5;
            imageView2.clipsToBounds = YES;
            [cell3.contentView addSubview:imageView2];
            
            NSString *picUrlString = getImageSearched;
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"editPic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData2 = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData2)
                        image = [UIImage imageWithData:imageData2];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            NSLog(@"valid");
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(70,215,100,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            nameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:13];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.text=getUserSearched;
            [cell3.contentView addSubview:nameLabel];
            
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,215,100,10)];
            placeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            placeLabel.font = [UIFont systemFontOfSize:8.0];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cell3.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,225,100,10)];
            timeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            timeLabel.font = [UIFont systemFontOfSize:8.0];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentRight;
            [cell3.contentView addSubview:timeLabel];
            
            NSString *smile;
            
            IFTweetLabel *descriptionLabel1=[[IFTweetLabel alloc]initWithFrame:CGRectMake(15,263,278,15)];
            descriptionLabel1.font = [UIFont systemFontOfSize:10.0];
            IFTweetLabel *descriptionLabel2=[[IFTweetLabel alloc]initWithFrame:CGRectMake(15,278,278,15)];
            descriptionLabel2.font = [UIFont systemFontOfSize:10.0];
            IFTweetLabel *descriptionLabel3=[[IFTweetLabel alloc]initWithFrame:CGRectMake(15,293,278,15)];
            descriptionLabel3.font = [UIFont systemFontOfSize:10.0];
            IFTweetLabel *descriptionLabel4=[[IFTweetLabel alloc]initWithFrame:CGRectMake(15,308,278,15)];
            descriptionLabel4.font = [UIFont systemFontOfSize:10.0];
            smile=[login smilyString:[comArray objectAtIndex:0]];
            [descriptionLabel1 setText:smile];
            [descriptionLabel1 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:1]];
            [descriptionLabel2 setText:smile];
            [descriptionLabel2 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:2]];
            [descriptionLabel3 setText:smile];
            [descriptionLabel3 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:3]];
            [descriptionLabel4 setText:smile];
            [descriptionLabel4 setLinksEnabled:TRUE];
            
            [cell3.contentView addSubview:descriptionLabel1];
            [cell3.contentView addSubview:descriptionLabel2];
            [cell3.contentView addSubview:descriptionLabel3];
            [cell3.contentView addSubview:descriptionLabel4];
            
            UILabel *likeShow = nil;
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(26,330,43,10)] autorelease];
            likeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:12.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cell3 addSubview:likeShow];
            
            
            UILabel *dislikeShow = nil;
            UIButton *dislikeBtn = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(80,330,43,10)] autorelease];
            dislikeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:12.0];
            dislikeShow.tag = lbltag;
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cell3 addSubview:dislikeShow];
            
            UILabel *commentShow = nil;
            NSLog(@"likes from in %@",[likearray objectAtIndex:in]);
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(134,330,43,10)] autorelease];
            commentShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:12.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cell3 addSubview:commentShow];
            //******************************************************************************
            UIButton *likeBtn=nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            //likeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            likeBtn.tag = indexPath.row+1;
            [likeBtn addTarget:self action:@selector(likeVideo:) forControlEvents:UIControlEventTouchUpInside];
            likeBtn.frame=CGRectMake(26,340,44,30);
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            NSLog(@"Confimation returned %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
                
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
                
            }
            [cell3 addSubview:likeBtn];
            
            
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            dislikeBtn.tag = indexPath.row+1;
            [dislikeBtn addTarget:self action:@selector(dislikeVideo:) forControlEvents:UIControlEventTouchUpInside];
            dislikeBtn.frame=CGRectMake(80,340,44,30);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
                
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cell3 addSubview:dislikeBtn];
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"commentbutton2.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(134,340,44,30)];
            [commentButton addTarget:self action:@selector(moreButtonVideoClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell3.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"share_button2.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(188,340,44,30)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell3.contentView addSubview:shareButton];
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"bottom-setting2.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(242,340,44,30)];
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            settingsButton.tag=lbltag;
            [cell3.contentView addSubview:settingsButton];
            
            
            UIButton *moreButtonV=nil;
            moreButtonV = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonV.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonV setBackgroundImage:more forState:UIControlStateNormal];
            
            [moreButtonV setFrame:CGRectMake(280,310,30,22)];
            [moreButtonV addTarget:self action:@selector(moreButtonVideoClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell3.contentView addSubview:moreButtonV];
            
            ce=cell3;
        }
    }
    else if ([typeString isEqualToString:@"more"]) {
		
		cell4 = [tableView dequeueReusableCellWithIdentifier:moreCellId];
		if (cell4 == nil) {
			cell4 = [[[UITableViewCell alloc]
                      initWithStyle:UITableViewCellStyleDefault
                      reuseIdentifier:moreCellId] autorelease];
		}
        UIActivityIndicatorView * activityindicator1 = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 5, 30, 30)];
        [activityindicator1 setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
        [activityindicator1 setColor:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1]];
        [cell4 addSubview:activityindicator1];
        [activityindicator1 startAnimating];
		ce=cell4;
		
	}
    
    else if ([typeString isEqualToString:@"nomore"]) {
		
		cell4 = [tableView dequeueReusableCellWithIdentifier:moreCellId];
		if (cell4 == nil) {
			cell4 = [[[UITableViewCell alloc]
                      initWithStyle:UITableViewCellStyleDefault
                      reuseIdentifier:moreCellId] autorelease];
		}
		cell4.textLabel.text =[dataUpdate objectAtIndex:indexPath.row];
		cell4.textLabel.textColor = [UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1];
		cell4.textLabel.font = [UIFont boldSystemFontOfSize:14];
		ce=cell4;
		
	}
    
    
    return ce;
    
    
}
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //[self refreshData];
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == [dataUpdate count]-1)
    {
        NSLog(@"Dragged table ************");
        [self refreshData];
        
    }
}
-(IBAction)settingButtonClicked:(id)sender
{
    self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:3];
    
}

//***********************************Like Methods****************************************************
-(void)likeFn:(UIButton*)btnClicked
{
    if([[likesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=story&username=%@&action=liked",indexrowtosend,userLogin];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        int likeCount=[[json valueForKey:@"count"] intValue];
        [likearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [likesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        [mytable reloadData];
    }
    
}
-(void)likeImage:(UIButton*)btnClicked
{
    if([[likesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=images&username=%@&action=liked",indexrowtosend,userLogin];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        int likeCount=[[json valueForKey:@"count"] intValue];
        [likearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [likesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        [mytable reloadData];
    }
}
-(void)likeVideo:(UIButton*)btnClicked
{
    if([[likesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=video&username=%@&action=liked",indexrowtosend,userLogin];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        int likeCount=[[json valueForKey:@"count"] intValue];
        [likearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [likesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        [mytable reloadData];
    }
}

//****************************DisLike Methods**********************************
-(void)dislikeFn:(UIButton*)btnClicked
{
    if([[dislikesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=story&username=%@&action=dislike",indexrowtosend,userLogin];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        int likeCount=[[json valueForKey:@"count"] intValue];
        [dislikearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [dislikesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        [mytable reloadData];
    }
}
-(void)dislikeImage:(UIButton*)btnClicked
{
    if([[dislikesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=images&username=%@&action=dislike",indexrowtosend,userLogin];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        int likeCount=[[json valueForKey:@"count"] intValue];
        [dislikearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [dislikesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        [mytable reloadData];
    }
}
-(void)dislikeVideo:(UIButton*)btnClicked
{
    if([[dislikesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=video&username=%@&action=dislike",indexrowtosend,userLogin];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        int likeCount=[[json valueForKey:@"count"] intValue];
        [dislikearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [dislikesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        [mytable reloadData];
    }
}

//********************* SEE MORE Mehthods **************************************
-(IBAction)moreButtonImageClicked:(UIButton*)btnClicked
{
    NSLog(@"Content Type %@",[dataType objectAtIndex:btnClicked.tag-1]);
    NSString *setImageData = [dataUpdate objectAtIndex:btnClicked.tag-1];
    imageDetailView *imageDetail=[self.storyboard instantiateViewControllerWithIdentifier:@"imageDetail"];
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    navBar.hidden=false;
    imageDetail.hidesBottomBarWhenPushed = YES;
    imageDetail.getImageData=setImageData;
    imageDetail.getUserImageData=getImageSearched;
    imageDetail.getName=getUserSearched;
    imageDetail.getSno=[snoArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getIfLikes=[likesNameArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getLikes=[likearray objectAtIndex:btnClicked.tag-1];
    imageDetail.getIfDisLikes=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getDisLikes=[dislikearray objectAtIndex:btnClicked.tag-1];
    imageDetail.getComments=[commentCounts objectAtIndex:btnClicked.tag-1];
    imageDetail.getType=[dataType objectAtIndex:btnClicked.tag-1];
    imageDetail.getDislikeConferm=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getLikeConferm=[likesNameArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getLocation=[locationArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getTime=[timeArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getRowIndex=[NSString stringWithFormat:@"%i",btnClicked.tag-1];
    imageDetail.getTabId=@"profile3";
    [self.navigationController pushViewController:imageDetail animated:YES];
}
-(IBAction)moreButtonStoryClicked:(UIButton*)btnClicked
{
    NSLog(@"Content Type %@",[dataType objectAtIndex:btnClicked.tag-1]);
    NSString *setStoryString = [dataUpdate objectAtIndex:btnClicked.tag-1];
    storyDetailView *storyDetail=[self.storyboard instantiateViewControllerWithIdentifier:@"storyDetail"];
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    navBar.hidden=false;
    storyDetail.hidesBottomBarWhenPushed = YES;
    storyDetail.getStory=setStoryString;
    storyDetail.getUserImageData=getImageSearched;
    storyDetail.getName=getUserSearched;
    storyDetail.getSno=[snoArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getIfLikes=[likesNameArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getLikes=[likearray objectAtIndex:btnClicked.tag-1];
    storyDetail.getIfDisLikes=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getDisLikes=[dislikearray objectAtIndex:btnClicked.tag-1];
    storyDetail.getComments=[commentCounts objectAtIndex:btnClicked.tag-1];
    storyDetail.getType=[dataType objectAtIndex:btnClicked.tag-1];
    storyDetail.getDislikeConferm=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getLikeConferm=[likesNameArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getLocation=[locationArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getTime=[timeArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getRowIndex=[NSString stringWithFormat:@"%i",btnClicked.tag-1];
    storyDetail.getTabId=@"profile3";
    [self.navigationController pushViewController:storyDetail animated:YES];
}
-(IBAction)moreButtonVideoClicked:(UIButton*)btnClicked
{
    NSLog(@"Content Type %@",[dataType objectAtIndex:btnClicked.tag-1]);
    videoDetailView *videoDetail=[self.storyboard instantiateViewControllerWithIdentifier:@"videoDetail"];
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    navBar.hidden=false;
    videoDetail.hidesBottomBarWhenPushed = YES;
    videoDetail.videoUrl=videoUrl;
    videoDetail.getUserImageData=getImageSearched;
    videoDetail.getName=getUserSearched;
    videoDetail.getSno=[snoArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getIfLikes=[likesNameArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getLikes=[likearray objectAtIndex:btnClicked.tag-1];
    videoDetail.getIfDisLikes=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getDisLikes=[dislikearray objectAtIndex:btnClicked.tag-1];
    videoDetail.getComments=[commentCounts objectAtIndex:btnClicked.tag-1];
    videoDetail.getType=[dataType objectAtIndex:btnClicked.tag-1];
    videoDetail.getDislikeConferm=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getLikeConferm=[likesNameArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getLocation=[locationArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getTime=[timeArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getRowIndex=[NSString stringWithFormat:@"%i",btnClicked.tag-1];
    videoDetail.getTabId=@"profile3";
    [self.navigationController pushViewController:videoDetail animated:YES];
}


//************************************************************************************************
- (void)reloadRowsAtIndexPaths:(NSArray *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation
{
    NSLog(@"cell reloaded");
}

-(void)shareButtonClicked:(UIButton*)btnClicked{
    NSLog(@"data type %@",[dataType objectAtIndex:btnClicked.tag-1]);
    NSString *type=[dataType objectAtIndex:btnClicked.tag-1];
    NSLog(@"Helllooo activity viewcontroller");
    
    NSArray *activityItems = NO;
    if([type isEqualToString:@"image"])
    {
        UIImage *image=[UIImage imageWithData:[dataUpdate objectAtIndex:btnClicked.tag-1]];
        if (image != nil) {
            activityItems = @[@"hello activityviewcontroller",image];
        }
        
        else {
            activityItems = @[[dataUpdate objectAtIndex:btnClicked.tag-1]];
        }
    }
    else if([type isEqualToString:@"text"]){
        activityItems = @[[dataUpdate objectAtIndex:btnClicked.tag-1]];
    }
    else if([type isEqualToString:@"video"]){
        // NSString *myString = [[dataUpdate objectAtIndex:btnClicked.tag-1] absoluteString];
        // NSLog(@"nsurl String %@",myString);
        activityItems = @[[dataUpdate objectAtIndex:btnClicked.tag-1]];
    }
    UIActivityViewController *activityController =
    [[UIActivityViewController alloc]
     initWithActivityItems:activityItems
     applicationActivities:nil];
    
    [self presentViewController:activityController
                       animated:YES completion:nil];
    
}


-(IBAction)reportButtonClicked:(UIButton*)btnClicked
{
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    delegate.snoReport=[snoArray objectAtIndex:btnClicked.tag-1];
    delegate.typeReport=[dataType objectAtIndex:btnClicked.tag-1];
    UIActionSheet  *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Report for Content" delegate:self
                                                     cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Inappropriate"
                                                     otherButtonTitles:@"Unfollow user",nil];
    actionSheet.backgroundColor = [UIColor greenColor];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet showInView:self.view];
    //[actionSheet showFromTabBar:self.tabBarController.tabBar];
}
-(void)actionSheet:(UIActionSheet *)actionSheet2 clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == [actionSheet2 destructiveButtonIndex]) {
        [self performSegueWithIdentifier:@"searchUserToReport" sender:self];
    } else if(buttonIndex == 1){
        NSString *followUrl=[NSString stringWithFormat:SERVER_URL@"/unfollowing.do?username=%@&following=%@",userLogin,getUserSearched];
        NSURL *furl=[NSURL URLWithString:followUrl];
        NSLog(@"Unfolloeing user url %@",furl);
        NSError* error;
        NSData *Data=[NSData dataWithContentsOfURL:furl];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:Data options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
    }
}

-(IBAction)playButtonClicked:(id)sender
{
    NSLog(@"VIDEO URL IN PLAY BUTTON %@",videoUrl);
    NSURL *video=[NSURL URLWithString:videoUrl];
    _movieplayer = [[MPMoviePlayerController alloc] initWithContentURL:video];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:_movieplayer];
    
    _movieplayer.controlStyle = MPMovieControlStyleDefault;
    _movieplayer.shouldAutoplay = YES;
    [self.view addSubview:_movieplayer.view];
    [_movieplayer setFullscreen:YES animated:YES];
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    MPMoviePlayerController *player = [notification object];
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:player];
    
    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
}

-(void)profilePullData
{
    Login *login=[[Login alloc]init];
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    NSLog(@"datao time inserted at Circle %@",delegate.getInertedTime);
    NSString *converted = [delegate.getInertedTime stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:SERVER_URL@"/profile_pull.do?username=%@&old_time=%@",getUserSearched,converted];
    NSURL *urlrequest=[NSURL URLWithString:url];
    NSURLRequest *req=[[NSURLRequest alloc]initWithURL:urlrequest];
    NSURLResponse  *response = nil;
    NSError* error;
    [NSURLConnection sendSynchronousRequest: req returningResponse: &response error: &error];
    
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    NSData * dataofStrig =[NSData dataWithContentsOfURL:urlrequest];
    if (!dataofStrig) {
        NSLog(@"No Data");
    }
    else{
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions                        error:&error];
        
        NSLog(@"JSON format:- %@",json);
        NSLog(@"time inserted%@",[[json objectForKey:@"data0"] valueForKey:@"inserted_at"]);
        NSLog(@"time delegate inserted%@",delegate.getInertedTime);
        if([PgetInertedTime isEqualToString:[insertedTimeArray objectAtIndex:0]])
        {
            
        }
        else{
            
            int length=[json count];
            NSLog(@"JSON count:- %i",length);
            if (length>5) {
                NSString *indexStr = [NSString stringWithFormat:@"%d",(length-6)];
                NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
                PgetInertedTime=[[json objectForKey:data] valueForKey:@"inserted_at"];
            }
            for(int i=0;i<(length-5);i++)    {
                
                NSString *indexStr = [NSString stringWithFormat:@"%d",i];
                NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
                [commentCounts insertObject:[[json objectForKey:data]valueForKey:@"comment"] atIndex:0];
                NSString *likesName=[[json objectForKey:data] valueForKey:@"likenames"];
                NSString *confrm=[login likesContent:likesName];
                [likesNameArray insertObject:confrm atIndex:0];
                NSString *dislikesName=[[json objectForKey:data] valueForKey:@"dislikenames"];
                NSString *disconfrm=[login likesContent:dislikesName];
                [dislikesNameArray insertObject:disconfrm atIndex:0];
                dataStr=[[json valueForKey:data] valueForKey:@"content1"];
                dataStr2=[[json valueForKey:data] valueForKey:@"content2"];
                NSString *sno=[[json valueForKey:data]valueForKey:@"sno"];
                [snoArray insertObject:sno atIndex:0];
                NSString *likeString = [[json valueForKey:data]valueForKey:@"like"];
                [likearray insertObject:likeString atIndex:0];
                NSString *dislikeString = [[json valueForKey:data]valueForKey:@"dislike"];
                [dislikearray insertObject:dislikeString atIndex:0];
                if ([[[json objectForKey:data] valueForKey:@"Elapsed"] isEqualToString:@""])
                {[timeArray insertObject:@"Not Updated" atIndex:0];
                }
                else
                {[timeArray insertObject:[[json objectForKey:data] valueForKey:@"Elapsed"] atIndex:0];
                }
                if([[[json objectForKey:data] valueForKey:@"location"] isEqualToString:@""])
                {[locationArray insertObject:@"Not Updated" atIndex:0];
                }
                else
                { [locationArray insertObject:[[json objectForKey:data] valueForKey:@"location"] atIndex:0];
                }
                
                NSLog(@"content from url %@",dataStr);
                NSString *type=[[json objectForKey:data]valueForKey:@"type"];
                [dataType insertObject:type atIndex:0];
                [dataUpdate insertObject:dataStr atIndex:0];
                [dataUpdate2 insertObject:dataStr2 atIndex:0];
                
            }
            
        }
    }
    [mytable reloadData];
}

-(void)refreshProfileData
{
    Login *login=[[Login alloc]init];
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    NSLog(@"datao time inserted at Circle %@",delegate.SgetLastInseted);
    NSString *converted = [delegate.SgetLastInseted stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    //PgetLastInertedTime=nil;
    NSString *url = [NSString stringWithFormat:SERVER_URL@"/profilemore.do?username=%@&old_time=%@",getUserSearched,converted];
    NSURL *urlrequest=[NSURL URLWithString:url];
    NSLog(@"requested url is $$$$$$$$$$$$$$$$##:- %@",urlrequest);
    NSError* error;
    NSData *receivedData2=[NSData dataWithContentsOfURL:urlrequest];
    if(!receivedData2 )
    {
        [indicator stopAnimating];
        indicator.hidden=YES;
        NSLog(@"errorrrrr no response");
        UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Connection failed" message:@"Check intenet connection" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
        [alert1 show];
    }
    else{
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:receivedData2 options:kNilOptions error:&error];
        NSLog(@"JSON format:- %@",json);
        int length=[json count];
        NSLog(@"JSON count:- %i",length);
        if (length>5) {
            NSString *indexStr = [NSString stringWithFormat:@"%d",(length-6)];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            delegate.SgetLastInseted=[[json objectForKey:data] valueForKey:@"inserted_at"];        }
        // else{
        
        for(int i=0;i<length-5;i++)
        {
            NSString *indexStr = [NSString stringWithFormat:@"%d",i];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            PgetLastInertedTime=[[json objectForKey:data] valueForKey:@"inserted_at"];
            [commentCounts insertObject:[[json objectForKey:data]valueForKey:@"comment"] atIndex:[commentCounts count]-1];
            NSString *likesName=[[json objectForKey:data] valueForKey:@"likenames"];
            NSString *confrm=[login likesContent:likesName];
            [likesNameArray insertObject:confrm atIndex:[likesNameArray count]-1];
            NSString *dislikesName=[[json objectForKey:data] valueForKey:@"dislikenames"];
            NSString *disconfrm=[login likesContent:dislikesName];               
            [dislikesNameArray insertObject:disconfrm atIndex:[dislikesNameArray count]-1];
            NSString *snoString = [[json valueForKey:data]valueForKey:@"sno"];               
            [snoArray insertObject:snoString atIndex:[snoArray count]-1];
            NSString *likeString = [[json valueForKey:data]valueForKey:@"like"];               
            [likearray insertObject:likeString atIndex:[likearray count]-1];
            NSString *dislikeString = [[json valueForKey:data]valueForKey:@"dislike"];                
            [dislikearray insertObject:dislikeString atIndex:[dislikearray count]-1];
            if ([[[json objectForKey:data] valueForKey:@"Elapsed"] isEqualToString:@""])
            {[timeArray insertObject:@"Not Updated" atIndex:timeArray.count-1];
            }
            else
            {[timeArray insertObject:[[json objectForKey:data] valueForKey:@"Elapsed"] atIndex:timeArray.count-1];
            }
            if([[[json objectForKey:data] valueForKey:@"location"] isEqualToString:@""])
            { [locationArray insertObject:@"Not Updated" atIndex:locationArray.count-1];
            }
            else
            {  [locationArray insertObject:[[json objectForKey:data] valueForKey:@"location"] atIndex:locationArray.count-1];
            }
            dataStr=[[json valueForKey:data] valueForKey:@"content1"];
            dataStr2=[[json valueForKey:data] valueForKey:@"content2"];
            NSLog(@"content from url %@",dataStr);
            NSString *type=[[json objectForKey:data]valueForKey:@"type"];
            
            [dataType insertObject:type atIndex:dataType.count-1];
            [dataUpdate insertObject:dataStr atIndex:dataUpdate.count-1];
            [dataUpdate2 insertObject:dataStr2 atIndex:dataUpdate2.count-1];
            NSString *fourcomment=[[json objectForKey:data]valueForKey:@"commentiphone"];
            NSLog(@"fourcomment %@",fourcomment);
            if ([fourcomment length]==0) {
                [fourCommentArray insertObject:@"No Comments" atIndex:fourCommentArray.count-1];
            }
            else{
                [fourCommentArray insertObject:fourcomment atIndex:fourCommentArray.count-1];
            }
            
        }
    }
    // }
    NSLog(@"Last inserted time %@",delegate.SgetLastInseted);
}


- (void)viewWillDisappear:(BOOL)animated {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:IFTweetLabelURLNotification object:nil];
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    navBar.hidden=false;
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dealloc {
    [_profileback release];
    [trans release];
    [_followreq release];
    [blurView release];
    [_mainScroll release];
    [super dealloc];
}
@end
