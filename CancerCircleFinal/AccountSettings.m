//
//  AccountSettings.m
//  Cancerosity
//
//  Created by Grant Bevan on 31/03/2014.
//  Copyright (c) 2014 Raminder. All rights reserved.
//

#import "AccountSettings.h"
#import "AppDelegate.h"

@interface AccountSettings ()

@end

@implementation AccountSettings

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    username=delegate.mynameLogin;
    accTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
    //  return 5;
    
}
- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    
    static NSString *CellIdentifier = @"firstNCell";
    static NSString *CellIdentifier2 = @"lastNCell";
    static NSString *CellIdentifier3 = @"emailCell";
    static NSString *CellIdentifier4 = @"phoneCell";
    static NSString *CellIdentifier5 = @"passCell";
    
    accTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    [accTable setBackgroundColor: [[UIColor alloc] initWithRed:(218.0/255.0) green:(221.0/255.0) blue:(226.0/255.0) alpha:1.0]];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
    }
    if(indexPath.row == 0){
        
        self.firstNCell = [tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!self.firstNCell)
        {
            self.firstNCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
            
            AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
            
            UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(15, 32, 100, 10)];
            desc.text = @"First name";
            desc.font = [UIFont systemFontOfSize:10];
            desc.textColor = [UIColor grayColor];
            [self.firstNCell.contentView addSubview:desc];
            
            dpName = [[UITextField alloc] initWithFrame:CGRectMake(15, 8, 200, 30)];
            dpName.backgroundColor = [UIColor clearColor];
            dpName.text=delegate.firstName;
            [dpName setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
            
            
            [self.firstNCell.contentView addSubview:dpName];
            self.firstNCell.accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
            dpNameImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
            dpNameImg.image = [UIImage imageNamed:@"tick.png"];
             dpName.delegate = self;
            [self.firstNCell.accessoryView addSubview:dpNameImg];
            self.firstNCell.accessoryView.backgroundColor = [UIColor clearColor];
            
            
        }
        
        
        return self.firstNCell;
    }
    else if(indexPath.row ==  1){
        
        self.lastNCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (!self.lastNCell)
        {
             self.lastNCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
             AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
            
            
            UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(15, 32, 100, 10)];
            desc.text = @"Last name";
            desc.font = [UIFont systemFontOfSize:10];
            desc.textColor = [UIColor grayColor];
            [self.lastNCell.contentView addSubview:desc];
            
            userName = [[UITextField alloc] initWithFrame:CGRectMake(15, 8, 200, 30)];
            userName.backgroundColor = [UIColor clearColor];
            userName.text=delegate.lastName;
            [userName setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
            userName.returnKeyType = UIReturnKeyDone;
            
           
            [self.lastNCell.contentView addSubview:userName];
            self.lastNCell.accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
            userNameImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
            userNameImg.image = [UIImage imageNamed:@"tick.png"];
            userName.delegate = self;
            [self.lastNCell.accessoryView addSubview:userNameImg];
            self.lastNCell.accessoryView.backgroundColor = [UIColor clearColor];

          
     

            
        }
        
        return self.lastNCell;
    }
    else if(indexPath.row ==  2){
        
        self.emailCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier3];
        if (!self.emailCell)
        {
            self.emailCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
             AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
            
            
            UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(15, 32, 100, 10)];
            desc.text = @"Email address";
            desc.font = [UIFont systemFontOfSize:10];
            desc.textColor = [UIColor grayColor];
            [self.emailCell.contentView addSubview:desc];
            
            
            email = [[UITextField alloc] initWithFrame:CGRectMake(15, 8, 200, 30)];
            email.backgroundColor = [UIColor clearColor];
            email.text=delegate.emailId;
            [email setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
            email.returnKeyType = UIReturnKeyDone;
            
            
            
            [self.emailCell.contentView addSubview:email];
            self.emailCell.accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
            emailImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
            emailImg.image = [UIImage imageNamed:@"tick.png"];
            email.delegate = self;
            [self.emailCell.accessoryView addSubview:emailImg];
            self.emailCell.accessoryView.backgroundColor = [UIColor clearColor];
            

    
            
        }
        
        return self.emailCell;
    }
    else if(indexPath.row ==  3){
        
        self.phoneCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
        if (!self.phoneCell)
        {
            self.phoneCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
            AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
            
            
            
            UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(15, 32, 160, 10)];
            desc.text = @"Mobile phone number";
            desc.font = [UIFont systemFontOfSize:10];
            desc.textColor = [UIColor grayColor];
            [self.phoneCell.contentView addSubview:desc];
            
          
            
            contact = [[UITextField alloc] initWithFrame:CGRectMake(15, 8, 200, 30)];
            contact.backgroundColor = [UIColor clearColor];
            contact.text=delegate.contact;
            [contact setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
            contact.keyboardType=UIKeyboardTypeDecimalPad;
           
            
            [self.phoneCell.contentView addSubview:contact];
            self.phoneCell.accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
            contactImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
            contactImg.image = [UIImage imageNamed:@"tick.png"];
            contact.delegate = self;
            [self.phoneCell.accessoryView addSubview:contactImg];
            self.phoneCell.accessoryView.backgroundColor = [UIColor clearColor];
            
        }
        
        return self.phoneCell;
    }
    else if(indexPath.row ==  4){
        
        self.passCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
        if (!self.passCell)
        {
            self.passCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
            AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
            
            
            
            UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(15, 32, 100, 10)];
            desc.text = @"Password";
            desc.font = [UIFont systemFontOfSize:10];
            desc.textColor = [UIColor grayColor];
            [self.passCell.contentView addSubview:desc];
            
            password = [[UITextField alloc] initWithFrame:CGRectMake(15, 8, 200, 30)];
            password.backgroundColor = [UIColor clearColor];
            password.text=delegate.myPassword;
            [password setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
            password.secureTextEntry=YES;
 
            [self.passCell.contentView addSubview:password];
            self.passCell.accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
            passwordImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
            passwordImg.image = [UIImage imageNamed:@"tick.png"];
            password.delegate = self;
            [self.passCell.accessoryView addSubview:passwordImg];
            self.passCell.accessoryView.backgroundColor = [UIColor clearColor];
            
            
            
            
            
        }
        
        return self.passCell;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 1)
    {

        
        
    }
    if (indexPath.row == 2)
    {

        
        
    }
    if (indexPath.row == 3)
    {
      
        
        
    }
}
-(void)updateInfo
{
    AppDelegate *delegate=[[UIApplication sharedApplication]delegate];

      if (![delegate.lastName isEqualToString:userName.text])     {
        NSLog(@"usename changed");
        [self performSelectorOnMainThread:@selector(changeLastName) withObject:nil waitUntilDone:NO];
    }
  
    if (![delegate.firstName isEqualToString:dpName.text])
    {
        NSLog(@"usename changed");
        [self performSelectorOnMainThread:@selector(changeFirstName) withObject:nil waitUntilDone:NO];
    }
    if (![delegate.myPassword isEqualToString:password.text])
      {
        [self performSelectorOnMainThread:@selector(changePassword) withObject:nil waitUntilDone:NO];
    }
    if (![delegate.contact isEqualToString:contact.text])
           {
        [self performSelectorOnMainThread:@selector(changeNumber) withObject:nil waitUntilDone:NO];
    }
    if (![delegate.emailId isEqualToString:email.text])
        
    {
        [self performSelectorOnMainThread:@selector(changeEmail) withObject:nil waitUntilDone:NO];
    }
    
    
}
-(void)changeFirstName
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate.mynameLogin isEqualToString:userName.text]) {
        
    }
    else{
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/changesetting.do?username=%@&column_name=name_first&newname=%@",username,dpName.text];
        NSLog(@"Url for change username %@",url);
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSData *data=[NSData dataWithContentsOfURL:urlrequest];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        int length=[json count];
        NSLog(@"Objects in JSON %i",length);
        NSLog(@"JSON format:- %@",json);
        NSString *str=[json valueForKey:@"param"];
        NSLog(@"Revert of json for change username %@",str);
    }
    
}

-(void)changeLastName
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate.mynameLogin isEqualToString:userName.text]) {
        
    }
    else{
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/changesetting.do?username=%@&column_name=name_last&newname=%@",username,userName.text];
        NSLog(@"Url for change username %@",url);
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSData *data=[NSData dataWithContentsOfURL:urlrequest];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        int length=[json count];
        NSLog(@"Objects in JSON %i",length);
        NSLog(@"JSON format:- %@",json);
        NSString *str=[json valueForKey:@"param"];
        NSLog(@"Revert of json for change username %@",str);
    }
    
}
-(void)changeEmail
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate.emailId isEqualToString:email.text]) {
        
    }
    else{
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/changesetting.do?username=%@&column_name=email_id&newname=%@",username,email.text];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSData *data=[NSData dataWithContentsOfURL:urlrequest];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        int length=[json count];
        NSLog(@"Objects in JSON %i",length);
        NSLog(@"JSON format:- %@",json);
        NSString *str=[json valueForKey:@"param"];
        NSLog(@"Revert of json %@",str);
    }
    
}
-(void)changeNumber
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate.contact isEqualToString:contact.text]) {
        
    }
    else{
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/changesetting.do?username=%@&column_name=phone&newname=%@",username,contact.text];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSData *data=[NSData dataWithContentsOfURL:urlrequest];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        int length=[json count];
        NSLog(@"Objects in JSON %i",length);
        NSLog(@"JSON format:- %@",json);
        NSString *str=[json valueForKey:@"param"];
        NSLog(@"Revert of json %@",str);
    }
    
}
-(void)changePassword
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate.myPassword isEqualToString:password.text]) {
        
    }
    else{
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/changesetting.do?username=%@&column_name=password&newname=%@",username,password.text];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSData *data=[NSData dataWithContentsOfURL:urlrequest];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        int length=[json count];
        NSLog(@"Objects in JSON %i",length);
        NSLog(@"JSON format:- %@",json);
        NSString *str=[json valueForKey:@"param"];
        NSLog(@"Revert of json %@",str);
    }
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //[self circleData]; // 1
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            [self updateInfo];// 2
        });
    });
    
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(([dpName.text length]>16)||([dpName.text length]<2))
    {
        [dpNameImg setHidden:FALSE];
        dpNameImg.image = [UIImage imageNamed:@"error.png"];
    }
    else{
        
        [dpNameImg setHidden:FALSE];
        dpNameImg.image = [UIImage imageNamed:@"tick.png"];
    }
    
    if(([userName.text length]>16)||([userName.text length]<2))
    {
        [userNameImg setHidden:FALSE];
        userNameImg.image = [UIImage imageNamed:@"error.png"];
    }
    else{
        
        [userNameImg setHidden:FALSE];
        userNameImg.image = [UIImage imageNamed:@"tick.png"];
    }
    if([password.text length]<6)
    {
        
        [passwordImg setHidden:FALSE];
        passwordImg.image = [UIImage imageNamed:@"error.png"];
    }
    else
    {
        [passwordImg setHidden:FALSE];
        passwordImg.image = [UIImage imageNamed:@"tick.png"];
    }
    
    if([contact.text length]<10)
    {
        [contactImg setHidden:FALSE];
        contactImg.image = [UIImage imageNamed:@"error.png"];
        
    }
    else{
        [contactImg setHidden:FALSE];
        contactImg.image = [UIImage imageNamed:@"tick.png"];
    }
    if(!([self validateEmailWithString:email.text]))
    {
        [emailImg setHidden:FALSE];
        emailImg.image = [UIImage imageNamed:@"error.png"];
    }
    else
    {
        [emailImg setHidden:FALSE];
        emailImg.image = [UIImage imageNamed:@"tick.png"];
    }
    
    return YES;
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)validateEmailWithString:(NSString*)emailId
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailId];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"touchesBegan:withEvent:");
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    NSLog(@"textViewShouldBeginEditing:");
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    NSLog(@"textViewDidBeginEditing:");
    

    
    
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    NSLog(@"textViewShouldEndEditing:");
    return YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"textViewDidEndEditing:");
    NSMutableAttributedString * string;
    NSArray *words=[textView.text componentsSeparatedByString:@" "];
    /*if([words count]>50)
     {
     alertLimit =[[UIAlertView alloc]initWithTitle:@"Error" message:@"Characters are more than 50" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
     [alertLimit show];
     
     }*/
    
    for (NSString *word in words) {
        if (([word hasPrefix:@"#"])) {
            // NSString *newWord = [word substringFromIndex:[@"@" length]];
            // NSString *newWord=[word stringByReplacingOccurrencesOfString:@"@" withString:@""];
            NSRange range=[textView.text rangeOfString:word];
            NSString *newString=[textView.text stringByReplacingOccurrencesOfString:@"#" withString:@""];
            string = [[NSMutableAttributedString alloc]initWithString:newString];
            // [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
            NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1],NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:13],
                                         NSBackgroundColorAttributeName: [UIColor whiteColor]
                                         };
            [string addAttributes:attributes range:range];
            [textView setAttributedText:string];
            
            
            
        }
    }
    
    double delayInSeconds = 4.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //[self circleData]; // 1
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
       
        });
    });
    
    
}

-(void)textViewDidChange:(UITextView *)textView{
    NSLog(@"textViewDidChange:");
}
- (void)textViewDidChangeSelection:(UITextView *)textView{
    NSLog(@"textViewDidChangeSelection:");
}


- (void)dealloc {
    [accTable release];
    [super dealloc];
}
@end
