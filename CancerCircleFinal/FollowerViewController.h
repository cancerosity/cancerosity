//
//  FollowerViewController.h
//  CancerCircleFinal
//
//  Created by Raminder on 17/05/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "newsCell.h"
#import <QuartzCore/QuartzCore.h>

@interface FollowerViewController : UIViewController
{
    IBOutlet UITableView *myTable;
    NSMutableArray *followerNameArray;
    NSMutableArray *followerPicArray;
    NSMutableArray *followerStatusArray;
}
@property (nonatomic, strong) NSOperationQueue *imageDownloadingQueue;
@property (nonatomic, strong) NSCache *imageCache;
@property(nonatomic,retain)NSString *getName;

@end
