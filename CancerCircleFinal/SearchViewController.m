



//
//  SearchViewController.m
//  CancerCircleFirst
//
//  Created by Raminder on 21/03/13.
//
//

#import "SearchViewController.h"
#import "AppDelegate.h"
#import "searchedUserView.h"
#import "storyDetailView.h"
#import "OpenUrlView.h"
#import "ProfileFirst.h"
#import "Login.h"

@interface SearchViewController ()

@end

@implementation SearchViewController
@synthesize getTagText,getWord,getTabTitle;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.navigationController.navigationBarHidden = NO;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleTweetNotification:) name:IFTweetLabelURLNotification object:nil];
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    delegate.tabId=@"search";
    // [self displayData];
}
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    self.imageDownloadingQueuePic = [[NSOperationQueue alloc] init];
    self.imageDownloadingQueuePic.maxConcurrentOperationCount = 4;
    self.imageCachePic = [[NSCache alloc] init];
    
    NSLog(@"Search Tag Text %@",getTagText);
    
    NSLog(@"search view controller");
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"640x960bg.png"]];
    
    
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"640x960bg.png"]];

    
   
    CGFloat titleWidth2=100;
    CGFloat titleWidth = MIN(titleWidth2, 300);
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 30)];
    label.backgroundColor = [UIColor clearColor];
    // label.font = [UIFont boldSystemFontOfSize:20.0];
    label.font=[UIFont fontWithName:@"Futura_Light" size:20];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=@"Search";
    self.navigationItem.titleView = label;
    

    

    
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    getusername = delegate.mynameLogin;
    search.text = delegate.wordToSearch;
    
    NSString *newWord;
    NSLog(@"Search text  %@",search.text);
    
        if([search.text hasPrefix:@"#"])
        {
            searchType=@"hashTag";
            NSString *removeHash=[search.text  stringByReplacingOccurrencesOfString:@"#" withString:@""];
            newWord=removeHash;
            [self searchDataIndicator:newWord];
            
        }
        else
        {
            searchType=@"users";
            if([search.text hasPrefix:@"@"])
            {
                NSString *removeHash=[search.text stringByReplacingOccurrencesOfString:@"@" withString:@""];
                newWord=removeHash;
                [self searchUserIndicator:newWord];
                
            }
            else{
                newWord=search.text;
                [self searchUserIndicator:newWord];
            }
        
        }

}
-(void)setsearch {
    search.text = _searchtext;
}

- (IBAction)goBack:(id)sender {
     [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTable" object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)goBack
{
    [self dismissViewControllerAnimated:YES completion:nil];
    search.text=@"";
    
}
- (void)handleTweetNotification:(NSNotification *)notification
{
    NSDictionary *dict = (NSDictionary*)notification.object;
    NSString *strDict = [NSString stringWithFormat:@"%@", dict];
    if([strDict hasPrefix:@"http://"])
    {   OpenUrlView *openUrl=[self.storyboard instantiateViewControllerWithIdentifier:@"OpenUrl"];
        //openUrl.hidesBottomBarWhenPushed = YES;
        openUrl.getUrl=strDict;
        [self.navigationController pushViewController:openUrl animated:YES];
    }
    else
    {
        if (([strDict hasPrefix:@"#"])||([strDict hasPrefix:@"@"])) {
            search.text = strDict;
        }
        else
        {
            [self userButtonVideoClicked:strDict];
        }
        
    }
}
-(IBAction)hidekeyboard:(id)sender
{
    [searchText resignFirstResponder];
    
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.text = @"";
    [searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSString *newWord;
    
    NSLog(@"Search text  %@",searchBar.text);
    if([searchBar.text isEqualToString:@""])
    {
        UIAlertView *alertS=[[UIAlertView alloc]initWithTitle:@"No results" message:@"Please Enter #tag and Username" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertS show];
        
    }
    else
    {
        if([searchBar.text hasPrefix:@"#"])
        {
            searchType=@"hashTag";
            NSString *removeHash=[searchBar.text stringByReplacingOccurrencesOfString:@"#" withString:@""];
            newWord=removeHash;
            [self searchDataIndicator:newWord];
            
        }
        else
        {
            searchType=@"users";
            if([searchBar.text hasPrefix:@"@"])
            {
                NSString *removeHash=[searchBar.text stringByReplacingOccurrencesOfString:@"@" withString:@""];
                newWord=removeHash;
                [self searchUserIndicator:newWord];
                
            }
            else{
                newWord=searchBar.text;
                [self searchUserIndicator:newWord];
                
            }
        }
    }
    searchBar.text = @"";
    [searchBar resignFirstResponder];
}

-(IBAction)searchButton
{
    NSString *newWord;
    NSLog(@"Search text  %@",searchText.text);
    if([searchText.text isEqualToString:@""])
    {
        UIAlertView *alertS=[[UIAlertView alloc]initWithTitle:@"No results" message:@"Please Enter #tag and Username" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertS show];
        
    }
    else
    {
        if([searchText.text hasPrefix:@"#"])
        {
            searchType=@"hashTag";
            NSString *removeHash=[searchText.text stringByReplacingOccurrencesOfString:@"#" withString:@""];
            newWord=removeHash;
            [self searchDataIndicator:newWord];
            
        }
        else
        {
            searchType=@"users";
            if([searchText.text hasPrefix:@"@"])
            {
                NSString *removeHash=[searchText.text stringByReplacingOccurrencesOfString:@"@" withString:@""];
                newWord=removeHash;
                [self searchUserIndicator:newWord];
                
            }
            else{
                newWord=searchText.text;
                [self searchUserIndicator:newWord];
                
            }
        }
    }
}
-(IBAction)clearSearch
{
    searchText.text=@"";
}
-(void)searchUserIndicator:(NSString*)newString{
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];//UIActivityIndicatorViewStyleWhiteLarge
    indicator.frame = CGRectMake(self.view.bounds.size.width / 2.0f - indicator.frame.size.width /2.0f, self.view.bounds.size.height / 2.0f - indicator.frame.size.height /2.0f, indicator.frame.size.width, indicator.frame.size.height);
    //indicator.color = [UIColor colorWithRed:1 green:0.8 blue:0.9 alpha:1 ];
    indicator.color = [UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
    CGAffineTransform transform = CGAffineTransformMakeScale(2.2f, 2.2f);
    indicator.transform = transform;
    [indicator startAnimating];
    [self.view addSubview:indicator];
    // [self performSelector:@selector(searchUser) withObject:newString afterDelay:0.1];
    [self searchUser:newString];
}
-(void)searchUser:(NSString*)newString
{
    NSString *url = [NSString stringWithFormat:SERVER_URL@"/searchuser.do?username=%@&loginuser=%@",newString,getusername];
    // NSString *url = [NSString stringWithFormat:@"http://192.168.10.53:8080/Cancer_final/login.do?method=myAction1&username=%@&password=%@",getuser,getuserpass];
    NSURL *urlrequest=[NSURL URLWithString:url];
    NSURLRequest *req=[[NSURLRequest alloc]initWithURL:urlrequest];
    NSURLResponse  *response = nil;
    NSError* error;
    [NSURLConnection sendSynchronousRequest: req returningResponse: &response error: &error];
    
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    NSString *stringData= [NSString stringWithContentsOfURL:urlrequest encoding:nil error:nil];
    NSLog(@"Recieved data String &&&&&&&&&&&& %@",stringData);
    
    //NSData *dataofStrig=[NSData dataWithContentsOfFile:stringData];
    NSData * dataofStrig = [stringData dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"Recieved data Data &&&&&&&&&&&& %@",stringData);
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions                        error:&error];
    if (!json) {
        NSLog(@"Not Json");
    }
   
    dataUpdate = [[NSMutableArray alloc] initWithCapacity:dataUpdate.count];
    userPicArray = [[NSMutableArray alloc] initWithCapacity:userPicArray.count];
    userStatusArray = [[NSMutableArray alloc] initWithCapacity:userStatusArray.count];
    userFollowers = [[NSMutableArray alloc] initWithCapacity:userFollowers.count];
    userFollowings = [[NSMutableArray alloc] initWithCapacity:userFollowings.count];
    displayNameArray = [[NSMutableArray alloc] initWithCapacity:displayNameArray.count];
    NSLog(@"JSON format:- %@",json);
    int length=[json count];
    if(length==1)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Search failed" message:@"Searched user doesn't exist" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [indicator stopAnimating];
        [alert show];
    }
    else{
        for(int i=0;i<(length-1);i++)    {
            
            NSString *indexStr = [NSString stringWithFormat:@"%d",i];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            [userFollowers addObject:[[json objectForKey:data] valueForKey:@"followerCount"]];
            [userFollowings addObject:[[json objectForKey:data] valueForKey:@"followingCount"]];
            NSString *userName=[[json objectForKey:data] valueForKey:@"username"];
            [dataUpdate addObject:userName];
            NSString *status=[[json objectForKey:data]valueForKey:@"status"];
            [userStatusArray addObject:status];
            NSString *disName=[[json objectForKey:data]valueForKey:@"name"];
            NSLog(@"Display name %@",disName);
            [displayNameArray addObject:disName];
            NSString *userPic=[[json objectForKey:data]valueForKey:@"profile_image"];
            [userPicArray addObject:userPic];
            [self.searchDisplayController.searchResultsTableView reloadData];
            [mytable reloadData];
        }
        [indicator stopAnimating];
    }
    searchText.text=@"";
    
    
    
}

-(void)searchDataIndicator :(NSString*)newString{
    
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];//UIActivityIndicatorViewStyleWhiteLarge
    indicator.frame = CGRectMake(self.view.bounds.size.width / 2.0f - indicator.frame.size.width /2.0f, self.view.bounds.size.height / 2.0f - indicator.frame.size.height /2.0f, indicator.frame.size.width, indicator.frame.size.height);
    //indicator.color = [UIColor colorWithRed:1 green:0.8 blue:0.9 alpha:1 ];
    indicator.color = [UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
    CGAffineTransform transform = CGAffineTransformMakeScale(2.2f, 2.2f);
    indicator.transform = transform;
    [indicator startAnimating];
    [self.view addSubview:indicator];
    NSString *newtext = newString;
    NSLog(@"newtwxt is:- %@",newtext);
    //[self performSelector:@selector(searchData) withObject:newtext afterDelay:0.1];
    [self searchData:(NSString *)newtext];
}

-(void)searchData :(NSString*)string
{
    NSString *currstring = string;
    NSString *url = [NSString stringWithFormat:SERVER_URL@"/hash.do?hashvalue=%@",currstring];
    NSURL *urlrequest=[NSURL URLWithString:url];
    NSURLRequest *req=[[NSURLRequest alloc]initWithURL:urlrequest];
    NSURLResponse  *response = nil;
    NSError* error;
    [NSURLConnection sendSynchronousRequest: req returningResponse: &response error: &error];
    
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    NSString *stringData= [NSString stringWithContentsOfURL:urlrequest encoding:nil error:nil];
    NSLog(@"Recieved data String &&&&&&&&&&&& %@",stringData);
    
    //NSData *dataofStrig=[NSData dataWithContentsOfFile:stringData];
    NSData * dataofStrig = [stringData dataUsingEncoding:NSUTF8StringEncoding];
    if(!dataofStrig)
    {
        searchText.text=@"";
        NSLog(@"errorrrrr no response");
        UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Connection failed" message:@"Check intenet connection" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
        [indicator stopAnimating];
        [alert1 show];
    }
    
    else
    {
        NSLog(@"Recieved data Data &&&&&&&&&&&& %@",stringData);
        
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions error:&error];
        if (!json) {
            NSLog(@"Not Json");
        }
        NSLog(@"JSON format:- %@",json);
        int length=[json count];
        NSLog(@"JSON count:- %i",length);
        
        dataUpdate = [[NSMutableArray alloc] initWithCapacity:dataUpdate.count];
        differUser = [[NSMutableArray alloc] initWithCapacity:differUser.count];
        snoArray = [[NSMutableArray alloc] initWithCapacity:snoArray.count];
        profielImages=[[NSMutableArray alloc] initWithCapacity:profielImages.count];
        likearray=[[NSMutableArray alloc] initWithCapacity:likearray.count];
        dislikearray=[[NSMutableArray alloc] initWithCapacity:dislikearray.count];
        likesNameArray=[[NSMutableArray alloc] initWithCapacity:likesNameArray.count];
        dislikesNameArray=[[NSMutableArray alloc] initWithCapacity:dislikesNameArray.count];
        imageDatas=[[NSMutableArray alloc]initWithCapacity:imageDatas.count];
        dataType=[[NSMutableArray alloc]initWithCapacity:dataType.count];
        commentArray=[[NSMutableArray alloc]initWithCapacity:commentArray.count];
        locationArray=[[NSMutableArray alloc]initWithCapacity:locationArray.count];
        fourCommentArray=[[NSMutableArray alloc]initWithCapacity:fourCommentArray.count];
        commentCounts=[[NSMutableArray alloc]initWithCapacity:fourCommentArray.count];
        timeArray=[[NSMutableArray alloc]initWithCapacity:commentCounts.count];
        for(int i=0;i<(length-1);i++){
            NSString *indexStr = [NSString stringWithFormat:@"%d",i];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            
            
            if ([[[json objectForKey:data] valueForKey:@"elapsed"] isEqualToString:@""]) {
                [timeArray addObject:@"not updated"];
            }
            else
            {
                [timeArray addObject:[[json objectForKey:data] valueForKey:@"elapsed"]];
            }
            if([[[json objectForKey:data] valueForKey:@"location"] isEqualToString:@""])
            {
                [locationArray addObject:@"not updated"];
            }
            else
            {
                [locationArray addObject:[[json objectForKey:data] valueForKey:@"location"]];
            }
            
            [commentCounts addObject:[[json objectForKey:data]valueForKey:@"comment"]];
            NSString *likesName=[[json objectForKey:data] valueForKey:@"likenames"];
            NSString *confrm=[self likesContent:likesName];
            [likesNameArray addObject:confrm];
            
            NSString *dislikesName=[[json objectForKey:data] valueForKey:@"dislikenames"];
            NSString *disconfrm=[self likesContent:dislikesName];
            [dislikesNameArray addObject:disconfrm];
            
            dataStr=[[json valueForKey:data] valueForKey:@"content"];
            NSString *user=[[json valueForKey:data] valueForKey:@"username"];
            [differUser addObject:user];
            
            NSString *sno=[[json valueForKey:data]valueForKey:@"sno"];
            [snoArray addObject:sno];
            
            NSString *likeString = [[json valueForKey:data]valueForKey:@"like"];
            NSLog(@"value of like string is:- %@",likeString);
            [likearray addObject:likeString];
            
            NSString *dislikeString = [[json valueForKey:data]valueForKey:@"dislike"];
            NSLog(@"value of like string is:- %@",dislikeString);
            [dislikearray addObject:dislikeString];
            [commentArray addObject:[[json valueForKey:data] valueForKey:@"comment"]];
            NSString *userDP=[[json valueForKey:data]valueForKey:@"profile_image"];
            [profielImages addObject:userDP];
            NSLog(@"content from url is text %@",dataStr);
            [dataUpdate addObject:dataStr];
            NSString *fourcomment=[[json objectForKey:data]valueForKey:@"commentiphone"];
            NSLog(@"fourcomment %@",fourcomment);
            if ([fourcomment length]==0) {
                [fourCommentArray addObject:@"No Comments"];
            }
            else{
                [fourCommentArray addObject:fourcomment];
            }
            
            //[self.searchDisplayController.searchResultsTableView reloadData];
            [mytable reloadData];
        }
        
        searchText.text=@"";
        
        [indicator stopAnimating];
    }
    
}

-(NSString *)likesContent:(NSString *)allNames
{
    NSString *name;
    
    if ([allNames rangeOfString:getusername].location != NSNotFound)
    {
        name=@"Yes";
    }
    
    else
    {
        name=@"No";
    }
    
    return name;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataUpdate count];
}

- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    int height;
    if([searchType isEqualToString:@"hashTag"])
    {
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)) {
            height= 180;
        }
        else if ([comArray count]==3){
            height= 195;
        }
        else{
            
            height=225;
        }
    }
   if([searchType isEqualToString:@"users"]){
        height=70;
    }
    
    
    return height;
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    Login *login=[[Login alloc]init];
    UIImage *minus = [UIImage imageNamed:@"dislikebutton2.png"];
    UIImage *plus = [UIImage imageNamed:@"likebutton2.png"];
    UIImage *minusDark = [UIImage imageNamed:@"minusDark.png"];
    UIImage *plusDark = [UIImage imageNamed:@"plusDark.png"];
    static NSString *CellIdentifier1 = @"Cell1";
    static NSString *CellIdentifier2 = @"Cell2";
    static NSString *story1 = @"story1";
    static NSString *story2 = @"story2";
    mytable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [mytable setBackgroundView:[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"307x410.png"]] autorelease]];
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    UITableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    UITableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
    UITableViewCell *cellstory1 = [tableView dequeueReusableCellWithIdentifier:story1];
    UITableViewCell *cellstory2 = [tableView dequeueReusableCellWithIdentifier:story2];
    cell2.selectionStyle = UITableViewCellSelectionStyleNone;
    cellstory1.selectionStyle = UITableViewCellSelectionStyleNone;
    cellstory2.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSLog(@"helllloooo");
    //imageViewUrl.image=nil;
    introw=indexPath.row+1;
    in=indexPath.row;
    
    if ([searchType isEqualToString:@"hashTag"]) {
        
        
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)){
            NSLog(@"helllloooo");
            
            if (cellstory1 == nil)
            {
                cellstory1 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:story1] autorelease];
                
            }
            cellstory1.backgroundView = [ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cellWhiteBg.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];
            introw=indexPath.row+1;
            tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,12,290,20)];
            tweetLabel.numberOfLines=0;
            tweetLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            tweetLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:13];
            NSString *smile2=[login smilyString:[dataUpdate objectAtIndex:in]];
            [tweetLabel setText:smile2];
            [tweetLabel setLinksEnabled:TRUE];
            tweetLabel.clipsToBounds=YES;
            [cellstory1.contentView addSubview:tweetLabel];
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,40,40,40)];
            imageView2.layer.cornerRadius = 5;
            imageView2.clipsToBounds = YES;
            [cellstory1.contentView addSubview:imageView2];
            
            NSString *picUrlString = [profielImages objectAtIndex: indexPath.row];
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"editPic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData)
                        image = [UIImage imageWithData:imageData];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            
            NSLog(@"valid");
            
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(70,40,100,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            nameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:13];
            NSString *smile3=[differUser objectAtIndex:in];//[@"@" stringByAppendingString:[differUser objectAtIndex:in]];
            [nameLabel setText:smile3];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.clipsToBounds=YES;
            
            [cellstory1.contentView addSubview:nameLabel];
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,40,100,10)];
            placeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            placeLabel.font = [UIFont systemFontOfSize:8.0];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cellstory1.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,50,100,10)];
            timeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            timeLabel.font = [UIFont systemFontOfSize:8.0];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentRight;
            [cellstory1.contentView addSubview:timeLabel];
            
            NSString *smile;
            IFTweetLabel *descriptionLabel1 = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,86,60,15)];
            
            descriptionLabel1.font = [UIFont systemFontOfSize:10.0];
            
            NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
            if ([comArray count]==2) {
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
            }
            
            [cellstory1 addSubview:descriptionLabel1];
            
            UILabel *likeShow = nil;
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(26,110,43,10)] autorelease];
            likeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:12.0];
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cellstory1 addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(80,110,43,10)] autorelease];
            dislikeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:12.0];
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cellstory1 addSubview:dislikeShow];
            
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(134,110,43,10)] autorelease];
            commentShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:12.0];
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cellstory1 addSubview:commentShow];
            //******************************************************************************
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            likeBtn.frame=CGRectMake(26,120,44,30);
            [likeBtn addTarget:self action:@selector(likeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cellstory1 addSubview:likeBtn];
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            dislikeBtn.frame=CGRectMake(80,120,44,30);
            [dislikeBtn addTarget:self action:@selector(dislikeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[dislikesNameArray objectAtIndex:indexPath.row]);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            [cellstory1 addSubview:likeBtn];
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cellstory1 addSubview:dislikeBtn];
            
            
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"commentbutton2.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(134,120,44,30)];
            [commentButton addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory1.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"share_button2.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(188,120,44,30)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory1.contentView addSubview:shareButton];
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"bottom-setting2.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(242,120,44,30)];
            settingsButton.tag=indexPath.row+1;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory1.contentView addSubview:settingsButton];
            
            UIButton *moreButtonS=nil;
            moreButtonS = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonS.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonS setBackgroundImage:more forState:UIControlStateNormal];
            moreButtonS.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [moreButtonS setFrame:CGRectMake(280,90,30,22)];
            [moreButtonS addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory1.contentView addSubview:moreButtonS];
            cell=cellstory1;
            
        }
        else if(([comArray count]==3))
        {
            cellstory2.backgroundView = [ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cellWhiteBg.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];
            NSLog(@"helllloooo");
            
            if (cellstory2 == nil)
            {
                cellstory2 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:story2] autorelease];
                
            }
            introw=indexPath.row+1;
            tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,12,290,20)];
            tweetLabel.numberOfLines=0;
            tweetLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            tweetLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:13];
            NSString *smile2=[login smilyString:[dataUpdate objectAtIndex:in]];
            [tweetLabel setText:smile2];
            [tweetLabel setLinksEnabled:TRUE];
            tweetLabel.clipsToBounds=YES;
            [cellstory2.contentView addSubview:tweetLabel];
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,40,40,40)];
            imageView2.layer.cornerRadius = 5;
            imageView2.clipsToBounds = YES;
            [cellstory2.contentView addSubview:imageView2];
            
            NSString *picUrlString = [profielImages objectAtIndex: indexPath.row];
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"editPic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData)
                        image = [UIImage imageWithData:imageData];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            
            NSLog(@"valid");
            
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(70,40,100,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            nameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:13];
            NSString *smile3=[differUser objectAtIndex:in];//[@"@" stringByAppendingString:[differUser objectAtIndex:in]];
            [nameLabel setText:smile3];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.clipsToBounds=YES;
            
            [cellstory2.contentView addSubview:nameLabel];
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,40,100,10)];
            placeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            placeLabel.font = [UIFont systemFontOfSize:8.0];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cellstory2.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,50,100,10)];
            timeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            timeLabel.font = [UIFont systemFontOfSize:8.0];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentRight;
            [cellstory2.contentView addSubview:timeLabel];
            
            NSString *smile;
            IFTweetLabel *descriptionLabel1 = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,86,278,15)];
            
            descriptionLabel1.font = [UIFont systemFontOfSize:10.0];
            IFTweetLabel *descriptionLabel2 = [[IFTweetLabel alloc]initWithFrame:CGRectMake(15,101,260,15)];
            descriptionLabel2.font = [UIFont systemFontOfSize:10.0];
            NSMutableAttributedString *attributed;
            
            if ([comArray count]==3) {
                attributed=[login attibutedUsername:[comArray objectAtIndex:0]];
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
                attributed=[login attibutedUsername:[comArray objectAtIndex:1]];
                smile=[login smilyString:[comArray objectAtIndex:1]];
                [descriptionLabel2 setText:smile];
                [descriptionLabel2 setLinksEnabled:TRUE];
            }
            
            [cellstory2 addSubview:descriptionLabel1];
            [cellstory2 addSubview:descriptionLabel2];
            
            UILabel *likeShow = nil;
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(26,125,43,10)] autorelease];
            likeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:12.0];
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cellstory2 addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(80,125,43,10)] autorelease];
            dislikeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:12.0];
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cellstory2 addSubview:dislikeShow];
            
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(134,125,43,10)] autorelease];
            commentShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:12.0];
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cellstory2 addSubview:commentShow];
            //******************************************************************************
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            likeBtn.frame=CGRectMake(26,135,44,30);
            [likeBtn addTarget:self action:@selector(likeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cellstory2 addSubview:likeBtn];
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            dislikeBtn.frame=CGRectMake(80,135,44,30);
            [dislikeBtn addTarget:self action:@selector(dislikeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[dislikesNameArray objectAtIndex:indexPath.row]);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            [cellstory2 addSubview:likeBtn];
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cellstory2 addSubview:dislikeBtn];
            
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"commentbutton2.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(134,135,44,30)];
            [commentButton addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory2.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"share_button2.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(188,135,44,30)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory2.contentView addSubview:shareButton];
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"bottom-setting2.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(242,135,44,30)];
            settingsButton.tag=indexPath.row+1;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory2.contentView addSubview:settingsButton];
            
            UIButton *moreButtonS=nil;
            moreButtonS = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonS.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonS setBackgroundImage:more forState:UIControlStateNormal];
            moreButtonS.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [moreButtonS setFrame:CGRectMake(280,105,30,22)];
            [moreButtonS addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory2.contentView addSubview:moreButtonS];
            cell=cellstory2;
        }
        else if(([comArray count]==4)||([comArray count]==5))
        {
            if (cell2 == nil)
            {
                cell2 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
                
            }
            cell2.backgroundView = [ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cellWhiteBg.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];
            int lbltag=indexPath.row+1;
            
            tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,12,290,30)];
            tweetLabel.numberOfLines=0;
            tweetLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            tweetLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:13];
            NSString *smile2=[login smilyString:[dataUpdate objectAtIndex:indexPath.row]];
            [tweetLabel setText:smile2];
            [tweetLabel setLinksEnabled:TRUE];
            tweetLabel.clipsToBounds=YES;
            [cell2.contentView addSubview:tweetLabel];
            
            //int lbltag =indexPath.row;
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,40,40,40)];
            imageView2.layer.cornerRadius = 5;
            imageView2.clipsToBounds = YES;
            [cell2.contentView addSubview:imageView2];
            
            NSString *picUrlString = [profielImages objectAtIndex: indexPath.row];
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"editPic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData)
                        image = [UIImage imageWithData:imageData];
                    if (image)
                    {
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            // IFTweetLabel *nameLabel=[[IFTweetLabel alloc]initWithFrame:CGRectMake(70,40,100,30)];
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(70,40,100,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            nameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:13];
            NSString *smile3=[differUser objectAtIndex:indexPath.row];//[@"@" stringByAppendingString:[differUser objectAtIndex:in]];
            [nameLabel setText:smile3];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.clipsToBounds=YES;
            
            
            nameLabel.clipsToBounds=YES;
            [cell2.contentView addSubview:nameLabel];
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,40,100,10)];
            placeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            placeLabel.font = [UIFont systemFontOfSize:8.0];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cell2.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(200,50,100,10)];
            timeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            timeLabel.font = [UIFont systemFontOfSize:8.0];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentRight;
            [cell2.contentView addSubview:timeLabel];
            
            NSString *smile;
            IFTweetLabel *descriptionLabel1 = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,86,278,15)];
            
            descriptionLabel1.font = [UIFont systemFontOfSize:10.0];
            IFTweetLabel *descriptionLabel2 = [[IFTweetLabel alloc]initWithFrame:CGRectMake(15,101,278,15)];
            descriptionLabel2.font = [UIFont systemFontOfSize:10.0];
            IFTweetLabel *descriptionLabel3 = [[IFTweetLabel alloc]initWithFrame:CGRectMake(15,116,240,15)];
            descriptionLabel3.font = [UIFont systemFontOfSize:10.0];
            IFTweetLabel *descriptionLabel4 = [[IFTweetLabel alloc]initWithFrame:CGRectMake(15,131,240,15)];
            descriptionLabel4.font = [UIFont systemFontOfSize:10.0];
            NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
            NSMutableAttributedString *attributed;
            
            if ([commentStr isEqualToString:@"No Comments"]) {
                descriptionLabel1.text=@"No Comments ......";
                attributed=[login attibutedUsername:@"You can post first comment :) :) ......"];
                smile=[login smilyString:@"You can post first comment :) :) ......"];
                [descriptionLabel2 setText:smile];
                [descriptionLabel2 setLinksEnabled:TRUE];
                
            }
            else
            {
                NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
                if ([comArray count]==2) {
                    //descriptionLabel1.text=@"Only one comment add your comment....";
                    smile=[login smilyString:[comArray objectAtIndex:0]];
                    [descriptionLabel1 setText:smile];
                    [descriptionLabel1 setLinksEnabled:TRUE];
                    
                }
                else if ([comArray count]==3) {
                    attributed=[login attibutedUsername:[comArray objectAtIndex:0]];
                    smile=[login smilyString:[comArray objectAtIndex:0]];
                    [descriptionLabel1 setText:smile];
                    [descriptionLabel1 setLinksEnabled:TRUE];
                    attributed=[login attibutedUsername:[comArray objectAtIndex:1]];
                    smile=[login smilyString:[comArray objectAtIndex:1]];
                    [descriptionLabel2 setText:smile];
                    [descriptionLabel2 setLinksEnabled:TRUE];
                }
                else if ([comArray count]==4) {
                    attributed=[login attibutedUsername:[comArray objectAtIndex:0]];
                    // descriptionLabel1.attributedText=attributed;
                    smile=[login smilyString:[comArray objectAtIndex:0]];
                    [descriptionLabel1 setText:smile];
                    [descriptionLabel1 setLinksEnabled:TRUE];
                    smile=[login smilyString:[comArray objectAtIndex:1]];
                    [descriptionLabel2 setText:smile];
                    [descriptionLabel2 setLinksEnabled:TRUE];
                    smile=[login smilyString:[comArray objectAtIndex:2]];
                    [descriptionLabel3 setText:smile];
                    [descriptionLabel3 setLinksEnabled:TRUE];
                }
                else if ([comArray count]==5) {
                    attributed=[login attibutedUsername:[comArray objectAtIndex:0]];
                    //  descriptionLabel1.attributedText=attributed;
                    smile=[login smilyString:[comArray objectAtIndex:0]];
                    [descriptionLabel1 setText:smile];
                    [descriptionLabel1 setLinksEnabled:TRUE];
                    smile=[login smilyString:[comArray objectAtIndex:1]];
                    [descriptionLabel2 setText:smile];
                    [descriptionLabel2 setLinksEnabled:TRUE];
                    smile=[login smilyString:[comArray objectAtIndex:2]];
                    [descriptionLabel3 setText:smile];
                    [descriptionLabel3 setLinksEnabled:TRUE];
                    smile=[login smilyString:[comArray objectAtIndex:3]];
                    [descriptionLabel4 setText:smile];
                    [descriptionLabel4 setLinksEnabled:TRUE];
                }
                
            }
            [cell2 addSubview:descriptionLabel1];
            [cell2 addSubview:descriptionLabel2];
            [cell2 addSubview:descriptionLabel3];
            [cell2 addSubview:descriptionLabel4];
            
            
            UILabel *likeShow = nil;
            
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(26,155,43,10)] autorelease];
            likeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:12.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cell2 addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(80,155,43,10)] autorelease];
            dislikeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:12.0];
            dislikeShow.tag = lbltag;
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cell2 addSubview:dislikeShow];
            
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(134,155,43,10)] autorelease];
            commentShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:12.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cell2 addSubview:commentShow];
            
            //******************************************************************************
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            likeBtn.frame=CGRectMake(26,165,44,30);
            [likeBtn addTarget:self action:@selector(likeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cell2 addSubview:likeBtn];
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            dislikeBtn.frame=CGRectMake(80,165,44,30);
            [dislikeBtn addTarget:self action:@selector(dislikeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[dislikesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            [cell2 addSubview:likeBtn];
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cell2 addSubview:dislikeBtn];
            
            
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"commentbutton2.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(134,165,44,30)];
            [commentButton addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell2.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"share_button2.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(188,165,44,30)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell2.contentView addSubview:shareButton];
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"bottom-setting2.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(242,165,44,30)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell2.contentView addSubview:settingsButton];
            
            UIButton *moreButtonS=nil;
            moreButtonS = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonS.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonS setBackgroundImage:more forState:UIControlStateNormal];
            moreButtonS.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [moreButtonS setFrame:CGRectMake(280,135,30,22)];
            [moreButtonS addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell2.contentView addSubview:moreButtonS];
            cell=cell2;
        }
    }
    else if([searchType isEqualToString:@"users"])
    {
        if (cell1 == nil)
        {
            cell1 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] autorelease];
            
        }
        cell1.backgroundView = [ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cellWhiteBg.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];
        cell1.selectionStyle = UITableViewCellAccessoryNone;
        cell1.accessoryType=UITableViewCellAccessoryDetailDisclosureButton;
        
        UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(10,5,60,60)];
        imageView2.layer.cornerRadius = 5;
        imageView2.clipsToBounds = YES;
        [cell1.contentView addSubview:imageView2];
        
        NSString *picUrlString = [userPicArray objectAtIndex: indexPath.row];
        UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
        
        if (cachedPic)
        {
            imageView2.image = cachedPic;
            
        }
        else
        {
            // you'll want to initialize the image with some blank image as a placeholder
            
            //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
            imageView2.image = [UIImage imageNamed:@"editPic.png"];
            [self.imageDownloadingQueuePic addOperationWithBlock:^{
                NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                UIImage *image    = nil;
                if (imageData)
                    image = [UIImage imageWithData:imageData];
                if (image)
                {
                    // add the image to your cache
                    
                    [self.imageCachePic setObject:image forKey:picUrlString];
                    
                    // finally, update the user interface in the main queue
                    
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                        if (updateCell)
                            imageView2.image = image;
                        
                    }];
                }
            }];
        }
        
        NSLog(@"valid");
        
        UILabel *nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(80,10,200,20)];
        nameLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
        nameLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:15];
        nameLabel.text=[dataUpdate objectAtIndex:indexPath.row];
        [cell1.contentView addSubview:nameLabel];
        
        UILabel *palceLabel=[[UILabel alloc]initWithFrame:CGRectMake(80,25,200,20)];
        palceLabel.numberOfLines=0;
        palceLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:13];
        NSMutableAttributedString *attributed=[login atributedString:[userStatusArray objectAtIndex:indexPath.row]];
        palceLabel.attributedText=attributed;
        [cell1.contentView addSubview:palceLabel];
        
        
        cell=cell1;
    }
    
    return cell;
    
}
-(IBAction)settingButtonClicked:(id)sender
{
    self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:3];
    
}

//***********************************Like Methods****************************
-(void)likeFn:(UIButton*)btnClicked
{
    
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    if([[likesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=story&username=%@&action=liked",indexrowtosend,[differUser objectAtIndex:btnClicked.tag-1]];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        NSString *count=[json valueForKey:@"count"];
        int likeCount=[[json valueForKey:@"count"] intValue];
        [likearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [likesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        NSLog(@"changed value of like %@",[likearray objectAtIndex:btnClicked.tag-1]);
        
        if ([delegate.likearray containsObject:indexrowtosend])
        {
            NSUInteger index1 = [delegate.snoArray indexOfObject:indexrowtosend];
            NSString *datatype=[delegate.dataType objectAtIndex:index1];
            if([datatype isEqualToString:@"story"])
            {
                [delegate.likearray replaceObjectAtIndex:index1 withObject:count];
                [delegate.likesNameArray replaceObjectAtIndex:index1 withObject:@"Yes"];
            }
        }
    }
    [mytable reloadData];
}
//****************************** DisLike Methods *******************************
-(void)dislikeFn:(UIButton*)btnClicked
{
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    if([[dislikesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=story&username=%@&action=dislike",indexrowtosend,[differUser objectAtIndex:btnClicked.tag-1]];
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        NSString *count=[json valueForKey:@"count"];
        int likeCount=[[json valueForKey:@"count"] intValue];
        [dislikearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [dislikesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        NSLog(@"changed value of dislike %@",[dislikearray objectAtIndex:btnClicked.tag-1]);
        if ([delegate.dislikearray containsObject:indexrowtosend])
        {
            NSUInteger index1 = [delegate.snoArray indexOfObject:indexrowtosend];
            NSString *datatype=[delegate.dataType objectAtIndex:index1];
            if([datatype isEqualToString:@"story"])
            {
                [delegate.dislikearray replaceObjectAtIndex:index1 withObject:count];
                [delegate.dislikesNameArray replaceObjectAtIndex:index1 withObject:@"Yes"];
            }
        }
    }
    [mytable reloadData];
    //NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:btnClicked.tag-1 inSection:0];
    // NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    // [mytable reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
    if ([delegate.mynameLogin isEqualToString:[differUser objectAtIndex:indexPath.row]]) {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
    }
    else
    {
        searchedUserView *searchedUser=[self.storyboard instantiateViewControllerWithIdentifier:@"searchedUser"];
        UINavigationBar *navBar = [[self navigationController] navigationBar];
        navBar.hidden=false;
        searchedUser.getUserSearched=[differUser objectAtIndex:indexPath.row];
        searchedUser.getImageSearched=[userPicArray objectAtIndex:indexPath.row];
        searchedUser.getStatusSearched=[userStatusArray objectAtIndex:indexPath.row];
        searchedUser.getfollowers=[userFollowers objectAtIndex:indexPath.row];
        searchedUser.getfollowing=[userFollowings objectAtIndex:indexPath.row];
        searchedUser.getDisName=[displayNameArray objectAtIndex:indexPath.row];
        searchedUser.getId=@"search";
        [self.navigationController pushViewController:searchedUser animated:YES];
    }
}
-(void)userButtonVideoClicked:(NSString *)searchUser
{
    AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
    if ([delegate.mynameLogin isEqualToString:searchUser]) {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
    }
    else
    {
        searchedUserView *searchedUser=[self.storyboard instantiateViewControllerWithIdentifier:@"searchedUser"];
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/userinfo.do?username=%@",searchUser];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSURLRequest *req=[[NSURLRequest alloc]initWithURL:urlrequest];
        NSURLResponse  *response = nil;
        NSError* error;
        [NSURLConnection sendSynchronousRequest: req returningResponse: &response error: &error];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        
        NSData * dataofStrig = [NSData dataWithContentsOfURL:urlrequest];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions                        error:&error];
        if (!json) {
            NSLog(@"Not Json");
        }
        else{
            NSLog(@"JSON format:- %@",json);
            searchedUser.getfollowers=[json objectForKey:@"followerCount"];
            searchedUser.getfollowing=[json objectForKey:@"followingCount"];
            NSString *userName=[json objectForKey:@"username"];
            searchedUser.getUserSearched=userName;
            NSString *status=[json objectForKey:@"status"];
            searchedUser.getStatusSearched=status;
            NSString *userPic=[json objectForKey:@"profile_image"];
            searchedUser.getImageSearched=userPic;
            searchedUser.getId=@"circle";
            [self.navigationController pushViewController:searchedUser animated:YES];
        }
    }
}


//****************************************************************************************************
- (void)reloadRowsAtIndexPaths:(NSArray *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation
{
    NSLog(@"cell reloaded");
}

-(void)shareButtonClicked:(UIButton*)btnClicked{
    
    NSString *type=@"text";
    NSLog(@"Helllooo activity viewcontroller");
    
    NSArray *activityItems = NO;
    if([type isEqualToString:@"image"])
    {
        UIImage *image=[UIImage imageWithData:[dataUpdate objectAtIndex:btnClicked.tag-1]];
        if (image != nil) {
            activityItems = @[@"hello activityviewcontroller",image];
        }
        
        else {
            activityItems = @[[dataUpdate objectAtIndex:btnClicked.tag-1]];
        }
    }
    else if([type isEqualToString:@"text"]){
        activityItems = @[[dataUpdate objectAtIndex:btnClicked.tag-1]];
    }
    else if([type isEqualToString:@"video"]){
        NSString *myString = [[dataUpdate objectAtIndex:btnClicked.tag-1] absoluteString];
        NSLog(@"nsurl String %@",myString);
        activityItems = @[myString];
    }
    UIActivityViewController *activityController =
    [[UIActivityViewController alloc]
     initWithActivityItems:activityItems
     applicationActivities:nil];
    
    [self presentViewController:activityController
                       animated:YES completion:nil];
    
    
}


-(IBAction)reportButtonClicked:(UIButton*)btnClicked
{
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    delegate.snoReport=[snoArray objectAtIndex:btnClicked.tag-1];
    delegate.typeReport=@"story";
    if([[differUser objectAtIndex:btnClicked.tag-1] isEqualToString:delegate.mynameLogin])
    {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:3];
    }
    else{
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Report for Content" delegate:self
                                                        cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Inappropriate"
                                                        otherButtonTitles:@"Unfollow user",nil];
        actionSheet.backgroundColor = [UIColor greenColor];
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        //[actionSheet showInView:self.view];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    }
}
-(void)actionSheet:(UIActionSheet *)actionSheet2 clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == [actionSheet2 destructiveButtonIndex]) {
        [self performSegueWithIdentifier:@"searchToReport" sender:self];
    } else if(buttonIndex == 1){
        
        NSString *followUrl=[NSString stringWithFormat:SERVER_URL@"/unfollowing.do?username=%@&following=%@",getusername,[differUser objectAtIndex:introw-2]];
        NSURL *furl=[NSURL URLWithString:followUrl];
        NSLog(@"Unfolloeing user url %@",furl);
        NSError* error;
        NSData *Data=[NSData dataWithContentsOfURL:furl];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:Data options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
    }
}
-(IBAction)moreButtonStoryClicked:(UIButton*)btnClicked
{
    //NSLog(@"Content Type %@",[dataType objectAtIndex:btnClicked.tag-1]);
    NSString *setStoryString = [dataUpdate objectAtIndex:btnClicked.tag-1];
    
    storyDetailView *storyDetail=[self.storyboard instantiateViewControllerWithIdentifier:@"storyDetail"];
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    navBar.hidden=false;
    storyDetail.getStory=setStoryString;
    storyDetail.hidesBottomBarWhenPushed = YES;
    storyDetail.getUserImageData=[profielImages objectAtIndex:btnClicked.tag-1];
    storyDetail.getName=[differUser objectAtIndex:btnClicked.tag-1];
    storyDetail.getSno=[snoArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getIfLikes=[likesNameArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getLikes=[likearray objectAtIndex:btnClicked.tag-1];
    storyDetail.getIfDisLikes=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getDisLikes=[dislikearray objectAtIndex:btnClicked.tag-1];
    storyDetail.getComments=[commentArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getType=@"text";
    storyDetail.getDislikeConferm=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getLikeConferm=[likesNameArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getLocation=[locationArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getTime=[timeArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getRowIndex=[NSString stringWithFormat:@"%i",btnClicked.tag-1];
    storyDetail.getTabId=@"search";
    [self.navigationController pushViewController:storyDetail animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:IFTweetLabelURLNotification object:nil];
    // UINavigationBar *navBar = [[self navigationController] navigationBar];
    //navBar.hidden=true;
}
-(void)hideNavBar:(NSTimer*) t
{
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    navBar.hidden=true;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [super dealloc];
}
@end
