//
//  FollowRequestView.h
//  CancerCircleFinal
//
//  Created by Raminder on 01/05/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface FollowRequestView : UIViewController
{
    NSMutableArray *userPicArray;
    NSMutableArray *userNameArray;
     NSMutableArray *userStatusArray;
    IBOutlet UITableView *myTable;
    UIActivityIndicatorView *indicator;
}

@end
