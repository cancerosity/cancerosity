//
//  UploadButtons.m
//  CancerCircleFirst
//
//  Created by Raminder on 25/01/13.
//
//

#import "UploadButtons.h"
#import "Login.h"
#import "SettingButtons.h"
#import "ShareSettingsView.h"
#import "AppDelegate.h"
//#import "ProfileFirst.h"
#import <Social/Social.h>
#import "SBJSON.h"
#import "FbGraphFile.h"
#import "FXBlurView.h"
#import <QuartzCore/QuartzCore.h>
#import "ILTranslucentView.h"
#import "getFbTokenViewController.h"
#import "DLCGrayscaleContrastFilter.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "QuartzCore/CALayer.h"
#import <QuartzCore/QuartzCore.h>


#define viewposition CGPointMake(160, 142)
@interface UploadButtons ()
@property (nonatomic, strong) IBOutlet FXBlurView *blurView;
@end

@implementation UploadButtons
@synthesize filtersbutton, filterbutton;
@synthesize fbGraph;

@synthesize facebookClientID;
@synthesize redirectUri;
@synthesize accessToken;
//@synthesize webView;

@synthesize callbackObject;
@synthesize callbackSelector;
FXBlurView *blurView;
GPUImageStillCamera *stillCamera;
GPUImageOutput<GPUImageInput> *filter;
GPUImageOutput<GPUImageInput> *filter2;
GPUImageCropFilter *cropFilter;
UIImageOrientation staticPictureOriginalOrientation;
GPUImagePicture *staticPicture;
BOOL isStatic;
BOOL hasBlur;
int selectedFilter;
int selectedBorder;
@synthesize getUsrname;

-(void)viewWillAppear:(BOOL)animated
{
    
}
-(void)viewDidAppear:(BOOL)animated
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    idAtUpload=delegate.tabId;
    NSLog(@"Id at upload %@",idAtUpload);
    delegate.tabId=@"upload";
    storyView.text=@"";
    [self viewDidLoad];
    flagFb = fbButton.tag;
    
     if (FBSession.activeSession.state == FBSessionStateClosed || FBSession.activeSession.state == FBSessionStateClosedLoginFailed)
     {
         [fbButton setSelected:FALSE];
         NSLog(@"Facebook Sharing OFF");
         flagFb = 0;
     }
    
   
}
- (NSString *)deviceLocation {
    NSString *theLocation = [NSString stringWithFormat:@"latitude: %f longitude: %f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude];
    NSLog(@"the location %@",theLocation);
    
    return theLocation;
}
-(IBAction)postToTumblr
{
    if(flagTu==1)
    {
        [tuButton setBackgroundImage:[UIImage imageNamed:@"tumblr.png"] forState:UIControlStateNormal];
        flagTu=0;
    }
    else
    {
        
        [tuButton setBackgroundImage:[UIImage imageNamed:@"tumblrDark.png"] forState:UIControlStateNormal];
        
        NSUserDefaults *itemValue=[NSUserDefaults standardUserDefaults];
        [itemValue setObject:@"" forKey:@"typeValue"];
        
        flagTu=1;
    }
    
}
-(IBAction)postToFlicker
{
    if ((imageView2.image=nil)) {
        UIAlertView *alertImage=[[UIAlertView alloc]initWithTitle:@"Connect to Flickr failed" message:@"Select image" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertImage show];
    }
    else
    {
        if(flagFl==1)
        {
            [flButton setBackgroundImage:[UIImage imageNamed:@"twoDots.png"] forState:UIControlStateNormal];
            flagFl=0;
        }
        else
        {
            NSUserDefaults *flickrName=[NSUserDefaults standardUserDefaults];
            NSString *flUser=[flickrName objectForKey:@"flickrName"];
            NSLog(@"flickr user %@",flUser);
            if(flUser.length==0)
            {
                //NSData  *imageData = UIImageJPEGRepresentation(imageView.image, 90);
                //UIImage *image=[UIImage imageWithData:imageData];
                
                //flagFl=1;
                [flButton setBackgroundImage:[UIImage imageNamed:@"flickrDark.png"] forState:UIControlStateNormal];
            }
            else
            {
                //if ([storyView.text isEqualToString:@""]) {
                flagFl=1;
                [flButton setBackgroundImage:[UIImage imageNamed:@"flickrDark.png"] forState:UIControlStateNormal];
                //}
            }
        }
        
    }
}

- (IBAction)addText:(UIButton *)sender {
    blurView.dynamic = YES;
   /* NSArray* buttons = [NSArray arrayWithObjects: bordersbutton, filtersbutton, nil];
    for (UIButton* button in buttons) {
        if (button != sender) {
            [button setSelected: NO];
            [UIView animateWithDuration:0.1 animations:^{
                [transview setAlpha:0.0];
                [transview setHidden:YES];
                [self.filtersbutton setSelected:NO];
                [self.bordersbutton setSelected:NO];
                [self.textButton setSelected:NO];
                [self.bordersScrollview setHidden:YES];
                [self.filtersScrollview setHidden:YES];
                [self.filtersbutton setBackgroundColor:[UIColor clearColor]];
                [self.bordersbutton setBackgroundColor:[UIColor clearColor]];
                [self.textButton setBackgroundColor:[UIColor clearColor]];
                self.bordersbutton.backgroundColor = [UIColor clearColor];
                self.filtersbutton.backgroundColor = [UIColor clearColor];
                self.textButton.backgroundColor = [UIColor clearColor];
                dropArrow1.alpha = 0;
                dropArrow2.alpha = 0;
                dropArrow3.alpha = 0;
                dropArrow4.alpha = 0;
                
            }];
            
            
            
            
        }
    }
    int tag = sender.tag;*/
    if (sender.selected == TRUE) {
        [sender setSelected:FALSE];
        [self.view endEditing:YES];
      
        [UIView animateWithDuration:0.4 animations:^{
            [imageText setHidden:YES];
            [self.textButton setBackgroundColor:[UIColor clearColor]];
            self.textButton.backgroundColor = [UIColor clearColor];
            dropArrow2.alpha = 0;
            
        }];
    }
    else if (sender.selected == NO) {
        [sender setSelected:YES];
        
        UIPanGestureRecognizer *gesture = [[UIPanGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(labelDragged:)];
        [imageText addGestureRecognizer:gesture];
        [self.filtersbutton setSelected:NO];
        [self.bordersButton setSelected:NO];
        [self.textButton setSelected:YES];
        [self.filtersScrollview setHidden:YES];
        [self.bordersScrollview setHidden:YES];
        self.filtersbutton.backgroundColor = [UIColor clearColor];
        dropArrow1.alpha = 0;
        dropArrow4.alpha = 0;
        dropArrow3.alpha = 0;
        [UIView animateWithDuration:0.4 animations:^{
            [imageText setHidden:NO];
            [self.textButton setBackgroundColor:[UIColor colorWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0]];
            dropArrow2.alpha = 1;
            
        }];
        
        
    }
}

- (IBAction)enlargeTextBox:(UIButton *)sender {

    if (sender.selected == YES) {
        [sender setSelected:NO];
        
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^ {
                             
                             self.textBack.frame = textbackOriginal;
                             storyView.frame = textboxOriginal;
                             self.buttonView.frame = buttonsOriginal;
                             bgImage.frame = backOriginal;
                             
                             
                         }
                         completion:^(BOOL finished) {
                             fbButton.hidden = NO;
                             twButton.hidden = NO;
                             tuButton.hidden = NO;
                             flButton.hidden = NO;
                             saveView.hidden = NO;
                             borderView.hidden = NO;
                             transView.hidden = NO;
                             buttonsShadowV.hidden = NO;
                         }];

        
    }
    
    else if (sender.selected == NO) {
        [sender setSelected:YES];

        fbButton.hidden = YES;
        twButton.hidden = YES;
        tuButton.hidden = YES;
        flButton.hidden = YES;
        saveView.hidden = YES;
        borderView.hidden = YES;
        transView.hidden = YES;
        buttonsShadowV.hidden = YES;
    
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^ {
                        
                         
                         self.textBack.frame = CGRectMake(10, 10, self.view.bounds.size.width - 20, self.view.bounds.size.height - 175);
                         storyView.frame = CGRectMake(15, 15, self.view.bounds.size.width - 20, self.view.bounds.size.height - 192);
                         self.buttonView.frame = CGRectMake(0,self.textBack.frame.size.height + 5, self.buttonView.frame.size.width, self.buttonView.frame.size.height);
                        bgImage.frame = CGRectMake(bgImage.frame.origin.x,bgImage.frame.origin.y, bgImage.frame.size.width, self.buttonView.frame.origin.y + 38);
                     }
                     completion:^(BOOL finished) {
                         
                     }];
    }



}
- (void)labelDragged:(UIPanGestureRecognizer *)gesture
{
	imageText = (UITextView *)gesture.view;
	CGPoint translation = [gesture translationInView:imageText];
    
    CGPoint point = [gesture locationInView:self.view];
    
   CGRect boundsRect = CGRectMake(100, 100, 90, 200);
    
    if (CGRectContainsPoint(boundsRect, point)) {
        imageText.center = CGPointMake(imageText.center.x + translation.x,
                                   imageText.center.y + translation.y);
    }
    
	// move label
	
    
	// reset translation
	[gesture setTranslation:CGPointZero inView:imageText];
}

-(void)postImageToFlickr
{
   // NSData  *imageData = UIImageJPEGRepresentation(imageView.image, 90);
    //UIImage *image=[UIImage imageWithData:imageData];
    
    
}
-(void)sendToTumblr
{
    
    NSUserDefaults *itemType=[NSUserDefaults standardUserDefaults];
    NSUserDefaults *itemValue=[NSUserDefaults standardUserDefaults];
    
    if (![storyView.text isEqualToString:@""]) {
        
        [itemType setObject:@"1" forKey:@"typeItem"];
        [itemValue setObject:storyView.text forKey:@"typeValue"];
    }
    else if( [extension isEqualToString:@"testVideo.mov"])
    {
        NSLog(@"selected url is: - %@",_selectedVideoUrl);
        //NSURL *url=[NSURL URLWithString:uploadedData];
        
        [itemType setObject:@"2" forKey:@"typeItem"];
        [itemValue setObject:uploadedData forKey:@"typeValue"];
    }
    else //if (!(imageView.image=nil))
    {
       NSData  *imageData = UIImageJPEGRepresentation(imageView2.image, 90);
       //UIImage *image=[UIImage imageWithData:imageData];
        
        [itemType setObject:@"3" forKey:@"typeItem"];
        [itemValue setObject:imageData forKey:@"typeValue"];
    }
    //[itemType setObject:@"SHKShareTypeURL" forKey:@"typeItem"];
    
}
- (void)viewDidLoad
{
    [self loadFilters];
    [self loadBorders];
    [self loadFonts];
    [self loadColors];
    NSLog(@"button title: %@",buttonTitle);
    imageText.textColor = [UIColor lightGrayColor];
    textbackOriginal = self.textBack.frame;
    textboxOriginal = storyView.frame;
    buttonsOriginal = self.buttonView.frame;
    backOriginal = bgImage.frame;
    if (isStatic == NO) {
        _imageView.layer.borderColor = [[UIColor alloc] initWithRed:(203.0/255.0) green:(232.0/255.0) blue:(198.0/255.0) alpha:1.0].CGColor;
        _imageView.layer.borderWidth = 2;
        self.imageView.layer.shadowColor = [UIColor clearColor].CGColor;
    }
    else if (isStatic == YES) {
        _imageView.layer.borderColor = [UIColor clearColor].CGColor;
        _imageView.layer.borderWidth = 0;
        self.imageView.layer.shadowColor = [UIColor blackColor].CGColor;
        self.imageView.layer.shadowOffset = CGSizeMake(0, 0);
        self.imageView.layer.shadowOpacity = 0.3;
        self.imageView.layer.shadowRadius = 0.5;
        self.imageView.clipsToBounds = NO;
    }
    
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];//UIActivityIndicatorViewStyleWhiteLarge
    indicator.frame = CGRectMake(self.view.bounds.size.width / 2.0f - indicator.frame.size.width /2.0f, self.view.bounds.size.height / 2.0f - indicator.frame.size.height /2.0f, indicator.frame.size.width, indicator.frame.size.height);
    //indicator.color = [UIColor colorWithRed:1 green:0.8 blue:0.9 alpha:1 ];
    indicator.color = [UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
    CGAffineTransform transform = CGAffineTransformMakeScale(2.2f, 2.2f);
    indicator.transform = transform;
    indicator.hidesWhenStopped = NO;
    
    
    circleRefresh = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"refreshcircle.png"]];
    circleRefresh.frame = CGRectMake(floor(self.view.frame.size.width / 2),
                                     floor(self.view.frame.size.height / 2),
                                     60, 60);
    
    CGRect frame2 = circleRefresh.frame;
    frame2.origin.x = self.view.frame.size.width / 2 - frame2.size.width / 2;
    frame2.origin.y = self.view.frame.size.height / 2 - frame2.size.height / 2;
    circleRefresh.frame = frame2;
    
    circleRefresh.center = CGPointMake(floor(self.view.frame.size.width / 2), floor(self.view.frame.size.height / 2));
    
  
    
    circleRefresh2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"refreshcircle.png"]];
    circleRefresh2.frame = CGRectMake(floor(self.view.frame.size.width / 2),
                                      floor(self.view.frame.size.height / 2),
                                      30, 30);
    
    CGRect frame3 = circleRefresh2.frame;
    frame3.origin.x = self.view.frame.size.width / 2 - frame3.size.width / 2;
    frame3.origin.y = self.view.frame.size.height / 2 - frame3.size.height / 2;
    circleRefresh2.frame = frame3;
    
    circleRefresh2.center = CGPointMake(floor(self.view.frame.size.width / 2), floor(self.view.frame.size.height / 2));
    
   
    
    self.blurAcView = [[FXBlurView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.blurAcView.backgroundColor = [UIColor clearColor];
    self.blurAcView.alpha = 0.0f;
    [self.blurAcView addSubview:circleRefresh];
    [self.blurAcView addSubview:circleRefresh2];

    [self.view addSubview:self.blurAcView];

    CABasicAnimation *fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    fullRotation.fromValue = [NSNumber numberWithFloat:0];
    fullRotation.toValue = [NSNumber numberWithFloat:((360*M_PI)/180)];
    fullRotation.duration = 1.1;
    fullRotation.repeatCount = 1e100;
    [circleRefresh.layer addAnimation:fullRotation forKey:@"360"];
    
    
    
    CABasicAnimation *fullRotation2 = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    fullRotation2.fromValue = [NSNumber numberWithFloat:0];
    fullRotation2.toValue = [NSNumber numberWithFloat:((-360*M_PI)/180)];
    fullRotation2.duration = 1.2;
    fullRotation2.repeatCount = 1e100;
    [circleRefresh2.layer addAnimation:fullRotation2 forKey:@"360"];
    
    
    [self.textBack.layer setBorderWidth:1.0f];
    [self.textBack.layer setBorderColor:[[[UIColor alloc] initWithRed:(233.0/255.0) green:(233.0/255.0) blue:(233.0/255.0) alpha:1.0] CGColor]];

    
       /*[filtersbutton setTitleColor:[[UIColor alloc] initWithRed:(167.0/255.0) green:(205.0/255.0) blue:(164.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [bordersbutton setTitleColor:[[UIColor alloc] initWithRed:(167.0/255.0) green:(205.0/255.0) blue:(164.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [_textButton setTitleColor:[[UIColor alloc] initWithRed:(167.0/255.0) green:(205.0/255.0) blue:(164.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [_fontsButton setTitleColor:[[UIColor alloc] initWithRed:(167.0/255.0) green:(205.0/255.0) blue:(164.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [bordersbutton setTitleColor:[[UIColor alloc] initWithRed:(218.0/255.0) green:(221.0/255.0) blue:(226.0/255.0) alpha:1.0] forState:UIControlStateNormal];*/
    
    [storyView setReturnKeyType:UIReturnKeyDone];
    [msgText setReturnKeyType:UIReturnKeyDone];
    
    storyView.delegate = self;
    
    /*blurView = [[FXBlurView alloc] initWithFrame: CGRectMake (9, 325, 302, 107)];
    blurView.dynamic = YES;
    blurView.tintColor = [[UIColor alloc] initWithRed:(0.0/255.0) green:(255.0/255.0) blue:(0.0/255.0) alpha:1.0];
    [blurView.layer displayIfNeeded]; //force immediate redraw
    blurView.contentMode = UIViewContentModeBottom;
    [blurView setHidden:YES];
    [myScroll addSubview:blurView];
    blurView.blurRadius = 30;
    blurView.opaque = NO;
    [blurView setBackgroundColor:[[UIColor alloc] initWithRed:(255.0/255.0) green:(255.0/255.0) blue:(255.0/255.0) alpha:1.0]];*/
    myScroll.contentSize = CGSizeMake(320, 500);
    staticPictureOriginalOrientation = UIImageOrientationUp;
    
    transView.layer.shadowColor = [UIColor blackColor].CGColor;
    transView.layer.shadowOffset = CGSizeMake(0, 0);
    transView.layer.shadowOpacity = 0.3;
    transView.layer.shadowRadius = 0.5;
    transView.clipsToBounds = NO;
    
    //transview.dynamic = YES;
    //transview.tintColor = [[UIColor alloc] initWithRed:(0.0/255.0) green:(255.0/255.0) blue:(0.0/255.0) alpha:1.0];
    //[transview.layer displayIfNeeded]; //force immediate redraw
    //transview.contentMode = UIViewContentModeBottom;
    
    
    [transView setHidden:YES];
    [transView setAlpha:0.0];
  
    cropFilter = [[GPUImageCropFilter alloc] initWithCropRegion:CGRectMake(0.0f, 0.0f, 1.0f, 0.75f)];
    
    filter = [[GPUImageFilter alloc] init];
    filter2 = [[GPUImageMaskFilter alloc] init];
    
    [transView setBackgroundColor:[[UIColor alloc] initWithRed:(255.0/255.0) green:(255.0/255.0) blue:(255.0/255.0) alpha:1.0]];
    
    //NSString *client_id = @"151898678325186";
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [locationManager startUpdatingLocation];
    NSLog(@"Location  ********** %@", [self deviceLocation]);
    /*bgImage.backgroundColor = [UIColor whiteColor];
    bgImage.layer.shadowColor = [UIColor blackColor].CGColor;
    bgImage.layer.shadowOffset = CGSizeMake(0, 0);
    bgImage.layer.shadowOpacity = 0.3;
    bgImage.layer.shadowRadius = 0.5;
    bgImage.clipsToBounds = NO;*/
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"640x960bg.png"]];
    
    buttonsShadowV.layer.shadowColor = [UIColor blackColor].CGColor;
    buttonsShadowV.layer.shadowOffset = CGSizeMake(0, 0.5);
    buttonsShadowV.layer.shadowOpacity = 0.3;
    buttonsShadowV.layer.shadowRadius = 0.5;
    buttonsShadowV.clipsToBounds = NO;
    
    dropArrow1 = [[UIImageView alloc] initWithFrame:CGRectMake(31, 292, 18, 7)];
    dropArrow1.image = [UIImage imageNamed:@"buttonArrow.png"];
    
    dropArrow2 = [[UIImageView alloc] initWithFrame:CGRectMake(111, 292, 18, 7)];
    dropArrow2.image = [UIImage imageNamed:@"buttonArrow.png"];
    
    dropArrow3 = [[UIImageView alloc] initWithFrame:CGRectMake(191, 292, 18, 7)];
    dropArrow3.image = [UIImage imageNamed:@"buttonArrow.png"];
    dropArrow4 = [[UIImageView alloc] initWithFrame:CGRectMake(271, 292, 18, 7)];
    dropArrow4.image = [UIImage imageNamed:@"buttonArrow.png"];
    
    dropArrow1.alpha = 0;
    dropArrow2.alpha = 0;
    dropArrow3.alpha = 0;
    dropArrow4.alpha = 0;
    [addView addSubview:dropArrow1];
    [addView addSubview:dropArrow2];
    [addView addSubview:dropArrow3];
    [addView addSubview:dropArrow4];
    
    storyView.textAlignment = NSTextAlignmentLeft ;
    // [storyView becomeFirstResponder];
    //myScroll.contentSize=CGSizeMake(320,600);
    //myScroll.pagingEnabled=YES;
    // profielImage.image = [UIImage imageNamed:@"photo.png"];
    halfImage.layer.cornerRadius=10;
    halfImage.clipsToBounds = YES;
    //buttonTitle=@"story";
    
    storyView.font = [UIFont fontWithName:@"Myriadpro-Regular" size:13];
    msgText.font = [UIFont fontWithName:@"MyriadPro-Regular" size:10];
    
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    //idName = delegate.myname;
    dpImage=delegate.profileImage;
    idName = delegate.mynameLogin;
    NSLog(@"GET on UPload %@",idName);
    getName=delegate.mynameLogin;
    NSLog(@"GET on UPload username %@",getName);
    
    CGFloat titleWidth2=100;
    CGFloat titleWidth = MIN(titleWidth2, 300);
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 20)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Futura_Light" size:20];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=@"Upload";
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    
    navBar.barTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    
    
    
    
    //self.navigationItem.title =@"Sign Up";
    self.navigationItem.titleView = label;
    UIImage* image4 = [UIImage imageNamed:@"settingProfile2.png"];
    CGRect frameimg2 = CGRectMake(0, 0,18,18);
    UIButton *someButton2 = [[UIButton alloc] initWithFrame:frameimg2];
    [someButton2 setBackgroundImage:image4 forState:UIControlStateNormal];
    [someButton2 addTarget:self action:@selector(goSettings)
          forControlEvents:UIControlEventTouchUpInside];
    [someButton2 setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *backbutton2 =[[UIBarButtonItem alloc] initWithCustomView:someButton2];
    self.navigationItem.rightBarButtonItem=backbutton2;
    
    
    
    UIImage* image3 = [UIImage imageNamed:@"close.png"];
    CGRect frameimg = CGRectMake(0, 0,18,18);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(goLogin) forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    UIBarButtonItem *backbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.leftBarButtonItem=backbutton;
    [someButton release];
    [someButton2 release];
    //storyView.backgroundColor = [UIColor yellowColor];
    storyView.returnKeyType = UIReturnKeyDone;
    storyView.font=[UIFont fontWithName:@"MyriadPro-Regular" size:13];
    storyView.textColor=[UIColor blackColor];
    //storyView.text=@"What is on your mind !!!!";
    //[self.view addSubview:storyView];
    storyView.delegate = self;
    selectedFilter = 0;
    selectedBorder = 0;
}
-(void) loadFilters {
    for(int i = 0; i < 10; i++) {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"NS%d.png", i + 1]] forState:UIControlStateNormal];
        button.frame = CGRectMake(10+i*(65+10), 10.0f, 65.0f, 65.0f);
        button.layer.cornerRadius = 32.0f;
        
        //use bezier path instead of maskToBounds on button.layer
        UIBezierPath *bi = [UIBezierPath bezierPathWithRoundedRect:button.bounds
                                                 byRoundingCorners:UIRectCornerAllCorners
                                                       cornerRadii:CGSizeMake(30.0,30.0)];
        
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = button.bounds;
        maskLayer.path = bi.CGPath;
        button.layer.mask = maskLayer;
        
        button.layer.borderWidth = 1;
        button.layer.borderColor = [[UIColor whiteColor] CGColor];
        
       
        
        [button addTarget:self
                   action:@selector(filterClicked:)
         forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        if(i == 0){
            [button setSelected:YES];
            button.layer.borderWidth = 2.0;
            button.layer.borderColor = [[UIColor colorWithRed:(72.0/255) green:(190.0/255) blue:(78.0/255) alpha:1.0]  CGColor];
        }
		[self.filtersScrollview addSubview:button];
	}
	[self.filtersScrollview setContentSize:CGSizeMake(10 + 10*(65+10), 75.0)];
}
-(void) loadBorders {
    for(int i = 0; i < 6; i++) {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"B%d.png", i + 1]] forState:UIControlStateNormal];
        button.frame = CGRectMake(10+i*(65+10), 10.0f, 65.0f, 65.0f);
        button.layer.cornerRadius = 32.0f;
        
        //use bezier path instead of maskToBounds on button.layer
        UIBezierPath *bi = [UIBezierPath bezierPathWithRoundedRect:button.bounds
                                                 byRoundingCorners:UIRectCornerAllCorners
                                                       cornerRadii:CGSizeMake(30.0,30.0)];
        
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = button.bounds;
        maskLayer.path = bi.CGPath;
        button.layer.mask = maskLayer;
        
        button.layer.borderWidth = 0.5;
        button.layer.borderColor = [[UIColor colorWithWhite:0.0 alpha:0.3] CGColor];
        /*button.layer.shadowColor = [UIColor blackColor].CGColor;
        button.layer.shadowOffset = CGSizeMake(0, 0.5);
        button.layer.shadowOpacity = 0.3;
        button.layer.shadowRadius = 0.5;
        button.clipsToBounds = NO;*/
        
        [button addTarget:self
                   action:@selector(borderClicked:)
         forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        
        button.titleLabel.font = [UIFont systemFontOfSize:40];
        [button.titleLabel setTextAlignment: NSTextAlignmentCenter];
        if(i == 0){
            [button setSelected:YES];
            button.layer.borderWidth = 2.0;
            button.layer.borderColor = [[UIColor colorWithRed:(72.0/255) green:(190.0/255) blue:(78.0/255) alpha:1.0]  CGColor];
        }
		[self.bordersScrollview addSubview:button];
	}
	[self.bordersScrollview setContentSize:CGSizeMake(10 + 10*(65+15), 75.0)];
}
-(void) loadFonts {
    for(int i = 0; i < 11; i++) {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"FN%d.png", i + 1]] forState:UIControlStateNormal];
        button.frame = CGRectMake(10+i*(65+10), 10.0f, 65.0f, 65.0f);
        button.layer.cornerRadius = 32.0f;
        
        //use bezier path instead of maskToBounds on button.layer
        UIBezierPath *bi = [UIBezierPath bezierPathWithRoundedRect:button.bounds
                                                 byRoundingCorners:UIRectCornerAllCorners
                                                       cornerRadii:CGSizeMake(65.0,65.0)];
        
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = button.bounds;
        maskLayer.path = bi.CGPath;
        button.layer.mask = maskLayer;
        
        button.layer.borderWidth = 0.5;
        button.layer.borderColor = [[UIColor colorWithWhite:0.0 alpha:0.3] CGColor];
        /*button.layer.shadowColor = [UIColor blackColor].CGColor;
         button.layer.shadowOffset = CGSizeMake(0, 0.5);
         button.layer.shadowOpacity = 0.3;
         button.layer.shadowRadius = 0.5;
         button.clipsToBounds = NO;*/
        
        [button addTarget:self
                   action:@selector(fontClicked:)
         forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        
        button.titleLabel.font = [UIFont systemFontOfSize:40];
        [button.titleLabel setTextAlignment: NSTextAlignmentCenter];
        if(i == 0){
            [button setSelected:YES];
            button.layer.borderWidth = 2.0;
            button.layer.borderColor = [[UIColor colorWithRed:(72.0/255) green:(190.0/255) blue:(78.0/255) alpha:1.0]  CGColor];
        }
		[self.fontsScrollview addSubview:button];
	}
	[self.fontsScrollview setContentSize:CGSizeMake(10 + 10*(65+15), 80.0)];
}
-(void) loadColors {
    for(int i = 0; i < 6; i++) {
        fontColors = [[NSArray alloc] initWithObjects: [UIColor blackColor],[UIColor whiteColor],[UIColor redColor],[UIColor blueColor], [UIColor greenColor], [UIColor yellowColor], nil];
        
        
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = i;
        
        [button setBackgroundColor:[fontColors objectAtIndex:button.tag]];
        //[button setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"FN%d.png", i + 1]] forState:UIControlStateNormal];
        button.frame = CGRectMake(10+i*(30+10), 10.0f, 30.0f, 30.0f);
        button.layer.cornerRadius = 15.0f;
        
        
        
        //use bezier path instead of maskToBounds on button.layer
        UIBezierPath *bi = [UIBezierPath bezierPathWithRoundedRect:button.bounds
                                                 byRoundingCorners:UIRectCornerAllCorners
                                                       cornerRadii:CGSizeMake(30.0,30.0)];
        
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = button.bounds;
        maskLayer.path = bi.CGPath;
        button.layer.mask = maskLayer;
        
        button.layer.shadowColor = [UIColor blackColor].CGColor;
         button.layer.shadowOffset = CGSizeMake(0, 0);
         button.layer.shadowOpacity = 1;
         button.layer.shadowRadius = 5;
         button.clipsToBounds = NO;
        
        [button addTarget:self
                   action:@selector(colorClicked:)
         forControlEvents:UIControlEventTouchUpInside];
       
        
        button.titleLabel.font = [UIFont systemFontOfSize:40];
        [button.titleLabel setTextAlignment: NSTextAlignmentCenter];
        if(i == 0){
            [button setSelected:YES];
            button.layer.borderWidth = 2.0;
            button.layer.borderColor = [[UIColor colorWithRed:(72.0/255) green:(190.0/255) blue:(78.0/255) alpha:1.0]  CGColor];
        }
		[self.colorsScrollview addSubview:button];
	}
	[self.colorsScrollview setContentSize:CGSizeMake(10 + 10*(30+15), 30.0)];
}



-(void) filterClicked:(UIButton *) sender {
    for(UIView *view in self.filtersScrollview.subviews){
        if([view isKindOfClass:[UIButton class]]){
            [(UIButton *)view setSelected:NO];
            ((UIButton *)view).layer.borderWidth = 0;
            ((UIButton *)view).layer.borderColor = [[UIColor clearColor]  CGColor];
        }
    }
    
    [sender setSelected:YES];
    [self removeAllTargets];
    
    sender.layer.borderWidth = 2.0;
    sender.layer.borderColor = [[UIColor colorWithRed:(72.0/255) green:(190.0/255) blue:(78.0/255) alpha:1.0]  CGColor];
    
    selectedFilter = sender.tag;
    [self setFilter:sender.tag];
    [self prepareFilter];
}
-(void) borderClicked:(UIButton *) sender {
    for(UIView *view in self.bordersScrollview.subviews){
        if([view isKindOfClass:[UIButton class]]){
            [(UIButton *)view setSelected:NO];
            ((UIButton *)view).layer.borderWidth = 0;
            ((UIButton *)view).layer.borderColor = [[UIColor clearColor]  CGColor];
        }
    }
    
    sender.layer.borderWidth = 2.0;
    sender.layer.borderColor = [[UIColor colorWithRed:(72.0/255) green:(190.0/255) blue:(78.0/255) alpha:1.0]  CGColor];
    
    
    selectedBorder = sender.tag;
    [self setBorder:sender.tag];
    
    
    
}
-(void) fontClicked:(UIButton *) sender {
    for(UIView *view in self.fontsScrollview.subviews){
        if([view isKindOfClass:[UIButton class]]){
            [(UIButton *)view setSelected:NO];
            ((UIButton *)view).layer.borderWidth = 0;
            ((UIButton *)view).layer.borderColor = [[UIColor clearColor]  CGColor];
        }
    }
    
    sender.layer.borderWidth = 2.0;
    sender.layer.borderColor = [[UIColor colorWithRed:(72.0/255) green:(190.0/255) blue:(78.0/255) alpha:1.0]  CGColor];
    
    
    
    [self setFont:sender.tag];
    
    
    
}
-(void) colorClicked:(UIButton *) sender {
    for(UIView *view in self.colorsScrollview.subviews){
        if([view isKindOfClass:[UIButton class]]){
            [(UIButton *)view setSelected:NO];
            ((UIButton *)view).layer.borderWidth = 0;
            ((UIButton *)view).layer.borderColor = [[UIColor clearColor]  CGColor];
        }
    }
    
    sender.layer.borderWidth = 2.0;
    sender.layer.borderColor = [[UIColor colorWithRed:(72.0/255) green:(190.0/255) blue:(78.0/255) alpha:1.0]  CGColor];
    
    imageText.textColor = [fontColors objectAtIndex:sender.tag];
   
    
    
    
}


-(void) setFilter:(int) index {
    switch (index) {
        case 1:{
            filter = [[GPUImageContrastFilter alloc] init];
            [(GPUImageContrastFilter *) filter setContrast:1.75];
        } break;
        case 2: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"2"];
        } break;
        case 3: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"3"];
        } break;
        case 4: {
            filter = [[DLCGrayscaleContrastFilter alloc] init];
        } break;
        case 5: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"5"];
        } break;
        case 6: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"6"];
        } break;
        case 7: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"7"];
        } break;
        case 8: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"8"];
        } break;
        case 9: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"9"];
        } break;
        default:
            filter = [[GPUImageFilter alloc] init];
            break;
    }
}
-(void) setFont:(int) index {
    switch (index) {
        case 1:{
            imageText.font = [UIFont fontWithName:@"Action Man" size:16.0];
        } break;
        case 2: {
            imageText.font = [UIFont fontWithName:@"Aex Brush-Regular" size:16.0];
            
        } break;
        case 3: {
            imageText.font = [UIFont fontWithName:@"Bebas Neue" size:16.0];
            
        } break;
        case 4: {
            imageText.font = [UIFont fontWithName:@"Carrington" size:16.0];
          
        } break;
        case 5: {
            imageText.font = [UIFont fontWithName:@"Caviar Dreams" size:16.0];
            
        } break;
        case 6: {
            imageText.font = [UIFont fontWithName:@"Exo-Light" size:16.0];
            
        } break;
        case 7: {
            imageText.font = [UIFont fontWithName:@"Kaushan Script" size:16.0];
            
            
        } break;
        case 8: {
            imageText.font = [UIFont fontWithName:@"Oswald" size:16.0];
            
           
        } break;
        case 9: {
            imageText.font = [UIFont fontWithName:@"Snickles" size:18.0];
            
        } break;
        case 10: {
            imageText.font = [UIFont fontWithName:@"Sofia" size:16.0];
            
            
        } break;
        default:
            imageText.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0];
           
            break;
    }
}
-(void) setBorder:(int) index {
    switch (index) {
        case 1:{
           
            borderImage = [UIImage imageNamed:@"OL1.png"];
            
        
            self.borderView.image = borderImage;
            /*if(selectedFilter == 0){
                imageView2.image = imageView2.image;
                
            }else{
                UIImage *currentFilteredImage = [filter imageFromCurrentlyProcessedOutput];
                imageView2.image = currentFilteredImage;
            }
            
           GPUImageAlphaBlendFilter *overlayBlendFilter = [[GPUImageAlphaBlendFilter alloc] init];
            GPUImagePicture *pic1 = [[GPUImagePicture alloc] initWithImage:imageView2.image];
            GPUImagePicture *pic2 = [[GPUImagePicture alloc] initWithImage:borderImage];
            overlayBlendFilter.mix = 1.0f;
            
            [pic1 addTarget:overlayBlendFilter];
            [pic1 processImage];
            [pic2 addTarget:overlayBlendFilter];
            [pic2 processImage];
            [overlayBlendFilter addTarget:self.imageView];
            
            NSLog(@"adding imageView2");
            imageView2.image = [overlayBlendFilter imageFromCurrentlyProcessedOutput];*/
            
            noBorder = YES;
          
           
           
            
        
        } break;
        case 2: {
            borderImage = [UIImage imageNamed:@"OL2.png"];
            
            
            self.borderView.image = borderImage;
            noBorder = YES;
         
            
            /*borderImage = [UIImage imageNamed:@"mask3.png"];
            GPUImageAlphaBlendFilter *overlayBlendFilter = [[GPUImageAlphaBlendFilter alloc] init];
            GPUImagePicture *pic2 = [[GPUImagePicture alloc] initWithImage:borderImage];
            overlayBlendFilter.mix = 1.0f;
            
            [staticPicture addTarget:overlayBlendFilter];
            [staticPicture processImage];
            [pic2 addTarget:overlayBlendFilter];
            [pic2 processImage];
            [overlayBlendFilter addTarget:self.imageView];
            imageView2.image = [overlayBlendFilter imageFromCurrentlyProcessedOutput];
            imageView2.hidden = NO;*/
            
            
            
            
            
        } break;
        case 3: {
            borderImage = [UIImage imageNamed:@"OL3.png"];
            
            
            self.borderView.image = borderImage;
            
        } break;
        case 4: {
            borderImage = [UIImage imageNamed:@"OL4.png"];
            
            
            self.borderView.image = borderImage;
            
        } break;
        case 5: {
            borderImage = [UIImage imageNamed:@"OL5.png"];
            
            
            self.borderView.image = borderImage;
            
        } break;
        case 6: {
          
           
        } break;
        case 7: {
            
        } break;
        case 8: {
            
        } break;
        case 9: {
            
        } break;
        default:
            borderImage = nil;
            self.borderView.image = nil;
            noBorder = YES;
            
            break;
    }
}
-(void)addBorderToImage {
    
   
    GPUImageAlphaBlendFilter *overlayBlendFilter = [[GPUImageAlphaBlendFilter alloc] init];
    GPUImagePicture *pic2 = [[GPUImagePicture alloc] initWithImage:borderImage];
    overlayBlendFilter.mix = 1.0f;
    
    [staticPicture addTarget:overlayBlendFilter];
    [staticPicture processImage];
    [pic2 addTarget:overlayBlendFilter];
    [pic2 processImage];
    [overlayBlendFilter addTarget:self.imageView];
    
    NSLog(@"adding imageView2");
    imageView2.image = [overlayBlendFilter imageFromCurrentlyProcessedOutput];
    
    /*borderView.image = [overlayBlendFilter imageFromCurrentlyProcessedOutput];
    borderView.hidden = NO;*/
}
-(void)finalizeImageForUpload{
     self.imageView.layer.shadowColor = [UIColor clearColor].CGColor;
    self.imageView.backgroundColor = [UIColor clearColor];
    if(self.textButton.selected)
    {
        UIGraphicsBeginImageContextWithOptions([saveView bounds].size, YES, 0.0f);
        [[saveView layer] renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        imageView2.image = outputImage;

        /*if(selectedFilter == 0){
                UIGraphicsBeginImageContextWithOptions([saveView bounds].size, YES, 0.0f);
                [[saveView layer] renderInContext:UIGraphicsGetCurrentContext()];
                UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                imageView2.image = outputImage;
        }else {
            
            NSLog(@"adding filtered Image");
            [filter addTarget:self.imageView];
            UIImage *currentFilteredImage = [filter imageFromCurrentlyProcessedOutput];
            imageView2.image = currentFilteredImage;
            
            UIGraphicsBeginImageContextWithOptions([saveView bounds].size, YES, 0.0f);
            [[saveView layer] renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            imageView2.image = outputImage;
            
        }
        if(selectedBorder == 0){
                UIGraphicsBeginImageContextWithOptions([saveView bounds].size, YES, 0.0f);
                [[saveView layer] renderInContext:UIGraphicsGetCurrentContext()];
                UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                imageView2.image = outputImage;
        }else {
            GPUImageAlphaBlendFilter *overlayBlendFilter = [[GPUImageAlphaBlendFilter alloc] init];
            GPUImagePicture *pic1 = [[GPUImagePicture alloc] initWithImage:imageView2.image];
            GPUImagePicture *pic2 = [[GPUImagePicture alloc] initWithImage:borderImage];
            overlayBlendFilter.mix = 1.0f;
            
            [pic1 addTarget:overlayBlendFilter];
            [pic1 processImage];
            [pic2 addTarget:overlayBlendFilter];
            [pic2 processImage];
            [overlayBlendFilter addTarget:self.imageView];

            NSLog(@"adding imageView2");
            imageView2.image = [overlayBlendFilter imageFromCurrentlyProcessedOutput];
            
            UIGraphicsBeginImageContextWithOptions([saveView bounds].size, YES, 0.0f);
            [[saveView layer] renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            imageView2.image = outputImage;
        }*/
        
        
    } else {
        if(selectedFilter == 0){
            imageView2.image = imageView2.image;
            
        } else {
            UIImage *currentFilteredImage = [filter imageFromCurrentlyProcessedOutput];
            imageView2.image = currentFilteredImage;
            
        }
        if(selectedBorder == 0){
            imageView2.image = imageView2.image;
        } else {
            GPUImageAlphaBlendFilter *overlayBlendFilter = [[GPUImageAlphaBlendFilter alloc] init];
            GPUImagePicture *pic1 = [[GPUImagePicture alloc] initWithImage:imageView2.image];
            GPUImagePicture *pic2 = [[GPUImagePicture alloc] initWithImage:borderImage];
            overlayBlendFilter.mix = 1.0f;
            
            [pic1 addTarget:overlayBlendFilter];
            [pic1 processImage];
            [pic2 addTarget:overlayBlendFilter];
            [pic2 processImage];
            [overlayBlendFilter addTarget:self.imageView];
            
            
            
            NSLog(@"adding imageView2");
            imageView2.image = [overlayBlendFilter imageFromCurrentlyProcessedOutput];
            
        }
        
    }
   /* if(selectedFilter == 0){
        if(self.textButton.selected){
            UIGraphicsBeginImageContextWithOptions([saveView bounds].size, YES, 0.0f);
            [[saveView layer] renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            imageView2.image = outputImage;
        }else {
        imageView2.image = imageView2.image;
       
        
        
        
        }
        
        
    }else{
        if(self.textButton.selected){
            UIImage *currentFilteredImage = [filter imageFromCurrentlyProcessedOutput];
            imageView2.image = currentFilteredImage;
            UIGraphicsBeginImageContextWithOptions([saveView bounds].size, YES, 0.0f);
            [[saveView layer] renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            imageView2.image = outputImage;
        }else {
            
        UIImage *currentFilteredImage = [filter imageFromCurrentlyProcessedOutput];
        imageView2.image = currentFilteredImage;
    }
    }
    if(selectedBorder == 0){
         if(self.textButton.selected){
             UIGraphicsBeginImageContextWithOptions([saveView bounds].size, YES, 0.0f);
             [[saveView layer] renderInContext:UIGraphicsGetCurrentContext()];
             UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
             UIGraphicsEndImageContext();
             imageView2.image = outputImage;
         } else {
             imageView2.image = imageView2.image;
         }
        
        
    }else {
        if(self.textButton.selected){
            
            GPUImageAlphaBlendFilter *overlayBlendFilter = [[GPUImageAlphaBlendFilter alloc] init];
            GPUImagePicture *pic1 = [[GPUImagePicture alloc] initWithImage:imageView2.image];
            GPUImagePicture *pic2 = [[GPUImagePicture alloc] initWithImage:borderImage];
            overlayBlendFilter.mix = 1.0f;
            
            [pic1 addTarget:overlayBlendFilter];
            [pic1 processImage];
            [pic2 addTarget:overlayBlendFilter];
            [pic2 processImage];
            [overlayBlendFilter addTarget:self.imageView];
            
            
            
            NSLog(@"adding imageView2");
            imageView2.image = [overlayBlendFilter imageFromCurrentlyProcessedOutput];
            
            UIGraphicsBeginImageContextWithOptions([saveView bounds].size, YES, 0.0f);
            [[saveView layer] renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            imageView2.image = outputImage;
            
        }else{
            
        GPUImageAlphaBlendFilter *overlayBlendFilter = [[GPUImageAlphaBlendFilter alloc] init];
        GPUImagePicture *pic1 = [[GPUImagePicture alloc] initWithImage:imageView2.image];
        GPUImagePicture *pic2 = [[GPUImagePicture alloc] initWithImage:borderImage];
        overlayBlendFilter.mix = 1.0f;
        
        [pic1 addTarget:overlayBlendFilter];
        [pic1 processImage];
        [pic2 addTarget:overlayBlendFilter];
        [pic2 processImage];
        [overlayBlendFilter addTarget:self.imageView];
            
            
        
        NSLog(@"adding imageView2");
        imageView2.image = [overlayBlendFilter imageFromCurrentlyProcessedOutput];
            
            
    }
    }*/
  
    
   
}
+(UIImage *)grabImageFromView: (UIView *) viewToGrab {
    
    UIGraphicsBeginImageContext(viewToGrab.bounds.size);
    
    [[viewToGrab layer] renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return viewImage;
}
-(void) prepareFilter {
    if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
        isStatic = YES;
    }
    
    if (!isStatic) {
        [self prepareLiveFilter];
    } else {
        [self prepareStaticFilter];
        [self prepareStaticBorder];
        
    }
}

-(void) prepareLiveFilter {
    
    [stillCamera addTarget:cropFilter];
    [cropFilter addTarget:filter];
    //blur is terminal filter
    
    [filter addTarget:_imageView];
    
    [filter prepareForImageCapture];
    
}


-(void) prepareStaticFilter {
    
    [staticPicture addTarget:filter];
    
    
    // blur is terminal filter
    
    [filter addTarget:self.imageView];
   
    
    
    GPUImageRotationMode imageViewRotationMode = kGPUImageNoRotation;
    switch (staticPictureOriginalOrientation) {
        case UIImageOrientationLeft:
            imageViewRotationMode = kGPUImageRotateLeft;
            break;
        case UIImageOrientationRight:
            imageViewRotationMode = kGPUImageRotateRight;
            break;
        case UIImageOrientationDown:
            imageViewRotationMode = kGPUImageRotate180;
            break;
        default:
            imageViewRotationMode = kGPUImageNoRotation;
            break;
    }
    
    // seems like atIndex is ignored by GPUImageView...
    [_imageView setInputRotation:imageViewRotationMode atIndex:0];
    
    
    [staticPicture processImage];
}
-(void) prepareStaticBorder {
    
    [staticPicture addTarget:filter2];
    
    // blur is terminal filter
    
    [filter2 addTarget:self.imageView];
    
    
    GPUImageRotationMode imageViewRotationMode = kGPUImageNoRotation;
    switch (staticPictureOriginalOrientation) {
        case UIImageOrientationLeft:
            imageViewRotationMode = kGPUImageRotateLeft;
            break;
        case UIImageOrientationRight:
            imageViewRotationMode = kGPUImageRotateRight;
            break;
        case UIImageOrientationDown:
            imageViewRotationMode = kGPUImageRotate180;
            break;
        default:
            imageViewRotationMode = kGPUImageNoRotation;
            break;
    }
    
    // seems like atIndex is ignored by GPUImageView...
    [_imageView setInputRotation:imageViewRotationMode atIndex:0];
    
    
    [staticPicture processImage];
}

-(void) removeAllTargets {
    [stillCamera removeAllTargets];
    [staticPicture removeAllTargets];
    [cropFilter removeAllTargets];
    
    //regular filter
    [filter removeAllTargets];
    
    //blur
    
}

-(IBAction)goLogin
{
    
    // if ([delegate.newsId isEqualToString:@"newsId"]) {
    //[self.slidingViewController anchorTopViewTo:ECRight];
    // }
    // else
    //{
    if([idAtUpload isEqualToString:@"circle"])
    {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:0];
        [storyView setHidden:FALSE];
        
    }
    if([idAtUpload isEqualToString:@"profile"])
    {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
        [storyView setHidden:FALSE];
    }
    if([idAtUpload isEqualToString:@"settings"])
    {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:3];
        [storyView setHidden:FALSE];
        
    }
    if([idAtUpload isEqualToString:@"upload"])
    {
        
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:0];
        
       
        [storyView setHidden:FALSE];
        
    }
    //}
    
}
-(IBAction)cancel
{
    [self viewDidLoad];
    [imageView2 setHidden:TRUE];
    [storyView setHidden:FALSE];
    storyView.text=@"";
    
}
- (void)scrollViewToCenterOfScreen:(UIView *)theView {
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    CGFloat availableHeight = applicationFrame.size.height - 100;            // Remove area covered by keyboard
    
    CGFloat y = viewCenterY - availableHeight / 2.5;
    if (y < 0) {
        y = 0;
    }
    [myScroll setContentOffset:CGPointMake(0, y) animated:YES];
    [myScroll setScrollEnabled:YES];
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"touchesBegan:withEvent:");
    [self.view endEditing:YES];
    [storyView resignFirstResponder];
    [super touchesBegan:touches withEvent:event];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    NSLog(@"textViewShouldBeginEditing:");
    if(self.enlargeText.selected == YES)
    {
    
    }else {
        

        [self scrollViewToCenterOfScreen:textView];
    }
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView {
    // buttonTitle=@"story";
    
    NSLog(@"textViewDidBeginEditing:");
    if (textView.textColor == [UIColor lightGrayColor]) {
        textView.textColor=[UIColor blackColor];
        textView.text=@"";
    }
    
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    NSLog(@"textViewShouldEndEditing:");
  
    
    return YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    
  [myScroll setContentOffset:CGPointMake(0, -64) animated:YES];
    NSLog(@"textViewDidEndEditing:");
    NSMutableAttributedString * string;
    string = [[NSMutableAttributedString alloc]initWithString:textView.text];
    NSArray *words=[textView.text componentsSeparatedByString:@" "];
    CGRect frame = imageText.frame;
    frame.size.height = imageText.contentSize.height;
    imageText.frame = frame;
    
    if(imageText.text.length == 0)
    {
        imageText.textColor = [UIColor lightGrayColor];
        imageText.text = @"Click here to edit text";
    }

    
    for (NSString *word in words) {
        if (([word hasPrefix:@"#"])||([word hasPrefix:@"@"])) {
            NSRange range=[textView.text rangeOfString:word];
            NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1],NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:13],
                                         NSBackgroundColorAttributeName: [UIColor whiteColor]//,NSUnderlineStyleAttributeName: @1
                                         
                                         };
            [string addAttributes:attributes range:range];
            [textView setAttributedText:string];
        }
    }
    
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSCharacterSet *doneButtonCharacterSet = [NSCharacterSet newlineCharacterSet];
    NSRange replacementTextRange = [text rangeOfCharacterFromSet:doneButtonCharacterSet];
    NSUInteger location = replacementTextRange.location;
    if (textView.text.length + text.length >1000){
        if (location != NSNotFound){
            [textView resignFirstResponder];
        }
        return NO;
    }
    else if (location != NSNotFound){
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
-(void)textViewDidChange:(UITextView *)textView{
    CGRect frame = imageText.frame;
    frame.size.height = imageText.contentSize.height;
    imageText.frame = frame;
    NSLog(@"textViewDidChange:");
}
- (void)textViewDidChangeSelection:(UITextView *)textView{
    NSLog(@"textViewDidChangeSelection:");
}
-(IBAction)postStory:(id)sender
{
    NSLog(@"Button pressed: %@", [sender currentTitle]);
    buttonTitle=[sender currentTitle];
   
}
-(void)uploadStory{
  
    [indicator startAnimating];
    
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^ {
                         self.blurAcView.alpha = 1.0f;
                     }
                     completion:^(BOOL finished) {
                         [self blurViewIn];
                      
                         
                     }];

    
   
    [self performSelector:@selector(indicatorStory) withObject:nil afterDelay:0.1];
}
-(void) blurViewIn {
    [indicator startAnimating];
    [UIView animateWithDuration:0.5 animations:^{
        self.blurAcView.blurRadius = 40;
        indicator.alpha = 1.0f;
    }];
}

-(void) removeBlurView {
    [UIView animateWithDuration:0.5 animations:^{
        self.blurAcView.blurRadius = 0;
    }];
    [self removeBlurViewAlpha];
    
    
    
    
    
    
}
-(void) removeActiView {
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^ {
                         indicator.alpha = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         [self removeBlurView];
                         
                         
                     }];
    
    
}
-(void) removeBlurViewAlpha {
    [UIView animateWithDuration:0.5 animations:^{
        self.blurAcView.alpha = 0.0f;
    }];
    
}
-(void)indicatorStory{
    
    NSLog(@"Story %@",storyView.text);
    NSString *converted = [storyView.text stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/story_upload.do?username=%@&story=%@&latitude=%f&longitude=%f",idName,converted,locationManager.location.coordinate.latitude,locationManager.location.coordinate.longitude];
    NSLog(@"URL String ************** %@",url);
    NSURL *urlrequest=[NSURL URLWithString:url];
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    NSData *data = [NSData dataWithContentsOfURL:urlrequest];
    NSLog(@"values in datafordistance is:- %@",data);
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
    NSLog(@"JSON format:- %@",json);
    authStr = [json objectForKey:@"param"];
    NSLog(@"valu in elemant is:- %@",authStr);
    uploadedElapsed=[json objectForKey:@"elapsed"];
    uploadedInserted=[json objectForKey:@"timestamp"];
    uploadedSno=[json objectForKey:@"sno"];
    uploadedLocation=[json objectForKey:@"location"];
    uploadedType=@"story";
    if([authStr isEqualToString:@"True"])
    {
        uploadedData=storyView.text;
        uploadedType=@"story";
        [self addCurentUpdate:uploadedData :uploadedType];
        
        if (flagFb == 1) {
            
            [self postStoryOnFb];
            NSLog(@"posting story to facebook");
            
        }
       
         [self removeActiView];
        //[indicator stopAnimating];
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
        [imageView2 setHidden:TRUE];
     
        [storyView setHidden:FALSE];
        
        
        
       /* if (flagTu==1) {
            [self sendToTumblr];
        }
        if (flagTw==1) {
            [self postStatusToTwitter];
        }*/
        storyView.text=@"";
    }
    else if([authStr isEqualToString:@"False"])
    {
        [indicator stopAnimating];
        alert =[[UIAlertView alloc]initWithTitle:@"Failed" message:@"Try agian" delegate:self cancelButtonTitle:@"ok"otherButtonTitles:nil];
        [alert show];
    }
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)imagePicker
{
    [self dismissViewControllerAnimated:NO completion:nil];
  if (isStatic == NO)
  {
      [_captureImage setHidden:NO];
      [_captureVIdeo setHidden:NO];
      [_galleryImage setHidden:NO];
      _imageView.layer.borderColor = [[UIColor alloc] initWithRed:(203.0/255.0) green:(232.0/255.0) blue:(198.0/255.0) alpha:1.0].CGColor;
      _imageView.layer.borderWidth = 2;
      self.imageView.layer.shadowColor = [UIColor clearColor].CGColor;
  
     
  }else if (isStatic == YES) {
      [_captureImage setHidden:YES];
      [_captureVIdeo setHidden:YES];
      [_galleryImage setHidden:YES];
      _imageView.layer.borderColor = [UIColor clearColor].CGColor;
      _imageView.layer.borderWidth = 0;
      self.imageView.backgroundColor = [UIColor clearColor];
      self.imageView.layer.shadowColor = [UIColor blackColor].CGColor;
      self.imageView.layer.shadowOffset = CGSizeMake(0, 0);
      self.imageView.layer.shadowOpacity = 0.3;
      self.imageView.layer.shadowRadius = 0.5;
      self.imageView.clipsToBounds = NO;
   
  }
}
-(IBAction)browseImage:(id)sender
{
    // [imageView setHidden:FALSE];
    extension=@"testImage.png";
    NSLog(@"Button pressed: %@", [sender currentTitle]);
    buttonTitle=[sender currentTitle];
    [storyView setHidden:FALSE];
    UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
    pickerView.allowsEditing = YES;
    pickerView.delegate = self;
    pickerView.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:pickerView animated:YES completion:nil];
}
-(IBAction)captureImage:(id)sender
{
    //[imageView setHidden:FALSE];
    extension=@"testImage.png";
    NSLog(@"Button pressed: %@", [sender currentTitle]);
    buttonTitle=[sender currentTitle];
    // [storyView setHidden:TRUE];
    UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
    pickerView.allowsEditing = YES;
    pickerView.delegate = self;
    pickerView.sourceType=UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:pickerView animated:YES completion:nil];
    
}

-(IBAction)captureVideo:(id)sender;
{
    //[imageView setHidden:FALSE];
    extension=@"testVideo.mov";
    buttonTitle=[sender currentTitle];
    [storyView setHidden:TRUE];
    storyView.text=@"";
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
	{
        UIImagePickerController *videoRecorder = [[UIImagePickerController alloc] init];
        videoRecorder.sourceType = UIImagePickerControllerSourceTypeCamera;
        videoRecorder.delegate = self;
        NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
        NSArray *videoMediaTypesOnly = [mediaTypes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(SELF contains %@)", @"movie"]];
        
        if ([videoMediaTypesOnly count] == 0)		//Is movie output possible?
        {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Sorry but your device does not support video recording"
                                                                     delegate:nil
                                                            cancelButtonTitle:@"OK"
                                                       destructiveButtonTitle:nil
                                                            otherButtonTitles:nil];
            [actionSheet showInView:[[self view] window]];
            [actionSheet autorelease];
        }
        else
        {
            //Select front facing camera if possible
            //if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront])
            if([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraCaptureModeVideo])
                videoRecorder.cameraDevice = UIImagePickerControllerCameraDeviceRear;
            videoRecorder.delegate = self;
            videoRecorder.mediaTypes = videoMediaTypesOnly;
            videoRecorder.videoQuality = UIImagePickerControllerQualityTypeMedium;
            videoRecorder.videoMaximumDuration = 180;			//Specify in seconds (600 is default)
            [self presentViewController:videoRecorder animated:YES completion:nil];
        }
        [videoRecorder release];
    }
    else
    {
        //No camera is availble
    }
}

- (IBAction)postOnFb:(UIButton *)sender {
    
   
    
    int tag = sender.tag;
    if (sender.selected == TRUE) {
        
        [sender setSelected:FALSE];
        [fbButton setSelected:FALSE];
       NSLog(@"Facebook Sharing OFF");
        flagFb = 0;
       
    }
    else if (sender.selected == FALSE) {
        
        if (FBSession.activeSession.state == FBSessionStateOpen
            || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {

        [sender setSelected:TRUE];
         [fbButton setSelected:TRUE];
        NSLog(@"Facebook Sharing ON");
        flagFb = 1;
        } else {
            [sender setSelected:FALSE];
            [fbButton setSelected:FALSE];
            NSLog(@"Facebook Sharing OFF");
            flagFb = 0;
            [self showSettings:@"You currently don't have a facebook account linked, please go to settings and login" withTitle:@"No Account"];
            
       
    }

}
}
- (void)showSettings:(NSString *)text withTitle:(NSString *)title
{
        /*[[[UIAlertView alloc] initWithTitle:title
                                    message:text
                                   delegate:self
                          cancelButtonTitle:@"Close"
                          otherButtonTitles:@"Settings",nil] show];*/
    
    UIAlertView *tmp = [[UIAlertView alloc]
                        initWithTitle:title
                        message:text
                        delegate:self
                        cancelButtonTitle:@"Close"
                        otherButtonTitles:@"settings", nil];
    
    tmp.tag = 100;
    [tmp show];

    
    
    }
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        if (alertView.tag==100) {
            [self performSegueWithIdentifier:@"uploadShare" sender:self];
        NSLog(@"user pressed Button Indexed 0");
    }
   }
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"uploadShare"])
    {
        // Get reference to the destination view controller
        ShareSettingsView *settings = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        [settings createDismissButton];
    }
}
-(BOOL)startMediaBrowserFromViewController:(UIViewController*)controller usingDelegate:(id )delegate {
    // 1 - Validations
    if (([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
        || (delegate == nil)
        || (controller == nil)) {
        return NO;
    }
    // 2 - Get image picker
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    mediaUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    mediaUI.allowsEditing = YES;
    mediaUI.delegate = delegate;
    // 3 - Display image picker
    [controller presentViewController:mediaUI animated:YES completion:nil];
    return YES;
}


# pragma delegate methods of uiimagepickercontrioller

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [storyView setHidden:FALSE];
    [_captureImage setHidden:YES];
    [_captureVIdeo setHidden:YES];
    [_galleryImage setHidden:YES];
    _imageView.layer.borderWidth = 0;
    _imageView.layer.borderColor = [UIColor clearColor].CGColor;
    self.imageView.backgroundColor = [UIColor clearColor];
    self.imageView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.imageView.layer.shadowOffset = CGSizeMake(0, 0);
    self.imageView.layer.shadowOpacity = 0.3;
    self.imageView.layer.shadowRadius = 0.5;
    self.imageView.clipsToBounds = NO;
    dropArrow1.alpha = 0;
    dropArrow2.alpha = 0;
    dropArrow3.alpha = 0;
    dropArrow4.alpha = 0;
   
    [self dismissViewControllerAnimated:true completion:^{
        _imageView.layer.borderWidth = 0;
        _imageView.layer.borderColor = [UIColor clearColor].CGColor;
        self.imageView.layer.shadowColor = [UIColor blackColor].CGColor;
        self.imageView.layer.shadowOffset = CGSizeMake(0, 0);
        self.imageView.layer.shadowOpacity = 0.3;
        self.imageView.layer.shadowRadius = 0.5;
        self.imageView.clipsToBounds = NO;
        dropArrow1.alpha = 0;
        dropArrow2.alpha = 0;
        dropArrow3.alpha = 0;
        dropArrow4.alpha = 0;
    
    }];
    UIImage * img = [info valueForKey:UIImagePickerControllerEditedImage];
    UIImage * img2 = [info valueForKey:UIImagePickerControllerEditedImage];
    
    
    if([buttonTitle isEqualToString:@"camera"])
    {
        [imageView2 setHidden:FALSE];
        img = [info valueForKey:UIImagePickerControllerEditedImage];
        NSLog(@"image nme********* %@",img);
        img2 = [info valueForKey:UIImagePickerControllerEditedImage];
        imageView2.image = img;
        staticPicture = [[GPUImagePicture alloc] initWithImage:img2 smoothlyScaleOutput:YES];
        
        [self prepareStaticFilter];
        isStatic = YES;
    }
    if([buttonTitle isEqualToString:@"gallery"])
    {
        
        [imageView2 setHidden:FALSE];
        img = [info valueForKey:UIImagePickerControllerEditedImage];
        NSLog(@"image nme********* %@",img);
        img2 = [info valueForKey:UIImagePickerControllerEditedImage];
        UIImage* outputImage = [info objectForKey:UIImagePickerControllerEditedImage];
        imageView2.image = img;
        staticPictureOriginalOrientation = outputImage.imageOrientation;
        
        staticPicture = [[GPUImagePicture alloc] initWithImage:outputImage smoothlyScaleOutput:YES];
        [self prepareStaticFilter];
        isStatic = YES;
        
    }
    if([buttonTitle isEqualToString:@"video"])
    {
        [imageView2 setHidden:FALSE];
        UIImage *play = [UIImage imageNamed:@"thumbVideo.png"];
        imageView2.image = play;
        _selectedVideoUrl = [info objectForKey:UIImagePickerControllerMediaURL];
        
        NSLog(@"selected video url is:- %@",_selectedVideoUrl);
        videourlarray = [[NSMutableArray alloc]initWithCapacity:1];
        [videourlarray addObject:_selectedVideoUrl];
        
        
        
       
        
        AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL:_selectedVideoUrl options:nil];
        AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
        generate1.appliesPreferredTrackTransform = YES;
        NSError *err = NULL;
        CMTime time = CMTimeMake(1, 2);
        CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
        UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
        imageView2.image = one;
        imageView2.contentMode = UIViewContentModeScaleAspectFit;
        
      
    }
    
    
}
-(void)upload{
 
    
    [indicator startAnimating];
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^ {
                         self.blurAcView.alpha = 1.0f;
                     }
                     completion:^(BOOL finished) {
                         [self blurViewIn];
                         
                         
                     }];
    

    
   /* if(borderView.image){
     [self addBorderToImage];
    }else{
        
        
    }*/
    NSLog(@"About to call indicator");
    [self performSelector:@selector(indicator) withObject:nil afterDelay:0.1];
}
-(void)indicator
{
    
    NSLog(@"helllooooooooo");
    NSData *imageData =[[NSData alloc]init];
    if([buttonTitle isEqualToString:@"camera"])
    {
        imageData = UIImageJPEGRepresentation(imageView2.image, 90);
        /*if(selectedFilter == 0)
    {
        
        imageData = UIImageJPEGRepresentation(imageView2.image, 90);
        
    } else {
        
        UIImage *currentFilteredImage = [filter imageFromCurrentlyProcessedOutput];
        imageData = UIImageJPEGRepresentation(currentFilteredImage, 90);
    }*/
        uploadedType=@"image";
    }
    if([buttonTitle isEqualToString:@"gallery"])
    {
        imageData = UIImageJPEGRepresentation(imageView2.image, 90);
        
        /*if(selectedFilter == 0)
    {
        
        imageData = UIImageJPEGRepresentation(imageView2.image, 90);
        
    } else {
        
        UIImage *currentFilteredImage = [filter imageFromCurrentlyProcessedOutput];
        imageData = UIImageJPEGRepresentation(currentFilteredImage, 90);
    }*/
uploadedType=@"image";
    }
    if([buttonTitle isEqualToString:@"video"])
    {
        NSLog(@"selected url is: - %@",_selectedVideoUrl);
        uploadedType=@"video";
        imageData = [NSData dataWithContentsOfURL:_selectedVideoUrl];
        //NSLog(@"%@",imageData);
    }
    NSString *theLat = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
    NSString *theLong = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
    
    NSString *urlString = @"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/upload.do?";
    NSLog(@"hiiiiiiihellooooo %@",urlString);
    // NSString *urlString=@"http://192.168.10.53:8080/Cancer_final/upload.do?";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    [request setValue:@"300"       forHTTPHeaderField:@"Keep-Alive"];
    [request setValue:@"keep-live" forHTTPHeaderField:@"Connection"];
    
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *postBody = [NSMutableData data];
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *str=[[[[[[[[[@"Content-Disposition: form-data;name=\"" stringByAppendingString:idName] stringByAppendingString:@",iphone,"] stringByAppendingString:theLat] stringByAppendingString:@","]stringByAppendingString:theLong] stringByAppendingString:@"\";"]
                     stringByAppendingString:@"filename=\""]stringByAppendingString:extension] stringByAppendingFormat:@"\"\r\n"];
    
    NSLog(@"*******%@",str);
    
    [postBody appendData:[[NSString stringWithString:str] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:imageData];
    [postBody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    //append caption
    
    
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
     NSString *caption=[[@"Content-Disposition: form-data;name=\"" stringByAppendingString:@"caption"]stringByAppendingFormat:@"\"\r\n"];
    
     NSLog(@"*************%######@",caption);
    
    [postBody appendData:[[NSString stringWithString:caption] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Type: text/plain\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[[NSString stringWithString:storyView.text] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    
    [request setHTTPBody:postBody];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"yepieeeeeeeeee %@",returnString);
    NSURL *urlrequest=[NSURL URLWithString:urlString];
    NSData *data = [NSData dataWithContentsOfURL:urlrequest];
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
    NSLog(@"JSON format:- %@",json);
    authStr = [json objectForKey:@"message"];
    NSLog(@"valu in elemant is:- %@",authStr);
    uploadedData=[json objectForKey:@"link"];
    uploadedElapsed=[json objectForKey:@"elapsed"];
    uploadedInserted=[json objectForKey:@"timestamp"];
    uploadedSno=[json objectForKey:@"sno"];
    uploadedLocation=[json objectForKey:@"location"];
    
    
    
    if([authStr isEqualToString:@"success"])
    {
        NSLog(@"Yes success is working");
       
       [self addCurentUpdate:uploadedData :uploadedType];
  
        
        if (flagFb == 1) {
            
            [self postImagetoFacebook];
            NSLog(@"posting image to facebook");
            
        }

    
    //    if (flagTu==1) {
    //
    //        [self sendToTumblr];
    //
    //    }
    //    if (flagFl==1) {
    //        [self postImageToFlickr];
    //    }
    //    if (flagTw==1) {
    //        [self postImageToTwitter];
    //    }
    //
        [indicator stopAnimating];
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
         [self removeAfterSuccess];
        
        
        [storyView setHidden:FALSE];
    }
    else if([authStr isEqualToString:@"Please try again later there is some problem in network"])
    {
        [indicator stopAnimating];
        alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"Uploading failed" delegate:self cancelButtonTitle:@"Try again"otherButtonTitles:nil];
        alert.tag=1;
        [alert show];
    }
   
}

-(void) postImagetoFacebook {
    
        NSData *imageData = UIImageJPEGRepresentation(imageView2.image, 90);
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    [params setObject: storyView.text forKey:@"message"];
    [params setObject:imageData forKey:@"picture"];
    [FBRequestConnection startWithGraphPath:@"me/photos"
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error)
     {
         if (error)
         {
    

         }
         else
         {

         }
     }];
}

- (void)showMessage:(NSString *)text withTitle:(NSString *)title
{
    [[[UIAlertView alloc] initWithTitle:title
                                message:text
                               delegate:self
                      cancelButtonTitle:@"OK!"
                      otherButtonTitles:nil] show];
}
-(IBAction)post
{
    NSLog(@"Ready to past ok done");
    NSLog(@"Action perfomed %@",buttonTitle);
    if([storyView.text length]>0 && buttonTitle == NULL)
    {
        [self uploadStory];
        
    }

    if([storyView.text length]>0 && [buttonTitle isEqualToString:@"camera"])
    {
        NSLog(@"Upload Action - Camera");
        [self finalizeImageForUpload];
        [self upload];
        
    }
    else if([buttonTitle isEqualToString:@"camera"])
    {
        NSLog(@"Upload Action - Camera");
        [self finalizeImageForUpload];
        [self upload];
    }
    if([storyView.text length]>0  && [buttonTitle isEqualToString:@"gallery"]) {
        NSLog(@"Upload Action - Gallery");
        [self finalizeImageForUpload];
        [self upload];
    }
     else if([buttonTitle isEqualToString:@"gallery"])
    {
        NSLog(@"Upload Action - Gallery");
        [self finalizeImageForUpload];
        [self upload];
        
    }
    if([buttonTitle isEqualToString:@"story"])
    {
        [self uploadStory];
        
    }
    if([buttonTitle isEqualToString:@"video"])
    {
        NSLog(@"video url is: - %@",[videourlarray objectAtIndex:0]);
        [self upload];
    }
}
- (IBAction)bordersButton:(UIButton *)sender {
    blurView.dynamic = YES;
    NSArray* buttons = [NSArray arrayWithObjects: bordersbutton,fontsButton, filtersbutton, nil];
    for (UIButton* button in buttons) {
        if (button != sender) {
            [button setSelected: NO];
            [UIView animateWithDuration:0.1 animations:^{
                [transView setAlpha:0.0];
                [transView setHidden:YES];
                [self.filtersbutton setSelected:NO];
                [self.fontsButton setSelected:NO];
                [self.bordersbutton setSelected:NO];
                [self.bordersScrollview setHidden:YES];
                [self.filtersScrollview setHidden:YES];
                [self.fontsScrollview setHidden:YES];
                 [self.colorsScrollview setHidden:YES];
                [self.filtersbutton setBackgroundColor:[UIColor clearColor]];
                [self.bordersbutton setBackgroundColor:[UIColor clearColor]];
                [self.fontsButton setBackgroundColor:[UIColor clearColor]];
                self.bordersbutton.backgroundColor = [UIColor clearColor];
                self.filtersbutton.backgroundColor = [UIColor clearColor];
                self.fontsButton.backgroundColor = [UIColor clearColor];
                dropArrow1.alpha = 0;
                dropArrow2.alpha = 0;
                dropArrow3.alpha = 0;
                dropArrow4.alpha = 0;
              
            }];
            
        
            
            
        }
}
    int tag = sender.tag;
    if (sender.selected == TRUE) {
        [sender setSelected:FALSE];
        [UIView animateWithDuration:0.4 animations:^{
            [transView setAlpha:0.0];
            [transView setHidden:YES];
            [self.bordersbutton setBackgroundColor:[UIColor clearColor]];
             self.bordersbutton.backgroundColor = [UIColor clearColor];
            dropArrow4.alpha = 0;
            
        }];
    }
    else if (sender.selected == NO) {
        [sender setSelected:YES];
        CGRect backFrame;
        backFrame.origin = transView.frame.origin;
        backFrame.size = CGSizeMake(310, 80);
        transView.frame = backFrame;
        [self.filtersbutton setSelected:NO];
        [self.filtersScrollview setHidden:YES];
        [self.fontsButton setSelected:NO];
        [self.fontsScrollview setHidden:YES];
        [self.bordersScrollview setHidden:NO];
         [self.colorsScrollview setHidden:YES];
        self.filtersbutton.backgroundColor = [UIColor clearColor];
         self.fontsButton.backgroundColor = [UIColor clearColor];
        dropArrow1.alpha = 0;
        dropArrow2.alpha = 0;
        dropArrow3.alpha = 0;
        [UIView animateWithDuration:0.4 animations:^{
            [transView setAlpha:1.0];
            [transView setHidden:NO];
            [self.bordersbutton setBackgroundColor:[UIColor colorWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0]];
            dropArrow4.alpha = 1;
            
        }];


    }
}
- (IBAction)filtersbutton: (UIButton *)sender
{
    blurView.dynamic = YES;
    
    NSArray* buttons = [NSArray arrayWithObjects:filtersbutton ,fontsButton, bordersbutton,  nil];
    for (UIButton* button in buttons) {
        if (button != sender) {
            [button setSelected: NO];
            
            [UIView animateWithDuration:0.1 animations:^{
                [transView setAlpha:0.0];
                [transView setHidden:YES];
                [self.filtersbutton setSelected:NO];
                [self.fontsButton setSelected:NO];
                [self.bordersbutton setSelected:NO];
                [self.bordersScrollview setHidden:YES];
                [self.filtersScrollview setHidden:YES];
                [self.fontsScrollview setHidden:YES];
                [self.colorsScrollview setHidden:YES];
                [self.filtersbutton setBackgroundColor:[UIColor clearColor]];
                [self.bordersbutton setBackgroundColor:[UIColor clearColor]];
                [self.fontsButton setBackgroundColor:[UIColor clearColor]];
                self.bordersbutton.backgroundColor = [UIColor clearColor];
                self.filtersbutton.backgroundColor = [UIColor clearColor];
                self.fontsButton.backgroundColor = [UIColor clearColor];
                dropArrow1.alpha = 0;
                dropArrow2.alpha = 0;
                dropArrow3.alpha = 0;
                dropArrow4.alpha = 0;
               
                
                
            }];
            
            
            
        }
    }
    int tag = sender.tag;
    if (sender.selected == YES) {
        [sender setSelected:NO];
        
        [UIView animateWithDuration:0.4 animations:^{
            [transView setAlpha:0.0];
            [transView setHidden:YES];
            [self.filtersbutton setBackgroundColor:[UIColor clearColor]];
            dropArrow1.alpha = 0;
          
            
            
        }];
    }
    
    else if (sender.selected == NO) {
        [sender setSelected:YES];
        CGRect backFrame;
        backFrame.origin = transView.frame.origin;
        backFrame.size = CGSizeMake(310, 80);
        transView.frame = backFrame;
        [self.filtersScrollview setHidden:NO];
        [self.colorsScrollview setHidden:YES];
        [self.bordersScrollview setHidden:YES];
        [self.bordersbutton setSelected:NO];
         self.bordersbutton.backgroundColor = [UIColor clearColor];
        [self.fontsButton setSelected:NO];
        [self.fontsScrollview setHidden:YES];
        self.fontsButton.backgroundColor = [UIColor clearColor];
        dropArrow2.alpha = 0;
        dropArrow3.alpha = 0;
        dropArrow4.alpha = 0;
        [UIView animateWithDuration:0.4 animations:^{
            [transView setAlpha:1.0];
            [transView setHidden:NO];
            [self.filtersbutton setBackgroundColor:[UIColor colorWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0]];
            dropArrow1.alpha = 1;
            
            
        }];
        
        
    }
}
- (IBAction)fontsbutton: (UIButton *)sender
{
    blurView.dynamic = YES;
    
    NSArray* buttons = [NSArray arrayWithObjects:fontsButton,filtersbutton , bordersbutton,  nil];
    for (UIButton* button in buttons) {
        if (button != sender) {
            [button setSelected: NO];
            
            [UIView animateWithDuration:0.1 animations:^{
                [transView setAlpha:0.0];
                [transView setHidden:YES];
                [self.filtersbutton setSelected:NO];
                [self.fontsButton setSelected:NO];
                [self.bordersbutton setSelected:NO];
                [self.bordersScrollview setHidden:YES];
                [self.filtersScrollview setHidden:YES];
                [self.colorsScrollview setHidden:YES];
                [self.fontsScrollview setHidden:YES];
                [self.filtersbutton setBackgroundColor:[UIColor clearColor]];
                [self.bordersbutton setBackgroundColor:[UIColor clearColor]];
                [self.fontsButton setBackgroundColor:[UIColor clearColor]];
                self.bordersbutton.backgroundColor = [UIColor clearColor];
                self.filtersbutton.backgroundColor = [UIColor clearColor];
                self.fontsButton.backgroundColor = [UIColor clearColor];
                dropArrow1.alpha = 0;
                dropArrow2.alpha = 0;
                dropArrow3.alpha = 0;
                dropArrow4.alpha = 0;
                
                
                
            }];
            
            
            
        }
    }
    int tag = sender.tag;
    if (sender.selected == YES) {
        [sender setSelected:NO];
        
        [UIView animateWithDuration:0.4 animations:^{
            [transView setAlpha:0.0];
            [transView setHidden:YES];
            [self.fontsButton setBackgroundColor:[UIColor clearColor]];
            dropArrow3.alpha = 0;
            
            
            
        }];
    }
    
    else if (sender.selected == NO) {
        [sender setSelected:YES];
        
        CGRect backFrame;
        backFrame.origin = transView.frame.origin;
        backFrame.size = CGSizeMake(310, 118);
        transView.frame = backFrame;
       
        [self.filtersScrollview setHidden:YES];
        [self.fontsScrollview setHidden:NO];
        [self.colorsScrollview setHidden:NO];
        [self.bordersScrollview setHidden:YES];
        [self.bordersbutton setSelected:NO];
        [self.filtersbutton setSelected:NO];
        self.bordersbutton.backgroundColor = [UIColor clearColor];
        self.filtersbutton.backgroundColor = [UIColor clearColor];
        dropArrow1.alpha = 0;
        dropArrow2.alpha = 0;
        dropArrow4.alpha = 0;
        [UIView animateWithDuration:0.4 animations:^{
            [transView setAlpha:1.0];
            [transView setHidden:NO];
            [self.fontsButton setBackgroundColor:[UIColor colorWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0]];
            dropArrow3.alpha = 1;
            
            
        }];
        
        
    }
}



-(void)removeAfterSuccess {
    self.borderView.image = nil;
    borderImage = nil;
    imageView2.image = nil;
    self.imageView = nil;
    staticPicture = nil;
    selectedFilter = 0;
    selectedBorder = 0;
    [UIView animateWithDuration:0.1 animations:^{
        [transView setAlpha:0.0];
        [transView setHidden:YES];
        [self.filtersbutton setSelected:NO];
        [self.bordersbutton setSelected:NO];
        [self.fontsButton setSelected:NO];
        [self.bordersScrollview setHidden:YES];
        [self.filtersScrollview setHidden:YES];
        [self.fontsScrollview setHidden:YES];
        [self.colorsScrollview setHidden:YES];
        [self.filtersbutton setBackgroundColor:[UIColor clearColor]];
        [self.bordersbutton setBackgroundColor:[UIColor clearColor]];
        self.bordersbutton.backgroundColor = [UIColor clearColor];
        self.filtersbutton.backgroundColor = [UIColor clearColor];
        self.fontsButton.backgroundColor = [UIColor clearColor];
        dropArrow1.alpha = 0;
        dropArrow2.alpha = 0;
        dropArrow3.alpha = 0;
        dropArrow4.alpha = 0;
        
    }];

}
-(IBAction)goSettings
{
    self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:3];
    
}
-(IBAction)hidekeyboard:(id)sender
{
    [storyView resignFirstResponder];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
/*-(IBAction)postOnFb

{
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
        
        if (fbButton.selected == YES) {
            //[fbButton setBackgroundImage:[UIImage imageNamed:@"fb.png"] forState:UIControlStateNormal];
            //flagFb=0;
            [fbButton setSelected:NO];
            NSLog(@"flab for fb button is %d",flagFb);
        }
        else if (fbButton.selected == NO)
        {
            [fbButton setSelected:YES];
      
    
    } else {
        
        [self showMessage:@"You are Currently not Logged into Facebook" withTitle:@"Facebook"];
        [fbButton setSelected:NO];
        
       
    }

    
   
        AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
        
        NSUserDefaults *outhToken=[NSUserDefaults standardUserDefaults];
        NSString *keyOfOuth=[ delegate.mynameLogin stringByAppendingString:@"outhToken"];
        NSString *getToken=[outhToken objectForKey:keyOfOuth];
        delegate.token=getToken;
        NSLog(@"delegate.token %@",fbGraph.accessToken);
        //if([getToken isEqualToString:@"no"])
        if ((fbGraph.accessToken.length == 0))
        {
            
           [self authenticateUserWithCallbackObject:self andSelector:@selector(fbGraphCallback:)                      andExtendedPermissions:@"user_photos,user_videos,publish_stream,offline_access,user_checkins,friends_checkins,email,user_birthday"];
            
        }
        
        flagFb=1;
        NSLog(@"flab for fb button is %d",flagFb);
        
        
        if (FBSession.activeSession.state == FBSessionStateOpen
            || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
            
            // Close the session and remove the access token from the cache
            // The session state handler (in the app delegate) will be called automatically
            [FBSession.activeSession closeAndClearTokenInformation];
            
            // If the session state is not any of the two "open" states when the button is clicked
        } else {
            // Open a session showing the user the login UI
            // You must ALWAYS ask for basic_info permissions when opening a session
            [FBSession openActiveSessionWithReadPermissions:@[@"basic_info"]
                                               allowLoginUI:YES
                                          completionHandler:
             ^(FBSession *session, FBSessionState state, NSError *error) {
                 
                 // Retrieve the app delegate
                 AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
                 // Call the app delegate's sessionStateChanged:state:error method to handle session state changes
                 [appDelegate sessionStateChanged:session state:state error:error];
             }];
        }
        [fbButton setBackgroundImage:[UIImage imageNamed:@"fbDark.png"] forState:UIControlStateNormal];
    }
    
}*/
- (BOOL)userHasAccessToTwitter
{
    return [SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter];
}







-(IBAction)postOnTw
{
    if (flagTw==1) {
        [twButton setBackgroundImage:[UIImage imageNamed:@"tweet.png"] forState:UIControlStateNormal];
        flagTw=0;
    }
    else
    {
        if ([self userHasAccessToTwitter]) {
            flagTw=1;
            [twButton setBackgroundImage:[UIImage imageNamed:@"tweetDark.png"] forState:UIControlStateNormal];
            ACAccountStore *accountStore = [[ACAccountStore alloc] init];
            ACAccountType *twitterAccountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
            [accountStore requestAccessToAccountsWithType:twitterAccountType options:nil completion:^(BOOL granted, NSError *error) {
                if(granted) {
                    NSArray *accountsArray = [accountStore accountsWithAccountType:twitterAccountType];
                    
                    if ([accountsArray count] > 0) {
                        ACAccount *twitterAccount = [accountsArray objectAtIndex:0];
                        NSUserDefaults *tweetUser=[NSUserDefaults standardUserDefaults];
                        [tweetUser setObject:twitterAccount.username forKey:@"tweetUser"];
                        NSLog(@"%@",twitterAccount.username);
                        NSLog(@"%@",twitterAccount.accountType);
                    }
                }
            }];
        }
        else
        {
            UIAlertView *alertTweet=[[UIAlertView alloc]initWithTitle:@"Go to Settings" message:@"No Twitter Account in iPhone settings" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertTweet show];
        }
    }
}

-(void)addCurentUpdate:(NSString *)updateString :(NSString *)dataType
{
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    [delegate.PdataUpdate insertObject:updateString atIndex:0];
    [delegate.PdataType insertObject:dataType atIndex:0];
    [delegate.PsnoArray insertObject:uploadedSno atIndex:0];
    [delegate.Plikearray insertObject:@"0" atIndex:0];
    [delegate.Pdislikearray insertObject:@"0" atIndex:0];
    [delegate.PcommentCounts insertObject:@"" atIndex:0];
    [delegate.PlikesNameArray insertObject:@"" atIndex:0];
    [delegate.PdislikesNameArray insertObject:@"" atIndex:0];
    [delegate.PtimeArray insertObject:uploadedElapsed atIndex:0];
    [delegate.PlocationArray insertObject:uploadedLocation atIndex:0];
    [delegate.PfourCommentArray insertObject:@"No Comments" atIndex:0];
    delegate.PgetInertedTime=uploadedInserted;
    
    
    [delegate.dataUpdate insertObject:updateString atIndex:0];
    [delegate.dataType insertObject:dataType atIndex:0];
    [delegate.snoArray insertObject:uploadedSno atIndex:0];
    [delegate.likearray insertObject:@"0" atIndex:0];
    [delegate.dislikearray insertObject:@"0" atIndex:0];
    [delegate.differUser insertObject:delegate.mynameLogin atIndex:0];
    [delegate.likesNameArray insertObject:@"" atIndex:0];
    [delegate.dislikesNameArray insertObject:@"" atIndex:0];
    [delegate.profielImages insertObject:delegate.profileImage atIndex:0];
    [delegate.commentCounts insertObject:@"" atIndex:0];
    [delegate.locationArray insertObject:uploadedLocation atIndex:0];
    delegate.getInertedTime=uploadedInserted;
    [delegate.timeArray insertObject:uploadedElapsed atIndex:0];
    [delegate.fourCommentArray insertObject:@"No Comments"atIndex:0];
}
-(void)postDataOnFb
{
    NSMutableDictionary *variables = [NSMutableDictionary dictionaryWithCapacity:2];
	
	//create a UIImage (you could use the picture album or camera too)
    NSData  *imageData = UIImageJPEGRepresentation(imageView2.image, 90);
	UIImage *picture = [UIImage imageWithData:imageData];
	
	/*//create a FbGraphFile object insance and set the picture we wish to publish on it
	FbGraphFile *graph_file = [[FbGraphFile alloc] initWithImage:picture];
	
	//finally, set the FbGraphFileobject onto our variables dictionary....
	[variables setObject:graph_file forKey:@"file"];
	
	[variables setObject:@"Via Cancerosity iOS" forKey:@"message"];
	
	//the fbGraph object is smart enough to recognize the binary image data inside the FbGraphFile
	//object and treat that is such.....
	FbGraphResponse *fb_graph_response = [self doGraphPost:@"me/feed" withPostVars:variables];
	NSLog(@"postPictureButtonPressed:  %@", fb_graph_response.htmlResponse);*/

    
    
   /* NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"Via Cancerosity iOS", @"message",
                                    picture, @"source",
                                    nil];
    
    [FBRequestConnection startWithGraphPath:@"me/photos"
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              
                          }];*/
    
    [FBRequestConnection startForUploadPhoto:picture
                           completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                               if (!error) {
                                   NSLog(@"!!!!!!!!!!@@@@@@@@@@@@facebook not uploaded");
                               } else {
                                  NSLog(@"!!!!!!!!!!@@@@@@@@@@@@facebook uploaded");
                               }
                  
                           }];
    

    
}
-(void)postStoryOnFb
{
    
    [FBRequestConnection startForPostStatusUpdate:storyView.text
                                completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
         if (error)
         {
             NSLog(@"Story couldnt be posted right now");
             
         }
         else
         {
             NSLog(@"Story Posted to Facebook Succesfully");
             
         }
     }
         ];
    
    
   /* NSMutableDictionary *variables = [NSMutableDictionary dictionaryWithCapacity:1];
    
	[variables setObject:uploadedData forKey:@"message"];
	
	//the fbGraph object is smart enough to recognize the binary image data inside the FbGraphFile
	//object and treat that is such.....
	FbGraphResponse *fb_graph_response = [self doGraphPost:@"me/feed" withPostVars:variables];
	NSLog(@"postPictureButtonPressed:  %@", fb_graph_response.htmlResponse);*/
}

- (FbGraphResponse *)doGraphPost:(NSString *)action withPostVars:(NSDictionary *)post_vars {
	AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
    NSLog(@"fb token %@",delegate.token);
	FbGraphResponse *return_value = [[[FbGraphResponse alloc] init] autorelease];
	
	NSString *urlString = [NSString stringWithFormat:@"https://graph.facebook.com/%@", action];
	NSLog(@"URL FB %@",urlString);
	NSURL *url = [NSURL URLWithString:urlString];
	NSString *boundary = @"----1010101010";
	NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	[request setHTTPMethod:@"POST"];
	[request addValue:contentType forHTTPHeaderField: @"Content-Type"];
	
	NSMutableData *body = [NSMutableData data];
	[body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
	
	NSEnumerator *enumerator = [post_vars keyEnumerator];
	NSString *key;
	NSString *value;
	NSString *content_disposition;
	
	//loop through all our parameters
	while ((key = (NSString *)[enumerator nextObject])) {
		
		//if it's a picture (file)...we have to append the binary data
		if ([key isEqualToString:@"file"]) {
			
			/*
			 * the FbGraphFile object is smart enough to append it's data to
			 * the request automagically, regardless of the type of file being
			 * attached
			 */
			FbGraphFile *upload_file = (FbGraphFile *)[post_vars objectForKey:key];
			[upload_file appendDataToBody:body];
			
			//key/value nsstring/nsstring
		} else {
			value = (NSString *)[post_vars objectForKey:key];
			
			content_disposition = [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key];
			[body appendData:[content_disposition dataUsingEncoding:NSUTF8StringEncoding]];
			[body appendData:[value dataUsingEncoding:NSUTF8StringEncoding]];
			
		}//end else
		
		[body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
		
	}//end while
	
	//add our access token
	[body appendData:[@"Content-Disposition: form-data; name=\"access_token\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
	[body appendData:[delegate.token dataUsingEncoding:NSUTF8StringEncoding]];
	[body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
	
	//button up the request body
	[body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
	
	[request setHTTPBody:body];
	[request addValue:[NSString stringWithFormat:@"%d", body.length] forHTTPHeaderField: @"Content-Length"];
	
	//quite a few lines of code to simply do the business of the HTTP connection....
    NSURLResponse *response;
    NSData *data_reply;
	NSError *err;
	
    data_reply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
	
	NSString *stringResponse = [[NSString alloc] initWithData:data_reply encoding:NSUTF8StringEncoding];
    NSLog(@"stringResponse %@",stringResponse);
    return_value.htmlResponse = stringResponse;
	[stringResponse release];
	
	/*if (err != nil) {
     return_value.error = err;
     }
     
	 * return the json array.  we could parse it, but that would incur overhead
	 * some users might not want (not to mention dependencies), besides someone
	 * may want raw strings back, keep it simple.
	 *
	 * See:  http://code.google.com/p/json-framework for an easy json parser
	 */
	
	return return_value;
}
-(void)postStatusToTwitter
{
    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:
                                  ACAccountTypeIdentifierTwitter];
    
    [account requestAccessToAccountsWithType:accountType options:nil
                                  completion:^(BOOL granted, NSError *error)
     {
         if (granted == YES)
         {
             NSArray *arrayOfAccounts = [account accountsWithAccountType:accountType];
             
             if ([arrayOfAccounts count] > 0)
             {
                 
                 NSDictionary *message = @{@"status": uploadedData};
                 
                 NSURL *requestURL = [NSURL
                                      URLWithString:@"http://api.twitter.com/1/statuses/update.json"];
                 SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter                                   requestMethod:SLRequestMethodPOST URL:requestURL parameters:message];
                 [request setAccount:[arrayOfAccounts lastObject]];
                 [request performRequestWithHandler:^(NSData *responseData,
                                                      NSHTTPURLResponse *urlResponse, NSError *error)
                  {
                      NSLog(@"Twitter HTTP response: %i", [urlResponse
                                                           statusCode]);
                  }];
             }
         }
     }];
    
}

- (void)postImageToTwitter
{
    UIImage *image=imageView2.image;
    NSString *status=@"Via Cancer Circle iOS";
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *twitterType =
    [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    SLRequestHandler requestHandler =
    ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (responseData) {
            NSInteger statusCode = urlResponse.statusCode;
            if (statusCode >= 200 && statusCode < 300) {
                NSDictionary *postResponseData =
                [NSJSONSerialization JSONObjectWithData:responseData
                                                options:NSJSONReadingMutableContainers
                                                  error:NULL];
                NSLog(@"[SUCCESS!] Created Tweet with ID: %@", postResponseData[@"id_str"]);
            }
            else {
                NSLog(@"[ERROR] Server responded: status code %d %@", statusCode,
                      [NSHTTPURLResponse localizedStringForStatusCode:statusCode]);
            }
        }
        else {
            NSLog(@"[ERROR] An error occurred while posting: %@", [error localizedDescription]);
        }
    };
    
    ACAccountStoreRequestAccessCompletionHandler accountStoreHandler =
    ^(BOOL granted, NSError *error) {
        if (granted) {
            NSArray *accounts = [accountStore accountsWithAccountType:twitterType];
            NSURL *url = [NSURL URLWithString:@"https://api.twitter.com"
                          @"/1.1/statuses/update_with_media.json"];
            NSDictionary *params = @{@"status" : status};
            SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                    requestMethod:SLRequestMethodPOST
                                                              URL:url
                                                       parameters:params];
            NSData *imageData = UIImageJPEGRepresentation(image, 1.f);
            [request addMultipartData:imageData
                             withName:@"media[]"
                                 type:@"image/jpeg"
                             filename:@"image.jpg"];
            [request setAccount:[accounts lastObject]];
            [request performRequestWithHandler:requestHandler];
        }
        else {
            NSLog(@"[ERROR] An error occurred while asking for user authorization: %@",
                  [error localizedDescription]);
        }
    };
    
    [accountStore requestAccessToAccountsWithType:twitterType
                                          options:NULL
                                       completion:accountStoreHandler];
}

//////////// FB Token


- (void)fbGraphCallback:(id)sender {
    NSLog(@"Send to next screen");
	
	if ( (fbGraph.accessToken == nil) || ([fbGraph.accessToken length] == 0) ) {
		
		NSLog(@"You pressed the 'cancel' or 'Dont Allow' button, you are NOT logged into Facebook...I require you to be logged in & approve access before you can do anything useful....");
		
		//restart the authentication process.....
		[self authenticateUserWithCallbackObject:self andSelector:@selector(fbGraphCallback:)
                          andExtendedPermissions:@"user_photos,user_videos,publish_stream,offline_access,user_checkins,friends_checkins"];
		
	} else {
        
        NSLog(@"--->CONGRATULATIONS<----, You're logged into Facebook...  Your oAuth token is:  %@",accessToken);
        
        
        FbGraphResponse *fb_graph_response = [self doGraphGet:@"me" withGetVars:nil];
        NSLog(@"getMeButtonPressed:  %@", fb_graph_response.htmlResponse);
        NSData* data=[fb_graph_response.htmlResponse dataUsingEncoding: [NSString defaultCStringEncoding] ];
        NSError *error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data //1
                                                             options:kNilOptions
                                                               error:&error];
        NSLog(@"########### %@",json);
        NSLog(@"Verified ===== %@",[json valueForKey:@"verified"]);
        NSLog(@"Birthday ===== %@",[json valueForKey:@"birthday"]);
        NSLog(@"First Name === %@",[json valueForKey:@"first_name"]);
        NSLog(@"Last Name ==== %@",[json valueForKey:@"last_name"]);
        NSLog(@"Email ======== %@",[json valueForKey:@"email"]);
        NSLog(@"Name ========= %@",[json valueForKey:@"name"]);
        NSLog(@"Username ===== %@",[json valueForKey:@"username"]);
        NSLog(@"Gender ======= %@",[json valueForKey:@"gender"]);
        
        
        // NSString *keyOfName=[delegate.mynameLogin stringByAppendingString:@"outhName"];
        NSUserDefaults *outhName=[NSUserDefaults standardUserDefaults];
        [outhName setObject:[json valueForKey:@"username"] forKey:@"outhName"];
        [self dismissViewControllerAnimated:YES completion:nil];
       	
    }
}
- (void)authenticateUserWithCallbackObject:(id)anObject andSelector:(SEL)selector andExtendedPermissions:(NSString *)extended_permissions andSuperView:(UIView *)super_view {
	
	self.callbackObject = anObject;
	self.callbackSelector = selector;
    
	facebookClientID = @"366546850134442";
    redirectUri = @"https://www.facebook.com/connect/login_success.html";
	NSString *url_string = [NSString stringWithFormat:@"https://graph.facebook.com/oauth/authorize?client_id=%@&redirect_uri=%@&scope=%@&type=user_agent&display=touch", facebookClientID, redirectUri, extended_permissions];
    NSLog(@"url_string FACEBOOk %@",url_string);
	NSURL *url = [NSURL URLWithString:url_string];
	NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webView loadRequest:request];
    
}

-(void)authenticateUserWithCallbackObject:(id)anObject andSelector:(SEL)selector andExtendedPermissions:(NSString *)extended_permissions {
	
	UIWindow* window = [UIApplication sharedApplication].keyWindow;
	if (!window) {
		window = [[UIApplication sharedApplication].windows objectAtIndex:0];
	}
	
	[self authenticateUserWithCallbackObject:anObject andSelector:selector andExtendedPermissions:extended_permissions andSuperView:window];
}

- (FbGraphResponse *)doGraphGet:(NSString *)action withGetVars:(NSDictionary *)get_vars {
	
	NSString *url_string = [NSString stringWithFormat:@"https://graph.facebook.com/%@?", action];
	
	//tack on any get vars we have...
	if ( (get_vars != nil) && ([get_vars count] > 0) ) {
		
		NSEnumerator *enumerator = [get_vars keyEnumerator];
		NSString *key;
		NSString *value;
		while ((key = (NSString *)[enumerator nextObject])) {
			
			value = (NSString *)[get_vars objectForKey:key];
			url_string = [NSString stringWithFormat:@"%@%@=%@&", url_string, key, value];
			
		}//end while
	}//end if
	
	if (accessToken != nil) {
		//now that any variables have been appended, let's attach the access token....
		url_string = [NSString stringWithFormat:@"%@access_token=%@", url_string, self.accessToken];
	}
	
	//encode the string
	url_string = [url_string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	NSLog(@"URL STRING ME %@",url_string);
	return [self doGraphGetWithUrlString:url_string];
}
- (FbGraphResponse *)doGraphGetWithUrlString:(NSString *)url_string {
	
	FbGraphResponse *return_value = [[[FbGraphResponse alloc] init] autorelease];
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url_string]];
	
	NSError *err;
	NSURLResponse *resp;
	NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:&resp error:&err];
	
	if (resp != nil) {
		
		/**
		 * In the case we request a picture (avatar) the Graph API will return to us the actual image
		 * bits versus a url to the image.....
		 **/
		if ([resp.MIMEType isEqualToString:@"image/jpeg"]) {
			
			UIImage *image = [UIImage imageWithData:response];
			return_value.imageResponse = image;
			
		} else {
		    
			NSString *stringResponse = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
			return_value.htmlResponse = stringResponse;
			[stringResponse release];
		}
		
	} else if (err != nil) {
		return_value.error = err;
	}
	
	return return_value;
	
}
- (void)webViewDidFinishLoad:(UIWebView *)_webView {
	
	/**
	 * Since there's some server side redirecting involved, this method/function will be called several times
	 * we're only interested when we see a url like:  http://www.facebook.com/connect/login_success.html#access_token=..........
	 */
	
	//get the url string
	NSString *url_string = [((_webView.request).URL) absoluteString];
	
	//looking for "access_token="
	NSRange access_token_range = [url_string rangeOfString:@"access_token="];
	
	//looking for "error_reason=user_denied"
	NSRange cancel_range = [url_string rangeOfString:@"error_reason=user_denied"];
	
	//it exists?  coolio, we have a token, now let's parse it out....
	if (access_token_range.length > 0) {
		
		//we want everything after the 'access_token=' thus the position where it starts + it's length
		int from_index = access_token_range.location + access_token_range.length;
		NSString *access_token = [url_string substringFromIndex:from_index];
		
		//finally we have to url decode the access token
		access_token = [access_token stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		//remove everything '&' (inclusive) onward...
		NSRange period_range = [access_token rangeOfString:@"&"];
		
		//move beyond the .
		access_token = [access_token substringToIndex:period_range.location];
		
		//store our request token....
		self.accessToken = access_token;
        AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
        delegate.token=access_token;
        NSUserDefaults *outhToken=[NSUserDefaults standardUserDefaults];
        //NSString *keyOfOuth=[ delegate.mynameLogin stringByAppendingString:@"outhToken"];
        [outhToken setObject:access_token forKey:@"outhToken"];
		
		//remove our window
		UIWindow* window = [UIApplication sharedApplication].keyWindow;
		if (!window) {
			window = [[UIApplication sharedApplication].windows objectAtIndex:0];
		}
		
		//[self.webView removeFromSuperview];
		
		//tell our callback function that we're done logging in :)
		if ( (callbackObject != nil) && (callbackSelector != nil) ) {
			[callbackObject performSelector:callbackSelector];
		}
		
		//the user pressed cancel
	} else if (cancel_range.length > 0) {
		//remove our window
		UIWindow* window = [UIApplication sharedApplication].keyWindow;
		if (!window) {
			window = [[UIApplication sharedApplication].windows objectAtIndex:0];
		}
		
		//[self.webView removeFromSuperview];
		
		//tell our callback function that we're done logging in :)
		if ( (callbackObject != nil) && (callbackSelector != nil) ) {
			[callbackObject performSelector:callbackSelector];
		}
		
	}
}
-(void)viewDidDisappear:(BOOL)animated
{
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [bgImage release];
   
    
    [_buttonScroll release];
 
    [filterbutton release];
    [filtersbutton release];
    [_imageView release];
    [_filtersScrollview release];
    [_imageView release];
    [imageView2 release];
    [_imageView release];
    [_imageView release];
    [_captureImage release];
    [_captureVIdeo release];
   
    [gallery release];
    [_galleryImage release];
    [_galleryImage release];
    [_textButton release];
    [_fontsButton release];
    [_bordersButton release];
    [_bordersScrollview release];
    [_bordersbutton release];
    [bordersbutton release];
    [_bordersbutton release];
    [_bordersScrollview release];
    [borderView release];
    [_borderView release];
    [buttonsShadowV release];
    [addView release];
    [imageText release];
    [saveView release];
    [_fontsScrollview release];
    [fontsButton release];
    [transView release];
    [_colorsScrollview release];
    [_fbButton release];
    [_textBack release];
    [_enlargeText release];
    [_buttonView release];
    [super dealloc];
}
@end
