//
//  ForgotPassword.m
//  CancerCircleFirst
//
//  Created by Raminder on 24/01/13.
//
//

#import "ForgotPassword.h"
#import "Login.h"

@implementation ForgotPassword

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    self.view.superview.bounds = CGRectMake(0, 0, 320, 320);
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"640x960bg.png"]];
    bgImage.layer.cornerRadius=5;
    bgImage.clipsToBounds = YES;
  
    CGFloat titleWidth2=100;
    CGFloat titleWidth = MIN(titleWidth2, 300);
    _contactfield.layer.cornerRadius = 3;
    _contactfield.layer.masksToBounds = YES;
    _emailfield.layer.cornerRadius = 3;
    _emailfield.layer.masksToBounds = YES;
    _submitb.layer.cornerRadius = 5;
    _submitb.layer.masksToBounds = YES;
    [_emailfield.layer setBorderWidth:1.0f];
    [_emailfield.layer setBorderColor:[[[UIColor alloc] initWithRed:(233.0/255.0) green:(233.0/255.0) blue:(233.0/255.0) alpha:1.0] CGColor]];
    [_contactfield.layer setBorderWidth:1.0f];
    [_contactfield.layer setBorderColor:[[[UIColor alloc] initWithRed:(233.0/255.0) green:(233.0/255.0) blue:(233.0/255.0) alpha:1.0] CGColor]];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth+3, 30)];
    label.backgroundColor = [UIColor clearColor];
    
    label.font=[UIFont fontWithName:@"Futura_Light" size:20];
 
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=@"Forgot Password";
  
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIColor *barColour = [[UIColor alloc] initWithRed:(022.0/255.0) green:(180.0/255.0) blue:(030.0/255.0) alpha:1.0];
    UIView *colourView = [[UIView alloc] initWithFrame:CGRectMake(0.f, -20.f, 320.f, 64.f)];
    colourView.opaque = NO;
    colourView.alpha = 0.6f;
    colourView.backgroundColor = barColour;
    [navBar.layer insertSublayer:colourView.layer atIndex:1];
    navBar.barTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    
    self.navigationItem.titleView = label;
    
    UIImage* image3 = [UIImage imageNamed:@"close.png"];
    CGRect frameimg = CGRectMake(0, 0,18,18);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
   
 
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(goLogin)
         forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *backbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.leftBarButtonItem=backbutton;
    [someButton release];
    
    [mailText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [contactText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _submitb.bounds;
    gradient.colors = [NSArray arrayWithObjects: (id)[[[UIColor alloc] initWithRed:(231.0/255.0) green:(248.0/255.0) blue:(227.0/255.0) alpha:1.0] CGColor], (id)[[[UIColor alloc] initWithRed:(200.0/255.0) green:(240.0/255.0) blue:(194.0/255.0) alpha:1.0] CGColor],nil];
   
    [_submitb.layer insertSublayer:gradient atIndex:0];

    /* CGRect frameRect = mailText.frame;
     frameRect.size.height = 40;
     mailText.frame = frameRect;
     CGRect frameRect1 = contactText.frame;
     frameRect1.size.height = 40;
     contactText.frame = frameRect1;*/
}

-(IBAction)sendMail
{
    [self mailResponse];
}
-(IBAction)hidekeyboard:(id)sender
{
    [mailText resignFirstResponder];
}
-(IBAction)goLogin
{
    [self dismissViewControllerAnimated:YES completion:nil];
    /*       Login *login=[self.storyboard instantiateViewControllerWithIdentifier:@"login"];
     [[self navigationController] setNavigationBarHidden:YES animated:YES];
     [self.navigationController pushViewController:login animated:YES];*/
}


-(void)mailResponse
{
    NSString *url = [NSString stringWithFormat:SERVER_URL@"/forget_password.do?contact=%@&email=%@",contactText.text,mailText.text];
    //  NSString *url = [NSString stringWithFormat:@"http://203.134.223.38/Cancer_test/forget_password.do?contact=%@&email=%@",contactText.text,mailText.text];
    NSURL *urlrequest=[NSURL URLWithString:url];
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    NSData *data = [NSData dataWithContentsOfURL:urlrequest];
    NSLog(@"values in datafordistance is:- %@",data);
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
    NSLog(@"JSON format:- %@",json);
    mailStr = [json objectForKey:@"param"];
    NSLog(@"valu in elemant is:- %@",mailStr);
    if([mailStr isEqualToString:@"True"])
    {
        alert=[[UIAlertView alloc]initWithTitle:@"Password Sent" message:@"Check ur email" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alert.tag=0;
        [alert show];
    }
    else if([mailStr isEqualToString:@"False"])
    {
        alert=[[UIAlertView alloc]initWithTitle:@"Wrong credentials" message:@"Check ur email and contact" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alert.tag=1;
        [alert show];
        
    }
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(alert.tag==0)
    {
        if (buttonIndex == 0) {
            [self performSegueWithIdentifier:@"forgotTologin" sender:self];
        }
        else if(alert.tag==1)
        {
            
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_contactfield release];
    [_emailfield release];
    [_submitb release];
    [super dealloc];
}
@end
