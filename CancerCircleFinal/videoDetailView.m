//
//  videoDetailView.m
//  CancerCircleFirst
//
//  Created by Raminder on 02/04/13.
//
//

#import "videoDetailView.h"
#import "AppDelegate.h"
#import "searchedUserView.h"
#import "Login.h"
#import "OpenUrlView.h"
#import "SearchViewController.h"
#define CELL_CONTENT_WIDTH2 340.0f
#define CELL_CONTENT_MARGIN2 40.0f
#define FONT_SIZE 11.0f
#define kNavBarDefaultPosition CGPointMake(160, 42)

@interface videoDetailView ()

@end

@implementation videoDetailView
CGFloat startContentOffset;
CGFloat lastContentOffset;
BOOL hidden;

@synthesize getImageData,getDisLikes,getIfDisLikes,getIfLikes,getLikes,getUserImageData,getSno,getName,getComments,getType,videoUrl,getLocation,getTime,getDislikeConferm,getLikeConferm,getRowIndex,getTabId;
@synthesize movieplayer = _movieplayer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.navigationController.navigationBarHidden = NO;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleTweetNotification:) name:IFTweetLabelURLNotification object:nil];
     [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}
- (void)handleTweetNotification:(NSNotification *)notification
{
    NSDictionary *dict = (NSDictionary*)notification.object;
    NSString *strDict = [NSString stringWithFormat:@"%@", dict];
    if([strDict hasPrefix:@"http://"])
    {
        OpenUrlView *openUrl=[self.storyboard instantiateViewControllerWithIdentifier:@"OpenUrl"];
        // openUrl.hidesBottomBarWhenPushed = YES;
        openUrl.getUrl=strDict;
        [self.navigationController pushViewController:openUrl animated:YES];
        
    }
    else
    {
        AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
        delegate.wordToSearch=strDict;
        if (([strDict hasPrefix:@"#"])||([strDict hasPrefix:@"@"])) {
            //[self performSegueWithIdentifier:@"circleToSearch" sender:self];
            SearchViewController *search=[self.storyboard instantiateViewControllerWithIdentifier:@"SearchView"];
            UINavigationBar *navBar = [[self navigationController] navigationBar];
            navBar.hidden=false;
            [self.navigationController pushViewController:search animated:YES];
        }
        else
        {
            [self userButtonVideoClicked:strDict];
        }
        
        
    }
}

- (void)viewDidLoad
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideOrShow:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideOrShow:) name:UIKeyboardWillShowNotification object:nil];
    CALayer *layer = self.navigationController.navigationBar.layer;
    layer.position = kNavBarDefaultPosition;
    commentText.layer.cornerRadius = 5;
    commentText.layer.masksToBounds = YES;
    
    transView.translucentAlpha = 1.0;
    transView.translucentStyle = UIBarStyleDefault;
    transView.translucentTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    

    
    
    self.imageDownloadingQueuePic = [[NSOperationQueue alloc] init];
    self.imageDownloadingQueuePic.maxConcurrentOperationCount = 4;
    self.imageCachePic = [[NSCache alloc] init];
    [super viewDidLoad];
    NSLog(@"image view detail");
    
    //myScroll.contentSize=CGSizeMake(320,600);
    //myScroll.pagingEnabled=YES;
    
    
    //************* Nvigation bar ****************************************************
    
    self.navigationController.navigationBar.barTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    self.navigationController.navigationBar.translucent = YES;
    CGFloat titleWidth2=100;
    CGFloat titleWidth = MIN(titleWidth2, 300);
    label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 30)];
    label.backgroundColor = [UIColor clearColor];
    label.font=[UIFont fontWithName:@"Futura_Light" size:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=@"Comments";
    self.navigationItem.titleView = label;
    getType=@"video";
    [self commentsData];
    
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    getusername = delegate.mynameLogin;
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"640x960bg.png"]];
    [mytable reloadData];
}
-(void)commentsData
{
    NSLog(@"image view comments data");
    commnetsArray=[[NSMutableArray alloc]initWithCapacity:commnetsArray.count];
    commentNameArray=[[NSMutableArray alloc]initWithCapacity:commentNameArray.count];
    commentPicArray=[[NSMutableArray alloc]initWithCapacity:commentPicArray.count];
    typeData=[[NSMutableArray alloc]initWithCapacity:typeData.count];
    [commentNameArray addObject:@"nothing"];
    [commnetsArray addObject:@"nothing"];
    [commentPicArray addObject:@"nothing"];
    [typeData addObject:@"video"];
    NSString *urlComment = [NSString stringWithFormat:SERVER_URL@"/showcomment.do?sno=%@&type=%@",getSno,getType];
    NSURL *urlrequest=[NSURL URLWithString:urlComment];
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    NSString *stringData= [NSString stringWithContentsOfURL:urlrequest encoding:nil error:nil];
    NSLog(@"Recieved data String &&&&&&&&&&&& %@",stringData);
    
    //NSData *dataofStrig=[NSData dataWithContentsOfFile:stringData];
    NSData * dataofStrig = [stringData dataUsingEncoding:NSUTF8StringEncoding];
    NSError* error;
    
    if(!dataofStrig )
    {
        NSLog(@"errorrrrr no response");
        UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Connection failed" message:@"Check intenet connection" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
        [alert1 show];
        
    }
    
    else{
        NSLog(@"comments data %@",dataofStrig);
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        int length=[json count];
        //getComments=[[json valueForKey:@"count"] stringValue];
        for(int i=0;i<length-2;i++)
        {
            NSString *indexStr = [NSString stringWithFormat:@"%d",i];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            NSString *commentStr=[[json objectForKey:data]valueForKey:@"comment"];
            
            if ([commentStr isEqualToString:@""]) {
                // getComments=@"0";
            }
            else{
                NSArray *info=[commentStr componentsSeparatedByString:@"-SEPARATOR-"];
                [commentNameArray addObject:[info objectAtIndex:0]];
                [commnetsArray addObject:[info objectAtIndex:1]];
                [commentPicArray addObject:[info objectAtIndex:2]];
                [typeData addObject:@"comment"];
                [mytable reloadData];
            }
        }
    }
    
}
-(IBAction)hidekeyboard:(id)sender
{
    [commentText resignFirstResponder];
    CGPoint point = mytable.contentOffset;
    point .y = 0;
    mytable.contentOffset = point;
    [self.view endEditing:YES];

    
}



-(void)expand
{
    if(hidden)
        return;
    
    hidden = YES;
    [transView setHidden:NO];
    
    
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

-(void)contract
{
    if(!hidden)
        return;
    
    hidden = NO;
    
    
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    startContentOffset = lastContentOffset = scrollView.contentOffset.y;
    //NSLog(@"scrollViewWillBeginDragging: %f", scrollView.contentOffset.y);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat differenceFromStart = startContentOffset - currentOffset;
    CGFloat differenceFromLast = lastContentOffset - currentOffset;
    lastContentOffset = currentOffset;
    
    
    
    if((differenceFromStart) < 0)
    {
        // scroll up
        if(scrollView.isTracking && (abs(differenceFromLast)>1))
            [self expand];
    }
    else {
        if(scrollView.isTracking && (abs(differenceFromLast)>1))
            [self contract];
    }
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    [self contract];
    return YES;
}
- (void)keyboardWillHideOrShow:(NSNotification *)note {
    NSDictionary *userInfo = [note userInfo];
    
    NSTimeInterval duration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
    
    CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    CGRect keyboardFrameForTextField = [commentView.superview convertRect:keyboardFrame fromView:nil];
    CGRect newFrame = commentView.frame;
    
    newFrame.origin.y = keyboardFrameForTextField.origin.y - newFrame.size.height;
    
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
        commentView.frame = newFrame;
        CGPoint point = mytable.contentOffset;
        point .y = newFrame.origin.y;
        mytable.contentOffset = point;
        
        
    } completion:nil];
}


-(IBAction)commentButton
{
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    if([commentText.text isEqualToString:@""])
    {
        
    }
    else{
        NSString *converted = [commentText.text stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/comment.do?username=%@&sno=%@&type=video&comment=%@",getusername,getSno,converted];
        NSLog(@"comment Url %@",urlLike);
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSString *newCounts=[json objectForKey:@"count"];
        getComments=newCounts;
        int index=[getRowIndex intValue];
        if ([getTabId isEqualToString:@"circle"]) {
            [delegate.commentCounts replaceObjectAtIndex:index withObject:newCounts];
            NSArray *comArray=[[delegate.fourCommentArray objectAtIndex:index] componentsSeparatedByString:@"-END_OF_DATA-LINE-"];
            if ([comArray count]==5) {
                
                NSString *oldArray=[[[[[[comArray objectAtIndex:1]stringByAppendingFormat:@"-END_OF_DATA-LINE-"]
                                       stringByAppendingFormat:@"%@",[comArray objectAtIndex:2]]stringByAppendingFormat:@"-END_OF_DATA-LINE-"]
                                     stringByAppendingFormat:@"%@",[comArray objectAtIndex:3]]stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
                
                NSString *commentStr=[[[[oldArray stringByAppendingFormat:@"@ "] stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
                [delegate.fourCommentArray replaceObjectAtIndex:index withObject:commentStr];
            }
            else if([[delegate.fourCommentArray objectAtIndex:index] isEqualToString:@"No Comments"])
            {
                NSString *commentStr=[[@"@" stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@", [commentText.text  stringByAppendingFormat:@"-END_OF_DATA-LINE-"]];
                [delegate.fourCommentArray replaceObjectAtIndex:index withObject:commentStr];
            }
            else
            {
                NSString *commentStr=[[[[[delegate.fourCommentArray objectAtIndex:index]stringByAppendingFormat:@"@"] stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
                [delegate.fourCommentArray replaceObjectAtIndex:index withObject:commentStr];
            }
        }
        else if([getTabId isEqualToString:@"profile"])
        {
            [delegate.PcommentCounts replaceObjectAtIndex:index withObject:newCounts];
            NSArray *comArray=[[delegate.PfourCommentArray objectAtIndex:index] componentsSeparatedByString:@"-END_OF_DATA-LINE-"];
            if ([comArray count]==5) {
                
                NSString *oldArray=[[[[[[comArray objectAtIndex:1]stringByAppendingFormat:@"-END_OF_DATA-LINE-"]
                                       stringByAppendingFormat:@"%@",[comArray objectAtIndex:2]]stringByAppendingFormat:@"-END_OF_DATA-LINE-"]
                                     stringByAppendingFormat:@"%@",[comArray objectAtIndex:3]]stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
                
                NSString *commentStr=[[[[oldArray stringByAppendingFormat:@"@ "] stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
                [delegate.PfourCommentArray replaceObjectAtIndex:index withObject:commentStr];
            }
            else if([[delegate.PfourCommentArray objectAtIndex:index] isEqualToString:@"No Comments"])
            {
                NSString *commentStr=[[[@"@" stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
                [delegate.PfourCommentArray replaceObjectAtIndex:index withObject:commentStr];
            }
            else
            {
                NSString *commentStr=[[[[[delegate.PfourCommentArray objectAtIndex:index]stringByAppendingFormat:@"@"] stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
                [delegate.PfourCommentArray replaceObjectAtIndex:index withObject:commentStr];
            }
            
        }
        
        NSString *resultStr=[json objectForKey:@"param"];
        if([resultStr isEqualToString:@"true"])
        {
            // [self commentsData];
            [typeData addObject:@"comment"];
            [commnetsArray addObject:commentText.text];
            [commentPicArray addObject:delegate.profileImage];
            [commentNameArray addObject:getusername];
            [mytable reloadData];
            commentText.text=@"";
        }
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *typeString=[typeData objectAtIndex:indexPath.row];
    int h;
    if([typeString isEqualToString:@"video"])
    {
        h= 400;
    }
    if([typeString isEqualToString:@"comment"]){
        //height=50;
        NSString *text = [commnetsArray objectAtIndex:indexPath.row];
        NSString *commentStrLength=[[@"@" stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",text];
        
        CGSize constraint = CGSizeMake(self.view.bounds.size.width, 20000.0f);
        
        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont fontWithName:@"HelveticaNeue" size:12] forKey: NSFontAttributeName];
        
        CGSize size = [commentStrLength boundingRectWithSize:constraint
                                                     options:NSLineBreakByWordWrapping
                                                  attributes:stringAttributes context:nil].size;
        
        
        CGFloat height = MAX(size.height, 20.0f);
        
        h = height + (5 * 2);

    }
    
    return h;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    
    return [commnetsArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Login *login=[[Login alloc]init];
    UIImage *minus = [UIImage imageNamed:@"minus.png"];
    UIImage *plus = [UIImage imageNamed:@"Plus.png"];
    UIImage *minusDark = [UIImage imageNamed:@"mDark.png"];
    UIImage *plusDark = [UIImage imageNamed:@"pDark.png"];
    /*UIImage *plusOut = [UIImage imageNamed:@"pOut.png"];
    UIImage *minusOut = [UIImage imageNamed:@"mOut.png"];*/
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cellReturn=nil;
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    static NSString *CellIdentifier2 = @"Cell2";
    UITableViewCell *cell2 =[tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
    mytable.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell2.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [mytable setBackgroundColor: [UIColor clearColor]];
    cell.backgroundColor = [UIColor clearColor];
    cell2.backgroundColor = [UIColor clearColor];
    NSString *typeString=[typeData objectAtIndex:indexPath.row];
    int t=indexPath.row;
    NSLog(@"index path row %i",t);
    if([typeString isEqualToString:@"video"])
    {
       
        
        
        
        if (cell == nil)
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            NSLog(@"index path in cell3 %i",indexPath.row);
        }
        
        CGFloat x = [UIScreen mainScreen].bounds.origin.x;
        UIView *backgroundCellView = [[UIView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 395)];
        backgroundCellView.backgroundColor = [UIColor clearColor];
        UIImageView *backimage = [[UIImageView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 395)];
        backimage.backgroundColor = [UIColor whiteColor];
        [backgroundCellView addSubview:backimage];
        cell.backgroundView = backgroundCellView;
        
        for(UIView *v in [cell.contentView subviews])
        {
            if([v isKindOfClass:[UILabel class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[IFTweetLabel class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[IFLabelUsername class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[UIImageView class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[UIButton class]])
                [v removeFromSuperview];
        }

        
        int lbltag=indexPath.row+1;
        UIButton *playButton=nil;
        playButton.enabled = YES;
        playButton.userInteractionEnabled=YES;
        playButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        UIImage *play = [UIImage imageNamed:@"playbutton.png"];
        [playButton setImage:play forState:UIControlStateNormal];
        [playButton setBackgroundImage:play forState:UIControlStateNormal];
        [playButton showsTouchWhenHighlighted];
         playButton.tintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:0.8];
        
        playButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
        [playButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
        [playButton setFrame:CGRectMake(0,0,320,285)];
        [playButton addTarget:self action:@selector(playButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        NSURL *video=[NSURL URLWithString:videoUrl];
        AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL:video options:nil];
        AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
        generate1.appliesPreferredTrackTransform = YES;
        NSError *err = NULL;
        CMTime time = CMTimeMake(1, 2);
        CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
        UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
        
        
        UIImage *croppedImage;
        
        CGRect croppedRect;
        
        croppedRect = CGRectMake(0,0, 400, 356);
        
        CGImageRef tmp = CGImageCreateWithImageInRect([one CGImage], croppedRect);
        
        croppedImage = [UIImage imageWithCGImage:tmp];
        
        CGImageRelease(tmp);
        
        
        
        NSData *CroppedImageData = UIImageJPEGRepresentation(croppedImage, 0.7);
        
        one = [UIImage imageWithData:CroppedImageData];
        
        [playButton setBackgroundImage:one forState:UIControlStateNormal];
        [playButton setContentMode:UIViewContentModeCenter];
        
        
        [cell.contentView addSubview:playButton];

        
        //int lbltag =indexPath.row;
        UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,290,40,40)];
        imageView2.layer.cornerRadius = 20;
        imageView2.clipsToBounds = YES;
        [cell.contentView addSubview:imageView2];
        
        NSString *picUrlString = getUserImageData;
        UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
        
        if (cachedPic)
        {
            //imgCell.imageView.image = cachedImage;
            imageView2.image = cachedPic;
            
        }
        else
        {
            // you'll want to initialize the image with some blank image as a placeholder
            
            //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
            imageView2.image = [UIImage imageNamed:@"editPic.png"];
            [self.imageDownloadingQueuePic addOperationWithBlock:^{
                NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                UIImage *image    = nil;
                if (imageData)
                    image = [UIImage imageWithData:imageData];
                if (image)
                {
                    // add the image to your cache
                    
                    [self.imageCachePic setObject:image forKey:picUrlString];
                    
                    // finally, update the user interface in the main queue
                    
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                        if (updateCell)
                            imageView2.image = image;
                        
                    }];
                }
            }];
        }
        
        
        NSLog(@"valid");
        IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,290,200,30)];
        nameLabel.numberOfLines=0;
        //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
        nameLabel.textColor = [UIColor clearColor];
        NSString *smile2=getName;//[@"@" stringByAppendingString:[differUser objectAtIndex:in]];
        [nameLabel setText:smile2];
        [nameLabel setLinksEnabled:TRUE];
        nameLabel.clipsToBounds=YES;
        [cell.contentView addSubview:nameLabel];
        
        UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 352, 310, 43)];
        [cell addSubview:buttonsBack];
        buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
        
        UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,295,100,10)];
        placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
        placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
        placeLabel.text=getLocation;
        placeLabel.textAlignment=NSTextAlignmentRight;
        [cell.contentView addSubview:placeLabel];
        UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,315,100,10)];
        timeLabel.textColor=[[UIColor alloc] initWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:1];
        timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
        timeLabel.text=getTime;
        timeLabel.textAlignment=NSTextAlignmentLeft;
        [cell.contentView addSubview:timeLabel];
        
        
        UILabel *likeShow = nil;
        likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,382,43,10)] autorelease];
        likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
        likeShow.text = [NSString stringWithFormat:@"%d",[getLikes intValue]];
        likeShow.font=[UIFont systemFontOfSize:7.0];
        likeShow.tag = lbltag;
        likeShow.textAlignment=NSTextAlignmentCenter;
        [cell addSubview:likeShow];
        
        UILabel *dislikeShow = nil;
        UIButton *dislikeBtn = nil;
        dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,382,43,10)] autorelease];
        dislikeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
        dislikeShow.text = [NSString stringWithFormat:@"%d",[getDisLikes intValue]];
        dislikeShow.font=[UIFont systemFontOfSize:7.0];
        dislikeShow.textAlignment=NSTextAlignmentCenter;
        [cell addSubview:dislikeShow];
        
        UILabel *commentShow = nil;
        commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,382,43,10)] autorelease];
        commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
        commentShow.text = [NSString stringWithFormat:@"%d",[getComments intValue]];
        commentShow.font=[UIFont systemFontOfSize:7.0];
        commentShow.tag = lbltag;
        commentShow.textAlignment=NSTextAlignmentCenter;
        [cell addSubview:commentShow];
        //******************************************************************************
        
        UIButton *likeBtn=nil;
        likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        likeBtn.tag = indexPath.row+1;
        [likeBtn addTarget:self action:@selector(likeVideo:) forControlEvents:UIControlEventTouchUpInside];
        likeBtn.frame=CGRectMake(5,352,62,43);
        likeBtn.titleLabel.font=[UIFont systemFontOfSize:10.0];
        [likeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        if([getLikeConferm  isEqualToString:@"Yes"])
        {
            likeBtn.userInteractionEnabled=NO;
            [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
        }
        else
        {
            [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
        }
        [cell addSubview:likeBtn];
        
        
        dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        dislikeBtn.tag = indexPath.row+1;
        [dislikeBtn addTarget:self action:@selector(dislikeVideo:) forControlEvents:UIControlEventTouchUpInside];
        dislikeBtn.frame=CGRectMake(67,352,62,43);
        if([getDislikeConferm  isEqualToString:@"Yes"])
        {
            dislikeBtn.userInteractionEnabled=NO;
            [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
        }
        else
        {
            [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
        }
        [cell addSubview:dislikeBtn];
        
        UIButton *commentButton=nil;
        commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        commentButton.tag = indexPath.row+1;
        UIImage *comment = [UIImage imageNamed:@"Comment.png"];
        [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
        commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
        [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
        [commentButton setFrame:CGRectMake(129,352,62,43)];
        [commentButton addTarget:self action:@selector(commentClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:commentButton];
        
        
        UIButton *shareButton=nil;
        shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        shareButton.tag = indexPath.row+1;
        UIImage *share = [UIImage imageNamed:@"Share.png"];
        [shareButton setBackgroundImage:share forState:UIControlStateNormal];
        shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
        
        [shareButton setFrame:CGRectMake(191,352,62,43)];
        [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:shareButton];
        
        UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        UIImage *setting = [UIImage imageNamed:@"Settings.png"];
        [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
        [settingsButton setTitle:@"" forState:UIControlStateNormal];
        [settingsButton setFrame:CGRectMake(253,352,62,43)];
        [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        settingsButton.tag=lbltag;
        [cell.contentView addSubview:settingsButton];
        
        
        cellReturn=cell;
        
    }
    else if([typeString isEqualToString:@"comment"])
    {
        if (cell2 == nil) {
            cell2 = [[UITableViewCell alloc]
                     initWithStyle:UITableViewCellStyleDefault
                     reuseIdentifier:CellIdentifier2];
        }
        for(UIView *v in [cell2.contentView subviews])
        {
            if([v isKindOfClass:[UILabel class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[IFTweetLabel class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[IFLabelUsername class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[UIImageView class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[UIButton class]])
                [v removeFromSuperview];
        }
        NSString *textLength = [commnetsArray objectAtIndex:indexPath.row];
        NSString *commentStrLength=[[@"@" stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",textLength];
        
        
        CGSize constraint2 = CGSizeMake(self.view.bounds.size.width, 20000.0f);
        
        
        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont fontWithName:@"HelveticaNeue" size:12] forKey: NSFontAttributeName];
        
        CGSize size2 = [commentStrLength boundingRectWithSize:constraint2
                                                      options:NSLineBreakByWordWrapping
                                                   attributes:stringAttributes context:nil].size;
        
        CGFloat height = MAX(size2.height, 20.0f);
        
        
        
        CGFloat x = [UIScreen mainScreen].bounds.origin.x;
        CGFloat height2 = height + (5 * 2);
        UIView *backgroundCellView = [[UIView alloc] initWithFrame:CGRectMake(x+5, 0, 310, height2)];
        backgroundCellView.backgroundColor = [UIColor clearColor];
        UIImageView *backimage = [[UIImageView alloc] initWithFrame:CGRectMake(x+5, 0, 310, height2)];
        backimage.backgroundColor = [UIColor whiteColor];
        [backgroundCellView addSubview:backimage];
        cell2.backgroundView = backgroundCellView;
        
        
        /*UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(10,5,35,35)];
        [cell2.contentView addSubview:imageView2];
        
        NSString *picUrlString = [commentPicArray objectAtIndex: indexPath.row];
        UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
        
        if (cachedPic)
        {
            //imgCell.imageView.image = cachedImage;
            imageView2.image = cachedPic;
            
        }
        else
        {
            // you'll want to initialize the image with some blank image as a placeholder
            
            //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
            imageView2.image = [UIImage imageNamed:@"editPic.png"];
            [self.imageDownloadingQueuePic addOperationWithBlock:^{
                NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                UIImage *image    = nil;
                if (imageData)
                    image = [UIImage imageWithData:imageData];
                if (image)
                {
                    // add the image to your cache
                    
                    [self.imageCachePic setObject:image forKey:picUrlString];
                    
                    // finally, update the user interface in the main queue
                    
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                        if (updateCell)
                            imageView2.image = image;
                        
                    }];
                }
            }];
        }
        
        IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(55,5,200,20)];
        nameLabel.numberOfLines=0;
        //nameLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
        nameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:12];
        NSString *smile2=[commentNameArray objectAtIndex:t];//[@"@" stringByAppendingString:[differUser objectAtIndex:in]];
        [nameLabel setText:smile2];
        [nameLabel setLinksEnabled:TRUE];
        nameLabel.clipsToBounds=YES;
        
        [cell2.contentView addSubview:nameLabel];*/
        
       
        
        UILabel *commentlabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [commentlabel setLineBreakMode:NSLineBreakByWordWrapping];
        commentlabel.adjustsFontSizeToFitWidth = NO;
        [commentlabel setNumberOfLines:0];
        [commentlabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
        [commentlabel setTag:1];
        
        tweetLabel2 = [[IFTweetLabel alloc] initWithFrame:CGRectZero];
        [tweetLabel2 setLinksEnabled:TRUE];
        tweetLabel2.clipsToBounds=YES;
        [tweetLabel2 setNumberOfLines:0];
        [tweetLabel2 setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
        [tweetLabel2 setTag:1];
        
        NSString *text = [commnetsArray objectAtIndex:t];
        NSString *commentStr=[[@"@" stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",text];
        
        
        CGSize constraint = CGSizeMake(self.view.bounds.size.width, 20000.0f);
        NSDictionary *stringAttributes2 = [NSDictionary dictionaryWithObject:[UIFont fontWithName:@"HelveticaNeue" size:12] forKey: NSFontAttributeName];
        
        CGSize size = [commentStr boundingRectWithSize:constraint
                                               options:NSLineBreakByWordWrapping
                                            attributes:stringAttributes2 context:nil].size;
        
        
        if (!tweetLabel2)
            tweetLabel2 = (IFTweetLabel*)[cell viewWithTag:1];
        //tweetLabel2.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
        NSString *smile=[login smilyString:commentStr];
        [tweetLabel2 setText:smile];
        
        commentHeight = [tweetLabel2.text boundingRectWithSize:CGSizeMake(300, 100) options:(NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName: tweetLabel2.font} context:nil].size;
        tweetLabel2.textColor=[UIColor blackColor];
        [tweetLabel2 setFrame:CGRectMake(self.view.bounds.origin.x + 10,5, self.view.bounds.size.width - 10, MAX(size.height,20.0f))];
        [tweetLabel2 sizeToFit];
        [cell2.contentView addSubview:tweetLabel2];
        
        cellReturn=cell2;
    }
    return cellReturn;
}
-(void)commentClicked:(UIButton*)btnClicked{
    
    [commentText becomeFirstResponder];
    
}
-(void)likeVideo:(UIButton*)btnClicked
{
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    if([getLikeConferm isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend =getSno;
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=video&username=%@&action=liked",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        getLikes =[json valueForKey:@"count"];
        getLikeConferm=@"Yes";
        int index=[getRowIndex intValue];
        if ([getTabId isEqualToString:@"circle"]) {
            [delegate.likearray replaceObjectAtIndex:index withObject:getLikes];
            [delegate.likesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        else if([getTabId isEqualToString:@"profile"])
        {
            [delegate.Plikearray replaceObjectAtIndex:index withObject:getLikes];
            [delegate.PlikesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        
        NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:btnClicked.tag-1 inSection:0];
        NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
        [mytable reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
        //[mytable reloadData];
    }
    
}
-(void)dislikeVideo:(UIButton*)btnClicked
{
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    if([getDislikeConferm isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend =getSno;
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=video&username=%@&action=dislike",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        getDisLikes=[json valueForKey:@"count"];
        getDislikeConferm=@"Yes";
        int index=[getRowIndex intValue];
        if ([getTabId isEqualToString:@"circle"]) {
            [delegate.dislikearray replaceObjectAtIndex:index withObject:getDisLikes];
            [delegate.dislikesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        else if([getTabId isEqualToString:@"profile"])
        {
            [delegate.Pdislikearray replaceObjectAtIndex:index withObject:getDisLikes];
            [delegate.PdislikesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        
        NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:btnClicked.tag-1 inSection:0];
        NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
        [mytable reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    }
    //[mytable reloadData];
    
}

-(IBAction)settingButtonClicked:(id)sender
{
    self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:3];
    
}
-(IBAction)playButtonClicked:(id)sender
{
    NSLog(@"VIDEO URL IN PLAY BUTTON %@",videoUrl);
    NSURL *video=[NSURL URLWithString:videoUrl];
    //NSURL *url = [NSURL URLWithString:videoUrl];
    _movieplayer = [[MPMoviePlayerController alloc] initWithContentURL:video];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:_movieplayer];
    
    _movieplayer.controlStyle = MPMovieControlStyleDefault;
    _movieplayer.shouldAutoplay = YES;
    [self.view addSubview:_movieplayer.view];
    [_movieplayer setFullscreen:YES animated:YES];
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    MPMoviePlayerController *player = [notification object];
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:player];
    
    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
}
-(void)userButtonVideoClicked:(NSString*)username
{
    NSLog(@"userbutton clicked");
    AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
    if ([delegate.mynameLogin isEqualToString:username]) {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
    }
    else
    {
        searchedUserView *searchedUser=[self.storyboard instantiateViewControllerWithIdentifier:@"searchedUser"];
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/userinfo.do?username=%@",username];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSError* error;
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        NSString *stringData= [NSString stringWithContentsOfURL:urlrequest encoding:nil error:nil];
        NSLog(@"Recieved data String &&&&&&&&&&&& %@",stringData);
        
        //NSData *dataofStrig=[NSData dataWithContentsOfFile:stringData];
        NSData * dataofStrig = [stringData dataUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"Recieved data Data &&&&&&&&&&&& %@",stringData);
        
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions                        error:&error];
        if (!json) {
            NSLog(@"Not Json");
        }
        
        NSLog(@"JSON format:- %@",json);
        int length=[json count];
        for(int i=0;i<(length-1);i++)    {
            
            NSString *indexStr = [NSString stringWithFormat:@"%d",i];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            searchedUser.getfollowers=[[json objectForKey:data] valueForKey:@"followerCount"];
            searchedUser.getfollowing=[[json objectForKey:data] valueForKey:@"followingCount"];
            NSString *userName=[[json objectForKey:data] valueForKey:@"username"];
            searchedUser.getUserSearched=userName;
            NSString *status=[[json objectForKey:data]valueForKey:@"status"];
            searchedUser.getStatusSearched=status;
            NSString *userPic=[[json objectForKey:data]valueForKey:@"profile_image"];
            searchedUser.getImageSearched=userPic;
        }
        
        searchedUser.getId=@"circle";
        [self.navigationController pushViewController:searchedUser animated:YES];
    }
}


//****************************************************************************************************
-(void)shareButtonClicked:(UIButton*)btnClicked{
    
    NSLog(@"Helllooo activity viewcontroller");
    
    NSArray *activityItems;
    
    // NSString *myString = [videoUrl absoluteString];
    // NSLog(@"nsurl String %@",myString);
    activityItems = @[videoUrl];
    
    UIActivityViewController *activityController =
    [[UIActivityViewController alloc]
     initWithActivityItems:activityItems
     applicationActivities:nil];
    
    [self presentViewController:activityController
                       animated:YES completion:nil];
    
    
}


-(IBAction)reportButtonClicked:(UIButton*)btnClicked
{
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    delegate.snoReport=getSno;
    delegate.typeReport=getType;
    if([getusername isEqualToString:delegate.mynameLogin])
    {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:3];
    }
    else{
        UIActionSheet  *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Report for Content" delegate:self
                                                         cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Inappropriate"
                                                         otherButtonTitles:nil];
        actionSheet.backgroundColor = [UIColor greenColor];
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        //[actionSheet showInView:self.view];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    }
}
-(void)actionSheet:(UIActionSheet *)actionSheet2 clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == [actionSheet2 destructiveButtonIndex]) {
        [self performSegueWithIdentifier:@"circleToReport" sender:self];
    } else if(buttonIndex == 1){
        
        
    }
}

- (void)viewWillDisappear:(BOOL)animated {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:IFTweetLabelURLNotification object:nil];
    if([getTabId isEqualToString:@"profile"])
    {
        UINavigationBar *navBar = [[self navigationController] navigationBar];
        navBar.hidden=true;
        
    }
    if([getTabId isEqualToString:@"profile2"])
    {
        UINavigationBar *navBar = [[self navigationController] navigationBar];
        navBar.hidden=true;
        
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [commentView release];
    [transView release];
    [super dealloc];
}
@end
