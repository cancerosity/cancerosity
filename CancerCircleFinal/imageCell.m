//
//  imageCell.m
//  CancerCircleFinal
//
//  Created by Raminder on 09/05/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import "imageCell.h"

@implementation imageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    self.imageView.frame = CGRectMake(0,0,320,320);
    self.imageView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.imageView.layer.shadowOffset = CGSizeMake(0, 0);
    self.imageView.layer.shadowOpacity = 0.3;
    self.imageView.layer.shadowRadius = 0.5;
    self.imageView.clipsToBounds = NO;
    //self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imgCellbg.png"]];
    self.contentView.clipsToBounds = YES;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@""]];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat x = [UIScreen mainScreen].bounds.origin.x;
    self.backgroundView.frame = CGRectMake(x+5,0,width-10,495);
    self.backgroundView.backgroundColor = [UIColor whiteColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];

}

@end
