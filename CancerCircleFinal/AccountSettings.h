//
//  AccountSettings.h
//  Cancerosity
//
//  Created by Grant Bevan on 31/03/2014.
//  Copyright (c) 2014 Raminder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "FBViewControllerLogin.h"

@interface AccountSettings : UIViewController<UITextFieldDelegate, UITextViewDelegate>
{
    NSString *idAtSettings;
    NSString *username;
    IBOutlet UITextField *dpName;
    IBOutlet UITextField *userName;
    IBOutlet UITextField *email;
    IBOutlet UITextField *share;
    IBOutlet UITextField *password;
    IBOutlet UITextField *contact;
    IBOutlet UIImageView *userNameImg;
    IBOutlet UIImageView *emailImg;
    IBOutlet UIImageView *shareImg;
    IBOutlet UIImageView *passwordImg;
    IBOutlet UIImageView *contactImg;
    IBOutlet UIImageView *dpNameImg;

    int flag;
    UIAlertView *alertLimit;
    UIAlertView *alert;
    FBViewControllerLogin *fbGraph;
    
    IBOutlet UITableView *accTable;
}
@property (retain, nonatomic) IBOutlet UITableViewCell *firstNCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *lastNCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *emailCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *phoneCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *passCell;

-(IBAction)changeEmail;
-(IBAction)changeNumber;
-(IBAction)changePassword;
-(IBAction)hidekeyboard:(id)sender;
@property (nonatomic, retain) FBViewControllerLogin *fbGraph;

@end
