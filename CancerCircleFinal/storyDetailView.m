//
//  storyDetailView.m
//  CancerCircleFirst
//
//  Created by Raminder on 30/03/13.
//
//

#import "storyDetailView.h"
#import "AppDelegate.h"
#import <Social/Social.h>
#import "Login.h"
#import "searchedUserView.h"
#import "OpenUrlView.h"
#import "SearchViewController.h"
#define FONT_SIZE 11.0f
#define CELL_CONTENT_WIDTH 300.0f
#define CELL_CONTENT_MARGIN 10.0f
#define CELL_CONTENT_WIDTH2 340.0f
#define CELL_CONTENT_MARGIN2 40.0f
#define kNavBarDefaultPosition CGPointMake(160, 42)


@interface storyDetailView ()

@end

CGFloat startContentOffset;
CGFloat lastContentOffset;
BOOL hidden;

@implementation storyDetailView
@synthesize getStory,getDisLikes,getIfDisLikes,getIfLikes,getLikes,getUserImageData,getSno,getName,getComments,getType,getLocation,getTime,getDislikeConferm,getLikeConferm,getRowIndex,getTabId;
@synthesize transBack;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.navigationController.navigationBarHidden = NO;
        
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleTweetNotification:) name:IFTweetLabelURLNotification object:nil];
     [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    commentText.layer.cornerRadius = 5;
    commentText.layer.masksToBounds = YES;

    
    self.view.backgroundColor = [UIColor colorWithRed:(218.0/255.0) green:(221.0/255.0) blue:(226.0/255.0) alpha:1.0];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideOrShow:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideOrShow:) name:UIKeyboardWillShowNotification object:nil];
    //myScroll.pagingEnabled=YES;
    //************* Nvigation bar ****************************************************
    CALayer *layer = self.navigationController.navigationBar.layer;
    layer.position = kNavBarDefaultPosition;
   
    
    self.imageDownloadingQueuePic = [[NSOperationQueue alloc] init];
    self.imageDownloadingQueuePic.maxConcurrentOperationCount = 4;
    self.imageCachePic = [[NSCache alloc] init];
  
   
    
    

    CGFloat titleWidth2 =100;
    CGFloat titleWidth = MIN(titleWidth2, 300);
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 20)];
    label2.backgroundColor = [UIColor clearColor];
    // label.font = [UIFont boldSystemFontOfSize:20.0];
    label2.font = [UIFont fontWithName:@"Futura_Light" size:20];
   
    label2.textAlignment = NSTextAlignmentCenter;
    label2.textColor =[UIColor whiteColor];
    label2.text=@"Comments";
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    
    
    navBar.barTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    navBar.translucent = YES;
   
    //self.navigationItem.title =@"Sign Up";
    self.navigationItem.titleView = label2;
    [self commentsData];
    getType=@"story";
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    getusername = delegate.mynameLogin;
   	// Do any additional setup after loading the view.
    [mytable reloadData];
   
}
-(void)commentsData
{
    NSLog(@"image view comments data");
    commnetsArray=[[NSMutableArray alloc]initWithCapacity:commnetsArray.count];
    commentNameArray=[[NSMutableArray alloc]initWithCapacity:commentNameArray.count];
    commentPicArray=[[NSMutableArray alloc]initWithCapacity:commentPicArray.count];
    typeData=[[NSMutableArray alloc]initWithCapacity:typeData.count];
    [commentNameArray addObject:@"nothing"];
    [commnetsArray addObject:@"nothing"];
    [commentPicArray addObject:@"nothing"];
    [typeData addObject:@"image"];
    
    
    NSString *urlComment = [NSString stringWithFormat:SERVER_URL@"/showcomment.do?sno=%@&type=story",getSno];
    NSURL *urlrequest=[NSURL URLWithString:urlComment];
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    NSString *stringData= [NSString stringWithContentsOfURL:urlrequest encoding:nil error:nil];
    NSLog(@"Recieved data String &&&&&&&&&&&& %@",stringData);
    
    //NSData *dataofStrig=[NSData dataWithContentsOfFile:stringData];
    NSData * dataofStrig = [stringData dataUsingEncoding:NSUTF8StringEncoding];
    NSError* error;
    
    if(!dataofStrig )
    {
        NSLog(@"errorrrrr no response");
        UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Connection failed" message:@"Check intenet connection" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
        [alert1 show];
        
    }
    
    else{
        NSLog(@"comments data %@",dataofStrig);
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        int length=[json count];
        //getComments=[json objectForKey:@"count"];
        
        for(int i=0;i<length-2;i++)
        {
            NSString *indexStr = [NSString stringWithFormat:@"%d",i];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            NSString *commentStr=[[json objectForKey:data]valueForKey:@"comment"];
            if ([commentStr isEqualToString:@""]) {
                
            }
            else{
                NSArray *info=[commentStr componentsSeparatedByString:@"-SEPARATOR-"];
                [commentNameArray addObject:[info objectAtIndex:0]];
                [commnetsArray addObject:[info objectAtIndex:1]];
                [commentPicArray addObject:[info objectAtIndex:2]];                [typeData addObject:@"comment"];
                [mytable reloadData];
            }
        }
    }
    
}
-(void) popBack {
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)hidekeyboard:(id)sender
{
    [commentText resignFirstResponder];
    CGPoint point = mytable.contentOffset;
    point .y = 0;
    mytable.contentOffset = point;
    [self.view endEditing:YES];
    
}
-(void)expand
{
    if(hidden)
        return;
    
    hidden = YES;
    [transView setHidden:NO];
    
    
    
    [self.navigationController setNavigationBarHidden:YES
                                             animated:YES];
}

-(void)contract
{
    if(!hidden)
        return;
    
    hidden = NO;
    
    
    
    [self.navigationController setNavigationBarHidden:NO
                                             animated:YES];
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    startContentOffset = lastContentOffset = scrollView.contentOffset.y;
    //NSLog(@"scrollViewWillBeginDragging: %f", scrollView.contentOffset.y);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat differenceFromStart = startContentOffset - currentOffset;
    CGFloat differenceFromLast = lastContentOffset - currentOffset;
    lastContentOffset = currentOffset;
    
    
    
    if((differenceFromStart) < 0)
    {
        // scroll up
        if(scrollView.isTracking && (abs(differenceFromLast)>1))
            [self expand];
    }
    else {
        if(scrollView.isTracking && (abs(differenceFromLast)>1))
            [self contract];
    }
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    [self contract];
    return YES;
}
- (void)scrollViewToCenterOfScreen:(UIView *)theView {
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    CGFloat availableHeight = applicationFrame.size.height - 160;            // Remove area covered by keyboard
    CGFloat y = viewCenterY - availableHeight / 1.5;
    //CGFloat y = viewCenterY - availableHeight / 2.5;
    if (y < 0) {
        y = 0;
    }
    [myScroll setContentOffset:CGPointMake(0, y) animated:YES];
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    
    return YES;
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //[scrollView setContentOffset:svos animated:YES];
    
    
    BOOL isDone = YES;
	
	if (isDone)
    {
        [commentText resignFirstResponder];
        CGPoint point = mytable.contentOffset;
        point .y = 0;
        mytable.contentOffset = point;
		return YES;
	} else
    {
		return NO;
	}
}

- (void)keyboardWillHideOrShow:(NSNotification *)note {
    NSDictionary *userInfo = [note userInfo];
    
    NSTimeInterval duration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
    
    CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    CGRect keyboardFrameForTextField = [commentView.superview convertRect:keyboardFrame fromView:nil];
    CGRect newFrame = commentView.frame;
    
    newFrame.origin.y = keyboardFrameForTextField.origin.y - newFrame.size.height;
    
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
        commentView.frame = newFrame;
        CGPoint point = mytable.contentOffset;
        point .y = newFrame.origin.y;
        mytable.contentOffset = point;
        
        
    } completion:nil];
}
- (IBAction)popBack:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)commentButton
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if([commentText.text isEqualToString:@""])
    {
        
    }
    else{
        NSString *converted = [commentText.text stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/comment.do?username=%@&sno=%@&type=story&comment=%@",getusername,getSno,converted];
        NSLog(@"comment Url %@",urlLike);
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSString *newCounts=[json objectForKey:@"count"];
        getComments=newCounts;
        int index=[getRowIndex intValue];
        if ([getTabId isEqualToString:@"circle"]) {
            [delegate.commentCounts replaceObjectAtIndex:index withObject:newCounts];
            NSArray *comArray=[[delegate.fourCommentArray objectAtIndex:index] componentsSeparatedByString:@"-END_OF_DATA-LINE-"];
            if ([comArray count]==5) {
                
                NSString *oldArray=[[[[[[comArray objectAtIndex:1]stringByAppendingFormat:@"-END_OF_DATA-LINE-"]
                                       stringByAppendingFormat:@"%@",[comArray objectAtIndex:2]]stringByAppendingFormat:@"-END_OF_DATA-LINE-"]
                                     stringByAppendingFormat:@"%@",[comArray objectAtIndex:3]]stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
                
                NSString *commentStr=[[[[oldArray stringByAppendingFormat:@"@ "] stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
                [delegate.fourCommentArray replaceObjectAtIndex:index withObject:commentStr];
            }
            else if([[delegate.fourCommentArray objectAtIndex:index] isEqualToString:@"No Comments"])
            {
                NSString *commentStr=[[[@"@" stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
                [delegate.fourCommentArray replaceObjectAtIndex:index withObject:commentStr];
            }
            else
            {
                NSString *commentStr=[[[[[delegate.fourCommentArray objectAtIndex:index]stringByAppendingFormat:@"@"] stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
                [delegate.fourCommentArray replaceObjectAtIndex:index withObject:commentStr];
            }
        }
        else if([getTabId isEqualToString:@"profile"])
        {
            [delegate.PcommentCounts replaceObjectAtIndex:index withObject:newCounts];
            NSArray *comArray=[[delegate.PfourCommentArray objectAtIndex:index] componentsSeparatedByString:@"-END_OF_DATA-LINE-"];
            if ([comArray count]==5) {
                
                NSString *oldArray=[[[[[[comArray objectAtIndex:1]stringByAppendingFormat:@"-END_OF_DATA-LINE-"]
                                       stringByAppendingFormat:@"%@",[comArray objectAtIndex:2]]stringByAppendingFormat:@"-END_OF_DATA-LINE-"]
                                     stringByAppendingFormat:@"%@",[comArray objectAtIndex:3]]stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
                
                NSString *commentStr=[[[[oldArray stringByAppendingFormat:@"@ "] stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
                [delegate.PfourCommentArray replaceObjectAtIndex:index withObject:commentStr];
            }
            else if([[delegate.PfourCommentArray objectAtIndex:index] isEqualToString:@"No Comments"])
            {
                NSString *commentStr=[[[@"@" stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
                [delegate.PfourCommentArray replaceObjectAtIndex:index withObject:commentStr];
            }
            else
            {
                NSString *commentStr=[[[[[delegate.PfourCommentArray objectAtIndex:index]stringByAppendingFormat:@"@"] stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
                [delegate.PfourCommentArray replaceObjectAtIndex:index withObject:commentStr];
            }
            
        }
        NSString *resultStr=[json objectForKey:@"param"];
        if([resultStr isEqualToString:@"true"])
        {
            //[self commentsData];
            [typeData addObject:@"comment"];
            [commnetsArray addObject:commentText.text];
            [commentPicArray addObject:delegate.profileImage];
            [commentNameArray addObject:getusername];
            [mytable reloadData];
            commentText.text=@"";
        }
    }
    
}
- (void)handleTweetNotification:(NSNotification *)notification
{
    NSDictionary *dict = (NSDictionary*)notification.object;
    NSString *strDict = [NSString stringWithFormat:@"%@", dict];
    if([strDict hasPrefix:@"http://"])
    {  OpenUrlView *openUrl=[self.storyboard instantiateViewControllerWithIdentifier:@"OpenUrl"];
        //openUrl.hidesBottomBarWhenPushed = YES;
        openUrl.getUrl=strDict;
        [self.navigationController pushViewController:openUrl animated:YES];
    }
    {
        AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
        delegate.wordToSearch=strDict;
        if (([strDict hasPrefix:@"#"])||([strDict hasPrefix:@"@"])) {
            //[self performSegueWithIdentifier:@"circleToSearch" sender:self];
            SearchViewController *search=[self.storyboard instantiateViewControllerWithIdentifier:@"SearchView"];
            UINavigationBar *navBar = [[self navigationController] navigationBar];
            navBar.hidden=false;
            [self.navigationController pushViewController:search animated:YES];
        }
        else
        {
            [self userButtonVideoClicked:strDict];
        }
        
        
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *typeString=[typeData objectAtIndex:indexPath.row];
    int h;
    if([typeString isEqualToString:@"image"])
    {
        //height= 360;
        NSString *text = getStory;
        
        CGSize constraint = CGSizeMake(310, 20000.0f);
        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont fontWithName:@"HelveticaNeue" size:12] forKey: NSFontAttributeName];
        
        CGSize size = [text boundingRectWithSize:constraint
                                          options:NSLineBreakByCharWrapping
                                       attributes:stringAttributes context:nil].size;

  
        
        CGFloat height = MAX(size.height, 20.0f);
        
        h = height + 135;
    }
   if([typeString isEqualToString:@"comment"]){
        // h=50;
       NSString *textLength = [commnetsArray objectAtIndex:indexPath.row];
       NSString *commentStrLength=[[@"@" stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",textLength];
       
       
       CGSize constraint2 = CGSizeMake(self.view.bounds.size.width, 20000.0f);
       
       
       NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont fontWithName:@"HelveticaNeue" size:12] forKey: NSFontAttributeName];
       
       
       
       
       
       CGSize size = [commentStrLength boundingRectWithSize:constraint2
                                                    options:NSLineBreakByCharWrapping
                                                 attributes:stringAttributes context:nil].size;
       
       
       
       CGFloat height = MAX(size.height, 20.0f);
       
       h = height + (5 * 2);
    }
    
    
    return h;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    
    return [commnetsArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Login *login=[[Login alloc]init];
    UIImage *minus = [UIImage imageNamed:@"minus.png"];
    UIImage *plus = [UIImage imageNamed:@"Plus.png"];
    UIImage *minusDark = [UIImage imageNamed:@"mDark.png"];
    UIImage *plusDark = [UIImage imageNamed:@"pDark.png"];
    /*UIImage *plusOut = [UIImage imageNamed:@"pOut.png"];
    UIImage *minusOut = [UIImage imageNamed:@"mOut.png"];*/
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cellReturn=nil;
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    static NSString *CellIdentifier2 = @"Cell2";
    UITableViewCell *cell2 =[tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
    mytable.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell2.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
      cell2.backgroundColor = [UIColor clearColor];
    mytable.backgroundColor = [UIColor clearColor];
  
   
    
    
    
    NSString *typeString=[typeData objectAtIndex:indexPath.row];
    int t=indexPath.row;
  
    if([typeString isEqualToString:@"image"])
    {
        if (cell == nil) {
            cell = [[UITableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:CellIdentifier];
            
        }
      
        NSString *text = getStory;
         CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);

        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont fontWithName:@"HelveticaNeue" size:12] forKey: NSFontAttributeName];
        
        CGSize size = [text boundingRectWithSize:constraint
                                         options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                      attributes:stringAttributes context:nil].size;
        
       

        
        CGFloat x = [UIScreen mainScreen].bounds.origin.x;
        UIView *backgroundCellView = [[UIView alloc] initWithFrame:CGRectMake(x+5, 0, 310,  MAX(size.height, 20.0f) + 130)];
        backgroundCellView.backgroundColor = [UIColor clearColor];
        UIImageView *backimage = [[UIImageView alloc] initWithFrame:CGRectMake(x+5, 0, 310, MAX(size.height, 20.0f) + 130)];
        backimage.backgroundColor = [UIColor whiteColor];
        [backgroundCellView addSubview:backimage];
        cell.backgroundView = backgroundCellView;
        
        
        
        tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectZero];
        [tweetLabel setNumberOfLines:0];
        tweetLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:12];
         tweetLabel.textColor=[UIColor blackColor];
        [tweetLabel setTag:1];
        [tweetLabel setLinksEnabled:TRUE];
        tweetLabel.clipsToBounds=YES;
        
        

        if (!tweetLabel)
            tweetLabel = (IFTweetLabel*)[cell viewWithTag:1];
        tweetLabel.textColor=[UIColor blackColor];
        NSString *smile=[login smilyString:text];
        [tweetLabel setText:smile];
        [tweetLabel setFrame:CGRectMake(15, 5,300,  MAX(size.height, 20.0f))];
        [cell.contentView addSubview:tweetLabel];
        
        int lbltag=indexPath.row+1;
        UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,  MAX(size.height, 20.0f)+15,40,40)];
        imageView2.layer.cornerRadius = 20;
        imageView2.clipsToBounds = YES;
        [cell.contentView addSubview:imageView2];
        
        NSString *picUrlString = getUserImageData;
        UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
        
        if (cachedPic)
        {
            //imgCell.imageView.image = cachedImage;
            imageView2.image = cachedPic;
            
        }
        else
        {
            // you'll want to initialize the image with some blank image as a placeholder
            
            //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
            imageView2.image = [UIImage imageNamed:@"editPic.png"];
            [self.imageDownloadingQueuePic addOperationWithBlock:^{
                NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                UIImage *image    = nil;
                if (imageData)
                    image = [UIImage imageWithData:imageData];
                if (image)
                {
                    // add the image to your cache
                    
                    [self.imageCachePic setObject:image forKey:picUrlString];
                    
                    // finally, update the user interface in the main queue
                    
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                        if (updateCell)
                            imageView2.image = image;
                        
                    }];
                }
            }];
        }
        
        IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65, MAX(size.height, 20.0f)+15,200,30)];
        nameLabel.numberOfLines=0;
        //nameLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
        //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
        nameLabel.textColor = [UIColor clearColor];
        NSString *smile2=getName;//[@"@" stringByAppendingString:[differUser objectAtIndex:in]];
        [nameLabel setText:smile2];
        [nameLabel setLinksEnabled:TRUE];
        nameLabel.clipsToBounds=YES;
        [cell.contentView addSubview:nameLabel];
        
        UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205, MAX(size.height, 20.0f)+25,100,40)];
        placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
        placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
        placeLabel.text=getLocation;
        placeLabel.textAlignment=NSTextAlignmentRight;
        [cell.contentView addSubview:placeLabel];
        UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65, MAX(size.height, 20.0f)+40,200,10)];
        timeLabel.textColor=[UIColor blackColor];
        timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
        timeLabel.text=getTime;
        timeLabel.textAlignment=NSTextAlignmentLeft;
        [cell.contentView addSubview:timeLabel];

        UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5,  MAX(size.height, 20.0f)+87, 310, 43)];
        [cell addSubview:buttonsBack];
        buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
        
        UILabel *likeShow = nil;
        likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,  MAX(size.height, 20.0f)+117,43,10)] autorelease];
       likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
        likeShow.text =getLikes;// [NSString stringWithFormat:@"%d",[getLikes intValue]];
        likeShow.font=[UIFont systemFontOfSize:7.0];
        likeShow.tag = lbltag;
        likeShow.textAlignment=NSTextAlignmentCenter;
        [cell addSubview:likeShow];
        UILabel *dislikeShow = nil;
        dislikeShow.backgroundColor=[UIColor redColor];
        dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,  MAX(size.height, 20.0f)+117,43,10)] autorelease];
        dislikeShow.textColor= [[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
        dislikeShow.text =getDisLikes;
        dislikeShow.font=[UIFont systemFontOfSize:7.0];
        dislikeShow.textAlignment=NSTextAlignmentCenter;
        [cell addSubview:dislikeShow];
        
        
        UILabel *commentShow = nil;
        commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,  MAX(size.height, 20.0f)+117,43,10)] autorelease];
        commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
        commentShow.text = [NSString stringWithFormat:@"%d",[getComments intValue]];
        commentShow.font=[UIFont systemFontOfSize:7.0];
        commentShow.tag = lbltag;
        commentShow.textAlignment=NSTextAlignmentCenter;
        [cell addSubview:commentShow];
        
        UIButton *likeBtn = nil;
        likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        likeBtn.tag = indexPath.row+1;
        [likeBtn addTarget:self action:@selector(likeFn:) forControlEvents:UIControlEventTouchUpInside];
        likeBtn.frame=CGRectMake(5, MAX(size.height, 20.0f)+87,62,43);
        if([getLikeConferm  isEqualToString:@"Yes"])
        {
            likeBtn.userInteractionEnabled=NO;
            [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
        }
        else
        {
            [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
        }
        
        [cell addSubview:likeBtn];
        
        
        
        
        UIButton *dislikeBtn;
        dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        dislikeBtn.tag = indexPath.row+1;
        [dislikeBtn addTarget:self action:@selector(dislikeFn:) forControlEvents:UIControlEventTouchUpInside];
        dislikeBtn.frame=CGRectMake(67, MAX(size.height, 20.0f)+87,62,43);
        if([getDislikeConferm isEqualToString:@"Yes"])
        {
            dislikeBtn.userInteractionEnabled=NO;
            [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
        }
        else
        {
            [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
        }
        [cell addSubview:dislikeBtn];
        
        UIButton *commentButton=nil;
        commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
       UIImage *comment = [UIImage imageNamed:@"Comment.png"];
        commentButton.tag = indexPath.row+1;
        [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
        commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
        [commentButton setFrame:CGRectMake(129, MAX(size.height, 20.0f)+87,62,43)];
        [commentButton addTarget:self action:@selector(commentClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:commentButton];
        
        
        UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        UIImage *share = [UIImage imageNamed:@"Share.png"];
        shareButton.tag = indexPath.row+1;
        [shareButton setBackgroundImage:share forState:UIControlStateNormal];
        [shareButton setTitle:@"" forState:UIControlStateNormal];
        [shareButton setFrame:CGRectMake(191, MAX(size.height, 20.0f)+87,62,43)];
        [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:shareButton];
        
        UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        UIImage *setting = [UIImage imageNamed:@"Settings.png"];
        settingsButton.tag = indexPath.row+1;
        [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
        [settingsButton setTitle:@"" forState:UIControlStateNormal];
        [settingsButton setFrame:CGRectMake(253, MAX(size.height, 20.0f)+87,62,43)];
        [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:settingsButton];
        
        
        cellReturn=cell;
    }
    else if([typeString isEqualToString:@"comment"])
    {
        if (cell2 == nil) {
            cell2 = [[UITableViewCell alloc]
                     initWithStyle:UITableViewCellStyleDefault
                     reuseIdentifier:CellIdentifier2];
        }
        NSString *textLength = [commnetsArray objectAtIndex:indexPath.row];
        NSString *commentStrLength=[[@"@" stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",textLength];
        
        
        CGSize constraint2 = CGSizeMake(self.view.bounds.size.width, 20000.0f);
        
        
        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont fontWithName:@"HelveticaNeue" size:12] forKey: NSFontAttributeName];
        
        CGSize size2 = [commentStrLength boundingRectWithSize:constraint2
                                                      options:NSLineBreakByWordWrapping
                                                   attributes:stringAttributes context:nil].size;
        
        CGFloat height = MAX(size2.height, 20.0f);
        
        
        
        CGFloat x = [UIScreen mainScreen].bounds.origin.x;
        CGFloat height2 = height + (5 * 2);
        UIView *backgroundCellView = [[UIView alloc] initWithFrame:CGRectMake(x+5, 0, 310, height2)];
        backgroundCellView.backgroundColor = [UIColor clearColor];
        UIImageView *backimage = [[UIImageView alloc] initWithFrame:CGRectMake(x+5, 0, 310, height2)];
        backimage.backgroundColor = [UIColor whiteColor];
        [backgroundCellView addSubview:backimage];
        cell2.backgroundView = backgroundCellView;
        
       /* UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(10,5,35,35)];
        [cell2.contentView addSubview:imageView2];
        
        NSString *picUrlString = [commentPicArray objectAtIndex: indexPath.row];
        UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
        
        if (cachedPic)
        {
            //imgCell.imageView.image = cachedImage;
            imageView2.image = cachedPic;
            
        }
        else
        {
            // you'll want to initialize the image with some blank image as a placeholder
            
            //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
            imageView2.image = [UIImage imageNamed:@"editPic.png"];
            [self.imageDownloadingQueuePic addOperationWithBlock:^{
                NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                UIImage *image    = nil;
                if (imageData)
                    image = [UIImage imageWithData:imageData];
                if (image)
                {
                    // add the image to your cache
                    
                    [self.imageCachePic setObject:image forKey:picUrlString];
                    
                    // finally, update the user interface in the main queue
                    
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                        if (updateCell)
                            imageView2.image = image;
                        
                    }];
                }
            }];
        }
        
        IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(55,5,200,20)];
        nameLabel.numberOfLines=1;
        //nameLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
        nameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:12];
        NSString *smile2=[commentNameArray objectAtIndex:t];//[@"@" stringByAppendingString:[differUser objectAtIndex:in]];
        [nameLabel setText:smile2];
        [nameLabel setLinksEnabled:TRUE];
        nameLabel.clipsToBounds=YES;
        
        [cell2.contentView addSubview:nameLabel];*/
        
        UILabel *commentlabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [commentlabel setLineBreakMode:NSLineBreakByWordWrapping];
        commentlabel.adjustsFontSizeToFitWidth = YES;
        [commentlabel setNumberOfLines:0];
        [commentlabel setFont:[UIFont systemFontOfSize:11]];
        [commentlabel setTag:1];
        
        tweetLabel2 = [[IFTweetLabel alloc] initWithFrame:CGRectZero];
        [tweetLabel2 setLinksEnabled:TRUE];
        tweetLabel2.clipsToBounds=YES;
        [tweetLabel2 setNumberOfLines:0];
        [tweetLabel2 setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
        [tweetLabel2 setTag:1];

        
        // [[label layer] setBorderWidth:2.0f];
        
        //[[cell contentView] addSubview:label];
        NSString *text = [commnetsArray objectAtIndex:t];
        NSString *commentStr=[[@"@" stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",text];
        
        CGSize constraint = CGSizeMake(310, 20000.0f);
        
       
        
        NSDictionary *stringAttributes2 = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:FONT_SIZE] forKey: NSFontAttributeName];
        
        CGSize size = [text boundingRectWithSize:constraint
                                         options:NSLineBreakByCharWrapping
                                      attributes:stringAttributes2 context:nil].size;
 
        if (!tweetLabel2)
            tweetLabel2 = (IFTweetLabel*)[cell viewWithTag:1];
        tweetLabel2.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
        NSString *smile=[login smilyString:commentStr];
        [tweetLabel2 setText:smile];
        tweetLabel2.textColor=[UIColor blackColor];
        [tweetLabel2 setFrame:CGRectMake(10,5, CELL_CONTENT_WIDTH2 - (CELL_CONTENT_MARGIN2*2)-15, MAX(size.height,20.0f))];
        [tweetLabel2 sizeToFit];
        [cell2.contentView addSubview:tweetLabel2];
        
        /*UILabel *palceLabel=[[UILabel alloc]initWithFrame:CGRectMake(60,20,200,20)];
         palceLabel.numberOfLines=0;
         palceLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:11];
         NSMutableAttributedString *attributed=[login atributedString:[commnetsArray objectAtIndex:t]];
         palceLabel.attributedText=attributed;
         [cell2.contentView addSubview:palceLabel];*/
        cellReturn=cell2;
    }
    return cellReturn;
}
-(void)commentClicked:(UIButton*)btnClicked{
    
    [commentText becomeFirstResponder];
    
}
-(IBAction)goSettings
{
    self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:3];
}
-(void)likeFn:(UIButton*)btnClicked
{
    NSLog(@"Tab Id %@",getTabId);
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    if([getLikeConferm isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend =getSno;
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=story&username=%@&action=liked",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        NSString *count=[json valueForKey:@"count"];
        getLikes=count;
        getLikeConferm=@"Yes";
        int index=[getRowIndex intValue];
        if ([getTabId isEqualToString:@"circle"]) {
            [delegate.likearray replaceObjectAtIndex:index withObject:getLikes];
            [delegate.likesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        else if([getTabId isEqualToString:@"profile"])
        {
            [delegate.Plikearray replaceObjectAtIndex:index withObject:getLikes];
            [delegate.PlikesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        else if([getTabId isEqualToString:@"search"])
        {
            NSUInteger index1 = [delegate.snoArray indexOfObject:getSno];
            NSString *datatype=[delegate.dataType objectAtIndex:index1];
            NSLog(@"DataType %@",datatype);
            if([datatype isEqualToString:@"text"])
            {
                [delegate.likearray replaceObjectAtIndex:index1 withObject:getLikes];
                [delegate.likesNameArray replaceObjectAtIndex:index1 withObject:@"Yes"];
            }
            
            //[delegate.Slikearray replaceObjectAtIndex:index withObject:getLikes];
            //[delegate.SlikesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        
        NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:btnClicked.tag-1 inSection:0];
        NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
        [mytable reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
        //[mytable reloadData];
    }
    
}
-(void)dislikeFn:(UIButton*)btnClicked
{
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    if([getDislikeConferm isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend =getSno;
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=story&username=%@&action=dislike",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        NSString *count=[json valueForKey:@"count"];
        getDisLikes=count;
        getDislikeConferm=@"Yes";
        int index=[getRowIndex intValue];
        if ([getTabId isEqualToString:@"circle"]) {
            [delegate.dislikearray replaceObjectAtIndex:index withObject:getDisLikes];
            [delegate.dislikesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        else if([getTabId isEqualToString:@"profile"])
        {
            [delegate.Pdislikearray replaceObjectAtIndex:index withObject:getDisLikes];
            [delegate.PdislikesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        else if([getTabId isEqualToString:@"search"])
        {
            NSUInteger index1 = [delegate.snoArray indexOfObject:getSno];
            NSString *datatype=[delegate.dataType objectAtIndex:index1];
            NSLog(@"DataType %@",datatype);
            if([datatype isEqualToString:@"text"])
            {
                [delegate.dislikearray replaceObjectAtIndex:index1 withObject:getDisLikes];
                [delegate.dislikesNameArray replaceObjectAtIndex:index1 withObject:@"Yes"];
            }
            
            //[delegate.Slikearray replaceObjectAtIndex:index withObject:getLikes];
            //[delegate.SlikesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        
        NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:btnClicked.tag-1 inSection:0];
        NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
        [mytable reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    }
    //[mytable reloadData];
    
}
-(IBAction)userButtonVideoClicked:(NSString*)username
{
    NSLog(@"userbutton clicked");
    AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
    if ([delegate.mynameLogin isEqualToString:username]) {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
    }
    else
    {
        searchedUserView *searchedUser=[self.storyboard instantiateViewControllerWithIdentifier:@"searchedUser"];
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/userinfo.do?username=%@",username];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSURLRequest *req=[[NSURLRequest alloc]initWithURL:urlrequest];
        NSURLResponse  *response = nil;
        NSError* error;
        [NSURLConnection sendSynchronousRequest: req returningResponse: &response error: &error];
        
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        NSString *stringData= [NSString stringWithContentsOfURL:urlrequest encoding:nil error:nil];
        NSLog(@"Recieved data String &&&&&&&&&&&& %@",stringData);
        NSData * dataofStrig = [stringData dataUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"Recieved data Data &&&&&&&&&&&& %@",stringData);
        
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions                        error:&error];
        if (!json) {
            NSLog(@"Not Json");
        }
        
        NSLog(@"JSON format:- %@",json);
        int length=[json count];
        for(int i=0;i<(length-1);i++)    {
            
            NSString *indexStr = [NSString stringWithFormat:@"%d",i];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            searchedUser.getfollowers=[[json objectForKey:data] valueForKey:@"followerCount"];
            searchedUser.getfollowing=[[json objectForKey:data] valueForKey:@"followingCount"];
            NSString *userName=[[json objectForKey:data] valueForKey:@"username"];
            searchedUser.getUserSearched=userName;
            NSString *status=[[json objectForKey:data]valueForKey:@"status"];
            searchedUser.getStatusSearched=status;
            NSString *userPic=[[json objectForKey:data]valueForKey:@"profile_image"];
            searchedUser.getImageSearched=userPic;
        }
        
        searchedUser.getId=@"circle";
        
        [self.navigationController pushViewController:searchedUser animated:YES];
    }
}


//****************************************************************************************************
-(void)shareButtonClicked:(UIButton*)btnClicked{
    
    NSLog(@"Helllooo activity viewcontroller");
    
    NSArray *activityItems;
    
    activityItems = @[getStory];
    
    UIActivityViewController *activityController =
    [[UIActivityViewController alloc]
     initWithActivityItems:activityItems
     applicationActivities:nil];
    
    [self presentViewController:activityController
                       animated:YES completion:nil];
    
    
}


-(IBAction)reportButtonClicked:(UIButton*)btnClicked
{
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    delegate.snoReport=getSno;
    delegate.typeReport=getType;
    if([getusername isEqualToString:delegate.mynameLogin])
    {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:3];
    }
    else{
        UIActionSheet  *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Report for Content" delegate:self
                                                         cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Inappropriate"
                                                         otherButtonTitles:nil];
        actionSheet.backgroundColor = [UIColor greenColor];
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        //[actionSheet showInView:self.view];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    }
}
-(void)actionSheet:(UIActionSheet *)actionSheet2 clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == [actionSheet2 destructiveButtonIndex]) {
        [self performSegueWithIdentifier:@"circleToReport" sender:self];
    } else if(buttonIndex == 1){
        
        
    }
}
- (void)viewWillDisappear:(BOOL)animated {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:IFTweetLabelURLNotification object:nil];
    self.hidesBottomBarWhenPushed = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc {
    [transBack release];
    [scroller release];
    [commentView release];
    [transView release];
    [super dealloc];
}
@end
