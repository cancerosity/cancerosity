//
//  FBTableViewCell.m
//  Cancerosity
//
//  Created by Grant Bevan on 27/03/2014.
//  Copyright (c) 2014 Raminder. All rights reserved.
//

#import "FBTableViewCell.h"

@implementation FBTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_fbName release];
    [super dealloc];
}
@end
