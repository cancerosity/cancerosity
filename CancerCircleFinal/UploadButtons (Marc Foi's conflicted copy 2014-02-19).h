//
//  UploadButtons.h
//  CancerCircleFirst
//
//  Created by Raminder on 25/01/13.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MediaPlayer/MediaPlayer.h"
#import "MobileCoreServices/UTCoreTypes.h"
#import <CoreText/CTStringAttributes.h>
#import <CoreLocation/CoreLocation.h>
#import "FBViewControllerLogin.h"
#import "FbGraphResponse.h"
#import "FXBlurView.h"
#import "GPUImage.h"



@interface UploadButtons : UIViewController <UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    
    
    BOOL flag;
    IBOutlet UIImageView *bgImage;
    UIActivityIndicatorView *indicator;
    IBOutlet UITextField *msgText;
    IBOutlet UITextView *storyView;
    UIAlertView *alert;
    NSString *authStr;
    NSString *getName;
    NSString *idName ;
    
    IBOutlet UIImageView *imageView2;
    UIButton *story;
    UIButton *camera;
    UIButton *gallery;
    
    NSString *buttonTitle;
    NSData *convertedData;
    NSData *videoConvertedData;
    NSString *extension;
    NSMutableArray *videourlarray;
    IBOutlet UIImageView *halfImage;
    IBOutlet UIScrollView *myScroll;
    NSString *idAtUpload;
    NSString *dpImage;
    NSString *PdataStr;
    NSString *dataStr;
    NSString *uploadedData;
    NSString *uploadedType;
    NSString *uploadedElapsed;
    NSString *uploadedInserted;
    NSString *uploadedSno;
    NSString *uploadedLocation;
    CLLocationManager *locationManager;
    FBViewControllerLogin *fbGraph;
    int flagFb;
    BOOL noBorder;
    

    int flagTu;
    int flagFl;
    int flagTw;
    IBOutlet UIButton *fbButton;
    IBOutlet UIButton *twButton;
    IBOutlet UIButton *flButton;
    IBOutlet UIButton *tuButton;
    IBOutlet UIButton *filtersbutton;
    
    IBOutlet UIButton *bordersbutton;
    IBOutlet UIImage *defaultImage;
    CGFloat _scrollViewContentOffsetYThreshold;
   
    
    IBOutlet FXBlurView *transview;
    NSString *facebookClientID;
    NSString *redirectUri;
    NSString *accessToken;
    IBOutlet UIWebView *webView;
    id callbackObject;
    SEL callbackSelector;
    UIImage *borderImage;
    GPUImagePicture *overlay;
    IBOutlet UIImageView *borderView;
    

}
@property (retain, nonatomic) IBOutlet UIImageView *borderView;

@property (retain, nonatomic) IBOutlet GPUImageView *imageView;
@property (retain, nonatomic) IBOutlet UIButton *captureVIdeo;

@property (retain, nonatomic) IBOutlet UIButton *captureImage;
@property (retain, nonatomic) IBOutlet UIButton *galleryImage;

@property (retain, nonatomic) IBOutlet UIButton *photo;
@property (retain, nonatomic) IBOutlet UIButton *video;
@property (nonatomic, retain) UIViewController *fbGraph;

@property (nonatomic, retain) UIImagePickerController *pickView;
@property (retain, nonatomic) IBOutlet UIButton *filtersbutton;
@property (retain, nonatomic) IBOutlet UIButton *textButton;
@property (retain, nonatomic) IBOutlet UIButton *fontsButton;
@property (retain, nonatomic) IBOutlet UIButton *bordersButton;

@property(nonatomic,retain)NSString *getUsrname;
-(IBAction)hidekeyboard:(id)sender;
- (IBAction)filtersbutton:(UIButton *)sender;
@property (retain, nonatomic) IBOutlet UIButton *bordersbutton;
- (IBAction)bordersButton:(UIButton *)sender;
-(IBAction)goSettings;
-(IBAction)postStory:(id)sender;
-(IBAction)browseImage:(id)sender;
-(IBAction)captureImage:(id)sender;
-(IBAction)captureVideo:(id)sender;
-(IBAction)post;
-(IBAction)cancel;
-(IBAction)postOnFb;
-(IBAction)postOnTw;
-(IBAction)postToTumblr;
-(IBAction)postToFlicker;

@property (retain, nonatomic) IBOutlet UIButton *filterbutton;
@property (retain, nonatomic) IBOutlet UIScrollView *filtersScrollview;
@property (retain, nonatomic) IBOutlet UIScrollView *bordersScrollview;


@property (retain, nonatomic) IBOutlet UIScrollView *buttonScroll;
@property(nonatomic, retain)NSURL *selectedVideoUrl;

@property (nonatomic, assign) CGFloat outputJPEGQuality;
@property (nonatomic, assign) CGSize requestedImageSize;



////FB Token
@property (nonatomic, retain) NSString *facebookClientID;
@property (nonatomic, retain) NSString *redirectUri;
@property (nonatomic, retain) NSString *accessToken;
@property (assign) id callbackObject;
@property (assign) SEL callbackSelector;
- (void)authenticateUserWithCallbackObject:(id)anObject andSelector:(SEL)selector andExtendedPermissions:(NSString *)extended_permissions;
@end
