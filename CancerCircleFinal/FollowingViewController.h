//
//  FollowingViewController.h
//  CancerCircleFinal
//
//  Created by Raminder on 17/05/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "newsCell.h"
#import <QuartzCore/QuartzCore.h>

@interface FollowingViewController : UIViewController
{
    IBOutlet UITableView *myTable;
    NSMutableArray *followingNameArray;
    NSMutableArray *followingPicArray;
    NSMutableArray *followingStatusArray;
}
@property (nonatomic, strong) NSOperationQueue *imageDownloadingQueue;
@property (nonatomic, strong) NSCache *imageCache;
@property(nonatomic,retain)NSString *getName;
@end
