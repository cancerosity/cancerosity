//
//  searchedUserView.h
//  CancerCircleFirst
//
//  Created by Raminder on 25/03/13.
//
//

#import <UIKit/UIKit.h>
#import "MediaPlayer/MediaPlayer.h"
#import "MobileCoreServices/UTCoreTypes.h"
#import "MobileCoreServices/UTType.h"
#import <QuartzCore/QuartzCore.h>
#import "IFTweetLabel.h"
#import "imageCell.h"
#import "ILTranslucentView.h"
#import "FXBlurView.h"

@interface searchedUserView : UIViewController<UIActionSheetDelegate>
{
    IBOutlet UITableView * mytable;   
    NSData *imageData;
    NSMutableArray *dataType;
    NSMutableArray *dataUpdate;
    NSMutableArray *dataUpdate2;
    NSString *dataStr;
    NSString *dataStr2;
    UIAlertView *alert;
    IBOutlet UILabel *usernameLabel;   
    UIButton *playButton ;
    int indexrow;
    IBOutlet UIImageView *profileImage;
    IBOutlet UILabel *statuslabel;  
    NSMutableArray *snoArray;
    NSMutableArray *likearray;
    NSMutableArray *dislikearray;   
    UIButton *likeButton;
    UIButton *dislikeButton;
    NSString *userLogin;
    NSString *searchfollowing;
    NSString *searchfollowers;
    NSString *searchfollowingNames;
    NSString *searchfollowersNames;
  //  IBOutlet UILabel *followerLabel;
   // IBOutlet UILabel *followingLabel;
    IBOutlet UIButton *followerLabel;
    IBOutlet UIButton *followingLabel;
    NSMutableArray *likesNameArray;
    NSMutableArray *dislikesNameArray;
    NSMutableArray *commentCounts;
    NSMutableArray *locationArray;
    NSMutableArray *timeArray;
    NSMutableArray *insertedTimeArray;
    NSString *existFollowing;
    NSString *PgetInertedTime;
    NSString *PgetLastInertedTime;
    NSString *profile_image;
    int introw;
    IFTweetLabel *tweetLabel;
    NSMutableArray *fourCommentArray;
    IBOutlet UIButton *followButton;
    UIImage *darkFollow;
    NSString *userPrivacy;
   
    IBOutlet FXBlurView *blurView;
    IBOutlet ILTranslucentView *trans;

}
-(IBAction)followingButton;
-(IBAction)goBack;
-(IBAction)getFollowings;
-(IBAction)getFollowers;
@property (retain, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (retain, nonatomic) IBOutlet UIImageView *profileback;
@property (retain, nonatomic) NSURLConnection *connection;
@property (nonatomic, strong) MPMoviePlayerController *movieplayer;
@property(retain,nonatomic)NSString *videoUrl;
@property(nonatomic,retain)NSString *getUserSearched;
@property(nonatomic,retain)NSString *getImageSearched;
@property (retain, nonatomic) IBOutlet UIButton *followreq;
@property(nonatomic,retain)NSString *getStatusSearched;
@property (strong, nonatomic) NSNumber *getfollowing;
@property (strong, nonatomic) NSNumber *getfollowers;
@property (strong, nonatomic) NSString *getId;
@property (strong, nonatomic) NSString *getDisName;
@property (nonatomic, strong) NSOperationQueue *imageDownloadingQueue;
@property (nonatomic, strong) NSCache *imageCache;

@property (nonatomic, strong) NSOperationQueue *imageDownloadingQueuePic;
@property (nonatomic, strong) NSCache *imageCachePic;
@property (nonatomic, strong) NSOperationQueue *imageDownloadingQueueProfilePic;
@property (nonatomic, strong) NSCache *imageCacheProfilePic;
@property(nonatomic,retain)NSString *getRowIndex;
@end
