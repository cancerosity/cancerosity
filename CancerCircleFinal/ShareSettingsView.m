//
//  ShareSettingsView.m
//  CancerCircleFinal
//
//  Created by Raminder on 22/06/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import "ShareSettingsView.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <Twitter/Twitter.h>

#import "AppDelegate.h"
#import "UploadButtons.h"


@interface ShareSettingsView ()

@end

@implementation ShareSettingsView
@synthesize fbGraph;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated
{
   // AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
    NSUserDefaults *itemUser=[NSUserDefaults standardUserDefaults];
    //NSString *TumblrKey=[delegate.mynameLogin stringByAppendingString:@"itemUser"];
    NSString *tumblrUser=[itemUser objectForKey:@"tumblrUser"];
    tuUsername.text=tumblrUser;
    NSUserDefaults *outhName=[NSUserDefaults standardUserDefaults];
    //NSString *keyOfName=[delegate.mynameLogin stringByAppendingString:@"outhName"];
    NSString *fbUser=[outhName objectForKey:@"outhName"];
    fbUsername.text=fbUser;
    NSUserDefaults *flickrName=[NSUserDefaults standardUserDefaults];
    //NSString *flUser=[flickrName objectForKey:@"flickrName"];
    //flUsername.text=flUser;
    NSUserDefaults *twitterName=[NSUserDefaults standardUserDefaults];
    NSString *twUser=[twitterName objectForKey:@"tweetUser"];
    twUsername.text=twUser;
    UIBarButtonItem *backbutton =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(goBack:)];
    //self.navigationItem.leftBarButtonItem=backbutton;
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
        
        
        [self.fbLogin setTitle:@"Logout" forState:UIControlStateNormal];
        [self.fbLog setText:@"Logout"];
        
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
             if (!error) {
                 [self.fbCell.detailTextLabel setText:user.name];
                 self.fbCell.detailTextLabel.text = user.name;
             } else {
             }
         }];
        
        
    } else {
        
        
        [self.fbLog setText:@"Login"];
        [self.fbCell.detailTextLabel setText:@"No Account"];
        self.fbCell.detailTextLabel.text = @"No Account";
    }
    
    account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:
                                  ACAccountTypeIdentifierTwitter];
    arrayOfAccounts = [account accountsWithAccountType:accountType];
    twitterAccount = [arrayOfAccounts lastObject];
 
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    twitterSignedIn = [prefs boolForKey:@"twitterSign"];
    
    if(twitterSignedIn == NO)
    {
        self.twCell.detailTextLabel.text = @"No Account";
        self.twLog.text = @"Login";
          self.twCell.imageView.image = [UIImage imageNamed:@"twitterGray.png"];
        twitterSignedIn = NO;
        
    }
    else {
       
        
       
        
        twitterSignedIn = YES;
        
        self.twCell.detailTextLabel.text = twitterAccount.username;
         self.twCell.imageView.image = [UIImage imageNamed:@"tweet.png"];
        [self.twLog setText:@"Logout"];
        
        
    }
    
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    self.shareTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
	// Do any additional setup after loading the view.
     self.view.backgroundColor = [UIColor colorWithRed:0.913 green:0.952 blue:0.909 alpha:1];
    [self.fbLogin setNeedsDisplay];
	
    CGFloat titleWidth2 = 100;
    CGFloat titleWidth = MIN(titleWidth2, 300);
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 30)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Futura_Light" size:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=@"Share Settings";
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    navBar.barTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    self.navigationItem.titleView = label;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    UIBarButtonItem *backbutton =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(goBack:)];
    //self.navigationItem.leftBarButtonItem=backbutton;
}
-(IBAction)goBack: (id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)logoutFromTumblr
{
    // AppDelegate *delegate=[[UIApplication sharedApplication]delegate];

    // NSString *TumblrKey=[delegate.mynameLogin stringByAppendingString:@"itemUser"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"tumblrUser"];
     tuUsername.text=@"Logged out";
}
-(IBAction)logoutFromFlickr
{

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"flickrName"];
    flUsername.text=@"Logged out";
}
-(IBAction)logoutFromTwitter: (UIButton *) sender;
{
    
   
    
    account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:
                                  ACAccountTypeIdentifierTwitter];
     twitterAccount = [[ACAccount alloc] initWithAccountType:accountType];
  
    if(twitterSignedIn == YES) {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setBool:NO forKey:@"twitterSign"];
        [prefs synchronize];
        
       
                self.twCell.detailTextLabel.text = @"No Account";
                [self.twLog setText:@"Login"];
                self.twCell.imageView.image = [UIImage imageNamed:@"twitterGray.png"];
                twitterSignedIn = NO;
                
                NSLog(@"the account was removed");
        
    } else {
    [account requestAccessToAccountsWithType:accountType options:nil
                                  completion:^(BOOL granted, NSError *error)
    {
        if (granted == YES)
        {
            arrayOfAccounts = [account accountsWithAccountType:accountType];
            
            
            
            //[self showMessage:@"Your Twitter Account is now linked" withTitle:@"Accounts"];
            
            
           
          
            
            if ([arrayOfAccounts count] > 0)
            {
                twitterAccount = [[ACAccount alloc] initWithAccountType:accountType];
                twitterAccount = [arrayOfAccounts lastObject];
                
                
                ACAccountCredential *credentials = [[ACAccountCredential alloc] initWithOAuthToken:@"2419961347-D0kSpxI4aVfbk49oiwiBvo4PUS8bNBYd2icYvFh" tokenSecret:@"D9HzaKAwnFBIPne1UTPkEWLjGRTPouShVOvd2gmF99YRJ"];
                
                [twitterAccount setCredential:credentials];
                [twitterAccount setUsername:twitterAccount.username];
                
               
                
                
                
                
                [account saveAccount:twitterAccount withCompletionHandler:^(BOOL success, NSError *error) {
                    
                    if (success) {
                        
                        NSLog(@"the account was saved!");
                        
                    } else {
                        
                        NSLog(@"the account was NOT saved");
                    }
                }];

                
                
                twitterSignedIn = YES;
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                [prefs setBool:YES forKey:@"twitterSign"];
                [prefs synchronize];
                self.twCell.detailTextLabel.text = twitterAccount.username;
                self.twCell.imageView.image = [UIImage imageNamed:@"tweet.png"];
                [self.twLog setText:@"Logout"];
                

                
                
                
                
              
            }
        
            else {
               
                        self.twCell.detailTextLabel.text = @"No Account";
                        [self.twLog setText:@"Login"];
                        self.twCell.imageView.image = [UIImage imageNamed:@"twitterGray.png"];
                        NSLog(@"the account was removed");
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                [prefs setBool:NO forKey:@"twitterSign"];
                [prefs synchronize];
            
                    
                
                
                
             
                
            }
        }
    }];
    
    }
    
   }

-(IBAction)logoutFromFb
{
    //AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
   /* fbGraph.accessToken = nil;
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies])
    {
        NSString* domainName = [cookie domain];
        NSRange domainRange = [domainName rangeOfString:@"facebook"];
        if(domainRange.length > 0)
        {
            [storage deleteCookie:cookie];
        }
    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"outhName"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"outhToken"];
     NSUserDefaults *itemUser=[NSUserDefaults standardUserDefaults];
    // NSString *keyOfName=[delegate.mynameLogin stringByAppendingString:@"outhName"];
    [itemUser setObject:nil forKey:@"outhName"];
     NSUserDefaults *outhToken=[NSUserDefaults standardUserDefaults];
   // NSString *keyOfOuth=[delegate.mynameLogin stringByAppendingString:@"outhToken"];
    [outhToken setObject:nil forKey:@"outhToken"];
*/
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
        
        // Close the session and remove the access token from the cache
        // The session state handler (in the app delegate) will be called automatically
        [FBSession.activeSession closeAndClearTokenInformation];
        UploadButtons * upload = [[UploadButtons alloc]init];
        [upload.self.fbButton setSelected:FALSE];
        [upload.self.fbButton setSelected:NO];
        [self userLoggedOut];
        
  
        
        // If the session state is not any of the two "open" states when the button is clicked
    } else {
        // Open a session showing the user the login UI
        // You must ALWAYS ask for basic_info permissions when opening a session
       /* [FBSession openActiveSessionWithReadPermissions:@[@"basic_info"]
                                           allowLoginUI:YES
                                      completionHandler:
         ^(FBSession *session, FBSessionState state, NSError *error) {
             
             if (error) {
                 
                 AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
                 [appDelegate sessionStateChanged:session state:state error:error];
                 
             } else if (FB_ISSESSIONOPENWITHSTATE(state)) {
                 
                 [self userLoggedIn];
     
                 [self askforPublishPermisions];
                 
                 
             }
             
         }];*/

        
        // first check if this is first time (no read permissions yet)
        [FBSession openActiveSessionWithReadPermissions:@[@"basic_info"] allowLoginUI:YES completionHandler:^(FBSession *session1, FBSessionState status, NSError *error) {
            if (session1.state == FBSessionStateOpen) {
                
                if (FBSession.activeSession.isOpen) {
                    [session1 requestNewPublishPermissions:@[@"publish_actions"] defaultAudience:FBSessionDefaultAudienceFriends completionHandler:^(FBSession *session2, NSError *error) {
                        if (session2.state == FBSessionStateOpenTokenExtended) {
                            [self userLoggedIn];
                        
                        }
                    }];
                }
            }
        }];
    }

}
+ (BOOL) checkFacebookPermissions:(FBSession *)session
{
    NSArray *permissions = [session permissions];
    NSArray *requiredPermissions = @[@"publish_actions"];
    
    for (NSString *perm in requiredPermissions) {
        if (![permissions containsObject:perm]) {
            return NO; // required permission not found
        }
    }
    return YES;
}
-(void) askforPublishPermisions {
    [FBSession openActiveSessionWithPublishPermissions: [NSArray arrayWithObjects: @"publish_stream", nil]
                                       defaultAudience: FBSessionDefaultAudienceFriends
                                          allowLoginUI: YES
     
                                     completionHandler: ^(FBSession *session, FBSessionState state, NSError *error)
     
     {
       
         if (error) {
             
             AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
             [appDelegate sessionStateChanged:session state:state error:error];
             
         } else if (FB_ISSESSIONOPENWITHSTATE(state)) {
             
        
             
             
         }
         
     }];

}

- (void)userLoggedOut
{
    
    self.fbCell.imageView.image = [UIImage imageNamed:@"fbGray.png"];
    [self.fbLog setText:@"Login"];
    [self.fbCell.detailTextLabel setText:@"No Account"];
    self.fbCell.detailTextLabel.text = @"No Account";
    
}

// Show the user the logged-in UI
- (void)userLoggedIn
{
    
  
    [self.fbLog setText:@"Logout"];
    self.fbCell.imageView.image = [UIImage imageNamed:@"fb.png"];
    
    [[FBRequest requestForMe] startWithCompletionHandler:
     ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
         if (!error) {
             [self.fbCell.detailTextLabel setText:user.name];
             self.fbCell.detailTextLabel.text = user.name;
             
         } else {
         }
     }];
      [self showMessage:@"Your Facebook Account is now linked" withTitle:@"Accounts"];
    
    
    
}
- (void)showMessage:(NSString *)text withTitle:(NSString *)title
{
    [[[UIAlertView alloc] initWithTitle:title
                                message:text
                               delegate:self
                      cancelButtonTitle:@"close"
                      otherButtonTitles:nil] show];
}

-(IBAction)dismiss: (id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}
-(void)createDismissButton {
    UIBarButtonItem *backbutton =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(dismiss:)];
    self.navigationItem.leftBarButtonItem=backbutton;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
    //  return 5;
    
}
- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    
    static NSString *CellIdentifier = @"fbCell";
    static NSString *CellIdentifier2 = @"twCell";
    static NSString *CellIdentifier3 = @"flCell";
    static NSString *CellIdentifier4 = @"tmCell";

    self.shareTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    [self.shareTable setBackgroundColor: [[UIColor alloc] initWithRed:(218.0/255.0) green:(221.0/255.0) blue:(226.0/255.0) alpha:1.0]];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
    }
     if(indexPath.row == 0){
         
         self.fbCell = [tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
         if (!self.fbCell)
         {
             self.fbCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];

            

             
             self.fbCell.textLabel.text = @"Facebook";
             self.fbLog = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 70, 50)];
             self.fbLog.textColor = [UIColor grayColor];
             self.fbCell.accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 50)];
             [self.fbCell.accessoryView addSubview:self.fbLog];
             self.fbCell.accessoryView.backgroundColor = [UIColor clearColor];
             
             
             if (FBSession.activeSession.state == FBSessionStateOpen
                 || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
                 
                 
                 [self.fbLog setText:@"Logout"];
                 self.fbCell.imageView.image = [UIImage imageNamed:@"fb.png"];
                 
                 [[FBRequest requestForMe] startWithCompletionHandler:
                  ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
                      if (!error) {
                          [self.fbCell.detailTextLabel setText:user.name];
                          self.fbCell.detailTextLabel.text = user.name;
                          
                      } else {
                      }
                  }];
                 
                 
             } else {
                 
                 self.fbCell.imageView.image = [UIImage imageNamed:@"fbGray.png"];
                 [self.fbLog setText:@"Login"];
                 [self.fbCell.detailTextLabel setText:@"No Account"];
                 self.fbCell.detailTextLabel.text = @"No Account";
                 
             }
             
     }
    
    
    return self.fbCell;
     }
    else if(indexPath.row ==  1){
        
     self.twCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
    if (!self.twCell)
    {
        self.twCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
        
        self.twCell.textLabel.text = @"Twitter";
        self.twCell.imageView.image = [UIImage imageNamed:@"twitterGray.png"];
        self.twLog = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 70, 50)];
        self.twLog.textColor = [UIColor grayColor];
        
        self.twCell.accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 50)];
        [self.twCell.accessoryView addSubview:self.twLog];
        self.twCell.accessoryView.backgroundColor = [UIColor clearColor];
        
        if(twitterSignedIn == YES)
        {
            twitterAccount =
            [arrayOfAccounts lastObject];
            twitterSignedIn = YES;
            self.twCell.detailTextLabel.text = twitterAccount.username;
            [self.twLog setText:@"Logout"];
        }
        else {
            self.twCell.detailTextLabel.text = @"No Account";
            self.twLog.text = @"Login";
        }
        
    }
    
    return self.twCell;
    }
    else if(indexPath.row ==  2){
        
        self.tmCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier3];
        if (!self.tmCell)
        {
            self.tmCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
            self.tmCell.detailTextLabel.text = @"No Account";
            self.tmCell.textLabel.text = @"Tumblr";
            self.tmCell.imageView.image = [UIImage imageNamed:@"tumblrGray.png"];
            self.tmLog = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 70, 50)];
            self.tmLog.textColor = [UIColor grayColor];
            self.tmLog.text = @"Login";
            self.tmCell.accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 50)];
            [self.tmCell.accessoryView addSubview:self.tmLog];
            self.tmCell.accessoryView.backgroundColor = [UIColor clearColor];
            
        }
        
        return self.tmCell;
    }
    else if(indexPath.row ==  3){
        
        self.flCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier3];
        if (!self.flCell)
        {
            self.flCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
            self.flCell.detailTextLabel.text = @"No Account";
            self.flCell.textLabel.text = @"Flickr";
            self.flCell.imageView.image = [UIImage imageNamed:@"flickrGray.png"];
            self.flLog = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 70, 50)];
            self.flLog.textColor = [UIColor grayColor];
            self.flLog.text = @"Login";
            self.flCell.accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 50)];
            [self.flCell.accessoryView addSubview:self.flLog];
            self.flCell.accessoryView.backgroundColor = [UIColor clearColor];
            
        }
        
        return self.flCell;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0)
    {
        [self performSelector:@selector(logoutFromFb) withObject:nil];
        
        
    }
    if (indexPath.row == 1)
    {
        [self performSelector:@selector(logoutFromTwitter:) withObject:nil];
        
        
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_fbLogin release];
    [_fbUsername release];
    [fbLogin release];
    [_shareTable release];
    [super dealloc];
}
@end
