//
//  ProfileFirst.h
//  CancerCircleFirst
//
//  Created by Raminder on 25/01/13.
//
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "MediaPlayer/MediaPlayer.h"
#import "MobileCoreServices/UTCoreTypes.h"
#import "MobileCoreServices/UTType.h"
#import <QuartzCore/QuartzCore.h>
#import "IFTweetLabel.h"
#import "imageCell.h"
#import "ILTranslucentView.h"
#import "FXBlurView.h"
@interface ProfileFirst : UIViewController<UIActionSheetDelegate>
{
    IBOutlet UITableView * mytable;
    NSString *getStrory;
    NSString *nameStr;
    NSMutableArray *dataType;
    NSMutableArray *dataUpdate;
    NSMutableArray *dataUpdate2;
    NSString *dataStr;
    NSString *dataStr2;
    NSString *whoAreYou;
    UIAlertView *alert;
    NSString *getusername;
    NSString *getuserpass;
    IBOutlet UILabel *usernameLabel;
    NSArray *fields;
    NSString *imageUrl;
    UIButton *moreButton;
    UIButton *playButton ;
    UILabel *storyLabel;
    NSString *idAtProfile;
    UIImageView * imageViewUrl;
    NSString *urlData;
    int introw;
    IBOutlet UIImageView *profileImage;
    IBOutlet UILabel *statuslabel;
    NSString *dpUrl;
    NSString *status;
    NSData *picData;
    NSData *imageData;
    NSMutableArray *snoArray;
    NSMutableArray *likearray;
    NSMutableArray *dislikearray;
    NSString *contentType;
    NSString *countLike;
    NSString *countDilike;
    NSMutableArray *tags;
    UIButton *likeButton;
    UIButton *dislikeButton;
    //IBOutlet UILabel *followinglabel;
    //IBOutlet UILabel *followerLabel;
    IBOutlet UIButton *followinglabel;
    IBOutlet UIButton *followerLabel;
    NSMutableArray *likesNameArray;
    NSMutableArray *dislikesNameArray;
    NSMutableArray *commentCounts;
    NSMutableArray *locationArray;
    NSMutableArray *timeArray;
    NSString *following;
    NSString *followers;
    NSString *followingNames;
    NSString *followersNames;
    IFTweetLabel *tweetLabel;
    NSMutableArray *insertedTimeArray;
    NSMutableArray *fourCommentArray;
    NSString *meImageStr;
    IBOutlet FXBlurView *blurView;
    
    IBOutlet FXBlurView *blurView2;
    IBOutlet UINavigationBar *blurView3;
    UIActionSheet *actionSheet;
    
}
@property (retain, nonatomic) IBOutlet UIImageView *profileback;
-(IBAction)goLogin;
-(IBAction)getFollowers;
-(IBAction)getFollowings;
-(IBAction)goSettings;
-(IBAction)followRequestClicked;
- (IBAction)goNews: (UIButton *)sender;

@property (retain, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (retain, nonatomic) IBOutlet UIButton *followreq;
@property (retain, nonatomic) NSURLConnection *connection;
@property (nonatomic, strong) MPMoviePlayerController *movieplayer;
@property(retain,nonatomic)NSString *videoUrl;
@property (nonatomic, retain) NSArray *imageURLs;
@property (nonatomic, strong) NSOperationQueue *imageDownloadingQueue;
@property (nonatomic, strong) NSCache *imageCache;
@property (strong, nonatomic) NSNumber *getfollowings;
@property (strong, nonatomic) NSNumber *getfollowers;

@end
