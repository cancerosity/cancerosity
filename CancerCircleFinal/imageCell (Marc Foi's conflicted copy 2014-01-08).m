//
//  imageCell.m
//  CancerCircleFinal
//
//  Created by Raminder on 09/05/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import "imageCell.h"

@implementation imageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    self.imageView.frame = CGRectMake(20,10,280,250);
    //self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imgCellbg.png"]];
    self.backgroundView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@""]];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat x = [UIScreen mainScreen].bounds.origin.x;
    self.backgroundView.frame = CGRectMake(x+10,0,width-20,445);
    self.backgroundView.backgroundColor = [UIColor whiteColor];
    self.backgroundColor = [UIColor clearColor];
}

@end
