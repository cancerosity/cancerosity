//
//  ODRefreshControl.m
//  ODRefreshControl
//
//  Created by Fabio Ritrovato on 6/13/12.
//  Copyright (c) 2012 orange in a day. All rights reserved.
//
// https://github.com/Sephiroth87/ODRefreshControl
//

#import "ODRefreshControl.h"
#import <QuartzCore/QuartzCore.h>

#define kTotalViewHeight    100
#define kOpenedViewHeight   44
#define kMinTopPadding      9
#define kMaxTopPadding      5
#define kMinTopRadius       12.5
#define kMaxTopRadius       16
#define kMinBottomRadius    3
#define kMaxBottomRadius    16
#define kMinBottomPadding   4
#define kMaxBottomPadding   6
#define kMinArrowSize       2
#define kMaxArrowSize       3
#define kMinArrowRadius     5
#define kMaxArrowRadius     7
#define kMaxDistance        50

@interface ODRefreshControl ()

@property (nonatomic, assign) UIScrollView *scrollView;
@property (nonatomic, assign) UIEdgeInsets originalContentInset;
@end

@implementation ODRefreshControl

@synthesize refreshing = _refreshing;
@synthesize tintColor = _tintColor;
@synthesize originalContentInset = _originalContentInset;
@synthesize scrollView = _scrollView;
BOOL animating;

static inline CGFloat lerp(CGFloat a, CGFloat b, CGFloat p)
{
    return a + (b - a) * p;
}

- (id)initInScrollView:(UIScrollView *)scrollView
{
    self = [super initWithFrame:CGRectMake(0, -kTotalViewHeight, scrollView.frame.size.width, kTotalViewHeight)];
    if (self) {
        self.scrollView = scrollView;
        self.originalContentInset = scrollView.contentInset;
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [scrollView addSubview:self];
        [scrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
        
        _activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _activity.center = CGPointMake(floor(self.frame.size.width / 2), floor(self.frame.size.height / 2));
        _activity.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        _activity.alpha = 0;
        [_activity startAnimating];
       
        
        _refreshing = NO;
        _canRefresh = YES;
        _tintColor = [UIColor colorWithRed:155.0 / 255.0 green:162.0 / 255.0 blue:172.0 / 255.0 alpha:1.0];
        
        _shapeLayer = [CAShapeLayer layer];
        _shapeLayer.fillColor = [[UIColor clearColor] CGColor];
        _shapeLayer.strokeColor = [[[UIColor clearColor] colorWithAlphaComponent:0.5] CGColor];
        _shapeLayer.lineWidth = 0;
        [self.layer addSublayer:_shapeLayer];
    
        
        circleRefresh = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"refreshcircle.png"]];
        circleRefresh.frame = CGRectMake(floor(self.frame.size.width / 2),
                                         floor(self.frame.size.height / 2),
                                         20, 20);
        
        CGRect frame2 = circleRefresh.frame;
        frame2.origin.x = self.frame.size.width / 2 - frame2.size.width / 2;
        frame2.origin.y = self.frame.size.height / 2 - frame2.size.height / 2;
        circleRefresh.frame = frame2;
        
         circleRefresh.center = CGPointMake(floor(self.frame.size.width / 2), floor(self.frame.size.height / 2));
        
        [self addSubview:circleRefresh];
        
        circleRefresh2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"refreshcircle.png"]];
        circleRefresh2.frame = CGRectMake(floor(self.frame.size.width / 2),
                                         floor(self.frame.size.height / 2),
                                         10, 10);
        
        CGRect frame3 = circleRefresh2.frame;
        frame3.origin.x = self.frame.size.width / 2 - frame3.size.width / 2;
        frame3.origin.y = self.frame.size.height / 2 - frame3.size.height / 2;
        circleRefresh2.frame = frame3;
        
        circleRefresh2.center = CGPointMake(floor(self.frame.size.width / 2), floor(self.frame.size.height / 2));
        
        [self addSubview:circleRefresh2];
        
       
        
        circleRefresh.alpha = 0;
        circleRefresh2.alpha = 0;
        
        _refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pulldown.png"]];
        _refreshArrow.frame = CGRectMake(floor(self.frame.size.width / 2),
                                        floor(kTotalViewHeight - 50 / 2),
                                        20, 11);
        _refreshArrow.alpha = 0;
        CGRect frame = _refreshArrow.frame;
        frame.origin.x = self.frame.size.width / 2 - frame.size.width / 2;
        frame.origin.y = floor(kTotalViewHeight - 50 / 2);
        _refreshArrow.frame = frame;
        [self addSubview:_refreshArrow];
        
        _highlightLayer = [CAShapeLayer layer];
        _highlightLayer.fillColor = [[[UIColor whiteColor] colorWithAlphaComponent:0.2] CGColor];
        [_shapeLayer addSublayer:_highlightLayer];
    }
    return self;
}
- (void) spinWithOptions: (UIViewAnimationOptions) options {
    // this spin completes 360 degrees every 2 seconds
    [UIView animateWithDuration: 0.4f
                          delay: 0.0f
                        options: options
                     animations: ^{
                         circleRefresh.transform = CGAffineTransformRotate(circleRefresh.transform, M_PI / 1);
                     }
                     completion: ^(BOOL finished) {
                         if (finished) {
                             if (animating) {
                                 // if flag still set, keep spinning with constant speed
                                 [self spinWithOptions: UIViewAnimationOptionCurveLinear];
                             } else if (options != UIViewAnimationOptionCurveEaseOut) {
                                 // one last spin, with deceleration
                                 [self spinWithOptions: UIViewAnimationOptionCurveEaseOut];
                             }
                         }
                     }];
    [UIView animateWithDuration: 0.7f
                          delay: 0.0f
                        options: options
                     animations: ^{
                         circleRefresh2.transform = CGAffineTransformRotate(circleRefresh2.transform, -M_PI / 1);
                     }
                     completion: ^(BOOL finished) {
                         if (finished) {
                             if (animating) {
                                 // if flag still set, keep spinning with constant speed
                                 [self spinWithOptions: UIViewAnimationOptionCurveLinear];
                             } else if (options != UIViewAnimationOptionCurveEaseOut) {
                                 // one last spin, with deceleration
                                 [self spinWithOptions: UIViewAnimationOptionCurveEaseOut];
                             }
                         }
                     }];
}

- (void) startSpin {
   /* if (!animating) {
        animating = YES;
        [self spinWithOptions: UIViewAnimationOptionCurveEaseIn];
    }*/
    
    CABasicAnimation *fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    fullRotation.fromValue = [NSNumber numberWithFloat:0];
    fullRotation.toValue = [NSNumber numberWithFloat:((360*M_PI)/180)];
    fullRotation.duration = 1.1;
    fullRotation.repeatCount = 1e100;
    [circleRefresh.layer addAnimation:fullRotation forKey:@"360"];
    
    
    
    CABasicAnimation *fullRotation2 = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    fullRotation2.fromValue = [NSNumber numberWithFloat:0];
    fullRotation2.toValue = [NSNumber numberWithFloat:((-360*M_PI)/180)];
    fullRotation2.duration = 1.2;
    fullRotation2.repeatCount = 1e100;
    [circleRefresh2.layer addAnimation:fullRotation2 forKey:@"360"];
}

- (void) stopSpin {
    // set the flag to stop spinning after one last 90 degree increment
    //animating = NO;
    
    [circleRefresh.layer removeAllAnimations];
    [circleRefresh2.layer removeAllAnimations];
}


- (void)dealloc
{
    [super dealloc];
    [self.scrollView removeObserver:self forKeyPath:@"contentOffset"];
    self.scrollView = nil;
}

- (void)setEnabled:(BOOL)enabled
{
    super.enabled = enabled;
    _shapeLayer.hidden = !self.enabled;
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [super willMoveToSuperview:newSuperview];
    if (!newSuperview) {
        [self.scrollView removeObserver:self forKeyPath:@"contentOffset"];
        self.scrollView = nil;
    }
}

- (void)setTintColor:(UIColor *)tintColor
{
    _tintColor = tintColor;
    _shapeLayer.fillColor = [[UIColor clearColor] CGColor];
}

- (void)setActivityIndicatorViewStyle:(UIActivityIndicatorViewStyle)activityIndicatorViewStyle
{
    _activity.activityIndicatorViewStyle = activityIndicatorViewStyle;
}

- (UIActivityIndicatorViewStyle)activityIndicatorViewStyle
{
    return _activity.activityIndicatorViewStyle;
}

- (UIEdgeInsets)openedInsets
{
    UIEdgeInsets newContentInset = self.originalContentInset;
    newContentInset.top = newContentInset.top + kOpenedViewHeight;
    return newContentInset;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (!self.enabled) {
        self.alpha = 0;
        return;
    }
    
    CGFloat offset = [[change objectForKey:@"new"] CGPointValue].y + self.scrollView.contentInset.top;
    
    CGFloat alpha = 1.0f;
    if ( !_refreshing )
        alpha = ( offset >= 0 ? 0 : ABS(offset)/kOpenedViewHeight);
    self.alpha = alpha;
    
    if (_refreshing) {
        if (offset != 0) {
            // Keep thing pinned at the top
            
            [CATransaction begin];
            [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
            _shapeLayer.position = CGPointMake(0, kMaxDistance + offset + kOpenedViewHeight);
            [CATransaction commit];
            
            //_activity.center = CGPointMake(floor(self.frame.size.width / 2), MIN(offset + self.frame.size.height + floor(self.frame.size.height / 2), floor(self.frame.size.height / 2)));
            _activity.center = CGPointMake(floor(self.frame.size.width / 2), MIN(offset + self.frame.size.height + floor(kOpenedViewHeight / 2), self.frame.size.height - kOpenedViewHeight/ 2));
            circleRefresh.center = CGPointMake(floor(self.frame.size.width / 2), MIN(offset + self.frame.size.height + floor(kOpenedViewHeight / 2), self.frame.size.height - kOpenedViewHeight/ 2));
            circleRefresh2.center = CGPointMake(floor(self.frame.size.width / 2), MIN(offset + self.frame.size.height + floor(kOpenedViewHeight / 2), self.frame.size.height - kOpenedViewHeight/ 2));

            
            // Set the inset only when bouncing back and not dragging
            if (offset >= -kOpenedViewHeight && !self.scrollView.dragging) {
                [self.scrollView setContentInset:self.openedInsets];
            }
        }
        return;
    } else {
        // Check if we can trigger a new refresh
        if (!_canRefresh) {
            if (offset >= 0) {
                _canRefresh = YES;
            } else {
                return;
            }
        } else {
            if (offset >= 0) {
                return;
            }
        }
    }
    
    BOOL triggered = NO;
    
    CGMutablePathRef path = CGPathCreateMutable();
    
    //Calculate some useful points and values
    CGFloat verticalShift = MAX(0, -((kMaxTopRadius + kMaxBottomRadius + kMaxTopPadding + kMaxBottomPadding) + offset));
    CGFloat distance = MIN(kMaxDistance, fabs(verticalShift));
    CGFloat percentage = 1 - (distance / kMaxDistance);
    
    CGFloat currentTopPadding = lerp(kMinTopPadding, kMaxTopPadding, percentage);
    CGFloat currentTopRadius = lerp(kMinTopRadius, kMaxTopRadius, percentage);
    CGFloat currentBottomRadius = lerp(kMinBottomRadius, kMaxBottomRadius, percentage);
    CGFloat currentBottomPadding =  lerp(kMinBottomPadding, kMaxBottomPadding, percentage);
    
    CGPoint bottomOrigin = CGPointMake(floor(self.bounds.size.width / 2), self.bounds.size.height - currentBottomPadding -currentBottomRadius);
    CGPoint topOrigin = CGPointZero;
    if (distance == 0) {
        topOrigin = CGPointMake(floor(self.bounds.size.width / 2), bottomOrigin.y);
    } else {
        topOrigin = CGPointMake(floor(self.bounds.size.width / 2), self.bounds.size.height + offset + currentTopPadding + currentTopRadius);
        if (percentage == 0) {
            bottomOrigin.y -= (fabs(verticalShift) - kMaxDistance);
            triggered = YES;
        }
    }
    NSLog(@"Top Origin: %f %f", topOrigin.x, topOrigin.y);
    NSLog(@"Triggered: %i", triggered);
 
    
    if (!triggered) {

       
        [UIView animateWithDuration:0.25 animations:^{
            [_refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);}];
        
        [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            
            _refreshArrow.alpha = 1; }
                         completion:nil];
        
        
        
    } else {
        // Start the shape disappearance animation
        self.originalContentInset = self.scrollView.contentInset;
   
       
        [CATransaction begin];
        [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
        _activity.layer.transform = CATransform3DMakeScale(0.1, 0.1, 1);
        [CATransaction commit];
        [self startSpin];
        [UIView animateWithDuration:0.25 animations:^{
            [_refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);}];
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            
            _refreshArrow.alpha = 0; }
         completion:nil];
        
        
        [UIView animateWithDuration:0.2 delay:0.15 options:UIViewAnimationOptionCurveLinear animations:^{
            _activity.alpha = 1;
            circleRefresh.alpha = 1;
          
            _activity.layer.transform = CATransform3DMakeScale(1, 1, 1);
         
        } completion:nil];
        [UIView animateWithDuration:0.2 delay:0.5 options:UIViewAnimationOptionCurveLinear animations:^{
           
            
            circleRefresh2.alpha = 1;
          
            
        } completion:nil];
        _refreshing = YES;
        _canRefresh = NO;
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
    
    
    CGPathRelease(path);
}

- (void)beginRefreshing
{
    if (!_refreshing) {
        self.originalContentInset = self.scrollView.contentInset;
        CABasicAnimation *alphaAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        alphaAnimation.duration = 0.2;
        alphaAnimation.toValue = [NSNumber numberWithFloat:0];
        alphaAnimation.fillMode = kCAFillModeForwards;
        alphaAnimation.removedOnCompletion = NO;
        [_shapeLayer addAnimation:alphaAnimation forKey:nil];
        
        [self startSpin];
      
        [_highlightLayer addAnimation:alphaAnimation forKey:nil];
        [_refreshArrow.layer addAnimation:alphaAnimation forKey:nil];
        
        _activity.alpha = 1;
        circleRefresh.alpha = 1;
         circleRefresh2.alpha = 1;
        
        _activity.layer.transform = CATransform3DMakeScale(1, 1, 1);
     
        CGPoint offset = self.scrollView.contentOffset;
        
        [self.scrollView setContentInset:self.openedInsets];
        [self.scrollView setContentOffset:offset animated:NO];
        
        _refreshing = YES;
        _canRefresh = NO;
    }
}

- (void)endRefreshing
{
    if (_refreshing) {
        _refreshing = NO;
        // Create a temporary retain-cycle, so the scrollView won't be released
        // halfway through the end animation.
        // This allows for the refresh control to clean up the observer,
        // in the case the scrollView is released while the animation is running
        [self stopSpin];
        __block UIScrollView *blockScrollView = self.scrollView;
        [UIView animateWithDuration:0.4 animations:^{
            [blockScrollView setContentInset:self.originalContentInset];
            _activity.alpha = 0;
            
            circleRefresh.alpha = 0;
            circleRefresh2.alpha = 0;
            _activity.layer.transform = CATransform3DMakeScale(0.1, 0.1, 1);
          
        } completion:^(BOOL finished) {
            [_shapeLayer removeAllAnimations];
            
            _shapeLayer.path = nil;
            _shapeLayer.shadowPath = nil;
            _shapeLayer.position = CGPointZero;
        
      
            [_highlightLayer removeAllAnimations];
            _highlightLayer.path = nil;
            _refreshArrow.alpha = 1;
            
            // We need to use the scrollView somehow in the end block,
            // or it'll get released in the animation block.
            [blockScrollView setContentInset:self.originalContentInset];
        }];
    }
}

@end
