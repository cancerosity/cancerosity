//
//  FBViewController.h
//  CancerCircleFinal
//
//  Created by Raminder on 07/05/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"
#import <Foundation/Foundation.h>
#import "FbGraphResponse.h"

@interface FBViewControllerLogin :ECSlidingViewController //UIViewController
{
    NSString *facebookClientID;
	NSString *redirectUri;
	NSString *accessToken;
    IBOutlet UIWebView *webView;
    id callbackObject;
	SEL callbackSelector;
    IBOutlet UIActivityIndicatorView *activity;
}

@property (nonatomic, retain) NSString *facebookClientID;
@property (nonatomic, retain) NSString *redirectUri;
@property (nonatomic, retain) NSString *accessToken;
@property (assign) id callbackObject;
@property (assign) SEL callbackSelector;

- (void)authenticateUserWithCallbackObject:(id)anObject andSelector:(SEL)selector andExtendedPermissions:(NSString *)extended_permissions;
-(IBAction)goTab;
@end
