//
//  CircleFirst.m
//  CancerCircleFinal
//
//  Created by Raminder on 29/04/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import "CircleFirst.h"
#import "Login.h"
#import "AppDelegate.h"
#import "ODRefreshControl.h"
#import "imageDetailView.h"
#import "storyDetailView.h"
#import "videoDetailView.h"
#import "SearchViewController.h"
#import "searchedUserView.h"
#import <Social/Social.h>
#import "OpenUrlView.h"
#import "ECSlidingViewController.h"
#import <AVFoundation/AVFoundation.h>


#define kNavBarDefaultPosition CGPointMake(160, 42)


@interface CircleFirst ()

@end

@implementation CircleFirst
@synthesize  getUsername,getNameLogin,getpassword;
@synthesize connection,receivedData,videoUrl,getLatestTime,introw;
@synthesize movieplayer = _movieplayer;

CircleFirst *Cfirst;
UIActivityIndicatorView *indicator;
CGFloat startContentOffset;
CGFloat lastContentOffset;
BOOL hidden;
int x = 0;



-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:hidden
                                             animated:YES];
    thesearchbar.delegate = self;
}


-(void)viewDidAppear:(BOOL)animated
{
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleTweetNotification:) name:IFTweetLabelURLNotification object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleTweetNotification:) name:thesearchbar.text object:nil];
   
    
    [self request];
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    getusername = delegate.mynameLogin;
    delegate.tabId=@"circle";
    delegate.secondTabId=@"circle";
}
- (void)changeSorting:(ODRefreshControl *)refreshControl
//- (void)changeSorting:(UIRefreshControl *)refreshControl
{
  
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self circleData]; // 1
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            [refreshControl endRefreshing];// 2
        });
    });
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    self.imageDownloadingQueue = [[NSOperationQueue alloc] init];
    self.imageDownloadingQueue.maxConcurrentOperationCount = 4;
    self.imageCache = [[NSCache alloc] init];
    
    self.imageDownloadingQueuePic = [[NSOperationQueue alloc] init];
    self.imageDownloadingQueuePic.maxConcurrentOperationCount = 4;
    self.imageCachePic = [[NSCache alloc] init];
   
    
    ODRefreshControl *refreshControl = [[ODRefreshControl alloc] initInScrollView:mytable];
    refreshControl.tintColor=[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1];
    
    refreshControl.backgroundColor = [UIColor clearColor];
    [refreshControl addTarget:self action:@selector(changeSorting:) forControlEvents:UIControlEventValueChanged];
    self.view.backgroundColor = [UIColor colorWithRed:0.913 green:0.952 blue:0.909 alpha:1];
    //************* Nvigation bar ****************************************************
    CGFloat titleWidth2 = 100;
    
    CGFloat titleWidth = MIN(titleWidth2, 300);
    label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 20)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Futura_Light" size:30];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=@"Cancerosity";
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    
    navBar.alpha = 0.5;
    navBar.translucent = YES;
    [navBar setTranslucent:YES];
    navBar.opaque = NO;
    self.navigationItem.titleView = label;
    navBar.backgroundColor = [UIColor clearColor];
     navBar.barTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    
    
    searchview = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,74)];
    searchview.backgroundColor = [[UIColor alloc] initWithRed:(239.0/255.0) green:(239.0/255.0) blue:(244.0/255.0) alpha:1.0];
    thesearchbar = [[UISearchBar alloc] initWithFrame:CGRectMake(0,0,320,44)];
    thesearchbar.searchBarStyle = UISearchBarStyleMinimal;
    searchButtonsView = [[UIView alloc] initWithFrame:CGRectMake(0, 44, 320, 30)];
    recentSearch = [[UIButton alloc] initWithFrame:CGRectMake(-0.5, 0, 108.5, 30)];
    favouriteSearch = [[UIButton alloc] initWithFrame:CGRectMake(108,0, 108.5, 30)];
    popularSearch = [[UIButton alloc] initWithFrame:CGRectMake(215.5,0, 108.5, 30)];
    
    [recentSearch setTitle:@"recents" forState:UIControlStateNormal];
    [favouriteSearch setTitle:@"ravourites" forState:UIControlStateNormal];
    [popularSearch setTitle:@"popular" forState:UIControlStateNormal];
    
    recentSearch.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:10.0];
    recentSearch.titleLabel.textColor = [UIColor colorWithWhite:0.8 alpha:0.5];
    
    recentSearch.backgroundColor = [UIColor clearColor];
    favouriteSearch.backgroundColor = [UIColor clearColor];
    popularSearch.backgroundColor = [UIColor clearColor];
    
    
    
    recentSearch.layer.borderWidth = 0.5;
    recentSearch.layer.borderColor = [UIColor colorWithWhite:0.8 alpha:0.5].CGColor;
    favouriteSearch.layer.borderWidth = 0.5;
    favouriteSearch.layer.borderColor = [UIColor colorWithWhite:0.8 alpha:0.5].CGColor;
    popularSearch.layer.borderWidth = 0.5;
    popularSearch.layer.borderColor = [UIColor colorWithWhite:0.8 alpha:0.5].CGColor;
    
    
    [searchButtonsView addSubview:recentSearch];
    [searchButtonsView addSubview:favouriteSearch];
    [searchButtonsView addSubview:popularSearch];
    
    [searchview addSubview:thesearchbar];
    [searchview addSubview:searchButtonsView];
    searchview.hidden = YES;
   
    
    newsOpen = NO;
    
    [self.view addSubview:searchview];
    
    
    _topBar.backgroundColor = [UIColor clearColor];
    [_topBar setTranslucent:YES];
    _topBar.translucent = YES;
    _topBar.barTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    UILabel* titleLabel = [[UILabel alloc]
                            initWithFrame:CGRectMake(0, -10,
                                                    50, 20)];
    titleLabel.text = @"news";
    titleLabel.font = [UIFont fontWithName:@"Helvetica" size: 15.0];
    titleLabel.textColor = [[UIColor alloc] initWithRed:(255.0/255.0) green:(255.0/255.0) blue:(255.0/255.0) alpha:0.7];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment =  NSTextAlignmentLeft;
    
    UIButton *btn =  [UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor = [UIColor clearColor];
    
    [btn addTarget:self action:@selector(goNews:) forControlEvents:UIControlEventTouchUpInside];
   
    
  
    
   
    [someButton setTitle:@"news" forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(goNews:) forControlEvents:UIControlEventTouchUpInside];
    
    [someButton setShowsTouchWhenHighlighted:YES];
    UIBarButtonItem *backbutton =[[UIBarButtonItem alloc] initWithTitle:@"news" style:UIBarButtonItemStyleBordered target:self action:@selector(goNews:)];
    
    UIBarButtonItem *searchbutton =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch  target:self action:@selector(showsearch:)];
    searchbutton.tintColor = [[UIColor alloc] initWithRed:(255.0/255.0) green:(255.0/255.0) blue:(255.0/255.0) alpha:0.7];
    backbutton.tintColor = [[UIColor alloc] initWithRed:(255.0/255.0) green:(255.0/255.0) blue:(255.0/255.0) alpha:0.7];
    [backbutton setTitleTextAttributes:@{
                                         NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:15.0],
                                         NSForegroundColorAttributeName: [[UIColor alloc] initWithRed:(255.0/255.0) green:(255.0/255.0) blue:(255.0/255.0) alpha:0.7]
                                         } forState:UIControlStateNormal];
    [backbutton setTitlePositionAdjustment:UIOffsetMake(0.0f, 30.0f) forBarMetrics:UIBarMetricsDefault];
  
    self.navigationItem.leftBarButtonItem=backbutton;
    self.navigationItem.rightBarButtonItem = searchbutton;
    [mytable reloadData];
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchbar
{
     [thesearchbar resignFirstResponder];
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    getusername = delegate.mynameLogin;
    delegate.wordToSearch = thesearchbar.text;
    NSString *searchText = [thesearchbar text];
    [recentSearchesDict setObject:searchText forKey:searchText];
    
    self.recentSearchesStrings = [[self.recentSearchesDict allValues] mutableCopy];
    
    [self saveRecentSearches];
    
    [self.recentSearchList reloadData];
    
    if([thesearchbar.text isEqualToString:@""])
    {
        UIAlertView *alertS=[[UIAlertView alloc]initWithTitle:@"No results" message:@"Please Enter #tag and Username" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertS show];
        
    }else {
   [self performSegueWithIdentifier:@"circletosearch" sender:self];
    
    }
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    if(self.slidingViewController.panGesture.state == UIGestureRecognizerStateBegan){
        
        
        if (searchopen == YES)
        {
            
            [thesearchbar resignFirstResponder];
            [UIView animateWithDuration:0.7f
                             animations:^
             {
                 
                 searchview.frame = CGRectMake(searchview.frame.origin.x, 0, searchview.frame.size.width, searchview.frame.size.height);
                 
                 
             }
                             completion:^(BOOL finished)
             {
                 
                 searchview.hidden = YES;
                 searchopen = NO;
             }
             ];
        }else {
            searchopen = YES;
            [UIView animateWithDuration:0.7f
                             animations:^
             {
                 searchview.hidden = NO;
                 searchview.frame = CGRectMake(searchview.frame.origin.x, 64, searchview.frame.size.width, searchview.frame.size.height);
                 
                 
                 
             }
                             completion:^(BOOL finished)
             {
                 
                 [thesearchbar becomeFirstResponder];
             }
             ];
            
        }
        
        
    }
    [super touchesMoved:touches withEvent:event];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if(self.slidingViewController.panGesture.state == UIGestureRecognizerStateBegan){
        
    
    if (searchopen == YES)
    {
        
        [thesearchbar resignFirstResponder];
        [UIView animateWithDuration:0.7f
                         animations:^
         {
             
             searchview.frame = CGRectMake(searchview.frame.origin.x, 0, searchview.frame.size.width, searchview.frame.size.height);
             
             
         }
                         completion:^(BOOL finished)
         {
             
             searchview.hidden = YES;
             searchopen = NO;
         }
         ];
    }else {
        searchopen = YES;
        [UIView animateWithDuration:0.7f
                         animations:^
         {
             searchview.hidden = NO;
             searchview.frame = CGRectMake(searchview.frame.origin.x, 64, searchview.frame.size.width, searchview.frame.size.height);
             
             
             
         }
                         completion:^(BOOL finished)
         {
             
             [thesearchbar becomeFirstResponder];
         }
         ];
        
    }
        
        
    }
    
    [super touchesBegan:touches withEvent:event];
}
-(void)dismissKeyboard {
  
            [thesearchbar resignFirstResponder];
            [UIView animateWithDuration:0.7f
                             animations:^
             {
                 
                 searchview.frame = CGRectMake(searchview.frame.origin.x, 0, searchview.frame.size.width, searchview.frame.size.height);
                 
                 
             }
                             completion:^(BOOL finished)
             {
                 
                 searchview.hidden = YES;
                 
             }
             ];
    
        
    }
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    keyboardShown = YES;
}
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    SearchViewController *destViewController = segue.destinationViewController;
    if ([segue.identifier isEqualToString:@"circletosearch"]) {
        destViewController.searchtext = thesearchbar.text;
        
       
    }}
- (IBAction)showsearch: (UIButton *)sender
{

    
    if (sender.tag == 0) {
        sender.tag = 1;
        
        searchopen = YES;
        [UIView animateWithDuration:0.7f
                         animations:^
         {
             searchview.hidden = NO;
             searchview.frame = CGRectMake(searchview.frame.origin.x, 64, searchview.frame.size.width, searchview.frame.size.height);
           
            
         
         }
                         completion:^(BOOL finished)
         {
             
             [thesearchbar becomeFirstResponder];
         }
         ];
    


        
        
        
        
    }
    else{
        sender.tag=0;
        
      
        searchopen = NO;
        [UIView animateWithDuration:0.7f
                         animations:^
         {
             [thesearchbar resignFirstResponder];
             searchview.frame = CGRectMake(searchview.frame.origin.x, 0, searchview.frame.size.width, searchview.frame.size.height);
             
             
         }
                         completion:^(BOOL finished)
         {
             
            searchview.hidden = YES;
         }
         ];

    }
}
#pragma mark - The Magic!

-(void)expand
{
    if(hidden)
        return;
    
    hidden = YES;
    [_topBar setHidden:NO];
    
   
    
    [self.navigationController setNavigationBarHidden:YES
                                             animated:YES];
}

-(void)contract
{
    if(!hidden)
        return;
    
    hidden = NO;
    
  
    
    [self.navigationController setNavigationBarHidden:NO
                                             animated:YES ];
     [_topBar setHidden:YES];
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
    startContentOffset = lastContentOffset = scrollView.contentOffset.y;
    
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat differenceFromStart = startContentOffset - currentOffset;
    CGFloat differenceFromLast = lastContentOffset - currentOffset;
    lastContentOffset = currentOffset;
    
    
    
    if((differenceFromStart) < 0)
    {
        // scroll up
        if(scrollView.isTracking && (abs(differenceFromLast)>1))
        {
            
            
            if (searchopen == YES) {
            
                   [thesearchbar resignFirstResponder];
                [UIView animateWithDuration:0.7f
                                 animations:^
                 {
                  
                     searchview.frame = CGRectMake(searchview.frame.origin.x, 0, searchview.frame.size.width, searchview.frame.size.height);
                     
                     
                 }
                                 completion:^(BOOL finished)
                 {
                     
                     searchview.hidden = YES;
                 }
                 ];
                [self expand];
                
                
            }else {
            
            [self expand];
            
            }
        }
    }
    else {
        if(scrollView.isTracking && (abs(differenceFromLast)>1))
        {
            if(searchopen == YES)
            {
                searchview.hidden = NO;
                [UIView animateWithDuration:0.7f
                                 animations:^
                 {
                     
                     searchview.frame = CGRectMake(searchview.frame.origin.x, 64, searchview.frame.size.width, searchview.frame.size.height);
                     
                     
                     
                 }
                                 completion:^(BOOL finished)
                 {
                     
                     [thesearchbar becomeFirstResponder];
                 }
                 ];

                [self contract];
            }else {
            
            [self contract];
            }
            
        }
    }
    
}




- (void)handleTweetNotification:(NSNotification *)notification
{
    
    NSDictionary *dict = (NSDictionary*)notification.object;
    NSString *strDict = [NSString stringWithFormat:@"%@", dict];
    
    if([strDict hasPrefix:@"http://"]||[strDict hasPrefix:@"https://"])
    {
        OpenUrlView *openUrl=[self.storyboard instantiateViewControllerWithIdentifier:@"OpenUrl"];
        //openUrl.hidesBottomBarWhenPushed = YES;
        UINavigationBar *navBar = [[self navigationController] navigationBar];
        navBar.hidden=false;
        openUrl.getUrl=strDict;
        [self.navigationController pushViewController:openUrl animated:YES];
        
    }
        else
    {
        AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
        delegate.wordToSearch=strDict;
        if (([strDict hasPrefix:@"#"])||([strDict hasPrefix:@"@"])) {
            //[self performSegueWithIdentifier:@"circleToSearch" sender:self];
            SearchViewController *search=[self.storyboard instantiateViewControllerWithIdentifier:@"SearchView"];
            UINavigationBar *navBar = [[self navigationController] navigationBar];
            navBar.hidden=false;
            [self.navigationController pushViewController:search animated:YES];
        }
        else
        {
            [self userButtonVideoClicked:strDict];
        }
    }
}



- (IBAction)refreshData
{
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self refreshcircleData]; // 1
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [mytable reloadData];// 2
        });
    });
    
}

-(IBAction)goNews: (UIButton *)sender
{
    
    
    if (sender.tag == 0) {
        sender.tag = 1;
        if (searchopen == YES) {
            
            [thesearchbar resignFirstResponder];
            [UIView animateWithDuration:0.7f
                             animations:^
             {
                 
                 searchview.frame = CGRectMake(searchview.frame.origin.x, 0, searchview.frame.size.width, searchview.frame.size.height);
                 
                 
             }
                             completion:^(BOOL finished)
             {
                 
                 searchview.hidden = YES;
             }
             ];
          [self.slidingViewController anchorTopViewTo:ECRight];
            
            
        }else {
            
           [self.slidingViewController anchorTopViewTo:ECRight];

         
        
        
        }
        
        
        
    }
    else{
        sender.tag=0;
        if(searchopen == YES)
        {
            searchview.hidden = NO;
            [UIView animateWithDuration:0.7f
                             animations:^
             {
                 
                 searchview.frame = CGRectMake(searchview.frame.origin.x, 64, searchview.frame.size.width, searchview.frame.size.height);
                 
                 
                 
             }
                             completion:^(BOOL finished)
             {
                 
                 [thesearchbar becomeFirstResponder];
             }
             ];
            
            [self.slidingViewController resetTopView];
        }else {
            
             [self.slidingViewController resetTopView];
        }
        
       
        
    }
}



-(void)request
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    dataUpdate=delegate.dataUpdate;
    NSLog(@"dataUpdate count %i",[dataUpdate count]);
    differUser=delegate.differUser;
    snoArray=delegate.snoArray;
    profielImages=delegate.profielImages;
    likearray=delegate.likearray;
    dislikearray=delegate.dislikearray;
    likesNameArray=delegate.likesNameArray;
    dislikesNameArray=delegate.dislikesNameArray;
    imageDatas=delegate.imageDatas;
    dataType=delegate.dataType;
    NSLog(@"datatypes count %i",[dataType count]);
    commentCounts=delegate.commentCounts;
    locationArray=delegate.locationArray;
    timeArray=delegate.timeArray;
    fourCommentArray=delegate.fourCommentArray;
    getLatestTime=[timeArray objectAtIndex:0];
    // NSLog(@"comments Array %@",commentsArray);
    [mytable reloadData];
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataUpdate count] ;
    //  return 5;
    
}

- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *typeString=[dataType objectAtIndex:indexPath.row];
    int height;
    if([typeString isEqualToString:@"image"])
    {
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)) {
            height= 420;
        }
        else if ([comArray count]==3){
            height= 455;
        }
        else{
            
            height= 465;
        }
        
    }
    else if([typeString isEqualToString:@"video"]){
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)) {
            height= 420;
        }
        else if ([comArray count]==3){
            height= 455;
        }
        else{
            
            height= 465;
        }
        
        
        
    }
    else if([typeString isEqualToString:@"story"]){
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)) {
            height= 155;
        }
        else if ([comArray count]==3){
            height= 185;
        }
        else{
            
            height=225;
        }
        
    }
    else
    {
        height=30;
    }
    return height;
}
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    Login *login=[[Login alloc]init];
    UIImage *minus = [UIImage imageNamed:@"minus.png"];
    UIImage *plus = [UIImage imageNamed:@"Plus.png"];
    UIImage *minusDark = [UIImage imageNamed:@"mDark.png"];
    UIImage *plusDark = [UIImage imageNamed:@"pDark.png"];
     UIImage *plusOut = [UIImage imageNamed:@"pOut.png"];
      UIImage *minusOut = [UIImage imageNamed:@"mOut.png"];
    static NSString *CellIdentifier = @"Cell";
    static NSString *CellIdentifier2 = @"Cell2";
    static NSString *CellIdentifier3 = @"Cell3";
    static NSString *moreCellId = @"more";
    static NSString *image1 = @"image1";
    static NSString *image2 = @"image2";
    static NSString *story1 = @"story1";
    static NSString *story2 = @"story2";
    static NSString *video1 = @"video1";
    static NSString *video2 = @"video2";
    mytable.separatorStyle = UITableViewCellSeparatorStyleNone;
   
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UITableViewCell *ce=[[UITableViewCell alloc]init];
    UITableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
    UITableViewCell *cell3 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier3];
    UITableViewCell *cell4 = [tableView dequeueReusableCellWithIdentifier:moreCellId];
    UITableViewCell *cellstory1 = [tableView dequeueReusableCellWithIdentifier:story1];
    UITableViewCell *cellstory2 = [tableView dequeueReusableCellWithIdentifier:story2];
    UITableViewCell *cellvideo1 = [tableView dequeueReusableCellWithIdentifier:video1];
    UITableViewCell *cellvideo2 = [tableView dequeueReusableCellWithIdentifier:video2];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell2.selectionStyle = UITableViewCellSelectionStyleNone;
    cell3.selectionStyle = UITableViewCellSelectionStyleNone;
    cell4.selectionStyle = UITableViewCellSelectionStyleNone;
    cellstory1.selectionStyle = UITableViewCellSelectionStyleNone;
    cellstory2.selectionStyle = UITableViewCellSelectionStyleNone;
    cellvideo1.selectionStyle = UITableViewCellSelectionStyleNone;
    cellvideo2.selectionStyle = UITableViewCellSelectionStyleNone;
    [mytable setBackgroundColor: [[UIColor alloc] initWithRed:(218.0/255.0) green:(221.0/255.0) blue:(226.0/255.0) alpha:1.0]];
     
    cell.backgroundColor = [UIColor clearColor];
    cell2.backgroundColor = [UIColor clearColor];
    cell3.backgroundColor = [UIColor clearColor];
    cell4.backgroundColor = [UIColor clearColor];
    cellstory1.backgroundColor = [UIColor clearColor];
    cellstory2.backgroundColor = [UIColor clearColor];
    cellvideo1.backgroundColor = [UIColor clearColor];
    cellvideo2.backgroundColor = [UIColor clearColor];
    
    NSString *imageString =[dataUpdate objectAtIndex:indexPath.row];
    NSString *typeString=[dataType objectAtIndex:indexPath.row];
    in=indexPath.row;
    
    int lbltag =indexPath.row+1;
    if([typeString isEqualToString:@"image"])
    {
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)){
            NSLog(@"hiiiiiiiiii image %@",imageString);
            image2Cell *imgCell=[tableView dequeueReusableCellWithIdentifier:image2];
            if (imgCell == nil)
            {
                imgCell = [[[image2Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
                NSLog(@"index path in cell %i",indexPath.row);
                
            }
            for(UIView *v in [imgCell.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFTweetLabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFLabelUsername class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIImageView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIButton class]])
                    [v removeFromSuperview];
            }
            introw=indexPath.row+1;
            
            NSLog(@"value in data update is:- %@",dataUpdate);
            NSString *imageUrlString = [dataUpdate objectAtIndex: indexPath.row];
            
            NSLog(@"image url string iss:- %@",imageUrlString);
            UIImage *cachedImage = [self.imageCache objectForKey:imageUrlString];
            if (cachedImage)
            {
                //imgCell.imageView.image = cachedImage;
                imgCell.imageView.image = cachedImage;
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imgCell.imageView.image = [UIImage imageNamed:@"placholder.png"];
                [self.imageDownloadingQueue addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:imageUrlString];
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData)
                        image = [UIImage imageWithData:imageData];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCache setObject:image forKey:imageString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                //imgCell.imageView.image = image;
                                
                                imgCell.imageView.image = image;
                            
                            
                        }];
                    }
                }];
            }
            
            
            //***********************************
            
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,290,40,40)];
            imageView2.layer.cornerRadius = 20;
            imageView2.clipsToBounds = YES;
            
            [imgCell.contentView addSubview:imageView2];
            
            NSString *picUrlString = [profielImages objectAtIndex: indexPath.row];
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                
                imageView2.image = [UIImage imageNamed:@"CachePic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData)
                        image = [UIImage imageWithData:imageData];
                    if (image)
                    {
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                       
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            
            
            //***********************************
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,290,200,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
             nameLabel.textColor = [UIColor clearColor];
            NSString *smile2=[differUser objectAtIndex:in];
            
            [nameLabel setText:smile2];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.clipsToBounds=YES;
            [imgCell.contentView addSubview:nameLabel];
            UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 372, 310, 43)];
            [imgCell addSubview:buttonsBack];
            buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
            
            NSString *smile;
            IFTweetLabel *descriptionLabel1=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,345,280,20)];
            descriptionLabel1.numberOfLines=0;
            descriptionLabel1.textColor = [UIColor blackColor];
            descriptionLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel1.clipsToBounds=YES;
            
            NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
            NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
            if ([comArray count]==2) {
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
            }
            [imgCell.contentView addSubview:descriptionLabel1];
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,295,100,10)];
            placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [imgCell.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,315,100,10)];
            timeLabel.textColor=[[UIColor alloc] initWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:1];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentLeft;
            [imgCell.contentView addSubview:timeLabel];
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            [likeBtn addTarget:self action:@selector(likeImage:) forControlEvents:UIControlEventTouchUpInside];
            likeBtn.frame=CGRectMake(5,372,62,43);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }        [imgCell addSubview:likeBtn];
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            [dislikeBtn addTarget:self action:@selector(dislikeImage:) forControlEvents:UIControlEventTouchUpInside];
            dislikeBtn.frame=CGRectMake(67,372,62,43);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            [imgCell addSubview:dislikeBtn];
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"Comment.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(129,372,62,43)];
            [commentButton addTarget:self action:@selector(moreButtonImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:commentButton];
            
            
            UILabel *likeShow = nil;
            likeShow.backgroundColor=[UIColor redColor];
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,402,43,10)] autorelease];
            likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:7.0];
            likeShow.textAlignment=NSTextAlignmentCenter;
            likeShow.tag = lbltag;
            [imgCell addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            
            
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,402,43,10)] autorelease];
            dislikeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:7.0];
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            dislikeShow.tag = lbltag;
            [imgCell addSubview:dislikeShow];
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,402,43,10)] autorelease];
            commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:7.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [imgCell addSubview:commentShow];
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"Share.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(191,372,62,43)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:shareButton];
            
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"Settings.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(253,372,62,43)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:settingsButton];
            
            UIButton *moreButtonI=nil;
            moreButtonI = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonI.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonI setBackgroundImage:more forState:UIControlStateNormal];
            
            [moreButtonI setFrame:CGRectMake(285,310,20,4)];
            [moreButtonI addTarget:self action:@selector(moreButtonImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:moreButtonI];
            
            //******************************************************************************
            
            //return cell;
            ce=imgCell;
            
            
        }
        
        else if([comArray count]==3) {
            
            NSLog(@"hiiiiiiiiii image %@",imageString);
            image1Cell *imgCell=[tableView dequeueReusableCellWithIdentifier:image1];
          
            if (imgCell == nil)
            {
                imgCell = [[[image1Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
                NSLog(@"index path in cell %i",indexPath.row);
                
            }
            for(UIView *v in [imgCell.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFTweetLabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFLabelUsername class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIImageView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIButton class]])
                    [v removeFromSuperview];
            }
            introw=indexPath.row+1;
          
            NSLog(@"value in data update is:- %@",dataUpdate);
            NSString *imageUrlString = [dataUpdate objectAtIndex: indexPath.row];
            
            NSLog(@"image url string iss:- %@",imageUrlString);
            UIImage *cachedImage = [self.imageCache objectForKey:imageUrlString];
            if (cachedImage)
            {
                //imgCell.imageView.image = cachedImage;
                imgCell.imageView.image = cachedImage;
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imgCell.imageView.image = [UIImage imageNamed:@"placholder.png"];
                [self.imageDownloadingQueue addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:imageUrlString];
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData)
                        image = [UIImage imageWithData:imageData];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCache setObject:image forKey:imageString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                //imgCell.imageView.image = image;
                                
                                imgCell.imageView.image = image;
                            
                            
                        }];
                    }
                }];
            }
            
            
            //***********************************
            
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,290,40,40)];
            imageView2.layer.cornerRadius = 20;
            imageView2.clipsToBounds = YES;
            [imgCell.contentView addSubview:imageView2];
            
            NSString *picUrlString = [profielImages objectAtIndex: indexPath.row];
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"CachePic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData)
                        image = [UIImage imageWithData:imageData];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            
            
            //***********************************
            //***********************************
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,290,200,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
            nameLabel.textColor = [UIColor clearColor];
            NSString *smile2=[differUser objectAtIndex:in];
            nameLabel.textColor = [UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            [nameLabel setText:smile2];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.clipsToBounds=YES;
            [imgCell.contentView addSubview:nameLabel];
            UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 407, 310, 43)];
            [imgCell addSubview:buttonsBack];
            buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
            
            
            NSString *smile;
            IFTweetLabel *descriptionLabel1=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,345,280,20)];
            descriptionLabel1.numberOfLines=0;
            descriptionLabel1.textColor = [UIColor blackColor];
            descriptionLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel1.clipsToBounds=YES;
            IFTweetLabel *descriptionLabel2=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,360,280,20)];
            descriptionLabel2.numberOfLines=0;
            descriptionLabel2.textColor = [UIColor blackColor];
            descriptionLabel2.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel2.clipsToBounds=YES;
            IFTweetLabel *descriptionLabel3=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,375,280,20)];
            descriptionLabel3.numberOfLines=0;
            descriptionLabel3.textColor = [UIColor blackColor];
            descriptionLabel3.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel3.clipsToBounds=YES;
           
            IFTweetLabel *descriptionLabel4=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,390,280,20)];
            descriptionLabel4.numberOfLines=0;
            descriptionLabel4.textColor = [UIColor blackColor];
            descriptionLabel4.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel4.clipsToBounds=YES;
           
            
            
            NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
            NSMutableAttributedString *attributed;
            NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
            if ([comArray count]==2) {
                //descriptionLabel1.text=@"Only one comment add your comment....";
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
                
            }
            else if ([comArray count]==3) {
                attributed=[login attibutedUsername:[comArray objectAtIndex:0]];
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
                attributed=[login attibutedUsername:[comArray objectAtIndex:1]];
                smile=[login smilyString:[comArray objectAtIndex:1]];
                [descriptionLabel2 setText:smile];
                [descriptionLabel2 setLinksEnabled:TRUE];
            }
            
            
            // }
            
            [imgCell.contentView addSubview:descriptionLabel1];
            [imgCell.contentView addSubview:descriptionLabel2];
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,295,100,10)];
            placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [imgCell.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,315,200,10)];
            timeLabel.textColor=[[UIColor alloc] initWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:1];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentLeft;
            [imgCell.contentView addSubview:timeLabel];
            
            
            //******************************************************************************
            
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            [likeBtn addTarget:self action:@selector(likeImage:) forControlEvents:UIControlEventTouchUpInside];
            likeBtn.frame=CGRectMake(5,407,62,43);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }        [imgCell addSubview:likeBtn];
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            [dislikeBtn addTarget:self action:@selector(dislikeImage:) forControlEvents:UIControlEventTouchUpInside];
            dislikeBtn.frame=CGRectMake(67,407,62,43);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            [imgCell addSubview:dislikeBtn];


            
          
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"Comment.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(129,407,62,43)];
            [commentButton addTarget:self action:@selector(moreButtonImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:commentButton];
            
            
            UILabel *likeShow = nil;
            likeShow.backgroundColor=[UIColor redColor];
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,437,43,10)] autorelease];
            likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:7.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [imgCell addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            
            
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,437,43,10)] autorelease];
            dislikeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:7.0];
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            dislikeShow.tag = lbltag;
            [imgCell addSubview:dislikeShow];
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,437,43,10)] autorelease];
            commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:7.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [imgCell addSubview:commentShow];
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"Share.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(191,407,62,43)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:shareButton];
            
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"Settings.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(253,407,62,43)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:settingsButton];
            
            UIButton *moreButtonI=nil;
            moreButtonI = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonI.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonI setBackgroundImage:more forState:UIControlStateNormal];
            
            [moreButtonI setFrame:CGRectMake(285,310,20,4)];
            [moreButtonI addTarget:self action:@selector(moreButtonImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:moreButtonI];
            
            //******************************************************************************
            
            //return cell;
            ce=imgCell;
            
            
            
        }
        else if(([comArray count]==4)||([comArray count]==5)){
            
            imageCell *imgCell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
           
            if (imgCell == nil)
            {
                imgCell = [[[imageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
                NSLog(@"index path in cell %i",indexPath.row);
                
            }
            for(UIView *v in [imgCell.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFTweetLabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFLabelUsername class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIImageView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIButton class]])
                    [v removeFromSuperview];
            }
            introw=indexPath.row+1;
          
            NSLog(@"value in data update is:- %@",dataUpdate);
            NSString *imageUrlString = [dataUpdate objectAtIndex: indexPath.row];
            
            NSLog(@"image url string iss:- %@",imageUrlString);
            UIImage *cachedImage = [self.imageCache objectForKey:imageUrlString];
            if (cachedImage)
            {
                //imgCell.imageView.image = cachedImage;
                imgCell.imageView.image = cachedImage;
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imgCell.imageView.image = [UIImage imageNamed:@"placholder.png"];
                [self.imageDownloadingQueue addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:imageUrlString];
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData)
                        image = [UIImage imageWithData:imageData];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCache setObject:image forKey:imageString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                //imgCell.imageView.image = image;
                                
                                imgCell.imageView.image = image;
                            
                            
                        }];
                    }
                }];
            }
            
            
            //***********************************
            
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,290,40,40)];
            imageView2.layer.cornerRadius = 20;
            imageView2.clipsToBounds = YES;
            [imgCell.contentView addSubview:imageView2];
             imageView2.image = [UIImage imageNamed:@"CachePic.png"];
            NSString *picUrlString = [profielImages objectAtIndex: indexPath.row];
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"CachePic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData)
                        image = [UIImage imageWithData:imageData];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,290,200,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
            nameLabel.textColor = [UIColor clearColor];
            NSString *smile2=[differUser objectAtIndex:in];
           
            [nameLabel setText:smile2];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.clipsToBounds=YES;
            [imgCell.contentView addSubview:nameLabel];
            UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 417, 310, 43)];
            [imgCell addSubview:buttonsBack];
            buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
            
           
            
            NSString *smile;
            
            IFTweetLabel *descriptionLabel1=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,345,280,20)];
            descriptionLabel1.numberOfLines=0;
            descriptionLabel1.textColor = [UIColor blackColor];
            descriptionLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel1.clipsToBounds=YES;
            IFTweetLabel *descriptionLabel2=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,360,280,20)];
            descriptionLabel2.numberOfLines=0;
            descriptionLabel2.textColor = [UIColor blackColor];
            descriptionLabel2.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel2.clipsToBounds=YES;
            IFTweetLabel *descriptionLabel3=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,375,280,20)];
            descriptionLabel3.numberOfLines=0;
            descriptionLabel3.textColor = [UIColor blackColor];
            descriptionLabel3.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel3.clipsToBounds=YES;
            
            IFTweetLabel *descriptionLabel4=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,390,280,20)];
            descriptionLabel4.numberOfLines=0;
            descriptionLabel4.textColor = [UIColor blackColor];
            descriptionLabel4.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel4.clipsToBounds=YES;
            
            NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
            NSMutableAttributedString *attributed;
            
            NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
            if ([comArray count]==2) {
                //descriptionLabel1.text=@"Only one comment add your comment....";
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
                
            }
            else if ([comArray count]==3) {
                attributed=[login attibutedUsername:[comArray objectAtIndex:0]];
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
                attributed=[login attibutedUsername:[comArray objectAtIndex:1]];
                smile=[login smilyString:[comArray objectAtIndex:1]];
                [descriptionLabel2 setText:smile];
                [descriptionLabel2 setLinksEnabled:TRUE];
            }
            else if ([comArray count]==4) {
                attributed=[login attibutedUsername:[comArray objectAtIndex:0]];
                // descriptionLabel1.attributedText=attributed;
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
                smile=[login smilyString:[comArray objectAtIndex:1]];
                [descriptionLabel2 setText:smile];
                [descriptionLabel2 setLinksEnabled:TRUE];
                smile=[login smilyString:[comArray objectAtIndex:2]];
                [descriptionLabel3 setText:smile];
                [descriptionLabel3 setLinksEnabled:TRUE];
            }
            else if ([comArray count]==5) {
                attributed=[login attibutedUsername:[comArray objectAtIndex:0]];
                //  descriptionLabel1.attributedText=attributed;
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
                smile=[login smilyString:[comArray objectAtIndex:1]];
                [descriptionLabel2 setText:smile];
                [descriptionLabel2 setLinksEnabled:TRUE];
                smile=[login smilyString:[comArray objectAtIndex:2]];
                [descriptionLabel3 setText:smile];
                [descriptionLabel3 setLinksEnabled:TRUE];
                smile=[login smilyString:[comArray objectAtIndex:3]];
                [descriptionLabel4 setText:smile];
                [descriptionLabel4 setLinksEnabled:TRUE];
            }
            
            
            [imgCell.contentView addSubview:descriptionLabel1];
            [imgCell.contentView addSubview:descriptionLabel2];
            [imgCell.contentView addSubview:descriptionLabel3];
            [imgCell.contentView addSubview:descriptionLabel4];
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,295,100,10)];
            placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [imgCell.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,315,200,10)];
            timeLabel.textColor=[[UIColor alloc] initWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:1];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentLeft;
            [imgCell.contentView addSubview:timeLabel];
            
            //******************************************************************************
            
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            [likeBtn addTarget:self action:@selector(likeImage:) forControlEvents:UIControlEventTouchUpInside];
            likeBtn.frame=CGRectMake(5,417,62,43);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }        [imgCell addSubview:likeBtn];
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            [dislikeBtn addTarget:self action:@selector(dislikeImage:) forControlEvents:UIControlEventTouchUpInside];
            dislikeBtn.frame=CGRectMake(67,417,62,43);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            [imgCell addSubview:dislikeBtn];
            
            
            
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"Comment.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(129,417,62,43)];
            [commentButton addTarget:self action:@selector(moreButtonImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:commentButton];
            
            
            UILabel *likeShow = nil;
            likeShow.backgroundColor=[UIColor redColor];
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,447,43,10)] autorelease];
            likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:7.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [imgCell addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            
            
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,447,43,10)] autorelease];
            dislikeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:7.0];
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            dislikeShow.tag = lbltag;
            [imgCell addSubview:dislikeShow];
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,447,43,10)] autorelease];
            commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:7.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [imgCell addSubview:commentShow];
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"Share.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(191,417,62,43)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:shareButton];
            
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"Settings.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(253,417,62,43)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:settingsButton];
            
            UIButton *moreButtonI=nil;
            moreButtonI = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonI.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonI setBackgroundImage:more forState:UIControlStateNormal];
            
            [moreButtonI setFrame:CGRectMake(285,310,20,4)];
            [moreButtonI addTarget:self action:@selector(moreButtonImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:moreButtonI];

            
            //******************************************************************************
            
            //return cell;
            ce=imgCell;
        }
    }
    
    else if([typeString isEqualToString:@"story"])
        
    {
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)){
            NSLog(@"helllloooo");
            
            if (cellstory1 == nil)
            {
                cellstory1 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:story1] autorelease];
                
            }
            
            CGFloat x = [UIScreen mainScreen].bounds.origin.x;
            UIView *backgroundCellView = [[UIView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 150)];
            backgroundCellView.backgroundColor = [UIColor clearColor];
            UIImageView *backimage = [[UIImageView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 150)];
            backimage.backgroundColor = [UIColor whiteColor];
            [backgroundCellView addSubview:backimage];
            cellstory1.backgroundView = backgroundCellView;
            
            
            for(UIView *v in [cellstory1.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFTweetLabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFLabelUsername class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIImageView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIButton class]])
                    [v removeFromSuperview];
            }
            introw=indexPath.row+1;
            tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,5,300,20)];
            tweetLabel.numberOfLines=0;
            tweetLabel.textColor=[UIColor blackColor];
            tweetLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:12];
            NSString *smile2=[login smilyString:[dataUpdate objectAtIndex:in]];
            [tweetLabel setText:smile2];
            [tweetLabel setLinksEnabled:TRUE];
            tweetLabel.clipsToBounds=YES;
            [cellstory1.contentView addSubview:tweetLabel];
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,35,40,40)];
            imageView2.layer.cornerRadius = 20;
            imageView2.clipsToBounds = YES;
            [cellstory1.contentView addSubview:imageView2];
            
            NSString *picUrlString = [profielImages objectAtIndex: indexPath.row];
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"CachePic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData)
                        image = [UIImage imageWithData:imageData];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            
            NSLog(@"valid");
            
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,35,200,30)];
            
            nameLabel.numberOfLines=0;
            //nameLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
            nameLabel.textColor = [UIColor clearColor];
            NSString *smile3=[differUser objectAtIndex:in];//[@"@" stringByAppendingString:[differUser objectAtIndex:in]];
            [nameLabel setText:smile3];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.clipsToBounds=YES;
            
            [cellstory1.contentView addSubview:nameLabel];
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,40,100,10)];
            placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cellstory1.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,60,200,10)];
            timeLabel.textColor=[UIColor blackColor];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentLeft;
            [cellstory1.contentView addSubview:timeLabel];
            
            NSString *smile;
            IFTweetLabel *descriptionLabel1 = [[IFTweetLabel alloc] initWithFrame:CGRectMake(20,86,250,20)];
            descriptionLabel1.numberOfLines=0;
             descriptionLabel1.textColor = [UIColor blackColor];
              descriptionLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12];// correction made here origional was 60 and i predict it for tha 1st comment label  // and now working great at 190
            
         
            
            NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
            if ([comArray count]==2) {
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
            }
            
            [cellstory1 addSubview:descriptionLabel1];
            UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 107, 310, 43)];
            [cellstory1 addSubview:buttonsBack];
            buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
            UILabel *likeShow = nil;
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,137,43,10)] autorelease];
            likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:7.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cellstory1 addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,137,43,10)] autorelease];
            dislikeShow.textColor= [[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:7.0];
            dislikeShow.tag = lbltag;
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cellstory1 addSubview:dislikeShow];
            
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,137,43,10)] autorelease];
            commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:7.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cellstory1 addSubview:commentShow];
            
            //******************************************************************************
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            dislikeBtn.frame=CGRectMake(67,107,62,43);
            [dislikeBtn addTarget:self action:@selector(dislikeFn:) forControlEvents:UIControlEventTouchUpInside];

            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            likeBtn.frame=CGRectMake(5,107,62,43);
            [likeBtn addTarget:self action:@selector(likeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                dislikeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
                [dislikeBtn setBackgroundImage:minusOut forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cellstory1 addSubview:likeBtn];
            
                      NSLog(@"confermation %@",[dislikesNameArray objectAtIndex:indexPath.row]);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                 likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusOut forState:UIControlStateNormal];
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
                
            }
            [cellstory1 addSubview:likeBtn];
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cellstory1 addSubview:dislikeBtn];
            
            
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"Comment.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(129,107,62,43)];
            [commentButton addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory1.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"Share.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(191,107,62,43)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory1.contentView addSubview:shareButton];
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"Settings.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(253,107,62,43)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory1.contentView addSubview:settingsButton];
            
            UIButton *moreButtonS=nil;
            moreButtonS = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonS.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonS setBackgroundImage:more forState:UIControlStateNormal];
            moreButtonS.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [moreButtonS setFrame:CGRectMake(285,55,20,4)];
            [moreButtonS addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory1.contentView addSubview:moreButtonS];
            ce=cellstory1;
            
        }
        else if(([comArray count]==3))
        {
            CGFloat x = [UIScreen mainScreen].bounds.origin.x;
            UIView *backgroundCellView = [[UIView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 180)];
            backgroundCellView.backgroundColor = [UIColor clearColor];
            UIImageView *backimage = [[UIImageView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 180)];
            backimage.backgroundColor = [UIColor whiteColor];
            [backgroundCellView addSubview:backimage];
            cellstory2.backgroundView = backgroundCellView;
            NSLog(@"helllloooo");
            
            if (cellstory2 == nil)
            {
                cellstory2 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:story2] autorelease];
                
            }
            for(UIView *v in [cellstory2.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFTweetLabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFLabelUsername class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIImageView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIButton class]])
                    [v removeFromSuperview];
            }
            
            for(UIView *v in [cellvideo1.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFTweetLabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFLabelUsername class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIImageView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIButton class]])
                    [v removeFromSuperview];
            }
            
            introw=indexPath.row+1;
            tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,5,300,20)];
            tweetLabel.numberOfLines=0;
            tweetLabel.textColor=[UIColor blackColor];
            tweetLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:12];
            NSString *smile2=[login smilyString:[dataUpdate objectAtIndex:in]];
            [tweetLabel setText:smile2];
            [tweetLabel setLinksEnabled:TRUE];
            tweetLabel.clipsToBounds=YES;
            [cellstory2.contentView addSubview:tweetLabel];
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,35,40,40)];
            imageView2.layer.cornerRadius = 20;
            imageView2.clipsToBounds = YES;
            [cellstory2.contentView addSubview:imageView2];

    
            
            NSString *picUrlString = [profielImages objectAtIndex: indexPath.row];
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"CachePic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData)
                        image = [UIImage imageWithData:imageData];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            
            NSLog(@"valid");
            
            
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,35,100,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
            nameLabel.textColor = [UIColor clearColor];
            NSString *smile3=[differUser objectAtIndex:in];//[@"@" stringByAppendingString:[differUser objectAtIndex:in]];
            [nameLabel setText:smile3];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.clipsToBounds=YES;
            
            [cellstory2.contentView addSubview:nameLabel];
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,40,100,10)];
            placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cellstory2.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,60,200,10)];
            timeLabel.textColor=[UIColor blackColor];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentLeft;
            [cellstory2.contentView addSubview:timeLabel];

            
            
            UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 137, 310, 43)];
            [cellstory2 addSubview:buttonsBack];
            buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
            
            NSString *smile;
            IFTweetLabel *descriptionLabel1 = [[IFTweetLabel alloc] initWithFrame:CGRectMake(20,86,250,20)];
            descriptionLabel1.numberOfLines=0;
            descriptionLabel1.textColor = [UIColor blackColor];
            descriptionLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
             descriptionLabel1.clipsToBounds=YES;
            
            IFTweetLabel *descriptionLabel2 = [[IFTweetLabel alloc]initWithFrame:CGRectMake(20,101,250,20)];
            descriptionLabel2.numberOfLines=0;
            descriptionLabel2.textColor = [UIColor blackColor];//correction made here origional was 260 and working fine after changing the width to 230. //the error was that for every second comment the three dot image changes to two as the label hide some portion of image
            descriptionLabel2.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
             descriptionLabel2.clipsToBounds=YES;
            NSMutableAttributedString *attributed;
            
            if ([comArray count]==3) {
                attributed=[login attibutedUsername:[comArray objectAtIndex:0]];
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
                attributed=[login attibutedUsername:[comArray objectAtIndex:1]];
                smile=[login smilyString:[comArray objectAtIndex:1]];
                [descriptionLabel2 setText:smile];
                [descriptionLabel2 setLinksEnabled:TRUE];
            }
            
            [cellstory2 addSubview:descriptionLabel1];
            [cellstory2 addSubview:descriptionLabel2];
            UILabel *likeShow = nil;
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,167,43,10)] autorelease];
            likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:7.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cellstory2 addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,167,43,10)] autorelease];
            dislikeShow.textColor= [[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:7.0];
            dislikeShow.tag = lbltag;
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cellstory2 addSubview:dislikeShow];
            
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,167,43,10)] autorelease];
            commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:7.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cellstory2 addSubview:commentShow];
           
            //******************************************************************************
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            likeBtn.frame=CGRectMake(5,137,62,43);
            [likeBtn addTarget:self action:@selector(likeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cellstory2 addSubview:likeBtn];
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            dislikeBtn.frame=CGRectMake(67,137,62,43);
            [dislikeBtn addTarget:self action:@selector(dislikeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[dislikesNameArray objectAtIndex:indexPath.row]);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            [cellstory2 addSubview:likeBtn];
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cellstory2 addSubview:dislikeBtn];
            
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"Comment.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(129,137,62,43)];
            [commentButton addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory2.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"Share.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(191,137,62,43)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory2.contentView addSubview:shareButton];
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"Settings.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(253,137,62,43)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory2.contentView addSubview:settingsButton];
            
            
            UIButton *moreButtonS=nil;
            moreButtonS = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonS.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonS setBackgroundImage:more forState:UIControlStateNormal];
            moreButtonS.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [moreButtonS setFrame:CGRectMake(285,55,20,4)];
            [moreButtonS addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory2.contentView addSubview:moreButtonS];
            ce=cellstory2;
        }
        else if(([comArray count]==4)||([comArray count]==5))
        {
           
            NSLog(@"helllloooo");
            
            if (cell2 == nil)
            {
                cell2 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
                
            }
            UIView *backgroundCellView2 = [[UIView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 220)];
            backgroundCellView2.backgroundColor = [UIColor clearColor];
            UIImageView *backimage2 = [[UIImageView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 220)];
            backimage2.backgroundColor = [UIColor whiteColor];
            [backgroundCellView2 addSubview:backimage2];
            cell2.backgroundView = backgroundCellView2;
            
            for(UIView *v in [cell2.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFTweetLabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFLabelUsername class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIImageView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIButton class]])
                    [v removeFromSuperview];
            }
            
            introw=indexPath.row+1;
            tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectMake(20,12,290,20)];
            tweetLabel.numberOfLines=0;
            tweetLabel.textColor=[UIColor blackColor];
            tweetLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:12];
            NSString *smile2=[login smilyString:[dataUpdate objectAtIndex:in]];
            [tweetLabel setText:smile2];
            [tweetLabel setLinksEnabled:TRUE];
            tweetLabel.clipsToBounds=YES;
            [cell2.contentView addSubview:tweetLabel];
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,35,40,40)];
            imageView2.layer.cornerRadius = 20;
            imageView2.clipsToBounds = YES;
            [cell2.contentView addSubview:imageView2];
            
            NSString *picUrlString = [profielImages objectAtIndex: indexPath.row];
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"CachePic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData)
                        image = [UIImage imageWithData:imageData];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,35,100,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
            
            nameLabel.textColor = [UIColor clearColor];
            NSString *smile3=[differUser objectAtIndex:in];//[@"@" stringByAppendingString:[differUser objectAtIndex:in]];
            [nameLabel setText:smile3];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.clipsToBounds=YES;
            
            [cell2.contentView addSubview:nameLabel];
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,40,100,10)];
            placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cell2.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,60,200,10)];
            timeLabel.textColor=[UIColor blackColor];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentLeft;
            [cell2.contentView addSubview:timeLabel];

        
            
            NSString *smile;
            IFTweetLabel *descriptionLabel1 = [[IFTweetLabel alloc] initWithFrame:CGRectMake(20,85,278,20)];
            descriptionLabel1.numberOfLines=0;
            descriptionLabel1.textColor = [UIColor blackColor];
            descriptionLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel1.clipsToBounds=YES;

            IFTweetLabel *descriptionLabel2 = [[IFTweetLabel alloc]initWithFrame:CGRectMake(20,100,278,20)];
            descriptionLabel2.numberOfLines=0;
            descriptionLabel2.textColor = [UIColor blackColor];
            descriptionLabel2.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel2.clipsToBounds=YES;

            IFTweetLabel *descriptionLabel3 = [[IFTweetLabel alloc]initWithFrame:CGRectMake(20,115,240,20)];
            descriptionLabel3.numberOfLines=0;
            descriptionLabel3.textColor = [UIColor blackColor];
            descriptionLabel3.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel3.clipsToBounds=YES;

            IFTweetLabel *descriptionLabel4 = [[IFTweetLabel alloc]initWithFrame:CGRectMake(20,130,240,20)];
            descriptionLabel4.numberOfLines=0;
            descriptionLabel4.textColor = [UIColor blackColor];
            descriptionLabel4.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel4.clipsToBounds=YES;

            NSMutableAttributedString *attributed;
            
            attributed=[login attibutedUsername:[comArray objectAtIndex:0]];
            smile=[login smilyString:[comArray objectAtIndex:0]];
            [descriptionLabel1 setText:smile];
            [descriptionLabel1 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:1]];
            [descriptionLabel2 setText:smile];
            [descriptionLabel2 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:2]];
            [descriptionLabel3 setText:smile];
            [descriptionLabel3 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:3]];
            [descriptionLabel4 setText:smile];
            [descriptionLabel4 setLinksEnabled:TRUE];
            
            [cell2 addSubview:descriptionLabel1];
            [cell2 addSubview:descriptionLabel2];
            [cell2 addSubview:descriptionLabel3];
            [cell2 addSubview:descriptionLabel4];
            
           
            UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(10, 177, 300, 43)];
            [cell2 addSubview:buttonsBack];
            buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
            

            
            UILabel *likeShow = nil;
            
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,207,43,10)] autorelease];
            likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:7.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cell2 addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,207,43,10)] autorelease];
            dislikeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:7.0];
            dislikeShow.tag = lbltag;
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cell2 addSubview:dislikeShow];
            
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,207,43,10)] autorelease];
            commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:7.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cell2 addSubview:commentShow];
         
            //******************************************************************************
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            likeBtn.frame=CGRectMake(5,177,60,43);
            [likeBtn addTarget:self action:@selector(likeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cell2 addSubview:likeBtn];
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            dislikeBtn.frame=CGRectMake(67,177,60,43);
            [dislikeBtn addTarget:self action:@selector(dislikeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[dislikesNameArray objectAtIndex:indexPath.row]);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            [cell2 addSubview:likeBtn];
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cell2 addSubview:dislikeBtn];
            
            
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"Comment.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(129,177,60,43)];
            [commentButton addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell2.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"Share.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(191,177,60,43)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell2.contentView addSubview:shareButton];
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"Settings.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(253,177,60,43)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell2.contentView addSubview:settingsButton];
            
            UIButton *moreButtonS=nil;
            moreButtonS = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonS.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonS setBackgroundImage:more forState:UIControlStateNormal];
            moreButtonS.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [moreButtonS setFrame:CGRectMake(285,55,20,4)];
            [moreButtonS addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell2.contentView addSubview:moreButtonS];
            ce=cell2;
        }
    }
    else if([typeString isEqualToString:@"video"])
    {
        
        NSLog(@"hiiiiiiiiii videoooooo");
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)){
            NSLog(@"helllloooo");
            if (cellvideo1 == nil)
            {
                cellvideo1 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:video1] autorelease];
                NSLog(@"index path in cell3 %i",indexPath.row);
            }
            UIView *backgroundCellView2 = [[UIView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 415)];
            backgroundCellView2.backgroundColor = [UIColor clearColor];
            UIImageView *backimage2 = [[UIImageView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 415)];
            backimage2.backgroundColor = [UIColor whiteColor];
            [backgroundCellView2 addSubview:backimage2];
            cellvideo1.backgroundView = backgroundCellView2;
            
            for(UIView *v in [cellvideo1.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFTweetLabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFLabelUsername class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIImageView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIButton class]])
                    [v removeFromSuperview];
            }
            
            introw=indexPath.row+1;
            videoUrl=[dataUpdate objectAtIndex:in];
            playButton.enabled = YES;
          playButton.userInteractionEnabled=YES;
            playButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *play = [UIImage imageNamed:@"playbutton.png"];
            [playButton setImage:play forState:UIControlStateNormal];
            
            [playButton setBackgroundImage:play forState:UIControlStateNormal];
            [playButton showsTouchWhenHighlighted];
            playButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [playButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            playButton.tintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:0.8];
            
            [playButton setFrame:CGRectMake(0,0,320,285)];
            [playButton addTarget:self action:@selector(playButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            NSURL *video=[NSURL URLWithString:videoUrl];
            AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL:video options:nil];
            AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
            generate1.appliesPreferredTrackTransform = YES;
            NSError *err = NULL;
            CMTime time = CMTimeMake(1, 2);
            CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
            UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
            
            
            UIImage *croppedImage;
            
            CGRect croppedRect;
            
            croppedRect = CGRectMake(0,0, 400, 356);
            
            CGImageRef tmp = CGImageCreateWithImageInRect([one CGImage], croppedRect);
            
            croppedImage = [UIImage imageWithCGImage:tmp];
            
            CGImageRelease(tmp);
            
            
            
            NSData *CroppedImageData = UIImageJPEGRepresentation(croppedImage, 0.7);
            
            one = [UIImage imageWithData:CroppedImageData];
            
            [playButton setBackgroundImage:one forState:UIControlStateNormal];
            [playButton setContentMode:UIViewContentModeCenter];
            
            [cellvideo1.contentView addSubview:playButton];
          
            
            //get top corner coordinate of crop frame
      
            
          
            
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,290,40,40)];
            imageView2.layer.cornerRadius = 20;
            imageView2.clipsToBounds = YES;
            [cellvideo1.contentView addSubview:imageView2];
            
            NSString *picUrlString = [profielImages objectAtIndex: indexPath.row];
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"CachePic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData)
                        image = [UIImage imageWithData:imageData];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            
            [cellvideo1.contentView addSubview:imageView2];
            NSLog(@"valid");
            
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,290,200,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
            nameLabel.textColor = [UIColor clearColor];
            NSString *smile3=[differUser objectAtIndex:in];
            nameLabel.textColor = [UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            nameLabel.clipsToBounds=YES;
            [nameLabel setText:smile3];
            [nameLabel setLinksEnabled:TRUE];
            [cellvideo1.contentView addSubview:nameLabel];
            UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 372, 310, 43)];
            [cellvideo1 addSubview:buttonsBack];
            buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
            
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,295,100,10)];
            placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cellvideo1.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,320,200,10)];
            timeLabel.textColor=[[UIColor alloc] initWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:1];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentLeft;
            [cellvideo1.contentView addSubview:timeLabel];

            

            
            
            
            NSString *smile;
            IFTweetLabel *descriptionLabel1=[[IFTweetLabel alloc]initWithFrame:CGRectMake(15,345,278,20)];
            descriptionLabel1.numberOfLines=0;
            descriptionLabel1.textColor = [UIColor blackColor];
            descriptionLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel1.clipsToBounds=YES;
            NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
            NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
            if ([comArray count]==2) {
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
            }
            [cellvideo1.contentView addSubview:descriptionLabel1];
            
            
            
            
            UILabel *likeShow = nil;
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,402,43,10)] autorelease];
            likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:7.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cellvideo1 addSubview:likeShow];
            
           
            
            
            UILabel *dislikeShow = nil;
            
            UIButton *dislikeBtn = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,402,43,10)] autorelease];
            dislikeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:7.0];
            dislikeShow.tag = lbltag;
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cellvideo1 addSubview:dislikeShow];
            
            

            
            UILabel *commentShow = nil;
            NSLog(@"likes from in %@",[likearray objectAtIndex:in]);
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,402,43,10)] autorelease];
            commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:7.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cellvideo1 addSubview:commentShow];
            

            //******************************************************************************
            UIButton *likeBtn=nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            [likeBtn addTarget:self action:@selector(likeVideo:) forControlEvents:UIControlEventTouchUpInside];
            likeBtn.frame=CGRectMake(5,372,62,43);
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            NSLog(@"Confimation returned %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cellvideo1 addSubview:likeBtn];
            
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            [dislikeBtn addTarget:self action:@selector(dislikeVideo:) forControlEvents:UIControlEventTouchUpInside];
            dislikeBtn.frame=CGRectMake(67,372,62,43);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cellvideo1 addSubview:dislikeBtn];
            
            
            

            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"Comment.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(129,372,62,43)];
            [commentButton addTarget:self action:@selector(moreButtonVideoClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellvideo1.contentView addSubview:commentButton];
            
          
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
             UIImage *share = [UIImage imageNamed:@"Share.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(191,372,62,43)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellvideo1.contentView addSubview:shareButton];
            
           

            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"Settings.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(253,372,62,43)];
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            settingsButton.tag=lbltag;
            [cellvideo1.contentView addSubview:settingsButton];
            
            
            UIButton *moreButtonV=nil;
            moreButtonV = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonV.tag = indexPath.row+1;
              UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonV setBackgroundImage:more forState:UIControlStateNormal];
            
             [moreButtonV setFrame:CGRectMake(285,310,20,4)];
            [moreButtonV addTarget:self action:@selector(moreButtonVideoClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellvideo1.contentView addSubview:moreButtonV];
            
         

            
            ce=cellvideo1;
            
        }
        else if([comArray count]==3)
        {
            
            if (cellvideo2 == nil)
            {
                cellvideo2 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:video2] autorelease];
                NSLog(@"index path in cell3 %i",indexPath.row);
            }
            CGFloat x = [UIScreen mainScreen].bounds.origin.x;
            UIView *backgroundCellView = [[UIView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 415)];
            backgroundCellView.backgroundColor = [UIColor clearColor];
            UIImageView *backimage = [[UIImageView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 415)];
            backimage.backgroundColor = [UIColor whiteColor];
            [backgroundCellView addSubview:backimage];
            cellvideo2.backgroundView = backgroundCellView;
            
            for(UIView *v in [cellvideo2.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFTweetLabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFLabelUsername class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIImageView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIButton class]])
                    [v removeFromSuperview];
            }
            
            introw=indexPath.row+1;
            videoUrl=[dataUpdate objectAtIndex:in];
            playButton.enabled = YES;
            playButton.userInteractionEnabled=YES;
            playButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *play = [UIImage imageNamed:@"playbutton.png"];
            [playButton setImage:play forState:UIControlStateNormal];
            
            [playButton setBackgroundImage:play forState:UIControlStateNormal];
            [playButton showsTouchWhenHighlighted];
            playButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [playButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            playButton.tintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:0.8];
            
            [playButton setFrame:CGRectMake(0,0,320,285)];
            [playButton addTarget:self action:@selector(playButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            NSURL *video=[NSURL URLWithString:videoUrl];
            AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL:video options:nil];
            AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
            generate1.appliesPreferredTrackTransform = YES;
            NSError *err = NULL;
            CMTime time = CMTimeMake(1, 2);
            CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
            UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
            
            
            UIImage *croppedImage;
            
            CGRect croppedRect;
            
            croppedRect = CGRectMake(0,0, 400, 356);
            
            CGImageRef tmp = CGImageCreateWithImageInRect([one CGImage], croppedRect);
            
            croppedImage = [UIImage imageWithCGImage:tmp];
            
            CGImageRelease(tmp);
            
            
            
            NSData *CroppedImageData = UIImageJPEGRepresentation(croppedImage, 0.7);
            
            one = [UIImage imageWithData:CroppedImageData];
            
            [playButton setBackgroundImage:one forState:UIControlStateNormal];
            [playButton setContentMode:UIViewContentModeCenter];
            [cellvideo2.contentView addSubview:playButton];
            
            //int lbltag =indexPath.row;
            
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,290,40,40)];
            imageView2.layer.cornerRadius = 20;
            imageView2.clipsToBounds = YES;
            [cellvideo2.contentView addSubview:imageView2];
            
            NSString *picUrlString = [profielImages objectAtIndex: indexPath.row];
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"CachePic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData)
                        image = [UIImage imageWithData:imageData];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            
            [cellvideo2.contentView addSubview:imageView2];
            NSLog(@"valid");
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,290,200,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
            nameLabel.textColor = [UIColor clearColor];
            NSString *smile2=[differUser objectAtIndex:in];
            nameLabel.textColor = [UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            [nameLabel setText:smile2];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.clipsToBounds=YES;
            [cellvideo2.contentView addSubview:nameLabel];
            UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 407, 310, 43)];
            [cellvideo2 addSubview:buttonsBack];
            buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
            
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,295,100,10)];
            placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cellvideo2.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,320,200,10)];
            timeLabel.textColor=[[UIColor alloc] initWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:1];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentLeft;
            [cellvideo2.contentView addSubview:timeLabel];
            
            NSString *smile;
            
            IFTweetLabel *descriptionLabel1=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,345,280,20)];
            descriptionLabel1.numberOfLines=0;
            descriptionLabel1.textColor = [UIColor blackColor];
            descriptionLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel1.clipsToBounds=YES;
            IFTweetLabel *descriptionLabel2=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,360,280,20)];
            descriptionLabel2.numberOfLines=0;
            descriptionLabel2.textColor = [UIColor blackColor];
            descriptionLabel2.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel2.clipsToBounds=YES;

            smile=[login smilyString:[comArray objectAtIndex:0]];
            [descriptionLabel1 setText:smile];
            [descriptionLabel1 setLinksEnabled:TRUE];
            
            smile=[login smilyString:[comArray objectAtIndex:1]];
            [descriptionLabel2 setText:smile];
            [descriptionLabel2 setLinksEnabled:TRUE];
            
            
            [cellvideo2.contentView addSubview:descriptionLabel1];
            [cellvideo2.contentView addSubview:descriptionLabel2];
            
            
            UILabel *likeShow = nil;
            
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,437,43,10)] autorelease];
            likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:7.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cellvideo2 addSubview:likeShow];
            
            
            UILabel *dislikeShow = nil;
            UIButton *dislikeBtn = nil;
            
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,437,43,10)] autorelease];
            dislikeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:7.0];
            dislikeShow.tag = lbltag;
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cellvideo2 addSubview:dislikeShow];
            
            UILabel *commentShow = nil;
            NSLog(@"likes from in %@",[likearray objectAtIndex:in]);
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,437,43,10)] autorelease];
            commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:7.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cellvideo2 addSubview:commentShow];
            //******************************************************************************
            UIButton *likeBtn=nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            [likeBtn addTarget:self action:@selector(likeVideo:) forControlEvents:UIControlEventTouchUpInside];
            likeBtn.frame=CGRectMake(5,407,62,43);
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            NSLog(@"Confimation returned %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cellvideo2 addSubview:likeBtn];
            
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            [dislikeBtn addTarget:self action:@selector(dislikeVideo:) forControlEvents:UIControlEventTouchUpInside];
            dislikeBtn.frame=CGRectMake(67,407,62,43);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cellvideo2 addSubview:dislikeBtn];
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"commentbutton2.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(129,407,62,43)];
            [commentButton addTarget:self action:@selector(moreButtonVideoClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellvideo2.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"share_button2.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(191,407,62,43)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellvideo2.contentView addSubview:shareButton];
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"bottom-setting2.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(253,407,62,43)];
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            settingsButton.tag=lbltag;
            [cellvideo2.contentView addSubview:settingsButton];
            
            
            UIButton *moreButtonV=nil;
            moreButtonV = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonV.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonV setBackgroundImage:more forState:UIControlStateNormal];
            
            [moreButtonV setFrame:CGRectMake(285,310,20,4)];
            [moreButtonV addTarget:self action:@selector(moreButtonVideoClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell3.contentView addSubview:moreButtonV];
            
            ce=cellvideo2;
            
            
        }
        else if(([comArray count]==4)||([comArray count]==5))
        {
            if (cell3 == nil)
            {
                cell3 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier3] autorelease];
                NSLog(@"index path in cell3 %i",indexPath.row);
            }
            CGFloat x = [UIScreen mainScreen].bounds.origin.x;
             cell3.backgroundColor = [UIColor clearColor];
            UIView *backgroundCellView2 = [[UIView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 460)];
            backgroundCellView2.backgroundColor = [UIColor clearColor];
            UIImageView *backimage2 = [[UIImageView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 460)];
            backimage2.backgroundColor = [UIColor whiteColor];
            [backgroundCellView2 addSubview:backimage2];
            cell3.backgroundView = backgroundCellView2;
            
            for(UIView *v in [cell3.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFTweetLabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFLabelUsername class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIImageView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIButton class]])
                    [v removeFromSuperview];
            }
            
            introw=indexPath.row+1;
            videoUrl=[dataUpdate objectAtIndex:in];
            videoUrl=[dataUpdate objectAtIndex:in];
            playButton.enabled = YES;
            playButton.userInteractionEnabled=YES;
            playButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *play = [UIImage imageNamed:@"playbutton.png"];
            [playButton setImage:play forState:UIControlStateNormal];
            
            [playButton setBackgroundImage:play forState:UIControlStateNormal];
            [playButton showsTouchWhenHighlighted];
            playButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [playButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            playButton.tintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:0.8];
            
            [playButton setFrame:CGRectMake(0,0,320,285)];
            [playButton addTarget:self action:@selector(playButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            NSURL *video=[NSURL URLWithString:videoUrl];
            AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL:video options:nil];
            AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
            generate1.appliesPreferredTrackTransform = YES;
            NSError *err = NULL;
            CMTime time = CMTimeMake(1, 2);
            CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
            UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
            
            
            UIImage *croppedImage;
            
            CGRect croppedRect;
            
            croppedRect = CGRectMake(0,0, 400, 356);
            
            CGImageRef tmp = CGImageCreateWithImageInRect([one CGImage], croppedRect);
            
            croppedImage = [UIImage imageWithCGImage:tmp];
            
            CGImageRelease(tmp);
            
            
            
            NSData *CroppedImageData = UIImageJPEGRepresentation(croppedImage, 0.7);
            
            one = [UIImage imageWithData:CroppedImageData];
            
            [playButton setBackgroundImage:one forState:UIControlStateNormal];
            [playButton setContentMode:UIViewContentModeCenter];
            [cell3.contentView addSubview:playButton];
            //int lbltag =indexPath.row;
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,290,40,40)];
            imageView2.layer.cornerRadius = 20;
            imageView2.clipsToBounds = YES;
            [cell3.contentView addSubview:imageView2];
            
            NSString *picUrlString = [profielImages objectAtIndex: indexPath.row];
            UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
            
            if (cachedPic)
            {
                //imgCell.imageView.image = cachedImage;
                imageView2.image = cachedPic;
                
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                imageView2.image = [UIImage imageNamed:@"CachePic.png"];
                [self.imageDownloadingQueuePic addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData)
                        image = [UIImage imageWithData:imageData];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCachePic setObject:image forKey:picUrlString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                imageView2.image = image;
                            
                        }];
                    }
                }];
            }
            
            [cell3.contentView addSubview:imageView2];
            NSLog(@"valid");
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,290,200,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
            nameLabel.textColor = [UIColor clearColor];
            NSString *smile2=[differUser objectAtIndex:in];
            nameLabel.textColor = [UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
            [nameLabel setText:smile2];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.clipsToBounds=YES;
            [cell3.contentView addSubview:nameLabel];
            UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 417, 310, 43)];
            [cell3 addSubview:buttonsBack];
            buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
            
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,295,100,10)];
            placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cell3.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,320,200,10)];
            timeLabel.textColor=[[UIColor alloc] initWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:1];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentLeft;
            [cell3.contentView addSubview:timeLabel];
            
            NSString *smile;
            
            IFTweetLabel *descriptionLabel1=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,345,280,20)];
            descriptionLabel1.numberOfLines=0;
            descriptionLabel1.textColor = [UIColor blackColor];
            descriptionLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel1.clipsToBounds=YES;
            IFTweetLabel *descriptionLabel2=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,360,280,20)];
            descriptionLabel2.numberOfLines=0;
            descriptionLabel2.textColor = [UIColor blackColor];
            descriptionLabel2.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel2.clipsToBounds=YES;
            IFTweetLabel *descriptionLabel3=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,375,280,20)];
            descriptionLabel3.numberOfLines=0;
            descriptionLabel3.textColor = [UIColor blackColor];
            descriptionLabel3.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel3.clipsToBounds=YES;
            
            IFTweetLabel *descriptionLabel4=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,390,280,20)];
            descriptionLabel4.numberOfLines=0;
            descriptionLabel4.textColor = [UIColor blackColor];
            descriptionLabel4.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel4.clipsToBounds=YES;
            
            NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
            NSMutableAttributedString *attributed;
            
            NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
            if ([comArray count]==2) {
                // descriptionLabel1.text=@"Only one comment add your comment....";
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
                
            }
            else if ([comArray count]==3) {
                attributed=[login attibutedUsername:[comArray objectAtIndex:0]];
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
                attributed=[login attibutedUsername:[comArray objectAtIndex:1]];
                smile=[login smilyString:[comArray objectAtIndex:1]];
                [descriptionLabel2 setText:smile];
                [descriptionLabel2 setLinksEnabled:TRUE];
            }
            else if ([comArray count]==4) {
                attributed=[login attibutedUsername:[comArray objectAtIndex:0]];
                // descriptionLabel1.attributedText=attributed;
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
                smile=[login smilyString:[comArray objectAtIndex:1]];
                [descriptionLabel2 setText:smile];
                [descriptionLabel2 setLinksEnabled:TRUE];
                smile=[login smilyString:[comArray objectAtIndex:2]];
                [descriptionLabel3 setText:smile];
                [descriptionLabel3 setLinksEnabled:TRUE];
            }
            else if ([comArray count]==5) {
                attributed=[login attibutedUsername:[comArray objectAtIndex:0]];
                //  descriptionLabel1.attributedText=attributed;
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
                smile=[login smilyString:[comArray objectAtIndex:1]];
                [descriptionLabel2 setText:smile];
                [descriptionLabel2 setLinksEnabled:TRUE];
                smile=[login smilyString:[comArray objectAtIndex:2]];
                [descriptionLabel3 setText:smile];
                [descriptionLabel3 setLinksEnabled:TRUE];
                smile=[login smilyString:[comArray objectAtIndex:3]];
                [descriptionLabel4 setText:smile];
                [descriptionLabel4 setLinksEnabled:TRUE];
            }
            
            [cell3.contentView addSubview:descriptionLabel1];
            [cell3.contentView addSubview:descriptionLabel2];
            [cell3.contentView addSubview:descriptionLabel3];
            [cell3.contentView addSubview:descriptionLabel4];
            
            UILabel *likeShow = nil;
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,447,43,10)] autorelease];
            likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:7.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cell3 addSubview:likeShow];
            
            
            UILabel *dislikeShow = nil;
            UIButton *dislikeBtn = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,447,43,10)] autorelease];
            dislikeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:7.0];
            dislikeShow.tag = lbltag;
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cell3 addSubview:dislikeShow];
            
            UILabel *commentShow = nil;
            NSLog(@"likes from in %@",[likearray objectAtIndex:in]);
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,447,43,10)] autorelease];
            commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:7.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cell3 addSubview:commentShow];
            //******************************************************************************
            UIButton *likeBtn=nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            [likeBtn addTarget:self action:@selector(likeVideo:) forControlEvents:UIControlEventTouchUpInside];
            likeBtn.frame=CGRectMake(5,417,62,43);
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            NSLog(@"Confimation returned %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cell3 addSubview:likeBtn];
            
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            [dislikeBtn addTarget:self action:@selector(dislikeVideo:) forControlEvents:UIControlEventTouchUpInside];
            dislikeBtn.frame=CGRectMake(67,417,62,43);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cell3 addSubview:dislikeBtn];
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"Comment.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(129,417,62,43)];
            [commentButton addTarget:self action:@selector(moreButtonVideoClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell3.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"Share.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(191,417,62,43)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell3.contentView addSubview:shareButton];
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"Settings.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(253,417,62,43)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell3.contentView addSubview:settingsButton];
            
            
            UIButton *moreButtonV=nil;
            moreButtonV = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonV.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonV setBackgroundImage:more forState:UIControlStateNormal];
            
            [moreButtonV setFrame:CGRectMake(285,310,20,4)];
            [moreButtonV addTarget:self action:@selector(moreButtonVideoClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell3.contentView addSubview:moreButtonV];
            
            ce=cell3;
        }
    }
    else if ([typeString isEqualToString:@"more"]) {
		
		cell4 = [tableView dequeueReusableCellWithIdentifier:moreCellId];
		if (cell4 == nil) {
			cell4 = [[[UITableViewCell alloc]
                      initWithStyle:UITableViewCellStyleDefault
                      reuseIdentifier:moreCellId] autorelease];
		}
        UIActivityIndicatorView * activityindicator1 = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 5, 30, 30)];
        [activityindicator1 setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
        [activityindicator1 setColor:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1]];
        [cell4 addSubview:activityindicator1];
        activityindicator1.backgroundColor = [UIColor clearColor];
        [activityindicator1 startAnimating];
		//cell4.textLabel.text =[dataUpdate objectAtIndex:indexPath.row];
		cell4.textLabel.textColor = [UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1];
		cell4.textLabel.font = [UIFont boldSystemFontOfSize:14];
        cell4.backgroundColor = [UIColor clearColor];
		ce=cell4;
		
	}
    
    return ce;
    
    
}
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // [self refreshData];
}
- (void)tableView:(UITableView *)tableView didEndReorderingRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@" ************ last row dragged try ************* ");
    if(indexPath.row == [updatesAarray count])
    {
        [self refreshData];
    }
}
-(IBAction)settingButtonClicked:(id)sender
{
    self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:3];
    
}

//***********************************Like Methods****************************************************
-(void)likeFn:(UIButton*)btnClicked
{
    if([[likesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/liked.do?sno=%@&type=story&username=%@&action=liked",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        int likeCount=[[json valueForKey:@"count"] intValue];
        [likearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [likesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        [mytable reloadData];
    }
    
}
-(void)likeImage:(UIButton*)btnClicked
{
    if([[likesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/liked.do?sno=%@&type=images&username=%@&action=liked",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        int likeCount=[[json valueForKey:@"count"] intValue];
        [likearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [likesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        NSLog(@"changed value of like %@",[likearray objectAtIndex:btnClicked.tag-1]);
        [mytable reloadData];
    }
}
-(void)likeVideo:(UIButton*)btnClicked
{
    if([[likesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/liked.do?sno=%@&type=video&username=%@&action=liked",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        int likeCount=[[json valueForKey:@"count"] intValue];
        // NSString *strLikes = [likearray objectAtIndex:btnClicked.tag-1];
        //int likeCount = [strLikes intValue] + 1;
        [likearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [likesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        NSLog(@"changed value of like %@",[likearray objectAtIndex:btnClicked.tag-1]);
        [mytable reloadData];
        
    }
}

//***********************************DisLike Methods*********************************************
-(void)dislikeFn:(UIButton*)btnClicked
{
    if([[dislikesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/liked.do?sno=%@&type=story&username=%@&action=dislike",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        int likeCount=[[json valueForKey:@"count"] intValue];
        [dislikearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [dislikesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        [mytable reloadData];
        
    }
}
-(void)dislikeImage:(UIButton*)btnClicked
{
    if([[dislikesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/liked.do?sno=%@&type=images&username=%@&action=dislike",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        int likeCount=[[json valueForKey:@"count"] intValue];
        [dislikearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [dislikesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        [mytable reloadData];
    }
}
-(void)dislikeVideo:(UIButton*)btnClicked
{
    if([[dislikesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/liked.do?sno=%@&type=video&username=%@&action=dislike",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        int likeCount=[[json valueForKey:@"count"] intValue];
        [dislikearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [dislikesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        [mytable reloadData];
    }
}

//********************* SEE MORE Mehthods ****************************************
-(IBAction)moreButtonImageClicked:(UIButton*)btnClicked
{
    
    
    imageDetailView *imageDetail=[self.storyboard instantiateViewControllerWithIdentifier:@"imageDetail"];
    imageDetail.hidesBottomBarWhenPushed = YES;
    NSString *UrlString=[dataUpdate objectAtIndex:btnClicked.tag-1];
    imageDetail.getImage=[self.imageCache objectForKey:UrlString];
    NSString *picUrlString=[profielImages objectAtIndex:btnClicked.tag-1];
    imageDetail.getUserImage=[self.imageCachePic objectForKey:picUrlString];
    NSLog(@"image srting %@",[dataUpdate objectAtIndex:btnClicked.tag-1]);
    imageDetail.getImageData=[dataUpdate objectAtIndex:btnClicked.tag-1];
    
    imageDetail.getUserImageData=[profielImages objectAtIndex:btnClicked.tag-1];
    imageDetail.getName=[differUser objectAtIndex:btnClicked.tag-1];
    imageDetail.getSno=[snoArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getIfLikes=[likesNameArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getLikes=[likearray objectAtIndex:btnClicked.tag-1];
    imageDetail.getIfDisLikes=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getDisLikes=[dislikearray objectAtIndex:btnClicked.tag-1];
    imageDetail.getComments=[commentCounts objectAtIndex:btnClicked.tag-1];
    imageDetail.getType=[dataType objectAtIndex:btnClicked.tag-1];
    imageDetail.getDislikeConferm=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getLikeConferm=[likesNameArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getLocation=[locationArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getTime=[timeArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getRowIndex=[NSString stringWithFormat:@"%i",btnClicked.tag-1];
    imageDetail.getTabId=@"circle";

        
    
    [self.navigationController pushViewController:imageDetail animated:YES];
  
    
   
    
}

-(IBAction)moreButtonStoryClicked:(UIButton*)btnClicked
{
    NSLog(@"Content Type %@",[dataType objectAtIndex:btnClicked.tag-1]);
    NSString *setStoryString = [dataUpdate objectAtIndex:btnClicked.tag-1];
    
    storyDetailView *storyDetail=[self.storyboard instantiateViewControllerWithIdentifier:@"storyDetail"];
    storyDetail.hidesBottomBarWhenPushed = YES;
    storyDetail.getStory=setStoryString;
    storyDetail.getUserImageData=[profielImages objectAtIndex:btnClicked.tag-1];
    storyDetail.getName=[differUser objectAtIndex:btnClicked.tag-1];
    storyDetail.getSno=[snoArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getIfLikes=[likesNameArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getLikes=[likearray objectAtIndex:btnClicked.tag-1];
    storyDetail.getIfDisLikes=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getDisLikes=[dislikearray objectAtIndex:btnClicked.tag-1];
    storyDetail.getComments=[commentCounts objectAtIndex:btnClicked.tag-1];
    storyDetail.getType=[dataType objectAtIndex:btnClicked.tag-1];
    storyDetail.getDislikeConferm=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getLikeConferm=[likesNameArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getLocation=[locationArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getTime=[timeArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getRowIndex=[NSString stringWithFormat:@"%i",btnClicked.tag-1];
    storyDetail.getTabId=@"circle";
    [self.navigationController pushViewController:storyDetail animated:YES];
}
-(IBAction)moreButtonVideoClicked:(UIButton*)btnClicked
{
    NSLog(@"Content Type %@",[dataType objectAtIndex:btnClicked.tag-1]);
    //NSString *setStoryString = [dataUpdate objectAtIndex:btnClicked.tag-1];
    
    videoDetailView *videoDetail=[self.storyboard instantiateViewControllerWithIdentifier:@"videoDetail"];
    videoDetail.hidesBottomBarWhenPushed = YES;
    videoDetail.videoUrl=videoUrl;
    videoDetail.getUserImageData=[profielImages objectAtIndex:btnClicked.tag-1];
    videoDetail.getName=[differUser objectAtIndex:btnClicked.tag-1];
    videoDetail.getSno=[snoArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getIfLikes=[likesNameArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getLikes=[likearray objectAtIndex:btnClicked.tag-1];
    videoDetail.getIfDisLikes=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getDisLikes=[dislikearray objectAtIndex:btnClicked.tag-1];
    videoDetail.getComments=[commentCounts objectAtIndex:btnClicked.tag-1];
    videoDetail.getType=[dataType objectAtIndex:btnClicked.tag-1];
    videoDetail.getDislikeConferm=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getLikeConferm=[likesNameArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getLocation=[locationArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getTime=[timeArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getRowIndex=[NSString stringWithFormat:@"%i",btnClicked.tag-1];
    videoDetail.getTabId=@"circle";
    [self.navigationController pushViewController:videoDetail animated:YES];
}
//-(IBAction)userButtonVideoClicked:(UILabel*)btnClicked
-(void)userButtonVideoClicked:(NSString *)searchName
{
    if ([searchName isEqualToString:getusername]) {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
    }
    else
    {
        NSLog(@"userbutton clicked");
        searchedUserView *searchedUser=[self.storyboard instantiateViewControllerWithIdentifier:@"searchedUser"];
        NSString *url = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/userinfo.do?username=%@",searchName];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSURLRequest *req=[[NSURLRequest alloc]initWithURL:urlrequest];
        NSURLResponse  *response = nil;
        NSError* error;
        [NSURLConnection sendSynchronousRequest: req returningResponse: &response error: &error];
        
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        // NSString *stringData= [NSString stringWithContentsOfURL:urlrequest encoding:nil error:nil];
        // NSLog(@"Recieved data String &&&&&&&&&&&& %@",stringData);
        
        NSData *dataofStrig=[NSData dataWithContentsOfURL:urlrequest];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions error:&error];
        if (!json) {
            NSLog(@"Not Json");
        }
        
        NSLog(@"JSON format:- %@",json);
        searchedUser.getfollowers=[json objectForKey:@"followerCount"];
        searchedUser.getfollowing=[json objectForKey:@"followingCount"];
        NSString *userName=[json objectForKey:@"username"];
        searchedUser.getUserSearched=userName;
        NSString *status=[json objectForKey:@"status"];
        searchedUser.getStatusSearched=status;
        NSString *userPic=[json objectForKey:@"profile_image"];
        searchedUser.getImageSearched=userPic;
        searchedUser.getId=@"circle";
        [self.navigationController pushViewController:searchedUser animated:YES];
    }
}


//**********************************************************************************************
- (void)reloadRowsAtIndexPaths:(NSArray *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation
{
    NSLog(@"cell reloaded");
}

-(void)shareButtonClicked:(UIButton*)btnClicked{
    NSLog(@"data type %@",[dataType objectAtIndex:btnClicked.tag-1]);
    NSString *type=[dataType objectAtIndex:btnClicked.tag-1];
    NSLog(@"Helllooo activity viewcontroller");
    
    NSArray *activityItems = NO;
    if([type isEqualToString:@"image"])
    {
        NSURL *Url=[[NSURL alloc]initWithString:[dataUpdate objectAtIndex:btnClicked.tag-1]];
        NSData *ImageData=[NSData dataWithContentsOfURL:Url];
        UIImage *image=[UIImage imageWithData:ImageData];
        if (image != nil) {
            activityItems = @[@"Post from cancer circle iPhone app",image];
        }
        
        else {
            activityItems = @[[dataUpdate objectAtIndex:btnClicked.tag-1]];
        }
    }
    else if([type isEqualToString:@"story"]){
        activityItems = @[[dataUpdate objectAtIndex:btnClicked.tag-1]];
    }
    else if([type isEqualToString:@"video"]){
        // NSString *myString = [[dataUpdate objectAtIndex:btnClicked.tag-1] absoluteString];
        //NSLog(@"nsurl String %@",myString);
        activityItems = @[[dataUpdate objectAtIndex:btnClicked.tag-1]];
    }
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewController.excludedActivityTypes = @[UIActivityTypePostToWeibo, UIActivityTypeAssignToContact ];
    
    [self presentViewController:activityViewController animated:YES completion:nil];  }

-(IBAction)reportButtonClicked:(UIButton*)btnClicked
{
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    delegate.snoReport=[snoArray objectAtIndex:btnClicked.tag-1];
    delegate.typeReport=[dataType objectAtIndex:btnClicked.tag-1];
    actionSheet = [[UIActionSheet alloc] initWithTitle:@"Report for Content" delegate:self
                                     cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Inappropriate"
                                     otherButtonTitles:@"Unfollow user",nil];
    actionSheet.backgroundColor = [UIColor clearColor];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    //[actionSheet showInView:self.view];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}
-(void)actionSheet:(UIActionSheet *)actionSheet2 clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == [actionSheet2 destructiveButtonIndex]) {
        [self performSegueWithIdentifier:@"circleToReport" sender:self];
    } else if(buttonIndex == 1){
        
        NSString *followUrl=[NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/unfollowing.do?username=%@&following=%@",getusername,[differUser objectAtIndex:introw-2]];
        NSURL *furl=[NSURL URLWithString:followUrl];
        NSLog(@"Unfolloeing user url %@",furl);
        NSError* error;
        NSData *Data=[NSData dataWithContentsOfURL:furl];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:Data options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        
    }
}
-(IBAction)playButtonClicked:(id)sender
{
    NSLog(@"VIDEO URL IN PLAY BUTTON %@",videoUrl);
    NSURL *video=[NSURL URLWithString:videoUrl];
    //NSURL *url = [NSURL URLWithString:videoUrl];
    _movieplayer = [[MPMoviePlayerController alloc] initWithContentURL:video];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:_movieplayer];
    
    _movieplayer.controlStyle = MPMovieControlStyleDefault;
    _movieplayer.shouldAutoplay = YES;
    [self.view addSubview:_movieplayer.view];
    [_movieplayer setFullscreen:YES animated:YES];
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    MPMoviePlayerController *player = [notification object];
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:player];
    
    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
}
-(void)circleData
{
    Login *login=[[Login alloc]init];
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    NSLog(@"datao time inserted at Circle %@",delegate.getInertedTime);
    NSString *converted = [delegate.getInertedTime stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/pull.do?old_time=%@",converted];
    // NSString *url = [NSString stringWithFormat:@"http://192.168.10.53:8080/Cancer_final/login.do?method=myAction1&username=%@&password=%@",getuser,getuserpass];
    NSURL *urlrequest=[NSURL URLWithString:url];
    NSURLRequest *req=[[NSURLRequest alloc]initWithURL:urlrequest];
    NSURLResponse  *response = nil;
    NSError* error;
    [NSURLConnection sendSynchronousRequest: req returningResponse: &response error: &error];
    
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    NSString *stringData= [NSString stringWithContentsOfURL:urlrequest encoding:Nil error:nil];
    NSLog(@"Recieved data String &&&&&&&&&&&& %@",stringData);
    
    //NSData *dataofStrig=[NSData dataWithContentsOfFile:stringData];
    NSData * dataofStrig = [stringData dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"Recieved data Data &&&&&&&&&&&& %@",stringData);
    if (!dataofStrig) {
        NSLog(@"No Data");
    }
    else{
        
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions                        error:&error];
        
        NSLog(@"JSON format:- %@",json);
        NSLog(@"time inserted%@",[[json objectForKey:@"data0"] valueForKey:@"inserted_at"]);
        NSLog(@"time delegate inserted%@",delegate.getInertedTime);
        /*if([delegate.getInertedTime isEqualToString:[delegate.insertedTimeArray objectAtIndex:0]])
         {
         NSLog(@"delegate.getInertedTime %@",delegate.getInertedTime);
         NSLog(@"[delegate.insertedTimeArray objectAtIndex:0] %@",[delegate.insertedTimeArray objectAtIndex:0]);
         }
         else{*/
        
        int length=[json count];
        NSLog(@"JSON count:- %i",length);
        if (length>5) {
            NSString *indexStr = [NSString stringWithFormat:@"%d",(length-6)];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            delegate.getInertedTime=[[json objectForKey:data] valueForKey:@"inserted_at"];
        }
        for(int i=0;i<(length-5);i++)    {
            
            NSString *indexStr = [NSString stringWithFormat:@"%d",i];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            [commentCounts insertObject:[[json objectForKey:data]valueForKey:@"comment"] atIndex:0];
            NSString *likesName=[[json objectForKey:data] valueForKey:@"likenames"];
            NSString *confrm=[login likesContent:likesName];
            [likesNameArray insertObject:confrm atIndex:0];
            NSString *dislikesName=[[json objectForKey:data] valueForKey:@"dislikenames"];
            NSString *disconfrm=[login likesContent:dislikesName];
            [dislikesNameArray insertObject:disconfrm atIndex:0];
            dataStr=[[json valueForKey:data] valueForKey:@"content"];
            NSString *user=[[json valueForKey:data] valueForKey:@"username"];
            [differUser insertObject:user atIndex:0];
            NSString *sno=[[json valueForKey:data]valueForKey:@"sno"];
            [snoArray insertObject:sno atIndex:0];
            NSString *likeString = [[json valueForKey:data]valueForKey:@"like"];
            [likearray insertObject:likeString atIndex:0];
            [insertedTimeArray insertObject:[[json objectForKey:data] valueForKey:@"inserted_at"] atIndex:0];
            NSString *dislikeString = [[json valueForKey:data]valueForKey:@"dislike"];
            NSLog(@"value of like string is:- %@",dislikeString);
            [dislikearray insertObject:dislikeString atIndex:0];
            if ([[[json objectForKey:data] valueForKey:@"Elapsed"] isEqualToString:@""]) {
                [timeArray insertObject:@"Not Updated" atIndex:0];
            }
            else
            {
                [timeArray insertObject:[[json objectForKey:data] valueForKey:@"Elapsed"] atIndex:0];
            }
            if([[[json objectForKey:data] valueForKey:@"location"] isEqualToString:@""])
            {
                [locationArray insertObject:@"Not Updated" atIndex:0];
            }
            else
            {
                [locationArray insertObject:[[json objectForKey:data] valueForKey:@"location"] atIndex:0];
            }
            
            NSString *userDP=[[json valueForKey:data]valueForKey:@"profile_image"];
            
            [profielImages insertObject:userDP atIndex:0];
            
            NSString *type=[[json objectForKey:data]valueForKey:@"type"];
            
            [dataType insertObject:type atIndex:0];
            [dataUpdate insertObject:dataStr atIndex:0];
            NSString *fourcomment=[[json objectForKey:data]valueForKey:@"commentiphone"];
            NSLog(@"fourcomment %@",fourcomment);
            if ([fourcomment length]==0) {
                [fourCommentArray insertObject:@"No Comments" atIndex:0];
            }
            else{
                [fourCommentArray insertObject:fourcomment atIndex:0];
            }
        }
        
    }
    // }
    delegate.dataUpdate=dataUpdate;
    delegate.dataType=dataType;
    delegate.snoArray=snoArray;
    delegate.likearray=likearray;
    delegate.dislikearray=dislikearray;
    delegate.differUser=differUser;
    delegate.likesNameArray=likesNameArray;
    delegate.dislikesNameArray=dislikesNameArray;
    delegate.profielImages=profielImages;
    delegate.commentCounts=commentCounts;
    delegate.locationArray=locationArray;
    delegate.timeArray=timeArray;
    delegate.fourCommentArray=fourCommentArray;
    [mytable reloadData];
}
-(void)refreshcircleData
{
    Login *login=[[Login alloc]init];
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    NSLog(@"datao time inserted at Circle %@",delegate.getLastInertedTime);
    NSString *converted = [delegate.getLastInertedTime stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/refresh.do?old_time=%@",converted];
    NSURL *urlrequest=[NSURL URLWithString:url];
    NSURLRequest *req=[[NSURLRequest alloc]initWithURL:urlrequest];
    NSURLResponse  *response = nil;
    NSError* error;
    [NSURLConnection sendSynchronousRequest: req returningResponse: &response error: &error];
    
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    NSString *stringData= [NSString stringWithContentsOfURL:urlrequest encoding:nil error:nil];
    NSLog(@"Recieved data String &&&&&&&&&&&& %@",stringData);
    
    //NSData *dataofStrig=[NSData dataWithContentsOfFile:stringData];
    NSData * dataofStrig = [stringData dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"Recieved data Data &&&&&&&&&&&& %@",stringData);
    if (!dataofStrig) {
        NSLog(@"No Data");
    }
    else{
        
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions                        error:&error];
        
        NSLog(@"JSON format:- %@",json);
        if([delegate.getLastInertedTime isEqualToString:[[json objectForKey:@"data0"] valueForKey:@"inserted_at"]])
        {
            
        }
        else{
            
            int length=[json count];
            NSLog(@"JSON count:- %i",length);
            if (length>5) {
                NSString *indexStr = [NSString stringWithFormat:@"%d",(length-6)];
                NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
                delegate.getLastInertedTime=[[json objectForKey:data] valueForKey:@"inserted_at"];
            }
            for(int i=0;i<(length-5);i++)    {
                
                NSString *indexStr = [NSString stringWithFormat:@"%d",i];
                NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
                [commentCounts insertObject:[[json objectForKey:data]valueForKey:@"comment"] atIndex:[commentCounts count]-1];
                NSString *likesName=[[json objectForKey:data] valueForKey:@"likenames"];
                NSString *confrm=[login likesContent:likesName];
                [likesNameArray insertObject:confrm atIndex:[likesNameArray count]-1];
                NSString *dislikesName=[[json objectForKey:data] valueForKey:@"dislikenames"];
                NSString *disconfrm=[login likesContent:dislikesName];
                [dislikesNameArray insertObject:disconfrm atIndex:[dislikesNameArray count]-1];
                dataStr=[[json valueForKey:data] valueForKey:@"content"];
                NSString *user=[[json valueForKey:data] valueForKey:@"username"];
                [differUser insertObject:user atIndex:[differUser count]-1];
                NSString *sno=[[json valueForKey:data]valueForKey:@"sno"];
                [snoArray insertObject:sno atIndex:[snoArray count]-1];
                NSString *likeString = [[json valueForKey:data]valueForKey:@"like"];
                [likearray insertObject:likeString atIndex:[likearray count]-1];
                NSString *dislikeString = [[json valueForKey:data]valueForKey:@"dislike"];
                [dislikearray insertObject:dislikeString atIndex:[dislikearray count]-1];
                if ([[[json objectForKey:data] valueForKey:@"Elapsed"] isEqualToString:@""]) {
                    [timeArray insertObject:@"Not Updated" atIndex:timeArray.count-1];
                }
                else
                {
                    [timeArray insertObject:[[json objectForKey:data] valueForKey:@"Elapsed"] atIndex:timeArray.count-1];
                }
                if([[[json objectForKey:data] valueForKey:@"location"] isEqualToString:@""])
                {
                    [locationArray insertObject:@"Not Updated" atIndex:locationArray.count-1];
                }
                else
                {
                    [locationArray insertObject:[[json objectForKey:data] valueForKey:@"location"] atIndex:locationArray.count-1];
                }
                
                NSString *userDP=[[json valueForKey:data]valueForKey:@"profile_image"];
                [profielImages insertObject:userDP atIndex:profielImages.count-1];
                
                NSString *type=[[json objectForKey:data]valueForKey:@"type"];
                [dataType insertObject:type atIndex:dataType.count-1];
                [dataUpdate insertObject:dataStr atIndex:dataUpdate.count-1];
                NSString *fourcomment=[[json objectForKey:data]valueForKey:@"commentiphone"];
                NSLog(@"fourcomment %@",fourcomment);
                if ([fourcomment length]==0) {
                    [fourCommentArray insertObject:@"No Comments" atIndex:fourCommentArray.count-1];
                }
                else{
                    [fourCommentArray insertObject:fourcomment atIndex:fourCommentArray.count-1];
                }
                
            }
            
        } 
    }
    delegate.dataUpdate=dataUpdate;
    delegate.dataType=dataType;
    delegate.snoArray=snoArray;
    delegate.likearray=likearray;
    delegate.dislikearray=dislikearray;
    delegate.differUser=differUser;
    delegate.likesNameArray=likesNameArray;
    delegate.dislikesNameArray=dislikesNameArray;
    delegate.profielImages=profielImages;
    delegate.commentCounts=commentCounts;
    delegate.locationArray=locationArray;
    delegate.timeArray=timeArray;
    delegate.fourCommentArray=fourCommentArray;
    //[mytable reloadData];
}
/*- (void)scrollViewDidEndDragging:(UITableView *)tableView  willDecelerate:(BOOL)decelerate
 {
 NSLog(@"Dragged table ************");
 if(in ==[dataUpdate count])
 {
 [self refreshData];
 }
 }*/
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == [dataUpdate count]-1)
    {
        NSLog(@"Dragged table ************");
        [self refreshData];
        
    }
}


/*-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Delete the row from the data source
 // if ([dataUpdate count] >= 1)
 if(indexPath.row == [dataUpdate count]-1){
 [tableView beginUpdates];
 [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 [dataUpdate removeObjectAtIndex:[indexPath row]];
 
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString *documentsDirectory = [paths objectAtIndex:0];
 NSString *somepath = [documentsDirectory stringByAppendingPathComponent:@"something.plist"];
 [dataUpdate writeToFile:somepath atomically:YES];
 
 if ([dataUpdate count] == 0) {
 [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 [tableView endUpdates];
 }
 }   
 }*/
- (void)viewWillDisappear:(BOOL)animated {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:IFTweetLabelURLNotification object:nil];
    
    //[super dealloc];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc {
  
    [_topBar release];
    [thesearchbar release];
   
    [super dealloc];
}
@end
