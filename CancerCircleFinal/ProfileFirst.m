//
//  ProfileFirst.m
//  CancerCircleFirst
//
//  Created by Raminder on 25/01/13.
//
//

#import "ProfileFirst.h"
#import "Login.h"
#import "AppDelegate.h"
#import "SettingButtons.h"
#import <Social/Social.h>
#import <QuartzCore/QuartzCore.h>
#import "ODRefreshControl.h"
#import "imageDetailView.h"
#import "videoDetailView.h"
#import "storyDetailView.h"
#import "SearchViewController.h"
#import "CircleFirst.h"
#import "OpenUrlView.h"
#import "FollowRequestView.h"
#import "TabBarViewController.h"
#import "FollowerViewController.h"
#import "FollowingViewController.h"
#import "ILTranslucentView.h"

@interface ProfileFirst ()

@end

@implementation ProfileFirst
@synthesize connection,videoUrl;
@synthesize movieplayer = _movieplayer;
@synthesize getfollowings, getfollowers;
UIActivityIndicatorView *indicator;
CGFloat startContentOffset;
CGFloat lastContentOffset;
BOOL hidden;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.navigationController.navigationBarHidden = NO;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
   [self.navigationController setNavigationBarHidden:NO animated:YES];
    
   
}
-(void)viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleTweetNotification:) name:IFTweetLabelURLNotification object:nil];
  
   
    blurView.dynamic = YES;


    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    imageData=delegate.profilePicData;
    meImageStr=delegate.profileImage;
    Login *login=[[Login alloc]init];
    NSMutableAttributedString *attributed=[login atributedString:delegate.status];
    statuslabel.attributedText=attributed;
    profileImage.image=[UIImage imageWithData:imageData];
     self.profileback.image=[UIImage imageWithData:imageData];
    self.profileback.contentMode = UIViewContentModeScaleAspectFill;
    self.profileback.clipsToBounds = YES;
    
    CALayer *imageLayer = profileImage.layer;
   
    
  
    [imageLayer setCornerRadius:30];
    [imageLayer setBorderWidth:1];
    [imageLayer setBorderColor:[[UIColor alloc] initWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:0.8].CGColor];
    [imageLayer setMasksToBounds:YES];
    
 
    
    NSLog(@"followers %@",delegate.followers);
    NSLog(@"followings %@",delegate.following);
    
    
  
    
     followers = [getfollowers stringValue];
     following = [getfollowings stringValue];
    

    [followerLabel setTitle:[followers stringByAppendingFormat:@" Followers"] forState: UIControlStateNormal];
    [followerLabel setTitle:[followers stringByAppendingFormat:@" Followers"] forState: UIControlStateSelected];
    [followerLabel setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    [followinglabel setTitle:[following stringByAppendingFormat:@" Followings"] forState: UIControlStateNormal];
    [followinglabel setTitle:[following stringByAppendingFormat:@" Followings"] forState: UIControlStateSelected];
    [followinglabel setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    idAtProfile=delegate.tabId;
    NSLog(@"idAtCricle %@",idAtProfile);
    delegate.tabId=@"profile";
    delegate.secondTabId=@"profile";
    followerLabel.layer.borderColor = [[UIColor alloc] initWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:0.34].CGColor;
    followerLabel.layer.borderWidth = 1;
    followinglabel.layer.borderColor = [[UIColor alloc] initWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:0.34].CGColor;
    followinglabel.layer.borderWidth = 1;
    _followreq.layer.borderColor = [[UIColor alloc] initWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:0.34].CGColor;
    _followreq.layer.borderWidth = 1;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    {
            AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
        [self profileData];
        [self refreshProfileData];
    self.imageDownloadingQueue = [[NSOperationQueue alloc] init];
    self.imageDownloadingQueue.maxConcurrentOperationCount = 4;
    self.imageCache = [[NSCache alloc] init];
      
        
        NSLog(@"The button title is %@ ", followerLabel.titleLabel.text);
    
    //************* Nvigation bar ****************************************************
    
        UIInterpolatingMotionEffect *horizontalMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
        
        horizontalMotionEffect.minimumRelativeValue = @(-30);
        
        horizontalMotionEffect.maximumRelativeValue = @(30);
        
        [_profileback addMotionEffect:horizontalMotionEffect];
        
        UIInterpolatingMotionEffect *vertMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
        
        vertMotionEffect.minimumRelativeValue = @(-30);
        
        vertMotionEffect.maximumRelativeValue = @(30);
        
        [_profileback addMotionEffect:vertMotionEffect];
        
  
    getusername = delegate.mynameLogin;
    NSLog(@"GET on UPload %@",getusername);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Profile"
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
        CGFloat titleWidth2 = 100;
      
        CGFloat titleWidth = MIN(titleWidth2, 300);
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 20)];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont fontWithName:@"Futura_Light" size:20];
     
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor =[UIColor whiteColor];
        label.text=@"You";
    UINavigationBar *navBar = [[self navigationController] navigationBar];
     self.navigationItem.titleView = label;
    
    navBar.barTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
   
        UIImage* image4 = [UIImage imageNamed:@"settingNav.png"];
        CGRect frameimg2 = CGRectMake(0, 0,20,20);
        UIButton *someButton2 = [[UIButton alloc] initWithFrame:frameimg2];
        [someButton2 setImage:image4 forState:UIControlStateNormal];
        [someButton2 addTarget:self action:@selector(goSettings)
              forControlEvents:UIControlEventTouchUpInside];
        [someButton2 setShowsTouchWhenHighlighted:YES];
        
        UIBarButtonItem *backbutton2 =[[UIBarButtonItem alloc] initWithCustomView:someButton2];
        self.navigationItem.rightBarButtonItem=backbutton2;
        
        UIBarButtonItem *backbutton =[[UIBarButtonItem alloc] initWithTitle:@"news" style:UIBarButtonItemStyleBordered target:self action:@selector(goNews:)];
        
        backbutton.tintColor = [[UIColor alloc] initWithRed:(255.0/255.0) green:(255.0/255.0) blue:(255.0/255.0) alpha:0.7];
        [backbutton setTitleTextAttributes:@{
                                             NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:15.0],
                                             NSForegroundColorAttributeName: [[UIColor alloc] initWithRed:(255.0/255.0) green:(255.0/255.0) blue:(255.0/255.0) alpha:0.7]
                                             } forState:UIControlStateNormal];
        [backbutton setTitlePositionAdjustment:UIOffsetMake(0.0f, 30.0f) forBarMetrics:UIBarMetricsDefault];
        
        self.navigationItem.leftBarButtonItem=backbutton;
        
        
        
        [someButton2 release];

   
        mytable.backgroundColor = [UIColor clearColor];
    ODRefreshControl *refreshControl = [[ODRefreshControl alloc] initInScrollView:mytable];
    refreshControl.tintColor=[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1];
    refreshControl.backgroundColor = [UIColor clearColor];
    [refreshControl addTarget:self action:@selector(changeSorting:) forControlEvents:UIControlEventValueChanged];
    [self changeSorting:(refreshControl)];
    self.view.backgroundColor = [[UIColor alloc] initWithRed:(233.0/255.0) green:(234.0/255.0) blue:(241.0/255.0) alpha:1];
    
    usernameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:18];
        
    NSString *labelText = [NSString stringWithFormat:@"%@ (%@)", delegate.displayName,
                              delegate.whoAreYou];
    usernameLabel.text=labelText ;
    getuserpass=delegate.myPassword;
    statuslabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:12];
    // dpUrl=delegate.profileImage;
    Login *login=[[Login alloc]init];
    NSLog(@"Status in delegate %@",delegate.status);
    NSMutableAttributedString *attributed=[login atributedString:delegate.status];
    statuslabel.attributedText=attributed;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleTweetNotification:) name:IFTweetLabelURLNotification object:nil];
  
    
    [mytable reloadData];
    }
    //[indicator stopAnimating];
    //[indicator setHidden:YES];
}

-(IBAction)goNews: (UIButton *)sender
{
    
    
    if (sender.tag == 0) {
        sender.tag = 1;
        
        [self.slidingViewController anchorTopViewTo:ECRight];
        
        
        
        
    }
    else{
        sender.tag=0;
        
        [self.slidingViewController resetTopView];
        
        
        
    }
}

-(void)expand
{
    
    [_mainScroll bringSubviewToFront:mytable];
     mytable.backgroundColor = [[UIColor alloc] initWithRed:(233.0/255.0) green:(234.0/255.0) blue:(241.0/255.0) alpha:1];
    
    [UIView animateWithDuration:0.5f
                     animations:^
     {
         
         mytable.frame = CGRectMake(0, 0, mytable.frame.size.width, mytable.frame.size.height);
         
         
     }
                     completion:^(BOOL finished)
     {
         
      
     }
     ];
}

-(void)contract
{
   
    [UIView animateWithDuration:0.5f
                     animations:^
     {
         
         mytable.frame = CGRectMake(0, 190, mytable.frame.size.width, mytable.frame.size.height);
         
         
         
     }
                     completion:^(BOOL finished)
     {
         mytable.backgroundColor = [UIColor clearColor];
         [_mainScroll sendSubviewToBack:mytable];
         
     }
     ];

  
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    startContentOffset = lastContentOffset = scrollView.contentOffset.y;
    
    
    //NSLog(@"scrollViewWillBeginDragging: %f", scrollView.contentOffset.y);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat differenceFromStart = startContentOffset - currentOffset;
    CGFloat differenceFromLast = lastContentOffset - currentOffset;
    lastContentOffset = currentOffset;
    
    
    
    if((differenceFromStart) < 0)
    {
        // scroll up
        if(scrollView.isTracking && (abs(differenceFromLast)>1))
        {
            
                
            
               
            [self expand];
        
            
        }
    }
    else {
        if(scrollView.isTracking && (abs(differenceFromLast)>1))
        {
            [self contract];
          
                                }
    }
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    [self contract];
    return YES;
}

- (void)handleTweetNotification:(NSNotification *)notification
{
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    NSDictionary *dict = (NSDictionary*)notification.object;
    NSString *strDict = [NSString stringWithFormat:@"%@", dict];
    NSLog(@"output is %@", strDict);
    if([strDict isEqualToString:delegate.mynameLogin])
    {
        [mytable reloadData];
    }
    
    else if([strDict hasPrefix:@"http://"])
    {
        OpenUrlView *openUrl=[self.storyboard instantiateViewControllerWithIdentifier:@"OpenUrl"];
        //openUrl.hidesBottomBarWhenPushed = YES;
        UINavigationBar *navBar = [[self navigationController] navigationBar];
        navBar.hidden=false;
        openUrl.getUrl=strDict;
        openUrl.getId=@"profile";
        [self.navigationController pushViewController:openUrl animated:YES];
    }
    else
    {
        
        delegate.wordToSearch=strDict;
        //[self performSegueWithIdentifier:@"profileToSearch" sender:self];
        SearchViewController *search=[self.storyboard instantiateViewControllerWithIdentifier:@"SearchView"];
        //search.hidesBottomBarWhenPushed = YES;
        UINavigationBar *navBar = [[self navigationController] navigationBar];
        navBar.hidden=false;
        [self.navigationController pushViewController:search animated:YES];
        
    }
}
-(IBAction)getFollowings
{
    NSNumber *yourNumber = [NSNumber numberWithInt:0];
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate.following isEqualToNumber:yourNumber]) {
        
    }
    else
    {
        FollowingViewController *getfollowing=[self.storyboard instantiateViewControllerWithIdentifier:@"following"];
        getfollowing.getName=getusername;
        [self.navigationController pushViewController:getfollowing animated:YES];
    }
}
-(IBAction)getFollowers
{
    NSNumber *yourNumber = [NSNumber numberWithInt:0];
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate.followers isEqualToNumber:yourNumber]) {
        
    }
    else
    {
        FollowerViewController *getfollower=[self.storyboard instantiateViewControllerWithIdentifier:@"follower"];
        getfollower.getName=getusername;
        [self.navigationController pushViewController:getfollower animated:YES];
    }
}
-(IBAction)goBack
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate.tabId isEqualToString:@"circle"]) {
        CircleFirst   *search=[self.storyboard instantiateViewControllerWithIdentifier:@"CircleFirst"];
        [self.navigationController pushViewController:search animated:YES];
    }
    else  if ([delegate.tabId isEqualToString:@"profile"]) {
        ProfileFirst   *search=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileFirst"];
        [self.navigationController pushViewController:search animated:YES];
    }
    else  if (([delegate.tabId isEqualToString:@"search"])&&([delegate.secondTabId isEqualToString:@"profile"])) {
        ProfileFirst   *search=[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileFirst"];
        [self.navigationController pushViewController:search animated:YES];
    }
    else  if (([delegate.tabId isEqualToString:@"search"])&&([delegate.secondTabId isEqualToString:@"circle"])) {
        CircleFirst   *search=[self.storyboard instantiateViewControllerWithIdentifier:@"CircleFirst"];
        [self.navigationController pushViewController:search animated:YES];
    }
    /* else  if ([delegate.tabId isEqualToString:@"following"]){
     FollowingViewController *getfollowings=[self.storyboard instantiateViewControllerWithIdentifier:@"following"];
     [self.navigationController pushViewController:getfollowings animated:YES];
     }
     else  if ([delegate.tabId isEqualToString:@"follower"]){
     FollowerViewController *getfollower=[self.storyboard instantiateViewControllerWithIdentifier:@"follower"];
     [self.navigationController pushViewController:getfollower animated:YES];
     }*/
    
    //[self dismissViewControllerAnimated:YES completion:nil];
    
    
}

-(IBAction)goLogin
{
    SearchViewController *search=[self.storyboard instantiateViewControllerWithIdentifier:@"SearchView"];
    // search.hidesBottomBarWhenPushed = YES;
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    navBar.hidden=false;
    [self.navigationController pushViewController:search animated:YES];
    //[self performSegueWithIdentifier:@"profileToSearch" sender:self];
}
-(IBAction)followRequestClicked
{
    /* AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
     if ([delegate.requestCount isEqualToString:@"0"]) {
     UIAlertView *alertReq=[[UIAlertView alloc]initWithTitle:@"No New" message:@"Follow Request" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
     [alertReq show];
     }
     else
     {
     [self performSegueWithIdentifier:@"profileToRequest" sender:self];
     }*/
    
}
- (IBAction)refreshData
{
    [indicator startAnimating];
    [indicator setHidden:NO];
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self refreshProfileData]; // 1
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            [mytable reloadData];
            [indicator stopAnimating];
            [indicator setHidden:YES];// 2
        });
    });
   
}

- (void)changeSorting:(ODRefreshControl *)refreshControl
//- (void)changeSorting:(UIRefreshControl *)refreshControl
{
    //[indicator startAnimating];
    //[indicator setHidden:NO];
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self profilePullData]; // 1
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            [refreshControl endRefreshing];
            //[indicator stopAnimating];
            //[indicator setHidden:YES];// 2
        });
    });
  
}


-(void)startIndicator
{
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];//UIActivityIndicatorViewStyleWhiteLarge
    indicator.frame = CGRectMake(self.view.bounds.size.width / 2.0f - indicator.frame.size.width /2.0f, self.view.bounds.size.height / 2.0f - indicator.frame.size.height /2.0f, indicator.frame.size.width, indicator.frame.size.height);
    //indicator.color = [UIColor colorWithRed:1 green:0.8 blue:0.9 alpha:1 ];
    indicator.color = [UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
    indicator.backgroundColor = [UIColor clearColor];
    CGAffineTransform transform = CGAffineTransformMakeScale(2.2f, 2.2f);
    indicator.transform = transform;
    [indicator startAnimating];
    [self.view addSubview:indicator];
    indicator.hidden = NO;
    [self performSelector:@selector(request) withObject:nil afterDelay:0.1];
    
    
}
-(void)profileData
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    dataUpdate=delegate.PdataUpdate;
    //dataUpdate2=delegate.PdataUpdate2;
    //dataUpdate2=nil;
    snoArray=delegate.PsnoArray;
    likearray=delegate.Plikearray;
    dislikearray=delegate.Pdislikearray;
    dataType=delegate.PdataType;
    likesNameArray=delegate.PlikesNameArray;
    dislikesNameArray=delegate.PdislikesNameArray;
    commentCounts=delegate.PcommentCounts;
    locationArray=delegate.PlocationArray;
    timeArray=delegate.PtimeArray;
    fourCommentArray=delegate.PfourCommentArray;
    getfollowers=delegate.followers;
    getfollowings=delegate.following;
    [mytable reloadData];
   
    
}

-(IBAction)goSettings
{
    self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:3];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataUpdate count];
}
- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *typeString=[dataType objectAtIndex:indexPath.row];
    int height;
    if([typeString isEqualToString:@"image"])
    {
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)) {
            height= 455;
        }
        else if ([comArray count]==3){
            height= 490;
        }
        else{
            
            height= 500;
        }
        
    }
    else if([typeString isEqualToString:@"video"]){
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)) {
            height= 455;
        }
        else if ([comArray count]==3){
            height= 490;
        }
        else{
            
            height= 500;
        }
        
        
    }
    else if([typeString isEqualToString:@"story"]){
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)) {
            height= 155;
        }
        else if ([comArray count]==3){
            height= 185;
        }
        else{
            
            height=225;
        }
        
    }
    else
    {
        height=30;
    }
    return height;
    
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    Login *login=[[Login alloc]init];
    UIImage *minus = [UIImage imageNamed:@"minus.png"];
    UIImage *plus = [UIImage imageNamed:@"Plus.png"];
    UIImage *minusDark = [UIImage imageNamed:@"mDark.png"];
    UIImage *plusDark = [UIImage imageNamed:@"pDark.png"];
    UIImage *plusOut = [UIImage imageNamed:@"pOut.png"];
    UIImage *minusOut = [UIImage imageNamed:@"mOut.png"];
    
   
    
    static NSString *CellIdentifier = @"Cell";
    static NSString *CellIdentifier2 = @"Cell2";
    static NSString *CellIdentifier3 = @"Cell3";
    static NSString *moreCellId = @"more";
    static NSString *image1 = @"image1";
    static NSString *image2 = @"image2";
    static NSString *story1 = @"story1";
    static NSString *story2 = @"story2";
    static NSString *video1 = @"video1";
    static NSString *video2 = @"video2";
    
    UITableViewCell *cell4 = [tableView dequeueReusableCellWithIdentifier:moreCellId];
    cell4.selectionStyle = UITableViewCellSelectionStyleNone;
    mytable.separatorStyle = UITableViewCellSeparatorStyleNone;
   
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UITableViewCell *ce=[[UITableViewCell alloc]init];
    UITableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
    UITableViewCell *cell3 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier3];
    UITableViewCell *cellstory1 = [tableView dequeueReusableCellWithIdentifier:story1];
    UITableViewCell *cellstory2 = [tableView dequeueReusableCellWithIdentifier:story2];
    UITableViewCell *cellvideo1 = [tableView dequeueReusableCellWithIdentifier:video1];
    UITableViewCell *cellvideo2 = [tableView dequeueReusableCellWithIdentifier:video2];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell2.selectionStyle = UITableViewCellSelectionStyleNone;
    cell3.selectionStyle = UITableViewCellSelectionStyleNone;
    cell4.selectionStyle = UITableViewCellSelectionStyleNone;
    cellstory1.selectionStyle = UITableViewCellSelectionStyleNone;
    cellstory2.selectionStyle = UITableViewCellSelectionStyleNone;
    cellvideo1.selectionStyle = UITableViewCellSelectionStyleNone;
    cellvideo2.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    cell2.backgroundColor = [UIColor clearColor];
    cell3.backgroundColor = [UIColor clearColor];
    cell4.backgroundColor = [UIColor clearColor];
    cellstory1.backgroundColor = [UIColor clearColor];
    cellstory2.backgroundColor = [UIColor clearColor];
    cellvideo1.backgroundColor = [UIColor clearColor];
    cellvideo2.backgroundColor = [UIColor clearColor];
    NSData *imageString =[dataUpdate objectAtIndex:indexPath.row];
    NSString *typeString=[dataType objectAtIndex:indexPath.row];
    int in=indexPath.row;
    introw=indexPath.row;
    int lbltag =indexPath.row+1;
    // NSString *content= [self contentTypeForImageData:imageString];
    if([typeString isEqualToString:@"image"])
    {
        NSLog(@"hiiiiiiiiii image %@",imageString);
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)){
            NSLog(@"hiiiiiiiiii image %@",imageString);
            image2Cell *imgCell=[tableView dequeueReusableCellWithIdentifier:image2];
            if (imgCell == nil)
            {
                imgCell = [[[image2Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
                NSLog(@"index path in cell %i",indexPath.row);
                
            }
            for(UIView *v in [imgCell.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFTweetLabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFLabelUsername class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIImageView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIButton class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIInterpolatingMotionEffect class]])
                    [v removeFromSuperview];
            }
            
            introw=indexPath.row+1;
       
            UIView *imageShadow = [[UIView alloc]initWithFrame:CGRectMake(10, 0, 300, 310)];
            UIView *imageParallaxView = [[UIView alloc]initWithFrame:CGRectMake(5, 0, 310, 310)];
            imageParallaxView.clipsToBounds = YES;
            CALayer *layer = imageShadow.layer;
            layer.shadowOffset = CGSizeMake(0, 1);
            layer.shadowColor = [[UIColor blackColor] CGColor];
            layer.shadowRadius = 2.0f;
            layer.shadowOpacity = 0.60f;
            layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];
            
            UIImageView *parallaxImage = [[UIImageView alloc] initWithFrame:CGRectMake(-20, -20, 340, 340)];
            
            [imageParallaxView addSubview:parallaxImage];
            [imgCell.contentView addSubview:imageShadow];
            [imgCell.contentView addSubview:imageParallaxView];
            
            NSLog(@"value in data update is:- %@",dataUpdate);
            NSString *imageUrlString = [dataUpdate objectAtIndex: indexPath.row];
            
            NSLog(@"image url string iss:- %@",imageUrlString);
            UIImage *cachedImage = [self.imageCache objectForKey:imageUrlString];
            if (cachedImage)
            {
                //imgCell.imageView.image = cachedImage;
                parallaxImage.image = cachedImage;
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                //imgCell.imageView.image = [UIImage imageNamed:@"placholder.png"];
                parallaxImage.image = [UIImage imageNamed:@"placholder.png"];
                [self.imageDownloadingQueue addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:imageUrlString];
                    NSData *imageData2 = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData2)
                        image = [UIImage imageWithData:imageData2];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCache setObject:image forKey:imageString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                //imgCell.imageView.image = image;
                                
                                parallaxImage.image = image;
                            
                         
                            
                            
                        }];
                    }
                }];
            }
            
            
            UIInterpolatingMotionEffect *horizontalMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
            
            horizontalMotionEffect.minimumRelativeValue = @(-30);
            
            horizontalMotionEffect.maximumRelativeValue = @(40);
            
            [parallaxImage addMotionEffect:horizontalMotionEffect];
            
            UIInterpolatingMotionEffect *vertMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
            
            vertMotionEffect.minimumRelativeValue = @(-34);
            
            vertMotionEffect.maximumRelativeValue = @(34);
            
            [parallaxImage addMotionEffect:vertMotionEffect];
            
            NSString *caption2=[login smilyString:[dataUpdate2 objectAtIndex:in]];
            tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,320,295,20)];
            tweetLabel.numberOfLines=2;
            tweetLabel.textColor=[UIColor blackColor];
            tweetLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:12];
            [tweetLabel setText:caption2];
            [tweetLabel setLinksEnabled:TRUE];
            tweetLabel.clipsToBounds=YES;
            
            //***********************************
            
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,328,40,40)];
            imageView2.layer.cornerRadius = 20;
            imageView2.clipsToBounds = YES;
            [imgCell.contentView addSubview:imageView2];
            imageView2.image=[UIImage imageWithData:imageData];
            
            
            
            //***********************************
            
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,325,200,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
            nameLabel.textColor = [UIColor clearColor];
            NSString *smile2=getusername;
            [nameLabel setText:smile2];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.clipsToBounds=YES;
            [imgCell.contentView addSubview:nameLabel];
            
            UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 407, 310, 43)];
            [imgCell addSubview:buttonsBack];
            buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
            
            
            //NSString *smile;
            //IFTweetLabel *descriptionLabel1=[[IFTweetLabel alloc]initWithFrame:CGRectMake(15,380,278,15)];
            //descriptionLabel1.numberOfLines=0;
            //descriptionLabel1.textColor = [UIColor blackColor];
            //descriptionLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            //descriptionLabel1.clipsToBounds=YES;

            //NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
            //NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
            //if ([comArray count]==2) {
            //    smile=[login smilyString:[comArray objectAtIndex:0]];
            //    [descriptionLabel1 setText:smile];
            //    [descriptionLabel1 setLinksEnabled:TRUE];
            //}
            //[imgCell.contentView addSubview:descriptionLabel1];
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,335,100,10)];
            placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [imgCell.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,350,100,10)];
            timeLabel.textColor=[[UIColor alloc] initWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:1];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentLeft;
            [imgCell.contentView addSubview:timeLabel];
           
            
            
            
            if(caption2.length > 0){
                [imgCell.contentView addSubview:tweetLabel];
                imageView2.frame = CGRectMake(15,348,40,40);
                nameLabel.frame = CGRectMake(65,345,200,30);
                placeLabel.frame = CGRectMake(205,355,100,10);
                timeLabel.frame = CGRectMake(65,370,100,10);
            }else {
                imageView2.frame = CGRectMake(15,318,40,40);
                nameLabel.frame = CGRectMake(65,315,200,30);
                placeLabel.frame = CGRectMake(205,320,100,10);
                timeLabel.frame = CGRectMake(65,340,100,10);
            }
    //**************** like button
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            [likeBtn addTarget:self action:@selector(likeImage:) forControlEvents:UIControlEventTouchUpInside];
            likeBtn.frame=CGRectMake(5,407,62,43);
            
    //**************** dislike button
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            [dislikeBtn addTarget:self action:@selector(dislikeImage:) forControlEvents:UIControlEventTouchUpInside];
            dislikeBtn.frame=CGRectMake(67,407,62,43);
            
    //**************** like button
            
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                dislikeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
                [dislikeBtn setBackgroundImage:minusOut forState:UIControlStateNormal];
                
                
                
            }
            else
            {
                dislikeBtn.userInteractionEnabled=YES;
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            
            [imgCell addSubview:likeBtn];
     
    //**************** dislike button
            
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusOut forState:UIControlStateNormal];
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                likeBtn.userInteractionEnabled=YES;
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            
            [imgCell addSubview:dislikeBtn];
            
            
    //**************** comment button
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"Comment.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(129,407,62,43)];
            [commentButton addTarget:self action:@selector(moreButtonImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:commentButton];
            
    //**************** like show
            
            UILabel *likeShow = nil;
            likeShow.backgroundColor=[UIColor redColor];
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,437,43,10)] autorelease];
            likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:7.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [imgCell addSubview:likeShow];
            
    //**************** dislike show
            
            UILabel *dislikeShow = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,437,43,10)] autorelease];
            dislikeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:7.0];
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            dislikeShow.tag = lbltag;
            [imgCell addSubview:dislikeShow];
            
    //**************** comment show
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,437,43,10)] autorelease];
            commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:7.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [imgCell addSubview:commentShow];
        
    //**************** share button
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"Share.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [shareButton setFrame:CGRectMake(191,407,62,43)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:shareButton];
            
    //**************** settings button
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"Settings.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(253,407,62,43)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:settingsButton];
            
    //**************** more button
            
           /* UIButton *moreButtonI=nil;
            moreButtonI = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonI.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonI setBackgroundImage:more forState:UIControlStateNormal];
            
            [moreButtonI setFrame:CGRectMake(285,310,20,4)];
            [moreButtonI addTarget:self action:@selector(moreButtonImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:moreButtonI]; */
            
            //******************************************************************************
            
            //return cell;
            ce=imgCell;
            
            
        }
        
        else if([comArray count]==3) {
            
            NSLog(@"hiiiiiiiiii image %@",imageString);
            image1Cell *imgCell=[tableView dequeueReusableCellWithIdentifier:image1];
            if (imgCell == nil)
            {
                imgCell = [[[image1Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
                NSLog(@"index path in cell %i",indexPath.row);
                
            }
            
            for(UIView *v in [imgCell.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFTweetLabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFLabelUsername class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIImageView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIButton class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIInterpolatingMotionEffect class]])
                    [v removeFromSuperview];
            }
            
            introw=indexPath.row+1;
            
            UIView *imageShadow = [[UIView alloc]initWithFrame:CGRectMake(10, 0, 300, 310)];
            UIView *imageParallaxView = [[UIView alloc]initWithFrame:CGRectMake(5, 0, 310, 310)];
            imageParallaxView.clipsToBounds = YES;
            CALayer *layer = imageShadow.layer;
            layer.shadowOffset = CGSizeMake(0, 1);
            layer.shadowColor = [[UIColor blackColor] CGColor];
            layer.shadowRadius = 2.0f;
            layer.shadowOpacity = 0.60f;
            layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];
            
            UIImageView *parallaxImage = [[UIImageView alloc] initWithFrame:CGRectMake(-20, -20, 340, 340)];
            
            [imageParallaxView addSubview:parallaxImage];
            [imgCell.contentView addSubview:imageShadow];
            [imgCell.contentView addSubview:imageParallaxView];
            
            UIInterpolatingMotionEffect *horizontalMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
            
            horizontalMotionEffect.minimumRelativeValue = @(-30);
            
            horizontalMotionEffect.maximumRelativeValue = @(40);
            
            [parallaxImage addMotionEffect:horizontalMotionEffect];
            
            UIInterpolatingMotionEffect *vertMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
            
            vertMotionEffect.minimumRelativeValue = @(-34);
            
            vertMotionEffect.maximumRelativeValue = @(34);
            
            [parallaxImage addMotionEffect:vertMotionEffect];
            
            NSLog(@"value in data update is:- %@",dataUpdate);
            NSString *imageUrlString = [dataUpdate objectAtIndex: indexPath.row];
            
            NSLog(@"image url string iss:- %@",imageUrlString);
            UIImage *cachedImage = [self.imageCache objectForKey:imageUrlString];
            if (cachedImage)
            {
                //imgCell.imageView.image = cachedImage;
                parallaxImage.image = cachedImage;
            }
            else
            {
                // you'll want to initialize the image with some blank image as a placeholder
                
                //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
                //imgCell.imageView.image = [UIImage imageNamed:@"placholder.png"];
                 parallaxImage.image = [UIImage imageNamed:@"placholder.png"];
                [self.imageDownloadingQueue addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:imageUrlString];
                    NSData *imageData2 = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData2)
                        image = [UIImage imageWithData:imageData2];
                    if (image)
                    {
                        // add the image to your cache
                        
                        [self.imageCache setObject:image forKey:imageString];
                        
                        // finally, update the user interface in the main queue
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                //imgCell.imageView.image = image;
                                
                                parallaxImage.image = image;
                            
                            
                        }];
                    }
                }];
            }
            
            
            NSString *caption2=[login smilyString:[dataUpdate2 objectAtIndex:in]];
            tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,320,295,20)];
            tweetLabel.numberOfLines=2;
            tweetLabel.textColor=[UIColor blackColor];
            tweetLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:12];
            [tweetLabel setText:caption2];
            [tweetLabel setLinksEnabled:TRUE];
            tweetLabel.clipsToBounds=YES;
            
            //***********************************
            
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,328,40,40)];
            imageView2.layer.cornerRadius = 20;
            imageView2.clipsToBounds = YES;
            imageView2.image=[UIImage imageWithData:imageData];
            [imgCell.contentView addSubview:imageView2];
            
            
            //***********************************
            
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,325,200,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
            nameLabel.textColor = [UIColor clearColor];
            NSString *smile2=getusername;
            [nameLabel setText:smile2];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.clipsToBounds=YES;
            [imgCell.contentView addSubview:nameLabel];
            
            UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 442, 310, 43)];
            [imgCell addSubview:buttonsBack];
            buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
            
            
            NSString *smile;
            
            IFTweetLabel *descriptionLabel1=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,380,280,20)];
            descriptionLabel1.numberOfLines=0;
            descriptionLabel1.textColor = [UIColor blackColor];
            descriptionLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel1.clipsToBounds=YES;
           
            IFTweetLabel *descriptionLabel2=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,395,280,20)];
            descriptionLabel2.numberOfLines=0;
            descriptionLabel2.textColor = [UIColor blackColor];
            descriptionLabel2.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel2.clipsToBounds=YES;
            
            IFTweetLabel *descriptionLabel3=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,410,280,20)];
            descriptionLabel3.numberOfLines=0;
            descriptionLabel3.textColor = [UIColor blackColor];
            descriptionLabel3.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel3.clipsToBounds=YES;
            
            IFTweetLabel *descriptionLabel4=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,425,280,20)];
            descriptionLabel4.numberOfLines=0;
            descriptionLabel4.textColor = [UIColor blackColor];
            descriptionLabel4.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel4.clipsToBounds=YES;
            
            
            NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
            NSMutableAttributedString *attributed;
            NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
            if ([comArray count]==2) {
                //descriptionLabel1.text=@"Only one comment add your comment....";
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
                
            }
            else if ([comArray count]==3) {
                attributed=[login attibutedUsername:[comArray objectAtIndex:0]];
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
                attributed=[login attibutedUsername:[comArray objectAtIndex:1]];
                smile=[login smilyString:[comArray objectAtIndex:1]];
                [descriptionLabel2 setText:smile];
                [descriptionLabel2 setLinksEnabled:TRUE];
            }
            
            
            // }
            
            [imgCell.contentView addSubview:descriptionLabel1];
            [imgCell.contentView addSubview:descriptionLabel2];
            
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,335,100,10)];
            placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [imgCell.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,350,200,10)];
            timeLabel.textColor=[[UIColor alloc] initWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:1];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentLeft;
            [imgCell.contentView addSubview:timeLabel];
            
            
            //******************************************************************************
            if(caption2.length > 0){
                [imgCell.contentView addSubview:tweetLabel];
                imageView2.frame = CGRectMake(15,348,40,40);
                nameLabel.frame = CGRectMake(65,345,200,30);
                placeLabel.frame = CGRectMake(205,355,100,10);
                timeLabel.frame = CGRectMake(65,370,100,10);
                
                descriptionLabel1.frame = CGRectMake(20,395,280,20);
                descriptionLabel2.frame = CGRectMake(20,410,280,20);
                descriptionLabel3.frame = CGRectMake(20,425,280,20);
                descriptionLabel4.frame = CGRectMake(20,440,280,20);
            }else {
                imageView2.frame = CGRectMake(15,318,40,40);
                nameLabel.frame = CGRectMake(65,315,200,30);
                placeLabel.frame = CGRectMake(205,320,100,10);
                timeLabel.frame = CGRectMake(65,340,100,10);
                
                descriptionLabel1.frame = CGRectMake(20,380,280,20);
                descriptionLabel2.frame = CGRectMake(20,395,280,20);
                descriptionLabel3.frame = CGRectMake(20,410,280,20);
                descriptionLabel4.frame = CGRectMake(20,425,280,20);
            }
            
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            [likeBtn addTarget:self action:@selector(likeImage:) forControlEvents:UIControlEventTouchUpInside];
            likeBtn.frame=CGRectMake(5,442,62,43);
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            NSLog(@"Confimation returned %@",[likesNameArray objectAtIndex:indexPath.row]);
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            [dislikeBtn addTarget:self action:@selector(dislikeImage:) forControlEvents:UIControlEventTouchUpInside];
            dislikeBtn.frame=CGRectMake(67,442,62,43);
            
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                dislikeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
                [dislikeBtn setBackgroundImage:minusOut forState:UIControlStateNormal];
            }
            else
            {
                dislikeBtn.userInteractionEnabled=YES;
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            
            [imgCell addSubview:likeBtn];
            
            
            
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusOut forState:UIControlStateNormal];
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                likeBtn.userInteractionEnabled=YES;
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [imgCell addSubview:dislikeBtn];
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"Comment.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(129,442,62,43)];
            [commentButton addTarget:self action:@selector(moreButtonImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:commentButton];
            
            
            UILabel *likeShow = nil;
            likeShow.backgroundColor=[UIColor redColor];
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,472,43,10)] autorelease];
            likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:7.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [imgCell addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            
            
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,472,43,10)] autorelease];
            dislikeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:7.0];
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            dislikeShow.tag = lbltag;
            [imgCell addSubview:dislikeShow];
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,472,43,10)] autorelease];
            commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:7.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [imgCell addSubview:commentShow];
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"Share.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [shareButton setFrame:CGRectMake(191,442,62,43)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:shareButton];
            
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"Settings.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(253,442,62,43)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:settingsButton];
            
            /* UIButton *moreButtonI=nil;
            moreButtonI = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonI.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonI setBackgroundImage:more forState:UIControlStateNormal];
            [moreButtonI setFrame:CGRectMake(285,310,20,4)];
            [moreButtonI addTarget:self action:@selector(moreButtonImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:moreButtonI];*/
            
            //******************************************************************************
            
            //return cell;
            ce=imgCell;
            
            
            
        }
        else if(([comArray count]==4)||([comArray count]==5)){
            imageCell *imgCell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (imgCell == nil)
            {
                imgCell = [[[imageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
                NSLog(@"index path in cell %i",indexPath.row);
                
            }
            
            for(UIView *v in [imgCell.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFTweetLabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFLabelUsername class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIImageView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIButton class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIInterpolatingMotionEffect class]])
                    [v removeFromSuperview];
            }

            
            introw=indexPath.row+1;
           
            UIView *imageShadow = [[UIView alloc]initWithFrame:CGRectMake(10, 0, 300, 310)];
            UIView *imageParallaxView = [[UIView alloc]initWithFrame:CGRectMake(5, 0, 310, 310)];
            imageParallaxView.clipsToBounds = YES;
            CALayer *layer = imageShadow.layer;
            layer.shadowOffset = CGSizeMake(0, 1);
            layer.shadowColor = [[UIColor blackColor] CGColor];
            layer.shadowRadius = 2.0f;
            layer.shadowOpacity = 0.60f;
            layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];
            
            UIImageView *parallaxImage = [[UIImageView alloc] initWithFrame:CGRectMake(-20, -20, 340, 340)];
            
            [imageParallaxView addSubview:parallaxImage];
            [imgCell.contentView addSubview:imageShadow];
            [imgCell.contentView addSubview:imageParallaxView];
            
            UIInterpolatingMotionEffect *horizontalMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
            
            horizontalMotionEffect.minimumRelativeValue = @(-30);
            
            horizontalMotionEffect.maximumRelativeValue = @(40);
            
            [parallaxImage addMotionEffect:horizontalMotionEffect];
            
            UIInterpolatingMotionEffect *vertMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
            
            vertMotionEffect.minimumRelativeValue = @(-34);
            
            vertMotionEffect.maximumRelativeValue = @(34);
            
            [parallaxImage addMotionEffect:vertMotionEffect];
            
            NSLog(@"value in data update is:- %@",dataUpdate);
            NSString *imageUrlString = [dataUpdate objectAtIndex: indexPath.row];
            
            NSLog(@"image url string iss:- %@",imageUrlString);
            UIImage *cachedImage = [self.imageCache objectForKey:imageUrlString];
            if (cachedImage)
            {
                //imgCell.imageView.image = cachedImage;
                parallaxImage.image = cachedImage;

            }
            else
            {
                //imgCell.imageView.image = [UIImage imageNamed:@"placholder.png"];
                parallaxImage.image = [UIImage imageNamed:@"placholder.png"];
                [self.imageDownloadingQueue addOperationWithBlock:^{
                    NSURL *imageURL   = [NSURL URLWithString:imageUrlString];
                    NSData *imageData2 = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image    = nil;
                    if (imageData2)
                        image = [UIImage imageWithData:imageData2];
                    if (image)
                    {
                        [self.imageCache setObject:image forKey:imageString];
                        
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell)
                                
                                
                                //imgCell.imageView.image = image;
                                parallaxImage.image = image;
                           
                            
                        }];
                    }
                }];
            }
            NSString *caption2=[login smilyString:[dataUpdate2 objectAtIndex:in]];
            tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,320,295,20)];
            tweetLabel.numberOfLines=2;
            tweetLabel.textColor=[UIColor blackColor];
            tweetLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:12];
            [tweetLabel setText:caption2];
            [tweetLabel setLinksEnabled:TRUE];
            tweetLabel.clipsToBounds=YES;
            
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,328,40,40)];
            imageView2.layer.cornerRadius = 20;
            imageView2.clipsToBounds = YES;
            
            UIImage *image = [UIImage imageWithData:imageData];
            imageView2.image=image;
            [imgCell.contentView addSubview:imageView2];
            NSLog(@"valid");
            
            
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,325,200,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
            nameLabel.textColor = [UIColor clearColor];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.text=getusername;
            [imgCell.contentView addSubview:nameLabel];
            
            UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 452, 310, 43)];
            [imgCell addSubview:buttonsBack];
            buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
            
            
            NSLog(@"Location %@",[locationArray objectAtIndex:in]);
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,335,100,10)];
            placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [imgCell.contentView addSubview:placeLabel];
            
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,350,200,10)];
            timeLabel.textColor=[[UIColor alloc] initWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:1];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentLeft;
            [imgCell.contentView addSubview:timeLabel];
            
            NSString *smile;
           
            IFTweetLabel *descriptionLabel1=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,380,280,20)];
            descriptionLabel1.numberOfLines=0;
            descriptionLabel1.textColor = [UIColor blackColor];
            descriptionLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel1.clipsToBounds=YES;
            
            IFTweetLabel *descriptionLabel2=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,395,280,20)];
            descriptionLabel2.numberOfLines=0;
            descriptionLabel2.textColor = [UIColor blackColor];
            descriptionLabel2.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel2.clipsToBounds=YES;
            
            IFTweetLabel *descriptionLabel3=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,410,280,20)];
            descriptionLabel3.numberOfLines=0;
            descriptionLabel3.textColor = [UIColor blackColor];
            descriptionLabel3.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel3.clipsToBounds=YES;
            
            IFTweetLabel *descriptionLabel4=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,425,280,20)];
            descriptionLabel4.numberOfLines=0;
            descriptionLabel4.textColor = [UIColor blackColor];
            descriptionLabel4.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel4.clipsToBounds=YES;

            
            smile=[login smilyString:[comArray objectAtIndex:0]];
            [descriptionLabel1 setText:smile];
            
            
            [descriptionLabel1 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:1]];
            [descriptionLabel2 setText:smile];
            [descriptionLabel2 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:2]];
            [descriptionLabel3 setText:smile];
            [descriptionLabel3 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:3]];
            [descriptionLabel4 setText:smile];
            [descriptionLabel4 setLinksEnabled:TRUE];
            
            
            [imgCell.contentView addSubview:descriptionLabel1];
            [imgCell.contentView addSubview:descriptionLabel2];
            [imgCell.contentView addSubview:descriptionLabel3];
            [imgCell.contentView addSubview:descriptionLabel4];
            
            if(caption2.length > 0){
                [imgCell.contentView addSubview:tweetLabel];
                imageView2.frame = CGRectMake(15,348,40,40);
                nameLabel.frame = CGRectMake(65,345,200,30);
                placeLabel.frame = CGRectMake(205,355,100,10);
                timeLabel.frame = CGRectMake(65,370,100,10);
                
                descriptionLabel1.frame = CGRectMake(20,395,280,20);
                descriptionLabel2.frame = CGRectMake(20,410,280,20);
                descriptionLabel3.frame = CGRectMake(20,425,280,20);
                descriptionLabel4.frame = CGRectMake(20,440,280,20);
            }else {
                imageView2.frame = CGRectMake(15,318,40,40);
                nameLabel.frame = CGRectMake(65,315,200,30);
                placeLabel.frame = CGRectMake(205,320,100,10);
                timeLabel.frame = CGRectMake(65,340,100,10);
                
                descriptionLabel1.frame = CGRectMake(20,380,280,20);
                descriptionLabel2.frame = CGRectMake(20,395,280,20);
                descriptionLabel3.frame = CGRectMake(20,410,280,20);
                descriptionLabel4.frame = CGRectMake(20,425,280,20);
            }
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            [likeBtn addTarget:self action:@selector(likeImage:) forControlEvents:UIControlEventTouchUpInside];
            likeBtn.frame=CGRectMake(5,452,62,43);
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            NSLog(@"Confimation returned %@",[likesNameArray objectAtIndex:indexPath.row]);
            
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            dislikeBtn.tag = indexPath.row+1;
            [dislikeBtn addTarget:self action:@selector(dislikeImage:) forControlEvents:UIControlEventTouchUpInside];
            dislikeBtn.frame=CGRectMake(67,452,62,43);
            
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                dislikeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
                [dislikeBtn setBackgroundImage:minusOut forState:UIControlStateNormal];
                
            }
            else
            {
                dislikeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            [imgCell addSubview:likeBtn];
            
            
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                 likeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
                [likeBtn setBackgroundImage:plusOut forState:UIControlStateNormal];
            }
            else
            {
                 likeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [imgCell addSubview:dislikeBtn];
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"Comment.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(129,452,62,43)];
            [commentButton addTarget:self action:@selector(moreButtonImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:commentButton];
            
            
            UILabel *likeShow = nil;
            likeShow.backgroundColor=[UIColor redColor];
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,482,43,10)] autorelease];
            likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:7.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [imgCell addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,482,43,10)] autorelease];
            dislikeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:7.0];
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            dislikeShow.tag = lbltag;
            [imgCell addSubview:dislikeShow];
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,482,43,10)] autorelease];
            commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:7.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [imgCell addSubview:commentShow];
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"Share.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [shareButton setFrame:CGRectMake(191,452,62,43)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:shareButton];
            
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"Settings.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(253,452,62,43)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(settingButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:settingsButton];
            
            /*UIButton *moreButtonI=nil;
            moreButtonI = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonI.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonI setBackgroundImage:more forState:UIControlStateNormal];
            [moreButtonI setFrame:CGRectMake(285,310,20,4)];
            [moreButtonI addTarget:self action:@selector(moreButtonImageClicked:) forControlEvents:UIControlEventTouchUpInside];
            [imgCell.contentView addSubview:moreButtonI];*/
            ce=imgCell;
            
            
        }
    }
    
    else if([typeString isEqualToString:@"story"])
    {
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)){
            NSLog(@"helllloooo");
            
            if (cellstory1 == nil)
            {
                cellstory1 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:story1] autorelease];
                
            }
            CGFloat x = [UIScreen mainScreen].bounds.origin.x;
            UIView *backgroundCellView = [[UIView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 150)];
            backgroundCellView.backgroundColor = [UIColor clearColor];
            UIImageView *backimage = [[UIImageView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 150)];
            backimage.backgroundColor = [UIColor whiteColor];
            [backgroundCellView addSubview:backimage];
            cellstory1.backgroundView = backgroundCellView;
            cellstory1.backgroundColor = [UIColor clearColor];
            
            
            
            for(UIView *v in [cellstory1.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFTweetLabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFLabelUsername class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIImageView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIButton class]])
                    [v removeFromSuperview];
            }
            
            introw=indexPath.row+1;
            tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,5,300,20)];
            tweetLabel.numberOfLines=0;
            tweetLabel.textColor=[UIColor blackColor];
            tweetLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:12];
            NSString *smile2=[login smilyString:[dataUpdate objectAtIndex:in]];
            [tweetLabel setText:smile2];
            [tweetLabel setLinksEnabled:TRUE];
            tweetLabel.clipsToBounds=YES;
            [cellstory1.contentView addSubview:tweetLabel];
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,35,40,40)];
            imageView2.layer.cornerRadius = 20;
            imageView2.clipsToBounds = YES;
            imageView2.image=[UIImage imageWithData:imageData];
            [cellstory1.contentView addSubview:imageView2];
            
            
            NSLog(@"valid");
            
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,35,200,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
            nameLabel.textColor = [UIColor clearColor];
            NSString *smile3=getusername;//[@"@" stringByAppendingString:[differUser objectAtIndex:in]];
            [nameLabel setText:smile3];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.clipsToBounds=YES;
            
            UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 107, 310, 43)];
            [cellstory1 addSubview:buttonsBack];
            buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
            
            [cellstory1.contentView addSubview:nameLabel];
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,40,100,10)];
            placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cellstory1.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,60,200,10)];
            timeLabel.textColor=[UIColor blackColor];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentLeft;
            [cellstory1.contentView addSubview:timeLabel];

            
            NSString *smile;
            IFTweetLabel *descriptionLabel1 = [[IFTweetLabel alloc] initWithFrame:CGRectMake(20,86,280,20)];
            descriptionLabel1.numberOfLines=0;
            descriptionLabel1.textColor = [UIColor blackColor];
            descriptionLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            
            NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
            if ([comArray count]==2) {
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
            }
            
            [cellstory1 addSubview:descriptionLabel1];
            
            UILabel *likeShow = nil;
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,137,43,10)] autorelease];
            likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:7.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cellstory1 addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,137,43,10)] autorelease];
            dislikeShow.textColor= [[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:7.0];
            dislikeShow.tag = lbltag;
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cellstory1 addSubview:dislikeShow];
            
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,137,43,10)] autorelease];
            commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:7.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cellstory1 addSubview:commentShow];
          
            //******************************************************************************
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            dislikeBtn.frame=CGRectMake(67,107,62,43);
            [dislikeBtn addTarget:self action:@selector(dislikeFn:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            likeBtn.frame=CGRectMake(5,107,62,43);
            [likeBtn addTarget:self action:@selector(likeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                dislikeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
                [dislikeBtn setBackgroundImage:minusOut forState:UIControlStateNormal];
            }
            else
            {
                dislikeBtn.userInteractionEnabled=YES;
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cellstory1 addSubview:likeBtn];
            
            NSLog(@"confermation %@",[dislikesNameArray objectAtIndex:indexPath.row]);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusOut forState:UIControlStateNormal];
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                likeBtn.userInteractionEnabled=YES;
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
                
            }
            [cellstory1 addSubview:likeBtn];
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cellstory1 addSubview:dislikeBtn];
            
            
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"Comment.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(129,107,62,43)];
            [commentButton addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory1.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"Share.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [shareButton setFrame:CGRectMake(191,107,62,43)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory1.contentView addSubview:shareButton];
            
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"Settings.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(253,107,62,43)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory1.contentView addSubview:settingsButton];
            
            UIButton *moreButtonS=nil;
            moreButtonS = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonS.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonS setBackgroundImage:more forState:UIControlStateNormal];
            moreButtonS.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [moreButtonS setFrame:CGRectMake(285,55,20,4)];
            [moreButtonS addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory1.contentView addSubview:moreButtonS];
            ce=cellstory1;
            
        }
        else if(([comArray count]==3))
        {
            
            CGFloat x = [UIScreen mainScreen].bounds.origin.x;
            UIView *backgroundCellView = [[UIView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 180)];
            backgroundCellView.backgroundColor = [UIColor clearColor];
            UIImageView *backimage = [[UIImageView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 180)];
            backimage.backgroundColor = [UIColor whiteColor];
            [backgroundCellView addSubview:backimage];
            cellstory2.backgroundView = backgroundCellView;
            cellstory2.backgroundColor = [UIColor clearColor];
            NSLog(@"helllloooo");
            
            if (cellstory2 == nil)
            {
                cellstory2 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:story2] autorelease];
                
            }
            
            for(UIView *v in [cellstory2.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFTweetLabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFLabelUsername class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIImageView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIButton class]])
                    [v removeFromSuperview];
            }

            
            introw=indexPath.row+1;
            tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,5,300,20)];
            tweetLabel.numberOfLines=0;
            tweetLabel.textColor=[UIColor blackColor];
            tweetLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:12];
            NSString *smile2=[login smilyString:[dataUpdate objectAtIndex:in]];
            [tweetLabel setText:smile2];
            [tweetLabel setLinksEnabled:TRUE];
            tweetLabel.clipsToBounds=YES;
            [cellstory2.contentView addSubview:tweetLabel];
            
            
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,35,40,40)];
            imageView2.layer.cornerRadius = 20;
            imageView2.clipsToBounds = YES;
            imageView2.image=[UIImage imageWithData:imageData];
            [cellstory2.contentView addSubview:imageView2];
            
            
            NSLog(@"valid");
            
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,35,100,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
            nameLabel.textColor = [UIColor clearColor];
            NSString *smile3=getusername;
            [nameLabel setText:smile3];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.clipsToBounds=YES;
            
            UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 137, 310, 43)];
            [cellstory2 addSubview:buttonsBack];
            buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
            
            [cellstory2.contentView addSubview:nameLabel];
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,40,100,10)];
            placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cellstory2.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,60,200,10)];
            timeLabel.textColor=[UIColor blackColor];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentLeft;
            [cellstory2.contentView addSubview:timeLabel];
            
            NSString *smile;
            IFTweetLabel *descriptionLabel1 = [[IFTweetLabel alloc] initWithFrame:CGRectMake(20,86,280,20)];
            descriptionLabel1.numberOfLines=0;
            descriptionLabel1.textColor = [UIColor blackColor];
            descriptionLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel1.clipsToBounds=YES;
            
            IFTweetLabel *descriptionLabel2 = [[IFTweetLabel alloc]initWithFrame:CGRectMake(20,101,280,20)];
            descriptionLabel2.numberOfLines=0;
            descriptionLabel2.textColor = [UIColor blackColor];
            descriptionLabel2.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel2.clipsToBounds=YES;
            
            NSMutableAttributedString *attributed;
            
            if ([comArray count]==3) {
                attributed=[login attibutedUsername:[comArray objectAtIndex:0]];
                smile=[login smilyString:[comArray objectAtIndex:0]];
                [descriptionLabel1 setText:smile];
                [descriptionLabel1 setLinksEnabled:TRUE];
                attributed=[login attibutedUsername:[comArray objectAtIndex:1]];
                smile=[login smilyString:[comArray objectAtIndex:1]];
                [descriptionLabel2 setText:smile];
                [descriptionLabel2 setLinksEnabled:TRUE];
            }
            
            [cellstory2 addSubview:descriptionLabel1];
            [cellstory2 addSubview:descriptionLabel2];
            
            UILabel *likeShow = nil;
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,167,43,10)] autorelease];
            likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:7.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cellstory2 addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,167,43,10)] autorelease];
            dislikeShow.textColor= [[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:7.0];
            dislikeShow.tag = lbltag;
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cellstory2 addSubview:dislikeShow];
            
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,167,43,10)] autorelease];
            commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:7.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cellstory2 addSubview:commentShow];
           
            //******************************************************************************
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            likeBtn.frame=CGRectMake(5,137,62,43);
            [likeBtn addTarget:self action:@selector(likeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[likesNameArray objectAtIndex:indexPath.row]);
            
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            dislikeBtn.frame=CGRectMake(80,135,44,30);
            [dislikeBtn addTarget:self action:@selector(dislikeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[dislikesNameArray objectAtIndex:indexPath.row]);

            
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                dislikeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
                [dislikeBtn setBackgroundImage:minusOut forState:UIControlStateNormal];
            }
            else
            {
                dislikeBtn.userInteractionEnabled=YES;
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cellstory2 addSubview:likeBtn];
          
            
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                likeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
                [likeBtn setBackgroundImage:plusOut forState:UIControlStateNormal];
            }
            else
            {
                likeBtn.userInteractionEnabled=YES;
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            [cellstory2 addSubview:likeBtn];
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cellstory2 addSubview:dislikeBtn];
            
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"Comment.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(129,137,62,43)];
            [commentButton addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory2.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"Share.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [shareButton setFrame:CGRectMake(191,137,62,43)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory2.contentView addSubview:shareButton];
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"Settings.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(253,137,62,43)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory2.contentView addSubview:settingsButton];
            
            UIButton *moreButtonS=nil;
            moreButtonS = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonS.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonS setBackgroundImage:more forState:UIControlStateNormal];
            moreButtonS.titleLabel.font = [UIFont systemFontOfSize:10.0];
             [moreButtonS setFrame:CGRectMake(285,55,20,4)];
            [moreButtonS addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellstory2.contentView addSubview:moreButtonS];
            ce=cellstory2;
        }
        else if(([comArray count]==4)||([comArray count]==5))
        {
            CGFloat x = [UIScreen mainScreen].bounds.origin.x;
            UIView *backgroundCellView2 = [[UIView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 220)];
            backgroundCellView2.backgroundColor = [UIColor clearColor];
            UIImageView *backimage2 = [[UIImageView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 220)];
            backimage2.backgroundColor = [UIColor whiteColor];
            [backgroundCellView2 addSubview:backimage2];
            cell2.backgroundView = backgroundCellView2;
            cell2.backgroundColor = [UIColor clearColor];
            
            NSLog(@"helllloooo");
            //imageViewUrl.image=nil;
            
            if (cell2 == nil)
            {
                cell2 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
                
            }
            
            for(UIView *v in [cell2.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFTweetLabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFLabelUsername class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIImageView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIButton class]])
                    [v removeFromSuperview];
            }

            
            Login *login=[[Login alloc]init];
            
            tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectMake(20,12,290,20)];
            tweetLabel.numberOfLines=0;
            tweetLabel.textColor=[UIColor blackColor];
            tweetLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:12];
            NSString *smile2=[login smilyString:[dataUpdate objectAtIndex:in]];
            [tweetLabel setText:smile2];
            [tweetLabel setLinksEnabled:TRUE];
            tweetLabel.clipsToBounds=YES;
            [cell2.contentView addSubview:tweetLabel];
            
            //int lbltag =indexPath.row;
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,35,40,40)];
            imageView2.layer.cornerRadius = 20;
            imageView2.clipsToBounds = YES;
            //imageView2.backgroundColor=[UIColor redColor];
            
            UIImage *image = [UIImage imageWithData:imageData];
            imageView2.image=image;
            [cell2.contentView addSubview:imageView2];
            NSLog(@"valid");
            
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,35,100,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
            nameLabel.textColor = [UIColor clearColor];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.text=getusername;
            [cell2.contentView addSubview:nameLabel];
            
            UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 177, 310, 43)];
            [cell2 addSubview:buttonsBack];
            buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,40,100,10)];
            placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cell2.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,60,200,10)];
            timeLabel.textColor=[UIColor blackColor];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentLeft;
            [cell2.contentView addSubview:timeLabel];
            
            NSString *smile;
            IFTweetLabel *descriptionLabel1 = [[IFTweetLabel alloc] initWithFrame:CGRectMake(20,86,280,20)];
            descriptionLabel1.numberOfLines=0;
            descriptionLabel1.textColor = [UIColor blackColor];
            descriptionLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel1.clipsToBounds=YES;
            
            IFTweetLabel *descriptionLabel2 = [[IFTweetLabel alloc]initWithFrame:CGRectMake(20,101,280,20)];
            descriptionLabel2.numberOfLines=0;
            descriptionLabel2.textColor = [UIColor blackColor];
            descriptionLabel2.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel2.clipsToBounds=YES;
            
            IFTweetLabel *descriptionLabel3 = [[IFTweetLabel alloc]initWithFrame:CGRectMake(20,116,280,20)];
            descriptionLabel3.numberOfLines=0;
            descriptionLabel3.textColor = [UIColor blackColor];
            descriptionLabel3.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel3.clipsToBounds=YES;
            
            IFTweetLabel *descriptionLabel4 = [[IFTweetLabel alloc]initWithFrame:CGRectMake(20,131,280,20)];
            descriptionLabel4.numberOfLines=0;
            descriptionLabel4.textColor = [UIColor blackColor];
            descriptionLabel4.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel4.clipsToBounds=YES;
            
            smile=[login smilyString:[comArray objectAtIndex:0]];
            [descriptionLabel1 setText:smile];
            [descriptionLabel1 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:1]];
            [descriptionLabel2 setText:smile];
            [descriptionLabel2 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:2]];
            [descriptionLabel3 setText:smile];
            [descriptionLabel3 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:3]];
            [descriptionLabel4 setText:smile];
            [descriptionLabel4 setLinksEnabled:TRUE];
            
            [cell2 addSubview:descriptionLabel1];
            [cell2 addSubview:descriptionLabel2];
            [cell2 addSubview:descriptionLabel3];
            [cell2 addSubview:descriptionLabel4];
            
            
            UILabel *likeShow = nil;
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,207,43,10)] autorelease];
            likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:7.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cell2 addSubview:likeShow];
            
            UILabel *dislikeShow = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,207,43,10)] autorelease];
            dislikeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:7.0];
            dislikeShow.tag = lbltag;
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cell2 addSubview:dislikeShow];
            
            
            UILabel *commentShow = nil;
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,207,43,10)] autorelease];
            commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:7.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cell2 addSubview:commentShow];
           
            //******************************************************************************
            
            UIButton *likeBtn = nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            likeBtn.frame=CGRectMake(5,177,60,43);
            
            [likeBtn addTarget:self action:@selector(likeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cell2 addSubview:likeBtn];
            
            UIButton *dislikeBtn = nil;
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            dislikeBtn.frame=CGRectMake(67,177,60,43);
            [dislikeBtn addTarget:self action:@selector(dislikeFn:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"confermation %@",[dislikesNameArray objectAtIndex:indexPath.row]);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            [cell2 addSubview:likeBtn];
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cell2 addSubview:dislikeBtn];
            
            
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"Comment.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(129,177,60,43)];
            [commentButton addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell2.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"Share.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(191,177,60,43)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell2.contentView addSubview:shareButton];
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"Settings.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(253,177,60,43)];
            settingsButton.tag=lbltag;
            [settingsButton addTarget:self action:@selector(settingButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell2.contentView addSubview:settingsButton];
            
            UIButton *moreButtonS=nil;
            moreButtonS = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonS.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonS setBackgroundImage:more forState:UIControlStateNormal];
            moreButtonS.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [moreButtonS setFrame:CGRectMake(285,55,20,4)];
            [moreButtonS addTarget:self action:@selector(moreButtonStoryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell2.contentView addSubview:moreButtonS];
            ce=cell2;
            
        }
    }
    //if([content isEqualToString:@"image"])
    else if([typeString isEqualToString:@"video"])
    {
        NSLog(@"hiiiiiiiiii videoooooo");
        NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
        NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
        if (([commentStr isEqualToString:@"No Comments"])||([comArray count]==2)){
            NSLog(@"helllloooo");
            if (cellvideo1 == nil)
            {
                cellvideo1 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:video1] autorelease];
                NSLog(@"index path in cell3 %i",indexPath.row);
            }
            CGFloat x = [UIScreen mainScreen].bounds.origin.x;
            UIView *backgroundCellView2 = [[UIView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 450)];
            backgroundCellView2.backgroundColor = [UIColor clearColor];
            UIImageView *backimage2 = [[UIImageView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 450)];
            backimage2.backgroundColor = [UIColor whiteColor];
            [backgroundCellView2 addSubview:backimage2];
            cellvideo1.backgroundView = backgroundCellView2;
            cellvideo1.backgroundColor = [UIColor clearColor];
            
            for(UIView *v in [cellvideo1.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFTweetLabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFLabelUsername class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIImageView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIButton class]])
                    [v removeFromSuperview];
            }

            NSString *caption2=[login smilyString:[dataUpdate2 objectAtIndex:in]];
            tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,320,295,20)];
            tweetLabel.numberOfLines=2;
            tweetLabel.textColor=[UIColor blackColor];
            tweetLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:12];
            [tweetLabel setText:caption2];
            [tweetLabel setLinksEnabled:TRUE];
            tweetLabel.clipsToBounds=YES;
            
            introw=indexPath.row+1;
            videoUrl=[dataUpdate objectAtIndex:in];
           
            playButton.userInteractionEnabled=YES;
            playButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *play = [UIImage imageNamed:@"playbutton.png"];
            [playButton setImage:play forState:UIControlStateNormal];
            
            [playButton setBackgroundImage:play forState:UIControlStateNormal];
            [playButton showsTouchWhenHighlighted];
            playButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [playButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            playButton.tintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:0.8];
            
            [playButton setFrame:CGRectMake(5,0,310,310)];
            [playButton addTarget:self action:@selector(playButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            UIView *imageShadow = [[UIView alloc]initWithFrame:CGRectMake(10, 0, 300, 310)];
            CALayer *layer = imageShadow.layer;
            layer.shadowOffset = CGSizeMake(0, 1);
            layer.shadowColor = [[UIColor blackColor] CGColor];
            layer.shadowRadius = 2.0f;
            layer.shadowOpacity = 0.60f;
            layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];
            
            NSURL *video=[NSURL URLWithString:videoUrl];
            AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL:video options:nil];
            AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
            generate1.appliesPreferredTrackTransform = YES;
            NSError *err = NULL;
            CMTime time = CMTimeMake(1, 2);
            CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
            UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
            
            
            UIImage *croppedImage;
            
            CGRect croppedRect;
            
            croppedRect = CGRectMake(0,0, 400, 400);
            
            CGImageRef tmp = CGImageCreateWithImageInRect([one CGImage], croppedRect);
            
            croppedImage = [UIImage imageWithCGImage:tmp];
            
            CGImageRelease(tmp);
            
            
            
            NSData *CroppedImageData = UIImageJPEGRepresentation(croppedImage, 0.7);
            
            one = [UIImage imageWithData:CroppedImageData];
            
            [playButton setBackgroundImage:one forState:UIControlStateNormal];
            [playButton setContentMode:UIViewContentModeCenter];
            [cellvideo1.contentView addSubview:imageShadow];
            [cellvideo1.contentView addSubview:playButton];

            //int lbltag =indexPath.row;
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,328,40,40)];
            imageView2.layer.cornerRadius = 20;
            imageView2.clipsToBounds = YES;
            imageView2.image=[UIImage imageWithData:imageData];
            [cellvideo1.contentView addSubview:imageView2];
            
            
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,325,200,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
            nameLabel.textColor = [UIColor clearColor];
            NSString *smile3=getusername;
            [nameLabel setText:smile3];
            [nameLabel setLinksEnabled:TRUE];
            [cellvideo1.contentView addSubview:nameLabel];
            
            UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 407, 310, 43)];
            [cellvideo1 addSubview:buttonsBack];
            buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
            
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,335,100,10)];
            placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cellvideo1.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,350,200,10)];
            timeLabel.textColor=[[UIColor alloc] initWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:1];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentLeft;
            [cellvideo1.contentView addSubview:timeLabel];
            
            //NSString *smile;
            //IFTweetLabel *descriptionLabel1=[[IFTweetLabel alloc]initWithFrame:CGRectMake(15,380,278,20)];
            //descriptionLabel1.numberOfLines=0;
            //descriptionLabel1.textColor = [UIColor blackColor];
            //descriptionLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            //NSString *commentStr=[fourCommentArray objectAtIndex:indexPath.row];
            //NSArray *comArray=[commentStr componentsSeparatedByString:@"-END_OF_DATA-LINE-" ];
            //if ([comArray count]==2) {
            //    smile=[login smilyString:[comArray objectAtIndex:0]];
            //    [descriptionLabel1 setText:smile];
            //    [descriptionLabel1 setLinksEnabled:TRUE];
            //}
            //[cellvideo1.contentView addSubview:descriptionLabel1];
            
            if(caption2.length > 0){
                [cellvideo1.contentView addSubview:tweetLabel];
                imageView2.frame = CGRectMake(15,348,40,40);
                nameLabel.frame = CGRectMake(65,345,200,30);
                placeLabel.frame = CGRectMake(205,355,100,10);
                timeLabel.frame = CGRectMake(65,370,100,10);
            }else {
                imageView2.frame = CGRectMake(15,318,40,40);
                nameLabel.frame = CGRectMake(65,315,200,30);
                placeLabel.frame = CGRectMake(205,320,100,10);
                timeLabel.frame = CGRectMake(65,340,100,10);
            }
            
            UILabel *likeShow = nil;
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,437,43,10)] autorelease];
            likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:7.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cellvideo1 addSubview:likeShow];
            
            
            UILabel *dislikeShow = nil;
            UIButton *dislikeBtn = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,437,43,10)] autorelease];
            dislikeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:7.0];
            dislikeShow.tag = lbltag;
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cellvideo1 addSubview:dislikeShow];
            
            UILabel *commentShow = nil;
            NSLog(@"likes from in %@",[likearray objectAtIndex:in]);
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,437,43,10)] autorelease];
            commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:7.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cellvideo1 addSubview:commentShow];
            //******************************************************************************
            UIButton *likeBtn=nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            [likeBtn addTarget:self action:@selector(likeVideo:) forControlEvents:UIControlEventTouchUpInside];
            likeBtn.frame=CGRectMake(5,407,62,43);
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            NSLog(@"Confimation returned %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cellvideo1 addSubview:likeBtn];
            
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            [dislikeBtn addTarget:self action:@selector(dislikeVideo:) forControlEvents:UIControlEventTouchUpInside];
            dislikeBtn.frame=CGRectMake(67,407,62,43);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cellvideo1 addSubview:dislikeBtn];
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"Comment.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(129,407,62,43)];
            [commentButton addTarget:self action:@selector(moreButtonVideoClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellvideo1.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"Share.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(191,407,62,43)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellvideo1.contentView addSubview:shareButton];
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"Settings.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(253,407,62,43)];
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            settingsButton.tag=lbltag;
            [cellvideo1.contentView addSubview:settingsButton];
            
            
            /*UIButton *moreButtonV=nil;
            moreButtonV = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonV.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonV setBackgroundImage:more forState:UIControlStateNormal];
            
            [moreButtonV setFrame:CGRectMake(285,407,20,4)];
            [moreButtonV addTarget:self action:@selector(moreButtonVideoClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellvideo1.contentView addSubview:moreButtonV];*/
            
            ce=cellvideo1;
            
        }
        else if([comArray count]==3)
        {
            
            if (cellvideo2 == nil)
            {
                cellvideo2 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:video2] autorelease];
                NSLog(@"index path in cell3 %i",indexPath.row);
            }
            CGFloat x = [UIScreen mainScreen].bounds.origin.x;
            UIView *backgroundCellView = [[UIView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 485)];
            backgroundCellView.backgroundColor = [UIColor clearColor];
            UIImageView *backimage = [[UIImageView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 485)];
            backimage.backgroundColor = [UIColor whiteColor];
            [backgroundCellView addSubview:backimage];
            cellvideo2.backgroundView = backgroundCellView;
            cellvideo2.backgroundColor = [UIColor clearColor];
            for(UIView *v in [cellvideo2.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFTweetLabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFLabelUsername class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIImageView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIButton class]])
                    [v removeFromSuperview];
            }
            
            introw=indexPath.row+1;
            videoUrl=[dataUpdate objectAtIndex:in];
         
            playButton.userInteractionEnabled=YES;
            playButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *play = [UIImage imageNamed:@"playbutton.png"];
            [playButton setImage:play forState:UIControlStateNormal];
            
            [playButton setBackgroundImage:play forState:UIControlStateNormal];
            [playButton showsTouchWhenHighlighted];
            playButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [playButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            playButton.tintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:0.8];
            
            [playButton setFrame:CGRectMake(0,0,320,320)];
            [playButton addTarget:self action:@selector(playButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            UIView *imageShadow = [[UIView alloc]initWithFrame:CGRectMake(10, 0, 300, 310)];
            CALayer *layer = imageShadow.layer;
            layer.shadowOffset = CGSizeMake(0, 1);
            layer.shadowColor = [[UIColor blackColor] CGColor];
            layer.shadowRadius = 2.0f;
            layer.shadowOpacity = 0.60f;
            layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];
            
            NSURL *video=[NSURL URLWithString:videoUrl];
            AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL:video options:nil];
            AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
            generate1.appliesPreferredTrackTransform = YES;
            NSError *err = NULL;
            CMTime time = CMTimeMake(1, 2);
            CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
            UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
            
            
            UIImage *croppedImage;
            
            CGRect croppedRect;
            
            croppedRect = CGRectMake(0,0, 400, 400);
            
            CGImageRef tmp = CGImageCreateWithImageInRect([one CGImage], croppedRect);
            
            croppedImage = [UIImage imageWithCGImage:tmp];
            
            CGImageRelease(tmp);
            
            
            
            NSData *CroppedImageData = UIImageJPEGRepresentation(croppedImage, 0.7);
            
            one = [UIImage imageWithData:CroppedImageData];
            
            [playButton setBackgroundImage:one forState:UIControlStateNormal];
            [playButton setContentMode:UIViewContentModeCenter];
            [cellvideo2.contentView addSubview:imageShadow];
            [cellvideo2.contentView addSubview:playButton];
            
            NSString *caption2=[login smilyString:[dataUpdate2 objectAtIndex:in]];
            tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,320,295,20)];
            tweetLabel.numberOfLines=2;
            tweetLabel.textColor=[UIColor blackColor];
            tweetLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:12];
            [tweetLabel setText:caption2];
            [tweetLabel setLinksEnabled:TRUE];
            tweetLabel.clipsToBounds=YES;
    
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,328,40,40)];
            imageView2.layer.cornerRadius = 20;
            imageView2.clipsToBounds = YES;
            imageView2.image=[UIImage imageWithData:imageData];
            [cellvideo2.contentView addSubview:imageView2];
            
            
            
            [cellvideo2.contentView addSubview:imageView2];
            NSLog(@"valid");
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,325,200,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
            nameLabel.textColor = [UIColor clearColor];
            NSString *smile3=getusername;
            [nameLabel setText:smile3];
            [nameLabel setLinksEnabled:TRUE];
            [cellvideo2.contentView addSubview:nameLabel];
            
            UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 442, 310, 43)];
            [cellvideo2 addSubview:buttonsBack];
            buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,335,100,10)];
            placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cellvideo2.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,350,200,10)];
            timeLabel.textColor=[[UIColor alloc] initWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:1];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentLeft;
            [cellvideo2.contentView addSubview:timeLabel];
            
            NSString *smile;
            
            IFTweetLabel *descriptionLabel1=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,380,280,20)];
            descriptionLabel1.numberOfLines=0;
            descriptionLabel1.textColor = [UIColor blackColor];
            descriptionLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel1.clipsToBounds=YES;
            IFTweetLabel *descriptionLabel2=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,395,280,20)];
            descriptionLabel2.numberOfLines=0;
            descriptionLabel2.textColor = [UIColor blackColor];
            descriptionLabel2.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel2.clipsToBounds=YES;
            
            IFTweetLabel *descriptionLabel3=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,410,280,20)];
            descriptionLabel3.numberOfLines=0;
            descriptionLabel3.textColor = [UIColor blackColor];
            descriptionLabel3.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel3.clipsToBounds=YES;
            
            smile=[login smilyString:[comArray objectAtIndex:0]];
            [descriptionLabel1 setText:smile];
            [descriptionLabel1 setLinksEnabled:TRUE];
            
            smile=[login smilyString:[comArray objectAtIndex:1]];
            [descriptionLabel2 setText:smile];
            [descriptionLabel2 setLinksEnabled:TRUE];
            
            smile=[login smilyString:[comArray objectAtIndex:2]];
            [descriptionLabel3 setText:smile];
            [descriptionLabel3 setLinksEnabled:TRUE];
            
            
            [cellvideo2.contentView addSubview:descriptionLabel1];
            [cellvideo2.contentView addSubview:descriptionLabel2];
            [cellvideo2.contentView addSubview:descriptionLabel3];
            
            if(caption2.length > 0){
                [cellvideo2.contentView addSubview:tweetLabel];
                imageView2.frame = CGRectMake(15,348,40,40);
                nameLabel.frame = CGRectMake(65,345,200,30);
                placeLabel.frame = CGRectMake(205,355,100,10);
                timeLabel.frame = CGRectMake(65,370,100,10);
                
                descriptionLabel1.frame = CGRectMake(20,395,280,20);
                descriptionLabel2.frame = CGRectMake(20,410,280,20);
                descriptionLabel3.frame = CGRectMake(20,425,280,20);
                
            }else {
                imageView2.frame = CGRectMake(15,318,40,40);
                nameLabel.frame = CGRectMake(65,315,200,30);
                placeLabel.frame = CGRectMake(205,320,100,10);
                timeLabel.frame = CGRectMake(65,340,100,10);
                
                descriptionLabel1.frame = CGRectMake(20,380,280,20);
                descriptionLabel2.frame = CGRectMake(20,395,280,20);
                descriptionLabel3.frame = CGRectMake(20,410,280,20);
                
            }
            
            UILabel *likeShow = nil;
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,472,43,10)] autorelease];
            likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:7.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cellvideo2 addSubview:likeShow];
            
            
            UILabel *dislikeShow = nil;
            UIButton *dislikeBtn = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,472,43,10)] autorelease];
            dislikeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:7.0];
            dislikeShow.tag = lbltag;
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cellvideo2 addSubview:dislikeShow];
            
            UILabel *commentShow = nil;
            NSLog(@"likes from in %@",[likearray objectAtIndex:in]);
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,472,43,10)] autorelease];
            commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:7.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cellvideo2 addSubview:commentShow];
            //******************************************************************************
            UIButton *likeBtn=nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            [likeBtn addTarget:self action:@selector(likeVideo:) forControlEvents:UIControlEventTouchUpInside];
           likeBtn.frame=CGRectMake(5,442,62,43);
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            NSLog(@"Confimation returned %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cellvideo2 addSubview:likeBtn];
            
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            [dislikeBtn addTarget:self action:@selector(dislikeVideo:) forControlEvents:UIControlEventTouchUpInside];
            dislikeBtn.frame=CGRectMake(67,442,62,43);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cellvideo2 addSubview:dislikeBtn];
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"commentbutton2.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(129,442,62,43)];
            [commentButton addTarget:self action:@selector(moreButtonVideoClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellvideo2.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"share_button2.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(191,442,62,43)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cellvideo2.contentView addSubview:shareButton];
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"bottom-setting2.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(253,442,62,43)];
            [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            settingsButton.tag=lbltag;
            [cellvideo2.contentView addSubview:settingsButton];
            
            
            /*UIButton *moreButtonV=nil;
            moreButtonV = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonV.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonV setBackgroundImage:more forState:UIControlStateNormal];
            
            [moreButtonV setFrame:CGRectMake(285,310,20,4)];
            [moreButtonV addTarget:self action:@selector(moreButtonVideoClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell3.contentView addSubview:moreButtonV];*/
            
            ce=cellvideo2;
            
            
        }
        else if(([comArray count]==4)||([comArray count]==5))
        {
            if (cell3 == nil)
            {
                cell3 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier3] autorelease];
                NSLog(@"index path in cell3 %i",indexPath.row);
            }
            CGFloat x = [UIScreen mainScreen].bounds.origin.x;
            cell3.backgroundColor = [UIColor clearColor];
            UIView *backgroundCellView2 = [[UIView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 495)];
            backgroundCellView2.backgroundColor = [UIColor clearColor];
            UIImageView *backimage2 = [[UIImageView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 495)];
            backimage2.backgroundColor = [UIColor whiteColor];
            [backgroundCellView2 addSubview:backimage2];
            cell3.backgroundView = backgroundCellView2;
            cell3.backgroundColor = [UIColor clearColor];
            
            for(UIView *v in [cell3.contentView subviews])
            {
                if([v isKindOfClass:[UILabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFTweetLabel class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[IFLabelUsername class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIImageView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIView class]])
                    [v removeFromSuperview];
                
                if([v isKindOfClass:[UIButton class]])
                    [v removeFromSuperview];
            }
            
            videoUrl=[dataUpdate objectAtIndex:in];
            videoUrl=[dataUpdate objectAtIndex:in];
         
            playButton.userInteractionEnabled=YES;
            playButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *play = [UIImage imageNamed:@"playbutton.png"];
            [playButton setImage:play forState:UIControlStateNormal];
            
            [playButton setBackgroundImage:play forState:UIControlStateNormal];
            [playButton showsTouchWhenHighlighted];
            playButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            [playButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
            playButton.tintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:0.8];
            
            [playButton setFrame:CGRectMake(0,0,320,320)];
            [playButton addTarget:self action:@selector(playButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            UIView *imageShadow = [[UIView alloc]initWithFrame:CGRectMake(10, 0, 300, 310)];
            CALayer *layer = imageShadow.layer;
            layer.shadowOffset = CGSizeMake(0, 1);
            layer.shadowColor = [[UIColor blackColor] CGColor];
            layer.shadowRadius = 2.0f;
            layer.shadowOpacity = 0.60f;
            layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];
            
            NSURL *video=[NSURL URLWithString:videoUrl];
            AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL:video options:nil];
            AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
            generate1.appliesPreferredTrackTransform = YES;
            NSError *err = NULL;
            CMTime time = CMTimeMake(1, 2);
            CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
            UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
            
            
            UIImage *croppedImage;
            
            CGRect croppedRect;
            
            croppedRect = CGRectMake(0,0, 400, 400);
            
            CGImageRef tmp = CGImageCreateWithImageInRect([one CGImage], croppedRect);
            
            croppedImage = [UIImage imageWithCGImage:tmp];
            
            CGImageRelease(tmp);
            
            
            
            NSData *CroppedImageData = UIImageJPEGRepresentation(croppedImage, 0.7);
            
            one = [UIImage imageWithData:CroppedImageData];
            
            [playButton setBackgroundImage:one forState:UIControlStateNormal];
            [playButton setContentMode:UIViewContentModeCenter];
            [cell3.contentView addSubview:imageShadow];
            [cell3.contentView addSubview:playButton];
            
            NSString *caption2=[login smilyString:[dataUpdate2 objectAtIndex:in]];
            tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectMake(15,320,295,20)];
            tweetLabel.numberOfLines=2;
            tweetLabel.textColor=[UIColor blackColor];
            tweetLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:12];
            [tweetLabel setText:caption2];
            [tweetLabel setLinksEnabled:TRUE];
            tweetLabel.clipsToBounds=YES;

            
            UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,328,40,40)];
            imageView2.layer.cornerRadius = 20;
            imageView2.clipsToBounds = YES;
            
            UIImage *image = [UIImage imageWithData:imageData];
            imageView2.image=image;
            [cell3.contentView addSubview:imageView2];
            
            NSLog(@"valid");
            
            IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,325,200,30)];
            nameLabel.numberOfLines=0;
            //nameLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
            nameLabel.textColor = [UIColor clearColor];
            [nameLabel setLinksEnabled:TRUE];
            nameLabel.text=getusername;
            [cell3.contentView addSubview:nameLabel];
            [cell3.contentView addSubview:nameLabel];
            UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 452, 310, 43)];
            [cell3 addSubview:buttonsBack];
            buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
            
            UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,335,100,10)];
            placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
            placeLabel.text=[locationArray objectAtIndex:in];
            placeLabel.textAlignment=NSTextAlignmentRight;
            [cell3.contentView addSubview:placeLabel];
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,350,200,10)];
            timeLabel.textColor=[[UIColor alloc] initWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:1];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
            timeLabel.text=[timeArray objectAtIndex:in];
            timeLabel.textAlignment=NSTextAlignmentLeft;
            [cell3.contentView addSubview:timeLabel];

            
            NSString *smile;
            
            IFTweetLabel *descriptionLabel1=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,380,280,20)];
            descriptionLabel1.numberOfLines=0;
            descriptionLabel1.textColor = [UIColor blackColor];
            descriptionLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel1.clipsToBounds=YES;
            IFTweetLabel *descriptionLabel2=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,395,280,20)];
            descriptionLabel2.numberOfLines=0;
            descriptionLabel2.textColor = [UIColor blackColor];
            descriptionLabel2.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel2.clipsToBounds=YES;
            IFTweetLabel *descriptionLabel3=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,410,280,20)];
            descriptionLabel3.numberOfLines=0;
            descriptionLabel3.textColor = [UIColor blackColor];
            descriptionLabel3.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel3.clipsToBounds=YES;
            
            IFTweetLabel *descriptionLabel4=[[IFTweetLabel alloc]initWithFrame:CGRectMake(20,425,280,20)];
            descriptionLabel4.numberOfLines=0;
            descriptionLabel4.textColor = [UIColor blackColor];
            descriptionLabel4.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
            descriptionLabel4.clipsToBounds=YES;

            smile=[login smilyString:[comArray objectAtIndex:0]];
            [descriptionLabel1 setText:smile];
            [descriptionLabel1 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:1]];
            [descriptionLabel2 setText:smile];
            [descriptionLabel2 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:2]];
            [descriptionLabel3 setText:smile];
            [descriptionLabel3 setLinksEnabled:TRUE];
            smile=[login smilyString:[comArray objectAtIndex:3]];
            [descriptionLabel4 setText:smile];
            [descriptionLabel4 setLinksEnabled:TRUE];
            
            [cell3.contentView addSubview:descriptionLabel1];
            [cell3.contentView addSubview:descriptionLabel2];
            [cell3.contentView addSubview:descriptionLabel3];
            [cell3.contentView addSubview:descriptionLabel4];
            
            if(caption2.length > 0){
                [cell3.contentView addSubview:tweetLabel];
                imageView2.frame = CGRectMake(15,348,40,40);
                nameLabel.frame = CGRectMake(65,345,200,30);
                placeLabel.frame = CGRectMake(205,355,100,10);
                timeLabel.frame = CGRectMake(65,370,100,10);
                
                descriptionLabel1.frame = CGRectMake(20,395,280,20);
                descriptionLabel2.frame = CGRectMake(20,410,280,20);
                descriptionLabel3.frame = CGRectMake(20,425,280,20);
                descriptionLabel4.frame = CGRectMake(20,440,280,20);
            }else {
                imageView2.frame = CGRectMake(15,318,40,40);
                nameLabel.frame = CGRectMake(65,315,200,30);
                placeLabel.frame = CGRectMake(205,320,100,10);
                timeLabel.frame = CGRectMake(65,340,100,10);
                
                descriptionLabel1.frame = CGRectMake(20,380,280,20);
                descriptionLabel2.frame = CGRectMake(20,395,280,20);
                descriptionLabel3.frame = CGRectMake(20,410,280,20);
                descriptionLabel4.frame = CGRectMake(20,425,280,20);
            }
            
            UILabel *likeShow = nil;
            likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,482,43,10)] autorelease];
            likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            likeShow.text = [NSString stringWithFormat:@"%d",[[likearray objectAtIndex:indexPath.row] intValue]];
            likeShow.font=[UIFont systemFontOfSize:7.0];
            likeShow.tag = lbltag;
            likeShow.textAlignment=NSTextAlignmentCenter;
            [cell3 addSubview:likeShow];
            
            
            UILabel *dislikeShow = nil;
            UIButton *dislikeBtn = nil;
            dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,482,43,10)] autorelease];
            dislikeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            dislikeShow.text = [NSString stringWithFormat:@"%d",[[dislikearray objectAtIndex:indexPath.row] intValue]];
            dislikeShow.font=[UIFont systemFontOfSize:7.0];
            dislikeShow.tag = lbltag;
            dislikeShow.textAlignment=NSTextAlignmentCenter;
            [cell3 addSubview:dislikeShow];
            
            UILabel *commentShow = nil;
            NSLog(@"likes from in %@",[likearray objectAtIndex:in]);
            commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,482,43,10)] autorelease];
            commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
            commentShow.text = [NSString stringWithFormat:@"%d",[[commentCounts objectAtIndex:indexPath.row] intValue]];
            commentShow.font=[UIFont systemFontOfSize:7.0];
            commentShow.tag = lbltag;
            commentShow.textAlignment=NSTextAlignmentCenter;
            [cell3 addSubview:commentShow];
            //******************************************************************************
            UIButton *likeBtn=nil;
            likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            likeBtn.tag = indexPath.row+1;
            [likeBtn addTarget:self action:@selector(likeVideo:) forControlEvents:UIControlEventTouchUpInside];
           likeBtn.frame=CGRectMake(5,452,62,43);
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            NSLog(@"Confimation returned %@",[likesNameArray objectAtIndex:indexPath.row]);
            if([[likesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                likeBtn.userInteractionEnabled=NO;
                [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
            }
            else
            {
                [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
            }
            [cell3 addSubview:likeBtn];
            
            
            
            dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            dislikeBtn.tag = indexPath.row+1;
            [dislikeBtn addTarget:self action:@selector(dislikeVideo:) forControlEvents:UIControlEventTouchUpInside];
            dislikeBtn.frame=CGRectMake(67,452,62,43);
            if([[dislikesNameArray objectAtIndex:indexPath.row] isEqualToString:@"Yes"])
            {
                dislikeBtn.userInteractionEnabled=NO;
                [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
            }
            else
            {
                [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
            }
            NSLog(@"Serial number of content in cell %@",[snoArray objectAtIndex:indexPath.row]);
            [cell3 addSubview:dislikeBtn];
            
            UIButton *commentButton=nil;
            commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            commentButton.tag = indexPath.row+1;
            UIImage *comment = [UIImage imageNamed:@"Comment.png"];
            [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
            [commentButton setFrame:CGRectMake(129,452,62,43)];
            [commentButton addTarget:self action:@selector(moreButtonVideoClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell3.contentView addSubview:commentButton];
            
            
            UIButton *shareButton=nil;
            shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            shareButton.tag = indexPath.row+1;
            UIImage *share = [UIImage imageNamed:@"Share.png"];
            [shareButton setBackgroundImage:share forState:UIControlStateNormal];
            shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
            
            [shareButton setFrame:CGRectMake(191,452,62,43)];
            [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell3.contentView addSubview:shareButton];
            
            UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            UIImage *setting = [UIImage imageNamed:@"Settings.png"];
            [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
            [settingsButton setTitle:@"" forState:UIControlStateNormal];
            [settingsButton setFrame:CGRectMake(253,452,62,43)];
            [settingsButton addTarget:self action:@selector(settingButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            settingsButton.tag=lbltag;
            [cell3.contentView addSubview:settingsButton];
            
            
            /*UIButton *moreButtonV=nil;
            moreButtonV = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            moreButtonV.tag = indexPath.row+1;
            UIImage *more = [UIImage imageNamed:@"seeMore.png"];
            [moreButtonV setBackgroundImage:more forState:UIControlStateNormal];
            
             [moreButtonV setFrame:CGRectMake(285,310,20,4)];
            [moreButtonV addTarget:self action:@selector(moreButtonVideoClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell3.contentView addSubview:moreButtonV];*/
            
            
            ce=cell3;
            
        }
    }
    else if ([typeString isEqualToString:@"more"]) {
		
		cell4 = [tableView dequeueReusableCellWithIdentifier:moreCellId];
		if (cell4 == nil) {
			cell4 = [[[UITableViewCell alloc]
                      initWithStyle:UITableViewCellStyleDefault
                      reuseIdentifier:moreCellId] autorelease];
		}
        indicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150,5, 30, 30)];
        [indicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
        [indicator setColor:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1]];
     
        cell4.backgroundColor = [UIColor clearColor];
        [cell4 addSubview:indicator];
        [indicator stopAnimating];
        [indicator setHidden:YES];
        ce=cell4;
		
	}
    
    
    return ce;
    
}
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //[self refreshData];
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == [dataUpdate count]-1)
    {
        NSLog(@"Dragged table ************");
        [self refreshData];
        
    }
}
-(IBAction)settingButtonClicked:(id)sender
{
    self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:3];
    
}

//***********************************Like Methods****************************************************
-(void)likeFn:(UIButton*)btnClicked
{
    if([[likesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else
    {
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=story&username=%@&action=liked",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        int likeCount=[[json valueForKey:@"count"] intValue];
        [likearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [likesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        [mytable reloadData];
    }
    
}
-(void)likeImage:(UIButton*)btnClicked
{
    if([[likesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else
    {
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=images&username=%@&action=liked",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        int likeCount=[[json valueForKey:@"count"] intValue];
        [likearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [likesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        [mytable reloadData];
    }
}
-(void)likeVideo:(UIButton*)btnClicked
{
    if([[likesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else
    {
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=video&username=%@&action=liked",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        int likeCount=[[json valueForKey:@"count"] intValue];
        [likearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [likesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        [mytable reloadData];
    }
}

//***********************************DisLike Methods*********************************************
-(void)dislikeFn:(UIButton*)btnClicked
{
    if([[dislikesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=story&username=%@&action=dislike",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        int likeCount=[[json valueForKey:@"count"] intValue];
        [dislikearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [dislikesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        [mytable reloadData];
    }
}
-(void)dislikeImage:(UIButton*)btnClicked
{
    if([[dislikesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=images&username=%@&action=dislike",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        int likeCount=[[json valueForKey:@"count"] intValue];
        [dislikearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [dislikesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        [mytable reloadData];
    }
}
-(void)dislikeVideo:(UIButton*)btnClicked
{
    if([[dislikesNameArray objectAtIndex:btnClicked.tag-1] isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend = [snoArray objectAtIndex:btnClicked.tag-1];
        NSLog(@"Serial number of content in lyk action %@",[snoArray objectAtIndex:btnClicked.tag-1]);
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=video&username=%@&action=dislike",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        int likeCount=[[json valueForKey:@"count"] intValue];
        [dislikearray replaceObjectAtIndex:btnClicked.tag-1 withObject:[NSString stringWithFormat:@"%d",likeCount]];
        [dislikesNameArray replaceObjectAtIndex:btnClicked.tag-1 withObject:@"Yes"];
        [mytable reloadData];
    }
}

//********************* SEE MORE Mehthods *********************************************************
-(IBAction)moreButtonImageClicked:(UIButton*)btnClicked
{
    NSLog(@"Content Type %@",[dataType objectAtIndex:btnClicked.tag-1]);
    NSString *setImageData = [dataUpdate objectAtIndex:btnClicked.tag-1];
    
    imageDetailView *imageDetail=[self.storyboard instantiateViewControllerWithIdentifier:@"imageDetail"];
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    navBar.hidden=false;
    imageDetail.hidesBottomBarWhenPushed = YES;
    imageDetail.getImageData=setImageData;
    imageDetail.getUserImageData=meImageStr;
    imageDetail.getName=getusername;
    imageDetail.getSno=[snoArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getIfLikes=[likesNameArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getLikes=[likearray objectAtIndex:btnClicked.tag-1];
    imageDetail.getIfDisLikes=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getDisLikes=[dislikearray objectAtIndex:btnClicked.tag-1];
    imageDetail.getComments=[commentCounts objectAtIndex:btnClicked.tag-1];
    imageDetail.getType=[dataType objectAtIndex:btnClicked.tag-1];
    imageDetail.getDislikeConferm=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getLikeConferm=[likesNameArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getLocation=[locationArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getTime=[timeArray objectAtIndex:btnClicked.tag-1];
    imageDetail.getRowIndex=[NSString stringWithFormat:@"%i",btnClicked.tag-1];
    imageDetail.getTabId=@"profile";
    [self.navigationController pushViewController:imageDetail animated:YES];
    
}
-(IBAction)moreButtonStoryClicked:(UIButton*)btnClicked
{
    NSLog(@"Content Type %@",[dataType objectAtIndex:btnClicked.tag-1]);
    NSString *setStoryString = [dataUpdate objectAtIndex:btnClicked.tag-1];
    
    storyDetailView *storyDetail=[self.storyboard instantiateViewControllerWithIdentifier:@"storyDetail"];
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    navBar.hidden=false;
    storyDetail.hidesBottomBarWhenPushed = YES;
    storyDetail.getStory=setStoryString;
    storyDetail.getUserImageData=meImageStr;
    storyDetail.getName=getusername;
    storyDetail.getSno=[snoArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getIfLikes=[likesNameArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getLikes=[likearray objectAtIndex:btnClicked.tag-1];
    storyDetail.getIfDisLikes=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getDisLikes=[dislikearray objectAtIndex:btnClicked.tag-1];
    storyDetail.getComments=[commentCounts objectAtIndex:btnClicked.tag-1];
    storyDetail.getType=[dataType objectAtIndex:btnClicked.tag-1];
    storyDetail.getDislikeConferm=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getLikeConferm=[likesNameArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getLocation=[locationArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getTime=[timeArray objectAtIndex:btnClicked.tag-1];
    storyDetail.getRowIndex=[NSString stringWithFormat:@"%i",btnClicked.tag-1];
    storyDetail.getTabId=@"profile";
    [self.navigationController pushViewController:storyDetail animated:YES];
}
-(IBAction)moreButtonVideoClicked:(UIButton*)btnClicked
{
    videoDetailView *videoDetail=[self.storyboard instantiateViewControllerWithIdentifier:@"videoDetail"];
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    navBar.hidden=false;
    videoDetail.hidesBottomBarWhenPushed = YES;
    videoDetail.videoUrl=videoUrl;
    videoDetail.getUserImageData=meImageStr;
    videoDetail.getName=getusername;
    videoDetail.getSno=[snoArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getIfLikes=[likesNameArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getLikes=[likearray objectAtIndex:btnClicked.tag-1];
    videoDetail.getIfDisLikes=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getDisLikes=[dislikearray objectAtIndex:btnClicked.tag-1];
    videoDetail.getComments=[commentCounts objectAtIndex:btnClicked.tag-1];
    videoDetail.getType=[dataType objectAtIndex:btnClicked.tag-1];
    videoDetail.getDislikeConferm=[dislikesNameArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getLikeConferm=[likesNameArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getLocation=[locationArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getTime=[timeArray objectAtIndex:btnClicked.tag-1];
    videoDetail.getRowIndex=[NSString stringWithFormat:@"%i",btnClicked.tag-1];
    videoDetail.getTabId=@"profile";
    [self.navigationController pushViewController:videoDetail animated:YES];
}
//******************************************************************************************
- (void)reloadRowsAtIndexPaths:(NSArray *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation
{
    NSLog(@"cell reloaded");
}

-(void)shareButtonClicked:(UIButton*)btnClicked{
    NSLog(@"data type %@",[dataType objectAtIndex:btnClicked.tag-1]);
    NSString *type=[dataType objectAtIndex:btnClicked.tag-1];
    NSLog(@"Helllooo activity viewcontroller");
    
    NSArray *activityItems = nil;
    if([type isEqualToString:@"image"])
    {
        NSURL *Url=[[NSURL alloc]initWithString:[dataUpdate objectAtIndex:btnClicked.tag-1]];
        NSData *ImageData=[NSData dataWithContentsOfURL:Url];
        UIImage *image=[UIImage imageWithData:ImageData];
        if (image != nil) {
            activityItems = @[@"Post from cancer circle iPhone app",image];
        }
        
        else {
            activityItems = @[[dataUpdate objectAtIndex:btnClicked.tag-1]];
        }
    }
    else if([type isEqualToString:@"story"]){
        activityItems = @[[dataUpdate objectAtIndex:btnClicked.tag-1]];
    }
    else if([type isEqualToString:@"video"]){
        // NSString *myString = [[dataUpdate objectAtIndex:btnClicked.tag-1] absoluteString];
        // NSLog(@"nsurl String %@",myString);
        activityItems = @[[dataUpdate objectAtIndex:btnClicked.tag-1]];
    }
    UIActivityViewController *activityController =
    [[UIActivityViewController alloc]
     initWithActivityItems:activityItems
     applicationActivities:nil];
    
    [self presentViewController:activityController
                       animated:YES completion:nil];
    
    
}


-(IBAction)playButtonClicked:(id)sender
{
    NSLog(@"videoUrl %@",videoUrl);
    NSURL *video=[NSURL URLWithString:videoUrl];
    _movieplayer = [[MPMoviePlayerController alloc] initWithContentURL:video];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:_movieplayer];
    
    _movieplayer.controlStyle = MPMovieControlStyleDefault;
    _movieplayer.shouldAutoplay = YES;
    [self.view addSubview:_movieplayer.view];
    [_movieplayer setFullscreen:YES animated:YES];
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    MPMoviePlayerController *player = [notification object];
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:player];
    
    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
}
-(IBAction)reportButtonClicked:(UIButton*)btnClicked
{
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    delegate.snoReport=[snoArray objectAtIndex:btnClicked.tag-1];
    delegate.typeReport=[dataType objectAtIndex:btnClicked.tag-1];
    actionSheet = [[UIActionSheet alloc] initWithTitle:@"Post Settings" delegate:self
                                     cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete Post"
                                     otherButtonTitles:nil,nil];
    actionSheet.backgroundColor = [UIColor clearColor];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    //[actionSheet showInView:self.view];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}
-(void)actionSheet:(UIActionSheet *)actionSheet2 clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == [actionSheet2 destructiveButtonIndex]) {
        
    } else if(buttonIndex == 1){
        
        
        
    }
}
-(void)profilePullData//scorll up for newer posts
{
    Login *login=[[Login alloc]init];
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    NSLog(@"datao time inserted at Circle %@",delegate.PgetInertedTime);
  
    
    NSLog(@"Profile Newest SNo = %@",delegate.profileNewestSno);
    NSLog(@"Profile Oldest SNo = %@",delegate.profileOldestSno);
    
    
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    
    
    
    NSString *url = [NSString stringWithFormat:SERVER_URL@"/profile_pull.do?username=%@&profileNewestSno=%@&offset=%d",delegate.mynameLogin,delegate.profileNewestSno,currentTimeZone.secondsFromGMT];
    NSURL *urlrequest=[NSURL URLWithString:url];
    NSURLRequest *req=[[NSURLRequest alloc]initWithURL:urlrequest];
    NSURLResponse  *response = nil;
    NSError* error;
    [NSURLConnection sendSynchronousRequest: req returningResponse: &response error: &error];
    
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    NSString *stringData= [NSString stringWithContentsOfURL:urlrequest encoding:nil error:nil];
    NSLog(@"Recieved data String &&&&&&&&&&&& %@",stringData);
    
    //NSData *dataofStrig=[NSData dataWithContentsOfFile:stringData];
    NSData * dataofStrig = [stringData dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"Recieved data Data &&&&&&&&&&&& %@",stringData);
    if (!dataofStrig) {
        NSLog(@"No Data");
        
    }
    else{
        
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions                        error:&error];
        
        NSLog(@"JSON format:- %@",json);
        NSLog(@"time inserted%@",[[json objectForKey:@"data0"] valueForKey:@"inserted_at"]);
        NSLog(@"time delegate inserted%@",delegate.PgetInertedTime);
        /* if([delegate.PgetInertedTime isEqualToString:[delegate.PinsertedTimeArray objectAtIndex:0]])
         {
         
         }
         else{*/
        
        int length=[json count];
        NSLog(@"JSON count:- %i",length);
        if (length>5) {
            NSString *indexStr = [NSString stringWithFormat:@"%d",(length-6)];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            delegate.PgetInertedTime=[[json objectForKey:data] valueForKey:@"inserted_at"];
            
            //get the biggest sno in this json response,and assign to delegate.newestSno,the last one is the biggest
            NSString *indexItem = [NSString stringWithFormat:@"data%d", length-6];
            delegate.profileNewestSno = [[json objectForKey:indexItem] valueForKey:@"sno"];

            
        }
        for(int i=0;i<(length-5);i++)    {
            
            NSString *indexStr = [NSString stringWithFormat:@"%d",i];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            [commentCounts insertObject:[[json objectForKey:data]valueForKey:@"comment"] atIndex:0];
            NSString *likesName=[[json objectForKey:data] valueForKey:@"likenames"];
            NSString *confrm=[login likesContent:likesName];
            [likesNameArray insertObject:confrm atIndex:0];
            NSString *dislikesName=[[json objectForKey:data] valueForKey:@"dislikenames"];
            NSString *disconfrm=[login likesContent:dislikesName];
            [dislikesNameArray insertObject:disconfrm atIndex:0];
            dataStr=[[json valueForKey:data] valueForKey:@"content1"];
            dataStr2=[[json valueForKey:data] valueForKey:@"content2"];
            NSString *sno=[[json valueForKey:data]valueForKey:@"sno"];
            [snoArray insertObject:sno atIndex:0];
            NSString *likeString = [[json valueForKey:data]valueForKey:@"like"];
            [likearray insertObject:likeString atIndex:0];
            NSString *dislikeString = [[json valueForKey:data]valueForKey:@"dislike"];
            [insertedTimeArray addObject:[[json objectForKey:data] valueForKey:@"inserted_at"]];
            [dislikearray insertObject:dislikeString atIndex:0];
            if ([[[json objectForKey:data] valueForKey:@"Elapsed"] isEqualToString:@""]) {
                [timeArray insertObject:@"Not Updated" atIndex:0];                }
            else
            {   [timeArray insertObject:[[json objectForKey:data] valueForKey:@"Elapsed"] atIndex:0];
            }
            if([[[json objectForKey:data] valueForKey:@"location"] isEqualToString:@""])
            {
                [locationArray insertObject:@"Not Updated" atIndex:0];
            }
            else
            {
                [locationArray insertObject:[[json objectForKey:data] valueForKey:@"location"] atIndex:0];
            }
            followers = [getfollowers stringValue];
            following = [getfollowings stringValue];
            
            
            [followerLabel setTitle:[followers stringByAppendingFormat:@" Followers"] forState: UIControlStateNormal];
            [followerLabel setTitle:[followers stringByAppendingFormat:@" Followers"] forState: UIControlStateSelected];
            [followerLabel setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
            [followinglabel setTitle:[following stringByAppendingFormat:@" Followings"] forState: UIControlStateNormal];
            [followinglabel setTitle:[following stringByAppendingFormat:@" Followings"] forState: UIControlStateSelected];
            [followinglabel setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
            NSString *type=[[json objectForKey:data]valueForKey:@"type"];
            [dataType insertObject:type atIndex:0];
            [dataUpdate insertObject:dataStr atIndex:0];
            [dataUpdate2 insertObject:dataStr2 atIndex:0];
            NSString *fourcomment=[[json objectForKey:data]valueForKey:@"commentiphone"];
            NSLog(@"fourcomment %@",fourcomment);
            if ([fourcomment length]==0) {
                [fourCommentArray insertObject:@"No Comments" atIndex:0];
            }
            else{
                [fourCommentArray insertObject:fourcomment atIndex:0];
            }
        }
    }
    delegate.PdataUpdate=dataUpdate;
    delegate.PdataUpdate2=dataUpdate2;
    delegate.PdataType=dataType;
    delegate.PsnoArray=snoArray;
    delegate.Plikearray=likearray;
    delegate.Pdislikearray=dislikearray;
    delegate.PlikesNameArray=likesNameArray;
    delegate.PdislikesNameArray=dislikesNameArray;
    delegate.PcommentCounts=commentCounts;
    delegate.PlocationArray=locationArray;
    delegate.PtimeArray=timeArray;
    delegate.PinsertedTimeArray=insertedTimeArray;
    [mytable reloadData];
}

-(void)refreshProfileData //or scroll to bottom, fetch some older posts,when enter this scene,this method is called first
{
    Login *login=[[Login alloc]init];
    
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
 
    NSLog(@"Profile Newest SNo = %@",delegate.profileNewestSno);
    NSLog(@"Profile Oldest SNo = %@",delegate.profileOldestSno);
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    
    
    NSString *url = [NSString stringWithFormat:SERVER_URL@"/profilemore.do?username=%@&profileOldestSno=%@&offset=%d",delegate.mynameLogin,delegate.profileOldestSno,currentTimeZone.secondsFromGMT];
    
    NSLog(@"Url =  %@",url);

    
    NSURL *urlrequest=[NSURL URLWithString:url];
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    NSError* error;
    NSData *receivedData2=[NSData dataWithContentsOfURL:urlrequest];
    if(!receivedData2 )
    {
        [indicator stopAnimating];
        indicator.hidden=YES;
        NSLog(@"errorrrrr no response");
        UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Connection failed" message:@"Check intenet connection" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
        [alert1 show];
    }
    else{
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:receivedData2 options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        if([delegate.PgetLastInertedTime isEqualToString:[[json objectForKey:@"data0"] valueForKey:@"inserted_at"]])
        {
            
        }
        else{
            int length=[json count];
            NSLog(@"JSON count:- %i",length);
            if (length>5) {
                NSString *indexStr = [NSString stringWithFormat:@"%d",(length-6)];
                NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
                delegate.PgetLastInertedTime=[[json objectForKey:data] valueForKey:@"inserted_at"];
                
                
                
                //because this method is called to initlize th posts table view,just in case the profileNewestSno is null now,
                
                if (delegate.profileNewestSno == nil) {
                    
                     //get the biggest sno in this json response,and assign to delegate.newestSno,the first one is the biggest
                     NSString *indexItem = [NSString stringWithFormat:@"data%d", 0];
                     delegate.profileNewestSno = [[json objectForKey:indexItem] valueForKey:@"sno"];
                    
                    
                }
                 NSLog(@"Profile Newest SNo = %@",delegate.profileNewestSno);
            
                //get the smallest sno in this json response,and assign to delegate.profileOldestSno
                NSString *indexItem = [NSString stringWithFormat:@"data%d", length-6];
                
                
                delegate.profileOldestSno = [[json objectForKey:indexItem] valueForKey:@"sno"];
                NSLog(@"Profile Oldest SNo = %@",delegate.profileOldestSno);
                
                
               
                
            }
            for(int i=0;i<length-5;i++)
            {
                NSString *indexStr = [NSString stringWithFormat:@"%d",i];
                NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
                [commentCounts insertObject:[[json objectForKey:data]valueForKey:@"comment"] atIndex:[commentCounts count]-1];
                
                NSString *likesName=[[json objectForKey:data] valueForKey:@"likenames"];
                NSString *confrm=[login likesContent:likesName];
                [likesNameArray insertObject:confrm atIndex:[likesNameArray count]-1];
                NSString *dislikesName=[[json objectForKey:data] valueForKey:@"dislikenames"];
                NSString *disconfrm=[login likesContent:dislikesName];
                [dislikesNameArray insertObject:disconfrm atIndex:[dislikesNameArray count]-1];
                NSString *snoString = [[json valueForKey:data]valueForKey:@"sno"];
                [snoArray insertObject:snoString atIndex:[snoArray count]-1];
                NSString *likeString = [[json valueForKey:data]valueForKey:@"like"];
                NSLog(@"value of like string is:- %@",likeString);
                [likearray insertObject:likeString atIndex:[likearray count]-1];
                NSString *dislikeString = [[json valueForKey:data]valueForKey:@"dislike"];
                [dislikearray insertObject:dislikeString atIndex:[dislikearray count]-1];
                if ([[[json objectForKey:data] valueForKey:@"Elapsed"] isEqualToString:@""]) {
                    [timeArray insertObject:@"Not Updated" atIndex:timeArray.count-1];
                }
                else
                { [timeArray insertObject:[[json objectForKey:data] valueForKey:@"Elapsed"] atIndex:timeArray.count-1];
                }
                if([[[json objectForKey:data] valueForKey:@"location"] isEqualToString:@""])
                {
                    [locationArray insertObject:@"Not Updated" atIndex:locationArray.count-1];
                }
                else
                {  [locationArray insertObject:[[json objectForKey:data] valueForKey:@"location"] atIndex:locationArray.count-1];
                }
                
                followers = [getfollowers stringValue];
                following = [getfollowings stringValue];
                
                
                [followerLabel setTitle:[followers stringByAppendingFormat:@" Followers"] forState: UIControlStateNormal];
                [followerLabel setTitle:[followers stringByAppendingFormat:@" Followers"] forState: UIControlStateSelected];
                [followerLabel setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
                [followinglabel setTitle:[following stringByAppendingFormat:@" Followings"] forState: UIControlStateNormal];
                [followinglabel setTitle:[following stringByAppendingFormat:@" Followings"] forState: UIControlStateSelected];
                [followinglabel setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
                dataStr=[[json valueForKey:data] valueForKey:@"content1"];
                dataStr2=[[json valueForKey:data] valueForKey:@"content2"];
                whoAreYou=[[json valueForKey:data] valueForKey:@"whoAreYou"];

                NSString *type=[[json objectForKey:data]valueForKey:@"type"];
                [dataType insertObject:type atIndex:dataType.count-1];
                [dataUpdate insertObject:dataStr atIndex:dataUpdate.count-1];
                [dataUpdate2 insertObject:dataStr2 atIndex:dataUpdate2.count-1];
                NSString *fourcomment=[[json objectForKey:data]valueForKey:@"commentiphone"];
                NSLog(@"fourcomment %@",fourcomment);
                if ([fourcomment length]==0) {
                    [fourCommentArray insertObject:@"No Comments" atIndex:fourCommentArray.count-1];
                }
                else{
                    [fourCommentArray insertObject:fourcomment atIndex:fourCommentArray.count-1];
                }
                
                
            }
        }
    }
    
    delegate.PdataUpdate=dataUpdate;
   // delegate.PdataUpdate2=dataUpdate2;
    delegate.PdataType=dataType;
    delegate.PsnoArray=snoArray;
    delegate.Plikearray=likearray;
    delegate.Pdislikearray=dislikearray;
    delegate.PcommentCounts=commentCounts;
    delegate.PlikesNameArray=likesNameArray;
    delegate.PdislikesNameArray=dislikesNameArray;
    delegate.PtimeArray=timeArray;
    delegate.PlocationArray=locationArray;    
    delegate.followersNames=followersNames;
    delegate.followingNames=followingNames;
    delegate.PfourCommentArray=fourCommentArray;
    
}
- (void)viewWillDisappear:(BOOL)animated {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:IFTweetLabelURLNotification object:nil];
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    navBar.hidden=false;
    
    
    // [super dealloc];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc {
    [_profileback release];
    [_followreq release];
    [_mainScroll release];
    [blurView release];
    [super dealloc];
}
@end
