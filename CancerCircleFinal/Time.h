//
//  Time.h
//  Cancerosity
//
//  Created by Grant Bevan on 3/12/2013.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Time : NSObject
+ (NSString*)getTimeAsString:(NSDate *)lastDate;

@end
