//
//  AppDelegate.h
//  CancerCircleFinal
//
//  Created by Raminder on 29/04/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import <UIKit/UIKit.h>


#import <FacebookSDK/FacebookSDK.h>
#import "UploadButtons.h"
#import "ShareSettingsView.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    CGRect *width;
    BOOL loggedIn;
}
@property (strong, nonatomic) UploadButtons *uploadPage;
@property (strong, nonatomic) ShareSettingsView *shareSettingsPage;
@property (strong, nonatomic) NSString *myname;
@property (strong, nonatomic) NSString *mynameLogin;
@property (strong, nonatomic) NSString *myPassword;
@property (strong, nonatomic) NSString *flag;
@property (strong, nonatomic) NSString *tabId;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *profileImage;
@property (strong, nonatomic) NSData *profilePicData;
@property  BOOL *loggedIN;

@property (strong, nonatomic) FBSession *session;

@property (strong, nonatomic) NSMutableArray *dataUpdate;
@property (strong, nonatomic) NSMutableArray *dataUpdate2;
@property (strong, nonatomic) NSMutableArray *differUser;
@property (strong, nonatomic) NSMutableArray *snoArray;
@property (strong, nonatomic) NSMutableArray *likearray;
@property (strong, nonatomic) NSMutableArray *dislikearray;
@property (strong, nonatomic) NSMutableArray *profielImages;
@property (strong, nonatomic) NSMutableArray *imageDatas;
@property (strong, nonatomic) NSMutableArray *dataType;
@property (strong, nonatomic) NSMutableArray *likesNameArray;
@property (strong, nonatomic) NSMutableArray *dislikesNameArray;
@property (strong, nonatomic) NSMutableArray *commentsArray;
@property (strong, nonatomic) NSMutableArray *commentNameArray;
@property (strong, nonatomic) NSMutableArray *commentPicArray;
@property (strong, nonatomic) NSMutableArray *commentCounts;
@property (strong, nonatomic) NSMutableArray *locationArray;
@property (strong, nonatomic) NSMutableArray *timeArray;
@property (strong, nonatomic) NSMutableArray *insertedTimeArray;
@property (strong, nonatomic) NSMutableArray *fourCommentArray;
@property (strong, nonatomic) NSMutableArray *privacyArray;


@property (strong, nonatomic) NSMutableArray *PsnoArray;
@property (strong, nonatomic) NSMutableArray *Plikearray;
@property (strong, nonatomic) NSMutableArray *Pdislikearray;
@property (strong, nonatomic) NSMutableArray *PdataUpdate;
@property (strong, nonatomic) NSMutableArray *PdataUpdate2;
@property (strong, nonatomic) NSMutableArray *PdataType;
@property (strong, nonatomic) NSNumber *following;
@property (strong, nonatomic) NSNumber *followers;
@property (strong, nonatomic) NSString *followingNames;
@property (strong, nonatomic) NSString *followersNames;
@property (strong, nonatomic) NSMutableArray *PlikesNameArray;
@property (strong, nonatomic) NSMutableArray *PdislikesNameArray;
@property (strong, nonatomic) NSMutableArray *PcommentCounts;
@property (strong, nonatomic) NSMutableArray *PlocationArray;
@property (strong, nonatomic) NSMutableArray *PtimeArray;
@property (strong, nonatomic) NSMutableArray *PinsertedTimeArray;
@property (strong, nonatomic) NSMutableArray *PfourCommentArray;
@property (strong, nonatomic) NSMutableArray *imageComments;
@property (strong, nonatomic) NSMutableArray *imageCommentNames;
@property (strong, nonatomic) NSMutableArray *imageCommentsPics;
@property (strong, nonatomic) NSMutableArray *imageCommentsType;

//for posts refresh
 
@property (strong, nonatomic) NSNumber *newestSno;
@property (strong, nonatomic) NSNumber *oldestSno;

//for user own profile refresh
@property (strong, nonatomic) NSNumber *profileNewestSno;
@property (strong, nonatomic) NSNumber *profileOldestSno;

@property (strong, nonatomic) NSString *snoReport;
@property (strong, nonatomic) NSString *userReport;
@property (strong, nonatomic) NSString *typeReport;
@property (strong, nonatomic) NSString *getInertedTime;
@property (strong, nonatomic) NSString *getLastInertedTime;
@property (strong, nonatomic) NSString *PgetInertedTime;
@property (strong, nonatomic) NSString *PgetLastInertedTime;
@property (strong, nonatomic) NSString *setLocation;
@property (strong, nonatomic) NSString *setEmailNotification;
@property (strong, nonatomic) NSString *setPrivacy;
@property (strong, nonatomic) NSString *SgetLastInseted;



@property (strong, nonatomic) NSString *displayName;
@property (strong, nonatomic) NSString *whoAreYou;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *emailId;
@property (strong, nonatomic) NSString *contact;
@property (strong, nonatomic) NSString *setUrlCricle;
@property (strong, nonatomic) NSString *setUrlProfile;
@property (strong, nonatomic) NSString *wordToSearch;
@property (strong, nonatomic) NSString *requestCount;

@property (strong, nonatomic) NSMutableArray *activityArray;
@property (strong, nonatomic) NSMutableArray *userArray;
@property (strong, nonatomic) NSString *token;
@property (strong, nonatomic) NSString *secondTabId;
@property (strong, nonatomic) NSString *newsSerial;
@property (strong, nonatomic) NSString *contentSTR;


- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error;
- (void)userLoggedIn;
- (void)userLoggedOut;


@end
