//
//  IFTweetLabel.h
//  CancerCircleFinal
//
//  Created by Raminder on 14/05/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

extern NSString *IFTweetLabelURLNotificationuser;

@interface IFLabelUsername : UIView
{
	UIColor *normalColor;
	UIColor *highlightColor;
    
	UIImage *normalImage;
	UIImage *highlightImage;
    
	UILabel *label;
	
	BOOL linksEnabled;
}

@property (nonatomic, retain) UIColor *normalColor;
@property (nonatomic, retain) UIColor *highlightColor;

@property (nonatomic, retain) UIImage *normalImage;
@property (nonatomic, retain) UIImage *highlightImage;

@property (nonatomic, retain) UILabel *label;

@property (nonatomic, assign) BOOL linksEnabled;

- (void)setBackgroundColor:(UIColor *)backgroundColor;
- (void)setFrame:(CGRect)frame;

@end


@interface IFLabelUsername (ForwardInvocation)

@property(nonatomic, copy) NSString *text;
@property(nonatomic) NSInteger numberOfLines;
@property(nonatomic, retain) UIColor *textColor;
@property(nonatomic, retain) UIFont *font;

@end
