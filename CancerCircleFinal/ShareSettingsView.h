//
//  ShareSettingsView.h
//  CancerCircleFinal
//
//  Created by Raminder on 22/06/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBViewControllerLogin.h"
#import <FacebookSDK/FacebookSDK.h>
#import <Accounts/Accounts.h>
#import <Social/Social.h>

@interface ShareSettingsView : UIViewController
{
    IBOutlet UILabel *fbUsername;
    IBOutlet UILabel *tuUsername;
    IBOutlet UILabel *flUsername;
    IBOutlet UILabel *twUsername;
    IBOutlet UIImageView *bgImage;
    IBOutlet UIButton *fbLogin;
    ACAccount *twitterAccount;
    NSArray *arrayOfAccounts;
    BOOL twitterSignedIn;
    ACAccountStore *account;
}
@property (nonatomic, retain) FBViewControllerLogin *fbGraph;
-(IBAction)logoutFromTumblr;
-(IBAction)logoutFromFb;
-(IBAction)logoutFromFlickr;
-(IBAction)logoutFromTwitter: (UIButton *) sender;
@property (strong, nonatomic) IBOutlet UILabel *fbUsername;
@property (strong, nonatomic) IBOutlet UIButton *fbLogin;
@property (strong, nonatomic) IBOutlet UILabel *fbLog;
@property (strong, nonatomic) IBOutlet UILabel *twLog;
@property (strong, nonatomic) IBOutlet UILabel *tmLog;
@property (strong, nonatomic) IBOutlet UILabel *flLog;
- (void)userLoggedIn;
- (void)userLoggedOut;
-(void)createDismissButton;
@property (retain, nonatomic) IBOutlet UITableView *shareTable;

+ (BOOL) checkFacebookPermissions:(FBSession *)session;

@property (retain, nonatomic) IBOutlet UITableViewCell *fbCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *twCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *flCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *tmCell;


@end
