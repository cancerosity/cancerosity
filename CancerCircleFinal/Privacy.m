//
//  Privacy.m
//  CancerCircleEdited
//
//  Created by Grant Bevan on 21/10/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import "Privacy.h"

@interface Privacy ()

@end

@implementation Privacy

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    //[self startIndicator];
  
}
- (void)viewDidLoad
{
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"640x960bg.png"]];
   
  
    CGFloat titleWidth2=100;
    CGFloat titleWidth = MIN(titleWidth2, 300);
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 30)];
    label.backgroundColor = [UIColor clearColor];
    // label.font = [UIFont boldSystemFontOfSize:20.0];
    label.font=[UIFont fontWithName:@"Futura_Light" size:20];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=@"Privacy Policy";
 
    
    self.navigationItem.titleView = label;
    UIImage* image3 = [UIImage imageNamed:@"close.png"];
    CGRect frameimg = CGRectMake(0, 0,18,18);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(goLogin)
         forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *backbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.leftBarButtonItem=backbutton;
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIColor *barColour = [[UIColor alloc] initWithRed:(022.0/255.0) green:(180.0/255.0) blue:(030.0/255.0) alpha:1.0];
    UIView *colourView = [[UIView alloc] initWithFrame:CGRectMake(0.f, -20.f, 320.f, 64.f)];
    colourView.opaque = NO;
    colourView.alpha = 0.6f;
    colourView.backgroundColor = barColour;
    [navBar.layer insertSublayer:colourView.layer atIndex:1];
   navBar.barTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    [someButton release];
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)goLogin
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    /*       Login *login=[self.storyboard instantiateViewControllerWithIdentifier:@"login"];
     [[self navigationController] setNavigationBarHidden:YES animated:YES];
     [self.navigationController pushViewController:login animated:YES];*/
}

@end
