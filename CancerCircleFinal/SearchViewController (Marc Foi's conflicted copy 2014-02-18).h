//
//  SearchViewController.h
//  CancerCircleFirst
//
//  Created by Raminder on 21/03/13.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "IFTweetLabel.h"
#import "IFLabelUsername.h"
@interface SearchViewController : UIViewController<UIActionSheetDelegate>
{
    NSMutableArray *dataUpdate;
    NSMutableArray *snoArray;
    NSMutableArray *likearray;
    NSMutableArray *dislikearray;
    NSMutableArray *profielImages;
    NSString *contentType;
    NSMutableArray *tags;
    UIButton *likeButton;
    UIButton *dislikeButton;
    NSMutableArray *imageDatas;
    NSMutableArray *dataType;
    NSString *countsLyk;
    UIImageView *imageViewUrl;
    NSMutableArray *likesNameArray;
    NSMutableArray *dislikesNameArray;
    NSMutableArray *differUser;
    NSString *dataStr;
    IBOutlet UITableView *mytable;
    NSInteger in;
    UILabel *storyLabel;
    NSString *getusername;
    IBOutlet UITextField *searchText;
    NSMutableArray *displayNameArray;
    NSMutableArray *userNamesArray;
    NSMutableArray *userStatusArray;
    NSMutableArray *userPicArray;
    NSMutableArray *userFollowings;
    NSMutableArray *userFollowers;
    NSMutableArray *locationArray;
    NSMutableArray *timeArray;
    NSMutableArray *commentArray;
    NSMutableArray *fourCommentArray;
    NSMutableArray *commentCounts;
    int introw;
    NSString *searchType;
    IFTweetLabel *tweetLabel;
    UIActivityIndicatorView *indicator;
    IBOutlet UISearchBar *search;
    //NSString *getWord;
    IBOutlet UILabel *label;
    IBOutlet UIButton *someButton;
    
    
}
@property(nonatomic,strong)NSString *getWord;
-(IBAction)clearSearch;
-(IBAction)searchButton;
-(void)setsearch;

@property (strong , nonatomic) NSString *getTagText;
@property (strong , nonatomic) NSString *getTabTitle;
@property (nonatomic, strong) NSOperationQueue *imageDownloadingQueuePic;
@property (nonatomic, strong) NSCache *imageCachePic;
@property (nonatomic, strong) NSString *searchtext;
@end
