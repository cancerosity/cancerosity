//
//  iNewsViewController.m
//  CancerCircleFinal
//
//  Created by Raminder on 28/06/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import "iNewsViewController.h"
#import "AppDelegate.h"
#import "Login.h"
#import <Social/Social.h>
#import "searchedUserView.h"
#import "SearchViewController.h"
#import "OpenUrlView.h"
#import "Login.h"
#import "NewsViewController.h"
#define CELL_CONTENT_WIDTH2 340.0f
#define CELL_CONTENT_MARGIN2 40.0f
#define FONT_SIZE 11.0f
#define kNavBarDefaultPosition CGPointMake(160, 42)

@interface iNewsViewController ()

@end

Login *login;
CGFloat startContentOffset;
CGFloat lastContentOffset;
BOOL hidden;
@implementation iNewsViewController
@synthesize getDisLikes,getIfDisLikes,getIfLikes,getLikes,getUserImageData,getSno,getName,getComments,getType,getLocation,getTime,getDislikeConferm,getLikeConferm,getRowIndex,getTabId,getImage,getUserImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleTweetNotification:) name:IFTweetLabelURLNotification object:nil];
    [super viewWillAppear:animated];
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[NewsViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"News"];
    }
    
    /*if (![self.slidingViewController.underRightViewController isKindOfClass:[UnderRightViewController class]]) {
     self.slidingViewController.underRightViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UnderRight"];
     }*/
    
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
    
}
- (void)handleTweetNotification:(NSNotification *)notification
{
    NSDictionary *dict = (NSDictionary*)notification.object;
    NSString *strDict = [NSString stringWithFormat:@"%@", dict];
    if([strDict hasPrefix:@"http://"])
    {
        OpenUrlView *openUrl=[self.storyboard instantiateViewControllerWithIdentifier:@"OpenUrl"];
        //openUrl.hidesBottomBarWhenPushed = YES;
        openUrl.getUrl=strDict;
        //[self.navigationController pushViewController:openUrl animated:YES];
    }
    else
    {
        AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
        delegate.wordToSearch=strDict;
        if (([strDict hasPrefix:@"#"])||([strDict hasPrefix:@"@"])) {
            //[self performSegueWithIdentifier:@"circleToSearch" sender:self];
           
            UINavigationBar *navBar = [[self navigationController] navigationBar];
            navBar.hidden=false;
            // [self.navigationController pushViewController:search animated:YES];
        }
        else
        {
            //[self userButtonVideoClicked:strDict];
        }
        
    }
}

-(IBAction)goToNews
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}
- (void)viewDidLoad
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideOrShow:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideOrShow:) name:UIKeyboardWillShowNotification object:nil];
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    idAtUpload=delegate.tabId;
    getType=@"images";
    [super viewDidLoad];
    NSLog(@"image view detail");
    // [commentText becomeFirstResponder];
    //************* Nvigation bar ****************************************************
    CALayer *layer = self.navigationController.navigationBar.layer;
    layer.position = kNavBarDefaultPosition;
    commentText.layer.cornerRadius = 5;
    commentText.layer.masksToBounds = YES;
    
    transView.translucentAlpha = 1.0;
    transView.translucentStyle = UIBarStyleDefault;
    transView.translucentTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    
    
    self.imageDownloadingQueue = [[NSOperationQueue alloc] init];
    self.imageDownloadingQueue.maxConcurrentOperationCount =2;
    self.imageCache = [[NSCache alloc] init];
    
    self.imageDownloadingQueuePic = [[NSOperationQueue alloc] init];
    self.imageDownloadingQueuePic.maxConcurrentOperationCount = 4;
    self.imageCachePic = [[NSCache alloc] init];
    
   
    
    
    self.navigationController.navigationBar.barTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    self.navigationController.navigationBar.translucent = YES;
    CGFloat titleWidth2=100;
    CGFloat titleWidth = MIN(titleWidth2, 300);
    label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 30)];
    label.backgroundColor = [UIColor clearColor];
    label.font=[UIFont fontWithName:@"Futura_Light" size:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=@"Register";
    self.navigationItem.titleView = label;
   
    
    

    getusername = delegate.mynameLogin;
    getSno=delegate.newsSerial;
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"640x960bg.png"]];
    [self getDataInfo];
    [self commentsData];
    [mytable reloadData];
   
}

-(void)expand
{
    if(hidden)
        return;
    
    hidden = YES;
    [transView setHidden:NO];
    
    
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

-(void)contract
{
    if(!hidden)
        return;
    
    hidden = NO;
    
    
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    startContentOffset = lastContentOffset = scrollView.contentOffset.y;
    //NSLog(@"scrollViewWillBeginDragging: %f", scrollView.contentOffset.y);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat differenceFromStart = startContentOffset - currentOffset;
    CGFloat differenceFromLast = lastContentOffset - currentOffset;
    lastContentOffset = currentOffset;
    
    
    
    if((differenceFromStart) < 0)
    {
        // scroll up
        if(scrollView.isTracking && (abs(differenceFromLast)>1))
            [self expand];
    }
    else {
        if(scrollView.isTracking && (abs(differenceFromLast)>1))
            [self contract];
    }
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    [self contract];
    return YES;
}
- (void)keyboardWillHideOrShow:(NSNotification *)note {
    NSDictionary *userInfo = [note userInfo];
    
    NSTimeInterval duration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
    
    CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    CGRect keyboardFrameForTextField = [commentView.superview convertRect:keyboardFrame fromView:nil];
    CGRect newFrame = commentView.frame;
    
    newFrame.origin.y = keyboardFrameForTextField.origin.y - newFrame.size.height;
    
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
        commentView.frame = newFrame;
        CGPoint point = mytable.contentOffset;
        point .y = newFrame.origin.y;
        mytable.contentOffset = point;
        
        
    } completion:nil];
}

-(void)getDataInfo
{
    infoArray=[[NSMutableArray alloc]initWithCapacity:infoArray.count];
    Login *login=[[Login alloc]init];
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    NSString *urlString=[NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/content_info.do?sno=%@&type=images",delegate.newsSerial];
    NSLog(@"urlString %@",urlString);
    NSURL *urlRequest=[NSURL URLWithString:urlString];
    NSData *data=[NSData dataWithContentsOfURL:urlRequest];
    NSError* error;
    NSDictionary *json=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    imageUrl=[[json objectForKey:@"data0"] valueForKey:@"content"];
    NSLog(@"getImageData %@",imageUrl);
    [infoArray addObject:imageUrl];
    getName=[[json objectForKey:@"data0"] valueForKey:@"username"];
    [infoArray addObject:getName];
    getUserImageData=[[json objectForKey:@"data0"] valueForKey:@"profile_image"];
    [infoArray addObject:getUserImageData];
    NSLog(@"getUserImageData %@",getUserImageData);
    getLikes=[[json objectForKey:@"data0"] valueForKey:@"like"];
    [infoArray addObject:getLikes];
    NSLog(@"getLikes %@",getLikes);
    getDisLikes=[[json objectForKey:@"data0"] valueForKey:@"dislike"];
    [infoArray addObject:getDisLikes];
    NSLog(@"getDisLikes %@",getDisLikes);
    getComments=[[json objectForKey:@"data0"] valueForKey:@"comment"];
    [infoArray addObject:getComments];
    NSLog(@"getComments %@",getComments);
    NSString *likesName=[[json objectForKey:@"data0"] valueForKey:@"likenames"];
    getLikeConferm=[login likesContent:likesName];
    [infoArray addObject:getLikeConferm];
    NSString *dislikesName=[[json objectForKey:@"data0"] valueForKey:@"dislikenames"];
    getDislikeConferm=[login likesContent:dislikesName];
    [infoArray addObject:getDislikeConferm];
    getLocation=[[json objectForKey:@"data0"] valueForKey:@"location"];
    [infoArray addObject:getLocation];
    getTime=[[json objectForKey:@"data0"] valueForKey:@"Elapsed"];
    [infoArray addObject:getTime];
    [mytable reloadData];
}
-(void)commentsData
{
    NSLog(@"image view comments data");
    commnetsArray=[[NSMutableArray alloc]initWithCapacity:commnetsArray.count];
    commentNameArray=[[NSMutableArray alloc]initWithCapacity:commentNameArray.count];
    commentPicArray=[[NSMutableArray alloc]initWithCapacity:commentPicArray.count];
    typeData=[[NSMutableArray alloc]initWithCapacity:typeData.count];
    [commentNameArray addObject:@"nothing"];
    [commnetsArray addObject:@"nothing"];
    [commentPicArray addObject:@"nothing"];
    [typeData addObject:@"image"];
    NSString *urlComment = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/showcomment.do?sno=%@&type=%@",getSno,getType];
    NSURL *urlrequest=[NSURL URLWithString:urlComment];
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    
    NSData * dataofStrig = [NSData dataWithContentsOfURL:urlrequest];
    NSError* error;
    
    if(!dataofStrig )
    {
        NSLog(@"errorrrrr no response");
        UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Connection failed" message:@"Check intenet connection" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
        [alert1 show];
        
    }
    
    else{
        NSLog(@"comments data %@",dataofStrig);
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        int length=[json count];
        //getComments=[NSString stringWithFormat:@"%u",[json objectForKey:@"count"];
        for(int i=0;i<length-2;i++)
        {
            NSString *indexStr = [NSString stringWithFormat:@"%d",i];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            NSString *commentStr=[[json objectForKey:data]valueForKey:@"comment"];
            if ([commentStr isEqualToString:@""]) {
                
            }
            else{
                NSArray *info=[commentStr componentsSeparatedByString:@"-SEPARATOR-"];
                [commentNameArray addObject:[info objectAtIndex:0]];
                [commnetsArray addObject:[info objectAtIndex:1]];
                [commentPicArray addObject:[info objectAtIndex:2]];
                [typeData addObject:@"comment"];
                [mytable reloadData];
            }
        }
    }
    
}

-(IBAction)hidekeyboard:(id)sender
{
    
    [commentText resignFirstResponder];
    CGPoint point = mytable.contentOffset;
    point .y = 0;
    mytable.contentOffset = point;
    [self.navigationController.view endEditing:YES];
    
}
- (void)scrollViewToCenterOfScreen:(UIView *)theView {
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    CGFloat availableHeight = applicationFrame.size.height - 160;            // Remove area covered by keyboard
    CGFloat y = viewCenterY - availableHeight / 1.5;
    //CGFloat y = viewCenterY - availableHeight / 2.5;
    if (y < 0) {
        y = 0;
    }
    [mytable setContentOffset:CGPointMake(0, y) animated:YES];
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    [self scrollViewToCenterOfScreen:textField];
    return YES;
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
   
    BOOL isDone = YES;
	
	if (isDone)
    {
		//[self finishedSearching];
         [textField resignFirstResponder];
        [mytable setContentOffset:CGPointMake(0, 0) animated:YES];
		return YES;
	} else
    {
		return NO;
	}
}

-(IBAction)commentButton
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if([commentText.text isEqualToString:@""])
    {
        
    }
    else{
        NSLog(@"comment text %@",commentText.text);
        NSString *converted = [commentText.text stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        NSString *urlLike = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/comment.do?username=%@&sno=%@&type=images&comment=%@",getusername,getSno,converted];
        NSLog(@"comment Url %@",urlLike);
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSString *newCounts=[json objectForKey:@"count"];
        getComments=newCounts;
        /* int index=[getRowIndex intValue];
         if ([getTabId isEqualToString:@"circle"]) {
         [delegate.commentCounts replaceObjectAtIndex:index withObject:newCounts];
         NSArray *comArray=[[delegate.fourCommentArray objectAtIndex:index] componentsSeparatedByString:@"-END_OF_DATA-LINE-"];
         if ([comArray count]==5) {
         
         NSString *oldArray=[[[[[[comArray objectAtIndex:1]stringByAppendingFormat:@"-END_OF_DATA-LINE-"]
         stringByAppendingFormat:@"%@",[comArray objectAtIndex:2]]stringByAppendingFormat:@"-END_OF_DATA-LINE-"]
         stringByAppendingFormat:@"%@",[comArray objectAtIndex:3]]stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
         
         NSString *commentStr=[[[[oldArray stringByAppendingFormat:@"@ "] stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
         [delegate.fourCommentArray replaceObjectAtIndex:index withObject:commentStr];
         }
         else if([[delegate.fourCommentArray objectAtIndex:index] isEqualToString:@"No Comments"])
         {
         NSString *commentStr=[[[@"@" stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
         [delegate.fourCommentArray replaceObjectAtIndex:index withObject:commentStr];
         }
         else
         {
         NSString *commentStr=[[[[[delegate.fourCommentArray objectAtIndex:index]stringByAppendingFormat:@"@"] stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
         [delegate.fourCommentArray replaceObjectAtIndex:index withObject:commentStr];
         }
         }
         else if([getTabId isEqualToString:@"profile"])
         {
         [delegate.PcommentCounts replaceObjectAtIndex:index withObject:newCounts];
         NSArray *comArray=[[delegate.PfourCommentArray objectAtIndex:index] componentsSeparatedByString:@"-END_OF_DATA-LINE-"];
         if ([comArray count]==5) {
         
         NSString *oldArray=[[[[[[comArray objectAtIndex:1]stringByAppendingFormat:@"-END_OF_DATA-LINE-"]
         stringByAppendingFormat:@"%@",[comArray objectAtIndex:2]]stringByAppendingFormat:@"-END_OF_DATA-LINE-"]
         stringByAppendingFormat:@"%@",[comArray objectAtIndex:3]]stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
         
         NSString *commentStr=[[[[oldArray stringByAppendingFormat:@"@ "] stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
         [delegate.PfourCommentArray replaceObjectAtIndex:index withObject:commentStr];
         }
         else if([[delegate.PfourCommentArray objectAtIndex:index] isEqualToString:@"No Comments"])
         {
         NSString *commentStr=[[[@"@" stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
         [delegate.PfourCommentArray replaceObjectAtIndex:index withObject:commentStr];
         }
         else
         {
         NSString *commentStr=[[[[[delegate.PfourCommentArray objectAtIndex:index]stringByAppendingFormat:@"@"] stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
         [delegate.PfourCommentArray replaceObjectAtIndex:index withObject:commentStr];
         }
         }*/
        NSString *resultStr=[json objectForKey:@"param"];
        if([resultStr isEqualToString:@"true"])
        {
            //[self commentsData];
            //[login circleData];
            [typeData addObject:@"comment"];
            [commnetsArray addObject:commentText.text];
            [commentPicArray addObject:delegate.profileImage];
            [commentNameArray addObject:getusername];
            [mytable reloadData];
        }
        commentText.text=@"";
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *typeString=[typeData objectAtIndex:indexPath.row];
    int h;
    if([typeString isEqualToString:@"image"])
    {
        h=400;
    }
    if([typeString isEqualToString:@"comment"]){
        // height=50;
        NSString *text = [commnetsArray objectAtIndex:indexPath.row];
        NSString *commentStrLength=[[@"@" stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",text];
        
        CGSize constraint = CGSizeMake(self.view.bounds.size.width, 20000.0f);
        
        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont fontWithName:@"HelveticaNeue" size:12] forKey: NSFontAttributeName];
        
        CGSize size = [commentStrLength boundingRectWithSize:constraint
                                         options:NSLineBreakByWordWrapping
                                      attributes:stringAttributes context:nil].size;
        
        
        CGFloat height = MAX(size.height, 20.0f);
        
        h = height + (5 * 2);
    }
    
    return h;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    
    return [commnetsArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"INFO ARRAY %@",infoArray);
    Login *login=[[Login alloc]init];
    UIImage *minus = [UIImage imageNamed:@"minus.png"];
    UIImage *plus = [UIImage imageNamed:@"Plus.png"];
    UIImage *minusDark = [UIImage imageNamed:@"mDark.png"];
    UIImage *plusDark = [UIImage imageNamed:@"pDark.png"];
    UIImage *plusOut = [UIImage imageNamed:@"pOut.png"];
    UIImage *minusOut = [UIImage imageNamed:@"mOut.png"];
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cellReturn=nil;
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    static NSString *CellIdentifier2 = @"Cell2";
    UITableViewCell *cell2 =[tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
    mytable.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell2.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [mytable setBackgroundColor: [UIColor clearColor]];
    cell.backgroundColor = [UIColor clearColor];
    cell2.backgroundColor = [UIColor clearColor];
    NSString *typeString=[typeData objectAtIndex:indexPath.row];
    int t=indexPath.row;
    int lbltag=indexPath.row+1;
    NSLog(@"index path row %i",t);
    if([typeString isEqualToString:@"image"])
        //if(imageString)
    {
        NSLog(@"hiiiiiiiiii image");
        if (cell == nil) {
            cell = [[UITableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:CellIdentifier];
        }
        
        CGFloat x = [UIScreen mainScreen].bounds.origin.x;
        UIView *backgroundCellView = [[UIView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 395)];
        backgroundCellView.backgroundColor = [UIColor clearColor];
        UIImageView *backimage = [[UIImageView alloc] initWithFrame:CGRectMake(x+5, 0, 310, 395)];
        backimage.backgroundColor = [UIColor whiteColor];
        [backgroundCellView addSubview:backimage];
        cell.backgroundView = backgroundCellView;
        
        for(UIView *v in [cell.contentView subviews])
        {
            if([v isKindOfClass:[UILabel class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[IFTweetLabel class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[IFLabelUsername class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[UIImageView class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[UIButton class]])
                [v removeFromSuperview];
        }

        
        NSString *imageUrlString =imageUrl;
        NSLog(@"image url string iss:- %@",imageUrl);
       UIImageView * imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,320,285)];
        [cell.contentView addSubview:imageView1];
        UIImage *cachedImage = [self.imageCache objectForKey:imageUrlString];
        if (cachedImage)
        {
            //imgCell.imageView.image = cachedImage;
            imageView1.image = cachedImage;
        }
        else
        {
            // you'll want to initialize the image with some blank image as a placeholder
            
            //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
            imageView1.image = [UIImage imageNamed:@"placholder.png"];
            [self.imageDownloadingQueue addOperationWithBlock:^{
                NSURL *imageURL   = [NSURL URLWithString:imageUrlString];
                NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                UIImage *image    = nil;
                if (imageData)
                    image = [UIImage imageWithData:imageData];
                if (image)
                {
                    // add the image to your cache
                    
                    [self.imageCache setObject:image forKey:imageUrlString];
                    
                    // finally, update the user interface in the main queue
                    
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                        if (updateCell)
                            //imgCell.imageView.image = image;
                            
                            imageView1.image = image;
                        
                        
                    }];
                }
            }];
        }
        // imageView1.image = getImage;
        
        
        //***********************************
        
        UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15,290,40,40)];
        imageView2.layer.cornerRadius = 20;
        imageView2.clipsToBounds = YES;
        [cell.contentView addSubview:imageView2];
        imageView2.image=getUserImage;
        
        NSString *picUrlString = getUserImageData;
        UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
        
        if (cachedPic)
        {
            //imgCell.imageView.image = cachedImage;
            imageView2.image = cachedPic;
            
        }
        else
        {
            // you'll want to initialize the image with some blank image as a placeholder
            
            //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
            imageView2.image = [UIImage imageNamed:@"editPic.png"];
            [self.imageDownloadingQueuePic addOperationWithBlock:^{
                NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                UIImage *image    = nil;
                if (imageData)
                    image = [UIImage imageWithData:imageData];
                if (image)
                {
                    // add the image to your cache
                    
                    [self.imageCachePic setObject:image forKey:picUrlString];
                    
                    // finally, update the user interface in the main queue
                    
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                        if (updateCell)
                            imageView2.image = image;
                        
                    }];
                }
            }];
        }
        
        IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(65,290,200,30)];
        nameLabel.numberOfLines=0;
        nameLabel.textColor=[UIColor clearColor];
        //nameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:12];
        NSString *smile2=getName;//[@"@" stringByAppendingString:[differUser objectAtIndex:in]];
        [nameLabel setText:smile2];
        [nameLabel setLinksEnabled:TRUE];
        nameLabel.clipsToBounds=YES;
        [cell.contentView addSubview:nameLabel];
        
        UIImageView *buttonsBack  = [[UIImageView alloc] initWithFrame:CGRectMake(5, 352, 310, 43)];
        [cell addSubview:buttonsBack];
        buttonsBack.image = [UIImage imageNamed:@"buttonsBack.png"];
        
        UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(205,295,100,10)];
        placeLabel.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
        placeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:9];
        placeLabel.text=getLocation;
        placeLabel.textAlignment=NSTextAlignmentRight;
        [cell.contentView addSubview:placeLabel];
        UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(65,315,100,10)];
        timeLabel.textColor=[[UIColor alloc] initWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:1];
        timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:9];
        timeLabel.text=getTime;
        timeLabel.textAlignment=NSTextAlignmentLeft;
        [cell.contentView addSubview:timeLabel];
        
        UILabel *likeShow = nil;
        likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(15,382,43,10)] autorelease];
        likeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
        likeShow.text =getLikes;// [NSString stringWithFormat:@"%d",[getLikes intValue]];
        likeShow.font=[UIFont systemFontOfSize:7.0];
        likeShow.textAlignment=NSTextAlignmentCenter;
        likeShow.tag = lbltag;
        [cell addSubview:likeShow];
        
        UILabel *dislikeShow = nil;
        UIButton *dislikeBtn = nil;
        dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(76,382,43,10)] autorelease];
        dislikeShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
        dislikeShow.text =getDisLikes;
        dislikeShow.font=[UIFont systemFontOfSize:7.0];
        dislikeShow.textAlignment=NSTextAlignmentCenter;
        dislikeShow.tag = lbltag;
        [cell addSubview:dislikeShow];
        
        
        UILabel *commentShow = nil;
        commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(137,382,43,10)] autorelease];
        commentShow.textColor=[[UIColor alloc] initWithRed:(180.0/255.0) green:(180.0/255.0) blue:(180.0/255.0) alpha:1];
        commentShow.text = getComments; //[NSString stringWithFormat:@"%d",[getComments intValue]];
        commentShow.font=[UIFont systemFontOfSize:7.0];
        commentShow.tag = lbltag;
        commentShow.textAlignment=NSTextAlignmentCenter;
        [cell addSubview:commentShow];
        //******************************************************************************
        
        
        UIButton *likeBtn = nil;
        likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        likeBtn.tag = indexPath.row+1;
        [likeBtn addTarget:self action:@selector(likeImage:) forControlEvents:UIControlEventTouchUpInside];
        likeBtn.titleLabel.font=[UIFont systemFontOfSize:10.0];
        [likeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        likeBtn.frame=CGRectMake(5,352,62,43);
        [likeBtn addTarget:self action:@selector(likeImage:) forControlEvents:UIControlEventTouchUpInside];
        if([getLikeConferm  isEqualToString:@"Yes"])
        {
            likeBtn.userInteractionEnabled=NO;
            [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
        }
        else
        {
            [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
        }
        [cell addSubview:likeBtn];
        
        
        dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        dislikeBtn.tag = indexPath.row+1;
        [dislikeBtn addTarget:self action:@selector(dislikeImage:) forControlEvents:UIControlEventTouchUpInside];
         dislikeBtn.frame=CGRectMake(67,352,62,43);
        dislikeBtn.titleLabel.font=[UIFont systemFontOfSize:10.0];
        [dislikeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        if([getDislikeConferm isEqualToString:@"Yes"])
        {
            dislikeBtn.userInteractionEnabled=NO;
            [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
        }
        else
        {
            [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
        }
        [cell addSubview:dislikeBtn];
        
        UIButton *commentButton=nil;
        commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        commentButton.tag = indexPath.row+1;
         UIImage *comment = [UIImage imageNamed:@"Comment.png"];
        [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
        commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
        [commentButton setTitleColor:[UIColor colorWithRed:0 green:0.376 blue:0.024 alpha:1] forState:UIControlStateNormal];
        [commentButton setFrame:CGRectMake(129,352,62,43)];
        [commentButton addTarget:self action:@selector(commentClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:commentButton];
        
        UIButton *shareButton=nil;
        shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        shareButton.tag = indexPath.row+1;
         UIImage *share = [UIImage imageNamed:@"Share.png"];
        [shareButton setBackgroundImage:share forState:UIControlStateNormal];
        shareButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
        
       [shareButton setFrame:CGRectMake(191,352,62,43)];
        [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:shareButton];
        
        
        UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        UIImage *setting = [UIImage imageNamed:@"Settings.png"];
        [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
        [settingsButton setTitle:@"" forState:UIControlStateNormal];
        [settingsButton setFrame:CGRectMake(253,352,62,43)];
        settingsButton.tag=lbltag;
        [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:settingsButton];
        
        
        cellReturn=cell;
        
    }
    else if([typeString isEqualToString:@"comment"])
    {
        if (cell2 == nil) {
            cell2 = [[UITableViewCell alloc]
                     initWithStyle:UITableViewCellStyleDefault
                     reuseIdentifier:CellIdentifier2];
        }
        for(UIView *v in [cell2.contentView subviews])
        {
            if([v isKindOfClass:[UILabel class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[IFTweetLabel class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[IFLabelUsername class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[UIImageView class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[UIButton class]])
                [v removeFromSuperview];
        }
        NSString *textLength = [commnetsArray objectAtIndex:indexPath.row];
        NSString *commentStrLength=[[@"@" stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",textLength];
        
        
        CGSize constraint2 = CGSizeMake(self.view.bounds.size.width, 20000.0f);
        
        
        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont fontWithName:@"HelveticaNeue" size:12] forKey: NSFontAttributeName];
        
        CGSize size2 = [commentStrLength boundingRectWithSize:constraint2
                                                      options:NSLineBreakByWordWrapping
                                                   attributes:stringAttributes context:nil].size;
        
        CGFloat height = MAX(size2.height, 20.0f);
        
        
        
        CGFloat x = [UIScreen mainScreen].bounds.origin.x;
        CGFloat height2 = height + (5 * 2);
        UIView *backgroundCellView = [[UIView alloc] initWithFrame:CGRectMake(x+5, 0, 310, height2)];
        backgroundCellView.backgroundColor = [UIColor clearColor];
        UIImageView *backimage = [[UIImageView alloc] initWithFrame:CGRectMake(x+5, 0, 310, height2)];
        backimage.backgroundColor = [UIColor whiteColor];
        [backgroundCellView addSubview:backimage];
        cell2.backgroundView = backgroundCellView;
        
        
        //UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(20,10,40,40)];
        //imageView2.layer.cornerRadius = 20;
        //imageView2.clipsToBounds = YES;
        
        //[cell2.contentView addSubview:imageView2];
        
        //NSString *picUrlString = [commentPicArray objectAtIndex: indexPath.row];
        //UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
        
        //if (cachedPic)
        //{
        //    //imgCell.imageView.image = cachedImage;
        //    imageView2.image = cachedPic;
        
        //}
        //else
        //{
        // you'll want to initialize the image with some blank image as a placeholder
        
        //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
        //    imageView2.image = [UIImage imageNamed:@"CachePic.png"];
        //    [self.imageDownloadingQueuePic addOperationWithBlock:^{
        //        NSURL *imageURL   = [NSURL URLWithString:picUrlString];
        //        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        //        UIImage *image    = nil;
        //        if (imageData)
        //            image = [UIImage imageWithData:imageData];
        //        if (image)
        //        {
        // add the image to your cache
        
        //            [self.imageCachePic setObject:image forKey:picUrlString];
        
        // finally, update the user interface in the main queue
        
        //            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        //                UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
        //                if (updateCell)
        //                    imageView2.image = image;
        
        //            }];
        //        }
        //    }];
        // }
        
        //IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(70,20,200,20)];
        //nameLabel.numberOfLines=0;
        //nameLabel.textColor=[UIColor clearColor];
        //nameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:12];
        //NSString *smile2=[commentNameArray objectAtIndex:t];//[@"@" stringByAppendingString:[differUser objectAtIndex:in]];
        //[nameLabel setText:smile2];
        //[nameLabel setLinksEnabled:TRUE];
        //nameLabel.clipsToBounds=YES;
        
        //[cell2.contentView addSubview:nameLabel];
        
        UILabel *commentlabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [commentlabel setLineBreakMode:NSLineBreakByWordWrapping];
        commentlabel.adjustsFontSizeToFitWidth = NO;
        [commentlabel setNumberOfLines:0];
        [commentlabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
        [commentlabel setTag:1];
        
        tweetLabel2 = [[IFTweetLabel alloc] initWithFrame:CGRectZero];
        [tweetLabel2 setLinksEnabled:TRUE];
        tweetLabel2.clipsToBounds=YES;
        [tweetLabel2 setNumberOfLines:0];
        [tweetLabel2 setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
        [tweetLabel2 setTag:1];
        
        NSString *text = [commnetsArray objectAtIndex:t];
        NSString *commentStr=[[@"@" stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",text];
        
        
        CGSize constraint = CGSizeMake(self.view.bounds.size.width, 20000.0f);
        NSDictionary *stringAttributes2 = [NSDictionary dictionaryWithObject:[UIFont fontWithName:@"HelveticaNeue" size:12] forKey: NSFontAttributeName];
        
        CGSize size = [commentStr boundingRectWithSize:constraint
                                               options:NSLineBreakByWordWrapping
                                            attributes:stringAttributes2 context:nil].size;
        
        
        if (!tweetLabel2)
            tweetLabel2 = (IFTweetLabel*)[cell viewWithTag:1];
        //tweetLabel2.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
        NSString *smile=[login smilyString:commentStr];
        [tweetLabel2 setText:smile];
        
        commentHeight = [tweetLabel2.text boundingRectWithSize:CGSizeMake(300, 100) options:(NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName: tweetLabel2.font} context:nil].size;
        tweetLabel2.textColor=[UIColor blackColor];
        [tweetLabel2 setFrame:CGRectMake(self.view.bounds.origin.x + 10,5, self.view.bounds.size.width - 10, MAX(size.height,20.0f))];
        [tweetLabel2 sizeToFit];
        [cell2.contentView addSubview:tweetLabel2];
        cellReturn=cell2;
    }
    return cellReturn;
}
-(void)commentClicked:(UIButton*)btnClicked{
    
    [commentText becomeFirstResponder];
    
}
-(void)likeImage:(UIButton*)btnClicked
{
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    if([getLikeConferm isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend =getSno;
        NSString *urlLike = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/liked.do?sno=%@&type=images&username=%@&action=liked",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        NSString *count=[json valueForKey:@"count"];
        getLikes=count;
        getLikeConferm=@"Yes";
        int index=[getRowIndex intValue];
        if ([getTabId isEqualToString:@"circle"]) {
            [delegate.likearray replaceObjectAtIndex:index withObject:getLikes];
            [delegate.likesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        else if([getTabId isEqualToString:@"profile"])
        {
            [delegate.Plikearray replaceObjectAtIndex:index withObject:getLikes];
            [delegate.PlikesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:btnClicked.tag-1 inSection:0];
        NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
        [mytable reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
        //[mytable reloadData];
    }
    
}
-(void)dislikeImage:(UIButton*)btnClicked
{
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    if([getDislikeConferm isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend =getSno;
        NSString *urlLike = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/liked.do?sno=%@&type=images&username=%@&action=dislike",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        NSString *count=[json valueForKey:@"count"];
        getDisLikes=count;
        int index=[getRowIndex intValue];
        getDislikeConferm=@"Yes";
        if ([getTabId isEqualToString:@"circle"]) {
            [delegate.dislikearray replaceObjectAtIndex:index withObject:getDisLikes];
            [delegate.dislikesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        else if([getTabId isEqualToString:@"profile"])
        {
            [delegate.Pdislikearray replaceObjectAtIndex:index withObject:getDisLikes];
            [delegate.PdislikesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:btnClicked.tag-1 inSection:0];
        NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
        [mytable reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    }
    //[mytable reloadData];
    
}

-(IBAction)settingButtonClicked:(id)sender
{
    self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:3];
    
}
-(void)userButtonVideoClicked:(NSString*)username
{
    NSLog(@"userbutton clicked");
    AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
    if ([delegate.mynameLogin isEqualToString:username]) {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
    }
    else
    {
        searchedUserView *searchedUser=[self.storyboard instantiateViewControllerWithIdentifier:@"searchedUser"];
        NSString *url = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/userinfo.do?username=%@",username];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSURLRequest *req=[[NSURLRequest alloc]initWithURL:urlrequest];
        NSURLResponse  *response = nil;
        NSError* error;
        [NSURLConnection sendSynchronousRequest: req returningResponse: &response error: &error];
        
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        NSString *stringData= [NSString stringWithContentsOfURL:urlrequest encoding:nil error:nil];
        NSLog(@"Recieved data String &&&&&&&&&&&& %@",stringData);
        
        //NSData *dataofStrig=[NSData dataWithContentsOfFile:stringData];
        NSData * dataofStrig = [stringData dataUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"Recieved data Data &&&&&&&&&&&& %@",stringData);
        
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions                        error:&error];
        if (!json) {
            NSLog(@"Not Json");
        }
        
        NSLog(@"JSON format:- %@",json);
        searchedUser.getfollowers=[json objectForKey:@"followerCount"];
        searchedUser.getfollowing=[json objectForKey:@"followingCount"];
        NSString *userName=[json objectForKey:@"username"];
        searchedUser.getUserSearched=userName;
        NSString *status=[json objectForKey:@"status"];
        searchedUser.getStatusSearched=status;
        NSString *userPic=[json objectForKey:@"profile_image"];
        searchedUser.getImageSearched=userPic;
        searchedUser.getId=@"circle";
        [self.navigationController pushViewController:searchedUser animated:YES];
    }
}


//****************************************************************************************************
-(void)shareButtonClicked:(UIButton*)btnClicked{
    
    NSLog(@"Helllooo activity viewcontroller");
    
    NSArray *activityItems;
    NSURL *profielUrl=[[NSURL alloc]initWithString:imageUrl];
    NSData *profileImageData=[NSData dataWithContentsOfURL:profielUrl];
    
    UIImage *image=[UIImage imageWithData:profileImageData];
    if (image != nil) {
        activityItems = @[@"post from cancer circle iPhone app",image];
    }
    
    else {
        activityItems = @[@"post from cancer circle iPhone app"];
    }
    UIActivityViewController *activityController =
    [[UIActivityViewController alloc]
     initWithActivityItems:activityItems
     applicationActivities:nil];
    
    [self presentViewController:activityController
                       animated:YES completion:nil];
    
    
}


-(IBAction)reportButtonClicked:(UIButton*)btnClicked
{
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    delegate.snoReport=getSno;
    delegate.typeReport=getType;
    if([getusername isEqualToString:delegate.mynameLogin])
    {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:3];
    }
    else{
        UIActionSheet  *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Report for Content" delegate:self
                                                         cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Inappropriate"
                                                         otherButtonTitles:nil];
        actionSheet.backgroundColor = [UIColor greenColor];
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        //[actionSheet showInView:self.view];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    }
}
-(void)actionSheet:(UIActionSheet *)actionSheet2 clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == [actionSheet2 destructiveButtonIndex]) {
        [self performSegueWithIdentifier:@"circleToReport" sender:self];
    } else if(buttonIndex == 1){
        
        
    }
}

- (void)viewWillDisappear:(BOOL)animated {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:IFTweetLabelURLNotification object:nil];
    self.hidesBottomBarWhenPushed = NO;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dealloc {
    [transView release];
    [commentView release];
    [super dealloc];
}
- (IBAction)goCircle:(id)sender {
    self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
}
@end
