//
//  newsDetailViewController.h
//  CancerCircleFinal
//
//  Created by Raminder on 28/06/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "IFTweetLabel.h"
#import "IFLabelUsername.h"

@interface newsDetailViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate>
{
    IBOutlet UIScrollView *myScroll;
    IBOutlet UITableView *mytable;
    NSString *getusername;
    NSMutableArray *commnetsArray;
    NSMutableArray *commentNameArray;
    NSMutableArray *commentPicArray;
    NSMutableArray *setcommentArray;
    NSMutableArray *setcommentCountArray;
    NSMutableArray *typeData;
    IBOutlet UITextField *commentText;
    UILabel *label;
    IFTweetLabel *tweetLabel;
    IFTweetLabel *tweetLabel2;
    NSString *getStory;
    NSMutableArray *infoArray;
    CGRect tableSize;
}
-(IBAction)commentButton;
-(IBAction)hidekeyboard:(id)sender;
-(IBAction)goToNews;
@property(nonatomic,retain)NSString *getUserImageData;
@property(nonatomic,retain)NSString *getLikes;
@property(nonatomic,retain)NSString *getIfLikes;
@property(nonatomic,retain)NSString *getDisLikes;
@property(nonatomic,retain)NSString *getIfDisLikes;
@property(nonatomic,retain)NSString *getSno;
@property(nonatomic,retain)NSString *getName;
@property(nonatomic,retain)NSString *getComments;
@property(nonatomic,retain)NSString *getType;
@property(nonatomic,retain)NSString *getLocation;
@property(nonatomic,retain)NSString *getTime;
@property(nonatomic,retain)NSString *getLikeConferm;
@property(nonatomic,retain)NSString *getDislikeConferm;
@property(nonatomic,retain)NSString *getRowIndex;
@property(nonatomic,retain)NSString *getTabId;
@property (nonatomic, strong) NSOperationQueue *imageDownloadingQueuePic;
@property (nonatomic, strong) NSCache *imageCachePic;
@end
