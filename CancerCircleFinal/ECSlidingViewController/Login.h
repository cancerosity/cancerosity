//
//  Login.h
//  CancerCircleFinal
//
//  Created by Raminder on 29/04/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <Security/Security.h>
#import <Accounts/Accounts.h>
#import "ECSlidingViewController.h"
#import "MobileCoreServices/UTType.h"
#import "MobileCoreServices/UTCoreTypes.h"
#import "KeychainItemWrapper.h"
#import "KDJKeychainItemWrapper.h"
@class Registration;
@interface Login :ECSlidingViewController <UITextFieldDelegate> //UIViewController
{ IBOutlet UIScrollView *myScroll;
    IBOutlet UITextField *userText;
    IBOutlet UITextField *passText;
    UIAlertView *alert;
    NSString *authStr;
    NSString *profileStr;
    NSString *profileId;
    NSString *profileInfo;
    NSString *profileDataStr;
    BOOL flag;
    IBOutlet UIButton *login;
   
    IBOutlet UILabel *testinglabel;
    NSString *prfileStatus;
    NSString *dataStr;
    NSString *dataStr2;
    NSString *PdataStr;
    NSString *PdataStr2;
    NSMutableArray *tags;
    NSString *getInsertedTime;
    NSString *getLastInsertedTime;
    
   
    
    
    NSMutableArray *insertedTimeArray;
    NSMutableArray *PinsertedTimeArray;
    NSMutableArray *fourCommentArray;
    NSMutableArray *PfourCommentArray;
     NSMutableArray *privacyArray;
    NSNumber *following;
    NSNumber *followers;
     UITextField *activeField;
    BOOL isLoggedIN;
    KeychainItemWrapper *keychain;
    KDJKeychainItemWrapper *keychain2;
    BOOL isUserLogged;
    
}
-(void)removeRegister;
@property (nonatomic,retain) Registration *currentViewController;
@property NSInteger childNumber;
- (IBAction)register:(id)sender;
- (BOOL)disablesAutomaticKeyboardDismissal;
@property (nonatomic, retain) ACAccountStore *accountStore;
@property (retain, nonatomic) IBOutlet UIImageView *background;
@property (nonatomic, retain) ACAccount *facebookAccount;
@property(nonatomic,retain)NSString *getId;
@property(nonatomic,retain)NSString *getName;
@property (retain, nonatomic) NSURLConnection *connection;
@property (retain, nonatomic) NSMutableData *receivedData;
@property (strong, nonatomic) NSMutableArray *dataUpdate;
@property (strong, nonatomic) NSMutableArray *dataUpdate2;
@property (retain, nonatomic) IBOutlet UILabel *TCs;
@property (strong, nonatomic) NSMutableArray *differUser;
@property (strong, nonatomic) NSMutableArray *snoArray;
@property (strong, nonatomic) NSMutableArray *likearray;
@property (strong, nonatomic) NSMutableArray *dislikearray;
@property (strong, nonatomic) NSMutableArray *profielImages;
@property (strong, nonatomic) NSMutableArray *imageDatas;
@property (strong, nonatomic) NSMutableArray *dataType;
@property (strong, nonatomic) NSMutableArray *likesNameArray;
@property (retain, nonatomic) IBOutlet UIImageView *passfield;
@property (retain, nonatomic) IBOutlet UIImageView *userfield;
@property (strong, nonatomic) NSMutableArray *dislikesNameArray;
@property (strong, nonatomic) NSMutableArray *commentsArray;
@property (strong, nonatomic) NSMutableArray *commentNameArray;
@property (strong, nonatomic) NSMutableArray *commentPicArray;
@property (strong, nonatomic) NSMutableArray *commentCounts;
@property (strong, nonatomic) NSMutableArray *locationArray;
@property (strong, nonatomic) NSMutableArray *timeArray;

@property (strong, nonatomic) NSMutableArray *PsnoArray;
@property (strong, nonatomic) NSMutableArray *Plikearray;
@property (strong, nonatomic) NSMutableArray *Pdislikearray;
@property (strong, nonatomic) NSMutableArray *PdataUpdate;
@property (strong, nonatomic) NSMutableArray *PdataUpdate2;
@property (strong, nonatomic) NSMutableArray *PdataType;
@property (strong, nonatomic) NSString *followingNames;
@property (strong, nonatomic) NSString *followersNames;
@property (strong, nonatomic) NSMutableArray *PlikesNameArray;
@property (strong, nonatomic) NSMutableArray *PdislikesNameArray;
@property (strong, nonatomic) NSMutableArray *PcommentCounts;
@property (strong, nonatomic) NSMutableArray *PlocationArray;
@property (strong, nonatomic) NSMutableArray *PtimeArray;
@property (retain, nonatomic) IBOutlet UIImageView *imageView;
@property (retain, nonatomic) IBOutlet UIButton *loginFB;
@property (retain, nonatomic) IBOutlet UIButton *loginb;
@property (retain, nonatomic) IBOutlet UIButton *regib;



-(IBAction)login;
-(IBAction)loginWithFB;

-(IBAction)hidekeyboard:(id)sender;



-(void)circleData;
-(void)profileData;
-(NSMutableAttributedString *)atributedString:(NSString *)getString;
-(NSMutableAttributedString *)attibutedUsername:(NSString *)commentString;
-(NSString *)likesContent:(NSString *)allNames;

-(NSString *)smilyString:(NSString *)getString;
@end
