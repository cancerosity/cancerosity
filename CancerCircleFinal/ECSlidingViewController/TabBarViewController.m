//
//  TabBarViewController.m
//  CancerCircleFinal
//
//  Created by Raminder on 29/04/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import "TabBarViewController.h"

@interface TabBarViewController ()

@end

@implementation TabBarViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[NewsViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"News"];
    }
    
    /*if (![self.slidingViewController.underRightViewController isKindOfClass:[UnderRightViewController class]]) {
     self.slidingViewController.underRightViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UnderRight"];
     }*/
    
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    
    
    /* UIImage* tabBarBackground = [UIImage imageNamed:@"tabbar.png"];
     [[UITabBar appearance] setBackgroundImage:tabBarBackground];
     [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"tabbar_selected.png"]];*/
    
   
    [[UITabBar appearance] setSelectionIndicatorImage:[[UIImage alloc] init]];
    UITabBar *tabBar = self.tabBar;
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
    UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
    tabBar.barTintColor = [[UIColor alloc] initWithRed:(0.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:0.8];
    tabBar.tintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    tabBar.backgroundColor = [UIColor clearColor];
  
    /*tabBarItem1.title = @"Home";
     tabBarItem2.title = @"Maps";
     tabBarItem3.title = @"My Plan";
     tabBarItem4.title = @"Settings";*/
    [tabBarItem1 setSelectedImage:[[UIImage imageNamed:@"circle_selected.png"] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem1 setImage:[[UIImage imageNamed:@"circle.png"] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
    
    [tabBarItem2 setSelectedImage:[[UIImage imageNamed:@"profile_selected.png"] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem2 setImage:[[UIImage imageNamed:@"profile.png"] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
    
    [tabBarItem3 setSelectedImage:[[UIImage imageNamed:@"upload_selected.png"] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem3 setImage:[[UIImage imageNamed:@"upload.png"] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
    
    [tabBarItem4 setSelectedImage:[[UIImage imageNamed:@"setting_selected.png"] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem4 setImage:[[UIImage imageNamed:@"setting.png"] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
   

   
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIColor whiteColor], NSForegroundColorAttributeName,
                                                       nil] forState:UIControlStateNormal];
    UIColor *titleHighlightedColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       titleHighlightedColor, NSForegroundColorAttributeName,
                                                       nil] forState:UIControlStateHighlighted];
}

@end
