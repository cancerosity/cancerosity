//
//  newsCell.m
//  CancerCircleFinal
//
//  Created by Raminder on 09/05/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import "newsCell.h"

@implementation newsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (void) layoutSubviews
{
    [super layoutSubviews];
    self.imageView.frame = CGRectMake(10,10,35,35);
    //self.textLabel.frame = CGRectMake(5,5,40,40);
    self.backgroundColor = [UIColor whiteColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}


@end
