//
//  newsDetailViewController.m
//  CancerCircleFinal
//
//  Created by Raminder on 28/06/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import "newsDetailViewController.h"
#import "ECSlidingViewController.h"
#import "NewsViewController.h"
#import "AppDelegate.h"
#import <Social/Social.h>
#import "Login.h"
#import "searchedUserView.h"
#import "OpenUrlView.h"
#import "SearchViewController.h"
#import "CircleFirst.h"
#define FONT_SIZE 11.0f
#define CELL_CONTENT_WIDTH 320.0f
#define CELL_CONTENT_MARGIN 10.0f
#define CELL_CONTENT_WIDTH2 340.0f
#define CELL_CONTENT_MARGIN2 40.0f

@interface newsDetailViewController ()

@end

@implementation newsDetailViewController
@synthesize getDisLikes,getIfDisLikes,getIfLikes,getLikes,getUserImageData,getSno,getName,getComments,getType,getLocation,getTime,getDislikeConferm,getLikeConferm,getRowIndex,getTabId;
-(void)viewWillAppear:(BOOL)animated
{
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleTweetNotification:) name:IFTweetLabelURLNotification object:nil];
    
    myScroll.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"640x960bg.png"]];
    [super viewWillAppear:animated];
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[NewsViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"News"];
    }
    
    /*if (![self.slidingViewController.underRightViewController isKindOfClass:[UnderRightViewController class]]) {
     self.slidingViewController.underRightViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UnderRight"];
     }*/
    
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
}
- (IBAction)revealMenu:(id)sender
{
    [self.slidingViewController anchorTopViewTo:ECRight];
    
}
-(IBAction)goToNews
{
    [self.slidingViewController anchorTopViewTo:ECRight];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    tableSize = mytable.frame;
    
    //myScroll.contentSize=CGSizeMake(320,600);
    //myScroll.pagingEnabled=YES;
    //************* Nvigation bar ****************************************************
    
    self.imageDownloadingQueuePic = [[NSOperationQueue alloc] init];
    self.imageDownloadingQueuePic.maxConcurrentOperationCount = 4;
    self.imageCachePic = [[NSCache alloc] init];
    
    getType=@"story";
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    getusername = delegate.mynameLogin;
    getSno=delegate.newsSerial;
    [self getDataInfo];
    [self commentsData];
    [mytable reloadData];
    UINavigationBar *navBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    
    
    navBar.barTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    navBar.translucent = YES;
        [self.view addSubview:navBar];
}

-(void)getDataInfo
{
    Login *login=[[Login alloc]init];
    infoArray=[[NSMutableArray alloc]initWithCapacity:infoArray.count];
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    NSString *urlString=[NSString stringWithFormat:SERVER_URL@"/content_info.do?sno=%@&type=story",delegate.newsSerial];
    NSLog(@"urlString %@",urlString);
    NSURL *urlRequest=[NSURL URLWithString:urlString];
    NSData *data=[NSData dataWithContentsOfURL:urlRequest];
    NSError* error;
    NSDictionary *json=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    getStory = [[NSString alloc] init];
    getStory=[[json objectForKey:@"data0"] valueForKey:@"content"];
    delegate.contentSTR=getStory;
    [infoArray addObject:getStory];
    getName=[[json objectForKey:@"data0"] valueForKey:@"username"];
    [infoArray addObject:getName];
    NSLog(@"getImageData %@",getStory);
    getUserImageData=[[json objectForKey:@"data0"] valueForKey:@"profile_image"];
    [infoArray addObject:getUserImageData];
    NSLog(@"getUserImageData %@",getUserImageData);
    getLikes=[[json objectForKey:@"data0"] valueForKey:@"like"];
    [infoArray addObject:getLikes];
    NSLog(@"getLikes %@",getLikes);
    getDisLikes=[[json objectForKey:@"data0"] valueForKey:@"dislike"];
    [infoArray addObject:getDisLikes];
    NSLog(@"getDisLikes %@",getDisLikes);
    getComments=[[json objectForKey:@"data0"] valueForKey:@"comment"];
    [infoArray addObject:getComments];
    NSLog(@"getComments %@",getComments);
    NSString *likesName=[[json objectForKey:@"data0"] valueForKey:@"likenames"];
    getLikeConferm=[login likesContent:likesName];
    [infoArray addObject:getLikeConferm];
    NSString *dislikesName=[[json objectForKey:@"data0"] valueForKey:@"dislikenames"];
    getDislikeConferm=[login likesContent:dislikesName];
    [infoArray addObject:getDislikeConferm];
    getLocation=[[json objectForKey:@"data0"] valueForKey:@"location"];
    [infoArray addObject:getLocation];
    getTime=[[json objectForKey:@"data0"] valueForKey:@"Elapsed"];
    [infoArray addObject:getTime];
    [mytable reloadData];
}
-(void)commentsData
{
    NSLog(@"image view comments data");
    commnetsArray=[[NSMutableArray alloc]initWithCapacity:commnetsArray.count];
    commentNameArray=[[NSMutableArray alloc]initWithCapacity:commentNameArray.count];
    commentPicArray=[[NSMutableArray alloc]initWithCapacity:commentPicArray.count];
    typeData=[[NSMutableArray alloc]initWithCapacity:typeData.count];
    [commentNameArray addObject:@"nothing"];
    [commnetsArray addObject:@"nothing"];
    [commentPicArray addObject:@"nothing"];
    [typeData addObject:@"image"];
    
    NSString *urlComment = [NSString stringWithFormat:SERVER_URL@"/showcomment.do?sno=%@&type=story",getSno];
    NSURL *urlrequest=[NSURL URLWithString:urlComment];
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    
    //NSString *stringData= [NSString stringWithContentsOfURL:urlrequest encoding:nil error:nil];
    // NSLog(@"Recieved data String &&&&&&&&&&&& %@",stringData);
    
    NSData *dataofStrig=[NSData dataWithContentsOfURL:urlrequest];
    //NSData * dataofStrig = [stringData dataUsingEncoding:NSUTF8StringEncoding];
    NSError* error;
    
    if(!dataofStrig )
    {
        NSLog(@"errorrrrr no response");
        UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Connection failed" message:@"Check intenet connection" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
        [alert1 show];
        
    }
    
    else{
        NSLog(@"comments data %@",dataofStrig);
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        int length=[json count];
        //getComments=[json objectForKey:@"count"];
        
        for(int i=0;i<length-2;i++)
        {
            NSString *indexStr = [NSString stringWithFormat:@"%d",i];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            NSString *commentStr=[[json objectForKey:data]valueForKey:@"comment"];
            if ([commentStr isEqualToString:@""]) {
                
            }
            else{
                NSArray *info=[commentStr componentsSeparatedByString:@"-SEPARATOR-"];
                [commentNameArray addObject:[info objectAtIndex:0]];
                [commnetsArray addObject:[info objectAtIndex:1]];
                [commentPicArray addObject:[info objectAtIndex:2]];                [typeData addObject:@"comment"];
                [mytable reloadData];
            }
        }
    }
    
}
-(IBAction)hidekeyboard:(id)sender
{
    [commentText resignFirstResponder];
    CGPoint point = mytable.contentOffset;
    point .y = 0;
    mytable.contentOffset = point;
    [self.view endEditing:YES];
    
}

- (void)scrollViewToCenterOfScreen:(UIView *)theView {
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    CGFloat availableHeight = applicationFrame.size.height - 160;            // Remove area covered by keyboard
    CGFloat y = viewCenterY - availableHeight / 1.5;
    //CGFloat y = viewCenterY - availableHeight / 2.5;
    if (y < 0) {
        y = 0;
    }
    [myScroll setContentOffset:CGPointMake(0, y) animated:YES];
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    [self scrollViewToCenterOfScreen:textField];
    return YES;
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //[scrollView setContentOffset:svos animated:YES];
    
    //[textField resignFirstResponder];
    BOOL isDone = YES;
	
	if (isDone)
    {
		//[self finishedSearching];
        [myScroll setContentOffset:CGPointMake(0, 0) animated:YES];
		return YES;
	} else
    {
		return NO;
	}
}
-(IBAction)commentButton
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if([commentText.text isEqualToString:@""])
    {
        
    }
    else{
        NSString *converted = [commentText.text stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/comment.do?username=%@&sno=%@&type=story&comment=%@",getusername,getSno,converted];
        NSLog(@"comment Url %@",urlLike);
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSString *newCounts=[json objectForKey:@"count"];
        getComments=newCounts;
        [infoArray insertObject:getComments atIndex:5];
        /*  int index=[getRowIndex intValue];
         if ([getTabId isEqualToString:@"circle"]) {
         [delegate.commentCounts replaceObjectAtIndex:index withObject:newCounts];
         NSArray *comArray=[[delegate.fourCommentArray objectAtIndex:index] componentsSeparatedByString:@"-END_OF_DATA-LINE-"];
         if ([comArray count]==5) {
         
         NSString *oldArray=[[[[[[comArray objectAtIndex:1]stringByAppendingFormat:@"-END_OF_DATA-LINE-"]
         stringByAppendingFormat:@"%@",[comArray objectAtIndex:2]]stringByAppendingFormat:@"-END_OF_DATA-LINE-"]
         stringByAppendingFormat:@"%@",[comArray objectAtIndex:3]]stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
         
         NSString *commentStr=[[[[oldArray stringByAppendingFormat:@"@ "] stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
         [delegate.fourCommentArray replaceObjectAtIndex:index withObject:commentStr];
         }
         else if([[delegate.fourCommentArray objectAtIndex:index] isEqualToString:@"No Comments"])
         {
         NSString *commentStr=[[[@"@" stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
         [delegate.fourCommentArray replaceObjectAtIndex:index withObject:commentStr];
         }
         else
         {
         NSString *commentStr=[[[[[delegate.fourCommentArray objectAtIndex:index]stringByAppendingFormat:@"@"] stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
         [delegate.fourCommentArray replaceObjectAtIndex:index withObject:commentStr];
         }
         }
         else if([getTabId isEqualToString:@"profile"])
         {
         [delegate.PcommentCounts replaceObjectAtIndex:index withObject:newCounts];
         NSArray *comArray=[[delegate.PfourCommentArray objectAtIndex:index] componentsSeparatedByString:@"-END_OF_DATA-LINE-"];
         if ([comArray count]==5) {
         
         NSString *oldArray=[[[[[[comArray objectAtIndex:1]stringByAppendingFormat:@"-END_OF_DATA-LINE-"]
         stringByAppendingFormat:@"%@",[comArray objectAtIndex:2]]stringByAppendingFormat:@"-END_OF_DATA-LINE-"]
         stringByAppendingFormat:@"%@",[comArray objectAtIndex:3]]stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
         
         NSString *commentStr=[[[[oldArray stringByAppendingFormat:@"@ "] stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
         [delegate.PfourCommentArray replaceObjectAtIndex:index withObject:commentStr];
         }
         else if([[delegate.PfourCommentArray objectAtIndex:index] isEqualToString:@"No Comments"])
         {
         NSString *commentStr=[[[@"@" stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
         [delegate.PfourCommentArray replaceObjectAtIndex:index withObject:commentStr];
         }
         else
         {
         NSString *commentStr=[[[[[delegate.PfourCommentArray objectAtIndex:index]stringByAppendingFormat:@"@"] stringByAppendingFormat:@"%@",getusername] stringByAppendingFormat:@" %@",commentText.text] stringByAppendingFormat:@"-END_OF_DATA-LINE-"];
         [delegate.PfourCommentArray replaceObjectAtIndex:index withObject:commentStr];
         }
         
         }*/
        NSString *resultStr=[json objectForKey:@"param"];
        if([resultStr isEqualToString:@"true"])
        {
            //[self commentsData];
            [typeData addObject:@"comment"];
            [commnetsArray addObject:commentText.text];
            [commentPicArray addObject:delegate.profileImage];
            [commentNameArray addObject:getusername];
            [mytable reloadData];
            commentText.text=@"";
        }
    }
    
}
- (void)handleTweetNotification:(NSNotification *)notification
{
    NSDictionary *dict = (NSDictionary*)notification.object;
    NSString *strDict = [NSString stringWithFormat:@"%@", dict];
    if([strDict hasPrefix:@"http://"])
    {  OpenUrlView *openUrl=[self.storyboard instantiateViewControllerWithIdentifier:@"OpenUrl"];
        //openUrl.hidesBottomBarWhenPushed = YES;
        openUrl.getUrl=strDict;
        //[self.navigationController pushViewController:openUrl animated:YES];
    }
    {
        AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
        delegate.wordToSearch=strDict;
        if (([strDict hasPrefix:@"#"])||([strDict hasPrefix:@"@"])) {
            //[self performSegueWithIdentifier:@"circleToSearch" sender:self];
          
            UINavigationBar *navBar = [[self navigationController] navigationBar];
            navBar.hidden=false;
            //[self.navigationController pushViewController:search animated:YES];
        }
        else
        {
            //[self userButtonVideoClicked:strDict];
        }
        
        
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *typeString=[typeData objectAtIndex:indexPath.row];
    int h;
    if([typeString isEqualToString:@"image"])
    {
        //height= 360;
        
        
        
        NSString *text = getStory;
        
        CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 4), 20000.0f);
        
        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:FONT_SIZE] forKey: NSFontAttributeName];
        
        CGSize size = [text boundingRectWithSize:constraint
                                         options:NSLineBreakByCharWrapping
                                      attributes:stringAttributes context:nil].size;
        
        
        CGFloat height = MAX(size.height, 44.0f);
        
        h = height + (CELL_CONTENT_MARGIN * 2)+135;
    }
    if([typeString isEqualToString:@"comment"]){
        // h=50;
        NSString *text = [commnetsArray objectAtIndex:indexPath.row];
        
        CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH2, 20000.0f);
        
        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:FONT_SIZE] forKey: NSFontAttributeName];
        
        CGSize size = [text boundingRectWithSize:constraint
                                         options:NSLineBreakByCharWrapping
                                      attributes:stringAttributes context:nil].size;
        
        CGFloat height = MAX(size.height, 20.0f);
        
        h = height + (20 * 2);
    }
    
    
    return h;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section{
    
    return [commnetsArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"GET INFO %@",infoArray);
    
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    Login *login=[[Login alloc]init];
    UIImage *minus = [UIImage imageNamed:@"dislikebutton2.png"];
    UIImage *plus = [UIImage imageNamed:@"likebutton2.png"];
    UIImage *minusDark = [UIImage imageNamed:@"minusDark.png"];
    UIImage *plusDark = [UIImage imageNamed:@"plusDark.png"];
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cellReturn=nil;
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    static NSString *CellIdentifier2 = @"Cell2";
    UITableViewCell *cell2 =[tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
    mytable.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell2.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    [mytable setBackgroundView:[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"307x410.png"]] autorelease]];
    cell.backgroundView = [ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cellWhiteBg.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];
    cell2.backgroundView = [ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cellWhiteBg.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];
    
    
    
    NSString *typeString=[typeData objectAtIndex:indexPath.row];
    int t=indexPath.row;
    NSLog(@"index path row %i",t);
    if([typeString isEqualToString:@"image"])
    {
        if (cell == nil) {
            cell = [[UITableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:CellIdentifier];
            
        }
        for(UIView *v in [cell.contentView subviews])
        {
            if([v isKindOfClass:[UILabel class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[IFTweetLabel class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[IFLabelUsername class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[UIImageView class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[UIButton class]])
                [v removeFromSuperview];
        }
        
        tweetLabel = [[IFTweetLabel alloc] initWithFrame:CGRectZero];
        [tweetLabel setNumberOfLines:0];
        [tweetLabel setFont:[UIFont systemFontOfSize:FONT_SIZE]];
        [tweetLabel setTag:1];
        [tweetLabel setLinksEnabled:TRUE];
        tweetLabel.clipsToBounds=YES;
        //NSLog(@"Story Story %@",getStory);
        NSString *text = delegate.contentSTR;
        
        CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
        
        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:FONT_SIZE] forKey: NSFontAttributeName];
        
        CGSize size = [text boundingRectWithSize:constraint
                                         options:NSLineBreakByWordWrapping
                                      attributes:stringAttributes context:nil].size;
        
        
        
        if (!tweetLabel)
            tweetLabel = (IFTweetLabel*)[cell viewWithTag:1];
        tweetLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
        NSString *smile=[login smilyString:text];
        [tweetLabel setText:smile];
        
        [tweetLabel setFrame:CGRectMake(CELL_CONTENT_MARGIN+5, CELL_CONTENT_MARGIN, CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2)-5, MAX(size.height, 44.0f))];
        [cell.contentView addSubview:tweetLabel];
        
        int lbltag=indexPath.row+1;
        UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(15, MAX(size.height, 44.0f)+15,40,40)];
        imageView2.layer.cornerRadius = 5;
        imageView2.clipsToBounds = YES;
        [cell.contentView addSubview:imageView2];
        
        NSString *picUrlString = getUserImageData;
        UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
        
        if (cachedPic)
        {
            //imgCell.imageView.image = cachedImage;
            imageView2.image = cachedPic;
            
        }
        else
        {
            // you'll want to initialize the image with some blank image as a placeholder
            
            //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
            imageView2.image = [UIImage imageNamed:@"editPic.png"];
            [self.imageDownloadingQueuePic addOperationWithBlock:^{
                NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                UIImage *image    = nil;
                if (imageData)
                    image = [UIImage imageWithData:imageData];
                if (image)
                {
                    // add the image to your cache
                    
                    [self.imageCachePic setObject:image forKey:picUrlString];
                    
                    // finally, update the user interface in the main queue
                    
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                        if (updateCell)
                            imageView2.image = image;
                        
                    }];
                }
            }];
        }
        
        IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(70,MAX(size.height, 44.0f)+15,100,30)];
        nameLabel.numberOfLines=0;
        //nameLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
        nameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:12];
        NSString *smile2=getName;//[@"@" stringByAppendingString:[differUser objectAtIndex:in]];
        [nameLabel setText:smile2];
        [nameLabel setLinksEnabled:TRUE];
        nameLabel.clipsToBounds=YES;
        [cell.contentView addSubview:nameLabel];
        
        UILabel *placeLabel=[[UILabel alloc]initWithFrame:CGRectMake(120,MAX(size.height, 44.0f)+15,180,10)];
        placeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
        placeLabel.font = [UIFont systemFontOfSize:8.0];
        placeLabel.text=getLocation;
        placeLabel.textAlignment=NSTextAlignmentRight;
        [cell.contentView addSubview:placeLabel];
        UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(120,MAX(size.height, 44.0f)+25,180,10)];
        timeLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
        timeLabel.font = [UIFont systemFontOfSize:8.0];
        timeLabel.text=getTime;
        timeLabel.textAlignment=NSTextAlignmentRight;
        [cell.contentView addSubview:timeLabel];
        
        UILabel *likeShow = nil;
        likeShow=[[[UILabel alloc]initWithFrame:CGRectMake(26, MAX(size.height, 44.0f)+85,43,10)] autorelease];
        likeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
        likeShow.text =getLikes;// [NSString stringWithFormat:@"%d",[getLikes intValue]];
        likeShow.font=[UIFont systemFontOfSize:12.0];
        likeShow.tag = lbltag;
        likeShow.textAlignment=NSTextAlignmentCenter;
        [cell addSubview:likeShow];
        UILabel *dislikeShow = nil;
        dislikeShow.backgroundColor=[UIColor redColor];
        dislikeShow=[[[UILabel alloc]initWithFrame:CGRectMake(80, MAX(size.height, 44.0f)+85,43,10)] autorelease];
        dislikeShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
        dislikeShow.text =getDisLikes;
        dislikeShow.font=[UIFont systemFontOfSize:12.0];
        dislikeShow.textAlignment=NSTextAlignmentCenter;
        [cell addSubview:dislikeShow];
        
        
        UILabel *commentShow = nil;
        commentShow=[[[UILabel alloc]initWithFrame:CGRectMake(134, MAX(size.height, 44.0f)+85,43,10)] autorelease];
        commentShow.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0.2 alpha:1];
        commentShow.text = [NSString stringWithFormat:@"%d",[getComments intValue]];
        commentShow.font=[UIFont systemFontOfSize:12.0];
        commentShow.tag = lbltag;
        commentShow.textAlignment=NSTextAlignmentCenter;
        [cell addSubview:commentShow];
        
        UIButton *likeBtn = nil;
        likeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        likeBtn.tag = indexPath.row+1;
        [likeBtn addTarget:self action:@selector(likeFn:) forControlEvents:UIControlEventTouchUpInside];
        likeBtn.frame=CGRectMake(26,MAX(size.height, 44.0f)+95,44,30);
        if([getLikeConferm  isEqualToString:@"Yes"])
        {
            likeBtn.userInteractionEnabled=NO;
            [likeBtn setBackgroundImage:plusDark forState:UIControlStateNormal];
        }
        else
        {
            [likeBtn setBackgroundImage:plus forState:UIControlStateNormal];
        }
        
        [cell addSubview:likeBtn];
        
        
        
        
        UIButton *dislikeBtn;
        dislikeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        dislikeBtn.tag = indexPath.row+1;
        [dislikeBtn addTarget:self action:@selector(dislikeFn:) forControlEvents:UIControlEventTouchUpInside];
        dislikeBtn.frame=CGRectMake(80,MAX(size.height, 44.0f)+95,44,30);
        if([getDislikeConferm isEqualToString:@"Yes"])
        {
            dislikeBtn.userInteractionEnabled=NO;
            [dislikeBtn setBackgroundImage:minusDark forState:UIControlStateNormal];
        }
        else
        {
            [dislikeBtn setBackgroundImage:minus forState:UIControlStateNormal];
        }
        [cell addSubview:dislikeBtn];
        
        UIButton *commentButton=nil;
        commentButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        UIImage *comment = [UIImage imageNamed:@"commentbutton2.png"];
        commentButton.tag = indexPath.row+1;
        [commentButton setBackgroundImage:comment forState:UIControlStateNormal];
        commentButton.titleLabel.font = [UIFont systemFontOfSize:10.0];
        [commentButton setFrame:CGRectMake(134,MAX(size.height, 44.0f)+95,44,30)];
        [commentButton addTarget:self action:@selector(commentClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:commentButton];
        
        
        UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        UIImage *share = [UIImage imageNamed:@"share_button2.png"];
        shareButton.tag = indexPath.row+1;
        [shareButton setBackgroundImage:share forState:UIControlStateNormal];
        [shareButton setTitle:@"" forState:UIControlStateNormal];
        [shareButton setFrame:CGRectMake(188, MAX(size.height, 44.0f)+95,44,30)];
        [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:shareButton];
        
        UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        UIImage *setting = [UIImage imageNamed:@"bottom-setting2.png"];
        settingsButton.tag = indexPath.row+1;
        [settingsButton setBackgroundImage:setting forState:UIControlStateNormal];
        [settingsButton setTitle:@"" forState:UIControlStateNormal];
        [settingsButton setFrame:CGRectMake(242, MAX(size.height, 44.0f)+95,44,30)];
        [settingsButton addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:settingsButton];
        
        
        cellReturn=cell;
    }
    else if([typeString isEqualToString:@"comment"])
    {
        if (cell2 == nil) {
            cell2 = [[UITableViewCell alloc]
                     initWithStyle:UITableViewCellStyleDefault
                     reuseIdentifier:CellIdentifier2];
        }
        for(UIView *v in [cell2.contentView subviews])
        {
            if([v isKindOfClass:[UILabel class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[IFTweetLabel class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[IFLabelUsername class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[UIImageView class]])
                [v removeFromSuperview];
            
            if([v isKindOfClass:[UIButton class]])
                [v removeFromSuperview];
        }
        
        
        UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(10,5,35,35)];
        [cell2.contentView addSubview:imageView2];
        
        NSString *picUrlString = [commentPicArray objectAtIndex: indexPath.row];
        UIImage *cachedPic = [self.imageCachePic objectForKey:picUrlString];
        
        if (cachedPic)
        {
            //imgCell.imageView.image = cachedImage;
            imageView2.image = cachedPic;
            
        }
        else
        {
            // you'll want to initialize the image with some blank image as a placeholder
            
            //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
            imageView2.image = [UIImage imageNamed:@"editPic.png"];
            [self.imageDownloadingQueuePic addOperationWithBlock:^{
                NSURL *imageURL   = [NSURL URLWithString:picUrlString];
                NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                UIImage *image    = nil;
                if (imageData)
                    image = [UIImage imageWithData:imageData];
                if (image)
                {
                    // add the image to your cache
                    
                    [self.imageCachePic setObject:image forKey:picUrlString];
                    
                    // finally, update the user interface in the main queue
                    
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                        if (updateCell)
                            imageView2.image = image;
                        
                    }];
                }
            }];
        }
        
        IFLabelUsername *nameLabel=[[IFLabelUsername alloc]initWithFrame:CGRectMake(55,5,200,20)];
        nameLabel.numberOfLines=0;
        //nameLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
        nameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:12];
        NSString *smile2=[commentNameArray objectAtIndex:t];//[@"@" stringByAppendingString:[differUser objectAtIndex:in]];
        [nameLabel setText:smile2];
        [nameLabel setLinksEnabled:TRUE];
        nameLabel.clipsToBounds=YES;
        
        [cell2.contentView addSubview:nameLabel];
        
        UILabel *commentlabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [commentlabel setLineBreakMode:NSLineBreakByWordWrapping];
        commentlabel.adjustsFontSizeToFitWidth = YES;
        [commentlabel setNumberOfLines:0];
        [commentlabel setFont:[UIFont systemFontOfSize:11]];
        [commentlabel setTag:1];
        
        tweetLabel2 = [[IFTweetLabel alloc] initWithFrame:CGRectZero];
        [tweetLabel2 setLinksEnabled:TRUE];
        tweetLabel2.clipsToBounds=YES;
        [tweetLabel2 setNumberOfLines:0];
        [tweetLabel2 setFont:[UIFont systemFontOfSize:FONT_SIZE]];
        [tweetLabel2 setTag:1];
        
        // [[label layer] setBorderWidth:2.0f];
        
        //[[cell contentView] addSubview:label];
        
        NSString *text = [commnetsArray objectAtIndex:t];
        
        CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH2, 20000.0f);
        
        NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:FONT_SIZE] forKey: NSFontAttributeName];
        
        CGSize size = [text boundingRectWithSize:constraint
                                         options:NSLineBreakByWordWrapping
                                      attributes:stringAttributes context:nil].size;
        
        
        if (!tweetLabel2)
            tweetLabel2 = (IFTweetLabel*)[cell viewWithTag:1];
        tweetLabel2.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
        NSString *smile=[login smilyString:text];
        [tweetLabel2 setText:smile];
        tweetLabel2.textColor=[UIColor blackColor];
        [tweetLabel2 setFrame:CGRectMake(CELL_CONTENT_MARGIN2+15,20, CELL_CONTENT_WIDTH2 - (CELL_CONTENT_MARGIN2*2)-15, MAX(size.height,20.0f))];
        [tweetLabel2 sizeToFit];
        [cell2.contentView addSubview:tweetLabel2];
        
        /*UILabel *palceLabel=[[UILabel alloc]initWithFrame:CGRectMake(60,20,200,20)];
         palceLabel.numberOfLines=0;
         palceLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:11];
         NSMutableAttributedString *attributed=[login atributedString:[commnetsArray objectAtIndex:t]];
         palceLabel.attributedText=attributed;
         [cell2.contentView addSubview:palceLabel];*/
        cellReturn=cell2;
    }
    return cellReturn;
}
-(void)commentClicked:(UIButton*)btnClicked{
    
    [commentText becomeFirstResponder];
    
}

-(void)likeFn:(UIButton*)btnClicked
{
    NSLog(@"Tab Id %@",getTabId);
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    if([getLikeConferm isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend =getSno;
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=story&username=%@&action=liked",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        NSString *count=[json valueForKey:@"count"];
        getLikes=count;
        getLikeConferm=@"Yes";
        int index=[getRowIndex intValue];
        if ([getTabId isEqualToString:@"circle"]) {
            [delegate.likearray replaceObjectAtIndex:index withObject:getLikes];
            [delegate.likesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        else if([getTabId isEqualToString:@"profile"])
        {
            [delegate.Plikearray replaceObjectAtIndex:index withObject:getLikes];
            [delegate.PlikesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        else if([getTabId isEqualToString:@"search"])
        {
            NSUInteger index1 = [delegate.snoArray indexOfObject:getSno];
            NSString *datatype=[delegate.dataType objectAtIndex:index1];
            NSLog(@"DataType %@",datatype);
            if([datatype isEqualToString:@"text"])
            {
                [delegate.likearray replaceObjectAtIndex:index1 withObject:getLikes];
                [delegate.likesNameArray replaceObjectAtIndex:index1 withObject:@"Yes"];
            }
            
            //[delegate.Slikearray replaceObjectAtIndex:index withObject:getLikes];
            //[delegate.SlikesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        
        NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:btnClicked.tag-1 inSection:0];
        NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
        [mytable reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
        //[mytable reloadData];
    }
    
}
-(void)dislikeFn:(UIButton*)btnClicked
{
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    if([getDislikeConferm isEqualToString:@"Yes"])
    {
        
    }
    else{
        NSString *indexrowtosend =getSno;
        NSString *urlLike = [NSString stringWithFormat:SERVER_URL@"/liked.do?sno=%@&type=story&username=%@&action=dislike",indexrowtosend,getusername];
        
        NSURL *urlrequestLike =[NSURL URLWithString:urlLike ];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequestLike);
        NSError* error;
        NSData *dataLike =[NSData dataWithContentsOfURL:urlrequestLike];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        NSLog(@"returned count from like action %@",[json valueForKey:@"count"]);
        NSString *count=[json valueForKey:@"count"];
        getDisLikes=count;
        getDislikeConferm=@"Yes";
        int index=[getRowIndex intValue];
        if ([getTabId isEqualToString:@"circle"]) {
            [delegate.dislikearray replaceObjectAtIndex:index withObject:getDisLikes];
            [delegate.dislikesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        else if([getTabId isEqualToString:@"profile"])
        {
            [delegate.Pdislikearray replaceObjectAtIndex:index withObject:getDisLikes];
            [delegate.PdislikesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        else if([getTabId isEqualToString:@"search"])
        {
            NSUInteger index1 = [delegate.snoArray indexOfObject:getSno];
            NSString *datatype=[delegate.dataType objectAtIndex:index1];
            NSLog(@"DataType %@",datatype);
            if([datatype isEqualToString:@"text"])
            {
                [delegate.dislikearray replaceObjectAtIndex:index1 withObject:getDisLikes];
                [delegate.dislikesNameArray replaceObjectAtIndex:index1 withObject:@"Yes"];
            }
            
            //[delegate.Slikearray replaceObjectAtIndex:index withObject:getLikes];
            //[delegate.SlikesNameArray replaceObjectAtIndex:index withObject:@"Yes"];
        }
        
        NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:btnClicked.tag-1 inSection:0];
        NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
        [mytable reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    }
    //[mytable reloadData];
    
}
-(IBAction)userButtonVideoClicked:(NSString*)username
{
    NSLog(@"userbutton clicked");
    AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
    if ([delegate.mynameLogin isEqualToString:username]) {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
    }
    else
    {
        searchedUserView *searchedUser=[self.storyboard instantiateViewControllerWithIdentifier:@"searchedUser"];
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/userinfo.do?username=%@",username];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSURLRequest *req=[[NSURLRequest alloc]initWithURL:urlrequest];
        NSURLResponse  *response = nil;
        NSError* error;
        [NSURLConnection sendSynchronousRequest: req returningResponse: &response error: &error];
        
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        NSString *stringData= [NSString stringWithContentsOfURL:urlrequest encoding:nil error:nil];
        NSLog(@"Recieved data String &&&&&&&&&&&& %@",stringData);
        NSData * dataofStrig = [stringData dataUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"Recieved data Data &&&&&&&&&&&& %@",stringData);
        
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions                        error:&error];
        if (!json) {
            NSLog(@"Not Json");
        }
        
        NSLog(@"JSON format:- %@",json);
        int length=[json count];
        for(int i=0;i<(length-1);i++)    {
            
            NSString *indexStr = [NSString stringWithFormat:@"%d",i];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            searchedUser.getfollowers=[[json objectForKey:data] valueForKey:@"followerCount"];
            searchedUser.getfollowing=[[json objectForKey:data] valueForKey:@"followingCount"];
            NSString *userName=[[json objectForKey:data] valueForKey:@"username"];
            searchedUser.getUserSearched=userName;
            NSString *status=[[json objectForKey:data]valueForKey:@"status"];
            searchedUser.getStatusSearched=status;
            NSString *userPic=[[json objectForKey:data]valueForKey:@"profile_image"];
            searchedUser.getImageSearched=userPic;
        }
        
        searchedUser.getId=@"circle";
        
        [self.navigationController pushViewController:searchedUser animated:YES];
    }
}


//****************************************************************************************************
-(void)shareButtonClicked:(UIButton*)btnClicked{
    
    NSLog(@"Helllooo activity viewcontroller");
    
    NSArray *activityItems;
    
    activityItems = @[getStory];
    
    UIActivityViewController *activityController =
    [[UIActivityViewController alloc]
     initWithActivityItems:activityItems
     applicationActivities:nil];
    
    [self presentViewController:activityController
                       animated:YES completion:nil];
    
    
}


-(IBAction)reportButtonClicked:(UIButton*)btnClicked
{
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    delegate.snoReport=getSno;
    delegate.typeReport=getType;
    if([getusername isEqualToString:delegate.mynameLogin])
    {
        //[self performSegueWithIdentifier:@"sDetailToSettings" sender:self];
        
    }
    else{
        UIActionSheet  *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Report for Content" delegate:self
                                                         cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Inappropriate"
                                                         otherButtonTitles:nil];
        actionSheet.backgroundColor = [UIColor greenColor];
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
        //[actionSheet showInView:self.view];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    }
}
-(void)actionSheet:(UIActionSheet *)actionSheet2 clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == [actionSheet2 destructiveButtonIndex]) {
        [self performSegueWithIdentifier:@"circleToReport" sender:self];
    } else if(buttonIndex == 1){
        
        
    }
}
- (void)viewWillDisappear:(BOOL)animated {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:IFTweetLabelURLNotification object:nil];
    if([getTabId isEqualToString:@"profile"])
    {
        UINavigationBar *navBar = [[self navigationController] navigationBar];
        navBar.hidden=true;
        /*NSTimer* myTimer = [NSTimer scheduledTimerWithTimeInterval: 00.0000000 target: self
         selector: @selector(hideNavBar:) userInfo: nil repeats: YES];*/
        
    }
    if([getTabId isEqualToString:@"profile2"])
    {
        UINavigationBar *navBar = [[self navigationController] navigationBar];
        navBar.hidden=true;
        /*NSTimer* myTimer = [NSTimer scheduledTimerWithTimeInterval: 00.0000000 target: self
         selector: @selector(hideNavBar:) userInfo: nil repeats: YES];*/
        
    }
}


@end
