//
//  iNewsViewController.h
//  CancerCircleFinal
//
//  Created by Raminder on 28/06/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MobileCoreServices/UTCoreTypes.h"
#import "MobileCoreServices/UTType.h"
#import "IFTweetLabel.h"
#import "imageCell.h"
#import "IFLabelUsername.h"
#import "ILTranslucentView.h"
@interface iNewsViewController : UIViewController
<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate>
{
    IBOutlet UIScrollView *myScroll;
    IBOutlet UITableView *mytable;
    NSString *getusername;
    NSMutableArray *commnetsArray;
    NSMutableArray *commentNameArray;
    NSMutableArray *commentPicArray;
    NSMutableArray *setcommentArray;
    NSMutableArray *setcommentCountArray;
    NSMutableArray *typeData;
    // NSMutableArray *tags;
    IBOutlet UITextField *commentText;
    UILabel *label;
    IFTweetLabel *tweetLabel2;
    NSString *imageUrl;
    NSMutableArray *infoArray;
    CGSize commentHeight;
    IBOutlet ILTranslucentView *transView;
    UINavigationBar *navBar;
    NSString *idAtUpload;
    
    IBOutlet UIView *commentView;
}
-(IBAction)commentButton;
-(IBAction)hidekeyboard:(id)sender;
-(IBAction)goToNews;
@property(nonatomic,retain)NSData *meImageData;
@property(nonatomic,retain)NSString *getUserImageData;
@property(nonatomic,retain)NSString *getLikes;
@property(nonatomic,retain)NSString *getIfLikes;
@property(nonatomic,retain)NSString *getDisLikes;
@property(nonatomic,retain)NSString *getIfDisLikes;
@property(nonatomic,retain)NSString *getSno;
@property(nonatomic,retain)NSString *getName;
@property(nonatomic,retain)NSString *getComments;
@property(nonatomic,retain)NSString *getType;
@property(nonatomic,retain)NSString *getLocation;
@property(nonatomic,retain)NSString *getTime;
@property(nonatomic,retain)NSString *getLikeConferm;
@property(nonatomic,retain)NSString *getDislikeConferm;
@property(nonatomic,retain)NSString *getRowIndex;
@property(nonatomic,retain)NSString *getTabId;
@property(nonatomic,retain)UIImage *getImage;
@property(nonatomic,retain)UIImage *getUserImage;
@property (nonatomic, strong) NSOperationQueue *imageDownloadingQueue;
@property (nonatomic, strong) NSCache *imageCache;
@property (nonatomic, strong) NSOperationQueue *imageDownloadingQueuePic;
@property (nonatomic, strong) NSCache *imageCachePic;
- (IBAction)goCircle:(id)sender;
- (void)scrollViewDidScroll:(UIScrollView*)aScrollView;

@end
