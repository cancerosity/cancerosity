//
//  Login.m
//  CancerCircleFinal
//
//  Created by Raminder on 29/04/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import "Login.h"
#import "AppDelegate.h"
#import "ForgotPassword.h"
#import "Registration.h"
#import "KeychainItemWrapper.h"

#import <Security/Security.h>
#define TAG_CFAIL 1
#define TAG_FAIL 2
@interface Login ()

@end

@implementation Login

@synthesize getId;
@synthesize getName;
@synthesize connection,receivedData;
@synthesize dataUpdate,snoArray,likearray,dislikearray,profielImages,imageDatas,dataType,likesNameArray,dislikesNameArray,differUser,commentNameArray,commentsArray,commentPicArray,locationArray,timeArray;
@synthesize PdataType,PdataUpdate,Pdislikearray,Plikearray,PsnoArray,commentCounts,followersNames,followingNames,PdislikesNameArray,PlikesNameArray,PcommentCounts,PlocationArray,PtimeArray;

UIActivityIndicatorView *indicator;


-(void)viewDidLoad
{
    // myScroll.contentSize=CGSizeMake(320,600);
    // myScroll.pagingEnabled=YES;
    
    //[self updateView];
    keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"Cancerosity" accessGroup:nil];
    [keychain setObject:kSecAttrAccessibleWhenUnlocked forKey:kSecAttrAccessible];
    
    NSLog(@"%@, %@", [keychain objectForKey:kSecAttrAccount], [keychain objectForKey:kSecValueData]);

    [keychain setObject:@"Myappstring" forKey: (__bridge id)kSecAttrService];


    
    NSString *userName = [keychain objectForKey:(__bridge id)kSecAttrAccount];
    NSString *userPass = [keychain objectForKey:(__bridge id)kSecValueData];
    
    userText.text = userName;
    passText.text = userPass;
    passText.secureTextEntry = YES;
    
    userText.delegate = self;
    passText.delegate = self;
    if(isUserLogged == TRUE){
        NSLog(@"AUTO Login ON");
    }
    else {
         NSLog(@"AUTO Login OFF");
    }
    

    
    AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
    delegate.mynameLogin=userText.text;
   
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 400, 44)];
    label.backgroundColor = [UIColor clearColor];
    myScroll.backgroundColor = [[UIColor alloc] initWithRed:(57.0/255.0) green:(188.0/255.0) blue:(63.0/255.0) alpha:1.0];
    label.font = [UIFont fontWithName:@"Futura_Light" size:20];
  
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=@"CancerCircle";
    [super viewDidLoad];
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    [navBar setTintColor:[UIColor colorWithRed:0.4 green:0.753 blue:0.047 alpha:1]];
    UIImage *background = [UIImage imageNamed:@"navigationBar.png"];
    [navBar setBackgroundImage:background forBarMetrics:UIBarMetricsDefault];
    _imageView.layer.cornerRadius = 5;
   _imageView.layer.masksToBounds = YES;
    _loginFB.layer.cornerRadius = 5;
    _loginFB.layer.masksToBounds = YES;
    _loginb.layer.cornerRadius = 5;
    _loginb.layer.masksToBounds = YES;
    _regib.layer.cornerRadius = 5;
    _regib.layer.masksToBounds = YES;
    _passfield.layer.cornerRadius = 3;
    _passfield.layer.masksToBounds = YES;
    _userfield.layer.cornerRadius = 3;
    _userfield.layer.masksToBounds = YES;
    [_passfield.layer setBorderWidth:1.0f];
    [_passfield.layer setBorderColor:[[[UIColor alloc] initWithRed:(233.0/255.0) green:(233.0/255.0) blue:(233.0/255.0) alpha:1.0] CGColor]];
    [_userfield.layer setBorderWidth:1.0f];
    [_userfield.layer setBorderColor:[[[UIColor alloc] initWithRed:(233.0/255.0) green:(233.0/255.0) blue:(233.0/255.0) alpha:1.0] CGColor]];
    [_loginFB setBackgroundColor:[UIColor blackColor]];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _loginFB.bounds;
    gradient.colors = [NSArray arrayWithObjects: (id)[[[UIColor alloc] initWithRed:(231.0/255.0) green:(248.0/255.0) blue:(227.0/255.0) alpha:1.0] CGColor], (id)[[[UIColor alloc] initWithRed:(200.0/255.0) green:(240.0/255.0) blue:(194.0/255.0) alpha:1.0] CGColor],nil];
    [_background setBackgroundColor:[[UIColor alloc] initWithRed:(57.0/255.0) green:(188.0/255.0) blue:(63.0/255.0) alpha:1.0]];
    [_loginFB.layer insertSublayer:gradient atIndex:0];
    
    CAGradientLayer *gradient2 = [CAGradientLayer layer];
    gradient2.frame = _loginb.bounds;
    gradient2.colors = [NSArray arrayWithObjects: (id)[[[UIColor alloc] initWithRed:(231.0/255.0) green:(248.0/255.0) blue:(227.0/255.0) alpha:1.0] CGColor], (id)[[[UIColor alloc] initWithRed:(200.0/255.0) green:(240.0/255.0) blue:(194.0/255.0) alpha:1.0] CGColor],nil];
      [_loginb.layer insertSublayer:gradient2 atIndex:0];
    
    CAGradientLayer *gradient3 = [CAGradientLayer layer];
    gradient3.frame = _regib.bounds;
    gradient3.colors = [NSArray arrayWithObjects: (id)[[[UIColor alloc] initWithRed:(231.0/255.0) green:(248.0/255.0) blue:(227.0/255.0) alpha:1.0] CGColor], (id)[[[UIColor alloc] initWithRed:(200.0/255.0) green:(240.0/255.0) blue:(194.0/255.0) alpha:1.0] CGColor],nil];
    [_regib.layer insertSublayer:gradient3 atIndex:0];
   
    self.navigationItem.titleView = label;
    [userText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [passText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
       passText.secureTextEntry = YES;
    
   
    
   // if((savedUser == nil) + (savedPass == nil)){
     
   // }else{
     //   [self login];
   // }
    }

- (BOOL)isUserLogged
{
    keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"Cancerosity" accessGroup:nil];
    NSString *username = [keychain objectForKey:(__bridge id)kSecAttrAccount];
    NSString *password = [keychain objectForKey:(__bridge id)kSecValueData];
    if([username length] > 0 && [password length] > 0){
        return TRUE;
    } else {
        
    }
  
    return FALSE;
}
-(void)removeRegister{
    [UIView animateWithDuration:0.5
                     animations:^{
                         _currentViewController.view.frame = CGRectMake(self.view.bounds.origin.x, 570, self.view.frame.size.width, self.view.frame.size.height);
                     } completion:^(BOOL finished) {
                        
                         [(Registration *)self.parentViewController goBack];
}
     ];
    
}

- (IBAction)register:(id)sender {
    [passText resignFirstResponder];
    [userText resignFirstResponder];
    [myScroll setContentOffset:CGPointMake(0, 0) animated:NO];
    [myScroll setScrollEnabled:NO];
    
    {
    _currentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"registration"];
   
    
    _currentViewController.view.frame = CGRectMake(self.view.bounds.origin.x, 570.0f, _currentViewController.view.frame.size.width,_currentViewController.view.frame.size.height);
    [self addChildViewController: _currentViewController];
    [_currentViewController didMoveToParentViewController:self];
    
    [self.view addSubview:_currentViewController.view];
   
    [UIView animateWithDuration:0.5
                     animations:^{
                         _currentViewController.view.frame = CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, _currentViewController.view.frame.size.width, _currentViewController.view.frame.size.height);
                     }];
    
    }}

- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;
}
- (void)scrollViewToCenterOfScreen:(UIView *)theView {
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    CGFloat availableHeight = applicationFrame.size.height - 200;            // Remove area covered by keyboard
    
    CGFloat y = viewCenterY - availableHeight / 2.5;
    if (y < 0) {
        y = 0;
    }
    [myScroll setContentOffset:CGPointMake(0, y) animated:YES];
    [myScroll setScrollEnabled:YES];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [passText resignFirstResponder];
    [userText resignFirstResponder];
   
  
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    myScroll.contentInset = contentInsets;
    myScroll.scrollIndicatorInsets = contentInsets;

    activeField = nil;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
     activeField = textField;
     [self scrollViewToCenterOfScreen:textField];
    [myScroll setScrollEnabled:YES];
    myScroll.contentSize = CGSizeMake(myScroll.frame.size.width, self.view.frame.size.height + 200);
   
    return YES;
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == userText) {
      
        [passText becomeFirstResponder];
        
        
    } else if (textField == passText){
        [passText resignFirstResponder];
        
       [myScroll setContentOffset:CGPointMake(0, 0) animated:NO];
        [self login];
    }
    return YES;
}

-(void)addRegister {
    UIView *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"registration"];
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [delegate.window addSubview:controller];
}

-(void)request
{
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    [data release];
    // NSString *url = [NSString stringWithFormat:@"http://192.168.10.53:8080/Cancer_final/login.do?method=myAction1&username=%@&password=%@",userText.text,passText.text];
    NSString *url = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/login.do?method=myAction1&username=%@&password=%@",userText.text,passText.text];
    NSURL *urlrequest=[NSURL URLWithString:url];
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlrequest];
    //initialize a connection from request
    NSURLConnection *connection1 = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection1;
    [connection release];
    //start the connection
    [connection start];
    
}
-(void)startIndicator
{
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];//UIActivityIndicatorViewStyleWhiteLarge
    indicator.frame = CGRectMake(self.view.bounds.size.width / 2.0f - indicator.frame.size.width /2.0f, self.view.bounds.size.height / 2.0f - indicator.frame.size.height /2.0f, indicator.frame.size.width, indicator.frame.size.height);
    //indicator.color = [UIColor colorWithRed:1 green:0.8 blue:0.9 alpha:1 ];
    indicator.color = [UIColor whiteColor];
    CGAffineTransform transform = CGAffineTransformMakeScale(2.2f, 2.2f);
    indicator.transform = transform;
    [indicator startAnimating];
    [self.view addSubview:indicator];
    indicator.hidden = NO;
    //[self performSelector:@selector(stopIndicator) withObject:nil afterDelay:0.0];
    
    
}

-(void)authentication
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    //receivedData = [NSData dataWithContentsOfURL:urlrequest];
    // NSLog(@"values in dataforLogin is:- %@",receivedData);
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:receivedData options:kNilOptions error:&error];
    int length=[json count];
    if (length==0) {
        
        alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please try again" delegate:self cancelButtonTitle:@"ok"otherButtonTitles: nil];
        [alert show];
        userText.text=@"";
        passText.text=@"";
        [indicator stopAnimating];
        indicator.hidden = YES;
        
    }
    NSLog(@"Objects in JSON %i",length);
    NSLog(@"JSON format:- %@",json);
    authStr = [json objectForKey:@"param"];
    delegate.displayName=[json objectForKey:@"name"];
    NSArray *nameInfo=[[json objectForKey:@"name"] componentsSeparatedByString:@" "];
    delegate.firstName=[nameInfo objectAtIndex:0];
    delegate.lastName=[nameInfo objectAtIndex:1];
    profileId=[json objectForKey:@"user_id"];
    prfileStatus=[json objectForKey:@"status"];
    delegate.setLocation=[json objectForKey:@"location"];
    delegate.setEmailNotification=[json objectForKey:@"email_notification"];
    delegate.setPrivacy=[json objectForKey:@"privacy"];
    delegate.emailId=[json objectForKey:@"email_id"];
    delegate.contact=[json objectForKey:@"phone"];
    delegate.requestCount=[json objectForKey:@"request_count"];
    NSString  *profileDpLink=[json objectForKey:@"profile_image"];
    delegate.profileImage=profileDpLink;
    NSLog(@"valu in elemant is:- %@",profileInfo);
    NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
    [userData setObject:delegate.displayName forKey:@"display_name"];
    [userData setObject:delegate.firstName forKey:@"first_name"];
    [userData setObject:delegate.lastName forKey:@"last_name"];
    [userData setObject:profileId forKey:@"profile_id"];
    [userData setObject:prfileStatus forKey:@"profile_status"];
    [userData setObject:delegate.setLocation forKey:@"location"];
    [userData setObject:delegate.setEmailNotification forKey:@"email_not"];
    [userData setObject:delegate.setPrivacy forKey:@"privacy"];
    [userData setObject:delegate.emailId forKey:@"email_id"];
    [userData setObject:delegate.contact forKey:@"phone_number"];
    [userData setObject:delegate.requestCount forKey:@"request_count"];
    [userData setObject:delegate.profielImages forKey:@"profile_image"];
     [userData synchronize];
    
    
    if([authStr isEqualToString:@"True"])
    {
        [userText resignFirstResponder];
        [passText resignFirstResponder];
        [myScroll setContentOffset:CGPointMake(0, 0) animated:NO];
        [myScroll setScrollEnabled:NO];
        
        delegate.myname =getName;
        delegate.mynameLogin=userText.text;
        NSUserDefaults *loginName=[NSUserDefaults standardUserDefaults];
        [loginName setObject:delegate.mynameLogin forKey:@"loginName"];
        
        
        
        /* NSUserDefaults *itemUser=[NSUserDefaults standardUserDefaults];
         NSString *keyOfName=[delegate.mynameLogin stringByAppendingString:@"outhName"];
         //[itemUser setObject:nil forKey:keyOfName];
         NSUserDefaults *outhToken=[NSUserDefaults standardUserDefaults];
         NSString *keyOfOuth=[delegate.mynameLogin stringByAppendingString:@"outhToken"];
         // [outhToken setObject:nil forKey:keyOfOuth];*/
        
        delegate.myPassword=passText.text;
        delegate.status=prfileStatus;
        NSData *imageDataUp = [NSData dataWithContentsOfURL:[NSURL URLWithString:profileDpLink]];
        delegate.profilePicData=imageDataUp;
        NSUserDefaults *picData=[NSUserDefaults standardUserDefaults];
        NSString *keyOfName=[delegate.mynameLogin stringByAppendingString:@"picData"];
        [picData setObject:imageDataUp forKey:keyOfName];
       // [[NSUserDefaults standardUserDefaults] setValue:userText.text forKey:@"Username"];
        //[[NSUserDefaults standardUserDefaults] setValue:passText.text forKey:@"password"];
       // [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self circleData];
        [self profileData];
        self.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
        delegate.loggedIN = YES;
        [[NSUserDefaults standardUserDefaults] setValue:@"logged in" forKey:@"loggedIN"];
        [[NSUserDefaults standardUserDefaults] synchronize];
      
        
        
        
        NSLog(@"%@, %@", [keychain objectForKey:(id)kSecAttrAccount], [keychain objectForKey:(id)kSecValueData]);
        
        [keychain setObject:userText.text forKey:(__bridge id)kSecAttrAccount];
        [keychain setObject:passText.text forKey:(__bridge id)kSecValueData];
        
        [indicator stopAnimating];
        isLoggedIN = YES;
        indicator.hidden = YES;
        myScroll.delegate = nil;
        [myScroll release];
        
    }
    else if([authStr isEqualToString:@"False"])
    {
        [indicator stopAnimating];
        indicator.hidden = YES;
        alert =[[UIAlertView alloc]initWithTitle:@"something went wrong" message:@"please check your login details" delegate:self cancelButtonTitle:@"try again"otherButtonTitles:@"register", nil];
         alert.tag = TAG_FAIL;
        [alert show];
        userText.text=@"";
        passText.text=@"";
    }
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    if(data==nil)
    {
        [indicator stopAnimating];
        indicator.hidden=YES;
        UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"your connection has failed" message:@"please verify that you are connected to the internet" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"try again", nil];
          alert1.tag = TAG_CFAIL;
        [alert1 show];
    }
    else{
        [self.receivedData appendData:data];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    }
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    [indicator stopAnimating];
    indicator.hidden = YES;
    NSLog(@"%@" , error);
    UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"your connection has failed" message:@"please verify that you are connected to the internet" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"try again", nil];
    alert1.tag = TAG_CFAIL;
    [alert1 show];
}

/*
 if data is successfully received, this method will be called by connection
 */
-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [alert dismissWithClickedButtonIndex:0 animated:YES];
    [self authentication];
    
}

-(IBAction)hidekeyboard:(id)sender
{
    [passText resignFirstResponder];
    [userText resignFirstResponder];
    [myScroll setContentOffset:CGPointMake(0, 0) animated:YES];
    [myScroll setScrollEnabled:NO];
    
}
-(IBAction)nextkeyboard:(id)sender
{
    [userText resignFirstResponder];
     [passText resignFirstResponder];
    
}
-(IBAction)login
{
    // [self performSegueWithIdentifier:@"circle" sender:self];
    
    if([userText.text isEqualToString:@""])
    {
        [userText resignFirstResponder];
        alert=[[UIAlertView alloc]initWithTitle:@"username empty" message:@"please enter your username and try again" delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil];
        [alert show];
        
        
    }else{
        [userText resignFirstResponder];
        [passText resignFirstResponder];
        [myScroll setContentOffset:CGPointMake(0, 0) animated:NO];
        [myScroll setScrollEnabled:NO];
        [self request];
        [self startIndicator];
        
    }
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView.tag == TAG_CFAIL && buttonIndex == 0) { // handle the altdev
        [self viewDidLoad];
        
    }
    else if (alertView.tag == TAG_CFAIL && buttonIndex == 1)
    {
        [self login];
    }
    
    
    else if (alertView.tag == TAG_FAIL && buttonIndex == 0)
    {
        [self viewDidLoad];
    }
    
  
    
    else if (alertView.tag == TAG_FAIL && buttonIndex == 1) {
        [self performSegueWithIdentifier:@"loginToRegister" sender:self];
      
        
    }
    
}


-(IBAction)loginWithFB
{
    [userText resignFirstResponder];
    [passText resignFirstResponder];
     [myScroll setContentOffset:CGPointMake(0, 0) animated:NO];
    // [self performSegueWithIdentifier:@"loginToFb" sender:self];
}
-(void)circleData
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    
    NSString *url = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/circle.do"];
    // NSString *url = [NSString stringWithFormat:@"http://192.168.10.53:8080/Cancer_final/login.do?method=myAction1&username=%@&password=%@",getuser,getuserpass];
    NSURL *urlrequest=[NSURL URLWithString:url];
    NSURLRequest *req=[[NSURLRequest alloc]initWithURL:urlrequest];
    NSURLResponse  *response = nil;
    NSError* error;
    [NSURLConnection sendSynchronousRequest: req returningResponse: &response error: &error];
    
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    NSString *stringData= [NSString stringWithContentsOfURL:urlrequest encoding:nil error:nil];
    NSLog(@"Recieved data String &&&&&&&&&&&& %@",stringData);
    
    //NSData *dataofStrig=[NSData dataWithContentsOfFile:stringData];
    NSData * dataofStrig = [stringData dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"Recieved data Data &&&&&&&&&&&& %@",stringData);
    if (!dataofStrig) {
        NSLog(@"No Data");
    }
    else{
        
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions                        error:&error];
        
        NSLog(@"JSON format:- %@",json);
        int length=[json count];
        NSLog(@"JSON count:- %i",length);
        dataUpdate = [[NSMutableArray alloc] initWithCapacity:dataUpdate.count];
        differUser = [[NSMutableArray alloc] initWithCapacity:differUser.count];
        snoArray = [[NSMutableArray alloc] initWithCapacity:snoArray.count];
        profielImages =[[NSMutableArray alloc] initWithCapacity:profielImages.count];
        likearray =[[NSMutableArray alloc] initWithCapacity:likearray.count];
        dislikearray =[[NSMutableArray alloc] initWithCapacity:dislikearray.count];
        likesNameArray =[[NSMutableArray alloc] initWithCapacity:likesNameArray.count];
        dislikesNameArray =[[NSMutableArray alloc] initWithCapacity:likesNameArray.count];
        imageDatas =[[NSMutableArray alloc]initWithCapacity:imageDatas.count];
        dataType =[[NSMutableArray alloc]initWithCapacity:dataType.count];
        commentsArray=[[NSMutableArray alloc]initWithCapacity:commentsArray.count];
        commentNameArray=[[NSMutableArray alloc]initWithCapacity:commentNameArray.count];
        commentPicArray=[[NSMutableArray alloc]initWithCapacity:commentPicArray.count];
        commentCounts=[[NSMutableArray alloc]initWithCapacity:commentCounts.count];
        insertedTimeArray=[[NSMutableArray alloc]initWithCapacity:insertedTimeArray.count];
        locationArray=[[NSMutableArray alloc]initWithCapacity:locationArray.count];
        timeArray=[[NSMutableArray alloc]initWithCapacity:timeArray.count];
        fourCommentArray=[[NSMutableArray alloc]initWithCapacity:fourCommentArray.count];
        privacyArray=[[NSMutableArray alloc]initWithCapacity:privacyArray.count];
        delegate.getInertedTime=[[json objectForKey:@"data0"] valueForKey:@"inserted_at"];
        NSLog(@"First inserted time on login %@",delegate.getInertedTime);
        NSString *indexStr = [NSString stringWithFormat:@"%d",(length-6)];
        NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
        delegate.getLastInertedTime=[[json objectForKey:data] valueForKey:@"inserted_at"];
        
        NSLog(@"datao time inserted at login %@",delegate.getInertedTime);
        
        
        for(int i=0;i<(length-5);i++)    {
            
            NSString *indexStr = [NSString stringWithFormat:@"%d",i];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            [privacyArray addObject:[[json objectForKey:data]valueForKey:@"privacy"]];
            [commentCounts addObject:[[json objectForKey:data]valueForKey:@"comment"]];
            NSString *likesName=[[json objectForKey:data] valueForKey:@"likenames"];
            NSString *confrm=[self likesContent:likesName];
            [likesNameArray addObject:confrm];
            NSString *dislikesName=[[json objectForKey:data] valueForKey:@"dislikenames"];
            NSString *disconfrm=[self likesContent:dislikesName];
            [dislikesNameArray addObject:disconfrm];
            dataStr=[[json valueForKey:data] valueForKey:@"content"];
            NSString *user=[[json valueForKey:data] valueForKey:@"username"];
            [differUser addObject:user];
            NSString *sno=[[json valueForKey:data]valueForKey:@"sno"];
            [snoArray addObject:sno];
            NSString *likeString = [[json valueForKey:data]valueForKey:@"like"];
            NSLog(@"value of like string is:- %@",likeString);
            [likearray addObject:likeString];
            [insertedTimeArray addObject:[[json objectForKey:data] valueForKey:@"inserted_at"]];
            NSString *dislikeString = [[json valueForKey:data]valueForKey:@"dislike"];
            NSLog(@"value of like string is:- %@",dislikeString);
            [dislikearray addObject:dislikeString];
            if ([[[json objectForKey:data] valueForKey:@"Elapsed"] isEqualToString:@""]) {
                [timeArray addObject:@"Not updated"];
            }
            else
            {
                [timeArray addObject:[[json objectForKey:data] valueForKey:@"Elapsed"]];
            }
            if([[[json objectForKey:data] valueForKey:@"location"] isEqualToString:@""])
            {
                [locationArray addObject:@"Not updated"];
            }
            else
            {
                [locationArray addObject:[[json objectForKey:data] valueForKey:@"location"]];
            }
            
            NSString *fourcomment=[[json objectForKey:data]valueForKey:@"commentiphone"];
            NSLog(@"fourcomment %@",fourcomment);
            if ([fourcomment length]==0) {
                [fourCommentArray addObject:@"No Comments"];
            }
            else{
                [fourCommentArray addObject:fourcomment];
            }
            NSLog(@"fourCommentArray %@",fourCommentArray);
            NSString *userDP=[[json valueForKey:data]valueForKey:@"profile_image"];
            [profielImages addObject:userDP];
            NSString *type=[[json objectForKey:data]valueForKey:@"type"];
            
            [dataType addObject:type];
            [dataUpdate addObject:dataStr];
        }
        [dataUpdate addObject:@"See More Updates....."];
        [dataType addObject:@"more"];
        [snoArray addObject:@"more"];
        [likearray addObject:@"more"];
        [dislikearray addObject:@"more"];
        [differUser addObject:@"more"];
        [likesNameArray addObject:@"more"];
        [dislikesNameArray addObject:@"more"];
        [profielImages addObject:@"more"];
        [commentCounts addObject:@"more"];
        [locationArray addObject:@"more"];
        [timeArray addObject:@"more"];
        [insertedTimeArray addObject:@"more"];
        [fourCommentArray addObject:@"more"];
        delegate.dataUpdate=dataUpdate;
        delegate.dataType=dataType;
        delegate.snoArray=snoArray;
        delegate.likearray=likearray;
        delegate.dislikearray=dislikearray;
        delegate.differUser=differUser;
        delegate.likesNameArray=likesNameArray;
        delegate.dislikesNameArray=dislikesNameArray;
        delegate.profielImages=profielImages;
        delegate.commentCounts=commentCounts;
        delegate.locationArray=locationArray;
        delegate.timeArray=timeArray;
        delegate.insertedTimeArray=insertedTimeArray;
        delegate.fourCommentArray=fourCommentArray;
    
    }
    
}
-(NSString *)likesContent:(NSString *)allNames
{
    NSLog(@"alll names %@",allNames);
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    NSLog(@"username %@",delegate.mynameLogin);
    NSString *name=[[NSString alloc]init];
    /* if ([allNames isEqualToString:@""]) {
     name=@"No";
     }
     else{*/
    
    if ([allNames rangeOfString:delegate.mynameLogin].location != NSNotFound)
    {
        name=@"Yes";
    }
    
    else
    {
        name=@"No";
    }
    
    // }
    return name;
}
-(void)profileData
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    NSString *url = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/login.do?method=myAction1&username=%@&password=",delegate.mynameLogin];
    NSURL *urlrequest=[NSURL URLWithString:url];
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    NSError* error;
    NSURLRequest *req=[[NSURLRequest alloc]initWithURL:urlrequest];
    NSURLResponse  *response = nil;
    
    [NSURLConnection sendSynchronousRequest: req returningResponse: &response error: &error];
    
    if (!error) {
        [indicator stopAnimating];
        indicator.hidden=YES;
        NSLog(@"errorrrrr no response");
        UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Connection failed" message:@"Check intenet connection" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
        [alert1 show];
    }
    
    NSData *receivedData2=[NSData dataWithContentsOfURL:urlrequest];
    
    if(!receivedData2 )
    {
        [indicator stopAnimating];
        indicator.hidden=YES;
        NSLog(@"errorrrrr no response");
        UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Connection failed" message:@"Check intenet connection" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
        [alert1 show];
        
    }
    
    else{
        
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:receivedData2 options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",json);
        int length=[json count];
        PdataUpdate = [[NSMutableArray alloc] initWithCapacity:PdataUpdate.count];
        PdataType = [[NSMutableArray alloc] initWithCapacity:PdataType.count];
        Plikearray = [[NSMutableArray alloc] initWithCapacity:Plikearray.count];
        Pdislikearray = [[NSMutableArray alloc] initWithCapacity:Pdislikearray.count];
        PsnoArray=[[NSMutableArray alloc]initWithCapacity:PsnoArray.count];
        PlikesNameArray=[[NSMutableArray alloc]initWithCapacity:PlikesNameArray.count];
        PdislikesNameArray=[[NSMutableArray alloc]initWithCapacity:PdislikesNameArray.count];
        PcommentCounts=[[NSMutableArray alloc]initWithCapacity:PcommentCounts.count];
        PlocationArray=[[NSMutableArray alloc]initWithCapacity:PlocationArray.count];
        PtimeArray=[[NSMutableArray alloc]initWithCapacity:PtimeArray.count];
        PinsertedTimeArray=[[NSMutableArray alloc]initWithCapacity:PinsertedTimeArray.count];
        PfourCommentArray=[[NSMutableArray alloc]initWithCapacity:PfourCommentArray.count];
        following=[json valueForKey:@"followingCount"];
        followingNames=[json valueForKey:@"followingNames"];
        followers=[json valueForKey:@"followerCount"];
        followersNames=[json valueForKey:@"followerNames"];
        delegate.PgetInertedTime=[[json objectForKey:@"data0"] valueForKey:@"inserted_at"];
        NSString *indexStr = [NSString stringWithFormat:@"%d",(length-6)];
        NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
        delegate.PgetLastInertedTime=[[json objectForKey:data] valueForKey:@"inserted_at"];
        for(int i=0;i<length-5;i++)
        {
            NSString *indexStr = [NSString stringWithFormat:@"%d",i];
            NSString *data2=[@"data" stringByAppendingFormat:@"%@",indexStr];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            [PcommentCounts addObject:[[json objectForKey:data]valueForKey:@"comment"]];
            NSString *likesName=[[json objectForKey:data] valueForKey:@"likenames"];
            NSString *confrm=[self likesContent:likesName];
            [PlikesNameArray addObject:confrm];
            NSString *dislikesName=[[json objectForKey:data] valueForKey:@"dislikenames"];
            NSString *disconfrm=[self likesContent:dislikesName];
            [PdislikesNameArray addObject:disconfrm];
            NSString *snoString = [[json valueForKey:data2]valueForKey:@"sno"];
            [PsnoArray addObject:snoString];
            NSString *likeString = [[json valueForKey:data2]valueForKey:@"like"];
            NSLog(@"value of like string is:- %@",likeString);
            [Plikearray addObject:likeString];
            NSString *dislikeString = [[json valueForKey:data2]valueForKey:@"dislike"];
            NSLog(@"value of like string is:- %@",dislikeString);
            [Pdislikearray addObject:dislikeString];
            [PinsertedTimeArray addObject:[[json objectForKey:data] valueForKey:@"inserted_at"]];
            if ([[[json objectForKey:data] valueForKey:@"Elapsed"] isEqualToString:@""]) {
                [PtimeArray addObject:@"Not Updated"];
            }
            else
            {
                [PtimeArray addObject:[[json objectForKey:data] valueForKey:@"Elapsed"]];
            }
            if([[[json objectForKey:data] valueForKey:@"location"] isEqualToString:@""])
            {
                [PlocationArray addObject:@"Not Updated"];
            }
            else
            {
                [PlocationArray addObject:[[json objectForKey:data] valueForKey:@"location"]];
            }
            PdataStr=[[json valueForKey:data2] valueForKey:@"content"];
            NSLog(@"content from url %@",PdataStr);
            NSString *type=[[json objectForKey:data]valueForKey:@"type"];
            [PdataType addObject:type];
            [PdataUpdate addObject:PdataStr];
            NSString *fourcomment=[[json objectForKey:data]valueForKey:@"commentiphone"];
            NSLog(@"fourcomment %@",fourcomment);
            if ([fourcomment length]==0) {
                [PfourCommentArray addObject:@"No Comments"];
            }
            else{
                [PfourCommentArray addObject:fourcomment];
            }
            
            
        }
    }
    [PdataUpdate addObject:@"See More Updates....."];
    [PdataType addObject:@"more"];
    [PsnoArray addObject:@"more"];
    [Plikearray addObject:@"more"];
    [Pdislikearray addObject:@"more"];
    [PlikesNameArray addObject:@"more"];
    [PdislikesNameArray addObject:@"more"];
    [PcommentCounts addObject:@"more"];
    [PlocationArray addObject:@"more"];
    [PtimeArray addObject:@"more"];
    [PinsertedTimeArray addObject:@"more"];
    [PfourCommentArray addObject:@"more"];
    delegate.PdataUpdate=PdataUpdate;
    delegate.PdataType=PdataType;
    delegate.PsnoArray=PsnoArray;
    delegate.Plikearray=Plikearray;
    delegate.Pdislikearray=Pdislikearray;
    delegate.PcommentCounts=PcommentCounts;
    delegate.PlikesNameArray=PlikesNameArray;
    delegate.PdislikesNameArray=PdislikesNameArray;
    delegate.PtimeArray=PtimeArray;
    delegate.PlocationArray=PlocationArray;
    delegate.followers=followers;
    delegate.following=following;
    delegate.followersNames=followersNames;
    delegate.followingNames=followingNames;
    delegate.PinsertedTimeArray=PinsertedTimeArray;
    delegate.PfourCommentArray=PfourCommentArray;
    
}
-(NSString *)smilyString:(NSString *)getString
{
    NSString *a1 = @":)";
    
    NSString *a2 = @":P";
    
    NSString *a3 = @":(";
    
    NSString *a4 = @"=D";
    
    NSString *a5 = @";)";
    
    NSString *a6 = @":D";
    
    NSString *b1 = @"\\ud83d\\ude0a";
    
    NSString *b2 = @"\\ud83d\\ude1b";
    
    NSString *b3 = @"\\ud83d\\ude12";
    
    NSString *b4 = @"\\ud83d\\ude1c";
    
    NSString *b5 = @"\\ud83d\\ude09";
    
    NSString *b6 = @"\\ud83d\\ude1c";
    
    
    NSData *data = [getString dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    
    NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    
    
    NSRange rangeValue = [goodValue rangeOfString:a1 options:NSCaseInsensitiveSearch];
    
    if (rangeValue.length > 0)
        
    {
        
        goodValue = [goodValue stringByReplacingOccurrencesOfString:@":)" withString:b1];
        
    }
    
    
    
    NSRange rangeValue1 = [goodValue rangeOfString:a2 options:NSCaseInsensitiveSearch];
    
    if (rangeValue1.length > 0)
        
    {
        
        goodValue = [goodValue stringByReplacingOccurrencesOfString:@":P" withString:b2];
        
    }
    
    
    
    NSRange rangeValue2 = [goodValue rangeOfString:a3 options:NSCaseInsensitiveSearch];
    
    if (rangeValue2.length > 0)
        
    {
        
        goodValue = [goodValue stringByReplacingOccurrencesOfString:@":(" withString:b3];
        
    }
    
    
    
    NSRange rangeValue3 = [goodValue rangeOfString:a4 options:NSCaseInsensitiveSearch];
    
    if (rangeValue3.length > 0)
        
    {
        
        goodValue = [goodValue stringByReplacingOccurrencesOfString:@"=D" withString:b4];
        
    }
    
    NSRange rangeValue4 = [goodValue rangeOfString:a5 options:NSCaseInsensitiveSearch];
    
    if (rangeValue4.length > 0)
        
    {
        
        goodValue = [goodValue stringByReplacingOccurrencesOfString:@";)" withString:b5];
        
    }
    NSRange rangeValue5 = [goodValue rangeOfString:a6 options:NSCaseInsensitiveSearch];
    
    if (rangeValue5.length > 0)
        
    {
        
        goodValue = [goodValue stringByReplacingOccurrencesOfString:@":D" withString:b6];
        
    }
    
    
    
    NSData *datanew = [goodValue dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *goodValuenew = [[NSString alloc] initWithData:datanew encoding:NSNonLossyASCIIStringEncoding];
    NSLog(@"value of good string is:- %@",goodValuenew);
    return goodValuenew;
    
}
-(NSMutableAttributedString *)atributedString:(NSString *)getString
{
    NSString *a1 = @":)";
    
    NSString *a2 = @":P";
    
    NSString *a3 = @":(";
    
    NSString *a4 = @"=D";
    
    NSString *a5 = @";)";
    
    NSString *a6 = @":D";
    
    NSString *b1 = @"\\ud83d\\ude0a";
    
    NSString *b2 = @"\\ud83d\\ude1b";
    
    NSString *b3 = @"\\ud83d\\ude12";
    
    NSString *b4 = @"\\ud83d\\ude1c";
    
    NSString *b5 = @"\\ud83d\\ude09";
    
    NSString *b6 = @"\\ud83d\\ude1c";
    
    
    NSData *data = [getString dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    
    NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    
    
    NSRange rangeValue = [goodValue rangeOfString:a1 options:NSCaseInsensitiveSearch];
    
    if (rangeValue.length > 0)
        
    {
        
        goodValue = [goodValue stringByReplacingOccurrencesOfString:@":)" withString:b1];
        
    }
    
    
    
    NSRange rangeValue1 = [goodValue rangeOfString:a2 options:NSCaseInsensitiveSearch];
    
    if (rangeValue1.length > 0)
        
    {
        
        goodValue = [goodValue stringByReplacingOccurrencesOfString:@":P" withString:b2];
        
    }
    
    
    
    NSRange rangeValue2 = [goodValue rangeOfString:a3 options:NSCaseInsensitiveSearch];
    
    if (rangeValue2.length > 0)
        
    {
        
        goodValue = [goodValue stringByReplacingOccurrencesOfString:@":(" withString:b3];
        
    }
    
    
    
    NSRange rangeValue3 = [goodValue rangeOfString:a4 options:NSCaseInsensitiveSearch];
    
    if (rangeValue3.length > 0)
        
    {
        
        goodValue = [goodValue stringByReplacingOccurrencesOfString:@"=D" withString:b4];
        
    }
    
    NSRange rangeValue4 = [goodValue rangeOfString:a5 options:NSCaseInsensitiveSearch];
    
    if (rangeValue4.length > 0)
        
    {
        
        goodValue = [goodValue stringByReplacingOccurrencesOfString:@";)" withString:b5];
        
    }
    NSRange rangeValue5 = [goodValue rangeOfString:a6 options:NSCaseInsensitiveSearch];
    
    if (rangeValue5.length > 0)
        
    {
        
        goodValue = [goodValue stringByReplacingOccurrencesOfString:@":D" withString:b6];
        
    }
    
    
    
    NSData *datanew = [goodValue dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *goodValuenew = [[NSString alloc] initWithData:datanew encoding:NSNonLossyASCIIStringEncoding];
    NSLog(@"value of good string is:- %@",goodValuenew);
    NSMutableAttributedString * string;
    string = [[NSMutableAttributedString alloc]initWithString:goodValuenew];
    NSArray *words=[goodValuenew componentsSeparatedByString:@" "];
    tags=[[NSMutableArray alloc]initWithCapacity:tags.count];
    
    for (NSString *word in words) {
        if (([word hasPrefix:@"#"])||([word hasPrefix:@"@"])) {
            [tags addObject:word];
        }
    }
    if ([tags count]==0) {
        //string=string;
    }
    else{
        for (NSString *word in words) {
            if (([word hasPrefix:@"#"])||([word hasPrefix:@"@"])) {
                [tags addObject:word];
                NSRange range=[goodValuenew rangeOfString:word];
                NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor colorWithRed:0 green:0.4 blue:0 alpha:1],NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-Bold" size:12],
                                             NSBackgroundColorAttributeName: [UIColor whiteColor]
                                             };
                [string addAttributes:attributes range:range];
                
            }
        }
        
    }
    
    return string;
    
}
-(NSMutableAttributedString *)attibutedUsername:(NSString *)commentString
{
    NSString *a1 = @":)";
    
    NSString *a2 = @":P";
    
    NSString *a3 = @":(";
    
    NSString *a4 = @"=D";
    
    NSString *a5 = @";)";
    
    NSString *a6 = @":D";
    
    NSString *b1 = @"\\ud83d\\ude0a";
    
    NSString *b2 = @"\\ud83d\\ude1b";
    
    NSString *b3 = @"\\ud83d\\ude12";
    
    NSString *b4 = @"\\ud83d\\ude1c";
    
    NSString *b5 = @"\\ud83d\\ude09";
    
    NSString *b6 = @"\\ud83d\\ude1c";
    
    
    NSData *data = [commentString dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    
    NSString *goodValue = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    
    
    NSRange rangeValue = [goodValue rangeOfString:a1 options:NSCaseInsensitiveSearch];
    
    if (rangeValue.length > 0)
        
    {
        
        goodValue = [goodValue stringByReplacingOccurrencesOfString:@":)" withString:b1];
        
    }
    
    
    
    NSRange rangeValue1 = [goodValue rangeOfString:a2 options:NSCaseInsensitiveSearch];
    
    if (rangeValue1.length > 0)
        
    {
        
        goodValue = [goodValue stringByReplacingOccurrencesOfString:@":P" withString:b2];
        
    }
    
    
    
    NSRange rangeValue2 = [goodValue rangeOfString:a3 options:NSCaseInsensitiveSearch];
    
    if (rangeValue2.length > 0)
        
    {
        
        goodValue = [goodValue stringByReplacingOccurrencesOfString:@":(" withString:b3];
        
    }
    
    
    
    NSRange rangeValue3 = [goodValue rangeOfString:a4 options:NSCaseInsensitiveSearch];
    
    if (rangeValue3.length > 0)
        
    {
        
        goodValue = [goodValue stringByReplacingOccurrencesOfString:@"=D" withString:b4];
        
    }
    
    NSRange rangeValue4 = [goodValue rangeOfString:a5 options:NSCaseInsensitiveSearch];
    
    if (rangeValue4.length > 0)
        
    {
        
        goodValue = [goodValue stringByReplacingOccurrencesOfString:@";)" withString:b5];
        
    }
    NSRange rangeValue5 = [goodValue rangeOfString:a6 options:NSCaseInsensitiveSearch];
    
    if (rangeValue5.length > 0)
        
    {
        
        goodValue = [goodValue stringByReplacingOccurrencesOfString:@":D" withString:b6];
        
    }
    
    
    
    NSData *datanew = [goodValue dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *goodValuenew = [[NSString alloc] initWithData:datanew encoding:NSNonLossyASCIIStringEncoding];
    NSLog(@"value of good string is:- %@",goodValuenew);
    NSMutableAttributedString * string;
    string = [[NSMutableAttributedString alloc]initWithString:goodValuenew];
    NSArray *words=[goodValuenew componentsSeparatedByString:@" "];
    tags=[[NSMutableArray alloc]initWithCapacity:tags.count];
    
    for (NSString *word in words) {
        if (([word hasPrefix:@"#"])||([word hasPrefix:@"@"])) {
            [tags addObject:word];
        }
    }
    if ([tags count]==0) {
        //string=string;
    }
    else{
        for (NSString *word in words) {
            if (([word hasPrefix:@"#"])||([word hasPrefix:@"@"])) {
                [tags addObject:word];
                NSRange range=[goodValuenew rangeOfString:word];
                NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor blackColor],NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-Bold" size:10],
                                             NSBackgroundColorAttributeName: [UIColor whiteColor]
                                             };
                [string addAttributes:attributes range:range];
                
            }
        }
        
    }
    
    return string;
}
-(void)viewDidUnload
{
    
    myScroll.delegate = nil;
    myScroll.scrollEnabled = NO;
    
}

- (void)dealloc {
    [_loginFB release];
 
    [_loginb release];
    [_regib release];
    [_passfield release];
    [_userfield release];
    [_TCs release];
    [_background release];
    [myScroll release];
    myScroll.delegate = nil;
    [super dealloc];
}
@end

