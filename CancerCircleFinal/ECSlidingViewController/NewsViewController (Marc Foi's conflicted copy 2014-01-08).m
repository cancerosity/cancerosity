//
//  NewsViewController.m
//  CancerCircleFinal
//
//  Created by Raminder on 29/04/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import "NewsViewController.h"
#import "AppDelegate.h"
#import "QuartzCore/QuartzCore.h"
#import "Login.h"



@interface NewsViewController ()

@end

@implementation NewsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    //[self startIndicator];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UINavigationBar *navBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 290, 64)];
    
    
    navBar.barTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    navBar.translucent = YES;
    

    _dropshadow.layer.masksToBounds = NO;
    _dropshadow.layer.shadowPath = [UIBezierPath
                                    bezierPathWithRect:CGRectMake(_dropshadow.layer.bounds.origin.x,_dropshadow.layer.bounds.origin.y,_dropshadow.layer.bounds.size.width + 40,_dropshadow.layer.bounds.size.height + 20)].CGPath;
    [_dropshadow.layer setShadowOffset:CGSizeMake(-40.0f, 0.0f)];
    [_dropshadow.layer setShadowColor:[[UIColor alloc] initWithRed:(46.0/255.0) green:(178.0/255.0) blue:(53.0/255.0) alpha:1.0].CGColor];
    self.view.backgroundColor = [[UIColor alloc] initWithRed:(46.0/255.0) green:(178.0/255.0) blue:(53.0/255.0) alpha:1.0];
    _dropshadow.backgroundColor = [[UIColor alloc] initWithRed:(46.0/255.0) green:(178.0/255.0) blue:(53.0/255.0) alpha:1.0];
    [_dropshadow.layer setShadowRadius:5.0f];
    [_dropshadow.layer setShadowOpacity:1.0f];
    myTable.separatorColor = [[UIColor alloc] initWithRed:(194.0/255.0) green:(195.0/255.0) blue:(196.0/255.0) alpha:1.0];

    myTable.backgroundColor = [[UIColor alloc] initWithRed:(218.0/255.0) green:(221.0/255.0) blue:(226.0/255.0) alpha:1.0];
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    imageData=delegate.profilePicData;
    _profileImage.image=[UIImage imageWithData:imageData];
    
    CALayer *imageLayer = _profileImage.layer;
    
    
    _usernameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:15];
    _usernameLabel.text=delegate.displayName;
   
    _statusLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:9];
    // dpUrl=delegate.profileImage;
    Login *login=[[Login alloc]init];
    NSLog(@"Status in delegate %@",delegate.status);
    NSMutableAttributedString *attributed=[login atributedString:delegate.status];
    _statusLabel.attributedText=attributed;

    
    
    [imageLayer setCornerRadius:30];
    [imageLayer setBorderWidth:1];
    [imageLayer setBorderColor:[[UIColor alloc] initWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:0.8].CGColor];
    [imageLayer setMasksToBounds:YES];
    _getfollowers=delegate.followers;
    _getfollowings=delegate.following;
    NSString *follower = [_getfollowers stringValue];
    NSString *followings = [_getfollowings stringValue];
    
    
    [_followers setTitle:[follower stringByAppendingFormat:@" Followers"] forState: UIControlStateNormal];
    [_followers setTitle:[follower stringByAppendingFormat:@" Followers"] forState: UIControlStateSelected];
    [_followers setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    [_following setTitle:[followings stringByAppendingFormat:@" Followings"] forState: UIControlStateNormal];
    [_following setTitle:[followings stringByAppendingFormat:@" Followings"] forState: UIControlStateSelected];
    [_following setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    CGFloat titleWidth2=100;
    CGFloat titleWidth = MIN(titleWidth2, 300);
    
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 30)];
    label.backgroundColor = [UIColor clearColor];
    // label.font = [UIFont boldSystemFontOfSize:20.0];
    label.font= [UIFont fontWithName:@"Futura_Light" size:20];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=@"news";
    
    
    
    UIImage* image3 = [UIImage imageNamed:@"close.png"];
    CGRect frameimg = CGRectMake(0, 0,18,18);
    someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(goToCircle)
         forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *backbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    
    
    UINavigationItem *buttonCarrier = [[UINavigationItem alloc]initWithTitle:@""];
    buttonCarrier.titleView = label;
    //Creating some buttons:
    
    
    
    //Putting the Buttons on the Carrier
    [buttonCarrier setLeftBarButtonItem:backbutton];
    
    
    //The NavigationBar accepts those "Carrier" (UINavigationItem) inside an Array
    NSArray *barItemArray = [[NSArray alloc]initWithObjects:buttonCarrier,nil];
    
    // Attaching the Array to the NavigationBar
    [navBar setItems:barItemArray];
    [someButton release];
   
    
    
    myTable.dataSource = self;
    [self startIndicator];
    self.imageDownloadingQueue = [[NSOperationQueue alloc] init];
    self.imageDownloadingQueue.maxConcurrentOperationCount = 4;
    self.imageCache = [[NSCache alloc] init];
    
	// Do any additional setup after loading the view.
    self.peekLeftAmount = 40.0f;
    [self.slidingViewController setAnchorRightRevealAmount:260.0f];
    self.slidingViewController.underLeftWidthLayout = ECFullWidth ;// ECFixedRevealWidth;//
    [myTable reloadData];
    
}
-(void)startIndicator
{
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];//UIActivityIndicatorViewStyleWhiteLarge
    indicator.frame = CGRectMake(self.view.bounds.size.width / 2.0f - indicator.frame.size.width /2.0f, self.view.bounds.size.height / 2.0f - indicator.frame.size.height /2.0f, indicator.frame.size.width, indicator.frame.size.height);
    //indicator.color = [UIColor colorWithRed:1 green:0.8 blue:0.9 alpha:1 ];
    indicator.color = [UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
    CGAffineTransform transform = CGAffineTransformMakeScale(2.2f, 2.2f);
    indicator.transform = transform;
    [indicator startAnimating];
    [self.view addSubview:indicator];
    indicator.hidden = NO;
    [self performSelector:@selector(newsData) withObject:nil afterDelay:0.001];
    
}
-(void)newsData
{
    AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
    activityArray=[[NSMutableArray alloc]initWithCapacity:activityArray.count];
    userArray=[[NSMutableArray alloc]initWithCapacity:userArray.count];
    typeArray=[[NSMutableArray alloc]initWithCapacity:typeArray.count];
    serialArray=[[NSMutableArray alloc]initWithCapacity:serialArray.count];
    NSString *url = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/news.do?username=%@",delegate.mynameLogin];
    NSURL *urlrequest=[NSURL URLWithString:url];
    
    //NSString *stringData= [NSString stringWithContentsOfURL:urlrequest encoding:nil error:nil];
    // NSLog(@"Recieved data String &&&&&&&&&&&& %@",stringData);
    
    NSData * dataofStrig = [NSData dataWithContentsOfURL:urlrequest];//[stringData dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions                        error:&error];
    NSLog(@"JSOn from url %@",json);
    NSString *param=[json valueForKey:@"param"];
    if ([param isEqualToString:@"false"]) {
        //[activityArray addObject:@"No News"];
    }
    else
    {
        int length=[json count];
        for(int i=0;i<length-1;i++)    {
            
            NSString *indexStr = [NSString stringWithFormat:@"%d",i];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            [typeArray addObject:[[json objectForKey:data]valueForKey:@"type"]];
            [serialArray addObject:[[json objectForKey:data]valueForKey:@"sno"]];
            NSLog(@"Message from news %@",[[json objectForKey:data]valueForKey:@"message"]);
            [activityArray addObject:[[json objectForKey:data]valueForKey:@"message"]];
            NSString *userDP=[[json objectForKey:data]valueForKey:@"profile_image"];
            [userArray addObject:userDP];
            
        }
    }
    //activityArray=delegate.activityArray;
    //userArray=delegate.userArray;
    [indicator stopAnimating];
    indicator.hidden=YES;
    [myTable reloadData];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [activityArray count];
}
- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
  
    
    
    newsCell *imgCell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (imgCell == nil)
    {
        imgCell = [[[newsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        NSLog(@"index path in cell %i",indexPath.row);
        
    }
    imgCell.backgroundColor = [UIColor clearColor];
    imgCell.imageView.layer.cornerRadius = 16;
    imgCell.imageView.clipsToBounds = YES;
    NSString *imageUrlString = [userArray objectAtIndex: indexPath.row];
    
    NSLog(@"image url string iss:- %@",imageUrlString);
    UIImage *cachedImage = [self.imageCache objectForKey:imageUrlString];
    if (cachedImage)
    {
        //imgCell.imageView.image = cachedImage;
        imgCell.imageView.image = cachedImage;
    }
    else
    {
        // you'll want to initialize the image with some blank image as a placeholder
        
        //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
        imgCell.imageView.image = [UIImage imageNamed:@"editPic.png"];
        [self.imageDownloadingQueue addOperationWithBlock:^{
            NSURL *imageURL   = [NSURL URLWithString:imageUrlString];
            NSData *imageData2 = [NSData dataWithContentsOfURL:imageURL];
            UIImage *image    = nil;
            if (imageData2)
                image = [UIImage imageWithData:imageData2];
            if (image)
            {
                // add the image to your cache
                
                [self.imageCache setObject:image forKey:imageUrlString];
                
                // finally, update the user interface in the main queue
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                    if (updateCell)
                        //imgCell.imageView.image = image;
                        
                        imgCell.imageView.image = image;
                    
                    
                }];
            }
        }];
    }
    
    UILabel *descriptionLabel=[[UILabel alloc]initWithFrame:CGRectMake(60,10,200,30)];
    descriptionLabel.font = [UIFont systemFontOfSize:10.0];
    descriptionLabel.textColor=[UIColor blackColor];
    //[descriptionLabel setNumberOfLines:0];
    descriptionLabel.backgroundColor=[UIColor clearColor];
    [descriptionLabel setText:[activityArray objectAtIndex:indexPath.row]];
    [imgCell.contentView addSubview:descriptionLabel];
    
    return imgCell;
    //}
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
    delegate.newsSerial=[serialArray objectAtIndex:indexPath.row];
    NSString *identifier=[typeArray objectAtIndex:indexPath.row];
    if ([identifier isEqualToString:@"images"]) {
        delegate.newsSerial=[serialArray objectAtIndex:indexPath.row];
        UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"iNewsDetail"];
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
    }
    else  if ([identifier isEqualToString:@"story"]) {
        UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"sNewsDetail"];
        
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
    }
    else  if ([identifier isEqualToString:@"video"]) {
        UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"vNewsDetail"];
        
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
    }
}

-(IBAction)goToCircle
{
    
    
    
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_dropshadow release];
    [_profileImage release];
    [_statusLabel release];
    [_usernameLabel release];
    [_following release];
    [_followers release];
    [super dealloc];
}
@end
