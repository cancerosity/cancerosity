//
//  vNewsViewController.h
//  CancerCircleFinal
//
//  Created by Raminder on 28/06/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MediaPlayer/MediaPlayer.h"
#import "MobileCoreServices/UTCoreTypes.h"
#import "MobileCoreServices/UTType.h"
#import <Social/Social.h>
#import "IFTweetLabel.h"
#import "IFLabelUsername.h"
#import "ILTranslucentView.h"

@interface vNewsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate>
{
    IBOutlet UIScrollView *myScroll;
    IBOutlet UITableView *mytable;
    NSString *getusername;
    NSMutableArray *commnetsArray;
    NSMutableArray *commentNameArray;
    NSMutableArray *commentPicArray;
    NSMutableArray *setcommentArray;
    NSMutableArray *setcommentCountArray;
    NSMutableArray *typeData;
    NSMutableArray *tags;
    IBOutlet UITextField *commentText;
    NSString *count;
    UILabel *label;
    IFTweetLabel *tweetLabel2;
    NSMutableArray *infoArray;
    IBOutlet UIView *commentView;
    IBOutlet ILTranslucentView *transView;
    CGSize commentHeight;
}
-(IBAction)commentButton;
-(IBAction)hidekeyboard:(id)sender;
-(IBAction)goToNews;
- (IBAction)goCircle:(id)sender;
@property(nonatomic,retain)NSData *getImageData;
@property(nonatomic,retain)NSString *getUserImageData;
@property(nonatomic,retain)NSString *getLikes;
@property(nonatomic,retain)NSString *getIfLikes;
@property(nonatomic,retain)NSString *getDisLikes;
@property(nonatomic,retain)NSString *getIfDisLikes;
@property(nonatomic,retain)NSString *getSno;
@property(nonatomic,retain)NSString *getName;
@property(nonatomic,retain)NSString *getComments;
@property(nonatomic,retain)NSString *getType;
@property (nonatomic, strong) MPMoviePlayerController *movieplayer;
@property(retain,nonatomic)NSString *videoUrl;
@property(nonatomic,retain)NSString *getLocation;
@property(nonatomic,retain)NSString *getTime;
@property(nonatomic,retain)NSString *getLikeConferm;
@property(nonatomic,retain)NSString *getDislikeConferm;
@property(nonatomic,retain)NSString *getRowIndex;
@property(nonatomic,retain)NSString *getTabId;
@property (nonatomic, strong) NSOperationQueue *imageDownloadingQueuePic;
@property (nonatomic, strong) NSCache *imageCachePic;
- (void)scrollViewDidScroll:(UIScrollView*)aScrollView;

@end
