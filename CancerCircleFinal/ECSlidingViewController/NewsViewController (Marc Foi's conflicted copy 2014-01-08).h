//
//  NewsViewController.h
//  CancerCircleFinal
//
//  Created by Raminder on 29/04/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "ECSlidingViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "newsCell.h"

@interface NewsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView *myTable;
    NSMutableArray *activityArray;
    NSMutableArray *userArray;   
    NSMutableArray *typeArray;
    NSMutableArray *serialArray;
    UIActivityIndicatorView *indicator;
        IBOutlet UILabel *label;
    IBOutlet UIButton *someButton;
      NSData *imageData;
}
@property (nonatomic, assign) CGFloat peekLeftAmount;
@property (nonatomic, strong) NSOperationQueue *imageDownloadingQueue;
@property (nonatomic, strong) NSCache *imageCache;
-(IBAction)goToCircle;
@property (retain, nonatomic) IBOutlet UIImageView *dropshadow;

@property (retain, nonatomic) IBOutlet UIImageView *profileImage;
@property (retain, nonatomic) IBOutlet UILabel *statusLabel;
@property (retain, nonatomic) IBOutlet UILabel *usernameLabel;
@property (retain, nonatomic) IBOutlet UIButton *following;
@property (retain, nonatomic) IBOutlet UIButton *followers;
@property (strong, nonatomic) NSNumber *getfollowings;
@property (strong, nonatomic) NSNumber *getfollowers;

@end
