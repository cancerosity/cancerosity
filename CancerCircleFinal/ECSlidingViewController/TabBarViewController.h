//
//  TabBarViewController.h
//  CancerCircleFinal
//
//  Created by Raminder on 29/04/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"
#import "NewsViewController.h"

@interface TabBarViewController : UITabBarController

@end
