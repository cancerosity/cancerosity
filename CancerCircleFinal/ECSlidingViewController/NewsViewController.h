//
//  NewsViewController.h
//  CancerCircleFinal
//
//  Created by Raminder on 29/04/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "ECSlidingViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "newsCell.h"
#import "ILTranslucentView.h"
#import "MobileCoreServices/UTCoreTypes.h"
#import "MobileCoreServices/UTType.h"
#import "FXBlurView.h"
typedef enum ScrollDirection {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
} ScrollDirection;
@interface NewsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView *myTable;
    NSMutableArray *activityArray;
    NSMutableArray *userArray;
    NSMutableArray *userPic;
    NSMutableArray *typeArray;
    NSMutableArray *serialArray;
    NSString *searchfollowing;
    NSString *searchfollowers;
    UIActivityIndicatorView *indicator;
        IBOutlet UILabel *label;
    IBOutlet UIButton *someButton;
      NSData *imageData;
    IBOutlet ILTranslucentView *transView;
    NSString *meImageStr;
    IBOutlet UIScrollView *mainScroll;
    CGFloat startContentOffset;
    CGFloat lastContentOffset;
    CGFloat startHeight;
    CGFloat lastHeight;
    IBOutlet FXBlurView *blurView;
    IBOutlet UIImageView *profileBack;
    IBOutlet UIToolbar *blurView2;
    CGRect tableSize;
}
@property (nonatomic, assign) CGFloat peekLeftAmount;
@property (nonatomic, strong) NSOperationQueue *imageDownloadingQueue;
@property (nonatomic, strong) NSCache *imageCache;
-(IBAction)goToCircle;
@property (retain, nonatomic) IBOutlet UIImageView *dropshadow;
@property (nonatomic, assign) NSInteger lastContentOffset2;
@property (retain, nonatomic) IBOutlet UIImageView *profileImage;
@property (retain, nonatomic) IBOutlet UILabel *statusLabel;
@property (retain, nonatomic) IBOutlet UILabel *usernameLabel;
@property (retain, nonatomic) IBOutlet UIButton *following;
@property (retain, nonatomic) IBOutlet UIButton *followers;
@property (strong, nonatomic) NSNumber *getfollowings;
@property (strong, nonatomic) NSNumber *getfollowers;

@end
