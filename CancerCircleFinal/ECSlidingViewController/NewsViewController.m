//
//  NewsViewController.m
//  CancerCircleFinal
//
//  Created by Raminder on 29/04/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import "NewsViewController.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "Login.h"
#import "ODRefreshControl.h"



@interface NewsViewController ()

@end

@implementation NewsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated{
     myTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    imageData=delegate.profilePicData;
    meImageStr=delegate.profileImage;
    // Login *login=[[Login alloc]init];
    //NSMutableAttributedString *attributed=[login atributedString:delegate.status];
    //_statusLabel.attributedText=attributed;
    _statusLabel.text = delegate.status;
    
    
    _profileImage.image=[UIImage imageWithData:imageData];
    profileBack.image=[UIImage imageWithData:imageData];
    profileBack.contentMode = UIViewContentModeScaleAspectFill;
    profileBack.clipsToBounds = YES;
    CALayer *imageLayer = _profileImage.layer;
    
    
    _usernameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:18];
    _usernameLabel.text=delegate.displayName;
    
    _statusLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:12];
    
    
    [imageLayer setCornerRadius:30];
    [imageLayer setBorderWidth:1];
    [imageLayer setBorderColor:[[UIColor alloc] initWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:0.8].CGColor];
    [imageLayer setMasksToBounds:YES];
    _getfollowers=delegate.followers;
    _getfollowings=delegate.following;
    NSString *follower = [_getfollowers stringValue];
    NSString *followings = [_getfollowings stringValue];
    
    
    
    [_followers setTitle:[follower stringByAppendingFormat:@"Followers"] forState:UIControlStateNormal];
    [_followers setTitle:[follower stringByAppendingFormat:@" Followers"] forState: UIControlStateSelected];
    [_followers setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    [_following setTitle:[followings stringByAppendingFormat:@" Followings"] forState: UIControlStateNormal];
    [_following setTitle:[followings stringByAppendingFormat:@" Followings"] forState: UIControlStateSelected];
    [_following setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    
    _followers.layer.borderColor = [[UIColor alloc] initWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:0.34].CGColor;
    _followers.layer.borderWidth = 1;
    _following.layer.borderColor = [[UIColor alloc] initWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:0.34].CGColor;
    _following.layer.borderWidth = 1;


    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    transView.translucentAlpha = 0.8;
    transView.translucentStyle = UIBarStyleDefault;
    transView.translucentTintColor = [UIColor clearColor];
    CircleFirst *circle = [[CircleFirst alloc] init];
    [circle dismissKeyboard];
  
    
}

- (void)viewDidLoad
{
    tableSize = myTable.frame;
    UIInterpolatingMotionEffect *horizontalMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    
    horizontalMotionEffect.minimumRelativeValue = @(-30);
    
    horizontalMotionEffect.maximumRelativeValue = @(30);
    
    [profileBack addMotionEffect:horizontalMotionEffect];
    
    UIInterpolatingMotionEffect *vertMotionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    
    vertMotionEffect.minimumRelativeValue = @(-30);
    
    vertMotionEffect.maximumRelativeValue = @(30);
    
    [profileBack addMotionEffect:vertMotionEffect];
    
    blurView.Dynamic = YES;
    blurView.tintColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    blurView.contentMode = UIViewContentModeBottom;


 
    [super viewDidLoad];
    UINavigationBar *navBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 290, 64)];
    
    
    navBar.barTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    navBar.translucent = YES;
    

  
    //myTable.separatorColor = [[UIColor alloc] initWithRed:(194.0/255.0) green:(195.0/255.0) blue:(196.0/255.0) alpha:1.0];

  
    
    
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    imageData=delegate.profilePicData;
    meImageStr=delegate.profileImage;
   // Login *login=[[Login alloc]init];
    //NSMutableAttributedString *attributed=[login atributedString:delegate.status];
    //_statusLabel.attributedText=attributed;
    _statusLabel.text = delegate.status;
    
    
    _profileImage.image=[UIImage imageWithData:imageData];
    profileBack.image=[UIImage imageWithData:imageData];
    profileBack.contentMode = UIViewContentModeScaleAspectFill;
    profileBack.clipsToBounds = YES;
    CALayer *imageLayer = _profileImage.layer;
    
    
    _usernameLabel.font=[UIFont fontWithName:@"MyriadPro-Bold" size:15];
    _usernameLabel.text=delegate.displayName;
   
    _statusLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:9];
    // dpUrl=delegate.profileImage;
 

    
    
    [imageLayer setCornerRadius:30];
    [imageLayer setBorderWidth:1];
    [imageLayer setBorderColor:[[UIColor alloc] initWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:0.8].CGColor];
    [imageLayer setMasksToBounds:YES];
    _getfollowers=delegate.followers;
    _getfollowings=delegate.following;
    NSString *follower = [_getfollowers stringValue];
    NSString *followings = [_getfollowings stringValue];
    
    
   
    [_followers setTitle:[follower stringByAppendingFormat:@"Followers"] forState:UIControlStateNormal];
    [_followers setTitle:[follower stringByAppendingFormat:@" Followers"] forState: UIControlStateSelected];
    [_followers setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    [_following setTitle:[followings stringByAppendingFormat:@" Followings"] forState: UIControlStateNormal];
    [_following setTitle:[followings stringByAppendingFormat:@" Followings"] forState: UIControlStateSelected];
    [_following setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    
    _followers.layer.borderColor = [[UIColor alloc] initWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:0.34].CGColor;
    _followers.layer.borderWidth = 1;
    _following.layer.borderColor = [[UIColor alloc] initWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:0.34].CGColor;
    _following.layer.borderWidth = 1;
    
    CGFloat titleWidth2=100;
    CGFloat titleWidth = MIN(titleWidth2, 300);
    
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 30)];
    label.backgroundColor = [UIColor clearColor];
    // label.font = [UIFont boldSystemFontOfSize:20.0];
    label.font= [UIFont fontWithName:@"Futura_Light" size:20];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=@"news";
    
    
    
    UIImage* image3 = [UIImage imageNamed:@"close.png"];
    CGRect frameimg = CGRectMake(0, 0,18,18);
    someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(goToCircle)
         forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *backbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    
    
    UINavigationItem *buttonCarrier = [[UINavigationItem alloc]initWithTitle:@""];
    buttonCarrier.titleView = label;
    //Creating some buttons:
    
    
    
    //Putting the Buttons on the Carrier
    [buttonCarrier setLeftBarButtonItem:backbutton];
    
    
    //The NavigationBar accepts those "Carrier" (UINavigationItem) inside an Array
    NSArray *barItemArray = [[NSArray alloc]initWithObjects:buttonCarrier,nil];
    
    // Attaching the Array to the NavigationBar
    [navBar setItems:barItemArray];
    [someButton release];
   
    
    
    myTable.dataSource = self;
    
    [self startIndicator];
    self.imageDownloadingQueue = [[NSOperationQueue alloc] init];
    self.imageDownloadingQueue.maxConcurrentOperationCount = 4;
    self.imageCache = [[NSCache alloc] init];
    
	// Do any additional setup after loading the view.
    self.peekLeftAmount = 40.0f;
    [self.slidingViewController setAnchorRightRevealAmount:260.0f];
    self.slidingViewController.underLeftWidthLayout = ECFullWidth ;// ECFixedRevealWidth;//
    [myTable reloadData];
    
}

-(void)expand
{
    
 CGRect screenRect = [[UIScreen mainScreen] bounds];
 myTable.frame = CGRectMake(0,myTable.frame.origin.y, myTable.frame.size.width, screenRect.size.height);
    
    [UIView animateWithDuration:0.5f
                     animations:^
     {
         
         myTable.frame = CGRectMake(0, 0, myTable.frame.size.width, screenRect.size.height);
         
         
     }
                     completion:^(BOOL finished)
     {
         
       
     }
     ];
 
}

-(void)contract
{
 
 
    [UIView animateWithDuration:0.5f
                     animations:^
     {
         
         myTable.frame = CGRectMake(0, tableSize.origin.y, myTable.frame.size.width, myTable.frame.size.height);
        
         
     }
                     completion:^(BOOL finished)
     {
         
       
 
        
     }
     ];
  
     myTable.frame = CGRectMake(0, tableSize.origin.y, myTable.frame.size.width, tableSize.size.height);
    
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
  
    startContentOffset = lastContentOffset = scrollView.contentOffset.y;

   
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat differenceFromStart = startContentOffset - currentOffset;
    CGFloat differenceFromLast = lastContentOffset - currentOffset;
    lastContentOffset = currentOffset;
    
    
    
    if((differenceFromStart) < 0)
    {
        // scroll up
        if(scrollView.isTracking && (abs(differenceFromLast)>0.5))
        {
            
            
          
            
            [self expand];
            
            
        }
    }
    else {
        if(scrollView.isTracking && (abs(differenceFromLast)>1))
        {
            
          
            [self contract];
            
            
        }
    }
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
}
- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    [self contract];
    return YES;
}
- (void)changeSorting:(ODRefreshControl *)refreshControl
//- (void)changeSorting:(UIRefreshControl *)refreshControl
{
    
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self startIndicator]; // 1
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            [refreshControl endRefreshing];// 2
        });
    });
    
}


-(void)startIndicator
{
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];//UIActivityIndicatorViewStyleWhiteLarge
    indicator.frame = CGRectMake(self.view.bounds.size.width / 2.0f - indicator.frame.size.width /2.0f, self.view.bounds.size.height / 2.0f - indicator.frame.size.height /2.0f, indicator.frame.size.width, indicator.frame.size.height);
    //indicator.color = [UIColor colorWithRed:1 green:0.8 blue:0.9 alpha:1 ];
    indicator.color = [UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
    CGAffineTransform transform = CGAffineTransformMakeScale(2.2f, 2.2f);
    indicator.transform = transform;
   
    [self.view addSubview:indicator];
    indicator.hidden = YES;
    [self performSelector:@selector(newsData) withObject:nil afterDelay:0.001];
    
}
-(void)newsData
{
    AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
    activityArray=[[NSMutableArray alloc]initWithCapacity:activityArray.count];
    userArray=[[NSMutableArray alloc]initWithCapacity:userArray.count];
    typeArray=[[NSMutableArray alloc]initWithCapacity:typeArray.count];
    serialArray=[[NSMutableArray alloc]initWithCapacity:serialArray.count];
    NSString *url = [NSString stringWithFormat:SERVER_URL@"/news.do?username=%@",delegate.mynameLogin];
  
    NSURL *urlrequest=[NSURL URLWithString:url];
    
    
    //NSString *stringData= [NSString stringWithContentsOfURL:urlrequest encoding:nil error:nil];
    // NSLog(@"Recieved data String &&&&&&&&&&&& %@",stringData);
    
    NSData * dataofStrig = [NSData dataWithContentsOfURL:urlrequest];//[stringData dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions                        error:&error];
    NSLog(@"JSOn from url %@",json);
    NSString *param=[json valueForKey:@"param"];
    if ([param isEqualToString:@"false"]) {
        //[activityArray addObject:@"No News"];
    }
    else
    {
        int length=[json count];
        for(int i=0;i<length-1;i++)    {
            
            NSString *indexStr = [NSString stringWithFormat:@"%d",i];
            NSString *data=[@"data" stringByAppendingFormat:@"%@",indexStr];
            [typeArray addObject:[[json objectForKey:data]valueForKey:@"type"]];
            [serialArray addObject:[[json objectForKey:data]valueForKey:@"sno"]];
            NSLog(@"Message from news %@",[[json objectForKey:data]valueForKey:@"message"]);
            [activityArray addObject:[[json objectForKey:data]valueForKey:@"message"]];
            NSString *userDP=[[json objectForKey:data]valueForKey:@"profile_image"];
            [userArray addObject:userDP];
            
        }
    }
    //activityArray=delegate.activityArray;
    //userArray=delegate.userArray;
    [indicator stopAnimating];
    indicator.hidden=YES;
    [myTable reloadData];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [activityArray count];
}
- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
  
    
    
    newsCell *imgCell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (imgCell == nil)
    {
        imgCell = [[[newsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        NSLog(@"index path in cell %i",indexPath.row);
        
    }
    CALayer *imageLayer = imgCell.imageView.layer;
    
    
    
    [imageLayer setCornerRadius:17.5f];
    [imageLayer setBorderWidth:1];
    [imageLayer setBorderColor:[[UIColor alloc] initWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:0.8].CGColor];
    [imageLayer setMasksToBounds:YES];
    
   
    
    NSString *imageUrlString = [userArray objectAtIndex: indexPath.row];
    
    NSLog(@"image url string iss:- %@",imageUrlString);
    UIImage *cachedImage = [self.imageCache objectForKey:imageUrlString];
    if (cachedImage)
    {
        //imgCell.imageView.image = cachedImage;
        imgCell.imageView.image = cachedImage;
    }
    else
    {
        // you'll want to initialize the image with some blank image as a placeholder
        
        //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
        imgCell.imageView.image = [UIImage imageNamed:@"editPic.png"];
        [self.imageDownloadingQueue addOperationWithBlock:^{
            NSURL *imageURL   = [NSURL URLWithString:imageUrlString];
            NSData *imageData2 = [NSData dataWithContentsOfURL:imageURL];
            UIImage *image    = nil;
            if (imageData2)
                image = [UIImage imageWithData:imageData2];
            if (image)
            {
                // add the image to your cache
                
                [self.imageCache setObject:image forKey:imageUrlString];
                
                // finally, update the user interface in the main queue
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                    if (updateCell)
                        //imgCell.imageView.image = image;
                        
                        imgCell.imageView.image = image;
                    
                    
                }];
            }
        }];
    }
    
    UILabel *descriptionLabel=[[UILabel alloc]initWithFrame:CGRectMake(55,10,200,30)];
    descriptionLabel.font = [UIFont systemFontOfSize:10.0];
    descriptionLabel.textColor=[UIColor blackColor];
    //[descriptionLabel setNumberOfLines:0];
    descriptionLabel.backgroundColor=[UIColor clearColor];
    [descriptionLabel setText:[activityArray objectAtIndex:indexPath.row]];
    [imgCell.contentView addSubview:descriptionLabel];
    
    return imgCell;
    //}
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
    delegate.newsSerial=[serialArray objectAtIndex:indexPath.row];
    NSString *identifier=[typeArray objectAtIndex:indexPath.row];
    if ([identifier isEqualToString:@"images"]) {
        delegate.newsSerial=[serialArray objectAtIndex:indexPath.row];
        UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"iNewsDetail"];
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
    }
    else  if ([identifier isEqualToString:@"story"]) {
        UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"sNewsDetail"];
        
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
    }
    else  if ([identifier isEqualToString:@"video"]) {
        UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"vNewsDetail"];
        
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
    }
}

-(IBAction)goToCircle
{
    
    
    
    UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_dropshadow release];
    [_profileImage release];
    [_statusLabel release];
    [_usernameLabel release];
    [_following release];
    [_followers release];
    [transView release];
    [mainScroll release];
    [blurView release];
    [profileBack release];
    [blurView2 release];
    [blurView2 release];
    [super dealloc];
}
@end
