//
//  Registration.h
//  CancerCircleFirst
//
//  Created by Raminder on 11/01/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MobileCoreServices/UTType.h"
#import "MobileCoreServices/UTCoreTypes.h"
#import "FXBlurView.h"

@interface Registration : UIViewController<UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextViewDelegate>
{
    
    IBOutlet UIScrollView *myScroll;
    IBOutlet UITextField *userText;
    IBOutlet UITextField *passText;
    IBOutlet UITextField *contactText;
    IBOutlet UITextField *mailText;
    IBOutlet UITextField *firstText;
    IBOutlet UITextField *lastText;
    IBOutlet UITextField *dobText;
    IBOutlet UITextField *whoText;
    IBOutlet UITextField *conpassText;
    IBOutlet UITextView  *tellText;
    
    NSString *usernameText;
    NSString *passwordText;
    NSString *phoneText;
    NSString *emailText;
    NSString *firstNText;
    NSString *lastNText;
    NSString *birthText;
    NSString *whoareText;
    NSString *confirmpassText;
    NSString  *tellusText;
    
     BOOL flag;
    UIImage *yourImg, *myImg;
    UIAlertView *alert;
    UIAlertView *alert2;
     NSString *authStr;
    IBOutlet UIImageView *cpassImg;
    IBOutlet UIImageView *bgImage;
    IBOutlet UIImageView *userImg;
    IBOutlet UIImageView *passImg;
    IBOutlet UIImageView *contactImg;
    IBOutlet UIImageView *mailImg;
    IBOutlet UIImageView *firstImg;
    IBOutlet UIImageView *lastImg;
    IBOutlet UIImageView *dobImg;
    IBOutlet UIButton *someButton;
    IBOutlet UIImageView *whoimg;
    IBOutlet UIImageView *tellImg;
     NSString* userNameResult;
    IBOutlet UILabel *label;
    NSString *day;
    NSString *month;
    NSString *year;
    IBOutlet UILabel *warning;
    NSArray *dateArray;
     UITextField *activeField;
    IBOutlet UITableView *registraionTable;
@protected
      // we keep a weak pointer because, well, we don't need a strong pointer.
    CGFloat _scrollViewContentOffsetYThreshold;  // defines at what contentOffset of the above scrollView the navigationBar should start scrolling
}
@property (retain, nonatomic) IBOutlet FXBlurView *transBack;
@property (strong, nonatomic) NSArray *whoareyou;
@property (retain, nonatomic) IBOutletCollection(UIImageView) NSArray *textfields;
@property (retain, nonatomic) IBOutlet UIButton *button;
@property (retain, nonatomic) IBOutlet UIPickerView *whoInput;
-(void)goBack;
@property (nonatomic, strong) CALayer *colorLayer;
-(IBAction)registered;
-(IBAction) goLogin;
-(IBAction)hidekeyboard:(id)sender;

@property (nonatomic, retain) IBOutlet UIToolbar *accessoryView;
@property (nonatomic, retain) IBOutlet UIDatePicker *customInput;
- (IBAction)dateChanged:(id)sender;
- (IBAction)doneEditing:(id)sender;
- (IBAction)whoareyou:(id)sender;
// other stuff related to your app here...

@property (retain, nonatomic) IBOutlet UIView *backView;
@property (retain, nonatomic) IBOutlet UITableViewCell *userCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *emailCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *firstCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *lastCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *phoneCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *passCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *CpassCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *dobCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *whoCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *tellCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *regCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *tocCell;
@property (strong, nonatomic)  NSMutableArray *textData;
@end
