//
//  reportViewController.m
//  CancerCircleFirst
//
//  Created by Raminder on 09/04/13.
//
//

#import "reportViewController.h"
#import "AppDelegate.h"

@interface reportViewController ()

@end

@implementation reportViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"640x960bg.png"]];
    UINavigationBar *navBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    
    
    navBar.barTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    navBar.translucent = YES;
    
    
    CGFloat titleWidth2=100;
    CGFloat titleWidth = MIN(titleWidth2, 300);
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 30)];
    label.backgroundColor = [UIColor clearColor];
    // label.font = [UIFont boldSystemFontOfSize:20.0];
    label.font=[UIFont fontWithName:@"Futura_Light" size:20];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=@"Report Content";
    
    
    
    UIImage* image3 = [UIImage imageNamed:@"close.png"];
    CGRect frameimg = CGRectMake(0, 0,18,18);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(goBack)
         forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *backbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    
    
    UINavigationItem *buttonCarrier = [[UINavigationItem alloc]initWithTitle:@""];
    buttonCarrier.titleView = label;
    //Creating some buttons:
    
    
    
    //Putting the Buttons on the Carrier
    [buttonCarrier setLeftBarButtonItem:backbutton];
    
    
    //The NavigationBar accepts those "Carrier" (UINavigationItem) inside an Array
    NSArray *barItemArray = [[NSArray alloc]initWithObjects:buttonCarrier,nil];
    
    // Attaching the Array to the NavigationBar
    [navBar setItems:barItemArray];
    [someButton release];
    [self.view addSubview:navBar];
    
	// Do any additional setup after loading the view.
    reports=[[NSArray alloc]initWithObjects:@"I don't like this content",@"This content is spam or a scam",@"Self-harm(eating disorder,cutting,or suicidal content)",@"Harassment or bullying",@"Drug use",@"Graphic violence",@"hate speech or symbol",@"Intellectual property violation", nil];
    [myTable reloadData];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [reports count];
    //  return 5;
    
}

- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
}
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    myTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    myTable.backgroundColor = [UIColor clearColor];
    [myTable setBackgroundView:nil];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.accessoryType=UITableViewCellAccessoryDetailDisclosureButton;
    cell.backgroundView = [ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cellWhiteBg2.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];
    
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        NSLog(@"index path in cell %i",indexPath.row);
        
    }
    // cell.accessoryView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"20x20.png"]] autorelease];
    cell.textLabel.font=[UIFont fontWithName:@"Futura_Light" size:14];
    cell.textLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
    cell.textLabel.text=[reports objectAtIndex:indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if([delegate.typeReport isEqualToString:@"image"])
    {
        delegate.typeReport=@"images";
    }
    NSString *converted = [[reports objectAtIndex:indexPath.row] stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    NSString *urlReport = [NSString stringWithFormat:SERVER_URL@"/report.do?username=%@&sno=%@&type=%@,reason=%@",delegate.mynameLogin,delegate.snoReport,delegate.typeReport,converted];
    NSLog(@"comment Url %@",urlReport);
    NSURL *urlrequest =[NSURL URLWithString:urlReport ];
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    NSError* error;
    NSData *dataLike =[NSData dataWithContentsOfURL:urlrequest];
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataLike options:kNilOptions                        error:&error];
    NSLog(@"JSON format:- %@",json);
    if([[json objectForKey:@"param"] isEqualToString:@"true"])
    {
        //[self performSegueWithIdentifier:@"reportSent" sender:self];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Report sent" message:@" your message has been recorded." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Report not sent" message:@"You already has sent the report" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}
-(IBAction)goBack
{
    [self dismissViewControllerAnimated:YES completion:nil];
    //[self performSegueWithIdentifier:@"reportToCircle" sender:self];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
