//
//  AppDelegate.m
//  CancerCircleFinal
//
//  Created by Raminder on 29/04/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import "AppDelegate.h"
#import <AWSRuntime/AWSRuntime.h>
#import <FacebookSDK/FacebookSDK.h>
#import "TestFlight.h"
#import "UploadButtons.h"

@implementation AppDelegate
@synthesize window = _window;
@synthesize myname,mynameLogin,myPassword,flag,tabId,status,profileImage,profilePicData,emailId,contact,requestCount,activityArray,userArray;
@synthesize dataUpdate,dataUpdate2,snoArray,likearray,dislikearray,profielImages,imageDatas,dataType,likesNameArray,dislikesNameArray,differUser,commentNameArray,commentsArray,commentPicArray,commentCounts,locationArray,timeArray,setEmailNotification,insertedTimeArray,setPrivacy,fourCommentArray,privacyArray;
@synthesize PdataType,PdataUpdate,PdataUpdate2,Pdislikearray,Plikearray,PsnoArray,followers,following,followersNames,PcommentCounts,PdislikesNameArray,PlikesNameArray,followingNames,PlocationArray,PtimeArray,PgetInertedTime,PfourCommentArray;

@synthesize imageCommentNames,imageComments,imageCommentsPics,imageCommentsType,wordToSearch;
@synthesize snoReport,userReport,typeReport,getInertedTime,getLastInertedTime,setLocation,SgetLastInseted;
@synthesize token,secondTabId,newsSerial,contentSTR;
@synthesize session = _session;

- (void)dealloc
{
    [_window release];
    [super dealloc];
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

  

    [TestFlight takeOff:@"cf999bd6-ea3d-4635-a864-c2d5aa667f46"];
    
     [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    
    // Override point for customization after application launch.
    
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
        NSLog(@"Found a cached session");
        // If there's one, just open the session silently, without showing the user the login UI
        [FBSession openActiveSessionWithReadPermissions:@[@"basic_info"]
                                           allowLoginUI:NO
                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                          // Handler for session state changes
                                          // This method will be called EACH time the session state changes,
                                          // also for intermediate states and NOT just when the session open
                                          [self sessionStateChanged:session state:state error:error];
                                      }];
        
        // If there's no cached session, we will show a login button
    } else {
      
        
    }

    
    return YES;
    
}
- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    // If the session was opened successfully
    if (!error && state == FBSessionStateOpen){
        NSLog(@"Session opened");
        // Show the user the logged-in UI
        //ShareSettingsView * shareSettings = [[ShareSettingsView alloc]init];
        //[shareSettings userLoggedIn];
        return;
    }
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
        // If the session is closed
        NSLog(@"Session closed");
        // Show the user the logged-out UI
        //ShareSettingsView * shareSettings = [[ShareSettingsView alloc]init];
        //[shareSettings userLoggedOut];
    }
    
    // Handle errors
    if (error){
        NSLog(@"Error");
        NSString *alertText;
        NSString *alertTitle;
        // If the error requires people using an app to make an action outside of the app in order to recover
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
            alertTitle = @"Something went wrong";
            alertText = [FBErrorUtility userMessageForError:error];
            //[self showMessage:alertText withTitle:alertTitle];
        } else {
            
            // If the user cancelled login, do nothing
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                NSLog(@"User cancelled login");
                
                // Handle session closures that happen outside of the app
            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
                alertTitle = @"Session Error";
                alertText = @"Your current session is no longer valid. Please log in again.";
                //[self showMessage:alertText withTitle:alertTitle];
                
                // For simplicity, here we just show a generic message for all other errors
                // You can learn how to handle other errors using our guide: https://developers.facebook.com/docs/ios/errors
            } else {
                //Get more error information from the error
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                
                // Show the user an error message
                alertTitle = @"Something went wrong";
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                //[self showMessage:alertText withTitle:alertTitle];
            }
        }
        // Clear this token
        [FBSession.activeSession closeAndClearTokenInformation];
        // Show the user the logged-out UI
       
    }
}

// Show the user the logged-out UI
- (void)userLoggedOut
{

    [self.shareSettingsPage.fbLogin setTitle:@"Login" forState:UIControlStateNormal];

}

// Show the user the logged-in UI
- (void)userLoggedIn
{
    
    [self.shareSettingsPage.fbLogin setTitle:@"Log out" forState:UIControlStateNormal];

    [[FBRequest requestForMe] startWithCompletionHandler:
     ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
         if (!error) {
             NSLog(@"logged in and users name is: %@", user.name);
             self.shareSettingsPage.fbUsername.text=user.name;
         } else {
             NSLog(@"error: %@", error);
         }
     }];
    
   
    
}
// Show an alert message
- (void)showMessage:(NSString *)text withTitle:(NSString *)title
{
    [[[UIAlertView alloc] initWithTitle:title
                                message:text
                               delegate:self
                      cancelButtonTitle:@"OK!"
                      otherButtonTitles:nil] show];
}




- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation

{
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                        withSession:self.session];
    
     return [FBSession.activeSession handleOpenURL:url];
    
    
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url

{
    
    return self;
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application

{
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
}



- (void)applicationWillEnterForeground:(UIApplication *)application

{
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
}



- (void)applicationDidBecomeActive:(UIApplication *)application

{
    
    [FBAppCall handleDidBecomeActiveWithSession:self.session];
     [FBAppCall handleDidBecomeActive];
    
  
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
}



- (void)applicationWillTerminate:(UIApplication *)application

{
    
    
  
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
}
//- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
//{
//    [coder encodeObject:dataUpdate forKey:@"data_update"];
//     [coder encodeObject:snoArray forKey:@"snoArray"];
//     [coder encodeObject:profielImages forKey:@"profile_images"];
//    [coder encodeObject:differUser forKey:@"differuser"];
//    [coder encodeObject:likearray forKey:@"like_count"];
//     [coder encodeObject:dislikearray forKey:@"dislike_count"];
//     [coder encodeObject:likesNameArray forKey:@"likes_name"];
//     [coder encodeObject:dislikesNameArray forKey:@"dislikes_name"];
//     [coder encodeObject:imageDatas forKey:@"image_data"];
//     [coder encodeObject:dataType forKey:@"data_type"];
//     [coder encodeObject:commentCounts forKey:@"comment_count"];
//     [coder encodeObject:locationArray forKey:@"location"];
//     [coder encodeObject:timeArray forKey:@"time"];
//     [coder encodeObject:fourCommentArray forKey:@"comment_array"];
//
//    [self encodeRestorableStateWithCoder:coder];
//}
//- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
//{
//    dataUpdate = [coder decodeObjectForKey:@"data_update"];
//    snoArray = [coder decodeObjectForKey:@"snoArray"];
//    profielImages = [coder decodeObjectForKey:@"profile_images"];
//    differUser = [coder decodeObjectForKey:@"differuser"];
//    likearray = [coder decodeObjectForKey:@"like_count"];
//    dislikearray = [coder decodeObjectForKey:@"dislike_count"];
//    likesNameArray = [coder decodeObjectForKey:@"likes_name"];
//    dislikesNameArray = [coder decodeObjectForKey:@"dislikes_name"];
//    imageDatas = [coder decodeObjectForKey:@"image_data"];
//    dataType = [coder decodeObjectForKey:@"data_type"];
//    commentCounts  = [coder decodeObjectForKey:@"comment_count"];
//    locationArray = [coder decodeObjectForKey:@"location"];
//    timeArray = [coder decodeObjectForKey:@"time"];
//    fourCommentArray = [coder decodeObjectForKey:@"comment_array"];
//    [self decodeRestorableStateWithCoder:coder];
//
//}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}
-(BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder
{
    return YES;
}

-(BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder
{
    return YES;
}

-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    completionHandler(UIBackgroundFetchResultNewData);
}






@end
