//
//  OpenUrlView.m
//  CancerCircleFirst
//
//  Created by Raminder on 17/04/13.
//
//

#import "OpenUrlView.h"
#import "AppDelegate.h"

@interface OpenUrlView ()

@end

@implementation OpenUrlView
@synthesize getUrl,webView,getId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
       self.navigationController.navigationBarHidden = NO;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    
}
- (void)viewDidLoad {
    
    NSLog(@"URL to open in web view %@",getUrl);
   
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    UIImage* image3 = [UIImage imageNamed:@"close.png"];
    CGRect frameimg = CGRectMake(0, 0,18,18);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(goBack)
         forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    self.tabBarController.tabBar.hidden = YES;

    CGRect frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height - 44, [[UIScreen mainScreen] bounds].size.width, 44);
   
    toolBar = [[UIToolbar alloc] initWithFrame:frame];
    backButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRewind target:self action:@selector(goBack2)];
    forwardButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFastForward target:self action:@selector(goForward)];
    refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshWebView)];
    //add buttons to the toolbar
    backButton.enabled = NO;
    forwardButton.enabled = NO;
    webView.delegate = self;
    
   
    
    UIColor *green = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
 refreshButton.tintColor = green;
    backButton.tintColor = green;
    forwardButton.tintColor = green;
    loadingActivity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite] ;
    [loadingActivity startAnimating];
    loadingActivity.tintColor = green;
    loadingActivity.color = green;
    loadingActivity.frame = CGRectMake(0, 0, 5, 5);
  
    UIBarButtonItem *activity = [[UIBarButtonItem alloc] initWithCustomView:loadingActivity];
    loadingActivity.hidesWhenStopped = YES;
    
    
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    
    fixedItem.width = 60.0f;
    UIBarButtonItem *fixedItem2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem2.width = 50.0f;
    [toolBar setItems:[NSArray arrayWithObjects:backButton,fixedItem, forwardButton, fixedItem, refreshButton,fixedItem, activity, nil]];
 
    
      UIBarButtonItem *backbutton2 =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.leftBarButtonItem = backbutton2;
    [self.view addSubview:toolBar];

    [navBar setTintColor:[[UIColor alloc] initWithRed:(0.0/255.0) green:(214.0/255.0) blue:(11.0/255.0) alpha:1.0]];
    /*UIImage* image3 = [UIImage imageNamed:@"back22.png"];
     CGRect frameimg = CGRectMake(0, 0,35,35);
     UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
     [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
     [someButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
     [someButton setShowsTouchWhenHighlighted:YES];
     UIBarButtonItem *backbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
     self.navigationItem.leftBarButtonItem=backbutton;
     AppDelegate *delegate=[[UIApplication sharedApplication]delegate];*/
    webView.mediaPlaybackRequiresUserAction = NO;
    NSString *urlAddress = getUrl;
    
    //Create a URL object.
    NSURL *url = [NSURL URLWithString:urlAddress];
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    NSString *youtubeURL = urlAddress;
    
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)" options:NSRegularExpressionCaseInsensitive error:&error];
    NSRange rangeOfFirstMatch = [regex rangeOfFirstMatchInString:youtubeURL options:0 range:NSMakeRange(0, [youtubeURL length])];
    static NSString *youTubeVideoHTML = @"<!DOCTYPE html><html><head><style>body{margin:0px 0px 0px 0px;}</style></head> <body> <div id=\"player\"></div> <script> var tag = document.createElement('script'); tag.src = \"http://www.youtube.com/player_api\"; var firstScriptTag = document.getElementsByTagName('script')[0]; firstScriptTag.parentNode.insertBefore(tag, firstScriptTag); var player; function onYouTubePlayerAPIReady() { player = new YT.Player('player', { width:'%0.0f', height:'%0.0f', videoId:'%@', events: { 'onReady': onPlayerReady, } }); } function onPlayerReady(event) { event.target.playVideo(); } </script> </body> </html>";
    if(!NSEqualRanges(rangeOfFirstMatch, NSMakeRange(NSNotFound, 0)))
    {
        NSString *substringForFirstMatch = [youtubeURL substringWithRange:rangeOfFirstMatch];
        
     
      
        
        CGFloat height = 250;

       
        
        NSString *html = [NSString stringWithFormat:youTubeVideoHTML, self.view.frame.size.width,height, substringForFirstMatch];
        
        [webView loadHTMLString:html baseURL:[[NSBundle mainBundle] resourceURL]];
        webView.frame = CGRectMake(0,0, [[UIScreen mainScreen] bounds].size.width, 290);

        CGRect frame = CGRectMake(0, 320, [[UIScreen mainScreen] bounds].size.width, 200);
        UILabel *pagename = [[UILabel alloc] initWithFrame:frame];
        pagename.backgroundColor = [UIColor clearColor];
        pagename.text = title;
        pagename.textColor = [UIColor blackColor];
        [self.view addSubview:pagename];
        
       
     
        
        
        
    } else {
       
       [webView loadRequest:requestObj];
    }
  
  
   
}
-(void)actualizeButtons {
    backButton.enabled = [webView canGoBack];
    forwardButton.enabled = [webView canGoForward];
}

-(IBAction)refreshWebView {
    if (isRefreshing == YES) {
        [webView stopLoading];
        refreshButton.style = UIBarButtonSystemItemRefresh;
    }else {
        [webView reload];
        refreshButton.style = UIBarButtonSystemItemStop;
    }
}
-(IBAction)goBack2 {
    [webView goBack];
    [self actualizeButtons];
}
-(IBAction)goForward{
    [webView goForward];
    [self actualizeButtons];
}

- (void)playVideoWithId:(NSString *)videoId {
   
}

-(void)goBack
{
    self.tabBarController.tabBar.hidden = NO;
    [self.webView setDelegate:nil];
    [self.webView stopLoading];
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)viewWillDisappear:(BOOL)animated {
	
    webView.delegate = nil;
    [webView stopLoading];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [loadingActivity stopAnimating];
    [refreshButton release];
    refreshButton = nil;
    refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshWebView)];
    isRefreshing = NO;
   
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [loadingActivity stopAnimating];
    [refreshButton release];
    refreshButton = nil;
          title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    
    CGFloat titleWidth2=100;
    CGFloat titleWidth = MIN(titleWidth2, 300);
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 30)];
    label.backgroundColor = [UIColor clearColor];
    // label.font = [UIFont boldSystemFontOfSize:20.0];
    label.font=[UIFont fontWithName:@"Futura_Light" size:20];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=title;
    
    self.navigationItem.titleView = label;
    refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshWebView)];
    isRefreshing = NO;
   
    [self actualizeButtons];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [loadingActivity startAnimating];
    [refreshButton release];
    refreshButton = nil;
    isRefreshing = YES;
    
    refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(refreshWebView)];
   
}
- (void) dealloc
{
    [super dealloc];
    webView.delegate = nil;
}

@end
