//
//  SettingButtons.m
//  CancerCircleFirst
//
//  Created by Raminder on 25/01/13.
//
//

#import "SettingButtons.h"
#import "Login.h"
#import "AppDelegate.h"
#import "ShareSettingsView.h"
#import <QuartzCore/QuartzCore.h>
#define kNavBarDefaultPosition CGPointMake(160, 42)

@interface SettingButtons ()

@end

@implementation SettingButtons
@synthesize fbGraph;

-(void)viewWillAppear:(BOOL)animated
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    profileImage.image=[UIImage imageWithData:delegate.profilePicData];
  
    
}
-(void)viewDidAppear:(BOOL)animated
{
    statusView.textColor=[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1];
    // statusView.text=@"Modify your profile status with not more than 100 characters";
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    idAtSettings=delegate.tabId;
    statusView.text=delegate.status;
    NSLog(@"Id at upload %@",idAtSettings);
    delegate.tabId=@"settings";
    username=delegate.mynameLogin;
    statusView.returnKeyType = UIReturnKeyDone;
    dpNameImg.image = [UIImage imageNamed:@"tick.png"];
    userNameImg.image = [UIImage imageNamed:@"tick.png"];
    passwordImg.image = [UIImage imageNamed:@"tick.png"];
    contactImg.image = [UIImage imageNamed:@"tick.png"];
    emailImg.image = [UIImage imageNamed:@"tick.png"];
    shareImg.image= [UIImage imageNamed:@"tick.png"];
    [dpName setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [userName setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [password setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [email setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [share setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [contact setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    circleRefresh = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"refreshcircleBig.png"]];
    circleRefresh.frame = CGRectMake(floor(self.view.frame.size.width / 2),
                                     floor(self.view.frame.size.height / 2),
                                     60, 60);
    
    CGRect frame2 = circleRefresh.frame;
    frame2.origin.x = self.view.frame.size.width / 2 - frame2.size.width / 2;
    frame2.origin.y = self.view.frame.size.height / 2 - frame2.size.height / 2;
    circleRefresh.frame = frame2;
    
    circleRefresh.center = CGPointMake(floor(self.view.frame.size.width / 2), floor(self.view.frame.size.height / 2));
    
    
    
    circleRefresh2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"refreshcircleBig.png"]];
    circleRefresh2.frame = CGRectMake(floor(self.view.frame.size.width / 2),
                                      floor(self.view.frame.size.height / 2),
                                      30, 30);
    
    CGRect frame3 = circleRefresh2.frame;
    frame3.origin.x = self.view.frame.size.width / 2 - frame3.size.width / 2;
    frame3.origin.y = self.view.frame.size.height / 2 - frame3.size.height / 2;
    circleRefresh2.frame = frame3;
    
    circleRefresh2.center = CGPointMake(floor(self.view.frame.size.width / 2), floor(self.view.frame.size.height / 2));
    
    
    self.indiView = [[FXBlurView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.indiView.tintColor = [UIColor clearColor];
    self.indiView.blurRadius = 0;
    self.indiView.alpha = 0.0f;
    [self.indiView addSubview:circleRefresh];
    [self.indiView addSubview:circleRefresh2];
    
    [self.view addSubview:self.indiView];
    
    CABasicAnimation *fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    fullRotation.fromValue = [NSNumber numberWithFloat:0];
    fullRotation.toValue = [NSNumber numberWithFloat:((360*M_PI)/180)];
    fullRotation.duration = 1.1;
    fullRotation.repeatCount = 1e100;
    [circleRefresh.layer addAnimation:fullRotation forKey:@"360"];
   
    
    
    
    CABasicAnimation *fullRotation2 = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    fullRotation2.fromValue = [NSNumber numberWithFloat:0];
    fullRotation2.toValue = [NSNumber numberWithFloat:((-360*M_PI)/180)];
    fullRotation2.duration = 1.2;
    fullRotation2.repeatCount = 1e100;
    [circleRefresh2.layer addAnimation:fullRotation2 forKey:@"360"];
    

    
   
    
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];//UIActivityIndicatorViewStyleWhiteLarge
    indicator.frame = CGRectMake(self.view.bounds.size.width / 2.0f - indicator.frame.size.width /2.0f, self.view.bounds.size.height / 2.0f - indicator.frame.size.height /2.0f, indicator.frame.size.width, indicator.frame.size.height);
    //indicator.color = [UIColor colorWithRed:1 green:0.8 blue:0.9 alpha:1 ];
    indicator.color = [UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
    CGAffineTransform transform = CGAffineTransformMakeScale(2.2f, 2.2f);
    indicator.transform = transform;
    //[self.indiView addSubview:indicator];
    
    
  
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    flag=0;
   
    // myScroll.pagingEnabled=YES;
    // [profielImage setHidden:TRUE];
    
    [_logoutButton setBackgroundColor:[UIColor blackColor]];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _logoutButton.bounds;
    gradient.colors = [NSArray arrayWithObjects: (id)[[[UIColor alloc] initWithRed:(239.0/255.0) green:(198.0/255.0) blue:(198.0/255.0) alpha:1.0] CGColor], (id)[[[UIColor alloc] initWithRed:(250.0/255.0) green:(175.0/255.0) blue:(175.0/255.0) alpha:1.0] CGColor],nil];
    [_logoutButton.layer insertSublayer:gradient atIndex:0];
    _logoutButton.layer.cornerRadius = 5;
    _logoutButton.layer.masksToBounds = YES;

    
	
    CGFloat titleWidth2 = 100;
    CGFloat titleWidth = MIN(titleWidth2, 300);
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 30)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Futura_Light" size:20];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=@"Settings";
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
   
    navBar.barTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    
    
    self.navigationItem.titleView = label;
    UIImage* image3 = [UIImage imageNamed:@"close.png"];
    CGRect frameimg = CGRectMake(0, 0,18,18);
    someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(goLogin)
         forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *backbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.leftBarButtonItem=backbutton;
    [someButton release];
    statusView.returnKeyType = UIReturnKeyDone;
    statusView.font=[UIFont fontWithName:@"MyriadPro-Regular" size:12];
    statusView.textColor=[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1];
    //statusView.text=@"Modify your profile status with not more than 100 characters";
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    profileStatus=delegate.status;
    statusView.text=delegate.status;
    profileImage.image=[UIImage imageWithData:delegate.profilePicData];
    dpName.text=delegate.firstName;
    [dpName setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [userName setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    userName.text=delegate.lastName;
    [password setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    password.text=delegate.myPassword;
    [email setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    email.text=delegate.emailId;
    [share setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [contact setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    contact.text=delegate.contact;
    contact.keyboardType=UIKeyboardTypeDecimalPad;
    password.secureTextEntry=YES;
    [myScroll setContentSize:CGSizeMake(320, 580)];
    myScroll.scrollEnabled = YES;
    
    //[self.view addSubview:storyView];
    //statusView.delegate = self;
    
    for(UIImageView *imageView in self.textFields)
    {
        imageView.layer.cornerRadius = 3;
        imageView.layer.masksToBounds = YES;
        [imageView.layer setBorderWidth:1.0f];
        [imageView.layer setBorderColor:[[[UIColor alloc] initWithRed:(233.0/255.0) green:(233.0/255.0) blue:(233.0/255.0) alpha:1.0] CGColor]];
        [imageView setBackgroundColor:[[UIColor alloc] initWithRed:(247.0/255.0) green:(254.0/255.0) blue:(246.0/255.0)alpha:1.0]];
        
    }
    
    
    if([delegate.setLocation isEqualToString:@"yes"])
        
    {
        [switchToggle setOn:YES animated:YES];
    }
    else{
        [switchToggle setOn:NO animated:YES];
    }
    if([delegate.setEmailNotification isEqualToString:@"yes"])
        
    {
        [switchToggle2 setOn:YES animated:YES];
    }
    else{
        [switchToggle2 setOn:NO animated:YES];
    }
    if([delegate.setPrivacy isEqualToString:@"yes"])
        
    {
        [switchToggle3 setOn:YES animated:YES];
    }
    else{
        [switchToggle3 setOn:NO animated:YES];
    }
    
}
- (void)scrollViewDidScroll:(UIScrollView*)aScrollView
{
    if(myScroll == aScrollView){
        
        CALayer *layer = self.navigationController.navigationBar.layer;
        
        CGFloat contentOffsetY = aScrollView.contentOffset.y;
        
        if (contentOffsetY > _scrollViewContentOffsetYThreshold) {
            layer.position = CGPointMake(layer.position.x,
                                         30 - MIN((contentOffsetY - _scrollViewContentOffsetYThreshold), 30.0));
            [self fadeout];
            
        }
        else
        {
            [self fadein];
            layer.position = kNavBarDefaultPosition;
            
        }
    }
}
-(void)fadeout{
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options:UIViewAnimationCurveEaseInOut
                     animations:^ {
                         label.alpha = 0.0;
                         someButton.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         
                     }];
    
}
-(void)fadein{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationCurveEaseInOut
                     animations:^ {
                         label.alpha = 1.0;
                         someButton.alpha = 1.0;
                     }
                     completion:^(BOOL finished) {
                         [someButton setHidden:NO];
                         [label setHidden:NO];
                         
                     }];
}
-(IBAction)shareSetting
{
    //UIAlertView *alert2=[[UIAlertView alloc]initWithTitle:@"Change Share Settings" message:@"Go to your iphone settings" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //[alert2 show];
    ShareSettingsView *share2=[self.storyboard instantiateViewControllerWithIdentifier:@"share"];
    [self.navigationController pushViewController:share2 animated:YES];
}
-(IBAction)hidekeyboard:(id)sender
{
    [userName resignFirstResponder];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(([dpName.text length]>16)||([dpName.text length]<2))
    {
        [dpNameImg setHidden:FALSE];
        dpNameImg.image = [UIImage imageNamed:@"error.png"];
    }
    else{
        
        [dpNameImg setHidden:FALSE];
        dpNameImg.image = [UIImage imageNamed:@"tick.png"];
    }

    if(([userName.text length]>16)||([userName.text length]<2))
    {
        [userNameImg setHidden:FALSE];
        userNameImg.image = [UIImage imageNamed:@"error.png"];
    }
    else{
        
        [userNameImg setHidden:FALSE];
        userNameImg.image = [UIImage imageNamed:@"tick.png"];
    }
    if([password.text length]<7)
    {
        
        [passwordImg setHidden:FALSE];
        passwordImg.image = [UIImage imageNamed:@"error.png"];
    }
    else
    {
        [passwordImg setHidden:FALSE];
        passwordImg.image = [UIImage imageNamed:@"tick.png"];
    }
    
    if([contact.text length]<10)
    {
        [contactImg setHidden:FALSE];
        contactImg.image = [UIImage imageNamed:@"error.png"];
        
    }
    else{
        [contactImg setHidden:FALSE];
        contactImg.image = [UIImage imageNamed:@"tick.png"];
    }
    if(!([self validateEmailWithString:email.text]))
    {
        [emailImg setHidden:FALSE];
        emailImg.image = [UIImage imageNamed:@"error.png"];
    }
    else
    {
        [emailImg setHidden:FALSE];
        emailImg.image = [UIImage imageNamed:@"tick.png"];
    }
    
    return YES;
}
- (BOOL)validateEmailWithString:(NSString*)emailId
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailId];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"touchesBegan:withEvent:");
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    NSLog(@"textViewShouldBeginEditing:");
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    NSLog(@"textViewDidBeginEditing:");
    NSLog(@"text in tex view %@",statusView.text);

        statusView.text=@"";
    
    
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    NSLog(@"textViewShouldEndEditing:");
    return YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"textViewDidEndEditing:");
    NSMutableAttributedString * string;
    NSArray *words=[textView.text componentsSeparatedByString:@" "];
    /*if([words count]>50)
     {
     alertLimit =[[UIAlertView alloc]initWithTitle:@"Error" message:@"Characters are more than 50" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
     [alertLimit show];
     
     }*/
    
    for (NSString *word in words) {
        if (([word hasPrefix:@"#"])) {
            // NSString *newWord = [word substringFromIndex:[@"@" length]];
            // NSString *newWord=[word stringByReplacingOccurrencesOfString:@"@" withString:@""];
            NSRange range=[textView.text rangeOfString:word];
            NSString *newString=[textView.text stringByReplacingOccurrencesOfString:@"#" withString:@""];
            string = [[NSMutableAttributedString alloc]initWithString:newString];
            // [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
            NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1],NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:13],
                                         NSBackgroundColorAttributeName: [UIColor whiteColor]
                                         };
            [string addAttributes:attributes range:range];
            [textView setAttributedText:string];
            
            
            
        }
    }
    
    double delayInSeconds = 4.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //[self circleData]; // 1
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            [self statusUpload];// 2
        });
    });

    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) { // means share button pressed
        [self viewDidLoad];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSCharacterSet *doneButtonCharacterSet = [NSCharacterSet newlineCharacterSet];
    NSRange replacementTextRange = [text rangeOfCharacterFromSet:doneButtonCharacterSet];
    NSUInteger location = replacementTextRange.location;
    if (statusView.text.length + text.length > 100){
        if (location != NSNotFound){
            [statusView resignFirstResponder];
        }
        return NO;
    }
    else if (location != NSNotFound){
        [statusView resignFirstResponder];
        return NO;
    }
    return YES;
}
-(void)textViewDidChange:(UITextView *)textView{
    NSLog(@"textViewDidChange:");
}
- (void)textViewDidChangeSelection:(UITextView *)textView{
    NSLog(@"textViewDidChangeSelection:");
}

-(IBAction)goLogin
{  /* if(flag==1)
    {
    [self upload];
    }*/
    // [self statusUpload];
    UIImage *dummyImge=[UIImage imageNamed:@"editPic.png"];
    profileImage.image=dummyImge;
    if([idAtSettings isEqualToString:@"circle"])
    {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:0];
        
    }
    if([idAtSettings isEqualToString:@"profile"])
    {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
        
    }
    if([idAtSettings isEqualToString:@"upload"])
    {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:2];
        
    }
    if([idAtSettings isEqualToString:@"settings"])
    {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
        
    }
    
}
-(IBAction)logout
{
     [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"loggedIN"];
    
    [self performSegueWithIdentifier:@"settingsTologin" sender:self];
    
}
-(void)changeFirstName
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate.mynameLogin isEqualToString:userName.text]) {
        
    }
    else{
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/changesetting.do?username=%@&column_name=name_first&newname=%@",username,dpName.text];
        NSLog(@"Url for change username %@",url);
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSData *data=[NSData dataWithContentsOfURL:urlrequest];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        int length=[json count];
        NSLog(@"Objects in JSON %i",length);
        NSLog(@"JSON format:- %@",json);
        NSString *str=[json valueForKey:@"param"];
        NSLog(@"Revert of json for change username %@",str);
    }
    
}

-(void)changeLastName
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate.mynameLogin isEqualToString:userName.text]) {
        
    }
    else{
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/changesetting.do?username=%@&column_name=name_last&newname=%@",username,userName.text];
        NSLog(@"Url for change username %@",url);
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSData *data=[NSData dataWithContentsOfURL:urlrequest];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        int length=[json count];
        NSLog(@"Objects in JSON %i",length);
        NSLog(@"JSON format:- %@",json);
        NSString *str=[json valueForKey:@"param"];
        NSLog(@"Revert of json for change username %@",str);
    }
    
}
-(void)changeEmail
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate.emailId isEqualToString:email.text]) {
        
    }
    else{
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/changesetting.do?username=%@&column_name=email_id&newname=%@",username,email.text];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSData *data=[NSData dataWithContentsOfURL:urlrequest];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        int length=[json count];
        NSLog(@"Objects in JSON %i",length);
        NSLog(@"JSON format:- %@",json);
        NSString *str=[json valueForKey:@"param"];
        NSLog(@"Revert of json %@",str);
    }
    
}
-(void)changeNumber
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate.contact isEqualToString:contact.text]) {
        
    }
    else{
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/changesetting.do?username=%@&column_name=phone&newname=%@",username,contact.text];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSData *data=[NSData dataWithContentsOfURL:urlrequest];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        int length=[json count];
        NSLog(@"Objects in JSON %i",length);
        NSLog(@"JSON format:- %@",json);
        NSString *str=[json valueForKey:@"param"];
        NSLog(@"Revert of json %@",str);
    }
    
}
-(void)changePassword
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate.myPassword isEqualToString:password.text]) {
        
    }
    else{
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/changesetting.do?username=%@&column_name=password&newname=%@",username,password.text];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSData *data=[NSData dataWithContentsOfURL:urlrequest];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        int length=[json count];
        NSLog(@"Objects in JSON %i",length);
        NSLog(@"JSON format:- %@",json);
        NSString *str=[json valueForKey:@"param"];
        NSLog(@"Revert of json %@",str);
    }
    
}
-(void)selectImage
{
    
    UIActionSheet *actionSheet;
    actionSheet=[[[UIActionSheet alloc] initWithTitle:@" Set your profile image"
                                             delegate:nil
                                    cancelButtonTitle:@"Cancel"
                               destructiveButtonTitle:@"Capture with Camera"
                                    otherButtonTitles:@"Choose from Gallery",nil] autorelease];
    
    actionSheet.delegate =self;
    //[actionSheet showInView:self.view];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
    
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == [actionSheet destructiveButtonIndex]) {
        NSLog(@"Button index %i",buttonIndex);
        NSLog(@"Destroy");
        UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
        pickerView.allowsEditing = YES;
        pickerView.delegate = self;
        pickerView.sourceType=UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:pickerView animated:YES completion:nil];
    } else if(buttonIndex == 1){
        UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
        pickerView.allowsEditing = YES;
        pickerView.delegate = self;
        pickerView.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:pickerView animated:YES completion:nil];
    }
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    flag=1;
    [self dismissViewControllerAnimated:YES completion:nil];
    
    UIImage * img = [info valueForKey:UIImagePickerControllerEditedImage];
    //NSLog(@"image nme********* %@",img);
    profileImage.image = img;
    dataImage = [[NSData alloc]init];
    dataImage= UIImageJPEGRepresentation(profileImage.image, 90);
    if(flag==1)
    {
        //[self upload];
        
        [self performSelectorOnMainThread:@selector(upload) withObject:nil waitUntilDone:NO];
    }
    
}

-(void)upload{
   /*
    self.indiView.blurRadius = 0;
    [self.view addSubview:self.indiView];
    
    
   
    [self blurviewIn];
     [indicator startAnimating];
   */
 
   
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^ {
                         self.indiView.alpha = 1.0f;
                     }
                     completion:^(BOOL finished) {
                         [self blurViewIn];
                         
                         
                     }];
       
       [self performSelector:@selector(indicatorImage) withObject:nil afterDelay:0.1];
    
  

}

-(void) blurViewIn {
    [UIView animateWithDuration:0.5 animations:^{
        self.indiView.blurRadius = 20;
        indicator.alpha = 1.0f;
    }];
}

-(void) removeBlurView {
    [UIView animateWithDuration:0.5 animations:^{
        self.indiView.blurRadius = 0;
    }];
    [self removeBlurViewAlpha];
    
    
    
    
    
    
}
-(void) removeActiView {
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^ {
                         indicator.alpha = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         [self removeBlurView];
                         
                         
                     }];
    
    
}
-(void) removeBlurViewAlpha {
    [UIView animateWithDuration:0.5 animations:^{
        self.indiView.alpha = 0.0f;
    }];
    
}

-(void)indicatorImage
{
    
    NSLog(@"helllooooooooo");
    NSData *imageData = UIImageJPEGRepresentation(profileImage.image, 90);
    
    NSLog(@"just testing ,,,,*******");
    // NSString *urlString = @"http://112.196.7.94/cancer_circle/upImageFile.jsp";
    NSString *urlString=SERVER_URL@"/profileEdit.do?";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    [request setValue:@"300"       forHTTPHeaderField:@"Keep-Alive"];
    [request setValue:@"keep-live" forHTTPHeaderField:@"Connection"];
    
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *postBody = [NSMutableData data];
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // NSString *userStatus=[[username stringByAppendingFormat:@","] stringByAppendingFormat:statusUpdate];
    
    NSString *str=[[@"Content-Disposition: form-data;name=\"" stringByAppendingString:username] stringByAppendingString:@",iphone\";filename=\"profile.png\"\r\n"];
    
    NSLog(@"*******%@",str);
    [postBody appendData:[[NSString stringWithString:str] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:imageData];
    [postBody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:postBody];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"yepieeeeeeeeee %@",returnString);
    // NSData *data = [NSData dataWithContentsOfURL:urlrequest];
    NSLog(@"values in datafordistance is:- %@",returnData);
    NSURL *urlrequest=[NSURL URLWithString:urlString];
    NSData *data = [NSData dataWithContentsOfURL:urlrequest];
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
    NSLog(@"JSON format:- %@",json);
    NSString  *authStr = [json objectForKey:@"message"];
    NSLog(@"valu in elemant is:- %@",authStr);
    if([authStr isEqualToString:@"success"])
    {
        NSString *dpUrl=[json valueForKey:@"link"];
        NSLog(@"valu in dpUrl is:- %@",dpUrl);
        NSURL *upPic=[NSURL URLWithString:dpUrl];
        NSData *picData=[NSData dataWithContentsOfURL:upPic];
        AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
        delegate.profilePicData=picData;
        Login *login=[[Login alloc]init];
        [login circleData];
        [self removeActiView];

        
    }
    else
    {
        alert =[[UIAlertView alloc]initWithTitle:@"Failed" message:@"Try agian" delegate:self cancelButtonTitle:@"ok"otherButtonTitles:nil];
        [alert show];
    }
    
    
}
/*-(void)statusUpload{
 indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];//UIActivityIndicatorViewStyleWhiteLarge
 indicator.frame = CGRectMake(self.view.bounds.size.width / 2.0f - indicator.frame.size.width /2.0f, self.view.bounds.size.height / 2.0f - indicator.frame.size.height /2.0f, indicator.frame.size.width, indicator.frame.size.height);
 //indicator.color = [UIColor colorWithRed:1 green:0.8 blue:0.9 alpha:1 ];
 indicator.color = [UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
 CGAffineTransform transform = CGAffineTransformMakeScale(2.2f, 2.2f);
 indicator.transform = transform;
 [indicator startAnimating];
 [self.view addSubview:indicator];
 [self performSelector:@selector(indicatorStatus) withObject:nil afterDelay:0.1];
 }*/
-(void)statusUpload
{
    /*if (([statusView.text isEqualToString:@"Modify your profile status with not more than 100 characters"])||([statusView.text isEqualToString:@""])) {
     //statusView.textColor=[UIColor blackColor];
     //statusView.text=@"";
     }*/
    if (([statusView.text isEqualToString:profileStatus])||([statusView.text isEqualToString:@""])) {
        //statusView.textColor=[UIColor blackColor];
        //statusView.text=@"";
    }
    else{
        NSLog(@"Story %@",statusView.text);
        NSString *converted = [statusView.text stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        // NSString *urlToBeSearched = [NSString stringWithFormat:@"http://www.example.com/search/%@", converted];
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/status.do?status=%@&username=%@",converted,username];
        
        
        // NSString *url = [NSString stringWithFormat:@"http://192.168.10.53:8080/Cancer_final/story_upload.do?username=%@&story=%@",idName,converted];
        NSLog(@"URL String ************** %@",url);
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        NSData *data = [NSData dataWithContentsOfURL:urlrequest];
        NSLog(@"values in datafordistance is:- %@",data);
        
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        if (!json) {
            NSLog(@"this is not a json format");
        }
        NSLog(@"JSON format:- %@",json);
        NSString *authStr = [json objectForKey:@"param"];
        NSLog(@"valu in elemant is:- %@",authStr);
        if([authStr isEqualToString:@"True"])
        {
            AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
            delegate.status=statusView.text;
            
            
             // NSString *url = [NSString stringWithFormat:SERVER_URL@"/getLatestStatus.do?username=%@",delegate.mynameLogin];
         
                    [indicator stopAnimating];
            
        }
        
    }
    
}
-(void)updateInfo
{
    AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
    //UIImage *dummyImge=[UIImage imageNamed:@"editPic.png"];
    // profileImage.image=dummyImge;
    [self statusUpload];
    // if((userNameImg.image = [UIImage imageNamed:@"right.png"])&&([userName.text length]>0))
    if (![delegate.lastName isEqualToString:userName.text])     {
        NSLog(@"usename changed");
        [self performSelectorOnMainThread:@selector(changeLastName) withObject:nil waitUntilDone:NO];
    }
    //if((userNameImg.image = [UIImage imageNamed:@"right.png"])&&([dpName.text length]>0))
    if (![delegate.firstName isEqualToString:dpName.text])
    {
        NSLog(@"usename changed");
        [self performSelectorOnMainThread:@selector(changeFirstName) withObject:nil waitUntilDone:NO];
    }
    if (![delegate.myPassword isEqualToString:password.text])
        //if((passwordImg.image = [UIImage imageNamed:@"right.png"])&&([password.text length]>0))
    {
        [self performSelectorOnMainThread:@selector(changePassword) withObject:nil waitUntilDone:NO];
    }
    if (![delegate.contact isEqualToString:contact.text])
        //if((contactImg.image = [UIImage imageNamed:@"right.png"])&&([contact.text length]>0))
    {
        [self performSelectorOnMainThread:@selector(changeNumber) withObject:nil waitUntilDone:NO];
    }
    if (![delegate.emailId isEqualToString:email.text])
        //if((emailImg.image = [UIImage imageNamed:@"right.png"])&&([email.text length]>0))
    {
        [self performSelectorOnMainThread:@selector(changeEmail) withObject:nil waitUntilDone:NO];
    }
    
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //[self circleData]; // 1
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            [self updateInfo];// 2
        });
    });
    
    
}
-(IBAction)switchButtonLocation
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    
    if(switchToggle.on){
        //[switchToggle setOn:NO animated:YES];
        NSLog(@"ON");
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/contentLoc.do?username=%@&location=yes",username];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        NSData *data = [NSData dataWithContentsOfURL:urlrequest];
        
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        if (!json) {
            NSLog(@"this is not a json format");
        }
        NSLog(@"JSON format:- %@",json);
        NSString *authStr = [json objectForKey:@"param"];
        if([authStr isEqualToString:@"true"])
            
        {
            delegate.setLocation=@"yes";
            UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Request is accepted" message:@"Location add to content is activated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert1 show];
        }
    }
    else{
        //[switchToggle setOn:YES animated:YES];
        NSLog(@"OFF");
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/contentLoc.do?username=%@&location=no",username];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        NSData *data = [NSData dataWithContentsOfURL:urlrequest];
        
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        if (!json) {
            NSLog(@"this is not a json format");
        }
        NSLog(@"JSON format:- %@",json);
        NSString *authStr = [json objectForKey:@"param"];
        if([authStr isEqualToString:@"true"])
            
        {
            delegate.setLocation=@"no";
            
            UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Request is accepted" message:@"Location add to content is dectivated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert1 show];
        }
        
    }
    
}
-(IBAction)switchButtonEmail
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if(switchToggle2.on){
        //[switchToggle setOn:NO animated:YES];
        NSLog(@"ON");
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/email_notification.do?username=%@&e_notification=yes",username];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        NSData *data = [NSData dataWithContentsOfURL:urlrequest];
        
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        if (!json) {
            NSLog(@"this is not a json format");
        }
        NSLog(@"JSON format:- %@",json);
        NSString *authStr = [json objectForKey:@"param"];
        if([authStr isEqualToString:@"true"])
            
        {
            delegate.setEmailNotification=@"yes";
            UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Request is accepted" message:@"Email Notifications are activated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert1 show];
        }
    }
    else{
        //[switchToggle setOn:YES animated:YES];
        NSLog(@"OFF");
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/email_notification.do?username=%@&e_notification=no",username];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        NSData *data = [NSData dataWithContentsOfURL:urlrequest];
        
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        if (!json) {
            NSLog(@"this is not a json format");
        }
        NSLog(@"JSON format:- %@",json);
        NSString *authStr = [json objectForKey:@"param"];
        if([authStr isEqualToString:@"true"])
            
        {
            delegate.setEmailNotification=@"no";
            UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Request is accepted" message:@"Email Notifications are dectivated" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert1 show];
        }
        
    }
    
}
-(IBAction)switchButtonPrivate
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if(switchToggle3.on){
        //[switchToggle setOn:NO animated:YES];
        NSLog(@"ON");
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/privacy.do?username=%@&privacy=yes",username];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        NSData *data = [NSData dataWithContentsOfURL:urlrequest];
        
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        if (!json) {
            NSLog(@"this is not a json format");
        }
        NSLog(@"JSON format:- %@",json);
        NSString *authStr = [json objectForKey:@"param"];
        if([authStr isEqualToString:@"true"])
            
        {
            delegate.setPrivacy=@"yes";
            UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Request is accepted" message:@"Your content have been made private" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert1 show];
        }
    }
    else{
        //[switchToggle setOn:YES animated:YES];
        NSLog(@"OFF");
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/privacy.do?username=%@&privacy=no",username];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        NSData *data = [NSData dataWithContentsOfURL:urlrequest];
        
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        if (!json) {
            NSLog(@"this is not a json format");
        }
        NSLog(@"JSON format:- %@",json);
        NSString *authStr = [json objectForKey:@"param"];
        if([authStr isEqualToString:@"true"])
            
        {
            delegate.setPrivacy=@"no";
            UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Request is accepted" message:@"Your content have been made public" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert1 show];
        }
        
    }
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
    
}
- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 0){
        return 200;
    } else {
    return 50;
    }
}
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    
    static NSString *CellIdentifier = @"ProfCell";
    static NSString *CellIdentifier2 = @"AccCell";
    static NSString *CellIdentifier3 = @"ShareCell";
    static NSString *CellIdentifier4 = @"LNPCell";
    static NSString *CellIdentifier5 = @"logCell";
    
    settingsTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    [settingsTable setBackgroundColor: [[UIColor alloc] initWithRed:(218.0/255.0) green:(221.0/255.0) blue:(226.0/255.0) alpha:1.0]];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
    }
    if(indexPath.row == 0){
        
        self.ProfCell = [tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!self.ProfCell)
        {
            self.ProfCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            
            
            
            
            self.ProfCell.textLabel.text = @"";
            self.ProfCell.backgroundColor = [UIColor clearColor];
            self.ProfCell.contentView.backgroundColor = [UIColor clearColor];
            self.statusBack = [[UIImageView alloc] initWithFrame:CGRectMake(-2, 140, self.view.bounds
                                                                            .size.width+2, 60)];
            [self.statusBack.layer setBorderWidth:0.5f];
            [self.statusBack.layer setBorderColor:settingsTable.separatorColor.CGColor];
            
            [self.statusBack setBackgroundColor:[[UIColor alloc] initWithRed:(255.0/255.0) green:(255.0/255.0) blue:(255.0/255.0)alpha:1.0]];
            profileImage = [[UIImageView alloc] initWithFrame:CGRectMake(98, 5, 125, 125)];
            imageButton = [[UIButton alloc] initWithFrame:CGRectMake(98, 43, 125, 49)];
            imageButton.titleLabel.text = @"Change profile image";
            [imageButton setTitle:@"Change profile image" forState:UIControlStateNormal];
            imageButton.titleLabel.font = [UIFont systemFontOfSize:10];
            imageButton.titleLabel.numberOfLines = 2;
            [imageButton addTarget:self action:@selector(selectImage) forControlEvents:UIControlEventTouchDown];
            
            statusView = [[UITextView alloc] initWithFrame:CGRectMake(10, 140, 300, 60)];
            statusView.backgroundColor = [UIColor clearColor];
            statusView.delegate = self;
            
            AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
            profileStatus=delegate.status;
            
            profileImage.image=[UIImage imageWithData:delegate.profilePicData];
            
            [self.ProfCell.contentView addSubview:self.statusBack];
            [self.ProfCell.contentView addSubview:profileImage];
            [self.ProfCell.contentView addSubview:imageButton];
            [self.ProfCell.contentView addSubview:statusView];
            
            statusView.text=delegate.status;
            
            
            
        }
        
        
        return self.ProfCell;
    }
    else if(indexPath.row ==  1){
        
        self.AccCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (!self.AccCell)
        {
            self.AccCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            self.AccCell.textLabel.text = @"Account Information";
            self.AccCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                   }
        
        return self.AccCell;
    }
    else if(indexPath.row ==  2){
        
        self.ShareCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier3];
        if (!self.ShareCell)
        {
            self.ShareCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];

            self.ShareCell.textLabel.text = @"Social Sharing Accounts";
            self.ShareCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

            
        }
        
        return self.ShareCell;
    }
    else if(indexPath.row ==  3){
        
        self.LNPCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
        if (!self.LNPCell)
        {
            self.LNPCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            self.LNPCell.textLabel.text = @"Location, Notifications & Privacy";
            self.LNPCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }
        
        return self.LNPCell;
    }
    else if(indexPath.row ==  4){
        
        self.LogCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
        if (!self.LogCell)
        {
            self.LogCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            UILabel *log = [[UILabel alloc] initWithFrame:CGRectMake(110, 13, 100, 21)];
            log.textAlignment = NSTextAlignmentCenter;
            log.font = [UIFont systemFontOfSize:17];
            log.textColor = [UIColor whiteColor];
            log.text = @"Logout";
            [self.LogCell.contentView addSubview:log];

            self.LogCell.backgroundColor = [UIColor colorWithRed:(225.0/255.0) green:(58.0/255.0) blue:(58.0/255.0) alpha:1.0];
            self.LogCell.contentView.backgroundColor =  [UIColor colorWithRed:(225.0/255.0) green:(58.0/255.0) blue:(58.0/255.0) alpha:1.0];
            
        }
        
        return self.LogCell;
    }

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 1)
    {
        [self performSegueWithIdentifier:@"AccountSettings" sender:self];
        
        
    }
    if (indexPath.row == 2)
    {
        [self performSegueWithIdentifier:@"shareSettings" sender:self];
        
        
    }
    if (indexPath.row == 3)
    {
        [self performSegueWithIdentifier:@"LNPSettings" sender:self];
        
        
    }
    if (indexPath.row == 4)
    {
        [self performSelector:@selector(logout) withObject:self];
        
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [dpNameImg release];
    [_textFields release];
    [_logoutButton release];
    [settingsTable release];
    [settingsTable release];
    [super dealloc];
}
- (IBAction)exit:(id)sender {
}
                             @end
