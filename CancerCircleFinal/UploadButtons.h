//
//  UploadButtons.h
//  CancerCircleFirst
//
//  Created by Raminder on 25/01/13.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MediaPlayer/MediaPlayer.h"
#import "MobileCoreServices/UTCoreTypes.h"
#import <CoreText/CTStringAttributes.h>
#import <CoreLocation/CoreLocation.h>
#import "FBViewControllerLogin.h"
#import "FbGraphResponse.h"
#import "FbGraphFile.h"
#import "FXBlurView.h"
#import "GPUImage.h"






@interface UploadButtons : UIViewController <UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource>
{
    
    
    BOOL flag;
    IBOutlet UIImageView *bgImage;
    UIActivityIndicatorView *indicator;
    IBOutlet UITextField *msgText;
    IBOutlet UITextView *storyView;
    UIAlertView *alert;
    NSString *authStr;
    NSString *getName;
    NSString *idName ;
   // ACAccount *twitterAccount;
    IBOutlet UIImageView *imageView2;
    UIButton *story;
    UIButton *camera;
    UIButton *gallery;
    
    NSString *buttonTitle;
    NSData *convertedData;
    NSData *videoConvertedData;
    NSString *extension;
    NSMutableArray *videourlarray;
    IBOutlet UIImageView *halfImage;
    IBOutlet UIScrollView *myScroll;
    NSString *idAtUpload;
    NSString *dpImage;
    NSString *PdataStr;
    NSString *dataStr;
    NSString *uploadedData;
    NSString *uploadedType;
    NSString *uploadedElapsed;
    NSString *uploadedInserted;
    NSString *uploadedSno;
    NSString *uploadedLocation;
    CLLocationManager *locationManager;
    FBViewControllerLogin *fbGraph;
    FbGraphFile *FBGraphF;
    int flagFb;
    BOOL noBorder;
    IBOutlet UIControl *addView;
    IBOutlet UITextView *imageText;
    
    IBOutlet UIView *buttonsShadowV;
    NSArray * fontColors;
    int flagTu;
    int flagFl;
    int flagTw;
    IBOutlet UIButton *fontsButton;
    IBOutlet UIButton *fbButton;
    IBOutlet UIButton *twButton;
    IBOutlet UIButton *flButton;
    IBOutlet UIButton *tuButton;
    IBOutlet UIButton *filtersbutton;
    
    IBOutlet UIButton *bordersbutton;
    IBOutlet UIImage *defaultImage;
    CGFloat _scrollViewContentOffsetYThreshold;
   
    UIImageView *circleRefresh;
    UIImageView *circleRefresh2;
    
    
    IBOutlet UIView *transView;
    NSString *facebookClientID;
    NSString *redirectUri;
    NSString *accessToken;
    IBOutlet UIWebView *webView;
    id callbackObject;
    SEL callbackSelector;
    UIImage *borderImage;
    GPUImagePicture *overlay;
    GPUImageUIElement *uiElementInput;
    IBOutlet UIImageView *borderView;
    IBOutlet UIImageView *dropArrow1;
    IBOutlet UIImageView *dropArrow2;
    IBOutlet UIImageView *dropArrow3;
    IBOutlet UIImageView *dropArrow4;
    
    IBOutlet UIView *saveView;
    
    CGRect textboxOriginal;
    CGRect buttonsOriginal;
    CGRect textbackOriginal;
    CGRect backOriginal;
    BOOL animating;

}
@property (retain, nonatomic) IBOutlet UIImageView *borderView;
@property (retain, nonatomic) IBOutlet UIButton *fbButton;

@property (retain, nonatomic) IBOutlet GPUImageView *imageView;
@property (retain, nonatomic) IBOutlet UIButton *captureVIdeo;

@property (retain, nonatomic) IBOutlet UIButton *captureImage;
@property (retain, nonatomic) IBOutlet UIButton *galleryImage;

@property (retain, nonatomic) IBOutlet UIButton *photo;
@property (retain, nonatomic) IBOutlet UIButton *video;
@property (nonatomic, retain) UIViewController *fbGraph;

@property (nonatomic, retain) UIImagePickerController *pickView;
@property (retain, nonatomic) IBOutlet UIButton *filtersbutton;
@property (retain, nonatomic) IBOutlet UIButton *textButton;
@property (retain, nonatomic) IBOutlet UIButton *fontsButton;
@property (retain, nonatomic) IBOutlet UIButton *bordersButton;

@property (strong, nonatomic) IBOutlet FXBlurView *blurAcView;

@property(nonatomic,retain)NSString *getUsrname;
-(IBAction)hidekeyboard:(id)sender;
- (IBAction)filtersbutton:(UIButton *)sender;
- (IBAction)fontsbutton:(UIButton *)sender;
@property (retain, nonatomic) IBOutlet UIButton *bordersbutton;
- (IBAction)bordersButton:(UIButton *)sender;
-(IBAction)goSettings;
-(IBAction)postStory:(id)sender;
-(IBAction)browseImage:(id)sender;
-(IBAction)captureImage:(id)sender;
-(IBAction)captureVideo:(id)sender;
- (IBAction)postOnFb:(UIButton *)sender;
-(IBAction)post;
-(IBAction)cancel;
-(IBAction)postOnFb;
-(IBAction)postOnTw: (UIButton *)sender;
-(IBAction)postToTumblr;
-(IBAction)postToFlicker;
- (IBAction)addText:(UIButton *)sender;
- (IBAction)enlargeTextBox:(UIButton *)sender;
- (IBAction)goNews: (UIButton *)sender;
@property (retain, nonatomic) IBOutlet UIButton *enlargeText;
@property (retain, nonatomic) IBOutlet UIView *buttonView;

@property (retain, nonatomic) IBOutlet UIImageView *textBack;
@property (retain, nonatomic) IBOutlet UIButton *filterbutton;
@property (retain, nonatomic) IBOutlet UIScrollView *filtersScrollview;
@property (retain, nonatomic) IBOutlet UIScrollView *bordersScrollview;
@property (retain, nonatomic) IBOutlet UIScrollView *fontsScrollview;
@property (retain, nonatomic) IBOutlet UIScrollView *colorsScrollview;


@property (retain, nonatomic) IBOutlet UIScrollView *buttonScroll;
@property(nonatomic, retain)NSURL *selectedVideoUrl;

@property (nonatomic, assign) CGFloat outputJPEGQuality;
@property (nonatomic, assign) CGSize requestedImageSize;
- (IBAction)scaleImage:(UIPinchGestureRecognizer *)recognizer;


////FB Token
@property (nonatomic, retain) NSString *facebookClientID;
@property (nonatomic, retain) NSString *redirectUri;
@property (nonatomic, retain) NSString *accessToken;
@property (assign) id callbackObject;
@property (assign) SEL callbackSelector;
- (void)authenticateUserWithCallbackObject:(id)anObject andSelector:(SEL)selector andExtendedPermissions:(NSString *)extended_permissions;
@end
