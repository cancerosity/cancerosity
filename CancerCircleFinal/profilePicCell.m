//
//  profilePicCell.m
//  CancerCircleFinal
//
//  Created by Raminder on 10/05/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import "profilePicCell.h"

@implementation profilePicCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (void) layoutSubviews
{
    [super layoutSubviews];
    self.imageView.frame = CGRectMake(5,5,40,40);
    self.textLabel.frame = CGRectMake(5,5,40,40);
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"newsCellBg.png"]];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
