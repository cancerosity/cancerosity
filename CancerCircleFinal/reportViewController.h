//
//  reportViewController.h
//  CancerCircleFirst
//
//  Created by Raminder on 09/04/13.
//
//

#import <UIKit/UIKit.h>

@interface reportViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *reports;
    IBOutlet UITableView *myTable;
}

-(IBAction)goBack;

@end
