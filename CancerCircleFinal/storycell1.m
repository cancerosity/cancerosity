//
//  storycell1.m
//  Cancerosity
//
//  Created by Grant Bevan on 2/12/2013.
//  Copyright (c) 2013 Raminder. All rights reserved.
//


#import "storycell1.h"

@implementation storycell1

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void) layoutSubviews
{
    [super layoutSubviews];
    
   
    //self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imgCellbg.png"]];
    self.backgroundView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@""]];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat x = [UIScreen mainScreen].bounds.origin.x;
     UIView *backgroundCellView = [[UIView alloc] initWithFrame:CGRectMake(x+10, 0, width-20, 160)];
    backgroundCellView.backgroundColor = [UIColor blackColor];
    self.backgroundView.frame = CGRectMake(x+10,0,width-20,160);
    self.backgroundView.backgroundColor = [UIColor blackColor];
    self.backgroundColor = [UIColor clearColor];
    self.backgroundView = backgroundCellView;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
