//
//  LNPSettings.m
//  Cancerosity
//
//  Created by Grant Bevan on 31/03/2014.
//  Copyright (c) 2014 Raminder. All rights reserved.
//

#import "LNPSettings.h"
#import "AppDelegate.h"

@interface LNPSettings ()

@end

@implementation LNPSettings

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    username=delegate.mynameLogin;
    toggleTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated {
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    username=delegate.mynameLogin;
  

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
    //  return 5;
    
}
- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    
    static NSString *CellIdentifier = @"locCell";
    static NSString *CellIdentifier2 = @"privCell";
    static NSString *CellIdentifier3 = @"emCell";
    static NSString *CellIdentifier4 = @"phCell";
    
    toggleTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    [toggleTable setBackgroundColor: [[UIColor alloc] initWithRed:(218.0/255.0) green:(221.0/255.0) blue:(226.0/255.0) alpha:1.0]];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
    }
    if(indexPath.row == 0){
        
        self.locCell = [tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!self.locCell)
        {
            
            
            self.locCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
            self.locCell.textLabel.text = @"Location";
            self.locCell.detailTextLabel.text = @"Add your location to content";
            
            switchLoc = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
            switchLoc.onTintColor = [UIColor grayColor];
            [switchLoc addTarget:self action:@selector(switchButtonLocation) forControlEvents:UIControlEventTouchUpInside];
            self.locCell.accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
            [self.locCell.accessoryView addSubview:switchLoc];
            self.locCell.accessoryView.backgroundColor = [UIColor clearColor];
            
            AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
            if([delegate.setLocation isEqualToString:@"yes"])
                
            {
                [switchLoc setOn:YES animated:YES];
            }
            else{
                [switchLoc setOn:NO animated:YES];
            }

            

            
        }
        
        
        return self.locCell;
    }
    else if(indexPath.row ==  1){
        
        self.privCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (!self.privCell)
        {
            self.privCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
            self.privCell.textLabel.text = @"Privacy";
            self.privCell.detailTextLabel.text = @"Make your content private";
            switchPriv = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
            switchPriv.onTintColor = [UIColor grayColor];
            [switchPriv addTarget:self action:@selector(switchButtonPrivate) forControlEvents:UIControlEventTouchUpInside];
            self.privCell.accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
            [self.privCell.accessoryView addSubview:switchPriv];
            self.privCell.accessoryView.backgroundColor = [UIColor clearColor];
            
            AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
            if([delegate.setPrivacy isEqualToString:@"yes"])
                
            {
                [switchPriv setOn:YES animated:YES];
            }
            else{
                [switchPriv setOn:NO animated:YES];
            }
            
        }
        
        return self.privCell;
    }
    else if(indexPath.row ==  2){
        
        self.emCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier3];
        if (!self.emCell)
        {
            self.emCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
            self.emCell.textLabel.text = @"Email Notification";
            self.emCell.detailTextLabel.text = @"Receive Email Notifications";
            switchEm = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
            switchEm.onTintColor = [UIColor grayColor];
            [switchEm addTarget:self action:@selector(switchButtonEmail) forControlEvents:UIControlEventTouchUpInside];
            self.emCell.accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
            [self.emCell.accessoryView addSubview:switchEm];
            self.emCell.accessoryView.backgroundColor = [UIColor clearColor];
            
            AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
            if([delegate.setEmailNotification isEqualToString:@"yes"])
                
            {
                [switchEm setOn:YES animated:YES];
            }
            else{
                [switchEm setOn:NO animated:YES];
            }

            
        }
        
        return self.emCell;
    }
    else if(indexPath.row ==  3){
        
        self.phCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
        if (!self.phCell)
        {
            self.phCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
            self.phCell.textLabel.text = @"Push Notifications";
            self.phCell.detailTextLabel.text = @"Receive Push Notifications";
            switchPh = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
            switchPh.onTintColor = [UIColor grayColor];
            self.phCell.accessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
            [self.phCell.accessoryView addSubview:switchPh];
            self.phCell.accessoryView.backgroundColor = [UIColor clearColor];
            
        }
        
        return self.phCell;
    }
    return cell;
}
-(IBAction)switchButtonLocation
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    
    if(switchLoc.on){
        //[switchToggle setOn:NO animated:YES];
        NSLog(@"ON");
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/contentLoc.do?username=%@&location=yes",username];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        NSData *data = [NSData dataWithContentsOfURL:urlrequest];
        
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        if (!json) {
            NSLog(@"this is not a json format");
        }
        NSLog(@"JSON format:- %@",json);
        NSString *authStr = [json objectForKey:@"param"];
        if([authStr isEqualToString:@"true"])
            
        {
            delegate.setLocation=@"yes";
            UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Location Services" message:@"Your Location will now be display on your content" delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
            [alert1 show];
        }
    }
    else{
        //[switchToggle setOn:YES animated:YES];
        NSLog(@"OFF");
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/contentLoc.do?username=%@&location=no",username];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        NSData *data = [NSData dataWithContentsOfURL:urlrequest];
        
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        if (!json) {
            NSLog(@"this is not a json format");
        }
        NSLog(@"JSON format:- %@",json);
        NSString *authStr = [json objectForKey:@"param"];
        if([authStr isEqualToString:@"true"])
            
        {
            delegate.setLocation=@"no";
            
            UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Location Services" message:@"Location display is now disabled" delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
            [alert1 show];
        }
        
    }
    
}
-(IBAction)switchButtonEmail
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if(switchEm.on){
        //[switchToggle setOn:NO animated:YES];
        NSLog(@"ON");
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/email_notification.do?username=%@&e_notification=yes",username];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        NSData *data = [NSData dataWithContentsOfURL:urlrequest];
        
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        if (!json) {
            NSLog(@"this is not a json format");
        }
        NSLog(@"JSON format:- %@",json);
        NSString *authStr = [json objectForKey:@"param"];
        if([authStr isEqualToString:@"true"])
            
        {
            delegate.setEmailNotification=@"yes";
            UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Email Notifications" message:@"You will now receive email notifications" delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
            
            [alert1 show];
        }
    }
    else{
        //[switchToggle setOn:YES animated:YES];
        NSLog(@"OFF");
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/email_notification.do?username=%@&e_notification=no",username];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        NSData *data = [NSData dataWithContentsOfURL:urlrequest];
        
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        if (!json) {
            NSLog(@"this is not a json format");
        }
        NSLog(@"JSON format:- %@",json);
        NSString *authStr = [json objectForKey:@"param"];
        if([authStr isEqualToString:@"true"])
            
        {
            delegate.setEmailNotification=@"no";
            UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Email Notifications" message:@"You will no longer recive email notifications" delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
            [alert1 show];
        }
        
    }
    
}
-(IBAction)switchButtonPrivate
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    if(switchPriv.on){
        //[switchToggle setOn:NO animated:YES];
        NSLog(@"ON");
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/privacy.do?username=%@&privacy=yes",username];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        NSData *data = [NSData dataWithContentsOfURL:urlrequest];
        
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        if (!json) {
            NSLog(@"this is not a json format");
        }
        NSLog(@"JSON format:- %@",json);
        NSString *authStr = [json objectForKey:@"param"];
        if([authStr isEqualToString:@"true"])
            
        {
            delegate.setPrivacy=@"yes";
            UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Privacy" message:@"Your content had been made private" delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
            
            [alert1 show];
        }
    }
    else{
        //[switchToggle setOn:YES animated:YES];
        NSLog(@"OFF");
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/privacy.do?username=%@&privacy=no",username];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        NSData *data = [NSData dataWithContentsOfURL:urlrequest];
        
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
        if (!json) {
            NSLog(@"this is not a json format");
        }
        NSLog(@"JSON format:- %@",json);
        NSString *authStr = [json objectForKey:@"param"];
        if([authStr isEqualToString:@"true"])
            
        {
            delegate.setPrivacy=@"no";
            UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Privacy" message:@"Your content have been made public" delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
            [alert1 show];
        }
        
    }
    
}


- (void)dealloc {
    [toggleTable release];
    [super dealloc];
}
@end
