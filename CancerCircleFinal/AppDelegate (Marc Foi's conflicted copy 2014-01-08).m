//
//  AppDelegate.m
//  CancerCircleFinal
//
//  Created by Raminder on 29/04/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import "AppDelegate.h"
#import <AWSRuntime/AWSRuntime.h>

@implementation AppDelegate
@synthesize window = _window;
@synthesize myname,mynameLogin,myPassword,flag,tabId,status,profileImage,profilePicData,emailId,contact,requestCount,activityArray,userArray;
@synthesize dataUpdate,snoArray,likearray,dislikearray,profielImages,imageDatas,dataType,likesNameArray,dislikesNameArray,differUser,commentNameArray,commentsArray,commentPicArray,commentCounts,locationArray,timeArray,setEmailNotification,insertedTimeArray,setPrivacy,fourCommentArray,privacyArray;
@synthesize PdataType,PdataUpdate,Pdislikearray,Plikearray,PsnoArray,followers,following,followersNames,PcommentCounts,PdislikesNameArray,PlikesNameArray,followingNames,PlocationArray,PtimeArray,PgetInertedTime,PfourCommentArray;

@synthesize imageCommentNames,imageComments,imageCommentsPics,imageCommentsType,wordToSearch;
@synthesize snoReport,userReport,typeReport,getInertedTime,getLastInertedTime,setLocation,SgetLastInseted;
@synthesize token,secondTabId,newsSerial,contentSTR;
@synthesize session = _session;

- (void)dealloc
{
    [_window release];
    [super dealloc];
}



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
   
    
    // Override point for customization after application launch.
    
    return YES;
    
}



- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation

{
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                        withSession:self.session];
    
    
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url

{
    
    return self;
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application

{
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
}



- (void)applicationWillEnterForeground:(UIApplication *)application

{
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
}



- (void)applicationDidBecomeActive:(UIApplication *)application

{
    
    [FBAppCall handleDidBecomeActiveWithSession:self.session];
    
  
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
}



- (void)applicationWillTerminate:(UIApplication *)application

{
    
    
  
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}



							




@end
