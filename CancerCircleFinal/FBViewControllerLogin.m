//
//  FBViewController.m
//  CancerCircleFinal
//
//  Created by Raminder on 07/05/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import "FBViewControllerLogin.h"
#import "Login.h"
#import "AppDelegate.h"
#import "FbGraphFile.h"

@interface FBViewControllerLogin ()

@end

@implementation FBViewControllerLogin
@synthesize facebookClientID;
@synthesize redirectUri;
@synthesize accessToken;
//@synthesize webView;

@synthesize callbackObject;
@synthesize callbackSelector;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"640x960bg.png"]];
    UINavigationBar *navBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    
   
    navBar.barTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    navBar.translucent = YES;
    
 
    CGFloat titleWidth2=100;
    CGFloat titleWidth = MIN(titleWidth2, 300);
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 30)];
    label.backgroundColor = [UIColor clearColor];
    // label.font = [UIFont boldSystemFontOfSize:20.0];
    label.font=[UIFont fontWithName:@"Futura_Light" size:20];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=@"Facebook Login";
    
    
   
    UIImage* image3 = [UIImage imageNamed:@"close.png"];
    CGRect frameimg = CGRectMake(0, 0,18,18);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(goLogin)
         forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *backbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
 
    [activity setHidden:NO];
    
    UINavigationItem *buttonCarrier = [[UINavigationItem alloc]initWithTitle:@""];
     buttonCarrier.titleView = label;
    //Creating some buttons:
   

    
  
    
    //Putting the Buttons on the Carrier
    [buttonCarrier setLeftBarButtonItem:backbutton];
   
    
    //The NavigationBar accepts those "Carrier" (UINavigationItem) inside an Array
    NSArray *barItemArray = [[NSArray alloc]initWithObjects:buttonCarrier,nil];
    
    // Attaching the Array to the NavigationBar
    [navBar setItems:barItemArray];
     [someButton release];
    [self.view addSubview:navBar];
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"640x960bg.png"]];
	// Do any additional setup after loading the view.
[self authenticateUserWithCallbackObject:self andSelector:@selector(fbGraphCallback:)                      andExtendedPermissions:@"user_photos,user_videos,publish_stream,offline_access,user_checkins,friends_checkins,email,user_birthday"];
}
-(IBAction)goLogin
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    /*       Login *login=[self.storyboard instantiateViewControllerWithIdentifier:@"login"];
     [[self navigationController] setNavigationBarHidden:YES animated:YES];
     [self.navigationController pushViewController:login animated:YES];*/
}
-(IBAction)goTab
{
    self.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
    
}

- (void)fbGraphCallback:(id)sender {
    [activity setHidden:NO];
       NSLog(@"Send to next screen");
	AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
    Login *login=[[Login alloc]init];
   
    
	if ( (accessToken == nil) || ([accessToken length] == 0) ) {
      
		
		NSLog(@"You pressed the 'cancel' or 'Dont Allow' button, you are NOT logged into Facebook...I require you to be logged in & approve access before you can do anything useful....");
		
		//restart the authentication process.....
		[self authenticateUserWithCallbackObject:self andSelector:@selector(fbGraphCallback:)
                          andExtendedPermissions:@"user_photos,user_videos,publish_stream,offline_access,user_checkins,friends_checkins"];
		
	} else {
        
        NSLog(@"--->CONGRATULATIONS<----, You're logged into Facebook...  Your oAuth token is:  %@",accessToken);
          [activity setHidden:NO];
        
        FbGraphResponse *fb_graph_response = [self doGraphGet:@"me" withGetVars:nil];
        NSLog(@"getMeButtonPressed:  %@", fb_graph_response.htmlResponse);
        NSData* data=[fb_graph_response.htmlResponse dataUsingEncoding: [NSString defaultCStringEncoding] ];
        NSError *error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data //1
                                                             options:kNilOptions
                                                               error:&error];
        NSLog(@"########### %@",json);            
        NSLog(@"Verified ===== %@",[json valueForKey:@"verified"]);
        NSLog(@"Birthday ===== %@",[json valueForKey:@"birthday"]);
        NSLog(@"First Name === %@",[json valueForKey:@"first_name"]);
        NSLog(@"Last Name ==== %@",[json valueForKey:@"last_name"]);
        NSLog(@"Email ======== %@",[json valueForKey:@"email"]);
        NSLog(@"Name ========= %@",[json valueForKey:@"name"]);
        NSLog(@"Username ===== %@",[json valueForKey:@"username"]);
        NSLog(@"Gender ======= %@",[json valueForKey:@"gender"]);
        NSString *fbPictureURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture", [json objectForKey:@"id"]];      
    NSArray   *dateArray=[[json valueForKey:@"birthday"] componentsSeparatedByString:@"/"];
    NSString *url = [NSString stringWithFormat:SERVER_URL@"/register.do?first=%@&last=%@&day=%@&month=%@&year=%@&email=%@&phone=&name=%@&pswd=Facebook",[json valueForKey:@"first_name"],[json valueForKey:@"last_name"],[dateArray objectAtIndex:0],[dateArray objectAtIndex:1],[dateArray objectAtIndex:2],[json valueForKey:@"email"],[json valueForKey:@"username"]];
        NSLog(@"String url is $$$$$$$$$$$$$$$$:- %@",url);
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        NSData *regdata = [NSData dataWithContentsOfURL:urlrequest];
       
        NSDictionary* regJson = [NSJSONSerialization JSONObjectWithData:regdata options:kNilOptions                        error:&error];
        NSLog(@"JSON format:- %@",regJson);
        delegate.mynameLogin=[json valueForKey:@"username"];
       // NSString *keyOfName=[ delegate.mynameLogin stringByAppendingString:@"outhName"];
        NSUserDefaults *outhName=[NSUserDefaults standardUserDefaults];
        [outhName setObject:[json valueForKey:@"username"] forKey:@"outhName"];
        
        NSUserDefaults *loginName=[NSUserDefaults standardUserDefaults];
        [loginName setObject:delegate.mynameLogin forKey:@"loginName"];
        
         delegate.myPassword=@"Facebook";
        delegate.displayName=[json valueForKey:@"name"];
        NSArray *nameInfo=[[json objectForKey:@"name"] componentsSeparatedByString:@" "];
        delegate.firstName=[nameInfo objectAtIndex:0];
        delegate.lastName=[nameInfo objectAtIndex:1];
       delegate.profileImage=fbPictureURL;
        delegate.status=@"Please Update Your Status";
       NSData *imageDataUp = [NSData dataWithContentsOfURL:[NSURL URLWithString:fbPictureURL]];
      delegate.profilePicData=imageDataUp;
        delegate.emailId = [json valueForKey:@"email"];
         NSLog(@"email is ====== %@",delegate.emailId);
        NSString *msg=[regJson objectForKey:@"message"];
        NSString *param=[regJson objectForKey:@"param"];
        if([msg isEqualToString:@"You are already registered user "])
        {
            
            [login circleData];
            [login profileData];
            
            self.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
            
        }       
        if([param isEqualToString:@"True"])
        {
             [activity startAnimating];
            NSString *picUpdate=[NSString stringWithFormat:SERVER_URL@"/ProfilePicUpdate.do?username=%@&profilePicLink=%@",delegate.mynameLogin,fbPictureURL];
            NSURL *picUrl=[NSURL URLWithString:picUpdate];
            NSData *picData=[NSData dataWithContentsOfURL:picUrl];
            NSDictionary* picJson = [NSJSONSerialization JSONObjectWithData:picData options:kNilOptions error:&error];
            NSLog(@"JSON format:- %@",picJson);
            [login circleData];
            [login profileData];
            self.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
        }
       
        NSString *urlInfo = [NSString stringWithFormat:SERVER_URL@"/userinfo.do?username=%@",delegate.mynameLogin];
        NSURL *urlrequestInfo=[NSURL URLWithString:urlInfo];
        NSData *dataofInfo=[NSData dataWithContentsOfURL:urlrequestInfo];
        
        NSDictionary* jsonInfo = [NSJSONSerialization JSONObjectWithData:dataofInfo options:kNilOptions error:&error];
        NSLog(@"jsonInfo %@",jsonInfo);
        delegate.setLocation=[jsonInfo objectForKey:@"location"];
        delegate.setEmailNotification=[jsonInfo objectForKey:@"email_notification"];
        delegate.setPrivacy=[jsonInfo objectForKey:@"privacy"];
        //delegate.emailId=[jsonInfo objectForKey:@"email"];
        
        delegate.contact=[jsonInfo objectForKey:@"phone"];

        
	}
	
}

- (void)authenticateUserWithCallbackObject:(id)anObject andSelector:(SEL)selector andExtendedPermissions:(NSString *)extended_permissions andSuperView:(UIView *)super_view {
    [activity setHidden:NO];
	self.callbackObject = anObject;
	self.callbackSelector = selector;
    
	facebookClientID = @"366546850134442";
    redirectUri = @"https://www.facebook.com/connect/login_success.html";
	NSString *url_string = [NSString stringWithFormat:@"https://graph.facebook.com/oauth/authorize?client_id=%@&redirect_uri=%@&scope=%@&type=user_agent&display=touch", facebookClientID, redirectUri, extended_permissions];
    NSLog(@"url_string FACEBOOk %@",url_string);
	NSURL *url = [NSURL URLWithString:url_string];
	NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webView loadRequest:request];
    
}

-(void)authenticateUserWithCallbackObject:(id)anObject andSelector:(SEL)selector andExtendedPermissions:(NSString *)extended_permissions {
	
	UIWindow* window = [UIApplication sharedApplication].keyWindow;
	if (!window) {
		window = [[UIApplication sharedApplication].windows objectAtIndex:0];
	}
	[activity setHidden:YES];
	[self authenticateUserWithCallbackObject:anObject andSelector:selector andExtendedPermissions:extended_permissions andSuperView:window];
}

- (FbGraphResponse *)doGraphGet:(NSString *)action withGetVars:(NSDictionary *)get_vars {
	[activity setHidden:YES];
	NSString *url_string = [NSString stringWithFormat:@"https://graph.facebook.com/%@?", action];
	
	//tack on any get vars we have...
	if ( (get_vars != nil) && ([get_vars count] > 0) ) {
		
		NSEnumerator *enumerator = [get_vars keyEnumerator];
		NSString *key;
		NSString *value;
		while ((key = (NSString *)[enumerator nextObject])) {
			
			value = (NSString *)[get_vars objectForKey:key];
			url_string = [NSString stringWithFormat:@"%@%@=%@&", url_string, key, value];
			
		}//end while
	}//end if
	
	if (accessToken != nil) {
         [activity setHidden:NO];
		//now that any variables have been appended, let's attach the access token....
		url_string = [NSString stringWithFormat:@"%@access_token=%@", url_string, self.accessToken];
	}
	
	//encode the string
	url_string = [url_string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	NSLog(@"URL STRING ME %@",url_string);
	return [self doGraphGetWithUrlString:url_string];
}
- (FbGraphResponse *)doGraphGetWithUrlString:(NSString *)url_string {
	
	FbGraphResponse *return_value = [[[FbGraphResponse alloc] init] autorelease];
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url_string]];
	
	NSError *err;
	NSURLResponse *resp;
	NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:&resp error:&err];
	
	if (resp != nil) {
		
		/**
		 * In the case we request a picture (avatar) the Graph API will return to us the actual image
		 * bits versus a url to the image.....
		 **/
		if ([resp.MIMEType isEqualToString:@"image/jpeg"]) {
			
			UIImage *image = [UIImage imageWithData:response];
			return_value.imageResponse = image;
			
		} else {
		    
			NSString *stringResponse = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
			return_value.htmlResponse = stringResponse;
			[stringResponse release];
		}
		
	} else if (err != nil) {
		return_value.error = err;
	}
	
	return return_value;
	
}

- (void)webViewDidFinishLoad:(UIWebView *)_webView {
	
	/**
	 * Since there's some server side redirecting involved, this method/function will be called several times
	 * we're only interested when we see a url like:  http://www.facebook.com/connect/login_success.html#access_token=..........
	 */
	
	//get the url string
    [activity setHidden:YES];
	NSString *url_string = [((_webView.request).URL) absoluteString];
	
	//looking for "access_token="
	NSRange access_token_range = [url_string rangeOfString:@"access_token="];
	
	//looking for "error_reason=user_denied"
	NSRange cancel_range = [url_string rangeOfString:@"error_reason=user_denied"];
	
	//it exists?  coolio, we have a token, now let's parse it out....
	if (access_token_range.length > 0) {
        [activity setHidden:NO];
		
		//we want everything after the 'access_token=' thus the position where it starts + it's length
		int from_index = access_token_range.location + access_token_range.length;
		NSString *access_token = [url_string substringFromIndex:from_index];
		
		//finally we have to url decode the access token
		access_token = [access_token stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		//remove everything '&' (inclusive) onward...
		NSRange period_range = [access_token rangeOfString:@"&"];
		
		//move beyond the .
		access_token = [access_token substringToIndex:period_range.location];
		
		//store our request token....
		self.accessToken = access_token;
        AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
        delegate.token=access_token;
        NSUserDefaults *outhToken=[NSUserDefaults standardUserDefaults];
        //NSString *keyOfOuth=[ delegate.mynameLogin stringByAppendingString:@"outhToken"];
        [outhToken setObject:access_token forKey:@"outhToken"];
        self->webView.hidden=TRUE;
        
       
        
		//remove our window
		UIWindow* window = [UIApplication sharedApplication].keyWindow;
		if (!window) {
			window = [[UIApplication sharedApplication].windows objectAtIndex:0];
		}
		
		//[self.webView removeFromSuperview];
		
		//tell our callback function that we're done logging in :)
		if ( (callbackObject != nil) && (callbackSelector != nil) ) {
			[callbackObject performSelector:callbackSelector];
            [activity setHidden:NO];
		}
		
		//the user pressed cancel
	} else if (cancel_range.length > 0) {
		//remove our window
		UIWindow* window = [UIApplication sharedApplication].keyWindow;
		if (!window) {
			window = [[UIApplication sharedApplication].windows objectAtIndex:0];
		}
		
		//[self.webView removeFromSuperview];
		
		//tell our callback function that we're done logging in :)
		if ( (callbackObject != nil) && (callbackSelector != nil) ) {
			[callbackObject performSelector:callbackSelector];
            
		}
		
	}
}
-(void) dealloc {
	
	[facebookClientID release];
	[redirectUri release];
	[accessToken release];
	[webView release];
    webView.delegate  = nil;
    [super dealloc];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
