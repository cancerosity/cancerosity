//
//  UploadButtons.m
//  CancerCircleFirst
//
//  Created by Raminder on 25/01/13.
//
//

#import "UploadButtons.h"
#import "Login.h"
#import "SettingButtons.h"
#import "AppDelegate.h"
//#import "ProfileFirst.h"
#import <Social/Social.h>
#import "SBJSON.h"
#import "FbGraphFile.h"
#import "FXBlurView.h"
#import <QuartzCore/QuartzCore.h>
#import "ILTranslucentView.h"
#import "getFbTokenViewController.h"
#import "DLCGrayscaleContrastFilter.h"
#import <AssetsLibrary/AssetsLibrary.h>

#define viewposition CGPointMake(160, 142)
@interface UploadButtons ()
@property (nonatomic, strong) IBOutlet FXBlurView *blurView;
@end

@implementation UploadButtons
@synthesize filtersbutton, filterbutton;
@synthesize fbGraph;

@synthesize facebookClientID;
@synthesize redirectUri;
@synthesize accessToken;
//@synthesize webView;

@synthesize callbackObject;
@synthesize callbackSelector;
FXBlurView *blurView;
GPUImageStillCamera *stillCamera;
GPUImageOutput<GPUImageInput> *filter;
GPUImageCropFilter *cropFilter;
UIImageOrientation staticPictureOriginalOrientation;
GPUImagePicture *staticPicture;
BOOL isStatic;
BOOL hasBlur;
int selectedFilter;
@synthesize getUsrname;

-(void)viewWillAppear:(BOOL)animated
{
    
}
-(void)viewDidAppear:(BOOL)animated
{
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    idAtUpload=delegate.tabId;
    NSLog(@"Id at upload %@",idAtUpload);
    delegate.tabId=@"upload";
    storyView.text=@"";
    [self viewDidLoad];
   
}
- (NSString *)deviceLocation {
    NSString *theLocation = [NSString stringWithFormat:@"latitude: %f longitude: %f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude];
    NSLog(@"the location %@",theLocation);
    
    return theLocation;
}
-(IBAction)postToTumblr
{
    if(flagTu==1)
    {
        [tuButton setBackgroundImage:[UIImage imageNamed:@"tumblr.png"] forState:UIControlStateNormal];
        flagTu=0;
    }
    else
    {
        
        [tuButton setBackgroundImage:[UIImage imageNamed:@"tumblrDark.png"] forState:UIControlStateNormal];
        
        NSUserDefaults *itemValue=[NSUserDefaults standardUserDefaults];
        [itemValue setObject:@"" forKey:@"typeValue"];
        
        flagTu=1;
    }
    
}
-(IBAction)postToFlicker
{
    if ((imageView2.image=nil)) {
        UIAlertView *alertImage=[[UIAlertView alloc]initWithTitle:@"Connect to Flickr failed" message:@"Select image" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertImage show];
    }
    else
    {
        if(flagFl==1)
        {
            [flButton setBackgroundImage:[UIImage imageNamed:@"twoDots.png"] forState:UIControlStateNormal];
            flagFl=0;
        }
        else
        {
            NSUserDefaults *flickrName=[NSUserDefaults standardUserDefaults];
            NSString *flUser=[flickrName objectForKey:@"flickrName"];
            NSLog(@"flickr user %@",flUser);
            if(flUser.length==0)
            {
                //NSData  *imageData = UIImageJPEGRepresentation(imageView.image, 90);
                //UIImage *image=[UIImage imageWithData:imageData];
                
                //flagFl=1;
                [flButton setBackgroundImage:[UIImage imageNamed:@"flickrDark.png"] forState:UIControlStateNormal];
            }
            else
            {
                //if ([storyView.text isEqualToString:@""]) {
                flagFl=1;
                [flButton setBackgroundImage:[UIImage imageNamed:@"flickrDark.png"] forState:UIControlStateNormal];
                //}
            }
        }
        
    }
}
-(void)postImageToFlickr
{
   // NSData  *imageData = UIImageJPEGRepresentation(imageView.image, 90);
    //UIImage *image=[UIImage imageWithData:imageData];
    
    
}
-(void)sendToTumblr
{
    
    NSUserDefaults *itemType=[NSUserDefaults standardUserDefaults];
    NSUserDefaults *itemValue=[NSUserDefaults standardUserDefaults];
    
    if (![storyView.text isEqualToString:@""]) {
        
        [itemType setObject:@"1" forKey:@"typeItem"];
        [itemValue setObject:storyView.text forKey:@"typeValue"];
    }
    else if( [extension isEqualToString:@"testVideo.mov"])
    {
        NSLog(@"selected url is: - %@",_selectedVideoUrl);
        //NSURL *url=[NSURL URLWithString:uploadedData];
        
        [itemType setObject:@"2" forKey:@"typeItem"];
        [itemValue setObject:uploadedData forKey:@"typeValue"];
    }
    else //if (!(imageView.image=nil))
    {
       NSData  *imageData = UIImageJPEGRepresentation(imageView2.image, 90);
       //UIImage *image=[UIImage imageWithData:imageData];
        
        [itemType setObject:@"3" forKey:@"typeItem"];
        [itemValue setObject:imageData forKey:@"typeValue"];
    }
    //[itemType setObject:@"SHKShareTypeURL" forKey:@"typeItem"];
    
}
- (void)viewDidLoad
{
    [self loadFilters];
    
  
       [self authenticateUserWithCallbackObject:self andSelector:@selector(fbGraphCallback:)                      andExtendedPermissions:@"user_photos,user_videos,publish_stream,offline_access,user_checkins,friends_checkins,email,user_birthday"];
    if (isStatic == NO) {
        _imageView.layer.borderColor = [[UIColor alloc] initWithRed:(203.0/255.0) green:(232.0/255.0) blue:(198.0/255.0) alpha:1.0].CGColor;
        _imageView.layer.borderWidth = 2;
    }
    else if (isStatic == YES) {
        _imageView.layer.borderColor = [UIColor clearColor].CGColor;
        _imageView.layer.borderWidth = 0;
    }
    
       [filtersbutton setTitleColor:[[UIColor alloc] initWithRed:(167.0/255.0) green:(205.0/255.0) blue:(164.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [_bordersButton setTitleColor:[[UIColor alloc] initWithRed:(167.0/255.0) green:(205.0/255.0) blue:(164.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [_textButton setTitleColor:[[UIColor alloc] initWithRed:(167.0/255.0) green:(205.0/255.0) blue:(164.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [_fontsButton setTitleColor:[[UIColor alloc] initWithRed:(167.0/255.0) green:(205.0/255.0) blue:(164.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    
    [storyView setReturnKeyType:UIReturnKeyDone];
    [msgText setReturnKeyType:UIReturnKeyDone];
    
    storyView.delegate = self;
    
    blurView = [[FXBlurView alloc] initWithFrame: CGRectMake (9, 325, 302, 107)];
    blurView.dynamic = YES;
    blurView.tintColor = [[UIColor alloc] initWithRed:(0.0/255.0) green:(255.0/255.0) blue:(0.0/255.0) alpha:1.0];
    [blurView.layer displayIfNeeded]; //force immediate redraw
    blurView.contentMode = UIViewContentModeBottom;
    
    [blurView setHidden:YES];
    
    [myScroll addSubview:blurView];
    myScroll.contentSize = CGSizeMake(320, 520);
    blurView.blurRadius = 30;
    
    blurView.opaque = YES;
  
    [blurView setBackgroundColor:[[UIColor alloc] initWithRed:(255.0/255.0) green:(255.0/255.0) blue:(255.0/255.0) alpha:1.0]];
    
    staticPictureOriginalOrientation = UIImageOrientationUp;
    
    transview.dynamic = YES;
    transview.tintColor = [[UIColor alloc] initWithRed:(0.0/255.0) green:(255.0/255.0) blue:(0.0/255.0) alpha:1.0];
    [transview.layer displayIfNeeded]; //force immediate redraw
    transview.contentMode = UIViewContentModeBottom;
    
    
    [transview setHidden:YES];
    [transview setAlpha:0.0];
    
    transview.blurRadius = 30;
    
    transview.opaque = NO;
    cropFilter = [[GPUImageCropFilter alloc] initWithCropRegion:CGRectMake(0.0f, 0.0f, 1.0f, 0.75f)];
    
    filter = [[GPUImageFilter alloc] init];
    
    [transview setBackgroundColor:[[UIColor alloc] initWithRed:(255.0/255.0) green:(255.0/255.0) blue:(255.0/255.0) alpha:1.0]];
    
    //NSString *client_id = @"151898678325186";
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [locationManager startUpdatingLocation];
    NSLog(@"Location  ********** %@", [self deviceLocation]);
    bgImage.backgroundColor = [UIColor whiteColor];
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"640x960bg.png"]];
    
    
    
    storyView.textAlignment = NSTextAlignmentLeft ;
    // [storyView becomeFirstResponder];
    //myScroll.contentSize=CGSizeMake(320,600);
    //myScroll.pagingEnabled=YES;
    // profielImage.image = [UIImage imageNamed:@"photo.png"];
    halfImage.layer.cornerRadius=10;
    halfImage.clipsToBounds = YES;
    //buttonTitle=@"story";
    
    storyView.font = [UIFont fontWithName:@"Myriadpro-Regular" size:13];
    msgText.font = [UIFont fontWithName:@"MyriadPro-Regular" size:10];
    
    AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
    //idName = delegate.myname;
    dpImage=delegate.profileImage;
    idName = delegate.mynameLogin;
    NSLog(@"GET on UPload %@",idName);
    getName=delegate.mynameLogin;
    NSLog(@"GET on UPload username %@",getName);
    
    CGFloat titleWidth2=100;
    CGFloat titleWidth = MIN(titleWidth2, 300);
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 20)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Futura_Light" size:20];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=@"Upload";
    UINavigationBar *navBar = [[self navigationController] navigationBar];
    
    navBar.barTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    
    
    
    
    //self.navigationItem.title =@"Sign Up";
    self.navigationItem.titleView = label;
    UIImage* image4 = [UIImage imageNamed:@"settingProfile2.png"];
    CGRect frameimg2 = CGRectMake(0, 0,18,18);
    UIButton *someButton2 = [[UIButton alloc] initWithFrame:frameimg2];
    [someButton2 setBackgroundImage:image4 forState:UIControlStateNormal];
    [someButton2 addTarget:self action:@selector(goSettings)
          forControlEvents:UIControlEventTouchUpInside];
    [someButton2 setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *backbutton2 =[[UIBarButtonItem alloc] initWithCustomView:someButton2];
    self.navigationItem.rightBarButtonItem=backbutton2;
    
    
    
    UIImage* image3 = [UIImage imageNamed:@"close.png"];
    CGRect frameimg = CGRectMake(0, 0,18,18);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(goLogin) forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    UIBarButtonItem *backbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.leftBarButtonItem=backbutton;
    [someButton release];
    [someButton2 release];
    //storyView.backgroundColor = [UIColor yellowColor];
    storyView.returnKeyType = UIReturnKeyDone;
    storyView.font=[UIFont fontWithName:@"MyriadPro-Regular" size:13];
    storyView.textColor=[UIColor blackColor];
    //storyView.text=@"What is on your mind !!!!";
    //[self.view addSubview:storyView];
    storyView.delegate = self;
    selectedFilter = 0;
}

-(void) loadFilters {
    for(int i = 0; i < 10; i++) {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%d.jpg", i + 1]] forState:UIControlStateNormal];
        button.frame = CGRectMake(10+i*(60+15), 5.0f, 60.0f, 60.0f);
        button.layer.cornerRadius = 30.0f;
        
        //use bezier path instead of maskToBounds on button.layer
        UIBezierPath *bi = [UIBezierPath bezierPathWithRoundedRect:button.bounds
                                                 byRoundingCorners:UIRectCornerAllCorners
                                                       cornerRadii:CGSizeMake(30.0,30.0)];
        
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = button.bounds;
        maskLayer.path = bi.CGPath;
        button.layer.mask = maskLayer;
        
        button.layer.borderWidth = 1;
        button.layer.borderColor = [[UIColor whiteColor] CGColor];
        
        [button addTarget:self
                   action:@selector(filterClicked:)
         forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        [button setTitle:@"*" forState:UIControlStateSelected];
        if(i == 0){
            [button setSelected:YES];
        }
		[self.filtersScrollview addSubview:button];
	}
	[self.filtersScrollview setContentSize:CGSizeMake(10 + 10*(60+15), 75.0)];
}

-(void) filterClicked:(UIButton *) sender {
    for(UIView *view in self.filtersScrollview.subviews){
        if([view isKindOfClass:[UIButton class]]){
            [(UIButton *)view setSelected:NO];
        }
    }
    
    [sender setSelected:YES];
    [self removeAllTargets];
    
    selectedFilter = sender.tag;
    [self setFilter:sender.tag];
    [self prepareFilter];
}


-(void) setFilter:(int) index {
    switch (index) {
        case 1:{
            filter = [[GPUImageContrastFilter alloc] init];
            [(GPUImageContrastFilter *) filter setContrast:1.75];
        } break;
        case 2: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"crossprocess"];
        } break;
        case 3: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"02"];
        } break;
        case 4: {
            filter = [[DLCGrayscaleContrastFilter alloc] init];
        } break;
        case 5: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"17"];
        } break;
        case 6: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"aqua"];
        } break;
        case 7: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"yellow-red"];
        } break;
        case 8: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"06"];
        } break;
        case 9: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"purple-green"];
        } break;
        default:
            filter = [[GPUImageFilter alloc] init];
            break;
    }
}

-(void) prepareFilter {
    if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
        isStatic = YES;
    }
    
    if (!isStatic) {
        [self prepareLiveFilter];
    } else {
        [self prepareStaticFilter];
    }
}

-(void) prepareLiveFilter {
    
    [stillCamera addTarget:cropFilter];
    [cropFilter addTarget:filter];
    //blur is terminal filter
    
    [filter addTarget:_imageView];
    
    [filter prepareForImageCapture];
    
}


-(void) prepareStaticFilter {
    
    [staticPicture addTarget:filter];
    
    // blur is terminal filter
    
    [filter addTarget:self.imageView];
    
    
    GPUImageRotationMode imageViewRotationMode = kGPUImageNoRotation;
    switch (staticPictureOriginalOrientation) {
        case UIImageOrientationLeft:
            imageViewRotationMode = kGPUImageRotateLeft;
            break;
        case UIImageOrientationRight:
            imageViewRotationMode = kGPUImageRotateRight;
            break;
        case UIImageOrientationDown:
            imageViewRotationMode = kGPUImageRotate180;
            break;
        default:
            imageViewRotationMode = kGPUImageNoRotation;
            break;
    }
    
    // seems like atIndex is ignored by GPUImageView...
    [_imageView setInputRotation:imageViewRotationMode atIndex:0];
    
    
    [staticPicture processImage];
}

-(void) removeAllTargets {
    [stillCamera removeAllTargets];
    [staticPicture removeAllTargets];
    [cropFilter removeAllTargets];
    
    //regular filter
    [filter removeAllTargets];
    
    //blur
    
}

-(IBAction)goLogin
{
    
    // if ([delegate.newsId isEqualToString:@"newsId"]) {
    //[self.slidingViewController anchorTopViewTo:ECRight];
    // }
    // else
    //{
    if([idAtUpload isEqualToString:@"circle"])
    {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:0];
        [storyView setHidden:FALSE];
        
    }
    if([idAtUpload isEqualToString:@"profile"])
    {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
        [storyView setHidden:FALSE];
    }
    if([idAtUpload isEqualToString:@"settings"])
    {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:3];
        [storyView setHidden:FALSE];
        
    }
    if([idAtUpload isEqualToString:@"upload"])
    {
        
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:0];
        [imageView2 setHidden:TRUE];
       
        [storyView setHidden:FALSE];
        
    }
    //}
    
}
-(IBAction)cancel
{
    [self viewDidLoad];
    [imageView2 setHidden:TRUE];
    [storyView setHidden:FALSE];
    storyView.text=@"";
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"touchesBegan:withEvent:");
    [self.view endEditing:YES];
    [storyView resignFirstResponder];
    [super touchesBegan:touches withEvent:event];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    NSLog(@"textViewShouldBeginEditing:");
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView {
    // buttonTitle=@"story";
    
    NSLog(@"textViewDidBeginEditing:");
    if (textView.textColor == [UIColor lightGrayColor]) {
        textView.textColor=[UIColor blackColor];
        textView.text=@"";
    }
    
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    NSLog(@"textViewShouldEndEditing:");
    
    return YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"textViewDidEndEditing:");
    NSMutableAttributedString * string;
    string = [[NSMutableAttributedString alloc]initWithString:textView.text];
    NSArray *words=[textView.text componentsSeparatedByString:@" "];
    
    for (NSString *word in words) {
        if (([word hasPrefix:@"#"])||([word hasPrefix:@"@"])) {
            NSRange range=[textView.text rangeOfString:word];
            NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1],NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Bold" size:13],
                                         NSBackgroundColorAttributeName: [UIColor whiteColor]//,NSUnderlineStyleAttributeName: @1
                                         
                                         };
            [string addAttributes:attributes range:range];
            [textView setAttributedText:string];
        }
    }
    
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSCharacterSet *doneButtonCharacterSet = [NSCharacterSet newlineCharacterSet];
    NSRange replacementTextRange = [text rangeOfCharacterFromSet:doneButtonCharacterSet];
    NSUInteger location = replacementTextRange.location;
    if (textView.text.length + text.length >1000){
        if (location != NSNotFound){
            [textView resignFirstResponder];
        }
        return NO;
    }
    else if (location != NSNotFound){
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
-(void)textViewDidChange:(UITextView *)textView{
    NSLog(@"textViewDidChange:");
}
- (void)textViewDidChangeSelection:(UITextView *)textView{
    NSLog(@"textViewDidChangeSelection:");
}
-(IBAction)postStory:(id)sender
{
    NSLog(@"Button pressed: %@", [sender currentTitle]);
    buttonTitle=[sender currentTitle];
    imageView2=nil;
}
-(void)uploadStory{
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];//UIActivityIndicatorViewStyleWhiteLarge
    indicator.frame = CGRectMake(self.view.bounds.size.width / 2.0f - indicator.frame.size.width /2.0f, self.view.bounds.size.height / 2.0f - indicator.frame.size.height /2.0f, indicator.frame.size.width, indicator.frame.size.height);
    //indicator.color = [UIColor colorWithRed:1 green:0.8 blue:0.9 alpha:1 ];
    indicator.color = [UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
    CGAffineTransform transform = CGAffineTransformMakeScale(2.2f, 2.2f);
    indicator.transform = transform;
    [indicator startAnimating];
    [self.view addSubview:indicator];
    [self performSelector:@selector(indicatorStory) withObject:nil afterDelay:0.1];
}
-(void)indicatorStory{
    
    NSLog(@"Story %@",storyView.text);
    NSString *converted = [storyView.text stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    NSString *url = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/story_upload.do?username=%@&story=%@&latitude=%f&longitude=%f",idName,converted,locationManager.location.coordinate.latitude,locationManager.location.coordinate.longitude];
    NSLog(@"URL String ************** %@",url);
    NSURL *urlrequest=[NSURL URLWithString:url];
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    NSData *data = [NSData dataWithContentsOfURL:urlrequest];
    NSLog(@"values in datafordistance is:- %@",data);
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
    NSLog(@"JSON format:- %@",json);
    authStr = [json objectForKey:@"param"];
    NSLog(@"valu in elemant is:- %@",authStr);
    uploadedElapsed=[json objectForKey:@"elapsed"];
    uploadedInserted=[json objectForKey:@"timestamp"];
    uploadedSno=[json objectForKey:@"sno"];
    uploadedLocation=[json objectForKey:@"location"];
    uploadedType=@"story";
    if([authStr isEqualToString:@"True"])
    {
        uploadedData=storyView.text;
        uploadedType=@"story";
        [self addCurentUpdate:uploadedData :uploadedType];
        
        [indicator stopAnimating];
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
        [imageView2 setHidden:TRUE];
     
        [storyView setHidden:FALSE];
        if (flagTu==1) {
            [self sendToTumblr];
        }
        if(flagFb==1)
        {
            [self postStoryOnFb];
        }
        if (flagTw==1) {
            [self postStatusToTwitter];
        }
        storyView.text=@"";
    }
    else if([authStr isEqualToString:@"False"])
    {
        [indicator stopAnimating];
        alert =[[UIAlertView alloc]initWithTitle:@"Failed" message:@"Try agian" delegate:self cancelButtonTitle:@"ok"otherButtonTitles:nil];
        [alert show];
    }
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)imagePicker
{
    [self dismissViewControllerAnimated:NO completion:nil];
  if (isStatic == NO)
  {
      [_captureImage setHidden:NO];
      [_captureVIdeo setHidden:NO];
      [_galleryImage setHidden:NO];
      _imageView.layer.borderColor = [[UIColor alloc] initWithRed:(203.0/255.0) green:(232.0/255.0) blue:(198.0/255.0) alpha:1.0].CGColor;
      _imageView.layer.borderWidth = 2;
     
  }else if (isStatic == YES) {
      [_captureImage setHidden:YES];
      [_captureVIdeo setHidden:YES];
      [_galleryImage setHidden:YES];
      _imageView.layer.borderColor = [UIColor clearColor].CGColor;
      _imageView.layer.borderWidth = 0;
   
  }
}
-(IBAction)browseImage:(id)sender
{
    // [imageView setHidden:FALSE];
    extension=@"testImage.png";
    NSLog(@"Button pressed: %@", [sender currentTitle]);
    buttonTitle=[sender currentTitle];
    [storyView setHidden:FALSE];
    UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
    pickerView.allowsEditing = YES;
    pickerView.delegate = self;
    pickerView.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:pickerView animated:YES completion:nil];
}
-(IBAction)captureImage:(id)sender
{
    //[imageView setHidden:FALSE];
    extension=@"testImage.png";
    NSLog(@"Button pressed: %@", [sender currentTitle]);
    buttonTitle=[sender currentTitle];
    // [storyView setHidden:TRUE];
    UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
    pickerView.allowsEditing = YES;
    pickerView.delegate = self;
    pickerView.sourceType=UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:pickerView animated:YES completion:nil];
    
}

-(IBAction)captureVideo:(id)sender;
{
    //[imageView setHidden:FALSE];
    extension=@"testVideo.mov";
    buttonTitle=[sender currentTitle];
    [storyView setHidden:TRUE];
    storyView.text=@"";
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
	{
        UIImagePickerController *videoRecorder = [[UIImagePickerController alloc] init];
        videoRecorder.sourceType = UIImagePickerControllerSourceTypeCamera;
        videoRecorder.delegate = self;
        NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
        NSArray *videoMediaTypesOnly = [mediaTypes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(SELF contains %@)", @"movie"]];
        
        if ([videoMediaTypesOnly count] == 0)		//Is movie output possible?
        {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Sorry but your device does not support video recording"
                                                                     delegate:nil
                                                            cancelButtonTitle:@"OK"
                                                       destructiveButtonTitle:nil
                                                            otherButtonTitles:nil];
            [actionSheet showInView:[[self view] window]];
            [actionSheet autorelease];
        }
        else
        {
            //Select front facing camera if possible
            //if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront])
            if([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraCaptureModeVideo])
                videoRecorder.cameraDevice = UIImagePickerControllerCameraDeviceRear;
            videoRecorder.delegate = self;
            videoRecorder.mediaTypes = videoMediaTypesOnly;
            videoRecorder.videoQuality = UIImagePickerControllerQualityTypeMedium;
            videoRecorder.videoMaximumDuration = 180;			//Specify in seconds (600 is default)
            [self presentViewController:videoRecorder animated:YES completion:nil];
        }
        [videoRecorder release];
    }
    else
    {
        //No camera is availble
    }
}



-(BOOL)startMediaBrowserFromViewController:(UIViewController*)controller usingDelegate:(id )delegate {
    // 1 - Validations
    if (([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
        || (delegate == nil)
        || (controller == nil)) {
        return NO;
    }
    // 2 - Get image picker
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    mediaUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    mediaUI.allowsEditing = YES;
    mediaUI.delegate = delegate;
    // 3 - Display image picker
    [controller presentViewController:mediaUI animated:YES completion:nil];
    return YES;
}


# pragma delegate methods of uiimagepickercontrioller

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [storyView setHidden:FALSE];
    storyView.text=@"";
    [_captureImage setHidden:YES];
    [_captureVIdeo setHidden:YES];
    [_galleryImage setHidden:YES];
    _imageView.layer.borderWidth = 0;
    _imageView.layer.borderColor = [UIColor clearColor].CGColor;
   
    [self dismissViewControllerAnimated:true completion:^{
        _imageView.layer.borderWidth = 0;
        _imageView.layer.borderColor = [UIColor clearColor].CGColor;
    
    }];
    UIImage * img = [info valueForKey:UIImagePickerControllerEditedImage];
    UIImage * img2 = [info valueForKey:UIImagePickerControllerEditedImage];
    
    if([buttonTitle isEqualToString:@"camera"])
    {
        [imageView2 setHidden:FALSE];
        img = [info valueForKey:UIImagePickerControllerEditedImage];
        NSLog(@"image nme********* %@",img);
        img2 = [info valueForKey:UIImagePickerControllerEditedImage];
        imageView2.image = img;
        staticPicture = [[GPUImagePicture alloc] initWithImage:img2 smoothlyScaleOutput:YES];
        
        [self prepareStaticFilter];
        isStatic = YES;
    }
    if([buttonTitle isEqualToString:@"gallery"])
    {
        
        [imageView2 setHidden:FALSE];
        img = [info valueForKey:UIImagePickerControllerEditedImage];
        NSLog(@"image nme********* %@",img);
        img2 = [info valueForKey:UIImagePickerControllerEditedImage];
        UIImage* outputImage = [info objectForKey:UIImagePickerControllerEditedImage];
        
        staticPictureOriginalOrientation = outputImage.imageOrientation;
        imageView2.image = img;
        staticPicture = [[GPUImagePicture alloc] initWithImage:outputImage smoothlyScaleOutput:YES];
        [self prepareStaticFilter];
        isStatic = YES;
        
    }
    if([buttonTitle isEqualToString:@"video"])
    {
        [imageView2 setHidden:FALSE];
        UIImage *play = [UIImage imageNamed:@"thumbVideo.png"];
        imageView2.image = play;
        _selectedVideoUrl = [info objectForKey:UIImagePickerControllerMediaURL];
        
        NSLog(@"selected video url is:- %@",_selectedVideoUrl);
        videourlarray = [[NSMutableArray alloc]initWithCapacity:1];
        [videourlarray addObject:_selectedVideoUrl];
        
        
        
       
        
        AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL:_selectedVideoUrl options:nil];
        AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
        generate1.appliesPreferredTrackTransform = YES;
        NSError *err = NULL;
        CMTime time = CMTimeMake(1, 2);
        CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
        UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
        imageView2.image = one;
        imageView2.contentMode = UIViewContentModeScaleAspectFit;
        
      
    }
    
    
}
-(void)upload{
    indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];//UIActivityIndicatorViewStyleWhiteLarge
    indicator.frame = CGRectMake(self.view.bounds.size.width / 2.0f - indicator.frame.size.width /2.0f, self.view.bounds.size.height / 2.0f - indicator.frame.size.height /2.0f, indicator.frame.size.width, indicator.frame.size.height);
    //indicator.color = [UIColor colorWithRed:1 green:0.8 blue:0.9 alpha:1 ];
    indicator.color = [UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
    CGAffineTransform transform = CGAffineTransformMakeScale(2.2f, 2.2f);
    indicator.transform = transform;
    [indicator startAnimating];
    [self.view addSubview:indicator];
    [self performSelector:@selector(indicator) withObject:nil afterDelay:0.1];
}
-(void)indicator
{
    
    NSLog(@"helllooooooooo");
    NSData *imageData =[[NSData alloc]init];
    if([buttonTitle isEqualToString:@"camera"])
    {    if(selectedFilter == 0)
    {
        
        imageData = UIImageJPEGRepresentation(imageView2.image, 90);
        
    } else {
        
        UIImage *currentFilteredImage = [filter imageFromCurrentlyProcessedOutput];
        imageData = UIImageJPEGRepresentation(currentFilteredImage, 90);
    }
        uploadedType=@"image";
    }
    if([buttonTitle isEqualToString:@"gallery"])
    {
        if(selectedFilter == 0)
    {
        
        imageData = UIImageJPEGRepresentation(imageView2.image, 90);
        
    } else {
        
        UIImage *currentFilteredImage = [filter imageFromCurrentlyProcessedOutput];
        imageData = UIImageJPEGRepresentation(currentFilteredImage, 90);
    }
uploadedType=@"image";
    }
    if([buttonTitle isEqualToString:@"video"])
    {
        NSLog(@"selected url is: - %@",_selectedVideoUrl);
        uploadedType=@"video";
        imageData = [NSData dataWithContentsOfURL:_selectedVideoUrl];
        //NSLog(@"%@",imageData);
    }
    NSString *theLat = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
    NSString *theLong = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
    
    NSString *urlString = @"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/upload.do?";
    NSLog(@"hiiiiiiihellooooo %@",urlString);
    // NSString *urlString=@"http://192.168.10.53:8080/Cancer_final/upload.do?";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    [request setValue:@"300"       forHTTPHeaderField:@"Keep-Alive"];
    [request setValue:@"keep-live" forHTTPHeaderField:@"Connection"];
    
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *postBody = [NSMutableData data];
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *str=[[[[[[[[[@"Content-Disposition: form-data;name=\"" stringByAppendingString:idName] stringByAppendingString:@",iphone,"] stringByAppendingString:theLat] stringByAppendingString:@","]stringByAppendingString:theLong] stringByAppendingString:@"\";"]
                     stringByAppendingString:@"filename=\""]stringByAppendingString:extension] stringByAppendingFormat:@"\"\r\n"];
    
    NSLog(@"*******%@",str);
    
    [postBody appendData:[[NSString stringWithString:str] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:imageData];
    [postBody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:postBody];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"yepieeeeeeeeee %@",returnString);
    NSURL *urlrequest=[NSURL URLWithString:urlString];
    NSData *data = [NSData dataWithContentsOfURL:urlrequest];
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
    NSLog(@"JSON format:- %@",json);
    authStr = [json objectForKey:@"message"];
    NSLog(@"valu in elemant is:- %@",authStr);
    uploadedData=[json objectForKey:@"link"];
    uploadedElapsed=[json objectForKey:@"elapsed"];
    uploadedInserted=[json objectForKey:@"timestamp"];
    uploadedSno=[json objectForKey:@"sno"];
    uploadedLocation=[json objectForKey:@"location"];
    if([authStr isEqualToString:@"success"])
    {
       [self addCurentUpdate:uploadedData :uploadedType];
    //    if (flagFb==1) {
    //        if ( [extension isEqualToString:@"testVideo.mov"]) {
    //            [self postStoryOnFb];
    //        }
    //        else
    //        {
    //            [self postDataOnFb];
    //        }
    //    }
    //    if (flagTu==1) {
    //
    //        [self sendToTumblr];
    //
    //    }
    //    if (flagFl==1) {
    //        [self postImageToFlickr];
    //    }
    //    if (flagTw==1) {
    //        [self postImageToTwitter];
    //    }
    //
        [indicator stopAnimating];
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
        
        
        [storyView setHidden:FALSE];
    }
    else if([authStr isEqualToString:@"Please try again later there is some problem in network"])
    {
        [indicator stopAnimating];
        alert =[[UIAlertView alloc]initWithTitle:@"Error" message:@"Uploading failed" delegate:self cancelButtonTitle:@"Try again"otherButtonTitles:nil];
        alert.tag=1;
        [alert show];
    }
   
}
-(IBAction)post
{
    NSLog(@"Ready to past ok done");
    NSLog(@"Action perfomed %@",buttonTitle);
    if([storyView.text length]>0)
    {
        [self uploadStory];
        
    }
    if([buttonTitle isEqualToString:@"camera"])
    {
        [self upload];
        
    }
    if([buttonTitle isEqualToString:@"gallery"])
    {
        [self upload];
        
    }
    if([buttonTitle isEqualToString:@"story"])
    {
        [self uploadStory];
        
    }
    if([buttonTitle isEqualToString:@"video"])
    {
        NSLog(@"video url is: - %@",[videourlarray objectAtIndex:0]);
        [self upload];
    }
}
-(IBAction)goSettings
{
    self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:3];
    
}
-(IBAction)hidekeyboard:(id)sender
{
    [storyView resignFirstResponder];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
-(IBAction)postOnFb

{
    
    [self authenticateUserWithCallbackObject:self andSelector:@selector(fbGraphCallback:)                      andExtendedPermissions:@"user_photos,user_videos,publish_stream,offline_access,user_checkins,friends_checkins,email,user_birthday"];
    
    if (flagFb==1) {
        [fbButton setBackgroundImage:[UIImage imageNamed:@"fb.png"] forState:UIControlStateNormal];
        flagFb=0;
    }
    else
    {
        AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
        
        NSUserDefaults *outhToken=[NSUserDefaults standardUserDefaults];
        NSString *keyOfOuth=[ delegate.mynameLogin stringByAppendingString:@"outhToken"];
        NSString *getToken=[outhToken objectForKey:keyOfOuth];
        delegate.token=getToken;
        NSLog(@"delegate.token %@",getToken);
        //if([getToken isEqualToString:@"no"])
        if ((getToken.length==0))
        {
            
           [self authenticateUserWithCallbackObject:self andSelector:@selector(fbGraphCallback:)                      andExtendedPermissions:@"user_photos,user_videos,publish_stream,offline_access,user_checkins,friends_checkins,email,user_birthday"];
            
        }
        
        flagFb=1;
        [fbButton setBackgroundImage:[UIImage imageNamed:@"fbDark.png"] forState:UIControlStateNormal];
    }
    
}
- (BOOL)userHasAccessToTwitter
{
    return [SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter];
}

- (IBAction)filtersbutton: (UIButton *)sender
{
    blurView.dynamic = YES;
    
    if (sender.tag == 0) {
        sender.tag = 1;
        
        [UIView animateWithDuration:0.2 animations:^{
            [transview setAlpha:1.0];
            [transview setHidden:NO];
            [filtersbutton setTitleColor:[[UIColor alloc] initWithRed:(0.0/255.0) green:(217.0/255.0) blue:(13.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        }];
        [filtersbutton setSelected:YES];
        
        
        
        
    }
    else{
        sender.tag=0;
        
        [UIView animateWithDuration:0.1 animations:^{
            [transview setAlpha:0.0];
            [transview setHidden:YES];
            [filtersbutton setTitleColor:[[UIColor alloc] initWithRed:(167.0/255.0) green:(205.0/255.0) blue:(164.0/255.0) alpha:1.0] forState:UIControlStateNormal];
        }];
        
        [filtersbutton setSelected:NO];
    }
}





-(IBAction)postOnTw
{
    if (flagTw==1) {
        [twButton setBackgroundImage:[UIImage imageNamed:@"tweet.png"] forState:UIControlStateNormal];
        flagTw=0;
    }
    else
    {
        if ([self userHasAccessToTwitter]) {
            flagTw=1;
            [twButton setBackgroundImage:[UIImage imageNamed:@"tweetDark.png"] forState:UIControlStateNormal];
            ACAccountStore *accountStore = [[ACAccountStore alloc] init];
            ACAccountType *twitterAccountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
            [accountStore requestAccessToAccountsWithType:twitterAccountType options:nil completion:^(BOOL granted, NSError *error) {
                if(granted) {
                    NSArray *accountsArray = [accountStore accountsWithAccountType:twitterAccountType];
                    
                    if ([accountsArray count] > 0) {
                        ACAccount *twitterAccount = [accountsArray objectAtIndex:0];
                        NSUserDefaults *tweetUser=[NSUserDefaults standardUserDefaults];
                        [tweetUser setObject:twitterAccount.username forKey:@"tweetUser"];
                        NSLog(@"%@",twitterAccount.username);
                        NSLog(@"%@",twitterAccount.accountType);
                    }
                }
            }];
        }
        else
        {
            UIAlertView *alertTweet=[[UIAlertView alloc]initWithTitle:@"Go to Settings" message:@"No Twitter Account in iPhone settings" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertTweet show];
        }
    }
}

-(void)addCurentUpdate:(NSString *)updateString :(NSString *)dataType
{
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
    [delegate.PdataUpdate insertObject:updateString atIndex:0];
    [delegate.PdataType insertObject:dataType atIndex:0];
    [delegate.PsnoArray insertObject:uploadedSno atIndex:0];
    [delegate.Plikearray insertObject:@"0" atIndex:0];
    [delegate.Pdislikearray insertObject:@"0" atIndex:0];
    [delegate.PcommentCounts insertObject:@"" atIndex:0];
    [delegate.PlikesNameArray insertObject:@"" atIndex:0];
    [delegate.PdislikesNameArray insertObject:@"" atIndex:0];
    [delegate.PtimeArray insertObject:uploadedElapsed atIndex:0];
    [delegate.PlocationArray insertObject:uploadedLocation atIndex:0];
    [delegate.PfourCommentArray insertObject:@"No Comments" atIndex:0];
    delegate.PgetInertedTime=uploadedInserted;
    
    
    [delegate.dataUpdate insertObject:updateString atIndex:0];
    [delegate.dataType insertObject:dataType atIndex:0];
    [delegate.snoArray insertObject:uploadedSno atIndex:0];
    [delegate.likearray insertObject:@"0" atIndex:0];
    [delegate.dislikearray insertObject:@"0" atIndex:0];
    [delegate.differUser insertObject:delegate.mynameLogin atIndex:0];
    [delegate.likesNameArray insertObject:@"" atIndex:0];
    [delegate.dislikesNameArray insertObject:@"" atIndex:0];
    [delegate.profielImages insertObject:delegate.profileImage atIndex:0];
    [delegate.commentCounts insertObject:@"" atIndex:0];
    [delegate.locationArray insertObject:uploadedLocation atIndex:0];
    delegate.getInertedTime=uploadedInserted;
    [delegate.timeArray insertObject:uploadedElapsed atIndex:0];
    [delegate.fourCommentArray insertObject:@"No Comments"atIndex:0];
}
-(void)postDataOnFb
{
    NSMutableDictionary *variables = [NSMutableDictionary dictionaryWithCapacity:2];
	
	//create a UIImage (you could use the picture album or camera too)
    NSData  *imageData = UIImageJPEGRepresentation(imageView2.image, 90);
	UIImage *picture = [UIImage imageWithData:imageData];
	
	//create a FbGraphFile object insance and set the picture we wish to publish on it
	FbGraphFile *graph_file = [[FbGraphFile alloc] initWithImage:picture];
	
	//finally, set the FbGraphFileobject onto our variables dictionary....
	[variables setObject:graph_file forKey:@"file"];
	
	[variables setObject:@"Via Cancer Circle iOS" forKey:@"message"];
	
	//the fbGraph object is smart enough to recognize the binary image data inside the FbGraphFile
	//object and treat that is such.....
	FbGraphResponse *fb_graph_response = [self doGraphPost:@"151898678325186/photos" withPostVars:variables];
	NSLog(@"postPictureButtonPressed:  %@", fb_graph_response.htmlResponse);
    
    
}
-(void)postStoryOnFb
{
    NSMutableDictionary *variables = [NSMutableDictionary dictionaryWithCapacity:1];
    
	[variables setObject:uploadedData forKey:@"message"];
	
	//the fbGraph object is smart enough to recognize the binary image data inside the FbGraphFile
	//object and treat that is such.....
	FbGraphResponse *fb_graph_response = [self doGraphPost:@"me/feed" withPostVars:variables];
	NSLog(@"postPictureButtonPressed:  %@", fb_graph_response.htmlResponse);
}

- (FbGraphResponse *)doGraphPost:(NSString *)action withPostVars:(NSDictionary *)post_vars {
	AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
    NSLog(@"fb token %@",delegate.token);
	FbGraphResponse *return_value = [[[FbGraphResponse alloc] init] autorelease];
	
	NSString *urlString = [NSString stringWithFormat:@"https://graph.facebook.com/%@", action];
	NSLog(@"URL FB %@",urlString);
	NSURL *url = [NSURL URLWithString:urlString];
	NSString *boundary = @"----1010101010";
	NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	[request setHTTPMethod:@"POST"];
	[request addValue:contentType forHTTPHeaderField: @"Content-Type"];
	
	NSMutableData *body = [NSMutableData data];
	[body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
	
	NSEnumerator *enumerator = [post_vars keyEnumerator];
	NSString *key;
	NSString *value;
	NSString *content_disposition;
	
	//loop through all our parameters
	while ((key = (NSString *)[enumerator nextObject])) {
		
		//if it's a picture (file)...we have to append the binary data
		if ([key isEqualToString:@"file"]) {
			
			/*
			 * the FbGraphFile object is smart enough to append it's data to
			 * the request automagically, regardless of the type of file being
			 * attached
			 */
			FbGraphFile *upload_file = (FbGraphFile *)[post_vars objectForKey:key];
			[upload_file appendDataToBody:body];
			
			//key/value nsstring/nsstring
		} else {
			value = (NSString *)[post_vars objectForKey:key];
			
			content_disposition = [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key];
			[body appendData:[content_disposition dataUsingEncoding:NSUTF8StringEncoding]];
			[body appendData:[value dataUsingEncoding:NSUTF8StringEncoding]];
			
		}//end else
		
		[body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
		
	}//end while
	
	//add our access token
	[body appendData:[@"Content-Disposition: form-data; name=\"access_token\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
	[body appendData:[delegate.token dataUsingEncoding:NSUTF8StringEncoding]];
	[body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
	
	//button up the request body
	[body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
	
	[request setHTTPBody:body];
	[request addValue:[NSString stringWithFormat:@"%d", body.length] forHTTPHeaderField: @"Content-Length"];
	
	//quite a few lines of code to simply do the business of the HTTP connection....
    NSURLResponse *response;
    NSData *data_reply;
	NSError *err;
	
    data_reply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
	
	NSString *stringResponse = [[NSString alloc] initWithData:data_reply encoding:NSUTF8StringEncoding];
    NSLog(@"stringResponse %@",stringResponse);
    return_value.htmlResponse = stringResponse;
	[stringResponse release];
	
	/*if (err != nil) {
     return_value.error = err;
     }
     
	 * return the json array.  we could parse it, but that would incur overhead
	 * some users might not want (not to mention dependencies), besides someone
	 * may want raw strings back, keep it simple.
	 *
	 * See:  http://code.google.com/p/json-framework for an easy json parser
	 */
	
	return return_value;
}
-(void)postStatusToTwitter
{
    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:
                                  ACAccountTypeIdentifierTwitter];
    
    [account requestAccessToAccountsWithType:accountType options:nil
                                  completion:^(BOOL granted, NSError *error)
     {
         if (granted == YES)
         {
             NSArray *arrayOfAccounts = [account accountsWithAccountType:accountType];
             
             if ([arrayOfAccounts count] > 0)
             {
                 
                 NSDictionary *message = @{@"status": uploadedData};
                 
                 NSURL *requestURL = [NSURL
                                      URLWithString:@"http://api.twitter.com/1/statuses/update.json"];
                 SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter                                   requestMethod:SLRequestMethodPOST URL:requestURL parameters:message];
                 [request setAccount:[arrayOfAccounts lastObject]];
                 [request performRequestWithHandler:^(NSData *responseData,
                                                      NSHTTPURLResponse *urlResponse, NSError *error)
                  {
                      NSLog(@"Twitter HTTP response: %i", [urlResponse
                                                           statusCode]);
                  }];
             }
         }
     }];
    
}

- (void)postImageToTwitter
{
    UIImage *image=imageView2.image;
    NSString *status=@"Via Cancer Circle iOS";
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *twitterType =
    [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    SLRequestHandler requestHandler =
    ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (responseData) {
            NSInteger statusCode = urlResponse.statusCode;
            if (statusCode >= 200 && statusCode < 300) {
                NSDictionary *postResponseData =
                [NSJSONSerialization JSONObjectWithData:responseData
                                                options:NSJSONReadingMutableContainers
                                                  error:NULL];
                NSLog(@"[SUCCESS!] Created Tweet with ID: %@", postResponseData[@"id_str"]);
            }
            else {
                NSLog(@"[ERROR] Server responded: status code %d %@", statusCode,
                      [NSHTTPURLResponse localizedStringForStatusCode:statusCode]);
            }
        }
        else {
            NSLog(@"[ERROR] An error occurred while posting: %@", [error localizedDescription]);
        }
    };
    
    ACAccountStoreRequestAccessCompletionHandler accountStoreHandler =
    ^(BOOL granted, NSError *error) {
        if (granted) {
            NSArray *accounts = [accountStore accountsWithAccountType:twitterType];
            NSURL *url = [NSURL URLWithString:@"https://api.twitter.com"
                          @"/1.1/statuses/update_with_media.json"];
            NSDictionary *params = @{@"status" : status};
            SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                    requestMethod:SLRequestMethodPOST
                                                              URL:url
                                                       parameters:params];
            NSData *imageData = UIImageJPEGRepresentation(image, 1.f);
            [request addMultipartData:imageData
                             withName:@"media[]"
                                 type:@"image/jpeg"
                             filename:@"image.jpg"];
            [request setAccount:[accounts lastObject]];
            [request performRequestWithHandler:requestHandler];
        }
        else {
            NSLog(@"[ERROR] An error occurred while asking for user authorization: %@",
                  [error localizedDescription]);
        }
    };
    
    [accountStore requestAccessToAccountsWithType:twitterType
                                          options:NULL
                                       completion:accountStoreHandler];
}

//////////// FB Token


- (void)fbGraphCallback:(id)sender {
    NSLog(@"Send to next screen");
	
	if ( (accessToken == nil) || ([accessToken length] == 0) ) {
		
		NSLog(@"You pressed the 'cancel' or 'Dont Allow' button, you are NOT logged into Facebook...I require you to be logged in & approve access before you can do anything useful....");
		
		//restart the authentication process.....
		[self authenticateUserWithCallbackObject:self andSelector:@selector(fbGraphCallback:)
                          andExtendedPermissions:@"user_photos,user_videos,publish_stream,offline_access,user_checkins,friends_checkins"];
		
	} else {
        
        NSLog(@"--->CONGRATULATIONS<----, You're logged into Facebook...  Your oAuth token is:  %@",accessToken);
        
        
        FbGraphResponse *fb_graph_response = [self doGraphGet:@"me" withGetVars:nil];
        NSLog(@"getMeButtonPressed:  %@", fb_graph_response.htmlResponse);
        NSData* data=[fb_graph_response.htmlResponse dataUsingEncoding: [NSString defaultCStringEncoding] ];
        NSError *error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data //1
                                                             options:kNilOptions
                                                               error:&error];
        NSLog(@"########### %@",json);
        NSLog(@"Verified ===== %@",[json valueForKey:@"verified"]);
        NSLog(@"Birthday ===== %@",[json valueForKey:@"birthday"]);
        NSLog(@"First Name === %@",[json valueForKey:@"first_name"]);
        NSLog(@"Last Name ==== %@",[json valueForKey:@"last_name"]);
        NSLog(@"Email ======== %@",[json valueForKey:@"email"]);
        NSLog(@"Name ========= %@",[json valueForKey:@"name"]);
        NSLog(@"Username ===== %@",[json valueForKey:@"username"]);
        NSLog(@"Gender ======= %@",[json valueForKey:@"gender"]);
        
        
        // NSString *keyOfName=[delegate.mynameLogin stringByAppendingString:@"outhName"];
        NSUserDefaults *outhName=[NSUserDefaults standardUserDefaults];
        [outhName setObject:[json valueForKey:@"username"] forKey:@"outhName"];
        [self dismissViewControllerAnimated:YES completion:nil];
       	
    }
}
- (void)authenticateUserWithCallbackObject:(id)anObject andSelector:(SEL)selector andExtendedPermissions:(NSString *)extended_permissions andSuperView:(UIView *)super_view {
	
	self.callbackObject = anObject;
	self.callbackSelector = selector;
    
	facebookClientID = @"366546850134442";
    redirectUri = @"https://www.facebook.com/connect/login_success.html";
	NSString *url_string = [NSString stringWithFormat:@"https://graph.facebook.com/oauth/authorize?client_id=%@&redirect_uri=%@&scope=%@&type=user_agent&display=touch", facebookClientID, redirectUri, extended_permissions];
    NSLog(@"url_string FACEBOOk %@",url_string);
	NSURL *url = [NSURL URLWithString:url_string];
	NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webView loadRequest:request];
    
}

-(void)authenticateUserWithCallbackObject:(id)anObject andSelector:(SEL)selector andExtendedPermissions:(NSString *)extended_permissions {
	
	UIWindow* window = [UIApplication sharedApplication].keyWindow;
	if (!window) {
		window = [[UIApplication sharedApplication].windows objectAtIndex:0];
	}
	
	[self authenticateUserWithCallbackObject:anObject andSelector:selector andExtendedPermissions:extended_permissions andSuperView:window];
}

- (FbGraphResponse *)doGraphGet:(NSString *)action withGetVars:(NSDictionary *)get_vars {
	
	NSString *url_string = [NSString stringWithFormat:@"https://graph.facebook.com/%@?", action];
	
	//tack on any get vars we have...
	if ( (get_vars != nil) && ([get_vars count] > 0) ) {
		
		NSEnumerator *enumerator = [get_vars keyEnumerator];
		NSString *key;
		NSString *value;
		while ((key = (NSString *)[enumerator nextObject])) {
			
			value = (NSString *)[get_vars objectForKey:key];
			url_string = [NSString stringWithFormat:@"%@%@=%@&", url_string, key, value];
			
		}//end while
	}//end if
	
	if (accessToken != nil) {
		//now that any variables have been appended, let's attach the access token....
		url_string = [NSString stringWithFormat:@"%@access_token=%@", url_string, self.accessToken];
	}
	
	//encode the string
	url_string = [url_string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	NSLog(@"URL STRING ME %@",url_string);
	return [self doGraphGetWithUrlString:url_string];
}
- (FbGraphResponse *)doGraphGetWithUrlString:(NSString *)url_string {
	
	FbGraphResponse *return_value = [[[FbGraphResponse alloc] init] autorelease];
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url_string]];
	
	NSError *err;
	NSURLResponse *resp;
	NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:&resp error:&err];
	
	if (resp != nil) {
		
		/**
		 * In the case we request a picture (avatar) the Graph API will return to us the actual image
		 * bits versus a url to the image.....
		 **/
		if ([resp.MIMEType isEqualToString:@"image/jpeg"]) {
			
			UIImage *image = [UIImage imageWithData:response];
			return_value.imageResponse = image;
			
		} else {
		    
			NSString *stringResponse = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
			return_value.htmlResponse = stringResponse;
			[stringResponse release];
		}
		
	} else if (err != nil) {
		return_value.error = err;
	}
	
	return return_value;
	
}
- (void)webViewDidFinishLoad:(UIWebView *)_webView {
	
	/**
	 * Since there's some server side redirecting involved, this method/function will be called several times
	 * we're only interested when we see a url like:  http://www.facebook.com/connect/login_success.html#access_token=..........
	 */
	
	//get the url string
	NSString *url_string = [((_webView.request).URL) absoluteString];
	
	//looking for "access_token="
	NSRange access_token_range = [url_string rangeOfString:@"access_token="];
	
	//looking for "error_reason=user_denied"
	NSRange cancel_range = [url_string rangeOfString:@"error_reason=user_denied"];
	
	//it exists?  coolio, we have a token, now let's parse it out....
	if (access_token_range.length > 0) {
		
		//we want everything after the 'access_token=' thus the position where it starts + it's length
		int from_index = access_token_range.location + access_token_range.length;
		NSString *access_token = [url_string substringFromIndex:from_index];
		
		//finally we have to url decode the access token
		access_token = [access_token stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		//remove everything '&' (inclusive) onward...
		NSRange period_range = [access_token rangeOfString:@"&"];
		
		//move beyond the .
		access_token = [access_token substringToIndex:period_range.location];
		
		//store our request token....
		self.accessToken = access_token;
        AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
        delegate.token=access_token;
        NSUserDefaults *outhToken=[NSUserDefaults standardUserDefaults];
        //NSString *keyOfOuth=[ delegate.mynameLogin stringByAppendingString:@"outhToken"];
        [outhToken setObject:access_token forKey:@"outhToken"];
		
		//remove our window
		UIWindow* window = [UIApplication sharedApplication].keyWindow;
		if (!window) {
			window = [[UIApplication sharedApplication].windows objectAtIndex:0];
		}
		
		//[self.webView removeFromSuperview];
		
		//tell our callback function that we're done logging in :)
		if ( (callbackObject != nil) && (callbackSelector != nil) ) {
			[callbackObject performSelector:callbackSelector];
		}
		
		//the user pressed cancel
	} else if (cancel_range.length > 0) {
		//remove our window
		UIWindow* window = [UIApplication sharedApplication].keyWindow;
		if (!window) {
			window = [[UIApplication sharedApplication].windows objectAtIndex:0];
		}
		
		//[self.webView removeFromSuperview];
		
		//tell our callback function that we're done logging in :)
		if ( (callbackObject != nil) && (callbackSelector != nil) ) {
			[callbackObject performSelector:callbackSelector];
		}
		
	}
}
-(void)viewDidDisappear:(BOOL)animated
{
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [bgImage release];
   
    
    [_buttonScroll release];
 
    [filterbutton release];
    [transview release];
    [filtersbutton release];
    [_imageView release];
    [_filtersScrollview release];
    [_imageView release];
    [imageView2 release];
    [_imageView release];
    [_imageView release];
    [_captureImage release];
    [_captureVIdeo release];
   
    [gallery release];
    [_galleryImage release];
    [_galleryImage release];
    [_textButton release];
    [_fontsButton release];
    [_bordersButton release];
    [super dealloc];
}
@end
