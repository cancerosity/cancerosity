//
//  LNPSettings.h
//  Cancerosity
//
//  Created by Grant Bevan on 31/03/2014.
//  Copyright (c) 2014 Raminder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LNPSettings : UIViewController
{
    IBOutlet UISwitch *switchLoc;
    IBOutlet UISwitch *switchPriv;
    IBOutlet UISwitch *switchEm;
    IBOutlet UISwitch *switchPh;
    IBOutlet UITableView *toggleTable;
    NSString *username;
}
@property (retain, nonatomic) IBOutlet UITableViewCell *locCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *privCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *emCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *phCell;
-(IBAction)switchButtonLocation;
-(IBAction)switchButtonEmail;
-(IBAction)switchButtonPrivate;

@end
