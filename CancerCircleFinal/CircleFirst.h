//
//  CircleFirst.h
//  CancerCircleFinal
//
//  Created by Raminder on 29/04/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//


#import "MediaPlayer/MediaPlayer.h"
#import "MobileCoreServices/UTCoreTypes.h"
#import "MobileCoreServices/UTType.h"
#import <CoreMotion/CoreMotion.h>
#import <QuartzCore/QuartzCore.h>
#import "IFTweetLabel.h"
#import "ECSlidingViewController.h"
#import "NewsViewController.h"
#import "imageCell.h"
#import "image1Cell.h"
#import "image2Cell.h"
#import "IFLabelUsername.h"
#import "FXBlurView.h"
#import "ILTranslucentView.h"
#import "storycell1.h"
#import <AVFoundation/AVFoundation.h>
#import "RecentSearchView.h"


@interface CircleFirst : UIViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UITabBarControllerDelegate,UIActionSheetDelegate,UISearchBarDelegate>
{
    IBOutlet UITableView * mytable;
    NSInteger selectedIndex;
    NSMutableArray *updatesAarray;
    UIAlertView *alert;
    NSMutableArray *dataUpdate;
    NSMutableArray *dataUpdate2;
    UIButton *playButton ;
    NSString *dataStr;
    NSString *dataStr2;
    NSString *getusername;
    NSString *getuserpass;
    NSMutableArray *differUser;
    NSMutableArray *snoArray;
    NSMutableArray *likearray;
    NSMutableArray *dislikearray;
    NSMutableArray *profielImages;
    NSString *contentType;
    NSMutableArray *imageDatas;
    NSMutableArray *dataType;
    NSString *countsLyk;
    UIImageView *imageViewUrl;
    NSMutableArray *likesNameArray;
    NSMutableArray *dislikesNameArray;
    UIActionSheet *actionSheet;
    NSMutableArray *commentsArray;
    NSMutableArray *commentNameArray;
    NSMutableArray *commentPicArray;
    NSMutableArray *commentCounts;
    NSMutableArray *locationArray;
    NSMutableArray *timeArray;
    NSMutableArray *insertedTimeArray;
    NSMutableArray *fourCommentArray;
    IBOutlet UIButton *someButton;
    UIPanGestureRecognizer *swipeLeftRight;
    UIView *searchview;
    UIView *searchButtonsView;
    UIView *searchMain;
    UISearchBar *thesearchbar;
    IBOutlet UIButton *recentSearch;
    IBOutlet UIButton *popularSearch;
    IBOutlet UIButton *favouriteSearch;
    IBOutlet UILabel *label;
    UILabel *likeShow2;
    NSMutableDictionary *recentSearchesDict;
    NSMutableArray *recentSearchesStrings;
    int in;
    IFTweetLabel *tweetLabel;
    RecentSearchView *recentView;
    BOOL swap;
    BOOL searchopen;
    BOOL keyboardShown;
    BOOL newsOpen;
    UIBarButtonItem *searchbutton;
    UIImage *scaleImage;
    //////// Pop up image ////////
    IBOutlet UIImageView *imageToOpen;
    IBOutlet UIView *imageContainer;
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIButton *doneButton;
    BOOL imageIsOpen;
    BOOL buttonsShown;
    BOOL imageIsZoom;
    ///////////////////////
@protected
      // we keep a weak pointer because, well, we don't need a strong pointer.
    CGFloat _scrollViewContentOffsetYThreshold;  // defines at what contentOffset of the above scrollView the navigationBar should start scrolling
}
@property (retain, nonatomic) IBOutlet UINavigationBar *topBar;
@property (nonatomic, strong) NSOperationQueue *imageDownloadingQueue;

@property (nonatomic, strong) NSCache *imageCache;

-(void)dismissKeyboard;
@property (nonatomic, strong) NSOperationQueue *imageDownloadingQueuePic;
@property (nonatomic, strong) NSCache *imageCachePic;
@property (retain, nonatomic) NSURLConnection *connection;
@property (retain, nonatomic) NSMutableData *receivedData;
@property (strong, nonatomic) NSString *getUsername;
@property (strong, nonatomic) NSString *getNameLogin;
@property (strong, nonatomic) NSString *getpassword;
@property (strong, nonatomic) NSString *getLatestTime;
@property (nonatomic, strong) MPMoviePlayerController *movieplayer;
@property(retain,nonatomic)NSString *videoUrl;
@property (nonatomic,readonly)  int introw;
-(void)shareButtonClicked:(UIButton*)btnClicked;
-(void)userButtonVideoClicked:(NSString *)searchName;
- (void)scrollViewDidScroll:(UIScrollView*)aScrollView;
- (IBAction)showsearch: (UIButton *)sender;
- (IBAction)goNews: (UIButton *)sender;
- (IBAction)showsRecent: (UIButton *)sender;
-(void) displayRecent;
@property(nonatomic,retain)NSString *getProfileImage;
//////// Pop up image ////////
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIImageView *expandImage;
- (void)view:(UIView*)view setCenter:(CGPoint)centerPoint;
-(IBAction)closeView:(UIButton *) sender;
/////////////////////////////////

@property (strong, nonatomic) CMMotionManager *imageMotioManager;



@end
