//
//  getFbTokenViewController.h
//  CancerCircleFinal
//
//  Created by Raminder on 22/06/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FbGraphResponse.h"
#import <Foundation/Foundation.h>

@interface getFbTokenViewController : UIViewController
{
NSString *facebookClientID;
NSString *redirectUri;
NSString *accessToken;
IBOutlet UIWebView *webView;
id callbackObject;
SEL callbackSelector;
}

@property (nonatomic, retain) NSString *facebookClientID;
@property (nonatomic, retain) NSString *redirectUri;
@property (nonatomic, retain) NSString *accessToken;
@property (assign) id callbackObject;
@property (assign) SEL callbackSelector;
-(IBAction)goback;
- (void)authenticateUserWithCallbackObject:(id)anObject andSelector:(SEL)selector andExtendedPermissions:(NSString *)extended_permissions;

@end
