//
//  FollowingViewController.m
//  CancerCircleFinal
//
//  Created by Raminder on 17/05/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import "FollowingViewController.h"
#import "Login.h"
#import "searchedUserView.h"
#import "AppDelegate.h"
@interface FollowingViewController ()

@end

@implementation FollowingViewController
@synthesize getName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.navigationController.navigationBarHidden = NO;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.imageDownloadingQueue = [[NSOperationQueue alloc] init];
    self.imageDownloadingQueue.maxConcurrentOperationCount = 4;
    self.imageCache = [[NSCache alloc] init];
    
    AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
    delegate.tabId=@"following";
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"640x960bg.png"]];
   
    CGFloat titleWidth2=100;
    CGFloat titleWidth = MIN(titleWidth2, 300);
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 20)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Futura_Light" size:20];

    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=@"Followings";
    
    UINavigationBar *navBar = [[self navigationController] navigationBar];
 
    [navBar setTintColor:[[UIColor alloc] initWithRed:(0.0/255.0) green:(214.0/255.0) blue:(11.0/255.0) alpha:1.0]];
    
    self.navigationItem.titleView = label;
    [self getFollowings];
}
-(void)getFollowings
{
    NSString *followingStr=[NSString stringWithFormat:SERVER_URL@"/follow_name.do?username=%@",getName];
    NSURL *followingUrl=[NSURL URLWithString:followingStr];
    NSData *followingData=[NSData dataWithContentsOfURL:followingUrl];
    NSError* error;
    NSDictionary* followingJson = [NSJSONSerialization JSONObjectWithData:followingData options:kNilOptions error:&error];
    int jsonCount=[followingJson count];
    followingNameArray =[[NSMutableArray alloc]initWithCapacity:followingNameArray.count];
    followingStatusArray=[[NSMutableArray alloc]initWithCapacity:followingStatusArray.count];
    followingPicArray=[[NSMutableArray alloc]initWithCapacity:followingPicArray.count];
    for (int i=0;i<jsonCount-1;i++) {
        NSString *data=[@"data" stringByAppendingString:[NSString stringWithFormat:@"%d",i]];
        [followingNameArray addObject:[[followingJson objectForKey:data] valueForKey:@"Name"]];
        [followingStatusArray addObject:[[followingJson objectForKey:data] valueForKey:@"status"]];
        [followingPicArray addObject:[[followingJson objectForKey:data] valueForKey:@"picture"]];
        
    }
    [myTable reloadData];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [followingNameArray count];
}
- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Login *login=[[Login alloc]init];
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell1=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    myTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [myTable setBackgroundView:[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"307x410.png"]] autorelease]];
    if (cell1 == nil)
    {
        cell1 = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
    }
    cell1.backgroundView = [ [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cellWhiteBg.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ]autorelease];
    cell1.selectionStyle = UITableViewCellAccessoryNone;
    UIImageView * imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(10,10,40,40)];
    imageView2.layer.cornerRadius = 5;
    imageView2.clipsToBounds = YES;
    [cell1.contentView addSubview:imageView2];
    
    NSString *picUrlString = [followingPicArray objectAtIndex: indexPath.row];
    UIImage *cachedPic = [self.imageCache objectForKey:picUrlString];
    
    if (cachedPic)
    {
        imageView2.image = cachedPic;
        
    }
    else
    {
        // you'll want to initialize the image with some blank image as a placeholder
        
        //imgCell.imageView.image = [UIImage imageNamed:@"star.png"];
        imageView2.image = [UIImage imageNamed:@"editPic.png"];
        [self.imageDownloadingQueue addOperationWithBlock:^{
            NSURL *imageURL   = [NSURL URLWithString:picUrlString];
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            UIImage *image    = nil;
            if (imageData)
                image = [UIImage imageWithData:imageData];
            if (image)
            {
                // add the image to your cache
                
                [self.imageCache setObject:image forKey:picUrlString];
                
                // finally, update the user interface in the main queue
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    UITableViewCell *updateCell = [tableView cellForRowAtIndexPath:indexPath];
                    if (updateCell)
                        imageView2.image = image;
                    
                }];
            }
        }];
    }
    
    NSLog(@"valid");
    
    UILabel *nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(60,10,200,20)];
    nameLabel.textColor=[UIColor colorWithRed:0.2 green:0.6 blue:0 alpha:1];
    nameLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:15];
    nameLabel.text=[followingNameArray objectAtIndex:indexPath.row];
    [cell1.contentView addSubview:nameLabel];
    
    UILabel *palceLabel=[[UILabel alloc]initWithFrame:CGRectMake(60,25,200,20)];
    palceLabel.numberOfLines=0;
    palceLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:13];
    NSMutableAttributedString *attributed=[login atributedString:[followingStatusArray objectAtIndex:indexPath.row]];
    palceLabel.attributedText=attributed;
    [cell1.contentView addSubview:palceLabel];
    
    return cell1;
}
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self userButtonVideoClicked:[followingNameArray objectAtIndex:indexPath.row]];
}
-(void)userButtonVideoClicked:(NSString *)searchName
{
    AppDelegate *delegate=[[UIApplication sharedApplication]delegate];
    if ([searchName isEqualToString:delegate.mynameLogin]) {
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
    }
    else
    {
        NSLog(@"userbutton clicked");
        searchedUserView *searchedUser=[self.storyboard instantiateViewControllerWithIdentifier:@"searchedUser"];
        NSString *url = [NSString stringWithFormat:SERVER_URL@"/userinfo.do?username=%@",searchName];
        NSURL *urlrequest=[NSURL URLWithString:url];
        NSURLRequest *req=[[NSURLRequest alloc]initWithURL:urlrequest];
        NSURLResponse  *response = nil;
        NSError* error;
        [NSURLConnection sendSynchronousRequest: req returningResponse: &response error: &error];
        
        NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
        // NSString *stringData= [NSString stringWithContentsOfURL:urlrequest encoding:nil error:nil];
        // NSLog(@"Recieved data String &&&&&&&&&&&& %@",stringData);
        
        NSData *dataofStrig=[NSData dataWithContentsOfURL:urlrequest];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:dataofStrig options:kNilOptions error:&error];
        if (!json) {
            NSLog(@"Not Json");
        }
        
        NSLog(@"JSON format:- %@",json);
        searchedUser.getfollowers=[json objectForKey:@"followerCount"];
        searchedUser.getfollowing=[json objectForKey:@"followingCount"];
        NSString *userName=[json objectForKey:@"username"];
        searchedUser.getUserSearched=userName;
        NSString *status=[json objectForKey:@"status"];
        searchedUser.getStatusSearched=status;
        NSString *userPic=[json objectForKey:@"profile_image"];
        searchedUser.getImageSearched=userPic;
        searchedUser.getId=@"circle";
        [self.navigationController pushViewController:searchedUser animated:YES];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
