//
//  Registration.m
//  CancerCircleFirst
//
//  Created by Raminder on 11/01/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "Registration.h"
#import "Login.h"
#define kNavBarDefaultPosition CGPointMake(160, 42)

@implementation Registration
static CGFloat const kDefaultColorLayerOpacity = 0.1f;
static CGFloat const kSpaceToCoverStatusBars = 20.0f;
@synthesize transBack;

-(void)viewDidLoad
{
    _whoareyou =  [[NSArray alloc]initWithObjects: @"fighter", @"survivor",
               @"supporter", nil];
    
    
    transBack.dynamic = YES;
    transBack.tintColor = [UIColor blackColor];
    transBack.backgroundColor = [UIColor colorWithRed:(0.0/0.0) green:(0.0/0.0) blue:(0.0/0.0) alpha:1.0f];
    [transBack.layer displayIfNeeded]; //force immediate redraw
    transBack.contentMode = UIViewContentModeBottom;
    transBack.blurRadius = 20;
    transBack.opaque = NO;
    _customInput.backgroundColor = [[UIColor alloc] initWithRed:(255.0/255.0) green:(255.0/255.0) blue:(255.0/255.0) alpha:1.0];
    dobText.inputView = self.customInput;
    dobText.inputAccessoryView = self.accessoryView;
    whoText.inputView = self.whoInput;
    whoText.inputAccessoryView = self.accessoryView;
    self.customInput.datePickerMode = UIDatePickerModeDate;
    self.customInput.hidden = YES;
    self.accessoryView.hidden = YES;
    self.whoInput.hidden = YES;
    [myScroll setScrollEnabled:YES];
     myScroll.contentSize = CGSizeMake(myScroll.frame.size.width, 800);
    [super viewDidLoad];
    [warning setHidden:TRUE];
    [userImg setHidden:TRUE];
    [passImg setHidden:TRUE];
    [firstImg setHidden:TRUE];
    [lastImg setHidden:TRUE];
    [contactImg setHidden:TRUE];
    [mailImg setHidden:TRUE];
    [dobImg setHidden:TRUE];
    whoText.delegate = self;
    dobText.delegate = self;
    mailText.delegate = self;
    // myScroll.contentSize=CGSizeMake(320,600);
    // myScroll.pagingEnabled=YES;
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"640x960bg.png"]];
    bgImage.layer.cornerRadius=5;
    bgImage.clipsToBounds = YES;
    
    self.backView.layer.cornerRadius=5;
    self.backView.clipsToBounds = YES;
    myScroll.scrollEnabled = NO;
    
  
  
    UINavigationBar *navBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    
    
    navBar.barTintColor = [[UIColor alloc] initWithRed:(22.0/255.0) green:(171.0/255.0) blue:(30.0/255.0) alpha:1.0];
    navBar.translucent = YES;
    
    
   
    
    CGFloat titleWidth2=100;
    CGFloat titleWidth = MIN(titleWidth2, 300);
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 30)];
    label.backgroundColor = [UIColor clearColor];
    // label.font = [UIFont boldSystemFontOfSize:20.0];
    label.font=[UIFont fontWithName:@"Futura_Light" size:20];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text=@"Register";
    
    
    
    UIImage* image3 = [UIImage imageNamed:@"close.png"];
    CGRect frameimg = CGRectMake(0, 0,18,18);
   someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(goLogin)
         forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *backbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    
    
    UINavigationItem *buttonCarrier = [[UINavigationItem alloc]initWithTitle:@""];
    buttonCarrier.titleView = label;
    //Creating some buttons:
    
    
    
    //Putting the Buttons on the Carrier
    [buttonCarrier setLeftBarButtonItem:backbutton];
    
    
    //The NavigationBar accepts those "Carrier" (UINavigationItem) inside an Array
    NSArray *barItemArray = [[NSArray alloc]initWithObjects:buttonCarrier,nil];
    
    // Attaching the Array to the NavigationBar
    [navBar setItems:barItemArray];
    [someButton release];
    [self.view addSubview:navBar];

    
    flag=1;
 
    
    //self.navigationItem.title =@"Sign Up";
    
    _button.layer.cornerRadius = 5;
    _button.layer.masksToBounds = YES;
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _button.bounds;
    gradient.colors = [NSArray arrayWithObjects: (id)[[[UIColor alloc] initWithRed:(231.0/255.0) green:(248.0/255.0) blue:(227.0/255.0) alpha:1.0] CGColor], (id)[[[UIColor alloc] initWithRed:(200.0/255.0) green:(240.0/255.0) blue:(194.0/255.0) alpha:1.0] CGColor],nil];
    [_button.layer insertSublayer:gradient atIndex:0];
    
    for(UIImageView *imageView in self.textfields)
    {
        imageView.layer.cornerRadius = 3;
        imageView.layer.masksToBounds = YES;
        [imageView.layer setBorderWidth:1.0f];
        [imageView.layer setBorderColor:[[[UIColor alloc] initWithRed:(233.0/255.0) green:(233.0/255.0) blue:(233.0/255.0) alpha:1.0] CGColor]];
        [imageView setBackgroundColor:[[UIColor alloc] initWithRed:(247.0/255.0) green:(254.0/255.0) blue:(246.0/255.0)alpha:1.0]];
        
    }
    
    //userText.layer.borderColor=[[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1]CGColor];
    // userText.layer.borderWidth=0.5;
    [userText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    // mailText.layer.borderColor=[[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1]CGColor];
    // mailText.layer.borderWidth=0.5;
    [mailText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    //firstText.layer.borderColor=[[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1]CGColor];
    // firstText.layer.borderWidth=0.5;
    [firstText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    // lastText.layer.borderColor=[[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1]CGColor];
    // lastText.layer.borderWidth=0.5;
    [lastText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    // passText.layer.borderColor=[[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1]CGColor];
    // passText.layer.borderWidth=0.5;
    [passText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [conpassText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    //contactText.layer.borderColor=[[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1]CGColor];
    //contactText.layer.borderWidth=0.5;
    [contactText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    //dobText.layer.borderColor=[[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1]CGColor];
    //dobText.layer.borderWidth=0.5;
    [dobText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    
    [whoText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    
    passText.secureTextEntry = YES;
    conpassText.secureTextEntry = YES;
    contactText.keyboardType=UIKeyboardTypeDecimalPad;
    mailText.keyboardType = UIKeyboardTypeEmailAddress;
    
    //dobText.keyboardType=UIKeyboardTypeDecimalPad;
  
  
    

}
-(void)fadeout{
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options:UIViewAnimationCurveEaseInOut
                     animations:^ {
                         label.alpha = 0.0;
                         someButton.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         
                     }];
    
}
-(void)fadein{
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationCurveEaseInOut
                     animations:^ {
                         label.alpha = 1.0;
                         someButton.alpha = 1.0;
                     }
                     completion:^(BOOL finished) {
                         [someButton setHidden:NO];
                         [label setHidden:NO];
                         
                     }];
}

- (void)scrollViewToCenterOfScreen:(UIView *)theView {
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    CGFloat availableHeight = applicationFrame.size.height - 100;            // Remove area covered by keyboard
    
    CGFloat y = viewCenterY - availableHeight / 2.5;
    if (y < 0) {
        y = 0;
    }
    [registraionTable setContentOffset:CGPointMake(0, y) animated:YES];
    [registraionTable setScrollEnabled:YES];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [passText resignFirstResponder];
    [userText resignFirstResponder];
    [mailText resignFirstResponder];
    [contactText resignFirstResponder];
    [conpassText resignFirstResponder];
    [lastText resignFirstResponder];
    [firstText resignFirstResponder];
    [whoText resignFirstResponder];
    [dobText resignFirstResponder];
    [myScroll setContentOffset:CGPointMake(0, 0) animated:YES];
   
    
    activeField = nil;
    
    
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
     registraionTable.contentInset =  UIEdgeInsetsMake(0, 0, self.view.frame.size.height - 150, 0);
     [registraionTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:textField.tag inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];

    if (textField == dobText) {
        
        //show the view
        self.customInput.hidden = NO;
        self.accessoryView.hidden = NO;
      
        
    } else if (textField == whoText)
    {
        self.accessoryView.hidden = NO;
        self.whoInput.hidden = NO;
      
    }
  

    
    
    activeField = textField;
    
    return YES;
    
}
-(void) textFieldDidBeginEditing:(UITextField *)textField {
    registraionTable.contentInset =  UIEdgeInsetsMake(0, 0, self.view.frame.size.height - 150, 0);
    [registraionTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:textField.tag inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == userText) {
        
        [mailText becomeFirstResponder];
        
        
    } else if (textField == mailText){
        [firstText becomeFirstResponder];
    } else if (textField == firstText){
        [lastText becomeFirstResponder];
    } else if (textField == lastText){
        [contactText becomeFirstResponder];
    } else if (textField == contactText){
        [passText becomeFirstResponder];
    } else if (textField == passText){
        [conpassText becomeFirstResponder];
    }else if (textField == conpassText){
        [dobText becomeFirstResponder];
    } else if (textField == dobText){
        [whoText becomeFirstResponder];
        
        
        
    }
    return YES;
    registraionTable.contentInset =  UIEdgeInsetsMake(0, 0, 0, 0);

    
}

    

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == userText) {
        //availbility checking.
        
        NSString *url = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/AvailbilityCheck.jsp?type=username&value=%@", userText.text];
        
        
        
        NSLog(@"AvailbilityCheck  url is $$$$$$$$$$$$$$$$:- %@",url);
        NSURL *urlrequest=[NSURL URLWithString:url];
        
        NSData *data = [NSData dataWithContentsOfURL:urlrequest];
        
        NSString* result;
        result = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSLog(@"AvailbilityCheck  result is $$$$$$$$$$$$$$$$:- %@",result);
        
        
        if([result rangeOfString:@"true"].location == NSNotFound){//if available
            [userImg setHidden:FALSE];
            userImg.image = [UIImage imageNamed:@"tick.png"];
        } else {
            [userImg setHidden:FALSE];
            userImg.image = [UIImage imageNamed:@"error.png"];
        }
    }  else if (textField == mailText){
        //availbility checking.
        
        NSString *url = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/AvailbilityCheck.jsp?type=email_id&value=%@", mailText.text];
        
        
        
        NSLog(@"AvailbilityCheck  url is $$$$$$$$$$$$$$$$:- %@",url);
        NSURL *urlrequest=[NSURL URLWithString:url];
        
        NSData *data = [NSData dataWithContentsOfURL:urlrequest];
        
        NSString* result;
        result = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSLog(@"AvailbilityCheck  result is $$$$$$$$$$$$$$$$:- %@",result);
        
        
        if([result rangeOfString:@"true"].location == NSNotFound){//if available
            [userImg setHidden:FALSE];
            mailImg.image = [UIImage imageNamed:@"tick.png"];
        } else {
            [userImg setHidden:FALSE];
            mailImg.image = [UIImage imageNamed:@"error.png"];
        }

    
    
    
    }
    registraionTable.contentInset =  UIEdgeInsetsMake(0, 0, 0, 0);

}
-(void)goBack
{
    
}
-(IBAction)goLogin
{
    [self willMoveToParentViewController:nil];
 
     [self.view removeFromSuperview];
 
 
    /*       Login *login=[self.storyboard instantiateViewControllerWithIdentifier:@"login"];
     [[self navigationController] setNavigationBarHidden:YES animated:YES];
     [self.navigationController pushViewController:login animated:YES];*/
}

-(IBAction)hidekeyboard:(id)sender
{
    [conpassText resignFirstResponder];
    [passText resignFirstResponder];
    [userText resignFirstResponder];
    [mailText resignFirstResponder];
    [contactText resignFirstResponder];
    [lastText resignFirstResponder];
    [firstText resignFirstResponder];
    [whoText resignFirstResponder];
    [dobText resignFirstResponder];
    registraionTable.contentInset =  UIEdgeInsetsMake(0, 0, 0, 0);
}

-(void)hitRegistration
{
    NSString *url = [NSString stringWithFormat:@"http://ec2-23-20-216-216.compute-1.amazonaws.com:8080/SocialServer/register.do?first=%@&last=%@&day=%@&month=%@&year=%@&email=%@&phone=%@&name=%@&password=%@whoAreYou=%@",firstText.text,lastText.text,day,month,year,mailText.text,contactText.text,userText.text,passText.text,whoText.text];
    
    
    NSLog(@"String url is $$$$$$$$$$$$$$$$:- %@",url);
    NSURL *urlrequest=[NSURL URLWithString:url];
    NSLog(@"requested url is $$$$$$$$$$$$$$$$:- %@",urlrequest);
    NSData *data = [NSData dataWithContentsOfURL:urlrequest];
    NSLog(@"values in data is:- %@",data);
    if(data==nil)
    {
        UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Connection failed" message:@"Check intenet connection" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles: nil];
        [alert1 show];
    }
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions                        error:&error];
    NSLog(@"JSON format:- %@",json);
    authStr = [json objectForKey:@"param"];
    NSString *msg=[json objectForKey:@"message"];
    if([msg isEqualToString:@"You are already registered user "])
    {
        alert =[[UIAlertView alloc]initWithTitle:@"You are already registered user" message:@"" delegate:self cancelButtonTitle:@"OK"otherButtonTitles:nil];
        
    }
    NSLog(@"valu in elemant is:- %@",authStr);
    if([authStr isEqualToString:@"True"])
    {
        alert =[[UIAlertView alloc]initWithTitle:@"Registration Sucessful" message:@"" delegate:self cancelButtonTitle:@"OK"otherButtonTitles:nil];
        alert.tag=1;
        [alert show];
    }
    else if([authStr isEqualToString:@"False"])
    {
        
        alert =[[UIAlertView alloc]initWithTitle:@"Registration" message:@"failed" delegate:self cancelButtonTitle:@"Check Credentials"otherButtonTitles:nil];
        [alert show];
    }
    
}

- (IBAction)confirmPassword:(id)sender {
    if(![conpassText.text isEqualToString:passText.text])
    {
        [cpassImg setHidden:FALSE];
        NSLog(@"password fields match");
        cpassImg.image = [UIImage imageNamed:@"tick.png"];
       
    }
    else
    {
        [cpassImg setHidden:FALSE];
        cpassImg.image = [UIImage imageNamed:@"error.png"];
        
    }

}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(([userText.text length]>16)||([userText.text length]<2))
    {
        [userImg setHidden:FALSE];
        userImg.image = [UIImage imageNamed:@"error.png"];
    }
    else{
          [userImg setHidden:FALSE];
            userImg.image = [UIImage imageNamed:@"tick.png"];
         
        
    }
    if([passText.text length]<7)
    {
        
        [passImg setHidden:FALSE];
        passImg.image = [UIImage imageNamed:@"error.png"];
    }
    else
    {
        [passImg setHidden:FALSE];
        passImg.image = [UIImage imageNamed:@"tick.png"];
    }
    /*if([conpassText.text isEqualToString:passText.text])
    {
        [cpassImg setHidden:FALSE];
        NSLog(@"password fields match");
        cpassImg.image = [UIImage imageNamed:@"tick.png"];
          [cpassImg setImage:[UIImage imageNamed:@"tick.png"]];
        
    }
    else
    {
        [cpassImg setHidden:FALSE];
        cpassImg.image = [UIImage imageNamed:@"error.png"];
     
    }*/

    if([firstText.text length]<2)
    {
        [firstImg setHidden:FALSE];
        firstImg.image = [UIImage imageNamed:@"error.png"];
    }
    else
    {
        [firstImg setHidden:FALSE];
        firstImg.image = [UIImage imageNamed:@"tick.png"];
    }
    if([lastText.text length]<2)
    {
        [lastImg setHidden:FALSE];
        lastImg.image = [UIImage imageNamed:@"error.png"];
        
    }
    else
    {
        [lastImg setHidden:FALSE];
        lastImg.image = [UIImage imageNamed:@"tick.png"];
    }
    
    if([contactText.text length]<9)
    {
        [contactImg setHidden:FALSE];
        contactImg.image = [UIImage imageNamed:@"error.png"];
        
    }
    else{
        [contactImg setHidden:FALSE];
        contactImg.image = [UIImage imageNamed:@"tick.png"];
    }
    if(!([self validateEmailWithString:mailText.text]))
    {
        [mailImg setHidden:FALSE];
        mailImg.image = [UIImage imageNamed:@"error.png"];
    }
    else
    {
        [mailImg setHidden:FALSE];
        mailImg.image = [UIImage imageNamed:@"tick.png"];
    }
    dateArray=[dobText.text componentsSeparatedByString:@"/"];
    if([dateArray count]!=3)
    {
        [dobImg setHidden:FALSE];
        dobImg.image = [UIImage imageNamed:@"error.png"];
    }
    else
    {
        [dobImg setHidden:FALSE];
        dobImg.image = [UIImage imageNamed:@"tick.png"];
        day=[dateArray objectAtIndex:0];
        int dd=[day intValue];
        month=[dateArray objectAtIndex:1];
        int mm=[month intValue];
        year=[dateArray objectAtIndex:2];
        if((dd>31)&&(mm>12)&&([year length]!=4))
        {
            [dobImg setHidden:FALSE];
            dobImg.image = [UIImage imageNamed:@"error.png"];
        }
        else
        {
            [dobImg setHidden:FALSE];
            dobImg.image = [UIImage imageNamed:@"tick.png"];
        }
        if([whoText.text length]<2)
        {
            [whoText setHidden:FALSE];
            whoimg.image = [UIImage imageNamed:@"error.png"];
            
        }
        else{
            [whoText setHidden:FALSE];
            whoimg.image = [UIImage imageNamed:@"tick.png"];
        }

    }
    
    return YES;
}

-(IBAction)registered
{
    NSString * string = [NSString stringWithFormat:@"%c", [self validateEmailWithString:mailText.text]];
    NSLog(@"#################### -- %@",string);
    if(([userText.text length]>16)||([userText.text length]<1))
    {
        [userImg setHidden:FALSE];
        userImg.image = [UIImage imageNamed:@"error.png"];
    }
    else{
        [userImg setHidden:FALSE];
        userImg.image = [UIImage imageNamed:@"tick.png"];
    }
    if([passText.text length]<6)
    {
        [passImg setHidden:FALSE];
        passImg.image = [UIImage imageNamed:@"error.png"];
        
    }
    else
    {
        [passImg setHidden:FALSE];
        passImg.image = [UIImage imageNamed:@"tick.png"];
    }
   /* if([conpassText.text isEqualToString:passText.text])
    {
        [cpassImg setHidden:FALSE];
        NSLog(@"password fields match");
        cpassImg.image = [UIImage imageNamed:@"tick.png"];
        [cpassImg setImage:[UIImage imageNamed:@"tick.png"]];
       
    }
    else
    {
        [cpassImg setHidden:FALSE];
        cpassImg.image = [UIImage imageNamed:@"error.png"];
        
    }*/
    
    if([firstText.text length]==0)
    {
        [firstImg setHidden:FALSE];
        firstImg.image = [UIImage imageNamed:@"error.png"];
        
    }
    else
    {
        [firstImg setHidden:FALSE];
        firstImg.image = [UIImage imageNamed:@"tick.png"];
    }
    if([lastText.text length]==0)
    {
        [lastImg setHidden:FALSE];
        lastImg.image = [UIImage imageNamed:@"error.png"];
        
    }
    else
    {
        [lastImg setHidden:FALSE];
        lastImg.image = [UIImage imageNamed:@"tick.png"];
    }
    
    if([contactText.text length]<9)
    {
        [contactImg setHidden:FALSE];
        contactImg.image = [UIImage imageNamed:@"error.png"];
        
    }
    else{
        [contactImg setHidden:FALSE];
        contactImg.image = [UIImage imageNamed:@"tick.png"];
    }
    if(!([self validateEmailWithString:mailText.text]))
    {
        [mailImg setHidden:FALSE];
        mailImg.image = [UIImage imageNamed:@"error.png"];
    }
    else
    {
        [mailImg setHidden:FALSE];
        mailImg.image = [UIImage imageNamed:@"tick.png"];
    }
    dateArray=[dobText.text componentsSeparatedByString:@"/"];
    if([dateArray count]!=3)
    {
        [dobImg setHidden:FALSE];
        dobImg.image = [UIImage imageNamed:@"error.png"];
    }
    else
    {
        [dobImg setHidden:FALSE];
        dobImg.image = [UIImage imageNamed:@"tick.png"];
        day=[dateArray objectAtIndex:0];
        int dd=[day intValue];
        month=[dateArray objectAtIndex:1];
        int mm=[month intValue];
        year=[dateArray objectAtIndex:2];
        if((dd>31)&&(mm>12)&&([year length]!=4))
        {
            [dobImg setHidden:FALSE];
            dobImg.image = [UIImage imageNamed:@"error.png"];
        }
        else
        {
            [dobImg setHidden:FALSE];
            dobImg.image = [UIImage imageNamed:@"tick.png"];
        }
        if([whoText.text length]==0)
        {
            [whoText setHidden:FALSE];
            whoimg.image = [UIImage imageNamed:@"error.png"];
            
        }
        else{
            [whoText setHidden:FALSE];
            whoimg.image = [UIImage imageNamed:@"tick.png"];
        }
    }
    if (([userText.text length]<17)&&([userText.text length]>2)&&([passText.text length]>7)&&([contactText.text length]>=10)&&([firstText.text length]>2)&&([lastText.text length]>2)&&([dateArray count]==3)&&([self validateEmailWithString:mailText.text])&&([whoText.text length]>2))
    {
        [self hitRegistration];
    }
    else
    {
        [warning setHidden:FALSE];
        /*alert =[[UIAlertView alloc]initWithTitle:@"Failed" message:@"Check Credentials with red circle" delegate:self cancelButtonTitle:@"OK"otherButtonTitles:nil];
         [alert show];*/
    }
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(alert.tag==1)
    {
        if (buttonIndex == 0) {
             [self.view removeFromSuperview];
            //Login *login = [self.storyboard instantiateViewControllerWithIdentifier:@"login"];
            //[self.navigationController pushViewController:login animated:YES];
        }
        else{
            
        }
    }
    else{
        if (buttonIndex == 0) {
            [self viewDidLoad];
        }
        /* else if (buttonIndex == 1) {
         Registration *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"registration"];
         [self.navigationController pushViewController:detail animated:YES];
         
         }*/
    }
}

- (IBAction)dateChanged:(id)sender {
    UIDatePicker *picker = (UIDatePicker *)sender;
    
    NSDateFormatter  *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"MM/dd/YYYY"];
    NSDate *selectedDate = [picker date];
    NSString *recordDate = [dateFormatter stringFromDate:selectedDate];
    dobText.text = recordDate;
    
    
    
}
- (IBAction)whoareyou:(id)sender{
    NSInteger selectedRow = [_whoInput selectedRowInComponent:0];
    NSString *selectedPickerRow=[_whoareyou objectAtIndex:selectedRow];
    whoText.text = selectedPickerRow;
}
- (IBAction)doneEditing:(id)sender {
    [dobText resignFirstResponder];
    [whoText resignFirstResponder];
}
- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    return _whoareyou.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    return [_whoareyou objectAtIndex:row];

}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    
    [whoText setText:[_whoareyou objectAtIndex:row]];
}
- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
        return YES;
    }
    
    [txtView resignFirstResponder];
    registraionTable.contentInset =  UIEdgeInsetsMake(0, 0, 0, 0);
    return NO;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 12;
    
}
- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 9){
        return 100;
    }
   else if(indexPath.row == 11)
    {
        return 120;
    }else {
        return 50;
    }
}
- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    
    static NSString *CellIdentifier = @"userCell";
    static NSString *CellIdentifier2 = @"emailCell";
    static NSString *CellIdentifier3 = @"firstCell";
    static NSString *CellIdentifier4 = @"lastCell";
    static NSString *CellIdentifier5 = @"phoneCell";
    static NSString *CellIdentifier6 = @"passCell";
    static NSString *CellIdentifier7 = @"CpassCell";
    static NSString *CellIdentifier8 = @"dobCell";
    static NSString *CellIdentifier9 = @"whoCell";
    static NSString *CellIdentifier10 = @"tellCell";
    static NSString *CellIdentifier11 = @"regCell";
    static NSString *CellIdentifier12 = @"tocCell";
    
    registraionTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [registraionTable setBackgroundColor:[UIColor clearColor]];
    
  
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
   
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
    }
    
    
    if(indexPath.row == 0){
        
        self.userCell = [tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!self.userCell)
        {
            
            self.userCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            self.userCell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, 100, 20)];
            desc.text = @"your username";
            desc.font = [UIFont systemFontOfSize:10];
            desc.textColor = [UIColor blackColor];
            desc.textAlignment = NSTextAlignmentRight;
            [self.userCell.contentView addSubview:desc];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(110, 10, 180, 30)];
            imageView.layer.cornerRadius = 3;
            imageView.layer.masksToBounds = YES;
            [imageView.layer setBorderWidth:1.0f];
            [imageView.layer setBorderColor:[[[UIColor alloc] initWithRed:(233.0/255.0) green:(233.0/255.0) blue:(233.0/255.0) alpha:1.0] CGColor]];
            [imageView setBackgroundColor:[[UIColor alloc] initWithRed:(247.0/255.0) green:(254.0/255.0) blue:(246.0/255.0)alpha:1.0]];
            [self.userCell.contentView addSubview:imageView];
            
            userText = [[UITextField alloc] initWithFrame:CGRectMake(117, 15, 170, 20)];
            userText.backgroundColor = [UIColor clearColor];
            [userText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
            [self.userCell.contentView addSubview:userText];
            
            
            userImg = [[UIImageView alloc]initWithFrame:CGRectMake(265, 16, 18, 18)];
            userImg.image = [UIImage imageNamed:@"tick.png"];
            [self.userCell.contentView addSubview:userImg];
            userImg.hidden = YES;
            
            
            userText.delegate = self;
    
 
            
            
            
        }
        
        
        return self.userCell;
    }
    else if(indexPath.row ==  1){
        
        self.emailCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (!self.emailCell)
        {
            
            self.emailCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            self.emailCell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, 100, 20)];
            desc.text = @"your email address";
            desc.font = [UIFont systemFontOfSize:10];
            desc.textColor = [UIColor blackColor];
            desc.textAlignment = NSTextAlignmentRight;
            [self.emailCell.contentView addSubview:desc];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(110, 10, 180, 30)];
            imageView.layer.cornerRadius = 3;
            imageView.layer.masksToBounds = YES;
            [imageView.layer setBorderWidth:1.0f];
            [imageView.layer setBorderColor:[[[UIColor alloc] initWithRed:(233.0/255.0) green:(233.0/255.0) blue:(233.0/255.0) alpha:1.0] CGColor]];
            [imageView setBackgroundColor:[[UIColor alloc] initWithRed:(247.0/255.0) green:(254.0/255.0) blue:(246.0/255.0)alpha:1.0]];
            [self.emailCell.contentView addSubview:imageView];
            
            mailText = [[UITextField alloc] initWithFrame:CGRectMake(117, 15, 170, 20)];
            mailText.backgroundColor = [UIColor clearColor];
            [mailText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
            [self.emailCell.contentView addSubview:mailText];
            
            
            mailImg = [[UIImageView alloc]initWithFrame:CGRectMake(265, 16, 18, 18)];
            mailImg.image = [UIImage imageNamed:@"tick.png"];
            [self.emailCell.contentView addSubview:mailImg];
            mailImg.hidden = YES;
            
            mailText.keyboardType = UIKeyboardTypeEmailAddress;
            mailText.delegate = self;
          
        }
        
        return self.emailCell;
    }
    else if(indexPath.row ==  2){
        
        self.firstCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier3];
        if (!self.firstCell)
        {
            
            self.firstCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            self.firstCell.selectionStyle = UITableViewCellSelectionStyleNone;
            UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, 100, 20)];
            desc.text = @"your first name";
            desc.font = [UIFont systemFontOfSize:10];
            desc.textColor = [UIColor blackColor];
            desc.textAlignment = NSTextAlignmentRight;
            [self.firstCell.contentView addSubview:desc];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(110, 10, 180, 30)];
            imageView.layer.cornerRadius = 3;
            imageView.layer.masksToBounds = YES;
            [imageView.layer setBorderWidth:1.0f];
            [imageView.layer setBorderColor:[[[UIColor alloc] initWithRed:(233.0/255.0) green:(233.0/255.0) blue:(233.0/255.0) alpha:1.0] CGColor]];
            [imageView setBackgroundColor:[[UIColor alloc] initWithRed:(247.0/255.0) green:(254.0/255.0) blue:(246.0/255.0)alpha:1.0]];
            [self.firstCell.contentView addSubview:imageView];
            
            firstText = [[UITextField alloc] initWithFrame:CGRectMake(117, 15, 170, 20)];
            firstText.backgroundColor = [UIColor clearColor];
            [firstText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
            [self.firstCell.contentView addSubview:firstText];
            
            
            firstImg = [[UIImageView alloc]initWithFrame:CGRectMake(265, 16, 18, 18)];
            firstImg.image = [UIImage imageNamed:@"tick.png"];
            [self.firstCell.contentView addSubview:firstImg];
            firstImg.hidden = YES;
            
            firstText.delegate = self;
            
            
        }
        
        return self.firstCell;
    }
    else if(indexPath.row ==  3){
        
        self.lastCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
        if (!self.lastCell)
        {
            
            self.lastCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            self.lastCell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, 100, 20)];
            desc.text = @"your last name";
            desc.font = [UIFont systemFontOfSize:10];
            desc.textColor = [UIColor blackColor];
            desc.textAlignment = NSTextAlignmentRight;
            [self.lastCell.contentView addSubview:desc];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(110, 10, 180, 30)];
            imageView.layer.cornerRadius = 3;
            imageView.layer.masksToBounds = YES;
            [imageView.layer setBorderWidth:1.0f];
            [imageView.layer setBorderColor:[[[UIColor alloc] initWithRed:(233.0/255.0) green:(233.0/255.0) blue:(233.0/255.0) alpha:1.0] CGColor]];
            [imageView setBackgroundColor:[[UIColor alloc] initWithRed:(247.0/255.0) green:(254.0/255.0) blue:(246.0/255.0)alpha:1.0]];
            [self.lastCell.contentView addSubview:imageView];
            
            lastText = [[UITextField alloc] initWithFrame:CGRectMake(117, 15, 170, 20)];
            lastText.backgroundColor = [UIColor clearColor];
            [lastText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
            [self.lastCell.contentView addSubview:lastText];
            
            
            lastImg = [[UIImageView alloc]initWithFrame:CGRectMake(265, 16, 18, 18)];
            lastImg.image = [UIImage imageNamed:@"tick.png"];
            [self.lastCell.contentView addSubview:lastImg];
            lastImg.hidden = YES;
            
            lastText.delegate = self;
            
        }
        
        return self.lastCell;
    }
    else if(indexPath.row ==  4){
        
        self.phoneCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier5];
        if (!self.phoneCell)
        {
            
            self.phoneCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            self.phoneCell.selectionStyle = UITableViewCellSelectionStyleNone;
           
            UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, 100, 20)];
            desc.text = @"your phone number";
            desc.font = [UIFont systemFontOfSize:10];
            desc.textColor = [UIColor blackColor];
            desc.textAlignment = NSTextAlignmentRight;
            [self.phoneCell.contentView addSubview:desc];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(110, 10, 180, 30)];
            imageView.layer.cornerRadius = 3;
            imageView.layer.masksToBounds = YES;
            [imageView.layer setBorderWidth:1.0f];
            [imageView.layer setBorderColor:[[[UIColor alloc] initWithRed:(233.0/255.0) green:(233.0/255.0) blue:(233.0/255.0) alpha:1.0] CGColor]];
            [imageView setBackgroundColor:[[UIColor alloc] initWithRed:(247.0/255.0) green:(254.0/255.0) blue:(246.0/255.0)alpha:1.0]];
            [self.phoneCell.contentView addSubview:imageView];
            
            contactText = [[UITextField alloc] initWithFrame:CGRectMake(117, 15, 170, 20)];
            contactText.backgroundColor = [UIColor clearColor];
            [contactText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
            [self.phoneCell.contentView addSubview:contactText];
            
            contactImg = [[UIImageView alloc]initWithFrame:CGRectMake(265, 16, 18, 18)];
            contactImg.image = [UIImage imageNamed:@"tick.png"];
            [self.phoneCell.contentView addSubview:contactImg];
            contactImg.hidden = YES;
            contactText.keyboardType = UIKeyboardTypePhonePad;
            contactText.delegate = self;

        }
        
        return self.phoneCell;
    }
    else if(indexPath.row ==  5){
        
        self.passCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier6];
        if (!self.passCell)
        {
            
            self.passCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            self.passCell.selectionStyle = UITableViewCellSelectionStyleNone;
            UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, 100, 20)];
            desc.text = @"your password";
            desc.font = [UIFont systemFontOfSize:10];
            desc.textColor = [UIColor blackColor];
            desc.textAlignment = NSTextAlignmentRight;
            [self.passCell.contentView addSubview:desc];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(110, 10, 180, 30)];
            imageView.layer.cornerRadius = 3;
            imageView.layer.masksToBounds = YES;
            [imageView.layer setBorderWidth:1.0f];
            [imageView.layer setBorderColor:[[[UIColor alloc] initWithRed:(233.0/255.0) green:(233.0/255.0) blue:(233.0/255.0) alpha:1.0] CGColor]];
            [imageView setBackgroundColor:[[UIColor alloc] initWithRed:(247.0/255.0) green:(254.0/255.0) blue:(246.0/255.0)alpha:1.0]];
            [self.passCell.contentView addSubview:imageView];
            
            passText = [[UITextField alloc] initWithFrame:CGRectMake(117, 15, 170, 20)];
            passText.backgroundColor = [UIColor clearColor];
            [passText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
            passText.secureTextEntry = YES;
            [self.passCell.contentView addSubview:passText];
            
            
            passImg = [[UIImageView alloc]initWithFrame:CGRectMake(265, 16, 18, 18)];
            passImg.image = [UIImage imageNamed:@"tick.png"];
            [self.passCell.contentView addSubview:passImg];
            passImg.hidden = YES;
            passText.delegate = self;
        }
        
        return self.passCell;
    }
  else if(indexPath.row ==  6){
            
      self.CpassCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier7];
            if (!self.CpassCell)
            {
                
                self.CpassCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
                self.CpassCell.selectionStyle = UITableViewCellSelectionStyleNone;
                UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, 100, 20)];
                desc.text = @"confirm password";
                desc.font = [UIFont systemFontOfSize:10];
                desc.textColor = [UIColor blackColor];
                desc.textAlignment = NSTextAlignmentRight;
                [self.CpassCell.contentView addSubview:desc];
                
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(110, 10, 180, 30)];
                imageView.layer.cornerRadius = 3;
                imageView.layer.masksToBounds = YES;
                [imageView.layer setBorderWidth:1.0f];
                [imageView.layer setBorderColor:[[[UIColor alloc] initWithRed:(233.0/255.0) green:(233.0/255.0) blue:(233.0/255.0) alpha:1.0] CGColor]];
                [imageView setBackgroundColor:[[UIColor alloc] initWithRed:(247.0/255.0) green:(254.0/255.0) blue:(246.0/255.0)alpha:1.0]];
                [self.CpassCell.contentView addSubview:imageView];
                
                conpassText = [[UITextField alloc] initWithFrame:CGRectMake(117, 15, 170, 20)];
                conpassText.backgroundColor = [UIColor clearColor];
                [conpassText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
                conpassText.secureTextEntry = YES;
                [self.CpassCell.contentView addSubview:conpassText];
                
                
                cpassImg = [[UIImageView alloc]initWithFrame:CGRectMake(265, 16, 18, 18)];
                cpassImg.image = [UIImage imageNamed:@"tick.png"];
                [self.CpassCell.contentView addSubview:cpassImg];
                cpassImg.hidden = YES;
                conpassText.delegate = self;
        }

        
        return self.CpassCell;
    }
  else if(indexPath.row ==  7){
      
      self.dobCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier8];
      if (!self.dobCell)
      {
         
          self.dobCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
           self.dobCell.selectionStyle = UITableViewCellSelectionStyleNone;
          UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, 100, 20)];
          desc.text = @"date of birth";
          desc.font = [UIFont systemFontOfSize:10];
          desc.textColor = [UIColor blackColor];
          desc.textAlignment = NSTextAlignmentRight;
          [self.dobCell.contentView addSubview:desc];
          
          UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(110, 10, 180, 30)];
          imageView.layer.cornerRadius = 3;
          imageView.layer.masksToBounds = YES;
          [imageView.layer setBorderWidth:1.0f];
          [imageView.layer setBorderColor:[[[UIColor alloc] initWithRed:(233.0/255.0) green:(233.0/255.0) blue:(233.0/255.0) alpha:1.0] CGColor]];
          [imageView setBackgroundColor:[[UIColor alloc] initWithRed:(247.0/255.0) green:(254.0/255.0) blue:(246.0/255.0)alpha:1.0]];
          [self.dobCell.contentView addSubview:imageView];
          
          dobText = [[UITextField alloc] initWithFrame:CGRectMake(117, 15, 170, 20)];
          dobText.backgroundColor = [UIColor clearColor];
          [dobText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
          [self.dobCell.contentView addSubview:dobText];
          
          
          dobImg = [[UIImageView alloc]initWithFrame:CGRectMake(265, 16, 18, 18)];
          dobImg.image = [UIImage imageNamed:@"tick.png"];
          [self.dobCell.contentView addSubview:dobImg];
          dobImg.hidden = YES;
          _customInput.backgroundColor = [[UIColor alloc] initWithRed:(255.0/255.0) green:(255.0/255.0) blue:(255.0/255.0) alpha:1.0];
          dobText.inputView = self.customInput;
          dobText.inputAccessoryView = self.accessoryView;
          dobText.delegate = self;
          
      }
      
      
      return self.dobCell;
  }
  else if(indexPath.row ==  8){
      
      self.whoCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier9];
      if (!self.whoCell)
      {
          
          self.whoCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
          self.whoCell.selectionStyle = UITableViewCellSelectionStyleNone;
          UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, 100, 20)];
          desc.text = @"who are you";
          desc.font = [UIFont systemFontOfSize:10];
          desc.textColor = [UIColor blackColor];
          desc.textAlignment = NSTextAlignmentRight;
          [self.whoCell.contentView addSubview:desc];
          
          UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(110, 10, 180, 30)];
          imageView.layer.cornerRadius = 3;
          imageView.layer.masksToBounds = YES;
          [imageView.layer setBorderWidth:1.0f];
          [imageView.layer setBorderColor:[[[UIColor alloc] initWithRed:(233.0/255.0) green:(233.0/255.0) blue:(233.0/255.0) alpha:1.0] CGColor]];
          [imageView setBackgroundColor:[[UIColor alloc] initWithRed:(247.0/255.0) green:(254.0/255.0) blue:(246.0/255.0)alpha:1.0]];
          [self.whoCell.contentView addSubview:imageView];
          
          whoText = [[UITextField alloc] initWithFrame:CGRectMake(117, 15, 170, 20)];
          whoText.backgroundColor = [UIColor clearColor];
          [whoText setValue:[UIColor colorWithRed:0.529 green:0.761 blue:0.494 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
          [self.whoCell.contentView addSubview:whoText];
          
          
          whoimg = [[UIImageView alloc]initWithFrame:CGRectMake(265, 16, 18, 18)];
          whoimg.image = [UIImage imageNamed:@"tick.png"];
          [self.whoCell.contentView addSubview:whoimg];
          whoimg.hidden = YES;
          whoText.inputView = self.whoInput;
          whoText.inputAccessoryView = self.accessoryView;
          whoText.delegate = self;
      }
      
      
      return self.whoCell;
  }
    
  else if(indexPath.row ==  9){
      
      self.tellCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier10];
      if (!self.tellCell)
      {
         
          self.tellCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
           self.tellCell.selectionStyle = UITableViewCellSelectionStyleNone;
          UILabel *desc = [[UILabel alloc] initWithFrame:CGRectMake(0, 12, 100, 41)];
          desc.text = @"tell us what you're fighting / have survied";
          desc.font = [UIFont systemFontOfSize:10];
          desc.lineBreakMode = NSLineBreakByWordWrapping;
          desc.numberOfLines = 3;
          desc.textColor = [UIColor blackColor];
          desc.textAlignment = NSTextAlignmentRight;
          [self.tellCell.contentView addSubview:desc];
          
          UILabel *desc2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 49, 100, 38)];
          desc2.text = @"put a comma between each word, this will generate seachable tags";
          desc2.font = [UIFont systemFontOfSize:8];
          desc2.lineBreakMode = NSLineBreakByWordWrapping;
          desc2.numberOfLines = 3;
          desc2.textColor = [UIColor blackColor];
          desc2.textAlignment = NSTextAlignmentRight;
          [self.tellCell.contentView addSubview:desc2];
          
          UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(110, 12, 180, 75)];
          imageView.layer.cornerRadius = 3;
          imageView.layer.masksToBounds = YES;
          [imageView.layer setBorderWidth:1.0f];
          [imageView.layer setBorderColor:[[[UIColor alloc] initWithRed:(233.0/255.0) green:(233.0/255.0) blue:(233.0/255.0) alpha:1.0] CGColor]];
          [imageView setBackgroundColor:[[UIColor alloc] initWithRed:(247.0/255.0) green:(254.0/255.0) blue:(246.0/255.0)alpha:1.0]];
          [self.tellCell.contentView addSubview:imageView];
          
          tellText = [[UITextView alloc] initWithFrame:CGRectMake(115, 17, 170, 65)];
          tellText.backgroundColor = [UIColor clearColor];
          tellText.editable = YES;
          [self.tellCell.contentView addSubview:tellText];
          
          
          tellImg = [[UIImageView alloc]initWithFrame:CGRectMake(265, 16, 18, 18)];
          tellImg.image = [UIImage imageNamed:@"tick.png"];
          [self.tellCell.contentView addSubview:tellImg];
          tellImg.hidden = YES;
          tellText.delegate = self;
      }
      
      
      return self.tellCell;
  }
  else if(indexPath.row ==  10){
      
      self.regCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier11];
      if (!self.regCell)
      {
          self.regCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
          UIButton *desc = [[UIButton alloc] initWithFrame:CGRectMake(100, 5, 100, 40)];
          [desc addTarget:self action:@selector(registered) forControlEvents:UIControlEventTouchUpInside];
          [desc setTitle:@"Register" forState:UIControlStateNormal];
          desc.titleLabel.textColor = [UIColor whiteColor];
          desc.titleLabel.textAlignment = NSTextAlignmentCenter;
          [self.regCell.contentView addSubview:desc];
          desc.layer.cornerRadius = 5;
          desc.layer.masksToBounds = YES;
          
         
          [desc.layer setBackgroundColor:[UIColor colorWithRed:(200.0/255.0) green:(240.0/255.0) blue:(194.0/255.0) alpha:1.0].CGColor];
          
          
      }
      
      
      return self.regCell;
  }
  else if(indexPath.row ==  11){
      
      self.tocCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier12];
      if (!self.tocCell)
      {
          self.tocCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
          UITextView *toc = [[UITextView alloc] initWithFrame:CGRectMake(10, 0, 280, 105)];
          toc.scrollEnabled = NO;
          toc.text = @"by clicking register you are confirming that you have read and agreed to our combined terms of use and privacy policy. you will receive an email confirmation as well as a text confirmation code this is to ensure you are a human being and to ensure that our network is full of relevant information and not spam";
          toc.textAlignment = NSTextAlignmentCenter;
          [self.tocCell addSubview:toc];
          
      }
      
      
      return self.tocCell;
  }



    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    if (![self.userCell selectionStyle] == UITableViewCellSelectionStyleNone) {
        // Handle tap code here
    }
    if (![self.emailCell selectionStyle] == UITableViewCellSelectionStyleNone) {
        // Handle tap code here
    }
    if (![self.firstCell selectionStyle] == UITableViewCellSelectionStyleNone) {
        // Handle tap code here
    }
    if (![self.lastCell selectionStyle] == UITableViewCellSelectionStyleNone) {
        // Handle tap code here
    }
    if (![self.phoneCell selectionStyle] == UITableViewCellSelectionStyleNone) {
        // Handle tap code here
    }
    if (![self.passCell selectionStyle] == UITableViewCellSelectionStyleNone) {
        // Handle tap code here
    }
    if (![self.CpassCell selectionStyle] == UITableViewCellSelectionStyleNone) {
        // Handle tap code here
    }
    if (![self.dobCell selectionStyle] == UITableViewCellSelectionStyleNone) {
        // Handle tap code here
    }
    if (![self.whoCell selectionStyle] == UITableViewCellSelectionStyleNone) {
        // Handle tap code here
    }
    if (![self.tellCell selectionStyle] == UITableViewCellSelectionStyleNone) {
        // Handle tap code here
    }
}

- (void)dealloc {
   
    [_textfields release];
    [_button release];
    [whoimg release];
    [whoText release];
    [_whoInput release];
    [conpassText release];
    [cpassImg release];
    [transBack release];
   
    [registraionTable release];
    [_backView release];
    [super dealloc];
}
@end
