//
//  image2Cell.m
//  CancerCircleFinal
//
//  Created by Raminder on 24/05/13.
//  Copyright (c) 2013 Raminder. All rights reserved.
//

#import "image2Cell.h"

@implementation image2Cell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void) layoutSubviews
{
    [super layoutSubviews];
    
    self.imageView.frame = CGRectMake(0,0,320,285);
    //self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"imgCellbg.png"]];
    self.backgroundView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@""]];
      CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat x = [UIScreen mainScreen].bounds.origin.x;
    self.backgroundView.frame = CGRectMake(x+5,0,width-10,415);
    self.backgroundView.backgroundColor = [UIColor whiteColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
