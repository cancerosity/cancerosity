//
//  FBTableViewCell.h
//  Cancerosity
//
//  Created by Grant Bevan on 27/03/2014.
//  Copyright (c) 2014 Raminder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FBTableViewCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *fbName;

@end
