//
//  SettingButtons.h
//  CancerCircleFirst
//
//  Created by Raminder on 25/01/13.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "FBViewControllerLogin.h"
#import "FXBlurView.h"

@interface SettingButtons : UIViewController<UIActionSheetDelegate,UITextViewDelegate,UITextFieldDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    UIActivityIndicatorView *indicator;
    NSString *idAtSettings;
    IBOutlet UIImageView *profileImage;
    IBOutlet UIImageView *bgImage;
    IBOutlet UIButton *imageButton;
    NSString *username;
    IBOutlet UITextView *statusView;
    IBOutlet UITextField *dpName;
    IBOutlet UITextField *userName;
    IBOutlet UITextField *email;
    IBOutlet UITextField *share;
    IBOutlet UITextField *password;
    IBOutlet UITextField *contact;
    IBOutlet UIImageView *userNameImg;
    IBOutlet UIImageView *emailImg;
    IBOutlet UIImageView *shareImg;
    IBOutlet UIImageView *passwordImg;
    IBOutlet UIImageView *contactImg;
    IBOutlet UIScrollView *myScroll;
    IBOutlet UIImageView *dpNameImg;
    NSData * dataImage;
    NSString *profileStatus;
    int flag;
    UIAlertView *alertLimit;
    UIAlertView *alert;
    IBOutlet UISwitch *switchToggle;
    IBOutlet UISwitch *switchToggle2;
    IBOutlet UISwitch *switchToggle3;
    FBViewControllerLogin *fbGraph;
    IBOutlet UILabel *label;
    IBOutlet UIButton *someButton;
    
    UIImageView *circleRefresh;
    UIImageView *circleRefresh2;
    IBOutlet UITableView *settingsTable;
    
@protected
    __weak UIScrollView *_myScroll;  // we keep a weak pointer because, well, we don't need a strong pointer.
    CGFloat _scrollViewContentOffsetYThreshold;  // defines at what contentOffset of the above scrollView the navigationBar should start scrolling
}

@property (retain, nonatomic) IBOutlet UIButton *logoutButton;
@property (retain, nonatomic) IBOutlet FXBlurView *indiView;
@property (retain, nonatomic) IBOutletCollection(UIImageView) NSArray *textFields;
@property (retain, nonatomic) IBOutlet UIImageView *statusBack;
-(IBAction)selectImage;
-(IBAction)goLogin;
-(IBAction)logout;
-(IBAction)changeEmail;
-(IBAction)changeNumber;
-(IBAction)changePassword;
-(IBAction)switchButtonLocation;
-(IBAction)switchButtonEmail;
-(IBAction)switchButtonPrivate;
-(IBAction)shareSetting;
-(IBAction)hidekeyboard:(id)sender;
@property (nonatomic, retain) FBViewControllerLogin *fbGraph;
- (void)scrollViewDidScroll:(UIScrollView*)aScrollView;

@property (retain, nonatomic) IBOutlet UITableViewCell *ProfCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *AccCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *ShareCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *LNPCell;
@property (retain, nonatomic) IBOutlet UITableViewCell *LogCell;


@end
