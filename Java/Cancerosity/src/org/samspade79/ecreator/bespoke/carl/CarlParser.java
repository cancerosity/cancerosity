package org.samspade79.ecreator.bespoke.carl;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

/**
 * @author Stephen K Samuel samspade79@gmail.com 7 Jan 2010 17:12:24
 *
 * Parses a CARL feed from a Document
 *
 */
public class CarlParser {

	/**
	 * Parse a new CarlFeed from a supplied File representing a JDOM Document
	 *
	 * @throws IOException
	 * @throws JDOMException
	 */
	public CarlFeed parse(File file) throws JDOMException, IOException {

		Document doc = new SAXBuilder().build(file);
		return parse(doc);
	}

	/**
	 * Parse a new CarlFeed from a supplied JDOM Document
	 */
	public CarlFeed parse(Document doc) {

		CarlFeed feed = new CarlFeed();

		// root is the transfer element
		Element root = doc.getRootElement();

		Element chainrecord = root.getChild("chainrecord");
		String name = chainrecord.getChildText("name");
		feed.setName(name);

		List<Element> propertyElements = root.getChildren("property");
		for (Element propertyElement : propertyElements) {

			String status = propertyElement.getChildText("status");
			String propref = propertyElement.getChildText("propref");
			String propnumber = propertyElement.getChildText("propnumber");
			String proproad = propertyElement.getChildText("proproad");
			String propdistrict = propertyElement.getChildText("propdistrict");
			String proptown = propertyElement.getChildText("proptown");
			String proppostcode = propertyElement.getChildText("proppostcode");

			String brochurefrontimage2 = propertyElement.getChildText("brochurefrontimage2");
			String brochurefrontimage3 = propertyElement.getChildText("brochurefrontimage3");
			String brochurefrontimage4 = propertyElement.getChildText("brochurefrontimage4");

			String description = propertyElement.getChildText("description");
			String bullet1 = propertyElement.getChildText("bullet1");
			String bullet2 = propertyElement.getChildText("bullet2");
			String bullet3 = propertyElement.getChildText("bullet3");
			String bullet4 = propertyElement.getChildText("bullet4");
			String bullet5 = propertyElement.getChildText("bullet5");
			String bullet6 = propertyElement.getChildText("bullet6");

			int beds = 0;
			try {
				beds = Integer.parseInt(propertyElement.getChildText("beds"));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}

			int saleprice = 0;
			try {
				saleprice = Integer.parseInt(propertyElement.getChildText("saleprice"));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}

			int letprice = 0;
			try {
				letprice = Integer.parseInt(propertyElement.getChildText("letprice"));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}

			String type = propertyElement.getChildText("type");
			String rental = propertyElement.getChildText("rental");
			boolean visible = "YES".equalsIgnoreCase(propertyElement.getChildText("visible"));
			String style = propertyElement.getChildText("style");
			String tenure = propertyElement.getChildText("tenure");

			CarlProperty property = new CarlProperty();
			property.setStatus(status);
			property.setNumber(propnumber);
			property.setRoad(proproad);
			property.setTown(proptown);
			property.setPostcode(proppostcode);
			property.setReference(propref);
			property.setBullet1(bullet1);
			property.setBullet2(bullet2);
			property.setBullet3(bullet3);
			property.setBullet4(bullet4);
			property.setBullet5(bullet5);
			property.setBullet6(bullet6);
			property.setStyle(style);
			property.setTenure(tenure);
			property.setType(type);
			property.setBeds(beds);
			property.setDescription(description);
			property.setDistrict(propdistrict);
			property.setLetprice(letprice);
			property.setSaleprice(saleprice);
			property.setRental(rental);
			property.setVisible(visible);

			if (brochurefrontimage2 != null)
				property.addImage(brochurefrontimage2);
			if (brochurefrontimage3 != null)
				property.addImage(brochurefrontimage3);
			if (brochurefrontimage4 != null)
				property.addImage(brochurefrontimage4);

			feed.addProperty(property);

		}

		return feed;
	}

}

