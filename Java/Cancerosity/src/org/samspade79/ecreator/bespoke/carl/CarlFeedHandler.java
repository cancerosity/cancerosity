package org.samspade79.ecreator.bespoke.carl;

import java.io.File;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 8 Apr 2008 16:57:24
 *
 */
@Path("admin-carl-feed.do")
public class CarlFeedHandler extends AdminHandler {

	private int		accountId;
	private String	path;
	private String	imageDirectory;
	private ItemType	itemType;

	public CarlFeedHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Carl Feed Manual Run Handler", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Run feed"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb
						.append(new AdminRow("Path", "The full path, eg, /home/mysite/ to where the files are uploaded to.", new TextTag(context,
								"path", 40)));

				sb.append(new AdminRow("imageDirectory", "The full path to where images go on the site, eg /home/www/www.artfullodger.co.uk/images/.",
						new TextTag(context, "imageDirectory", 40)));

				sb.append(new AdminRow("Account id", "The id of the account to assign these properties too.", new TextTag(context, "accountId", 8)));

				sb.append(new AdminRow("Item Type", "Choose the item type to add properties as.", new SelectTag(context, "itemType", null, ItemType
						.getSelectionMap(context), "None")));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(CarlFeedHandler.class, "run", "POST"));

                commands();
				general();
				commands();

				sb.append("</form>");
				return sb.toString();
			}

		});

		return doc;
	}

	public Object run() throws ServletException {

		Item account = EntityObject.getInstance(context, Item.class, accountId);
		if (account == null) {
			throw new ServletException("Account not found");
		}

		try {

			CarlProcessor processor = new CarlProcessor(context);
			processor.setItemType(itemType);
			processor.setAccount(account);
			int images = processor.run(new File(path), imageDirectory);

			return new ActionDoc(context, "Feed ran." + images + " images copied", new Link(CarlFeedHandler.class));

		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
}
