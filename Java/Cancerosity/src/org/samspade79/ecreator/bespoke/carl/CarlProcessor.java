package org.samspade79.ecreator.bespoke.carl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.jdom.JDOMException;
import org.sevensoft.commons.simpleio.FileExistRule;
import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.commons.simpleio.SimpleZip;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author Stephen K Samuel samspade79@gmail.com 8 Jan 2010 14:05:15
 */
public class CarlProcessor {

    /**
     * The ItemType to create the properties on
     */
    private ItemType itemType;

    private final RequestContext context;

    private Item account;

    public CarlProcessor(RequestContext context) {
        this.context = context;
    }

    public ItemType getItemType() {
        return itemType;
    }

    private void integrate(CarlFeed feed) {

        for (CarlProperty property : feed.getProperties()) {

            Item item = Item.getByRef(context, itemType, property.getReference());
            if (item == null) {
                item = new Item(context, itemType, property.getNumber() + " " + property.getRoad(), "LIVE", null);
                item.setReference(property.getReference());
                item.setAccount(account);
            }

            item.setContent(property.getDescription());
            item.save();

            Attribute attribute = item.getAttribute("Bedrooms");
            if (attribute == null)
                attribute = item.getAttribute("Beds");
            if (attribute == null)
                attribute = itemType.addAttribute("Bedrooms");
            item.setAttributeValue(attribute, String.valueOf(property.getBeds()));

            attribute = item.getAttribute("Postcode");
            if (attribute == null)
                attribute = itemType.addAttribute("Postcode");
            item.setAttributeValue(attribute, property.getPostcode());

            String postcode = property.getPostcode();
            if (postcode != null) {

                postcode = postcode.replace(" ", "");
                if (postcode.length() == 7)
                    postcode = postcode.substring(0, 4);
                else if (postcode.length() == 6)
                    postcode = postcode.substring(0, 3);

                attribute = item.getAttribute("Postcode short");
                if (attribute == null)
                    attribute = itemType.addAttribute("Postcode short");
                item.setAttributeValue(attribute, postcode);

            }

            attribute = item.getAttribute("Property Type");
            if (attribute == null)
                attribute = itemType.addAttribute("Property Type");
            item.setAttributeValue(attribute, property.getStyle());

            attribute = item.getAttribute("Advert Type");
            if (attribute == null)
                attribute = itemType.addAttribute("Advert Type");
            if ("rental".equalsIgnoreCase(property.getTenure())) {
                item.setAttributeValue(attribute, "ResidentialRent");
            }

            attribute = item.getAttribute("Address");
            if (attribute == null)
                attribute = itemType.addAttribute("Address");

            StringBuilder sb = new StringBuilder();
            if (property.getDistrict() != null)
                sb.append(property.getDistrict() + "\n");
            if (property.getTown() != null)
                sb.append(property.getTown() + "\n");

            item.setAttributeValue(attribute, sb.toString());

            attribute = item.getAttribute("Price");
            if (attribute == null)
                attribute = itemType.addAttribute("Price");
            if (property.getSaleprice() > 0) {
                item.setAttributeValue(attribute, String.valueOf(property.getSaleprice()));
            } else if (property.getLetprice() > 0) {
                item.setAttributeValue(attribute, String.valueOf(property.getLetprice()));
            }

            attribute = item.getAttribute("Price Qualifier");
            if (attribute == null)
                attribute = itemType.addAttribute("Price Qualifier");
            if ("monthly".equalsIgnoreCase(property.getRental())) {
                attribute = itemType.addAttribute("Monthly");
            } else if ("weekly".equalsIgnoreCase(property.getRental())) {
                attribute = itemType.addAttribute("Weekly");
            }

            for (String image : property.getImages()) {
                try {
                    item.addImage(image, true, true, true);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ImageLimitException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public int run(File path, String imageDirectory) throws JDOMException, IOException {

        // unzip any zips and delete them
        boolean foundAZip = false;
        for (File file : path.listFiles()) {

            if (file.getName().toLowerCase().endsWith(".zip")) {

                SimpleZip.extractToDir(file, path, true, null, FileExistRule.Skip);
                foundAZip = true;
                file.delete();

            }
        }

        if (foundAZip == false) {
            throw new FileNotFoundException("No zip files where located in: " + path);
        }

        // move all images first
        int images = 0;

        for (File file : path.listFiles()) {
            if (file.getName().toLowerCase().endsWith(".jpg")) {

                try {

                    SimpleFile.move(file, new File(imageDirectory + file.getName()));
                    images++;

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        // now process the XML files
        boolean foundAnXML = false;
        for (File file : path.listFiles(new XmlFilenameFillter())) {

            CarlParser parser = new CarlParser();
            CarlFeed feed = parser.parse(file);

            integrate(feed);
            file.delete();
            foundAnXML = true;
        }

        if (foundAnXML == false) {
            throw new FileNotFoundException("No XML files where located in: " + path);
        }

        return images;
    }

    /**
     *
     */
    public void setAccount(Item account) {
        this.account = account;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }
}
