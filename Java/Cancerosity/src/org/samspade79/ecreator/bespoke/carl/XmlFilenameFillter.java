package org.samspade79.ecreator.bespoke.carl;

import java.io.File;
import java.io.FilenameFilter;

/**
 * @author Stephen K Samuel samspade79@gmail.com 8 Jan 2010 14:07:00
 *
 */
public class XmlFilenameFillter implements FilenameFilter {

	public boolean accept(File dir, String name) {
		return name.toLowerCase().endsWith(".xml");
	}

}
