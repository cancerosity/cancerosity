package org.samspade79.ecreator.bespoke.carl;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Stephen K Samuel samspade79@gmail.com 7 Jan 2010 17:12:38
 *
 *
 *         status>A</> <>0000643</> <message /> <propname /> <>103</propnumber> <>Skellwith Road</proproad> <propdistrict>Grimesthorpe</propdistrict>
 *         <proptown>Sheffield</proptown>
 *
 *
 *         <propknownasarea /> <salepremsg /> <currency>GBP</currency> <saleprice>0</saleprice> <letprice>495</letprice> <salepostmsg /> <preletmsg />
 *         <postletmsg /> <description>This 4 Bedroom MID TERRACE HOUSE is conveniently situated close to MEADOWHALL, NORTHERN GENERAL HOSPITAL, CITY
 *         CENTRE and links to the M1. The property is deceptively SPACIOUS and WELL PROPORTIONED and would ideally suit FAMILY/SHARERS.
 *
 *         3 double bedrooms, 1 single Separate living area Spacious Family Room/Dining Room Modern kitchen with In Built Fridge/freer and separate
 *         washing Machine Family Bathroom Cellar Lawned garden to rear Available Now Part Furnished
 *
 *         ONE MONTHS FREE HEALTH AND FITNESS MEMBERSHIP WHEN YOU SIGN UP AS A LET TO LIVE TENANT.</description> <bullet1>A partly furnished 4 bedroom
 *         terrace.</bullet1> <bullet2>This property comprises of a lounge, a dining room, a kitchen and a 1 bathroom.</bullet2> <bullet3>There is a
 *         back garden.</bullet3> <bullet4 /> <bullet5 /> <bullet6 /> <beds>4</beds> <chain>8195</chain> <office>000</office>
 *         <class>Residential</class> <transactiontype>Agents</transactiontype> <type>Mid Terraced</type> <rental>Monthly</rental> <image>N</image>
 *         <attachmenttype /> <visible>Yes</visible> <style>House</style> <commercialspace /> <commercialunits /> <auctiondate /> <auctionlocation />
 *         <era>TBA</era> <tenure>RENTAL</tenure> <entrydate>04/11/2009</entrydate> <lastupdated />
 *         <brochurefrontimage2>0000643_8195000_2.JPG</brochurefrontimage2> <brochurefrontimage3>0000643_8195000_3.JPG</brochurefrontimage3>
 *         <brochurefrontimage4>0000643_8195000_4.JPG</brochurefrontimage4> <brochurefrontimage5 /> <custom /> <brochureimage2name />
 *         <brochureimage3name /> <brochureimage4name /> <brochureimage5name /> <hidestreet>YES</hidestreet> <bathrooms /> <receptions /> <smsref />
 *         <smstext /> <virtualtour /> <attribute> <propref>0000643</propref> <value>Partially furnished</value> </attribute> <attribute>
 *         <propref>0000643</propref> <value>Available now</value> </attribute> <attribute> <propref>0000643</propref> <value>Parking (street)</value>
 *         </attribute>
 *
 */
public class CarlProperty {

	/**
	 * propnumber - this is number on road
	 */
	private String		number;

	private List<String>	images	= new ArrayList();

	public List<String> getImages() {
		return images;
	}

	public void addImage(String image) {
		images.add(image);
	}

	/**
	 * proproad - road name
	 */
	private String	road;

	private String	status;

	private int		beds;

	private String	bullet1;

	private String	bullet2;

	private String	bullet3;

	private String	bullet4;

	private String	bullet5;

	private String	bullet6;

	private String	style;

	private String	tenure;

	private String	type;

	private String	rental;

	private String	clazz;

	private String	description;

	private int		saleprice;

	private int		letprice;

	/**
	 * proppostcode
	 */
	private String	postcode;

	/**
	 * proptown
	 */
	private String	town;

	/**
	 * propref
	 */
	private String	reference;

	private String	district;

	private boolean	visible;

	public int getBeds() {
		return beds;
	}

	public String getBullet1() {
		return bullet1;
	}

	public String getBullet2() {
		return bullet2;
	}

	public String getBullet3() {
		return bullet3;
	}

	public String getBullet4() {
		return bullet4;
	}

	public String getBullet5() {
		return bullet5;
	}

	public String getBullet6() {
		return bullet6;
	}

	public String getClazz() {
		return clazz;
	}

	public String getDescription() {
		return description;
	}

	public String getDistrict() {
		return district;
	}

	public int getLetprice() {
		return letprice;
	}

	public String getNumber() {
		return number;
	}

	public String getPostcode() {
		return postcode;
	}

	public String getReference() {
		return reference;
	}

	public String getRental() {
		return rental;
	}

	public String getRoad() {
		return road;
	}

	public int getSaleprice() {
		return saleprice;
	}

	public String getStatus() {
		return status;
	}

	public String getStyle() {
		return style;
	}

	public String getTenure() {
		return tenure;
	}

	public String getTown() {
		return town;
	}

	public String getType() {
		return type;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setBeds(int beds) {
		this.beds = beds;
	}

	public void setBullet1(String bullet1) {
		this.bullet1 = bullet1;
	}

	public void setBullet2(String bullet2) {
		this.bullet2 = bullet2;
	}

	public void setBullet3(String bullet3) {
		this.bullet3 = bullet3;
	}

	public void setBullet4(String bullet4) {
		this.bullet4 = bullet4;
	}

	public void setBullet5(String bullet5) {
		this.bullet5 = bullet5;
	}

	public void setBullet6(String bullet6) {
		this.bullet6 = bullet6;
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 *
	 */
	public void setDistrict(String district) {
		this.district = district;
	}

	public void setLetprice(int letprice) {
		this.letprice = letprice;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public void setRental(String rental) {
		this.rental = rental;
	}

	public void setRoad(String road) {
		this.road = road;
	}

	public void setSaleprice(int saleprice) {
		this.saleprice = saleprice;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public void setTenure(String tenure) {
		this.tenure = tenure;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public void setType(String type) {
		this.type = type;
	}

	/**
	 *
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
}
