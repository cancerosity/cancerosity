package org.samspade79.ecreator.bespoke.carl;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Stephen K Samuel samspade79@gmail.com 7 Jan 2010 17:25:59
 *
 */
public class CarlFeed {

	private List<CarlProperty>	properties	= new ArrayList();
	private String			name;

	public void addProperty(CarlProperty property) {
		properties.add(property);
	}

	public String getName() {
		return name;
	}

	public List<CarlProperty> getProperties() {
		return properties;
	}

	/**
	 *
	 */
	public void setName(String name) {
		this.name = name;
	}
}
