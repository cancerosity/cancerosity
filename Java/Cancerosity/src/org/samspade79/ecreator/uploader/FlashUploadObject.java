package org.samspade79.ecreator.uploader;

/**
 * @author Stephen K Samuel samspade79@gmail.com Nov 25, 2009 9:25:15 AM
 *
 */
public class FlashUploadObject {

	@Override
	public String toString() {

		return "<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\"" + "id=\"FileUploader\" width=\"100%\" height=\"104\""
				+ "codebase=\"http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab\">"
				+ "<param name=\"movie\" value=\"uploader/EFUSingleUploader.swf\" />\n" + "<param name=\"quality\" value=\"high\" />\n"
				+ "<param name=\"allowScriptAccess\" value=\"sameDomain\" />\n"
				+ "<param name=\"wmode\" value=\"transparent\"><embed src=\"uploader/EFUSingleUploader.swf\" wmode=\"transparent\" quality=\"high\""
				+ "width=\"100%\" height=\"104\" name=\"FileUploader\" align=\"middle\"" + "play=\"true\"" + "loop=\"false\"" + "quality=\"high\""
				+ "allowScriptAccess=\"sameDomain\"" + "type=\"application/x-shockwave-flash\""
				+ "pluginspage=\"http://www.adobe.com/go/getflashplayer\">" + "</embed></object>";
	}
}
