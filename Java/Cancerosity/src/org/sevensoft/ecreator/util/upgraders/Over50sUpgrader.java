package org.sevensoft.ecreator.util.upgraders;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.accounts.subscriptions.Subscription;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.interaction.pm.PrivateMessage;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.Row;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 30 Jun 2007 14:08:16
 *
 */
public class Over50sUpgrader {

	private RequestContext	context;

	public Over50sUpgrader(RequestContext context2) {
		context = context2;
	}

	public void fixpms() {

		Query q = new Query(context, "select id, recipient, sender from #");
		q.setTable(PrivateMessage.class);
		for (Row row : q.execute()) {

			// get recipient name
			q = new Query(context, "select name from members where id=?");
			q.setParameter(row.getInt(1));
			String recipientName = q.getString();

			q = new Query(context, "select name from members where id=?");
			q.setParameter(row.getInt(2));
			String senderName = q.getString();

			// get recipient id
			q = new Query(context, "select id from items where name=? and itemType=4");
			q.setParameter(recipientName);
			int recipientId = q.getInt();

			q = new Query(context, "select id from items where name=? and itemType=4");
			q.setParameter(senderName);
			int senderId = q.getInt();

			// update messages
			q = new Query(context, "update # set recipient=?, sender=? where id=?");
			q.setTable(PrivateMessage.class);
			q.setParameter(recipientId);
			q.setParameter(senderId);
			q.setParameter(row.getInt(0));
			q.run();
		}

	}

	public void fixaccounts() {

		Query q = new Query(context, "select id, member from items where member>0");
		for (Row row : q.execute()) {

			q = new Query(context, "select name from members where id=?");
			q.setParameter(row.getInt(1));
			String name = q.getString();

			q = new Query(context, "select id from items where name=? and itemType=4");
			q.setParameter(name);
			int id = q.getInt();

			q = new Query(context, "update items set account=? where id=?");
			q.setParameter(id);
			q.setParameter(row.getInt(0));
			q.run();
		}

	}

	public void movesubs() {

		SubscriptionLevel level = EntityObject.getInstance(context, SubscriptionLevel.class, 1);

		Query q = new Query(context, "select id, name from items where itemType=4");
		for (Row row : q.execute()) {

			q = new Query(context, "select subscriptionExpiryDate from members where name=?");
			q.setParameter(row.getString(1));

			Date date = q.getDate();
			if (date != null) {
				if (date.isFuture()) {

					Item account = row.getObject(0, Item.class);
					Subscription sub = account.getSubscription();

					sub.setSubscriptionExpiryDate(date);
					sub.setSubscriptionLevel(level);
					sub.save();
				}
			}
		}
	}

	/**
	 * 
	 */
	public void run() {
		fixpms();
	}
}
