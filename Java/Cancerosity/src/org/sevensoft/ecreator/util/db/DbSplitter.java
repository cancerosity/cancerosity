package org.sevensoft.ecreator.util.db;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author sks 21 Feb 2007 15:20:53
 *
 */
public class DbSplitter {

	public static void main(String[] args) throws IOException {

		File file = new File("c:/Users/sks/backup/fruitnut-01-07/fruitnut.sql");
		String dir = "c:/Users/sks/backup/fruitnut-01-07/";

		int n = 0;
		BufferedReader reader = new BufferedReader(new FileReader(file));

		BufferedWriter writer = null;
		String row;
		while ((row = reader.readLine()) != null) {

			if (row.startsWith("CREATE DATABASE")) {

				n++;
				File output = new File(dir + "/db" + n + ".sql");
				if (writer != null) {
					writer.close();
				}

				writer = new BufferedWriter(new FileWriter(output));
			}

			if (writer != null) {

				writer.write(row);
				writer.write("\n");

			}
		}
		if (writer != null) {
			writer.close();
		}
		reader.close();
	}
}
