package org.sevensoft.ecreator.util.log;

import org.apache.juli.FileHandler;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
public class LogHandler extends FileHandler {

    public LogHandler() {
        super();
        setFormatter(new LogFormatter());
    }

}
