package org.sevensoft.ecreator.util.log;

import java.util.logging.SimpleFormatter;
import java.util.logging.LogRecord;
import java.util.Date;
import java.text.SimpleDateFormat;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
public class LogFormatter extends SimpleFormatter {

    private SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy HH:mm:SSS");

    public synchronized String format(LogRecord record) {
        return "[" + MDC.domain + "] [" + format.format(new Date(record.getMillis())) + "] " + super.format(record);
    }
}
