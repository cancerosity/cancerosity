package org.sevensoft.ecreator.model.interaction.views.markers;

import java.util.Map;

import org.sevensoft.commons.samdate.Period;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 24 Sep 2006 09:08:48
 *
 */
public class ViewsSinceMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.MemberViewers.enabled(context)) {
			return null;
		}

		if (!params.containsKey("period")) {
			return null;
		}

		Item account = (Item) context.getAttribute("account");
		if (account == null) {
			return null;
		}

		Period period = new Period(params.get("period"));
		int views = account.getInteraction().getViewsSince(period);

		return super.string(context, params, views);
	}

	public Object getRegex() {
		return new String[] { "views_since", "interaction_views_since" };
	}

}
