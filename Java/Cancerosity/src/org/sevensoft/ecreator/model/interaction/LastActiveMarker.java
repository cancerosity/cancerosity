package org.sevensoft.ecreator.model.interaction;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class LastActiveMarker implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item profile, String location, int x, int y) {

		String prefix = params.get("prefix");
		String suffix = params.get("suffix");

		StringBuilder sb = new StringBuilder("<span class='last_active'>");
		if (prefix != null)
			sb.append(prefix);

		sb.append(profile.getLastActive());

		if (suffix != null)
			sb.append(suffix);

		sb.append("</span>");
		return sb.toString();
	}

	public Object getRegex() {
		return "last_active";
	}

}
