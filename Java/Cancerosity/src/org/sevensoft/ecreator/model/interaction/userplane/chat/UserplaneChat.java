package org.sevensoft.ecreator.model.interaction.userplane.chat;

import org.jdom.Document;
import org.jdom.Element;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.accounts.sessions.AccountSession;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 9 Dec 2006 19:26:46
 *
 */
public class UserplaneChat {

	private final RequestContext		context;
	private final UserplaneChatSettings	settings;

	public UserplaneChat(RequestContext context) {
		this.context = context;
		this.settings = UserplaneChatSettings.getInstance(context);
	}

	public Document getDomainPreferencesDoc() {

		Element root = new Element("communicationsuite");
		Document doc = new Document(root);

		root.addContent(new Element("time").setText(new Date().toString()));

		Element domain = new Element("domain");
		root.addContent(domain);

		domain.addContent(new Element("maxXMLRetries").setText("5"));
		domain.addContent(new Element("avEnabled").setText("true"));

		Element chat = new Element("chat");
		domain.addContent(chat);

		Element allowModeratedRooms = new Element("allowModeratedRooms").setText("false");
		chat.addContent(allowModeratedRooms);

		Element floodControlResetTime = new Element("floodControlResetTime").setText("5");
		chat.addContent(floodControlResetTime);

		Element floodControlInterval = new Element("floodControlInterval").setText("5");
		chat.addContent(floodControlInterval);

		Element floodControlMaxMessages = new Element("floodControlMaxMessages").setText("5");
		chat.addContent(floodControlMaxMessages);

		Element lobby = new Element("lobby");
		chat.addContent(lobby);

		lobby.addContent(new Element("name").setText(settings.getLobbyName()));
		lobby.addContent(new Element("description").setText("You wait here until you join a room"));

		Element maxroomusers = new Element("maxroomusers").setText("20");
		chat.addContent(maxroomusers);

		Element MaxdockItems = new Element("MaxdockItems").setText(String.valueOf(settings.getMaxDockItems()));
		chat.addContent(MaxdockItems);

		Element characterlimit = new Element("characterlimit").setText("400");
		chat.addContent(characterlimit);

		Element maxhistorymessages = new Element("maxhistorymessages").setText("0");
		chat.addContent(maxhistorymessages);

		Element showJoinLeaveMessages = new Element("showJoinLeaveMessages").setText("true");
		chat.addContent(showJoinLeaveMessages);

		Element gui = new Element("gui");
		chat.addContent(gui);

		gui.addContent(new Element("viewprofile").setText("false"));
		gui.addContent(new Element("instantcommunicator").setText("false"));
		gui.addContent(new Element("addfriend").setText("false"));
		gui.addContent(new Element("block").setText("false"));
		gui.addContent(new Element("help").setText("false"));

		Element roomlist = new Element("roomlist");
		chat.addContent(roomlist);

		for (String roomName : settings.getRooms()) {

			Element roomEl = new Element("room");
			roomlist.addContent(roomEl);

			roomEl.addContent(new Element("name").setText(roomName));
			roomEl.addContent(new Element("description").setText(""));
		}

		chat.addContent(new Element("conferenceCallEnabled").setText("false"));

		return doc;
	}

	public Document getEmptyDoc() {

		Element root = new Element("communicationsuite");
		Document doc = new Document(root);

		return doc;

	}

	public Document getUser(String sessionGUID) {

		// get user from session
		Item item= AccountSession.getItem(context, sessionGUID);

		Element root = new Element("communicationsuite");
		Document doc = new Document(root);

		root.addContent(new Element("time").setText(new Date().toString()));

		Element user = new Element("user");
		root.addContent(user);

		if (item == null) {

			user.addContent(new Element("userid").setText("INVALID"));
			return doc;

		}

		user.addContent(new Element("userid").setText(item.getIdString()));
		user.addContent(new Element("displayname").setText(item.getDisplayName()));

		Element avSettings = new Element("avsettings");
		user.addContent(avSettings);

		avSettings.addContent(new Element("avEnabled").setText("true"));
		avSettings.addContent(new Element("audioSend").setText("true"));
		avSettings.addContent(new Element("videoSend").setText("true"));
		avSettings.addContent(new Element("audioReceive").setText("true"));
		avSettings.addContent(new Element("videoReceive").setText("true"));
		avSettings.addContent(new Element("audiokbps").setText("32"));
		avSettings.addContent(new Element("videokbps").setText("160"));
		avSettings.addContent(new Element("videofps").setText("15"));
		avSettings.addContent(new Element("videosize").setText("2"));
		avSettings.addContent(new Element("videoDisplaySize").setText("1"));

		if (item.hasApprovedImages()) {

			Img image = item.getApprovedImage();
			if (image != null) {

				Element images = new Element("images");
				user.addContent(images);

				Element thumbnail = new Element("thumbnail").setText("http://" + settings.getImagesServer() + "/" + image.getThumbnailFilename());
				images.addContent(thumbnail);

				Element fullsize = new Element("fullsize").setText("http://" + settings.getImagesServer() + "/" + image.getFilename());
				images.addContent(fullsize);
			}
		}

		Element chat = new Element("chat");
		user.addContent(chat);

		Element gui = new Element("gui");
		chat.addContent(gui);
		gui.addContent(new Element("viewprofile").setText("false"));
		gui.addContent(new Element("instantcommunicator").setText("false"));

		chat.addContent(new Element("userroomcreate").setText("false"));
		chat.addContent(new Element("permitWhisper").setText("true"));
		chat.addContent(new Element("initialroom").setText("general"));
		chat.addContent(new Element("maxdockitems").setText("3"));
		chat.addContent(new Element("permitCopy").setText("true"));
		chat.addContent(new Element("sessionTimeout").setText("-1"));
		chat.addContent(new Element("sessionTimeoutMessage").setText("Your session has expired"));
		chat.addContent(new Element("inactivityTimeout").setText("5"));
		chat.addContent(new Element("inactivityTimeoutMessage").setText("You were timed out due to inactivity"));

		return doc;
	}
}
