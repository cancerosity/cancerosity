package org.sevensoft.ecreator.model.interaction.winks.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.interaction.winks.WinksHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.interaction.Interaction;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 03-Apr-2006 11:14:38
 *
 */
public class WinksMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		logger.fine("[WinksMarker] params=" + params);

		if (!Module.Nudges.enabled(context)) {
			logger.fine("[WinksMarker] winks/nudges disabled, exiting");
			return null;
		}

		if (params.containsKey("new")) {

			Item account = (Item) context.getAttribute("account");
			logger.fine("[WinksMarker] member=" + account);

			// only show if we have new winks
			if ("1".equals(params.get("new"))) {

				// if we are not logged in we cannot have new winks !
				if (account == null)
					return null;

				Interaction interaction = account.getInteraction();

				// if we do not have any winks then exit
				if (!interaction.hasNewWinks()) {
					return null;
				}

			} else if ("0".equals(params.get("new"))) {

				// if we are logged in and we have new winks then exit
				if (account != null && account.getInteraction().hasNewWinks()) {
					return null;
				}

			} else {

				// param must be 1 or 0
				return null;

			}
		}

		return super.link(context, params, new Link(WinksHandler.class), "account_link");

	}

	public Object getRegex() {
		return new String[] { "winks_page", "interaction_winks", "account_nudges" };
	}
}