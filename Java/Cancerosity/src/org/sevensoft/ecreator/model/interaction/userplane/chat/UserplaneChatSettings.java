package org.sevensoft.ecreator.model.interaction.userplane.chat;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 9 Dec 2006 19:27:30
 *
 */
@Table("userplane_chat")
@Singleton
public class UserplaneChatSettings extends EntityObject {

	public static UserplaneChatSettings getInstance(RequestContext context) {
		return EntityObject.getSingleton(context, UserplaneChatSettings.class);
	}

	private String		bannerZoneId;
	private String		domainID;
	private String		flashcomServer;
	private String		imagesServer;
	private String		textZoneId;

	private String		lobbyName;

	/**
	 * These rooms are created on startup
	 */
	private List<String>	rooms;
	private int	maxDockItems;

	public UserplaneChatSettings(RequestContext context) {
		super(context);
	}

	public final String getBannerZoneId() {
		return bannerZoneId;
	}

	public String getDomainID() {
		return domainID;
	}

	public String getFlashcomServer() {
		return flashcomServer;
	}

	public String getImagesServer() {
		return imagesServer;
	}

	public final String getLobbyName() {
		return lobbyName == null ? "Lobby" : lobbyName;
	}

	public int getMaxDockItems() {
		return maxDockItems;
	}

	public final List<String> getRooms() {
		if (rooms == null)
			rooms = new ArrayList<String>();
		return rooms;
	}

	public final String getTextZoneId() {
		return textZoneId;
	}

	public final void setBannerZoneId(String bannerZoneId) {
		this.bannerZoneId = bannerZoneId;
	}

	public void setDomainID(String domainID) {
		this.domainID = domainID;
	}

	public void setFlashcomServer(String flashcomServer) {
		this.flashcomServer = flashcomServer;
	}

	public void setImagesServer(String imagesServer) {
		this.imagesServer = imagesServer;
	}

	public final void setLobbyName(String lobbyName) {
		this.lobbyName = lobbyName;
	}

	public final void setRooms(List<String> rooms) {
		this.rooms = rooms;
	}

	public final void setTextZoneId(String textZoneId) {
		this.textZoneId = textZoneId;
	}

	
	public final void setMaxDockItems(int maxDockItems) {
		this.maxDockItems = maxDockItems;
	}

}
