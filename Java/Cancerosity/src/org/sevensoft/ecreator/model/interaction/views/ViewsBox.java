package org.sevensoft.ecreator.model.interaction.views;

import java.util.List;

import org.sevensoft.ecreator.iface.admin.accounts.boxes.ViewsBoxHandler;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 23 Sep 2006 14:17:05
 *
 */
@Table("boxes_members_views")
@Label("Viewers")
@HandlerClass(ViewsBoxHandler.class)
public class ViewsBox extends Box {

	private int		limit;
	private String	emptyMessage;

	private ViewsBox(RequestContext context) {
		super(context);
	}

	public ViewsBox(RequestContext context, String location) {
		super(context, location);
	}

	@Override
	protected String getCssIdDefault() {
		return "member_viewers";
	}

	public String getEmptyMessage() {
		return emptyMessage;
	}

	public int getLimit() {
		return limit < 1 || limit > 20 ? 1 : limit;
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	@Override
	public String render(RequestContext context) {

		if (!Module.MemberViewers.enabled(context))
			return null;

		Item member = (Item) context.getAttribute("account");
		if (member == null)
			return null;

		/*
		 * If buddies is disabled for this user then do not show buddies box
		 */
		if (!PermissionType.MemberViews.check(context, member))
			return null;

		StringBuilder sb = new StringBuilder(1024);
		sb.append(getTableTag());

		List<View> views = member.getInteraction().getViews(getLimit());
		if (views.isEmpty()) {

			if (emptyMessage == null)
				return null;

			sb.append("<tr><td>No one has viewed your profile</td></tr>");

		} else {

			for (View view : views) {

				sb.append("</td>");
				sb.append("<td class='name'>" + new LinkTag(view.getViewer().getUrl(), view.getViewer().getDisplayName()) + "</td></tr>");
				sb.append("<td class='date'>Viewed on " + view.getDate().toString("HH:mm dd-MMM") + "</td></tr>");
			}
		}

		sb.append("</table>");
		return sb.toString();
	}

	public void setEmptyMessage(String emptyMessage) {
		this.emptyMessage = emptyMessage;
	}

	public void setLimit(int l) {
		this.limit = l;
	}
}
