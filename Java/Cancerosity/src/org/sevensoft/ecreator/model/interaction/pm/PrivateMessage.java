package org.sevensoft.ecreator.model.interaction.pm;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.frontend.interaction.pm.PmHandler;
import org.sevensoft.ecreator.model.attachments.AttachmentSupport;
import org.sevensoft.ecreator.model.interaction.InteractionModule;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 08-Sep-2005 15:41:43
 * 
 */
@Table("members_messages")
public class PrivateMessage extends AttachmentSupport {

	/**
	 * The content of the private message
	 */
	private String	content;

	/**
	 * Date this message was sent.
	 */
	private DateTime	date;

	/**
	 * Flag to yes when this message has been read by the receipient. 
	 */
	private boolean	isRead;

	/**
	 * 
	 */
	private Item	recipient, sender;

	/**
	 * Flag to yes when the receipient has deleted from his inbox.
	 * 
	 */
	private boolean	recipientDeleted;

	/**
	 * Flag to yes when the sender has deleted from his sent items box.
	 */
	private boolean	senderDeleted;

	public PrivateMessage(RequestContext context) {
		super(context);
	}

	public PrivateMessage(RequestContext context, Item s, Item r, String c) {

		super(context);

		this.sender = s;
		this.content = c;

		for (Smilee smilee : Smilee.get(context)) {
			this.content = smilee.replace(content);
		}

		this.recipient = r;

		this.date = new DateTime();
		save();
	}

	@Override
	public synchronized boolean delete() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @param account
	 */
	public void delete(Item account) {

		if (account.equals(recipient)) {
			recipientDeleted = true;
			isRead = true;

		} else if (account.equals(sender)) {
			senderDeleted = true;
		}

		save();
		account.setMessageCount();

		/*
		 * If both recipient and sender have deleted this message then remove from database 
		 */
		if (recipientDeleted && senderDeleted) {
			super.delete();
		}
	}

	public int getAttachmentLimit() {
		return 0;
	}

	public String getContent() {
		return content;
	}

	/**
	 * Returns whoever the correspondant was (the opposite of the param)
	 */
	public Item getCorrespondant(Item acc) {

		if (acc.equals(sender)) {
			return getRecipient();

		} else {
			return getSender();
		}
	}

	public DateTime getDate() {
		return date;
	}

	public Item getRecipient() {
		return recipient.pop();
	}

	public Item getSender() {
		return sender.pop();
	}

	public String getSnippet(int i) {
		return StringHelper.toSnippet(content, i, "...");
	}

	/**
	 * Returns the url to read this message
	 */
	public String getUrl() {
		return Config.getInstance(context).getUrl() + "/" + new Link(PmHandler.class, "view", "message", this);
	}

	public boolean isNew() {
		return !isRead;
	}

	public boolean isRead() {
		return isRead;
	}

	public void read() {

		this.isRead = true;
		save();

	}

	/**
	 * Send an email to the recipient of this message advising them they have received a new private message
	 */
	public void sendNotification() throws EmailAddressException, SmtpServerException {

		Config config = Config.getInstance(context);

		String body = null;

		final InteractionModule im = getSender().getItemType().getInteractionModule();

		if (im.hasPmNotificationBody()) {
			body = im.getPmNotificationBody();
		}

		// if no msg body then do not send the msg
		if (body == null) {
			return;
		}

		new PmNotificationEmail(this, context, body).send(config.getSmtpHostname(), config.getUsername(), config.getPassword());
	}

	public void setSender(Item sender) {
		this.sender = sender;
	}

}
