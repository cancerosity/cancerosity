package org.sevensoft.ecreator.model.interaction.userplane.recorder.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.interaction.userplane.recorder.UserplaneRecorderRenderer;
import org.sevensoft.ecreator.iface.frontend.interaction.userplane.recorder.UserplaneViewerHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 14 Dec 2006 20:18:29
 *
 */
public class UserplaneViewerMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item profile, String location, int x, int y) {

		if (!Module.UserplaneRecorder.enabled(context)) {
			return null;
		}

		if (!profile.hasApprovedRecording()) {
			return null;
		}

		if (params.containsKey("embed")) {

			UserplaneRecorderRenderer r = new UserplaneRecorderRenderer(context, profile.getId(), false);

			if (params.containsKey("width")) {
				r.setWidth(Integer.parseInt(params.get("width")));
			}

			if (params.containsKey("height")) {
				r.setHeight(Integer.parseInt(params.get("height")));
			}

			return r;
		}

		return super.link(context, params, new Link(UserplaneViewerHandler.class, null, "profile", profile), "userplane_viewer");
	}

	@Override
	public String getDescription() {
		return "Makes a link to the view video page for this profile";
	}

	public Object getRegex() {
		return "userplane_viewer";
	}

}
