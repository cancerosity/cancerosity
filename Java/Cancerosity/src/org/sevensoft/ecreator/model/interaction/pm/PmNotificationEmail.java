package org.sevensoft.ecreator.model.interaction.pm;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 24-Nov-2005 22:11:09
 * 
 */
public class PmNotificationEmail extends Email {

	public PmNotificationEmail(PrivateMessage message, RequestContext context, String body) {

		Company company = Company.getInstance(context);
		Item recipient = message.getRecipient();

		setTo(recipient.getEmail());

		setSubject("You have a new message");
		setFrom(company.getName(), Config.getInstance(context).getServerEmail());

		body = body.replace("[site]", company.getName());
		body = body.replace("[url]", message.getUrl());
		body = body.replace("[recipient]", recipient.getDisplayName());
		body = body.replace("[sender]", message.getSender().getDisplayName());

		setBody(body);

	}

}
