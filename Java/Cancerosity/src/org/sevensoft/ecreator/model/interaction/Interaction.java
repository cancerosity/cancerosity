package org.sevensoft.ecreator.model.interaction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.Period;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.interaction.blocks.BlockedAccount;
import org.sevensoft.ecreator.model.interaction.buddies.Buddy;
import org.sevensoft.ecreator.model.interaction.buddies.BuddyLimitException;
import org.sevensoft.ecreator.model.interaction.pm.PrivateMessage;
import org.sevensoft.ecreator.model.interaction.userplane.recorder.Recording;
import org.sevensoft.ecreator.model.interaction.views.View;
import org.sevensoft.ecreator.model.interaction.views.ViewCount;
import org.sevensoft.ecreator.model.interaction.winks.Wink;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.marketing.sms.SmsNumberException;
import org.sevensoft.ecreator.model.marketing.sms.SmsSenderException;
import org.sevensoft.ecreator.model.marketing.sms.SmsSettings;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsCreditsException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayAccountException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayMessagingException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsMessageException;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;

/**
 * @author sks 24 Sep 2006 08:59:54
 *
 * This class contains all the methods used for account to account interaction
 * 
 */
@Table("interactions")
public class Interaction extends EntityObject {

	public static Interaction get(RequestContext context, Item account) {
		Interaction i = SimpleQuery.get(context, Interaction.class, "account", account);
		return i == null ? new Interaction(context, account) : i;
	}

	private Item	account;

	private boolean	newWinks;

	private int		recordingsCount;

	private int		winkCount;

	private int		viewCount;

	protected Interaction(RequestContext context) {
		super(context);
	}

	public Interaction(RequestContext context, Item account) {
		super(context);

		this.account = account;
		save();
	}

	public void addBuddy(Item buddy) throws BuddyLimitException {
		new Buddy(context, account, buddy);
	}

	public void addRecording() {
		new Recording(context, account, "default");
		setRecordingsCount();
	}

	public void block(Item toBlock) {
		new BlockedAccount(context, account, toBlock);
	}

	public final Item getAccount() {
		return (Item) (account == null ? null : account.pop());
	}

	public List<Item> getAddedToBuddies() {

		String query = "select i.* from # i join # b on i.id=b.account where b.buddy=? order by b.id desc";
		Query q = new Query(context, query);
		q.setTable(Item.class);
		q.setTable(Buddy.class);
		q.setParameter(account);
		return q.execute(Item.class);
	}

	public List<Item> getBlockedAccounts() {

		String query = "select m.* from # m join # bm on m.id=bm.blocked where bm.account=? order by m.displayName, m.name";
		Query q = new Query(context, query);
		q.setTable(Item.class);
		q.setTable(BlockedAccount.class);
		q.setParameter(account);
		return q.execute(Item.class);
	}

	public List<Item> getBuddies() {

		String query = "select m.* from # m join # b on m.id=b.buddy where b.account=? order by b.id desc";
		Query q = new Query(context, query);
		q.setTable(Item.class);
		q.setTable(Buddy.class);
		q.setParameter(account);
		return q.execute(Item.class);
	}

	/**
	 * Returns the number of buddy lists this account is included on
	 */
	public int getBuddiesWhoCount() {
		return SimpleQuery.count(context, Buddy.class, "buddy", account);
	}

	/**
	 * Returns a list of attachments sent to you from correspondant
	 */
	public List<Attachment> getMessageAttachmentsReceived(Item correspondant, int i) {

		List<Attachment> attachments = new ArrayList();
		for (PrivateMessage message : getMessagesReceived(correspondant, i)) {
			attachments.addAll(message.getAttachments());
		}

		return attachments;
	}

	public List<Attachment> getMessageAttachmentsSent(Item correspondant, int i) {

		List<Attachment> attachments = new ArrayList();
		for (PrivateMessage message : getMessagesSent(correspondant, i)) {
			attachments.addAll(message.getAttachments());
		}

		return attachments;
	}

	/**
	 * Gets all messages either sent or received to this account
	 *
	 */
	public List<PrivateMessage> getMessages(Item correspondant, int limit) {

		Query q = new Query(context, "select * from # where (sender=? and recipient=? and senderDeleted=0) "
				+ "or (sender=? and recipient=? and recipientDeleted=0) order by date desc");

		q.setTable(PrivateMessage.class);
		q.setParameter(account);
		q.setParameter(correspondant);
		q.setParameter(correspondant);
		q.setParameter(account);
		return q.execute(PrivateMessage.class, limit);
	}

	public List<PrivateMessage> getMessagesReceived(Item corres, int limit) {

		Query q = new Query(context, "select * from # where sender=? and recipient=? and recipientDeleted=0 order by date desc");

		q.setTable(PrivateMessage.class);

		q.setParameter(corres);
		q.setParameter(account);

		return q.execute(PrivateMessage.class, limit);
	}

	public List<PrivateMessage> getMessagesSent(Item corres, int limit) {

		Query q = new Query(context, "select * from # where sender=? and recipient=? and senderDeleted=0 order by date desc");

		q.setTable(PrivateMessage.class);

		q.setParameter(account);
		q.setParameter(corres);

		return q.execute(PrivateMessage.class, limit);
	}

	public InteractionModule getModule() {
		return account.getItemType().getInteractionModule();
	}

	public List<PrivateMessage> getPmInbox() {
		Query q = new Query(context, "select * from # where recipient=? and recipientDeleted=0 order by id desc");
		q.setTable(PrivateMessage.class);
		q.setParameter(account);
		return q.execute(PrivateMessage.class);
	}

	public List<PrivateMessage> getPmSent() {
		Query q = new Query(context, "select * from # where sender=? and senderDeleted=0 order by id desc");
		q.setTable(PrivateMessage.class);
		q.setParameter(account);
		return q.execute(PrivateMessage.class);
	}

	/**
	 * Returns the number of pms sent today 
	 */
	public final int getPmSentToday() {
		Query q = new Query(context, "select count(*) from # where sender=? and date>=? and date<?");
		q.setTable(PrivateMessage.class);
		q.setParameter(account);
		q.setParameter(new Date());
		q.setParameter(new Date().nextDay());
		return q.getInt();
	}

	/**
	 * Returns any recording for this account
	 */
	public Recording getRecording() {
		Recording recording = SimpleQuery.get(context, Recording.class, "account", account);
		return recording;
	}

	public final int getRecordingsCount() {
		return recordingsCount;
	}

	public final int getViewCount() {
		return viewCount;
	}

	public List<Item> getViewers(int limit) {

		Query q = new Query(context, "select i.* from # i join # v on i.id=v.viewer where v.account=? order by v.id desc limit " + limit);

		q.setTable(Item.class);
		q.setTable(View.class);

		q.setParameter(account);

		return q.execute(Item.class);
	}

	/**
	 * Returns the views for this account
	 */
	public List<View> getViews(int limit) {

		Query q = new Query(context, "select * from # where account=? order by id desc");
		q.setTable(View.class);
		q.setParameter(account);
		return q.execute(View.class, limit);
	}

	/**
	 * Returns the number of times this profile has been viewed by the param account
	 */
	public int getViewsCount(Item viewer) {
		Query q = new Query(context, "select count from # where account=? and viewer=?");
		q.setTable(ViewCount.class);
		q.setParameter(account);
		q.setParameter(viewer);
		return q.getInt();
	}

	/**
	 * Returns the number of people who have looked at this profile in the past 'period'
	 */
	public int getViewsSince(Period period) {

		Query q = new Query(context, "select count(*) from # where account=? and date>=?");
		q.setTable(View.class);
		q.setParameter(account);
		q.setParameter(period.remove(System.currentTimeMillis()));
		return q.getInt();
	}

	public int getWinkCount() {
		return winkCount;
	}

	/**
	 * Returns the last so many account who winked at this account
	 */
	public List<Item> getWinkers(int limit) {

		Query q = new Query(context, "select distinct m.* from # m join # w on m.id=w.sender where w.recipient=? order by w.id desc limit " + limit);
		q.setParameter(account);
		q.setTable(Item.class);
		q.setTable(Wink.class);
		return q.execute(Item.class);
	}

	public List<Wink> getWinks() {
		Query q = new Query(context, "select * from # where recipient=? order by id desc");
		q.setTable(Wink.class);
		q.setParameter(account);
		return q.execute(Wink.class);
	}

	public int getWinksSince(Period period) {

		Query q = new Query(context, "select count(*) from # where recipient=? and date>=?");
		q.setTable(Wink.class);
		q.setParameter(account);
		q.setParameter(period.remove(System.currentTimeMillis()));
		return q.getInt();
	}

	public final boolean hasNewWinks() {
		return newWinks;
	}

	public boolean hasPmInbox() {
		return getPmInboxCount() > 0;
	}

	public int getPmInboxCount() {
		return SimpleQuery.count(context, PrivateMessage.class, "recipient", account);
	}

	public int getPmSentCount() {
		return SimpleQuery.count(context, PrivateMessage.class, "sender", account);
	}

	public boolean hasRecording() {
		return recordingsCount > 0;
	}

	public int getUnreadPmCount() {
		return SimpleQuery.count(context, PrivateMessage.class, "isRead", 0, "recipient", account);
	}

	public boolean hasWinks() {
		return winkCount > 0;
	}

	/**
	 * Returns true if this account is BLOCKED by the account param
	 * 
	 */
	public boolean isBlockedBy(Item by) {

		if (!Module.MemberBlocking.enabled(context)) {
			return false;
		}

		return SimpleQuery.count(context, BlockedAccount.class, "account", by, "blocked", account) > 0;
	}

	/**
	 * Returns true if this account is blocking the account param.
	 */
	public boolean isBlocking(Item blocking) {

		if (!Module.MemberBlocking.enabled(context)) {
			return false;
		}

		return SimpleQuery.count(context, BlockedAccount.class, "account", account, "blocked", blocking) > 0;
	}

	public void removeBuddy(Item buddy) {
		SimpleQuery.delete(context, Buddy.class, "account", account, "buddy", buddy);
	}

	public void removeMessages() {

		for (PrivateMessage msg : getPmInbox()) {
			msg.delete(account);
		}

		for (PrivateMessage msg : getPmSent()) {
			msg.delete(account);
		}
	}

	public void removeRecording() {
		SimpleQuery.delete(context, Recording.class, "account", account);
		setRecordingsCount();
	}

	public PrivateMessage sendPm(Item recipient, String body, Upload upload) {

		PrivateMessage m = new PrivateMessage(context, account, recipient, body.trim());

		logger.fine("[Interaction] send message body=" + body + ", upload=" + upload);
		if (upload != null) {
			try {
				m.addAttachment(upload, false);
			} catch (AttachmentExistsException e) {
				e.printStackTrace();
			} catch (AttachmentTypeException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (AttachmentLimitException e) {
				e.printStackTrace();
			}
		}

		try {
			m.sendNotification();
		} catch (EmailAddressException e) {
			e.printStackTrace();
		} catch (SmtpServerException e) {
			e.printStackTrace();
		}

		return m;
	}

	/**
	 * @throws SmsSenderException 
	 * @throws SmsNumberException 
	 * @throws SmsCreditsException 
	 * @throws SmsGatewayMessagingException 
	 * @throws SmsGatewayAccountException 
	 * @throws IOException 
	 * @throws SmsMessageException 
	 * @throws SmsGatewayException 
	 * 
	 */
	public void sendSms(Item profile, String message) throws SmsGatewayException, SmsMessageException, IOException, SmsGatewayAccountException,
			SmsGatewayMessagingException, SmsCreditsException, SmsNumberException, SmsSenderException {

		// get sms number from 'mobile number' attribute
		String number = profile.getAttributeValue("mobile");
		if (number == null) {
			return;
		}

		SmsSettings smsSettings = SmsSettings.getInstance(context);
		smsSettings.sendMessage(message, number);

	}

	public final void setNewWinks(boolean newWinks) {
		this.newWinks = newWinks;
		save("newWinks");
	}

	public final void setRecordingsCount() {
		this.recordingsCount = SimpleQuery.count(context, Recording.class, "account", account);
		save("recordingsCount");
	}

	public void setWinkCount() {
		this.winkCount = SimpleQuery.count(context, Wink.class, "recipient", account);
		save("winkCount");
	}

	public void unblock(Item toUnblock) {
		SimpleQuery.delete(context, BlockedAccount.class, "account", account, "blocked", toUnblock);
	}

	/**
	 * Call this method on the receipient. so the sender / viewer is the param.
	 * 
	 */
	public void view(Item viewer) {

		if (!Module.MemberViewers.enabled(context)) {
			return;
		}

		if (account.equals(viewer)) {
			return;
		}

		new View(context, account, viewer);
		ViewCount.viewed(context, account, viewer);
		this.viewCount++;
		save("viewCount");
	}

	public void wink(Item recipient) {

		if (account.equals(recipient)) {
			return;
		}

		new Wink(context, account, recipient);

		Interaction i = recipient.getInteraction();
		i.setNewWinks(true);
		i.setWinkCount();

	}

}