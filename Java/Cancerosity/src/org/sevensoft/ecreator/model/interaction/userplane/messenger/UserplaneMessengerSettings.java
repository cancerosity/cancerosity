package org.sevensoft.ecreator.model.interaction.userplane.messenger;

import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 26 Nov 2006 14:47:14
 *
 */
@Table("userplane_messenger_settings")
@Singleton
public class UserplaneMessengerSettings extends EntityObject {

	public static UserplaneMessengerSettings getInstance(RequestContext context) {
		return EntityObject.getSingleton(context, UserplaneMessengerSettings.class);
	}

	private String	imagesServer;
	private String	domainID;
	private String	flashcomServer;
	private Attribute	profileAttribute1, profileAttribute2, profileAttribute3, profileAttribute4;

	private String	bannerZoneId, textZoneId;
	private String	forbiddenWordsList;

	protected UserplaneMessengerSettings(RequestContext context) {
		super(context);
	}

	public final String getBannerZoneId() {
		return bannerZoneId;
	}

	public String getDomainID() {
		return domainID;
	}

	public final void setForbiddenWordsList(String forbiddenWordsList) {
		this.forbiddenWordsList = forbiddenWordsList;
	}

	public String getFlashcomServer() {
		return flashcomServer;
	}

	public String getForbiddenWordsList() {
		return forbiddenWordsList;
	}

	public String getImagesServer() {
		return imagesServer;
	}

	public Attribute getProfileAttribute1() {
		return (Attribute) (profileAttribute1 == null ? null : profileAttribute1.pop());
	}

	public Attribute getProfileAttribute2() {
		return (Attribute) (profileAttribute2 == null ? null : profileAttribute2.pop());

	}

	public Attribute getProfileAttribute3() {
		return (Attribute) (profileAttribute3 == null ? null : profileAttribute3.pop());

	}

	public Attribute getProfileAttribute4() {
		return (Attribute) (profileAttribute4 == null ? null : profileAttribute4.pop());

	}

	public final String getTextZoneId() {
		return textZoneId;
	}

	public boolean hasProfileAttribute1() {
		return profileAttribute1 != null;
	}

	public boolean hasProfileAttribute2() {
		return profileAttribute2 != null;
	}

	public boolean hasProfileAttribute3() {
		return profileAttribute3 != null;
	}

	public boolean hasProfileAttribute4() {
		return profileAttribute4 != null;
	}

	public final void setBannerZoneId(String bannerZoneId) {
		this.bannerZoneId = bannerZoneId;
	}

	public void setDomainID(String domainID) {
		this.domainID = domainID;
	}

	public void setFlashcomServer(String flashcomServer) {
		this.flashcomServer = flashcomServer;
	}

	public void setImagesServer(String imagesServer) {
		this.imagesServer = imagesServer;
	}

	public void setProfileAttribute1(Attribute profileAttribute1) {
		this.profileAttribute1 = profileAttribute1;
	}

	public void setProfileAttribute2(Attribute profileAttribute2) {
		this.profileAttribute2 = profileAttribute2;
	}

	public void setProfileAttribute3(Attribute profileAttribute3) {
		this.profileAttribute3 = profileAttribute3;
	}

	public void setProfileAttribute4(Attribute profileAttribute4) {
		this.profileAttribute4 = profileAttribute4;
	}

	public final void setTextZoneId(String textZoneId) {
		this.textZoneId = textZoneId;
	}

}
