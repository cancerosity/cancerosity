package org.sevensoft.ecreator.model.interaction.userplane.recorder.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.interaction.userplane.recorder.UserplaneRecorderHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 14 Dec 2006 20:18:29
 *
 */
public class UserplaneRecorderMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		Item member = (Item) context.getAttribute("account");
		if (member == null)
			return null;

		return super.link(context, params, new Link(UserplaneRecorderHandler.class, "recorder"), "account_link");
	}

	@Override
	public String getDescription() {
		return "Makes a link to the userplane recordings page";
	}

	public Object getRegex() {
		return "userplane_recorder";
	}

}
