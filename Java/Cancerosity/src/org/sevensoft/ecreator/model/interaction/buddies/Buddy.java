package org.sevensoft.ecreator.model.interaction.buddies;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 01-Aug-2005 23:23:29
 * 
 */
@Table("members_buddies")
public class Buddy extends EntityObject {

	private Item	account, buddy;

	public Buddy(RequestContext context) {
		super(context);
	}

	public Buddy(RequestContext context, Item member, Item buddy) {

		super(context);

		if (member.getInteraction().getBuddies().contains(buddy))
			return;

		this.account = member;
		this.buddy = buddy;

		save();

	}

	public Item getAccount() {
		return account.pop();
	}

	public Item getBuddy() {
		return buddy.pop();
	}
}