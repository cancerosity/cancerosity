package org.sevensoft.ecreator.model.interaction.views;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 5 Feb 2007 18:23:03
 *
 */
@Table("interaction_views_count")
public class ViewCount extends EntityObject {

	public static void viewed(RequestContext context, Item account, Item viewer) {

		ViewCount vc = SimpleQuery.get(context, ViewCount.class, "account", account, "viewer", viewer);
		if (vc == null) {
			new ViewCount(context, account, viewer);
		} else {
			vc.inc();
		}

	}

	private int		count;
	private Item	viewer;
	private Item	account;

	protected ViewCount(RequestContext context) {
		super(context);
	}

	public ViewCount(RequestContext context, Item account, Item viewer) {
		super(context);

		this.account = account;
		this.viewer = viewer;
		this.count = 1;

		save();
	}

	private void inc() {
		this.count++;
		save("count");
	}

}
