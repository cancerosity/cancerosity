package org.sevensoft.ecreator.model.interaction.buddies.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 24 Sep 2006 00:07:43
 * 
 * Shows how many buddies lists we are on
 *
 */
public class BuddiesInclusionMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.Buddies.enabled(context)) {
			return null;
		}

		Item account = (Item) context.getAttribute("account");
		if (account == null) {
			return null;
		}

		return super.string(context, params, account.getInteraction().getBuddiesWhoCount());
	}

	public Object getRegex() {
		return new String[] { "interaction_buddies_who", "interaction_buddies_inclusion" };
	}
}
