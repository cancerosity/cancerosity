package org.sevensoft.ecreator.model.interaction.views;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 06-Oct-2005 02:01:33
 * 
 */
@Table("interaction_views")
public class View extends EntityObject {

	private DateTime	date;

	/**
	 * Viewer is the person doing the viewing !
	 */
	private Item	account, viewer;

	public View(RequestContext context) {
		super(context);
	}

	public View(RequestContext context, Item account, Item viewer) {
		super(context);

		// clear off any existing
		SimpleQuery.delete(context, View.class, "account", account, "viewer", viewer);

		this.account = account;
		this.viewer = viewer;
		this.date = new DateTime();

		save();
	}

	public Item getAccount() {
		return account.pop();
	}

	public DateTime getDate() {
		return date;
	}

	public Item getViewer() {
		return viewer.pop();
	}

}
