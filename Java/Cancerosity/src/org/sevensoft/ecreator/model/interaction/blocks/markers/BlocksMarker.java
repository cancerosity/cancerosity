package org.sevensoft.ecreator.model.interaction.blocks.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.interaction.blocks.BlockedMembersHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 3 Dec 2006 19:13:54
 *
 */
public class BlocksMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.MemberBlocking.enabled(context)) {
			return null;
		}

		return super.link(context, params, new Link(BlockedMembersHandler.class), "account_link");
	}

	public Object getRegex() {
		return new String[] { "blocks_page", "interaction_blocks" };
	}
}
