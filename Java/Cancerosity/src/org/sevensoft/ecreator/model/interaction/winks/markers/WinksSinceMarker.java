package org.sevensoft.ecreator.model.interaction.winks.markers;

import java.util.Map;

import org.sevensoft.commons.samdate.Period;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 24 Sep 2006 09:08:48
 *
 */
public class WinksSinceMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.Nudges.enabled(context)) {
			return null;
		}

		if (!params.containsKey("period")) {
			return null;
		}

		Item account = (Item) context.getAttribute("account");
		if (account == null) {
			return null;
		}

		Period period = new Period(params.get("period"));
		int nudges = account.getInteraction().getWinksSince(period);

		return super.string(context, params, nudges);
	}

	public Object getRegex() {
		return new String[] { "winks_since", "interaction_nudges_since", "interaction_winks_since" };
	}

}
