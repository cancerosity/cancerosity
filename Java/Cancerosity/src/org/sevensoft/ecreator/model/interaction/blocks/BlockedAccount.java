package org.sevensoft.ecreator.model.interaction.blocks;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 04-Apr-2006 15:33:23
 *
 */
@Table("members_blocks")
public class BlockedAccount extends EntityObject {

	private Item	account, blocked;

	protected BlockedAccount(RequestContext context) {
		super(context);
	}

	public BlockedAccount(RequestContext context, Item a, Item blocked) {
		super(context);

		if (!a.getInteraction().getBlockedAccounts().contains(blocked)) {

			this.account = a;
			this.blocked = blocked;

			save();

		}
	}

}
