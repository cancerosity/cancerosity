package org.sevensoft.ecreator.model.interaction.winks;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 04-Oct-2005 01:35:29
 * 
 */
@Table("members_nudges")
public class Wink extends EntityObject {

	private DateTime	date;
	private Item	recipient, sender;

	public Wink(RequestContext context) {
		super(context);
	}

	public Wink(RequestContext context, Item sender, Item recipient) {
		super(context);

		this.sender = sender;
		this.recipient = recipient;
		this.date = new DateTime();

		save();
	}

	public DateTime getDate() {
		return date;
	}

	public Item getRecipient() {
		return recipient.pop();
	}

	public Item getSender() {
		return sender.pop();
	}
}
