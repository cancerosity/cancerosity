package org.sevensoft.ecreator.model.interaction.pm.boxes;

import org.sevensoft.ecreator.iface.admin.accounts.boxes.PmBoxHandler;
import org.sevensoft.ecreator.iface.frontend.interaction.pm.PmHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.interaction.Interaction;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 31-Oct-2005 10:40:08
 * 
 */
@Table("boxes_member_messages")
@Label("Private messages")
@HandlerClass(PmBoxHandler.class)
public class PmBox extends Box {

	public PmBox(RequestContext context) {
		super(context);
	}

	public PmBox(RequestContext context, String location) {
		super(context, location);
	}

	@Override
	protected String getCssIdDefault() {
		return "member-messages";
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	public String render(RequestContext context) {

		if (!Module.PrivateMessages.enabled(context))
			return null;

		Item account = (Item) context.getAttribute("account");
		if (account == null)
			return null;

		Interaction interaction = account.getInteraction();

		StringBuilder sb = new StringBuilder();
		sb.append(getTableTag());

		sb.append("<tr><td>");

		if (interaction.getUnreadPmCount() > 0) {

			sb.append("You have ");
			sb.append(interaction.getUnreadPmCount());
			sb.append(" unread message(s). ");

		} else {

			sb.append("You have no unread messages. ");

		}

		sb.append(new LinkTag(PmHandler.class, "inbox", "Click here") + " to view your inbox.</td></tr>");
		sb.append("</table>");

		return sb.toString();

	}

}
