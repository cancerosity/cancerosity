package org.sevensoft.ecreator.model.interaction.buddies.box;

import java.util.List;

import org.sevensoft.ecreator.iface.admin.accounts.boxes.BuddiesBoxHandler;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Captions;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 31-Oct-2005 10:39:15
 * 
 */
@Table("boxes_buddies")
@Label("Buddies")
@HandlerClass(BuddiesBoxHandler.class)
public class BuddiesBox extends Box {

	public BuddiesBox(RequestContext context) {
		super(context);
	}

	public BuddiesBox(RequestContext context, String location) {
		super(context, location);
	}

	@Override
	protected String getCssIdDefault() {
		return "buddies";
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	public String render(RequestContext context) {

		if (!Module.Buddies.enabled(context)) {
			return null;
		}

		Item account = (Item) context.getAttribute("account");
		if (account == null) {
			return null;
		}

		Captions captions = Captions.getInstance(context);

		/*
		 * If buddies is disabled for this user then do not show buddies box
		 */
		if (!PermissionType.Buddies.check(context, account))
			return null;

		StringBuilder sb = new StringBuilder(1024);
		sb.append(getTableTag());

		List<Item> buddies = account.getInteraction().getBuddies();

		if (buddies.size() > 0) {

			for (Item buddy : buddies) {

				sb.append("<tr><td class='status'>");
				if (buddy.isOnline()) {

					sb.append("<img src='template-data/buddy_on.gif'/>");

				} else {

					sb.append("<img src='template-data/buddy_off.gif'/>");

				}

				sb.append("</td>");
				sb.append("<td class='name'>" + new LinkTag(buddy.getUrl(), buddy.getDisplayName()) + "</td></tr>");
			}

		} else {

			sb.append("<tr><td>You have not added any users to your " + captions.getBuddiesCaption() + " list yet.</td></tr>");

		}

		sb.append("<tr><td class='bottom'>&nbsp;</td></tr>");
		sb.append("</table>");
		return sb.toString();
	}

}
