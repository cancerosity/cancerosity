package org.sevensoft.ecreator.model.interaction.winks;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 16 Jan 2007 11:44:06
 *
 *remove winks older than 2 weeks
 */
public class WinkBot implements Runnable {

	private RequestContext	context;

	public WinkBot(RequestContext context) {
		this.context = context;
	}

	public void run() {

		Query q = new Query(context, "select * from # where date<?");
		q.setTable(Wink.class);
		q.setParameter(new Date().removeDays(14));
		for (Wink wink : q.execute(Wink.class)) {

			Item recipient = wink.getRecipient();
			wink.delete();

			recipient.getInteraction().setWinkCount();
		}
	}

}
