package org.sevensoft.ecreator.model.interaction.userplane.chat.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.interaction.userplane.chat.UserplaneChatHandler;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 26 Nov 2006 17:07:03
 *
 */
public class UserplaneChatMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.UserplaneChat.enabled(context)) {
			logger.fine("[UserplaneChatMarker] userplane chat disabled, exiting");
			return null;
		}

		String id = params.get("id");
		if (id == null) {
			params.put("id", "interaction_userplane_chat");
		}

		Item account = (Item) context.getAttribute("account");
		if (account == null) {
			return super.link(context, params, new Link(LoginHandler.class), "interaction");
		}

		// check we have permission
		if (!PermissionType.UserplaneChat.check(context, account)) {
			logger.fine("[UserplaneChatMarker] domain does not have permission for userplane chat");
			return null;
		}

		Link link = new Link(UserplaneChatHandler.class);
		return super.link(context, params, link, "interaction");
	}

	public Object getRegex() {
		return "userplane_chat";
	}
}
