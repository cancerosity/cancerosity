package org.sevensoft.ecreator.model.interaction.buddies.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.interaction.buddies.BuddiesHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 03-Apr-2006 11:14:36
 *
 */
public class BuddiesMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.Buddies.enabled(context)) {
			return null;
		}

		Link link;
		if (params.containsKey("who")) {

			link = new Link(BuddiesHandler.class, "who");

		} else {

			link = new Link(BuddiesHandler.class);
		}

		return super.link(context, params, link, "account_link");
	}

	@Override
	public String getDescription() {
		return "Makes a link to the buddies page, or the buddies who included you page";
	}

	public Object getRegex() {
		return new String[] { "buddies_page", "interaction_buddies_list", "account_buddies" };
	}
}