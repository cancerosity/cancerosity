package org.sevensoft.ecreator.model.interaction.pm;

import java.util.List;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 10-Oct-2005 14:36:50
 * 
 */
@Table("members_messages_smilees")
public class Smilee extends EntityObject {

	public static List<Smilee> get(RequestContext context) {

		Query q = new Query(context, "select * from #");
		q.setTable(Smilee.class);
		return q.execute(Smilee.class);
	}

	private String	filename, text;

	public Smilee(RequestContext context) {
		super(context);
	}

	public Smilee(RequestContext context, String text, String path) {
		super(context);
		this.filename = path;
		this.text = text;
	}

	public String getFilename() {
		return this.filename;
	}

	public String getText() {
		return this.text;
	}

	public String replace(String content) {
		return content;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public void setText(String text) {
		this.text = text;
	}

}
