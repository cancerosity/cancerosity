package org.sevensoft.ecreator.model.interaction.userplane.recorder;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 12 Dec 2006 10:07:07
 *
 */
@Table("userplane_recorder_settings")
@Singleton
public class UserplaneRecorderSettings extends EntityObject {

	public static UserplaneRecorderSettings getInstance(RequestContext context) {
		return EntityObject.getSingleton(context, UserplaneRecorderSettings.class);
	}

	private int		maxRecordSeconds;
	private boolean	autoApprove;
	private String	noVideoImage;
	private String	flashcomServer;
	private String	domainId;

	/**
	 * 
	 */
	private String	recorderPageHeader;

	/**
	 * 
	 */
	private String	recorderPageFooter;

	/**
	 * 
	 */
	private String	viewerPageFooter;

	/**
	 * 
	 */
	private String	viewerPageHeader;

	/**
	 * 
	 */
	private String	videoFps;

	protected UserplaneRecorderSettings(RequestContext context) {
		super(context);
	}

	public String getDomainId() {
		return domainId;
	}

	public String getFlashcomServer() {
		return flashcomServer;
	}

	public final int getMaxRecordSeconds() {
		return maxRecordSeconds == 0 ? 45 : maxRecordSeconds;
	}

	public final String getNoVideoImage() {
		return noVideoImage;
	}

	public final String getRecorderPageFooter() {
		return recorderPageFooter;
	}

	public final String getRecorderPageHeader() {
		return recorderPageHeader;
	}

	public String getVideoFps() {
		return videoFps == null ? "16" : videoFps;
	}

	public final String getViewerPageFooter() {
		return viewerPageFooter;
	}

	public final String getViewerPageHeader() {
		return viewerPageHeader;
	}

	public boolean hasRecorderPageFooter() {
		return recorderPageFooter != null;
	}

	public boolean hasRecorderPageHeader() {
		return recorderPageHeader != null;
	}

	public boolean hasViewerPageFooter() {
		return viewerPageFooter != null;
	}

	public boolean hasViewerPageHeader() {
		return viewerPageHeader != null;
	}

	public final boolean isAutoApprove() {
		return autoApprove;
	}

	public final void setAutoApprove(boolean autoApprove) {
		this.autoApprove = autoApprove;
	}

	public final void setDomainId(String domainId) {
		this.domainId = domainId;
	}

	public final void setFlashcomServer(String flashcomServer) {
		this.flashcomServer = flashcomServer;
	}

	public final void setMaxRecordSeconds(int maxRecordSeconds) {
		this.maxRecordSeconds = maxRecordSeconds;
	}

	public final void setNoVideoImage(String noVideoImage) {
		this.noVideoImage = noVideoImage;
	}

	public final void setRecorderPageFooter(String recordPageFooter) {
		this.recorderPageFooter = recordPageFooter;
	}

	public final void setRecorderPageHeader(String recordPageHeader) {
		this.recorderPageHeader = recordPageHeader;
	}

	public final void setViewerPageFooter(String viewerPageFooter) {
		this.viewerPageFooter = viewerPageFooter;
	}

	public final void setViewerPageHeader(String viewerPageHeader) {
		this.viewerPageHeader = viewerPageHeader;
	}

	
	public final void setVideoFps(String videoFps) {
		this.videoFps = videoFps;
	}

}
