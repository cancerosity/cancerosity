package org.sevensoft.ecreator.model.interaction.pm.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.interaction.Interaction;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 24 Sep 2006 00:07:43
 * 
 * Returns the number of unread messages
 *
 */
public class MessagesCountMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.PrivateMessages.enabled(context)) {
			logger.fine("[MessagesCountMarker] messages disabled");
			return null;
		}

		Item account = (Item) context.getAttribute("account");
		if (account == null) {
			return null;
		}

		Interaction i = account.getInteraction();

		if (params.containsKey("sent")) {
			return super.string(context, params, i.getPmSentCount());

		} else if (params.containsKey("new") || params.containsKey("unread")) {
			return super.string(context, params, i.getUnreadPmCount());

		} else if (params.containsKey("received")) {
			return super.string(context, params, i.getPmInboxCount());
		}

		return null;
	}

	public Object getRegex() {
		return new String[] { "pm_count", "interaction_messages_count" };
	}

}
