package org.sevensoft.ecreator.model.interaction;

import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 24 Sep 2006 09:00:53
 * 
 * Interaction settings per account type
 *
 */
@Table("interaction_module")
public class InteractionModule extends EntityObject {

	/**
	 * The item type this setting is for
	 */
	private ItemType	itemType;

	/**
	 * Buddies header
	 */
	private String	buddiesHeader;

	private String	winksContent, viewsContent, pmInboxContent, pmSentContent;

	/**
	 * 
	 */
	private String	pmNotificationBody;

	/**
	 * 
	 */
	private boolean	simpleEditor;

	/**
	 * Custom markup used for listing winkers 
	 */
	private Markup	winksMarkup;

	private String	pmHeader, pmFooter;

	/**
	 * Custom markup used for listing viewers
	 */
	private Markup	viewsMarkup;

	private Markup	buddiesMarkup;

	/**
	 * Forward on restriction
	 */
	private String	restrictionForward;

	/**
	 * 
	 */
	private String	buddiesFooter;

	/**
	 * 
	 */
	private String	addedToBuddiesHeader;

	/**
	 * 
	 */
	private String	addedToBuddiesFooter;

	/**
	 * The markup used to generate the profile card when viewing a sent / received message in private messages
	 */
	private Markup	pmProfileMarkup;

	/**
	 * Send email when a message is received.
	 */
	private boolean	pmNotification;

	private String	winksHeader;

	private String	winksFooter;

	private InteractionModule(RequestContext context) {
		super(context);
	}

	public InteractionModule(RequestContext context, ItemType itemType) {
		super(context);
		this.itemType = itemType;

		save();
	}

	public final String getAddedToBuddiesFooter() {
		return addedToBuddiesFooter;
	}

	public final String getAddedToBuddiesHeader() {
		return addedToBuddiesHeader;
	}

	public final String getBuddiesFooter() {
		return buddiesFooter;
	}

	public final String getBuddiesHeader() {
		return buddiesHeader;
	}

	public final Markup getBuddiesMarkup() {
		return (Markup) (buddiesMarkup == null ? null : buddiesMarkup.pop());
	}

	public ItemType getItemType() {
		return itemType.pop();
	}

	private String getMessageNotificationBodyDefault() {
		return "[recipient],\n\nYou have a new message at [site] from [sender]. Please click here to read it:\n[url]\n\nThanks, [site] team";
	}

	public final String getPmFooter() {
		return pmFooter;
	}

	public final String getPmHeader() {
		return pmHeader;
	}

	public final String getPmInboxContent() {
		return pmInboxContent;
	}

	public String getPmNotificationBody() {
		return pmNotificationBody;
	}

	public final Markup getPmProfileMarkup() {
		return (Markup) (pmProfileMarkup == null ? null : pmProfileMarkup.pop());
	}

	public final String getPmSentContent() {
		return pmSentContent;
	}

	public final String getRestrictionForward() {
		return restrictionForward;
	}

	public final String getViewsContent() {
		return viewsContent;
	}

	public final Markup getViewsMarkup() {
		return (Markup) (viewsMarkup == null ? null : viewsMarkup.pop());
	}

	public final String getWinksContent() {
		return winksContent;
	}

	public final String getWinksFooter() {
		return winksFooter;
	}

	public String getWinksHeader() {
		return winksHeader;
	}

	public final Markup getWinksMarkup() {
		return (Markup) (winksMarkup == null ? null : winksMarkup.pop());
	}

	public boolean hasAddedToBuddiesFooter() {
		return addedToBuddiesFooter != null;
	}

	public boolean hasAddedToBuddiesHeader() {
		return addedToBuddiesHeader != null;
	}

	/**
	 * 
	 */
	public boolean hasBuddiesFooter() {
		return buddiesFooter != null;
	}

	public boolean hasBuddiesHeader() {
		return buddiesHeader != null;
	}

	public boolean hasMessagesInboxContent() {
		return pmInboxContent != null;
	}

	public boolean hasMessagesSentContent() {
		return pmSentContent != null;
	}

	public boolean hasNudgesContent() {
		return winksContent != null;
	}

	public boolean hasPmFooter() {
		return pmFooter != null;
	}

	public boolean hasPmHeader() {
		return pmHeader != null;
	}

	public boolean hasPmNotificationBody() {
		return pmNotificationBody != null;
	}

	public boolean hasRestrictionForward() {
		return restrictionForward != null;
	}

	public boolean hasViewsContent() {
		return viewsContent != null;
	}

	public boolean hasWinksFooter() {
		return winksHeader != null;
	}

	public boolean hasWinksHeader() {
		return winksHeader != null;
	}

	public final boolean isPmNotification() {
		return pmNotification;
	}

	public final boolean isSimpleEditor() {
		return simpleEditor;
	}

	public final void setAddedToBuddiesFooter(String addedToBuddiesFooter) {
		this.addedToBuddiesFooter = addedToBuddiesFooter;
	}

	public final void setAddedToBuddiesHeader(String addedToBuddiesHeader) {
		this.addedToBuddiesHeader = addedToBuddiesHeader;
	}

	public final void setBuddiesFooter(String buddiesFooter) {
		this.buddiesFooter = buddiesFooter;
	}

	public final void setBuddiesHeader(String s) {
		this.buddiesHeader = s;
	}

	public final void setBuddiesMarkup(Markup buddiesMarkup) {
		this.buddiesMarkup = buddiesMarkup;
	}

	public final void setPmFooter(String pmFooter) {
		this.pmFooter = pmFooter;
	}

	public final void setPmHeader(String pmHeader) {
		this.pmHeader = pmHeader;
	}

	public final void setPmInboxContent(String messagesInboxContent) {
		this.pmInboxContent = messagesInboxContent;
	}

	public void setPmNotification(boolean pmNotification) {
		this.pmNotification = pmNotification;
	}

	public void setPmNotificationBody(String s) {
		this.pmNotificationBody = s;
	}

	public final void setPmProfileMarkup(Markup privateMessagesProfileMarkup) {
		this.pmProfileMarkup = privateMessagesProfileMarkup;
	}

	public final void setPmSentContent(String messagesSentContent) {
		this.pmSentContent = messagesSentContent;
	}

	public final void setRestrictionForward(String restrictionForward) {
		this.restrictionForward = restrictionForward;
	}

	public final void setSimpleEditor(boolean simpleEditor) {
		this.simpleEditor = simpleEditor;
	}

	public final void setViewsContent(String viewsContent) {
		this.viewsContent = viewsContent;
	}

	public final void setViewsMarkup(Markup viewsMarkup) {
		this.viewsMarkup = viewsMarkup;
	}

	public final void setWinksContent(String nudgesContent) {
		this.winksContent = nudgesContent;
	}

	public final void setWinksFooter(String winksFooter) {
		this.winksFooter = winksFooter;
	}

	public final void setWinksHeader(String winksHeader) {
		this.winksHeader = winksHeader;
	}

	public final void setWinksMarkup(Markup winksMarkup) {
		this.winksMarkup = winksMarkup;
	}

}
