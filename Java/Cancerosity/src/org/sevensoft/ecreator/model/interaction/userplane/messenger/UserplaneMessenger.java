package org.sevensoft.ecreator.model.interaction.userplane.messenger;

import org.jdom.Document;
import org.jdom.Element;
import org.sevensoft.ecreator.model.accounts.sessions.AccountSession;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 26 Nov 2006 16:44:57
 * 
 * This class contains the code for generating the userplane documents
 *
 */
public class UserplaneMessenger {

	public static final int				PopupHeight	= 700;
	public static final int				PopupWidth	= 480;

	private final RequestContext			context;
	private final UserplaneMessengerSettings	userplaneMessengerSettings;

	public UserplaneMessenger(RequestContext context) {
		this.context = context;
		this.userplaneMessengerSettings = UserplaneMessengerSettings.getInstance(context);
	}

	private void addImagePath(Element memberEl, Item member) {

		Element imagePathEl = new Element("imagepath");

		if (member.hasApprovedImages()) {

			Img image = member.getApprovedImage();
			if (image != null) {
				imagePathEl.setText("http://" + userplaneMessengerSettings.getImagesServer() + "/" + image.getThumbnailFilename());
			}
		}

		memberEl.addContent(imagePathEl);
	}

	public void declineChatRequest(Item target, Item originator) {
		SimpleQuery.delete(context, UserplaneMessengerRequest.class, "originator", originator, "target", target);
	}

	public Document getDomainPreferencesDoc() {

		Element root = new Element("icappserverxml");
		Document doc = new Document(root);

		//		root.addContent(new Element("allowCalls").setText(""));
		root.addContent(new Element("characterlimit").setText("200"));

		String forbiddenWordsList = userplaneMessengerSettings.getForbiddenWordsList();
		if (forbiddenWordsList == null)
			forbiddenWordsList = "";

		root.addContent(new Element("forbiddenwordslist").setText(forbiddenWordsList));
		root.addContent(new Element("maxvideobandwidth").setText("20000"));

		{
			Element lineEl = new Element("line1");
			if (userplaneMessengerSettings.hasProfileAttribute1()) {
				lineEl.setText(userplaneMessengerSettings.getProfileAttribute1().getName());
			}
			root.addContent(lineEl);
		}

		{
			Element lineEl = new Element("line2");
			if (userplaneMessengerSettings.hasProfileAttribute2()) {
				lineEl.setText(userplaneMessengerSettings.getProfileAttribute2().getName());
			}
			root.addContent(lineEl);
		}

		{
			Element lineEl = new Element("line3");
			if (userplaneMessengerSettings.hasProfileAttribute3()) {
				lineEl.setText(userplaneMessengerSettings.getProfileAttribute3().getName());
			}
			root.addContent(lineEl);
		}

		{
			Element lineEl = new Element("line4");
			if (userplaneMessengerSettings.hasProfileAttribute4()) {
				lineEl.setText(userplaneMessengerSettings.getProfileAttribute4().getName());
			}
			root.addContent(lineEl);
		}

		root.addContent(new Element("avEnabled").setText("true"));
		root.addContent(new Element("clickableUserName").setText("true"));
		root.addContent(new Element("clickableTextUserName").setText("false"));
		root.addContent(new Element("printButton").setText("true"));
		root.addContent(new Element("buddyListButton").setText("false"));
		root.addContent(new Element("preferencesButton").setText("true"));
		root.addContent(new Element("smileyButton").setText("true"));
		root.addContent(new Element("blockButton").setText("false"));
		root.addContent(new Element("addBuddyEnabled").setText("false"));
		root.addContent(new Element("connectionTimeout").setText("60"));
		root.addContent(new Element("sendConnectionListInterval").setText("0"));
		root.addContent(new Element("sendArchive").setText("false"));
		root.addContent(new Element("sendTextToImages").setText("false"));
		root.addContent(new Element("hideDropShadows").setText("true"));
		root.addContent(new Element("hideHelp").setText("true"));
		root.addContent(new Element("showLocalUserIcon").setText("true"));
		root.addContent(new Element("maxxmlretries").setText("5"));
		root.addContent(new Element("conferenceCallEnabled").setText("false"));

		return doc;
	}

	public Document getEmptyDoc() {

		Element root = new Element("icappserverxml");
		Document doc = new Document(root);

		return doc;
	}

	/**
	 * Description: 	This action is used to authenticate the user and give them access to use the Webmessenger. 
	 * 
	 * In the client-side HTML you will receive later, you will pass the communication server the sessionGUID of the person who is currently logged in.  
	 * 
	 * The communication server then passes this back to you here so that you can authenticate and authorize the user for this application.  
	 * You then return the memberID for the current sessionGUID, or "INVALID" if the sessionGUID is not valid.  
	 * 
	 * Our Webmessenger system is designed to be ultimately flexible.  
	 * Many of our clients never send the actual memberID of a user to that user browser.  
	 * Instead, they set a sessionGUID and then relate that GUID to the memberID in the database.  
	 * Also, some sites need to pass an additional key.  
	 * If you do not use this feature, you will pass the memberID as the sessionGUID in the HTML then just return the sessionGUID as the memberID when this action is called.
	 * 
	 * Parameters:
	 * action:			getMemberID
	 * domainID:		The identifier for the domain the user is on
	 * sessionGUID:	The site wide identifier for the particular user
	 * key:	Optional additional identifier information
	 * callID:	A unique identifier for this XML call
	 * 
	 * Returns
	 * memberID:		The id of the member being looked up or �INVALID� if not found
	 * 
	 * 
	 * 
	 * ---- Sams comments -----
	 * 
	 * This method returns the document that tells userplane the id of the member who is chatting. Userplane will then use this when sending in the other requests.
	 * The member is indentified by the sessionGUID. If we are not letting this member chat (blocked, banned, invalid etc)
	 * then we should return INVALID
	 */
	public Document getMemberIdDoc(String sessionGUID) {

		/* 
		 * get the member from the session id that the userplane app was sent, NOT the session id for this request. The session Id for this request will be the session id
		 * assigned to the userplane system and not the original member, as all requests to this handler come from the userplane system
		 */
		Item item = AccountSession.getItem(context, sessionGUID);

		Element root = new Element("icappserverxml");
		Document doc = new Document(root);

		Element memberIdEl = new Element("memberID");
		if (item == null) {

			memberIdEl.setText("INVALID");

		} else {

			memberIdEl.setText(item.getIdString());
		}

		root.addContent(memberIdEl);

		return doc;
	}

	/**
	 * notifyConnectionClosed( domainID, memberID, targetMemberID, 	callID )
	 * 
	 * Description: 	This action will be called every time an Webmessenger window is closed.
	 * If this window caused a startConversation action, and a startIC action never happened on the other end, 
	 * you now know that the originating user is gone so you should not open up an Webmessenger window on the target user�s computer.  
	 * 
	 * If there are undelivered messages (the user closed their window before the destination user opened theirs), 
	 * this requests is made as a FORM POST (all parameters are still on the query string, though) and a single FORM element of �xmlData� is sent in the following format:
	 * 
	 * 	 <?xml version='1.0' encoding='iso-8859-1'?>
	 * 		<undeliveredMessages>
	 * 			<message>First message typed</message>
	 * 			<message>Other messages typed</message>
	 * 		</undeliveredMessages>
	 * 
	 * There will be a single <message> entry for each message typed by the user in the WM window.
	 * 
	 * If you are using ASP.NET, you will need to make sure the following line is at the top of your WMXML page:
	 * <%@ Page Language="C#" ValidateRequest="false" %>
	 * 
	 * Parameters:
	 * action:				notifyConnectionClosed
	 * domainID:			The identifier for the domain the user is on
	 * memberID:			The user�s memberID
	 * targetMemberID:	The user who memberID was talking to
	 * callID:				A unique identifier for this XML call
	 * 
	 * Returns nothing
	 * 
	 * 
	 * -- sams coments ---
	 * 
	 * this is useful because we use this method to cancel off any pending startICs that have not yet been actioned - otherwise the other member
	 * would open up his messenger after the other person disappeared.
	 * 
	 * All we need to do is clear off and pending message objects in the database.
	 * 
	 */
	public void notifyConnectionClosed(Item member, Item targetMember) {
		SimpleQuery.delete(context, UserplaneMessengerRequest.class, "member", member, "targetMember", targetMember);
	}

	/**
	 * startConversation( domainID, memberID, targetMemberID, callID )
	 * 
	 * Description: 	You only need to handle this action if you do not license Userplane's presenceDetection or UserList application.
	 * 
	 * Submits a request for an Webmessenger window.  You will need to open an Webmessenger window 
	 * on targetMemberID�s computer that points to memberID.  
	 * This action will continually get called until that window is open every time the memberID user types a message 
	 * so it is a good idea to only react to this if you haven�t seen this action recently.
	 * 
	 * See the section titled �Use case for startConversation, startIC, and notifyConnectionClosed� for a detailed description 
	 * of how Userplane uses this action to launch Webmessenger sessions.
	 * 
	 * Parameters:
	 * action:			startConversation
	 * domainID:		The identifier for the domain the user is on
	 * memberID:		The user�s memberID
	 * targetMemberID:	The user to popup a window on
	 * callID:			A unique identifier for this XML call
	 * 
	 * Returns nothing
	 * 
	 * ---- sams comments -----
	 * 
	 * This action is called when a member opens up the messenger on their system by clicking on start chat (or whatever).
	 * It is a call that is used to log the fact the member wants to chat to the target member, so we need to create some object to keep track of that.
	 */
	public void startConversation(Item member, Item targetMember) {

		// create a new userplane messenger request object to log the chat request.
		new UserplaneMessengerRequest(context, member, targetMember);

	}

	/**
	 *
	 * 
	 * startIC( domainID, memberID, targetMemberID, callID ) 
	 * 
	 * Description: 	This action will be called every time an Webmessenger window is opened. 
	 * You need to return all data that is necessary to display the Webmessenger window to the user
	 * 
	 *  If you opened up this window automatically in response to a startConversation action, you now know that it was successful. 
	 *  In the case that this window was opened automatically, notice that the memberID and targetMemberID are going to be the opposite from what was passed into startConversation 
	 *  since it was called from the other users point of view. 
	 *  
	 *  See the section titled �Use case for startConversation, startIC, and notifyConnectionClosed� for a detailed description of how Userplane uses this action to launch Webmessenger sessions.
	 *  
	 *  This XML function is a consolidation of 5 previous XML calls that happened when an WM window first opens (getMemberInfo x 2, getMemberPreferences, getBlockedStatus, and notifyConnectionOpen).  
	 *  By implementing this function, you should drastically reduce WMXML load on your servers.  
	 *  To use this function, you will need to confirm that your WMXML conforms to version 1.8.5 or higher.  Use the WMXML tester to do so.  
	 *  Once you are comfortable with your changes, notify your Userplane representative to update your install to 1.8.5 (or higher)
	 *  
	 *   Parameters:
	 *   action:	startIC
	 *   domainID:		The identifier for the domain the user is on
	 *    memberID:		The user�s memberID
	 *    targetMemberID:		The user who memberID wants to talk to
	 *    callID:			A unique identifier for this XML call
	 *    
	 *    Returns (see full XML example in accompanied files)
	 *    
	 *    member:		All encapsulated data is for the user viewing the WM window
	 *    
	 *   	displayName:		The name that is displayed in the IM window
	 *   	imagePath:		The full URL to the 
	 *   	audioSend			If true, user can send audio.
	 *   	videoSend			If true, user can send video.
	 *   	audioReceive		If true, user can receive audio.
	 *   	videoReceive		if true, user can receive video.
	 *   	kissSmackEnabled	Whether or not the kiss smack button is visible for this user.  Must be specified by the text �true� or �false�. 	
	 *   	showErrors:		Not currently used
	 *   	sound:				Whether the user�s sound is initially on
	 *   	focus:				Whether the user wants their windows to focus
	 *   	autoOpenAV:		Set true or false depending on whether you want the user�s AV area to open when the WM loads
	 *   	autoStartAudio:	If you set autoOpenAV to true, use this value to specify if the audio starts automatically
	 *   	autoStartVideo:	If you set autoOpenAV to true, use this value to specify if the video starts automatically
	 *   	backgroundColor	Allows you to change the background color in the area between the bottom of the button bar and the top of the branding bar (or bottom of the screen if no branding bar is visible).
	 *   	fontColor			Allows you to change the color of the font that the user's name 					is written in if clickableUserName is false. 
	 *   	noTextEntry		Allows you to prevent a user from typing.  If they click on the input area, a javascript event is sent to your HTML page allowing you to handle that case if you wish.
	 *   	sessionTimeout	Specify the number of seconds a user is allowed to use the Webmessenger.  
	 *   						When a timeout occurs, the user will be disconnected and their interface will switch to the 
	 *   							standard error screen containing the text provided in the sessionTimeoutMessage parameter.  A javascript event �Session.Timeout� will also be fired.  
	 *   						See the WMHTML documentation for more.
	 *   	sessionTimeoutMessage	The message the user will see when their session times out.
	 *   	quickMessageList	Provides an interface for inserting pre-written messages.  
	 *   						Each message is contained in a message child element, and each message element contains title and body elements.  
	 *   						The title will appear in the interface as a link.  Clicking the title link will insert the body text into the text input area.
	 *   
	 *   targetMember:	All encapsulated data is for the user that is being talked to in the WM window
	 *   
	 *   	displayName:		The name that is displayed in the IM window
	 *   	line1:	The first line of the user's profile, is the data value for the label in getDomainPreferences
	 *   	line2:	The second line of the user's profile, is the data value for the label in getDomainPreferences
	 *   	line3:	The third line of the user's profile, is the data value for the label in getDomainPreferences
	 *   	line4:	The fourth line of the user's profile, is the data value for the label in getDomainPreferences
	 *   	imagePath:	The full url to the image
	 *   	avEnabled	Deprecated, see audioSend, videoSend. Whether this user has Audio and Video enabled.  Must be specified by the text �true� or �false�. This value will be forced false if getDomainPreferences returns false for avEnabled.
	 *   	audioSend	True if the target user can send audio.
	 *   	videoSend	True if the target user can send video.
	 *   	blocked	Whether this user is blocked by the user from the member section above
	 *   	backgroundColor	The hexadecimal color value of the area at the top of the window behind the target member's name and information.
	 *   	fontColor	The hexadecimal color value of the font that the target member's name and information is written in.
	 *   
	 *   
	 *   
	 *   ---- sams comments -----
	 *   
	 *   This action is called when userplane wants to get the settings for the messenger it has just opened up. It will send in the member id of 
	 *   the member whose computer it is, and also the target member which is the member the user is talking to.
	 */
	public Document startIC(Item member, Item targetMember) {

		Element root = new Element("icappserverxml");
		Document doc = new Document(root);

		Element memberEl = new Element("member");
		root.addContent(memberEl);

		memberEl.addContent(new Element("displayname").setText(member.getDisplayName()));

		addImagePath(memberEl, member);

		memberEl.addContent(new Element("avEnabled").setText("true"));
		memberEl.addContent(new Element("audioSend").setText("true"));
		memberEl.addContent(new Element("videoSend").setText("true"));
		memberEl.addContent(new Element("audioReceive").setText("true"));
		memberEl.addContent(new Element("videoReceive").setText("true"));
		memberEl.addContent(new Element("kissSmackEnabled").setText("true"));
		memberEl.addContent(new Element("sound").setText("true"));
		memberEl.addContent(new Element("focus").setText("true"));
		memberEl.addContent(new Element("noTextEntry").setText("false"));
		memberEl.addContent(new Element("sessionTimeout").setText("-1"));
		memberEl.addContent(new Element("sessionTimeoutMessage").setText("Your session has timed out"));

		Element targetMemberEl = new Element("targetMember");
		root.addContent(targetMemberEl);

		targetMemberEl.addContent(new Element("displayname").setText(targetMember.getDisplayName()));
		addImagePath(targetMemberEl, targetMember);

		{
			Element line1El = new Element("line1");
			if (userplaneMessengerSettings.hasProfileAttribute1()) {
				String line = targetMember.getAttributeValue(userplaneMessengerSettings.getProfileAttribute1());
				if (line != null) {
					line1El.setText(line);
				}
			}
			targetMemberEl.addContent(line1El);
		}

		{
			Element line2El = new Element("line2");
			if (userplaneMessengerSettings.hasProfileAttribute2()) {
				String line = targetMember.getAttributeValue(userplaneMessengerSettings.getProfileAttribute2());
				if (line != null) {
					line2El.setText(line);
				}
			}
			targetMemberEl.addContent(line2El);
		}

		// PROFILE LINE 3
		{
			Element line3El = new Element("line3");
			if (userplaneMessengerSettings.hasProfileAttribute3()) {
				String line = targetMember.getAttributeValue(userplaneMessengerSettings.getProfileAttribute3());
				if (line != null) {
					line3El.setText(line);
				}
			}
			targetMemberEl.addContent(line3El);
		}

		// PROFILE LINE 4
		{
			Element line4El = new Element("line4");
			if (userplaneMessengerSettings.hasProfileAttribute4()) {
				String line = targetMember.getAttributeValue(userplaneMessengerSettings.getProfileAttribute4());
				if (line != null) {
					line4El.setText(line);
				}
			}
			targetMemberEl.addContent(line4El);
		}

		targetMemberEl.addContent(new Element("blocked").setText("false"));
		targetMemberEl.addContent(new Element("backgroundColor").setText(""));
		targetMemberEl.addContent(new Element("fontColor").setText(""));

		return doc;
	}
}
