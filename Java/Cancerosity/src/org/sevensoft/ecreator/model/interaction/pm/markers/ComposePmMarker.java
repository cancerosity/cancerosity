package org.sevensoft.ecreator.model.interaction.pm.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.interaction.pm.PmHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Captions;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class ComposePmMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item profile, String location, int x, int y) {

		if (!Module.PrivateMessages.enabled(context)) {
			return null;
		}

		/*
		 * Make sure we are not trying to send a message to ourselves
		 */
		Item account = (Item) context.getAttribute("account");
		if (profile.equals(account)) {
			return null;
		}

		/*
		 * Make sure we're not blocked
		 */
		if (account != null) {
			if (profile.getInteraction().isBlocking(account)) {
				logger.fine("[ComposeMessageMarker] this profile is blocking the logged in member");
				return null;
			}
		}

		//		/*
		//		 * Only show message tag if this member can read messages, or can possibly read messages with an upgrade.
		//		 */
		//		if (!PermissionType.ReadMessages.available(context, profile)) {
		//			logger.fine("[ComposeMessageMarker] read messages permission not available for profile");
		//			return null;
		//		}

		if (!params.containsKey("text")) {
			params.put("text", "Send " + Captions.getInstance(context).getPmCaption().toLowerCase());
		}

		if (!params.containsKey("id")) {
			params.put("id", "interaction_message");
		}
		
		if (!params.containsKey("class")) {
			params.put("class", "interaction");
		}

		return super.link(context, params, new Link(PmHandler.class, "compose", "recipient", profile));
	}

	@Override
	public String getDescription() {
		return "Makes a link to the private message composition page";
	}

	public Object getRegex() {
		return new String[] { "pm_compose", "interaction_messages" };
	}
}
