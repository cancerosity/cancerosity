package org.sevensoft.ecreator.model.interaction.buddies.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.interaction.buddies.BuddiesHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class BuddyMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item profile, String location, int x, int y) {

		logger.fine("[BuddyMarker] ");

		if (!Module.Buddies.enabled(context)) {
			logger.fine("[BuddyMarker] no permissions for buddies");
			return null;
		}
		//
		//		if (!PermissionType.Buddies.available(context, item)) {
		//			logger.fine("[AddBuddyMarker] no permissions for buddies");
		//			return null;
		//		}

		/*
		 * Make sure we are not ourselves !
		 */
		if (profile.equals(context.getAttribute("account"))) {
			logger.fine("[BuddyMarker] cannot add buddy on own profile");
			return null;
		}

		if (!params.containsKey("id")) {
			params.put("id", "interaction_buddies");
		}

		Item account = (Item) context.getAttribute("account");
		logger.fine("[BuddyMarker] account=" + account);

		if (params.containsKey("remove")) {

			// if not in our list then then show remove
			if (account != null && account.getInteraction().getBuddies().contains(profile)) {
				return super.link(context, params, new Link(BuddiesHandler.class, "remove", "buddy", profile), "interaction");
			}

		} else {

			// if not in our list then then show add
			if (account == null || !account.getInteraction().getBuddies().contains(profile)) {
				return super.link(context, params, new Link(BuddiesHandler.class, "add", "buddy", profile), "interaction");
			}
		}

		return null;
	}

	@Override
	public String getDescription() {
		return "Shows the add or remove buddies button (dependant on if the profile is already on the buddies list or not)";
	}

	public Object getRegex() {
		return new String[] { "buddies", "interaction_buddies", "interaction_buddies_add" };
	}

}
