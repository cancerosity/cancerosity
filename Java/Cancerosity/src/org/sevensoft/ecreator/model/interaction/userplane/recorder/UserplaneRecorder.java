package org.sevensoft.ecreator.model.interaction.userplane.recorder;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.sevensoft.ecreator.model.accounts.sessions.AccountSession;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 12 Dec 2006 10:06:47
 *
 */
public class UserplaneRecorder {

	private static final Logger		logger	= Logger.getLogger("ecreator");

	private UserplaneRecorderSettings	settings;
	private RequestContext			context;

	public UserplaneRecorder(RequestContext context) {
		this.context = context;
		this.settings = UserplaneRecorderSettings.getInstance(context);
	}

	public Document getAdminIPAddresses() {
		return null;
	}

	public Document getDeletedRecordings() {

		List<Recording> recordings = SimpleQuery.execute(context, Recording.class, "status", "deleted");

		Element root = new Element("vrappserver");
		Document doc = new Document(root);

		for (Recording recording : recordings) {

			Element memberEl = new Element("memberID");
			root.addContent(memberEl);

			Item account = recording.getAccount();

			memberEl.setAttribute("recordingName", recording.getName());
			memberEl.setAttribute("status", "all");
			memberEl.setText(account.getIdString());
		}

		return doc;
	}

	public Document getDomainPreferencesDoc() {

		Element root = new Element("vrappserver");
		Document doc = new Document(root);

		root.addContent(new Element("maxxmlretries").setText("5"));
		root.addContent(new Element("maxRecordSeconds").setText(String.valueOf(settings.getMaxRecordSeconds())));
		root.addContent(new Element("autoApprove").setText("false"));
		root.addContent(new Element("sendRecordingListInterval").setText("1"));

		return doc;
	}

	public Document getEmptyDoc() {

		Element root = new Element("vrappserver");
		Document doc = new Document(root);

		return doc;
	}

	public Document getMemberID(String sessionGUID) {

		Item item = AccountSession.getItem(context, sessionGUID);
		User user = User.getUserFromSession(context, sessionGUID);

		Element root = new Element("vrappserver");
		Document doc = new Document(root);

		if (item == null && user == null) {

			root.addContent(new Element("memberID").setText("INVALID"));

		} else {

			if (item == null) {

				root.addContent(new Element("memberID").setText("0"));

			} else {

				root.addContent(new Element("memberID").setText(item.getIdString()));
				root.addContent(new Element("videoEnabled").setText("true"));
				root.addContent(new Element("audioKbps").setText("44"));
				root.addContent(new Element("videoKbps").setText("160"));
				root.addContent(new Element("videoFps").setText(settings.getVideoFps()));
				root.addContent(new Element("videoSize").setText("2"));
			}

			if (user == null) {

				root.addContent(new Element("admin").setText("false"));

			} else {

				root.addContent(new Element("admin").setText("true"));
			}
		}

		return doc;
	}

	public Document notifyRecordingChange(Item account, String status, String exists) {

		logger.fine("[UserplaneRecorder] notifyRecordingChange account=" + account + ", status=" + status + ", exists=" + exists);

		if ("new".equalsIgnoreCase(status)) {

			if ("true".equals(exists)) {

				Recording recorder = account.getInteraction().getRecording();
				if (recorder == null) {

					account.getInteraction().addRecording();

				} else {

					recorder.setStatus(status);
					recorder.save();
				}
			}

		} else if ("approved".equalsIgnoreCase(status)) {

			Recording recorder = account.getInteraction().getRecording();
			if (recorder != null) {

				if ("true".equals(exists)) {

					recorder.setStatus(status);
					recorder.save();

				} else if ("false".equals(exists)) {

					recorder.getAccount().getInteraction().removeRecording();

				}
			}
		}

		account.getInteraction().setRecordingsCount();

		return getEmptyDoc();
	}

	public Document sendRecordingList(String xmlData) throws JDOMException, IOException {

		Document doc = new SAXBuilder().build(new StringReader(xmlData));
		Element root = doc.getRootElement();

		List<String> list = new ArrayList();

		List<Element> members = root.getChildren("member");
		for (Element memberEl : members) {

			String memberId = memberEl.getAttributeValue("id");
			list.add(memberId);

			Item account = EntityObject.getInstance(context, Item.class, memberEl.getAttributeValue("id"));
			if (account != null) {

				List<Element> recordings = root.getChildren("recording");
				for (Element recordingEl : recordings) {

					String status = recordingEl.getAttributeValue("status");

					Recording recording = account.getInteraction().getRecording();
					if (recording != null && status != null) {

						recording.setStatus(status);
						recording.save();
					}
				}
			}
		}

		// remove all recordings not in this list
		for (Recording recorder : Recording.get(context)) {
			if (!list.contains(recorder.getAccount().getIdString())) {
				recorder.getAccount().getInteraction().removeRecording();
			}
		}

		return getEmptyDoc();
	}

}
