package org.sevensoft.ecreator.model.interaction.userplane.recorder;

import java.util.List;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 14 Dec 2006 13:09:16
 *
 */
@Table("userplane_recorder_recordings")
public class Recording extends EntityObject {

	public static List<Recording> get(RequestContext context) {
		return SimpleQuery.execute(context, Recording.class);
	}

	public static List<Recording> getPending(RequestContext context) {
		return SimpleQuery.execute(context, Recording.class, "status", "new");
	}

	/**
	 * The account who owns this recording
	 */
	private Item	account;

	/**
	 * The name of this recording
	 */
	private String	name;

	/**
	 * Flags if this recording has been approved
	 */
	private String	status;

	protected Recording(RequestContext context) {
		super(context);
	}

	public Recording(RequestContext context, Item account, String name) {
		super(context);

		this.account = account;
		this.name = name;
		this.status = "new";

		save();
	}

	public final Item getAccount() {
		return account.pop();
	}

	public final String getName() {
		return name;
	}

	public final String getStatus() {
		return status;
	}

	public final void setStatus(String status) {
		this.status = status;
	}

}
