package org.sevensoft.ecreator.model.interaction.userplane.messenger;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 26 Nov 2006 14:24:08
 *
 */
@Table("userplane_messenger_requests")
public class UserplaneMessengerRequest extends EntityObject {

	/**
	 * Returns any pending request for this member
	 */
	public static UserplaneMessengerRequest get(RequestContext context, Item target) {
		return SimpleQuery.get(context, UserplaneMessengerRequest.class, "target", target);
	}

	/**
	 * The member who originated the chat
	 */
	private Item	originator;

	/**
	 * The member they want to chat to
	 */
	private Item	target;

	protected UserplaneMessengerRequest(RequestContext context) {
		super(context);
	}

	UserplaneMessengerRequest(RequestContext context, Item originator, Item target) {
		super(context);

		this.originator = originator;
		this.target = target;

		save();
	}

	public final Item getOriginator() {
		return originator.pop();
	}

	public final Item getTarget() {
		return target.pop();
	}

}
