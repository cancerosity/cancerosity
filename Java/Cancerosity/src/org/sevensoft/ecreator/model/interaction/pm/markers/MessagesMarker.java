package org.sevensoft.ecreator.model.interaction.pm.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.interaction.pm.PmHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.interaction.Interaction;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 03-Apr-2006 11:41:59
 * 
 * Returns a link to the member's inbox
 *
 */
public class MessagesMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		logger.fine("[MessagesMarker] params=" + params);

		if (!Module.PrivateMessages.enabled(context)) {
			logger.fine("[MessagesMarker] PrivateMessages disabled, exiting");
			return null;
		}

		if (params.containsKey("new")) {

			Item account = (Item) context.getAttribute("account");
			logger.fine("[MessagesMarker] account=" + account);

			// only show if we have new messages
			if ("1".equals(params.get("new"))) {

				// if we are not logged in we cannot have new winks !
				if (account == null) {
					return null;
				}

				Interaction i = account.getInteraction();

				// if we do not have any winks then exit
				if (i.getUnreadPmCount() == 0) {
					return null;
				}

			} else if ("0".equals(params.get("new"))) {

				// if we are logged in and we have new winks then exit
				if (account != null && account.getInteraction().getUnreadPmCount() > 0) {
					return null;
				}

			} else {

				// param must be 1 or 0
				return null;

			}
		}

		return link(context, params, new Link(PmHandler.class), "account_link");
	}

	@Override
	public String getDescription() {
		return "Makes a link to the pm inbox page";
	}

	public Object getRegex() {
		return new String[] { "pm_inbox", "interaction_messages_inbox", "account_messages" };
	}
}