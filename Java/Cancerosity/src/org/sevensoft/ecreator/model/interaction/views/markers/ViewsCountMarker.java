package org.sevensoft.ecreator.model.interaction.views.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 7 Dec 2006 17:19:55
 * 
 * Shows how many times this profile has viewed you (the logged in member)
 *
 */
public class ViewsCountMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		if (!Module.MemberViewers.enabled(context)) {
			return null;
		}
		
		Item account = (Item) context.getAttribute("account");
		if (account == null) {
			return null;
		}

		int count = account.getInteraction().getViewsCount( item);
		return super.string(context, params, count);
	}

	public Object getRegex() {
		return new String[] { "views_count", "interaction_views_count" };
	}

}
