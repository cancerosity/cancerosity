package org.sevensoft.ecreator.model.interaction.userplane.messenger.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.account.RestrictionHandler;
import org.sevensoft.ecreator.iface.frontend.interaction.userplane.messenger.UserplaneMessengerHandler;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.interaction.userplane.messenger.UserplaneMessenger;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 26 Nov 2006 17:07:03
 *
 */
public class UserplaneMessengerMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item profile, String location, int x, int y) {

		if (!Module.UserplaneMessenger.enabled(context)) {
			return null;
		}

		Item account = (Item) context.getAttribute("account");
		if (account == null || !PermissionType.UserplaneMessenger.check(context, account)) {

			logger.fine("[UserplaneMessengerMarker] account does not have permission for userplane msg, showing restriction handler");
			return super.link(context, params, new Link(RestrictionHandler.class), "interaction");
		}

		if (!PermissionType.UserplaneMessenger.check(context, profile)) {

			logger.fine("[UserplaneMessengerMarker] profile does not have permission for userplane msg, not showing marker");
			return null;
		}
		
		// the other person must be online
		if (!profile.isOnline()) {
			return null;
		}

		// check we are not viewing our profile
		if (profile.equals(account)) {
			return null;
		}

		if (!params.containsKey("text")) {
			params.put("text", "Start messenger");
		}

		if (!params.containsKey("id")) {
			params.put("id", "interaction_userplane_msgr");
		}

		if (!params.containsKey("class")) {
			params.put("class", "interaction");
		}

		if (!params.containsKey("width")) {
			params.put("width", String.valueOf(UserplaneMessenger.PopupWidth));
		}

		if (!params.containsKey("height")) {
			params.put("height", String.valueOf(UserplaneMessenger.PopupHeight));
		}

		params.put("popup", "1");

		params.put("resize", "no");
		params.put("windowname", "userplane_msgr_" + profile.getId());

		return super.link(context, params, new Link(UserplaneMessengerHandler.class, null, "target", profile));

	}

	public Object getRegex() {
		return "userplane_messenger";
	}
}
