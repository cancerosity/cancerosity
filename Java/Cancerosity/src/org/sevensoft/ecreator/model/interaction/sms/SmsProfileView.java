package org.sevensoft.ecreator.model.interaction.sms;

import java.io.IOException;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.marketing.sms.SmsNumberException;
import org.sevensoft.ecreator.model.marketing.sms.SmsSenderException;
import org.sevensoft.ecreator.model.marketing.sms.SmsSettings;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsCreditsException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayAccountException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayMessagingException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsMessageException;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 12 Jun 2007 14:24:03
 *
 */
public class SmsProfileView {

	private final Item		item;
	private final RequestContext	context;
	private final Item		account;

	public SmsProfileView(RequestContext context, Item profile, Item account) throws SmsGatewayException, SmsMessageException, IOException,
			SmsGatewayAccountException, SmsGatewayMessagingException, SmsCreditsException, SmsNumberException, SmsSenderException {
		this.context = context;
		this.item = profile;
		this.account = account;

		// send sms if profile has enough credits
		if (profile.getSmsCredits() > 0) {

			SmsSettings smsSettings = SmsSettings.getInstance(context);

			if (account == null) {
				smsSettings.sendMessage("Someone has looked at your profile!", profile.getAttributeValue("Mobile "));
			} else {
				smsSettings.sendMessage(account.getDisplayName() + " has looked at your profile!", profile.getAttributeValue("Mobile "));
			}

		}
	}
}
