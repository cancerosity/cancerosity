package org.sevensoft.ecreator.model.interaction.views.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.interaction.views.ViewsHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 03-Apr-2006 11:41:59
 *
 */
public class ViewsMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.MemberViewers.enabled(context)) {
			return null;
		}

		return super.link(context, params, new Link(ViewsHandler.class), "account_link");

	}

	public Object getRegex() {
		return new String[] { "views_page", "interaction_views", "account_viewers" };
	}
}