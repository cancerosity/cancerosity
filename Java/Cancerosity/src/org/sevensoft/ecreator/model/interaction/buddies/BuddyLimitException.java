package org.sevensoft.ecreator.model.interaction.buddies;


/**
 * @author sks 11-Oct-2005 18:44:21
 *
 */
public class BuddyLimitException extends Exception {

	public BuddyLimitException() {
		super();
	}

	public BuddyLimitException(String message) {
		super(message);
	}

	public BuddyLimitException(String message, Throwable cause) {
		super(message, cause);
	}

	public BuddyLimitException(Throwable cause) {
		super(cause);
	}

}
