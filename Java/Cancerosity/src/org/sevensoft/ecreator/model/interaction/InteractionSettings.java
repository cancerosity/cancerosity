package org.sevensoft.ecreator.model.interaction;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 29 Dec 2006 14:34:41
 *
 */
@Table("interactions_settings")
@Singleton
public class InteractionSettings extends EntityObject {

	public static InteractionSettings getInstance(RequestContext context) {
		return EntityObject.getSingleton(context, InteractionSettings.class);
	}

	protected InteractionSettings(RequestContext context) {
		super(context);
	}

}
