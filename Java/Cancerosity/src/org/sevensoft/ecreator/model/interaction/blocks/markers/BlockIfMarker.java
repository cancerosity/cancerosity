package org.sevensoft.ecreator.model.interaction.blocks.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 21 Dec 2006 07:28:49
 *
 */
public class BlockIfMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item profile, String location, int x, int y) {

		if (!Module.MemberBlocking.enabled(context)) {
			return null;
		}

		String text = params.get("text");
		if (text == null)
			text = "You are blocking this member";

		return super.string(context, params, text);
	}

	@Override
	public String getDescription() {
		return "Outputs your message if you are blocking this member";
	}

	public Object getRegex() {
		return new String[] { "blocks_if", "interaction_blocks_yes" };
	}

}
