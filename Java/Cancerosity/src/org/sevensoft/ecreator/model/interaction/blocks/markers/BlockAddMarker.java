package org.sevensoft.ecreator.model.interaction.blocks.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.interaction.blocks.BlockedMembersHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 3 Dec 2006 18:13:42
 * 
 * Use this tag to make a link to block a member
 *
 */
public class BlockAddMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item profile, String location, int x, int y) {

		if (!Module.MemberBlocking.enabled(context))
			return null;

		Item account = (Item) context.getAttribute("account");
		if (account == null) {
			return null;
		}

		if (profile.equals(account)) {
			return null;
		}

		if (!params.containsKey("id")) {
			params.put("id", "interaction_block");
		}

		return super.link(context, params, new Link(BlockedMembersHandler.class, "block", "profile", profile), "interaction");
	}

	public Object getRegex() {
		return new String[] { "blocks_add", "interaction_block" };
	}

}
