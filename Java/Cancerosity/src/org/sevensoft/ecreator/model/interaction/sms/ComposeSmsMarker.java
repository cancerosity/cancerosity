package org.sevensoft.ecreator.model.interaction.sms;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.interaction.sms.SmsHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class ComposeSmsMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item profile, String location, int x, int y) {

		/*
		 * Make sure we are not trying to send a message to ourselves
		 */
		Item account = (Item) context.getAttribute("account");
		if (profile.equals(account)) {
			return null;
		}

		/*
		 * Make sure we're not blocked
		 */
		if (account != null) {
			if (profile.getInteraction().isBlocking(account)) {
				logger.fine("[ComposeSmsMarker] this profile is blocking the logged in member");
				return null;
			}
		}

		/* 
		 * Does this profile have suffient sms credits ?
		 */
		if (!profile.hasSmsCredits()) {
			logger.fine("[ComposeSmsMarker] no sms credits!");
			return null;
		}

		if (!params.containsKey("text") && !params.containsKey("label")) {
			params.put("text", "Send sms message");
		}

		if (!params.containsKey("id")) {
			params.put("id", "interaction_sms");
		}

		return super.link(context, params, new Link(SmsHandler.class, "compose", "profile", profile), "interaction");
	}

	@Override
	public String getDescription() {
		return "Makes a link to the sms message composition page";
	}

	public Object getRegex() {
		return new String[] { "sms_compose" };
	}
}
