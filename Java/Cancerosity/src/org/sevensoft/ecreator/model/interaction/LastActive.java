package org.sevensoft.ecreator.model.interaction;

import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 18 Sep 2006 20:54:54
 *
 */
public enum LastActive {

	OnlineNow("Online now"),

	Today("Today"),

	Within24Hours("Within 24 hours"),

	Within3Days("Within 3 days"),

	Week("Within a week"),

	Fortnight("Within a fortnight"),

	Month("Within a month"),

	OverMonth("Over a month ago");

	public static long getOnlineNowTimeout(RequestContext context) {

		if (Module.UserplaneMessenger.enabled(context)) {

			return 1000l * 30;

		} else {

			return 1000l * 60 * 5;
		}
	}

	private String	toString;

	LastActive(String toString) {
		this.toString = toString;
	}

	@Override
	public String toString() {
		return toString;
	}
}