package org.sevensoft.ecreator.model.interaction.winks.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.interaction.winks.WinksHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Captions;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 20 Apr 2006 17:32:32
 * 
 * Makes a link to do a wink
 *
 */
public class WinkMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item profile, String location, int x, int y) {

		if (!Module.Nudges.enabled(context)) {
			return null;
		}

		/*
		 * Make sure we are not viewing ourselves !
		 */
		if (profile.equals(context.getAttribute("account"))) {
			return null;
		}

		if (!params.containsKey("text")) {
			params.put("text", "Send " + Captions.getInstance(context).getWinksCaption().toLowerCase());
		}

		if (!params.containsKey("class")) {
			params.put("class", "interaction");
		}

		if (!params.containsKey("id")) {
			params.put("id", "interaction_nudge");
		}

		return super.link(context, params, new Link(WinksHandler.class, "wink", "recipient", profile), null);
	}

	public Object getRegex() {
		return new String[] { "wink", "interaction_nudges", "interaction_wink" };
	}

}
