package org.sevensoft.ecreator.model.reminders.blocks;


import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.validators.DateValidator;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.net.EmailValidator;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.reminders.HolidayReminder;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

import javax.servlet.ServletException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author dme 20-Aug-2007 10:41:09
 */
@Path("holiday-reminders.do")
public class HolidayReminderBlockHandler extends FrontendHandler {

    private String holidayDate;
    private String email;
    private Category category;

    public HolidayReminderBlockHandler(RequestContext context) {
        super(context);
    }

    public Object main() throws ServletException {
        FrontendDoc doc = new FrontendDoc(context, "Holiday reminder");
        doc.addTrail(HolidayReminderBlockHandler.class, "Holiday reminder");
        doc.addBody(new Body() {
            @Override
            public String toString() {
                return (new HolidayReminderBlock(context)).render(context);
            }
        });
        return doc;
    }

    private void validateInput() {
        test(new DateValidator(), "holidayDate");
        test(new RequiredValidator(), "holidayDate");
        test(new EmailValidator(), "email");
        test(new RequiredValidator(), "email");
    }

    public Object createReminder() throws ServletException, ParseException {

        validateInput();

        if (hasErrors()) {
            logger.fine("[HolidayReminderBlockHandler] errors=" + context.getErrors());
            if (category == null) {
                return main();
            } else {
                return new CategoryHandler(context, category).main();
            }
        }

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date holidayStartDate = dateFormat.parse(holidayDate);
        HolidayReminder holidayReminder = new HolidayReminder(context);
        holidayReminder.setEmail(email);
        holidayReminder.setHolidayDate(new DateTime(holidayStartDate.getTime()));
        holidayReminder.save();

        FrontendDoc doc = new FrontendDoc(context, "Holiday reminder");
        doc.addTrail(HolidayReminderBlockHandler.class, "Holiday reminder");
        doc.addBody(new Body() {
            @Override
            public String toString() {
                return "Your holiday reminder was successfully registered in system";
            }
        });
        return doc;
    }

}
