package org.sevensoft.ecreator.model.reminders;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.ToolTip;

/**
 * Created by IntelliJ IDEA.
 * User: MeleshkoDN
 * Date: 06.09.2007
 * Time: 15:17:03
 * To change this template use File | Settings | File Templates.
 */
public class HolidayReminderToolTip extends ToolTip {

    @Override
    public String getHelp() {
        return "Let us know when your holiday starts and we'll send you helpful reminders by e-mail as your holiday approaches";
    }
}
