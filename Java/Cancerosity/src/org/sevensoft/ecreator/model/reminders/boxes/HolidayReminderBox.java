package org.sevensoft.ecreator.model.reminders.boxes;


import org.sevensoft.ecreator.iface.admin.reminders.HolidayReminderBoxHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.EditHandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.reminders.HolidayReminderToolTip;
import org.sevensoft.ecreator.model.reminders.blocks.HolidayReminderBlockHandler;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author dme 06-Sep-2007 10:41:09
 */
@Table("boxes_holiday_reminders")
@Label("Holiday Reminder")
@HandlerClass(HolidayReminderBlockHandler.class)
@EditHandlerClass(HolidayReminderBoxHandler.class)
public class HolidayReminderBox extends Box {


    public HolidayReminderBox(RequestContext context) {
        super(context);
    }

    public HolidayReminderBox(RequestContext context, String location) {
        super(context, location);
    }

    @Override
    protected String getCssIdDefault() {
        return "holiday_reminder";
    }

    @Override
    public boolean hasEditableContent() {
        return false;
    }

    public String render(RequestContext context) {

        if (!Module.HolidayReminders.enabled(context))
            return null;

        TextTag dateTag = new TextTag(context, "holidayDate", null, 10);
        dateTag.setClass("date-pick");
        dateTag.setId("date");
        TextTag emailTag = new TextTag(context, "email", null, 12);
        emailTag.setClass("email");

        Category category = (Category) context.getAttribute("category");

        StringBuilder sb = new StringBuilder();
        sb.append(new FormTag(HolidayReminderBlockHandler.class, "createReminder", "POST"));
        sb.append(new HiddenTag("category", category));
        sb.append(getTableTag());
        sb.append("<tr><td colspan='2'><div class='info'>" + new HolidayReminderToolTip().getHelp() + "</div></td></tr>");
        sb.append("<tr><td>Start Date:</td><td>" + dateTag + "<br/>" + new ErrorTag(context, "holidayDate", "<br/>") + " </td></tr>");
        sb.append("<tr><td>E-mail:</td><td>" + emailTag + " " + new ErrorTag(context, "email", "<br/>") + " </td></tr>");
        sb.append("<tr><td>&nbsp;</td><td>" + new SubmitTag("Remind me") + " </td></tr>");
        sb.append("<tr><td colspan='2' class='bottom'>&nbsp;</td></tr>");
        sb.append("</table>");

        sb.append("</form>");
        return sb.toString();
    }

}