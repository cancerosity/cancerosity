package org.sevensoft.ecreator.model.reminders;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.List;

/**
 * @author dme 27-Aug-2007 10:41:09
 */
@Table("holiday_reminder_periods")
public class HolidayReminderPeriod extends EntityObject {

    public HolidayReminderPeriod(RequestContext context) {
        super(context);
    }

    private int timing;

    private String message;

    private boolean active;

    private String displayName;

    public int getTiming() {
        return timing;
    }

    public void setTiming(int timing) {
        this.timing = timing;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public static List<HolidayReminderPeriod> getActivePeriods(RequestContext context) {
        Query q = new Query(context, "SELECT * FROM # WHERE active=1 ORDER BY timing ASC");
        q.setTable(HolidayReminderPeriod.class);
        return q.execute(HolidayReminderPeriod.class);
    }

    public static List<HolidayReminderPeriod> getAllPeriods(RequestContext context) {
        Query q = new Query(context, "SELECT * FROM # ORDER BY timing ASC");
        q.setTable(HolidayReminderPeriod.class);
        return q.execute(HolidayReminderPeriod.class);
    }
}
