package org.sevensoft.ecreator.model.reminders.services;

import org.apache.commons.lang.time.DateUtils;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.ecreator.model.reminders.HolidayReminder;
import org.sevensoft.ecreator.model.reminders.HolidayReminderPeriod;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;


/**
 * User: MeleshkoDN
 * Date: 06.09.2007
 * Time: 18:07:47
 */
public class HolidayReminderServiceRunner implements Runnable {

    private static final Logger logger = Logger.getLogger("cron");
    private RequestContext context;

    public HolidayReminderServiceRunner(RequestContext context) {
        this.context = context;
    }


    public void run() {
        System.out.println(HolidayReminderPeriod.class);
        logger.fine("[Holiday Reminder Service] running");
        List<HolidayReminderPeriod> activePeriods = HolidayReminderPeriod.getActivePeriods(context);
        for (HolidayReminderPeriod period : activePeriods) {
            Query q = new Query(context, "SELECT * FROM # WHERE holidayDate=?");
            q.setTable(HolidayReminder.class);
            Date now = new Date();
            int timing = period.getTiming();
            q.setParameter(DateUtils.truncate(now, Calendar.DATE).getTime() + timing * DateUtils.MILLIS_IN_DAY);
            List<HolidayReminder> reminders = q.execute(HolidayReminder.class);
            String message = period.getMessage();
            String term = period.getDisplayName();
            for (HolidayReminder holidayReminder : reminders) {
                String emailAddress = holidayReminder.getEmail();
                Email reminderEmail = Email.composeEmail(context, "Holiday reminder", emailAddress, message);
                if (Email.sendEmail(context, reminderEmail)) {
                    logger.fine("[Holiday Reminder Service] reminder was sent to " + emailAddress + " prior" + term + " prior to the holiday start date");
                }
            }
            logger.fine("[Holiday Reminder Service] successfully finished");

        }
    }
}
