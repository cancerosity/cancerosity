package org.sevensoft.ecreator.model.reminders.blocks;


import org.sevensoft.ecreator.iface.admin.reminders.HolidayReminderEditBlockHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.feeds.annotations.EditHandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.reminders.HolidayReminderToolTip;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author dme 20-Aug-2007 10:41:09
 */
@Table("blocks_holiday_reminders")
@Label("Holiday Reminder")
@HandlerClass(HolidayReminderBlockHandler.class)
@EditHandlerClass(HolidayReminderEditBlockHandler.class)
public class HolidayReminderBlock extends Block {


    public HolidayReminderBlock(RequestContext context) {
        super(context);
    }

    public HolidayReminderBlock(RequestContext context, BlockOwner owner, int objId) {
        super(context, owner, objId);
    }


    public String render(RequestContext context) {

        TextTag dateTag = new TextTag(context, "holidayDate", null, 10);
        dateTag.setClass("date-pick");
        dateTag.setId("date");
        TextTag emailTag = new TextTag(context, "email", null, 12);
        emailTag.setClass("email");

        Category category = (Category) context.getAttribute("category");

        StringBuilder sb = new StringBuilder();
        sb.append(new FormTag(HolidayReminderBlockHandler.class, "createReminder", "POST"));
        sb.append(new HiddenTag("category", category));
        TableTag table = new TableTag("form");
        table.setCaption("Holiday Reminder");
        sb.append(table);
        sb.append("<tr><td colspan='2'><div class='info'>" + new HolidayReminderToolTip().getHelp() + "</div></td></tr>");
        sb.append("<tr><td>Start Date:</td><td>" + dateTag + "<br/>" + new ErrorTag(context, "holidayDate", "<br/>") + " </td></tr>");
        sb.append("<tr><td>E-mail:</td><td>" + emailTag + " " + new ErrorTag(context, "email", "<br/>") + " </td></tr>");
        sb.append("<tr><td>" + new SubmitTag("Remind me") + " </td><td>&nbsp;</td></tr>");
        sb.append("<tr><td colspan='2' class='bottom'>&nbsp;</td></tr>");
        sb.append("</table>");

        sb.append("</form>");

        return sb.toString();
    }

}