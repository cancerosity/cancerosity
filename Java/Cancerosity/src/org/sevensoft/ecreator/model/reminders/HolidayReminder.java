package org.sevensoft.ecreator.model.reminders;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

@Table("holiday_reminders")
public class HolidayReminder extends EntityObject {


    public HolidayReminder(RequestContext context) {
        super(context);
    }

    private String email;

    private DateTime holidayDate;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public DateTime getHolidayDate() {
        return holidayDate;
    }

    public void setHolidayDate(DateTime holidayDate) {
        this.holidayDate = holidayDate;
    }
}
