package org.sevensoft.ecreator.model.attributes;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.ImageOwner;
import org.sevensoft.ecreator.model.media.images.ImageSupport;
import org.sevensoft.ecreator.model.extras.reorder.ReorderOwner;
import org.sevensoft.ecreator.model.extras.select.SelectableOwner;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * 
 * A choosable value for this attribute.
 * 
 * @author sks 18-Sep-2005 21:01:35
 * 
 */
@Table("attributes_options")
public class AttributeOption extends ImageSupport implements Positionable, ImageOwner, ReorderOwner, SelectableOwner {

    /**
	 * Reveal these ids when this option is choosen
	 */
	private SortedSet<Integer>	revealIds;

	/**
	 * Numerical value used to score items by what people pick in attributes
	 */
	private int				score;

	/*
	 * Owner attribute
	 */
	private Attribute			attribute;

	private int				position;

	/**
	 * Default option for seach boxes
	 */
	private boolean			defaultForSearch;

	/**
	 * Default option when creating
	 */
	private boolean			defaultForNew;

	/*
	 *Text value of this attribute
	 */
	private String			value;

	/**
	 * Location value
	 */
	private String			location;

	/*
	 * Shows the value of this attribute, in addition to images if they are added.
	 */
	private boolean			showValue;

    private String rnid;

	public AttributeOption(RequestContext context) {
		super(context);
	}

	public AttributeOption(RequestContext context, Attribute attribute, String value) {

		super(context);

		value = value.trim();

		if (SimpleQuery.count(context, AttributeOption.class, "attribute", attribute, "value", value) == 0) {

			this.attribute = attribute;
			this.value = value;

			save();
		}
	}

	/**
	 * @return 
	 * @throws CloneNotSupportedException 
	 * 
	 */
	public AttributeOption copyTo(Attribute newAttribute) throws CloneNotSupportedException {

		AttributeOption copy = (AttributeOption) super.clone();
		copy.attribute = newAttribute;
		copy.save();

		try {
			copyImagesTo(copy);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ImageLimitException e) {
			e.printStackTrace();
		}

		return copy;
	}

	public String getAlt() {
		return value;
	}

	public final Attribute getAttribute() {
		return attribute.pop();
	}

	public int getImageLimit() {
		return 1;
	}

	public String getLocation() {
		return location;
	}

	public int getPosition() {
		return position;
	}

	public Set<Integer> getRevealIds() {
		if (revealIds == null)
			return Collections.emptySet();
		return revealIds;
	}

	public final int getScore() {
		return score;
	}

	public String getValue() {
		return value;
	}

    public String getRnid() {
        return rnid;
    }

    public boolean hasRevealIds() {
		return getRevealIds().size() > 0;
	}

	public boolean isDefaultForNew() {
		return defaultForNew;
	}

	public boolean isDefaultForSearch() {
		return defaultForSearch;
	}

	public final boolean isShowValue() {
		return hasApprovedImages() ? showValue : true;
	}

	public void setDefaultForNew(boolean defaultForNew) {
		this.defaultForNew = defaultForNew;
	}

	public void setDefaultForSearch(boolean defaultForSearch) {
		this.defaultForSearch = defaultForSearch;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setPosition(int position) {
		this.position = position;
	}

    public String getReorderName() {
        return getValue();
    }

    public String getSelectableName() {
        return getValue();
    }

    public void setRevealIds(Collection<Integer> c) {
		this.revealIds = new TreeSet<Integer>();
		this.revealIds.addAll(c);
	}

	public final void setScore(int score) {
		this.score = score;
	}

	public final void setShowValue(boolean showValue) {
		this.showValue = showValue;
	}

	public void setValue(String s) {

		s = s.trim();

		// update all existing values
		Query q = new Query(context, "update # set value=? where value=? and attribute=?");
		q.setTable(AttributeValue.class);
		q.setParameter(s);
		q.setParameter(value);
		q.setParameter(attribute);
		q.run();

		value = s;
		save();
	}

    public void setRnid(String rnid) {
        this.rnid = rnid;
    }

    public boolean useImageDescriptions() {
		return false;
	}

}
