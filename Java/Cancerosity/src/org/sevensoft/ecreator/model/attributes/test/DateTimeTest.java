package org.sevensoft.ecreator.model.attributes.test;

import java.text.ParseException;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.attributes.Attribute;

/**
 * @author sks 28 Oct 2006 22:21:26
 *
 */
public class DateTimeTest extends AttributeTest {

	@Override
	public String test(Attribute attribute, String value) {

		logger.fine("[DateTimeTest] testing value=" + value + " type=" + this);

		try {

			new DateTime(value);
			return null;

		} catch (ParseException e) {

			return "Please enter date in the format dd/mm/yy";
		}

	}

}
