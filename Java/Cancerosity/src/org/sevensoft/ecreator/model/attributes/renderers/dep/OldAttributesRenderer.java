package org.sevensoft.ecreator.model.attributes.renderers.dep;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.iface.admin.items.EventDatesUtils;
import org.sevensoft.ecreator.iface.admin.items.RecurrenceOption;
import org.sevensoft.ecreator.model.attributes.Attributable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.values.AValueRenderer;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.table.TableTag;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Renders a load of attribute values in a table or spans.
 *
 * @author sks 26 Apr 2006 16:38:15
 */
public class OldAttributesRenderer {

    private final boolean table;
    private final MultiValueMap<Attribute, String> attributeValues;
    private final RequestContext context;
    private final boolean summary;
    private final Collection<Integer> excludes;
    private String replace;
    private String with;
    private Attributable a;

    @Deprecated
    public OldAttributesRenderer(RequestContext context, Attributable a, MultiValueMap<Attribute, String> attributeValues, boolean table, boolean summary,
                                 boolean stripEmails, boolean permissions, Collection<Integer> excludes) {

        this.context = context;
        this.a = a;
        this.attributeValues = attributeValues;
        this.table = table;
        this.summary = summary;
        this.excludes = excludes;
    }

    public final String getReplace() {
        return replace;
    }

    public final String getWith() {
        return with;
    }

    public final void setReplace(String replace) {
        this.replace = replace;
    }

    public final void setWith(String with) {
        this.with = with;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        if (table) {
            sb.append(new TableTag("attributes"));
        }
        if (a instanceof Item && ((Item) a).isEvent()) {
            Item event = (Item) a;
            processEventAttributes(sb, event, attributeValues);
        }

        for (Map.Entry<Attribute, List<String>> entry : attributeValues.entryListSet()) {

            Attribute attribute = entry.getKey();
            List<String> values = entry.getValue();

            if (!check(attribute)) {
                continue;
            }

            addAttribute(sb, attribute, values);

        }

        if (table) {
            sb.append("</table>");
        }

        return sb.toString();
    }

    private void processEventAttributes(StringBuilder sb, Item event, MultiValueMap<Attribute, String> attributeValues) {
        Attribute startDateAttribute = event.getAttribute("Date start");
        Attribute endDateAttribute = event.getAttribute("Date end");

        Attribute recurrenceAttribute = event.getAttribute("Recurrence");
        Attribute repeatTermAttribute = event.getAttribute("Repeat term");
        Attribute repeatOptionAttribute = event.getAttribute("Repeat option");

        String start = event.getAttributeValue(startDateAttribute);
        String end = event.getAttributeValue(endDateAttribute);
        if (start == null || end == null || recurrenceAttribute == null) {
            return;
        }
        Date startDate = new Date(Long.parseLong(start));
        Date endDate = new Date(Long.parseLong(end));

        String recurrenceAttributeValue = attributeValues.get(recurrenceAttribute);
        if (recurrenceAttributeValue != null && recurrenceAttributeValue.equals("true")) {
            Integer repeatTerm = Integer.parseInt(event.getAttributeValue("Repeat term"));
            String repeatOptionValue = event.getAttributeValue("Repeat option");
            RecurrenceOption repeatOption = RecurrenceOption.valueOf(repeatOptionValue);
            List<Date> eventDates = EventDatesUtils.evaluateEventDates(new DateTime(), startDate, endDate, repeatOption, repeatTerm);
            if (check(startDateAttribute)) {
                addAttribute(sb, "Date start", eventDates.get(0).toDefaultFormat());
            }
            if (check(endDateAttribute)) {
                addAttribute(sb, "Date end", eventDates.get(1).toDefaultFormat());
            }
            if (check(repeatOptionAttribute)) {
                addAttribute(sb, "Repeat interval", repeatTerm + " " + repeatOptionValue.toLowerCase());
            }
        } else {
            if (check(startDateAttribute)) {
                addAttribute(sb, "Date start", startDate.toDefaultFormat());
            }
            if (check(endDateAttribute)) {
                addAttribute(sb, "Date end", endDate.toDefaultFormat());
            }
        }

        attributeValues.remove(startDateAttribute);
        attributeValues.remove(endDateAttribute);
        attributeValues.remove(repeatTermAttribute);
        attributeValues.remove(repeatOptionAttribute);
        attributeValues.remove(recurrenceAttribute);

    }

    private void addAttribute(StringBuilder sb, Attribute attribute, List<String> values) {
        final AValueRenderer displayRenderer = new AValueRenderer(context, a, attribute, values);
        displayRenderer.setReplace(replace);
        displayRenderer.setWith(with);

        if (table) {

            sb.append("<tr class='attribute'>");

            if (attribute.isShowLabel()) {

                sb.append("<td class='attribute-label' valign='top'>" + attribute.getName() + ": </td>");
                sb.append("<td class='attribute-value'>" + displayRenderer + "</td>");

            } else {

                sb.append("<td class='attribute-value' colspan='2' valign='top'>" + displayRenderer + "</td>");
            }

            sb.append("</tr>");

        } else {

            sb.append("<span class='attribute'>");

            if (attribute.isShowLabel())
                sb.append("<span class='attribute-label'>" + attribute.getName() + ": </span>");

            sb.append("<span class='attribute-value'>" + displayRenderer + "</span>");
            sb.append("</span>");

        }
    }

    private void addAttribute(StringBuilder sb, String attributeName, String value) {

        if (table) {
            sb.append("<tr class='attribute'>");
            sb.append("<td class='attribute-label' valign='top'>" + attributeName + ": </td>");
            sb.append("<td class='attribute-value'>" + value + "</td>");
            sb.append("</tr>");

        } else {
            sb.append("<span class='attribute'>");
            sb.append("<span class='attribute-label'>" + attributeName + ": </span>");
            sb.append("<span class='attribute-value'>" + value + "</span>");
            sb.append("</span>");
        }
    }

    private boolean check(Attribute attribute) {
        if (excludes != null && excludes.contains(attribute.getId()))
            return false;

        if (!attribute.isDisplayable()) {
            return false;
        }

        if (summary && !attribute.isSummary()) {
            return false;
        }

        return true;
    }

}
