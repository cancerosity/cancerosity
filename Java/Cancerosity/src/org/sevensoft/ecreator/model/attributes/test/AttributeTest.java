package org.sevensoft.ecreator.model.attributes.test;

import java.util.logging.Logger;

import org.sevensoft.ecreator.model.attributes.Attribute;

/**
 * @author sks 28 Oct 2006 22:20:05
 *
 */
public abstract class AttributeTest {

	protected static final Logger	logger	= Logger.getLogger("ecreator");

	public abstract String test(Attribute attribute, String value);
}
