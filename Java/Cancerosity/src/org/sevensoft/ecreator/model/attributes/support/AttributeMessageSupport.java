package org.sevensoft.ecreator.model.attributes.support;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.crm.messages.MessageOwner;
import org.sevensoft.ecreator.model.crm.messages.MessageUtil;
import org.sevensoft.ecreator.model.crm.messages.Msg;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 28 Oct 2006 23:21:36
 *
 */
public abstract class AttributeMessageSupport extends AttributeSupport implements MessageOwner {

	private int		messageCount;
	private String	lastMessageAuthor;
	private DateTime	lastMessageDate;

	protected AttributeMessageSupport(RequestContext context) {
		super(context);
	}

	public Msg addMessage(String author, String email, String body) {
		return new Msg(context, this, author, email, body);
	}

	public Msg addMessage(User user, String content) {

		Msg message = new Msg(context, this, user, content);
		return message;
	}

	public Set<User> getContributors() {

		Set<User> contributors = new HashSet<User>();
		for (Msg msg : getMessages()) {
			if (msg.hasUser()) {
				contributors.add(msg.getUser());
			}
		}

		return contributors;
	}

	public final Msg getLastMessage() {
		return MessageUtil.getLastMessage(context, this);
	}

	public final String getLastMessageAuthor() {
		return lastMessageAuthor;
	}

	public final DateTime getLastMessageDate() {
		return lastMessageDate;
	}

	public final int getMessageCount() {
		return messageCount;
	}

	public List<String> getMessageEmails() {
		return null;
	}

	public final List<Msg> getMessages() {
		return MessageUtil.getMessages(context, this);
	}

	public abstract String getMessageSubject();

	public final List<Msg> getPublicMessages() {
		return MessageUtil.getPublicMessages(context, this);
	}

	public final boolean hasMessages() {
		return messageCount > 0;
	}

	public final void removeMessages() {
		MessageUtil.removeMessages(context, this);
	}

	public final void setLastMessageAuthor() {
		this.lastMessageAuthor = MessageUtil.getLastMessageAuthor(context, this);
	}

	public final void setLastMessageDate() {
		this.lastMessageDate = MessageUtil.getLastMessageDate(context, this);
	}

	public final void setMessageCount() {
		this.messageCount = getMessages().size();
		save();
	}

}
