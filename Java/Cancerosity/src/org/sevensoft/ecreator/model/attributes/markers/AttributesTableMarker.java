package org.sevensoft.ecreator.model.attributes.markers;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class AttributesTableMarker extends AbstractAttributesMarker {

	public AttributesTableMarker() {
		super(true, false);
	}

	public Object getRegex() {
		return "attributes_table";
	}

}
