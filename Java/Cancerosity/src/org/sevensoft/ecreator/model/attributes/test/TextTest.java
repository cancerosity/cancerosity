package org.sevensoft.ecreator.model.attributes.test;

import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.misc.location.Pin;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 14 Dec 2006 22:27:34
 *
 */
public class TextTest extends AttributeTest {

	private RequestContext	context;

	public TextTest(RequestContext context) {
		this.context = context;
	}

	@Override
	public String test(Attribute attribute, String value) {

		/*
		 * If we have a reg exp then check it matches
		 */
		String regExp = attribute.getRegExp();
		if (regExp != null) {

			logger.fine("trying to match regexp=" + regExp);

			if (!value.matches(regExp)) {
				logger.fine("regexp failure");
				if (attribute.getCustomErrorMessage() == null) {
                    return "Please enter data in the correct format";
                }
                else {
                    return attribute.getCustomErrorMessage();
                }
			}
		}

		/*
		 * for text we must  check that this is a valid location if this is a location field
		 */
		if (attribute.isLocation()) {

			boolean valid = Pin.isValid(context, value);
			logger.fine("[attribute type] location attribute, checking for valid location, " + valid);
			if (!valid)
				return "Unknown location";
		}

		return null;
	}

}
