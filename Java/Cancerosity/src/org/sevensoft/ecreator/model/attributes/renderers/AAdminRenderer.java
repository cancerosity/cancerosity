package org.sevensoft.ecreator.model.attributes.renderers;

import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.admin.attributes.AttributeHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attributable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.ecreator.model.attributes.renderers.input.AInputRenderer;
import org.sevensoft.ecreator.model.attributes.renderers.search.ASelectionRenderer;
import org.sevensoft.ecreator.model.attributes.renderers.values.AValueRenderer;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 3 Nov 2006 20:16:35
 * 
 * This panel will display attributes in admin
 * 
 *
 */
public class AAdminRenderer {

	private static final Logger				logger	= Logger.getLogger("ecreator");

	private final Attributable				a;
	private final RequestContext				context;
	private final ARenderType				type;
	private final MultiValueMap<Attribute, String>	attributeValues;
	private final MultiValueMap<String, Attribute>	sections;
	private final boolean					superman;
	private final MiscSettings				miscSettings;
	private boolean						multi;
    private Item item;

	public AAdminRenderer(RequestContext context, Attributable a, Collection<Attribute> attributes, MultiValueMap<Attribute, String> attributeValues,
			ARenderType type) {

		this.context = context;
		this.a = a;

		if (attributes == null) {
			attributes = a.getAttributes();
		}

		this.attributeValues = attributeValues;
		this.type = type;
		this.sections = AttributeUtil.getSectionsMap(attributes);
		this.miscSettings = MiscSettings.getInstance(context);
		this.superman = context.containsAttribute("superman");

		logger.fine("[AAdminRenderer] attributeValues=" + attributeValues);
	}

    public AAdminRenderer(RequestContext context, Attributable a, Collection<Attribute> attributes, MultiValueMap<Attribute, String> attributeValues,
			ARenderType type, Item item) {
        this(context, a, attributes, attributeValues, type);
        this.item = item;
    }

    public final void setMulti(boolean multi) {
		this.multi = multi;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		for (String section : sections.keySet()) {

			sb.append(new AdminTable(section == null ? "Details" : section));
			for (Attribute attribute : sections.list(section)) {

				// get values for this attribute
				List<String> values = null;
				if (attributeValues != null) {
					values = attributeValues.list(attribute);
				}

				logger.fine("[AAdminRenderer] attribute=" + attribute.getName() + ", values=" + values);

				// get the renderer for the type
				// add in our admin row with link to edit attribute
				StringBuilder sb2 = new StringBuilder();

				switch (type) {

				case Input:
					AInputRenderer ir;
                    if (item != null) {
                        ir = new AInputRenderer(context, attribute, values, item.getId());
                    } else {
                        ir = new AInputRenderer(context, attribute, values);
                    }
					ir.setMulti(multi);
					sb2.append(ir);
					break;

				case Search:
                    ASelectionRenderer sr;
                    if (item != null) {
                        sr = new ASelectionRenderer(context, attribute, values, item.getId());
                    } else {
                        sr = new ASelectionRenderer(context, attribute, values);
                    }
					sb2.append(sr);
					break;

				case Value:
					AValueRenderer dr;
                    if (item != null) {
                        dr = new AValueRenderer(context, a, attribute, values, item.getId());
                    } else {
                        dr = new AValueRenderer(context, a, attribute, values);
                    }
					sb2.append(dr);
					break;
				}

                if (type == ARenderType.Input) {
                    sb2.append(new ErrorTag(context, attribute.getParamName(item), "<br/>"));
                }

				String edit;
				if (miscSettings.isAdvancedMode() || superman) {

					edit = new LinkTag(AttributeHandler.class, "edit", new ImageTag("files/graphics/admin/spanner.gif").setAlign("absmiddle"),
							"attribute", attribute)
							+ " ";
				} else {

					edit = "";
				}

				sb.append(new AdminRow(edit + attribute.getName(), attribute.getDescription(), sb2));
			}

			sb.append("</table>");
		}

		return sb.toString();
	}
}
