package org.sevensoft.ecreator.model.attributes.renderers.values;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.model.attributes.Attributable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 12 Mar 2007 19:09:26
 * 
 * Render attributes for confirmation
 *
 */
public class AFormSectionsValuePanel extends Panel {

	private final MultiValueMap<Attribute, String>	values;
	private final Attributable				a;
	private List<Attribute>					attributes;
	private List<String>					sections;

	public AFormSectionsValuePanel(RequestContext context, Attributable a) {
		super(context);

		this.a = a;
		this.values = a.getAttributeValues();

		this.attributes = new ArrayList(values.keySet());
		this.attributes = AttributeUtil.retainSectionAttributes(attributes);

		this.sections = AttributeUtil.getSections(attributes);
	}

	@Override
	public void makeString() {

		for (String section : sections) {

			// get attributes for this section
			List<Attribute> sectionAttributes = AttributeUtil.retainSectionAttributes(attributes, section);

			sb.append(new FormFieldSet(section));

			AFormValuePanel panel = new AFormValuePanel(context, a, sectionAttributes);
			sb.append(panel);

			sb.append("</fieldset>");
		}

	}
}
