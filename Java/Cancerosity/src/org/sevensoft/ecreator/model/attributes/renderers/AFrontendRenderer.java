package org.sevensoft.ecreator.model.attributes.renderers;

import java.util.Collection;
import java.util.List;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.ecreator.model.attributes.renderers.values.AValueRenderer;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 12 Sep 2006 12:22:06
 * 
 * Displays many attributes in a table
 *
 */
@Deprecated
public class AFrontendRenderer {

	private final Collection<Attribute>			attributes;
	private final RequestContext				context;
	private final MultiValueMap<String, Attribute>	sectionsMap;
	private final MultiValueMap<Attribute, String>	attributeValues;
	private boolean						script;
	private int							labelwidth;

	public AFrontendRenderer(RequestContext context, Collection<Attribute> attributes, MultiValueMap<Attribute, String> attributeValues) {

		this.context = context;
		this.attributes = attributes;
		this.attributeValues = attributeValues;

		this.labelwidth = 180;

		// build sections map
		this.sectionsMap = AttributeUtil.getSectionsMap(attributes);
	}

	private String getSectionName(String section) {
		return section.toLowerCase().replaceAll("[^a-zA-Z0-9]", "_");
	}

	public void setLabelWidth(int labelwidth) {
		this.labelwidth = labelwidth;
	}

	public void setScript(boolean script) {
		this.script = script;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		if (script) {

			sb.append("<script> \n ");
			sb.append("function hideSections() { \n");

			for (String section : sectionsMap.keySet()) {
				sb.append("	var e = document.getElementById('section_" + getSectionName(section) + "'); e.style.display = 'none'; \n");
			}

			sb.append(" } \n\n");
			sb.append(" function showSection() { \n");
			sb.append("	hideSections(); \n");
			sb.append("	for (i=0; i<showSection.arguments.length;i++) {\n");
			sb.append("		var e = document.getElementById(showSection.arguments[i]); if (e != null) e.style.display = ''; \n");
			sb.append("	}\n");
			sb.append("} \n");

			sb.append("\n</script>\n\n");

		}

		for (String section : sectionsMap.keySet()) {

			final TableTag tableTag = new TableTag("form");
			tableTag.setCaption(section);
			tableTag.setWidth("100%");

			if (section != null) {
				tableTag.setId("section_" + getSectionName(section));
			} else {
				tableTag.setId("attributes");
			}

			sb.append(tableTag);

			for (Attribute attribute : sectionsMap.list(section)) {

				List<String> values = attributeValues.list(attribute);

				sb.append("<tr><td class='attribute-label' valign='top' width='" + labelwidth + "'>");
				sb.append(attribute.getName());
				sb.append("</td><td class='attribute-value'>");
				sb.append(new AValueRenderer(context, null, attribute, values));
				sb.append("</td></tr>");

			}

			sb.append("</table>");

		}

		return sb.toString();
	}
}
