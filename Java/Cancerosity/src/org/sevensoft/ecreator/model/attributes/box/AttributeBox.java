package org.sevensoft.ecreator.model.attributes.box;

import java.util.List;

import org.sevensoft.ecreator.iface.admin.attributes.boxes.AttributeBoxHandler;
import org.sevensoft.ecreator.iface.frontend.search.ItemSearchHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.http.util.Link;

/**
 * Attribute quick search box
 * 
 * @author sks 08-Feb-2006 17:26:55
 *
 */
@Table("boxes_attribute")
@Label("Attribute search")
@HandlerClass(AttributeBoxHandler.class)
public class AttributeBox extends Box {

	/**
	 * The attribute we are searching on
	 */
	private Attribute	attribute;

	/**
	 * If list then render as static links, otherwise use select box
	 */
	private boolean	list;

	public AttributeBox(RequestContext context) {
		super(context);
	}

	public AttributeBox(RequestContext context, String location) {
		this(context, location, null);
	}

	public AttributeBox(RequestContext context, String location, Attribute attribute) {
		super(context, location);
		this.attribute = attribute;
		if (attribute != null)
			this.name = "Search by " + attribute.getName();

		save();
	}

	public Attribute getAttribute() {
		return (Attribute) (attribute == null ? null : attribute.pop());
	}

	@Override
	protected String getCssIdDefault() {
		return "attribute";
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	public boolean isList() {
		return list;
	}

	public String render(RequestContext context) {

		if (attribute == null) {
			return null;
		}

		attribute.pop();

		StringBuilder sb = new StringBuilder();
		sb.append(getTableTag());

		/*
		 * If selection then show all selections, if not then show the first 10 used values.
		 */
		List<String> values;
		if (attribute.isSelection()) {
			values = attribute.getOptionValues();
		} else {
			values = attribute.getCurrentValues(50);
		}

		/*
		 * If list is set then we should show text links
		 */
		sb.append("<tr><td class='values'>");
		if (list) {

			for (String value : values) {

				/*
				 * Make our link. Check for member attribute or item attribute
				 */
				LinkTag linkTag = null;
				if (attribute.hasItemType()) {

					linkTag = new LinkTag(attribute.getUrl(value), value);

				}

				if (linkTag != null)
					sb.append("<div class='value'>" + linkTag + "</div>");
			}

		} else {

			Link link = new Link(ItemSearchHandler.class);
			if (attribute.hasItemType()) {
				link.setParameter("itemType", attribute.getItemType());
			}

			SelectTag tag = new SelectTag(context, "area");
			tag.setOnChange("window.location='" + link + "?" + attribute.getParamName() + "=' + this.value;");
			tag.setAny("-" + attribute.getName() + "-");
			tag.addOptions(values);
			sb.append(tag);

		}
		sb.append("</td></tr>");

		sb.append("<tr><td class='bottom'></td></tr>");
		sb.append("</table>");

		return sb.toString();
	}

	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

	public void setList(boolean list) {
		this.list = list;
	}
}
