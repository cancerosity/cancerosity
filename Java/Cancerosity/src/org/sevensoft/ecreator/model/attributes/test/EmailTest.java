package org.sevensoft.ecreator.model.attributes.test;

import org.sevensoft.commons.validators.net.EmailValidator;
import org.sevensoft.ecreator.model.attributes.Attribute;

/**
 * @author sks 28 Oct 2006 22:22:21
 *
 */
public class EmailTest extends AttributeTest {

	@Override
	public String test(Attribute attribute, String value) {
		return new EmailValidator().validate(value);
	}

}
