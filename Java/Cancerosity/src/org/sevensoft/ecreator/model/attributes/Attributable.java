package org.sevensoft.ecreator.model.attributes;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.sevensoft.commons.collections.maps.MultiValueMap;

/**
 * This interface is for objects that can be assigned attribute values
 * 
 * Some objects are attribute value owners
 * some objects are attribute search value owners
 * 
 * @author sks 07-Oct-2005 01:26:51
 * 
 */
public interface Attributable {

	/**
	 * Adds a new attribute value to this object for this attribute 
	 */
	public void addAttributeValue(Attribute attribute, String value);

	/**
	 * Adds these new attribute values for this attribute to any existing ones
	 */
	public void addAttributeValues(Attribute attribute, List<String> values);

	/**
	 * Adds all the values for each attribute to this attributable instance
	 */
	public void addAttributeValues(Map<Attribute, String> map);

	/**
	 * 
	 */
	public void copyAttributeValuesTo(Attributable target);

	/**
	 * Returns a list of all attributes applicable to this object.
	 *
	 */
	public List<Attribute> getAttributes();

	/**
	 * Returns a list of attributes valid for this page only
	 */
	public List<Attribute> getAttributes(int page);

	/**
	 * Returns the first value set for this attribute for this attributable object
	 * 
	 */
	public String getAttributeValue(Attribute attribute);

	/**
	 * Returns a map of all attributes mapped to values. 
	 * Only includes attributes that have a value set
	 * 
	 */
	public MultiValueMap<Attribute, String> getAttributeValues();

	/**
	 * Returns a list of all values as strings for this attribute.
	 * 
	 */
	public List<String> getAttributeValues(Attribute attribute);

	/**
	 * Returns true if this object has at least one value for the attribute parameter.
	 * 
	 */
	public boolean hasAttributeValue(Attribute attribute);

	/**
	 * Returns true if this attribute value is set on this attributable for this attribute
	 */
	public boolean hasAttributeValue(Attribute attribute, String value);

	/**
	 * 
	 */
	public void moveAttributeValuesTo(Attributable target);

	/**
	 * Remove all attributes across the entire obj
	 */
	public void removeAttributeValues();

	/**
	 * Remove all attribute values for this attribute.
	 * 
	 */
	public void removeAttributeValues(Attribute attribute);

	/**
	 * Removes existing attribute values for this attribute and sets the new one in <param>value</param>
	 */
	public void setAttributeValue(Attribute attribute, String value);

	/**
	 * Removes existing attribute values for this attribute and replaces them with all the values in values.
	 */
	public void setAttributeValues(Attribute attribute, List<String> values);

	/**
	 * Set these attribute values for this subset of attributes
	 */
	public void setAttributeValues(Collection<Attribute> attributes, Map<Attribute, String> attributeValues);

	/**
	 * Removes all attriute values for this object and replaces them with the ones in this map
	 * 
	 */
	public void setAttributeValues(Map<Attribute, String> map);

    /**
     * Method returns an attribute which is used for iBex integration to define RNID
     */
    public Attribute getIbexAttribute();

}
