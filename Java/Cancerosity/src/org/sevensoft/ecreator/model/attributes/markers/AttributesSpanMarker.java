package org.sevensoft.ecreator.model.attributes.markers;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class AttributesSpanMarker extends AbstractAttributesMarker {

	public AttributesSpanMarker() {
		super(false, false);
	}

	public Object getRegex() {
		return "attributes_span";
	}

}
