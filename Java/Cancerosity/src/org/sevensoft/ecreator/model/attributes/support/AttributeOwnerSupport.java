package org.sevensoft.ecreator.model.attributes.support;

import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeOwner;
import org.sevensoft.ecreator.model.attributes.AttributeOwnerUtil;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.List;

/**
 * @author sks 7 Oct 2006 15:55:55
 */
public abstract class AttributeOwnerSupport extends EntityObject implements AttributeOwner {

    private int attributeCount;

    public AttributeOwnerSupport(RequestContext context) {
        super(context);
    }

    public final Attribute addAttribute(String name, AttributeType type) {
        return AttributeOwnerUtil.addAttribute(this, name, type, context);
    }

    public final List<Attribute> addAttributes(String newAttributes, String newAttributesSection, int newAttributesPage) {
        return AttributeOwnerUtil.addAttributes(this, newAttributes, newAttributesSection, newAttributesPage);
    }

    public final Attribute getAttribute(String name) {
        return AttributeOwnerUtil.getAttribute(this, name);
    }

    public final Attribute getAttribute(String name, AttributeType type) {

        Attribute a = getAttribute(name);
        if (a != null) {
            return a;
        }

        if (type != null) {
            return addAttribute(name, type);
        }

        return null;
    }

    public int getAttributeCount() {
        return attributeCount;
    }

    public List<Attribute> getAttributes() {
        return AttributeOwnerUtil.getAttributes(context, this);
    }

    public final boolean hasAttributes() {
        return attributeCount > 0;
    }

    public final void removeAttribute(Attribute attribute) {
        attribute.delete();
        setAttributeCount();
    }

    public final void removeAttributes() {
        for (Attribute a : getAttributes()) {
            a.delete();
        }

        setAttributeCount();
    }

    public final void setAttributeCount() {
        attributeCount = getAttributes().size();
        save();
    }
}
