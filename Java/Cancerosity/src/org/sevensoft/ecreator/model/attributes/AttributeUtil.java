package org.sevensoft.ecreator.model.attributes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.misc.location.LocationUtil;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 01-Nov-2005 09:47:20
 * 
 */
public class AttributeUtil {

	private static Logger	logger	= Logger.getLogger("ecreator");

	/**
	 * Format all values of this attribute as postcodes with spaces 
	 * @param context 
	 */
	public static void formatPostcodes(Attribute attribute, RequestContext context) {

		for (AttributeValue av : SimpleQuery.execute(context, AttributeValue.class, "attribute", attribute)) {

			String value = LocationUtil.getPostcodeFormatted(av.getValue());
			av.setValue(value);
			av.save();
		}
	}

	public static List<Attribute> getAttributes(Attributable a, final int page) {
		return getAttributes(a.getAttributes(), page);
	}

	public static List<Attribute> getAttributes(List<Attribute> attributes, final int page) {

		if (page < 1) {
			return attributes;
		}

		if (getPagesCount(attributes) == 1) {
			return attributes;
		}

		// sort attributes by section and page
		Collections.sort(attributes, new Comparator<Attribute>() {

			public int compare(Attribute o1, Attribute o2) {

				String section1 = o1.getSection();
				String section2 = o2.getSection();

				if (section1 == null) {
					section1 = "";
				}

				if (section2 == null) {
					section2 = "";
				}

				if (section1.equalsIgnoreCase(section2)) {

					int p1 = o1.getPosition();
					int p2 = o2.getPosition();

					if (p1 < p2) {
						return -1;
					}

					if (p1 > p2) {
						return 1;
					}

					return 0;
				}

				return section1.compareTo(section2);
			}
		});

		CollectionsUtil.filter(attributes, new Predicate<Attribute>() {

			public boolean accept(Attribute e) {
				return e.getPage() == page;
			}
		});

		return attributes;
	}

	public static String getAttributeValue(Attributable a, String attributeName, boolean exact) {

		if (attributeName == null) {
			return null;
		}

		attributeName = attributeName.toLowerCase();

		MultiValueMap<Attribute, String> attributeValues = a.getAttributeValues();
		for (Map.Entry<Attribute, String> entry : attributeValues.entrySet()) {

			if (exact) {

				if (entry.getKey().getName().toLowerCase().equals(attributeName)) {
					return entry.getValue();
				}

			} else {

				if (entry.getKey().getName().toLowerCase().contains(attributeName)) {
					return entry.getValue();
				}
			}

		}

		return null;
	}

	//	/**
	//	 * Returns a list of all page names
	//	 */
	//	public static List<String> getPages(Attributable a) {
	//		return getPages(a.getAttributes());
	//	}

	//	public static List<String> getPages(List<Attribute> attributes) {
	//
	//		List<Integer> pages = new ArrayList();
	//		for (Attribute a : attributes) {
	//			if (!pages.contains(a.getPage())) {
	//				pages.add(a.getPage());
	//			}
	//		}
	//		return pages;
	//	}

	public static List<Attribute> getDisplayableAttributes(Attributable a, int page) {

		List<Attribute> attributes = a.getAttributes();
		CollectionsUtil.filter(attributes, new Predicate<Attribute>() {

			public boolean accept(Attribute e) {
				return e.isDisplayable();
			}
		});

		// just keep ones from this page
		return getAttributes(attributes, page);
	}

	/**
	 * 
	 */
	public static List<Attribute> getListableAttributes(ItemType itemType) {

		if (itemType == null) {
			return Collections.emptyList();
		}

		List<Attribute> attributes = itemType.getAttributes();
		CollectionsUtil.filter(attributes, new Predicate<Attribute>() {

			public boolean accept(Attribute e) {
				return e.isListable();
			}
		});
		return attributes;
	}

	/**
	 * 
	 */
	public static List<Attribute> getNonSectionedAttributes(Attributable a) {

		List<Attribute> attributes = new ArrayList(a.getAttributes());
		CollectionsUtil.filter(attributes, new Predicate<Attribute>() {

			public boolean accept(Attribute e) {
				return e.getSection() == null;
			}
		});
		return attributes;
	}

	/**
	 * Returns the number of distinct pages for all attributes on this attributable
	 */
	public static int getPagesCount(Attributable a) {
		return getPagesCount(a.getAttributes());
	}

	/**
	 * Returns the number of distinct pages these attribute use 
	 */
	public static int getPagesCount(List<Attribute> attributes) {

		if (attributes.isEmpty()) {
			return 1;
		}

		Set<Integer> pages = new HashSet();
		for (Attribute a : attributes) {
			pages.add(a.getPage());
		}

		return pages.size();
	}

	public static int getScore(Attributable a) {

		int score = 0;
		for (Map.Entry<Attribute, String> entry : a.getAttributeValues().entrySet()) {

			final Attribute attribute = entry.getKey();
			if (attribute.isSelection()) {

				AttributeOption option = attribute.getOptionFromValue(entry.getValue());
				if (option != null) {
					score = score + option.getScore();
				}
			}

		}
		return score;
	}

	/**
	 * 
	 */
	public static List<Attribute> getSectionedAttributes(Attributable a) {
		return retainSectionAttributes(a.getAttributes());
	}

	public static List<String> getSections(Collection<Attribute> attributes) {

		List<String> sections = new ArrayList();
		for (Attribute attribute : attributes) {
			if (!sections.contains(attribute.getSection())) {
				sections.add(attribute.getSection());
			}
		}

		return sections;
	}

	/**
	 * Returns an ordered multi map of sections -> attributes
	 */
	public static MultiValueMap<String, Attribute> getSectionsMap(Collection<Attribute> attributes) {

		logger.fine("[AttributeUtil] building section map");

		MultiValueMap<String, Attribute> map = new MultiValueMap(new LinkedHashMap());
		for (Attribute attribute : attributes) {
			map.put(attribute.getSection(), attribute);
		}

		return map;
	}

	/**
	 * Returns true if this collection of attributes contains one that is mandatory
	 */
	public static boolean hasMandatory(Collection<Attribute> attributes) {

		for (Attribute attribute : attributes) {
			if (attribute.isMandatory()) {
				return true;
			}
		}

		return false;
	}

	private static boolean isHidden(Attribute attribute, List<Attribute> attributes) {

		for (Attribute a : attributes) {
			if (a.isSelection()) {
				for (AttributeOption option : a.getOptions()) {
					if (option.hasRevealIds()) {
						if (option.getRevealIds().contains(attribute.getId())) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public static void prepopulate(RequestContext context, List<Item> items) {

		String itemsQueryString = StringHelper.repeat("item=?", items.size(), " or ");
		Query q = new Query(context, "select * from # where " + itemsQueryString);
		q.setTable(AttributeValue.class);
		q.setParameters(items);

		for (AttributeValue av : q.execute(AttributeValue.class)) {
			av.getItem().insertAttributeValue(av.getAttribute(), av.getValue());
		}
	}

	/**
	 * Keep only attributes that have a section
	 */
	public static List<Attribute> retainSectionAttributes(List<Attribute> attributes) {

		List<Attribute> copy = new ArrayList(attributes);

		CollectionsUtil.filter(copy, new Predicate<Attribute>() {

			public boolean accept(Attribute e) {
				return e.getSection() != null;
			}
		});

		return copy;
	}

	/**
	 * Retain section attributes that match this section
	 */
	public static List<Attribute> retainSectionAttributes(List<Attribute> attributes, final String section) {

		List<Attribute> copy = new ArrayList(attributes);

		CollectionsUtil.filter(copy, new Predicate<Attribute>() {

			public boolean accept(Attribute e) {
				return ObjectUtil.equal(e.getSection(), section);
			}
		});

		return copy;
	}

	public static void setDefaults(Attributable a, List<Attribute> attributes) {

		for (Attribute attribute : attributes) {
			if (attribute.hasDefaultNew()) {
				a.addAttributeValue(attribute, attribute.getDefaultNew());
			}
			attribute.init(a);
		}
	}

	/**
	 * Peforms validation for the list of attributes.
	 * 
	 * It checks 
	 * that we have a value set if they are mandatory, and also that the value matches 
	 * the reg exp if applicable
	 */
	public static void setErrors(RequestContext context, List<Attribute> attributes, Map<Attribute, String> attributeValues) {

		for (Attribute attribute : attributes) {

			// only test if this attribute is not set to reveal
//			if (!isHidden(attribute, attributes)) {

				String value = attributeValues.get(attribute);

				String error = attribute.test(value);
				if (error != null) {
					context.setError(attribute.getParamName(), error);
				}

//			}
		}
	}
}