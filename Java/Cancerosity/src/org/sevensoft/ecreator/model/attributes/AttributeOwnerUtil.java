package org.sevensoft.ecreator.model.attributes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 19 Aug 2006 09:44:57
 *
 */
public class AttributeOwnerUtil {

	public static Attribute addAttribute(AttributeOwner owner, String name, AttributeType type, RequestContext context) {

		Attribute attribute = new Attribute(context, owner, name, type);
		owner.setAttributeCount();
		((EntityObject) owner).save();
		return attribute;
	}

	public static List<Attribute> addAttributes(AttributeOwner owner, String newAttributes, String newAttributesSection, int newAttributesPage) {

		if (newAttributes == null) {
			return Collections.emptyList();
		}

		List<Attribute> attributes = new ArrayList<Attribute>();

		for (String string : StringHelper.explodeStrings(newAttributes, "\n")) {
			Attribute attribute = owner.addAttribute(string, AttributeType.Text);
			
			attribute.setSection(newAttributesSection);
			attribute.setPage(newAttributesPage);
			attribute.save();
			
			attributes.add(attribute);
		}

		return attributes;
	}

	/**
	 * Returns the first attribute that matches the name, for this owner
	 */
	public static Attribute getAttribute(AttributeOwner owner, String name) {

		for (Attribute attribute : owner.getAttributes()) {
			if (attribute.getName().equalsIgnoreCase(name)) {
				return attribute;
			}
		}
		return null;
	}

	/**
	 * Returns all attributes defined on this owner
	 * 
	 */
	public static List<Attribute> getAttributes(RequestContext context, AttributeOwner owner) {
		return SimpleQuery.execute(context, Attribute.class, owner.getClass().getSimpleName(), owner);
	}

	/**
	 * Deletes this attribute
	 */
	public static void removeAttribute(AttributeOwner owner, Attribute attribute) {

		attribute.delete();
		owner.setAttributeCount();
		((EntityObject) owner).save();
	}
}
