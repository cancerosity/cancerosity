package org.sevensoft.ecreator.model.attributes.renderers.input;

import java.util.List;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.model.attributes.Attributable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Mar 2007 12:15:11
 * 
 * Renders each section of an attribute
 *
 */
public class AFormSectionsInputPanel extends Panel {

	private final List<Attribute>				attributes;
	private final MultiValueMap<Attribute, String>	attributeValues;
	private final Attributable				a;
	private final List<String>				sections;
	private String						defaultSection;

	public AFormSectionsInputPanel(RequestContext context, Attributable a, List<Attribute> attributes) {
		super(context);

		this.a = a;
		this.attributeValues = a.getAttributeValues();
		this.attributes = attributes;
		this.sections = AttributeUtil.getSections(attributes);

	}

	@Override
	public void makeString() {

		for (String section : sections) {

			// get attributes for this section
			List<Attribute> sectionAttributes = AttributeUtil.retainSectionAttributes(attributes, section);

			if (section == null) {
				section = defaultSection;
			}

			sb.append(new FormFieldSet(section).setId(section));

			AFormInputPanel panel = new AFormInputPanel(context, a, sectionAttributes);
			panel.setColumns(sectionAttributes.get(0).getCellsPerRow());

			sb.append(panel);

			sb.append("</fieldset>");
		}
	}

	public final void setDefaultSection(String defaultSection) {
		this.defaultSection = defaultSection;
	}

}
