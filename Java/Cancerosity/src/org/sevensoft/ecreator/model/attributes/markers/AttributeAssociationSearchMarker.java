package org.sevensoft.ecreator.model.attributes.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Dec 2006 17:42:36
 * 
 * Make a link to a search form for items that have this item set as a value on the associated attribute
 *
 */
public class AttributeAssociationSearchMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		return null;
	}

	public Object getRegex() {
		return null;
	}

}
