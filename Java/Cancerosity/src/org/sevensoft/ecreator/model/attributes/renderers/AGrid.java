package org.sevensoft.ecreator.model.attributes.renderers;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.frontend.misc.FieldInputCell;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.model.attributes.Attributable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.input.AInputRenderer;
import org.sevensoft.ecreator.model.attributes.renderers.search.ASelectionRenderer;
import org.sevensoft.ecreator.model.attributes.renderers.values.AValueRenderer;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.util.Grid;

import java.util.List;
import java.util.logging.Logger;

/**
 * @author sks 15 Sep 2006 06:37:12
 *         <p/>
 *         Renders these attributes in a grid
 */
public class AGrid {

    private static final Logger logger = Logger.getLogger("ecreator");

    private final ARenderType type;
    private final RequestContext context;
    private final List<Attribute> attributes;
    private final MultiValueMap<Attribute, String> attributeValues;
    private final Attributable a;
    private int columns;
    private String caption;

    public AGrid(RequestContext context, Attributable a, List<Attribute> attributes, MultiValueMap<Attribute, String> attributeValues, ARenderType type) {

        this.a = a;
        this.type = type;
        this.context = context;
        this.attributes = attributes;
        this.attributeValues = attributeValues;

        this.columns = attributes.get(0).getCellsPerRow();

    }

    public void setCaption(String string) {
        this.caption = string;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        if (caption != null) {
            sb.append(new FormFieldSet(caption));
        }

        Grid grid = new Grid(columns);
        grid.setTableClass("ec_agrid");

        for (Attribute attribute : attributes) {

            String label = null;
            Object control = null;
            String desc;

            if (attribute.isShowLabel()) {

                label = attribute.getName();

                if (type == ARenderType.Input) {
                    if (attribute.isMandatory()) {
                        label = "* " + label;
                    }
                }
            }

            List<String> values = attributeValues.list(attribute);
            if (type == ARenderType.Input) {

                control = new AInputRenderer(context, attribute, values) + ""
                        + new ErrorTag(context, attribute.getParamName(), " ").setMessage(attribute.getErrorMessage());

            } else if (type == ARenderType.Value) {

                control = new AValueRenderer(context, a, attribute, values);

            } else if (type == ARenderType.Search) {

                control = new ASelectionRenderer(context, attribute);
            }

            desc = attribute.getDescription();
            grid.addCell(new FieldInputCell(label, desc, control), attribute.getCellSpan());
        }

        sb.append(grid);

        if (caption != null) {
            sb.append("</fieldset>");
        }

        return sb.toString();
    }
}
