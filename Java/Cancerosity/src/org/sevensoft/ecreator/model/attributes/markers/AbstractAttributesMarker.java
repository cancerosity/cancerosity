package org.sevensoft.ecreator.model.attributes.markers;

import java.util.Collection;
import java.util.Map;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.attributes.Attributable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.dep.OldAttributesRenderer;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
abstract class AbstractAttributesMarker implements IItemMarker {

	private final boolean	table;
	private final boolean	summary;

	AbstractAttributesMarker(boolean table, boolean summary) {
		this.table = table;
		this.summary = summary;
	}

	private Object generate(RequestContext context, Map<String, String> params, Attributable a) {

		final MultiValueMap<Attribute, String> attributeValues = a.getAttributeValues();
		if (attributeValues.isEmpty()) {
			return null;
		}

		Collection<Integer> excludes;
		String exclude = params.get("exclude");
		excludes = StringHelper.explodeIntegers(exclude, ",");
		String with = params.get("with");
		String replace = params.get("replace");

		boolean stripEmails = false;

		final OldAttributesRenderer r = new OldAttributesRenderer(context, a, attributeValues, table, summary, stripEmails, true, excludes);
		r.setReplace(replace);
		r.setWith(with);

		return r;
	}

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
		return generate(context, params, item);
	}

}
