package org.sevensoft.ecreator.model.attributes.renderers.input;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.misc.FieldInputCell;
import org.sevensoft.ecreator.model.attributes.Attributable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeOption;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.util.Grid;

/**
 * @author sks 13 Mar 2007 12:15:11
 * 
 * Renders a group of attributes in a grid. 
 * Does not include field set but could be used inside one.
 *
 */
public class AFormInputPanel extends Panel {

	public static String getInputDivId(int id) {
		return "inputDiv" + id;
	}

	public static String getInputDivId(Attribute a) {
		return getInputDivId(a.getId());
	}

    public static String getInputName(int id) {
        return "attributeValues_" + id;
    }

	private final List<Attribute>			attributes;
	private MultiValueMap<Attribute, String>	attributeValues;
	private final Attributable			a;
	private int						columns;
	private List<Integer>				hiddenAttributes;

	public AFormInputPanel(RequestContext context, Attributable a, List<Attribute> attributes) {
		super(context);

		this.a = a;
		this.attributes = attributes;
		this.attributeValues = a.getAttributeValues();

		this.hiddenAttributes = new ArrayList();
		for (Attribute attribute : attributes) {
			if (attribute.isSelection()) {
				for (AttributeOption option : attribute.getOptions()) {
					for (Integer id : option.getRevealIds()) {
						this.hiddenAttributes.add(id);
					}
				}
			}
		}
	}

	@Override
	public void makeString() {

		if (columns < 1) {
			columns = 1;
		}

		Grid grid = new Grid(columns);

		for (Attribute attribute : attributes) {

			String label = null;
			if (attribute.isShowLabel()) {

				label = attribute.getName();
				if (attribute.isMandatory()) {
					label = "*" + label;
				}
			}

			String desc = attribute.getDescription();

			List<String> values = attributeValues.list(attribute);
			AInputRenderer input = new AInputRenderer(context, attribute, values);


			ErrorTag error = new ErrorTag(context, attribute.getParamName(), " ");
			error.setMessage(attribute.getErrorMessage());

			int span = attribute.getCellSpan();
			if (columns < span) {
				span = 1;
			}

			FieldInputCell inputCell = new FieldInputCell(label, desc, input, error);
			inputCell.setId(getInputDivId(attribute));
			if (hiddenAttributes.contains(attribute.getId())) {
				inputCell.setHidden(true);
			}

			grid.addCell(inputCell, span);
		}

		sb.append(grid);
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

}
