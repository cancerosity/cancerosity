package org.sevensoft.ecreator.model.attributes.markers;

import java.util.List;
import java.util.Map;

import org.sevensoft.ecreator.model.attributes.Attributable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.values.AValueRenderer;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class AttributeValueMarker extends MarkerHelper implements IItemMarker, IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (params.containsKey("account")) {

			Item account = (Item) context.getAttribute("account");
			if (account == null) {
				return null;
			}

			return generate(context, params, account);
		}

		return null;

	}

	public Object generate(RequestContext context, Map<String, String> params, Attributable a) {

		String id = params.get("id");
		String sep = params.get("sep");
		String replace = params.get("replace");
		String with = params.get("with");
        boolean value = params.containsKey("value");

		int max = 0;
		if (params.containsKey("max")) {
			max = Integer.parseInt(params.get("max"));
		}

		if (!params.containsKey("class")) {
			params.put("class", "attribute");
		}

		String dateFormat = params.get("dateformat");
		if (dateFormat == null) {
			dateFormat = params.get("format");
		}

		String replaceNewLines = params.get("replacenl");

		Attribute attribute = EntityObject.getInstance(context, Attribute.class, id);
		if (attribute == null) {
			logger.fine("[AttributeValueMarker] no attribute found for id=" + id);
			return null;
		}

		// check attribute is shown for all types of item
		if (a instanceof Item) {
			Item item = (Item) a;
			if (attribute.isDisplayOnSubscriberOnly() && !item.getSubscription().hasSubscriptionLevel()) {
				return null;
			}
		}

		List<String> values = a.getAttributeValues(attribute);
		if (values == null || values.isEmpty()) {
			logger.fine("[AttributeValueMarker] no values for attributable=" + a);
			return null;
		}

		StringBuilder sb = new StringBuilder();

        if (!value) {
            if (params.containsKey("name")) {

                sb.append("<span class='attribute-name'>");
                sb.append(attribute.getName());
                sb.append("</span>");

                if (sep != null) {
                    sb.append(sep);
                }
            }

            sb.append("<span class='attribute-value'>");
            addAttributeValues(context, a, replace, with, max, dateFormat, replaceNewLines, attribute, values, sb);
            sb.append("</span>");
            return super.string(context, params, sb.toString());

        } else {
            addAttributeValues(context, a, replace, with, max, dateFormat, replaceNewLines, attribute, values, sb);

            return sb.toString();
        }
    }

    private void addAttributeValues(RequestContext context, Attributable a, String replace, String with, int max, String dateFormat, String replaceNewLines, Attribute attribute, List<String> values, StringBuilder sb) {
        AValueRenderer displayRenderer = new AValueRenderer(context, a, attribute, values);
        displayRenderer.setDateFormat(dateFormat);
        displayRenderer.setReplace(replace);
        displayRenderer.setWith(with);
        displayRenderer.replaceNewLines(replaceNewLines);
        displayRenderer.setMax(max);

        sb.append(displayRenderer);
    }

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		if (params.containsKey("account")) {

			if (item.hasAccount()) {

				return generate(context, params, item.getAccount());

			} else {

				return null;
			}

		} else if (params.containsKey("assocationId")) {

			String associationId = params.get("associationId");
			if (associationId == null) {
				return null;
			}

			String associatedItemId = item.getAttributeValue(associationId);
			Item associatedItem = EntityObject.getInstance(context, Item.class, associatedItemId);

			if (associatedItem == null) {
				logger.fine("[AttributeValueMarker] no associated item found");
				return null;
			}

			return generate(context, params, associatedItem);

		} else {

			return generate(context, params, item);
		}
	}

	public Object getRegex() {
		return new String[] { "attribute", "attribute_value" };
	}
}
