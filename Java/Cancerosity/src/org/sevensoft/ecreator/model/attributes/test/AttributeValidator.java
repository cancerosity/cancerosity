package org.sevensoft.ecreator.model.attributes.test;

import org.sevensoft.commons.validators.Validator;
import org.sevensoft.ecreator.model.attributes.Attribute;

/**
 * @author sks 29 May 2006 23:15:50
 *
 */
public class AttributeValidator extends Validator {

	private final Attribute	attribute;

	public AttributeValidator(Attribute attribute) {
		this.attribute = attribute;
	}

	public String validate(String value) {
		String error = attribute.test(value);
		return error;
	}
}
