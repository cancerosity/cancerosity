package org.sevensoft.ecreator.model.attributes.renderers;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.EcreatorRenderer;
import org.sevensoft.ecreator.iface.admin.attributes.AttributeHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeOwner;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * Renders a list of all attributes for the owner object.
 * 
 * @author sks 27-Mar-2006 00:29:20
 *
 */
public class OwnerAttributesPanel extends EcreatorRenderer {

	private final boolean				showOwner;
	private boolean					showAddBox;
	private MultiValueMap<String, Attribute>	sections;
	private final Link				linkback;
	private String					caption;
	private final AttributeOwner			owner;

	public OwnerAttributesPanel(RequestContext context) {
		this(context, null);
	}

	public OwnerAttributesPanel(RequestContext context, AttributeOwner owner) {
		this(context, owner, null);
	}

	public OwnerAttributesPanel(RequestContext context, AttributeOwner owner, Link linkback) {

		super(context);

		this.owner = owner;
		this.linkback = linkback;
		this.sections = new MultiValueMap(new LinkedHashMap());

		if (owner == null) {

			this.showAddBox = false;
			this.showOwner = true;

			for (Attribute attribute : Attribute.get(context)) {
				sections.put(attribute.getSection(), attribute);
			}

		} else {

			this.showOwner = false;
			this.showAddBox = true;

			for (Attribute attribute : owner.getAttributes()) {
				sections.put(attribute.getSection(), attribute);
			}

		}
	}

	public final void setCaption(String caption) {
		this.caption = caption;
	}

	@Override
	public String toString() {

		int colspan = 4;

		StringBuilder sb = new StringBuilder();

		if (caption == null) {
			caption = "Attributes";
		}

		sb.append(new AdminTable(caption));

		sb.append("<tr>");
		sb.append("<th width='80'>Position</th>");
		if (owner == null) {
			sb.append("<th width='10'>Delete</th>");
		}
		sb.append("<th>Name</th>");
		sb.append("<th>Type</th>");

		if (showOwner) {
			colspan++;
			sb.append("<th>Owner</th>");
		}

		colspan++;
		sb.append("<th>Page</th>");

		if (owner != null) {
			sb.append("<th width='10'> </th>");
		}

		sb.append("</tr>");

		PositionRender pr = new PositionRender(AttributeHandler.class, "attribute");
		for (Map.Entry<String, List<Attribute>> entry : sections.entryListSet()) {

			String section = entry.getKey();
			if (section == null) {
				sb.append("<tr><th colspan='" + colspan + "'>Attributes not assigned a section</th></tr>");
			} else {
				sb.append("<tr><th colspan='" + colspan + "'>" + section + "</th></tr>");
			}

            for (Attribute attribute : entry.getValue()) {
                if (!attribute.isDublicate()) {

                    sb.append("<tr>");
                    sb.append("<td width='80'>" + pr.render(attribute, "linkback", linkback) + "</td>");

                    if (owner == null) {
                        sb.append("<td width='30'>" + new CheckTag(context, "attributesToDelete", attribute, false) + "</td>");
                    }

                    sb.append("<td>"
                            + new LinkTag(AttributeHandler.class, "edit", new ImageTag("files/graphics/admin/spanner.gif"), "attribute", attribute)
                            + " " + attribute.getName() + " (#" + attribute.getId() + ")</td>");

                    sb.append("<td>" + attribute.getType() + "</td>");

                    if (showOwner) {
                        sb.append("<td>" + attribute.getOwnerString() + "&nbsp;</td>");
                    }

                    int page = attribute.getPage();
                    sb.append("<td>" + page + "</td>");

                    if (owner != null) {

                        sb.append("<td width='10'>"
                                + new LinkTag(AttributeHandler.class, "delete", new DeleteGif(), "attribute", attribute)
                                .setConfirmation("Are you sure you want to delete this attribute?") + "</td>");
                    }

                    sb.append("</tr>");
                }
            }
        }

		if (showAddBox) {
			sb.append("<tr><td colspan='" + colspan + "'>" + new TextAreaTag(context, "newAttributes", 40, 3) + "</td></tr>");

			if (MiscSettings.getInstance(context).isAdvancedMode()) {
				sb.append("<tr><td colspan='" + colspan + "'>Section " + new TextTag(context, "newAttributesSection", 40) + "</td></tr>");
				sb.append("<tr><td colspan='" + colspan + "'>Page " + new TextTag(context, "newAttributesPage", 4) + "</td></tr>");
			}

		}

		sb.append("</table>");

		return sb.toString();
	}

}
