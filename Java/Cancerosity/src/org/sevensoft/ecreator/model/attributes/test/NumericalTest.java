package org.sevensoft.ecreator.model.attributes.test;

import org.sevensoft.ecreator.model.attributes.Attribute;

/**
 * @author sks 28 Oct 2006 22:21:11
 *
 */
public class NumericalTest extends AttributeTest {

	@Override
	public String test(Attribute attribute, String value) {

		logger.fine("[attribute type] testing value=" + value + " type=" + this);

		try {

			double d = Double.parseDouble(value);
			if (attribute.hasMin() && d < attribute.getMin())
				return "Value cannot be less than " + attribute.getMin();

			if (attribute.hasMax() && d > attribute.getMax())
				return "Value cannot be greater than " + attribute.getMax();

			return null;

		} catch (NumberFormatException e) {
			return "Please enter valid number";
		}
	}

}
