package org.sevensoft.ecreator.model.attributes;

/**
 * @author sks 26 Apr 2007 10:00:32
 *
 */
public enum SearchControl {

	Min, Max, Before, After, Equals, Wildcard

}
