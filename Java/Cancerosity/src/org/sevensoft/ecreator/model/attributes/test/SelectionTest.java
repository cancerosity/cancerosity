package org.sevensoft.ecreator.model.attributes.test;

import org.sevensoft.ecreator.model.attributes.Attribute;

/**
 * @author sks 28 Oct 2006 22:21:16
 *
 */
public class SelectionTest extends AttributeTest {

	@Override
	public String test(Attribute attribute, String value) {

		//		logger.fine("[SelectionTest] attribute=" + attribute + ", value=" + value);
		return attribute.getOptionValues().contains(value) ? null : "Please choose a valid option";
	}

}
