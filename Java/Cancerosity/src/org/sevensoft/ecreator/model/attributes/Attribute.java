package org.sevensoft.ecreator.model.attributes;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.collections.Transformer;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.RandomHelper;
import org.sevensoft.commons.superstrings.comparators.NaturalStringComparator;
import org.sevensoft.commons.superstrings.english.Pluraliser;
import org.sevensoft.commons.validators.net.EmailValidator;
import org.sevensoft.commons.validators.text.PostcodeValidator;
import org.sevensoft.ecreator.model.accounts.permissions.Permissions;
import org.sevensoft.ecreator.model.attributes.box.AttributeBox;
import org.sevensoft.ecreator.model.attributes.test.*;
import org.sevensoft.ecreator.model.bookings.BookingSettings;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategoryItem;
import org.sevensoft.ecreator.model.crm.jobsheets.Jobsheet;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.ecreator.model.extras.calendars.blocks.CalendarBlock;
import org.sevensoft.ecreator.model.extras.calendars.box.CalendarBox;
import org.sevensoft.ecreator.model.feeds.Feed;
import org.sevensoft.ecreator.model.feeds.csv.CsvImportFeedField;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.ItemTypeSettings;
import org.sevensoft.ecreator.model.items.favourites.FavouritesGroupAttribute;
import org.sevensoft.ecreator.model.items.options.OptionGroup;
import org.sevensoft.ecreator.model.items.reviews.ReviewsModule;
import org.sevensoft.ecreator.model.items.sorts.ItemSort;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterControl;
import org.sevensoft.ecreator.model.marketing.sms.SmsSettings;
import org.sevensoft.ecreator.model.misc.location.Locatable;
import org.sevensoft.ecreator.model.misc.location.LocationUtil;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.ecreator.model.misc.seo.GoogleSettings;
import org.sevensoft.ecreator.model.search.forms.Filter;
import org.sevensoft.ecreator.model.search.forms.SearchField;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.PositionComparator;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

import java.lang.reflect.Member;
import java.text.ParseException;
import java.util.*;

/**
 * @author sks 27-Sep-2005 18:19:17
 */
@Table("attributes")
public class Attribute extends EntityObject implements Positionable, Selectable, Logging, Comparable<Attribute>, Permissions {

    public enum AutoValue {
        RandomNumber, Incremental, CurrentDateTime;
    }

    public enum DateSearchType {
        Before, After, On, Between
    }

    public enum RandomType {
        Numeric, AlphaUpper, AlphaUpperNumeric;
    }

    /**
     * Returns all attributes across the site.
     */
    public static List<Attribute> get(RequestContext context) {
        Query q = new Query(context, "select * from # order by position ");
        q.setTable(Attribute.class);
        return q.execute(Attribute.class);
    }

    public static Map<String, String> getSelectionMap(RequestContext context) {
        Query q = new Query(context, "select id, name from # order by position");
        q.setTable(Attribute.class);
        return q.getMap(String.class, String.class, new LinkedHashMap());
    }

    /**
     * Returns a list of attributes that are of this type
     */
    public static List<Attribute> get(RequestContext context, AttributeType type) {
        Query q = new Query(context, "select * from # where type=? order by position ");
        q.setTable(Attribute.class);
        q.setParameter(type);
        return q.execute(Attribute.class);
    }

    public static Attribute get(RequestContext context, String string) {

        string = string.toLowerCase();

        for (Attribute attribute : get(context)) {
            if (attribute.getName().toLowerCase().startsWith(string)) {
                return attribute;
            }
        }
        return null;
    }

    private static List<Attribute> getBySection(RequestContext context, String section) {
        return SimpleQuery.execute(context, Attribute.class, "section", section);
    }

    /**
     * Strips out all attributes except for the lead attribute for each section
     */
    public static List<Attribute> getLead(List<Attribute> attributes) {

        // sort attributes by position
        Collections.sort(attributes);

        final Set<String> sections = new HashSet<String>();
        CollectionsUtil.filter(attributes, new Predicate<Attribute>() {

            public boolean accept(Attribute e) {
                if (sections.contains(e.getSection()))
                    return false;
                sections.add(e.getSection());
                return true;
            }
        });

        return attributes;
    }

    public static List<Attribute> getNameOrdered(RequestContext context) {
        Query q = new Query(context, "select * from # order by name ");
        q.setTable(Attribute.class);
        return q.execute(Attribute.class);
    }

    private String pageName;

    private Jobsheet jobsheet;

    /**
     * Show this attribute on the invoice for orderable item types
     */
    private boolean invoiceColumn;

    /**
     * If flagged to true then the value of this attribute will override the member's proper name when displaying their name,
     */
    private boolean memberName;

    /**
     * Owned by a ratings module
     */
    private ReviewsModule reviewsModule;

    private BookingSettings bookingSettings;

    /**
     * The item type owner
     */
    @Index()
    private ItemType itemType;

    /**
     * The item type of the association
     */
    private ItemType associationItemType;

    /**
     * Show this attribute when adding a listing
     */
    @Default("1")
    private boolean listable;

    /**
     * Extra details description this attribute
     */
    private String description;

    private OptionGroup optionGroup;

    /**
     * Flags if this attribute should be shown in category pages as a filter
     */
    private boolean filter;

    private SearchControl searchControl;

    /**
     * Number of cells this attribute VALUE should fill
     */
    private int cellSpan;

    /**
     * makes attribute value a link to search
     */
    private boolean linkable;

    /**
     * Allows us to sort options manually
     */
    private boolean customOptionsOrder;

    /**
     * Show this attribute at checkout, if this attribute is an order settngs one.
     */
    private boolean checkout;

    private boolean linkNewWindow;

    /**
     * Override the actual link url with this text.
     */
    private String linkText;

    /**
     * Min and max values for numerical attributes
     */
    private int max, min;

    private int step;

    /**
     * The subject of the email if this is an email attribute
     * Can use the special tag [item] to get the name of the item this attribute is on or [ref] or [id]
     */
    private String subject;

    /**
     * Number of places for the grouping
     */
    private boolean grouping;

    /**
     * When inputting for a listing show images instead of text links
     */
    private boolean imagesOnAddListing;

    /**
     * Flags if this attribute can have multiple input values
     */
    private boolean multi;

    /**
     *
     */
    private AutoValue autoValue;

    private int incrementStep;

    /**
     * Is this attribute user viewable (public) or admin only (private)
     * This has now become DISPLAYABLE,
     * ie if private it will not be displayed on the site.
     * <p/>
     * But an attribute can still be listable
     */
    private boolean displayable;

    /**
     * Can this attribute be edited after creation (by the user)
     */
    @Default("1")
    private boolean editable;

    private String emailCc;

    private String emailBcc;

    /**
     * The name of this attribute
     */
    private String name;

    /**
     * Cells / TDs per TR for images when adding a listing
     */
    private int imagesPerRow;

    /**
     * Is entry of this attribute optional if the attribute type allows optional values
     */
    private boolean optional;

    /**
     * How many options
     */
    private int optionCount;

    /**
     *
     */
    private Money sellPriceAdjustment, costPriceAdjustment;

    /**
     * Order of attributes
     */
    private int position;

    /**
     *
     */
    private boolean displayOnSubscriberOnly;

    /**
     * Flags if this attribute should be shown on the register page.
     */
    private boolean registration;

    /**
     * Split up attributes by page to keep signup forms simple.
     * <p/>
     * 23/08 Used to apply only to registration but now listings too, so called registraton page for BC
     */
    private int registrationPage;

    private int yearRange;

    private int startYear;

    /**
     * If an attribute is tagged as recommended then customers are reminded to fill it in periodically.
     */
    private boolean recommended;

    /**
     * Should we show a label for this attribute, If set to yes then Age: 25, otherwise just 25.
     */
    private boolean hideLabel;

    /**
     * The name of the column on the invoice. If null then the normal name
     */
    private String invoiceName;

    /**
     * Text used on mandatory selection attributes to show a non selectionable option in the drop down
     */
    private String intro;

    /**
     * The category this attribute is defined on.
     */
    private Category category;

    /**
     * Width and height for text
     * cols does number of columns for check boxed selections
     */
    private int rows, cols;

    /**
     * Lock this attribute from members until they have paid.
     */
    private boolean permissions;

    /**
     * Flags this attribute as the one that defines the location. This attribute when set will set the location back on the item or member.
     */
    private boolean location;

    /**
     *
     */
    private String section;

    /**
     * Should show in summaries
     */
    private boolean summary;

    /**
     * Sets this attribute to be used in calculating shipping on custom delivery rates.
     */
    private boolean delivery;

    /**
     * Data type of attribute
     */
    private AttributeType type;

    /**
     *
     */
    private String restrictionText;

    /**
     * Owned by order settings
     */
    private OrderSettings orderSettings;

    /**
     *
     */
    private String anyLabel;

    /**
     * Prefix put before values (numerical only atm)
     */
    private String prefix;

    /**
     *
     */
    private String regExp;

    /**
     * Mininum and max decimal places when formatting numbers
     */
    private int minDp, maxDp;

    private String suffix;

    /**
     * The default value when creating a new item / member using this attribute
     */
    private String defaultNew;

    /**
     * options cache
     */
    private transient List<AttributeOption> optionsCache;

    /**
     * Columns / cells per row when rendering
     */
    private int cellsPerRow;

    /**
     * Separator used between multi values when using multi value attributes.
     */
    private String multiValueSeparator;

    private NewsletterControl newsletterControl;

    private String noneLabel;

    private ItemTypeSettings itemTypeSettings;

    private SmsSettings smsSettings;

    private String customErrorMessage;

    /**
     * How to format a value for this attribute, using the standard java mask formatter
     * <p/>
     * #  	Any valid number, uses Character.isDigit.
     * ' 	Escape character, used to escape any of the special formatting characters.
     * U	Any character (Character.isLetter). All lowercase letters are mapped to upper case.
     * L	Any character (Character.isLetter). All upper case letters are mapped to lower case.
     * A	Any character or number (Character.isLetter or Character.isDigit)
     * ?	 Any character (Character.isLetter).
     * *	Anything.
     * H	Any hex character (0-9, a-f or A-F).
     */
    private String format;

    private int incrementStart;

    private int randomLength;

    /**
     * Custom error message
     */
    private String errorMessage;

    private boolean dublicate;

    /**
     *  a checkbox that appears as the first selection to act as the 'Any' option,
     */
    private boolean checkAllOptions;

    private String checkAllFrontLabel;

    private boolean noFollow;

    /**
     * Ibex attribute
     * if true this attribute options will have a field:
     * The region id (rnid) used for the iBex booking system
     */
    private boolean ibex;

    private boolean dateStart;
    private boolean dateEnd;

    public Attribute(RequestContext context) {
        super(context);
    }

    public Attribute(RequestContext context, AttributeOwner owner, String name, AttributeType type) {
        super(context);

        this.name = name;

        if (owner instanceof Category) {
            this.category = (Category) owner;
        }

        if (owner instanceof NewsletterControl) {
            this.newsletterControl = (NewsletterControl) owner;
        } else if (owner instanceof ItemType) {
            this.itemType = (ItemType) owner;
        } else if (owner instanceof OrderSettings) {
            this.orderSettings = (OrderSettings) owner;
        } else if (owner instanceof ReviewsModule) {
            this.reviewsModule = (ReviewsModule) owner;
        } else if (owner instanceof Jobsheet) {
            this.jobsheet = (Jobsheet) owner;
        } else if (owner instanceof OptionGroup) {
            this.optionGroup = (OptionGroup) owner;
        } else if (owner instanceof ItemTypeSettings) {
            this.itemTypeSettings = (ItemTypeSettings) owner;
        } else if (owner instanceof BookingSettings) {
            this.bookingSettings = (BookingSettings) owner;
        } else if (owner instanceof SmsSettings) {
            this.smsSettings = (SmsSettings) owner;
        } else {
            throw new RuntimeException();
        }

        if (type == null) {
            this.type = AttributeType.Text;
        } else {
            this.type = type;
        }

        this.optional = true;
        this.rows = 1;
        this.cols = 20;
        this.listable = true;
        this.registration = true;
        this.displayable = true;
        this.editable = true;

        save();
    }

    public void addOption(String value) {

        if (value != null) {

            new AttributeOption(context, this, value);
            optionsCache = null;

            setOptionCount();
            save();
        }

    }

    public int compareTo(Attribute a) {

        if (position < a.position) {
            return -1;
        }

        if (position > a.position) {
            return 1;
        }

        return super.compareTo(a);
    }

    /**
     * @throws CloneNotSupportedException
     */
    private void copyOptionsTo(Attribute copy) throws CloneNotSupportedException {
        for (AttributeOption option : getOptions()) {
            option.copyTo(copy);
        }
    }

    public Attribute copyTo(AttributeOwner owner) throws CloneNotSupportedException {
        return copyTo(owner, false);
    }

    public Attribute copyTo(AttributeOwner owner, boolean dublicate) throws CloneNotSupportedException {

        Attribute copy = (Attribute) super.clone();
        if (owner instanceof ItemType) {
            copy.itemType = (ItemType) owner;
        }
        copy.setDublicate(dublicate);
        copy.save();

        // copy options
        copyOptionsTo(copy);

        return copy;
    }

    /**
     * Creates options from pre-existing values set on items.
     */
    public void createOptionsFromValues() {

        List<String> optionValues = getOptionValues();
        for (String value : getValues()) {

            if (!optionValues.contains(value)) {
                new AttributeOption(context, this, value);
            }
        }

        optionsCache = null;
    }

    @Override
    public synchronized boolean delete() {

        // delete all options
        SimpleQuery.delete(context, AttributeOption.class, "attribute", this);
        SimpleQuery.delete(context, AttributeValue.class, "attribute", this);

        // filters
        SimpleQuery.delete(context, Filter.class, "attribute", this);

        // remove from feeds
        for (Feed feed : Feed.getAll(context)) {
            feed.removeAttribute(this);
        }

        SimpleQuery.delete(context, FavouritesGroupAttribute.class, "attribute", this);

        // remove from search fields
        new Query(context, "update # set attribute=0 where attribute=?").setTable(SearchField.class).setParameter(this).run();

        /*
           * Delete as import field
           */
        new Query(context, "delete from # where attribute=?").setTable(CsvImportFeedField.class).setParameter(this).run();

        // remove from calendar box / block attributes
        SimpleQuery.delete(context, CalendarBox.class, "startAttribute", this);
        SimpleQuery.delete(context, CalendarBox.class, "endAttribute", this);

        SimpleQuery.delete(context, CalendarBlock.class, "startAttribute", this);
        SimpleQuery.delete(context, CalendarBlock.class, "endAttribute", this);

        // remove sorts
        new Query(context, "delete from # where attribute=?").setTable(ItemSort.class).setParameter(this).run();

        // boxes
        new Query(context, "delete from # where attribute=?").setTable(AttributeBox.class).setParameter(this).run();

        boolean r = super.delete();

        return r;
    }

    /**
     * Returns the value of the any label, which is used when selecting from a drop down list and the value is optional.
     */
    public String getAnyLabel() {
        return anyLabel == null ? "Any" : anyLabel;
    }

    public List<String> getApplicableValues(Category category, Map<Attribute, String> attributeValues) {

        if (category == null) {
            return getApplicableValues(attributeValues);
        }

        if (attributeValues == null || attributeValues.isEmpty()) {
            return getCurrentValues(200);
        }

        StringBuilder sb = new StringBuilder();
        sb.append("select distinct av.value from # av join ( select item from # where ");

        for (int n = 0; n < attributeValues.size(); n++) {
            sb.append("(attribute=? and value=?)");

            if (n > 0) {
                sb.append(" or ");
            }
        }

        sb.append(" group by item having count(*) =? ) Z on av.item=Z.item join # ci on av.item=ci.item where av.attribute=?  and ci.category=? ");

        Query q = new Query(context, sb);

        q.setTable(AttributeValue.class);
        q.setTable(AttributeValue.class);
        q.setTable(CategoryItem.class);

        for (Map.Entry<Attribute, String> entry : attributeValues.entrySet()) {

            q.setParameter(entry.getKey());
            q.setParameter(entry.getValue());
        }

        q.setParameter(attributeValues.size());
        q.setParameter(this);
        q.setParameter(category);

        List<String> values = q.getStrings();

        /*
           * Use natural sorting for attribute values
           */
        Collections.sort(values, NaturalStringComparator.instance);

        return values;
    }

    /**
     * Returns a list of values for this attribute, for all items of param item type that also have attribute values contained in attribute values
     */
    public List<String> getApplicableValues(Map<Attribute, String> attributeValues) {

        if (attributeValues == null || attributeValues.isEmpty()) {
            return getCurrentValues(200);
        }

        StringBuilder sb = new StringBuilder();
        sb.append("select distinct av.value from # av join ( select item from # where ");

        for (int n = 0; n < attributeValues.size(); n++) {

            if (n > 0) {
                sb.append(" or ");
            }

            sb.append("(attribute=? and value=?)");

        }

        sb.append(" group by item having count(*) =? ) Z on av.item=Z.item where av.attribute=?");

        Query q = new Query(context, sb);

        q.setTable(AttributeValue.class);
        q.setTable(AttributeValue.class);

        for (Map.Entry<Attribute, String> entry : attributeValues.entrySet()) {

            q.setParameter(entry.getKey());
            q.setParameter(entry.getValue());
        }

        q.setParameter(attributeValues.size());
        q.setParameter(this);

        return q.getStrings();
    }

    public final ItemType getAssociationItemType() {
        return (ItemType) (associationItemType == null ? null : associationItemType.pop());
    }

    public Comparator getAttributableComparator() {

        return new Comparator() {

            public int compare(Object o1, Object o2) {

                String value1 = ((Attributable) o1).getAttributeValue(Attribute.this);
                String value2 = ((Attributable) o2).getAttributeValue(Attribute.this);

                if (value1 == null && value2 == null) {
                    return 0;
                }

                if (value1 == null) {
                    return 1;
                }

                if (value2 == null) {
                    return -1;
                }

                switch (getType()) {

                    default:
                        return NaturalStringComparator.instance.compare(value1, value2);

                    case DateTime:
                    case Date:

                        long l = (Long.parseLong(value1) - Long.parseLong(value2));

                        if (l < 0) {
                            return -1;
                        }

                        if (l > 0) {
                            return 1;
                        }

                        return 0;

                    case Numerical:
                        return (int) (Double.parseDouble(value1) - Double.parseDouble(value2));
                }
            }
        };
    }

    private <E extends EntityObject> List<Attributable> getAttributables(Class<E> clazz) {

        Query q = new Query(context, "select obj.* from # obj join # av on obj.id=av." + clazz.getSimpleName() + " where av.attribute=?");
        q.setTable(clazz);
        q.setTable(AttributeValue.class);
        q.setParameter(this);
        List list = q.execute(clazz);
        return list;
    }

    public final AutoValue getAutoValue() {
        return autoValue;
    }

    public Category getCategory() {
        return (Category) (category == null ? null : category.pop());
    }

    public int getCellSpan() {
        return cellSpan < 1 ? 1 : cellSpan;
    }

    public int getCellsPerRow() {
        return cellsPerRow < 1 ? 1 : cellsPerRow;
    }

    public int getCols() {

        if (type == AttributeType.Postcode) {
            return 12;
        }

        if (cols < 4) {
            return 4;
        }

        if (cols > 60) {
            return 60;
        }

        return cols;
    }

    public final Money getCostPriceAdjustment() {
        return costPriceAdjustment;
    }

    /**
     * Returns a list of all values currently in use by items of this attribute.
     */
    public List<String> getCurrentValues(int limit) {

        Query q = new Query(context, "select distinct value from # where attribute=? order by value limit " + limit);
        q.setTable(AttributeValue.class);
        q.setParameter(this);
        List<String> values = q.getStrings();
        Collections.sort(values, NaturalStringComparator.instance);
        return values;
    }

    public String getCustomErrorMessage() {
        return customErrorMessage;
    }

    public String getDefaultNew() {

        switch (getType()) {

            default:
                return defaultNew;

            case Selection:
                for (AttributeOption option : getOptions()) {
                    if (option.isDefaultForNew()) {
                        return option.getValue();
                    }
                }

                return null;

        }
    }

    public void setDefaultNew(String defaultNew) {
        this.defaultNew = defaultNew;
    }

    public AttributeOption getDefaultSearchOption() {

        List<AttributeOption> options = getOptions();
        for (AttributeOption option : options) {
            if (option.isDefaultForSearch())
                return option;
        }

        return null;
    }

    public String getDefaultSearchOptionValue() {

        AttributeOption option = getDefaultSearchOption();
        return option == null ? null : option.getValue();
    }

    public String getDescription() {
        return description;
    }

    public String getEmailBcc() {
        return emailBcc;
    }

    public String getEmailCc() {
        return emailCc;
    }

    /**
     *
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    public final String getFormat() {
        return format;
    }

    private String getFriendlyUrl(String value) {
        return Seo.getFriendlyUrl(value) + "-a" + getIdString() + ".html";
    }

    /**
     * Returns the name of this attribute preceeded by the name of the attribute owner
     */
    public String getFullName() {

        if (hasItemType())
            return getItemType().getName() + ": " + name;

        if (hasCategory())
            return getCategory().getName() + ": " + name;

        return name;
    }

    public int getImagesPerRow() {
        return imagesPerRow < 1 ? 3 : imagesPerRow;
    }

    public final int getIncrementStart() {
        return incrementStart;
    }

    public final int getIncrementStep() {
        return incrementStep < 1 ? 1 : incrementStep;
    }

    /**
     * A label used when the attribute is not optional but you want to have an initial option
     */
    public String getIntro() {
        return intro;
    }

    public String getInvoiceName() {
        return invoiceName == null ? getName() : invoiceName;
    }

    /**
     * Returns the first item to match this attributes value
     */
    public Item getItemFromValue(String value) {

        if (value == null)
            return null;

        Query q = new Query(context, "select i.* from # i join # av on i.id=av.item where av.attribute=? and av.value=?");
        q.setTable(Item.class);
        q.setTable(AttributeValue.class);
        q.setParameter(this);
        q.setParameter(value);
        return q.get(Item.class);

    }

    private List<Item> getItems() {

        Query q = new Query(context, "select i.* from # i join # av on i.id=av.item where av.attribute=?");
        q.setTable(Item.class);
        q.setTable(AttributeValue.class);
        q.setParameter(this);
        return q.execute(Item.class);
    }

    /**
     * Get all items that match this value
     */
    public List<Item> getItems(String value) {

        Query q = new Query(context, "select i.* from # i join # av on i.id=av.item where av.attribute=? and av.value=?");
        q.setTable(Item.class);
        q.setTable(AttributeValue.class);
        q.setParameter(this);
        q.setParameter(value);
        return q.execute(Item.class);
    }

    public ItemType getItemType() {
        return (ItemType) (itemType == null ? null : itemType.pop());
    }

    public Jobsheet getJobsheet() {
        return (Jobsheet) (jobsheet == null ? null : jobsheet.pop());
    }

    public String getLabel() {
        return getName();
    }

    public String getLinkText() {
        return linkText;
    }

    public List<LogEntry> getLogEntries() {
        return null;
    }

    public String getLogId() {
        return getFullId();
    }

    public String getLogName() {
        return "Attribute #" + getId() + " " + getName();
    }

    public int getMax() {
        return max;
    }

    public int getMaxDp() {
        return maxDp;
    }

    private List<Member> getMembers() {
        return null;
    }

    public int getMin() {
        return min;
    }

    public int getMinDp() {
        return minDp;
    }

    public String getMultiValueSeparator() {
        return multiValueSeparator;
    }

    public String getName() {
        return name;
    }

    public String getNamePlural() {
        return Pluraliser.toPlural(getName());
    }

    /**
     * Returns a label for use when 'none' is used in a selection type, when the selection type is set to optional
     */
    public String getNoneLabel() {
        return noneLabel == null ? "None" : noneLabel;
    }

    public int getOptionCount() {
        return optionCount;
    }

    public AttributeOption getOptionFromValue(String value) {

        for (AttributeOption option : getOptions())
            if (option.getValue().equalsIgnoreCase(value))
                return option;

        return null;
    }

    /**
     *
     */
    public OptionGroup getOptionGroup() {
        return (OptionGroup) (optionGroup == null ? null : optionGroup.pop());
    }

    public List<AttributeOption> getOptions() {

//        if (optionsCache == null) {

        Query q = new Query(context, "select * from # where attribute=? order by value");
        q.setTable(AttributeOption.class);
        q.setParameter(this);
        optionsCache = q.execute(AttributeOption.class);

        if (customOptionsOrder) {
            Collections.sort(optionsCache, PositionComparator.instance);
        } else {
            Collections.sort(optionsCache, new Comparator<AttributeOption>() {

                public int compare(AttributeOption o1, AttributeOption o2) {
                    return NaturalStringComparator.instance.compare(o1.getValue(), o2.getValue());
                }
            });
        }

//        }

        return optionsCache;
    }

    /**
     * Returns a list of strings of all option values for this attribute
     */
    public List<String> getOptionValues() {

        List<AttributeOption> options = getOptions();
        return CollectionsUtil.transform(options, new Transformer<AttributeOption, String>() {

            public String transform(AttributeOption obj) {
                return obj.getValue();
            }
        });
    }

    public AttributeOwner getOwner() {

        if (hasCategory()) {
            return getCategory();
        }

        if (hasItemType()) {
            return getItemType();
        }

        if (isOrder()) {
            return OrderSettings.getInstance(context);
        }

        if (hasJobsheet()) {
            return getJobsheet();
        }

        if (hasOptionGroup()) {
            return getOptionGroup();
        }

        if (isBookingAttribute()) {
            return BookingSettings.getInstance(context);
        }

        if (reviewsModule != null) {
            return getReviewsModule();
        }

        if (itemTypeSettings != null) {
            return ItemTypeSettings.getInstance(context);
        }

        if (newsletterControl != null) {
            return NewsletterControl.getInstance(context);
        }

        if (smsSettings != null) {
            return SmsSettings.getInstance(context);
        }

        throw new RuntimeException("No owner recognised");
    }

    public String getOwnerString() {

        if (hasCategory()) {
            return "Category: " + getCategory().getName();
        }

        if (hasItemType()) {
            return "Item type: " + getItemType().getName();
        }

        if (orderSettings != null) {
            return "Orders";
        }

        if (bookingSettings != null) {
            return "Bookings";
        }

        if (hasJobsheet()) {
            return "Jobsheet: " + getJobsheet().getName();
        }

        if (hasOptionGroup()) {
            return "Option group: " + getOptionGroup().getName();
        }

        if (itemTypeSettings != null) {
            return "Global item attribute";
        }

        return name;
    }

    public int getPage() {
        return registrationPage < 1 ? 1 : registrationPage;
    }

    public final String getPageName() {
        return pageName;
    }

    public String getParamName() {
        return "attributeValues_" + getId();
    }

    public String getParamName(Item item) {
        String paramName = getParamName();
        if (item != null) {
            paramName = paramName + "_" + item.getId();
        }
        return paramName;
    }

    public String getParamName(Integer id) {
        String paramName = getParamName();
        paramName = paramName + "_" + id;
        return paramName;
    }

    public int getPosition() {
        return position;
    }

    public String getPrefix() {
        return prefix;
    }

    public final int getRandomLength() {
        return randomLength;
    }

    public String getRegExp() {
        return regExp;
    }

    public int getRegistrationPages() {
        Query q = new Query(context, "select distinct registrationPage from # where registration=1");
        q.setTable(Attribute.class);
        q.setParameter(this);
        return q.getInt();
    }

    @Deprecated
    public String getRelativeUrl(String value) {
        return getUrl(value);
    }

    public String getRestrictionForwardUrl() {
        return null;
    }

    public final String getRestrictionText() {
        return restrictionText == null ? "Only available to premium subscribers" : restrictionText;
    }

    public ReviewsModule getReviewsModule() {
        return (ReviewsModule) (reviewsModule == null ? null : reviewsModule.pop());
    }

    public int getRows() {

        if (type == AttributeType.Postcode) {
            return 1;
        }

        if (rows < 1) {
            return 1;
        }

        if (rows > 30) {
            return 30;
        }

        return rows;
    }

    public String getSection() {
        return section;
    }

    public final Money getSellPriceAdjustment() {
        return sellPriceAdjustment;
    }

    public final int getStartYear() {
        return startYear;
    }

    public int getStep() {
        return step;
    }

    public final String getSubject() {
        return subject;
    }

    public String getSuffix() {
        return suffix;
    }

    private AttributeTest getTester() {

        switch (getType()) {

            case Association:
                return null;

            case Boolean:
                return new BooleanTest();

            case Date:
                return new DateTest();

            case DateTime:
                return new DateTimeTest();

            case Email:
                return new EmailTest();

            case Link:
                return new LinkTest();

            case Numerical:
                return new NumericalTest();

            case Postcode:
                return new PostcodeTest();

            case Selection:
                return new SelectionTest();

            case Text:
                return new TextTest(context);
        }

        return null;
    }

    public AttributeType getType() {

        if (type == null)
            type = AttributeType.Text;

        return type;
    }

    /**
     * Returns a search engine friendly url mapping this value to
     * the search
     */
    public String getUrl(String value) {
        return Config.getInstance(context).getUrl() + "/" + getFriendlyUrl(value);
    }

    public String getValue() {
        return getIdString();
    }

    /**
     * Returns a list of all distinct values currently used by attributables for this attribute
     */
    public List<String> getValues() {

        Query q = new Query(context, "select distinct value from # where attribute=? order by value");
        q.setTable(AttributeValue.class);
        q.setParameter(this);
        return q.getStrings();
    }

    public final int getYearRange() {
        return yearRange;
    }

    public boolean hasAutoValue() {
        return autoValue != null;
    }

    public boolean hasCategory() {
        return category != null;
    }

    public boolean hasDefaultNew() {

        if (isSelection()) {

            for (AttributeOption option : getOptions()) {
                if (option.isDefaultForNew()) {
                    return true;
                }
            }

            return false;

        } else {

            return defaultNew != null;

        }
    }

    public boolean hasDescription() {
        return description != null;
    }

    public boolean hasEmailBcc() {
        return emailBcc != null;
    }

    public boolean hasEmailCc() {
        return emailCc != null;
    }

    public boolean hasFormat() {
        return format != null;
    }

    /**
     *
     */
    public boolean hasImages() {
        for (AttributeOption option : getOptions()) {
            if (option.hasAnyImage()) {
                return true;
            }
        }
        return false;
    }

    public boolean hasIntro() {
        return intro != null;
    }

    public boolean hasItemType() {
        return itemType != null;
    }

    public boolean hasJobsheet() {
        return jobsheet != null;
    }

    public boolean hasLimits() {
        return min > 0 && max > 0;
    }

    public boolean hasLinkText() {
        return linkText != null;
    }

    public boolean hasMax() {
        return max > 0;
    }

    public boolean hasMin() {
        return min > 0;
    }

    public boolean hasOptionGroup() {
        return optionGroup != null;
    }

    public boolean hasOptions() {
        return optionCount > 0;
    }

    public boolean hasPrefix() {
        return prefix != null;
    }

    public boolean hasRegExp() {
        return regExp != null;
    }

    public boolean hasReviewsModule() {
        return reviewsModule != null;
    }

    public boolean hasSection() {
        return section != null;
    }

    public boolean hasStep() {
        return step > 0;
    }

    public boolean hasSubject() {
        return subject != null;
    }

    public boolean hasSuffix() {
        return suffix != null;
    }

    public boolean hasSummary() {
        return section != null;
    }

    public void init(Attributable a) {

        if (autoValue == null) {
            return;
        }

        String value = null;
        switch (getAutoValue()) {

            case Incremental:

                Query q = new Query(context, "select max(value+0) from # where attribute=?");
                q.setTable(AttributeValue.class);
                q.setParameter(this);
                int max = q.getInt();

                logger.fine("[Attribute] increment max=" + max);

                if (max < getIncrementStart()) {

                    logger.fine("[Attribute] getIncrementStart=" + getIncrementStart());
                    value = String.valueOf(getIncrementStart());

                } else {

                    logger.fine("[Attribute] increment step=" + getIncrementStep());
                    value = String.valueOf(max + getIncrementStep());
                }

                logger.fine("[Attribute] increment value=" + value);

                break;

            case RandomNumber:

                for (int n = 0; n < 1000; n++) {
                    value = RandomHelper.getRandomDigits(getRandomLength());
                    if (SimpleQuery.count(context, AttributeValue.class, "attribute", this, "value", value) == 0) {
                        break;
                    }
                }
                break;

        }

        if (value != null) {
            a.setAttributeValue(this, value);
        }
    }

    public boolean isBookingAttribute() {
        return bookingSettings != null;
    }

    public boolean isCheckout() {
        return checkout && isOrder();
    }

    public final boolean isCustomOptionsOrder() {
        return customOptionsOrder;
    }

    public boolean isDate() {
        return getType() == AttributeType.Date;
    }

    public boolean isDateTime() {
        return getType() == AttributeType.DateTime;
    }

    public boolean isDelivery() {
        return delivery;
    }

    public boolean isDisplayable() {
        return displayable;
    }

    public final boolean isDisplayName() {
        return memberName;
    }

    public final boolean isEditable() {
        return editable;
    }

    public boolean isFilter() {
        return filter && !isLink();
    }

    public boolean isGlobalItemType() {
        return itemTypeSettings != null;
    }

    public final boolean isGrouping() {
        return grouping;
    }

    /**
     * Returns true if this attribute is hidden by any of the attributes passed in
     */
    public boolean isHidden(Collection<Attribute> attributes) {

        for (Attribute attribute : attributes) {
            if (attribute.isSelection()) {
                for (AttributeOption option : attribute.getOptions()) {
                    if (option.getRevealIds().contains(getId())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public boolean isHideLabel() {
        return hideLabel;
    }

    public boolean isImagesOnAddListing() {
        return imagesOnAddListing;
    }

    public boolean isInvoiceColumn() {
        return invoiceColumn;
    }

    public boolean isLink() {
        return getType() == AttributeType.Link;
    }

    public boolean isLinkable() {
        return linkable && !isLink();
    }

    public boolean isLinkNewWindow() {
        return linkNewWindow;
    }

    public boolean isListable() {
        return listable;
    }

    public boolean isLocation() {
       //not actual
//        if (!Config.getInstance(context).isLocations()) {
//            return false;
//        }

        switch (getType()) {

            default:
            case Date:
            case Email:
            case Numerical:
            case Link:
                return false;

            case Postcode:
            case Text:
                return location;

        }
    }

    public boolean isMandatory() {
        return !isOptional();
    }

    public boolean isMultiValues() {
        return multi;
    }

    public boolean isNewsletter() {
        return newsletterControl != null;
    }

    public boolean isNoGuestLogin() {
        return false;
    }

    public boolean isNumerical() {
        return getType() == AttributeType.Numerical;
    }

    public boolean isOptional() {

        /*
           * can't be mandatory and have permissions for now
           */
        if (isPermissions()) {
            return true;
        }

        return optional;
    }

    public boolean isOrder() {
        return orderSettings != null;
    }

    public boolean isPermissions() {
        return permissions && Module.Permissions.enabled(context);
    }

    public boolean isRecommended() {
        return recommended;
    }

    public boolean isRegistration() {
        return registration;
    }

    public boolean isSelection() {
        return getType() == AttributeType.Selection;
    }

    public boolean isShowLabel() {
        return !isHideLabel();
    }

    public boolean isSingle() {
        return !isMultiValues();
    }

    public boolean isSms() {
        return smsSettings != null;
    }

    public boolean isSummary() {
        return summary && !isLink() && isDisplayable();
    }

    public boolean isText() {
        return getType() == AttributeType.Text;
    }

    public boolean isDublicate() {
        return dublicate;
    }

    public void log(String message) {
        log(null, message);
    }

    public void log(User user, String message) {
        new LogEntry(context, this, user, message);
    }

    public void makeOptions() {

        for (String value : getCurrentValues(100)) {
            addOption(value);
        }

    }

    /**
     * This method will randomly fill in all attributes with a selection of values.
     * <p/>
     * <p/>
     * - If the attribute is a selection then it will randomise from the list of selections.
     * <p/>
     * - If the attribute is a numerical with range and step then it will choose from the range
     * <p/>
     * - If the attribute is another type, it will first get all currently existing values and use those to randomise (including re-randomising existing).
     */
    public int randomiseValues() {

        logger.fine("[Attribute] randomising");

        List<String> values = null;
        switch (getType()) {

            default:
                break;

            case Numerical:

                if (step > 0 && max > 0) {

                    values = new ArrayList();
                    int n = min;
                    while (n <= max && values.size() < 50) {

                        values.add(String.valueOf(n));
                        n = n + step;
                    }
                }

                logger.fine("[Attribute] created step values=" + values);

                break;

            case Selection:

                values = getOptionValues();
                logger.fine("[Attribute] values from options=" + values);

                break;
        }

        if (values == null) {
            values = getCurrentValues(50);
        }

        if (values.isEmpty()) {
            logger.fine("[Attribute] values is empty, exiting");
            return 0;
        }

        int n = 0;
        if (hasItemType()) {

            logger.fine("[Attribute] applying values to items of item type= " + itemType);

            for (Item item : new ItemSearcher(context)) {
                Collections.shuffle(values);
                item.setAttributeValue(this, values.get(0));

                n++;
            }

        }

        return n;
    }

    public void removeOption(AttributeOption option) {
        option.delete();
        optionsCache = null;

        setOptionCount();
        save();
    }

    /**
     * Remove all options that aren't in use in attribute values
     */
    public void removeUnusedOptions() {

        List<String> values = getValues();
        for (AttributeOption option : getOptions()) {

            if (!values.contains(option.getValue()))
                removeOption(option);

        }
    }

    public void replace(String regex, String replacement) {
        for (AttributeValue av : SimpleQuery.execute(context, AttributeValue.class, "attribute", this)) {
            String value = av.getValue();
            value = value.replaceAll(regex, replacement);
            av.setValue(value);
        }
    }

    /**
     * Sets all attribute values to the string param for the owner objects
     * <p/>
     * Returns the number of attributables changed.
     */
    public int resetAttributeValues(String value) {

        if (hasItemType()) {

            ItemSearcher searcher = new ItemSearcher(context);
            searcher.setItemType(getItemType());
            List<Item> items = searcher.getItems();

            for (Attributable a : items) {
                a.addAttributeValue(this, value);

            }

            return items.size();
        }

        return 0;

    }

    /**
     * Goes through all values for this attribute and re-calculates the location co-ordinates
     */
    public void resetLocations() {

        if (!isLocation())
            return;

        for (Attributable a : getItems()) {

            if (a instanceof Locatable) {

                String value = a.getAttributeValue(this);

                Locatable locatable = (Locatable) a;

                // must remove location first, other wise the system will detect they are equal and not reset the coords !
                locatable.setLocation(null, 0, 0);
                LocationUtil.setLocation(context, locatable, value);
            }
        }
    }

    public void setAnyLabel(String anyLabel) {
        this.anyLabel = anyLabel;
    }

    public final void setAssociationItemType(ItemType associationItemType) {
        this.associationItemType = associationItemType;
    }

    public final void setAutoValue(AutoValue autoValue) {
        this.autoValue = autoValue;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    private void setCellsNoRecurse(int c, boolean b) {
        cellsPerRow = c;
    }

    public void setCellSpan(int colspan) {
        this.cellSpan = colspan;
    }

    public void setCellsPerRow(int c) {
        this.cellsPerRow = c;

        // copy to all other attributes in section
        for (Attribute attribute : getBySection(context, section)) {
            attribute.setCellsNoRecurse(c, false);
            attribute.save();
        }
    }

    public void setCheckout(boolean checkout) {
        this.checkout = checkout;
    }

    public final void setCostPriceAdjustment(Money costPriceAdjustment) {
        this.costPriceAdjustment = costPriceAdjustment;
    }

    public void setCustomErrorMessage(String customErrorMessage) {
        this.customErrorMessage = customErrorMessage;
    }

    public final void setCustomOptionsOrder(boolean customOptionsOrder) {
        this.customOptionsOrder = customOptionsOrder;
    }

    public void setDelivery(boolean delivery) {
        this.delivery = delivery;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDisplayable(boolean d) {
        this.displayable = d;
    }

    public final void setDisplayName(boolean b) {

        if (memberName == b) {
            return;
        }

        this.memberName = b;
        new Query(context, "update # set displayName=null").setTable(Item.class).run();
    }

    public final void setEditable(boolean editable) {
        this.editable = editable;
    }

    public void setEmailBcc(String emailBcc) {
        this.emailBcc = emailBcc;
    }

    public void setEmailCc(String emailCc) {
        this.emailCc = emailCc;
    }

    public final void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void setFilter(boolean b) {
        this.filter = b;
    }

    public final void setFormat(String format) {
        this.format = format;
    }

    public final void setGrouping(boolean grouping) {
        this.grouping = grouping;
    }

    public void setHeight(int rows) {
        this.rows = rows;
    }

    public void setHideLabel(boolean label) {
        this.hideLabel = label;
    }

    public void setImagesOnAddListing(boolean listingInputAsImages) {
        this.imagesOnAddListing = listingInputAsImages;
    }

    public void setImagesPerRow(int imagesPerRow) {
        this.imagesPerRow = imagesPerRow;
    }

    public final void setIncrementStart(int incrementStart) {
        this.incrementStart = incrementStart;
    }

    public final void setIncrementStep(int incrementStep) {
        this.incrementStep = incrementStep;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public void setInvoiceColumn(boolean invoiceColumn) {
        this.invoiceColumn = invoiceColumn;
    }

    public void setInvoiceName(String invoiceName) {
        this.invoiceName = invoiceName;
    }

    public void setItemType(ItemType clazz) {

        /*
           * If this is not an item class then we cannot ever change it into an item class so return
           */
        if (itemType == null)
            return;

        /*
           * if this is an item class then we cannot set a null item class on it
           */
        if (clazz == null)
            return;

        /*
           * No need to change if they are equal
           */
        if (itemType.equals(clazz))
            return;

        this.itemType = clazz;
    }

    public void setLinkable(boolean linkable) {
        this.linkable = linkable;
    }

    public void setLinkNewWindow(boolean linkNewWindow) {
        this.linkNewWindow = linkNewWindow;
    }

    public void setLinkText(String linkText) {
        this.linkText = linkText;
    }

    public void setListable(boolean listable) {
        this.listable = listable;
    }

    public void setLocation(boolean location) {
        this.location = location;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public void setMaxDp(int maxDp) {
        this.maxDp = maxDp;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setMinDp(int minDp) {
        this.minDp = minDp;
    }

    public void setMulti(boolean multi) {
        this.multi = multi;
    }

    public void setMultiValueSeparator(String multiValueSeparator) {
        this.multiValueSeparator = multiValueSeparator;
    }

    public void setName(String name) {

        if (name.equals("action"))
            this.name = "action_";
        else
            this.name = name;
    }

    public void setNoneLabel(String noneLabel) {
        this.noneLabel = noneLabel;
    }

    public void setOptional(boolean optional) {
        this.optional = optional;
    }

    public void setOptionCount() {
        optionsCache = null;
        this.optionCount = getOptions().size();
    }

    public void setPage(int page) {
        this.registrationPage = page;
    }

    public final void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public void setPermissions(boolean locked) {
        this.permissions = locked;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public final void setRandomLength(int randomLength) {
        this.randomLength = randomLength;
    }

    public void setRecommended(boolean recommended) {
        this.recommended = recommended;
    }

    public void setRegExp(String regExp) {
        this.regExp = regExp;
    }

    public void setRegistration(boolean registration) {
        this.registration = registration;
    }

    public void setRestrictionForwardUrl(String restrictionForwardUrl) {
    }

    public final void setRestrictionText(String restrictionText) {
        this.restrictionText = restrictionText;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public final void setSellPriceAdjustment(Money sellPriceAdjustment) {
        this.sellPriceAdjustment = sellPriceAdjustment;
    }

    public final void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public final void setSubject(String subject) {
        this.subject = subject;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public void setSummary(boolean summary) {
        this.summary = summary;
    }

    public final void setType(AttributeType t) {

        if (type == t)
            return;

        this.type = t;
        switch (type) {

            default:

                cols = 20;
                rows = 1;
                break;

            case Postcode:
                cols = 12;
                rows = 1;
                break;

            case Email:
                cols = 40;
                rows = 1;

        }
    }

    public final void setValue(Attributable owner, String value) {

        logger.fine("[Attribute] setting attribute value attribute=" + getName() + " value=" + value + " owner=" + owner);

        if (value == null) {
            return;
        }

        value = value.trim();
        if (value.length() == 0) {
            return;
        }

        logger.fine("[Attribute] attribute type=" + getType());

        switch (getType()) {

            case Association:

                // ensure this is a valid item
                Item item = EntityObject.getInstance(context, Item.class, value);
                if (item != null) {
                    new AttributeValue(context, owner, this, value);
                }
                break;

            case Selection:

                // check the value is an option, if not create it
                if (!getOptionValues().contains(value)) {
                    new AttributeOption(context, this, value);
                }

                new AttributeValue(context, owner, this, value);

                if (isLocation()) {

                    logger.fine("[Attribute] attribute is set as location");

                    if (owner instanceof Locatable) {

                        logger.fine("[Attribute] owner is a locatable object");

                        AttributeOption option = getOptionFromValue(value);
                        logger.fine("[Attribute] getting location from option: " + option.getLocation());

                        logger.fine("[Attribute] owner is a locatable object");
                        LocationUtil.setLocation(context, (Locatable) owner, option.getLocation());

                    } else {

                        logger.fine("[Attribute] owner is NOT a locatable object");
                    }
                }

                break;

            case Date:

                // get Date object so we can get the timestamp

                if ("*".equals(value)) {

                    new AttributeValue(context, owner, this, value);

                    // if we now have TWO wildcards then delete both
                    if (SimpleQuery.count(context, AttributeValue.class, "attribute", this, owner.getClass().getSimpleName(), owner, "value", "*") == 2) {
                        SimpleQuery.delete(context, AttributeValue.class, "attribute", this, owner.getClass().getSimpleName(), owner, "value", "*");
                    }

                } else {

                    try {

                        Date date = new Date(value);
                        logger.fine("[Attribute] parsed for timestamp=" + date.getTimestamp() + " from value=" + value);

                        new AttributeValue(context, owner, this, date.getTimestampString());

                    } catch (ParseException e1) {
                        logger.fine("[Attribute] unable to parse for date");
                        return;
                    }
                }

                break;

            case DateTime:

                // get DateTime object so we can get the timestamp
                try {

                    DateTime date = new DateTime(value);
                    logger.fine("[Attribute] parsed timestamp=" + date.getTimestamp() + " from value=" + value);

                    new AttributeValue(context, owner, this, date.getTimestampString());

                } catch (ParseException e1) {
                    logger.fine("[Attribute] unable to parse for date time");
                    return;
                }

                break;

            case Boolean:
                new AttributeValue(context, owner, this, Boolean.valueOf(value).toString());
                break;

            case Numerical:

                if (value.equals("*")) {

                    new AttributeValue(context, owner, this, value);

                    // if we now have TWO wildcards then delete both
                    if (SimpleQuery.count(context, AttributeValue.class, "attribute", this, owner.getClass().getSimpleName(), owner, "value", "*") == 2) {
                        SimpleQuery.delete(context, AttributeValue.class, "attribute", this, owner.getClass().getSimpleName(), owner, "value", "*");
                    }

                } else {

                    try {

                        // parse for double to ensure this is a valid number
                        value = value.replaceAll("[^\\d-.]", "");
                        logger.fine("Stripped non digit / decimal point: " + value);

                        double d = Double.parseDouble(value);

                        // check for bounds
                        logger.fine("checking for bounds min=" + getMin() + " max=" + getMax());
                        if (getMin() <= d && (getMax() == 0 || d <= getMax())) {
                            new AttributeValue(context, owner, this, value);
                        }

                    } catch (NumberFormatException e) {
                        logger.fine("unable to parse for double");
                    }
                }

                break;

            case Link:
            case Image:
                if (value.toLowerCase().startsWith("www."))
                    value = "http://" + value;

                if (value.toLowerCase().indexOf("http://") == -1 && value.toLowerCase().indexOf("https://") == -1) {
                    value = "http://" + value;
                }

                new AttributeValue(context, owner, this, value);

                break;

            case Postcode:

                value = value.toUpperCase().trim();

                // test simply for a string match, do not do postcode lookup in db
                if (!new PostcodeValidator().isValid(value)) {
                    break;
                }

                new AttributeValue(context, owner, this, value);

                /*
                 * Update location from postcode
                 */
                if (isLocation()) {

                    logger.fine("[Attribute] attribute is set as location");
                    if (owner instanceof Locatable) {

                        logger.fine("[Attribute] owner is a locatable object");
                        LocationUtil.setLocation(context, (Locatable) owner, value);

                    } else {
                        logger.fine("[Attribute] owner is NOT a locatable object");
                    }
                }

                break;

            case Email:

                new AttributeValue(context, owner, this, value);
                break;

            case Text:

                /*
                 * If a location attribute then we want to change to a matching location
                 */
                if (isLocation()) {

                    if (owner instanceof Locatable) {

                        logger.fine("[Attribute] owner is a locatable object");
                        LocationUtil.setLocation(context, (Locatable) owner, value);
                    }
                }

                new AttributeValue(context, owner, this, value);
                break;
        }

        /*
           * If this attribute is used for member names then re-gen member name
           */
        if (isDisplayName() && owner instanceof Item) {
            ((Item) owner).generateDisplayName();
        }
    }

    public void setWidth(int cols) {
        this.cols = cols;
    }

    public final void setYearRange(int yearRange) {
        this.yearRange = yearRange;
    }

    /**
     * Checks this value is valid for this attribute. Returns true if it is
     */
    public String test(String value) {

        //		logger.fine("[Attribute] testing attribute for value=" + value);

        if (value != null)
            value = value.trim();

        /*
           * If null then check we are allowed a null value
           */
        if (value == null || value.length() == 0) {

            if (isOptional()) {
                logger.fine("[Attribute] null allowed so valid");
                return null;
            } else {

                logger.fine("[Attribute] null not allowed, invalid");
                return "You must complete this field";

            }
        }

        AttributeTest tester = getTester();
        if (tester == null) {
            return null;
        }

        return tester.test(this, value);
    }

    public String validate(String value) {

        String error = null;

        switch (getType()) {

            default:
                break;

            case Email:
                error = new EmailValidator().validate(value);
                break;

            case Postcode:
                error = new PostcodeValidator().validate(value);
                break;

        }

        return error;
    }

    public final SearchControl getSearchControl() {
        return searchControl;
    }

    public final void setSearchControl(SearchControl searchControl) {
        this.searchControl = searchControl;
    }

    public boolean hasSearchControl() {
        return searchControl != null;
    }

    public boolean isDisplayOnSubscriberOnly() {
        return displayOnSubscriberOnly;
    }

    public void setDisplayOnSubscriberOnly(boolean onlyShowOnSubscribed) {
        this.displayOnSubscriberOnly = onlyShowOnSubscribed;
    }

    public void setDublicate(boolean dublicate) {
        this.dublicate = dublicate;
    }

    public boolean isCheckAllOptions() {
        return checkAllOptions;
    }

    public void setCheckAllOptions(boolean checkAllOptions) {
        this.checkAllOptions = checkAllOptions;
    }

    public String getCheckAllFrontLabel() {
        return checkAllFrontLabel == null ? "Any" : checkAllFrontLabel;
    }

    public void setCheckAllFrontLabel(String checkAllFrontLabel) {
        this.checkAllFrontLabel = checkAllFrontLabel;
    }

    public boolean isNoFollow() {
        return Module.NoFollow.enabled(context) && noFollow;
    }

    public void setNoFollow(boolean noFollow) {
        this.noFollow = noFollow;
    }

    public boolean isIbex() {
        return ibex;
    }

    public void setIbex(boolean ibex) {
        this.ibex = ibex;
    }

    public boolean isDateStart() {
        return dateStart;
    }

    public void setDateStart(boolean dateStart) {
        this.dateStart = dateStart;
    }

    public boolean isDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(boolean dateEnd) {
        this.dateEnd = dateEnd;
    }

    public boolean isExternal() {
        return Module.GATrackingOutboundLinks.enabled(context) && this.equals(GoogleSettings.getInstance(context).getExternalLinkAttributeTag());
    }

    public boolean isEmailClick() {
        return Module.GATrackingEmailLinks.enabled(context) && this.equals(GoogleSettings.getInstance(context).getEmailAttributeTag());
    }
}
