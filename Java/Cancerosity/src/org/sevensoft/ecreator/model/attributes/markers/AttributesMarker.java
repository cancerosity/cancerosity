package org.sevensoft.ecreator.model.attributes.markers;

import java.util.List;
import java.util.Map;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.AFrontendRenderer;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Apr 2006 17:32:32
 * 
 * A tag for showing a table of all displayable attributes
 * 
 * Also generates a script if required for hiding the tables that each section is in
 *
 */
public class AttributesMarker implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		boolean script = params.containsKey("script");
		boolean all = params.containsKey("all");

		List<Attribute> attributes = item.getAttributes();

		/*
		 * If no attributes defined then this will be a useless marker :D
		 */
		if (attributes.isEmpty()) {
			return null;
		}

		MultiValueMap<Attribute, String> attributeValues = item.getAttributeValues();

		/*
		 * If no attribute values then this will be a useless marker :D
		 */
		if (attributeValues.isEmpty()) {
			return null;
		}

		/*
		 * Filter out any attributes that are not displayable on the front end.
		 */
		CollectionsUtil.filter(attributes, new Predicate<Attribute>() {

			public boolean accept(Attribute e) {
				return e.isDisplayable();
			}
		});

		/*
		 * If all is set then we want to display all attributes, even ones that have no value.
		 * Otherwise we only want to display attributes that have a value.
		 */
		if (false == all) {
			attributes.retainAll(attributeValues.keySet());
		}

		/*
		 * Now render our attributes
		 */
		AFrontendRenderer adr = new AFrontendRenderer(context, attributes, attributeValues);
		adr.setScript(script);
		adr.setLabelWidth(180);

		return adr;
	}

	public Object getRegex() {
		return "attributes";
	}

}
