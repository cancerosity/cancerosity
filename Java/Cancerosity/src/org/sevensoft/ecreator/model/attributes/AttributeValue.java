package org.sevensoft.ecreator.model.attributes;

import org.sevensoft.ecreator.model.accounts.registration.RegistrationSession;
import org.sevensoft.ecreator.model.bookings.Booking;
import org.sevensoft.ecreator.model.bookings.sessions.BookingSession;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.promotions.Promotion;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.listings.sessions.ListingSession;
import org.sevensoft.ecreator.model.items.reviews.Review;
import org.sevensoft.ecreator.model.marketing.newsletter.Subscriber;
import org.sevensoft.ecreator.model.marketing.sms.SmsRegistration;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * A particular value of an attribute for an item
 * 
 * @author sks 31-Aug-2005 16:56:13
 * 
 */
@Table("attributes_values")
public class AttributeValue extends EntityObject {

	@Index()
	private Promotion			promotion;

	@Index()
	private Subscriber		subscriber;

	/**
	 * The attribute this value is for
	 */
    @Index()
	private Attribute			attribute;

	@Index()
	private Category			category;

	@Index()
	private SmsRegistration		smsRegistration;

	@Index()
	private Booking			booking;

	@Index()
	private BookingSession		bookingSession;

	/**
	 * The value of this attribute
	 */
	@Index()
	private String			value;

	/**
	 * The item owner
	 */
	@Index()
	private Item			item;

	@Index()
	private ListingSession		listingSession;

	@Index()
	private Basket			basket;

	private Order			order;

	/**
	 * Hide attribute values for items that are disabled or categories that are hidden etc.
	 */
	private boolean			hidden;

	@Index()
	private Review			review;

	@Index()
	private RegistrationSession	registrationSession;

	@Index()
	private Search			search;

	public AttributeValue(RequestContext context) {
		super(context);
	}

	public AttributeValue(RequestContext context, Attributable owner, Attribute attribute, String value) {
		super(context);

		if (owner instanceof Item) {
			item = (Item) owner;
		}

		else if (owner instanceof Subscriber) {
			this.subscriber = (Subscriber) owner;
		}

		else if (owner instanceof Category) {
			category = (Category) owner;
		}

		else if (owner instanceof Booking) {
			booking = (Booking) owner;
		}

		else if (owner instanceof Search) {
			search = (Search) owner;
		}

		else if (owner instanceof ListingSession) {
			listingSession = (ListingSession) owner;
		}

		else if (owner instanceof RegistrationSession) {
			registrationSession = (RegistrationSession) owner;
		}

		else if (owner instanceof BookingSession) {
			bookingSession = (BookingSession) owner;
		}

		else if (owner instanceof Review) {
			review = (Review) owner;
		}

		else if (owner instanceof Order) {
			order = (Order) owner;
		}

		else if (owner instanceof Basket) {
			basket = (Basket) owner;
		}

		else if (owner instanceof Promotion) {
			promotion = (Promotion) owner;
		}

		else if (owner instanceof SmsRegistration) {
			smsRegistration = (SmsRegistration) owner;
		}

		else {
			assert false : "Attribute owner not covered";
		}

		this.attribute = attribute;
		this.value = value;

		save();

	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
		save("value");
	}

	public final Attribute getAttribute() {
		return attribute.pop();
	}

	
	public final Item getItem() {
		return item.pop();
	}

}
