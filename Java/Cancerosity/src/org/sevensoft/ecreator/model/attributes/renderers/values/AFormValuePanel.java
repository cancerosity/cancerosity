package org.sevensoft.ecreator.model.attributes.renderers.values;

import java.util.List;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.misc.FieldValueCell;
import org.sevensoft.ecreator.model.attributes.Attributable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Mar 2007 12:15:11
 *
 */
public class AFormValuePanel extends Panel {

	private final List<Attribute>			attributes;
	private MultiValueMap<Attribute, String>	attributeValues;
	private final Attributable			a;
	private int						columns;

	public AFormValuePanel(RequestContext context, Attributable a, List<Attribute> attributes) {
		super(context);

		this.a = a;
		this.attributes = attributes;
		this.attributeValues = a.getAttributeValues();
	}

	@Override
	public void makeString() {

		for (Attribute attribute : attributes) {

			List<String> values = attributeValues.list(attribute);
			if (values != null) {

				AValueRenderer avr = new AValueRenderer(context, a, attribute, values);
				sb.append(new FieldValueCell(attribute.getName(), avr));
			}
		}

	}
}
