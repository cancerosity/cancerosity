package org.sevensoft.ecreator.model.attributes.renderers.input;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeOption;
import org.sevensoft.ecreator.model.items.AjaxItemSearch;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.bool.RadioTag;
import org.sevensoft.jeezy.http.html.form.date.DateTextTag;
import org.sevensoft.jeezy.http.html.form.date.DateTimeTag;
import org.sevensoft.jeezy.http.html.form.date.DatepickerTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.util.Keypad;
import org.sevensoft.jeezy.http.util.Renderer;

/**
 * @author sks 2 Nov 2006 23:13:11
 *
 */
public class AInputRenderer {

	private static final Logger	logger	= Logger.getLogger("ecreator");

	/**
	 *
	 */
	private final Attribute		attribute;

	/**
	 *
	 */
	private final RequestContext	context;

	/**
	 * Current values already set on this attribute
	 */
	private List<String>		values;

	private boolean			multi;

	private String			paramName;

	public AInputRenderer(RequestContext context, Attribute attribute, List<String> values) {

		this.context = context;
		this.attribute = attribute;
		this.values = values;
		this.paramName = attribute.getParamName();

		logger.fine("[AInputRenderer] attribute=" + attribute + ", values=" + values);
	}

    public AInputRenderer(RequestContext context, Attribute attribute, List<String> values, Integer id) {
        this(context, attribute, values);
        this.paramName = attribute.getParamName(id);
    }

    private String association() {

		// get item type of association
		ItemType itemType = attribute.getAssociationItemType();
		if (itemType == null) {
			return null;
		}

		if (values == null) {
			values = new ArrayList();
		}

		if (values.isEmpty() || attribute.isMultiValues()) {
			values.add(null);
		}

		StringBuilder sb = new StringBuilder();

		for (String value : values) {

			// get item for existing value
			Item item = EntityObject.getInstance(context, Item.class, value);

			if (item == null) {

				// get total count for this item type, if we have less than 200 then just show all in a drop down
				int count = SimpleQuery.count(context, Item.class, "status", "live", "itemType", itemType);
				if (count < 200) {

					ItemSearcher searcher = new ItemSearcher(context);
					searcher.setItemType(itemType);
					searcher.setStatus("live");
					searcher.setVisibleOnly(false);
					searcher.setIncludeHidden(true);

					List<Item> items = searcher.getItems();

					SelectTag tag = new SelectTag(context, paramName);
					tag.setAny("-Choose value-");
					for (Item i : items) {
						tag.addOption(i.getId(), "#" + i.getIdString() + " " + i.getName());
					}
					sb.append(tag);

					// otherwise show the ajax search box

				} else {

					sb.append(new AjaxItemSearch(context, paramName));
				}

				// if the existing value matched an item then use that item to populate a select box
			} else {

				SelectTag tag = new SelectTag(context, paramName, item.getIdString());
				tag.setAny("-Remove this value-");
				tag.addOption(item.getId(), "#" + item.getIdString() + " " + item.getName());

				sb.append(tag);
				sb.append("<br/>");
			}
		}

		return sb.toString();
	}

	/**
	 * render boolean as a simple check box
	 */
	private String bool() {

		String value = null;
		if (values != null && values.size() > 0) {
			value = values.get(0);
		}

		boolean checked = "true".equals(value);
		CheckTag tag = new CheckTag(context, paramName, "true", checked);
		return tag.toString();
	}

	/**
	 * Render checkboxes for a multi choice selection attribute
	 */
	private String checkboxes() {

		Keypad keypad = new Keypad(3);
		keypad.setTableClass("attribute_checks");
        if (attribute.isCheckAllOptions()){
           keypad.addObject(null);
        }
		keypad.setObjects(attribute.getOptions());
		keypad.setCellVAlign("top");
		keypad.setRenderer(new Renderer<AttributeOption>() {

			public Object render(AttributeOption option) {
                //extra option
                if (option == null) {

                    CheckTag tag = new CheckTag(context, "check", "");
                    tag.setAlign("absmiddle");
                    tag.setOnClick("function checkAll(checked) {\n" +
                            "   elem=document.getElementsByName('" + paramName + "');\n" +
                            "   for (i=0; i< elem.length; i++){\n" +
                            "       elem[i].checked = checked;\n" +
                            "   }\n" +
                            "}\n" +
                            "checkAll(this.checked);");
                    return tag + " " + attribute.getCheckAllFrontLabel();
                }

				String optionValue = option.getValue();

				boolean checked = values != null && values.contains(optionValue);
				CheckTag tag = new CheckTag(context, paramName, optionValue, checked);
				tag.setAlign("absmiddle");

				if (option.hasRevealIds()) {

					StringBuilder r = new StringBuilder();
					for (int id : option.getRevealIds()) {

						r.append(" e = document.getElementById('attribute_row_" + id + "'); ");
						r.append(" if (e != null) { ");
						r.append("		if (this.checked) e.style.display=''; ");
						r.append(" 	else e.style.display='none'; ");
						r.append(" }");
					}

					tag.setOnClick(r.toString());
				}

				return tag + " " + optionValue;
			}
		});

		return keypad.toString();
	}

	/**
	 * Date should be rendered as a text box
	 */
	private String date() {

		StringBuilder sb = new StringBuilder();

		if (values != null) {

			for (String value : values) {

				//				final DateTag dateTag = new DateTag(context, paramName, Long.parseLong(value));
				//				dateTag.setStartYear(attribute.getStartYear());
				//				dateTag.setYearRange(attribute.getYearRange());

				Date date = new Date(Long.parseLong(value));

//				DateTextTag dateTag = new DateTextTag(context, paramName, date);
                DatepickerTag dateTag = new DatepickerTag(context, paramName, date);
				sb.append(dateTag);
				sb.append("</br>");
			}
		}

		if (values == null || values.isEmpty() || attribute.isMultiValues()) {

//			final DateTextTag dateTag = new DateTextTag(context, paramName);
            final DatepickerTag dateTag = new DatepickerTag(context, paramName);
			//			final DateTag dateTag = new DateTag(context, paramName);
			//			dateTag.setStartYear(attribute.getStartYear());
			//			dateTag.setYearRange(attribute.getYearRange());

			sb.append(dateTag);
		}

		return sb.toString();
	}

	private String daterange() {
		return null;
	}

	private String datetime() {

		StringBuilder sb = new StringBuilder();

		if (values != null) {
			for (String value : values) {
				sb.append(new DateTimeTag(context, paramName, value));
				sb.append("</br>");
			}
		}

		if (values == null || values.isEmpty() || attribute.isMultiValues()) {
			sb.append(new DateTimeTag(context, paramName));
		}

		return sb.toString();
	}

	private String imageRadio() {

		Keypad keypad = new Keypad(3);
		keypad.setTableClass("attribute_images");
		keypad.setObjects(attribute.getOptions());
		keypad.setCellVAlign("top");
		keypad.setCellAlign("center");
		keypad.setRenderer(new Renderer<AttributeOption>() {

			public Object render(AttributeOption option) {

				StringBuilder sb = new StringBuilder();

                if (option.hasAnyImage()) {
                    Img img = option.getAnyImage();
                    if (img != null) {
                        sb.append(img.getThumbnailTag());
                        sb.append("<br/>");
                    }
                }

				String optionValue = option.getValue();

				boolean checked = values != null && values.contains(optionValue);
				RadioTag tag = new RadioTag(context, paramName, optionValue, checked);
				tag.setAlign("absmiddle");

				sb.append(tag + " " + optionValue);

				return sb;
			}
		});

		return keypad.toString();

	}

	private boolean isMulti() {
		return attribute.isMultiValues() || multi;
	}

	private String selection() {

		SelectTag tag = new SelectTag(context, paramName);

		/*
		 * if we have values, then set the first one as the current value.
		 * We can discard the rest, because we should not have multiple values on this selection type
		 * - otherwise it would be rendered as checkboxes
		 */
		if (values != null && !values.isEmpty()) {
			tag.setValue(values.get(0));
		}

		if (attribute.isOptional()) {
			tag.setAny("--");

		} else if (attribute.hasIntro()) {
			tag.setAny(attribute.getIntro());
		}

		MultiValueMap<String, Integer> revealMap = new MultiValueMap(new LinkedHashMap());

		//		boolean reveals = false;
		for (AttributeOption option : attribute.getOptions()) {

			if (option.hasRevealIds()) {
				//				reveals = true;
				revealMap.putAll(option.getValue(), option.getRevealIds());
			}

			tag.addOption(option.getValue());
		}

		StringBuilder sb = new StringBuilder();

		/*
		 * Do reveal javascrips
		 */
		if (revealMap.size() > 0) {



			String functionName = "attributeRevealer" + attribute.getId();
            String functionName2 = "clearAttributesValues()";

			tag.setOnChange(functionName + "(this.options[this.selectedIndex].value); "+functionName2);

            context.setAttribute("attrSection",
                    "var op=this.document.getElementsByName('" + tag.getName() + "')[0].options; \n" +
                    "var e=op[op.selectedIndex].value; \n" +
                    "if (e ==null) e='" + revealMap.keySet().toArray()[0] + "'; \n" +
                    functionName + "(e); \n");

			sb.append("<script>");
			sb.append("function " + functionName + "(value) { ");

			// clear dependant rows
            for (String id : new HashSet<String>(revealMap.keySet())) {
				sb.append(" e = document.getElementById('" + id + "'); if (e != null) e.style.display='none'; \n");
			}
			for (int id : new HashSet<Integer>(revealMap.values())) {
				sb.append(" e = document.getElementById('" + AFormInputPanel.getInputDivId(id) + "'); if (e != null) e.style.display='none';\n ");
			}

			for (Map.Entry<String, List<Integer>> entry : revealMap.entryListSet()) {

				String value = entry.getKey();
				List<Integer> ids = entry.getValue();

				sb.append("if (value == '" + value + "') {");

                sb.append(" e = document.getElementById('" + value + "'); if (e != null) e.style.display=''; ");
                for (int id : ids) {
                    sb.append(" e = document.getElementById('" + AFormInputPanel.getInputDivId(id) + "'); if (e != null) e.style.display=''; ");
                }

				sb.append(" } ");
			}

			sb.append(" } ");
			sb.append("</script>");

            sb.append("<script>");
            sb.append("function " + functionName2 + " { ");


            for (int id : new HashSet<Integer>(revealMap.values())) {
                sb.append(" e = document.getElementsByName('" + AFormInputPanel.getInputName(id) + "')[0]; if (e != null) e.value=''; \n");
            }

            sb.append(" } ");
            sb.append("</script>");

		}

		sb.append(tag);

		return sb.toString();
	}

	public void setMulti(boolean multi) {
		this.multi = multi;
	}

	public final void setParamName(String paramName) {
		this.paramName = paramName;
	}

	private String stepped() {

		StringBuilder sb = new StringBuilder();

		SelectTag tag = new SelectTag(context, paramName);
		if (attribute.isOptional()) {
			tag.setAny("--");
		}

		for (int n = attribute.getMin(); n <= attribute.getMax(); n += attribute.getStep()) {
			tag.addOption(String.valueOf(n));
		}

		if (values != null) {

			for (String value : values) {

				tag.setValue(value);
				sb.append(tag);
			}
		}

		if (values == null || values.isEmpty() || attribute.isMultiValues()) {
			tag.removeValue();
			sb.append(tag);
		}

		return sb.toString();
	}

	/**
	 * Render text boxes, each with the width w
	 */
	private String text(int w, int h) {

		StringBuilder sb = new StringBuilder();

		if (values != null) {

			for (String value : values) {

				if (h == 1) {
					sb.append(new TextTag(context, paramName, value, w));
				} else {
					sb.append(new TextAreaTag(context, paramName, value, w, h));
				}
			}
		}

		if (values == null || values.isEmpty() || isMulti()) {

			if (h == 1) {
				sb.append(new TextTag(context, paramName, w));
			} else {
				sb.append(new TextAreaTag(context, paramName, w, h));
			}

		}

		return sb.toString();
	}

	@Override
	public String toString() {

		switch (attribute.getType()) {

		case Association:
			return association();

		case Boolean:
			return bool();

		case Date:
			return date();

		case DateTime:
			return datetime();

		case Email:
			return text(30, 1);

		case Link:
        case Image:    
			return text(50, 1);

		case Numerical:

			if (attribute.hasStep() && attribute.hasMax()) {
				return stepped();
			} else {
				return text(attribute.getCols(), 1);
			}

		case Postcode:
			return text(12, 1);

		case Selection:

			// if we have images then render the images

			if (isMulti()) {
				return checkboxes();
			} else if (attribute.hasImages()) {
				return imageRadio();
			} else {
				return selection();
			}

		case Text:
			return text(attribute.getCols(), attribute.getRows());

		}

		return null;
	}
}
