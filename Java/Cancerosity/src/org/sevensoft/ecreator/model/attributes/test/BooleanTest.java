package org.sevensoft.ecreator.model.attributes.test;

import org.sevensoft.ecreator.model.attributes.Attribute;

/**
 * @author sks 28 Oct 2006 22:21:07
 *
 */
public class BooleanTest extends AttributeTest {

	@Override
	public String test(Attribute attribute, String value) {

		if ("true".equalsIgnoreCase(value) || "false".equalsIgnoreCase(value)) {
			return null;
		} else {
			return "Please choose valid value";
		}
	}

}
