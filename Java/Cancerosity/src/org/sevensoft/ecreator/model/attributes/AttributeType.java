package org.sevensoft.ecreator.model.attributes;

/**
 * @author sks 31 Aug 2006 13:02:40
 *
 */
public enum AttributeType {

	Association() {
	},

	Date() {

	},

	/**
	 * 
	 */
	DateTime {

		@Override
		public String toString() {
			return "Date & Time";
		}

	},

	/**
	 * 
	 */
	Email() {

	},

	/**
	 * 
	 */
	Link() {

	},

	Numerical() {

		@Override
		public SearchControl[] getSearchControls() {
			return new SearchControl[] { SearchControl.Min, SearchControl.Max, SearchControl.Equals };
		}

	},

	/**
	 * 
	 */
	Postcode() {

	},

	Selection() {

	},

	Text() {

	},

	Boolean() {

		@Override
		public String toString() {
			return "Yes / No";
		}

	},
    Image(){

    };

	public SearchControl[] getSearchControls() {
		return null;
	}

}