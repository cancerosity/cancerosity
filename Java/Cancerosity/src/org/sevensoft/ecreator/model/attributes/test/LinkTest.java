package org.sevensoft.ecreator.model.attributes.test;

import org.sevensoft.ecreator.model.attributes.Attribute;

/**
 * @author sks 28 Oct 2006 22:29:43
 *
 */
public class LinkTest extends AttributeTest {

	@Override
	public String test(Attribute attribute, String value) {

		/*
		 * a few simple checks
		 * 1. must contain a dot
		 * 2. must have www. or http:// or https://
		 */

		if (value.contains(".") && (value.startsWith("http:") || value.startsWith("https:") || value.startsWith("www.")))
			return null;

		return "Enter valid URL";
	}

}
