package org.sevensoft.ecreator.model.attributes;

import java.util.List;

/**
 * @author sks 27-Mar-2006 07:40:28
 *
 */
public interface AttributeOwner {

	/**
	 * Creates a new attribute in this owner with the param name and of the param type
	 */
	public Attribute addAttribute(String name, AttributeType type);

	/**
	 * Adds all these attributes on mass by splitting on new lines from the string
	 */
	public List<Attribute> addAttributes(String newAttributes, String newAttributesSection, int newAttributesPage);

	/**
	 * Returns the first attribute that exactly matches this name
	 */
	public Attribute getAttribute(String name);

	/**
	 * Returns the number of attributes on this owner
	 */
	public int getAttributeCount();

	/**
	 * Returns all attributes defined on this owner
	 */
	public abstract List<Attribute> getAttributes();

	/**
	 * Returns true if this owner has attributes created 
	 */
	public boolean hasAttributes();

	/**
	 * Removes the param attribute from this owner
	 */
	public void removeAttribute(Attribute attribute);

	/**
	 * Updates the count for number of attributes on this owner
	 */
	public void setAttributeCount();

}
