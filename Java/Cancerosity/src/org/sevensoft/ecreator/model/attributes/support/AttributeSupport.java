package org.sevensoft.ecreator.model.attributes.support;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.model.attributes.Attributable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.ecreator.model.attributes.AttributeValue;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 19 Aug 2006 09:54:57
 */
public abstract class AttributeSupport extends EntityObject implements Attributable {

	private transient MultiValueMap<Attribute, String>	attributeValues;

	public AttributeSupport(RequestContext context) {
		super(context);
	}

	public final void addAttributeValue(Attribute attribute, String value) {
		attribute.setValue(this, value);
		attributeValues = null;
	}

	public final void addAttributeValues(Attribute attribute, List<String> values) {

		if (values == null)
			return;

		for (String value : values) {
			addAttributeValue(attribute, value);
		}
	}

	public final void addAttributeValues(Map<Attribute, String> map) {

		if (map != null) {

			for (Map.Entry<Attribute, String> entry : map.entrySet()) {
				addAttributeValue(entry.getKey(), entry.getValue());
			}

		}
	}

	public final void copyAttributeValuesTo(Attributable target) {
		Map<Attribute, String> map = this.getAttributeValues();
		target.addAttributeValues(map);
	}

	@Override
	public synchronized boolean delete() {
		removeAttributeValues();
		return super.delete();
	}

	public final Attribute getAttribute(String name) {
		for (Attribute attribute : getAttributes()) {
			if (attribute.getName().startsWith(name)) {
				return attribute;
			}
		}
		return null;
	}

	/**
	 * Returns a list of attributes set on this page
	 */
	public final List<Attribute> getAttributes(final int page) {
		return AttributeUtil.getAttributes(this, page);
	}

	public final String getAttributeValue(Attribute attribute) {

		// if we try to use a null value in the get it messes up the tree map for some reason (must be the comaprator the tree uses?)
		if (attribute == null) {
			return null;
		}
		return getAttributeValues().get(attribute);
	}

	public String getAttributeValue(int id) {
		for (Attribute attribute : getAttributes()) {
			if (attribute.getId() == id) {
				return getAttributeValue(attribute);
			}
		}
		return null;
	}

	public final String getAttributeValue(String name) {
		return getAttributeValue(name, false);
	}

	/**
	 * Returns first value for this attribute
	 */
	public final String getAttributeValue(String attributeName, boolean exact) {
		return AttributeUtil.getAttributeValue(this, attributeName, exact);
	}

	public final MultiValueMap<Attribute, String> getAttributeValues() {

		// if (attributeValues == null || attributeValues.isEmpty()) {

		attributeValues = new MultiValueMap<Attribute, String>(new TreeMap());
		String column = getClass().getSimpleName();

		Query q = new Query(context, "select * from # where `" + column + "`=? order by value");
		q.setTable(AttributeValue.class);
		q.setParameter(this);

		for (AttributeValue av : q.execute(AttributeValue.class)) {

			Attribute a = av.getAttribute();
			String value = av.getValue();

			if (a != null && value != null) {
				attributeValues.put(a, value);
			}
		}
		// }

		return attributeValues;
	}

	public final List<String> getAttributeValues(Attribute attribute) {
		return getAttributeValues().list(attribute);
	}

    public final Attribute getIbexAttribute() {
        for (Attribute attr : getAttributes()) {
            if (attr.isIbex()) {
                return attr;
            }
        }
        return null;
    }

	public final MultiValueMap<String, Attribute> getSectionsAttributesMap() {

		MultiValueMap<String, Attribute> map = new MultiValueMap(new LinkedHashMap());
		for (Attribute attribute : getAttributes()) {
			map.put(attribute.getSection(), attribute);
		}

		return map;
	}

	public final boolean hasAttributeValue(Attribute attribute) {
		return getAttributeValues().size() > 0;
	}

	public final boolean hasAttributeValue(Attribute attribute, String value) {
		return getAttributeValues(attribute).contains(value);
	}

	public boolean hasAttributeValue(String string) {
		return getAttributeValue(string, false) != null;
	}

	public void insertAttributeValue(Attribute attribute, String value) {
		if (attributeValues == null) {
			attributeValues = new MultiValueMap(new TreeMap());
		}
		this.attributeValues.put(attribute, value);
	}

	public final void moveAttributeValuesTo(Attributable target) {

		// copy attributes first of all
		copyAttributeValuesTo(target);

		// then delete from this
		removeAttributeValues();
	}

	/**
	 * Remove all attribute values
	 */
	public final void removeAttributeValues() {
		SimpleQuery.delete(context, AttributeValue.class, getClass().getSimpleName(), this);
		attributeValues = null;
	}

	/**
	 * Remove all attribute values for this attribute
	 */
	public final void removeAttributeValues(Attribute attribute) {
		SimpleQuery.delete(context, AttributeValue.class, getClass().getSimpleName(), this, "attribute", attribute);
		attributeValues = null;
	}

	public final void removeAttributeValues(Collection<Attribute> attributes) {
		for (Attribute a : attributes) {
			removeAttributeValues(a);
		}
	}

	public final void removeEditableAttributesValues() {
		List<Attribute> attributes = SimpleQuery.execute(context, Attribute.class, "editable", true);
		for (Attribute attribute : attributes) {
			removeAttributeValues(attribute);
		}
	}

	public final void setAttributeValue(Attribute attribute, String value) {
		setAttributeValues(attribute, Collections.singletonList(value));
		attributeValues = null;
	}

	public final void setAttributeValues(Attribute attribute, List<String> values) {

		// remove attribute values then set all new ones
		removeAttributeValues(attribute);

		addAttributeValues(attribute, values);
	}

	public final void setAttributeValues(Collection<Attribute> attributes, Map<Attribute, String> attributeValues) {

		removeAttributeValues(attributes);

		for (Map.Entry<Attribute, String> entry : attributeValues.entrySet()) {
			addAttributeValue(entry.getKey(), entry.getValue());
		}
	}

	public final void setAttributeValues(Map<Attribute, String> map) {

		logger.fine("[AttributeSupport] setting attribute values a=" + this + ", values=" + map);

		removeAttributeValues();
		addAttributeValues(map);
	}

	public final void setEditableAttributeValues(Map<Attribute, String> map) {

		logger.fine("[AttributeSupport] setting attribute values a=" + this + ", values=" + map);

		removeEditableAttributesValues();
		addAttributeValues(map);
	}
}
