package org.sevensoft.ecreator.model.attributes.support;

import java.io.IOException;
import java.io.File;
import java.net.URL;
import java.util.List;

import org.sevensoft.ecreator.model.attachments.*;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;

/**
 * @author sks 21 Jan 2007 13:51:46
 *
 */
public abstract class AttributeImageAttachmentSupport extends AttributeImageSupport implements AttachmentOwner {

	/**
	 * Number of attachments. 
	 */
	protected int	attachmentCount;

	protected AttributeImageAttachmentSupport(RequestContext context) {
		super(context);
	}

	public final boolean acceptedFiletype(String filename) {
		return new AttachmentFilter().accept(filename);
	}

	public final boolean acceptMoreAttachments() {
		return attachmentCount < getAttachmentLimit();
	}

	public final Attachment addAttachment(Attachment attachment) throws AttachmentExistsException, AttachmentTypeException, IOException,
			AttachmentLimitException {
		return new Attachment(context, this, attachment);
	}

    public Attachment addAttachment(File file, String fileName) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException {
        return new Attachment(context, this, file, fileName, false);
    }

	public final Attachment addAttachment(Upload upload, boolean preview) throws IOException, AttachmentLimitException, AttachmentExistsException,
			AttachmentLimitException, AttachmentTypeException {

		return new Attachment(context, this, upload, preview);
	}

	public final Attachment addAttachment(URL url) throws IOException, AttachmentTypeException, AttachmentExistsException, AttachmentLimitException {
		return new Attachment(context, this, url, false);
	}

	public final void copyAttachmentsTo(AttachmentOwner target) throws AttachmentExistsException, AttachmentTypeException, IOException,
			AttachmentLimitException {
		AttachmentUtil.copyAttachmentsTo(this, target, context);
	}

	public final int getAttachmentCount() {
		return attachmentCount;
	}

	public final List<Attachment> getAttachments() {
		return AttachmentUtil.getAttachments(context, this);
	}

    public Attachment getPdfAttachment() {
        List<Attachment> list = getAttachments();
        for (Attachment attachment : list) {
            if (attachment.getFiletype() == Filetype.Pdf) {
                return attachment;
            }
        }
        return null;
    }

    public final boolean hasAttachment(String filename) {
		return AttachmentUtil.hasAttachment(this, filename);
	}

	public final boolean hasAttachments() {
		return attachmentCount > 0;
	}

	public final boolean isAtAttachmentLimit() {
		return AttachmentUtil.isAtAttachmentLimit(this);
	}

	public final void moveAttachmentsTo(AttachmentOwner target) throws AttachmentExistsException, AttachmentTypeException, IOException,
			AttachmentLimitException {
		AttachmentUtil.moveAttachmentsTo(context, this, target);
	}

	public final void removeAttachment(Attachment a) {
		a.delete();
	}

	public final void removeAttachments() {
		AttachmentUtil.removeAttachments(this);
	}

	public final void setAttachmentCount() {
		this.attachmentCount = SimpleQuery.count(context, Attachment.class, getClass().getSimpleName(), this);
	}

}
