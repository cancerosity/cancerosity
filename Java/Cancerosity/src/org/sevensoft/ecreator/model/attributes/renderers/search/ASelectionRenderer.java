package org.sevensoft.ecreator.model.attributes.renderers.search;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.samdate.QuickDate;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeOption;
import org.sevensoft.ecreator.model.attributes.Attribute.DateSearchType;
import org.sevensoft.ecreator.model.search.forms.SearchField.DateMethod;
import org.sevensoft.ecreator.model.search.forms.SearchField.SelectionMethod;
import org.sevensoft.ecreator.model.search.tags.RangeTags;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.date.DateTag;
import org.sevensoft.jeezy.http.html.form.date.DateTimeTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Keypad;
import org.sevensoft.jeezy.http.util.Renderer;

/**
 * @author sks 2 Nov 2006 23:48:09
 * 
 * A renderer used when we want to enter values to search against attributes
 * 
 * When we are searching all options should be optional
 *
 */
public class ASelectionRenderer {

	private static Logger		logger	= Logger.getLogger("ecreator");

	private Attribute			attribute;

	private RequestContext		context;

	/**
	 * The value of the 'any' tag on selections 
	 */
	private String			anyLabel;

	private boolean			optional;

	/**
	 * Width of text box fields
	 */
	private int				width;

	/**
	 * 
	 */
	private final List<String>	values;

	/**
	 * Convenient value for single value
	 */
	private String			value;

	private int				cols;

	/**
	 * If a start year and end year is set then render drop down list of years 
	 */
	private int				startYear;

	/**
	 * 
	 */
	private int				endYear;

	/**
	 * If a year range is set then show that many years starting from the start year (or this year if start year is zero)
	 */
	private int				yearRange;
	private boolean			hideTime;
	private boolean			autoSubmit;
	private List<String>		options;

	/**
	 * If set to true then this renderer will always render as options regardless of what the type really is
	 */
	private boolean			forceSelection;

	private boolean			fixedBands;
	private Collection<String>	ranges;
	private DateSearchType		dateSearchType;
	private SelectionMethod		selectionMethod;
	private DateMethod		dateMethod;
	private boolean			javascriptCheck;
	/**
	 * Parameter used for hte input
	 */
	private String			parameter;

	private boolean			ajax;

	private String			formId;

	public ASelectionRenderer(RequestContext context, Attribute attribute) {
		this(context, attribute, (List) null);
	}

	public ASelectionRenderer(RequestContext context, Attribute attribute, List<String> values) {

		this.context = context;
		this.attribute = attribute;
        this.optional = attribute != null ? attribute.isOptional() : true;

		if (values == null) {
			this.values = null;
		} else if (values.isEmpty()) {
			this.values = null;
		} else {
			this.values = values;
		}

		if (values != null) {
			value = values.get(0);
		} else {
			value = null;
		}

		// give any a default value
        this.anyLabel = attribute != null ? attribute.getAnyLabel() : "Any";
		this.cols = 3;
		this.parameter = attribute.getParamName();
	}

	public ASelectionRenderer(RequestContext context, Attribute attribute, String value) {
		this(context, attribute, Collections.singletonList(value));
	}

    public ASelectionRenderer(RequestContext context, Attribute attribute, List<String> values, Integer id) {
        this(context, attribute, values);
        this.parameter = attribute.getParamName(id);
    }

    private String bool() {

		// render boolean as a selection. No point doing check tags as we have only two options, and we can include any in the drop down anyway
		SelectTag tag = new SelectTag(context, parameter);

		tag.addOption("true", "Yes");
		tag.addOption("false", "No");
		tag.setAny(anyLabel);

		if (value != null) {
			tag.setValue(value);
		}

		return tag.toString();
	}

	private String checkboxes() {

		Keypad keypad = new Keypad(cols);
		keypad.setObjects(attribute.getOptions());
		keypad.setRenderer(new Renderer<AttributeOption>() {

			public Object render(AttributeOption option) {

				boolean checked;
				if (values == null) {
					checked = option.isDefaultForSearch();
				} else {
					checked = values.contains(option.getValue());
				}

				String param = "attributeOptionCheckTag" + option.getId();

				CheckTag checkTag = new CheckTag(context, parameter, option.getValue(), checked);
				checkTag.setId(param);

				String onClick = "var c = document.getElementById('" + param + "'); c.checked = !c.checked; ";

				if (autoSubmit) {
					if (ajax) {
						String submit = " $('#" + formId + "').ajaxSubmit(searchSubmitOptions);";
						checkTag.setOnChange(submit);
						onClick = onClick + submit;
					} else {
						checkTag.setAutoSubmit(true);
						onClick = onClick + " c.form.submit();";
					}
				} else {
					onClick = onClick + " return false;";
				}

				StringBuilder sb = new StringBuilder();
				sb.append(checkTag);
				sb.append(" ");

				LinkTag linkTag = new LinkTag("#", option.getValue());
				linkTag.setOnClick(onClick);
				sb.append(linkTag);

				return sb.toString();
			}

		});

		return keypad.toString();
	}

	private String datetime() {

		DateTime startDateTime = null, endDateTime = null;
		if (values != null) {

			if (values.size() > 0) {
				startDateTime = new DateTime(Long.parseLong(values.get(0)));
			}

			if (values.size() > 1) {
				endDateTime = new DateTime(Long.parseLong(values.get(0)));
			}
		}

		DateTimeTag startTag = new DateTimeTag(context, parameter, startDateTime);
		startTag.setYearRange(yearRange);
		startTag.setStartYear(startYear);
		startTag.setEndYear(endYear);

		switch (getDateSearchType()) {

		case Between:

			DateTimeTag endTag = new DateTimeTag(context, parameter, endDateTime);
			endTag.setStartYear(startYear);
			endTag.setEndYear(endYear);
			endTag.setYearRange(yearRange);

			return startTag + " to " + endTag;

		default:
		case After:

			HiddenTag afterTag = new HiddenTag(parameter, "*");
			return startTag.toString() + afterTag.toString();

		case Before:

			HiddenTag beforeTag = new HiddenTag(parameter, "*");
			return beforeTag.toString() + startTag.toString();

		case On:

			return startTag.toString();
		}

	}

	private String dayMonthYear() {

		Date startDate = null, endDate = null;
		if (values != null) {

			if (values.size() > 0) {
				startDate = new Date(Long.parseLong(values.get(0)));
			}

			if (values.size() > 1) {
				endDate = new Date(Long.parseLong(values.get(0)));
			}
		}

		DateTag startTag = new DateTag(context, parameter, startDate);
		startTag.setYearRange(yearRange);
		startTag.setStartYear(startYear);
		startTag.setOptional(true);

		switch (getDateSearchType()) {

		case Between:

			DateTag endTag = new DateTag(context, parameter, endDate);
			endTag.setYearRange(yearRange);
			endTag.setStartYear(startYear);
			endTag.setOptional(true);

			return startTag + " to " + endTag;

		default:
		case After:

			HiddenTag afterTag = new HiddenTag(parameter, "*");
			return startTag.toString() + afterTag.toString();

		case Before:

			HiddenTag beforeTag = new HiddenTag(parameter, "*");
			return beforeTag.toString() + startTag.toString();

		case On:

			return startTag.toString();
		}

	}

	private String dropdown() {

		SelectTag tag = new SelectTag(context, parameter);
		tag.setWidth(width);

		if (optional) {
			tag.setAny(anyLabel);
		}

		if (value != null) {
			tag.setValue(value);
		}

		if (autoSubmit) {

			if (ajax) {
				tag.setOnChange("$('#" + formId + "').ajaxSubmit(searchSubmitOptions);");
			} else {
				tag.setAutoSubmit(true);
			}
		}

		if (options == null) {

			for (AttributeOption option : attribute.getOptions()) {

				String s = option.getValue();
				tag.addOption(s);

				if (value == null) {
					if (option.isDefaultForSearch()) {
						tag.setValue(option.getValue());
					}
				}
			}

		} else {

			tag.addOptions(options);

		}

		return tag.toString();
	}

	public int getCols() {
		return cols < 1 ? 3 : cols;
	}

	public final DateSearchType getDateSearchType() {
		return dateSearchType == null ? DateSearchType.Between : dateSearchType;
	}

	private String links() {

		String currentValue = context.getParameter(parameter);

		StringBuilder sb = new StringBuilder();
		String hiddenId = "sf" + attribute.getId();
		sb.append(new HiddenTag(parameter, "").setId(hiddenId));

		for (AttributeOption option : attribute.getOptions()) {

			if (option.getValue().equals(currentValue)) {

				sb.append(option.getValue());

			} else {

				sb.append("<a href='#' onclick='document.getElementById(\"" + hiddenId + "\").value=\"" + option.getValue().replace("\"", "")
						+ "\"; document.getElementById(\"" + hiddenId + "\").form.submit()';'>");
				sb.append(option.getValue());
				sb.append("</a>");

			}

			sb.append("<br/>");
		}

		return sb.toString();
	}

	private String quickDates() {

		String paramValue = context.getParameter(parameter);

		QuickDate current = null;
		if (paramValue != null) {
			try {
				current = QuickDate.valueOf(paramValue);
			} catch (RuntimeException e) {
				e.printStackTrace();
			}
		}

		StringBuilder sb = new StringBuilder();
		String hiddenId = "sf" + attribute.getId();
		sb.append(new HiddenTag(parameter, "").setId(hiddenId));

		if (current == null) {

			sb.append("Anytime<br/>");

		} else {

			sb.append("<a href='#' onclick='document.getElementById(\"" + hiddenId + "\").form.submit()';'>Anytime</a><br/>");
		}

		for (QuickDate quickDate : QuickDate.values()) {

			if (quickDate == current) {

				sb.append(quickDate.toString());

			} else {

				sb.append("<a href='#' onclick='document.getElementById(\"" + hiddenId + "\").value=\"" + quickDate.name()
						+ "\"; document.getElementById(\"" + hiddenId + "\").form.submit()';'>");
				sb.append(quickDate.toString());
				sb.append("</a>");

			}

			sb.append("<br/>");
		}

		return sb.toString();
	}

	/**
	 * Render choice ranges - used for numericals
	 */
	private String ranges() {

		RangeTags tag = new RangeTags(context, ranges, parameter, parameter);
		tag.setAutoSubmit(autoSubmit);
		tag.setAjax(ajax, formId);
		tag.setFixedBands(fixedBands);
		tag.setGrouping(attribute.isGrouping());
		tag.setMaxDp(attribute.getMaxDp());
		tag.setMinDp(attribute.getMinDp());
		tag.setOptional(optional);
		tag.setAny(anyLabel);

		return tag.toString();
	}

	private String selection() {

		if (selectionMethod == null) {
			logger.fine("[ASearchRenderer] setting selection style to default");
			selectionMethod = SelectionMethod.DropDown;
		}

		switch (selectionMethod) {

		case Check:
			return checkboxes();

		default:
		case DropDown:
			return dropdown();

		case Link:
			return links();
		}

	}

	public void setAjax(boolean ajax, String formId) {
		this.ajax = ajax;
		this.formId = formId;
	}

	public void setAnyLabel(String any) {
		this.anyLabel = any;
	}

	public void setAutoSubmit(boolean autoSubmit) {
		this.autoSubmit = autoSubmit;
	}

	public void setCols(int cols) {
		this.cols = cols;
	}

	public void setDateMethod(DateMethod dateMethod) {
		this.dateMethod = dateMethod;
	}

	public void setDateSearchType(DateSearchType dateSearchType) {
		this.dateSearchType = dateSearchType;
	}

	public void setEndYear(int endYear) {
		this.endYear = endYear;
	}

	public void setFixedBands(boolean fixedBands) {
		this.fixedBands = fixedBands;
	}

	public void setForceSelection() {
		this.forceSelection = true;
	}

	public void setHideTime(boolean hideTime) {
		this.hideTime = hideTime;
	}

	public void setJavascriptCheck() {
		this.javascriptCheck = true;
	}

	public void setOptional(boolean opt) {
		this.optional = opt;
	}

	/**
	 * Use this list as options instead of all values
	 */
	public void setOptions(List<String> options) {
		this.options = options;
	}

	public void setParameter(String string) {

		this.parameter = string;

		if (!parameter.contains("_")) {
			parameter += "_" + attribute.getIdString();
		}
	}

	public void setRanges(Collection<String> strings) {
		this.ranges = strings;
	}

	public void setSelectionMethod(SelectionMethod selectionStyle) {
		this.selectionMethod = selectionStyle;
	}

	public void setStartYear(int startYear) {
		this.startYear = startYear;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setYearRange(int i) {
		this.yearRange = i;
	}

	private String step() {

		SelectTag tag = new SelectTag(context, parameter);
		tag.setAny(anyLabel);
		if (value != null) {
			tag.setValue(value);
		}

		if (autoSubmit) {
			tag.setAutoSubmit();
		}

		int n = attribute.getMin();
		while (n <= attribute.getMax()) {
			tag.addOption(String.valueOf(n));
			n = n + attribute.getStep();
		}

		return tag.toString();

	}

	private String text(int w) {

		TextTag tag = new TextTag(context, parameter, value, w);
		return tag.toString();
	}

	@Override
	public String toString() {

		logger.fine("[ASearchRenderer] attribute=" + attribute + ", type=" + attribute.getType());

		if (forceSelection) {
			selection();
		}

		switch (attribute.getType()) {

		case Boolean:
			return bool();

		case Date:

			if (dateMethod == null) {
				dateMethod = DateMethod.DayMonthYear;
			}

			switch (dateMethod) {

			case DayMonth:
			default:
				return dayMonthYear();

			case QuickDates:
				return quickDates();
			}

		case DateTime:

			if (hideTime) {
				return dayMonthYear();
			} else {
				return datetime();
			}

		case Email:
			return text(20);

		case Link:
			return text(20);

		case Numerical:

			if (ranges != null && ranges.size() > 2) {

				return ranges();

			} else if (attribute.hasStep() && attribute.getMin() < attribute.getMax() && attribute.getMax() > 0) {

				return step();

			} else {

				return text(attribute.getCols());
			}

		case Postcode:
			return text(12);

		case Selection:
			return selection();

		default:
		case Text:
			return text(width);

		}

	}
}
