package org.sevensoft.ecreator.model.attributes.renderers.values;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.superstrings.formatters.TextFormatter;
import org.sevensoft.ecreator.iface.frontend.search.ItemSearchHandler;
import org.sevensoft.ecreator.model.attributes.Attributable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeOption;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.html.links.EmailTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.ImageTag;

/**
 * @author sks 2 Nov 2006 23:27:08
 * 
 * Renders attribute values for display
 *
 */
public class AValueRenderer {

	private static final Logger	logger	= Logger.getLogger("ecreator");

	private final Attribute		attribute;
	private final RequestContext	context;
	private final List<String>	values;
	private String			dateTimeFormat;
	private String			dateFormat;
	private String			multiValueSep;
	private boolean			stripEmails;
	private Attributable		a;
	private String			with;
	private String			replace;
	private String			replaceNewLines;

	private int				max;
    private Integer itemId;

	public AValueRenderer(RequestContext context, Attributable a, Attribute attribute, List<String> values) {
		this.context = context;
		this.a = a;
		this.attribute = attribute;
		this.values = values;
		this.multiValueSep = ", ";

	}

    public AValueRenderer(RequestContext context, Attributable a, Attribute attribute, List<String> values, int id) {
        this(context, a, attribute, values);
        this.itemId = id;
    }

    private String association() {

		List<String> list = new ArrayList();

		for (String value : values) {

			Item item = EntityObject.getInstance(context, Item.class, value);
			if (item != null) {
				list.add(new LinkTag(item.getUrl(), item.getName()).toString());
			}

		}

		return StringHelper.implode(list, multiValueSep, false);
	}

	/**
	 * booleans are stored as true / false in database so convert from true to yes and false to no
	 */
	private String bool() {

		String string = StringHelper.implode(values, multiValueSep, false);

		string = string.replace("true", "Yes");
		string = string.replace("false", "No");

		return string;
	}

	/**
	 * 
	 */
	private String date() {

		if (dateFormat == null) {
			dateFormat = "dd MMM yyyy";
		}

		List<String> list = new ArrayList();

		// All dates are stored as timestamps, so parse for date and display
		for (String value : values) {

			try {

				Date date = new Date(Long.parseLong(value.trim()));
				list.add(date.toString(dateFormat));

			} catch (NumberFormatException e) {
				e.printStackTrace();
			}

		}

		return StringHelper.implode(list, multiValueSep, false);
	}

	private String datetime() {

		if (dateTimeFormat == null) {
			dateTimeFormat = "dd MMM yyyy HH:mm";
		}

		List<String> list = new ArrayList();

		// All dates are stored as timestamps, so parse for date and display
		for (String value : values) {

			try {

				DateTime date = new DateTime(Long.parseLong(value.trim()));
				list.add(date.toString(dateTimeFormat));

			} catch (NumberFormatException e) {
				e.printStackTrace();
			}

		}

		return StringHelper.implode(list, multiValueSep, false);
	}

	private String email() {

		List<EmailTag> list = new ArrayList();

		// render each email as an email link
		for (String value : values) {

			String text = attribute.getLinkText();
			if (text == null) {
				text = value;
			}

			EmailTag emailTag = new EmailTag(value, text);
			if (attribute.hasSubject()) {

				String subject = attribute.getSubject();
				if (a instanceof Item) {

					Item item = (Item) a;
					subject = subject.replace("[item]", item.getName());
					subject = subject.replace("[id]", item.getIdString());
				}

				emailTag.setSubject(subject);
			}

			emailTag.setCc(attribute.getEmailCc());
			emailTag.setBcc(attribute.getEmailBcc());

            if (attribute.isEmailClick()) {
                emailTag.setOnClick("javascript: _gaq.push(['_trackPageview', '/email/" + value + "']);");
            }
            
			list.add(emailTag);
		}

		return StringHelper.implode(list, multiValueSep, false);
	}

	public final String getReplace() {
		return replace;
	}

	public final String getWith() {
		return with;
	}

	private String link() {

		List<LinkTag> list = new ArrayList();

		for (String value : values) {

            final LinkTag linkTag = new LinkTag(value, attribute.hasLinkText() ? attribute.getLinkText() : value);
			if (attribute.isLinkNewWindow()) {
				linkTag.setTarget("_blank");
			}
            if (attribute.isNoFollow()) {
                linkTag.setRel("nofollow");
            }
            if (attribute.isExternal()) {
                String lnk = value.replaceFirst("http://", "");
                linkTag.setOnClick("recordOutboundLink(this, 'Outbound links', '" + lnk + "');return false;");
            }

            list.add(linkTag);
		}

		return StringHelper.implode(list, multiValueSep, false);
	}

	private String numerical() {

		List<String> list = new ArrayList();

		// render each value with a prefix or suffix if applicable and formatted with separator and dps if applicable
		for (String value : values) {

			try {

				StringBuilder sb = new StringBuilder();

				if (attribute.hasPrefix()) {
					sb.append(attribute.getPrefix());
				}

				// format

				if (attribute.hasFormat()) {

					TextFormatter formatter = new TextFormatter(attribute.getFormat());
					value = formatter.format(value);

				} else {

					NumberFormat format = NumberFormat.getInstance(Company.getInstance(context).getLocale());
					format.setGroupingUsed(attribute.isGrouping());
					format.setMaximumFractionDigits(attribute.getMaxDp());
					format.setMinimumFractionDigits(attribute.getMinDp());

					value = format.format(Double.parseDouble(value));
				}

				sb.append(value);

				if (attribute.hasSuffix()) {
					sb.append(attribute.getSuffix());
				}

				list.add(sb.toString());

			} catch (NumberFormatException e) {
				//	e.printStackTrace();

			}
		}

		return StringHelper.implode(list, multiValueSep, false);
	}

	public void replaceNewLines(String replaceNewLines) {
		this.replaceNewLines = replaceNewLines;
	}

	/**
	 * Need to see if we are showing images / text / both
	 */
	private String selection() {

		List<String> list = new ArrayList();

		for (String value : values) {

			StringBuilder sb = new StringBuilder();

			// get the underlying option
			AttributeOption option = attribute.getOptionFromValue(value);
			if (option == null) {
				continue;
			}

			if (option.isShowValue()) {

				if (attribute.isLinkable()) {

					sb.append(new LinkTag(ItemSearchHandler.class, null, value, attribute.getParamName(), value));

				} else {

					sb.append(value);

				}

			}

			if (option.hasAnyImage()) {

				Img img = option.getAnyImage();
				if (img != null) {
                    ImageTag imgTag = img.getImageTag();
                    imgTag.setTitle(option.getValue());
                    sb.append(imgTag);
				}
			}

			list.add(sb.toString());
		}

		return StringHelper.implode(list, multiValueSep, false);
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public void setDateTimeFormat(String dateTimeFormat) {
		this.dateTimeFormat = dateTimeFormat;
	}

	public void setMultiValueSep(String multiValueSep) {
		this.multiValueSep = multiValueSep;
	}

	public final void setReplace(String replace) {
		this.replace = replace;
	}

	public void setStripEmails(boolean stripEmails) {
		this.stripEmails = stripEmails;
	}

	public final void setWith(String with) {
		this.with = with;
	}

	@Override
	public String toString() {

		if (values == null || values.isEmpty())
			return "";

		switch (attribute.getType()) {

		case Association:
			return association();

		case Boolean:
			return bool();

		case Date:
			return date();

		case DateTime:
			return datetime();

		case Email:
			return email();

		case Link:
			return link();

		case Numerical:
			return numerical();

		case Postcode:
			return values();

		case Selection:
			return selection();

		case Text:
			return values();

        case Image:
            return imagelink();

		}

		return null;
	}

	/**
	 * Simple render the values as displayed in the database
	 */
	private String values() {

		/*
		 * If we are stripping emails from content then strip here
		 */
		List<String> list = new ArrayList();
		for (String value : values) {

			if (stripEmails) {

				value = value.replaceAll("\\s(.*?)@(.*?)\\s", " ");
				logger.fine("[ADisplayRenderer] stripping emails=" + value);
			}

			if (replace != null && with != null) {
				logger.fine("[ADisplayRenderer] replacing replace=" + replace + ", with=" + with);
				value = value.replaceAll(replace, with);
			}

			if (replaceNewLines != null) {
				logger.fine("[ADisplayRenderer] replacing new lines with " + replaceNewLines);
				List<String> strings = StringHelper.explodeStrings(value, "\n");
				value = StringHelper.implode(strings, replaceNewLines, true);
			}

			if (attribute.hasFormat()) {

				TextFormatter formatter = new TextFormatter(attribute.getFormat());
				value = formatter.format(value);

			}

			/*
			 * Replace \n with <br/>
			 */
			value = HtmlHelper.nl2br(value).trim();

			if (max > 0) {
				if (value.length() > max) {
					value = value.substring(0, max);
				}
			}

			if (attribute.isLinkable()) {

				list.add(new LinkTag(ItemSearchHandler.class, null, value, attribute.getParamName(), value).toString());

			} else {

				list.add(value);

			}
		}

		return StringHelper.implode(list, multiValueSep);
	}

    private String imagelink() {

		List<ImageTag> list = new ArrayList();

		// render each email as an email link
		for (String value : values) {

			ImageTag imageTag = new ImageTag(new Link(value));
			list.add(imageTag);
		}

		return StringHelper.implode(list, multiValueSep, false);
	}


	/**
	 * 
	 */
	public void setMax(int max) {
		this.max = max;
	}
}
