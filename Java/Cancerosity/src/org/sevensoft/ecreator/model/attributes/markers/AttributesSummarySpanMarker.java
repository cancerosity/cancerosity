package org.sevensoft.ecreator.model.attributes.markers;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class AttributesSummarySpanMarker extends AbstractAttributesMarker {

	public AttributesSummarySpanMarker() {
		super(false, true);
	}

	public Object getRegex() {
		return "attributes_summary_span";
	}

}
