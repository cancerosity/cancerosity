package org.sevensoft.ecreator.model.attributes.renderers;


/**
 * @author sks 30 Nov 2006 20:09:30
 *
 */
public enum ARenderType {

	Input, Search, Value
}
