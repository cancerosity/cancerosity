package org.sevensoft.ecreator.model.attributes.test;

import org.sevensoft.commons.validators.text.PostcodeValidator;
import org.sevensoft.ecreator.model.attributes.Attribute;

/**
 * @author sks 28 Oct 2006 22:21:31
 *
 */
public class PostcodeTest extends AttributeTest {

	@Override
	public String test(Attribute attribute, String value) {
		return new PostcodeValidator().validate(value);
	}

}
