package org.sevensoft.ecreator.model.feeds;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.simpleio.SimpleFtp;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategorySettings;
import org.sevensoft.ecreator.model.ecom.suppliers.Supplier;
import org.sevensoft.ecreator.model.feeds.categories.CategoryReference;
import org.sevensoft.ecreator.model.feeds.categories.GenericCategory;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 2 Aug 2006 18:36:07
 *
 */
public abstract class ImportFeed extends Feed {

	/**
	 * delete all existing data
	 */
	private boolean					deleteExpired;

	private transient List<Category>		genericCategoriesCache;

	/**
	 * An iteration indicator that increments each time this feed runs.
	 * This way we can delete all items from the last iteration
	 */
	private int						feedCount;

	/**
	 * Reset name field when feed re-runs
	 */
	private boolean					resetName;

	/**
	 * Supplier to assign SKUs to when importing for products
	 */
	private Supplier					supplier;

	/**
	 * Root is where new categories are created and categories are looked up from.
	 */
	private Category					root;

    private boolean createCategories;

    protected transient Map<String, Category>	categoriesLookupCache	= new HashMap();

	protected ImportFeed(RequestContext context) {
		super(context);
	}

	protected ImportFeed(RequestContext context, boolean create) {
		super(context, create);
	}

	/**
	 * Create a new generic category to import to
	 */
	public void addGenericCategory(RequestContext context, Category category) {

		new GenericCategory(context, this, category);
		genericCategoriesCache = null;
	}

	/**
	 * Creates a new item of type item type, and with name name.
	 * Will set initial state to Live.
	 * 
	 * This method will set the feed src and feed count and will add in any generic categories.
	 */
	public Item createItem(ItemType itemType, String name) {

		Item item = new Item(context, itemType, name, "Live", null);
		item.setFeedSrc(getFullId());
		item.setFeedCount(getFeedCount());
		item.log("Created from '" + getName() + "' feed");

		logger.fine("[ImportFeed] setting feed source details src=" + getFullId() + ", count=" + getFeedCount());

		if (!getGenericCategories().isEmpty()) {

			logger.fine("[ImportFeed] Adding generic categories: " + getGenericCategories());
			item.addCategories(getGenericCategories());
			item.log("Adding to categories: " + item.getCategoryNames());
		}

		return item;
	}

	public int deleteExpired() {

		int deleted = 0;

		// delete all items that were not updated on our last feed run
		ItemSearcher searcher = new ItemSearcher(context);
		searcher.setFeedCountBelow(getFeedCount() - 1);
		searcher.setFeedSrc(getFullId());

		for (Item item : searcher) {

			logger.fine("[ImportFeed] deleted item=" + item);
			item.delete();
			deleted++;
		}

		logger.fine("[ImportFeed] deleted " + deleted + " items");
		return deleted;
	}

	/**
	 *  Searches for a category with this reference
	 *  If the category does not exist then it will create it using the real name but stores a mapping using the reference name
	 */
	public Category getCategory(String referenceName, String realName) {

		// ****** 19/03/07 TURNING CACHE OFF AS TWO CATEGORIES WITH SAME NAME WILL GET COMBINED **********

		// first check our cache
		Category category = categoriesLookupCache.get(referenceName);
		if (category != null) {
			logger.fine("[ImportFeed] found category in cache=" + category);
			return category;
		}

		// check our category mappings for this reference
		category = getCategoryFromReference(referenceName);

		/* 
		 * if category does not exist then lookup by real name with respect to the root in case we already have another category with same name
		 * created by say another feed or manually, because some category names are v.common
		 */
		if (category == null) {
			category = getRoot().getChild(realName);
		}

		// if still null then create this as a new category under the root and map it to the feed
		if (category == null) {

			logger.fine("[ImportFeed] Creating category: " + referenceName);

			category = new Category(context, realName, getRoot());

			// set reference mapping
			new CategoryReference(context, this, category, referenceName);

			// default to 25% markup for sell price
			category.setSellPrice(null, 1, Amount.parse("25%"));
		}

		// add to cache
		categoriesLookupCache.put(referenceName, category);

		return category;
	}

    /**
     * Searches for a category with this reference and parent category name
     * If the category does not exist then it will create it using the real name but stores a mapping using the reference name
     */
    public Category getCategory(String referenceName, String realName, Category parebtCategory) {

        // first check our cache
        Category category = categoriesLookupCache.get(referenceName);
        if (category != null && category.getParent().getName().equals(parebtCategory.getName())) {
            logger.fine("[ImportFeed] found category in cache=" + category);
            return category;
        }

        // check our category mappings for this reference
        category = getCategoryFromReference(referenceName, parebtCategory);

        /*
           * if category does not exist then lookup by real name with respect to the root in case we already have another category with same name
           * created by say another feed or manually, because some category names are v.common
           */
        if (category == null) {
            category = getRoot().getChild(realName);
        }

        // if still null then create this as a new category under the root and map it to the feed
        if (category == null || !category.getParent().getName().equals(parebtCategory.getName())) {

            logger.fine("[ImportFeed] Creating category: " + referenceName);

            category = new Category(context, realName, getRoot());

            // set reference mapping
            new CategoryReference(context, this, category, referenceName);

            // default to 25% markup for sell price
            category.setSellPrice(null, 1, Amount.parse("25%"));
        }

        // add to cache
        categoriesLookupCache.put(referenceName, category);

        return category;
	}

    /**
	 * Returns the real category for this raw category name or null if this raw name has never been mapped to a category. 
	 */
	private Category getCategoryFromReference(String referenceName) {

		Query q = new Query(context, "select c.* from # c join # cm on c.id=cm.category where cm.feed=? and cm.name=?");
		q.setTable(Category.class);
		q.setTable(CategoryReference.class);
		q.setParameter(getFullId());
		q.setParameter(referenceName);
		return q.get(Category.class);
	}

    private Category getCategoryFromReference(String referenceName, Category parentCategory) {

        Query q = new Query(context, "select c.* from # c join # cm on c.id=cm.category where cm.feed=? and cm.name=? and " +
                "c.parent=?");
        q.setTable(Category.class);
        q.setTable(CategoryReference.class);
        q.setParameter(getFullId());
        q.setParameter(referenceName);
        q.setParameter(parentCategory);
        return q.get(Category.class);
    }

    public List<CategoryReference> getCategoryReferences() {
		Query q = new Query(context, "select * from # where feed=? order by name");
		q.setTable(CategoryReference.class);
		q.setParameter(getFullId());
		return q.execute(CategoryReference.class);
	}

	public final int getFeedCount() {
		return feedCount;
	}

	/**
	 *  Get the generic categories new items should be added to
	 */
	public List<Category> getGenericCategories() {

		logger.fine("[ImportFeed] getting generic categories cache=" + genericCategoriesCache);

		if (genericCategoriesCache == null) {

			Query q = new Query(context, "select c.* from # c join # gc on c.id=gc.category where gc.feed=?");
			q.setTable(Category.class);
			q.setTable(GenericCategory.class);
			q.setParameter(getFullId());

			genericCategoriesCache = q.execute(Category.class);
			logger.fine("[ImportFeed] built cache=" + genericCategoriesCache);

		}

		return genericCategoriesCache;
	}

	protected File getInputFile() throws IOException {

		try {

			if (httpUrl != null) {

				logger.fine("[ImportFeed] getting file from http url=" + httpUrl + ", method=" + httpMethod);

				HttpClient hc = new HttpClient(new URL(httpUrl), getHttpMethod());
				hc.setResponseToFile();
				hc.connect();

				return hc.getResponseFile();

			} else if (ftpHostname != null) {

				logger.fine("[ImportFeed] getting file from ftp hostname=" + ftpHostname + ", username=" + ftpUsername + ", path=" + ftpPath +
						", filename=" + ftpFilename);
				File file = SimpleFtp.retrieveFile(ftpHostname, getFtpPort(), ftpUsername, ftpPassword, ftpPath, ftpFilename, null);
				logger.fine("[ImportFeed] file size=" + file.length());
				return file;

			} else {

				logger.fine("[ImportFeed] neither ftp nor http");
				return null;
			}

		} catch (IOException e) {
			logger.warning(e.toString());
			throw e;
		}

	}

	public final Category getRoot() {

		if (root == null) {

			final CategorySettings cs = CategorySettings.getInstance(context);
			cs.setHidden(true);
			cs.save();

			root = Category.getByName(context, "new feed categories");
			if (root == null) {

				root = new Category(context, "new feed categories", null);
				root.setHidden(true);
				root.save();

				return root;
			}
		}

		return root.pop();
	}

	/**
	 * We must always have a supplier assigned to an feed
	 */
	public final Supplier getSupplier(boolean create) {

		if (supplier == null && create) {
			supplier = new Supplier(context, getName());
			save();
		}

		return (Supplier) (supplier == null ? null : supplier.pop());
	}

	public final String getValue() {
		return getIdString();
	}

	protected final Supplier initSupplier(String string) {

		if (supplier == null) {
			supplier = new Supplier(context, string);
			save();
		}
		return supplier.pop();
	}

	public final boolean isDeleteExpired() {
		return deleteExpired;
	}

	public boolean isIgnored(String name) {
		return getIgnoredCategories().contains(name.toLowerCase());
	}

	public final boolean isResetName() {
		return resetName;
	}

    public final boolean isCraeteCategories() {
        return createCategories;
    }

    public void removeGenericCategory(Category category) {

		SimpleQuery.delete(context, GenericCategory.class, "feed", getFullId(), "category", category);
		genericCategoriesCache = null;
	}

	public void setDeleteExpired(boolean deleteOnRun) {
		this.deleteExpired = deleteOnRun;
	}

	public final void setResetName(boolean resetName) {
		this.resetName = resetName;
	}

    public final void setCreateCategories(boolean createCategories) {
        this.createCategories = createCategories;
    }

    public final void setRoot(Category root) {
		this.root = root;
	}

	public final void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

    protected void updateFeedCount() {

		feedCount++;
		save("feedCount");

		logger.fine("[ImportFeed] updating feed count: " + feedCount);
		setLastRuntime();
	}

}
