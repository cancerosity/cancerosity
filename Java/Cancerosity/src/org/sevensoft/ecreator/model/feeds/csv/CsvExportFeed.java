package org.sevensoft.ecreator.model.feeds.csv;

import org.sevensoft.ecreator.model.feeds.ExportFeed;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.misc.csv.CsvImporter;
import org.sevensoft.ecreator.model.misc.csv.CsvUtil;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.iface.admin.feeds.csv.CsvExportFeedHandler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.jdom.JDOMException;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.enterprisedt.net.ftp.FTPException;
import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.CsvSpec;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
@Table("feeds_custom_export")
@Label("CSV Export")
@HandlerClass(CsvExportFeedHandler.class)
public class CsvExportFeed extends ExportFeed implements CsvImporter {

    public enum Type {
        Items,
    }

    /**
     * The type of feed
     */
    private Type type;

    /**
     * Our separator per field
     */
    private String separator;
    /**
     * Quote character used to enclose fields that contain a sep character
     */
    private String quote;

    /**
     * The character used for escaping quote characters inside already quoted text. EXCEL uses quote twice
     */
    private String escape;
    private ItemType itemType;
    private String endLine;

    protected CsvExportFeed(RequestContext context) {
        super(context);
    }

    public CsvExportFeed(RequestContext context, boolean create) {
        super(context, create);

        CsvManager csvman = new CsvManager();
        CsvSpec csvspec = csvman.getCsvSpec();

        separator = csvspec.getSeparator();
        quote = String.valueOf(csvspec.getQuote());
        escape = String.valueOf(csvspec.getEscape());
        endLine = "\n";

        save();
    }

    public final ItemType getItemType() {
        return (ItemType) (itemType == null ? null : itemType.pop());
    }

    public boolean setItemType(ItemType itemType) {
        if (ObjectUtil.equal(this.itemType, itemType)) {
            return false;
        }

        this.itemType = itemType;
        removeAttributeFields();

        return true;
    }

    public Type getType() {
        return type;
    }

    public String getEndLine() {
        return endLine;
    }

    public void setEndLine(String endLine) {
        this.endLine = endLine;
    }

    private void removeAttributeFields() {
        Query q = new Query(context, "delete from # where exFeed=? and attribute>0");
        q.setTable(CsvImportFeedField.class);
        q.setParameter(this);
        q.run();
    }

    public void removeAttribute(Attribute attribute) {
        for (CsvImportFeedField field : getFields()) {
            if (attribute.equals(field.getAttribute())) {
                removeField(field);
            }
        }
    }

    public void removeField(CsvImportFeedField field) {
        field.delete();
    }

    public FeedResult run(File file) throws IOException, FTPException, FeedException, JDOMException {
        return null;
    }


    public CsvManager getCsvManager() {
        return CsvUtil.getCsvManager(this);
    }

    public String getEscape() {
        return escape;
    }

    public String getQuote() {
        return quote;
    }

    public String getSeparator() {
        return separator;
    }

    public int getStartRow() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setEscape(String escape) {
        this.escape = escape;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public void setSeparator(String separator) {
        this.separator = separator;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setStartRow(int startRow) {
//To change body of implemented methods use File | Settings | File Templates.
    }

    public List<CsvImportFeedField> getFields() {
        return SimpleQuery.execute(context, CsvImportFeedField.class, "exFeed", this);
    }

    public CsvImportFeedField addField() {
        int nextPosition = new Query(context, "select max(position) from # where exFeed=?").setTable(CsvImportFeedField.class).setParameter(this).getInt() + 1;
        CsvImportFeedField field = new CsvImportFeedField(context, this);
        field.setPosition(nextPosition);
        field.setFieldType(FieldType.Name);
        field.save();
        return field;
    }

}
