package org.sevensoft.ecreator.model.feeds.spiders;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.httpc.HttpMethod;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 5 Oct 2006 10:35:09
 *
 */
public class EbayStoreSpider extends Spider {

	private void doCategory(RequestContext context, ItemType itemType, Category category, String url) throws IOException {

		HttpClient hc = new HttpClient(url, HttpMethod.Get);
		hc.connect();

		String response = hc.getResponseString();

		Pattern pattern = Pattern.compile("<h3 class=\"ens fontnormal\"><a href=\"(.*?)\">(.*?)</a></h3>", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(response);

		while (matcher.find()) {

			String itemUrl = matcher.group(1);
			doItem(context, itemType, category, itemUrl);

		}
	}

	private Item doItem(RequestContext context, ItemType itemType, Category category, String url) throws IOException {

		logger.fine("spidering for item url=" + url);

		HttpClient hc = new HttpClient(url, HttpMethod.Get);
		hc.connect();

		String page = hc.getResponseString();

		String description = null, price = null, name = null, imageUrl = null;

		Pattern namePattern = Pattern.compile("<h1 class=\"itemTitle\">([^<>]+?)</h1>", Pattern.CASE_INSENSITIVE);
		Matcher matcher = namePattern.matcher(page);
		if (matcher.find()) {

			name = matcher.group(1);
			logger.fine("[ebay spider] name found=" + name);
		}

		// check this item doesn't already exist
		if (new Query(context, "select count(*) from # where name like ?").setTable(Item.class).setParameter(name).getInt() > 0) {
			logger.fine("item already exists with same name, exiting");
			return null;
		}

		Pattern pricePattern = Pattern.compile("price:.*?</span></td><td nowrap=\"yes\"><b>(.*?)</b>", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		matcher = pricePattern.matcher(page);
		if (matcher.find()) {
			logger.fine("[ebay spider] price matcher=" + matcher.group(0));
			price = matcher.group(1);
			logger.fine("[ebay spider] price=" + price);
			price = price.replace("[^\\d.]", "");
			logger.fine("tidied price: " + price);
		}

		Pattern descriptionPattern = Pattern.compile("<!-- Begin Description -->(.+?)<!-- End Description -->", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		matcher = descriptionPattern.matcher(page);
		if (matcher.find())
			description = matcher.group(1);

		Pattern imagePattern = Pattern.compile("<img src=\"(.+?)\" name=\"eBayBig\"", Pattern.CASE_INSENSITIVE);
		matcher = imagePattern.matcher(page);
		if (matcher.find()) {

			imageUrl = matcher.group(1);
			logger.fine("[ebay spider] imageUrl=" + imageUrl);
		}

		Item item = new Item(context, itemType, name, "Live", null);

		if (description != null)
			item.setContent(description);

		if (price != null)
			item.setSellPrice(null, 1, Amount.parse(price));

		item.save();

		item.addCategory(category);
		logger.fine("added category: " + category);

		try {
			if (imageUrl != null)
				item.addImage(new URL(imageUrl), true, true, false);
		} catch (ImageLimitException e) {
			e.printStackTrace();
			logger.fine(e.toString());
		}

		return item;
	}

	@Override
	public String getName() {
		return "Ebay stores";
	}

	@Override
	public int spider(RequestContext context, ItemType itemType, String url, Category category) throws MalformedURLException, IOException {

		int created = 0;

		logger.fine("running ebay spider url=" + url);

		HttpClient hc = new HttpClient(url, HttpMethod.Get);
		hc.connect();

		String response = hc.getResponseString();

		Pattern pattern = Pattern.compile("<td class=\"category\"><b><a href=\"(.*?)\">(.*?)</a></b>");
		Matcher matcher = pattern.matcher(response);

		while (matcher.find()) {

			String caturl = url + matcher.group(1);
			String name = matcher.group(2);

			Category c = Category.getByName(context, name);
			if (c == null) {
				c = new Category(context, name, null);
			}

			doCategory(context, itemType, c, caturl);
		}
		
		return created;
	}
}
