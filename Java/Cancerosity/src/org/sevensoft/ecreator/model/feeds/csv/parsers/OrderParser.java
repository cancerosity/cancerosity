package org.sevensoft.ecreator.model.feeds.csv.parsers;

import java.util.List;

import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.feeds.csv.CsvImportFeed;
import org.sevensoft.ecreator.model.feeds.csv.CsvImportFeedField;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

import com.ricebridge.csvman.BadLine;
import com.ricebridge.csvman.LineListenerSupport;

/**
 * @author sks 24 Sep 2006 15:26:26
 *
 */
public class OrderParser extends LineListenerSupport {

	private int					processed;
	private final CsvImportFeed			feed;
	private final RequestContext		context;
	private final List<CsvImportFeedField>	fields;

	public OrderParser(final RequestContext context, CsvImportFeed feed) {
		this.context = context;
		this.feed = feed;
		this.fields = feed.getFields();
	}

	@Override
	public BadLine addLineImpl(String[] row, int arg1, long arg2, String arg3) throws Exception {

		processed++;

		String orderId = feed.getIdValue(row);
		String ref = feed.getRefValue(row);

		Order order = null;
		if (orderId != null)
			order = EntityObject.getInstance(context, Order.class, orderId);

		if (order == null && ref != null)
			order = Order.getByReference(context, ref);

		if (order == null)
			return null;

		// set reference
		if (ref != null)
			order.setReference(ref);

		for (CsvImportFeedField field : fields) {
			field.process(order, row);
		}

		return null;
	}

	public int getProcessed() {
		return processed;
	}

}
