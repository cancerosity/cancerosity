package org.sevensoft.ecreator.model.feeds.sykescottages;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.samdate.Date;
import org.jdom.Element;

import java.util.List;
import java.text.ParseException;

/**
 * User: Tanya
 * Date: 10.08.2011
 */
public class Cottage {
    private Element element;

    public Cottage(Element element) {
        this.element = element;
    }

    /**
     * <carea>
     * AttributeType.Text
     * '<carea>Scotland</carea>'
     */
    private String area;

    public String getArea() {
        return element.getChildTextTrim("carea");
    }

    /**
     * <csubarea>
     * AttributeType.Text
     * '<csubarea>Scotland - Ayrshire and Dumfries & Galloway</csubarea>'
     */
    private String subarea;

    public String getSubarea() {
        return element.getChildTextTrim("csubarea");
    }

    /**
     * <nlatitude>
     * AttributeType.Text
     * '<nlatitude>54.9419188778</nlatitude>'
     */
    private String latitude;

    public String getLatitude() {
        return element.getChildTextTrim("nlatitude");
    }

    /**
     * <nlongitude>
     * AttributeType.Text
     * '<nlongitude>-4.4669003429</nlongitude>'
     */
    private String longitude;

    public String getLongitude() {
        return element.getChildTextTrim("nlongitude");
    }

    /**
     * <cpriceinfo>
     * AttributeType.TextArea
     * '<cpriceinfo>All prices are inclusive of the booking fee.<BR><BR>
     * Winter Short Breaks (WSB) are available at many properties in the "Winter" period -
     * subject to availability. Extra nights are charged pro rata of the weekly rental.<BR><BR>
     * Off Season Breaks (OSB) are available at many properties in parts of the "Low" and "Mid" periods -
     * subject to availability. The price for a 3 night break is either 75% of the normal weekly rental or,
     * if it is higher, the quoted Winter Short Break price. Extra nights are charged pro rata of the weekly rental.
     * OSBs can be booked within a calendar month of the start date of the holiday.<BR><BR>
     * "Last Minute Breaks" (LMB) are available at most properties throughout the "LOW", "MID", "HIGH", "PEAK",
     * "XMAS" and "NEW YEAR" periods. The price for a 3 night break is either 65% of the normal weekly rental or,
     * if it is higher, the quoted Winter Short Break price. Extra nights are charged pro rata of the weekly rental.
     * LMBs can be booked within one week of the start date of the holiday.</cpriceinfo>'
     */
    private String priceinfo;

    public String getPriceinfo() {
        return element.getChildTextTrim("cpriceinfo");
    }

    /**
     * <nminprice>
     * AttributeType.Text
     * '<nminprice>169</nminprice>'
     */
    private String minprice;

    public String getMinprice() {
        return element.getChildTextTrim("nminprice");       //todo not attribute
    }

    /**
     * <nmaxprice>
     * AttributeType.Text
     * '<nmaxprice>427</nmaxprice>'
     */
    private String maxprice;

    public String getMaxprice() {
        return element.getChildTextTrim("nmaxprice");     //todo not attribute
    }

    /**
     * <nbedrooms>
     * AttributeType.Text
     * '<nbedrooms>1</nbedrooms>'
     */
    private String bedrooms;

    public String getBedrooms() {
        return element.getChildTextTrim("nbedrooms");
    }

    /**
     * <lkettle>
     * AttributeType.Boolean
     * '<lkettle>1</lkettle>'
     */
    private boolean kettle;

    public String isKettle() {
        return "0".equals(element.getChildTextTrim("lkettle")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <lparkingspace>
     * AttributeType.Boolean
     * <lparkingspace>1</lparkingspace>
     */
    private boolean parkingspace;

    public String isParkingspace() {
        return "0".equals(element.getChildTextTrim("lparkingspace")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <lsmoking>
     * AttributeType.Boolean
     * <lsmoking>0</lsmoking>
     */
    private boolean smoking;

    public String isSmoking() {
        return "0".equals(element.getChildTextTrim("lsmoking")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <nsinglebeds>
     * AttributeType.Text
     * <nsinglebeds>0</nsinglebeds>
     */
    private String singlebeds;

    public String getSinglebeds() {
        return element.getChildTextTrim("nsinglebeds");
    }

    /**
     * <ndoublebeds>
     * AttributeType.Text
     * <ndoublebeds>0</ndoublebeds>
     */
    private String doublebeds;

    public String getDoublebeds() {
        return element.getChildTextTrim("ndoublebeds");
    }

    /**
     * <ntwinbeds>
     * AttributeType.Text  //todo Text or Boolean    . Text because of Nthwinbeds - number
     * <ntwinbeds>1</ntwinbeds>
     */
    private String twinbeds;

    public String getTwinbeds() {
        return element.getChildTextTrim("ntwinbeds");
    }

    /**
     * <nbunkbeds>
     * AttributeType.Text  //todo Text or Boolean
     * <nbunkbeds>0</nbunkbeds>
     */
    private String bunkbeds;

    public String getBunkbeds() {
        return element.getChildTextTrim("nbunkbeds");
    }

    /**
     * <nfamilybeds>
     * AttributeType.Text  //todo Text or Boolean
     * <nfamilybeds>0</nfamilybeds>
     */
    private String familybeds;

    public String getFamilybeds() {
        return element.getChildTextTrim("nfamilybeds");
    }

    /**
     * <nmaxsleeps>
     * AttributeType.Text
     * <nmaxsleeps>2</nmaxsleeps>
     */
    private String maxsleeps;

    public String getMaxsleeps() {
        return element.getChildTextTrim("nmaxsleeps");
    }

    /**
     * <cproppostcode>
     * AttributeType.Postcode
     * <cproppostcode>DG8 6AU</cproppostcode>
     */
    private String proppostcode;

    public String getProppostcode() {
        return element.getChildTextTrim("cproppostcode");
    }

    /**
     * <cpropfeedback>
     * AttributeType.TextArea
     * <cpropfeedback>
     * The views from the picture window were stunning. The traffic free walk into Newton Stewart was brilliant.<BR>Mr Patterson, Widnes, Cheshire<BR>June 2011<BR><BR>,Views, location, garden, nice area, peaceful and dog friendly.<BR>Mr Douglas, Abbots Langley, Hertfordshire<BR>June 2011<BR><BR>,This was our second visit as we had such a lovely break last time. An extremely comfortable cottage in quiet, peaceful setting with amazing views. We couldn't ask for more.<BR>Mrs Lawson, Darlington, Durham<BR>May 2011<BR><BR>,The location of this property made the holiday perfect. Although the farmhouse is so very private and quiet, it is in easy distance of local places to visit. I loved visiting the sculptures and forests and having a dog made this an ideal place for me to visit. Quite wonderful gardens to the property meant sitting out for breakfast a real treat. Sunrise and sunsets were beautiful.<BR>Miss Walker, Harrogate, North Yorkshire<BR>April 2011<BR><BR>
     * </cpropfeedback>
     */
    private String propfeedback;

    public String getPropfeedback() {
        return element.getChildTextTrim("cpropfeedback");
    }

    /**
     * <ltenniscourt>
     * AttributeType.Boolean
     * <ltenniscourt>0</ltenniscourt>
     */
    private boolean tenniscourt;

    public String isTenniscourt() {
        return "0".equals(element.getChildTextTrim("ltenniscourt")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <lpets>
     * AttributeType .Boolean
     * <lpets>1</lpets>
     */
    private boolean pets;

    public String isPets() {
        return "0".equals(element.getChildTextTrim("lpets")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <cchangeoverday>
     * AttributeType .Selection week days
     * <cchangeoverday>Fri</cchangeoverday>
     */
    private String changeoverday;

    public String getChangeoverday() {
        return element.getChildTextTrim("cchangeoverday");
    }

    /**
     * <ljacuzzi>
     * AttributeType .Boolean
     * <ljacuzzi>0</ljacuzzi>
     */
    private boolean jacuzzi;

    public String isJacuzzi() {
        return "0".equals(element.getChildTextTrim("ljacuzzi")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <ltelephone>
     * AttributeType .Boolean
     * <ltelephone>0</ltelephone>
     */
    private boolean telephone;

    public String isTelephone() {
        return "0".equals(element.getChildTextTrim("ltelephone")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <lsauna>
     * AttributeType .Boolean
     * <lsauna>0</lsauna>
     */
    private boolean sauna;

    public String isSauna() {
        return "0".equals(element.getChildTextTrim("lsauna")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <llinenprovided>
     * AttributeType .Boolean
     * <llinenprovided>1</llinenprovided>
     */
    private boolean linenprovided;

    public String isLinenprovided() {
        return "0".equals(element.getChildTextTrim("llinenprovided")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <ltowelsprovided>
     * AttributeType .Boolean
     * <ltowelsprovided>1</ltowelsprovided>
     */
    private boolean towelsprovided;

    public String isTowelsprovided() {
        return "0".equals(element.getChildTextTrim("boolean")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <ldishwasher>
     * AttributeType .Boolean
     * <ldishwasher>0</ldishwasher>
     */
    private boolean dishwasher;

    public String isDishwasher() {
        return "0".equals(element.getChildTextTrim("ldishwasher")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <lcooker>
     * AttributeType .Boolean
     * <lcooker>1</lcooker>
     */
    private boolean cooker;

    public String isCooker() {
        return "0".equals(element.getChildTextTrim("lcooker")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <lfridge>
     * AttributeType .Boolean
     * <lfridge>1</lfridge>
     */
    private boolean fridge;

    public String isFridge() {
        return "0".equals(element.getChildTextTrim("lfridge")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <lwashingmachine>
     * AttributeType .Boolean
     * <lwashingmachine>0</lwashingmachine>
     */
    private boolean washingmachine;

    public String isWashingmachine() {
        return "0".equals(element.getChildTextTrim("lwashingmachine")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <ltv>
     * AttributeType .Boolean
     * <ltv>1</ltv>
     */
    private boolean tv;

    public String isTv() {
        return "0".equals(element.getChildTextTrim("ltv")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <lhighchair>
     * AttributeType .Boolean
     * <lhighchair>0</lhighchair>
     */
    private boolean highchair;

    public String isHighchair() {
        return "0".equals(element.getChildTextTrim("lhighchair")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <nbathrooms>
     * AttributeType .Text (number)
     * <nbathrooms>1</nbathrooms>
     */
    private String bathrooms;

    public String getBathrooms() {
        return element.getChildTextTrim("nbathrooms");
    }

    /**
     * <lcots>
     * AttributeType .Boolean
     * <lcots>0</lcots>
     */
    private boolean cots;

    public String isCots() {
        return "0".equals(element.getChildTextTrim("lcots")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <lhenstag>
     * AttributeType .Boolean
     * <lhenstag>0</lhenstag>
     */
    private boolean henstag;

    public String isHenstag() {
        return "0".equals(element.getChildTextTrim("lhenstag")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <lchildren>
     * AttributeType .Boolean
     * <lchildren>0</lchildren>
     */
    private boolean children;

    public String isChildren() {
        return "0".equals(element.getChildTextTrim("lchildren")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <lgamesroom>
     * AttributeType .Boolean
     * <lgamesroom>0</lgamesroom>
     */
    private boolean gamesroom;

    public String isGamesroom() {
        return "0".equals(element.getChildTextTrim("lgamesroom")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <lpingpong>
     * AttributeType .Boolean
     * <lpingpong>0</lpingpong>
     */
    private boolean pingpong;

    public String isPingpong() {
        return "0".equals(element.getChildTextTrim("lpingpong")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <lsnooker>
     * AttributeType .Boolean
     * <lsnooker>0</lsnooker>
     */
    private boolean snooker;

    public String isSnooker() {
        return "0".equals(element.getChildTextTrim("lsnooker")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <lwifi>
     * AttributeType .Boolean
     * <lwifi>0</lwifi>
     */
    private boolean wifi;

    public String isWifi() {
        return "0".equals(element.getChildTextTrim("lwifi")) ? Boolean.FALSE.toString() : Boolean.TRUE.toString();
    }

    /**
     * <nwebprice>
     * AttributeType .Text (number)
     * <nwebprice>169</nwebprice>
     */
    private String webprice;

    public String getWebprice() {
        return element.getChildTextTrim("nwebprice");
    }

    /**
     * AttributeType.TextArea
     * <cfurtherfacilitiesinternal/>
     */
    private String furtherfacilitiesinternal;

    public String getFurtherfacilitiesinternal() {
        return element.getChildTextTrim("cfurtherfacilitiesinternal");
    }

    /**
     * <cregiondesc>
     * AttributeType .TextArea
     * <cregiondesc>
     * From the lush green farmlands of the Borders to the ruggedness of the Highlands, the Scots are proud of their land and identity. Scotland also has some of Britain&#8217;s best beaches with glorious stretches of clean sand over which to roam.
     * </cregiondesc>
     */
    private String regiondesc;

    public String getRegiondesc() {
        return element.getChildTextTrim("cregiondesc");
    }

    /**
     * <ctowndesc>
     * AttributeType .TextArea
     * <ctowndesc>
     * Famous for its salmon and trout fishing, the market town of Newton Stewart is situated in a stunning location between the rolling countryside of the Machars and the Galloway Forest Park. Steeped in history, Scotland's first Christian Missionary landed at The Machars, founding the country's first church in Whithorn in AD 397. The Whithorn Dig Visitor's Centre is also close by, displaying material unearthed through years of excavation projects. Nearby, visitors can enjoy miles of coastline combining a mixture of sandy beaches and rocky shores, both of which are home to a diverse variety of birds. Alternatively, a trip inland will lead to superb walking country in the Galloway Hills and Forest Park. The surrounding area also boasts wonderful sailing, riding and fishing facilities, in addition to a variety of golf courses.
     * </ctowndesc>
     */
    private String towndesc;

    public String getTowndesc() {
        return element.getChildTextTrim("ctowndesc");
    }

    /**
     * <chomesummary>
     * AttributeType .TextArea
     * <chomesummary>
     * This wing to the owners' farmhouse is one mile from the town of Newton Stewart close to Galloway Forest Park and can sleep two people.
     * </chomesummary>
     */
    private String homesummary;

    public String getHomesummary() {
        return element.getChildTextTrim("chomesummary");
    }

    /**
     * <chomedesc>
     * AttributeType .TextArea
     * <chomedesc>
     * Nether Barr Farmhouse is a cottage that adjoins the owners' 19th century farmhouse, located one mile from Newton Stewart, near to Galloway Forest Park in Southern Scotland. This cottage has one twin bedroom with an en-suite shower room, making it suitable for two people. This all ground floor cottage also contains an open plan living area with a kitchen, a dining and a living area. The outside of the cottage has off road parking for one car and a large garden with patio and garden furniture. Nether Barr Farmhouse is situated in a beautiful, peaceful area and provides a perfect holiday home.
     * </chomedesc>
     */
    private String homedesc;

    public String getHomedesc() {
        return element.getChildTextTrim("chomedesc");
    }

    /**
     * AttributeType .TextArea
     * <cfurtherfacilitiesexternal/>
     */
    private String furtherfacilitiesexternal;

    public String getFurtherfacilitiesexternal() {
        return element.getChildTextTrim("cfurtherfacilitiesexternal");
    }

    /**
     * AttributeType .TextArea
     * <cfurtherdetails/>
     */
    private String furtherdetails;

    public String getFurtherdetails() {
        return element.getChildTextTrim("cfurtherdetails");
    }

    /**
     * AttributeType . ??? //todo
     * <cspecialconditions/>
     */
    private String specialconditions;

    public String getSpecialconditions() {
        return element.getChildTextTrim("cspecialconditions");
    }

    /**
     * <amenityNotes>
     * AttributeType .TextArea
     * <amenityNotes>
     * Full LPG central heating with additional heating from electric fires (easily transported to any room). Electric oven and hob, microwave, fridge, colour TV with Freeview, DVD player, radio, stereo/CD player, library of books, games and DVDs. All fuel and power inc. in rent. Bed linen and towels inc. in rent. Off road parking for one car. Large, lawned garden with patio area and garden furniture. Two well behaved pets welcome at a charge of &#163;10 per pet, per week. Sorry, no smoking. Shop and pub 1 mile. Note: Children are not accepted at this property.
     * </amenityNotes>
     */
    private String amenityNotes;

    public String getAmenityNotes() {
        return element.getChildTextTrim("amenityNotes");
    }

    /**
     * <tcreated>
     * AttributeType .DateTime                  //todo ecreator system date&time
     * <tcreated>2011-08-09 22:33:52</tcreated>
     */
    private DateTime created;

    /**
     * <nsykesid>
     * AttributeType .Text
     * <nsykesid>492</nsykesid>
     */
    private String nsykesid;

    public String getNsykesid() {
        return element.getChildTextTrim("nsykesid");
    }

    /**
     * <tupdated>
     * AttributeType .Date
     * <tupdated>2011-08-08</tupdated>
     */
    private DateTime updated;

    public DateTime getUpdated() throws ParseException {
        return new DateTime(element.getChildTextTrim("tupdated"), "YYYY-MM-DD");
    }

    /**
     * <cpropname>
     * AttributeType .TextArea
     * <cpropname>
     * Nether Barr Farmhouse Pet-Friendly Cottage, Newton Stewart, Southern Scotland (Ref 492)
     * </cpropname>
     */
    private String propname;

    public String getPropname() {
        return element.getChildTextTrim("cpropname");
    }

    /**
     * <ccountry>
     * AttributeType .Selection
     * <ccountry>Scotland</ccountry>
     */
    private String country;

    public String getCountry() {
        return element.getChildTextTrim("ccountry");
    }

    /**
     * <cvillagename>
     * AttributeType .Text
     * <cvillagename>NEWTON STEWART</cvillagename>
     */
    private String villagename;

    public String getVillagename() {
        return element.getChildTextTrim("cvillagename");
    }

    /**
     * <nsykesrating>
     * AttributeType .Selection 1,2,3,4,5
     * <nsykesrating>4</nsykesrating>
     */
    private Integer sykesrating;

    public String getSykesrating() {
        return element.getChildTextTrim("nsykesrating");
    }

    /**
     * <nvillageid>
     * AttributeType .Text
     * <nvillageid>586</nvillageid>
     */
    private Integer villageid;

    public String getVillageid() {
        return element.getChildTextTrim("nvillageid");
    }

    /**
     * <cweblink>
     * AttributeType .Link
     * <cweblink>
     * http://www.sykescottages.co.uk/cottages/492.php?rfx=10311
     * </cweblink>
     */
    private String weblink;

    public String getWeblink() {
        return element.getChildTextTrim("cweblink");
    }

}
