package org.sevensoft.ecreator.model.feeds.bespoke.prices.dealtime;

import java.io.File;
import java.io.IOException;

import org.jdom.JDOMException;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.ExportFeed;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.annotations.Ftp;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

import com.enterprisedt.net.ftp.FTPException;

/**
 * @author sks 26-Apr-2005 20:24:56
 * 
 * comma separated file
 * 
 * Product Name - Max of approx 50 Characters including spaces displayed. If you are extracting a field from your database longer than 50 characters,
 * we will automatically truncate it. You are advised not to truncate the field yourself as this may result in important keywords being excluded from
 * our Database. Depending on the category, Dealtime may not display these fields.
 * 
 * Product Description - Max of approx 100 Characters including spaces displayed. If you are extracting a field from your database longer than 100
 * characters, we will automatically truncate it. You are advised not to truncate the field yourself as this may result in important keywords being
 * excluded from our Database. Depending on the category, DealTime may not display these fields.
 * 
 * MPN - Manufacturers Part Number is our principle method of mapping on Computers products. It is therefore essential you supply this key field. MPNs
 * can be obtained from the manufacturers (their web sites/product literature) or you can look at other retailers who may list them.
 * 
 * UPC - Not essential, but can be supplied as a supplement to MPN
 * 
 * Manufacturer - This field is combined with MPN to achieve our category mapping, so is essential.
 * 
 * Category - You can use your own category structure (wording and phraseology) in this field or the DealTime taxonomy, which will be provided with
 * this sample. If you use your own category structure, you can if need be add extra fields for subcategories, for instance "computer" and "printers"
 * or "computer" and "modems". Equally, "computers - modems" or any clear variation on the format would work equally well in a single field. The
 * important thing is that the products are clearly categorised. If you elect to use the DealTime taxomony, you do not need subcategory fields, but
 * will be asked to paste into the single category field the precise category information as provided in the "fast track" taxomony file.
 * 
 * Price - Dealtime always displays pricing VAT inclusive. Should you only be able to supply prices in the feed VAT excluded, please advise us so we
 * can apply the relevant calculation to add VAT.
 * 
 * Product URL
 * 
 * Image URL
 * 
 * Shipping Cost - At present we do not include shipping on our search results. This is subject to review and so it is worth adding this field to
 * future proof your feed. If you have a single standard shipping charge for all products, you can exclude this field as it can be globally applied in
 * the database.
 * 
 * 
 * Stock (Y or N) - Must be a Y or an N. If all your products are always in stock, you may exclude this field and simply tell us this. We can apply an
 * "in stock" condition globally to all your products.
 * 
 * 
 * 
 */
@Table("feeds_dealtime")
@Ftp
@Label("Dealtime / Shopping.com")
public class DealtimeFeed extends ExportFeed {

	public DealtimeFeed(RequestContext context) {
		super(context);
	}

	public DealtimeFeed(RequestContext context, boolean create) {
		super(context, create);
	}

	public String getFeed() {

		StringBuilder sb = new StringBuilder();

		// String[] array = new String[13];
		// array[0] = product.getModelNumber();
		// if (product.hasManufacturer())
		// array[1] = product.getManufacturer().getName();
		//
		// array[3] = product.getName();
		// array[4] = product.getName();
		// array[5] = product.getSalePriceInc().toString();
		// array[6] = Site.getInstance().getProductUrl(product)
		// + "&referrer=dealtime";
		//
		// ProductImage image = product.getImage();
		// if (image != null) {
		//
		// Url url = Site.getInstance().getHomeUrl();
		// url.setPath(image.getThumbnailUrl());
		// array[7] = url.toString();
		// }
		//
		// array[8] = type.getDealtimeCategory();
		//
		// if (product.isAvailable()) {
		// array[9] = "Y";
		// array[10] = "In stock";
		// } else {
		// array[9] = "N";
		// array[10] = product.getLeadtime();
		// }
		//
		// array[11] = "6.95";
		// array[12] = String.valueOf(product.getWeight());
		//
		// String string = StringUtil.implode(array, "|");
		// builder.append(string);
		// builder.append("\n");

		return sb.toString();
	}

	@Override
	public void removeAttribute(Attribute attribute) {
	}

	@Override
	public FeedResult run(File file) throws IOException, FTPException, FeedException, JDOMException {
		return null;
	}

}
