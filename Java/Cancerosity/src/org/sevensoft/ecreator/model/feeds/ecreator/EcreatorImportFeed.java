package org.sevensoft.ecreator.model.feeds.ecreator;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.admin.feeds.ecreator.EcreatorImportFeedHandler;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.Http;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

import com.enterprisedt.net.ftp.FTPException;

/**
 * @author sks 27 Jan 2007 17:49:09
 *
 */
@Table("feeds_ecreator_import")
@Label("Ecreator import")
@Http
@HandlerClass(EcreatorImportFeedHandler.class)
public class EcreatorImportFeed extends ImportFeed {

	/**
	 * The item type to create items of
	 */
	private ItemType					itemType;

	/**
	 * Ignore the categories from the source feed
	 */
	private boolean					ignoreCategories;

	private transient Map<String, Attribute>	attributeCache	= new HashMap();

	public EcreatorImportFeed(RequestContext context) {
		super(context);
	}

	public EcreatorImportFeed(RequestContext context, boolean create) {
		super(context, create);
	}

	public final ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public final boolean isIgnoreCategories() {
		return ignoreCategories;
	}

	@Override
	public void removeAttribute(Attribute attribute) {
	}

	@Override
	public FeedResult run(File file) throws IOException, FTPException, FeedException, JDOMException {

		long time = System.currentTimeMillis();

		updateFeedCount();

		if (file == null) {
			file = getInputFile();
		}

		if (file == null) {
			throw new FeedException("No data supplied");
		}

		if (itemType == null) {
			throw new FeedException("No item type set");
		}

		Document doc = new SAXBuilder().build(file);
		Element root = doc.getRootElement();

		//		Element header = root.getChild("Header");
		//		String url = header.getChildText("Url");

		// process attributes first
		List<Element> attributeEls = root.getChildren("Attribute");
		for (Element attributeEl : attributeEls) {

			String name = attributeEl.getChildText("Name");
			String prefix = attributeEl.getChildText("Prefix");
			String suffix = attributeEl.getChildText("Suffix");
			AttributeType type = AttributeType.valueOf(attributeEl.getChildText("Type"));
			boolean displayable = Boolean.valueOf(attributeEl.getChildText("Displayable"));
			boolean editable = Boolean.valueOf(attributeEl.getChildText("Editable"));
			boolean customOptionsOrder = Boolean.valueOf(attributeEl.getChildText("CustomOptionsOrder"));
			boolean location = Boolean.valueOf(attributeEl.getChildText("Location"));
			boolean optional = Boolean.valueOf(attributeEl.getChildText("Optional"));
			int max = Integer.parseInt(attributeEl.getChildText("Max"));

			Attribute attribute = getItemType().getAttribute(name);
			if (attribute == null) {

				attribute = getItemType().addAttribute(name, type);
				attribute.setPrefix(prefix);
				attribute.setPrefix(suffix);
				attribute.setDisplayable(displayable);
				attribute.setEditable(editable);
				attribute.setMax(max);
				attribute.setCustomOptionsOrder(customOptionsOrder);
				attribute.setOptional(optional);
				attribute.setLocation(location);

				attribute.save();
			}
		}

		int created = 0;
		int processed = 0;

		// get all item sub elements
		List<Element> itemEls = root.getChildren("Item");
		for (Element itemEl : itemEls) {

			processed++;
            String reference = itemEl.getChildText("Reference");
            if (reference == null || "".equals(reference)) {
                reference = itemEl.getChildText("Id");
            }

			String name = itemEl.getChildText("Name");
			String content = itemEl.getChildText("Content");
			if (content != null && !getItemType().isContent()) {
				logger.fine("[EcreatorImportFeed] enabling content on item type");
				itemType.addModule(ItemModule.Content);
				itemType.save();
			}

			if (!getItemType().isReferences()) {
				logger.fine("[EcreatorImportFeed] enabling references on item type");
				itemType.setReferences(true);
				itemType.save();
			}

			boolean prioritised = "true".equals(itemEl.getChildText("Prioritised"));
			boolean featured = "true".equals(itemEl.getChildText("Featured"));

			attributeEls = itemEl.getChildren("Attribute");
			List<Element> imageEls = itemEl.getChildren("Image");
			List<Element> categoryEls = itemEl.getChildren("Category");
			List<Element> attachmentEls = itemEl.getChildren("Attachment");

			// then attempt to load by reference if not null
			Item item = Item.getByRef(context, itemType, reference);
			logger.fine("[EcreatorImportFeed] attempted to locate by ref=" + reference + ", item=" + item);

			if (item == null) {

				item = createItem(getItemType(), name);
				created++;

				item.setContent(content);
				item.setReference(reference);
				item.setFeatured(featured);
				item.setPrioritised(prioritised);
				item.save();

				// IMAGES
				for (Element imageEl : imageEls) {

					String url = imageEl.getText();
					try {
						logger.fine("[EcreatorImportFeed] adding image url=" + url);
						item.addImage(new URL(url), true, true, true);
					} catch (ImageLimitException e) {
						e.printStackTrace();
					}
				}

				if (!isIgnoreCategories()) {
					for (Element categoryEl : categoryEls) {
						String categoryName = categoryEl.getText();
						Category category = getCategory(categoryName, categoryName);
						if (category != null) {
							logger.fine("[EcreatorImportFeed] adding category=" + category);
							item.addCategory(category);
						}
					}
				}

				// ATTRIBUTES
				MultiValueMap<Attribute, String> attributeValues = new MultiValueMap(new LinkedHashMap());
				for (Element attributeEl : attributeEls) {

					String attributeName = attributeEl.getChildText("Name");
					String attributeValue = attributeEl.getChildText("Value");

					Attribute attribute = attributeCache.get(attributeName);

					// if null from cache then get from item type
					if (attribute == null) {
						attribute = getItemType().getAttribute(attributeName, AttributeType.Text);
						attributeCache.put(attributeName, attribute);
					}

					attributeValues.put(attribute, attributeValue);
				}

				if (attributeValues.size() > 0) {
					logger.fine("[EcreatorImportFeed] setting attribute values=" + attributeValues);
					item.setAttributeValues(attributeValues);
				}
			}

			// Update attachments
			for (Element attachmentEl : attachmentEls) {

				String aname = attachmentEl.getChildText("Name");
				String filename = attachmentEl.getChildText("Filename");
				String url = attachmentEl.getChildText("Url");
				String downloadLinkText = attachmentEl.getChildText("DownloadLinkText");
				Attachment.Method method = Attachment.Method.valueOf(attachmentEl.getChildText("Method"));

				if (!item.hasAttachment(filename)) {

					try {

						Attachment attachment = item.addAttachment(new URL(url));
						attachment.setMethod(method);
						attachment.setDownloadLinkText(downloadLinkText);
						attachment.setName(aname);
						attachment.save();

					} catch (AttachmentTypeException e) {
						e.printStackTrace();

					} catch (AttachmentExistsException e) {
						e.printStackTrace();

					} catch (AttachmentLimitException e) {
						e.printStackTrace();
					}

				}
			}
		}

		time = System.currentTimeMillis() - time;
		return new FeedResult(created, processed, time);
	}

	public final void setIgnoreCategories(boolean ignoreCategories) {
		this.ignoreCategories = ignoreCategories;
	}

	public final void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

}