package org.sevensoft.ecreator.model.feeds.spiders;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.httpc.HttpMethod;
import org.sevensoft.ecreator.model.categories.Category;

/**
 * @author sks 11-Nov-2005 13:09:42
 * 
 */
public class FilmVideoSpider {

	public int spider(List<String> urls, List<Category> categories) throws IOException {

		int n = 0;
		for (String url : urls) {
			try {
				spider(url, categories);
				n++;
			} catch (IOException e) {
				//e.printStackTrace();
			}
		}

		return n;

	}

	private void spider(String url, List<Category> categories) throws IOException {

		@SuppressWarnings("unused")
		String name = null, address = null, telephone = null, website = null, email = null, contact = null, fax = null, postcode = null;

		HttpClient hc = new HttpClient(url, HttpMethod.Get);
		hc.connect();
		String input = hc.getResponseString();

		Pattern p = Pattern.compile("<TR><TD><FONT size=2>Address</FONT></TD><TD><FONT size=2 face=verdana>(.*?)</TD></TR><TR><TD><FONT size=2>Telephone",
				Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		Matcher m = p.matcher(input);
		if (m.find())
			address = m.group(1);

		if (address != null) {

			p = Pattern.compile("<.*?>", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
			address = p.matcher(address).replaceAll(" ");

			p = Pattern.compile("\\s{2,}");
			address = p.matcher(address).replaceAll("\n");

			p = Pattern.compile(".*{0,5}\\d\\w\\w$");
			m = p.matcher(address);
			if (m.find()) {
				postcode = m.group(0);
				address = m.replaceFirst("");
			}
		}

		p = Pattern
				.compile("<TR><TD><FONT size=2>Telephone</FONT></TD><TD><FONT size=2 face=verdana>(.*?)</TD>", Pattern.CASE_INSENSITIVE
						| Pattern.DOTALL);
		m = p.matcher(input);
		if (m.find())
			telephone = m.group(1);
		//
		// p = Pattern.compile("<TR><TD valign=top><FONT size=2>Info</FONT></TD><TD colspan=2><FONT size=2 face=verdana>(.*?)</TD>",
		// Pattern.CASE_INSENSITIVE
		// | Pattern.DOTALL);
		// m = p.matcher(input);
		// if (m.find())
		// info = m.group(1);

		p = Pattern.compile("<TR><TD><FONT size=2>Email</FONT></TD><TD colspan=2><A.*?><FONT size=2 face=verdana>(.*?)</A></TD>", Pattern.CASE_INSENSITIVE
				| Pattern.DOTALL);
		m = p.matcher(input);
		if (m.find())
			email = m.group(1);

		p = Pattern.compile("<TR><TD><FONT size=2>Website</FONT></TD><TD colspan=2><FONT size=2 face=verdana><A HREF='.*?'>(.*?)</TD>",
				Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		m = p.matcher(input);
		if (m.find())
			website = m.group(1);

		p = Pattern.compile("<TR><TD><FONT size=2>Contact</FONT></TD><TD colspan=2><FONT size=2 face=verdana>(.*?)</TD>", Pattern.CASE_INSENSITIVE
				| Pattern.DOTALL);
		m = p.matcher(input);
		if (m.find())
			contact = m.group(1);

		p = Pattern.compile("<TR><TD><FONT size=2>Fax</FONT></TD><TD><FONT size=2 face=verdana>(.*?)</TD>", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		m = p.matcher(input);
		if (m.find())
			fax = m.group(1);

		p = Pattern.compile("<TABLE width='100%'><TR><TD colspan=3 align=center bgcolor='#e1e2ff'><FONT SIZE='6' face=verdana>(.*?)</FONT></TD></TR>",
				Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		m = p.matcher(input);
		if (m.find())
			name = m.group(1);

		throw new RuntimeException();

		//		Listing listing = new Listing(name, AbstractItem.Status.Live);
		//		listing.setAddress(address);
		//		listing.setName(name);
		//		listing.setPostcode(postcode);
		//		listing.setWebsite(website);
		//		listing.setTelephone(telephone);
		//		listing.setEmail(email);
		//		// listing.setContent(info);
		//
		//		for (Category category : categories)
		//			listing.addCategory(category);
		//
		//		Category root = Category.getRoot();
		//
		//		if (fax != null) {
		//			Attribute faxAttribute = Attribute.getAttribute(root, "Fax");
		//			if (faxAttribute != null) {
		//				listing.setAttributeValue(faxAttribute, fax);
		//			}
		//		}
		//
		//		if (contact != null) {
		//			Attribute contactNameAttribute = Attribute.getAttribute(root, "Contact name");
		//			if (contactNameAttribute != null) {
		//				listing.setAttributeValue(contactNameAttribute, contact);
		//			}
		//		}

	}
}
