package org.sevensoft.ecreator.model.feeds.spiders;

import java.io.IOException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 12 Jul 2006 19:32:20
 *
 */
public class ShoesDotComSpider {

	public static void main(String[] args) throws IOException {

		new ShoesDotComSpider().spider(null, "http://www.shoes.com/product.asp?p=5020015");

	}

	private RequestContext	context;

	public Item spider(ItemType itemType, String url) throws IOException {

		HttpClient hc = new HttpClient(new URL(url));
		hc.connect();
		String string = hc.getResponseString();

		Pattern contentPattern = Pattern.compile("<!--Begin htmProduct-->(.*?)<!--End htmProduct-->");
		Pattern namePattern = Pattern.compile("<td colspan=\"2\"><FONTclass=\"PD_BrandStyle\">(.*?)</FONT></td>");
		Pattern pricePattern = Pattern.compile("<td width=\"100\"><h1 id=\"ProductPrice\" >(.*?)</h1></td>");
		Pattern imagePattern = Pattern.compile("var pi_\\d+?='/productimages/(.*?)';");

		String content = null, name = null, price = null, imageUrl = null;

		Matcher matcher = contentPattern.matcher(string);
		if (matcher.find())
			content = matcher.group(1);

		matcher = pricePattern.matcher(string);
		if (matcher.find())
			price = matcher.group(1);

		matcher = imagePattern.matcher(string);
		if (matcher.find())
			imageUrl = matcher.group(1);

		matcher = namePattern.matcher(string);
		if (matcher.find())
			name = matcher.group(1);

		//		System.out.println(name);
		//		System.out.println(content);
		System.out.println(price);
		//		System.out.println(imageUrl);

		if (name != null) {

			Item item = new Item(context, itemType, name.trim(), "Live", null);
			//			if (price != null)
			//				item.setSellPrice(null, 1, price.trim());

			item.setContent(content);
			try {
				item.addImage(new URL("http://www.shoes.com/productimages/" + imageUrl), true, true, false);
			} catch (ImageLimitException e) {
				e.printStackTrace();
			}

			return item;
		}

		return null;
	}

}
