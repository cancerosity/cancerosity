package org.sevensoft.ecreator.model.feeds;

import com.ricebridge.csvman.CsvManager;

/**
 * @author sks 13-Jan-2006 22:04:35
 *
 */
public class FeedResult {

	private int		created;
	private double	timeSeconds;
	private int		processed;
	private int		updated;

	public FeedResult(CsvManager csvman) {
		this.processed = (int) csvman.getLineCount();
		this.timeSeconds = csvman.getTimeTakenInSeconds();
	}

	public FeedResult(int created, int processed, double timeSeconds) {
		this.created = created;
		this.processed = processed;
		this.timeSeconds = timeSeconds;
	}

	public FeedResult(int created, int processed, int updated, double timeSeconds) {
		this.created = created;
		this.processed = processed;
		this.updated = updated;
		this.timeSeconds = timeSeconds;
	}

	public int getCreated() {
		return created;
	}

	public String getDetails() {

		StringBuilder sb = new StringBuilder();
		sb.append(processed + " item(s) processed, " + created + " item(s) created in ");

		if (timeSeconds > 100) {

			sb.append((int) (timeSeconds / 60));
			sb.append(" minutes.");

		} else {

			sb.append(timeSeconds);
			sb.append(" seconds.");
		}

		return sb.toString();
	}

	public int getProcessed() {
		return processed;
	}

	public double getTimeSeconds() {
		return timeSeconds;
	}

	public int getUpdated() {
		return updated;
	}
}
