package org.sevensoft.ecreator.model.feeds.bespoke.prices.dealtime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author sks 18-Oct-2004 11:07:25
 */
public class DealtimeCategories {

	static List<String> categories;
	static {

		categories = new ArrayList<String>();

		categories.add("Appliances / Air Conditioners");
		categories.add("Appliances / Air Purifiers");
		categories.add("Appliances / Appliance Accessories");
		categories.add("Appliances / Dehumidifiers");
		categories.add("Appliances / Dishwashers");
		categories.add("Appliances / Electric Fans");
		categories.add("Appliances / Freestanding Cookers");
		categories.add("Appliances / Freezers");
		categories.add("Appliances / Heaters");
		categories.add("Appliances / Hobs");
		categories.add("Appliances / Humidifiers");
		categories.add("Appliances / Irons");
		categories.add("Appliances / Kitchen Hoods");
		categories.add("Appliances / Microwave Ovens");
		categories.add("Appliances / Ovens (Gas and Electric)");
		categories.add("Appliances / Refrigerators");
		categories.add("Appliances / Sewing Machines");
		categories.add("Appliances / Tumble Dryers");
		categories.add("Appliances / Vacuums");
		categories.add("Appliances / Washing Machines");
		categories.add("Clothing");
		categories
				.add("Computers / Accessories and Supplies / Cables and Connectors");
		categories
				.add("Computers / Accessories and Supplies / Desk Accessories");
		categories.add("Computers / Accessories and Supplies / KVM Switches");
		categories
				.add("Computers / Accessories and Supplies / Laptop Accessories");
		categories
				.add("Computers / Accessories and Supplies / Monitor Accessories");
		categories.add("Computers / Computer Memory / Cache Memory");
		categories.add("Computers / Computer Memory / Flash Memory");
		categories.add("Computers / Computer Memory / Memory Adapter");
		categories.add("Computers / Computer Memory / Random Access Memory ");
		categories.add("Computers / Computer Memory / Read Only Memory");
		categories.add("Computers / Computer Memory / Video Memory");
		categories.add("Computers / Computer Speakers");
		categories.add("Computers / Input Devices / Graphic Tablets");
		categories.add("Computers / Input Devices / Keyboards");
		categories.add("Computers / Input Devices / Mice and Trackballs");
		categories.add("Computers / Input Devices / PC Gaming Devices");
		categories.add("Computers / Input Devices / Web Cameras");
		categories.add("Computers / Modems");
		categories.add("Computers / Monitors");
		categories.add("Computers / Networking");
		categories.add("Computers / Performance Upgrades / Graphics Cards");
		categories.add("Computers / Performance Upgrades / Input Adapters");
		categories.add("Computers / Performance Upgrades / Motherboards");
		categories.add("Computers / Performance Upgrades / Processors");
		categories.add("Computers / Performance Upgrades / Sound Cards");
		categories.add("Computers / Performance Upgrades / System Cooling");
		categories.add("Computers / Personal Organisers / PDA Accessories");
		categories.add("Computers / Personal Organisers / Personal Organisers");
		categories.add("Computers / Printers / Printer Accessories");
		categories.add("Computers / Printers / Printers");
		categories.add("Computers / Scanners / Barcode Scanners");
		categories.add("Computers / Scanners / Scanner Accessories");
		categories.add("Computers / Scanners / Scanners");
		categories.add("Computers / Storage and Media / CD ROM Drives");
		categories.add("Computers / Storage and Media / CD and DVD Burners");
		categories.add("Computers / Storage and Media / DVD Drives");
		categories.add("Computers / Storage and Media / Drive Cases");
		categories.add("Computers / Storage and Media / Floppy Drives");
		categories.add("Computers / Storage and Media / Hard Drives");
		categories.add("Computers / Storage and Media / Network Storage");
		categories.add("Computers / Storage and Media / PC Cases");
		categories.add("Computers / Storage and Media / Removable media");
		categories.add("Computers / Storage and Media / Storage Controllers");
		categories.add("Computers / Storage and Media / Tape Drives");
		categories.add("Computers / Storage and Media / Zip and Other Drives");
		categories.add("Computers / Surge Suppressors");
		categories.add("Computers / System Power Supplies");
		categories.add("Computers / Systems / Barebone Systems");
		categories.add("Computers / Systems / Mac Desktops");
		categories.add("Computers / Systems / Mac Laptops");
		categories.add("Computers / Systems / Network Terminals");
		categories.add("Computers / Systems / PC Desktops");
		categories.add("Computers / Systems / PC Laptops");
		categories.add("Computers / Systems / Servers");
		categories.add("Computers / Systems / Tablet PCs");
		categories.add("Computers / Systems / Workstations");
		categories.add("Computers / UPS Accessories");
		categories.add("Computers / UPS Systems Devices");
		categories.add("Cosmetics");
		categories
				.add("Electronics / Cameras and Photography / 35mm SLR Cameras");
		categories
				.add("Electronics / Cameras and Photography / Camcorder Accessories");
		categories.add("Electronics / Cameras and Photography / Camcorders");
		categories.add("Electronics / Cameras and Photography / Camera Lenses");
		categories
				.add("Electronics / Cameras and Photography / Digital Camera Accessories");
		categories
				.add("Electronics / Cameras and Photography / Digital Cameras");
		categories.add("Electronics / Cameras and Photography / Flashes");
		categories
				.add("Electronics / Cameras and Photography / Instant Cameras");
		categories
				.add("Electronics / Cameras and Photography / Lens Converters");
		categories
				.add("Electronics / Cameras and Photography / Medium Format Cameras");
		categories
				.add("Electronics / Cameras and Photography / Photographic Accessories");
		categories
				.add("Electronics / Cameras and Photography / Photographic Film");
		categories
				.add("Electronics / Cameras and Photography / Point and Shoot Cameras");
		categories
				.add("Electronics / Cameras and Photography / Rangefinder Cameras");
		categories
				.add("Electronics / Cameras and Photography / Single Use Cameras");
		categories.add("Electronics / Cameras and Photography / Tripods");
		categories.add("Electronics / Car Audio and Electronics / CB Radios");
		categories
				.add("Electronics / Car Audio and Electronics / Car Amplifiers");
		categories
				.add("Electronics / Car Audio and Electronics / Car Audio Accessories");
		categories
				.add("Electronics / Car Audio and Electronics / Car CD Changers");
		categories
				.add("Electronics / Car Audio and Electronics / Car Equalizers");
		categories
				.add("Electronics / Car Audio and Electronics / Car Monitors");
		categories
				.add("Electronics / Car Audio and Electronics / Car Speakers and Subwoofers");
		categories
				.add("Electronics / Car Audio and Electronics / Car Video Players");
		categories
				.add("Electronics / Car Audio and Electronics / In Dash Receivers");
		categories
				.add("Electronics / Car Audio and Electronics / Radar Detectors");
		categories
				.add("Electronics / Car Audio and Electronics / Radio Scanners");
		categories.add("Electronics / Home Audio / Amplifiers and Preamps");
		categories.add("Electronics / Home Audio / Audio Shelf Systems");
		categories.add("Electronics / Home Audio / Audio and Video Media");
		categories.add("Electronics / Home Audio / CD Players");
		categories.add("Electronics / Home Audio / Equalizers");
		categories.add("Electronics / Home Audio / Home Audio Accessories");
		categories
				.add("Electronics / Home Audio / Home Audio Speakers and Subwoofers");
		categories.add("Electronics / Home Audio / Microphones");
		categories.add("Electronics / Home Audio / Mini Disc Players");
		categories.add("Electronics / Home Audio / Receivers");
		categories.add("Electronics / Home Audio / Tape Decks");
		categories.add("Electronics / Home Audio / Turntables");
		categories.add("Electronics / Home Theatre / DVD Players");
		categories.add("Electronics / Home Theatre / Home Theatre Systems");
		categories.add("Electronics / Phones and Clocks / Answering Machines");
		categories.add("Electronics / Phones and Clocks / Clock Radios");
		categories
				.add("Electronics / Phones and Clocks / Conferencing Stations");
		categories.add("Electronics / Phones and Clocks / Phones");
		categories
				.add("Electronics / Phones and Clocks / Telephone Accessories");
		categories.add("Electronics / Portable Electronics / 2 Way Radios");
		categories.add("Electronics / Portable Electronics / GPS Accessories");
		categories.add("Electronics / Portable Electronics / GPS Devices");
		categories.add("Electronics / Portable Electronics / Headphones");
		categories.add("Electronics / Portable Electronics / Headsets");
		categories.add("Electronics / Portable Electronics / MP3 Accessories");
		categories
				.add("Electronics / Portable Electronics / MP3 and Digital Media Players");
		categories
				.add("Electronics / Portable Electronics / Personal CD Players");
		categories
				.add("Electronics / Portable Electronics / Personal Cassette Players");
		categories
				.add("Electronics / Portable Electronics / Personal MiniDisc Players");
		categories.add("Electronics / Portable Electronics / Personal Radios");
		categories.add("Electronics / Portable Electronics / Portable Stereos");
		categories
				.add("Electronics / Portable Electronics / Telescopes and Binoculars");
		categories.add("Electronics / TV and Video / Digital TV Recorders");
		categories.add("Electronics / TV and Video / Flat Panel Televisions");
		categories.add("Electronics / TV and Video / Handheld Televisions");
		categories.add("Electronics / TV and Video / Projection Televisions");
		categories.add("Electronics / TV and Video / Remote Controls");
		categories.add("Electronics / TV and Video / Satellite TV");
		categories
				.add("Electronics / TV and Video / Security Systems and Surveillance");
		categories.add("Electronics / TV and Video / Standard Televisions");
		categories.add("Electronics / TV and Video / TV Accessories");
		categories.add("Electronics / TV and Video / TV Combo");
		categories.add("Electronics / TV and Video / VCRs");
		categories.add("Electronics / Weather Stations");
		categories.add("Fragrance");
		categories.add("Furniture");
		categories.add("Garden");
		categories.add("Health and Beauty / Health Aids / Blood Pressure Kits");
		categories.add("Health and Beauty / Health Aids / Home Test Kits");
		categories.add("Health and Beauty / Health Aids / Personal Health");
		categories.add("Health and Beauty / Shavers");
		categories.add("Home Furnishings");
		categories.add("Home and Garden / Babies and Kids");
		categories.add("Home and Garden / Bags and Luggage");
		categories.add("Home and Garden / Craft Supplies");
		categories.add("Home and Garden / Kitchen / Bakeware");
		categories.add("Home and Garden / Kitchen / Coffee and Tea Makers");
		categories.add("Home and Garden / Kitchen / Cookware");
		categories.add("Home and Garden / Kitchen / Cutlery and Knives");
		categories.add("Home and Garden / Kitchen / Kitchen Tools");
		categories.add("Home and Garden / Kitchen / Tableware");
		categories.add("Home and Garden / Pets");
		categories.add("Jewellery");
		categories.add("Jewellery and Watches / Sunglasses");
		categories.add("Jewellery and Watches / Watches");
		categories.add("Kitchen / Small Appliances / Blenders");
		categories.add("Kitchen / Small Appliances / Bread Machines");
		categories.add("Kitchen / Small Appliances / Coffee Grinders");
		categories
				.add("Kitchen / Small Appliances / Crock Pots and Slow Cookers");
		categories.add("Kitchen / Small Appliances / Deep Fryers");
		categories
				.add("Kitchen / Small Appliances / Electric Skillets and Woks");
		categories.add("Kitchen / Small Appliances / Food Processors");
		categories.add("Kitchen / Small Appliances / Ice Cream Makers");
		categories
				.add("Kitchen / Small Appliances / Indoor Grills and Sandwich Makers");
		categories.add("Kitchen / Small Appliances / Juicers");
		categories
				.add("Kitchen / Small Appliances / Kitchen Electrics Accessories");
		categories.add("Kitchen / Small Appliances / Mixers");
		categories.add("Kitchen / Small Appliances / Pasta Makers");
		categories.add("Kitchen / Small Appliances / Popcorn Makers");
		categories
				.add("Kitchen / Small Appliances / Rice Cookers and Steamers");
		categories.add("Kitchen / Small Appliances / Specialty Electrics");
		categories.add("Kitchen / Small Appliances / Toaster Ovens");
		categories.add("Kitchen / Small Appliances / Toasters");
		categories.add("Kitchen / Small Appliances / Waffle Makers");
		categories.add("Magazines");
		categories.add("Mobile Phone");
		categories.add("Mobile Phone Accessories");
		categories.add("Musical Instruments");
		categories.add("Nutrition");
		categories.add("Office / Office Machines and Supplies / Calculators");
		categories
				.add("Office / Office Machines and Supplies / Cartridges and Toners");
		categories.add("Office / Office Machines and Supplies / Copiers");
		categories
				.add("Office / Office Machines and Supplies / Electronic Dictionaries and Translators");
		categories.add("Office / Office Machines and Supplies / Fax Machines");
		categories
				.add("Office / Office Machines and Supplies / Slide Projectors");
		categories
				.add("Office / Office Machines and Supplies / Typewriters and Word Processors");
		categories
				.add("Office / Presentation Equipment / Multimedia Projectors");
		categories.add("Office / Presentation Equipment / Overhead Projectors");
		categories.add("Office / Shredders");
		categories.add("Office / Voice Recorders and Transcribers");
		categories.add("Office Supplies");
		categories.add("Personal Care");
		categories.add("Software");
		categories.add("Spirits");
		categories.add("Sports and Outdoors");
		categories.add("Tools and Hardware");
		categories.add("Toys and Games");
		categories.add("Video Games / Classic Consoles Games");
		categories.add("Video Games / Computer Games");
		categories.add("Video Games / DS Games");
		categories.add("Video Games / Dreamcast Accessories");
		categories.add("Video Games / Dreamcast Games");
		categories.add("Video Games / Game Boy Advance Accessories");
		categories.add("Video Games / Game Boy Advance Games");
		categories.add("Video Games / Game Boy Color Accessories");
		categories.add("Video Games / Game Boy Color Games");
		categories.add("Video Games / GameCube Accessories");
		categories.add("Video Games / GameCube Games");
		categories.add("Video Games / N Gage Accessories");
		categories.add("Video Games / N Gage Games");
		categories.add("Video Games / Nintendo 64 Accessories");
		categories.add("Video Games / Nintendo 64 Games");
		categories.add("Video Games / PSP Games");
		categories.add("Video Games / PlayStation 2 Accessories");
		categories.add("Video Games / PlayStation 2 Games");
		categories.add("Video Games / PlayStation Accessories");
		categories.add("Video Games / PlayStation Games");
		categories.add("Video Games / Video Game Consoles");
		categories.add("Video Games / Xbox Accessories");
		categories.add("Video Games / Xbox Games");
		categories.add("Wine");
		categories.add("Wine and Gifts / Chocolates and Sweets");
		categories
				.add("Wine and Gifts / Flowers Gifts and Hampers / Flowers and Plants");
		categories
				.add("Wine and Gifts / Flowers Gifts and Hampers / Gifts and Hampers");
		categories.add("Wine and Gifts / Food");

		Collections.sort(categories);
	}

	public static List<String> getCategories() {
		return categories;
	}
}