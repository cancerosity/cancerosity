package org.sevensoft.ecreator.model.feeds.categories;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 8 Jun 2006 17:08:44
 *
 */
@Table("feeds_generic_categories")
public class GenericCategory extends EntityObject {

	/**
	 * The FULL ID owner
	 */
	private String	feed;

	/**
	 * The generic category 
	 */
	private Category	category;

	public GenericCategory(RequestContext context) {
		super(context);
	}

	public GenericCategory(RequestContext context, ImportFeed feed, Category category) {

		super(context);

		if (!feed.getGenericCategories().contains(category)) {

			this.feed = feed.getFullId();
			this.category = category;

			save();

		}
	}
}
