package org.sevensoft.ecreator.model.feeds.bespoke.prices.edir;

import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.StreamResult;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.Ftp;
import org.sevensoft.ecreator.model.feeds.ExportFeed;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.iface.frontend.items.feeds.EdirectoryFeedExportHandler;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.EdirectoryFeedHandler;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.jdom.JDOMException;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;

import com.enterprisedt.net.ftp.FTPException;
import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.CsvSaver;

/**
 * @author Dmitry Lebedev
 *         Date: 27.10.2009
 */
@Table("feeds_edirectory")
@HandlerClass(EdirectoryFeedHandler.class)
@Label("Edirectory feed")
@Ftp
public class EdirectoryFeed extends ExportFeed {

    private Attribute manufacturerAttribute;
    private Attribute modelAttribute;
    private Attribute weightAttribute;
    private ItemType itemType;
    private int optionName1;
    private int optionName2;
    private int optionName3;
    private int optionName4;
    private String email;

    public EdirectoryFeed(RequestContext context) {
        super(context);
    }

    public EdirectoryFeed(RequestContext context, boolean create) {
        super(context, create);
    }

    private List doRows(Item item) {
        List rows = new ArrayList();
        ItemOption option1 = item.getOptionSet().getOption(optionName1);
        ItemOption option2 = item.getOptionSet().getOption(optionName2);
        ItemOption option3 = item.getOptionSet().getOption(optionName3);
        ItemOption option4 = item.getOptionSet().getOption(optionName4);
        logger.info((new StringBuilder()).append("option1=").append(option1).append(", option2=").append(option2).append(", option3=").append(option3).append(", option4=").append(option4).toString());
        List selections1;
        if (option1 == null)
            selections1 = Collections.singletonList(null);
        else
            selections1 = option1.getSelections();
        List selections2;
        if (option2 == null)
            selections2 = Collections.singletonList(null);
        else
            selections2 = option2.getSelections();
        List selections3;
        if (option3 == null)
            selections3 = Collections.singletonList(null);
        else
            selections3 = option3.getSelections();
        List selections4;
        if (option4 == null)
            selections4 = Collections.singletonList(null);
        else
            selections4 = option4.getSelections();
        for (Iterator i1 = selections1.iterator(); i1.hasNext();) {
            ItemOptionSelection selection1 = (ItemOptionSelection) i1.next();
            Iterator i2 = selections2.iterator();
            while (i2.hasNext()) {
                ItemOptionSelection selection2 = (ItemOptionSelection) i2.next();
                Iterator i3 = selections3.iterator();
                while (i3.hasNext()) {
                    ItemOptionSelection selection3 = (ItemOptionSelection) i3.next();
                    Iterator i4 = selections4.iterator();
                    while (i4.hasNext()) {
                        ItemOptionSelection selection4 = (ItemOptionSelection) i4.next();
                        String row[] = new String[43];
                        row[0] = "##";
                        row[1] = item.getIdString();
                        row[2] = item.getName();
                        String content = item.getContent();
                        if (content != null) {
                            content = content.replaceAll("<!--[\\x00-\\x7F]+?-->", "");
                        }
                        row[3] = content;
                        row[4] = selection1 != null ? selection1.getText() : null;
                        row[5] = selection2 != null ? selection2.getText() : null;
                        row[6] = selection3 != null ? selection3.getText() : null;
                        row[7] = selection4 != null ? selection4.getText() : null;
                        row[8] = null;
                        row[9] = null;
                        String manuf = item.getAttributeValue("manufacturer");
                        String model = item.getAttributeValue("model");
                        row[10] = manuf;
                        row[11] = model;
                        List images = item.getAllImages();
                        if (images.size() > 0) {
                            Img img = (Img) images.get(0);
                            if (img != null)
                                row[13] = img.getUrl();
                        }
                        if (images.size() > 1) {
                            Img img = (Img) images.get(1);
                            if (img != null)
                                row[15] = img.getUrl();
                        }
                        if (images.size() > 2) {
                            Img img = (Img) images.get(2);
                            if (img != null)
                                row[16] = img.getUrl();
                        }
                        String keywords = item.getKeywords();
                        if (keywords != null) {
                            keywords = keywords.replace(",", " ");
                        }
                        row[19] = keywords;
                        row[20] = "G";
                        Money sellPrice = item.getSellPriceInc();
                        if (sellPrice != null) {
                            if (sellPrice.getAmount() != 0) {
                                row[21] = sellPrice.toEditString();
                            } else {
                                Money costPrice = item.getCostPrice();
                                if (costPrice != null) {
                                    row[21] = costPrice.toEditString();
                                }
                            }
                        }
                        row[25] = "0";
                        row[28] = "5";
                        if (item.getAttributeValue(weightAttribute) != null) {
                            double weight = new Double(item.getAttributeValue(weightAttribute));
                            weight = weight / 1000;
                            row[30] = Double.toString(weight);
                        } else {
                            row[30] = "";
                        }
                        row[42] = "##";
                        logger.info((new StringBuilder()).append("row built=").append(row).toString());
                        rows.add(row);
                    }
                }
            }
        }

        logger.info((new StringBuilder()).append("returning rows=").append(rows).toString());
        return rows;
    }

    public String getExportUrl() {
        return (new StringBuilder()).append(Config.getInstance(context).getUrl()).append("/").append(new Link(EdirectoryFeedExportHandler.class, null, new Object[]{
                "feed", getIdString()
        })).toString();
    }

    public String getPreviewUrl() {
        return (new StringBuilder()).append(Config.getInstance(context).getUrl()).append("/").append(new Link(EdirectoryFeedExportHandler.class, null, new Object[]{
                "feed", getIdString(), "previewCount", 10
        })).toString();
    }

    public ItemType getItemType() {
        return (ItemType) (itemType != null ? itemType.pop() : null);
    }

    public Attribute getManufacturerAttribute() {
        return (Attribute) (manufacturerAttribute != null ? manufacturerAttribute.pop() : null);
    }

    public Attribute getModelAttribute() {
        return (Attribute) (modelAttribute != null ? modelAttribute.pop() : null);
    }

    public int getOptionName1() {
        return optionName1;
    }

    public int getOptionName2() {
        return optionName2;
    }

    public int getOptionName3() {
        return optionName3;
    }

    public int getOptionName4() {
        return optionName4;
    }

    public String getEmail() {
        return email;
    }

    public Attribute getWeightAttribute() {
        return weightAttribute;
    }

    public boolean hasEmail() {
        return email != null;
    }

    public boolean hasItemType() {
        return itemType != null;
    }

    public void removeAttribute(Attribute attribute1) {
    }

    public FeedResult run(File file)
            throws IOException, FTPException, FeedException, JDOMException {
        if (itemType == null)
            throw new FeedException("no item type set");
        ItemSearcher searcher = new ItemSearcher(context);
        searcher.setStatus("LIVE");
        searcher.setItemType(itemType);
        searcher.setLimit(getPreviewCount());
        CsvManager csvman = new CsvManager();
        if (file == null) {
            file = File.createTempFile("edirectory", ".csv");
        }
        CsvSaver saver = csvman.makeSaver(file);
        saver.begin();
        for (Iterator i1 = searcher.iterator(); i1.hasNext();) {
            Item item = (Item) i1.next();
            List rows = doRows(item);
            Iterator i2 = rows.iterator();
            while (i2.hasNext()) {
                String row[] = (String[]) i2.next();
                saver.next(row);
            }
        }

        saver.end();

        if (hasFtpDetails()) {
            this.exportFtp(file);
        }

        if (hasEmail()) {
            Email mail = new Email(Config.getInstance(context).getMailboxEmail(), "Edirectory feed", "Edirectory feed");
            mail.addAttachment(file);
            mail.setTo(email);
            try {
                new EmailDecorator(context, mail).send(Config.getInstance(context).getSmtpHostname());
            } catch (EmailAddressException e) {
                e.printStackTrace();
            } catch (SmtpServerException e) {
                e.printStackTrace();
            }
        }

        return new FeedResult(csvman);
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public void setManufacturerAttribute(Attribute manufacturerAttribute) {
        this.manufacturerAttribute = manufacturerAttribute;
    }

    public void setModelAttribute(Attribute modelAttribute) {
        this.modelAttribute = modelAttribute;
    }

    public void setOptionName1(int option1) {
        optionName1 = option1;
    }

    public void setOptionName2(int option2) {
        optionName2 = option2;
    }

    public void setOptionName3(int option3) {
        optionName3 = option3;
    }

    public void setOptionName4(int option4) {
        optionName4 = option4;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setWeightAttribute(Attribute weightAttribute) {
        this.weightAttribute = weightAttribute;
    }
}
