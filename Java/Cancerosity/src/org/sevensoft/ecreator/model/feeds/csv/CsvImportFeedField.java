package org.sevensoft.ecreator.model.feeds.csv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 28 Jun 2006 16:14:37
 *
 */
@Table("feeds_custom_fields")
public class CsvImportFeedField extends EntityObject implements Comparable<CsvImportFeedField>, Positionable {

	@SuppressWarnings("hiding")
	private static final Logger	logger	= Logger.getLogger("feeds");
    public static final String WHITESPACE_SEPARATOR = "-64";

	/**
	 * 
	 */
	private Attribute			attribute;

	/**
	 * Sep for combining fields
	 */
	private String			separator;

	/**
	 * 
	 */
	private FieldType			fieldType;

	/**
	 * Combine multi columns into one
	 */
	private boolean			combine;

	/**
	 * If flagged then each time this field runs it will set this value again
	 */
	@Default("1")
	private boolean			overwrite;

	/**
	 * Use the item's reference as the value for this field.
	 */
	private boolean			valueFromReference;

	/**
	 * Columns to check field against. STARTS FROM 1
	 */
	private List<Integer>		columns;

	/**
	 * The feed this field is assigend to
	 */
	private CsvImportFeed		feed;

	/**
	 * The format of the date we are importing
	 */
	private String			dateFormat;

	/**
	 * When a value is set, rather than use this field as a processor from the input data, will set the field to the value supplied here 
	 */
	private String			value;

	/**
	 * Prefix all values for this field with this value before resolving
	 */
	private String			prefix;

	/**
	 * 
	 */
	private String			suffix;

	/**
	 * A list of columns whose headers we want to suffix if the column has a value used by this field
	 */
	private List<Integer>		suffixHeaderColumns;

	/**
	 * Lookup items by this attribute only
	 */
	private boolean			individualLookup;

	/**
	 * Look for a filename that contains the value, doesn't have to be complete name.
	 */
	private boolean			partialImageMatch;

	/**
	 * Strip these characters from the value before processing
	 */
	private String			stripCharacters;

	/**
	 * Lookup item as a combination of all combination look up fields
	 */
	private boolean			combinationLookup;

    /**
     * Look for a filename that contains the value, doesn't have to be complete name.
     */
    private boolean partialPdfMatch;

    private CsvExportFeed exFeed;

    private int position;

    private String          parent;

    private String          child;

    public CsvImportFeedField(RequestContext context) {
		super(context);
	}

	CsvImportFeedField(RequestContext context, CsvImportFeed feed, FieldType fieldType) {
		super(context);
		this.feed = feed;
		this.fieldType = fieldType;
		this.overwrite = true;

		save();
	}

    CsvImportFeedField(RequestContext context, CsvExportFeed feed) {
        super(context);
        this.exFeed = feed;
        this.overwrite = true;

        save();
    }

    public int compareTo(CsvImportFeedField o) {

		List<Integer> cols1 = getColumns();
		List<Integer> cols2 = o.getColumns();

		if (cols1.isEmpty() && cols2.isEmpty()) {
			return getFieldType().compareTo(o.getFieldType());
		}

		if (cols1.isEmpty()) {
			return 1;
		}

		if (cols2.isEmpty()) {
			return -1;
		}

		return cols1.get(0).compareTo(cols2.get(0));

	}

	public final Attribute getAttribute() {
		return (Attribute) (attribute == null ? null : attribute.pop());
	}

    public String getChild() {
        return child;
    }

    public final List<Integer> getColumns() {
		if (columns == null) {
			columns = new ArrayList();
		}
//		Collections.sort(columns);
		return columns;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public final CsvImportFeed getFeed() {
		return feed.pop();
	}

	public final FieldType getFieldType() {
		return fieldType == null ? FieldType.Name : fieldType;
	}

    public String getParent() {
        return parent;
    }

    public String getPrefix() {
		return prefix;
	}

	public final String getSeparator() {
		return separator;
	}

	public final String getStripCharacters() {
		return stripCharacters;
	}

	public String getSuffix() {
		return suffix;
	}

	public final List<Integer> getSuffixHeaderColumns() {
		if (suffixHeaderColumns == null) {
			suffixHeaderColumns = new ArrayList();
		}
		return suffixHeaderColumns;
	}

	public String getValue() {
		return value;
	}

	/**
	 * returns the first value for this row and field type or null if no value exists
	 */
	public String getValue(String[] row) {
		List<String> values = getValues(row);
		return values == null || values.isEmpty() ? null : values.get(0);
	}

	public List<String> getValues(String[] row) {

		logger.fine("[CustomFeedField] getting values for fieldType=" + fieldType + " from row=" + Arrays.toString(row));

        if (getParent() != null && getChild() != null) {

            List<String> values = new ArrayList();

            values.add(row[new Integer(getParent()).intValue() - 1]);
            values.add(row[new Integer(getChild()).intValue() - 1]);

            return values;
        }

        if (isValueFromReference()) {

			String value = getFeed().getRefValue(row);

			if (prefix != null) {
				value = prefix + value;
			}

			if (suffix != null) {
				value = value + suffix;
			}

			logger.fine("[CustomFeedField] value from reference=" + value);

			return Collections.singletonList(value);
		}

		List<String> values = new ArrayList();

		for (int i : getColumns()) {

			if (i > row.length) {
				logger.fine("[CustomFeedField] column " + i + " bigger than row data");
				continue;
			}
            String value;

            if (new Integer(i) < 1) {
                value = String.valueOf((char) (i + 96));
                if (value.equals(" ")) {
                    values.add(value);
                    continue;
                }
            } else {
                value = row[i - 1];
            }
            logger.fine("[CustomFeedField] column=" + i + " value=" + value);
			if (value == null) {
				continue;
			}

			value = value.trim();
			if (value.length() == 0) {
				continue;
			}

			// strip characters from value
			if (stripCharacters != null) {
				for (char c : stripCharacters.toCharArray()) {
					value = value.replace(String.valueOf(c), "");
				}
			}

			// if we are not combing then add prefix / suffix to each value
			if (!isCombine()) {

				if (prefix != null) {
					value = prefix + value;
				}

				if (suffix != null) {
					value = value + suffix;
				}
			}

			values.add(value);
		}

		/*
		 * If combine is set then we will concat all the values and replace with this new value
		 * TO DO - let the user define the separate for concat
		 */
		if (isCombine()) {

			String sep = getSeparator();
			if (sep == null) {
				sep = "";
			}

			String value = StringHelper.implode(values, sep, false);
			logger.fine("[CustomFeedField] combined values=" + value);

			if (prefix != null) {
				value = prefix + value;
			}

			if (suffix != null) {
				value = value + suffix;
			}

			// replace values with this new value
			values = Collections.singletonList(value);
		}

		return values;
	}

	public boolean hasAttribute() {
		return attribute != null;
	}

	public boolean hasPrefix() {
		return prefix != null;
	}

	public boolean hasSuffix() {
		return suffix != null;
	}

	/**
	 * 
	 */
	public boolean hasValue() {
		return value != null;
	}

	public boolean isCombinationLookup() {
		return combinationLookup && attribute != null;
	}

	public boolean isCombine() {
		return combine;
	}

	public boolean isIndividualLookup() {
		return individualLookup;
	}

	public boolean isOverwrite() {
		return overwrite;
	}

	public final boolean isPartialImageMatch() {
		return partialImageMatch;
	}

	public boolean isValueFromReference() {
		return valueFromReference;
	}

    public boolean isPartialPdfMatch() {
        return partialPdfMatch;
    }


    public void process(ImportFeed feed, Item item, String[] row) {

		List<String> values = getValues(row);
		fieldType.process(context, this, item, values, getAttribute(), overwrite, feed);
	}

	public void process(ImportFeed feed, OrderLine line, String[] row) {
		List<String> values = getValues(row);
		fieldType.process(context, feed, this, line, values);
	}

	public void process(Order order, String[] row) {

		List<String> values;
		if (value == null) {

			values = getValues(row);
			if (values.isEmpty()) {
				return;
			}

		} else {
			values = Collections.singletonList(value);
		}

		fieldType.process(context, this, order, values);
	}

	public final void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

    public void setChild(String child) {
        this.child = child;
    }

    public final void setColumns(Collection<Integer> c) {
		this.columns = new ArrayList();
		if (columns != null) {
			this.columns.addAll(c);
		}
	}

	public void setColumns(List<String> strings) {

		this.columns = new ArrayList();
		for (String string : strings) {

			string = string.trim();

			// parse for int first
            try {
                int column = Integer.parseInt(string);
                this.columns.add(column);
            } catch (NumberFormatException e) {

                if (string.length() == 0) {
                    string = " ";
                }
                // try for letter
                if (string.length() == 1) {

                    char c = string.toLowerCase().toCharArray()[0];
                    this.columns.add(c - 96);
                }
            }

		}
	}

	public void setCombinationLookup(boolean combinationLookup) {
		this.combinationLookup = combinationLookup;
	}

	public void setCombine(boolean combine) {
		this.combine = combine;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public final void setFieldType(FieldType fieldType) {
		this.fieldType = fieldType;
	}

	public void setIndividualLookup(boolean lookup) {
		this.individualLookup = lookup;
	}

	public void setOverwrite(boolean overwrite) {
		this.overwrite = overwrite;
	}

    public void setParent(String parent) {
        this.parent = parent;
    }

    public final void setPartialImageMatch(boolean partialImageMatch) {
		this.partialImageMatch = partialImageMatch;
	}

    public void setPartialPdfMatch(boolean partialPdfMatch) {
        this.partialPdfMatch = partialPdfMatch;
    }

    public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public final void setSeparator(String separator) {
		this.separator = separator;
	}

	public final void setStripCharacters(String stripCharacters) {
		this.stripCharacters = stripCharacters;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public final void setSuffixHeaderColumns(List<Integer> suffixHeaderColumns) {
		this.suffixHeaderColumns = suffixHeaderColumns;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setValueFromReference(boolean valueFromReference) {
		this.valueFromReference = valueFromReference;
	}

    public CsvExportFeed getExFeed() {
        return (CsvExportFeed) (exFeed == null ? null : exFeed.pop());
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
	public String toString() {

		switch (getFieldType()) {

		default:
			return getFieldType().toString();

		case Attribute:
			Attribute attribute = getAttribute();
			return attribute == null ? "Attribute: not defined" : attribute.getName();

		}
	}

    public String export(Item item) {
        if (fieldType == FieldType.Attribute) {
            return item.getAttributeValue(attribute);
        }
        return fieldType.process(context, item);
    }

    public String export(Item item, ItemOptionSelection selection) {
        if (fieldType.equals(FieldType.OptionName)) {
            return selection.getLabel();
        }
        if (fieldType.equals(FieldType.OptionReference)) {
            return selection.getOption().getName();
        }
        if (fieldType.equals(FieldType.OptionSellprice)) {
            return selection.getSalePrice().toString();
        }
        if (fieldType.equals(FieldType.OptionStock)) {
            return Integer.toString(selection.getStock());
        }
        if (fieldType == FieldType.Attribute) {
            return item.getAttributeValue(attribute);
        }
        return fieldType.process(context, item);
    }

}
