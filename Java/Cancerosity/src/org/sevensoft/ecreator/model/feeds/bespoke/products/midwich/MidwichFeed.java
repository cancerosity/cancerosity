package org.sevensoft.ecreator.model.feeds.bespoke.products.midwich;

import com.ricebridge.csvman.CsvManager;
import org.jdom.JDOMException;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.products.MidwichFeedHandler;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.ecom.suppliers.Supplier;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.annotations.Ftp;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.modules.Modules;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * @author sks 30-Nov-2005 22:31:46
 */
@Table("feeds_midwich")
@Label("Midwich")
@HandlerClass(MidwichFeedHandler.class)
@Ftp
public class MidwichFeed extends ImportFeed {

    /**
     * Limited to 1000 images per day - so after that might as well not bother wasting bandwidth trying
     */
    private transient int imageCount;

    /**
     * Manufacturer part number attribute
     */
    private Attribute mpnAttribute;

    private Attribute manufAttribute;

    private String account, postcode;

    /**
     *
     */
    private transient Map<String, Category> categoriesCache = new HashMap();

    /**
     *
     */
    private Attribute weightAttribute;
    private ItemType itemType;

    private MidwichFeed(RequestContext context) {
        super(context);
    }

    public MidwichFeed(RequestContext context, boolean create) {
        super(context, create);
    }

    private void accessories(Supplier supplier, String data) {

        CsvManager csvman = new CsvManager();
        csvman.setSeparator("\t");

        List<String[]> rows = csvman.load(data);
        for (String[] row : rows) {

            /*
                * Get product from 2nd column id, and get accessory from 3rd column id
                */
            Item item = supplier.getItem(row[1], "live");
            if (item == null) {
                continue;
            }

            Item accessory = supplier.getItem(row[2], "live");
            if (accessory == null) {
                continue;
            }

            item.addAccessory(accessory);

        }
    }

    public String getAccount() {
        return account;
    }

    public final ItemType getItemType() {
        return (ItemType) (itemType == null ? null : itemType.pop());
    }

    public Attribute getManufAttribute() {
        return (Attribute) (manufAttribute == null ? null : manufAttribute.pop());
    }

    public Attribute getMpnAttribute() {
        return (Attribute) (mpnAttribute == null ? null : mpnAttribute.pop());
    }

    public String getPostcode() {
        return postcode;
    }

    public Attribute getWeightAttribute() {
        return (Attribute) (weightAttribute == null ? null : weightAttribute.pop());
    }

    public boolean hasItemType() {
        return itemType != null;
    }

    private void image(Item item, String midw_part_no, String img_filename) {

        if (item.hasApprovedImages()) {
            return;
        }

        logger.fine("Product does not have image");

        String imagesPath = ResourcesUtils.getRealImagesPath();

        try {

            /*
                * Checking for image filename as the feed provides.
                */
            File file = new File(imagesPath + "/" + img_filename);
            if (file.exists()) {

                logger.fine("Image file exists: " + img_filename);
                item.addImage(img_filename, true, false, false);

            } else {

                /*
                     * Otherwise we should attempt a download
                     */

                logger.fine("Image file does not exist");

                String filename = imageDownload(imagesPath, midw_part_no, img_filename);
                if (filename != null) {
                    item.addImage(filename, true, false, false);
                }

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();

        } catch (ImageLimitException e) {
            e.printStackTrace();
        }

    }

    private String imageDownload(String imagesPath, String midw_part_no, String filename) throws IOException, FileNotFoundException {

        if (account == null || postcode == null) {
            logger.fine("Details for image download not available");
            return null;
        }

        /*
           * Image already exists? If so return it.
           */
        if (ResourcesUtils.getRealImage(filename).exists()) {
            return filename;
        }

        logger.fine("Image retrieval: " + midw_part_no);

        if (midw_part_no == null || midw_part_no.length() < 3)
            return null;

        if (imageCount > 950) {
            logger.fine("Todays image limit reached so skipping");
            return null;
        }

        imageCount++;

        URL url = new URL("http://www.midwich.com/services/g.php?midw_pn=" + midw_part_no + "&account_no=" + account + "&postcode="
                + postcode.replace(" ", "%20"));

        logger.fine("Retrieving image from url: " + url);

        BufferedImage image = ImageIO.read(url);
        logger.fine("Image retrieved: " + image);

        if (image == null)
            return null;

        String fn = filename;
        try {
            ImageIO.write(image, "jpg", ResourcesUtils.getRealImage(fn));
        } catch (RuntimeException e) {
            e.printStackTrace();
        }

        logger.fine("Image written to: " + fn);

        return fn;
    }

    private void initAttributes() {

        if (weightAttribute == null)
            weightAttribute = getItemType().addAttribute("Weight", AttributeType.Numerical);

        if (manufAttribute == null) {
            manufAttribute = getItemType().addAttribute("Manufacturer", AttributeType.Selection);
        }

        if (mpnAttribute == null) {
            mpnAttribute = getItemType().addAttribute("Manuf part number", AttributeType.Text);
        }

        save();

    }

    private void initItemType() throws FeedException {

        if (getItemType() == null)
            throw new FeedException("Item type is not configured.");

        getItemType().addModule(ItemModule.Pricing);
        getItemType().addModule(ItemModule.Stock);

        // turn summaries on
        getItemType().setSummaries(true);
        getItemType().setCostPricing(true);
        getItemType().save();

        Modules modules = Modules.getInstance(context);
        modules.addModule(Module.Suppliers);
        modules.save();
    }

    private FeedResult items(CharSequence data) throws IOException {

        logger.fine("[MidwichFeed] Creating csv manager");

        CsvManager csvman = new CsvManager();
        csvman.setEscape('\\');
        csvman.setQuote('"');

        List<List<String>> rows = csvman.loadAsListsFromString(data.toString());
        if (rows.size() < 2) {
            return null;
        }

        logger.fine("[MidwichFeed] Rows loaded: " + rows.size());

        List<String> labels = rows.remove(0);
        logger.fine("[MidwichFeed] Labels parsed: " + labels + " " + labels.size());

        //Long description which we will use for our content
        final int longDescIndex = labels.indexOf("long_desc");
        final int lead_date_index = labels.indexOf("lead_date");
        final int assembly_no_index = labels.indexOf("assembly_no");

        for (List<String> row : rows) {
            try {
                String lead_date = null, img_id, weight, long_desc = null, stock, list, manu_name, midw_part_no, cost, img_filename, assembly_no = null;

                /*
                * category is a concat of category plus their type of product (mainline, accessories etc).
                * By combining the two we can filter out printers and printer accessories into two separate categories,
                * as they list htem all under printers and separate by type
                */
                String categoryName = row.get(labels.indexOf("category")) + " - " + row.get(labels.indexOf("type"));
                if (getIgnoredCategories().contains(categoryName.toLowerCase())) {

                    logger.fine("category [" + categoryName.toLowerCase() + "] is in ignored list, skipping");
                    continue;
                }

                // a v.short description,
                String stx_desc = row.get(labels.indexOf("stx_desc"));

                // product
                String product_name = row.get(labels.indexOf("product_name"));

                // description
                if (longDescIndex > -1) {
                    long_desc = row.get(longDescIndex);
                }

                // cost price
                cost = row.get(labels.indexOf("cost"));

                // supplier sku
                midw_part_no = row.get(labels.indexOf("midw_part_no"));

                // image id for retrieval from web site
                img_id = row.get(labels.indexOf("img_id"));
                weight = row.get(labels.indexOf("weight"));

                // list is RRP
                list = row.get(labels.indexOf("list"));
                manu_name = row.get(labels.indexOf("manu_name"));
                stock = row.get(labels.indexOf("stock"));

                String manu_part_no = row.get(labels.indexOf("manu_part_no"));
                if (manu_part_no != null && manu_part_no.length() == 0) {
                    manu_part_no = null;
                }

                // filename for image
                img_filename = row.get(labels.indexOf("img_filename"));
                if (img_filename != null && img_filename.length() == 0) {
                    img_filename = null;
                }

                // url to manuf pdf
                String pdf_src = row.get(labels.indexOf("pdf_src"));
                if (pdf_src != null && pdf_src.length() == 0) {
                    pdf_src = null;
                }

                if (assembly_no_index > -1) {
                    assembly_no = row.get(assembly_no_index);
                }

                if (lead_date_index > -1) {
                    lead_date = row.get(lead_date_index);
                }

                String eta = row.get(labels.indexOf("eta"));

                /*
                * no idea the tp_part_no is for
                */
                String tp_part_no = null;
                if (labels.contains("tp_part_no")) {
                    tp_part_no = row.get(labels.indexOf("tp_part_no"));
                }

                logger.fine("[MidwichFeed] -------------------------\nStart of row data\n-------------------");
                logger.fine("[MidwichFeed] stx_desc=" + stx_desc);
                logger.fine("[MidwichFeed] long_desc=" + long_desc);
                logger.fine("[MidwichFeed] cost=" + cost);
                logger.fine("midw_part_no=" + midw_part_no);
                logger.fine("img_id=" + img_id);
                logger.fine("weight=" + weight);
                logger.fine("list=" + list);
                logger.fine("manu_name=" + manu_name);
                logger.fine("stock=" + stock);
                logger.fine("category=" + categoryName);
                logger.fine("manu_part_no=" + manu_part_no);
                logger.fine("img_filename=" + img_filename);
                logger.fine("pdf_src=" + pdf_src);
                logger.fine("assembly_no=" + assembly_no);
                logger.fine("eta=" + eta);
                logger.fine("lead_date=" + lead_date);
                logger.fine("product_name=" + product_name);
                logger.fine("tp_part_no=" + tp_part_no);
                logger.fine("-------------------------\nEnd of row data\n-------------------");
                logger.fine("Looking up item");

                // lookup item by supplier
                Item item = getSupplier(true).getItem(midw_part_no, null);

                // if not found by supplier sku then lookup by a combination of mpn and manuf
                if (item == null) {

                    MultiValueMap<Attribute, String> map = new MultiValueMap(new HashMap());

                    map.put(mpnAttribute, manu_part_no != null ? manu_part_no : "");
                    map.put(manufAttribute, manu_name);

                    ItemSearcher search = new ItemSearcher(context);
                    search.setAttributeValues(map);
                    item = search.getItem();

                    if (item != null) {
                        item.setSupplierSku(getSupplier(true), midw_part_no);
                    }
                }

                //String name = stx_desc;
                String name = product_name;
                if (manu_name != null) {
                    name = manu_name + " " + name;
                }

                /*
                * If item is null then we need to create it
                */
                if (item == null) {

                    logger.fine("[MidwichFeed] Creating item with concat of manuf and prod name: " + name);
                    item = super.createItem(getItemType(), name);

                    item.setContent(long_desc);
                    item.setSupplierSku(getSupplier(true), midw_part_no);

                    if (weightAttribute != null) {
                        item.addAttributeValue(weightAttribute, weight);
                    }

                    logger.fine("[MidwichFeed] Item created: " + item);

                    Category category = getCategory(categoryName, categoryName);

                    logger.fine("[MidwichFeed] adding cat: " + category);
                    item.addCategory(category);

                }

                if (pdf_src != null) {

                    /*
                    * Add pdf as attachment if it is not already added.
                    */
                    if (!item.hasAttachment(pdf_src)) {

                        try {

                            if (!pdf_src.startsWith("http")) {
                                pdf_src = "http://" + pdf_src;
                            }

                            item.addAttachment(new URL(pdf_src));

                        } catch (AttachmentTypeException e) {
                            e.printStackTrace();

                        } catch (AttachmentExistsException e) {
                            e.printStackTrace();

                        } catch (IOException e) {
                            e.printStackTrace();

                        } catch (AttachmentLimitException e) {
                            e.printStackTrace();
                        }
                    }

                }

                // stock
                {
                    logger.fine("[MidwichFeed] Updating stock: " + stock);

                    item.setSupplierStock(getSupplier(true), Integer.parseInt(stock));
                }

                /*
                * Costing and RRP
                */
                {
                    logger.fine("[MidwichFeed] Updating rrp: " + list);
                    item.setRrp(new Money(list));

                    logger.fine("[MidwichFeed] Updating cost: " + cost);
                    item.setSupplierCostPrice(getSupplier(true), new Money(cost));
                }

                if (manu_name != null) {
                    setAttribute(item, manufAttribute, manu_name);
                }

                if (manu_part_no != null) {
                    setAttribute(item, mpnAttribute, manu_part_no);
                }

                if (lead_date != null) {
                    item.setOutStockMsg("Due in " + lead_date);
                }

                if (isResetName()) {
                    item.setName(name);
                }

                /*
                * Set short description as summary
                */
                item.setSummary(stx_desc);
                item.save();

                image(item, midw_part_no, img_filename);
            } catch (Exception e) {
                logger.log(Level.SEVERE, "[MidwichFeed] Error processing row: ", e);
            }
        }

        return new FeedResult(csvman);
    }

    private void obsolete(Supplier supplier, String obsolete) {

        logger.fine("[MidwichFeed] Beginning baryon sweep of obsoleted items :)");

        CsvManager csvman = new CsvManager();
        csvman.setEscape('\\');
        csvman.setQuote('"');

        List<List<String>> rows = csvman.loadAsListsFromString(obsolete);
        for (List<String> row : rows) {

            String midw_part_no = row.get(0);
            logger.fine("[MidwichFeed] Attempting to locate item with midwich sku: " + midw_part_no);

            // lookup product by midwich code which is our supplier sku
            Item item = supplier.getItem(midw_part_no, "live");
            if (item != null) {

                logger.fine("[MidwichFeed] Obsoleteing item: " + item.getId() + " " + item.getName());
                item.delete();

            } else {
                logger.fine("[MidwichFeed] Item not found");
            }
        }
    }

    @Override
    public void removeAttribute(Attribute attribute) {

        if (attribute.equals(manufAttribute)) {
            manufAttribute = null;
        }

        if (attribute.equals(weightAttribute)) {
            weightAttribute = null;
        }

        if (attribute.equals(mpnAttribute)) {
            mpnAttribute = null;
        }

        save();
    }

    public FeedResult run(File file) throws IOException, FeedException, JDOMException {

        updateFeedCount();

        if (file == null) {
            file = getInputFile();
        }

        if (file == null) {
            throw new FeedException("No data supplied");
        }

        initItemType();
        initSupplier("Midwich");

        initAttributes();

        logger.fine("[MidwichFeed] Running midwich feed with debug on");

        String products, obsolete, relations = null;

        ZipFile zipFile = null;

        try {

            zipFile = new ZipFile(file);
            ZipEntry entry = zipFile.getEntry("Midwich.csv");
            if (entry == null) {
                throw new FeedException("Midwich.csv not found in data.zip");
            }
            logger.fine("[MidwichFeed] Midwich.csv located in zip: " + entry);

            products = SimpleFile.readString(zipFile.getInputStream(entry));
            logger.fine("[MidwichFeed] Midwich.csv loaded into memory: " + products.length() + " characters");

            entry = zipFile.getEntry("obsolete_parts.csv");
            if (entry == null) {

                logger.fine("[MidwichFeed] obsolete_parts.csv not available");

            } else {

                logger.fine("[MidwichFeed] obsolete_parts.csv located in zip: " + entry);

                obsolete = SimpleFile.readString(zipFile.getInputStream(entry));
                logger.fine("[MidwichFeed] obsolete_parts.csv loaded into memory: " + obsolete.length() + " characters");

                obsolete(getSupplier(true), obsolete);

            }

            /*
                * Get product relations file
                */
            entry = zipFile.getEntry("product_relations.csv");
            if (entry == null) {

                logger.fine("[MidwichFeed] product_relations.csv located in zip: " + entry);

            } else {

                relations = SimpleFile.readString(zipFile.getInputStream(entry));
                logger.fine("[MidwichFeed] product_relations.csv loaded into memory: " + relations.length() + " characters");

                logger.fine("[MidwichFeed] product_relations.csv not located in zip");
            }

        } catch (IOException e) {
            throw e;

        } catch (FeedException e) {
            throw e;

        } finally {

            if (zipFile != null) {
                zipFile.close();
            }

        }

        FeedResult result = items(products);

        if (Module.Accessories.enabled(context) && relations != null) {
            accessories(getSupplier(true), relations);
        }

        return result;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    private void setAttribute(Item item, Attribute attribute, String value) {

        if (attribute == null) {
            return;
        }

        logger.fine("[MidwichFeed] Setting attribute=" + attribute.getName() + " to value=" + value);

        if (value == null) {
            return;
        }

        item.setAttributeValue(attribute, value);
    }

    public final void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public void setManufAttribute(Attribute manufAttribute) {
        this.manufAttribute = manufAttribute;
    }

    public void setMpnAttribute(Attribute mpnAttribute) {
        this.mpnAttribute = mpnAttribute;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public void setWeightAttribute(Attribute weightAttribute) {
        this.weightAttribute = weightAttribute;
    }

}
