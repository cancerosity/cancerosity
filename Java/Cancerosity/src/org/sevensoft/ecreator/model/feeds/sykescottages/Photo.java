package org.sevensoft.ecreator.model.feeds.sykescottages;

import org.jdom.Element;

/**
 * User: Tanya
 * Date: 10.08.2011
 */
public class Photo {
    private Element element;

    public Photo(Element element) {
        this.element = element;
    }
    /**
     * <photo>

     * <tUpdated>2009-01-23</tUpdated>
     * </photo>
     */

    /**
     * AttributeType.Text
     * <nsykesid>46</nsykesid>
     */
    private String nsykesid;

    public String getNsykesid() {
        return element.getChildTextTrim("nsykesid");
    }

    /**
     * <cURL>http://cottageimages.sykescottages.co.uk/1000_01.jpg</cURL>
     */
    private String url;

    public String getUrl() {
        return element.getChildTextTrim("cURL");
    }

    /**
     * <nPosition>1</nPosition>
     */
    private Integer position;

}
