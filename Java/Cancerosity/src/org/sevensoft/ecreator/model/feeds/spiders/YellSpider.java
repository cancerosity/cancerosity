package org.sevensoft.ecreator.model.feeds.spiders;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.httpc.HttpMethod;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 4 Dec 2006 21:16:09
 *
 */
public class YellSpider extends Spider {

	@Override
	public String getName() {
		return "Yell";
	}

	Pattern	postcodePattern		= Pattern.compile(".*$");
	Pattern	leadingSpaceRemove	= Pattern.compile("^\\s{1,}", Pattern.MULTILINE);

	@Override
	public int spider(RequestContext context, ItemType itemType, String url, Category category) throws MalformedURLException, IOException {

		int created = 0;

		Attribute addressAttribute = itemType.getAttribute("Address", AttributeType.Text);
		Attribute telAttribute = itemType.getAttribute("Telephone", AttributeType.Text);
		Attribute postcodeAttribute = itemType.getAttribute("Postcode", AttributeType.Postcode);

		HttpClient hc = new HttpClient(url, HttpMethod.Get);
		hc.connect();
		String response = hc.getResponseString();

		Pattern pattern = Pattern
				.compile(
						"<div class=\"fle-companyname\">.*?<h2>(.*?)</h2>.*?<div class=\"address\">(.*?)</div>.*?<div class=\"tel\">Tel: <strong>(.*?)</strong>",
						Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

		//		Pattern pattern = Pattern.compile("<div class=\"fle-companyname\">(.*?)address book", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		Matcher matcher = pattern.matcher(response);
		while (matcher.find()) {

			String postcode = null;
			String address = matcher.group(2).trim();
			String tel = matcher.group(3).trim();
			String name = matcher.group(1).trim();

			// get last line as postcode

			Matcher matcher2 = postcodePattern.matcher(address);

			if (matcher2.find()) {
				postcode = matcher2.group().trim().toUpperCase();
				address = matcher2.replaceAll("");
			}

			// strip all multiple spaces (2 or more) and all new lines, then replace all commas with new lines
			address = address.replaceAll("\\s{2,}", "").replaceAll("\n", "").replaceAll(",\\s{1,}", ",").replace(",", "\n");

			// replace &nbsp;
			tel = tel.replace("&nbsp;", " ");

			logger.fine("[YellSpider] name=" + name);
			logger.fine("[YellSpider] tel=" + tel);
			logger.fine("[YellSpider] address=" + address);
			logger.fine("[YellSpider] postcode=" + postcode);

			if (name != null) {

				Item item = new Item(context, itemType, name, "Live", null);
				item.setAttributeValue(addressAttribute, address);
				item.setAttributeValue(telAttribute, tel);
				item.setAttributeValue(postcodeAttribute, postcode);

				if (category != null) {
					item.addCategory(category);
				}

				created++;
			}

		}

		return created;
	}
}
