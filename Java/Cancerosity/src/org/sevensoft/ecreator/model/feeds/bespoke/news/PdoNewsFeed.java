package org.sevensoft.ecreator.model.feeds.bespoke.news;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.news.PdoNewsFeedHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.Http;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

import com.enterprisedt.net.ftp.FTPException;

/**
 * @author sks 14 Feb 2007 14:09:21
 *
 */
@Table("feeds_pdo_news")
@HandlerClass(PdoNewsFeedHandler.class)
@Label("PDO News")
@Http
public class PdoNewsFeed extends ImportFeed {

	private Attribute	authorAttribute;
	private ItemType	itemType;

	public PdoNewsFeed(RequestContext context) {
		super(context);
	}

	public PdoNewsFeed(RequestContext context, boolean create) {
		super(context, create);
	}

	public Attribute getAuthorAttribute() {
		return (Attribute) (authorAttribute == null ? null : authorAttribute.pop());
	}

	public ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public boolean hasItemType() {
		return itemType != null;
	}

	@Override
	public void removeAttribute(Attribute attribute) {

		if (attribute.equals(authorAttribute)) {
			this.authorAttribute = null;
			save();
		}

	}

	@Override
	public FeedResult run(File file) throws IOException, FTPException, FeedException, JDOMException {

		if (itemType == null) {
			throw new FeedException("No item type set");
		}

		if (file == null) {
			file = getInputFile();
		}

		SAXBuilder builder = new SAXBuilder();
		builder.setValidation(false);
		builder.setExpandEntities(false);
		builder.setIgnoringElementContentWhitespace(true);
		Document doc = builder.build(file);
		Element root = doc.getRootElement();

		List<Element> newsEls = root.getChildren("News");
		for (Element newsEl : newsEls) {

			String headline = newsEl.getChildTextTrim("Headline");
			String author = newsEl.getChildTextTrim("Author");
			String timestamp = newsEl.getChildTextTrim("Timestamp");
			String body = newsEl.getChildTextTrim("Body");
			String id = newsEl.getChildTextTrim("Id");

			logger.fine("[PdoNewsFeed] headline=" + headline);
			logger.fine("[PdoNewsFeed] id=" + id + ", author=" + author + ", timestamp=" + timestamp);
			logger.fine("[PdoNewsFeed] body=" + body);

			Item item = Item.getByRef(context, getItemType(), id);
			if (item == null) {

				item = super.createItem(getItemType(), headline);
				item.setAttributeValue(getAuthorAttribute(), author);
				try {
					item.setDateCreated(new DateTime(timestamp + "000"));
				} catch (ParseException e) {
					e.printStackTrace();
				}

				item.setContent(body);
				item.setReference(id);

				item.save();

				logger.fine("[PdoNewsFeed] item created=" + item);
			}
		}

		return null;
	}

	public void setAuthorAttribute(Attribute authorAttribute) {
		this.authorAttribute = authorAttribute;
	}

	public void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

}
