package org.sevensoft.ecreator.model.feeds.categories;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 11 Jul 2006 18:23:18
 * 
 * A feed category mapping maps the raw name of a category from a feed to a category name on our system
 *
 */
@Table("feeds_category_mappings")
public class CategoryReference extends EntityObject {

	/**
	 * The feed this mapping maps from
	 * The fullID Owner
	 */
	private String	feed;

	/**
	 * The category to map this raw name to
	 */
	private Category	category;

	/**
	 * The reference name of the category in the feed
	 */
	private String	name;

	protected CategoryReference(RequestContext context) {
		super(context);
	}

	public CategoryReference(RequestContext context, ImportFeed feed, Category category, String name) {

		super(context);

		this.feed = feed.getFullId();
		this.category = category;
		this.name = name;

		save();
	}

	public Category getCategory() {
		return category.pop();
	}

	public final String getFeed() {
		return feed;
	}

	public String getName() {
		return name;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

}
