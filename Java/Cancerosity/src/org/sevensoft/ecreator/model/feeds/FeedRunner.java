package org.sevensoft.ecreator.model.feeds;

import com.enterprisedt.net.ftp.FTPException;
import org.jdom.JDOMException;
import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.ecreator.model.misc.Interval;
import org.sevensoft.ecreator.model.system.messages.SystemMessage;
import org.sevensoft.jeezy.http.RequestContext;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author sks 13 Jun 2006 16:42:21
 */
public class FeedRunner implements Runnable {

    private Logger logger = Logger.getLogger("cron");

    private final Interval interval;
    private final RequestContext context;

    public FeedRunner(RequestContext context, Interval interval) {
        this.context = context;
        this.interval = interval;
    }

    public void run() {

        logger.config("[FeedRunner] running, interval=" + interval);

        List<Feed> feeds = Feed.getAll(context);
        logger.config("[FeedRunner] all feeds=" + feeds);

        CollectionsUtil.filter(feeds, new Predicate<Feed>() {

            public boolean accept(Feed e) {
                return e.getInterval() == interval;
            }
        });
        logger.config("[FeedRunner] filtered feeds=" + feeds);

        for (Feed feed : feeds) {

            logger.config("[FeedRunner] Running feed: " + feed.getName());

            try {

                FeedResult result;
                if (feed instanceof ExportFeed) {

                    // create a temp file for this feed to write to
                    File file = File.createTempFile("feed_export", null);
                    result = feed.run(file);

                    // delete file after use
                    file.delete();

                } else {

                    // we have an import feed so we don't want to pass in a file
                    result = feed.run(null);
                }

                feed.setLastRuntime();

                if (result == null) {
                    new SystemMessage(context, "The " + feed.getName() + " feed was automatically run.");
                } else {
                    new SystemMessage(context, "The " + feed.getName() + " feed was automatically run - " + result.getDetails());
                }

            } catch (IOException e) {
                e.printStackTrace();
                logger.warning(e.toString());

            } catch (FeedException e) {
                e.printStackTrace();
                logger.warning(e.toString());

            } catch (FTPException e) {
                e.printStackTrace();
                logger.warning(e.toString());

            } catch (JDOMException e) {
                e.printStackTrace();
                logger.warning(e.toString());
            } catch (Exception e) {
                e.printStackTrace();
                logger.warning(e.toString());
            }

        }
    }
}
