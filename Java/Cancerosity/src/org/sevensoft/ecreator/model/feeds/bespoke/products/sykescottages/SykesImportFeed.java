package org.sevensoft.ecreator.model.feeds.bespoke.products.sykescottages;

import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.xml.parser.sykescottages.SykesParser;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.Http;
import org.sevensoft.ecreator.model.feeds.sykescottages.Cottage;
import org.sevensoft.ecreator.model.feeds.sykescottages.Photo;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.products.SykesImportFeedHandler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.httpc.HttpMethod;
import org.jdom.JDOMException;
import org.jdom.Element;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.text.ParseException;

import com.enterprisedt.net.ftp.FTPException;

/**
 * User: Tanya
 * Date: 17.08.2011
 */
@Table("feeds_sykes")
@Label("Sykes feed")
@HandlerClass(SykesImportFeedHandler.class)
@Http
public class SykesImportFeed extends ImportFeed {

    private static final String propData = "http://peakdistrictonline.data.sykescottages.co.uk/prop/propDataDump.xml";
    private static final String photoData = "http://peakdistrictonline.data.sykescottages.co.uk/photo/photoDataDump.xml";
//    private static final String pricingData = "pricingDataDump.xml";
//    private static final String availabilityData = "availabilityDataDump.xml";

    private ItemType itemType;

    private boolean setupDone;
    // attributes
    private Attribute areaAttribute;
    private Attribute subareaAttribute;
    private Attribute latitudeAttribute;
    private Attribute longitudeAttribute;
    private Attribute priceinfoAttribute;
    private Attribute minpriceAttribute;
    private Attribute maxpriceAttribute;
    private Attribute bedroomsAttribute;
    private Attribute kettleAttribute;
    private Attribute parkingspaceAttribute;
    private Attribute smokingAttribute;
    private Attribute singlebedsAttribute;
    private Attribute doublebedsAttribute;
    private Attribute twinbedsAttribute;
    private Attribute bunkbedsAttribute;
    private Attribute familybedsAttribute;
    private Attribute maxsleepsAttribute;
    private Attribute proppostcodeAttribute;
    private Attribute propfeedbackAttribute;
    private Attribute tenniscourtAttribute;
    private Attribute petsAttribute;
    private Attribute changeoverdayAttribute;
    private Attribute jacuzziAttribute;
    private Attribute telephoneAttribute;
    private Attribute saunaAttribute;
    private Attribute linenprovidedAttribute;
    private Attribute towelsprovidedAttribute;
    private Attribute dishwasherAttribute;
    private Attribute cookerAttribute;
    private Attribute fridgeAttribute;
    private Attribute washingmachineAttribute;
    private Attribute tvAttribute;
    private Attribute highchairAttribute;
    private Attribute bathroomsAttribute;
    private Attribute cotsAttribute;
    private Attribute henstagAttribute;
    private Attribute childrenAttribute;
    private Attribute gamesroomAttribute;
    private Attribute pingpongAttribute;
    private Attribute snookerAttribute;
    private Attribute wifiAttribute;
    private Attribute webpriceAttribute;
    private Attribute furtherfacilitiesinternalAttribute;
    private Attribute regiondescAttribute;
    private Attribute towndescAttribute;
    private Attribute homesummaryAttribute;
    private Attribute homedescAttribute;
    private Attribute furtherfacilitiesexternalAttribute;
    private Attribute furtherdetailsAttribute;
    private Attribute specialconditionsAttribute;
    private Attribute amenityNotesAttribute;
    private Attribute countryAttribute;
    private Attribute villagenameAttribute;
    private Attribute sykesratingAttribute;
    private Attribute villageidAttribute;
    private Attribute weblinkAttribute;

    private Attribute photoAttribute;


    protected SykesImportFeed(RequestContext context) {
        super(context);
    }

    public SykesImportFeed(RequestContext context, boolean create) {
        super(context, create);
    }

    public final ItemType getItemType() {
        return (ItemType) (itemType == null ? null : itemType.pop());
    }

    public boolean hasItemType() {
        return itemType != null;
    }

    public boolean isSetupDone() {
        return setupDone;
    }

    public Attribute getAreaAttribute() {
        return (Attribute) (areaAttribute == null ? null : areaAttribute.pop());
    }

    public Attribute getSubareaAttribute() {
        return (Attribute) (subareaAttribute == null ? null : subareaAttribute.pop());
    }

    public Attribute getLatitudeAttribute() {
        return (Attribute) (latitudeAttribute == null ? null : latitudeAttribute.pop());
    }

    public Attribute getLongitudeAttribute() {
        return (Attribute) (longitudeAttribute == null ? null : longitudeAttribute.pop());
    }

    public Attribute getPriceinfoAttribute() {
        return (Attribute) (priceinfoAttribute == null ? null : priceinfoAttribute.pop());
    }

    public Attribute getMinpriceAttribute() {
        return (Attribute) (minpriceAttribute == null ? null : minpriceAttribute.pop());
    }

    public Attribute getMaxpriceAttribute() {
        return (Attribute) (maxpriceAttribute == null ? null : maxpriceAttribute.pop());
    }

    public Attribute getBedroomsAttribute() {
        return (Attribute) (bedroomsAttribute == null ? null : bedroomsAttribute.pop());
    }

    public Attribute getKettleAttribute() {
        return (Attribute) (kettleAttribute == null ? null : kettleAttribute.pop());
    }

    public Attribute getParkingspaceAttribute() {
        return (Attribute) (parkingspaceAttribute == null ? null : parkingspaceAttribute.pop());
    }

    public Attribute getSmokingAttribute() {
        return (Attribute) (smokingAttribute == null ? null : smokingAttribute.pop());
    }

    public Attribute getSinglebedsAttribute() {
        return (Attribute) (singlebedsAttribute == null ? null : singlebedsAttribute.pop());
    }

    public Attribute getDoublebedsAttribute() {
        return (Attribute) (doublebedsAttribute == null ? null : doublebedsAttribute.pop());
    }

    public Attribute getTwinbedsAttribute() {
        return (Attribute) (twinbedsAttribute == null ? null : twinbedsAttribute.pop());
    }

    public Attribute getBunkbedsAttribute() {
        return (Attribute) (bunkbedsAttribute == null ? null : bunkbedsAttribute.pop());
    }

    public Attribute getFamilybedsAttribute() {
        return (Attribute) (familybedsAttribute == null ? null : familybedsAttribute.pop());
    }

    public Attribute getMaxsleepsAttribute() {
        return (Attribute) (maxsleepsAttribute == null ? null : maxsleepsAttribute.pop());
    }

    public Attribute getProppostcodeAttribute() {
        return (Attribute) (proppostcodeAttribute == null ? null : proppostcodeAttribute.pop());
    }

    public Attribute getPropfeedbackAttribute() {
        return (Attribute) (propfeedbackAttribute == null ? null : propfeedbackAttribute.pop());
    }

    public Attribute getTenniscourtAttribute() {
        return (Attribute) (tenniscourtAttribute == null ? null : tenniscourtAttribute.pop());
    }

    public Attribute getPetsAttribute() {
        return (Attribute) (petsAttribute == null ? null : petsAttribute.pop());
    }

    public Attribute getChangeoverdayAttribute() {
        return (Attribute) (changeoverdayAttribute == null ? null : changeoverdayAttribute.pop());
    }

    public Attribute getJacuzziAttribute() {
        return (Attribute) (jacuzziAttribute == null ? null : jacuzziAttribute.pop());
    }

    public Attribute getTelephoneAttribute() {
        return (Attribute) (telephoneAttribute == null ? null : telephoneAttribute.pop());
    }

    public Attribute getSaunaAttribute() {
        return (Attribute) (saunaAttribute == null ? null : saunaAttribute.pop());
    }

    public Attribute getLinenprovidedAttribute() {
        return (Attribute) (linenprovidedAttribute == null ? null : linenprovidedAttribute.pop());
    }

    public Attribute getTowelsprovidedAttribute() {
        return (Attribute) (towelsprovidedAttribute == null ? null : towelsprovidedAttribute.pop());
    }

    public Attribute getDishwasherAttribute() {
        return (Attribute) (dishwasherAttribute == null ? null : dishwasherAttribute.pop());
    }

    public Attribute getCookerAttribute() {
        return (Attribute) (cookerAttribute == null ? null : cookerAttribute.pop());
    }

    public Attribute getFridgeAttribute() {
        return (Attribute) (fridgeAttribute == null ? null : fridgeAttribute.pop());
    }

    public Attribute getWashingmachineAttribute() {
        return (Attribute) (washingmachineAttribute == null ? null : washingmachineAttribute.pop());
    }

    public Attribute getTvAttribute() {
        return (Attribute) (tvAttribute == null ? null : tvAttribute.pop());
    }

    public Attribute getHighchairAttribute() {
        return (Attribute) (highchairAttribute == null ? null : highchairAttribute.pop());
    }

    public Attribute getBathroomsAttribute() {
        return (Attribute) (bathroomsAttribute == null ? null : bathroomsAttribute.pop());
    }

    public Attribute getCotsAttribute() {
        return (Attribute) (cotsAttribute == null ? null : cotsAttribute.pop());
    }

    public Attribute getHenstagAttribute() {
        return (Attribute) (henstagAttribute == null ? null : henstagAttribute.pop());
    }

    public Attribute getChildrenAttribute() {
        return (Attribute) (childrenAttribute == null ? null : childrenAttribute.pop());
    }

    public Attribute getGamesroomAttribute() {
        return (Attribute) (gamesroomAttribute == null ? null : gamesroomAttribute.pop());
    }

    public Attribute getPingpongAttribute() {
        return (Attribute) (pingpongAttribute == null ? null : pingpongAttribute.pop());
    }

    public Attribute getSnookerAttribute() {
        return (Attribute) (snookerAttribute == null ? null : snookerAttribute.pop());
    }

    public Attribute getWifiAttribute() {
        return (Attribute) (wifiAttribute == null ? null : wifiAttribute.pop());
    }

    public Attribute getWebpriceAttribute() {
        return (Attribute) (webpriceAttribute == null ? null : webpriceAttribute.pop());
    }

    public Attribute getFurtherfacilitiesinternalAttribute() {
        return (Attribute) (furtherfacilitiesinternalAttribute == null ? null : furtherfacilitiesinternalAttribute.pop());
    }

    public Attribute getRegiondescAttribute() {
        return (Attribute) (regiondescAttribute == null ? null : regiondescAttribute.pop());
    }

    public Attribute getTowndescAttribute() {
        return (Attribute) (towndescAttribute == null ? null : towndescAttribute.pop());
    }

    public Attribute getHomesummaryAttribute() {
        return (Attribute) (homesummaryAttribute == null ? null : homesummaryAttribute.pop());
    }

    public Attribute getHomedescAttribute() {
        return (Attribute) (homedescAttribute == null ? null : homedescAttribute.pop());
    }

    public Attribute getFurtherfacilitiesexternalAttribute() {
        return (Attribute) (furtherfacilitiesexternalAttribute == null ? null : furtherfacilitiesexternalAttribute.pop());
    }

    public Attribute getFurtherdetailsAttribute() {
        return (Attribute) (furtherdetailsAttribute == null ? null : furtherdetailsAttribute.pop());
    }

    public Attribute getSpecialconditionsAttribute() {
        return (Attribute) (specialconditionsAttribute == null ? null : specialconditionsAttribute.pop());
    }

    public Attribute getAmenityNotesAttribute() {
        return (Attribute) (amenityNotesAttribute == null ? null : amenityNotesAttribute.pop());
    }

    public Attribute getCountryAttribute() {
        return (Attribute) (countryAttribute == null ? null : countryAttribute.pop());
    }

    public Attribute getVillagenameAttribute() {
        return (Attribute) (villagenameAttribute == null ? null : villagenameAttribute.pop());
    }

    public Attribute getSykesratingAttribute() {
        return (Attribute) (sykesratingAttribute == null ? null : sykesratingAttribute.pop());
    }

    public Attribute getVillageidAttribute() {
        return (Attribute) (villageidAttribute == null ? null : villageidAttribute.pop());
    }

    public Attribute getWeblinkAttribute() {
        return (Attribute) (weblinkAttribute == null ? null : weblinkAttribute.pop());
    }

    public Attribute getPhotoAttribute() {
        return (Attribute) (photoAttribute == null ? null : photoAttribute.pop());
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public void setSetupDone(boolean setupDone) {
        this.setupDone = setupDone;
    }

    public void setAreaAttribute(Attribute areaAttribute) {
        this.areaAttribute = areaAttribute;
    }

    public void setSubareaAttribute(Attribute subareaAttribute) {
        this.subareaAttribute = subareaAttribute;
    }

    public void setLatitudeAttribute(Attribute latitudeAttribute) {
        this.latitudeAttribute = latitudeAttribute;
    }

    public void setLongitudeAttribute(Attribute longitudeAttribute) {
        this.longitudeAttribute = longitudeAttribute;
    }

    public void setPriceinfoAttribute(Attribute priceinfoAttribute) {
        this.priceinfoAttribute = priceinfoAttribute;
    }

    public void setMinpriceAttribute(Attribute minpriceAttribute) {
        this.minpriceAttribute = minpriceAttribute;
    }

    public void setMaxpriceAttribute(Attribute maxpriceAttribute) {
        this.maxpriceAttribute = maxpriceAttribute;
    }

    public void setBedroomsAttribute(Attribute bedroomsAttribute) {
        this.bedroomsAttribute = bedroomsAttribute;
    }

    public void setKettleAttribute(Attribute kettleAttribute) {
        this.kettleAttribute = kettleAttribute;
    }

    public void setParkingspaceAttribute(Attribute parkingspaceAttribute) {
        this.parkingspaceAttribute = parkingspaceAttribute;
    }

    public void setSmokingAttribute(Attribute smokingAttribute) {
        this.smokingAttribute = smokingAttribute;
    }

    public void setSinglebedsAttribute(Attribute singlebedsAttribute) {
        this.singlebedsAttribute = singlebedsAttribute;
    }

    public void setDoublebedsAttribute(Attribute doublebedsAttribute) {
        this.doublebedsAttribute = doublebedsAttribute;
    }

    public void setTwinbedsAttribute(Attribute twinbedsAttribute) {
        this.twinbedsAttribute = twinbedsAttribute;
    }

    public void setBunkbedsAttribute(Attribute bunkbedsAttribute) {
        this.bunkbedsAttribute = bunkbedsAttribute;
    }

    public void setFamilybedsAttribute(Attribute familybedsAttribute) {
        this.familybedsAttribute = familybedsAttribute;
    }

    public void setMaxsleepsAttribute(Attribute maxsleepsAttribute) {
        this.maxsleepsAttribute = maxsleepsAttribute;
    }

    public void setProppostcodeAttribute(Attribute proppostcodeAttribute) {
        this.proppostcodeAttribute = proppostcodeAttribute;
    }

    public void setPropfeedbackAttribute(Attribute propfeedbackAttribute) {
        this.propfeedbackAttribute = propfeedbackAttribute;
    }

    public void setTenniscourtAttribute(Attribute tenniscourtAttribute) {
        this.tenniscourtAttribute = tenniscourtAttribute;
    }

    public void setPetsAttribute(Attribute petsAttribute) {
        this.petsAttribute = petsAttribute;
    }

    public void setChangeoverdayAttribute(Attribute changeoverdayAttribute) {
        this.changeoverdayAttribute = changeoverdayAttribute;
    }

    public void setJacuzziAttribute(Attribute jacuzziAttribute) {
        this.jacuzziAttribute = jacuzziAttribute;
    }

    public void setTelephoneAttribute(Attribute telephoneAttribute) {
        this.telephoneAttribute = telephoneAttribute;
    }

    public void setSaunaAttribute(Attribute saunaAttribute) {
        this.saunaAttribute = saunaAttribute;
    }

    public void setLinenprovidedAttribute(Attribute linenprovidedAttribute) {
        this.linenprovidedAttribute = linenprovidedAttribute;
    }

    public void setTowelsprovidedAttribute(Attribute towelsprovidedAttribute) {
        this.towelsprovidedAttribute = towelsprovidedAttribute;
    }

    public void setDishwasherAttribute(Attribute dishwasherAttribute) {
        this.dishwasherAttribute = dishwasherAttribute;
    }

    public void setCookerAttribute(Attribute cookerAttribute) {
        this.cookerAttribute = cookerAttribute;
    }

    public void setFridgeAttribute(Attribute fridgeAttribute) {
        this.fridgeAttribute = fridgeAttribute;
    }

    public void setWashingmachineAttribute(Attribute washingmachineAttribute) {
        this.washingmachineAttribute = washingmachineAttribute;
    }

    public void setTvAttribute(Attribute tvAttribute) {
        this.tvAttribute = tvAttribute;
    }

    public void setHighchairAttribute(Attribute highchairAttribute) {
        this.highchairAttribute = highchairAttribute;
    }

    public void setBathroomsAttribute(Attribute bathroomsAttribute) {
        this.bathroomsAttribute = bathroomsAttribute;
    }

    public void setCotsAttribute(Attribute cotsAttribute) {
        this.cotsAttribute = cotsAttribute;
    }

    public void setHenstagAttribute(Attribute henstagAttribute) {
        this.henstagAttribute = henstagAttribute;
    }

    public void setChildrenAttribute(Attribute childrenAttribute) {
        this.childrenAttribute = childrenAttribute;
    }

    public void setGamesroomAttribute(Attribute gamesroomAttribute) {
        this.gamesroomAttribute = gamesroomAttribute;
    }

    public void setPingpongAttribute(Attribute pingpongAttribute) {
        this.pingpongAttribute = pingpongAttribute;
    }

    public void setSnookerAttribute(Attribute snookerAttribute) {
        this.snookerAttribute = snookerAttribute;
    }

    public void setWifiAttribute(Attribute wifiAttribute) {
        this.wifiAttribute = wifiAttribute;
    }

    public void setWebpriceAttribute(Attribute webpriceAttribute) {
        this.webpriceAttribute = webpriceAttribute;
    }

    public void setFurtherfacilitiesinternalAttribute(Attribute furtherfacilitiesinternalAttribute) {
        this.furtherfacilitiesinternalAttribute = furtherfacilitiesinternalAttribute;
    }

    public void setRegiondescAttribute(Attribute regiondescAttribute) {
        this.regiondescAttribute = regiondescAttribute;
    }

    public void setTowndescAttribute(Attribute towndescAttribute) {
        this.towndescAttribute = towndescAttribute;
    }

    public void setHomesummaryAttribute(Attribute homesummaryAttribute) {
        this.homesummaryAttribute = homesummaryAttribute;
    }

    public void setHomedescAttribute(Attribute homedescAttribute) {
        this.homedescAttribute = homedescAttribute;
    }

    public void setFurtherfacilitiesexternalAttribute(Attribute furtherfacilitiesexternalAttribute) {
        this.furtherfacilitiesexternalAttribute = furtherfacilitiesexternalAttribute;
    }

    public void setFurtherdetailsAttribute(Attribute furtherdetailsAttribute) {
        this.furtherdetailsAttribute = furtherdetailsAttribute;
    }

    public void setSpecialconditionsAttribute(Attribute specialconditionsAttribute) {
        this.specialconditionsAttribute = specialconditionsAttribute;
    }

    public void setAmenityNotesAttribute(Attribute amenityNotesAttribute) {
        this.amenityNotesAttribute = amenityNotesAttribute;
    }

    public void setCountryAttribute(Attribute countryAttribute) {
        this.countryAttribute = countryAttribute;
    }

    public void setVillagenameAttribute(Attribute villagenameAttribute) {
        this.villagenameAttribute = villagenameAttribute;
    }

    public void setSykesratingAttribute(Attribute sykesratingAttribute) {
        this.sykesratingAttribute = sykesratingAttribute;
    }

    public void setVillageidAttribute(Attribute villageidAttribute) {
        this.villageidAttribute = villageidAttribute;
    }

    public void setWeblinkAttribute(Attribute weblinkAttribute) {
        this.weblinkAttribute = weblinkAttribute;
    }

    public void setPhotoAttribute(Attribute photoAttribute) {
        this.photoAttribute = photoAttribute;
    }

    public void removeAttribute(Attribute attribute) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public FeedResult run(File file) throws IOException, FTPException, FeedException, JDOMException {
        updateFeedCount();

        long time = System.currentTimeMillis();

        initItemType();
        initAttributes();
        initImageAttribute();


        List<Element> cottages = getCottages(propData);

        int processed = 0;
        int created = 0;
        for (Element element : cottages) {
            Cottage cottage = new Cottage(element);
            SykesParser.createItem(context, this, cottage);
//            if (processItem(element))
//                created++;
            processed++;
        }

        List<Element> photos = getPhotos(photoData);
        for (Element element : photos) {
            Photo photo = new Photo(element);
            SykesParser.createPhoto(context, this, photo);
        }

        return new FeedResult(created, processed, System.currentTimeMillis() - time);
    }

    private void initItemType() throws FeedException {

        if (getItemType() == null)
            throw new FeedException("No item type set");

    }

    private void initAttributes() {
        if (setupDone)
            return;
        ItemType itemType = getItemType();
        if (areaAttribute == null) {
            setAreaAttribute(itemType.addAttribute("Area"));
        }
        if (subareaAttribute == null) {
            setSubareaAttribute(itemType.addAttribute("Subarea"));
        }
        if (latitudeAttribute == null) {
            setLatitudeAttribute(itemType.addAttribute("Latitude"));
        }

        if (longitudeAttribute == null) {
            setLongitudeAttribute(itemType.addAttribute("Longitude"));
        }
        if (priceinfoAttribute == null) {
            setPriceinfoAttribute(itemType.addAttribute("Price info"));
            getPriceinfoAttribute().setHeight(5);
            getPriceinfoAttribute().setWidth(80);
            getPriceinfoAttribute().save();
        }
        if (minpriceAttribute == null) {
            setMinpriceAttribute(itemType.addAttribute("Min price"));
        }
        if (maxpriceAttribute == null) {
            setMaxpriceAttribute(itemType.addAttribute("Max price"));
        }
        if (bedroomsAttribute == null) {
            setBedroomsAttribute(itemType.addAttribute("Bedrooms"));
        }
        if (kettleAttribute == null) {
            setKettleAttribute(itemType.addAttribute("Kettle", AttributeType.Boolean));
        }
        if (parkingspaceAttribute == null) {
            setParkingspaceAttribute(itemType.addAttribute("Parking space", AttributeType.Boolean));
        }
        if (smokingAttribute == null) {
            setSmokingAttribute(itemType.addAttribute("Smoking", AttributeType.Boolean));
        }
        if (singlebedsAttribute == null) {
            setSinglebedsAttribute(itemType.addAttribute("Single beds"));
        }
        if (doublebedsAttribute == null) {
            setDoublebedsAttribute(itemType.addAttribute("Double beds"));
        }
        if (twinbedsAttribute == null) {
            setTwinbedsAttribute(itemType.addAttribute("Twin beds"));
        }
        if (bunkbedsAttribute == null) {
            setBunkbedsAttribute(itemType.addAttribute("Bunk beds"));
        }
        if (familybedsAttribute == null) {
            setFamilybedsAttribute(itemType.addAttribute("Family beds"));
        }
        if (maxsleepsAttribute == null) {
            setMaxsleepsAttribute(itemType.addAttribute("Max sleeps"));
        }
        if (proppostcodeAttribute == null) {
            setProppostcodeAttribute(itemType.addAttribute("Prop postcode", AttributeType.Postcode));
        }
        if (propfeedbackAttribute == null) {
            setPropfeedbackAttribute(itemType.addAttribute("Prop feedback"));
            getPropfeedbackAttribute().setHeight(5);
            getPropfeedbackAttribute().setWidth(80);
            getPropfeedbackAttribute().save();
        }
        if (tenniscourtAttribute == null) {
            setTenniscourtAttribute(itemType.addAttribute("Tennis court", AttributeType.Boolean));
        }
        if (petsAttribute == null) {
            setPetsAttribute(itemType.addAttribute("Pets", AttributeType.Boolean));
        }
        if (changeoverdayAttribute == null) {
            setChangeoverdayAttribute(itemType.addAttribute("Change over day", AttributeType.Selection));
            getChangeoverdayAttribute().addOption("MON");
            getChangeoverdayAttribute().addOption("TUE");
            getChangeoverdayAttribute().addOption("WED");
            getChangeoverdayAttribute().addOption("THU");
            getChangeoverdayAttribute().addOption("FRI");
            getChangeoverdayAttribute().addOption("SAT");
            getChangeoverdayAttribute().addOption("SUN");
        }
        if (jacuzziAttribute == null) {
            setJacuzziAttribute(itemType.addAttribute("Jacuzzi", AttributeType.Boolean));
        }
        if (telephoneAttribute == null) {
            setTelephoneAttribute(itemType.addAttribute("Telephone", AttributeType.Boolean));
        }
        if (saunaAttribute == null) {
            setSaunaAttribute(itemType.addAttribute("Sauna", AttributeType.Boolean));
        }
        if (linenprovidedAttribute == null) {
            setLinenprovidedAttribute(itemType.addAttribute("Linen provided", AttributeType.Boolean));
        }
        if (towelsprovidedAttribute == null) {
            setTowelsprovidedAttribute(itemType.addAttribute("Towels", AttributeType.Boolean));
        }
        if (dishwasherAttribute == null) {
            setDishwasherAttribute(itemType.addAttribute("Dishwasher", AttributeType.Boolean));
        }
        if (cookerAttribute == null) {
            setCookerAttribute(itemType.addAttribute("Cooker", AttributeType.Boolean));
        }
        if (fridgeAttribute == null) {
            setFridgeAttribute(itemType.addAttribute("Fridge", AttributeType.Boolean));
        }
        if (washingmachineAttribute == null) {
            setWashingmachineAttribute(itemType.addAttribute("Washing machine", AttributeType.Boolean));
        }
        if (tvAttribute == null) {
            setTvAttribute(itemType.addAttribute("Tv", AttributeType.Boolean));
        }
        if (highchairAttribute == null) {
            setHighchairAttribute(itemType.addAttribute("High chair", AttributeType.Boolean));
        }
        if (bathroomsAttribute == null) {
            setBathroomsAttribute(itemType.addAttribute("Bathrooms"));
        }
        if (cotsAttribute == null) {
            setCotsAttribute(itemType.addAttribute("Cots", AttributeType.Boolean));
        }
        if (henstagAttribute == null) {
            setHenstagAttribute(itemType.addAttribute("Henstag", AttributeType.Boolean));
        }
        if (childrenAttribute == null) {
            setChildrenAttribute(itemType.addAttribute("Children", AttributeType.Boolean));
        }
        if (gamesroomAttribute == null) {
            setGamesroomAttribute(itemType.addAttribute("Gamesroom", AttributeType.Boolean));
        }
        if (pingpongAttribute == null) {
            setPingpongAttribute(itemType.addAttribute("Pingpong", AttributeType.Boolean));
        }
        if (snookerAttribute == null) {
            setSnookerAttribute(itemType.addAttribute("Snooker", AttributeType.Boolean));
        }
        if (wifiAttribute == null) {
            setWifiAttribute(itemType.addAttribute("Wifi", AttributeType.Boolean));
        }
        if (webpriceAttribute == null) {
            setWebpriceAttribute(itemType.addAttribute("Webprice"));
        }
        if (furtherfacilitiesinternalAttribute == null) {
            setFurtherfacilitiesinternalAttribute(itemType.addAttribute("Further facilities internal"));
            getFurtherfacilitiesinternalAttribute().setHeight(5);
            getFurtherfacilitiesinternalAttribute().setWidth(80);
            getFurtherfacilitiesinternalAttribute().save();
        }
        if (regiondescAttribute == null) {
            setRegiondescAttribute(itemType.addAttribute("Region description"));
            getRegiondescAttribute().setHeight(5);
            getRegiondescAttribute().setWidth(80);
            getRegiondescAttribute().save();
        }
        if (towndescAttribute == null) {
            setTowndescAttribute(itemType.addAttribute("Town description"));
            getTowndescAttribute().setHeight(5);
            getTowndescAttribute().setWidth(80);
            getTowndescAttribute().save();
        }
        if (homesummaryAttribute == null) {
            setHomesummaryAttribute(itemType.addAttribute("Home summary"));
            getHomesummaryAttribute().setHeight(5);
            getHomesummaryAttribute().setWidth(80);
            getHomesummaryAttribute().save();
        }
        if (homedescAttribute == null) {
            setHomedescAttribute(itemType.addAttribute("Home description"));
            getHomedescAttribute().setHeight(5);
            getHomedescAttribute().setWidth(80);
            getHomedescAttribute().save();
        }
        if (furtherfacilitiesexternalAttribute == null) {
            setFurtherfacilitiesexternalAttribute(itemType.addAttribute("Further facilities external"));
            getFurtherfacilitiesexternalAttribute().setHeight(5);
            getFurtherfacilitiesexternalAttribute().setWidth(80);
            getFurtherfacilitiesexternalAttribute().save();
        }
        if (furtherdetailsAttribute == null) {
            setFurtherdetailsAttribute(itemType.addAttribute("Further details"));

            getFurtherdetailsAttribute().setHeight(5);
            getFurtherdetailsAttribute().setWidth(80);
            getFurtherdetailsAttribute().save();
        }
        if (specialconditionsAttribute == null) {
            setSpecialconditionsAttribute(itemType.addAttribute("Special conditions"));
            getSpecialconditionsAttribute().setHeight(5);
            getSpecialconditionsAttribute().setWidth(80);
            getSpecialconditionsAttribute().save();
        }
        if (amenityNotesAttribute == null) {
            setAmenityNotesAttribute(itemType.addAttribute("Amenity Notes"));
            getAmenityNotesAttribute().setHeight(5);
            getAmenityNotesAttribute().setWidth(80);
            getAmenityNotesAttribute().save();
        }
        if (countryAttribute == null) {
            setCountryAttribute(itemType.addAttribute("Country"));

//            setCountryAttribute(getItemType().addAttribute("Country", AttributeType.Selection));
//            for (Country c: Country.getAll())
//            getCountryAttribute().addOption(c.getLabel());
        }
        if (villagenameAttribute == null) {
            setVillagenameAttribute(itemType.addAttribute("Village name"));
        }
        if (sykesratingAttribute == null) {
            setSykesratingAttribute(itemType.addAttribute("Sykes rating", AttributeType.Selection));
            getSykesratingAttribute().addOption("1");
            getSykesratingAttribute().addOption("2");
            getSykesratingAttribute().addOption("3");
            getSykesratingAttribute().addOption("4");
            getSykesratingAttribute().addOption("5");
        }
        if (villageidAttribute == null) {
            setVillageidAttribute(itemType.addAttribute("Village id"));
        }
        if (weblinkAttribute == null) {
            setWeblinkAttribute(itemType.addAttribute("Weblink", AttributeType.Link));
            getWeblinkAttribute().setLinkNewWindow(true);
            getWeblinkAttribute().save();
        }
        save();
    }

    private void initImageAttribute() {
        if (setupDone)
            return;
        ItemType itemType = getItemType();
        if (photoAttribute == null) {
            setPhotoAttribute(itemType.addAttribute("Photos", AttributeType.Image));
            getPhotoAttribute().setMulti(true);
            getPhotoAttribute().save();
        }
        setupDone = true;

        save();
    }

    private List<Element> getCottages(String propData) throws IOException, JDOMException {
        Document doc = getDoc(propData);

        return doc.getRootElement().getChildren("cottage");
    }

    private List<Element> getPhotos(String photoData) throws IOException, JDOMException {
        Document doc = getDoc(photoData);

        return doc.getRootElement().getChildren("photo");
    }

    private Document getDoc(String data) throws IOException, JDOMException {
        HttpClient hc = new HttpClient(data, HttpMethod.Get);
        hc.connect();

        SAXBuilder sax = new SAXBuilder();
        return sax.build(new StringReader(hc.getResponseString()));
    }
}
