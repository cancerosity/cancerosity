package org.sevensoft.ecreator.model.feeds.csv.parsers;

import java.util.Arrays;
import java.util.List;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.feeds.csv.CsvImportFeed;
import org.sevensoft.ecreator.model.feeds.csv.CsvImportFeedField;
import org.sevensoft.ecreator.model.feeds.csv.FieldType;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 4 Sep 2006 18:22:40
 *
 */
public class ItemParser extends LineParser {

	private int				created;
	private CsvImportFeedField	nameField;

	public ItemParser(RequestContext context, CsvImportFeed feed) {
		super(context, feed);

		this.nameField = feed.getField(FieldType.Name);
	}

	public int getCreated() {
		return created;
	}

	@Override
	protected void process(String[] row) {

		logger.fine("[ItemParser] row=" + Arrays.toString(row));
        if (!feed.isImportField(row)) {
            return;
        }

		boolean skuUsed = false;

		try {

			// check that the category isn't in our list of ignored categories

			List<String> values = feed.getValues(FieldType.AddCategory, row);
			if (feed.isIgnoredCategory(values)) {
				logger.fine("[ItemParser] skipping row because one of its categories is in the ignore list values=" + values);
				return;
			}

			// before we process the row check that the price (if applicable) is within our bounds
			if (feed.getMinCostPrice().isPositive()) {

				logger.fine("[ItemParser] we have a min cost price of " + feed.getMinCostPrice());
				String value = feed.getValue(FieldType.CostPrice, row);
				if (value == null) {
					logger.fine("[ItemParser] no cost price set, skipping");
					return;
				}

				try {

					Money money = new Money(value);
					logger.fine("[ItemParser] This item has a cost price of " + money);
					if (money.isLessThan(feed.getMinCostPrice())) {
						logger.fine("[ItemParser] skipping item");
						return;
					}

				} catch (RuntimeException e) {
					e.printStackTrace();
				}

			}

			Item item = null;
			String ref = feed.getRefValue(row);
			logger.fine("[ItemParser] ref=" + ref);

			String sku = feed.getSkuValue(row);
			logger.fine("[ItemParser] sku=" + sku);

			// try lookup by ref first
			if (ref != null) {
				item = Item.getByRef(context, feed.getItemType(), ref);
				logger.fine("[ItemParser] looking up item by ref, item=" + item);
			}

			// then try lookup by sku
			if (item == null && sku != null) {

				item = feed.getSupplier(true).getItem(sku, null);
				logger.fine("[ItemParser] looking up item by sku, item=" + item);

				if (item != null) {
					skuUsed = true;
				}
			}

			// trying name lookup if set
			if (item == null && nameField != null && nameField.isIndividualLookup()) {

				String name = feed.getValue(FieldType.Name, row);
				if (name != null) {
					item = Item.getByName(context, name);
					logger.fine("[ItemParser] looking up item by name, item=" + item);
				}
			}

			// if item not found, try lookup by ref then attributes
			if (item == null) {
				item = feed.getItemByAttributeLookup(row);
				logger.fine("[ItemParser] looking up item by attributes, item=" + item);
			}

			/*
			 * If item is null and we are creating items then create
			 */
			if (item == null && feed.isCreate()) {

				item = feed.createItem(feed.getItemType(), "");
				logger.fine("[ItemParser] creating item=" + item);

				created++;
			}

            if (feed.hasOptionReferenceField()) {
                feed.upadteOptions(row);
            }

			if (item == null) {

				logger.fine("[ItemParser] no item, skipping row");
				return;
			}

			// update feed count on item
			logger.fine("[ItemParser] setting feed count=" + feed.getFeedCount());
			item.setFeedCount(feed.getFeedCount());

			// set our internal ref field
			if (ref != null) {

				logger.fine("[ItemParser] setting reference=" + ref);
				item.setReference(ref);

				// make sure references are turned on
				if (!feed.getItemType().isReferences()) {
					feed.getItemType().setReferences(true);
					feed.getItemType().save();
				}
			}

			// if we didn't use a sku to lookup, and one is present, then set it
			if (!skuUsed && sku != null) {
				logger.fine("[ItemParser] setting sku, sku=" + sku + " supplier=" + feed.getSupplier(true));
				item.setSupplierSku(feed.getSupplier(true), sku);
			}

			logger.fine("[ItemParser] processing " + fields.size() + " fields");
			for (CsvImportFeedField field : fields) {
				field.process(feed, item, row);
			}

			item.save();

		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}
}