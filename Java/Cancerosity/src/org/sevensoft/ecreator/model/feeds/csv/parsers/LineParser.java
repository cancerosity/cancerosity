package org.sevensoft.ecreator.model.feeds.csv.parsers;

import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.ecreator.model.feeds.csv.CsvImportFeed;
import org.sevensoft.ecreator.model.feeds.csv.CsvImportFeedField;
import org.sevensoft.jeezy.http.RequestContext;

import com.ricebridge.csvman.BadLine;
import com.ricebridge.csvman.LineListenerSupport;

/**
 * @author sks 29 Nov 2006 09:45:36
 *
 */
public abstract class LineParser extends LineListenerSupport {

	protected static final Logger		logger	= Logger.getLogger("feeds");

	protected final RequestContext	context;
	protected final CsvImportFeed		feed;

	/**
	 * Number of rows processed
	 */
	private int					rowsProcessed;
	protected List<CsvImportFeedField>		fields;

	protected LineParser(RequestContext context, CsvImportFeed feed) {
		this.context = context;
		this.feed = feed;
		this.fields = feed.getFields();
	}

	@Override
	public final BadLine addLineImpl(String[] row, int arg1, long arg2, String arg3) throws Exception {
		process(row);
		rowsProcessed++;
		return null;
	}

	protected String getIdValue(String[] row) {

		if (feed.getIdField() < 1 || row.length < feed.getIdField()) {
			return null;

		} else {

			String id = row[feed.getIdField() - 1];

			if (id != null && id.length() == 0)
				id = null;

			return id;
		}

	}

	public final int getRowsProcessed() {
		return rowsProcessed;
	}

	protected abstract void process(String[] row);

}
