package org.sevensoft.ecreator.model.feeds.bespoke.products.vip;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletOutputStream;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.httpc.HttpMethod;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.products.VipFeedHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.Http;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.modules.Modules;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Label;

import com.enterprisedt.net.ftp.FTPException;

/**
 * @author sks 5 Jul 2006 09:32:31
 *
 */
@Table("feeds_vip")
@Label("Vip Computers")
@HandlerClass(VipFeedHandler.class)
@Http
public class VipFeed extends ImportFeed {

	private static String	Host			= "http://services.vip-computers.com";

	private static String	RequestTokenUrl	= Host + "/VIPWebservices/VIPWS_authentication.asmx/requestToken";

	private static String	ProductDetailsUrl	= Host + "/VIPWebservices/VIPWS_productsDetails.asmx/listProductDetails";
	private static String	ProductGroupsUrl	= Host + "/VIPWebservices/VIPWS_productsDetails.asmx/listProductGroups";
	/**
	 * 
	 */
	private String		sessionToken;
	/**
	 * 
	 */
	private Attribute		warrantyAttribute;

	/**
	 * 
	 */
	private Attribute		manufAttribute;

	private Attribute		mpnAttribute;
	private String		username;

	private String		password;

	private ItemType		itemType;

	public VipFeed(RequestContext context) {
		super(context);
	}

	public VipFeed(RequestContext context, boolean create) {
		super(context, create);
	}

	public final ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public Attribute getManufAttribute() {
		return (Attribute) (manufAttribute == null ? null : manufAttribute.pop());
	}

	public Attribute getMpnAttribute() {
		return (Attribute) (mpnAttribute == null ? null : mpnAttribute.pop());
	}

	public String getPassword() {
		return password;
	}

	public List<String> getProductGroups(String token) throws IOException, JDOMException {

		HttpClient hc = new HttpClient(ProductGroupsUrl, HttpMethod.Get);

		hc.setParameter("sessionToken", token);
		hc.setParameter("includeObsolete", "false");

		//	hc.debug();
		hc.connect();

		SAXBuilder sax = new SAXBuilder();
		Document doc = sax.build(new StringReader(hc.getResponseString()));

		List<Element> elements = doc.getRootElement().getChildren("VIPproductGroups");

		List<String> list = new ArrayList<String>();
		for (Element element : elements) {
			list.add(element.getChildText("productGroupName"));
		}
		return list;

	}

	/**
	 * Returns a list of all products on VIP's system.
	 */
	public List<Element> getProducts() throws IOException, JDOMException {

		HttpClient hc = new HttpClient(ProductDetailsUrl, HttpMethod.Get);

		hc.setParameter("sessionToken", sessionToken);
		hc.setParameter("sku", "");
		hc.setParameter("manufacturer", "");
		hc.setParameter("prodgroup", "");
		hc.setParameter("justNew", "0");
		hc.setParameter("XMLorder", "0");
		hc.setParameter("details", "Full");
		hc.setParameter("justClearance", "0");
		hc.setParameter("includeObsolete", "0");
		hc.setParameter("justReduced", "0");
		hc.setParameter("justInstock", "0");
		hc.setParameter("justSpecial", "0");

		hc.connect();

		SAXBuilder sax = new SAXBuilder();
		Document doc = sax.build(new StringReader(hc.getResponseString()));

		return doc.getRootElement().getChildren("VIPproductDetails");
	}

	/**
	 * Returns a session token which is needed for all requests to authenticicate
	 * 
	 */
	private void getSessionToken() throws IOException, JDOMException {

		HttpClient hc = new HttpClient(RequestTokenUrl, HttpMethod.Get);
		hc.setParameter("companyID", getUsername());
		hc.setParameter("password", getPassword());

		hc.connect();

		SAXBuilder sax = new SAXBuilder();
		Document doc = sax.build(new StringReader(hc.getResponseString()));

		logger.fine("Session token=" + sessionToken);

		sessionToken = doc.getRootElement().getChildText("sessionToken");
	}

	public String getUsername() {
		return username;
	}

	public Attribute getWarrantyAttribute() {
		return (Attribute) (warrantyAttribute == null ? null : warrantyAttribute.pop());
	}

	/**
	 * 
	 */
	public boolean hasItemType() {
		return itemType != null;
	}

	private void initAttributes() {

		if (warrantyAttribute == null)
			warrantyAttribute = getItemType().addAttribute("Warranty", AttributeType.Numerical);

		if (manufAttribute == null) {
			manufAttribute = getItemType().addAttribute("Manufacturer", AttributeType.Selection);
		}

		if (mpnAttribute == null) {
			logger.fine("getting mpn attribute from item type");
			mpnAttribute = getItemType().addAttribute("Manuf part number", AttributeType.Text);
			logger.fine("mpn=" + mpnAttribute);
		}

		save();
	}

	private void initItemType() throws FeedException {

		if (getItemType() == null)
			throw new FeedException("No item type set");

		// enable stock and pricing on item type
		getItemType().addModule(ItemModule.Pricing);
		getItemType().addModule(ItemModule.Stock);
		getItemType().save();

		Modules modules = Modules.getInstance(context);
		modules.addModule(Module.Pricing);
		modules.addModule(Module.Availabilitity);
		modules.save();
	}

	private String makeContent(String techDataXml) throws JDOMException, IOException {

		Document doc = new SAXBuilder().build(new StringReader(techDataXml));

		Element root = doc.getRootElement();

		List<Element> elements = root.getChildren();

		StringBuilder sb = new StringBuilder();
		sb.append(new TableTag("xml_desc"));

		for (Element e : elements) {

			sb.append("<tr><td><b>");
			sb.append(e.getName().replace("_x0020_", " "));
			sb.append("</b></td><td>");
			sb.append(e.getTextTrim());
			sb.append("</td</tr>");
		}
		sb.append("</table>");

		return sb.toString().replace("<br>", "<br/>");
	}

	/**
	 * @param element
	 */
	private boolean processItem(Element element) {

		boolean created = false;

		logger.fine("Process item from element: " + element);

		logger.fine("--Raw values--");
		List<Element> elements = element.getChildren();
		for (Element e : elements)
			logger.fine(e.getName() + "=" + e.getText());

		String product_group = element.getChildTextTrim("product_group");
		String manufacturer = element.getChildTextTrim("manufacturer");
		String product_name = element.getChildTextTrim("product_name");
		String model = element.getChildTextTrim("model");
		String price1 = element.getChildTextTrim("price1");
		String sku = element.getChildTextTrim("SKU");
		String qoH = element.getChildTextTrim("QoH");
		String wty = element.getChildTextTrim("wtyyrs");
		String dueDate = element.getChildTextTrim("dueDate");
		String imageUrl = element.getChildTextTrim("largeImageLink");
		String techDataXml = element.getChildTextTrim("techDataXML");

		// do not process names beginning with hashes as they are discontinued stock
		if (product_name.startsWith("#")) {
			logger.fine("Skipping discon product: " + product_name);
			return false;
		}

		int warranty;
		if (wty == null || wty.length() == 0)
			warranty = 0;
		else
			try {
				warranty = Integer.parseInt(wty.replaceAll("\\D", "")) * 365;
			} catch (NumberFormatException e) {
				warranty = 0;
			}

		// supplier look up
		logger.fine("lookup from supplier sku=" + sku);
		Item item = getSupplier(true).getItem(sku, null);
		if (item == null)
			logger.fine("item=" + item);

		/*
		 *  if the item is null then try looking up from mpn and manuf
		 */
		if (item == null) {

			MultiValueMap<Attribute, String> map = new MultiValueMap(new HashMap());
			if (manufAttribute != null)
				map.put(manufAttribute, manufacturer);
			if (mpnAttribute != null)
				map.put(mpnAttribute, model);
			logger.fine("lookup from attributes map=" + map);

			ItemSearcher search = new ItemSearcher(context);
			search.setAttributeValues(map);
			item = search.getItem();

			// set sku - we know this is never null
			if (item != null) {
				logger.fine("setting sku=" + sku);
				item.setSupplierSku(getSupplier(true), sku);
			}
		}

		/*
		 * If still null then we want to create
		 */
		if (item == null) {

			if (product_name.startsWith("#")) {
				product_name = product_name.replace("#DRC#", "Clearance C ");
				product_name = product_name.replace("#DRB#", "Clearance B ");
				product_name = product_name.replace("#DRA#", "Clearance A ");

				logger.fine("replacing # in name newname=" + product_name);
			}

			item = new Item(context, getItemType(), product_name, "live", null);
			logger.fine("created item name=" + product_name + ", item=" + item);

			// set sku - we know this is never null
			item.setSupplierSku(getSupplier(true), sku);

			try {

				String content = makeContent(techDataXml);
				item.setContent(content);

			} catch (JDOMException e) {
				e.printStackTrace();
				System.out.println(e);

			} catch (IOException e) {
				e.printStackTrace();
				System.out.println(e);
			}

			if (product_group != null) {

				Category category = getCategory(product_group, product_group);
				item.addCategory(category);
			}

			if (warrantyAttribute != null)
				item.setAttributeValue(warrantyAttribute, String.valueOf(warranty));

			if (model != null)
				item.setAttributeValue(mpnAttribute, model);

			if (manufacturer != null)
				item.setAttributeValue(manufAttribute, manufacturer);

			try {
				item.addImage(new URL(imageUrl), true, true, false);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ImageLimitException e) {
				e.printStackTrace();
			}

			created = true;
		}

		item.setSupplierStock(getSupplier(true), Integer.parseInt(qoH));
		item.setSupplierCostPrice(getSupplier(true), new Money(price1));
		item.setOutStockMsg(dueDate);

		item.save();

		return created;
	}

	@Override
	public void removeAttribute(Attribute attribute) {

		if (attribute.equals(manufAttribute))
			manufAttribute = null;

		if (attribute.equals(warrantyAttribute))
			warrantyAttribute = null;

		if (attribute.equals(mpnAttribute))
			mpnAttribute = null;

		save();
	}

	public FeedResult run(File file) throws IOException, FTPException, FeedException, JDOMException {

		updateFeedCount();

		long time = System.currentTimeMillis();

		initSupplier("Vip Computers");
		initItemType();
		initAttributes();

		getSessionToken();

		List<Element> elements = getProducts();

		int processed = 0;
		int created = 0;
		for (Element element : elements) {

			if (processItem(element))
				created++;
			processed++;
		}

		return new FeedResult(created, processed, System.currentTimeMillis() - time);
	}

	public final void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	public void setManufAttribute(Attribute manufAttribute) {
		this.manufAttribute = manufAttribute;
	}

	public void setMpnAttribute(Attribute mpnAttribute) {
		this.mpnAttribute = mpnAttribute;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setWarrantyAttribute(Attribute warrantyAttribute) {
		this.warrantyAttribute = warrantyAttribute;
	}

	public void write(ServletOutputStream out) throws IOException, FeedException {
	}
}
