package org.sevensoft.ecreator.model.feeds.bespoke.products.cars;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.output.XMLOutputter;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.ExportFeed;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.Http;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

import com.enterprisedt.net.ftp.FTPException;

/**
 * @author sks 10 May 2007 18:01:31
 *
 */
@Http
@Table("feeds_savemoneycars")
@HandlerClass(SaveMoneyCarsFeedHandler.class)
@Label("Save money on cars")
public class SaveMoneyCarsFeed extends ExportFeed {

	private ItemType	itemType;
	private Attribute	makeAttribute;
	private Attribute	yearAttribute;
	private Attribute	variantAttribute;
	private Attribute	modelAttribute;
	private Attribute	detailsAttribute;

	public SaveMoneyCarsFeed(RequestContext context) {
		super(context);
	}

	public SaveMoneyCarsFeed(RequestContext context, boolean create) {
		super(context, create);
	}

	public Attribute getDetailsAttribute() {
		return (Attribute) (detailsAttribute == null ? null : detailsAttribute.pop());
	}

	public ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public Attribute getMakeAttribute() {
		return (Attribute) (makeAttribute == null ? null : makeAttribute.pop());
	}

	public Attribute getModelAttribute() {
		return (Attribute) (modelAttribute == null ? null : modelAttribute.pop());
	}

	public Attribute getVariantAttribute() {
		return (Attribute) (variantAttribute == null ? null : variantAttribute.pop());
	}

	public Attribute getYearAttribute() {
		return (Attribute) (yearAttribute == null ? null : yearAttribute.pop());
	}

	public boolean hasItemType() {
		return itemType != null;
	}

	@Override
	public void removeAttribute(Attribute attribute) {
	}

	@Override
	public FeedResult run(File file) throws IOException, FTPException, FeedException, JDOMException {

		Element root = new Element("STOCK");
		Document doc = new Document(root);

		ItemSearcher searcher = new ItemSearcher(context);
		searcher.setItemType(getItemType());
		searcher.setStatus("LIVE");
        searcher.setLimit(getPreviewCount());

		for (Item item : searcher) {

			Element vehicleEl = new Element("VEHICLE");
			root.addContent(vehicleEl);

			Element makeEl = new Element("MAKE").setText(item.getAttributeValue(getMakeAttribute()));
			vehicleEl.addContent(makeEl);

			Element modelEl = new Element("MODEL").setText(item.getAttributeValue(getModelAttribute()));
			vehicleEl.addContent(modelEl);

			Element variantEl = new Element("VARIANT").setText(item.getAttributeValue(getVariantAttribute()));
			vehicleEl.addContent(variantEl);

			Element detailsEl = new Element("DETAILS").setText(item.getContent());
			vehicleEl.addContent(detailsEl);

			Element priceEl = new Element("PRICE").setText(item.getSellPrice().toEditString());
			vehicleEl.addContent(priceEl);

			Element yearEl = new Element("YEAROFMAKE").setText(item.getAttributeValue(getYearAttribute()));
			vehicleEl.addContent(yearEl);

			Element autoRenewEl = new Element("AUTORENEW").setText("0");
			vehicleEl.addContent(autoRenewEl);

			vehicleEl.addContent(new Element("DEEPLINK").setText(item.getUrl().replace("http://", "")));

		}

		XMLOutputter outputter = new XMLOutputter();

		FileOutputStream output = new FileOutputStream(file);

		outputter.output(doc, output);

		output.close();

		return null;

	}

	public void setDetailsAttribute(Attribute detailsAttribute) {
		this.detailsAttribute = detailsAttribute;
	}

	public void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	public void setMakeAttribute(Attribute makeAttribute) {
		this.makeAttribute = makeAttribute;
	}

	public void setModelAttribute(Attribute modelAttribute) {
		this.modelAttribute = modelAttribute;
	}

	public void setVariantAttribute(Attribute variantAttribute) {
		this.variantAttribute = variantAttribute;
	}

	public void setYearAttribute(Attribute yearAttribute) {
		this.yearAttribute = yearAttribute;
	}

}