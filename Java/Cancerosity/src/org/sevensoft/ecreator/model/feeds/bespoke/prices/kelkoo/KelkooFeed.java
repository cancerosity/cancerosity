package org.sevensoft.ecreator.model.feeds.bespoke.prices.kelkoo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jdom.JDOMException;
import org.sevensoft.commons.simpleio.SimpleFtp;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.prices.KelkooFeedHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.feeds.ExportFeed;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.annotations.Ftp;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.CsvManagerBaseException;
import com.ricebridge.csvman.CsvManagerException;

/**
 * @author sks 26-Apr-2005 20:25:01
 * 
 */
@Table("feeds_keloo")
@Label("Kelkoo")
@Ftp
@HandlerClass(KelkooFeedHandler.class)
public class KelkooFeed extends ExportFeed {

	private static final String	filename	= "kelkoofeed.csv";

	private Attribute			manufAttribute;

	private ItemType			itemType;

	private KelkooFeed(RequestContext context) {
		super(context);
	}

	public KelkooFeed(RequestContext context, boolean create) {
		super(context, create);
	}

	private String buildCodes(Item item) {
		return null;
	}

	public final ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public Attribute getManufAttribute() {
		return (Attribute) (manufAttribute == null ? null : manufAttribute.pop());
	}

	public boolean hasItemType() {
		return itemType != null;
	}

	@Override
	public void removeAttribute(Attribute attribute) {
		if (attribute.equals(manufAttribute)) {
			manufAttribute = null;
		}
		save();
	}

	public FeedResult run(File file) throws IOException, FTPException, FeedException, JDOMException {

		List<String[]> rows = new ArrayList();

		final List<Category> categories = Category.getKelkoo(context);
		logger.fine("Kelkoo categories retrieved: " + categories.size());

		for (Category category : categories) {

			logger.fine("Category: " + category.getName() + " " + category.getId());
			logger.fine("Kelkoo category value: " + category.getKelkooCategory());

			String[] kelkoo = category.getKelkooCategory().split(":");
			if (kelkoo.length != 2)
				continue;

			logger.fine("Iterating category");

			/*
			 * Iterator over all the live items in this category
			 */
			for (Item item : category.getItems(null, "Live", 0)) {

				Money salePrice = item.getSellPriceInc(null, 1, null);
				if (salePrice.isZero()) {

					logger.fine("Item: " + item.getName() + " has no sale price, skipping");

				} else {

					String[] row = new String[24];

					// add kelkoo category
					row[0] = kelkoo[0].trim();
					// add kelkoo type
					row[1] = kelkoo[1].trim();

					// manufacturer field 
					row[2] = item.getAttributeValue(getManufAttribute());

					// item name
					row[3] = item.getName();

					// any code fields seperated by semis. We will allow users to set up to three.
					row[10] = buildCodes(item);

					// our unique code		
					row[11] = item.getIdString();

					// content. We must remove all new lines
					String content = item.getContentStripped(200);
					content = StringHelper.stripNewLines(content);

					row[12] = content;

					// image
					if (item.hasApprovedImages()) {
						Img image = item.getApprovedImage();
						if (image != null)
							row[13] = image.getUrl();
					}

					// url back to product
					row[14] = item.getUrl();

					// standard sale price
					row[15] = salePrice.toEditString();
					row[16] = "5.00";
					row[17] = "2 to 3 days";

					// availability
					if (item.isAvailable())
						row[18] = "In stock";
					else
						row[18] = "Stock on Order";

					row[19] = "1 year";
					row[20] = "New";
					row[21] = "Single product";
					row[22] = "0.01";

					rows.add(row);

					logger.fine("Row: " + Arrays.toString(row));
				}
			}
            if (getPreviewCount() > 0 && rows.size() > getPreviewCount()) {
                break;
            }
		}

		CsvManager csvman = new CsvManager();
		csvman.setSeparator("|");

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			csvman.save(bos, rows);
		} catch (CsvManagerException ce) {
			System.out.println(ce.getUserMessage());
			if (CsvManagerBaseException.CODE_bad_line == ce.getCode()) {
				System.out.println("BAD LINE: " + ce);
			} else {
				throw ce;
			}
		}

		logger.fine("[KelkooFeed] Kelkoo feed saved into byte stream");

		SimpleFtp.putStream(getFtpHostname(), getFtpUsername(), getFtpPassword(), null, filename, FTPTransferType.ASCII, new ByteArrayInputStream(bos
				.toByteArray()));

        bos.close();
		return null;
	}

	public final void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	public void setManufAttribute(Attribute manufAttribute) {
		this.manufAttribute = manufAttribute;
	}

}
