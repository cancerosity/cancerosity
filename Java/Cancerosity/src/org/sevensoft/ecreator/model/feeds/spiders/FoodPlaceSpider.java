package org.sevensoft.ecreator.model.feeds.spiders;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.httpc.HttpMethod;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 24-Nov-2005 14:39:10
 * 
 */
public class FoodPlaceSpider extends Spider {

	private static Pattern	nameAddressPattern, telephonePattern, urlPattern;

	public FoodPlaceSpider() {

		nameAddressPattern = Pattern.compile("<td height=\"69\" align=\"left\" valign=\"top\"><b>(.*?)</b>(.*?)$", Pattern.MULTILINE);
		telephonePattern = Pattern.compile("<b>Telephone Number\\(s\\) :</b><br>(.*?)$", Pattern.MULTILINE);
		urlPattern = Pattern.compile("<a href=\"(/restaurants/.*?)\">.*?</a>");
	}

	@Override
	public String getName() {
		return "Foodplace";
	}

	@Override
	public int spider(RequestContext context, ItemType itemType, String url, Category category) throws MalformedURLException, IOException {

		Attribute addressAttribute = itemType.getAttribute("address");
		Attribute postcodeAttribute = itemType.getAttribute("postcode");
		Attribute telephoneAttribute = itemType.getAttribute("telephone");

		HttpClient hc = new HttpClient(url, HttpMethod.Get);
		hc.connect();
		String input = hc.getResponseString();

		Matcher matcher = urlPattern.matcher(input);

		Set<String> urls = new HashSet<String>();
		while (matcher.find()) {
			urls.add("http://www.thefoodplace.com" + matcher.group(1));
		}

		for (String restUrl : urls) {

			try {

				String address = null, name, postcode = null, telephone = null;

				hc = new HttpClient(restUrl, HttpMethod.Get);
				hc.connect();
				input = hc.getResponseString();

				matcher = nameAddressPattern.matcher(input);

				if (matcher.find()) {

					name = matcher.group(1).trim();
					address = matcher.group(2).replace("<br/>", "\n").replace("<br>", "\n");

					matcher = Pattern.compile(".*?$").matcher(address);

					if (matcher.find()) {
						postcode = matcher.group(0).replaceAll("\\W", "");
						address = matcher.replaceAll("").trim();
					}

					matcher = telephonePattern.matcher(input);

					if (matcher.find()) {
						telephone = matcher.group(1).trim();
					}

					Item item = new Item(context, itemType, name, "Live", null);

					if (addressAttribute != null) {
						item.setAttributeValue(addressAttribute, address);
					}

					if (telephoneAttribute != null) {
						item.setAttributeValue(telephoneAttribute, telephone);
					}

					if (postcodeAttribute != null) {
						item.setAttributeValue(postcodeAttribute, postcode);
					}

				}

			} catch (MalformedURLException e) {
				e.printStackTrace();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return 0;

	}
}
