package org.sevensoft.ecreator.model.feeds.csv;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.ecom.delivery.DeliveryOption;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.ecom.orders.OrderStatusException;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.modules.Modules;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.net.URL;
import java.net.URLDecoder;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Logger;

/**
 * @author sks 28 Jun 2006 16:18:49
 */
public enum FieldType {

	/**
	 * Looks up an item by id or reference or sku and then sets it as an accessory for this item
	 */
	Accessory() {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {

			/*
			 * Check accessory module is on
			 */
			if (!ItemModule.Accessories.enabled(context, item)) {

				item.getItemType().addModule(ItemModule.Accessories);
				item.getItemType().save();

				logger.fine("[FieldType] enabling accessories module on item type");
			}

			for (String value : values) {

				Item accessory = Item.getByRef(context, null, value);
				if (accessory == null) {
					accessory = EntityObject.getInstance(context, Item.class, value);
				}
				if (accessory == null) {
					accessory = feed.getSupplier(true).getItem(value, "Live");
				}

				if (accessory != null) {
					item.addAccessory(accessory);
				}
			}

		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			return item.getAccessory() == null ? null : item.getAccessory().getName();
		}

	},

	/**
	 * Sets a delivery rate on the first band
	 */
	Delivery() {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {

			logger.fine("[FieldType] setting delivery");

			for (String value : values) {

				if (value == null) {
					continue;
				}

				value = value.trim();

				if (value.length() == 0) {
					continue;
				}

				Money money = new Money(value);
				DeliveryOption band = SimpleQuery.get(context, DeliveryOption.class);

				logger.fine("[FieldType] new delivery rate option=" + band + ", charge=" + money);
				item.getDelivery().setDeliveryRate(band, money);

				return;
			}

		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) { // todo check format
			DeliveryOption deliveryOption = SimpleQuery.get(context, DeliveryOption.class);
			if (deliveryOption == null) {
				return null;
			}
			Money rate = item.getDelivery().getDeliveryRate(deliveryOption);
			return rate == null ? null : rate.getAmountString();
		}

	},

	Image() {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {

			// check if we already have an image
			if (item.hasApprovedImages()) {
				return;
			}

			for (String value : values) {

				if (value == null) {
					continue;
				}

				value = value.trim();

				// images can't really be less than 5 characters can they ?! a.jp ? and 4 is too short really for searching
				if (value.length() < 5) {
					continue;
				}

				logger.fine("[FieldType] Addding image value: " + value);

				/*
				 * Check for http download
				 */
				if (value.startsWith("http")) {

					try {

						Img img = item.addImage(new URL(URLDecoder.decode(value, "UTF-8")), true, false, false);
						logger.fine("[FieldType] Downloaded from url: " + value + " and added as image=" + img);

					} catch (Exception e) {
						logger.warning("[" + FieldType.class + "] " + Image + " " + e);
					}

					/*
					 * ... otherwise try to locate file on local file system.
					 */
				} else {

					try {

						/*
						 * Remove paths from the image filename
						 */
						value = value.replaceAll(".*/", "");
						value = value.replaceAll(".*\\\\", "");

						/*
						 * Replace spaces with underscores
						 */
						value = value.replaceAll("\\s", "_");

						logger.fine("[FieldType] stripped filename: " + value);

						// if partial match then do search in image store
						if (field.isPartialImageMatch()) {

							final String v = value.toLowerCase().replaceAll("\\s", "");

							String[] filenames = ResourcesUtils.getRealImagesDir().list(new FilenameFilter() {

								public boolean accept(File dir, String name) {
									return name.toLowerCase().replaceAll("\\s", "").contains(v);
								}
							});

							if (filenames != null && filenames.length > 0) {
								item.addImage(filenames[0], true, false, false);
								logger.fine("[FieldType] files found=" + filenames.length);
							}

						} else {

							/*
							 * Add image from filename
							 */
							item.addImage(value, true, false, false);

						}

					} catch (FileNotFoundException e) {
						logger.warning("[" + FieldType.class + "] " + Image + " " + e);
					} catch (Exception e) {
						logger.warning("[" + FieldType.class + "] " + Image + " " + e);

					}

				}

			}
		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			return item.getApprovedImage() == null ? null : item.getApprovedImage().getName();
		}
	},
	Stock() {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {

			int stock = 0;
			for (String value : values) {

				if (value != null) {

					if (value.toLowerCase().startsWith("y")) {

						stock += 1;

					} else {

						try {
							stock = stock + Integer.parseInt(value.trim());
						} catch (NumberFormatException e) {
							logger.warning("[" + FieldType.class + "] " + Stock + " " + e);
						}
					}
				}
			}

			item.setOurStock(stock);
		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			return item.getOurStockString();
		}

	},
	OptionName("Option name") {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {

		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			return null;
		}
	},
	OptionReference("Option reference") {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {

		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			item.getAttributeValue(item.getItemType().getProductCodeAttribute());
			return null;
		}
	},
	OptionStock("Option stock") {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {

		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {

			return null;
		}
	},
	OptionSellprice("Option sellprice") {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {

		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			return null;
		}
	},
	Leadtime() {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {

			for (String value : values) {

				if (value == null) {
					continue;
				}

				value = value.trim();

				if (value.length() == 0) {
					continue;
				}

				item.setOutStockMsg(value);
				return;
			}
		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			return item.getOutStockMsg();
		}

	},
	AddCategory("Category") {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {

			if (values.size() == 1 || values.get(1) == null) {

				for (String value : values) {

					if (value == null) {
						continue;
					}

					value = value.trim();

					if (value.length() == 0) {
						continue;
					}

					Category category = feed.getCategory(value, value);

					item.addCategory(category);
					logger.fine("[FieldType] category added " + category);
				}

			} else {

				boolean isParent = true;
				Category parentCategory = null;

				for (String value : values) {

					if (value == null) {
						continue;
					}

					value = value.trim();

					if (value.length() == 0) {
						continue;
					}

					if (isParent) {
						parentCategory = feed.getCategory(value, value);
						isParent = false;
					} else {
						Category chilCategory = feed.getCategory(value, value, parentCategory);
						chilCategory.setParent(parentCategory);
						item.addCategory(chilCategory);
					}
				}
			}
		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			return item.getCategoryName();
		}

	},
	ParentCategory("Parent category") {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {

			// for (String value : values) {
			//
			// if (value == null) {
			// continue;
			// }
			//
			// value = value.trim();
			//
			// if (value.length() == 0) {
			// continue;
			// }
			//
			// Category category = feed.getCategory(value, value);
			//
			// item.addCategory(category);
			// logger.fine("[FieldType] category added " + category);
			// }
		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
        public String process(RequestContext context, Item item) {
            Category category = item.getCategory();
            Category parentCategory = null;
            if (category != null) {
                parentCategory = category.getParent();
                if (parentCategory != null) {
                    category = parentCategory;
                }
            }

            return category == null ? null : category.getName();
        }

	},
    AddToCategoryId("Category ID") {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {
            if (values.size() == 1 && values.get(0) != null && values.get(0).length() != 0) {
                values = Arrays.asList(values.get(0).split(","));
            }
            for (String value : values) {

                if (value == null) {
                    continue;
                }

                value = value.trim();

                if (value.length() == 0) {
                    continue;
                }

                Category category = EntityObject.getInstance(context, Category.class, value);
                if (category != null) {
                    item.addCategory(category);
                    logger.fine("[FieldType] category added " + category);
                }
            }
		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
        public String process(RequestContext context, Item item) {
//            return item.getCategory() == null ? null : item.getCategory().getIdString();
            return item.getCategoryIds();
        }

	},
	CostPrice("Supplier cost price") {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {

			if (!Module.Suppliers.enabled(context)) {
				Modules modules = Modules.getInstance(context);
				modules.addModule(Module.Suppliers);
				modules.save();
			}

			if (!ItemModule.Pricing.enabled(context, item)) {
				item.getItemType().addModule(ItemModule.Pricing);
				item.getItemType().save();
			}

			if (!item.getItemType().isCostPricing()) {
				item.getItemType().setCostPricing(true);
				item.getItemType().save();
			}

			Money costPrice = new Money(0);
			for (String value : values) {

				try {
					costPrice = costPrice.add(new Money(value));
				} catch (RuntimeException e1) {
					logger.warning("[" + FieldType.class + "] " + CostPrice + " " + e1);
				}

			}

			logger.fine("[FieldType] new cost price=" + costPrice);

			item.setSupplierCostPrice(feed.getSupplier(true), costPrice);
			return;

		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			Money costPrice = item.getCostPrice();
			return costPrice == null ? null : costPrice.toEditString();
		}
	},
	OurCostPrice("Cost price") {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {

			if (!ItemModule.Pricing.enabled(context, item)) {
				item.getItemType().addModule(ItemModule.Pricing);
				item.getItemType().save();
			}

			if (!item.getItemType().isCostPricing()) {
				item.getItemType().setCostPricing(true);
				item.getItemType().save();
			}

			Money costPrice = new Money(0);
			for (String value : values) {

				try {
					costPrice = costPrice.add(new Money(value));
				} catch (RuntimeException e1) {
					logger.warning("[" + FieldType.class + "] " + OurCostPrice + " " + e1);
				}
			}

			logger.fine("[FieldType] new cost price=" + costPrice);

			item.setOurCostPrice(costPrice);
			return;

		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			Money costPrice = item.getOurCostPrice();
			return costPrice == null ? null : costPrice.toEditString();
		}
	},
	SalePrice("Selling price") {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {

			Money sellPrice = new Money(0);
			for (String value : values) {

				try {
					sellPrice = sellPrice.add(new Money(value));
				} catch (RuntimeException e1) {
					logger.warning("[" + FieldType.class + "] " + SalePrice + " " + e1);
				}

			}

			logger.fine("[FieldType] new sell price= " + sellPrice);
			item.setSellPrice(sellPrice);

		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {

			for (String value : values) {

				if (value != null) {

					Money sellPrice = new Money(value);

					logger.fine("[FieldType] setting line sell price=" + sellPrice);

					line.getOrder().setSellPrice(line, sellPrice);
					return;

				}
			}

		}

		@Override
		public String process(RequestContext context, Item item) {
			Money sellPrice = item.getSellPrice();
			return sellPrice == null ? null : sellPrice.toEditString();
		}
	},

	OrderReference() {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {
		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {

			// change or set the order reference
			for (String value : values) {

				if (value != null) {

					Order order = line.getOrder();
					order.setReference(value);
					order.save();

					return;
				}
			}
		}

		@Override
		public String process(RequestContext context, Item item) {
			return null; // To change body of implemented methods use File | Settings | File Templates.
		}

	},
	Status() {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {

			for (String value : values) {

				if (value == null)
					continue;

				value = value.trim();

				if (value.length() == 0)
					continue;

				try {

					item.setStatus(value);

					return;

				} catch (RuntimeException e) {
					logger.warning("[" + FieldType.class + "] " + Status + " " + e);
				}

			}
		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {

			if (values.isEmpty())
				return;

			String value = values.get(0);

			order.setStatus(value);
			order.save();
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			return item.getStatus();
		}

	},
	VatRate() {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {

			for (String value : values) {

				if (value == null)
					continue;

				value = value.trim();

				if (value.length() == 0)
					continue;

				/*
				 * Strip percentage signs
				 */
				value = value.replace("%", "");

				try {

					item.setVatRate(Double.parseDouble(value), true);
					return;

				} catch (NumberFormatException e) {
					logger.warning("[" + FieldType.class + "] " + VatRate + " " + e);
				}
			}
		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			return item.getVatRateString();
		}

	},
	Description() {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overwrite, ImportFeed feed) {

			String string = StringHelper.implode(values, "\n\n", true);
			if (string == null)
				return;

			string = string.trim();
			if (string.length() == 0)
				return;

			item.setContent(string);

		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		public String process(RequestContext context, Item item) {
			return item.getContent();
		}

	},
	Summary() {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overwrite, ImportFeed feed) {

			if (!item.hasSummary()) {

				String string = StringHelper.implode(values, "\n\n", true);
				item.setSummary(string);

			}
		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			return item.getSummary();
		}
	},
	Rrp() {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overwrite, ImportFeed feed) {

			for (String value : values) {

				if (value == null)
					continue;

				value = value.trim();

				if (value.length() == 0)
					continue;

				item.setRrp(new Money(value));
				return;
			}

		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			Money rrp = item.getRrp();
			return rrp == null ? null : rrp.toEditString();
		}

	},

	ShortName() {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {

			/*
			 * Make sure short names are enabled
			 */
			ItemType itemType = item.getItemType();
			itemType.setShortNames(true);
			itemType.save();

			/*
			 * We will never overwrite the short name
			 */
			if (item.hasShortName()) {
				logger.fine("[FieldType] item has short name, skipping field");
				return;
			}

			for (String value : values) {

				if (value != null && value.length() > 1) {

					logger.fine("[FieldType] setting short name to " + value);

					item.setShortName(value);
					item.save();
					return;
				}
			}
		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			return item.getShortName();
		}

	},

	Qty() {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {
		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {

			for (String value : values) {

				try {

					logger.fine("[FieldType] trying to set line qty to " + value);

					int qty = Integer.parseInt(value);
					line.getOrder().setQty(line, qty, context);

					// exit after first successful qty change
					return;

				} catch (NumberFormatException e) {
					logger.warning("[" + FieldType.class + "] " + Qty + " " + e);

				} catch (OrderStatusException e) {
					logger.warning("[" + FieldType.class + "] " + Qty + " " + e);
				}
			}

		}

		@Override
		public String process(RequestContext context, Item item) {
			return null;
		}

	},

	/**
	 * Long (normal) name
	 */
	Name() {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overwrite, ImportFeed feed) {

			// we only set name if blank or reset name
			if (item.getName().length() == 0 || feed.isResetName()) {

				// String name = StringHelper.implode(values, " ", true);

				// generate name only with white spaces added manually
				String name = StringHelper.implode(values, "", false);
				item.setName(name);
				logger.fine("[fieldtype] set name to " + name);

			}
		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			return item.getName();
		}

	},

	/**
	 * Email for members
	 */
	Email() {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overwrite, ImportFeed feed) {

			String email = StringHelper.implode(values, " ", true);
			logger.fine("[FieldType] setting email=" + email);

			item.setEmail(email);

		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			return item.getEmail();
		}

	},

	Password() {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {

			for (String value : values) {

				if (value != null && value.length() > 1) {
					item.setPassword(value);
					break;
				}
			}
		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			return item.getPasswordHash();
		}

	},

	Attribute() {

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {

			boolean overwrite = field.isOverwrite();
			Attribute attribute = field.getAttribute();

			if (attribute == null) {
				logger.fine("[FieldType] attribute field has no attribute set");
				return;
			}

			logger.fine("[FieldType] defined attribute=" + attribute);

			/*
			 * If we have a date format specified, then we will convert to timestamps first otherwise we will assume timestampes already
			 */
			if (field.getDateFormat() != null) {

				logger.fine("[FieldType] date format=" + field.getDateFormat());

				List<String> newValues = new ArrayList();
				for (String value : values) {

					try {

						DateTime datetime = new DateTime(value, field.getDateFormat());
						logger.fine("[FieldType] parsed for datetime, timestamp=" + datetime);
						newValues.add(datetime.getTimestampString());

					} catch (ParseException e) {
						logger.warning("[" + FieldType.class + "] " + Attribute + " " + e);
					}
				}

				values = newValues;
			}

			/*
			 * if overwrite is true then we will SET the attribute which will overwrite others, otherwise we will add the attribute in as an
			 * addition unless it is already present
			 */
			if (overwrite) {

				logger.fine("[FieldType] setting attribute=" + attribute + ", values=" + values);
				item.setAttributeValues(attribute, values);

			} else {

				for (String value : values) {
					logger.fine("[FieldType] adding attribute=" + attribute + ", values=" + values);
					item.addAttributeValue(attribute, value);
				}
			}
		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			return null;
		}

	},
	PDF() {

		// TODO: implement methods!
		public void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
				ImportFeed feed) {
			// check if we already have an attach
			if (item.hasAttachments()) {
				return;
			}

			for (String value : values) {

				if (value == null) {
					continue;
				}

				value = value.trim();

				logger.fine("[FieldType] Addding PDF value: " + value);

				try {

					/*
					 * Remove paths from the pdf filename
					 */
					value = value.replaceAll(".*/", "");
					value = value.replaceAll(".*\\\\", "");

					/*
					 * Replace spaces with underscores
					 */
					value = value.replaceAll("\\s", "_");

					logger.fine("[FieldType] stripped filename: " + value);

					// if partial match then do search in pdf store
					if (field.isPartialPdfMatch()) {

						final String v = value.toLowerCase().replaceAll("\\s", "");

						String[] filenames = ResourcesUtils.getRealPdfsDir().list(new FilenameFilter() {

							public boolean accept(File dir, String name) {
								return name.toLowerCase().replaceAll("\\s", "").contains(v);
							}
						});

						if (filenames != null && filenames.length > 0) {
							item.addImage(filenames[0], true, false, false);
							logger.fine("[FieldType] files found=" + filenames.length);
						}

					} else {

						/*
						 * Add pdf from filename
						 */
						item.addAttachment(ResourcesUtils.getRealPdf(value), value);

					}

				} catch (Exception e) {
					logger.warning("[" + FieldType.class + "] " + PDF + " " + e);
				}

			}
		}

		@Override
		public void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values) {
		}

		@Override
		public void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values) {
		}

		@Override
		public String process(RequestContext context, Item item) {
			return item.getPdfAttachment() == null ? null : item.getPdfAttachment().getFilename();
		}

	};

	protected static Logger	logger	= Logger.getLogger("feeds");

	public static List<FieldType> get() {

		List<FieldType> list = new ArrayList<FieldType>(Arrays.asList(values()));
		Collections.sort(list, new Comparator<FieldType>() {

			public int compare(FieldType o1, FieldType o2) {
				return o1.toString().compareTo(o2.toString());
			}
		});
		return list;
	}

	private final String	toString;

	FieldType() {
		this.toString = name();
	}

	private FieldType(String name) {
		this.toString = name;
	}

	public abstract void process(RequestContext context, CsvImportFeedField field, Item item, List<String> values, Object obj, boolean overrwrite,
			ImportFeed feed);

	public abstract void process(RequestContext context, CsvImportFeedField field, Order order, List<String> values);

	public abstract void process(RequestContext context, ImportFeed feed, CsvImportFeedField field, OrderLine line, List<String> values);

	public abstract String process(RequestContext context, Item item);

	@Override
	public final String toString() {
		return toString;
	}
}
