package org.sevensoft.ecreator.model.feeds.spiders;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.logging.Logger;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 07-Dec-2005 16:35:49
 * 
 */
public abstract class Spider implements Selectable {

	protected Logger	logger	= Logger.getLogger("feeds");

	public String getLabel() {
		return getName();
	}

	public abstract String getName();

	public String getValue() {
		return getClass().getName();
	}

	/**
	 * Returns how many items created
	 */
	public abstract int spider(RequestContext context, ItemType itemType, String url, Category category) throws MalformedURLException, IOException;

}
