package org.sevensoft.ecreator.model.feeds.bespoke.prices.pricerunner;

import java.io.File;
import java.io.IOException;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.prices.PriceRunnerFeedHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.ExportFeed;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.Http;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.CsvSaver;

/**
 * @author sks 29-Mar-2006 12:17:12
 *
 */
@Table("feeds_pricerunner")
@Label("Price Runner")
@Http
@HandlerClass(PriceRunnerFeedHandler.class)
public class PriceRunnerFeed extends ExportFeed {

	private Attribute	manufAttribute;
	private Attribute	mpnAttribute;
	private Attribute	isbnAttribute;
	private Attribute	upcAttribute;
	private ItemType	itemType;

	private PriceRunnerFeed(RequestContext context) {
		super(context);
	}

	public PriceRunnerFeed(RequestContext context, boolean create) {
		super(context, create);
	}

	public Attribute getIsbnAttribute() {
		return (Attribute) (isbnAttribute == null ? null : isbnAttribute.pop());
	}

	public final ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public Attribute getManufAttribute() {
		return (Attribute) (manufAttribute == null ? null : manufAttribute.pop());
	}

	public Attribute getMpnAttribute() {
		return (Attribute) (mpnAttribute == null ? null : mpnAttribute.pop());
	}

	public final Attribute getUpcAttribute() {
		return (Attribute) (upcAttribute == null ? null : upcAttribute.pop());
	}

	public boolean hasItemType() {
		return itemType != null;
	}

	@Override
	public void removeAttribute(Attribute attribute) {

		if (attribute.equals(isbnAttribute)) {
			isbnAttribute = null;
		}

		if (attribute.equals(manufAttribute)) {
			manufAttribute = null;
		}

		if (attribute.equals(mpnAttribute)) {
			mpnAttribute = null;
		}

		if (attribute.equals(upcAttribute)) {
			upcAttribute = null;
		}

		save();
	}

	public void setIsbnAttribute(Attribute isbnAttribute) {
		this.isbnAttribute = isbnAttribute;
	}

	public final void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	public void setManufAttribute(Attribute manufAttribute) {
		this.manufAttribute = manufAttribute;
	}

	public void setMpnAttribute(Attribute mpnAttribute) {
		this.mpnAttribute = mpnAttribute;
	}

	public final void setUpcAttribute(Attribute upcAttribute) {
		this.upcAttribute = upcAttribute;
	}

	@Override
	public FeedResult run(File file) throws IOException, FeedException {
        if (file == null) {
            file = File.createTempFile("price_runner", null);
            file.deleteOnExit();
        }

		logger.config("[PriceRunnerFeed] Running pricerunner feed");

		CsvManager csvman = new CsvManager();
		csvman.setSeparator(",");
		csvman.setQuote('"');
		csvman.setEscape('\\');

		CsvSaver saver = csvman.makeSaver(file);
		saver.begin();
		
		ItemSearcher searcher = new ItemSearcher(context);
		searcher.setStatus("Live");
        searcher.setLimit(getPreviewCount());
        
		for (Item item : searcher) {

			logger.fine("[PriceRunnerFeed] Processing item: " + item);

			Money salePrice = item.getSellPriceInc();
			if (salePrice.isZero()) {
				logger.fine("[PriceRunnerFeed] Item does not have sale price, skipping");
				continue;
			}

			if (!item.hasCategory()) {
				logger.fine("[PriceRunnerFeed] Item is not in a category, skipping");
				continue;
			}

			String[] row = new String[18];
			row[0] = item.getCategoryFullName(" > ", false);

			/*
			 * 2. A unique product ID. Normally the one used on Partner website.
			 */
			row[1] = item.getIdString();

			/*
			 * 3. The price Partner charge for the item. Comma or dot separated. The price must: 
			 * 	Exclude shipping cost.
			 * 	Exclude tax in United States
			 * 	Include tax (VAT) in Europe
			 * 
			 * Important  In case there are mail-in-rebates available for the product, the price Partner list 
			 *  MUST be the price the consumer pays Partner, not the price after the MIR. If Partner have instant rebates, 
			 *  lowering the price the consumer pays at checkout, input the actual price paid at checkout.
			 */
			row[2] = salePrice.toEditString();

			/*
			 * 4. Product url
			 */
			row[3] = item.getUrl();

			// 5	Product name
			row[4] = item.getName();

			/*
			 * 6	Product state	Describing if a product is new, refurbished, open box etc. 
			 * 	Note, the field is only required if Partner sell refurbished or open box products.
			 */
			row[5] = "New";

			// 7. manufacturer sku
			String mpn = null;
			if (manufAttribute != null) {
				mpn = item.getAttributeValue(getMpnAttribute());
			}
			row[6] = mpn;

			/*
			 * 8. Manufacturer name (product brand).
			 * This information is needed for us to be able to use the manufacturer SKU in our matching process.
			 */
			String manuf = null;
			if (manufAttribute != null) {
				manuf = item.getAttributeValue(getManufAttribute());
			}
			row[7] = manuf;

			/*
			 * 9. Information on stock, to be filled with yes or no, depending on if the product is in stock or not
			 */
			row[8] = item.isAvailable() ? "Yes" : "No";

			/*
			 * 10. Information on stock level, to be filled with the amount of products in stock. 
			 */
			row[9] = String.valueOf(item.getAvailableStock());

			/*
			 * 11	Delivery time	Delivery time from time of order to product arriving at customer. 
			 */
			row[10] = "2-3 days";

			/*
			 * 12	Shipping Cost	The shipping cost per product
			 * 	Only add shipping cost in feed if its a flat rate, i.e. the shipping cost for a specific product is equal over the whole country.
			 * 	If not a flat rate; send PriceRunner separately information on how Partner calculate shipping costs for Partner products. For instance, do Partner have free shipping above a certain amount etc?
			 */
			row[11] = "5.99";

			/*
			 * 13. A description of the product.  
			 */
			row[12] = item.getContentStripped();

			/*
			 * 14	Graphic URL	A URL to an image of the product on Partner Site/server.
			 * 	It has to be in gif or jpg format.
			 */
			Img image = null;
			if (item.hasApprovedImages()) {
				image = item.getApprovedImage();
			}
			if (image == null) {
				row[13] = null;
			} else {
				row[13] = image.getUrl();
			}

			/*
			 * 15	ISBN	The ISBN number of a book.
			 * 	Required field for book retailers. 
			 * 
			 */
			String isbn = null;
			if (isbnAttribute != null) {
				isbn = item.getAttributeValue(getIsbnAttribute());
			}
			row[14] = isbn;

			/*
			 * 16	EAN or UPC	A unique universal product code to identify a product and its manufacturer.
			 * 	Required field for DVD films
			 * 	The UPC is a 12 digit number to identify a product in United States. The EAN is a 13 digit number to identify a product in Europe.
			 * 	This information will make the process of getting Partner companys products listed much more timely and accurate.
			 * 	The EAN/UPC number can also be used as SKU (see field 2 above)
			 */
			String upc = null;
			if (upcAttribute != null) {
				upc = item.getAttributeValue(getUpcAttribute());
			}
			row[15] = upc;
			row[16] = null;
			row[17] = null;

			// replace nulls
			CollectionsUtil.replaceNulls(row, "");
            HtmlHelper.replaseSymbolsWithUTF(row);

			saver.next(row);
		}

		saver.end();

		return new FeedResult(csvman);
	}
}
