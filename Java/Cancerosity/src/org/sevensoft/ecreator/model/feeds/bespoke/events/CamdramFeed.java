package org.sevensoft.ecreator.model.feeds.bespoke.events;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletOutputStream;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.filter.Filter;
import org.jdom.input.SAXBuilder;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.samdate.Time;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.events.CamdramFeedHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.Http;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

import com.enterprisedt.net.ftp.FTPException;

/**
 * @author sks 31 Jul 2006 16:52:19
 *
 */
@Table("feeds_camdram")
@HandlerClass(CamdramFeedHandler.class)
@Label("Cambridge Dramatics")
@Http
public class CamdramFeed extends ImportFeed {

	/**
	 * Show id is unique per 'show' not particular date
	 */
	private Attribute		showIdAttribute;

	/**
	 * The society that has put on this performance
	 */
	private Attribute		societyAttribute;

	/**
	 * The author for this event
	 */
	private Attribute		authorAttribute;

	/**
	 * Venue for show
	 */
	private Attribute		venueAttribute;

	/**
	 * 
	 */
	private Attribute		dateAttribute;

	/**
	 * 
	 */
	private Attribute		linkAttribute;

	/**
	 * A list of venues that we want to show events from
	 */
	private List<String>	venues;

	private ItemType		itemType;

	public CamdramFeed(RequestContext context, boolean create) {
		super(context, create);
	}

	public CamdramFeed(RequestContext context) {
		super(context);
	}

	public Attribute getAuthorAttribute() {
		return (Attribute) (authorAttribute == null ? null : authorAttribute.pop());
	}

	public Set<String> getAvailableVenues() throws IOException, JDOMException {

		File file = getInputFile();

		Document doc = new SAXBuilder().build(new BufferedInputStream(new FileInputStream(file)));
		Element root = doc.getRootElement();

		Set<String> venues = new TreeSet<String>();
		Iterator<Element> iter = root.getDescendants(new Filter() {

			public boolean matches(Object arg0) {
				return arg0 instanceof Element;
			}
		});
		while (iter.hasNext()) {

			Element e = iter.next();
			if (e.getName().equals("venue"))
				venues.add(e.getText());
		}

		return venues;
	}

	public Attribute getDateAttribute() {
		return (Attribute) (dateAttribute == null ? null : dateAttribute.pop());
	}

	public final ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public Attribute getLinkAttribute() {
		return (Attribute) (linkAttribute == null ? null : linkAttribute.pop());
	}

	public final void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	public Attribute getShowIdAttribute() {
		return (Attribute) (showIdAttribute == null ? null : showIdAttribute.pop());
	}

	public Attribute getSocietyAttribute() {
		return (Attribute) (societyAttribute == null ? null : societyAttribute.pop());
	}

	public Attribute getVenueAttribute() {
		return (Attribute) (venueAttribute == null ? null : venueAttribute.pop());
	}

	public List<String> getVenues() {
		if (venues == null) {
			venues = new ArrayList();
		}
		return venues;
	}

	private void init() throws FeedException {

		logger.fine("[CamdramFeed] init feed");

		if (itemType == null)
			throw new FeedException("No item type set");

		logger.fine("[CamdramFeed] item type=" + getItemType());

		if (venueAttribute == null) {

			venueAttribute = getItemType().getAttribute("Venue");
			if (venueAttribute == null) {
				venueAttribute = getItemType().addAttribute("Venue", AttributeType.Selection);
			}
		}
		logger.fine("[CamdramFeed] venueAttribute=" + venueAttribute);

		if (linkAttribute == null) {

			linkAttribute = getItemType().getAttribute("Link");
			if (linkAttribute == null)
				linkAttribute = getItemType().addAttribute("Link", AttributeType.Link);
		}
		logger.fine("linkAttribute=" + linkAttribute);

		if (authorAttribute == null) {

			authorAttribute = getItemType().getAttribute("Author");
			if (authorAttribute == null)
				authorAttribute = getItemType().addAttribute("Author", AttributeType.Text);
		}
		logger.fine("authorAttribute=" + authorAttribute);

		if (societyAttribute == null) {

			societyAttribute = getItemType().getAttribute("Society");
			if (societyAttribute == null)
				societyAttribute = getItemType().addAttribute("Society", AttributeType.Text);
		}
		logger.fine("societyAttribute=" + societyAttribute);

		if (showIdAttribute == null) {

			showIdAttribute = getItemType().getAttribute("Show ID");
			if (showIdAttribute == null)
				showIdAttribute = getItemType().addAttribute("Show ID", AttributeType.Text);
		}
		logger.fine("showIdAttribute=" + showIdAttribute);

		if (dateAttribute == null) {

			dateAttribute = getItemType().getAttribute("Date");
			if (dateAttribute == null)
				dateAttribute = getItemType().addAttribute("Date", AttributeType.DateTime);
		}
		logger.fine("dateAttribute=" + dateAttribute);

		/*
		 * Make sure date is datetime
		 */
		if (dateAttribute.getType() != AttributeType.DateTime) {
			dateAttribute.setType(AttributeType.DateTime);
			dateAttribute.save();
		}

		save();
	}

	@Override
	public void removeAttribute(Attribute attribute) {

		logger.fine("Removing attributes");

		if (attribute.equals(linkAttribute))
			linkAttribute = null;

		if (attribute.equals(dateAttribute))
			dateAttribute = null;

		if (attribute.equals(showIdAttribute))
			showIdAttribute = null;

		if (attribute.equals(societyAttribute))
			societyAttribute = null;

		if (attribute.equals(authorAttribute))
			authorAttribute = null;

		save();
	}

	public FeedResult run(File file) throws IOException, FTPException, FeedException, JDOMException {

		updateFeedCount();

		logger.entering(getClass().getName(), "run");

		init();

		if (file == null)
			file = getInputFile();

		if (file == null)
			throw new FeedException("No input data");

		Document doc = new SAXBuilder().build(new BufferedInputStream(new FileInputStream(file)));

		Element root = doc.getRootElement();

		long seconds = System.currentTimeMillis() / 1000;
		int items = 0;

		List<Element> showElements = root.getChildren("show");
		int processed = 0;
		int created = 0;
		for (Element show : showElements) {

			logger.fine(show.toString());

			// going to ignore category, as it seems its just further separating dramatics, it's a bit too specialised for our needs right now
			// String category = show.getChildText("category");
			String title = show.getChildText("title");
			logger.fine("title=" + title);

			String society = show.getChildText("society");
			logger.fine("society=" + society);

			String description = show.getChildText("description");
			logger.fine("description=" + description);

			String author = show.getChildText("author");
			logger.fine("author=" + author);

			String link = show.getChildText("link");
			logger.fine("link=" + link);

			String showId = show.getAttributeValue("id");
			logger.fine("showId=" + showId);

			// the date range specifies a particular instance of an event at a particular location
			List<Element> dateranges = show.getChildren("daterange");
			logger.fine("dateranges=" + dateranges);
			for (Element dateRange : dateranges) {

				try {

					Date startDate = new Date(dateRange.getChildText("startdate"), "yyyy-MM-dd");
					Date endDate = new Date(dateRange.getChildText("enddate"), "yyyy-MM-dd");
					String timeText = dateRange.getChildText("time");

					/* 
					 * Only accept venues in our allowed venue list
					 */
					String venue = dateRange.getChildText("venue");

					if (getVenues().isEmpty() || venuesMatch(venue)) {

						/* 
						 * create a new item PER date
						 * Also we are supplied a date range so we must cycle through the dates
						 */
						for (Date date : startDate.listTo(endDate)) {

							Time time = new Time(timeText);

							DateTime datetime = new DateTime(date, time);

							// no point processing an expired event !
							if (datetime.isPast())
								continue;

							MultiValueMap<Attribute, String> attributeValues = new MultiValueMap(new HashMap());
							attributeValues.put(dateAttribute, datetime.getTimestamp() + "+");

							ItemSearcher searcher = new ItemSearcher(context);
							searcher.setName(title);
							searcher.setAttributeValues(attributeValues);
							Item item = searcher.getItem();

							if (item == null) {

								logger.fine("Item is null, creating");

								created++;

								item = new Item(context, getItemType(), title, "Live", null);
								logger.fine("Item created: " + item);

								item.setContent(description);
								item.save();

								item.addAttributeValue(linkAttribute, link);
								item.addAttributeValue(authorAttribute, author);
								item.addAttributeValue(societyAttribute, society);

								item.addAttributeValue(venueAttribute, venue);
								item.addAttributeValue(dateAttribute, datetime.getTimestampString());
								item.addAttributeValue(showIdAttribute, showId);

								item.addCategories(getGenericCategories());
							}

							processed++;
							items++;
						}

					}

				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}

		seconds = System.currentTimeMillis() / 1000 - seconds;

		logger.exiting(getClass().getName(), "run");

		return new FeedResult(created, processed, seconds);
	}

	public void setAuthorAttribute(Attribute authorAttribute) {
		this.authorAttribute = authorAttribute;
	}

	public void setDateAttribute(Attribute dateAttribute) {
		this.dateAttribute = dateAttribute;
	}

	public void setLinkAttribute(Attribute linkAttribute) {
		this.linkAttribute = linkAttribute;
	}

	public void setShowIdAttribute(Attribute showIdAttribute) {
		this.showIdAttribute = showIdAttribute;
	}

	public void setSocietyAttribute(Attribute societyAttribute) {
		this.societyAttribute = societyAttribute;
	}

	public void setVenueAttribute(Attribute venueAttribute) {
		this.venueAttribute = venueAttribute;
	}

	public void setVenues(List<String> venues) {
		this.venues = venues;
	}

	private boolean venuesMatch(String venue) {

		venue = venue.toLowerCase();

		for (String string : getVenues()) {

			if (string.toLowerCase().contains(venue))
				return true;

		}

		return false;
	}

	public void write(ServletOutputStream out) throws IOException, FeedException {
	}

	/**
	 * 
	 */
	public boolean hasItemType() {
		return itemType != null;
	}

}
