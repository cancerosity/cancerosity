package org.sevensoft.ecreator.model.feeds.bespoke.prices.shopzilla;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.prices.ShopzillaFeedHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.ExportFeed;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.annotations.Ftp;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

import com.enterprisedt.net.ftp.FTPException;
import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.CsvSaver;

/**
 * @author sks 23-Mar-2006 20:19:34
 *
 */
@Table("feeds_shopzilla")
@Label("Shopzilla")
@Ftp
@HandlerClass(ShopzillaFeedHandler.class)
public class ShopzillaFeed extends ExportFeed {

	private Attribute	upcAttribute;

	private Attribute	manufAttribute;

	private ItemType	itemType;

	public ShopzillaFeed(RequestContext context) {
		super(context);
	}

	public ShopzillaFeed(RequestContext context, boolean create) {
		super(context, create);
	}

	public final ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public Attribute getManufAttribute() {
		return (Attribute) (manufAttribute == null ? null : manufAttribute.pop());
	}

	public Attribute getUpcAttribute() {
		return (Attribute) (upcAttribute == null ? null : upcAttribute.pop());
	}

	public boolean hasItemType() {
		return itemType != null;
	}

	@Override
	public void removeAttribute(Attribute attribute) {

		if (attribute.equals(upcAttribute)) {
			upcAttribute = null;
		}

		if (attribute.equals(manufAttribute)) {
			manufAttribute = null;
		}

		save();
	}

	@Override
	public FeedResult run(File file) throws IOException, FTPException, FeedException {

		file = File.createTempFile("shopzilla", "txt");
		file.deleteOnExit();

		logger.fine("[ShopzillaFeed] running");

		CsvManager csvman = new CsvManager();
		csvman.setSeparator("|");

		CsvSaver saver = csvman.makeSaver(file);
		saver.begin();

		/*
		 * Setup header row
		 */
		String[] row = { "Category", "Mfr", "Title", "Desc", "Link", "Image", "SKU", "MPID", "# on Hand", "Condition", "Ship Cost", "Price" };
		logger.fine("[ShopzillaFeed] Header row: " + Arrays.toString(row));

		saver.next(row);

		List<Item> items = Item.getShopzilla(context);
		for (Item item : items) {

			logger.fine("[ShopzillaFeed] Processing item: " + item.getName());

			row = new String[12];
			row[0] = item.getShopzillaCategory();
			row[1] = item.getAttributeValue(getManufAttribute());
			row[2] = item.getName();
			row[3] = item.getContentStripped();
			row[4] = item.getUrl();

			Img image = item.getApprovedImage();
			if (image != null) {
				row[5] = image.getUrl();
			}

			/* 
			 * MPN, ISBN, UPC
			 */
			row[6] = item.getAttributeValue(getUpcAttribute());

			// our product code
			row[7] = item.getIdString();

			// stock level
			row[8] = String.valueOf(item.getAvailableStock());

			// condition
			row[9] = "New";

			/*
			 *delivery cost 
			 */
			row[10] = "5.00";

			// sale price inc vat
			row[11] = item.getSellPriceInc().toEditString();

			CollectionsUtil.replaceNulls(row, "");
			logger.fine("Row: " + Arrays.toString(row));

			saver.next(row);
		}

		saver.end();

		logger.fine("[ShopzillaFeed] Data complete");

		exportFtp(file);
		file.delete();

		return new FeedResult(csvman);
	}

	public final void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	public void setManufAttribute(Attribute manufAttribute) {
		this.manufAttribute = manufAttribute;
	}

	public void setUpcAttribute(Attribute upcAttribute) {
		this.upcAttribute = upcAttribute;
	}

}
