package org.sevensoft.ecreator.model.feeds;

import java.io.File;
import java.io.IOException;

import org.sevensoft.commons.simpleio.SimpleFtp;
import org.sevensoft.ecreator.iface.frontend.items.feeds.FeedExportHandler;
import org.sevensoft.ecreator.model.feeds.annotations.Http;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FTPTransferType;

/**
 * @author sks 2 Aug 2006 18:36:07
 *
 */
public abstract class ExportFeed extends Feed {

    /**
     * number of items to be exported for priview
     * (to check whether the feed is set up correct)
     */
    private int previewCount;

    protected ExportFeed(RequestContext context) {
		super(context);
	}

	protected ExportFeed(RequestContext context, boolean create) {
		super(context, create);
	}

	protected void exportFtp(File file) throws IOException, FTPException {
		SimpleFtp.putFile(getFtpHostname(), getFtpUsername(), getFtpPassword(), getFtpPath(), getFtpFilename(), FTPTransferType.ASCII, file);
	}

	/**
	 * Override filename
	 */
	protected void exportFtp(File file, String filename) throws IOException, FTPException {
		SimpleFtp.putFile(getFtpHostname(), getFtpUsername(), getFtpPassword(), null, filename, FTPTransferType.ASCII, file);
	}

	public String getExportUrl() {
		if (getClass().getAnnotation(Http.class) != null) {
			return Config.getInstance(context).getUrl() + "/"
					+ new Link(FeedExportHandler.class, null, "feedClass", getClass().getName(), "id", getIdString()).toString();
		} else {
			return null;
		}
	}

    public String getPreviewUrl() {
        if (getClass().getAnnotation(Http.class) != null) {
            return Config.getInstance(context).getUrl() + "/"
                    + new Link(FeedExportHandler.class, null, "feedClass", getClass().getName(), "id", getIdString(), "previewCount", 10).toString();
        } else {
            return null;
        }
    }

    public void setPreviewCount(int previewCount) {
        this.previewCount = previewCount;
    }

    public int getPreviewCount() {
        return previewCount;
    }
}
