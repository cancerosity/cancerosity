package org.sevensoft.ecreator.model.feeds.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


/**
 * @author sks 27 Jan 2007 19:14:45
 * 
 * For feeds that retrieve data via a HTTP call
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Http {

}
