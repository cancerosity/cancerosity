package org.sevensoft.ecreator.model.feeds.bespoke.prices.base;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.prices.GoogleBaseHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.ExportFeed;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.annotations.Ftp;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.ecom.delivery.DeliveryOption;
import org.sevensoft.ecreator.model.ecom.shopping.ShoppingSettings;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

import com.enterprisedt.net.ftp.FTPException;
import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.CsvSaver;

/**
 * @author sks 15 Dec 2006 12:20:44
 *
 */
@Table("feeds_googlebase")
@HandlerClass(GoogleBaseHandler.class)
@Label("Google base")
@Ftp
public class GoogleBase extends ExportFeed {

    private static final String COLON_SEPARATOR = ":";
    private static final String COMMA_SEPARATOR = ",";

	private Attribute				brandAttribute;
	private Attribute				mpcAttribute;
	private Attribute				isbnAttribute;
    private Attribute				eanAttribute;
    private Attribute				shipWeightAttribute;
	private String				productType;
	private ItemType				itemType;
	private transient List<String>	fields;

	protected GoogleBase(RequestContext context) {
		super(context);
	}

	public GoogleBase(RequestContext context, boolean create) {
		super(context, create);
		this.name = "Google base";
		save();
	}

	public final Attribute getBrandAttribute() {
		return (Attribute) (brandAttribute == null ? null : brandAttribute.pop());
	}

	public final Attribute getIsbnAttribute() {
		return (Attribute) (isbnAttribute == null ? null : isbnAttribute.pop());
	}

    public final Attribute getEanAttribute() {
		return (Attribute) (eanAttribute == null ? null : eanAttribute.pop());
	}

    public Attribute getShipWeightAttribute() {
        return (Attribute) (shipWeightAttribute == null ? null : shipWeightAttribute.pop());
    }

    public final ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public final Attribute getMpcAttribute() {
		return (Attribute) (mpcAttribute == null ? null : mpcAttribute.pop());
	}

	public final String getProductType() {
		return productType == null ? "electronics" : productType;
	}

	public boolean hasItemType() {
		return itemType != null;
	}

	private void header(CsvSaver saver) {

		fields = new ArrayList();

//		fields.add("actor");
//		fields.add("apparel_type");
//		fields.add("artist");
//		fields.add("author");
		fields.add("brand");               //0
		fields.add("color");               //1
		fields.add("condition");           //2
		fields.add("description");         //3
		fields.add("expiration_date");     //4
//		fields.add("format");
		fields.add("id");                  //5
		fields.add("image_link");          //6
        fields.add("additional_image_link");  //7
		fields.add("gtin");                //8
		fields.add("link");                //9
        fields.add("capacity");             //10		fields.add("memory");
		fields.add("mpn");                  //11
//		fields.add("payment_accepted");
//		fields.add("payment_notes");
//		fields.add("pickup");
//        fields.add("sale_price");         //12
		fields.add("price");                  //12
//		fields.add("processor_speed");
		fields.add("product_type");               //13
        fields.add("google_product_category");    //14
//		fields.add("quantity");
        fields.add("availability");               //15
		fields.add("shipping");      //todo    16
        fields.add("shipping_weight");           //17
		fields.add("size");                      //18
//		fields.add("tax_percent");          //todo 25
//		fields.add("tax_region");           //todo   26
		fields.add("title");                     //19
//		fields.add("upc");                  //todo     28
//        fields.add("tech_spec_link");
//        fields.add("condition");
//        fields.add("price_exvat");
//        fields.add("category");
        fields.add("online_only");                   //20

        saver.next(fields.toArray(new String[0]));
	}

	@Override
	public void removeAttribute(Attribute attribute) {
	}

	public final void setBrandAttribute(Attribute brandAttribute) {
		this.brandAttribute = brandAttribute;
	}

	public final void setIsbnAttribute(Attribute isbnAttribute) {
		this.isbnAttribute = isbnAttribute;
	}

    public void setEanAttribute(Attribute eanAttribute) {
        this.eanAttribute = eanAttribute;
    }

    public void setShipWeightAttribute(Attribute shipWeightAttribute) {
        this.shipWeightAttribute = shipWeightAttribute;
    }

    public final void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	public final void setMpcAttribute(Attribute mpnAttribute) {
		this.mpcAttribute = mpnAttribute;
	}

	public final void setProductType(String productType) {
		this.productType = productType;
	}

	@Override
	public FeedResult run(File file) throws IOException, FTPException {
        File path = ResourcesUtils.getRealFeedData("google_base");
        path = ResourcesUtils.recreatePath(path);
		file = File.createTempFile("google_base", "txt", path);
		file.deleteOnExit();

		context.disableCache();

		CsvManager csvman = new CsvManager();
		csvman.setSeparator("\t");
        csvman.setEncoding("UTF-8");

		CsvSaver saver = csvman.makeSaver(file);
		saver.begin();

		header(saver);

		ItemSearcher searcher = new ItemSearcher(context);
		searcher.setItemType(getItemType());
        searcher.setStatus("Live");
		searcher.setVisibleOnly(true);
        searcher.setLimit(getPreviewCount());

        if (ItemModule.IgnoreItemsInFeed.enabled(context, getItemType())) {
            searcher.setExcludeFromGoogleFeed(true);
        }

        boolean stockModule = ItemModule.Stock.enabled(context, getItemType()) && Module.Availabilitity.enabled(context);
        for (Item item : searcher) {

			// check price
			Money price = item.getSellPrice();
			if (price == null || price.isZero()) {
				continue;
			}

			String[] row = new String[fields.size()];

			//1 actor
//			row[0] = null;

            //2 artist
//            row[1] = null;

            //3 author
//            row[2] = null;

            //4 brand
            row[0] = item.getAttributeValue(getBrandAttribute());

            //5 color
            row[1] = null;

            //6 condition
            row[2] = "new";

            //7 description

            String content = item.getContentStripped(10000);
            if (content != null) {
                content = content.replace("\t", "").replace("\n", "").replace("\r", "");
            }
            if (content == null) {
                content = item.getName().replaceAll("\t", "");
            }
            row[3] = content;

            //8 expiration_date
            row[4] = null;

            //9 format
//            row[8] = null;

            // 10 id
            row[5] = item.getIdString();

            //11 image_link
            List<Img> imgs = item.getApprovedImages();
            if (imgs == null || imgs.isEmpty()) {
                row[6] = null;
            } else {
                row[6] = imgs.get(0).getUrl();
                imgs.remove(0);
            }

            //additional_image_link
            if (imgs == null || imgs.isEmpty()) {
                row[7] = null;
            } else {
                StringBuilder sb = new StringBuilder();
                if (!imgs.isEmpty()) {
                    for (Img img : imgs) {
                        sb.append(img.getUrl());
                        if (imgs.get(imgs.size() - 1) != img) {
                            sb.append(COMMA_SEPARATOR);
                        }
                    }
                    row[7] = sb.toString();
                }
            }

            //12 isbn
            row[8] = item.getAttributeValue(getEanAttribute()) != null ? item.getAttributeValue(getEanAttribute()) : item.getAttributeValue(getIsbnAttribute());

            //13 link
            row[9] = item.getUrl();

            //14 memory
            row[10] = null;

            //15 model_number
            row[11] = item.getAttributeValue(getMpcAttribute());

            //16 payment_accepted
//            row[12] = "Visa,Mastercard,Cash,Check";

            //17 payment_notes
//            if (PaymentSettings.getInstance(context).getPaymentTypes().contains(PaymentType.GoogleCheckout)) {
//                row[13] = "Google Checkout";
//            }
//            else {
//                row[13] = null;
//            }

            //18 pickup
//            row[14] = "true";

                        //12 price_type. we use sell price like 'price' column if the is no 'sell_price' column
            row[12] = item.getSellPriceInc().toEditString() + " " + Currency.getDefault(context).getName();

            //19 price
//            StringBuilder priceCol = new StringBuilder();
//            priceCol.append(!new Money(0).equals(item.getRrpInc(null)) ? item.getRrpInc(null).toEditString() : item.getSellPriceInc().toEditString());
//            priceCol.append(" ").append(Currency.getDefault(context).getName());
//            row[12] = priceCol.toString();


            //21 processor_speed
//            row[17] = null;

            //22 product_type
            String product_type;
            if (item.hasCategory()) {
                if (!item.getCategoryName().equals("CSV FAULT")) {
                    product_type = getProductType() + " - " + item.getCategoryName();
                } else {
                    product_type = getProductType();
                }
            } else {
                product_type = getProductType();
            }
            row[13] = product_type;
            row[14] = getProductType();


            if (stockModule) {
                if (item.getOurStock() > 0) {
                    row[15] = "in stock";
                } else if (item.isBackorders()) {
                    row[15] = "available for order";
                } else {
                    row[15] = "out of stock";
                }
            } else {
                row[15] = "in stock";
            }

            //15 availability  //todo     * 'in stock' * 'available for order' * 'out of stock' * 'preorder'

           // 16 shipping and 18 shipping_weight : only one required
            String weight = item.getAttributeValue(getShipWeightAttribute());
            if (weight == null) {
                //16 shipping
                StringBuilder sb = new StringBuilder();
                List<DeliveryOption> deliveryOptions = DeliveryOption.get(context);
                if (!deliveryOptions.isEmpty()) {
                    for (int i = 0; i < deliveryOptions.size(); i++) {
                        DeliveryOption option = deliveryOptions.get(i);
                        sb.append(!option.getCountries().isEmpty() ? option.getCountries().first().getIsoAlpha2() : Country.UK.getIsoAlpha2());
                        sb.append(COLON_SEPARATOR);
                        //State. not for UK
                        sb.append(COLON_SEPARATOR);
                        //optional
                        sb.append(COLON_SEPARATOR);
                        sb.append(option.getFlatCharge());
                        if (i < deliveryOptions.size() - 1) {
                            sb.append(COMMA_SEPARATOR);
                        }
                    }
                }

                row[16] = sb.toString();
                row[17] = null;
            } else {

                row[16] = null;
                row[17] = weight + " g";
            }

            //25 size
            row[18] = null;

//            //26 tax_percent                                                //no need for UK. included to price
//            row[25] = String.valueOf(item.getVatRate());
//
//            //27 tax_region
//            row[26] = null;

            //28 title
            row[19] = item.getName().replaceAll("\t", "");

            //29 upc
//            row[28] = null;      switched to gtin

            //30 tech_spec_link
//            if (!item.hasAttachments()) {
//                row[24] = null;
//            } else {
//                String contents = "";
//                for (Attachment attachment : item.getAttachments()) {
//                    contents = contents + attachment.getUrl() + " ";
//                }
//                contents.trim();
//                row[24] = contents;
//            }

            //31 condition
//            row[25] = "new";

//            //32 price_exvat
//            if (item.getSellPrice() != null) {
//                row[26] = item.getSellPrice().toString();
//            } else {
//                row[26] = null;
//            }

            //33 category
//            row[27] = "0";
            //online_only
            row[20] = ShoppingSettings.DispatchType.Collection.equals(ShoppingSettings.getInstance(context).getDispatchType()) ? "n" : "y";

            CollectionsUtil.replaceNulls(row, "");
            HtmlHelper.replaseSymbolsWithUTF(row);
			saver.next(row);

		}

		saver.end();


		super.exportFtp(file);

//		file.delete();

		return new FeedResult(csvman);
	}

}
