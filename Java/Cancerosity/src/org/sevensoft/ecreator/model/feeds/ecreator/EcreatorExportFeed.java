package org.sevensoft.ecreator.model.feeds.ecreator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import org.jdom.*;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.sevensoft.ecreator.iface.admin.feeds.ecreator.EcreatorExportFeedHandler;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.feeds.ExportFeed;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.Http;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

import com.enterprisedt.net.ftp.FTPException;

/**
 * @author sks 27 Jan 2007 17:30:09
 *
 * Export ecreator items
 *
 */
@Table("feeds_ecreator_export")
@Label("Ecreator export")
@Http
@HandlerClass(EcreatorExportFeedHandler.class)
public class EcreatorExportFeed extends ExportFeed {

	/**
	 * The item type to export
	 */
	private ItemType	itemType;

	public EcreatorExportFeed(RequestContext context) {
		super(context);
	}

	public EcreatorExportFeed(RequestContext context, boolean create) {
		super(context, create);
	}

	public final ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	@Override
	public void removeAttribute(Attribute attribute) {
	}

	@Override
	public FeedResult run(File file) throws IOException, FTPException, FeedException, JDOMException {
        if (file == null)
            file = File.createTempFile("itemexport", ".csv");

		if (itemType == null) {
			throw new FeedException("No item type set");
		}

		Element root = new Element("EcreatorExportFeed");
		Document doc = new Document(root);

		Element header = new Element("Header");
		root.addContent(header);

		header.addContent(new Element("Url").setText(config.getUrl()));

		// create attribute definitions
		for (Attribute attribute : getItemType().getAttributes()) {

			Element attributeEl = new Element("Attribute");
			root.addContent(attributeEl);

			Element nameEl = new Element("Name").setText(attribute.getName());
			attributeEl.addContent(nameEl);

			Element typeEl = new Element("Type").setText(attribute.getType().name());
			attributeEl.addContent(typeEl);

			Element displayableEl = new Element("Displayable").setText(String.valueOf(attribute.isDisplayable()));
			attributeEl.addContent(displayableEl);

			Element editableEl = new Element("Editable").setText(String.valueOf(attribute.isEditable()));
			attributeEl.addContent(editableEl);

			Element maxEl = new Element("Max").setText(String.valueOf(attribute.getMax()));
			attributeEl.addContent(maxEl);

			Element prefixEl = new Element("Prefix").setText(attribute.getPrefix());
			attributeEl.addContent(prefixEl);

			Element suffixEl = new Element("Suffix").setText(attribute.getSuffix());
			attributeEl.addContent(suffixEl);

			Element linkableEl = new Element("Linkable").setText(String.valueOf(attribute.isLinkable()));
			attributeEl.addContent(linkableEl);

			Element multiValuesEl = new Element("MultiValues").setText(String.valueOf(attribute.isMultiValues()));
			attributeEl.addContent(multiValuesEl);

			Element customOptionsOrderEl = new Element("CustomOptionsOrder").setText(String.valueOf(attribute.isCustomOptionsOrder()));
			attributeEl.addContent(customOptionsOrderEl);

			Element optionalEl = new Element("Optional").setText(String.valueOf(attribute.isOptional()));
			attributeEl.addContent(optionalEl);

			Element locationEl = new Element("Location").setText(String.valueOf(attribute.isLocation()));
			attributeEl.addContent(locationEl);

		}

		ItemSearcher searcher = new ItemSearcher(context);
		searcher.setStatus("live");
		searcher.setItemType(getItemType());
        searcher.setLimit(getPreviewCount());

		for (Item item : searcher) {

			Element itemEl = new Element("Item");
			root.addContent(itemEl);

			// id
			{
				Element idEl = new Element("Id").setText(item.getIdString());
				itemEl.addContent(idEl);
			}

			// name
			{
				Element nameEl = new Element("Name").setText(item.getName());
				itemEl.addContent(nameEl);
			}

			if (getItemType().isShortNames()) {
				Element shortNameEl = new Element("ShortName").setText(item.getShortName());
				itemEl.addContent(shortNameEl);
			}

			// content
			if (ItemModule.Content.enabled(context, item)) {
                String verify = null;
                if (item.getContent() != null) {
                    verify = Verifier.checkCharacterData(item.getContent());
                }
                if (verify == null) {
                    Element contentEl = new Element("Content").setText(item.getContent());
                    itemEl.addContent(contentEl);
                } else {
                   throw new FeedException(verify + ",  ItemId=" + item.getId());
                }
            }
			// reference
			if (getItemType().isReferences()) {
				Element reference = new Element("Reference").setText(item.getReference());
				itemEl.addContent(reference);
			}

			// item content
			if (getItemType().isFeatured()) {
				Element featuredEl = new Element("Featured").setText(String.valueOf(item.isFeatured()));
				itemEl.addContent(featuredEl);
			}

			// item content
			if (getItemType().isPrioritised()) {
				Element prioritisedEl = new Element("Prioritised").setText(String.valueOf(item.isPrioritised()));
				itemEl.addContent(prioritisedEl);
			}

			// categories
			if (ItemModule.Categories.enabled(context, item)) {
				for (Category category : item.getCategories()) {
					Element categoryEl = new Element("Category").setText(category.getName());
					itemEl.addContent(categoryEl);
				}
			}

			// attributes
			for (Map.Entry<Attribute, String> entry : item.getAttributeValues().entrySet()) {

				Attribute attribute = entry.getKey();
				String value = entry.getValue();

				Element attributeEl = new Element("Attribute");
				itemEl.addContent(attributeEl);

				Element attributeNameEl = new Element("Name").setText(attribute.getName());
				attributeEl.addContent(attributeNameEl);

				Element attributeValueEl = new Element("Value").setText(value);
				attributeEl.addContent(attributeValueEl);
			}

			// images
			if (itemType.getModules().contains(ItemModule.Images)) {
				for (Img image : item.getAllImages()) {

					Element imageEl = new Element("Image").setText(image.getUrl());
					itemEl.addContent(imageEl);
				}
			}

			// attachments
			for (Attachment attachment : item.getAttachments()) {

				Element attachmentEl = new Element("Attachment");
				itemEl.addContent(attachmentEl);

				attachmentEl.setAttribute("Id", attachment.getIdString());
				attachmentEl.addContent(new Element("Url").setText(attachment.getUrl()));
				attachmentEl.addContent(new Element("Filename").setText(attachment.getFilename()));
				attachmentEl.addContent(new Element("Name").setText(attachment.getName()));
				attachmentEl.addContent(new Element("DownloadLinkText").setText(attachment.getDownloadLinkText()));
				attachmentEl.addContent(new Element("Method").setText(attachment.getMethod().name()));

			}
		}

		XMLOutputter outputter = new XMLOutputter();
		outputter.setFormat(Format.getPrettyFormat());
		FileWriter writer = new FileWriter(file);
		outputter.output(doc, writer);
		writer.close();

		return null;
	}

	public final void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

}
