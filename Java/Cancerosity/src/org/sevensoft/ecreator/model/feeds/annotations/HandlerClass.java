package org.sevensoft.ecreator.model.feeds.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.sevensoft.jeezy.http.Handler;

/**
 * @author sks 29 Aug 2006 11:15:00
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface HandlerClass {

	Class<? extends Handler> value();
}
