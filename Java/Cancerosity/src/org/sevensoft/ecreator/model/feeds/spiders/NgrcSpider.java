package org.sevensoft.ecreator.model.feeds.spiders;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.httpc.HttpMethod;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 3 Nov 2006 16:07:16
 *
 */
public class NgrcSpider extends Spider {

	@Override
	public String getName() {
		return "Ngrc";
	}

	@Override
	public int spider(RequestContext context, ItemType itemType, String url, Category category) throws MalformedURLException, IOException {

		Attribute salutationAttribute = itemType.getAttribute("Saluation", AttributeType.Text);
		Attribute firstnameAttribute = itemType.getAttribute("First name", AttributeType.Text);
		Attribute surnameAttribute = itemType.getAttribute("Surname", AttributeType.Text);
		Attribute address1Attribute = itemType.getAttribute("Address1", AttributeType.Text);
		Attribute address2Attribute = itemType.getAttribute("Address2", AttributeType.Text);
		Attribute address3Attribute = itemType.getAttribute("Address3", AttributeType.Text);
		Attribute address4Attribute = itemType.getAttribute("Address4", AttributeType.Text);
		Attribute address5Attribute = itemType.getAttribute("Address5", AttributeType.Text);
		Attribute address6Attribute = itemType.getAttribute("Address6", AttributeType.Text);
		Attribute address7Attribute = itemType.getAttribute("Address7", AttributeType.Text);
		Attribute postcodeAttribute = itemType.getAttribute("Postcode", AttributeType.Text);
		Attribute telAttribute = itemType.getAttribute("Telephone", AttributeType.Text);
		Attribute trackAttribute = itemType.getAttribute("Track", AttributeType.Text);

		Attribute[] addressAttributes = new Attribute[] { address1Attribute, address2Attribute, address3Attribute, address4Attribute, address5Attribute,
				address6Attribute, address7Attribute };

		url = "http://www.ngrc.org.uk/default.asp?article=Trainers&parentarticletype=Racing&search=true&name=";

		for (char c = 65; c < 91; c++) {

			HttpClient hc = new HttpClient(url + c, HttpMethod.Get);
			hc.connect();
			String response = hc.getResponseString();

			Pattern pattern = Pattern
					.compile("<tr><td>&nbsp;</td></tr><tr bgcolor='#B7E6FF'><td><font face='Verdana, Arial, Helvetica, sans-serif' size='1'><b>(.*?)</b> - (.*?)</td></tr><tr bgcolor='#B7E6FF'><td><font face='Verdana, Arial, Helvetica, sans-serif' size='1'>(.*?).<tr bgcolor='#B7E6FF'><td><font face='Verdana, Arial, Helvetica, sans-serif' size='1'>(.*?)\\.");
			Matcher matcher = pattern.matcher(response);
			while (matcher.find()) {

				System.out.println(matcher.group(0));
				System.out.println(matcher.group(1));
				System.out.println(matcher.group(2));
				System.out.println(matcher.group(3));
				System.out.println("Telephone: " + matcher.group(4));

				String name = matcher.group(1).replace("&nbsp;", " ");
				String[] names = name.split("\\s");

				String track = matcher.group(2);
				String address = matcher.group(3);
				String tel = matcher.group(4);
				String postcode = address.replaceAll("^.*,", "").trim();
				address = address.replaceAll("[^,]*$", "").trim();

				String[] addresses = address.split(",");

				// check item exists
				if (Item.getByName(context, name) == null) {

					Item item = new Item(context, itemType, name, "Live", null);

					item.setAttributeValue(salutationAttribute, names[0]);

					if (names.length > 0)
						item.setAttributeValue(firstnameAttribute, names[1]);
					if (names.length > 1)
						item.setAttributeValue(surnameAttribute, names[2]);

					item.setAttributeValue(trackAttribute, track);
					item.setAttributeValue(telAttribute, tel);
					for (int n = 0; n < 7 && n < addresses.length; n++) {
						item.setAttributeValue(addressAttributes[n], addresses[n]);
					}
					item.setAttributeValue(postcodeAttribute, postcode);

				}
			}
		}

		return 0;
	}
}
