package org.sevensoft.ecreator.model.feeds;


/**
 * @author sks 25-Jan-2006 18:34:43
 *
 */
public class FeedException extends Exception {

	public FeedException() {
		super();
	}

	public FeedException(String message) {
		super(message);
	}

	public FeedException(String message, Throwable cause) {
		super(message, cause);
	}

	public FeedException(Throwable cause) {
		super(cause);
	}

}
