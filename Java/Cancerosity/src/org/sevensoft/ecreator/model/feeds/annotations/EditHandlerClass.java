package org.sevensoft.ecreator.model.feeds.annotations;

import org.sevensoft.jeezy.http.Handler;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author dme 29 Aug 2007 11:15:00
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface EditHandlerClass {

    public abstract Class<? extends Handler> value();
}