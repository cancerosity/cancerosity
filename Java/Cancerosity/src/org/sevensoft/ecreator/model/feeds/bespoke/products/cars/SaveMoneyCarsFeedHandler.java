package org.sevensoft.ecreator.model.feeds.bespoke.products.cars;

import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.Feed;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 10 May 2007 18:16:37
 *
 */
@Path("admin-feeds-savemoneycars.do")
public class SaveMoneyCarsFeedHandler extends FeedHandler {

	private SaveMoneyCarsFeed	feed;

	private Attribute			variantAttribute;
	private Attribute			makeAttribute;
	private Attribute			priceAttribute;
	private Attribute			detailsAttribute;
	private Attribute			modelAttribute;
	private Attribute			yearAttribute;
	private ItemType			itemType;

	public SaveMoneyCarsFeedHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Feed getFeed() {
		return feed;
	}

	@Override
	protected void saveSpecific() {

		feed.setDetailsAttribute(detailsAttribute);

		feed.setMakeAttribute(makeAttribute);
		feed.setModelAttribute(modelAttribute);
		feed.setYearAttribute(yearAttribute);
		feed.setVariantAttribute(variantAttribute);
		feed.setItemType(itemType);

	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Google base settings"));

		sb.append(new AdminRow("Item type", "Select the item type to import to.", new SelectTag(context, "itemType", feed.getItemType(), ItemType
				.getSelectionMap(context), "-None set-")));

		if (feed.hasItemType()) {

			sb.append(new AdminRow("Details attribute", new SelectTag(context, "detailsAttribute", feed.getDetailsAttribute(), feed.getItemType()
					.getAttributes(), "-None set-")));

			sb.append(new AdminRow("Make attribute", new SelectTag(context, "makeAttribute", feed.getMakeAttribute(), feed.getItemType().getAttributes(),
					"-None set-")));

			sb.append(new AdminRow("Model attribute", new SelectTag(context, "modelAttribute", feed.getModelAttribute(), feed.getItemType()
					.getAttributes(), "-None set-")));

			sb.append(new AdminRow("Year attribute", new SelectTag(context, "yearAttribute", feed.getYearAttribute(), feed.getItemType().getAttributes(),
					"-None set-")));

			sb.append(new AdminRow("Variant attribute", new SelectTag(context, "variantAttribute", feed.getVariantAttribute(), feed.getItemType()
					.getAttributes(), "-None set-")));

		}

		sb.append("</table>");
	}

}
