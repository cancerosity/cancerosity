package org.sevensoft.ecreator.model.feeds.bespoke.products.microp;

import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.Ftp;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.bespoke.products.openrange.OpenRangeImagesFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.products.openrange.OpenRangeFeed;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.stock.StockModule;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.products.MicroPFeedHandler;
import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.commons.skint.Money;
import org.jdom.JDOMException;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.enterprisedt.net.ftp.FTPException;
import com.ricebridge.csvman.CsvManager;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
@Table("feeds_micro_p")
@Label("Micro-P")
@HandlerClass(MicroPFeedHandler.class)
@Ftp
public class MicroPFeedOld extends ImportFeed {

    private static final int MANUFACTURER_NAME_INDEX = 0;           //Manufacturer Name
    private static final int STOCK_GROUP_CODE_INDEX = 1;            //Stock Group Code
    private static final int MP_PRODUCT_NUMBER_INDEX = 2;           //MP Product Number
    private static final int MANUFACTURER_PRODUCT_NUMBER_INDEX = 3; //Manufacturer Product Number
    private static final int PRODUCT_BARCODE_INDEX = 4;             //Product Barcode
    private static final int DESCRIPTION_INDEX = 5;                 //Product Description
    private static final int QTY_INDEX = 6;                         //Qty
    private static final int PRICE_INDEX = 7;                       //Price
    private static final int NEXT_TO_DUE_DATE_INDEX = 8;            //Next PO Due Date (To MP)
    private static final int CATEGORIES_INDEX = 9;                  //Category
//    private static final int OPEN_RANGE_URL_INDEX = 9;              //Open Range URL

    public static final String MANUF_PART_NUMBER = "Manuf. Part Number";

    protected Map<String, Category> categoriesCache = new HashMap();

    protected ItemType itemType;
    private boolean disableProducts;
    private Attribute manufPartNumberAttribute;

    protected MicroPFeedOld(RequestContext context) {
        super(context);
    }

    public MicroPFeedOld(RequestContext context, boolean create) {
        super(context, create);
        new OpenRangeImagesFeed(context, create);
        new OpenRangeFeed(context, create);
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public Attribute getManufPartNumberAttribute() {
        return (Attribute) (manufPartNumberAttribute == null ? null : manufPartNumberAttribute.pop());
    }

    public void setManufPartNumberAttribute(Attribute manufPartNumberAttribute) {
        this.manufPartNumberAttribute = manufPartNumberAttribute;
    }

    public void removeAttribute(Attribute attribute) {
    }

    public FeedResult run(File file) throws IOException, FTPException, FeedException, JDOMException {
        if (getItemType() == null) {
            return null;
        }
        logger.fine("["+ MicroPFeedOld.class+"] running feed=" + this);
		logger.fine("["+ MicroPFeedOld.class+"] itemType=" + getItemType());

        updateFeedCount();

        if (file == null) {
            file = getInputFile();
        }

        if (file == null) {
            throw new FeedException("No data supplied");
        }
        if (!ItemModule.Stock.enabled(context, getItemType())) {
            getItemType().addModule(ItemModule.Stock);
            getItemType().save();
            StockModule stockModule = getItemType().getStockModule();
            stockModule.setStockControl(StockModule.StockControl.RealTime);

            stockModule.save();

        }
        return prices(file);
    }

   private FeedResult prices(File file) throws IOException {
        CsvManager csvman = new CsvManager();

        List<List<String>> rows = csvman.loadAsListsFromString(SimpleFile.readString(file));

        if (rows.size() < 1) {
            logger.fine("["+ MicroPFeedOld.class+"] the file is empty!");
            return null;
        }

        Map<String, Item> itemsCache = new HashMap<String, Item>();
  //      Map<String, Category> categoriesCache = new HashMap();
       int created = 0;
       long time = System.currentTimeMillis();

        for (List<String> row : rows) {
            Item item = Item.getByRef(context, getItemType(), row.get(MANUFACTURER_PRODUCT_NUMBER_INDEX));
            if (item == null) {
                String name = row.get(DESCRIPTION_INDEX) != null ? row.get(DESCRIPTION_INDEX) : row.get(MANUFACTURER_NAME_INDEX);
                item = new Item(context, getItemType(), name, "LIVE", null);
                created ++;
            }

            item.setReference(row.get(MANUFACTURER_PRODUCT_NUMBER_INDEX));

            //stock level
            if (row.get(QTY_INDEX) != null && row.get(QTY_INDEX).length() != 0) {
                item.setOurStock(Integer.parseInt(row.get(QTY_INDEX)));
            }

            //cost price
            Money costPrice = new Money(0);

            try {
                costPrice = costPrice.add(new Money(row.get(PRICE_INDEX)));
            } catch (RuntimeException e1) {
                logger.warning("[" + MicroPFeedOld.class + "] can not add price. " + e1);
            }
            item.setStandardPrice(null);

            item.setOurCostPrice(costPrice);

            item.setAttributeValue(manufPartNumberAttribute, row.get(MANUFACTURER_PRODUCT_NUMBER_INDEX));

            //manufacturer
            /*Category category = categoriesCache.get(row.get(MANUFACTURER_NAME_INDEX));
            if (category == null) {
                category = Category.getByName(context, row.get(MANUFACTURER_NAME_INDEX));
            }
            if (category == null) {
                category = new Category(context, row.get(MANUFACTURER_NAME_INDEX), null);
                categoriesCache.put(row.get(MANUFACTURER_NAME_INDEX), category);
            } else {
                categoriesCache.put(row.get(MANUFACTURER_NAME_INDEX), category);
            }
            item.addCategory(category);

            item.save();*/
            String categorySequence = row.get(CATEGORIES_INDEX);
            String[] categories = categorySequence.split("~");
            Category category = categoriesCache.get(categories[categories.length - 1]);
            if (category == null) {
                category = Category.getByName(context, categories[categories.length - 1]);
            }
            if (category == null) {
                category = getCategory(context, categories[0], null);
                for (int i = 1; i < categories.length - 1; i++) {
                    category = getCategory(context, categories[i], category);
                }
                category = new Category(context, categories[categories.length - 1], category);
                categoriesCache.put(categories[categories.length - 1], category);
            }
            else {
                categoriesCache.put(categories[categories.length - 1], category);
            }

            item.addCategory(category);

            item.save();
        }
        itemsCache.clear();
        categoriesCache.clear();

       time = System.currentTimeMillis() - time;

       if (disableProducts) {
            List<Item> items = Item.get(context, getItemType(), "LIVE");
            for (Item item : items) {
                if (item.getLastUpdatedString().contains("days")) {
                    String[] tmpStr = item.getLastUpdatedString().split(" ");
                    if (new Integer(tmpStr[0]) > 30) {
                        item.setStatus("DISABLED");
                    }
                }
            }
        }

       return new FeedResult(created, (int) csvman.getLineCount(), time);
    }

    /**
     * Method terurns category with real name: finds it in cache or create a new one
     *
     * @param context - requestContext
     * @param realName - the name of the category
     * @param parent - parent category name
     * @return category
     */
    public Category getCategory(RequestContext context, String realName, Category parent) {
        Category category = categoriesCache.get(realName);
        if (category == null) {
            if (parent == null) {
                category = Category.getByName(context, realName);
            } else {
                category = parent.getChild(realName);
            }
        }
        if (category == null) {
            category = new Category(context, realName, parent);
            categoriesCache.put(realName, category);
        } else {
            categoriesCache.put(realName, category);
        }
        return category;
    }

    public boolean disableProducts() {
        return disableProducts;
    }

    public void setDisableProducts(boolean disableProducts) {
        this.disableProducts = disableProducts;
    }
}
