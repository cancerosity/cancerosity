package org.sevensoft.ecreator.model.feeds.csv;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletOutputStream;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.collections.maps.MultiHashMap;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.feeds.csv.CsvFeedHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.annotations.Ftp;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.Http;
import org.sevensoft.ecreator.model.feeds.csv.parsers.ItemParser;
import org.sevensoft.ecreator.model.feeds.csv.parsers.OrderLineParser;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.ecreator.model.misc.csv.CsvImporter;
import org.sevensoft.ecreator.model.misc.csv.CsvUtil;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.CsvSpec;

/**
 * @author sks 3 Aug 2006 06:18:49
 *
 */
@Table("feeds_custom")
@Label("CSV Import")
@Ftp
@Http
@HandlerClass(CsvFeedHandler.class)
public class CsvImportFeed extends ImportFeed implements CsvImporter {

	public enum Type {
		Items, Orders, OrderLines
	}

	/**
	 * The column ID used for sku lookup
	 */
	private int		skuField;

	/**
	 * The type of feed
	 */
	private Type	type;

	/**
	 * Our separator per field
	 */
	private String	separator;

	/**
	 * Start row of the data
	 */
	private int		startRow;

	/**
	 *  Quote character used to enclose fields that contain a sep character
	 */
	private String	quote;

	/**
	 * The character used for escaping quote characters inside already quoted text. EXCEL uses quote twice
	 */
	private String	escape;

	private int		refField;

	/**
	 * The column number of the field that is used to lookup the object by id
	 */
	private int		idField;

	/**
	 * Skip if item cannot be found, 
	 * otherwise create
	 */
	private boolean	skip;

	/**
	 * Generate a password and email it to the members email address
	 */
	private boolean	passwordEmail;

	/**
	 * After an order lines import remove any lines from orders that were not included in the import
	 */
	private boolean	removeOmittedLines;

	/**
	 * Change the status of all orders to this for all referenced order lines after the import has been completed.
	 */
	private String	orderStatus;

	private ItemType	itemType;

    /**
	 * The column number of the field that is used to Import item or not
	 */
	private int		importYnField;

	private CsvImportFeed(RequestContext context) {
		super(context);
	}

	public CsvImportFeed(RequestContext context, boolean create) {
		super(context, create);

		CsvManager csvman = new CsvManager();
		CsvSpec csvspec = csvman.getCsvSpec();

		separator = csvspec.getSeparator();
		quote = String.valueOf(csvspec.getQuote());
		escape = String.valueOf(csvspec.getEscape());

		save();
	}

	public void addField(FieldType fieldType) {
		new CsvImportFeedField(context, this, fieldType);
	}

	/**
	 * Returns true if this 
	 */
	private boolean containsFieldType(FieldType fieldType) {
		for (CsvImportFeedField field : getFields()) {
			if (field.getFieldType() == fieldType) {
				return true;
			}
		}
		return false;
	}

	@Override
	public synchronized boolean delete() {
		new Query(context, "delete from # where feed=?").setTable(CsvImportFeedField.class).setParameter(this).run();
		return super.delete();
	}

	private List<CsvImportFeedField> getCombLookupFields() {

		List<CsvImportFeedField> fields = new ArrayList<CsvImportFeedField>(getFields());
		CollectionsUtil.filter(fields, new Predicate<CsvImportFeedField>() {

			public boolean accept(CsvImportFeedField e) {
				return e.isCombinationLookup();
			}
		});
		return fields;
	}

	public CsvManager getCsvManager() {
		return CsvUtil.getCsvManager(this);
	}

	/**
	 * Returns the value of the first field set to field type email.
	 */
	public String getEmailValue(String[] row) {

		if (row == null) {
			return null;
		}

		for (CsvImportFeedField field : getFields()) {

			if (field.getFieldType() == FieldType.Email) {
				String value = field.getValue(row);
				logger.fine("return email value=" + value);
				return value;
			}
		}

		logger.fine("no email value found");
		return null;
	}

	public String getEscape() {
		return escape;
	}

	public List<CsvImportFeedField> getFields() {

		Query q = new Query(context, "select * from # where feed=?");
		q.setTable(CsvImportFeedField.class);
		q.setParameter(this);
		List<CsvImportFeedField> fields = q.execute(CsvImportFeedField.class);
		Collections.sort(fields);
		return fields;
	}

	public int getIdField() {
		return idField;
	}

	public String getIdValue(String[] row) {

		if (idField < 1 || idField > row.length) {
			return null;
		}

		String ref = row[idField - 1];
		if (ref != null && ref.length() == 0) {
			ref = null;
		}

		return ref;
	}

	private List<CsvImportFeedField> getIndLookupFields() {

		List<CsvImportFeedField> fields = new ArrayList<CsvImportFeedField>(getFields());
		CollectionsUtil.filter(fields, new Predicate<CsvImportFeedField>() {

			public boolean accept(CsvImportFeedField e) {
				return e.isIndividualLookup();
			}
		});
		return fields;
	}

	public Item getItemByAttributeLookup(String[] row) {

		/*
		 * Do lookup by individual attributes first
		 */
		List<CsvImportFeedField> fields = getIndLookupFields();
		if (fields.size() > 0) {
			logger.fine("ind lookup using fields=" + fields);
			for (CsvImportFeedField field : fields) {

				Attribute attribute = field.getAttribute();
				String value = field.getValue(row);

				if (value != null) {
					Item item = attribute.getItemFromValue(value);
					if (item != null) {
						return item;
					}
				}
			}
		}

		fields = getCombLookupFields();
		if (fields.size() > 0) {

			logger.fine("combo lookup using fields=" + fields);
			MultiValueMap<Attribute, String> map = new MultiHashMap<Attribute, String>();
			for (CsvImportFeedField field : fields) {

				Attribute attribute = field.getAttribute();
				String value = field.getValue(row);

				// if we have a single null value then it could break the lookup, so exit
				if (value == null) {
					return null;
				}

				map.put(attribute, value);
			}

			ItemSearcher search = new ItemSearcher(context);
			search.setAttributeValues(map);
			return search.getItem();
		}

		return null;
	}

	public final ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public String getQuote() {
		return quote;
	}

	public int getRefField() {
		return refField;
	}

	public String getRefValue(String[] row) {

		if (row == null) {
			return null;
		}

		if (refField < 1 || refField > row.length) {
			return null;
		}

		String ref = row[refField - 1];
		if (ref != null && ref.length() == 0) {
			ref = null;
		}

		return ref;

	}

    public boolean isImportField(String[] row) {

        if (row == null) {
            return false;
        }

        if (importYnField < 1 || importYnField > row.length) {
            return true;
        }

        String importField = row[importYnField - 1];
        return importField == null || importField.length() ==0 || importField.equalsIgnoreCase("yes");

    }

	public String getSeparator() {
		return separator;
	}

	public int getSkuField() {
		return skuField;
	}

	public String getSkuValue(String[] row) {

		if (row == null) {
			return null;
		}

		if (skuField < 1 || skuField > row.length) {
			return null;
		}

		String sku = row[skuField - 1];
		if (sku != null && sku.length() == 0) {
			sku = null;
		}

		return sku;
	}

    public int getImportYnField() {
        return importYnField;
    }

    public final int getStartRow() {
		return startRow;
	}

	public Type getType() {
		return type == null ? Type.Items : type;
	}

	public String getValue(FieldType fieldType, String[] row) {

		for (CsvImportFeedField field : getFields()) {

			if (field.getFieldType() == fieldType) {
				return field.getValue(row);

			}
		}

		return null;
	}

	/**
	 * Returns a list of values for this field type
	 */
	public List<String> getValues(FieldType fieldType, String[] row) {

		List<String> values = new ArrayList();
		for (CsvImportFeedField field : getFields()) {

			if (field.getFieldType() == fieldType) {
				values.addAll(field.getValues(row));
			}
		}

		return values;
	}

	public boolean hasItemType() {
		return itemType != null;
	}

	public boolean hasNameField() {

		for (CsvImportFeedField field : getFields()) {
			if (field.getFieldType() == FieldType.Name) {
				return true;
			}
		}

		return false;
	}

    public boolean hasOptionReferenceField() {

        for (CsvImportFeedField field : getFields()) {
            if (field.getFieldType() == FieldType.OptionReference) {
                return true;
            }
        }

        return false;
    }

	public boolean hasOrderStatus() {
		return orderStatus != null;
	}

	/**
	 * Create items if not found
	 */
	public boolean isCreate() {

		if (skip) {
			return false;
		}

		/*
		 * If we want to create we must have a name field
		 */
		return hasNameField();
	}

	public boolean isIgnoredCategory(List<String> values) {

		for (String value : values) {
			if (getIgnoredCategories().contains(value.toLowerCase())) {
				return true;
			}
		}
		return false;
	}

	public boolean isPasswordEmail() {
		return passwordEmail;
	}

	public boolean isRemoveOmittedLines() {
		return removeOmittedLines;
	}

	@Override
	public void removeAttribute(Attribute attribute) {

		for (CsvImportFeedField field : getFields()) {
			if (attribute.equals(field.getAttribute())) {
				removeField(field);
			}
		}

	}

	private void removeAttributeFields() {
		Query q = new Query(context, "delete from # where feed=? and attribute>0");
		q.setTable(CsvImportFeedField.class);
		q.setParameter(this);
		q.run();
	}

	public void removeField(CsvImportFeedField field) {
		field.delete();
	}

	private void removeFields() {
		SimpleQuery.delete(context, CsvImportFeedField.class, "feed", this);
	}

	public FeedResult run(File file) throws FeedException, IOException {

		logger.fine("[CsvImportFeed] running feed=" + this);
		logger.fine("[CsvImportFeed] itemType=" + getItemType());
		logger.fine("[CsvImportFeed] supplier=" + getSupplier(true));
		logger.fine("[CsvImportFeed] file=" + file);

		if (file == null) {
			file = getInputFile();
		}

		logger.fine("[CsvImportFeed] reading data from file=" + file);
		if (file == null) {
			throw new FeedException("No data found");
		}

		updateFeedCount();

		CsvManager csvman = getCsvManager();

		FeedResult result = null;
		switch (getType()) {

		case OrderLines:

			OrderLineParser importer = new OrderLineParser(context, this);
			logger.fine("[CsvImportFeed] importer=" + importer);
			csvman.load(file, importer);
			importer.teardown();

			break;

		case Items:

			if (itemType == null) {
				throw new FeedException("Item type is not set");
			}

			ItemParser itemParser = new ItemParser(context, this);
			logger.fine("[CsvImportFeed] itemParser=" + itemParser);
			csvman.load(file, itemParser);

			// if delete non updated then delete all items that do not equal our current feed count.
			if (isDeleteExpired()) {
				super.deleteExpired();
			}

			result = new FeedResult(itemParser.getCreated(), itemParser.getRowsProcessed(), csvman.getTimeTakenInSeconds());
			break;

		case Orders:
			break;
		}

		file.delete();

		return result;
	}

	public void setCreate(boolean create) {
		skip = !create;
	}

	public void setEscape(String s) {
		this.escape = s;
	}

	public void setIdField(int idField) {
		this.idField = idField;
	}

	public boolean setItemType(ItemType itemType) {

		if (ObjectUtil.equal(this.itemType, itemType)) {
			return false;
		}

		this.itemType = itemType;
		removeAttributeFields();

		return true;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public void setPasswordEmail(boolean generatePassword) {
		this.passwordEmail = generatePassword;
	}

	public void setQuote(String quote) {
		this.quote = quote;
	}

	public void setRefField(int refField) {
		this.refField = refField;
	}

	public void setRemoveOmittedLines(boolean removeOmittedLines) {
		this.removeOmittedLines = removeOmittedLines;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public void setSkuField(int skuFieldNumber) {
		this.skuField = skuFieldNumber;
	}

	public final void setStartRow(int startRow) {
		this.startRow = startRow;
	}

	public void setType(Type type) {
		this.type = type;
	}

    public void setImportYnField(int importYnField) {
        this.importYnField = importYnField;
    }

    public void write(ServletOutputStream out) throws IOException, FeedException {
	}

	public CsvImportFeedField getField(FieldType fieldType) {
		for (CsvImportFeedField field : getFields()) {
			if (field.getFieldType() == fieldType) {
				return field;
			}
		}
		return null;
	}

    public void upadteOptions(String[] row) {

        int optionReferenceCol = -1;
        int optionStockCol = -1;
        int optionPriceCol = -1;

        if (containsFieldType(FieldType.OptionReference)) {
            optionReferenceCol = getField(FieldType.OptionReference).getColumns().get(0);
        }

        if (containsFieldType(FieldType.OptionStock)) {
            optionStockCol = getField(FieldType.OptionStock).getColumns().get(0);
        }

        if (containsFieldType(FieldType.OptionSellprice)) {
            optionPriceCol = getField(FieldType.OptionSellprice).getColumns().get(0);
        }

        if (optionReferenceCol != -1) {
            ItemOptionSelection selection = ItemOptionSelection.getSelectionByProductCode(context, row[optionReferenceCol - 1]);
            if (optionStockCol != -1 && selection != null) {
                selection.setStock(Integer.parseInt(row[optionStockCol - 1]));
                selection.save();
            }
            if (optionPriceCol != -1 && selection != null) {
                selection.setSalePrice(new Money(row[optionPriceCol - 1]));
                selection.save();
            }
        }

    }

}
