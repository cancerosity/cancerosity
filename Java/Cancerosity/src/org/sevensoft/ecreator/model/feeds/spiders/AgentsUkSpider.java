package org.sevensoft.ecreator.model.feeds.spiders;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.categories.Category;

/**
 * @author sks 07-Dec-2005 16:35:30
 * 
 */
public class AgentsUkSpider {

	//	public static void main(String[] args) {
	//
	//		new AgentsUkSpider().spider("http://www.agents-uk.com/agents/short_info/67\n"
	//				+ "http://www.agents-uk.com/agents/info/66\nhttp://www.agents-uk.com/agents/short_info/695", null);
	//
	//	}

	Pattern	pattern		= Pattern.compile("<h3>E-mail / Web</h3></td></tr>(.*?)</table>", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
	Pattern	addressPattern	= Pattern.compile("<td valign=top width=33%>(.*?)</td>", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
	Pattern	telephonePattern	= Pattern.compile("<td valign=top width=34%>(.*?)</td>", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
	Pattern	emailWebPattern	= Pattern.compile("<td valign=top nowrap width=33%>(.*?)</td>", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
	Pattern	webPattern		= Pattern.compile("www\\.[a-zA-Z0-9\\.\\-]{3,}");
	Pattern	emailPattern	= Pattern.compile("[a-zA-Z0-9\\.]{2,}@[a-zA-Z0-9\\.\\-]{3,}");
	Pattern	postcodePattern	= Pattern.compile(".*{2,5}\\d\\w\\w$");
	Pattern	namePattern		= Pattern.compile("<h2>(.*?)</h2>", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

	public int spider(List<String> urls, List<Category> categories) {

		String name = null, address = null, email = null, web = null, postcode = null, telephone = null;
		int count = 0;

		for (String url : urls) {

			//HttpClient hc = new HttpClient(url);

			url.length();
			String input = null;

			Matcher matcher = namePattern.matcher(input);
			if (matcher.find())
				name = matcher.group(1);

			if (name == null)
				continue;

			int start = name.indexOf("=\"");
			if (start > 0) {
				if (name.charAt(start - 1) == 'h')
					start--;
			}

			int end = name.lastIndexOf("=\"") + 2;
			if (end < name.length()) {
				if (name.charAt(end) == '1')
					end++;
			}

			if (start > -1 && end > -1) {
				name = name.substring(0, start) + name.substring(end, name.length());
			}

			matcher = pattern.matcher(input);

			if (matcher.find())
				input = matcher.group(1);
			else
				continue;

			matcher = addressPattern.matcher(input);
			if (matcher.find()) {

				address = matcher.group(1);
				if (address != null) {

					List<String> lines = new ArrayList<String>();
					for (String line : address.split("<br.*?>")) {

						line = line.trim();
						if (line.length() > 0)
							lines.add(line);

					}

					address = StringHelper.implode(lines, "\n", true);

					matcher = postcodePattern.matcher(address);
					if (matcher.find()) {
						postcode = matcher.group(0).trim();
						address = matcher.replaceFirst("").trim();
					}
				}
			}

			matcher = telephonePattern.matcher(input);
			if (matcher.find())
				telephone = matcher.group(1).replaceAll("<br.*$", "").trim();

			matcher = emailWebPattern.matcher(input);
			if (matcher.find()) {

				String string = matcher.group(1);

				matcher = emailPattern.matcher(string);
				if (matcher.find())
					email = matcher.group(0);

				matcher = webPattern.matcher(string);
				if (matcher.find())
					web = matcher.group(0);

			}

			System.out.println(name);
			System.out.println(address);
			System.out.println(postcode);
			System.out.println(telephone);
			System.out.println(email);
			System.out.println(web);

			throw new RuntimeException();

			//				Listing listing = new Listing(name, Listing.Status.Live);
			//				listing.setWebsite(web);
			//				listing.setEmail(email);
			//				listing.setPostcode(postcode);
			//				listing.setAddress(address);
			//				listing.setTelephone(telephone);
			//				listing.save();
			//
			//				if (categories != null)
			//					for (Category category : categories)
			//						listing.addCategory(category);
			//
			//				count++;

		}

		return count;

	}
}
