package org.sevensoft.ecreator.model.feeds.bespoke.property.kyero;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.property.KyeroFeedHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.Http;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 27 Dec 2006 13:35:07
 *
 */
@Table("feeds_kyero")
@HandlerClass(KyeroFeedHandler.class)
@Label("Kyero Property")
@Http
public class KyeroFeed extends ImportFeed {

	private Attribute	priceAttribute;
	private Attribute	townAttribute;
	private Attribute	bedsAttribute;
	private Attribute	typeAttribute;
	private Attribute	bathsAttribute;
	private Attribute	provinceAttribute;
	private Attribute	poolAttribute;
	private ItemType	itemType;

	private KyeroFeed(RequestContext context) {
		super(context);
	}

	public KyeroFeed(RequestContext context, boolean create) {
		super(context, create);
	}

	public final Attribute getBathsAttribute() {
		return (Attribute) (bathsAttribute == null ? null : bathsAttribute.pop());
	}

	public final Attribute getBedsAttribute() {
		return (Attribute) (bedsAttribute == null ? null : bedsAttribute.pop());
	}

	public final ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public final Attribute getPoolAttribute() {
		return (Attribute) (poolAttribute == null ? null : poolAttribute.pop());
	}

	public final Attribute getPriceAttribute() {
		return (Attribute) (priceAttribute == null ? null : priceAttribute.pop());
	}

	public final Attribute getProvinceAttribute() {
		return (Attribute) (provinceAttribute == null ? null : provinceAttribute.pop());
	}

	public final Attribute getTownAttribute() {
		return (Attribute) (townAttribute == null ? null : townAttribute.pop());
	}

	public final Attribute getTypeAttribute() {
		return (Attribute) (typeAttribute == null ? null : typeAttribute.pop());
	}

	public boolean hasItemType() {
		return itemType != null;
	}

	@Override
	public void removeAttribute(Attribute attribute) {

		if (attribute.equals(priceAttribute)) {
			priceAttribute = null;
		}

		if (attribute.equals(typeAttribute)) {
			typeAttribute = null;
		}

		if (attribute.equals(townAttribute)) {
			townAttribute = null;
		}

		if (attribute.equals(provinceAttribute)) {
			provinceAttribute = null;
		}

		if (attribute.equals(poolAttribute)) {
			poolAttribute = null;
		}

		if (attribute.equals(bathsAttribute)) {
			bathsAttribute = null;
		}

		if (attribute.equals(bedsAttribute)) {
			bedsAttribute = null;
		}

		save();
	}

	@Override
	public FeedResult run(File file) throws MalformedURLException, IOException, JDOMException, FeedException {

		// get xml file
		if (file == null) {
			file = getInputFile();
		}

		if (itemType == null) {
			throw new FeedException("No item type set");
		}

		itemType.setReferences(true);
		itemType.save();

		long time = System.currentTimeMillis();
		int created = 0;
		int processed = 0;

		Document doc = new SAXBuilder().build(file);
		Element root = doc.getRootElement();

		List<Element> propertyEls = root.getChildren("property");
		for (Element propertyEl : propertyEls) {

			processed++;

			String ref = propertyEl.getChildTextTrim("ref");
			String price = propertyEl.getChildTextTrim("price");
			String baths = propertyEl.getChildTextTrim("baths");
			String beds = propertyEl.getChildTextTrim("beds");
			boolean pool = "1".equals(propertyEl.getChildTextTrim("pool"));
			String town = propertyEl.getChildTextTrim("town");
			String province = propertyEl.getChildTextTrim("province");

			logger.fine("[KyeroFeed] ref=" + ref + ", town=" + town + ", pool=" + pool + ", province=" + province);

			Element typeEl = propertyEl.getChild("type");
			String type = typeEl.getChildTextTrim("en");

			Element descEl = propertyEl.getChild("desc");
			String desc = descEl.getChildTextTrim("en");

			// get item from reference
			Item item = Item.getByRef(context, null, ref);
			if (item == null) {

				String name = beds + " bed " + type + " in " + town + " #" + ref;
				item = createItem(itemType, name);
				created++;
				logger.fine("[KyeroFeed] item created=" + created + ", name=" + name);

				item.setReference(ref);
				item.setContent(desc);
				item.save();

				Map<Attribute, String> avs = new HashMap();
				avs.put(getTypeAttribute(), type);
				avs.put(getPriceAttribute(), price);
				avs.put(getBathsAttribute(), String.valueOf(baths));
				avs.put(getProvinceAttribute(), province);
				avs.put(getBedsAttribute(), String.valueOf(beds));
				avs.put(getTownAttribute(), town);
				avs.put(getPoolAttribute(), pool ? "Yes" : "No");

				logger.fine("[KyeroFeed] avs=" + avs);
				item.setAttributeValues(avs);

				Element imagesEl = propertyEl.getChild("images");
				if (imagesEl != null) {
					List<Element> imageEls = imagesEl.getChildren("image");
					for (Element imageEl : imageEls) {

						String url = imageEl.getChildTextTrim("url");
						try {
							logger.fine("[KyeroFeed] adding image=" + url);
							item.addImage(new URL(url), true, true, false);
						} catch (ImageLimitException e) {
							e.printStackTrace();
						}

					}
				}
			}
		}

		time = System.currentTimeMillis() - time;
		return new FeedResult(created, processed, time);
	}

	public final void setBathsAttribute(Attribute bathsAttribute) {
		this.bathsAttribute = bathsAttribute;
	}

	public final void setBedsAttribute(Attribute bedsAttribute) {
		this.bedsAttribute = bedsAttribute;
	}

	public final void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	public final void setPoolAttribute(Attribute poolAttribute) {
		this.poolAttribute = poolAttribute;
	}

	public final void setPriceAttribute(Attribute priceAttribute) {
		this.priceAttribute = priceAttribute;
	}

	public final void setProvinceAttribute(Attribute provinceAttribute) {
		this.provinceAttribute = provinceAttribute;
	}

	public final void setTownAttribute(Attribute townAttribute) {
		this.townAttribute = townAttribute;
	}

	public final void setTypeAttribute(Attribute typeAttribute) {
		this.typeAttribute = typeAttribute;
	}

}
