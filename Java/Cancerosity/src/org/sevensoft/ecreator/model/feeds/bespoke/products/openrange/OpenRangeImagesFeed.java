package org.sevensoft.ecreator.model.feeds.bespoke.products.openrange;

import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.Ftp;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.products.OpenRangeImagesFeedHandler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.commons.simpleio.FileExistRule;
import org.sevensoft.commons.simpleio.SimpleFile;
import org.jdom.JDOMException;

import java.io.*;
import java.util.zip.ZipFile;
import java.util.zip.ZipEntry;
import java.util.List;
import java.util.ArrayList;
import java.util.Enumeration;

import com.enterprisedt.net.ftp.FTPException;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
@Table("feeds_open_range_images")
@Label("Open Range Images")
@HandlerClass(OpenRangeImagesFeedHandler.class)
@Ftp
public class OpenRangeImagesFeed extends ImportFeed {

    protected OpenRangeImagesFeed(RequestContext context) {
        super(context);
    }

    public OpenRangeImagesFeed(RequestContext context, boolean create) {
        super(context, create);
    }

    public void removeAttribute(Attribute attribute) {
    }

    public FeedResult run(File file) throws IOException, FTPException, FeedException, JDOMException {
        updateFeedCount();

        if (file == null) {
            file = getInputFile();
        }

        if (file == null) {
            throw new FeedException("No data supplied");
        }
        ZipFile zipFile = null;
        List<File> list;
        long time = System.currentTimeMillis();

        try {

            zipFile = new ZipFile(file);
            list = extractToDir(zipFile, ResourcesUtils.getRealImagesDir(), true, null, FileExistRule.Skip);
        }
        finally {
            if (zipFile != null) {
                zipFile.close();
            }
        }

        time = System.currentTimeMillis() - time;

        return new FeedResult(list.size(), list.size(), time);
    }

    public List<File> extractToDir(ZipFile zipFile, File dir, boolean stripPaths, FilenameFilter filter, FileExistRule fileExistRule)
            throws IOException {

        List<File> files = new ArrayList();
        Enumeration e = zipFile.entries();
        while (e.hasMoreElements()) {

            ZipEntry entry = (ZipEntry) e.nextElement();
            if (entry.isDirectory()){
                entry = (ZipEntry) e.nextElement();
            }
            String filename = entry.getName();

            // check filename in filter if not null
            if (filter != null) {
                if (!filter.accept(null, filename)) {
                    continue;
                }
            }

            if (stripPaths) {
                filename = SimpleFile.stripPaths(filename).replaceAll("/", "");
            }

            // create file to write to
            File file;
            if (dir == null) {

                file = File.createTempFile("simplezip_temp", null);
                file.deleteOnExit();

            } else {

                // strip whitespace
                filename = filename.replaceAll("\\s", "_");

                if (fileExistRule == FileExistRule.Rename) {
                    filename = SimpleFile.getUniqueFilename(filename, dir.getAbsolutePath());
                }

                file = new File(dir.getAbsolutePath() + "/" + filename);

                if (fileExistRule == FileExistRule.Skip) {
                    if (file.exists()) {
                        continue;
                    }
                }
            }

            BufferedInputStream in = null;
            BufferedOutputStream out = null;

            try {

                in = new BufferedInputStream(zipFile.getInputStream(entry));
                out = new BufferedOutputStream(new FileOutputStream(file));

                byte[] b = new byte[1024];
                int i = -1;
                while ((i = in.read(b, 0, 1024)) != -1) {
                    out.write(b, 0, i);
                }

                files.add(file);

            } catch (IOException e1) {
                throw e1;

            } finally {

                try {
                    if (in != null) {
                        in.close();
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }

        return files;
    }

}
