package org.sevensoft.ecreator.model.feeds.bespoke.property.propertyfinder;

import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FTPTransferType;
import com.ricebridge.csvman.CsvManager;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.property.PropertyFinderFeedHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.feeds.ExportFeed;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.annotations.Ftp;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author sks 04-Dec-2005 18:53:03
 */
@Table("feeds_propertyfeed")
@Label("Property Finder")
@Ftp()
@HandlerClass(PropertyFinderFeedHandler.class)
public class PropertyFinderFeed extends ExportFeed {

    private enum PropertyType {
        studio, flat, apartment, terrace, semi, detached, retirement, bungalow, shared_flat("shared flat"), shared_house("shared house");

        private String string;

        private PropertyType() {
        }

        private PropertyType(String string) {
            this.string = string;
        }

        @Override
        public String toString() {
            return string == null ? name() : string;
        }

    }

    private static List<String> locations;

    private String branchId;

    private ItemType itemType;

    public PropertyFinderFeed(RequestContext context) {
        super(context);
    }

    public PropertyFinderFeed(RequestContext context, boolean create) {
        super(context, create);
    }

    public String getBranchId() {
        return branchId;
    }

    public final ItemType getItemType() {
        return (ItemType) (itemType == null ? null : itemType.pop());
    }

    @Override
    public void removeAttribute(Attribute attribute) {
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    @Override
    public FeedResult run(File file) throws IOException, FTPException, FeedException {

        if (branchId == null) {
            throw new FeedException("No branch id set in account field");
        }

        if (itemType == null) {
            throw new FeedException("No itemType set");
        }

        List<String> imageFilenames = new ArrayList();

        CsvManager csvman = new CsvManager();
        csvman.setSeparator("|");

        // date
        String date = new Date().toString("yyyyMMdd");

        int limit = getPreviewCount() == 0 ? 1000 : getPreviewCount();
        List<Item> items = Item.get(context, getItemType(), "Live", 0, limit);
        if (items.size() == 0) {
            return null;
        }

        int records = 0;
        List<String[]> rows = new ArrayList<String[]>();
        for (Item item : items) {

            if (!item.hasContent())
                continue;

            String postcode = item.getAttributeValue("postcode", false);
            if (postcode == null)
                continue;

            String address = item.getAttributeValue("address", false);
            if (address == null)
                continue;

            String price = item.getAttributeValue("price", false);
            if (price == null)
                continue;

            // find out if this property is rental or not
            boolean rental = false;
            for (Category category : item.getCategories()) {
                String string = category.getFullName().toLowerCase();
                if (string.contains("rent") || string.contains("letting")) {
                    rental = true;
                    break;
                }
            }

            String[] row = new String[100];

            // 1 Unique ID Text 12 Yes A unique reference number for the branch/property. Must contain only alphanumeric characters or
            // underscores. If reference is null we will use standard id
            row[0] = item.getIdString();

            /*
                * 2 Street Name Text 255 Yes Use the format �street name� or �street name, area�. DO NOT include the Village/Town/City as this is
                * added automatically by the site, using Field 3 (in this case Bath) below together with the postcode. E.g. �College Road,
                * Lansdown�.
                *
                * NB � Under no circumstances must the house name or number be included in this field.
                */
            String name = item.getName();
            name = name.replaceAll("\\d", "");
            row[1] = name;

            // 3 District Text 255 Yes Village/Town/City if outside London. District or Area if inside London (for more accurate searches)
            //			 4 County Text 50 Yes Valid county name or �Greater London� or �London�
            //			String[] lines = ContactsUtil.getAddressLines(address);
            //			if (lines != null && lines.length > 0) {

            // we will take the district as the first line of the address
            //				row[2] = lines[0].trim();
            //				row[3] = "South Yorkshire";
            //			}

            // 5 Postcode Text 12 Yes Full valid U.K. postcode required, and essential for correct location searching.
            // The absolute minimum requirement for this field is the postcode minus the final alpha characters e.g. BS20 7, but this lack of
            // information would impair location searching routines.
            // For International properties please see the list of codes to be inserted in this field in Appendix 1
            //			row[4] = ContactsUtil.getPostcodeFormatted(postcode);

            // 6 Short Description Text 255 Yes Use correct case, good English, full words (no abbreviations). HTML tags CANNOT be used in this
            // field. All characters are included in this character count, including spaces.
            // 7 Long Description Text 8000 Yes Use correct case, good English, full words (no abbreviations). The following HTML tags are the
            // ONLY ones that can be used in this field - <li>, <b>, <br>, <u>, <i> and <p>.
            row[5] = item.getContentStripped(255).replace("\n", "");
            row[6] = item.getContentStripped().replace("\n", "");

            // 8 Property Type Text 12 Yes One of (studio, flat, apartment, terrace, semi, detached, retirement, bungalow). For shared
            // properties use �Shared Flat� or �Shared house�
            PropertyType propertyType = PropertyType.apartment;
            String value = item.getAttributeValue("property type", false);
            if (value != null) {

                value = value.toLowerCase();
                for (PropertyType p : PropertyType.values()) {

                    if (value.contains(p.toString())) {
                        propertyType = p;
                        break;
                    }
                }
            }
            row[7] = propertyType.toString();

            // 9 No of Bedrooms Integer N/A Yes Must be greater than zero, unless the Property Type field = studio, in which case zero is
            // allowed
            int bedrooms = 0;
            if (propertyType != PropertyType.studio) {

                value = item.getAttributeValue("bedrooms", false);
                try {

                    if (value != null)
                        bedrooms = Integer.parseInt(value.trim());

                } catch (NumberFormatException e) {

                }
            }
            row[8] = String.valueOf(bedrooms);

            // 10 Minimum Price / Rental Price integer N/A Yes Non-formatted number indicating the price of the property in \243�s (Pounds) e.g.
            // 50000.
            // If the record is for a group of properties (e.g. a development, or a block of flats) then this field should represent the minimum
            // property price.
            // If the property is a rental property, then please use this field in conjunction with Price Rental Period field.
            price = price.replaceAll("\\D", "");
            row[9] = price;

            // 11 Maximum Price Integer N/A No Non-formatted number indicating the maximum price if the record is for a group of properties (see
            // Minimum Price field).

            /*
                * 12 Price Text Text 50 No Price Text prefix/suffix to be displayed on site linked to Field 10 and Field 11 (where applicable)
                * above. Do NOT enter the numeric values in this field. Choose one of: � Asking price of � Auction * � Auction guide � By Tender * �
                * Guide Price of � In the region of � Offers invited * � Offers in excess of � Price range of � Prices from � Reduced to � Price on
                * application * � Fixed price of � Shared ownership No price displayed but field 10 still requires a price entry
                */

            // 13 Price Rental Period Text 1 No This field is mandatory for rental properties and should be used in conjunction with the Rental
            // Price field. It is used to indicate the rental period in the format �W� (Weekly), �M� (Monthly).
            if (rental)
                row[12] = "M";

            // 14 Rental Property Available Date Text 12 No The date that a rental property is available for occupancy in the format YYYYMMDD.
            if (rental)
                row[13] = date;

            // 15 Rental Let Term Text 1 No Either �S� for short let, or �L� for long let.
            String letTerm = null;
            if (rental) {
                value = item.getAttributeValue("let term", false);
                if (value != null) {
                    value = value.trim().toLowerCase();
                    if (value.contains("short"))
                        letTerm = "S";
                    else if (value.contains("long"))
                        letTerm = "L";
                }
            }
            row[14] = letTerm;

            // 16 Rental property pets allowed ? Text 1 No Y/N
            String pets = null;
            if (rental) {
                value = item.getAttributeValue("pets", false);
                if (value != null) {
                    value = value.trim().toLowerCase();
                    if (value.contains("y"))
                        letTerm = "Y";
                    else if (value.contains("n"))
                        letTerm = "N";
                }
            }
            row[15] = pets;

            // 17 Rent includes utility bills ? Text 1 No Y/N
            row[16] = null;

            // 18 Tenure Type Text 10 Yes One of (rental, freehold, leasehold)
            if (rental)
                row[17] = "rental";
            else
                row[17] = "freehold";

            // 19 Furnished Text 1 No Y/N
            String furnished = null;
            if (rental) {
                value = item.getAttributeValue("furnished", false);
                if (value != null) {
                    value = value.trim().toLowerCase();
                    if (value.contains("y"))
                        furnished = "Y";
                    else
                        furnished = "N";
                }
            }
            row[18] = furnished;

            // 20 Picture Name 1 Text 50 Yes Exact name of picture file including the extension. The picture file name itself must contain only
            // alphanumeric characters and underscores - No spaces.
            // NB picture file names are case sensitive.
            // e.g. valid name = �abc_123.jpg� invalid name = �abc 123.jpg�.
            // 21 - 25 pictures
            List<Img> images = item.getApprovedImages();
            int m = 0;
            for (m = 20; m < 25; m++) {
                if (images.size() > m) {

                    Img image = images.get(m);
                    row[m] = image.getFilename();

                    // only upload if the image is newer than 2 days - PF say 1 day but we'll use 2 in case of a cron job failure.
                    if (image.getDate().isAfter(new Date().removeDays(2)))
                        imageFilenames.add(image.getFilename());

                }
            }

            // 26 Floor Plan Name 1 Text 50 No
            // 27 floor plan 2
            // row[n] = null;
            // row[n] = null;

            // 28 Office Phone Text 20 Yes Contact point appropriate for the property
            String telephone = item.getAttributeValue("telephone", false);
            row[27] = telephone;

            // 29 Office email Text 255 Yes Contact point appropriate for the property
            String email = item.getAttributeValue("email", false);
            row[27] = email;

            // 30 Office Fax Text 20 Yes Contact point appropriate for the property
            // row[n] = null;

            // 31 Branch integer N/A Yes Estate Agent Branch ID as supplied by Propertyfinder. This can be obtained via the Propertyfinder
            // helpdesk number.
            row[30] = branchId;

            // 32 Age of Property integer N/A no Year of build (approx) in the format yyyy. E.g. 1985, not 85.
            // For New Homes, enter the word New instead of the year. This is the only exception.
            // row[n] = null;

            // 33 Location Text 20 Yes One of (urban, rural, town, riverside, country, countryside)
            String location = "urban";
            value = item.getAttributeValue("location", false);
            if (value != null)
                for (String string : locations) {
                    if (value.contains(string)) {
                        location = string;
                        break;
                    }
                }
            row[32] = location;

            // 34 Parking Garage Text 1 No Y/N
            // row[n] = null;

            // 35 Parking Off-Street Text 1 No Y/N
            // row[n] = null;

            // 36 Central Heating Text 1 No Y/N
            String centralHeating = null;
            value = item.getAttributeValue("central heating", false);
            if (value != null) {
                value = value.trim().toLowerCase();
                if (value.contains("y"))
                    centralHeating = "Y";
                else
                    centralHeating = "N";
            }
            row[35] = centralHeating;

            // 37 Double Glazing Text 1 No Y/N
            String glazing = null;
            value = item.getAttributeValue("glazing", false);
            if (value != null) {
                value = value.trim().toLowerCase();
                if (value.contains("y"))
                    glazing = "Y";
                else
                    glazing = "N";
            }
            row[36] = glazing;

            // 38 Reception Rooms Integer N/A No Number of reception rooms
            String receptions = null;
            value = item.getAttributeValue("Reception rooms", false);
            if (value != null)
                try {
                    Integer i = Integer.parseInt(value.trim());
                    receptions = i.toString();
                } catch (NumberFormatException e) {

                }
            row[37] = receptions;

            // 39 Bathrooms Integer N/A No Number of bathrooms
            String bathrooms = null;
            value = item.getAttributeValue("bathrooms", false);
            if (value != null)
                try {
                    Integer i = Integer.parseInt(value.trim());
                    bathrooms = i.toString();
                } catch (NumberFormatException e) {

                }
            row[38] = bathrooms;

            // 40 Conservatory Text 1 No Y/N
            String conservatory = null;
            value = item.getAttributeValue("conservatory", false);
            if (value != null) {
                value = value.trim().toLowerCase();
                if (value.contains("y"))
                    conservatory = "Y";
                else
                    conservatory = "N";
            }
            row[39] = conservatory;

            // 41 Swimming Pool Text 1 No Y/N

            // 42 Fireplace Text 1 No Y/N

            // 43 Waterfront Text 1 No Y/N

            // 44 Paddock Text 1 No Y/N

            // 45 Handicap Features Text 1 No Y/N

            // 46 Balcony Text 1 No Y/N

            // 47 Garden Text 1 No Y/N
            String garden = null;
            value = item.getAttributeValue("garden", false);
            if (value != null) {
                value = value.trim().toLowerCase();
                if (value.contains("y")) {
                    garden = "Y";
                } else {
                    garden = "N";
                }
            }
            row[46] = garden;

            // 48 Website URL Text 255 No Full website URL, which ideally links directly to the property details on the Estate Agents own
            // website.
            row[4] = item.getUrl();

            // 49 Status Text 13 Yes One of (available, not available, under offer, sold stc, hidden)
            row[48] = "available";

            // 50 Virtual Tour Text 3 No Three character alpha-numeric (letters must be capitals) code denoting the Virtual Tour provider.
            // Contact the Uploads Team for the latest list of supported Virtual Tour providers.

            // 51 Virtual Tour URL Text 255 No URL of the Virtual Tour for this property if applicable. NB Ehouse Virtual Tours (EHO) do not
            // require a Virtual Tour URL.

            records++;
        }

        StringBuilder sb = new StringBuilder();
        sb.append("#VERSION 3");
        sb.append("\n#SOURCE 7Soft - eCreator www.7soft.co.uk");
        sb.append("\n#DATE ");
        sb.append(date);
        sb.append("\n#RECORDS ");
        sb.append(records);
        sb.append("\n");
        sb.append(csvman.saveToString(rows));

        FTPClient ftp = new FTPClient();
        ftp.setRemoteHost(getFtpHostname());

        ftp.connect();

        ftp.user(getFtpUsername());
        ftp.password(getFtpPassword());

        ftp.setType(FTPTransferType.ASCII);

        ftp.put(sb.toString().getBytes(), "property_finder_" + branchId + ".txt");

        for (String filename : imageFilenames) {
            ftp.put(ResourcesUtils.getRealImagesPath() + File.separator + filename, filename);
        }

        return new FeedResult(csvman);
    }

}
