package org.sevensoft.ecreator.model.feeds.bespoke.prices.pricegrabber;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.prices.PriceGrabberFeedHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.ExportFeed;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.annotations.Ftp;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.ecom.delivery.DeliveryOption;
import org.sevensoft.ecreator.model.ecom.shopping.ShoppingSettings;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

import com.enterprisedt.net.ftp.FTPException;
import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.CsvSaver;

/**
 * User: Tanya
 * Date: 13.10.2011
 */

@Table("feeds_pricegrabber")
@HandlerClass(PriceGrabberFeedHandler.class)
@Label("Price Grabber")
@Ftp
public class PriceGrabber extends ExportFeed {

    private static final String COLON_SEPARATOR = ":";
    private static final String COMMA_SEPARATOR = ",";

	private Attribute brandAttribute;
	private Attribute				mpcAttribute;
	private Attribute				isbnAttribute;
    private Attribute				eanAttribute;
    private Attribute				shipWeightAttribute;
    private String              shipRate;
	private String				productType;
	private ItemType itemType;
	private transient List<String> fields;

	protected PriceGrabber(RequestContext context) {
		super(context);
	}

	public PriceGrabber(RequestContext context, boolean create) {
		super(context, create);
		this.name = "Price Grabber";
		save();
	}

	public final Attribute getBrandAttribute() {
		return (Attribute) (brandAttribute == null ? null : brandAttribute.pop());
	}

	public final Attribute getIsbnAttribute() {
		return (Attribute) (isbnAttribute == null ? null : isbnAttribute.pop());
	}

    public final Attribute getEanAttribute() {
		return (Attribute) (eanAttribute == null ? null : eanAttribute.pop());
	}

    public Attribute getShipWeightAttribute() {
        return (Attribute) (shipWeightAttribute == null ? null : shipWeightAttribute.pop());
    }

    public final ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public final Attribute getMpcAttribute() {
		return (Attribute) (mpcAttribute == null ? null : mpcAttribute.pop());
	}

	public final String getProductType() {
		return productType == null ? "electronics" : productType;
	}

    public String getShipRate() {
        return shipRate;
    }

    public boolean hasItemType() {
		return itemType != null;
	}

	private void header(CsvSaver saver) {

		fields = new ArrayList();

		fields.add("brand");               //0
		fields.add("color");               //1
		fields.add("condition");           //2
		fields.add("description");         //3
		fields.add("expiration_date");     //4
		fields.add("id");                  //5
		fields.add("image_link");          //6
        fields.add("additional_image_link");  //7
		fields.add("gtin");                //8
		fields.add("link");                //9
        fields.add("capacity");             //10		fields.add("memory");
		fields.add("mpn");                  //11
//        fields.add("sale_price");
		fields.add("price");                  //12
		fields.add("product_type");               //13
        fields.add("price_grabber_category");    //14
        fields.add("availability");               //15
		fields.add("shipping");      //todo    16
        fields.add("shipping_weight");           //17
		fields.add("size");                      //18
		fields.add("title");                     //19
        fields.add("online_only");               //20

        saver.next(fields.toArray(new String[0]));
	}

	@Override
	public void removeAttribute(Attribute attribute) {
	}

	public final void setBrandAttribute(Attribute brandAttribute) {
		this.brandAttribute = brandAttribute;
	}

	public final void setIsbnAttribute(Attribute isbnAttribute) {
		this.isbnAttribute = isbnAttribute;
	}

    public void setEanAttribute(Attribute eanAttribute) {
        this.eanAttribute = eanAttribute;
    }

    public void setShipWeightAttribute(Attribute shipWeightAttribute) {
        this.shipWeightAttribute = shipWeightAttribute;
    }

    public final void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	public final void setMpcAttribute(Attribute mpnAttribute) {
		this.mpcAttribute = mpnAttribute;
	}

	public final void setProductType(String productType) {
		this.productType = productType;
	}

    public void setShipRate(String shipRate) {
        this.shipRate = shipRate;
    }

	@Override
	public FeedResult run(File file) throws IOException, FTPException {

		file = File.createTempFile("google_base", null);
		file.deleteOnExit();

		context.disableCache();

		CsvManager csvman = new CsvManager();
		csvman.setSeparator("\t");
        csvman.setEncoding("UTF-8");

		CsvSaver saver = csvman.makeSaver(file);
		saver.begin();

		header(saver);

		ItemSearcher searcher = new ItemSearcher(context);
        searcher.setItemType(getItemType());
		searcher.setStatus("Live");
		searcher.setVisibleOnly(true);
        searcher.setLimit(getPreviewCount());

        if (ItemModule.IgnoreItemsInFeed.enabled(context, getItemType())) {
            searcher.setExcludeFromGoogleFeed(true);
        }

        boolean stockModule = ItemModule.Stock.enabled(context,getItemType()) && Module.Availabilitity.enabled(context);

        for (Item item : searcher) {

			// check price
			Money price = item.getSellPrice();
			if (price == null || price.isZero()) {
				continue;
			}

			String[] row = new String[fields.size()];


            //0 brand
            row[0] = item.getAttributeValue(getBrandAttribute());

            //1 color
            row[1] = null;

            //2 condition
            row[2] = "new";

            //3 description

            row[3] = item.getContentStripped(10000);

            //4 expiration_date
            row[4] = null;

            // 5 id
            row[5] = item.getIdString();

            //11 image_link
            List<Img> imgs = item.getApprovedImages();
            if (imgs == null || imgs.isEmpty()) {
                row[6] = null;
            } else {
                row[6] = imgs.get(0).getUrl();
                imgs.remove(0);
            }

            //additional_image_link
            if (imgs == null || imgs.isEmpty()) {
                row[7] = null;
            } else {
                StringBuilder sb = new StringBuilder();
                if (!imgs.isEmpty()) {
                    for (Img img : imgs) {
                        sb.append(img.getUrl());
                        if (imgs.get(imgs.size() - 1) != img) {
                            sb.append(COMMA_SEPARATOR);
                        }
                    }
                    row[7] = sb.toString();
                }
            }

            //8 isbn
            row[8] = item.getAttributeValue(getEanAttribute()) != null ? item.getAttributeValue(getEanAttribute()) : item.getAttributeValue(getIsbnAttribute());

            //9 link
            row[9] = item.getUrl();

            //10 memory
            row[10] = null;

            //11 model_number
            row[11] = item.getAttributeValue(getMpcAttribute());

              //12 price_type. we use sell price like 'price' column if the is no 'sell_price' column
            row[12] = item.getSellPriceInc().toEditString();
            // + " " + Currency.getDefault(context).getName();

//            //12 price
//            StringBuilder priceCol = new StringBuilder();
//            priceCol.append(!new Money(0).equals(item.getRrpInc(null)) ? item.getRrpInc(null).toEditString() : item.getSellPriceInc().toEditString());
//            priceCol.append(" ").append(Currency.getDefault(context).getName());
//            row[12] = priceCol.toString();

            //13 product_type
            String product_type;
            if (item.hasCategory()) {
                if (!item.getCategoryName().equals("CSV FAULT")) {
                    product_type = getProductType() + " - " + item.getCategoryName();
                } else {
                    product_type = getProductType();
                }
            } else {
                product_type = getProductType();
            }
            row[13] = product_type;
            row[14] = getProductType();

            if (stockModule) {
                if (item.getOurStock() > 0) {
                    row[15] = "in stock";
                } else if (item.isBackorders()) {
                    row[15] = "available for order";
                } else {
                    row[15] = "out of stock";
                }
            } else {
                row[15] = "in stock";
            }

            //15 availability  //todo     * 'in stock' * 'available for order' * 'out of stock' * 'preorder'


            // 16 shipping and 17 shipping_weight : only one required
            String weight = item.getAttributeValue(getShipWeightAttribute());
            if (weight == null) {
                //16 shipping
                if (getShipRate() == null || getShipRate().trim().length() == 0) {
                    StringBuilder sb = new StringBuilder();
                    List<DeliveryOption> deliveryOptions = DeliveryOption.get(context);
                    if (!deliveryOptions.isEmpty()) {
                        for (int i = 0; i < deliveryOptions.size(); i++) {
                            DeliveryOption option = deliveryOptions.get(i);
                            sb.append(!option.getCountries().isEmpty() ? option.getCountries().first().getIsoAlpha2() : Country.UK.getIsoAlpha2());
                            sb.append(COLON_SEPARATOR);
                            //State. not for UK
                            sb.append(COLON_SEPARATOR);
                            //optional
                            sb.append(COLON_SEPARATOR);
                            sb.append(option.getFlatCharge());
                            if (i < deliveryOptions.size() - 1) {
                                sb.append(COMMA_SEPARATOR);
                            }
                        }
                    }

                    row[16] = sb.toString();
                } else {
                    row[16] = getShipRate();
                }
                row[17] = null;
            } else {

                row[16] = null;
                row[17] = weight + " g";
            }

            //18 size
            row[18] = null;

            //19 title
            row[19] = item.getName().replaceAll("\t", "");

            //online_only
            row[20] = ShoppingSettings.DispatchType.Collection.equals(ShoppingSettings.getInstance(context).getDispatchType()) ? "n" : "y";

            CollectionsUtil.replaceNulls(row, "");
            HtmlHelper.replaseSymbolsWithUTF(row);
			saver.next(row);

		}

		saver.end();

		super.exportFtp(file);
		file.delete();

		return new FeedResult(csvman);
	}

}

