package org.sevensoft.ecreator.model.feeds.xml.parser.sykescottages;

import org.sevensoft.ecreator.model.feeds.sykescottages.Cottage;
import org.sevensoft.ecreator.model.feeds.sykescottages.Photo;
import org.sevensoft.ecreator.model.feeds.bespoke.products.sykescottages.SykesImportFeed;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;
import java.text.ParseException;


/**
 * User: Tanya
 * Date: 16.08.2011
 */
public class SykesParser {


    public static Item createItem(RequestContext context, SykesImportFeed sykesImportFeed, Cottage cottage) {
        Item item = Item.getByRef(context, sykesImportFeed.getItemType(), cottage.getNsykesid());
        if (item == null) {
            item = new Item(context, sykesImportFeed.getItemType(), cottage.getPropname(), "LIVE", null);
            item.setReference(cottage.getNsykesid());
            item.save();
        }

        Map<Attribute, String> avs = new HashMap();
        avs.put(sykesImportFeed.getAreaAttribute(), cottage.getArea());
        avs.put(sykesImportFeed.getSubareaAttribute(), cottage.getSubarea());
        avs.put(sykesImportFeed.getLatitudeAttribute(), cottage.getLatitude());
        avs.put(sykesImportFeed.getLongitudeAttribute(), cottage.getLongitude());
        avs.put(sykesImportFeed.getPriceinfoAttribute(), cottage.getPriceinfo());
        avs.put(sykesImportFeed.getMinpriceAttribute(), cottage.getMinprice());
        avs.put(sykesImportFeed.getMaxpriceAttribute(), cottage.getMaxprice());
        avs.put(sykesImportFeed.getBedroomsAttribute(), cottage.getBedrooms());
        avs.put(sykesImportFeed.getKettleAttribute(), cottage.isKettle());
        avs.put(sykesImportFeed.getParkingspaceAttribute(), cottage.isParkingspace());
        avs.put(sykesImportFeed.getSmokingAttribute(), cottage.isSmoking());
        avs.put(sykesImportFeed.getSinglebedsAttribute(), cottage.getSinglebeds());
        avs.put(sykesImportFeed.getDoublebedsAttribute(), cottage.getDoublebeds());
        avs.put(sykesImportFeed.getTwinbedsAttribute(), cottage.getTwinbeds());
        avs.put(sykesImportFeed.getBedroomsAttribute(), cottage.getBunkbeds());
        avs.put(sykesImportFeed.getFamilybedsAttribute(), cottage.getFamilybeds());
        avs.put(sykesImportFeed.getMaxsleepsAttribute(), cottage.getMaxsleeps());
        avs.put(sykesImportFeed.getProppostcodeAttribute(), cottage.getProppostcode());
        avs.put(sykesImportFeed.getPropfeedbackAttribute(), cottage.getPropfeedback());
        avs.put(sykesImportFeed.getTenniscourtAttribute(), cottage.isTenniscourt());
        avs.put(sykesImportFeed.getPetsAttribute(), cottage.isPets());
        avs.put(sykesImportFeed.getChangeoverdayAttribute(), cottage.getChangeoverday());
        avs.put(sykesImportFeed.getJacuzziAttribute(), cottage.isJacuzzi());
        avs.put(sykesImportFeed.getTelephoneAttribute(), cottage.isTelephone());
        avs.put(sykesImportFeed.getSaunaAttribute(), cottage.isSauna());
        avs.put(sykesImportFeed.getLinenprovidedAttribute(), cottage.isLinenprovided());
        avs.put(sykesImportFeed.getTowelsprovidedAttribute(), cottage.isTowelsprovided());
        avs.put(sykesImportFeed.getDishwasherAttribute(), cottage.isDishwasher());
        avs.put(sykesImportFeed.getCookerAttribute(), cottage.isCooker());
        avs.put(sykesImportFeed.getFridgeAttribute(), cottage.isFridge());
        avs.put(sykesImportFeed.getWashingmachineAttribute(), cottage.isWashingmachine());
        avs.put(sykesImportFeed.getTvAttribute(), cottage.isTv());
        avs.put(sykesImportFeed.getHighchairAttribute(), cottage.isHighchair());
        avs.put(sykesImportFeed.getBathroomsAttribute(), cottage.getBathrooms());
        avs.put(sykesImportFeed.getCotsAttribute(), cottage.isCots());
        avs.put(sykesImportFeed.getHenstagAttribute(), cottage.isHenstag());
        avs.put(sykesImportFeed.getChildrenAttribute(), cottage.isChildren());
        avs.put(sykesImportFeed.getGamesroomAttribute(), cottage.isGamesroom());
        avs.put(sykesImportFeed.getPingpongAttribute(), cottage.isPingpong());
        avs.put(sykesImportFeed.getSnookerAttribute(), cottage.isSnooker());
        avs.put(sykesImportFeed.getWifiAttribute(), cottage.isWifi());
        avs.put(sykesImportFeed.getWebpriceAttribute(), cottage.getWebprice());
        avs.put(sykesImportFeed.getFurtherfacilitiesinternalAttribute(), cottage.getFurtherfacilitiesinternal());
        avs.put(sykesImportFeed.getRegiondescAttribute(), cottage.getRegiondesc());
        avs.put(sykesImportFeed.getTowndescAttribute(), cottage.getTowndesc());
        avs.put(sykesImportFeed.getHomesummaryAttribute(), cottage.getHomesummary());
        avs.put(sykesImportFeed.getHomedescAttribute(), cottage.getHomedesc());
        avs.put(sykesImportFeed.getFurtherfacilitiesexternalAttribute(), cottage.getFurtherfacilitiesexternal());
        avs.put(sykesImportFeed.getFurtherdetailsAttribute(), cottage.getFurtherdetails());
        avs.put(sykesImportFeed.getSpecialconditionsAttribute(), cottage.getSpecialconditions());
        avs.put(sykesImportFeed.getAmenityNotesAttribute(), cottage.getAmenityNotes());
        avs.put(sykesImportFeed.getCountryAttribute(), cottage.getCountry());
        avs.put(sykesImportFeed.getVillagenameAttribute(), cottage.getVillagename());
        avs.put(sykesImportFeed.getSykesratingAttribute(), cottage.getSykesrating());
        avs.put(sykesImportFeed.getVillageidAttribute(), cottage.getVillageid());
        avs.put(sykesImportFeed.getWeblinkAttribute(), cottage.getWeblink());

//				avs.put(getProvinceAttribute(), province);

        item.setAttributeValues(avs);

        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    public static void createPhoto(RequestContext context, SykesImportFeed sykesImportFeed, Photo photo) {
        Item item = Item.getByRef(context, sykesImportFeed.getItemType(), photo.getNsykesid());
        if (item == null)
            return;
        //prev photos are removed in      createItem() , item.setAttributeValues(avs);
        if (sykesImportFeed.getPhotoAttribute() != null)
            item.addAttributeValue(sykesImportFeed.getPhotoAttribute(), photo.getUrl());
    }
}
