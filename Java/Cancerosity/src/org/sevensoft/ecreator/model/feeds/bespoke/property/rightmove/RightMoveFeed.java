package org.sevensoft.ecreator.model.feeds.bespoke.property.rightmove;

import com.enterprisedt.net.ftp.FTPException;
import com.ricebridge.csvman.CsvLoader;
import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.CsvSpec;
import org.jdom.JDOMException;
import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.property.RightMoveFeedHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeOption;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.listings.Listing;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

import javax.servlet.ServletOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author sks 09-Jan-2006 12:36:07 #DEFINITION#
 *         <p/>
 *         0 AGENT_REF
 *         1 ADDRESS_1
 *         2 POSTCODE1
 *         3 POSTCODE2
 *         4 SUMMARY
 *         5 DESCRIPTION
 *         6 BRANCH_ID
 *         7 STATUS_ID
 *         8 BEDROOMS
 *         9 PRICE
 *         10 PROP_SUB_ID
 *         11 CREATE_DATE
 *         12 UPDATE_DATE
 *         13 DISPLAY_ADDRESS
 *         14 PUBLISHED_FLAG
 *         15 LET_RENT_FREQUENCY
 *         16 TRANS_TYPE_ID
 *         17 MEDIA_IMAGE_00
 *         MEDIA_IMAGE_01
 *         MEDIA_IMAGE_02
 *         MEDIA_IMAGE_03
 *         MEDIA_IMAGE_04
 *         MEDIA_IMAGE_05
 *         MEDIA_IMAGE_06
 *         MEDIA_IMAGE_07
 */
@Table("feeds_rightmove")
@HandlerClass(RightMoveFeedHandler.class)
@Label("Right move")
public class RightMoveFeed extends ImportFeed {

    private Attribute postcodeAttribute;
    private Attribute bedroomsAttribute;
    private Attribute priceAttribute;
    private Attribute furnishedAttribute;
    private Attribute addressAttribute;
    private ItemType itemType;

    /**
     * the attribute for 'area', district of a town
     */
    private Attribute areaAttribute;

    /**
     * Sub directory of feed-data for this upload
     */
    private String directory;

    /**
     * The item who will be used to set on the agent assocation type
     */
    private Item agent;

    private Attribute websiteAttribute;

    private String websitePrefix;

    private String telephone;
    private String email;

    private RightMoveFeed(RequestContext context) {
        super(context);
    }

    public RightMoveFeed(RequestContext context, boolean b) {
        super(context, true);
    }

    public final Attribute getAddressAttribute() {
        return (Attribute) (addressAttribute == null ? null : addressAttribute.pop());
    }

    public final Item getAgent() {
        return (Item) (agent == null ? null : agent.pop());
    }

    public final Attribute getAreaAttribute() {
        return (Attribute) (areaAttribute == null ? null : areaAttribute.pop());
    }

    public final Attribute getBedroomsAttribute() {
        return (Attribute) (bedroomsAttribute == null ? null : bedroomsAttribute.pop());
    }

    public final String getDirectory() {
        return directory;
    }

    public final Attribute getFurnishedAttribute() {
        return (Attribute) (furnishedAttribute == null ? null : furnishedAttribute.pop());
    }

    public final ItemType getItemType() {
        return (ItemType) (itemType == null ? null : itemType.pop());
    }

    public final Attribute getPostcodeAttribute() {
        return (Attribute) (postcodeAttribute == null ? null : postcodeAttribute.pop());
    }

    public final Attribute getPriceAttribute() {
        return (Attribute) (priceAttribute == null ? null : priceAttribute.pop());
    }

    public final Attribute getWebsiteAttribute() {
        return (Attribute) (websiteAttribute == null ? null : websiteAttribute.pop());
    }

    public final String getWebsitePrefix() {
        return websitePrefix;
    }

    public boolean hasAgent() {
        return agent != null;
    }

    public boolean hasItemType() {
        return itemType != null;
    }

    @Override
    public void removeAttribute(Attribute attribute) {
    }

    public FeedResult run(File file) throws IOException, FTPException, FeedException, JDOMException {

        super.updateFeedCount();

        if (itemType == null) {
            throw new FeedException("null item type");
        }

        if (priceAttribute == null) {
            priceAttribute = getItemType().getAttribute("Price");
            if (priceAttribute == null) {
                priceAttribute = getItemType().addAttribute("Price", AttributeType.Numerical);
            }
            save();
        }

        if (websiteAttribute == null) {
            websiteAttribute = getItemType().getAttribute("Website");
            if (websiteAttribute == null) {
                websiteAttribute = getItemType().addAttribute("Website", AttributeType.Link);
            }
            save();
        }

        if (addressAttribute == null) {
            addressAttribute = getItemType().getAttribute("Address");
            if (addressAttribute == null) {
                addressAttribute = getItemType().addAttribute("Address", AttributeType.Postcode);
            }
            save();
        }

        if (postcodeAttribute == null) {
            postcodeAttribute = getItemType().getAttribute("Postcode");
            if (postcodeAttribute == null) {
                postcodeAttribute = getItemType().addAttribute("Postcode", AttributeType.Postcode);
            }
            save();
        }

        if (bedroomsAttribute == null) {
            bedroomsAttribute = getItemType().getAttribute("Bedrooms");
            if (bedroomsAttribute == null) {
                bedroomsAttribute = getItemType().addAttribute("Bedrooms", AttributeType.Numerical);
            }
            save();
        }

        if (furnishedAttribute == null) {
            furnishedAttribute = getItemType().getAttribute("Furnished");
            if (furnishedAttribute == null) {
                furnishedAttribute = getItemType().addAttribute("Furnished", AttributeType.Selection);
            }
            save();
        }

        if (areaAttribute == null) {
            areaAttribute = getItemType().getAttribute("Area");
            if (areaAttribute == null) {
                areaAttribute = getItemType().addAttribute("Area", AttributeType.Selection);
            }
            save();
        }

        if (areaAttribute == null) {
            areaAttribute = getItemType().getAttribute("Area");
            if (areaAttribute == null) {
                areaAttribute = getItemType().addAttribute("Area", AttributeType.Selection);
            }
            save();
        }

        Attribute statusAttribute = getItemType().getAttribute("Status");
        Attribute emailAttribute = getItemType().getAttribute("Email");
        Attribute telephoneAttribute = getItemType().getAttribute("Telephone");

//		String imagesDir = context.getRealPath(Config.ImagesPath);
        String imagesDir = ResourcesUtils.getRealImagesPath();
        logger.fine("[RightMoveFeed] imagesDir=" + imagesDir);

        String srcPath = Config.FeedDataPath;
        File srcDir;
        if (directory != null) {
            srcPath = srcPath + "/" + directory;
            srcDir = ResourcesUtils.getRealFeedData(srcPath);
        } else {
            srcDir = ResourcesUtils.getRealFeedDataDir();
        }
        logger.fine("[RightMoveFeed] srcDir=" + srcDir);

        if (!srcDir.exists()) {
            throw new FeedException("Specified source directory does not exist: " + srcDir);
        }

        // mv all images to image folder
        File[] files = srcDir.listFiles();

        File blm = null;
        logger.fine("[RightMoveFeed] source files count=" + files.length);
        for (File f : files) {

            String filename = f.getName().toLowerCase();

            if (filename.endsWith(".jpg")) {

                File dest = new File(imagesDir + "/" + filename);
                logger.fine("[RightMoveFeed] moving " + f + " to " + file);
                SimpleFile.move(f, dest);

            } else if (filename.endsWith(".blm")) {

                blm = f;

            } else {

                f.delete();
            }
        }

        logger.fine("[RightMoveFeed] blm file=" + blm);
        if (blm == null) {
            throw new FeedException("No blm file found in feed data");
        }

        if (!blm.exists()) {
            throw new FeedException("Specified blm file not found: " + blm);
        }

        // we need to load text file
        String input = SimpleFile.readString(blm);

        // strip out header info
        Pattern pattern = Pattern.compile(".*#DATA#\n", Pattern.DOTALL);
        input = pattern.matcher(input).replaceFirst("");

        // create csv manager
        CsvManager csv = new CsvManager();
        csv.setEncoding("UTF-8");
        CsvSpec spec = csv.getCsvSpec();

        spec.setIgnoreEmptyLines(true);
        spec.setSeparator("^");
        spec.setEndOfLine("~");

        csv.setCsvSpec(spec);

        int created = 0, processed = 0;
        CsvLoader loader = csv.makeLoader(new StringReader(input));
        loader.begin();
        while (loader.hasNext()) {

            processed++;

            String[] row = loader.next();
            logger.fine("[RightMoveFeed] processing row=" + Arrays.toString(row));

            String agentRef = row[0];
            String name = row[1];
            String postcode1 = row[2];
            String postcode2 = row[3];
            String description = row[5];
            String branchId = row[6];
            String bedrooms = row[8];
            String price = row[9];
            //	String propSubId = row[10];
            //			String createDate = row[11];
            //			String updateDate = row[12];
            String address = row[13];
            String LET_FURN_ID = row[15];
            logger.fine("[RightMoveFeed] LET_FURN_ID=" + LET_FURN_ID);

            String ref = branchId + ":" + agentRef;

            List<String> imageFilenames = new ArrayList();
            for (int n = 18; n <= 24; n++) {
                if (row[n] != null && row[n].length() > 4) {
                    imageFilenames.add(row[n].toLowerCase());
                }
            }

            // lookup item from reference
            Item item = Item.getByRef(context, getItemType(), ref);
            if (item == null) {

                item = super.createItem(getItemType(), name);
                logger.fine("[RightMoveFeed] created item=" + item);

                item.setReference(ref);

                if (statusAttribute != null) {
                    item.setAttributeValue(statusAttribute, "Residential for rent");
                }

                // strip name from address
                address = address.replace(name, "").trim();

                // remove weird characters from postcodes
                if (postcode1 != null) {
                    postcode1 = postcode1.toUpperCase().replaceAll("[^A-Z0-9]", "");
                }

                if (postcode2 != null) {
                    postcode2 = postcode2.toUpperCase().replaceAll("[^A-Z0-9]", "");
                }

                // remove postcodes from address
                address = address.replaceAll(postcode1 + " " + postcode2 + "$", "");
                address = address.replaceAll(postcode1 + postcode2 + "$", "");
                address = address.replaceAll(postcode2 + "$", "");
                address = address.replaceAll(postcode1 + "$", "");

                /*
                     * Replace commas with newlines.
                     */
                address = address.replaceAll("\\s*,\\s*", "\n");

                logger.fine("[RightMoveFeed] address=" + address);

                if (websitePrefix != null) {
                    item.setAttributeValue(getWebsiteAttribute(), websitePrefix + agentRef);
                }

                item.setAttributeValue(getAddressAttribute(), address);
                item.setAttributeValue(getPostcodeAttribute(), postcode1 + postcode2);

                item.setAttributeValue(getBedroomsAttribute(), bedrooms);

                String area = null;

                // search address for area
                String lowerAddress = address.toLowerCase();
                for (AttributeOption option : getAreaAttribute().getOptions()) {

                    if (lowerAddress.contains(option.getValue().toLowerCase())) {
                        area = option.getValue();
                        break;
                    }
                }

                if (area != null) {
                    item.setAttributeValue(getAreaAttribute(), area);
                }

                // search description for furnished string
                String furnished = null;
                if (description != null) {
                    String string = description.toLowerCase();
                    if (string.contains("unfurnished")) {
                        furnished = "Unfurnished";
                    } else if (string.contains("part furnished")) {
                        furnished = "Part Furnished";
                    } else if (string.contains("furnished")) {
                        furnished = "Furnished";
                    }
                }

                if (furnished == null) {
                    furnished = "Unfurnished";
                }
                item.setAttributeValue(getFurnishedAttribute(), furnished);

                if (agent != null) {
                    item.setAccount(agent);
                }

                // get listing package on artful
                if (config.getUrl().contains("artfullodger.co.uk")) {
                    ListingPackage lp = EntityObject.getInstance(context, ListingPackage.class, 5);
                    Listing listing = item.getListing();
                    listing.setListingPackage(lp);
                    listing.save();
                }

                created++;
            }

            item.setContent(description);
            item.setFeedCount(getFeedCount());
            item.save();

            item.setAttributeValue(getPriceAttribute(), price);
            item.setAttributeValue(telephoneAttribute, telephone);
            item.setAttributeValue(emailAttribute, email);

            // if we have no images on this item then try to tag images
            if (!item.hasAnyImage()) {

                for (String imageFilename : imageFilenames) {
                    try {
                        logger.fine("[RightMoveFeed] adding image filename=" + imageFilename);
                        item.addImage(imageFilename, true, true, true);
                    } catch (ImageLimitException e) {
                        e.printStackTrace();
                    } catch (FileNotFoundException e) {
                    }
                }
            }
        }
        loader.end();

        // delete blm file
        blm.delete();

        // remove any expired items
        super.deleteExpired();

        logger.fine("[RightMoveFeed] created=" + created + ", processed=" + processed);

        return new FeedResult(created, processed, 0);
    }

    public final void setAddressAttribute(Attribute addressAttribute) {
        this.addressAttribute = addressAttribute;
    }

    public final void setAgent(Item agent) {
        this.agent = agent;
    }

    public final void setAreaAttribute(Attribute areaAttribute) {
        this.areaAttribute = areaAttribute;
    }

    public final void setBedroomsAttribute(Attribute bedroomsAttribute) {
        this.bedroomsAttribute = bedroomsAttribute;
    }

    public final void setDirectory(String d) {

        if (ObjectUtil.equal(this.directory, d)) {
            return;
        }

        this.directory = d;
        if (directory.startsWith("/")) {
            directory = directory.substring(1);
        }
        if (directory.endsWith("/")) {
            directory = directory.substring(0, directory.length() - 1);
        }
    }

    public final void setFurnishedAttribute(Attribute furnishedAttribute) {
        this.furnishedAttribute = furnishedAttribute;
    }

    public final void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public final void setPostcodeAttribute(Attribute postcodeAttribute) {
        this.postcodeAttribute = postcodeAttribute;
    }

    public final void setPriceAttribute(Attribute priceAttribute) {
        this.priceAttribute = priceAttribute;
    }

    public final void setWebsiteAttribute(Attribute websiteAttribute) {
        this.websiteAttribute = websiteAttribute;
    }

    public final void setWebsitePrefix(String websitePrefix) {
        this.websitePrefix = websitePrefix;
    }

    public void write(ServletOutputStream out) throws IOException, FeedException {
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
