package org.sevensoft.ecreator.model.feeds;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import org.jdom.JDOMException;
import org.sevensoft.commons.httpc.HttpMethod;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.comparators.ClassSimpleNameComparator;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.bespoke.events.VueFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.news.PdoNewsFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.prices.base.GoogleBase;
import org.sevensoft.ecreator.model.feeds.bespoke.prices.kelkoo.KelkooFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.prices.pricerunner.PriceRunnerFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.prices.shopzilla.ShopzillaFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.prices.edir.EdirectoryFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.prices.pricegrabber.PriceGrabber;
import org.sevensoft.ecreator.model.feeds.bespoke.products.cars.SaveMoneyCarsFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.products.midwich.MidwichFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.products.vip.VipFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.products.openrange.OpenRangeImagesFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.products.openrange.OpenRangeFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.products.microp.MicroPFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.products.sykescottages.SykesImportFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.property.kyero.KyeroFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.property.propertyfinder.PropertyFinderFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.property.rightmove.RightMoveFeed;
import org.sevensoft.ecreator.model.feeds.csv.CsvImportFeed;
import org.sevensoft.ecreator.model.feeds.csv.CsvExportFeed;
import org.sevensoft.ecreator.model.feeds.ecreator.EcreatorExportFeed;
import org.sevensoft.ecreator.model.feeds.ecreator.EcreatorImportFeed;
import org.sevensoft.ecreator.model.misc.Interval;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.positionable.PositionComparator;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

import com.enterprisedt.net.ftp.FTPException;

/**
 * @author sks 27 Jan 2007 18:29:14
 *
 */
public abstract class Feed extends EntityObject implements Positionable, Comparable<Feed> {

	@SuppressWarnings("hiding")
	protected static Logger	logger	= Logger.getLogger("feeds");

	private static List<Feed> get(RequestContext context, Class clazz) {
		return SimpleQuery.execute(context, clazz);
	}

	public static List<Feed> getAll(RequestContext context) {

		List<Feed> feeds = new ArrayList();
		for (Class clazz : getClasses(context)) {
			feeds.addAll(get(context, clazz));
		}
		Collections.sort(feeds, new PositionComparator());
		return feeds;
	}

	public static List<Class> getClasses(RequestContext context) {

		List<Class> classes = new ArrayList();

		//	classes.add(CamdramFeed.class);

		classes.add(CsvImportFeed.class);
        classes.add(CsvExportFeed.class);
        classes.add(EcreatorExportFeed.class);
		classes.add(EcreatorImportFeed.class);
		classes.add(KelkooFeed.class);
		classes.add(GoogleBase.class);
        classes.add(EdirectoryFeed.class);

        classes.add(KyeroFeed.class);
		classes.add(MidwichFeed.class);
		classes.add(PriceRunnerFeed.class);
		classes.add(PropertyFinderFeed.class);
		classes.add(PdoNewsFeed.class);
		classes.add(RightMoveFeed.class);
		classes.add(ShopzillaFeed.class);
		classes.add(VipFeed.class);
		classes.add(VueFeed.class);
		classes.add(SaveMoneyCarsFeed.class);
        classes.add(MicroPFeed.class);   //todo TRO remove until finished
        classes.add(OpenRangeImagesFeed.class);  //todo TRO remove until finished
        classes.add(OpenRangeFeed.class);   //todo TRO remove until finished
        classes.add(SykesImportFeed.class);

        classes.add(PriceGrabber.class);

        Collections.sort(classes, ClassSimpleNameComparator.Instance);
		return classes;
	}

	/**
	 * Returns true if kelkoo is used by any feed
	 */
	public static boolean isKelkoo(RequestContext context) {
		return SimpleQuery.count(context, KelkooFeed.class) > 0;
	}

	/**
	 * Returns true if any feed is shopzilla
	 */
	public static boolean isShopzilla(RequestContext context) {
		return SimpleQuery.count(context, ShopzillaFeed.class) > 0;
	}

	/**
	 * Any items below this cost price will be ignored by the feed
	 */
	private Money				minCostPrice;

	protected final transient Config	config;

	/**
	 * the name for this feed
	 */
	protected String				name;

	/**
	 * Details for FTP retrieval of data
	 */
	protected String				ftpFilename;

	/**
	 * Details for FTP retrieval of data
	 */
	protected String				ftpHostname;

	/**
	 * Details for FTP retrieval of data
	 */
	protected String				ftpPassword;

	/**
	 * Details for FTP retrieval of data
	 */
	protected String				ftpPath;

	/**
	 * 
	 */
	protected int				ftpPort;

	/**
	 * Details for FTP retrieval of data
	 */
	protected String				ftpUsername;

	/**
	 * The method for HTTP requests.
	 */
	protected HttpMethod			httpMethod;

	/**
	 * The HTTP URL if we are using HTTP to get data.
	 * Usually we can hard wire this, but in some cases we might have multiple feeds from the same source and the url contains differing params, ie vue.
	 */
	protected String				httpUrl;

	private int					position;

	/**
	 * How often to run auto feeds
	 */
	protected Interval			interval;

	/**
	 * The time/date this feed was last ran
	 */
	protected DateTime			lastRuntime;

	/**
	 * A list of categories that data will be ignored from
	 */
	private List<String>			ignoredCategories;

	protected Feed(RequestContext context) {
		super(context);
		this.config = Config.getInstance(context);
	}

	protected Feed(RequestContext context, boolean create) {
		super(context);
		this.config = Config.getInstance(context);

		this.name = getClass().getAnnotation(Label.class).value();
		setNextPosition();
		save();
	}

	public final int compareTo(Feed o) {
		return 0;
	}

	public final int compareTo(ImportFeed o) {
		return getName().compareTo(o.getName());
	}

	public final String getFtpFilename() {
		return ftpFilename;
	}

	public final String getFtpHostname() {
		return ftpHostname;
	}

	public final String getFtpPassword() {
		return ftpPassword;
	}

	public final String getFtpPath() {
		return ftpPath;
	}

	public final String getFtpUsername() {
		return ftpUsername;
	}

	public final HttpMethod getHttpMethod() {
		return httpMethod == null ? HttpMethod.Post : httpMethod;
	}

	public final String getHttpUrl() {
		return httpUrl;
	}

	public final List<String> getIgnoredCategories() {
		if (ignoredCategories == null) {
			ignoredCategories = new ArrayList();
		}
		return ignoredCategories;
	}

	public final Interval getInterval() {
		return interval == null ? Interval.Manual : interval;
	}

	public final DateTime getLastRuntime() {
		return lastRuntime;
	}

	public final String getLastRuntimeString(String format) {
		return lastRuntime == null ? "Never" : lastRuntime.toString(format);
	}

	public final Money getMinCostPrice() {
		return minCostPrice;
	}

	public final String getName() {
		return name;
	}

	public final int getPosition() {
		return position;
	}

	protected final boolean hasFtpDetails() {
		return ftpHostname != null && ftpUsername != null && ftpFilename != null;
	}

	protected final boolean hasHttpUrl() {
		return httpUrl != null;
	}

	public abstract void removeAttribute(Attribute attribute);

	/**
	 * Runs the feed with the file parameter containing a data file
	 * If the feed can get its own data then this parameter is optional.
	 * 
	 */
	public abstract FeedResult run(File file) throws IOException, FTPException, FeedException, JDOMException;

	public final void setFtpFilename(String s) {
		this.ftpFilename = (s == null ? null : s.trim());
	}

	public final void setFtpHostname(String s) {
		this.ftpHostname = (s == null ? null : s.trim());
	}

	public final void setFtpPassword(String s) {
		this.ftpPassword = (s == null ? null : s.trim());
	}

	public final void setFtpPath(String ftpPath) {
		this.ftpPath = ftpPath;
	}

	public final void setFtpUsername(String s) {
		this.ftpUsername = (s == null ? null : s.trim());
	}

	public final void setHttpMethod(HttpMethod httpMethod) {
		this.httpMethod = httpMethod;
	}

	public final void setHttpUrl(String s) {
		this.httpUrl = (s == null ? null : s.trim());
	}

	public final void setIgnoredCategories(List<String> s) {

		this.ignoredCategories = new ArrayList();
		if (ignoredCategories != null) {
			for (String string : s) {
				ignoredCategories.add(string.toLowerCase());
			}
		}
	}

	public final void setInterval(Interval interval) {
		this.interval = interval;
	}

	public final void setLastRuntime() {
		this.lastRuntime = new DateTime();
		save("lastRuntime");
	}

	public final void setMinCostPrice(Money minSellPrice) {
		this.minCostPrice = minSellPrice;
	}

	public final void setName(String name) {
		this.name = name;
	}

	public final void setNextPosition() {

		List<Feed> feeds = getAll(context);
		if (feeds.isEmpty()) {
			position = 1;
		} else {
			position = feeds.get(feeds.size() - 1).getPosition() + 1;
		}
	}

	public final void setPosition(int position) {
		this.position = position;
	}

	public int getFtpPort() {
		return ftpPort == 0 ? 21 : ftpPort;
	}

	public void setFtpPort(int ftpPort) {
		this.ftpPort = ftpPort;
	}

}
