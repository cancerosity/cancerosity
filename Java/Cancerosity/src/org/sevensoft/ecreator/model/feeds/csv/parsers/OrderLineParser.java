package org.sevensoft.ecreator.model.feeds.csv.parsers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.feeds.csv.CsvImportFeed;
import org.sevensoft.ecreator.model.feeds.csv.CsvImportFeedField;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 29 Nov 2006 09:45:18
 *
 */
public class OrderLineParser extends LineParser {

	/**
	 * A set containing all the order ids of the lines we have processed this run
	 */
	private Set<Integer>	orderIds;

	/**
	 * A set containing all the line ids of the lines we have processed in this run
	 */
	private Set<Integer>	lineIds;

	public OrderLineParser(RequestContext context, CsvImportFeed feed) {
		super(context, feed);
		this.lineIds = new HashSet();
		this.orderIds = new HashSet();
	}

	public Set<Integer> getLineIds() {
		return lineIds;
	}

	public Set<Integer> getOrderIds() {
		return orderIds;
	}

	@Override
	protected void process(String[] row) {

		logger.fine("[OrderLineImporter] processing row=" + Arrays.toString(row));

		// lookup order line by id
		OrderLine line = EntityObject.getInstance(context, OrderLine.class, getIdValue(row));
		logger.fine("[OrderLineImporter] line found=" + line);

		if (line == null)
			return;

		for (CsvImportFeedField field : feed.getFields()) {
			field.process(feed, line, row);
		}

		lineIds.add(line.getId());
		orderIds.add(line.getOrder().getId());
	}

	/**
	 * 
	 */
	public void teardown() {

		// remove all lines from all referenced orders that did not appear in the feed
		if (feed.isRemoveOmittedLines()) {

			logger.fine("[OrderLineImporter] removing omitted lines");

			for (int orderId : orderIds) {

				Order order = EntityObject.getInstance(context, Order.class, orderId);
				if (order != null) {

					for (OrderLine line : order.getLines()) {

						if (!lineIds.contains(line.getId())) {
							logger.fine("[OrderLineImporter] removing line=" + line.getId());
							order.removeLine(line);

							if (!order.hasLines()) {
								order.cancel();
								break;
							}
						}
					}
				}
			}
		}

		if (feed.hasOrderStatus()) {

			logger.fine("[OrderLineImporter] changing order statuses to=" + feed.getOrderStatus());

			for (int orderId : orderIds) {

				Order order = EntityObject.getInstance(context, Order.class, orderId);
				if (order != null) {

					logger.fine("[OrderLineImporter] changing status on order=" + orderId);
					order.setStatus(feed.getOrderStatus());
					order.save();
				}
			}
		}
	}

}
