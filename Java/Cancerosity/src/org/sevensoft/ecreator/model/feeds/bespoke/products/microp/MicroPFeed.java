package org.sevensoft.ecreator.model.feeds.bespoke.products.microp;

import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.Ftp;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.bespoke.products.openrange.OpenRangeImagesFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.products.openrange.OpenRangeFeed;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.stock.StockModule;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.products.MicroPFeedHandler;
import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.commons.simpleio.SimpleZip;
import org.sevensoft.commons.skint.Money;
import org.jdom.JDOMException;

import java.io.File;
import java.io.IOException;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Enumeration;
import java.util.ArrayList;
import java.util.zip.ZipFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.enterprisedt.net.ftp.FTPException;
import com.ricebridge.csvman.CsvManager;

/**
 * @author LebedevDN
 * @version: $id$
 */
@Table("feeds_micro_p")
@Label("Micro-P")
@HandlerClass(MicroPFeedHandler.class)
@Ftp
public class MicroPFeed extends ImportFeed {
    //Columns in microp.csv
    private static final int MANUFACTURER_NAME_INDEX = 0;           //Manufacturer Name
    private static final int STOCK_GROUP_CODE_INDEX = 1;            //Stock Group Code
    private static final int MP_PRODUCT_NUMBER_INDEX = 2;           //MP Product Number
    private static final int MANUFACTURER_PRODUCT_NUMBER_INDEX = 3; //Manufacturer Product Number
    private static final int PRODUCT_BARCODE_INDEX = 4;             //Product Barcode
    private static final int DESCRIPTION_INDEX = 5;                 //Product Description
    private static final int QTY_INDEX = 6;                         //Qty
    private static final int PRICE_INDEX = 7;                       //Price
    private static final int NEXT_TO_DUE_DATE_INDEX = 8;            //Next PO Due Date (To MP)
    private static final int CATEGORIES_INDEX = 9;                  //Category
//    private static final int OPEN_RANGE_URL_INDEX = 9;              //Open Range URL

    //Columns in ECO_PRODUCTS_FILE.csv
    private static final int MP_PARTCODE = 0;
    private static final int MANF_ID = 1;
    private static final int CAT_ID_PROD = 2;

    //Columns in ECO_CATEGORIES_FILE1.csv
    private static final int CAT_ID_CAT = 0;
    private static final int WEB_DESC = 1;
    private static final int CAT_ID_HIERACHY_LINK = 2;

    public static final String MANUF_PART_NUMBER = "Manuf. Part Number";

    private static final String MICROP_FILE = "microp.csv";
    private static final String ECO_PRODUCTS_FILE = "ECO_PRODUCTS_FILE.csv";
    private static final String ECO_CATEGORIES_FILE = "ECO_CATEGORIES_FILE.csv";

    protected Map<String, Category> categoriesCache = new HashMap();

    protected ItemType itemType;
    private boolean disableProducts;
    private Attribute manufPartNumberAttribute;

    protected MicroPFeed(RequestContext context) {
        super(context);
    }

    public MicroPFeed(RequestContext context, boolean create) {
        super(context, create);
        new OpenRangeImagesFeed(context, create);
        new OpenRangeFeed(context, create);
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public Attribute getManufPartNumberAttribute() {
        return (Attribute) (manufPartNumberAttribute == null ? null : manufPartNumberAttribute.pop());
    }

    public void setManufPartNumberAttribute(Attribute manufPartNumberAttribute) {
        this.manufPartNumberAttribute = manufPartNumberAttribute;
    }

    public void removeAttribute(Attribute attribute) {
    }

    public FeedResult run(File file) throws IOException, FTPException, FeedException, JDOMException {
        if (getItemType() == null) {
            return null;
        }
        logger.fine("[" + MicroPFeed.class + "] running feed=" + this);
        logger.fine("[" + MicroPFeed.class + "] itemType=" + getItemType());

        updateFeedCount();

        if (file == null) {
            file = getInputFile();
        }

        if (file == null) {
            throw new FeedException("No data supplied");
        }
        if (!ItemModule.Stock.enabled(context, getItemType())) {
            getItemType().addModule(ItemModule.Stock);
            getItemType().save();
            StockModule stockModule = getItemType().getStockModule();
            stockModule.setStockControl(StockModule.StockControl.RealTime);

            stockModule.save();

        }
        return splitArchive(file);
    }

    private FeedResult prices(File file) throws IOException, FeedException {
        ZipFile zipFile = null;
        String micropFile, productsFile, categoriesFile;
        try {
            zipFile = new ZipFile(file);
            micropFile = getEntry(zipFile, MICROP_FILE);
            productsFile = getEntry(zipFile, ECO_PRODUCTS_FILE);
            categoriesFile = getEntry(zipFile, ECO_CATEGORIES_FILE);
        } finally {
            if (zipFile != null) {
                zipFile.close();
            }
        }

        CsvManager csvman = new CsvManager();

        List<List<String>> rowsMicrop = csvman.loadAsListsFromString(micropFile);
        List<List<String>> rowsProducts = csvman.loadAsListsFromString(productsFile);
        List<List<String>> rowsCategories = csvman.loadAsListsFromString(categoriesFile);

        if (rowsMicrop.size() < 1) {
            logger.fine("[" + MicroPFeed.class + "] the file is empty!");
            return null;
        }

        Map<String, Item> itemsCache = new HashMap<String, Item>();
        //      Map<String, Category> categoriesCache = new HashMap();
        int created = 0;
        long time = System.currentTimeMillis();

        for (List<String> rowMicrop : rowsMicrop) {
            Item item = Item.getByRef(context, getItemType(), rowMicrop.get(MANUFACTURER_PRODUCT_NUMBER_INDEX));
            if (item == null) {
                String name = rowMicrop.get(DESCRIPTION_INDEX) != null ? rowMicrop.get(DESCRIPTION_INDEX) : rowMicrop.get(MANUFACTURER_NAME_INDEX);
                item = new Item(context, getItemType(), name, "LIVE", null);
                created++;
            }

            item.setReference(rowMicrop.get(MANUFACTURER_PRODUCT_NUMBER_INDEX));

            //stock level
            if (rowMicrop.get(QTY_INDEX) != null && rowMicrop.get(QTY_INDEX).length() != 0) {
                item.setOurStock(Integer.parseInt(rowMicrop.get(QTY_INDEX)));
                item.calculateAvailableStock();
            }

            //cost price
            Money costPrice = new Money(0);

            try {
                costPrice = costPrice.add(new Money(rowMicrop.get(PRICE_INDEX)));
            } catch (RuntimeException e1) {
                logger.warning("[" + MicroPFeed.class + "] can not add price. " + e1);
            }
            item.setStandardPrice(null);

            item.setOurCostPrice(costPrice);

            item.setAttributeValue(manufPartNumberAttribute, rowMicrop.get(MANUFACTURER_PRODUCT_NUMBER_INDEX));

            String categorySequence = "";
            for (List rowProducts : rowsProducts) {
                if (rowProducts.get(MP_PARTCODE).equals(rowMicrop.get(MP_PRODUCT_NUMBER_INDEX))) {
                    for (List rowCategories : rowsCategories) {
                        if (rowCategories.get(CAT_ID_CAT).equals(rowProducts.get(CAT_ID_PROD))) {
                            categorySequence = rowCategories.get(WEB_DESC).toString();
                            String categoryId = rowCategories.get(CAT_ID_HIERACHY_LINK).toString();
                            HashMap cat = new HashMap();
                            for (int i = 0; i < rowsCategories.size();) {
                                if (rowsCategories.get(i).get(CAT_ID_CAT).equals(categoryId) && !cat.containsValue(rowsCategories.get(i).get(WEB_DESC))) {
                                    cat.put(i, rowsCategories.get(i).get(WEB_DESC));
                                    categoryId = rowsCategories.get(i).get(CAT_ID_HIERACHY_LINK);
                                    categorySequence = categorySequence + "~" + rowsCategories.get(i).get(WEB_DESC);
                                    i = 0;
                                }
                                else {
                                    i = i + 1;
                                }
                            }
                        }
                    }
                }
            }

            if (!categorySequence.equals("") && categorySequence != null && categorySequence.length() != 0) {
                String[] categories = categorySequence.split("~");
                String[] categ = new String[categories.length];
                int j = 0;
                for (int i = categories.length - 1; i >= 0; i--) {
                    if (categories[i] != null && !categories[i].equals("") && categories[i].length() != 0) {
                        categ[j] = categories[i];
                        j++;
                    }
                }
                Category category = categoriesCache.get(categ[categ.length - 1]);
                if (category == null) {
                    category = Category.getByName(context, categ[categ.length - 1]);
                }
                if (category == null) {
                    category = getCategory(context, categ[0], null);
                    for (int i = 1; i < categ.length - 1; i++) {
                        category = getCategory(context, categ[i], category);
                    }
                    category = new Category(context, categ[categ.length - 1], category);
                    categoriesCache.put(categ[categ.length - 1], category);
                }
                else {
                    categoriesCache.put(categ[categ.length - 1], category);
                }

                item.addCategory(category);

                item.save();
            }
        }
        itemsCache.clear();
        categoriesCache.clear();

        time = System.currentTimeMillis() - time;

        if (disableProducts) {
            List<Item> items = Item.get(context, getItemType(), "LIVE");
            for (Item item : items) {
                if (item.getLastUpdatedString().contains("days")) {
                    String[] tmpStr = item.getLastUpdatedString().split(" ");
                    if (new Integer(tmpStr[0]) > 7 && item.getStatus().equals("LIVE")) {
                        item.setStatus("DISABLED");
                    }
                }
            }
        }
        file.delete();
        return new FeedResult(created, (int) csvman.getLineCount(), time);
    }

    /**
     * Method terurns category with real name: finds it in cache or create a new one
     *
     * @param context  - requestContext
     * @param realName - the name of the category
     * @param parent   - parent category name
     * @return category
     */
    public Category getCategory(RequestContext context, String realName, Category parent) {
        Category category = categoriesCache.get(realName);
        if (category == null) {
            if (parent == null) {
                category = Category.getByName(context, realName);
            }
            else {
                category = parent.getChild(realName);
            }
        }
        if (category == null) {
            category = new Category(context, realName, parent);
            categoriesCache.put(realName, category);
        }
        else {
            categoriesCache.put(realName, category);
        }
        return category;
    }

    /**
     * Method takes file from zip archive
     *
     * @param zipFile - archive
     * @param file    - name of the file
     * @return content of the file
     * @throws FeedException
     * @throws IOException
     */
    private String getEntry(ZipFile zipFile, String file) throws FeedException, IOException {
        String string_file;

        ZipEntry entry = zipFile.getEntry(file);
        if (entry == null) {
            throw new FeedException(file + " not found in data.zip");
        }
        logger.fine("[OpenRangeFeed] " + file + " located in zip: " + entry);

        string_file = SimpleFile.readString(zipFile.getInputStream(entry));
        logger.fine("[OpenRangeFeed] " + file + " loaded into memory: " + string_file.length() + " characters");

        return string_file;
    }

    private FeedResult splitArchive(File file) throws FeedException, IOException {
        ZipFile zipFile = new ZipFile(file);
        Enumeration zipEntriesEnumeration = zipFile.entries();
        List<ZipEntry> zipEntries = new ArrayList<ZipEntry>();
        while (zipEntriesEnumeration.hasMoreElements()) {
            zipEntries.add((ZipEntry) zipEntriesEnumeration.nextElement());
        }
        List<ZipEntry> zipEntriesSupport = new ArrayList<ZipEntry>();
        List<ZipEntry> zipEntriesMicrop = new ArrayList<ZipEntry>();
        for (ZipEntry zipEntry : zipEntries) {
            if (zipEntry.getName().equals(ECO_PRODUCTS_FILE)) {
                zipEntriesSupport.add(zipEntry);
            }
            else if (zipEntry.getName().equals(ECO_CATEGORIES_FILE)) {
                zipEntriesSupport.add(zipEntry);
            }
            else {
                zipEntriesMicrop.add(zipEntry);
            }
        }
        int zipCount = 1;
        FeedResult feedResult = null;
        for (ZipEntry zipEntryMicrop : zipEntriesMicrop) {
            File archive = new File("archive_" + zipCount + ".zip");
            ZipOutputStream zipOutputStraem = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(archive)));
            for (ZipEntry zipEntrySupport : zipEntriesSupport) {
                SimpleZip.archiveString(SimpleZip.extractEntryToString(zipFile, zipEntrySupport), zipEntrySupport.getName(),
                        zipOutputStraem);
            }
            SimpleZip.archiveString(SimpleZip.extractEntryToString(zipFile, zipEntryMicrop), "microp.csv", zipOutputStraem);
            zipCount++;
            zipOutputStraem.close();
            feedResult = prices(archive);
        }
        return feedResult;
    }

    public boolean disableProducts() {
        return disableProducts;
    }

    public void setDisableProducts(boolean disableProducts) {
        this.disableProducts = disableProducts;
    }
}
