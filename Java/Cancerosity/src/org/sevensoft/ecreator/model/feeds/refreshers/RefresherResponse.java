package org.sevensoft.ecreator.model.feeds.refreshers;

import org.sevensoft.commons.skint.Money;

/**
 * @author sks 11 Jul 2006 14:18:44
 *
 */
public class RefresherResponse {

	private int		stock;
	private Money	costPrice;
	private Money	rrp;

	public Money getCostPrice() {
		return costPrice;
	}

	public Money getRrp() {
		return rrp;
	}

	public int getStock() {
		return stock;
	}

	public void updateCostPrice(Money m) {

		if (costPrice == null || costPrice.isGreaterThan(m))
			costPrice = m;
	}

	/**
	 * @param list
	 */
	public void updateRrp(Money m) {

		if (rrp == null || rrp.isGreaterThan(m))
			rrp = m;
	}

	/**
	 * @param stock2
	 */
	public void updateStock(int s) {
		this.stock += s;
	}

	/**
	 * @return
	 */
	public boolean hasRrp() {
		return rrp != null;
	}
}
