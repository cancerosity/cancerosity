package org.sevensoft.ecreator.model.feeds.bespoke.events;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.LinkedHashMap;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.samdate.Time;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.events.VueFeedHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.Http;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

import com.enterprisedt.net.ftp.FTPException;

/**
 * @author sks 28 Jul 2006 17:52:33
 *
 */
@Table("feeds_vue")
@Label("Vue cinemas")
@HandlerClass(VueFeedHandler.class)
@Http
public class VueFeed extends ImportFeed {

	/**
	 * 
	 */
	private Attribute	filmIdAttribute;

	/**
	 * 
	 */
	private Attribute	classificationAttribute;

	/**
	 * 
	 */
	private Attribute	bookingAttribute;

	/**
	 * 
	 */
	private Attribute	dateAttribute;

	/**
	 * 
	 */
	private Attribute	venueAttribute;

	/**
	 * What to set as the venue value
	 */
	private String	venue;

	private ItemType	itemType;

	public VueFeed(RequestContext context) {
		super(context);
	}

	public VueFeed(RequestContext context, boolean create) {
		super(context, create);
	}

	public Attribute getBookingAttribute() {
		return (Attribute) (bookingAttribute == null ? null : bookingAttribute.pop());
	}

	public Attribute getClassificationAttribute() {
		return (Attribute) (classificationAttribute == null ? null : classificationAttribute.pop());
	}

	public Attribute getDateAttribute() {
		return (Attribute) (dateAttribute == null ? null : dateAttribute.pop());
	}

	public Attribute getFilmIdAttribute() {
		return (Attribute) (filmIdAttribute == null ? null : filmIdAttribute.pop());
	}

	public final ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public String getVenue() {
		return venue == null ? "Vue" : venue;
	}

	public Attribute getVenueAttribute() {
		return (Attribute) (venueAttribute == null ? null : venueAttribute.pop());
	}

	public boolean hasItemType() {
		return itemType != null;
	}

	private void init() throws FeedException {

		if (getItemType() == null)
			throw new FeedException("No item type set");

		if (filmIdAttribute == null) {
			filmIdAttribute = getItemType().getAttribute("Film ID");
			if (filmIdAttribute == null) {
				filmIdAttribute = getItemType().addAttribute("Film ID", AttributeType.Text);
			}
		}

		if (classificationAttribute == null) {
			classificationAttribute = getItemType().getAttribute("Classification");
			if (classificationAttribute == null) {
				classificationAttribute = getItemType().addAttribute("Classification", AttributeType.Text);
			}
		}

		if (bookingAttribute == null) {
			bookingAttribute = getItemType().getAttribute("Booking link");
			if (bookingAttribute == null) {
				bookingAttribute = getItemType().addAttribute("Booking link", AttributeType.Link);
			}
		}

		if (dateAttribute == null) {
			dateAttribute = getItemType().getAttribute("Date");
			if (dateAttribute == null) {
				dateAttribute = getItemType().addAttribute("Date", AttributeType.DateTime);
			}
		}

		if (venueAttribute == null) {
			venueAttribute = getItemType().getAttribute("Venue");
			if (venueAttribute == null) {
				venueAttribute = getItemType().addAttribute("Venue", AttributeType.Text);
			}
		}

		if (getDateAttribute().getType() != AttributeType.DateTime) {
			dateAttribute.setType(AttributeType.DateTime);
			dateAttribute.save();
		}

		save();
	}

	@Override
	public void removeAttribute(Attribute attribute) {

		if (attribute.equals(filmIdAttribute)) {
			filmIdAttribute = null;
		}

		if (attribute.equals(classificationAttribute)) {
			classificationAttribute = null;
		}

		if (attribute.equals(bookingAttribute)) {
			bookingAttribute = null;
		}

		if (attribute.equals(dateAttribute)) {
			dateAttribute = null;
		}

		save();
	}

	public FeedResult run(File file) throws IOException, FTPException, FeedException, JDOMException {

		init();

		if (getGenericCategories().isEmpty())
			throw new FeedException("You must specify a category for VUE items");

		if (file == null)
			file = getInputFile();

		logger.fine("loading data from file=" + file);

		Document doc = new SAXBuilder().build(new FileReader(file));
		logger.fine("Document built: " + doc);

		Element root = doc.getRootElement();
		logger.fine("Root el: " + root);

		Element filmList = root.getChild("FilmList");
		List<Element> films = filmList.getChildren("Film");

		int created = 0;
		int processed = 0;
		long millis = System.currentTimeMillis();
		for (Element filmEl : films) {

			try {

				String name = filmEl.getAttributeValue("name");
				String classification = filmEl.getAttributeValue("classification");
				String filmId = filmEl.getAttributeValue("filmID");

				Element viewTimeList = filmEl.getChild("ViewTimeList");
				List<Element> viewTimesEl = viewTimeList.getChildren("ViewTime");

				/*
				 * Make a parent item for the actual film
				 * the show times can be rendereed as sub items
				 */

				for (Element viewTime : viewTimesEl) {

					String bookingLink = viewTime.getAttributeValue("booklink");
					Time time = new Time(viewTime.getAttributeValue("time"));
					Date date = new Date(viewTime.getAttributeValue("date"), "yyyy-MM-dd");

					DateTime dateTime = new DateTime(date, time);

					MultiValueMap<Attribute, String> attributeValues = new MultiValueMap(new LinkedHashMap());
					attributeValues.put(getFilmIdAttribute(), filmId);
					attributeValues.put(getDateAttribute(), dateTime.getTimestampString());
					attributeValues.put(getDateAttribute(), dateTime.getTimestampString());

					ItemSearcher searcher = new ItemSearcher(context);
					searcher.setAttributeValues(attributeValues);
					Item item = searcher.getItem();

					if (item == null) {

						item = new Item(context, getItemType(), name, "Live", null);
						logger.fine("creating item with name=" + name + ", item=" + item);

						attributeValues.clear();
						attributeValues.put(getFilmIdAttribute(), filmId);
						attributeValues.put(getDateAttribute(), dateTime.getTimestampString());
						attributeValues.put(getClassificationAttribute(), classification);
						attributeValues.put(getBookingAttribute(), bookingLink);
						attributeValues.put(getVenueAttribute(), getVenue());

						item.setAttributeValues(attributeValues);
						item.addCategories(getGenericCategories());

						created++;
					}

					processed++;

				}

			} catch (ParseException e) {
				e.printStackTrace();
			}

		}

		millis = System.currentTimeMillis() - millis;

		logger.fine(created + " items created, " + processed + " items processed");
		return new FeedResult(created, processed, millis / 1000);
	}

	public void setBookingAttribute(Attribute bookingAttribute) {
		this.bookingAttribute = bookingAttribute;
	}

	public void setClassificationAttribute(Attribute classificationAttribute) {
		this.classificationAttribute = classificationAttribute;
	}

	public void setDateAttribute(Attribute dateAttribute) {
		this.dateAttribute = dateAttribute;
	}

	public void setFilmIdAttribute(Attribute filmIdAttribute) {
		this.filmIdAttribute = filmIdAttribute;
	}

	public final void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public void setVenueAttribute(Attribute venueAttribute) {
		this.venueAttribute = venueAttribute;
	}

}
