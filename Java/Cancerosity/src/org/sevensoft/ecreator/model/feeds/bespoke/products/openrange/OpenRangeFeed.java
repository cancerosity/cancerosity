package org.sevensoft.ecreator.model.feeds.bespoke.products.openrange;

import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.FeedResult;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.Ftp;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.products.OpenRangeFeedHandler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.commons.simpleio.SimpleFile;
import org.jdom.JDOMException;

import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.zip.ZipFile;
import java.util.zip.ZipEntry;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.enterprisedt.net.ftp.FTPException;
import com.ricebridge.csvman.CsvManager;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
@Table("feeds_open_range")
@Label("Open Range")
@HandlerClass(OpenRangeFeedHandler.class)
@Ftp
public class OpenRangeFeed extends ImportFeed {

    private static String PRODUCTS_FILE = "Products.txt";
    private static String ATTRIBUTES_FILE = "Attributes.txt";
    private static String COMP_ATTRIBUTES_FILE = "CompAttributes.txt";
    private static String OPTIONS_FILE = "Options.txt";
    private static String FEATURES_FILE = "Features.txt";
    private static String SEARCHABLES_FILE = "Searchables.txt";
    private static String FORMAT_FILE = "format.txt";

    public static String LAST_UPDATED_ATTRIBUTE = "Last Updated";
    public static String MANUFACTURER_FAMILY_ATTRIBUTE = "Manufacturer Family";
    public static String MANUFACTURER_FAMILY_MEMBER_ATTRIBUTE = "Manufacturer Family Member";

    protected transient Map<String, Category> categoriesCache = new HashMap();
    protected transient Map<String, Item> itemsCache = new HashMap();

    private ItemType itemType;
    private Attribute lastUpdatedAttribute;
    private Attribute manufFamilyAttribute;
    private Attribute manufFamilyMemberAttribute;

    protected OpenRangeFeed(RequestContext context) {
        super(context);
    }

    public OpenRangeFeed(RequestContext context, boolean create) {
        super(context, create);
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public ItemType getItemType() {
        return (ItemType) (itemType == null ? null : itemType.pop());
    }

    public Attribute getLastUpdatedAttribute() {
        return (Attribute) (lastUpdatedAttribute == null ? null : lastUpdatedAttribute.pop());
    }

    public void setLastUpdatedAttribute(Attribute lastUpdatedAttribute) {
        this.lastUpdatedAttribute = lastUpdatedAttribute;
    }

    public Attribute getManufFamilyAttribute() {
        return (Attribute) (manufFamilyAttribute == null ? null : manufFamilyAttribute.pop());
    }

    public void setManufFamilyAttribute(Attribute manufFamilyAttribute) {
        this.manufFamilyAttribute = manufFamilyAttribute;
    }

    public Attribute getManufFamilyMemberAttribute() {
        return (Attribute) (manufFamilyMemberAttribute == null ? null : manufFamilyMemberAttribute.pop());
    }

    public void setManufFamilyMemberAttribute(Attribute manufFamilyMemberAttribute) {
        this.manufFamilyMemberAttribute = manufFamilyMemberAttribute;
    }

    public void removeAttribute(Attribute attribute) {
        if (attribute.equals(lastUpdatedAttribute)) {
            lastUpdatedAttribute = null;
        }

        if (attribute.equals(manufFamilyAttribute)) {
            manufFamilyAttribute = null;
        }

        if (attribute.equals(manufFamilyMemberAttribute)) {
            manufFamilyMemberAttribute = null;
        }

        save();
    }

    public FeedResult run(File file) throws IOException, FTPException, FeedException, JDOMException {
        if (getItemType()==null){
            return null;
        }
        
        updateFeedCount();

        if (file == null) {
            file = getInputFile();
        }

        if (file == null) {
            throw new FeedException("No data supplied");
        }

        initAttributes();

        String products, attributes, comp_attributes, options, features, searchables, format = null;

        ZipFile zipFile = null;
        try {
            zipFile = new ZipFile(file);

            products = getEntry(zipFile, PRODUCTS_FILE);
//            attributes = getEntry(zipFile, ATTRIBUTES_FILE);

            comp_attributes = getEntry(zipFile, COMP_ATTRIBUTES_FILE);
//            options = getEntry(zipFile, OPTIONS_FILE);
//            features = getEntry(zipFile, FEATURES_FILE);
//            searchables = getEntry(zipFile, SEARCHABLES_FILE);
//            format = getEntry(zipFile, FORMAT_FILE);

        } finally {

            if (zipFile != null) {
                zipFile.close();
            }

        }

        FeedResult result = items(products);
//        itemsContent(attributes);
        itemsContent(comp_attributes);
        return result;
    }

    /**
     * Method initializes all necessary attributes
     */
    private void initAttributes() {
        getItemType().setStatusControl(true);
        getItemType().setReferences(true);
        getItemType().setReferencesCaption("PartNo");

        if (lastUpdatedAttribute == null)
            lastUpdatedAttribute = getItemType().addAttribute(LAST_UPDATED_ATTRIBUTE, AttributeType.Text);

        if (manufFamilyAttribute == null) {
            manufFamilyAttribute = getItemType().addAttribute(MANUFACTURER_FAMILY_ATTRIBUTE, AttributeType.Selection);
        }

        if (manufFamilyMemberAttribute == null) {
            manufFamilyMemberAttribute = getItemType().addAttribute(MANUFACTURER_FAMILY_MEMBER_ATTRIBUTE, AttributeType.Text);
        }

        save();

    }

    /**
     * Method takes file from zip archive
     *
     * @param zipFile - archive
     * @param file - name of the file
     * @return content of the file
     * @throws FeedException
     * @throws IOException
     */
    private String getEntry(ZipFile zipFile, String file) throws FeedException, IOException {
        String string_file;

        ZipEntry entry = zipFile.getEntry(file);
        if (entry == null) {
            throw new FeedException(file + " not found in data.zip");
        }
        logger.fine("[OpenRangeFeed] " + file + " located in zip: " + entry);

        string_file = SimpleFile.readString(zipFile.getInputStream(entry));
        logger.fine("[OpenRangeFeed] " + file + " loaded into memory: " + string_file.length() + " characters");

        return string_file;
    }

    /**
     * Method parses data and create items and categories
     *
     * @param data - data from txt file
     * @return FeedResult
     */
    private FeedResult items(CharSequence data) {

        CsvManager csvman = new CsvManager();

        List<List<String>> rows = getRows(csvman, data);
        if (rows.size() < 2) {
            return null;
        }

        logger.fine("[OpenRangeFeed] Rows loaded: " + rows.size());

        List<String> labels = rows.remove(0);
        logger.fine("[OpenRangeFeed] Labels parsed: " + labels + " " + labels.size());

        //ProductID|Manufacturer|PartNo|Model|Manufacturer_Family|Manufacturer_Family_Member|UNSPSC_Segment|UNSPSC_Family|UNSPSC_Class|UNSPSC_Commodity|UNSPSC|Image|LastUpdated|Status
        //ProductID|InternalPartNo|Manufacturer|Partno|Model|Manufacturer_Family|Manufacturer_Family_Member|UNSPSC_Segment|UNSPSC_Family|UNSPSC_Class|UNSPSC_Commodity|UNSPSC|Image|LastUpdated|Status
                   
        final int product_id_index = labels.indexOf("ProductID");
        final int manufacturer_index = labels.indexOf("Manufacturer");
        int part_no_index = labels.indexOf("PartNo");  //todo need reference to be enabled in item type settings
        if (part_no_index == -1){
            part_no_index = labels.indexOf("Partno");
        }
        final int model_index = labels.indexOf("Model");
        final int manufacturer_family_index = labels.indexOf("Manufacturer_Family");
        final int manufacturer_family_member_index = labels.indexOf("Manufacturer_Family_Member");
        final int segment_index = labels.indexOf("UNSPSC_Segment");
        final int family_index = labels.indexOf("UNSPSC_Family");
        final int class_index = labels.indexOf("UNSPSC_Class");
        final int commodity_index = labels.indexOf("UNSPSC_Commodity");
        final int unspsc_index = labels.indexOf("UNSPSC");
        final int image_index = labels.indexOf("Image");
        final int last_updated_index = labels.indexOf("LastUpdated");
        final int status_index = labels.indexOf("Status");

        for (List<String> row : rows) {
           Item item = Item.getByRef(context, itemType, row.get(part_no_index));
            if (item == null) {
                continue;
            }
            item.setOpenRangeProductId(Long.parseLong(row.get(product_id_index)));
            item.setAttributeValue(lastUpdatedAttribute, row.get(last_updated_index));
            item.setAttributeValue(manufFamilyAttribute, row.get(manufacturer_family_index));
            item.setAttributeValue(manufFamilyMemberAttribute, row.get(manufacturer_family_member_index));

            if (isCraeteCategories()) {
                Category category = categoriesCache.get(row.get(commodity_index));
                if (category == null) {
                    category = Category.getByName(context, row.get(commodity_index));
                }
                if (category == null) {

                    Category segmentCategory = getCategory(context, row.get(segment_index), null);

                    Category familyCategory = getCategory(context, row.get(family_index), segmentCategory);

                    Category classCategory = getCategory(context, row.get(class_index), familyCategory);

                    category = new Category(context, row.get(commodity_index), classCategory);

                    categoriesCache.put(row.get(commodity_index), category);
                } else {
                    categoriesCache.put(row.get(commodity_index), category);
                }

                item.addCategory(category);
            }

            addImage(image_index, row, item);

            item.save();

            itemsCache.put(String.valueOf(item.getOpenRangeProductId()), item);

        }
        categoriesCache.clear();
        return new FeedResult(csvman);
    }

    /**
     * Method adds an image to existent item
     *
     * @param image_index - number of image in row
     * @param row - a row from list of rows of a file
     * @param item - created item
     */
    private void addImage(int image_index, List<String> row, Item item) {
        try {
            List<Img> images = Img.getByFilename(context, row.get(image_index));
            if (images.isEmpty()) {
                new Img(context, item, row.get(image_index), true, false, true);
            } else {
                boolean hasImage = false;
                for (Img image : images) {
                    if (image.getItem() != null && image.getItem().equals(item)) {
                        hasImage = true;
                    }
                }

                if (!hasImage) {
                    new Img(context, item, row.get(image_index), true, false, true);
                }
            }
        } catch (FileNotFoundException e) {
            logger.warning("[" + OpenRangeFeed.class + "] " + e);
        } catch (IOException e) {
            logger.warning("[" + OpenRangeFeed.class + "] Can't create an ImageInputStream! image=" + row.get(image_index));
        } catch (ImageLimitException e) {
            logger.warning("[" + OpenRangeFeed.class + "] images limit is exhausted");
        }
    }

    /**
     * Method adds attributes and contents of the existent items
     *
     * @param data - data from txt file
     */
    private void itemsContent(CharSequence data) {
        CsvManager csvman = new CsvManager();

        List<List<String>> rows = getRows(csvman, data);
        if (rows.size() < 2) {
            return;
        }

        logger.fine("[OpenRangeFeed] Rows loaded: " + rows.size());

        List<String> labels = rows.remove(0);
        logger.fine("[OpenRangeFeed] Labels parsed: " + labels + " " + labels.size());

//       ATTRIBUTES_FILE //  ProductID|Name|Value|Units
//       COMP_ATTRIBUTES_FILE  ProductID|Name|Value|Group|Sequence|Level|Overview

        //common attributes
        final int product_id_index = labels.indexOf("ProductID");
        final int name_index = labels.indexOf("Name");
        final int value_index = labels.indexOf("Value");
        // ATTRIBUTES_FILE
        final int units_index = labels.indexOf("Units");

        // COMP_ATTRIBUTES_FILE
        final  int group_index = labels.indexOf("Group");

        for (List<String> row : rows) {
            Item item = itemsCache.get(row.get(product_id_index));

            if (item == null) {
                List<Item> items = new Query(context, "SELECT * FROM # WHERE itemType=? AND openRangeProductId=?").setTable(Item.class).setParameter(getItemType()).setParameter(row.get(product_id_index)).execute(Item.class);
                if (items != null && !items.isEmpty()) {
                    item = items.get(0);
                    itemsCache.put(String.valueOf(item.getOpenRangeProductId()), item);
                }
            }
            if (item == null) {
                continue;
            }
            String name = row.get(name_index);
            String value = row.get(value_index);
//            String unit = row.get(units_index);
            if (name.equals("Image")) {
                //images have been already added
            } else if (name.equals("Marketing") || name.equals("Description")) {
                item.setContent(value);
            } else {
                Attribute attr = item.getAttribute(name);
                if (attr == null) {
                    attr = new Attribute(context, getItemType(), name, null);
                }
                if (attr.getSection() == null) {
                    attr.setSection(row.get(group_index));
                    attr.save();
                }
            //    item.addAttributeValue(attr, value/* + unit*/);
                item.setAttributeValue(attr, value);
            }
        }
        itemsCache.clear();
    }

    /**
     * Method returns list of rows of the file
     *
     * @param csvman instance of CsvManager
     * @param data data from txt file
     * @return list of rows of the file
     */
    private List<List<String>> getRows(CsvManager csvman, CharSequence data) {
        logger.fine("[OpenRangeFeed] Creating csv manager");

        csvman.setSeparator("|");

        List<List<String>> rows = csvman.loadAsListsFromString(data.toString().replaceAll("\"", "'"));

        logger.fine("[OpenRangeFeed] Rows loaded: " + rows.size());
        return rows;
    }

    /**
     * Method terurns category with real name: finds it in cache or create a new one
     *
     * @param context - requestContext
     * @param realName - the name of the category
     * @param parent - parent category name
     * @return category
     */
    public Category getCategory(RequestContext context, String realName, Category parent) {
        Category category = categoriesCache.get(realName);
        if (category == null) {
            if (parent == null) {
                category = Category.getByName(context, realName);
            } else {
                category = parent.getChild(realName);
            }
        }
        if (category == null) {
            category = new Category(context, realName, parent);
            categoriesCache.put(realName, category);
        } else {
            categoriesCache.put(realName, category);
        }
        return category;
    }

}
