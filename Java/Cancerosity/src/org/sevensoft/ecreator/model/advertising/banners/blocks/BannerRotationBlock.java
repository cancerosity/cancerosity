package org.sevensoft.ecreator.model.advertising.banners.blocks;

import java.util.List;

import org.sevensoft.commons.superstrings.RandomHelper;
import org.sevensoft.ecreator.iface.admin.extras.banners.BannerBlockHandler;
import org.sevensoft.ecreator.model.advertising.banners.Banner;
import org.sevensoft.ecreator.model.advertising.banners.BannerJoin;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 14 Jul 2006 10:34:32
 *
 */
@Table("blocks_banners_rotation")
@HandlerClass(BannerBlockHandler.class)
@Label("Banner rotation")
public class BannerRotationBlock extends Block {

	private BannerRotationBlock(RequestContext context) {
		super(context);
	}

	public BannerRotationBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, 0);
	}

	public void addBanner(Banner banner) {
		new BannerJoin(context, banner, this);
	}

	public List<Banner> getBanners() {
		Query q = new Query(context, "select b.* from # b join # bj on b.id=bj.banner where bj.block=?");
		q.setTable(Banner.class);
		q.setTable(BannerJoin.class);
		q.setParameter(this);
		return q.execute(Banner.class);
	}

	public void removeBanner(Banner banner) {
		SimpleQuery.delete(context, BannerJoin.class, "banner", banner, "block", this);
	}

	@Override
	public Object render(RequestContext context) {

		List<Banner> banners = getBanners();
		if (banners.isEmpty())
			return null;

		Banner banner = RandomHelper.getRandomCollectionItem(banners);
		if (!banner.hasApprovedImages())
			return null;

		banner.shown();
		return banner.render();
	}

}
