package org.sevensoft.ecreator.model.advertising.banners.boxes;

import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Nov 2006 17:01:16
 *
 */
public class BannerRotationBox extends Box {

	protected BannerRotationBox(RequestContext context) {
		super(context);
	}

	@Override
	protected String getCssIdDefault() {
		return null;
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	@Override
	public String render(RequestContext context) {
		return null;
	}

}
