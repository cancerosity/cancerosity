package org.sevensoft.ecreator.model.advertising.banners;

import java.util.List;

import org.sevensoft.ecreator.iface.frontend.extras.banners.BannerForwarderHandler;
import org.sevensoft.ecreator.model.advertising.banners.blocks.BannerBlock;
import org.sevensoft.ecreator.model.media.images.ImageSupport;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 02-Aug-2005 23:19:35
 * 
 */
@Table("banners")
public class Banner extends ImageSupport implements Selectable {

	public static List<Banner> get(RequestContext context) {
		return SimpleQuery.execute(context, Banner.class);
	}

	private int		clicks, impressionsMade, impressionsLimit;

	/**
	 * Third party code to generate the banner
	 */
	private String	bannerCode;

	/**
	 * Disabled / enable this banner
	 */
	private boolean	live;

	/**
	 * The URL this banner links to
	 */
	private String	url;

	/**
	 * Name this banner
	 */
	private String	name;

	public Banner(RequestContext context) {
		super(context);
	}

	public Banner(RequestContext context, String name) {
		super(context);

		this.name = name;
		save();
	}

	public void click() {
		clicks++;
		save();
	}

	@Override
	public synchronized boolean delete() {

		SimpleQuery.delete(context, BannerBlock.class, "banner", this);
		return super.delete();
	}

	public String getAlt() {
		return name;
	}

	public String getBannerCode() {
		return bannerCode;
	}

	public int getClicks() {
		return clicks;
	}

	public double getClicksPercentage() {
		return clicks / impressionsMade;
	}

	public int getImageLimit() {
		return 1;
	}

	public int getImpressionsLimit() {
		return impressionsLimit;
	}

	public int getImpressionsMade() {
		return impressionsMade;
	}

	public String getLabel() {
		return getName();
	}

	public LinkTag getLinkTag() {
		return hasApprovedImages() ? new LinkTag(BannerForwarderHandler.class, null, getApprovedImage().getImageTag(), "banner", this) : null;
	}

	public String getName() {
		return name;
	}

	public final String getUrl() {
		return url;
	}

	public String getValue() {
		return getIdString();
	}

	public boolean hasBannerCode() {
		return bannerCode != null;
	}

	public boolean hasImpressionsLimit() {
		return impressionsLimit > 0;
	}

	public boolean hasUrl() {
		return url != null;
	}

	public boolean isLive() {
		return live;
	}

	public String render() {

		if (hasApprovedImages())
			return getLinkTag().toString();
		else
			return bannerCode;
	}

	public void reset() {
		this.impressionsMade = 0;
		save();
	}

	public void setBannerCode(String bannerCode) {
		this.bannerCode = bannerCode;
	}

	public void setImpressionsLimit(int impressionsLimit) {
		this.impressionsLimit = impressionsLimit;
	}

	public void setLive(boolean live) {
		this.live = live;
	}

	public void setName(String name) {
		this.name = name;
	}

	public final void setUrl(String url) {

		if (url == null) {
			this.url = url;
			return;
		}

		if (!url.startsWith("http"))
			this.url = "http://" + url;
		else
			this.url = url;
	}

	public void shown() {

		this.impressionsMade++;
		if (this.impressionsMade >= this.impressionsLimit)
			this.live = false;

		save();
	}

	public boolean useImageDescriptions() {
		return false;
	}
}
