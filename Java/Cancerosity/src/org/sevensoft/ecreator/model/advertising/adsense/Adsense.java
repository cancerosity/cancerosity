package org.sevensoft.ecreator.model.advertising.adsense;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 14 Jul 2006 10:34:57
 *
 */
@Table("adsense")
public class Adsense extends EntityObject {

	public Adsense(RequestContext context) {
		super(context);
	}

}
