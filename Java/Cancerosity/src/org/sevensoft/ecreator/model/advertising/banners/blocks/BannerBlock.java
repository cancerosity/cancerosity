package org.sevensoft.ecreator.model.advertising.banners.blocks;

import org.sevensoft.ecreator.model.advertising.banners.Banner;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 5 Nov 2006 13:56:35
 *
 */
@Table("blocks_banners")
@Label("Banner")
public class BannerBlock extends Block {

	private Banner	banner;

	protected BannerBlock(RequestContext context) {
		super(context);
	}

	public BannerBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, 0);

		this.banner = EntityObject.getInstance(context, Banner.class, objId);
		setName("Banner: " + getBanner().getName());
		save();
	}

	public Banner getBanner() {
		return (Banner) (banner == null ? null : banner.pop());
	}

	@Override
	public Object render(RequestContext context) {

		getBanner().shown();
		return getBanner().render();
	}

}
