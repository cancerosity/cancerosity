package org.sevensoft.ecreator.model.advertising.banners.boxes;

import org.sevensoft.ecreator.iface.admin.extras.banners.BannerBoxHandler;
import org.sevensoft.ecreator.model.advertising.banners.Banner;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 14 Jul 2006 10:35:27
 *
 */
@Table("boxes_banners")
@Label("Banner")
@HandlerClass(BannerBoxHandler.class)
public class BannerBox extends Box {

	private Banner	banner;

	public BannerBox(RequestContext context) {
		super(context);
	}

	public BannerBox(RequestContext context, String location) {
		super(context, location);
	}

	@Override
	protected String getCssIdDefault() {
		return "banner";
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	@Override
	public String render(RequestContext context) {
		return null;
	}

}
