package org.sevensoft.ecreator.model.advertising.banners;

import org.sevensoft.ecreator.model.advertising.banners.blocks.BannerRotationBlock;
import org.sevensoft.ecreator.model.advertising.banners.boxes.BannerBox;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 23 Oct 2006 07:22:32
 *
 */
@Table("banners_joins")
public class BannerJoin extends EntityObject {

	private Banner		banner;
	private BannerRotationBlock	block;
	private BannerBox		box;

	public BannerJoin(RequestContext context) {
		super(context);
	}

	public BannerJoin(RequestContext context, Banner banner, BannerRotationBlock block) {
		super(context);

		if (!block.getBanners().contains(banner)) {
			this.banner = banner;
			this.block = block;
			save();
		}
	}

}
