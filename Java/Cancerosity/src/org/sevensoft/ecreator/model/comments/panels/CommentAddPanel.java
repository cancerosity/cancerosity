package org.sevensoft.ecreator.model.comments.panels;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.ecreator.iface.frontend.account.registration.RegistrationHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.comments.CommentHandler;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.design.marker.MarkerRenderer;
import org.sevensoft.ecreator.model.comments.CommentsSettings;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * User: Tanya
 * Date: 07.09.2012
 */
public class CommentAddPanel extends Panel {

    private BlockOwner blockOwner;
    private Item account;
    private Item blockOwnerItem;

    public CommentAddPanel(RequestContext context, BlockOwner blockOwner) {
        super(context);
        this.blockOwner = blockOwner;
    }

    public CommentAddPanel(RequestContext context, BlockOwner blockOwner, Item account) {
        super(context);
        this.blockOwner = blockOwner;
        this.account = account;
    }

    public void setAccount(Item account) {
        this.account = account;
    }

    public void setBlockOwnerItem(Item blockOwnerItem) {
        this.blockOwnerItem = blockOwnerItem;
    }

    public void makeString() {

        sb.append("<div class='commenttitle'>");
        sb.append("Comments:");
        sb.append("</div>");
        sb.append(MarkerRenderer.render(context, "[comments]", blockOwnerItem != null ? blockOwnerItem : blockOwner));
        Link linkback;
        if (blockOwner instanceof Category) {
            linkback = new Link(CategoryHandler.class, "main", "category", blockOwner);
        } else {
            linkback = new Link(ItemHandler.class, "main", "item", blockOwnerItem);
        }

        sb.append("<div class='addcommentform'>");
        if (account == null) {

            sb.append("<div class='registerlink'>");
            sb.append("<br>Not a member? " + new LinkTag(RegistrationHandler.class, null, "Create new account", "linkback", linkback) + " here.<br>");
            sb.append("</div>");
            sb.append(new FormTag(LoginHandler.class, "login", "post"));

            hiddenTags(linkback);

            sb.append(new MarkupRenderer(context, CommentsSettings.getInstance(context).getLoginMarkup()));
            sb.append("</form>");

        }
        sb.append(new MessagesTag(context));

        sb.append(new FormTag(CommentHandler.class, "add", "post"));
        sb.append(new HiddenTag("account", account));
        hiddenTags(linkback);
        sb.append(new MarkupRenderer(context, CommentsSettings.getInstance(context).getCommentsAddMarkup()));
        sb.append("</form>");

        sb.append("</div>");


    }

    private void hiddenTags(Link linkback) {
        sb.append(new HiddenTag("linkback", linkback));
        if (blockOwner instanceof Category) {
            sb.append(new HiddenTag("category", blockOwner));
        } else {
            sb.append(new HiddenTag("item", blockOwnerItem));
        }
    }
}
