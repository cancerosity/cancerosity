package org.sevensoft.ecreator.model.comments.blocks;

import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.design.marker.MarkerRenderer;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.comments.CommentsSettings;
import org.sevensoft.ecreator.model.comments.panels.CommentAddPanel;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.iface.frontend.comments.CommentHandler;
import org.sevensoft.ecreator.iface.frontend.account.registration.RegistrationHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;

/**
 * User: Tanya
 * Date: 29.07.2010
 */
@Table("blocks_comments")
@Label("Comments")
//@HandlerClass(CommentHandler.class)
public class CommentPostBlock extends Block {

    private Item account;


    private String post;

    private Item item;

    protected CommentPostBlock(RequestContext context) {
        super(context);
    }

    public CommentPostBlock(RequestContext context, BlockOwner owner, int objId) {
        super(context, owner, objId);
    }

    public Item getAccount() {
        return account;
    }

    public void setAccount(Item account) {
        this.account = account;
    }

    public Item getOwnerItem() {
        return item;
    }

    public void setOwnerItem(Item ownerItem) {
        this.item = ownerItem;
    }

    public Object render(RequestContext context) {
        return new CommentAddPanel(context, getOwner(), account);
    }

    public static int removeAtCategories(RequestContext context) {
       return new Query(context, "delete from # where ownerCategory!=0").setTable(CommentPostBlock.class).run();
    }

    public static CommentPostBlock getBlock(RequestContext context, BlockOwner owner) {
        String fieldOwner = owner instanceof Category ? "ownerCategory" : "ownerItemType";
        CommentPostBlock block = SimpleQuery.get(context, CommentPostBlock.class, fieldOwner, owner);
        if (block == null) {
            block = new CommentPostBlock(context, owner, 0);
        }
        return block;
    }
}
