package org.sevensoft.ecreator.model.comments.markers;

import org.sevensoft.ecreator.model.design.marker.interfaces.ICommentMarker;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.comments.Comment;
import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

import java.util.Map;

/**
 * User: Tanya
 * Date: 29.07.2010
 */
public class CommentAuthorMarker extends MarkerHelper implements ICommentMarker {

    public Object generate(RequestContext context, Map<String, String> params, Comment comment) {
        if (params.containsKey("link") && comment.getAccount() != null) {
            params.put("text", comment.getAuthorName());
            return super.link(context, params, new Link(ItemHandler.class, "main", "item", comment.getAccount()));
        } else if (params.containsKey("attribute") && comment.getAccount() != null) {
            Integer attrId = Integer.valueOf(params.get("attribute"));
            return super.string(context, params, comment.getAccount().getAttributeValue(attrId));
        }
        return super.string(context, params, comment.getAuthorName());
    }

    public Object getRegex() {
        return "comment_author";
    }
}
