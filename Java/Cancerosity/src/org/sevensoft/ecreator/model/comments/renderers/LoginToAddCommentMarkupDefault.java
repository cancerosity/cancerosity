package org.sevensoft.ecreator.model.comments.renderers;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * User: Tanya
 * Date: 30.07.2010
 */

public class LoginToAddCommentMarkupDefault implements MarkupDefault {

    public String getBetween() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getBody() {
        StringBuilder sb = new StringBuilder();

        sb.append("<p class='comment_section_heading'>Login to share your views</p>\n");
        sb.append("<fieldset class='ec_form'>\n");
        sb.append("<div class='field_input' style='float: left;'><div class='label'>Email</div><div class='input'>\n");
        sb.append("<input type='text' class='text' name='email' /></div><sub>(Your email address will never be published)</sub></div>\n");
        sb.append("\n");
        sb.append("<div class='field_input' style='float: right; margin-right: 100px;'><div class='label'>Password</div><div class='input'>\n");
        sb.append("<input type='password' class='password' name='password' /></div></div>\n");
        sb.append("<div class='ec_form_commands' style='clear: both;'><input value='Login' type='submit' /></div>\n");
        sb.append("</fieldset>\n");

        return sb.toString();
    }

    public String getEnd() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getStart() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getTds() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getCss() {
        return ".registerlink { display: block; color: #333333; padding: 10px 0 ; font-size: 12px; }";
    }

    public String getName() {
        return null;
    }
}
