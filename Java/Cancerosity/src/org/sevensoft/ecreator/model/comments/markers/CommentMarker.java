package org.sevensoft.ecreator.model.comments.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.ICategoryMarker;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.comments.Comment;
import org.sevensoft.ecreator.model.comments.CommentsSettings;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;
import java.util.List;

/**
 * User: Tanya
 * Date: 28.07.2010
 */
public class CommentMarker extends MarkerHelper implements IItemMarker, ICategoryMarker {


    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
        return generate(context, params, Comment.get(context, false, item));
    }

    public Object generate(RequestContext context, Map<String, String> params, Category category) {
        return generate(context, params, Comment.get(context, false, category));
    }

    public Object generate(RequestContext context, Map<String, String> params, List<Comment> comments) {

        if (!Module.Comments.enabled(context)) {
            return null;
        }

        if (comments.isEmpty()) {
            return null;
        }

        Markup markup = CommentsSettings.getInstance(context).getCommentsListMarkup();

        MarkupRenderer r = new MarkupRenderer(context, markup);
        r.setBodyObjects(comments);

        return r;

    }

    public Object getRegex() {
        return "comments";
    }
}
