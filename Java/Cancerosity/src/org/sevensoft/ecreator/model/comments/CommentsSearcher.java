package org.sevensoft.ecreator.model.comments;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.db.StringQueryBuilder;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.categories.Category;

import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

/**
 * User: Tanya
 * Date: 17.11.2011
 */
public class CommentsSearcher implements Iterable<Comment> {

    private static Logger logger = Logger.getLogger("ecreator");

    private final RequestContext context;

    private int limit;
    private Item searchParentItem;
    private Category searchParentCategory;
    private boolean visibleOnly;

    public CommentsSearcher(RequestContext context) {

        this.context = context;
        this.visibleOnly = true;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public Item getSearchParentItem() {
        return searchParentItem;
    }

    public void setSearchParentItem(Item searchParentItem) {
        this.searchParentItem = searchParentItem;
    }

    public Category getSearchParentCategory() {
        return searchParentCategory;
    }

    public void setSearchParentCategory(Category searchParentCategory) {
        this.searchParentCategory = searchParentCategory;
    }

    public Iterator<Comment> iterator() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public List<Comment> getComments() {
        return (List<Comment>) execute(limit);
    }

    private Object execute(int limit) {

        StringQueryBuilder b = new StringQueryBuilder(context);

        b.append("select distinct i.* ");

        b.append(" from # i  ");
        b.addTable(Comment.class);

        b.append(" where ");

        if (searchParentItem != null) {
            b.append(" i.item=? and ", searchParentItem);
        } else if (searchParentCategory != null) {
            b.append(" i.category=? and ", searchParentCategory);
        }

        if (visibleOnly) {
            b.append(" i.awaitingModeration=0 and  ");
        }

        b.append(" 1=1 ");

        logger.fine(b.toString());

        List objs = b.toQuery().execute(Comment.class, limit);

        logger.fine("[CommentSearcher] " + objs.size() + " objects retrieved");

        return objs;
    }
}
