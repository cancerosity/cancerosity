package org.sevensoft.ecreator.model.comments;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.comments.renderers.CommentListMarkupDefault;
import org.sevensoft.ecreator.model.comments.renderers.CommentAddMarkupDefault;
import org.sevensoft.ecreator.model.comments.renderers.LoginToAddCommentMarkupDefault;
import org.sevensoft.commons.superstrings.StringHelper;

import java.util.TreeSet;
import java.util.Collection;
import java.util.Set;
import java.util.HashSet;

/**
 * User: Tanya
 * Date: 26.07.2010
 */
@Table("settings_comments")
@Singleton
public class CommentsSettings extends EntityObject {

    public static CommentsSettings getInstance(RequestContext context) {
        return getSingleton(context, CommentsSettings.class);
    }

    private boolean addToNewCategories;

    private boolean moderate;

    private Markup commentsListMarkup;

    private Markup commentsAddMarkup;

    private Markup loginMarkup;

    private Set<String> commentNotificationEmails;


    protected CommentsSettings(RequestContext context) {
        super(context);
    }

    public boolean isAddToCategories() {
        return addToNewCategories;
    }

    public void setAddToCategories(boolean addToNewCategories) {
        this.addToNewCategories = addToNewCategories;
    }

    public boolean isModerate() {
        return moderate;
    }

    public void setModerate(boolean moderate) {
        this.moderate = moderate;
    }

    public Markup getCommentsListMarkup() {
        if (commentsListMarkup == null) {
            commentsListMarkup = new Markup(context, "Comments list markup", new CommentListMarkupDefault());
            save();
        }
        return commentsListMarkup.pop();
    }

    public void setCommentsListMarkup(Markup commentsListMarkup) {
        this.commentsListMarkup = commentsListMarkup;
    }

    public Markup getCommentsAddMarkup() {
        if (commentsAddMarkup == null) {
            commentsAddMarkup = new Markup(context, "Comments Add markup", new CommentAddMarkupDefault());
            save();
        }
        return commentsAddMarkup.pop();
    }

    public void setCommentsAddMarkup(Markup commentsAddMarkup) {
        this.commentsAddMarkup = commentsAddMarkup;
    }

    public Markup getLoginMarkup() {
        if (loginMarkup == null) {
            loginMarkup = new Markup(context, "Login to Add markup", new LoginToAddCommentMarkupDefault());
            save();
        }
        return loginMarkup.pop();
    }

    public void setLoginMarkup(Markup loginMarkup) {
        this.loginMarkup = loginMarkup;
    }

    public void setCommentNotificationEmails(Collection<String> commentNotificationEmails) {
        this.commentNotificationEmails = new TreeSet();
		if (commentNotificationEmails != null) {
			this.commentNotificationEmails.addAll(commentNotificationEmails);
		}
    }

    public Set<String> getCommentNotificationEmails() {
		if (commentNotificationEmails == null) {
			commentNotificationEmails = new HashSet();
		}
		return commentNotificationEmails;
	}

    public String getCommentNotificationEmailsString() {
		return StringHelper.implode(getCommentNotificationEmails(), "\n", true);
	}

    public boolean hasNotificationEmails() {
		return getCommentNotificationEmails().size() > 0;
	}
}
