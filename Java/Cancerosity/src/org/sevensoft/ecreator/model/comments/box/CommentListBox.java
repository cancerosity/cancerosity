package org.sevensoft.ecreator.model.comments.box;

import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.search.SearchOwner;
import org.sevensoft.ecreator.model.search.comments.CommentSearch;
import org.sevensoft.ecreator.model.misc.CacheContainer;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.comments.box.markup.CommentListBoxMarkup;
import org.sevensoft.ecreator.model.comments.Comment;
import org.sevensoft.ecreator.model.comments.CommentsSearcher;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.iface.admin.comments.box.CommentListBoxHandler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.db.annotations.Table;

import java.util.Map;
import java.util.HashMap;
import java.util.List;

/**
 * User: Tanya
 * Date: 16.11.2011
 */
@Table("boxes_highlighted_comments")
@Label("Comments list")
@HandlerClass(CommentListBoxHandler.class)
public class CommentListBox extends Box implements SearchOwner {

    public static Map<Integer, CacheContainer> htmlCache;

    static {
        htmlCache = new HashMap();
    }

    /**
     *
     */
    public static void clearHtmlCache() {
        htmlCache.clear();
    }

    protected CommentListBox(RequestContext context) {
        super(context);
    }

    public CommentListBox(RequestContext context, String location) {
        super(context, location);
    }

    @Override
    public synchronized boolean delete() {
        htmlCache.remove(getId());
        return super.delete();
    }

    @Override
    protected String getCssIdDefault() {
        return "highlighted_comments";
    }

    @Override
    public Markup getMarkup() {

        if (markup == null) {

            markup = new Markup(context, new CommentListBoxMarkup());
            markup.setName("Comments box markup");
            save();
        }
        return markup.pop();
    }

    //todo should be search not for itemtype but for comments
    public final CommentSearch getSearch() {

        CommentSearch search = CommentSearch.get(context, this);
        if (search == null) {

            search = new CommentSearch(context, this);
            search.setLimit(5);

            search.save();
        }

        logger.fine("[HighlightedItemsBox] search=" + search);

        return search;
    }

    @Override
    public final boolean hasEditableContent() {
        return false;
    }

    public String render(RequestContext context) {

        logger.fine("[HighlightedItemsBox] rendering=" + name);

        // check html cache first
        if (htmlCache.containsKey(getId())) {
            CacheContainer container = htmlCache.get(getId());
            if (container.isExpired()) {
                htmlCache.remove(getId());
            } else {
                return container.getCachedContent();
            }
        }

        CommentSearch search = getSearch();
        CommentsSearcher searcher = search.getSearcher();

        // override status

        // ensure a sensible bound on items
        if (searcher.getLimit() < 1 || searcher.getLimit() > 30) {
            searcher.setLimit(30);
        }

        if (search.isShowOnlyChildren()) {
            searcher.setSearchParentItem((Item) context.getAttribute("item"));
            searcher.setSearchParentCategory((Category) context.getAttribute("category"));
        }

        List<Comment> comments = searcher.getComments();
        if (comments.isEmpty()) {
            return null;
        }


        logger.fine("[CommentListBox] items=" + comments);

        StringBuilder sb = new StringBuilder("<!-- start highlighted items box -->");
        sb.append(getTableTag());
        sb.append("<tr><td>");

        renderStatic(context, sb, comments);

        sb.append("<tr><td class='bottom'>&nbsp;</td></tr>");
        sb.append("</table>");

        sb.append("<!-- end highlighted items box -->");

        String html = sb.toString();

        // put this box in the cache

        if (search.isShowOnlyChildren()) {
            return html;
        }

        htmlCache.put(getId(), new CacheContainer(html));
        return html;
    }

    private void renderStatic(RequestContext context, StringBuilder sb, List<Comment> comments) {

        MarkupRenderer r = new MarkupRenderer(context, getMarkup());
        r.setBodyObjects(comments);
        sb.append(r);
    }


    public final boolean useHtmlEditor() {
        return false;
    }

}

