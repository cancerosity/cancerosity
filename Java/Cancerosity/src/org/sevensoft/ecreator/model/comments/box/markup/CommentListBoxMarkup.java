package org.sevensoft.ecreator.model.comments.box.markup;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * User: alchemist
 * Date: 16.11.2011
 */
public class CommentListBoxMarkup implements MarkupDefault {

    public String getBetween() {
        return null;
    }

    public String getBody() {
        StringBuilder sb = new StringBuilder();
        sb.append("<div class=\"commentbox\"><span class=\"comment_name\">[comment_title]</span>\n");
        sb.append("<div class=\"commentboxauth\">[comment_date?prefix=Posted&suffix=GMT] by <span class=\"comment_author\">[comment_author]</span></div>\n");
        sb.append("<div class=\"commentboxcontent\">[comment_content]</div>\n");
        sb.append("</div>");
        return sb.toString();
    }

    public String getEnd() {
        return null;
    }

    public String getStart() {
        return null;
    }

    public int getTds() {
        return 0;
    }

    public String getCss() {
        StringBuilder sb = new StringBuilder();
        sb.append(".comment_name { font-weight: bold; }\n");
        sb.append(".comment_author { font-weight: bold; }");

        return sb.toString();
    }

    public String getName() {
        return "Comments list markup";
    }
}
