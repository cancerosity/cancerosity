package org.sevensoft.ecreator.model.comments;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.ecreator.iface.admin.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.comments.CommentModerationHandler;

import java.util.List;

/**
 * User: Tanya
 * Date: 27.07.2010
 */
@Table("comments")
public class Comment extends EntityObject {

    protected Comment(RequestContext context) {
        super(context);
    }

    public Comment(RequestContext context, Item account) {
        super(context);
        this.account = account;
        this.dateCreated = new DateTime();
        this.awaitingModeration = CommentsSettings.getInstance(context).isModerate();
        save();
    }

    private DateTime dateCreated;
    private Item account;
    private String title;
    private String comment;
    private Category category;
    private Item item;
    /**
     * Item's or categories name
     */
    private String parentName;
    private boolean awaitingModeration;

    private String contentLinked;
    private String contentStripped;

    private boolean autoContentLinking = Seo.getInstance(context).isAutoContentLinking() && Seo.getInstance(context).isAutoContentLinkingOverride();
    private boolean contentLinkingKeyword;


    public DateTime getDateCreated() {
        return dateCreated;
    }

    public Item getAccount() {
        return (Item) (account == null ? null : account.pop());
    }

    public void setAccount(Item account) {
        this.account = account;
    }

    public String getAuthorName() {
        return getAccount() == null ? "Admin" : getAccount().getName();
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
        this.contentStripped = StringHelper.stripNewLines(HtmlHelper.stripHtml(comment, " "));
        setContentLinked(null);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
        this.parentName = category != null ? category.getName() : null;
    }

    public boolean hasCategory() {
        return category != null;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
        this.parentName = item != null ? item.getName() : null;
    }

    public boolean isAwaitingModeration() {
        return awaitingModeration;
    }

    public void setAwaitingModeration(boolean awaitingModeration) {
        this.awaitingModeration = awaitingModeration;
    }

    public String getContentLinked() {
        return contentLinked;
    }

    public void setContentLinked(String contentLinked) {
        this.contentLinked = contentLinked;
        save("contentLinked");
    }

    public String getParentName() {
        return parentName;
    }

     public boolean isAutoContentLinking() {

        if (!Seo.getInstance(context).isAutoContentLinking()) {
            return false;
        }

        if (Seo.getInstance(context).isAutoContentLinkingOverride()) {
            return autoContentLinking;
        }

        return true;
    }

    public void setAutoContentLinking(boolean autoContentLinking) {
        this.autoContentLinking = autoContentLinking;
    }

     public boolean isContentLinkingKeyword() {

        if (!Seo.getInstance(context).isAutoContentLinkingOverride()) {
            return true;
        }

        return contentLinkingKeyword;
    }

    public void setContentLinkingKeyword(boolean contentLinkingKeyword) {
        this.contentLinkingKeyword = contentLinkingKeyword;
    }

    public boolean hasContentLinked() {
        return contentLinked != null;
    }

    public String getContentStripped() {
        if (contentStripped == null && hasComment()) {
            this.contentStripped = StringHelper.stripNewLines(HtmlHelper.stripHtml(getComment(), " "));
            save("contentStripped");
        }
        return contentStripped;
    }

    public String getContentStripped(int maxChars) {
        return getContentStripped(maxChars, null);
    }

    public String getContentStripped(int maxChars, String suffix) {
        return StringHelper.toSnippet(getContentStripped(), maxChars, suffix);
    }

    public boolean hasComment() {
        return comment != null;
    }


    public static List<Comment> get(RequestContext context, boolean awaitingModeration) {
        Query q = new Query(context, "select * from # where awaitingModeration=?");
        q.setTable(Comment.class);
        q.setParameter(awaitingModeration);

        return q.execute(Comment.class);
    }

    public static List<Comment> get(RequestContext context, boolean awaitingModeration, Category category) {
        Query q = new Query(context, "select * from # where awaitingModeration=? and category=?");
        q.setTable(Comment.class);
        q.setParameter(awaitingModeration);
        q.setParameter(category);

        return q.execute(Comment.class);
    }

    public static List<Comment> get(RequestContext context, boolean awaitingModeration, Item item) {
        Query q = new Query(context, "select * from # where awaitingModeration=? and item=?");
        q.setTable(Comment.class);
        q.setParameter(awaitingModeration);
        q.setParameter(item);

        return q.execute(Comment.class);
    }

    public void notification() {
        CommentsSettings commentsSettings = CommentsSettings.getInstance(context);
        if (commentsSettings.hasNotificationEmails()) {
            Config config = Config.getInstance(context);

            Email e = new Email();

//        String body = item.emailTagsParse(item.getItemType().getAcceptListingEmailContent());
//        e.setBody(body);

            StringBuilder sb = new StringBuilder();


            sb.append("\nA new comment has been received. To view it click this URL\n");
            if (isAwaitingModeration()) {
                sb.append(config.getUrl() + "/" + new Link(CommentModerationHandler.class));
            } else {
                if (category != null) {
                    sb.append(config.getUrl() + "/" + new Link(CategoryHandler.class, "comments", "category", category));
                } else {
                    sb.append(config.getUrl() + "/" + new Link(ItemHandler.class, "comments", "item", item));
                }

            }
            sb.append("\n\n\n");


            sb.append("\n\nCOMMENT SUMMARY");
            sb.append("\nReference: " + getId());
            sb.append("\nDate/Time: " + getDateCreated().toString("dd MMM yyyy HH:mm"));

            sb.append("\n\n");

            // ----- customer details -------//
            sb.append("ACCOUNT INFORMATION\n");
            sb.append("---------------------------------------------------------\n");
            sb.append("Name: " + getAccount().getName());
            sb.append("\nEmail: " + getAccount().getEmail());
            sb.append("\n\n");

            e.setBody(sb);

            e.setSubject("Comment acknowledgement #" + getId());
            e.setRecipients(commentsSettings.getCommentNotificationEmails());
            e.setFrom(config.getServerEmail());

            try {

                new EmailDecorator(context, e).send(config.getSmtpHostname());

            } catch (EmailAddressException ex) {
                logger.warning("[Comment] notification " + ex);
                System.out.println(ex);
            } catch (SmtpServerException ex) {
                logger.warning("[Comment] notification " + ex);
                System.out.println(ex);
            }

        }
    }

    public void approved() throws EmailAddressException, SmtpServerException {

        setAwaitingModeration(false);
        save("awaitingModeration");

        emailApproved();
    }

    private void emailApproved() throws EmailAddressException, SmtpServerException {

        Config config = Config.getInstance(context);
        Company company = Company.getInstance(context);

        Email e = new Email();

//        String body = item.emailTagsParse(item.getItemType().getAcceptListingEmailContent());
//        e.setBody(body);

        StringBuilder sb = new StringBuilder();
        sb.append("Hello,\n\nGood news! Your posted comment"
                + " has been approved for inclusion on our site.\n\nYou can view it by clicking this link:\n");
        if (category != null) {
            sb.append(category.getUrl());
        } else {
            sb.append(item.getUrl());
        }
        sb.append("\n\n\n" + company.getName());

        e.setBody(sb);

        e.setSubject("Comment approved");
        e.setTo(getAccount().getEmail());
        e.setFrom(config.getServerEmail());
        new EmailDecorator(context, e).send(config.getSmtpHostname());
    }


    public void rejected() throws EmailAddressException, SmtpServerException {

        delete();

        emailRejected();
    }

    private void emailRejected() throws EmailAddressException, SmtpServerException {

        Config config = Config.getInstance(context);
        Company company = Company.getInstance(context);

        Email e = new Email();

//        String body = item.emailTagsParse(item.getItemType().getRejectListingEmailContent());
//        e.setBody(body);

        StringBuilder sb = new StringBuilder();
        sb.append("Hello,\n\nUnfortunately your recent posting '" + getTitle() + "' has been rejected by our admin team.\nPlease try adding another.");
        sb.append("\n\n\n" + company.getName());

        e.setBody(sb);

        e.setSubject("Comment rejected");
        e.setTo(getAccount().getEmail());
        e.setFrom(config.getServerEmail());
        new EmailDecorator(context, e).send(config.getSmtpHostname());
    }



}
