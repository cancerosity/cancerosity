package org.sevensoft.ecreator.model.comments.markers;

import org.sevensoft.ecreator.model.design.marker.interfaces.ICommentMarker;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.comments.Comment;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.ecreator.model.categories.search.CategorySearcher;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.iface.frontend.search.ItemSearchHandler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.commons.superstrings.StringHelper;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * User: Tanya
 * Date: 29.07.2010
 */
public class CommentContentMarker extends MarkerHelper implements ICommentMarker {


    public Object generate(RequestContext context, Map<String, String> params, Comment comment) {
        String processedContent = comment.getComment();

        if (comment.isAutoContentLinking()) {
            String contentLinked = comment.getContentLinked();
            logger.fine("[ContentBlock] auto content linking is enabled.");

            // if the linked content is null, then we need to create it.
            if (!comment.hasContentLinked()) {
                logger.fine("[ContentBlock] generating linked content");
                contentLinked = makeLinkedContent(context, comment);
            }
            processedContent = contentLinked;
        }

        return super.string(context, params, processedContent);
    }

    public String makeLinkedContent(RequestContext context, Comment comment) {

        if (comment.getComment() == null) {
            return null;
        }

        String contentLinked;

        context.disableCache();

        Map<String, String> keywords = getKeywords(context, comment);

        contentLinked = comment.getComment();// == null ? new byte[0] : content;
        for (String keyword : keywords.keySet()) {

            contentLinked = compileLinkedContent(contentLinked, keywords, keyword);
            comment.setContentLinked(contentLinked);
        }
        return contentLinked;
    }

    private String compileLinkedContent(String contentLinked, Map<String, String> keywords, String keyword) {
        List<String> tags = new ArrayList();
        contentLinked = HtmlHelper.holdTags(contentLinked, tags);

        String url = keywords.get(keyword);
        if (url != null) {
            if (keyword.contains("|")) {
                keyword = keyword.replace("|", "\\|");
            }
            try {
                Pattern pattern = Pattern.compile("\\b(" + keyword.replace("\\s", "(&nbsp;)|\\s") + ")\\b", Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(contentLinked);
                contentLinked = matcher.replaceAll("<a href=\"" + url + "\">$1</a>");
            } catch (Exception e) {
                logger.warning(e.toString());
            }

        }

        contentLinked = HtmlHelper.restoreTags(contentLinked, tags);
        return contentLinked;
    }

    private Map<String, String> getKeywords(RequestContext context, Comment comment) {

        logger.fine("[ContentBlock] getting auto content keywords");

        Seo seo = Seo.getInstance(context);
        Map<String, String> keywords = new HashMap();

        CategorySearcher searcher = new CategorySearcher(context);
//        searcher.setLimit(500);
        searcher.setMaxParents(seo.getAutoContentLinkingCategoryDepth());
        searcher.setVisible(true);

        for (Category category : searcher.getCategories(false)) {

            if (category.isContentLinkingKeyword() && category.getName() != null && category.getName().length() != 0) {

                keywords.put(category.getName(), category.getUrl());

                for (String addKey: category.getContentLinkingKeywords()){
                    keywords.put(addKey, category.getUrl());
                }

                if (seo.isSuggestions()) {
                    addSuggestions(keywords, category.getSuggestions(), category.getUrl());
                }
            }
        }

        Query q = new Query(context, "select i.name, i.friendlyUrl from # i join items_types it on it.id=i.itemtype where i.status='LIVE' and it.hidden=0 ");
        q.setTable(Item.class);
        Map map = q.getMap(String.class, String.class, new HashMap());
        Map<String, String> itemsMap = new HashMap();
        for (Object key : map.keySet()) {
            if (((String) key).length() != 0) {
                itemsMap.put((String) key, Item.getUrl(context, (String) map.get(key), (String) key));
            }
        }
        keywords.putAll(itemsMap);

        //			if (seo.isSuggestions()) {
        //				addSuggestions(keywords, item.getSuggestions(), item.getRelativeUrl());
        //			}
        //		}

        if (comment.getCategory() != null) {

            keywords.remove(comment.getCategory().getName());

        } else if (comment.getItem() != null) {

            keywords.remove(comment.getItem().getName());

        }

        keywords = StringHelper.stripIllegals(keywords);

        logger.fine("[ContentBlock] keywords=" + keywords);

        return keywords;
    }

    private void addSuggestions(Map<String, String> keywords, List<String> suggestions, String url) {

        for (String suggestion : suggestions) {

            suggestion = suggestion.trim();
            if (suggestion.length() > 3) {

                if (keywords.containsKey(suggestion)) {

                    keywords.put(suggestion, new Link(ItemSearchHandler.class, null, "keywords", suggestion).toString());

                } else {

                    keywords.put(suggestion, url);

                }
            }
        }
    }


    public Object getRegex() {
        return "comment_content";
    }
}
