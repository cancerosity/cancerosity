package org.sevensoft.ecreator.model.comments.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.ICategoryMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.comments.panels.CommentAddPanel;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * User: Tanya
 * Date: 07.09.2012
 */
public class CommentAddMarker extends MarkerHelper implements IItemMarker, ICategoryMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
        return generate(context, params, item.getItemType(), item);
    }

    public Object generate(RequestContext context, Map<String, String> params, Category category) {
        return generate(context, params, (BlockOwner) category, null);
    }

    public Object generate(RequestContext context, Map<String, String> params, BlockOwner owner, Item blockOwnerItem) {

        if (!Module.Comments.enabled(context)) {
            return null;
        }

        Panel panel = new CommentAddPanel(context, owner);

        ((CommentAddPanel) panel).setAccount((Item) context.getAttribute("account"));
        ((CommentAddPanel) panel).setBlockOwnerItem(blockOwnerItem);

        return super.string(context, params, panel);

    }

    public Object getRegex() {
        return "comments_add";
    }
}
