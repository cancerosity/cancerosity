package org.sevensoft.ecreator.model.comments.renderers;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * User: Tanya
 * Date: 30.07.2010
 */

public class CommentAddMarkupDefault implements MarkupDefault {

    public String getBetween() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getBody() {
        StringBuilder sb = new StringBuilder();

        sb.append("<p class='comment_section_heading'>Add a comment</p>\n");
        sb.append("<fieldset class='ec_form'>\n");
        sb.append("<div class='field_input'><div class='label'>Title*</div><div class='input'>\n");
        sb.append("<input type='text' class='text comment_title' name='title' /></div></div>\n");
        sb.append("\n");
        sb.append("<div class='field_input'><div class='label'>Post*</div><div class='input'>\n");
        sb.append("<textarea cols='75' rows='10' class='comment_body' name='post'> </textarea>\n");
        sb.append("<div class='ec_form_commands'><input value='Post Comment' type='submit' /></div>\n");
        sb.append("</div></div>\n");
        sb.append("</fieldset>\n");
//        sb.append("</form>\n");

        return sb.toString();
    }

    public String getEnd() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getStart() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getTds() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getCss() {
        StringBuilder sb = new StringBuilder();

        sb.append("p.comment_section_heading { color: #675245; padding: 25px 10px 0 0; font-size: 13px; font-weight: bold; }\n");
        sb.append("div.addcommentform { margin: 5px 15px; }\n");
        sb.append("div.addcommentform fieldset.ec_form { background: #eee; padding: 20px; border: solid 1px #d5d5d5; }\n");
        sb.append("div.addcommentform fieldset.ec_form legend { color: #675245; }\n");
        sb.append("input.comment_title { width: 600px; }\n");
        sb.append("textarea.comment_body { width: 600px; }\n");
        sb.append("\n");
        sb.append(".postcommenttd { margin-bottom: 15px; text-transform: capitalize; font-family:Georgia, 'Times New Roman', serif; }\n");
        sb.append(".postcomment { text-transform: capitalize; font-family: Georgia, 'Times New Roman', serif; font-size: 14pt; letter-spacing: 0.1em; padding-bottom: 5px; color: #675245;  }\n");
        sb.append(".ec_form_commands { text-transform: uppercase; font-size: 19px; color: #333333; margin-bottom: 6px; }\n");
        sb.append(".commentcontent { font-size: 12px; padding-bottom: 15px; }\n");
        sb.append(".postcommenttop {  font-family: verdana; font-size: 14pt; padding-bottom: 5px; color: #675245; }\n");

        return sb.toString();
    }

    public String getName() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
