package org.sevensoft.ecreator.model.comments.renderers;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * User: tanya
 * Date: 28.07.2010
 */
public class CommentListMarkupDefault implements MarkupDefault {

    public String getBetween() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getBody() {
        StringBuilder sb = new StringBuilder();

        sb.append("<div class='commentblock'><div class='commentname'>[comment_title]</div>\n");
        sb.append("<div class='commentauth'>[comment_date?prefix=Posted&suffix=GMT] by <strong>[comment_author]</strong></div>\n");
        sb.append("<div class='commentcontent'>[comment_content]</div></div>\n");

        return sb.toString();
    }

    public String getEnd() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getStart() {
        StringBuilder sb = new StringBuilder();
//        sb.append("        <table>                                                                                                 ");
//          sb.append("  <tr>                                                                                                        ");
//          sb.append("      <td colspan='2'> Comments</td>                                                                          ");
//          sb.append("  </tr>   ");
//           sb.append("</table>                                                                                                        ");

        return sb.toString();
    }

    public int getTds() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getCss() {
        StringBuilder sb = new StringBuilder();

        sb.append(".commentblock { background: #eee; margin-bottom: 15px; margin: 15px 10px 0 10px; border: solid 1px #d5d5d5; }\n");
        sb.append(".commentname { color: #675245; padding: 10px 15px; xfont-family: Georgia, 'Times New Roman', serif; font-size: 12px; text-transform: capitalize; font-weight: bold; }\n");
        sb.append(".commenttitle { background: #675245; color: #fff; margin: 25px 0 15px 0; padding: 7px 10px; border-bottom: solid 1px #a29289; font-size: 13px; font-weight: bold; }\n");
        sb.append(".commentauth { color: #444; margin-bottom: 6px; padding: 0 15px; text-transform: uppercase; font-size: 9px; }\n");
        sb.append(".commentcontent { padding-bottom: 15px; padding: 0 15px 15px 15px; font-size: 11px; }\n");

        return sb.toString();
    }

    public String getName() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
