package org.sevensoft.ecreator.model.comments.markers;

import org.sevensoft.ecreator.model.design.marker.interfaces.ICommentMarker;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.comments.Comment;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * User: Tanya
 * Date: 29.07.2010
 */
public class CommentTitleMarker extends MarkerHelper implements ICommentMarker {

    public Object generate(RequestContext context, Map<String, String> params, Comment comment) {
        return super.string(context, params, comment.getTitle());
    }

    public Object getRegex() {
        return "comment_title";
    }
}
