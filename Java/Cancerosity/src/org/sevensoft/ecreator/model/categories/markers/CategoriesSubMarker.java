package org.sevensoft.ecreator.model.categories.markers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 31-Mar-2006 10:46:58
 *
 */
public class CategoriesSubMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		Category current = (Category) context.getAttribute("category");
		if (current == null) {
			return null;
		}

		if (current.isLeaf()) {
			return null;
		}

		String separator = params.get("sep");
		String exclude = params.get("exclude");
		String tag = params.get("tag");
		String valign = params.get("valign");
		String align = params.get("align");
		String clazz = params.get("class");

		if (tag == null) {
			tag = "span";
		}

		if (clazz == null) {
			clazz = "cat_link";
		}

		List<String> exclusions = new ArrayList();
		if (exclude != null) {
			for (String string : exclude.split("\\|")) {
				exclusions.add(string.toLowerCase());
			}
		}

		StringBuilder sb = new StringBuilder();

		// a class name just for this tab
		String tabclass;
		int n = 0;
		for (Category child : current.getChildren()) {

			if (child.isVisible() && !exclusions.contains(child.getName().toLowerCase())) {

				tabclass = clazz;

				if (n > 0 && separator != null) {
					sb.append(separator);
				}

				sb.append("<");
				sb.append(tag);

				sb.append(" class='");
				sb.append(tabclass);
				sb.append("'");

				sb.append(" id='category_primary_");
				sb.append(child.getIdString());
				sb.append("'");

				if (align != null) {
					sb.append(" align='");
					sb.append(align);
					sb.append("'");
				}

				if (valign != null) {
					sb.append(" valign='");
					sb.append(valign);
					sb.append("'");
				}

//				sb.append(" onmouseover='this.className=\"" + tabclass + "_hover\"' onmouseout='this.className=\"" + tabclass + "\"'");

				sb.append(">");

				sb.append(new LinkTag(child.getUrl(), child.getName()));
				sb.append("</" + tag + ">");

				n++;
			}

		}

		return sb;
	}

	public Object getRegex() {
		return "categories_sub";
	}

}
