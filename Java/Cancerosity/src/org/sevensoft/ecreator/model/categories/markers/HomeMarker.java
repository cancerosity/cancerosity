package org.sevensoft.ecreator.model.categories.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 03-Apr-2006 11:17:56
 *
 */
public class HomeMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {
		return super.link(context, params, ".", "link_home");
	}

	public String getDescription() {
		return "Creates a link to the home page";
	}

	public Object getRegex() {
		return "home";
	}

}