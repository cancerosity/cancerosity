package org.sevensoft.ecreator.model.categories.box;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategoryModel;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TR;
import org.sevensoft.jeezy.http.html.table.TableCell;

/**
 * @author sks 3 May 2007 12:50:42
 *
 */
public class CategoryBoxRenderer {

	private final RequestContext	context;
	private final Category		currentCategory;
	private final CategoryModel	model;
	private final CategoryBox	box;

	public CategoryBoxRenderer(RequestContext context, CategoryBox categoryBox, Category currentCategory, CategoryModel model) {
		this.context = context;
		this.box = categoryBox;
		this.currentCategory = currentCategory;
		this.model = model;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		/*
		 * Javascript functions required for certain styles
		 */
		switch (box.getStyle()) {

		default:
			break;

		case Floating:

			sb.append("<script language='JavaScript' type='text/javascript'>\n");
			sb.append("	var activeMenu;\n");
			sb.append("	function showSubMenu(menuId, submenuId) {\n");
			sb.append("		if (activeMenu != null)\n");
			sb.append("			activeMenu.style.display = 'none';\n");
			sb.append("		activeMenu = document.getElementById(submenuId);\n");
			sb.append("		var menu = document.getElementById(menuId);\n");
			sb.append("		if (menu) {\n");
			sb.append("			var curleft = 0; var curtop = 0;");
			sb.append("			if (menu.offsetParent) {");
			sb.append("				curleft = menu.offsetWidth + menu.offsetLeft;");
			sb.append("				curtop = menu.offsetTop;");
			sb.append("				while (menu = menu.offsetParent) {");
			sb.append("					curleft += menu.offsetLeft;");
			sb.append("					curtop += menu.offsetTop;");
			sb.append("				}");
			sb.append("			}");
			sb.append("			activeMenu.style.left = curleft + 'px';\n");
			sb.append("			activeMenu.style.top = curtop + 'px';\n");
			sb.append("		}\n");
			sb.append("		activeMenu.style.display = 'block';\n");
			sb.append("	}\n");
			sb.append("	function hideSubMenu() {\n");
			sb.append("		activeMenu.style.display = 'none';\n");
			sb.append("	}\n");
			sb.append("</script>\n");

			// now generate all sub category boxes
			for (Category category : model.getCategories(box.getDepth())) {

				if (category.hasChildren()) {

					sb.append("<table cellspacing='0' cellpadding='0' class='ec_floatingmenus' id=\"ec_floatingtable_" + box.getId() + "_"
							+ category.getId() + "\" onmouseover=\"showSubMenu('ec_boxtr_" + box.getId() + "_" + category.getId()
							+ "', 'ec_floatingtable_" + box.getId() + "_" + category.getId()
							+ "');\" onmouseout=\"hideSubMenu();\" style=\"display: none; position: absolute; z-index: 5;\">");

					for (Category child : category.getChildren()) {
						sb.append("<tr><td>" + new LinkTag(child.getUrl(), child.getName()) + "</td></tr>");
					}

					sb.append("</table>");

				}
			}

			break;

		case Expanding:

			sb.append("<script language='JavaScript' type='text/javascript'>\n");
			sb.append("function menu(id) {");
			sb.append("		var elements = document.getElementsByName('catboxrow' + id); ");
			sb.append("		for (var n=0; n < elements.length; n++) {");
			sb.append("			var e = elements[n]; ");
			sb.append("			if (e.style.display == 'none') e.style.display = ''; ");//=='hiddenrow') e.className=''; ");
			sb.append("			else e.style.display = 'none'; "); // className='hiddenrow'; ");
			sb.append("		}");
			sb.append("return false;");
			sb.append(" }");
			sb.append("</script>");

			break;
		}

		// -- START OF BOX TABLE -- //

		sb.append(box.getTableTag());

		for (Map.Entry<Category, Integer> entry : model.getMap().entrySet()) {

			TR row = new TR();

			final Integer level = entry.getValue();
			final Category category = entry.getKey();
			final Category parent = category.getParent();

			LinkTag linkTag = null;
			boolean active = false;

			switch (box.getStyle()) {

			default:

				/*
				 * Dynamic style shows the category if it is level 1 or on the current path
				 */
			case Dynamic:

				List<Category> trail = Collections.emptyList();
				if (currentCategory != null) {
					trail = currentCategory.getTrail();
				}

				if (level == 1 || trail.contains(parent)) {
					linkTag = category.getLinkTag();
				}

				break;

			case Floating:

				if (level == box.getDepth() && category.hasChildren()) {

					String rowId = "ec_boxtr_" + box.getId() + "_" + category.getId();
					String menuId = "ec_floatingtable_" + box.getId() + "_" + category.getId();

					row.setId(rowId);
					linkTag = new LinkTag("#", category.getName());

					row.setOnMouseOver("javascript:showSubMenu('" + rowId + "', '" + menuId + "');");
					row.setOnMouseOut("javascript:hideSubMenu();");
                    linkTag.setOnClick("return false;");

				} else {

					linkTag = new LinkTag(category.getUrl(), category.getName());
				}

				break;

			case Static:

				/*
				 * For static just display the category regardless, check for primary to set right style
				 */
				linkTag = category.getLinkTag();
				if (category.equals(currentCategory)) {
					active = true;
				}
				break;

			case Expanding:

				/*
				 * We only need to hide and use javascript tags on non level 1 categories
				 * as all level 1 cats will always be shown.
				 */
				if (level > 1) {

					row.setName("catboxrow" + parent.getIdString());
					row.setId("catboxrow" + parent.getIdString());

					/*
					 * Initially all non l1 cats will be closed
					 */
					row.setStyle("display: none; ");
				}

				/*
				 * If the category is a leaf or is equal to the depth then it should be a proper link otherwise a javascript function to
				 * show subcategores
				 */
				if (level == box.getDepth() || category.isLeaf() || category.isRoot()) {

					linkTag = category.getLinkTag();

				} else {

					linkTag = new LinkTag("#", category.getName());
					linkTag.setOnClick("javascript:menu('" + category.getId() + "'); return false;");

				}

				break;
			}

			if (linkTag != null) {

				sb.append(row);

                if (category.equals(currentCategory)) {
                    active = true;
                }
                
                TableCell cell = getTableCell(level, linkTag, active);
                cell.setId("c" + category.getId());
                sb.append(cell);

				sb.append("</td></tr>");

			}

		}

		sb.append("<tr><td class='bottom'></td></tr>");
		sb.append("</table>");

		return sb.toString();

	}

	private TableCell getTableCell(int level, LinkTag tag, boolean active) {

		TableCell cell = new TableCell();
		if (active) {

			cell.setClass("l" + level + "_active");
            cell.setOnMouseOver("this.className='l" + level + "_over_active'");
			cell.setOnMouseOut("this.className='l" + level + "_active'");

		} else {

			cell.setClass("l" + level);
			cell.setOnMouseOver("this.className='l" + level + "_over'");
			cell.setOnMouseOut("this.className='l" + level + "'");

		}

		cell.setContent(tag);
		return cell;
	}

}
