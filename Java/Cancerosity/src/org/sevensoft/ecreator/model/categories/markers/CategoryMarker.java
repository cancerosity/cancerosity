package org.sevensoft.ecreator.model.categories.markers;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.ICategoryMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author sks 3 Aug 2006 16:29:56
 */
public class CategoryMarker extends MarkerHelper implements IItemMarker, IGenericMarker, ICategoryMarker {

    public Object generate(RequestContext context, Map<String, String> params) {
        return generate(context, params, null);
    }

    public Object generate(RequestContext context, Map<String, String> params, Category category) {

        logger.fine("[CategoryMarker] params=" + params);

        Item account = (Item) context.getAttribute("account");
        if (account == null && params.containsKey("accounts")) {
            return null;
        }

        String id = params.get("id");
        if (id != null) {
            category = EntityObject.getInstance(context, Category.class, id);
        }

        String name = params.get("name");
        if (name != null) {
            category = Category.getByName(context, name);
        }

        logger.fine("[CategoryMarker] category=" + category);

        if (category == null) {
            return null;
        }

        boolean dropdown = params.containsKey("dropdown");
        String prefix = params.get("prefix");
        String suffix = params.get("suffix");
        boolean link = params.containsKey("link");
        String text = params.get("text");
        String label = params.get("label");
        boolean active = params.containsKey("active");
        String tag = params.get("tag");
        String clazz = params.get("class");
        String align = params.get("align");

        if (tag == null) {
            tag = "span";
        }

        List<String> onMouseOvers = new ArrayList();
        List<String> onMouseOuts = new ArrayList();

        if (dropdown) {

            if (category.hasChildren()) {
                onMouseOvers.add("hideSecondLevelSubMenu();");
                onMouseOvers.add("showSubMenu('category_primary_" + category.getIdString() + "', 'ec_submenu_" + category.getId() + "')");
                onMouseOuts.add("hideSubMenu();");
            }
        }

        String srchover = params.get("srchover");
        String src = params.get("src");
        if (src != null) {

            ImageTag imageTag = new ImageTag(src);
            if (srchover != null) {
                onMouseOvers.add("this.src='" + srchover + "'");
                onMouseOuts.add("this.src='" + src + "'");
            }

            if (onMouseOvers.size() > 0) {
                imageTag.setOnMouseOver(StringHelper.implode(onMouseOvers, "; "));
            }

            if (onMouseOuts.size() > 0) {
                imageTag.setOnMouseOut(StringHelper.implode(onMouseOuts, "; "));
            }

            LinkTag linkTag = new LinkTag(category.getUrl(), imageTag);

            if (clazz != null) {
                linkTag.setClass(clazz);
            }

            if (align != null) {
                linkTag.setAlign(align);
            }

            if (id != null) {
                linkTag.setId(id);
            }

            return linkTag;
        }

        if (label != null) {
            return new ButtonTag(category.getUrl(), label);
        }

        if (text == null) {
            text = category.getName();
            if (text == null) {
                return null;
                // throw new RuntimeException("Category name is null: " + category);
            }
            text = text.replace("\"", "&quot;");
        }

        StringBuilder sb = new StringBuilder();
        sb.append("<" + tag);

        if (link) {

            if (clazz != null) {

                sb.append(" class=\"" + clazz + "\" ");

            } else {

                if (active && category.equals(context.getAttribute("category"))) {

                    sb.append(" class=\"category_active\" ");

                    onMouseOvers.add("this.className='category_active_hover'");
                    onMouseOuts.add("this.className='category_active'");

                } else {

                    sb.append(" class=\"category\" ");

                    onMouseOvers.add("this.className='category_hover'");
                    onMouseOuts.add("this.className='category'");
                }
            }
        }

        if (onMouseOvers.size() > 0) {
            sb.append(" onmouseover=\"" + StringHelper.implode(onMouseOvers, "; ") + "\"");
        }

        if (onMouseOuts.size() > 0) {
            sb.append(" onmouseout=\"" + StringHelper.implode(onMouseOuts, "; ") + "\"");
        }

        sb.append(">");

        if (prefix != null) {
            sb.append(prefix);
        }

        if (link) {
            sb.append(new LinkTag(category.getUrl(), text));
        } else {
            sb.append(text);
        }

        if (suffix != null) {
            sb.append(suffix);
        }

        sb.append("</" + tag + ">");
        return sb.toString();
    }

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
        return generate(context, params, item.getCategory());
    }

    public Object getRegex() {
        return "category";
    }

}
