package org.sevensoft.ecreator.model.categories.renderers;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * @author sks 25 Oct 2006 15:13:11
 *
 */
public class CategorySearchMarkupDefault implements MarkupDefault {

	public String getBetween() {
		return null;
	}

	public String getCss() {
		return null;
	}

	public String getBody() {
		return "[category?class=cs_name]\n[summary?limit=200&class=cs_summary]\n[category?url=1&class=cs_url]";
	}

	public String getEnd() {
		return null;
	}

	public String getName() {
		return null;
	}

	public String getStart() {

		StringBuilder sb = new StringBuilder();

		sb.append("<style>");
		sb.append("span.cs_name { display: block; margin-top: 10px; } ");
		sb.append("span.cs_name a { font-weight: normal; font-family: Arial; font-size: 16px; color: #0000cc; text-decoration: underline; } ");
		sb.append("span.cs_name a:hover { text-decoration: underline; } ");
		sb.append("span.cs_summary { display: block; margin: 1px 0; font-family: Arial; font-size: 13px; color: black; } ");
		sb.append("span.cs_url { display: block;  font-family: Arial; font-size: 13px; color: #008000; } ");
		sb.append("</style>");

		return sb.toString();
	}

	public int getTds() {
		return 0;
	}

}
