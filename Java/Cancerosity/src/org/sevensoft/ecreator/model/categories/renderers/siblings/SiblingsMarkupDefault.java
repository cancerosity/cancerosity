package org.sevensoft.ecreator.model.categories.renderers.siblings;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * @author sks 15 Aug 2006 09:28:47
 *
 */
public class SiblingsMarkupDefault implements MarkupDefault {

	public String getBetween() {
		return null;
	}

	public String getCss() {
		return null;
	}

	public String getBody() {
		return "<li><span class='subcat' align='left'> [category?link=1] [summary]</span></li>";
	}

	public String getEnd() {
		return null;
	}

	public String getName() {
		return "Default siblings markup";
	}

	public String getStart() {
		return "<style>span.subcat { font-weight: bold; } </style>";

	}

	public int getTds() {
		return 0;
	}

}
