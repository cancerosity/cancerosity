package org.sevensoft.ecreator.model.categories;

import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 4 Apr 2007 12:52:45
 *
 */
public enum CategoryModule {

	/**
	 * 
	 */
	Videos,

	/**
	 * 
	 */
	Images,

	/**
	 * 
	 */
	Attachments,

	/**
	 * 
	 */
	Headers, 
	
	/**
	 * 
	 */
	Forwards, 
	
	/**
	 * 
	 */
	Summaries;

	public boolean enabled(RequestContext context) {
		return CategorySettings.getInstance(context).getModules().contains(this);
	}

	public String getDescription() {
		return null;
	}
}
