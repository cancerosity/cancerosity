package org.sevensoft.ecreator.model.categories.markers;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import java.util.Map;

/**
 * @author sks 23 Nov 2006 10:32:00
 */
public class CategoriesDropDownMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		// boolean horizontal = params.containsKey("horizontal") || params.containsKey("horiz");

		// String left = params.get("left");
		// String top = params.get("top");
		// String right = params.get("right");
		// String bottom = params.get("bottom");
        Category currentCategory = (Category) context.getAttribute("category");
        Integer categoryDeep = 10;  // if empty than set more than 3 
        String categoryDeepStr =  params.get("dropdown");
        if (categoryDeepStr!=null){
            categoryDeep = Integer.parseInt(categoryDeepStr);
        }

		StringBuilder sb = new StringBuilder();
//		StringBuilder secondLevelCategoriesTables = new StringBuilder();

		sb.append("<ul class='ec_dropdown'>");

		Category root = Category.getRoot(context);

		for (Category category : root.getVisibleChildren()) {
            if (category.equals(currentCategory)) {
                sb.append("<li class='active' id='category_primary_" + category.getId() + "'>");
            } else {
                sb.append("<li id='category_primary_" + category.getId() + "'>");
            }
			sb.append(new LinkTag(category.getUrl(), category.getName()) + "\n");
            if (categoryDeep > 1) {

			sb.append("<ul class='sub_menu'>");

                if (category.hasChildren()) {

                    for (Category child : category.getVisibleChildren()) {


                        sb.append("<li>");
                        sb.append(new LinkTag(child.getUrl(), child.getName()) + "\n");
                        if (categoryDeep > 2) {
                            boolean hasVisibleChildren = !child.getVisibleChildren().isEmpty();

                            if (hasVisibleChildren) {
                                sb.append("<ul>");
                                for (Category secondChild : child.getVisibleChildren()) {
                                    sb.append("<li>");
                                    sb.append(new LinkTag(secondChild.getUrl(), secondChild.getName()));
                                    sb.append("</li>");
                                }
                                sb.append("</ul>");
                            }
                        }

                        sb.append("</li>");
                    }
                }
			sb.append("</ul>");
            }
		}

		sb.append("</ul>");
		return sb.toString();
	}

	public Object getRegex() {
		return "categories_dropdown";
	}
}
