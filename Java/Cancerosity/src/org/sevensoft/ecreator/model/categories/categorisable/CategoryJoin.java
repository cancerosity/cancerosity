package org.sevensoft.ecreator.model.categories.categorisable;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 30 Nov 2006 11:00:24
 *
 */
@Table("categories_joins")
public class CategoryJoin extends EntityObject {

	private String	ownerId;
	private Category	category;

	protected CategoryJoin(RequestContext context) {
		super(context);
	}

	public CategoryJoin(RequestContext context, Categories owner, Category category) {
		super(context);
		
		if (category != null) {

			this.ownerId = owner.getFullId();
			this.category = category;
		}
		save();
	}
}
