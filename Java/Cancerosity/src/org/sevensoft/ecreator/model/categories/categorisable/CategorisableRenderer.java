package org.sevensoft.ecreator.model.categories.categorisable;

import java.util.Iterator;
import java.util.Map;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.admin.categories.CategoryOwnerHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategorySettings;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

/**
 * @author sks 2 May 2006 15:56:30
 *
 */
public class CategorisableRenderer {

	private final Categories		c;
	private final RequestContext		context;
	private final CategorySettings	categorySettings;
	private final int				page;

	public CategorisableRenderer(RequestContext context, Categories c, int page) {
		this.context = context;
		this.c = c;
		this.page = page;
		this.categorySettings = CategorySettings.getInstance(context);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		SelectTag tag;
		int n = 1;
		for (Category category : c.getCategories()) {

			StringBuilder sb2 = new StringBuilder();

			if (category.isRoot()) {

				sb2.append(new LinkTag(CategoryHandler.class, "edit", category.getName(), "category", category));

			} else {

				Iterator<Category> iter = category.getTrail().iterator();
				while (iter.hasNext()) {

					Category trail = iter.next();

					sb2.append(new LinkTag(CategoryHandler.class, "edit", trail.getName(), "category", trail));

					if (iter.hasNext())
						sb2.append(" > ");
				}
			}

			sb2.append(" ");
			sb2.append(new ButtonTag(CategoryOwnerHandler.class, "removeCategory", "Remove", StringHelper.toCamelCase(c.getClass()), c, "category",
					category));
			sb2.append("</td></tr>");

			sb.append(new AdminRow("Category " + n, null, sb2));
			n++;
		}

		int count = Category.getCount(context);
		final Results results = new Results(count, page, 500);
		final Map<String, String> categoryOptions = Category.getCategoryOptions(context, null, results.getStartIndex(), 500);

		Link link = new Link(ItemHandler.class, "edit", "item", c);

		tag = new SelectTag(context, "addCategory");
		tag.setAny("-Add to a new category-");
		tag.addOptions(categoryOptions);

		StringBuilder sb2 = new StringBuilder();
		if (results.hasMultiplePages()) {
			sb2.append(new ResultsControl(context, results, link));
		}

		sb2.append(tag);

		sb.append(new AdminRow("Category " + n, "Choose a category to add", sb2));

		return sb.toString();
	}
}
