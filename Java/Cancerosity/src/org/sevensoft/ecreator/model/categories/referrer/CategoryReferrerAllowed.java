package org.sevensoft.ecreator.model.categories.referrer;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.categories.Category;

/**
 * User: Tanya
 * Date: 13.01.2012
 */
@Table("categories_referrers")
public class CategoryReferrerAllowed extends EntityObject {

    private Category category;
    private String sessionId;

    protected CategoryReferrerAllowed(RequestContext context) {
        super(context);
    }

    public CategoryReferrerAllowed(RequestContext context, Category category) {
        super(context);
        this.category = category;
        this.sessionId = context.getSessionId();
        save();
    }

    public static boolean isAllowed(RequestContext context, Category category) {
        return SimpleQuery.exists(context, CategoryReferrerAllowed.class, "category", category, "sessionId", context.getSessionId());
    }

}
