package org.sevensoft.ecreator.model.categories.db;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 03-Sep-2005 10:51:36
 * 
 */
@Table("categories_ancestors")
public class CategoryAncestor extends EntityObject {

	@Index()
	private int	category;

	@Index()
	private int	ancestor;

	public CategoryAncestor(RequestContext context) {
		super(context);
	}

	public CategoryAncestor(RequestContext context, Category c, Category a) {
		super(context);
		this.category = c.getId();
		this.ancestor = a.getId();
		save();
	}
}
