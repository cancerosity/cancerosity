package org.sevensoft.ecreator.model.categories.categorisable;

import java.util.List;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 30 Nov 2006 11:02:02
 *
 */
public class CategoryUtil {

	public static List<Category> getCategories(RequestContext context, Categories owner) {
		Query q = new Query(context, "select c.*from # c join # cj on c.id=cj.category where cj.ownerId=? order by c.name");
		q.setTable(Category.class);
		q.setTable(CategoryJoin.class);
		q.setParameter(owner.getFullId());
		return q.execute(Category.class);
	}

	public static Category getCategory(Categories owner) {

		List<Category> categories = owner.getCategories();
		return categories.isEmpty() ? null : categories.get(0);
	}

	public static boolean hasCategory(RequestContext context, Categories owner) {
		return SimpleQuery.count(context, CategoryJoin.class, "ownerId", owner.getFullId()) > 0;
	}

	public static void removeCategories(RequestContext context, Categories owner) {
		SimpleQuery.delete(context, CategoryJoin.class, "ownerId", owner.getFullId());
	}

	public static void removeCategory(RequestContext context, Categories owner, Category category) {
		SimpleQuery.delete(context, CategoryJoin.class, "ownerId", owner.getFullId(), "category", category);
	}

	public static void setCategories(Categories owner, List<Category> categories) {
		owner.removeCategories();
		owner.addCategories(categories);
	}

	public static void setCategory(Categories owner, Category category) {
		owner.removeCategories();
		owner.addCategory(category);
	}
}
