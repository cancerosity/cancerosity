package org.sevensoft.ecreator.model.categories.blocks;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.iface.admin.categories.blocks.ItemsBlockHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.panels.OrderingForm;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategorySettings;
import org.sevensoft.ecreator.model.categories.CategoryItem;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.sorts.FrontSortRenderer;
import org.sevensoft.ecreator.model.items.sorts.ItemSort;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.ecreator.model.media.images.ImageUtil;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

/**
 * @author sks 25 Jul 2006 14:46:29
 *         <p/>
 *         This class is a place holder for all items in a category
 */
@Table("blocks_items")
@HandlerClass(ItemsBlockHandler.class)
@Label("Items")
public class ItemsBlock extends Block {

    public static MultiValueMap<Integer, List<Integer>>	randomUsedItemsCache;

	static {
        randomUsedItemsCache = new MultiValueMap<Integer, List<Integer>>();
	}

    public static void clearHtmlCache(Integer key) {
		randomUsedItemsCache.get(key).clear();
	}
    /**
     * Allow all items of this block to be bought @ once
     */
    private boolean multiBuy;

    private String noItemsMessage;

    private boolean includeSubcategoryItems;

    private boolean resultsTop;

    private boolean resultsBottom;

    /**
     * Default sort
     */
    private ItemSort sort;

    /**
     * Set how many items per page for this block
     */
    private int itemsPerPage;

    private boolean showSorts;

    private Markup listMarkup;

    private SortType sortType;

    private boolean excludeFeatured = CategorySettings.getInstance(context).isExcludeFeatured();

    protected ItemsBlock(RequestContext context) {
        super(context);
    }

    public ItemsBlock(RequestContext context, BlockOwner owner, int objId) {
        super(context, owner, 0);

        save();
    }

    public final int getItemsPerPage() {
        return itemsPerPage;
    }

    public final Markup getListMarkup() {
        return (Markup) (listMarkup == null ? null : listMarkup.pop());
    }

    public String getNoItemsMessage() {
        return noItemsMessage == null ? "No results found" : noItemsMessage;
    }

    public final ItemSort getSort() {
        return (ItemSort) (sort == null ? null : sort.pop());
    }

    public final SortType getSortType() {
        return sortType;
    }

    public final boolean isIncludeSubcategoryItems() {
        return includeSubcategoryItems;
    }

    public boolean isMultiBuy() {
        return multiBuy;
    }

    public boolean isResultsBottom() {
        CategorySettings cs = CategorySettings.getInstance(context);
        if (cs.isResultsOverride()) {
            return resultsBottom;
        } else {
            return cs.isResultsBottom();
        }
    }

    public boolean isResultsTop() {

        CategorySettings cs = CategorySettings.getInstance(context);
        if (cs.isResultsOverride()) {
            return resultsTop;
        } else {
            return cs.isResultsTop();
        }
    }

    public boolean isShowSorts() {

        CategorySettings cs = CategorySettings.getInstance(context);
        if (cs.isOverrideSorts()) {
            return showSorts;
        } else {
            return cs.isShowSorts();
        }
    }

    @Override
    public boolean isSimple() {
        return false;
    }

    public boolean isExcludeFeatured() {
        return excludeFeatured;
    }

    @Override
    public Object render(final RequestContext context) {

        CategorySettings categorySettings = CategorySettings.getInstance(context);
        Category category = getOwnerCategory();
        context.setAttribute("trailCategory", category);
        final Item account = (Item) context.getAttribute("account");
        int page = (Integer) context.getAttribute("page");
        Link link = (Link) context.getAttribute("link");
        ItemSort parameterSort = (ItemSort) context.getAttribute("sort");

        /*
           * If we are in category and it does not have live items nor a search wrapper then we should not show anything
           * (no 'no results message' etc as it will just be a content category
           */


        ItemSearcher searcher;

        if (category.isSearchWrapper()) {

            logger.fine("[ItemsBlock] category has search wrapper");

            searcher = category.getSearch().getSearcher();
            searcher.setAccount(account);
            searcher.setStatus("Live");

            logger.fine("[ItemsBlock] wrapper sort=" + searcher.getSortType());

            if (searcher.getSortType() == null) {

                ItemSort sort = categorySettings.getItemSort();
                if (sort != null) {

                    searcher.setSort(sort);

                    logger.fine("[ItemsBlock] using sort from category settings=" + sort);
                }

            }

        } else if (isIncludeSubcategoryItems() || category.hasLiveItems()) {

            logger.fine("[ItemsBlock] getting items direct from category");

            searcher = new ItemSearcher(context);
            searcher.setSearchCategory(category);
            searcher.setStatus("Live");
            searcher.setSubcats(isIncludeSubcategoryItems());
            searcher.prioritise();
            searcher.setAccount(account);

            // if a sort is present as a parameter that should take priority
            boolean sorted = false;
            if (parameterSort != null) {

                searcher.setSort(parameterSort);
                sorted = true;

                logger.fine("[ItemsBlock] using sort param=" + parameterSort);
            }

            // otherwise check for an item sort from the block
            if (!sorted) {

                ItemSort sort = getSort();
                if (sort != null) {

                    searcher.setSort(sort);
                    sorted = true;

                    logger.fine("[ItemsBlock] using sort from block=" + sort);
                }
            }

            // otherwise check for a generic sort type in the block
            if (!sorted) {

                if (sortType != null) {

                    searcher.setSortType(sortType);
                    sorted = true;

                    logger.fine("[ItemsBlock] using sortType from block=" + sortType);
                }
            }

            // then check for a default sort in category settings
            if (!sorted) {

                ItemSort sort = categorySettings.getItemSort();
                if (sort != null) {

                    searcher.setSort(sort);
                    sorted = true;

                    logger.fine("[ItemsBlock] using sort from category settings=" + sort);
                }
            }

            // finally just sort by name
            if (!sorted) {
                searcher.setSortType(SortType.Name);
                sorted = true;
            }

            if (Module.ListingsFeaturedExclude.enabled(context)) {
                if (categorySettings.isOverrideExclude()) {
                    if (isExcludeFeatured()) {
                        searcher.setFeaturedMethod(Search.FeaturedMethod.No);
                    } else {
                        searcher.setFeaturedMethod(Search.FeaturedMethod.Yes);
                    }
                } else {
                    if (categorySettings.isExcludeFeatured()) {
                        searcher.setFeaturedMethod(Search.FeaturedMethod.No);
                    } else {
                        searcher.setFeaturedMethod(Search.FeaturedMethod.Yes);
                    }
                }
            }

        } else {

            logger.fine("[ItemsBlock] items not applicable for this category");
            return null;
        }

        int count = searcher.size();

        // get default items per page
        int perPage = categorySettings.getItemsPerPage();
        if (categorySettings.isItemsPerPageOverride()) {
            if (getItemsPerPage() > 0) {
                perPage = getItemsPerPage();
            }
        }

        if (perPage < 2) {
            perPage = 20;
        }
        if (page == 0) {
            page = 1;
        }


        /*
           * Create results object encapsulating all the pages etc info
           */
        logger.fine("[ItemsBlock] creating results count=" + count + ", page=" + page + ", perPage=" + perPage);
        Results results = new Results(count, page, perPage);
        if (randomUsedItemsCache.get(getId()) != null && randomUsedItemsCache.get(getId()).size() >= count) {
            clearHtmlCache(getId());
        }
        if (randomUsedItemsCache.get(getId()) != null && count - randomUsedItemsCache.get(getId()).size() < perPage) {

            if (page < results.getLastPage() || randomUsedItemsCache.get(getId()).size() > results.getStartIndex())
                clearHtmlCache(getId());
        }
        // put results object in context
        context.setAttribute("results", results);

        searcher.setPrioritised(true);
        searcher.setStart(results.getStartIndex());
        searcher.setLimit(perPage);
        if(searcher.getSortType() == SortType.Random){
            searcher.setExcludeItemsIds(randomUsedItemsCache.get(getId()));
            searcher.setStart(0);
            if (count - results.getStartIndex() < perPage){
               searcher.setLimit(count - results.getStartIndex());
            }
        }

        List<Item> items = new ArrayList<Item>();
        if (!category.useManualSort() || !Module.ManualSort.enabled(context)) {
            items = searcher.getItems();
        }
        else if (category.useManualSort() && Module.ManualSort.enabled(context)) {
            int perPageCounter = 0;
            items = new ArrayList<Item>();
            List<CategoryItem> categoryItems = category.getCategoryItems();
            for (CategoryItem categoryItem : categoryItems) {
                if (categoryItem.getItem().getStatus().equals("LIVE") && perPageCounter < perPage * page) {
                    items.add(categoryItem.getItem());
                    perPageCounter++;
                }
            }
            if (page > 1) {
                if (perPage * page < items.size()) {
                    items = items.subList(perPage * (page - 1), perPage * page);
                } else {
                    items = items.subList(perPage * (page - 1), items.size());
                }
            }
        }

        if (items == null || items.isEmpty()) {

            logger.fine("[ItemsBlock] no items found from searcher");
            return null;
        }

        if (searcher.getSortType() == SortType.Random){
            if (!randomUsedItemsCache.containsKey(getId())){
                randomUsedItemsCache.put(getId(), new ArrayList<Integer>());
            }
            for (Item item: items){
                randomUsedItemsCache.get(getId()).add(item.getId());
            }
        }
        /*
           * Strip out any non visible items
           */
        CollectionsUtil.filter(items, new Predicate<Item>() {

            public boolean accept(Item e) {
                return e.isVisible();
            }
        });

        logger.fine("[ItemsBlock] items after non visible: " + items.size());

        /*
           * strip out any items we do not have permission to view.
           */
        if (Module.Permissions.enabled(context)) {
            CollectionsUtil.filter(items, new Predicate<Item>() {

                public boolean accept(Item e) {
                    return PermissionType.ItemSummary.check(context, account, e.getItemType());
                }
            });
        }

        StringBuilder sb = new StringBuilder();

        /*
           * if we have no items to show then show the empty message for this category or the generic one from search settings.
           */
        if (items.isEmpty()) {

            logger.fine("[ItemsBlock] no items to show after stripping non permissible and non-visible");
            sb.append(HtmlHelper.nl2br(getNoItemsMessage()));
            return sb;
        }

        ImageUtil.prepopulate(context, items);
//        AttributeUtil.prepopulate(context, items);  TODO

        String resultsControl = new ResultsControl(context, results, link).toString();

        /*
		 * Add in a JS variable to a link to the first item
		 */
        sb.append("<script> var firstItemUrl = \"" + items.get(0).getUrl() + "\"; </script>");

        if (isResultsTop()) {
            logger.fine("[ItemsBlock] results top");
            sb.append(resultsControl);
        }

        ItemType itemType = items.get(0).getItemType();

        // show sorting options if enabled
        if (isShowSorts()) {
            if (sort == null) {
                sort = itemType.getDefaultSort();
            }
            String string = new FrontSortRenderer(context, link, itemType.getSorts(), sort, null).toString();
            if (string != null) {
                sb.append(string);
            }
        }

        // we need to see if this block has custom list markup
        Markup markup = getListMarkup();

        if (markup == null) {

            // if markup is null then lets see if any of the parent categories have a markup set
            Iterator<Category> iter = category.getParentsReversed(true).iterator();
            while (iter.hasNext() && markup == null) {

                markup = iter.next().getSubcategoryListItemMarkup();
            }

            if (markup == null) {
                logger.fine("[ItemsBlock] using markup from item type");
                markup = items.get(0).getItemType().getListMarkup();
            } else {
                logger.fine("[ItemsBlock] using markup from parent category");
            }

        }

        logger.fine("[ItemsBlock] markup=" + markup);

        sb.append(new OrderingForm(context));
        if (getOwnerCategory() != null) {
            sb.append(new HiddenTag("linkback", new Link(CategoryHandler.class, null, "category", getOwner().getId())));
            context.setAttribute("trailCategory", category);
        }
        
        MarkupRenderer r = new MarkupRenderer(context, markup);
        r.setBodyObjects(items);
        sb.append(r);

        sb.append("</form>");

        // show bottom results if set in category
        if (isResultsBottom()) {
            logger.fine("[ItemsBlock] results bottom");
            sb.append(resultsControl);
        }

        return sb;
    }

    public final void setIncludeSubcategoryItems(boolean includeSubcategoryItems) {
        this.includeSubcategoryItems = includeSubcategoryItems;
    }

    public final void setItemsPerPage(int itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public final void setListMarkup(Markup listMarkup) {
        this.listMarkup = listMarkup;
    }

    public void setMultiBuy(boolean multiBuy) {
        this.multiBuy = multiBuy;
    }

    public void setNoItemsMessage(String noItemsMessage) {
        this.noItemsMessage = noItemsMessage;
    }

    public void setResultsBottom(boolean resultsBottom) {
        this.resultsBottom = resultsBottom;
    }

    public void setResultsTop(boolean resultsTop) {
        this.resultsTop = resultsTop;
    }

    public final void setShowSorts(boolean showSorts) {
        this.showSorts = showSorts;
    }

    public final void setSort(ItemSort sort) {
        this.sort = sort;
    }

    public void setSort(SortType sortType) {
        this.sortType = sortType;
    }

    public final void setSortType(SortType sortType) {
        this.sortType = sortType;
    }

    public void setExcludeFeatured(boolean excludeFeatured) {
        this.excludeFeatured = excludeFeatured;
    }
}
