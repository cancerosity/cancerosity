package org.sevensoft.ecreator.model.categories;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.categories.db.CategoryAncestor;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 22-Mar-2006 15:34:41
 *
 */
public class CategoryModel {

	private static Logger				logger		= Logger.getLogger("ecreator");

	public static final int				MaxCategories	= 100;

	/**
	 * The root node of this model. Does not necessarily have to be the main category root.
	 */
	private Category					root;

	/**
	 * How many subcategory levels to show. A value of 1 here would show the root's subcategories only.
	 */
	private int						depth;

	private Pattern					pattern;

	private List<Category>				categories;

	private LinkedHashMap<Category, Integer>	map;

	private RequestContext				context;

	private final boolean				excludeCurrent;

	private boolean					cacheThis;

	private Item					account;

	private List<String>				exclusions;

	public CategoryModel(RequestContext context, final Category root, final int depth, final List<String> exc, boolean includeHome, boolean excludeCurrent) {

		this.context = context;
		this.root = root;
		this.depth = depth;
		this.excludeCurrent = excludeCurrent;
		this.cacheThis = true;
		this.account = (Item) context.getAttribute("account");

		this.exclusions = new ArrayList();
        if (exc != null && !exc.isEmpty())
            this.exclusions.addAll(exc);


/*
		// change exclusions to lower case and create list of them             //todo change it
		if (exc != null) {
			for (String string : exc) {
				this.exclusions.add(string.toLowerCase());
			}
		}
*/

		this.categories = new ArrayList();
		addCategories(root, 1);

		// remove current if applicable
		logger.fine("[CategoryTreeModel] excludeCurrent=" + excludeCurrent);
		if (excludeCurrent) {

			Category current = (Category) context.getAttribute("category");
			this.categories.remove(current);

			logger.fine("[CategoryTreeModel] excluding category=" + current);
		}

		map = new LinkedHashMap();
		if (includeHome) {
			map.put(root, 1);
		}

		for (Category category : categories) {
			map.put(category, category.getParentCount() - root.getParentCount());
		}
	}

	private void addCategories(Category parent, int level) {

		for (Category child : parent.getVisibleChildren()) {

			// check we have permission to see this stub
			if (child.isPermissions()) {
				cacheThis = false;
			}

			// check this category or its parent is not in the exclusion list, if it just then exit
            if (!exclusions.isEmpty()) {
                if (exclusions.contains(child.getName().toLowerCase())) {
                    continue;
                }

                for (String name : child.getParentNames()) {
                    if (exclusions.contains(name.toLowerCase())) {
                        continue;
                    }
                }
            }

			if (PermissionType.CategoryStub.check(context, account, child)) {
				categories.add(child);
				if (level < depth) {
					addCategories(child, level + 1);
				}
			}

		}
	}

	private List<Category> getCategories(Category root, int depth) {

		// limit to 400 
		Query q = new Query(context,
				"select c.* from # c join # ca on c.id=ca.category where ca.ancestor=? and c.parentCount<=? order by c.fullName limit 300");
		q.setTable(Category.class);
		q.setTable(CategoryAncestor.class);
		q.setParameter(root);
		q.setParameter(root.getParentCount() + depth);

		return q.execute(Category.class);
	}

	/**
	 * Returns all the categories for this depth level
	 */
	public List<Category> getCategories(int level) {

		List<Category> list = new ArrayList();
		for (Category category : map.keySet()) {
			if (map.get(category) == level) {
				list.add(category);
			}
		}
		return list;
	}

	public int getDepth() {
		return depth;
	}

	/**
	 * Returns a map of category links mapped to the current depth. 
	 * the depth can be used in html tags for reference by css.
	 * 
	 * @return
	 */
	public Map<Category, Integer> getMap() {
		return map;
	}

	public Category getRoot() {
		return root;
	}

	public final boolean isCacheThis() {
		return cacheThis;
	}

	public final void setCacheThis(boolean cacheThis) {
		this.cacheThis = cacheThis;
	}

}
