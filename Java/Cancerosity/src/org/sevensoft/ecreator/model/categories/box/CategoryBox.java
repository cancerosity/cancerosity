package org.sevensoft.ecreator.model.categories.box;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.ecreator.iface.admin.categories.boxes.CategoryBoxHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategoryModel;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 31-Oct-2005 10:41:09
 * 
 */
@Table("categories_boxes")
@Label("Categories")
@HandlerClass(CategoryBoxHandler.class)
public class CategoryBox extends Box {

	public enum Context {
		Fixed, Relative
	}

	public enum Style {
		Expanding, Dynamic, Static, Floating
	}

	/**
	 * The style of the sidebar categories
	 */
	private Style		style;

	/**
	 * Number of categories deep to go.
	 */
	private int			depth;

	/**
	 * The start/root category for this category structure if fixed content is set
	 */
	private Category		root;

	/**
	 * Max level - any further and relative won't follow you any more.
	 */
	private int			maxLevel;

	/**
	 * The names of categories matching this list will not be included
	 */
	private List<String>	exclusions;

	/**
	 * Determines the context of where categories start.
	 */
	private Context		context;

	/**
	 * Include the home cat at the top
	 */
	private boolean		includeHome;

	/**
	 * 
	 */
	private boolean		excludeCurrent;

	public CategoryBox(RequestContext context) {
		super(context);
	}

	public CategoryBox(RequestContext context, String location) {

		super(context, location);

		this.context = Context.Fixed;
		this.style = Style.Static;
		this.depth = 2;

		save();
	}

	@Override
	public synchronized boolean delete() {

//		categoriesCache.remove(getId());
		return super.delete();
	}

	public Context getContext() {
		return context == null ? Context.Fixed : context;
	}

	@Override
	protected String getCssIdDefault() {
		return "categories";
	}

	public int getDepth() {
		return depth < 1 || depth > 5 ? 1 : depth;
	}

	public final List<String> getExclusions() {
		if (exclusions == null) {
			exclusions = new ArrayList();
		}
		return exclusions;
	}

	public final int getMaxLevel() {
		return maxLevel;
	}

	public Category getRoot() {
		return (Category) (root == null ? null : root.pop());
	}

	public Style getStyle() {
		return style == null ? Style.Static : style;
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	public final boolean isExcludeCurrent() {
		return excludeCurrent;
	}

	public boolean isIncludeHome() {
		return includeHome;
	}

	public boolean isRelative() {
		return getContext() == Context.Relative;
	}

	public String render(RequestContext context) {
		
		logger.fine("rendering category box");

		Category currentCategory = (Category) context.getAttribute("category");
		Category currentRoot = null;

		/*
		 * Get the appropriate root dependant on if we are using relative or fixed context
		 */
		switch (getContext()) {

		case Relative:

			currentRoot = currentCategory;

			/*
			 * If root is null then we are not in a category, so we will instead show root categories
			 */
			if (currentRoot == null) {
				currentRoot = Category.getRoot(context);
			}

			// if current root does not have children then go up a level
			if (!currentRoot.hasChildren() && currentRoot.isChild()) {
				currentRoot = currentRoot.getParent();
			}

			if (maxLevel > 0) {
				if (currentRoot.getParentCount() > maxLevel) {
					try {
						currentRoot = currentRoot.getParents(true).get(maxLevel);
					} catch (RuntimeException e) {
						e.printStackTrace();
					}
				}
			}

			break;

		default:
		case Fixed:

			currentRoot = getRoot();
			if (currentRoot == null) {
				currentRoot = Category.getRoot(context);
			}

			break;
		}
		
		logger.fine("root=" + currentRoot + ", subcats=" + currentRoot.getSubcategories());

		/*
		 * If no subcategories of our current root then no subcategories to show !
		 */
		if (currentRoot.isLeaf()) {
			
			logger.fine("no subcategories of context category");
			return null;
		}

		CategoryModel model = new CategoryModel(context, currentRoot, getDepth(), getExclusions(), includeHome, excludeCurrent);

		String string = new CategoryBoxRenderer(context, this, currentCategory, model).toString();

		return string;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public final void setExcludeCurrent(boolean excludeCurrent) {
		this.excludeCurrent = excludeCurrent;
	}

	public final void setExclusions(List<String> exclusions) {
		this.exclusions = exclusions;
	}

	public void setIncludeHome(boolean includeHome) {
		this.includeHome = includeHome;
	}

	public final void setMaxLevel(int maxLevel) {
		this.maxLevel = maxLevel;
	}

	public void setRoot(Category root) {
		this.root = root;
	}

	public void setStyle(Style style) {
		this.style = style;
	}

}
