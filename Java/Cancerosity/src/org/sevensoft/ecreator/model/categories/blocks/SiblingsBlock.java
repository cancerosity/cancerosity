package org.sevensoft.ecreator.model.categories.blocks;

import org.sevensoft.ecreator.model.categories.CategorySettings;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 14 Aug 2006 15:24:34
 * 
 * Holder block for siblings
 * 
 */
@Table("blocks_siblings")
@Label("Siblings")
public class SiblingsBlock extends Block {

	private Markup	markup;

	private SiblingsBlock(RequestContext context) {
		super(context);
	}

	public SiblingsBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, 0);
	}

	public Markup getMarkup() {
		return (Markup) (markup == null ? null : markup.pop());
	}

	@Override
	public boolean isSimple() {
		return false;
	}

	@Override
	public Object render(RequestContext context) {

		MarkupRenderer r = new MarkupRenderer(context, markup == null ? CategorySettings.getInstance(context).getSubcategoriesMarkup() : getMarkup());
		r.setBodyObjects(getOwnerCategory().getParent().getVisibleChildren());

		return r.toString();
	}

	public void setMarkup(Markup markup) {
		this.markup = markup;
	}

}
