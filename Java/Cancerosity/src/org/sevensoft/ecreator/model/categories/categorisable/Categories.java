package org.sevensoft.ecreator.model.categories.categorisable;

import java.util.List;

import org.sevensoft.ecreator.model.categories.Category;

/**
 * @author sks 30 Nov 2006 10:52:21
 * 
 * This interface is for objects that own or join categories. EG a listing session might have listing session categories.
 *
 */
public interface Categories {

	public void addCategories(List<Category> categories);

	public void addCategory(Category category);

	public List<Category> getCategories();

	public String getFullId();

	public boolean hasCategory();

	public void removeCategories();

	public void removeCategory(Category category);

}
