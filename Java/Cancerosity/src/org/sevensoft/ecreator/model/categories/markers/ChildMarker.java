package org.sevensoft.ecreator.model.categories.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.ICategoryMarker;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;
import java.util.List;

/**
 * @author sks 3 Aug 2006 16:29:56
 *
 *         Shows subcategories / children from a category
 *
 */
public class ChildMarker extends MarkerHelper implements ICategoryMarker {

	public Object generate(RequestContext context, Map<String, String> params, Category category) {

		String sep = params.get("sep");

		StringBuilder sb = new StringBuilder();

		List<Category> subs = category.getSubcategories();
		boolean next = false;
		for (Category sub : subs) {

			if (next) {
				sb.append(sep);
			}

			sb.append(sub.getLinkTag());

			next = true;
		}

		return super.string(context, params, sb);
	}

	public Object getRegex() {
		return "child";
	}

}
