package org.sevensoft.ecreator.model.categories.categorisable;

import java.util.Map;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.categories.CategoryOwnerHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

/**
 * @author sks 2 May 2006 15:56:30
 *
 */
public class CategoriesRenderer {

	private final Categories	c;
	private final RequestContext	context;
	private int				page;

	public CategoriesRenderer(RequestContext context, Categories c, int page) {
		this.context = context;
		this.c = c;
		this.page = page;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		StringBuilder sb2 = new StringBuilder();

		for (Category category : c.getCategories()) {

			sb2.append(category.getName()
					+ " "
					+ new ButtonTag(CategoryOwnerHandler.class, "removeCategory", "Remove", StringHelper.toCamelCase(c.getClass()), c, "category",
							category));

		}

		sb.append(new AdminRow("Categories", "These are the categories currently in use", sb2));

		int count = Category.getCount(context);
		final Results results = new Results(count, page, 500);
		final Map<String, String> categoryOptions = Category.getCategoryOptions(context, null, results.getStartIndex(), 500);

		Link link = new Link(ItemHandler.class, "edit", "item", c);

		SelectTag tag = new SelectTag(context, "addCategory");
		tag.setAny("-Select category-");
		tag.addOptions(categoryOptions);

		sb2 = new StringBuilder();
		if (results.hasMultiplePages()) {
			sb2.append(new ResultsControl(context, results, link));
		}

		sb2.append(tag);

		sb.append(new AdminRow("Add category ", "Choose a category to add", sb2));

		return sb.toString();
	}
}
