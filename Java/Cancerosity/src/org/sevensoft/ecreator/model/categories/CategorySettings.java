package org.sevensoft.ecreator.model.categories;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.sevensoft.commons.skint.Amount;
import org.sevensoft.ecreator.model.categories.renderers.CategorySearchMarkupDefault;
import org.sevensoft.ecreator.model.categories.renderers.siblings.SiblingsMarkupDefault;
import org.sevensoft.ecreator.model.categories.renderers.subcategories.SubcategoryMarkupDefault;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.items.sorts.ItemSort;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sam2 Nov 12, 2003 2:00:43 AM
 */
@Table("settings_category")
@Singleton
public class CategorySettings extends EntityObject {

	public static CategorySettings getInstance(RequestContext context) {
		return getSingleton(context, CategorySettings.class);
	}

	private String			header, footer;

	private boolean			subcategoryOrdering;

	@Deprecated
	private boolean			headers;

	@Deprecated
	private boolean			files;

	/**
	 * Default for items per page
	 */
	private int				itemsPerPage;

	/**
	 * Allow per category override of items per page.
	 */
	private boolean			itemsPerPageOverride;

	/**
	 * 
	 */
	/**
	 * 
	 */
	private boolean			categoryMenuFlat;

	/**
	 * 
	 */
	private boolean			hideSubcategories;

	/**
	 * Enables the use of hidden categories
	 */
	private boolean			hidden;

	@Deprecated
	private boolean			summaries;

	/**
	 * Show sort options in category
	 */
	@Default("1")
	private boolean			sorts;

	/**
	 * Allow each category to specify if it shows sorts or not
	 */
	private boolean			overrideSorts;

	@Deprecated
	private boolean			images;

	/**
	 * Should we should results bottom
	 */
	@Default("1")
	private boolean			resultsBottom;

	/**
	 * Should we should resutls top  
	 */
	@Default("1")
	private boolean			resultsTop;

	/**
	 * Allows us to override the settings to results top / bottom / etc in each category
	 */
	private boolean			resultsOverride;

	private Markup			siblingsMarkup;

	private Set<CategoryModule>	modules;

	/**
	 * The classname for the titles generator
	 */
	private String			titlesGenerator;

    private boolean excludeFeatured;
    private boolean overrideExclude;

	public boolean hasTitlesGenerator() {
		return titlesGenerator != null;
	}

	/**
	 * Set this to true to enable subcategories selector on all blocks
	 */
	private boolean	subcategoriesSelector;

	private String	categoryNameGfxGen;

	/**
	 * The default subcategories markup used by all subcategory blocks
	 */
	private Markup	subcategoriesMarkup;

	private Markup	categorySearchMarkup;

	private ItemSort	itemSort;

	@Deprecated
	private boolean	forwards;

    private boolean showHomePage;

    public CategorySettings(RequestContext context) {
		super(context);
	}

	public void addModule(CategoryModule module) {
		getModules().add(module);
	}

	/**
	 * Exports all categories (excluding root) in a text format for re-creation.
	 * 
	 * @return
	 */
	public String exportCategoryTree() {

		StringBuilder sb = new StringBuilder();
		for (Category category : Category.get(context)) {

			if (category.isRoot()) {
				continue;
			}

			for (int n = 1; n < category.getParentCount(); n++) {
				sb.append("+");
			}

			sb.append(category.getName());
			sb.append("\n");

		}
		return sb.toString();
	}

	public int getCategoryCount() {
		return new Query(context, "select count(*) from #").setTable(Category.class).getInt();
	}

	public final String getCategoryNameGfxGen() {
		return categoryNameGfxGen;
	}

	/**
	 * 
	 */
	public Markup getCategorySearchMarkup() {

		if (categorySearchMarkup == null) {
			categorySearchMarkup = new Markup(context, new CategorySearchMarkupDefault());
			save();
		}

		return categorySearchMarkup.pop();
	}

	public String getFooter() {
		return footer;
	}

	public String getHeader() {
		return header;
	}

	public final ItemSort getItemSort() {
		return (ItemSort) (itemSort == null ? null : itemSort.pop());
	}

	public int getItemsPerPage() {
		return itemsPerPage < 1 ? 20 : itemsPerPage;
	}

	public final Set<CategoryModule> getModules() {

		if (modules == null) {
			modules = new HashSet();
		}

		if (images) {
			modules.add(CategoryModule.Images);
			images = false;
			save();
		}

		if (files) {
			modules.add(CategoryModule.Attachments);
			files = false;
			save();
		}

		if (headers) {
			modules.add(CategoryModule.Headers);
			headers = false;
			save();
		}

		if (forwards) {
			modules.add(CategoryModule.Forwards);
			forwards = false;
			save();
		}

		if (summaries) {
			modules.add(CategoryModule.Summaries);
			summaries = false;
			save();
		}

		return modules;
	}

	public Markup getSiblingsMarkup() {

		if (siblingsMarkup == null) {

			siblingsMarkup = new Markup(context, new SiblingsMarkupDefault());
			save();
		}

		return siblingsMarkup.pop();
	}

	public Markup getSubcategoriesMarkup() {

		if (subcategoriesMarkup == null) {

			subcategoriesMarkup = new Markup(context, new SubcategoryMarkupDefault());
			save();
		}

		return subcategoriesMarkup.pop();
	}

	public boolean hasCategoryNameGfxGen() {
		return categoryNameGfxGen != null;
	}

	public boolean hasFooter() {
		return footer != null && getModules().contains(CategoryModule.Headers);
	}

	public boolean hasHeader() {
		return header != null && getModules().contains(CategoryModule.Headers);
	}

	public boolean isCategoryMenuFlat() {
		return categoryMenuFlat;
	}

	public boolean isHidden() {
		return hidden;
	}

	public boolean isHideSubcategories() {
		return hideSubcategories;
	}

	public boolean isItemsPerPageOverride() {
		return itemsPerPageOverride;
	}

	public boolean isOverrideSorts() {
		return overrideSorts;
	}

	public boolean isResultsBottom() {
		return resultsBottom;
	}

	public boolean isResultsOverride() {
		return resultsOverride;
	}

	public boolean isResultsTop() {
		return resultsTop;
	}

    public boolean isShowHomePage() {
        return showHomePage;
    }

    public boolean isShowSorts() {
		return sorts;
	}

	public boolean isSubcategoriesSelector() {
		return subcategoriesSelector;
	}

	public boolean isSubcategoryOrdering() {
		return subcategoryOrdering;
	}

    public boolean isExcludeFeatured() {
        return excludeFeatured;
    }

    public boolean isOverrideExclude() {
        return overrideExclude;
    }

    public void removeModule(CategoryModule module) {
		getModules().remove(module);
	}

	public void setCategoryMenuFlat(boolean categoryMenuFlat) {
		this.categoryMenuFlat = categoryMenuFlat;
	}

	public final void setCategoryNameGfxGen(String categoryNameGfxGen) {
		this.categoryNameGfxGen = categoryNameGfxGen;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public final void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public final void setHideSubcategories(boolean hideSubcategories) {
		this.hideSubcategories = hideSubcategories;
	}

	public final void setItemSort(ItemSort itemSort) {
		this.itemSort = itemSort;
	}

	public void setItemsPerPage(int itemsPerPage) {
		this.itemsPerPage = itemsPerPage;
	}

	public void setItemsPerPageOverride(boolean itemsPerPageOverride) {
		this.itemsPerPageOverride = itemsPerPageOverride;
	}

	public void setOverrideSorts(boolean overrideSorts) {
		this.overrideSorts = overrideSorts;
	}

	/**
	 * Set default price on all categories.
	 */
	public void setPrice(String price) {

		/*
		 * For speed remove all baskets
		 */
		Basket.deleteAll(context);

		for (Category category : Category.get(context)) {
			category.setSellPrice(null, 1, Amount.parse(price));
		}
	}

	public void setResultsBottom(boolean resultsBottom) {
		this.resultsBottom = resultsBottom;
	}

	public void setResultsOverride(boolean resultsOverride) {
		this.resultsOverride = resultsOverride;
	}

	public void setResultsTop(boolean resultsTop) {
		this.resultsTop = resultsTop;
	}

    public void setShowHomePage(boolean showHomePage) {
        this.showHomePage = showHomePage;
    }

    public void setSiblingsMarkup(Markup siblingsMarkup) {
		this.siblingsMarkup = siblingsMarkup;
	}

	public void setSorts(boolean sorts) {
		this.sorts = sorts;
	}

	public void setSubcategoriesMarkup(Markup markup) {
		this.subcategoriesMarkup = markup;
	}

	public void setSubcategoriesSelector(boolean subcategoriesSelector) {
		this.subcategoriesSelector = subcategoriesSelector;
	}

	public void setSubcategoryOrdering(boolean subcategoryOrdering) {
		this.subcategoryOrdering = subcategoryOrdering;
	}

    public void setExcludeFeatured(boolean excludeFeatured) {
        this.excludeFeatured = excludeFeatured;
    }

    public void setOverrideExclude(boolean overrideExclude) {
        this.overrideExclude = overrideExclude;
    }

    @Override
	protected void singletonInit(RequestContext context) {

		super.singletonInit(context);

		resultsTop = true;
		resultsBottom = true;

		save();
	}

	public Collection<CategoryModule> getUnusedModules() {

		Collection<CategoryModule> modules = new ArrayList();
		modules.addAll(Arrays.asList(CategoryModule.values()));
		modules.removeAll(getModules());
		return modules;
	}

	public final String getTitlesGenerator() {
		return titlesGenerator;
	}

	public final void setTitlesGenerator(String titlesGenerator) {
		this.titlesGenerator = titlesGenerator;
	}

}