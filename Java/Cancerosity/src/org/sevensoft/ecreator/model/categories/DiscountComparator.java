package org.sevensoft.ecreator.model.categories;

import java.util.Comparator;

/**
 * User: Tanya
 * Date: 05.07.2012
 */
public class DiscountComparator implements Comparator<Category> {

    public int compare(Category o1, Category o2) {
        if (o1.getDiscount() != null && o2.getDiscount() != null)
            if (o1.getDiscount().isFixed())
                return o1.getDiscount().getFixed().compareTo(o2.getDiscount().getFixed());
            else
                return Double.compare(o1.getDiscount().getPercentage(), o2.getDiscount().getPercentage());

        else if (o1.getDiscount() != null)
            return 1;
        else
            return -1;

    }
}
