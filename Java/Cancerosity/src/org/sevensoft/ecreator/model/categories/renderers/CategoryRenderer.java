package org.sevensoft.ecreator.model.categories.renderers;

import java.util.Iterator;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 12-Jan-2006 21:58:46
 *
 */
public class CategoryRenderer {

	class CategorySeparatorView {

		private String	separator;
		private int		depth;

		public CategorySeparatorView(int depth, String separator) {
			this.depth = depth;
			this.separator = separator;
		}

		@Override
		public String toString() {

			StringBuilder sb = new StringBuilder();
			toString(sb, root, 1, separator);
			return sb.toString();
		}

		public void toString(StringBuilder sb, Category category, int n, String separator) {

			Iterator<Category> iter = category.getChildren().iterator();
			while (iter.hasNext()) {

				Category cat = iter.next();

				if (cat.isVisible()) {

					LinkTag tag = new LinkTag(cat.getUrl(), cat.getName());
					sb.append(tag);

					if (separator != null)
						if (iter.hasNext() || (n < depth && cat.hasChildren()))
							sb.append(separator);

					if (n < depth)
						toString(sb, cat, n + 1, separator);
				}
			}
		}
	}

	class CategoryTagView {

		private int		depth;
		private String	tag;

		public CategoryTagView(int depth, String tag) {
			this.depth = depth;
			this.tag = tag;
		}

		@Override
		public String toString() {

			StringBuilder sb = new StringBuilder();
			toString(sb, root, 1, tag);
			return sb.toString();
		}

		public void toString(StringBuilder sb, Category category, int n, String tag) {

			Iterator<Category> iter = category.getChildren().iterator();
			while (iter.hasNext()) {

				Category cat = iter.next();

				if (cat.isVisible()) {

					if (current != null && current.getPrimaryParent().equals(cat))
						sb.append("<" + tag + " class='l" + n + "_active'>");
					else {
						sb.append("<" + tag + " class='l" + n + "' onmouseover=\"this.className='l" + n + "_over';\" ");
						sb.append(" onmouseout=\"this.className='l" + n + "';\">");
					}

					LinkTag link = new LinkTag(cat.getUrl(), cat.getName());
					sb.append(link);

					sb.append("</");
					sb.append(tag);
					sb.append(">");

					if (n < depth)
						toString(sb, cat, n + 1, tag);
				}
			}
		}
	}

	private Category	root, current;

	public CategoryRenderer(Category root) {
		this.root = root;
	}

	public CategorySeparatorView getSeparatorView(int depth, String sep) {
		return new CategorySeparatorView(depth, sep);
	}

	public CategoryTagView getTagView(int depth, String tag) {
		return new CategoryTagView(depth, tag);
	}

	public void setCurrent(Category current) {
		this.current = current;
	}

}
