package org.sevensoft.ecreator.model.categories.blocks;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.ecreator.iface.admin.categories.blocks.SubcategoriesBlockHandler;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategorySettings;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 25 Jul 2006 18:56:47
 * 
 * This class is a holder for category subcategories
 *
 */
@Table("blocks_subcategories")
@Label("Subcategories")
@HandlerClass(SubcategoriesBlockHandler.class)
public class SubcategoriesBlock extends Block {

	private Markup	markup;

	/**
	 * Show subcategories as a select box instead of using markup
	 */
	private boolean	selector;

	private String	introText;

	private SubcategoriesBlock(RequestContext context) {
		super(context);
	}

	public SubcategoriesBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, 0);
	}

	public String getIntroText() {
		return introText == null ? "-Choose subcategory-" : introText;
	}

	public Markup getMarkup() {
		return (Markup) (markup == null ? null : markup.pop());
	}

	public boolean isSelector() {
		return selector || CategorySettings.getInstance(context).isSubcategoriesSelector();
	}

	@Override
	public boolean isSimple() {
		return false;
	}

	private static Map<Integer, String>	htmlCache;

	static {
		htmlCache = new HashMap();
	}

	public static void clearHtmlCache() {
		htmlCache.clear();
	}

	@Override
	public Object render(RequestContext c) {

		/*
		 * Lets check cache first
		 */
		if (htmlCache.containsKey(getId())) {
			return htmlCache.get(getId());
		}

		if (!getOwnerCategory().hasChildren()) {
			logger.fine("[SubcategoriesBlock] owner category has no children");
			return null;
		}

		boolean cacheThis = true;

		// get visible children
		final List<Category> children = getOwnerCategory().getVisibleChildren();
		logger.fine("[SubcategoriesBlock] visibleChildren=" + children);

		if (children.isEmpty()) {
			logger.fine("[SubcategoriesBlock] owner category has no visible children");
			return null;
		}

		final Item account = (Item) context.getAttribute("account");

		// ensure we have permissions to see cat stubs
		if (Module.Permissions.enabled(c)) {

			// if permissions are enabled on any subcat then we cannot cache
			for (Category child : children) {
				
				if (child.isPermissions()) {
					cacheThis = false;
					break;
				}
			}

			CollectionsUtil.filter(children, new Predicate<Category>() {

				public boolean accept(Category e) {
					return PermissionType.CategoryStub.check(context, account, e);
				}
			});

		}

		if (children.isEmpty()) {
			logger.fine("[SubcategoriesBlock] no categories after checking for stub permissions");
			return null;
		}

		// check for selector first
		if (isSelector()) {

			SelectTag tag = new SelectTag(context, "category");
			tag.setAny(getIntroText());
			for (Category category : children) {
				tag.addOption(category.getUrl(), category.getName());
			}
			tag.setOnChange("window.location=this.value;");

			return tag;
		}

		Markup markup = getMarkup();
		logger.fine("[SubcategoriesBlock] own markup=" + markup);

		// if markup is not set on this category, then check our parent categories for subcategory markup
		if (markup == null) {
			markup = getParentMarkup(markup);
			logger.fine("[SubcategoriesBlock] markup from parents=" + markup);
		}

		if (markup == null) {
			logger.fine("[SubcategoriesBlock] using markup from category settings");
			markup = CategorySettings.getInstance(context).getSubcategoriesMarkup();
		}

		assert markup != null : "markup is null and that's a bad thing!";

		MarkupRenderer r = new MarkupRenderer(context, markup);
		r.setBodyObjects(children);
		String html = r.toString();

		// if we are ok to cache it then put in cache
		if (cacheThis) {
			htmlCache.put(getId(), html);
		}
		return html;
	}

	/**
	 * 
	 */
	private Markup getParentMarkup(Markup markup) {

		for (Category parent : getOwnerCategory().getParentsReversed(true)) {

			if (parent.hasSubcategoryMarkup()) {
				return parent.getSubcategoryMarkup();
			}
		}

		return null;
	}

	public final void setIntroText(String introText) {
		this.introText = introText;
	}

	public void setMarkup(Markup markup) {
		this.markup = markup;
	}

	public void setSelector(boolean selector) {
		this.selector = selector;
	}

}
