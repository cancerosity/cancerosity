package org.sevensoft.ecreator.model.categories.renderers.subcategories;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * @author sks 19 Jun 2006 12:03:28
 *
 */
public class SubcategoryMarkupDefault implements MarkupDefault {

	public String getBetween() {
		return null;
	}

	public String getBody() {
		return "<li><span class='subcat'> [category?link=1]</span></li>";
	}

	public String getCss() {
		return "span.subcat { font-weight: bold; }";
	}

	public String getEnd() {
		return "</ul>";
	}

	public String getName() {
		return "Default subcategories markup";
	}

	public String getStart() {
		return "<ul>";
	}

	public int getTds() {
		return 0;
	}

}
