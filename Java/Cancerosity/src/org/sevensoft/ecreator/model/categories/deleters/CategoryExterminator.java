package org.sevensoft.ecreator.model.categories.deleters;

import java.util.logging.Logger;

import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeValue;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategoryItem;
import org.sevensoft.ecreator.model.categories.blocks.ItemsBlock;
import org.sevensoft.ecreator.model.categories.blocks.SiblingsBlock;
import org.sevensoft.ecreator.model.categories.blocks.SubcategoriesBlock;
import org.sevensoft.ecreator.model.categories.db.CategoryAncestor;
import org.sevensoft.ecreator.model.design.template.TemplateConfig;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.misc.content.ContentBlock;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 12 Oct 2006 09:29:16
 *
 */
public class CategoryExterminator {

	private final RequestContext	context;
	private Logger			logger	= Logger.getLogger("ecreator");

	public CategoryExterminator(RequestContext context) {
		this.context = context;
	}

	public void delete() {

		// delete all categories
		SimpleQuery.delete(context, Category.class);

		// delete ancestors
		SimpleQuery.delete(context, CategoryAncestor.class);

		// and items
		SimpleQuery.delete(context, CategoryItem.class);

		// and contents
		SimpleQuery.delete(context, ContentBlock.class);

		// and any blocks on these
		SimpleQuery.delete(context, SubcategoriesBlock.class);
		SimpleQuery.delete(context, SiblingsBlock.class);
		SimpleQuery.delete(context, ItemsBlock.class);

		new Query(context, "delete from # where category>0").setTable(Attribute.class).run();
		new Query(context, "delete from # where category>0").setTable(AttributeValue.class).run();
		new Query(context, "delete from # where category>0").setTable(Img.class).run();
		new Query(context, "delete from # where category>0").setTable(Attachment.class).run();
		new Query(context, "delete from # where category>0").setTable(TemplateConfig.class).run();

		logger.fine("[CategoryExterminator] categories deleted");

		// empty category cache
		context.clearCache();

		SubcategoriesBlock.clearHtmlCache();

		// remove root
		context.removeAttribute("root");

		Category.checkRoot(context);
	}

}
