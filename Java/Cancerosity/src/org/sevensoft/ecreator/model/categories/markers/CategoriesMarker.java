package org.sevensoft.ecreator.model.categories.markers;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author sks 31-Mar-2006 10:46:58
 */
public class CategoriesMarker implements IGenericMarker {

    public Object generate(RequestContext context, Map<String, String> params) {

        Category currentPrimary = (Category) context.getAttribute("category");
        if (currentPrimary != null) {
            currentPrimary = currentPrimary.getPrimaryParent();
        }

        String sep = params.get("sep");
        String sepImage = params.get("sepimage");
        String sepImageAlt = params.get("sepimagealt");
        boolean hidden = params.containsKey("hidden");

        String exclude = params.get("exclude");
        String excludeIds = params.get("excludeids");
        String tag = params.get("tag");
        String valign = params.get("valign");
        String align = params.get("align");
        String clazz = params.get("class");
        String leftImage = params.get("leftimg");
        String rightImage = params.get("rightimg");
        String leftImageActive = params.get("leftimgactive");
        String rightImageActive = params.get("rightimgactive");
        String leftImageHover = params.get("leftimghover");
        String rightImageHover = params.get("rightimghover");
        boolean images = params.containsKey("images");
        boolean imagesbelow = params.containsKey("imagesbelow");

        int cycleIds = 0;
        if (params.containsKey("cycleids")) {
            cycleIds = Integer.parseInt(params.get("cycleids"));
        }

        boolean activehover = params.containsKey("activehover");
        boolean imagesonly = params.containsKey("imagesonly");
        boolean dropdown = params.containsKey("dropdown");

        if (tag == null) {
            tag = "span";
        }

        if (clazz == null) {
            clazz = "cat_link";
        }

        String openTag = "<" + tag;
        String closeTag = "</" + tag + ">";

        List<String> exclusions = new ArrayList();
        if (exclude != null) {
            for (String string : exclude.split("\\b")) {
                exclusions.add(string.toLowerCase());
            }
        }

        List<String> exclusionIds = new ArrayList();
        if (excludeIds != null) {
            for (String string : excludeIds.split("\\D")) {
                exclusionIds.add(string.toLowerCase());
            }
        }

        StringBuilder sb = new StringBuilder();

//		if (dropdown) {
//			script(sb);
//		}

        // a class name just for this tab
        String tabclass;
        int n = 0;

        Category root = EntityObject.getInstance(context, Category.class, params.get("root"));
        if (root == null) {
            root = Category.getRoot(context);
        }

        for (Category child : root.getVisibleChildren()) {

            if (exclusions.contains(child.getName().toLowerCase())) {
                continue;
            }

            if (exclusionIds.contains(child.getIdString())) {
                continue;
            }

            if (!child.hasApprovedImages() && imagesonly) {
                continue;
            }

            if (child.equals(currentPrimary)) {
                tabclass = clazz + "_active";
            } else {
                tabclass = clazz;
            }

            if (n > 0) {

                if (sepImage != null) {

                    ImageTag sepImageTag = new ImageTag(sepImage);
                    if (sepImageAlt != null) {
                        sepImageTag.setAlt(sepImageAlt);
                        sepImageTag.setTitle(sepImageAlt);
                    }
                    sb.append(sepImageTag);

                } else if (sep != null) {
                    sb.append(sep);
                }
            }

            sb.append(openTag);

            sb.append(" class=\"");
            sb.append(tabclass);
            sb.append("\"");

            sb.append(" id=\"category_primary_");
            if (cycleIds > 0) {
                sb.append(n % cycleIds);
            } else {
                sb.append(child.getIdString());
            }
            sb.append("\"");

            if (align != null) {
                sb.append(" align=\"");
                sb.append(align);
                sb.append("\"");
            }

            if (valign != null) {
                sb.append(" valign=\"");
                sb.append(valign);
                sb.append("\"");
            }

            if (hidden) {
                sb.append(" style=\"display: none;\"");
            }

            List<String> onMouseOvers = new ArrayList();
            List<String> onMouseOuts = new ArrayList();

            if (!child.equals(currentPrimary) || activehover) {

//                onMouseOvers.add("this.className='" + tabclass + "_hover'");
//                onMouseOuts.add("this.className='" + tabclass + "'");

                if (leftImageHover != null) {
                    onMouseOvers.add("document.getElementById('tabs_left_" + child.getId() + "').src='template-data/" + leftImageHover + "'");
                    onMouseOuts.add("document.getElementById('tabs_left_" + child.getId() + "').src='template-data/" + leftImage + "'");
                }

                if (rightImageHover != null) {
                    onMouseOvers.add("document.getElementById('tabs_right_" + child.getId() + "').src='template-data/" + rightImageHover + "'");
                    onMouseOuts.add("document.getElementById('tabs_right_" + child.getId() + "').src='template-data/" + rightImage + "'");
                }

            }

            if (dropdown) {

                if (child.hasChildren()) {
                    onMouseOvers.add("hideSecondLevelSubMenu();");
                    onMouseOvers.add("showSubMenu('category_primary_" + child.getIdString() + "', 'ec_submenu_" + child.getId() + "')");
                    onMouseOuts.add("hideSubMenu();");
                }
            }

            if (onMouseOvers.size() > 0) {
                sb.append(" onmouseover=\"" + StringHelper.implode(onMouseOvers, "; ") + "\"");
            }

            if (onMouseOuts.size() > 0) {
                sb.append(" onmouseout=\"" + StringHelper.implode(onMouseOuts, "; ") + "\"");
            }

            sb.append(">");

            if (child.equals(currentPrimary) && leftImageActive != null) {

                ImageTag leftImageTag = new ImageTag("template-data/" + leftImageActive);
                leftImageTag.setAlign("absmiddle");
                sb.append(leftImageTag);

            } else if (leftImage != null) {

                ImageTag leftImageTag = new ImageTag("template-data/" + leftImage);
                leftImageTag.setId("tabs_left_" + child.getId());
                leftImageTag.setAlign("absmiddle");
                sb.append(leftImageTag);

            }

            if (images) {

                StringBuilder sb2 = new StringBuilder();

                if (imagesbelow) {
                    sb2.append("<span>" + child.getName() + "</span>");
                }

                if (child.hasApprovedImages()) {

                    Img img = child.getApprovedImage();
                    if (img != null) {
                        sb2.append(img.getThumbnailTag());
                    }
                }

                if (!imagesbelow) {
                    sb2.append("<span>" + child.getName() + "</span>");
                }

                sb.append(new LinkTag(child.getUrl(), sb2));

            } else {

                sb.append(new LinkTag(child.getUrl(), child.getName()));
            }

            if (child.equals(currentPrimary) && rightImageActive != null) {

                ImageTag rightImageTag = new ImageTag("template-data/" + rightImageActive);
                rightImageTag.setAlign("absmiddle");
                sb.append(rightImageTag);

            } else if (rightImage != null) {

                ImageTag rightImageTag = new ImageTag("template-data/" + rightImage);
                rightImageTag.setId("tabs_right_" + child.getId());
                rightImageTag.setAlign("absmiddle");
                sb.append(rightImageTag);

            }

            sb.append(closeTag);

            n++;
        }

        return sb.toString();
    }

    /**
     *
     */
    /*private void script(StringBuilder sb) {

        sb.append("	<script language=\"JavaScript\" type=\"text/javascript\">\n");

        sb.append("		var activeMenu;\n");

        sb.append("		function showSubMenu(menuId, submenuId) {\n");

        sb.append("			if (activeMenu != null)\n");
        sb.append("				activeMenu.style.display = 'none';\n");

        sb.append("			activeMenu = document.getElementById(submenuId);\n");

        sb.append("			var menu = document.getElementById(menuId);\n");

        sb.append("			if (menu) {\n");

        sb.append("				var curleft = 0; var curtop = 0;");
        sb.append("				if (menu.offsetParent) {");
        sb.append("					curleft = menu.offsetLeft;");
        sb.append("					curtop Ca= menu.offsetHeight + menu.offsetTop;");
        sb.append("					while (menu = menu.offsetParent) {");
        sb.append("						curleft += menu.offsetLeft;");
        sb.append("						curtop += menu.offsetTop;");
        sb.append("					}");
        sb.append("				}");

        sb.append("				activeMenu.style.left = curleft + 'px';\n");
        sb.append("				activeMenu.style.top = curtop + 'px';\n");
        sb.append("			}\n");

        sb.append("			activeMenu.style.display = 'block';\n");
        sb.append("		}\n");

        sb.append("		function hideSubMenu() {\n");
        sb.append("			activeMenu.style.display = 'none';\n");
        sb.append("		}\n");

        sb.append("	</script>\n");
    }*/
    public Object getRegex() {
        return new String[]{"categories_primary", "categories"};
    }

}
