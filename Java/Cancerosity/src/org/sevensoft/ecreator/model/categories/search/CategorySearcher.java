package org.sevensoft.ecreator.model.categories.search;

import java.util.Iterator;
import java.util.List;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.misc.content.ContentBlock;
import org.sevensoft.jeezy.db.StringQueryBuilder;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 25 Oct 2006 15:00:02
 *
 */
public class CategorySearcher implements Iterable<Category> {

	private final RequestContext	context;
	private String			keywords;
	private int				limit;
	private int				start;
	private boolean			visibleOnly;
	private String			name;
	private int				maxParents;

	public CategorySearcher(RequestContext context) {
		this.context = context;
		this.visibleOnly = false;
	}

    protected List<Category> execute(int start, int limit) {
        return execute(start, limit, true);
    }
	protected List<Category> execute(int start, int limit, boolean useLimit) {

		StringQueryBuilder b = new StringQueryBuilder(context);

		b.append("select c.* from # c ");
		b.addTable(Category.class);

		if (keywords != null) {
			b.append(" left outer join # cb on c.id=cb.category ");
			b.addTable(ContentBlock.class);
		}

		b.append(" WHERE 1=1 ");

		if (visibleOnly) {
			b.append(" and hidden=0 ");
		}

		if (maxParents > 0) {
			b.append(" and c.parentCount<=?", maxParents);
		}

		if (keywords != null) {

			for (String string : keywords.split("\\s")) {

				if (string.startsWith("-")) {

					string = "%" + string.substring(1) + "%";
					b.append(" and (c.name not like ? and cb.content not like ?) ", string, string);

				} else {

					string = "%" + string + "%";
					b.append(" and (c.name like ? or cb.content like ?) ", string, string);

				}
			}
		}

		if (name != null) {
			b.append(" and name=? ", name);
		}

        if (useLimit) {
            if (limit < 1 || limit > 1000) {
                limit = 200;
            }
        }

		b.append(" order by c.name ");
		return b.toQuery().execute(Category.class, start, limit);
	}

	public Category get() {
		limit = 1;
		List<Category> categories = getCategories();
		return categories.isEmpty() ? null : categories.get(0);
	}

	public List<Category> getCategories() {
		return getCategories(true);
	}

    public List<Category> getCategories(boolean useLimit) {
		return execute(start, limit, useLimit);
	}

	public boolean isVisible() {
		return visibleOnly;
	}

	/**
	 * Iterate over categories
	 */
	public Iterator<Category> iterator() {

		return new Iterator<Category>() {

			private int			pos;
			private int			end;
			private List<Category>	categories;

			{
				pos = CategorySearcher.this.start;
				if (CategorySearcher.this.limit > 0) {
					end = start + CategorySearcher.this.limit;
				}
			}

			public boolean hasNext() {

				if (categories == null || categories.isEmpty()) {

					if (pos + 100 > end) {
						categories = execute(pos, end - pos);
					} else {
						categories = execute(pos, 100);
					}
				}

				return categories.size() > 0;
			}

			public Category next() {
				pos++;
				return categories.remove(0);
			}

			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	/**
	 * 
	 */
	public void setMaxParents(int maxParents) {
		this.maxParents = maxParents;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public void setVisible(boolean visible) {
		this.visibleOnly = visible;
	}

}
