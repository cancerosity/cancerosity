package org.sevensoft.ecreator.model.categories;

import java.util.List;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 05-Jul-2005 11:18:29
 * 
 */
@Table("categories_items")
public class CategoryItem extends EntityObject implements Positionable {

	/**
	 * 
	 */
	public static List<CategoryItem> get(RequestContext context, Category category) {
		return SimpleQuery.execute(context, CategoryItem.class, "category", category);
	}

	@Index()
	private Item	item;

	@Index()
	private Category	category;

	private int		position;

	public CategoryItem(RequestContext context) {
		super(context);
	}

	public CategoryItem(RequestContext context, Item item, Category category) {
		super(context);

		if (category == null) {
			return;
		}

		if (SimpleQuery.count(context, CategoryItem.class, "item", item, "category", category) == 0) {

			this.item = item;
			this.category = category;

			save();
		}
	}

	public final Category getCategory() {
		return category.pop();
	}

	public final Item getItem() {
		return item.pop();
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}
}
