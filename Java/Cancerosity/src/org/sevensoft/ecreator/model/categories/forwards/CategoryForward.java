package org.sevensoft.ecreator.model.categories.forwards;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 29 Dec 2006 13:38:01
 *
 */
@Table("categories_forwards")
public class CategoryForward extends EntityObject {

	/**
	 * The account or null for guests
	 */
	private ItemType	itemType;

	private String	url;

	private Category	category;

	protected CategoryForward(RequestContext context) {
		super(context);
	}
}
