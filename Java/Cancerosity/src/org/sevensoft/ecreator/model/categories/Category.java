package org.sevensoft.ecreator.model.categories;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.superstrings.comparators.NaturalStringComparator;
import org.sevensoft.ecreator.iface.admin.categories.CategoryHandler;
import org.sevensoft.ecreator.model.accounts.permissions.Permissions;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionsModule;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.AttachmentOwner;
import org.sevensoft.ecreator.model.attachments.AttachmentUtil;
import org.sevensoft.ecreator.model.attachments.blocks.AttachmentsBlock;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.attributes.*;
import org.sevensoft.ecreator.model.categories.blocks.ItemsBlock;
import org.sevensoft.ecreator.model.categories.blocks.SubcategoriesBlock;
import org.sevensoft.ecreator.model.categories.box.CategoryBox;
import org.sevensoft.ecreator.model.categories.db.CategoryAncestor;
import org.sevensoft.ecreator.model.categories.search.CategorySearcher;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.containers.blocks.BlockUtil;
import org.sevensoft.ecreator.model.containers.boxes.BoxWhere;
import org.sevensoft.ecreator.model.crm.forms.blocks.FormBlock;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.template.Template;
import org.sevensoft.ecreator.model.design.template.TemplateConfig;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.feeds.Feed;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.categories.CategoryReference;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.listings.ListingPackageCategory;
import org.sevensoft.ecreator.model.items.pricing.Price;
import org.sevensoft.ecreator.model.items.pricing.PriceBand;
import org.sevensoft.ecreator.model.items.pricing.PriceUtil;
import org.sevensoft.ecreator.model.items.pricing.Priceable;
import org.sevensoft.ecreator.model.items.sorts.ItemSort;
import org.sevensoft.ecreator.model.media.images.ImageOwner;
import org.sevensoft.ecreator.model.media.images.ImageSupport;
import org.sevensoft.ecreator.model.media.images.galleries.Gallery;
import org.sevensoft.ecreator.model.media.images.galleries.blocks.GalleryBlock;
import org.sevensoft.ecreator.model.media.videos.Video;
import org.sevensoft.ecreator.model.media.videos.VideoOwner;
import org.sevensoft.ecreator.model.media.videos.blocks.VideosBlock;
import org.sevensoft.ecreator.model.misc.Page;
import org.sevensoft.ecreator.model.misc.content.ContentBlock;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.misc.seo.Meta;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.ecreator.model.search.SearchOwner;
import org.sevensoft.ecreator.model.stats.StatablePage;
import org.sevensoft.ecreator.model.stats.hits.pages.PageHitCounterMonthly;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.installer.DefaultInstaller;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.model.comments.Comment;
import org.sevensoft.ecreator.model.comments.CommentsSettings;
import org.sevensoft.ecreator.model.comments.blocks.CommentPostBlock;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.QueryBuilder;
import org.sevensoft.jeezy.db.Row;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Selectable;

import java.io.IOException;
import java.io.File;
import java.net.URL;
import java.util.*;
import java.util.regex.Pattern;

/**
 * @author sks 28-Jul-2005 09:54:25
 */
@Table("categories")
public class Category extends ImageSupport implements StatablePage, ImageOwner, AttributeOwner, Selectable, Positionable, Meta, AttachmentOwner,
        Comparable<Category>, Permissions, Priceable, BlockOwner, Logging, SearchOwner, Page, VideoOwner {

    public enum ForwardWho {
        All, Guests, Members
    }

    public enum ProductOrdering {
        Manual, Name, Random;
    }

    public enum SubcategoryOrdering {
        Alphabetical, Manual;
    }

    public static boolean checkRoot(RequestContext context) {

        Category root = getRoot(context);
        if (root == null) {
            root = new Category(context, true);
            return true;
        }
        return false;
    }

    /**
     * Returns a list of all categories ordered alphabetically
     */
    public static List<Category> get(RequestContext context) {
        return get(context, true);
    }

    public static List<Category> get(RequestContext context, boolean limit) {
        Query q = new Query(context, "select * from # order by fullName");
        if (limit) {
            q.setLimit(2000);
        }
        q.setTable(Category.class);
        return q.execute(Category.class);
    }

    public static List<Category> get(RequestContext context, int start, int limit) {
        Query q = new Query(context, "select * from # order by fullName");
        q.setTable(Category.class);
        return q.execute(Category.class, start, limit);
    }

    /**
     * Returns a list of all categories ordered alphabetically
     */
    public static List<Category> getOrderedByName(RequestContext context) {
        Query q = new Query(context, "select * from # order by name limit 2000");
        q.setTable(Category.class);
        return q.execute(Category.class);
    }

    public static Category getByHash(RequestContext context, String hash) {
        Query q = new Query(context, "select * from # where hash=?");
        q.setTable(Category.class);
        q.setParameter(hash);
        return q.get(Category.class);
    }

    public static Category getByName(RequestContext context, String name) {

        if (name == null) {
            return null;
        }

        Query q = new Query(context, "select * from # where name=?");
        q.setParameter(name);
        q.setTable(Category.class);
        return q.get(Category.class);
    }

    public static Map<String, String> getCategoryOptions(RequestContext context) {
        return getCategoryOptions(context, null, 0, 5000);
    }

    public static Map<String, String> getCategoryOptions(RequestContext context, String name, int start, int limit) {

        QueryBuilder b = new QueryBuilder(context);
        b.select("id, replace(fullname, '|', ' > ') ");
        b.from("#", Category.class);
        b.order("fullname");
        b.limit(start, limit);

        if (name != null) {

            name = name.replaceAll("\\s", "%");
            b.clause(" name like ? ", "%" + name + "%");
        }

        return b.toQuery().getMap(String.class, String.class, new LinkedHashMap());
    }

    public static Map<String, String> getCategoryOptionsTruncated(RequestContext context) {

        Query q = new Query(context, "select id, replace(fullname, '|', ' > ') from # order by fullname limit 500");
        q.setTable(Category.class);
        Map<String, String> map = q.getMap(String.class, String.class, new LinkedHashMap<String, String>());
        Map<String, String> map2 = new LinkedHashMap<String, String>();
        for (Map.Entry<String, String> entry : map.entrySet()) {

            String id = entry.getKey();
            String name = entry.getValue();
            if (name.length() > 100)
                name = name.substring(name.length() - 100, name.length());

            map2.put(id, name);
        }
        return map2;
    }

    public static int getCount(RequestContext context) {
        return SimpleQuery.count(context, Category.class);
    }

    /**
     * Returns a list of categories that directly contain items which are live
     */
    public static List<Category> getItemContainers(RequestContext context) {
        Query q = new Query(context, "select * from # where liveItemsCount>0 order by name");
        q.setTable(Category.class);
        return q.execute(Category.class);
    }

    public static Map<String, String> getItemContainersMap(RequestContext context) {

        Query q = new Query(context, "select c.id, c.name from # c where c.liveItemsCount>0 order by c.name");
        q.setTable(Category.class);
        return q.getMap(String.class, String.class, new LinkedHashMap());
    }

    public static LinkedHashMap<String, String> getItemContainersMap(RequestContext context, int i, int j) {
        Query q = new Query(context, "select id, name from # where liveItemsCount>0 order by name limit " + i + ", " + j);
        q.setTable(Category.class);
        return (LinkedHashMap<String, String>) q.getMap(String.class, String.class, new LinkedHashMap());
    }

    public static Map<String, Category> getItemContainersMap1(RequestContext context, String orderby ) {

        if (orderby == null) {
            orderby = "c.parent, c.name";
        }

        Query q = new Query(context, "select c.id, c.* from # c where c.liveItemsCount>0 order by " + orderby);
        q.setTable(Category.class);
        return q.getMap(String.class, Category.class, new LinkedHashMap());
    }

    /**
     * Returns a map of category IDs mapped to category NAMEs for all categories that contain items that have ItemType of id=param
     */
    public static Map<String, String> getItemContainersMap(RequestContext context, ItemType itemType) {

        if (itemType == null)
            return getItemContainersMap(context);
        else
            return getItemContainersMap(context, itemType.getIdString());
    }

    public static Map<String, Category> getItemContainersMap1(RequestContext context, ItemType itemType, String orderby) {

        if (itemType == null)
            return getItemContainersMap1(context, orderby);
        else
            return getItemContainersMap1(context, itemType.getIdString(), orderby);
    }

    /**
     *
     */
    public static Map<String, String> getItemContainersMap(RequestContext context, String id) {

        Query q = new Query(context,
                "select c.id, c.name from # c join # co on c.id=co.category join # i on co.item=i.id where c.liveItemsCount>0 and i.itemType=? and i.status=? order by c.name");
        q.setTable(Category.class);
        q.setTable(CategoryItem.class);
        q.setTable(Item.class);
        q.setParameter(id);
        q.setParameter("Live");
        return q.getMap(String.class, String.class, new LinkedHashMap());
    }

    public static Map<String, Category> getItemContainersMap1(RequestContext context, String id, String orderby) {
        if (orderby == null) {
            orderby = "c.parent, c.name";
        }

        Query q = new Query(context,
                "select distinct(c.id), c.* from # c join # co on c.id=co.category join # i on co.item=i.id where c.liveItemsCount>0 and i.itemType=? and i.status=?" +
                        " order by "+ orderby);
        q.setTable(Category.class);
        q.setTable(CategoryItem.class);
        q.setTable(Item.class);
        q.setParameter(id);
        q.setParameter("Live");
        return q.getMap(String.class, Category.class, new LinkedHashMap());
    }


    /**
     * Returns a list of all categories that have a kelkoo category defined.
     */
    public static List<Category> getKelkoo(RequestContext context) {
        Query q = new Query(context, "select * from # where kelkooCategory is not null ");
        q.setTable(Category.class);
        return q.execute(Category.class);
    }

    /**
     * Returns a list of category names for use as keywords.
     * Only returns names that have length between 3 and max (inclusive)
     */
    public static List<String> getKeywords(RequestContext context, int max, int limit) {

        QueryBuilder b = new QueryBuilder(context);
        b.from("#", Category.class);
        b.select("distinct name");
        b.clause("length(name) > 2");

        if (max > 0)
            b.clause("length(name) <= ?", max);

        if (Seo.getInstance(context).isAutoContentLinkingOverride()) {
            b.clause("contentLinkingKeyword=1");
        }

        b.order("length(name) desc");

        return b.toQuery().getStrings(limit);
    }

    /**
     * Returns a list of all categories that have no children
     */
    public static List<Category> getLeafs(RequestContext context) {
        Query q = new Query(context, "select * from # where childCount=0 order by fullname");
        q.setTable(Category.class);
        return q.execute(Category.class);
    }

    /**
     * Returns a list of all categories that have permissions enabled
     */
    public static List<Category> getPermissions(RequestContext context) {
        Query q = new Query(context, "select * from # where permissions=1 order by fullname");
        q.setTable(Category.class);
        return q.execute(Category.class);
    }

    /**
     * Returns a list of primary categories
     */
    public static List<Category> getPrimary(RequestContext context) {
        return Category.getRoot(context).getChildren();
    }

    /**
     * Returns the root category
     */
    public static Category getRoot(RequestContext context) {

        Category root = (Category) context.getAttribute("root");
        if (root == null) {
            root = SimpleQuery.get(context, Category.class, "parent", 0);
            context.setAttribute("root", root);
        }

        return root;
    }

    public static Map<String, String> getSimpleMap(RequestContext context) {
        Query q = new Query(context, "select id, concat(repeat(' &nbsp; ', parentCount * 3), name) from # order by fullname");
        q.setTable(Category.class);
        return q.getLinkedHashMap(String.class, String.class);
    }

    public static Map<String, Category> getSimpleMap1(RequestContext context) {
        Query q = new Query(context, "select  concat(repeat(' &nbsp; ', c.parentCount * 3), c.name), c.* from # c order by c.fullname");
        q.setTable(Category.class);
        return q.getMap(String.class, Category.class, new LinkedHashMap());
    }

    public static Map<String, String> getSimpleMapVisible(RequestContext context) {
        Query q = new Query(context, "select id, concat(repeat(' &nbsp; ', parentCount * 3), name) from # where hidden=0 order by fullname");
        q.setTable(Category.class);
        return q.getLinkedHashMap(String.class, String.class);
    }

    /**
     * Repair category structure, dependancies, parent, child counts etc
     *
     * @param context
     */
    public static void repair(RequestContext context) {

        context.disableCache();
        SimpleQuery.delete(context, CategoryAncestor.class);

        Category root = Category.getRoot(context);
        root.doDependancies();

        for (Category category : Category.get(context)) {
            category.setLiveCount();
        }

    }

    public static int size(RequestContext context) {
        Query q = new Query(context, "select count(*) from #");
        q.setTable(Category.class);
        return q.getInt();
    }

    private ForwardWho forwardWho;

    /**
     * Should auto content keyword linking be enabled for this category ?
     */
    private boolean autoContentLinking;

    /**
     * Override standard template for this category only
     */
    @Deprecated
    private Template template;

    /**
     * The background image
     */
    private String background;

    /**
     * The number of child (sub) categories
     */
    private int childCount;

    private Category basketForwardCategory;

    /**
     * Markup applied to all subcategory blocks in subcategories of this category!
     */
    private Markup subcategoryMarkup;

    /**
     * Is this category going to be included in our list of keywords
     */
    private boolean contentLinkingKeyword;

    /**
     * Date the category was created in database
     */
    private DateTime dateCreated;

    /**
     * Last time the category was changed
     */
    private DateTime dateUpdated;

    private String descriptionTag;

    /**
     * number of file attachments
     */
    private int fileCount;

    /**
     *
     */
    private String footer;

    /*
      *Css that is automatically included with this category
      */
    private String css;

    /**
     * The url to forward to when using a category forward.
     */
    private String forwardUrl;

    /**
     * The name of this object concatenated with all its parents in order, separated by a pipe. Useful for showing the full trailed name in gui's
     */
    @Index()
    private String fullName;

    /**
     *
     */
    private String header;

    private String keywords;

    /**
     * Flags if we're to allow messages in this category.
     */
    private boolean messages;

    /**
     * Set a manual currency for items in this category
     */
    private Currency currency;

    /**
     * The immediate parent category
     */
    @Index()
    private Category parent;

    /**
     * number of parents
     */
    @Index()
    private int parentCount;

    /**
     * Position under the parent
     */
    @Index()
    private int position;

    /**
     *
     */
    private Category primaryParent;

    /**
     * Mark this category as restricted to enable access control for it
     */
    private boolean permissions;

    /**
     * Subcategory ordering method
     */
    private SubcategoryOrdering subcategoryOrdering;

    private String titleTag;

    /**
     * Lock down SEF URLS so changes of cat name won't change the url.
     */
    private String friendlyUrl;

    private boolean friendlyUrlLocked;

    private boolean supermanLock;

    /**
     * If flagged to true, then this SEFU will not change anymore, unless manually changed.
     */
    private boolean searchEngineUrlLocked;

    /**
     *
     */
    private int attributeCount;

    /**
     * Map this category to a kelkoo category for the kelkoo feed
     */
    private String kelkooCategory;

    @Index()
    private String name;

    /**
     * Number of live items of any item class in this category.
     */
    @Index()
    private int liveItemsCount;

    @Deprecated
    private String noItemsMessage;

    @Deprecated
    private int itemsPerPage;

    /**
     * Show sort options in this category.
     */
    @Deprecated
    private boolean sort;

    private TreeSet<Integer> priceBreakValues;

    /**
     * Enable pricing by different pricing levels- called member groups for posterity
     */
    private boolean memberGroupPricing;

    private String shopzillaCategory;

    private String summary;

    private boolean forum;

    /**
     * Hide this category so it does not appear in lists, but only used for manual linking
     */
    private boolean hidden;

    /**
     * A hash code generated to (try) to ensure this category is unique by name
     */
    private String hash;

    /**
     * Sets markers to be rendered in this category.
     * called renderTags for BC
     */
    private boolean renderTags;

    /**
     * Per category simple editor
     */
    private boolean simpleEditor;

    /**
     * Should we should resutls top and bottom - override by showResultsOnSinglePage
     */
    @Deprecated
    private boolean resultsTop;
    @Deprecated
    private boolean resultsBottom;

    @Deprecated
    private String content;

    /**
     * extra keywords
     */
    private List<String> suggestions;

    @Deprecated
    private Markup listItemMarkup;

    /**
     * A list item markup that applies to subcategories of this category
     */
    private Markup subcategoryListItemMarkup;

    @Deprecated
    private boolean includeSubcategoryItems;

    private boolean searchWrapper;

    private transient List<Category> childrenCache;

    @Deprecated
    private ItemSort itemSort;

    private String restrictionForwardUrl;

    /**
     * Use a manual items order in this category
     */
    private boolean manualItemsOrder;

    private transient List<Category> parents;

    private transient List<Price> prices;

    private int videoCount;

    private boolean advanced;

    private boolean useManualSort;

    private Amount discount;

    private boolean hideDiscountInfo;

    private List<String> contentLinkingKeywords;

    protected Category(RequestContext context) {
        super(context);
    }

    /**
     * Create the root category
     */
    private Category(RequestContext context, boolean root) {
        super(context);

        this.name = "Home Page";
        this.fullName = "Home Page";

        this.dateCreated = new DateTime();
        this.autoContentLinking = true;
//        this.comments = CommentsSettings.getInstance(context).isAddToCategories();

        save();

        // content block
        Block block = new ContentBlock(context, this, 0);
        block.setPosition(2);
        block.save();

        // items block
        block = new ItemsBlock(context, this, 0);
        block.setPosition(3);
        block.save();

        // attachments
        block = new AttachmentsBlock(context, this, 0);
        block.setPosition(4);
        block.save();
    }

    public Category(RequestContext context, String name, Category p) {

        this(context, true);

        if (p == null) {

            this.parent = Category.getRoot(context);

        } else {

            this.parent = p;
        }

        this.primaryParent = parent.getPrimaryParent();

        save();

        setName(name);

        // create subcategories block
        Block block = new SubcategoriesBlock(context, this, 0);
        block.setPosition(1);
        block.save();

        // update parent's child count
        this.parent.setChildCount();
    }

    public boolean acceptedFiletype(String filename) {
        return true;
    }

    public Attachment addAttachment(Attachment attachment) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException {
        return new Attachment(context, this, attachment);
    }

    public Attachment addAttachment(File file, String fileName) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException {
        return null;
    }

    public Attachment addAttachment(Upload upload, boolean sample) throws IOException, AttachmentLimitException, AttachmentExistsException,
            AttachmentLimitException, AttachmentTypeException {

        return new Attachment(context, this, upload, false);
    }

    public Attachment addAttachment(URL url) throws IOException, AttachmentTypeException, AttachmentExistsException, AttachmentLimitException {

        return new Attachment(context, this, url, false);
    }

    public Attribute addAttribute(String name, AttributeType type) {

        return new Attribute(context, this, name, type);
    }

    public List<Attribute> addAttributes(String newAttributes, String newAttributesSection, int newAttributesPage) {
        return AttributeOwnerUtil.addAttributes(this, newAttributes, newAttributesSection, newAttributesPage);
    }

    public void addBlock(String blockString) {
        BlockUtil.addBlock(context, this, blockString);
    }

    public void addGalleryBlock(Gallery gallery) {
        new GalleryBlock(context, this, gallery);
    }

    public Video addVideo(String filename) throws IOException {
        return new Video(context, this, filename);
    }

    public Video addVideo(Upload upload) throws IOException {
        return new Video(context, this, upload);
    }

    public void clearPriceCaches() {
        PriceUtil.clearPriceCaches(context);
    }

    public int compareTo(Category o) {
        return NaturalStringComparator.instance.compare(name, o.name);
    }

    public void copyAttachmentsTo(AttachmentOwner target) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException {
        AttachmentUtil.copyAttachmentsTo(this, target, context);
    }

    @Override
    public synchronized boolean delete() {
        throw new UnsupportedOperationException();
    }

    /**
     *
     */
    public synchronized void delete(boolean contents) {

        logger.fine("[Category] deleting cat=" + this);

        if (isRoot()) {
            logger.fine("[Category] cannot delete root");
            throw new UnsupportedOperationException();
        }

        for (Category child : getChildren()) {
            logger.fine("[Category] deleting child=" + child);
            child.delete(contents);
        }

        if (contents) {
            logger.fine("[Category] deleting contents");
            deleteItems();
        }

        removeItems();
        removeAttributes();
        removeAttachments();
        removeBlocks();
        removeFromSeaches();
        removeStats();

        // remove as root from feeds
        for (Feed feed : Feed.getAll(context)) {
            if (feed instanceof ImportFeed) {
                new Query(context, "update # set root=0 where root=?").setTable(feed.getClass()).setParam(this).run();
            }
        }

        // remove as default category from item types
        new Query(context, "update # set defaultCategory=0 where defaultCategory=?").setTable(ItemType.class).setParam(this).run();

        // remove from searches
        new Query(context, "delete from # where category=?").setTable(Search.class).setParameter(this).run();

        // remove feed mappings
        new Query(context, "delete from # where category=?").setTable(CategoryReference.class).setParameter(this).run();

        // updatae category boxes
        new Query(context, "update # set root=0 where root=?").setTable(CategoryBox.class).setParameter(this).run();

        SimpleQuery.delete(context, TemplateConfig.class, "category", this);

        // delete from box wheres
        new Query(context, "delete from # where category=?").setTable(BoxWhere.class).setParameter(this).run();

        // delete from listng packages
        SimpleQuery.delete(context, ListingPackageCategory.class, "category", this);

        // delete from ancestors table
        SimpleQuery.delete(context, CategoryAncestor.class, "category", this);

        // delete from ancestors table
        SimpleQuery.delete(context, CategoryAncestor.class, "ancestor", this);

        super.delete();

        // clear category hmtl cache

        SubcategoriesBlock.clearHtmlCache();

        // update parent's child count
        parent.pop();
        parent.setChildCount();
    }

    /**
     * Delete all items in this category (sets all items status to deleted)
     */
    private void deleteItems() {

        Query q = new Query(context, "update # i join # ci on i.id=ci.item set i.status=? where ci.category=?");
        q.setTable(Item.class);
        q.setTable(CategoryItem.class);
        q.setParameter("Deleted");
        q.setParameter(this);
        q.run();
    }

    /**
     *
     */
    private void deleteSearch() {
        SimpleQuery.delete(context, Search.class, "category", this);
    }

    public void doDependancies() {

        logger.fine("[Category] doDependancies");

        // remove from ancestors table
        SimpleQuery.delete(context, CategoryAncestor.class, "category", this);

        if (isChild()) {

            if (getParent().isRoot()) {
                primaryParent = null;
            } else {
                primaryParent = parent.getPrimaryParent();
            }

            logger.fine("[Category] primaryParent=" + primaryParent);

            fullName = parent.getFullName() + "|" + name;
            logger.fine("[Category] fullname=" + fullName);
            parentCount = parent.getParentCount() + 1;
            logger.fine("[Category] parentcount=" + parentCount);
            save();

            Category p = parent;
            while (p != null) {
                new CategoryAncestor(context, this, p);
                logger.fine("[Category] creating ancestor=" + p);
                p = p.getParent();
            }
        }

        setChildCount();

        if (hasChildren()) {
            for (Category child : getChildren()) {
                child.doDependancies();
            }
        }
    }

    /**
     * Flags all baskets containing items in this category for a recalc
     */
    public void flagBaskets() {

        Query q = new Query(context, "update # b join # bl on b.id=bl.basket join # ci on bl.item=ci.item set b.recalc=1 where ci.category=?");
        q.setTable(Basket.class);
        q.setTable(BasketLine.class);
        q.setTable(CategoryItem.class);
        q.setParameter(this);
        q.run();
    }

    /**
     * Returns a list of categories that contain items of item type param.
     *
     * @param type
     * @return
     */
    public List<Category> get(ItemType type) {
        return Collections.emptyList();
    }

    public String getAlt() {
        return name;
    }

    public int getAttachmentCount() {
        return fileCount;
    }

    public int getAttachmentLimit() {
        return 0;
    }

    public List<Attachment> getAttachments() {
        return AttachmentUtil.getAttachments(context, this);
    }

    public Attribute getAttribute(String name) {
        return AttributeOwnerUtil.getAttribute(this, name);
    }

    public int getAttributeCount() {
        return attributeCount;
    }

    //old
    /*public List<Attribute> getAttributes() {
        return AttributeOwnerUtil.getAttributes(context, this);
    }*/
    public List<Attribute> getAttributes() {
        Query q = new Query(context, "select * from # where category=? and itemtype=0 and itemtypesettings=0 order by position");
        q.setTable(Attribute.class);
        q.setParameter(this);
        return q.execute(Attribute.class);
    }


    public List<Attribute> getAttributesAvailable() {
        Query q = new Query(context, "select * from # where category=? or category in (select ancestor from # where category=?) order by position");
        q.setTable(Attribute.class);
        q.setTable(CategoryAncestor.class);
        q.setParameter(this);
        q.setParameter(this);
        return q.execute(Attribute.class);
    }

    public String getBackground() {
        return background;
    }

    public final Category getBasketForwardCategory() {
        return (Category) (basketForwardCategory == null ? null : basketForwardCategory.pop());
    }

    public List<Block> getBlocks() {

        List<Block> blocks = Block.get2(context, this);

        // ensure we have a content block
        if (!Block.contains(ContentBlock.class, blocks)) {
            new ContentBlock(context, this, 0);
        }

        // ensure we have a items block
        if (!Block.contains(ItemsBlock.class, blocks)) {
            new ItemsBlock(context, this, 0);
        }

        // ensure we have a Attachments block
        if (CategoryModule.Attachments.enabled(context)) {
            if (!Block.contains(AttachmentsBlock.class, blocks)) {
                new AttachmentsBlock(context, this, 0);
            }
        }

        // ensure we have a videos block
        if (CategoryModule.Videos.enabled(context)) {
            if (!Block.contains(VideosBlock.class, blocks)) {
                new VideosBlock(context, this, 0);
            }
        }

        // ensure we have a Comments block
        if (Module.Comments.enabled(context) && CommentsSettings.getInstance(context).isAddToCategories()) {
            if (!Block.contains(CommentPostBlock.class, blocks)) {
                blocks.add(new CommentPostBlock(context, this, 0));
            }
        }

        // get items block and fix deprecated values 05/04/07
        if (itemsPerPage > 0 || itemSort != null || includeSubcategoryItems || resultsTop || resultsBottom || noItemsMessage != null || sort ||
                listItemMarkup != null) {

            for (Block block : blocks) {

                if (block instanceof ItemsBlock) {

                    ItemsBlock itemsBlock = (ItemsBlock) block;

                    itemsBlock.setIncludeSubcategoryItems(includeSubcategoryItems);
                    itemsBlock.setSort(itemSort);
                    itemsBlock.setItemsPerPage(itemsPerPage);
                    itemsBlock.setResultsBottom(resultsBottom);
                    itemsBlock.setResultsTop(resultsTop);
                    itemsBlock.setNoItemsMessage(noItemsMessage);
                    itemsBlock.setShowSorts(sort);
                    itemsBlock.setListMarkup(listItemMarkup);
                    itemsBlock.save();

                    itemsPerPage = 0;
                    itemSort = null;
                    includeSubcategoryItems = false;
                    resultsTop = false;
                    resultsBottom = false;
                    noItemsMessage = null;
                    listItemMarkup = null;
                    sort = false;
                    save();
                }
            }
        }

        return blocks;
    }

    public List<CategoryItem> getCategoryItems() {
        return CategoryItem.get(context, this);
    }

    /**
     * Returns the first category which is a subcategory of this category
     * and also equals the name param
     */
    public Category getChild(String name) {

        if (isRoot()) {
            CategorySearcher searcher = new CategorySearcher(context);
            searcher.setName(name);
            return searcher.get();
        }

        Query q = new Query(context, "select c.* from # c join # ca on c.id=ca.category where ca.ancestor=? and c.name=?");
        q.setTable(Category.class);
        q.setTable(CategoryAncestor.class);
        q.setParameter(this);
        q.setParameter(name);
        return q.get(Category.class);
    }

    public int getChildCount() {
        return childCount;
    }

    public List<Category> getChildren() {

        if (childCount == 0) {
            return Collections.emptyList();
        }
                                     // in case of prev code errors
        if (childrenCache == null || childCount != childrenCache.size()) {

            String query = "select * from # where parent=? order by ";

            if (CategorySettings.getInstance(context).isSubcategoryOrdering()) {

                if (subcategoryOrdering == null) {
                    subcategoryOrdering = SubcategoryOrdering.Alphabetical;
                }

                switch (this.subcategoryOrdering) {

                    default:
                    case Alphabetical:
                        query += "name";
                        break;

                    case Manual:
                        query += "position";
                        break;
                }

            } else {

                query += "name";

            }

            Query q = new Query(context, query);
            q.setTable(getClass());
            q.setParameter(this);

            childrenCache = q.execute(Category.class);
            /*if (SubcategoryOrdering.Alphabetical == subcategoryOrdering) {
                Collections.sort(childrenCache);
            }*/

        }

        return childrenCache;
    }

    /**
     * Returns all categories that begin with this name and are subcategories of this category
     */
    public List<Category> getChildren(String name) {

        if (isRoot()) {
            CategorySearcher searcher = new CategorySearcher(context);
            searcher.setName(name);
            return searcher.getCategories();
        }

        Query q = new Query(context, "select c.* from # c join # ca on c.id=ca.category " + "where ca.ancestor=? and c.name=?");
        q.setTable(Category.class);
        q.setTable(CategoryAncestor.class);
        q.setParameter(this);
        q.setParameter(name);
        return q.execute(Category.class);
    }

    /**
     * Returns a List<Category> of all children not just the immediate children.
     */
    public List<Category> getChildrenDeep() {
        return getChildrenDeep(0);
    }

    /**
     * Returns get children up to a max depth
     */
    public List<Category> getChildrenDeep(int depth) {

        if (childCount == 0)
            return Collections.emptyList();

        Query q = new Query(context, "select c.* from # c join # ca on c.id=ca.category where ca.ancestor=? and c.parentCount<=? order by c.fullName");
        q.setTable(Category.class);
        q.setTable(CategoryAncestor.class);
        q.setParameter(this);
        q.setParameter(depth == 0 ? 9999 : (depth + getParentCount()));
        return q.execute(Category.class);
    }

    /**
     * Returns the first content block
     * <p/>
     * Categories always have at least one content block so this method will create one if none exists
     */
    public ContentBlock getContentBlock() {

        ContentBlock cb = SimpleQuery.get(context, ContentBlock.class, "ownerCategory", this);
        if (cb == null) {
            cb = new ContentBlock(context, this, 0);
        }

        /*
           * If we have content in this category then it means this category was created prior to the content block system.
           * We can copy this content into the content block BUT if the content block already has content, then do not do this
           */
        if (content != null) {

            if (!cb.hasContent()) {
                cb.setContent(content);
            }

            content = null;
            save("content");
        }

        return cb;
    }

    public List<ContentBlock> getContentBlocks() {
        return SimpleQuery.execute(context, ContentBlock.class, "ownerCategory", this);
    }

    public String getContentDeprecated() {
        return content;
    }

    public final String getCss() {
        return css;
    }

    public final Currency getCurrency() {
        return currency;
    }

    public DateTime getDateCreated() {
        if (dateCreated == null) {
            dateCreated = new DateTime();
            save("dateCreated");
        }
        return dateCreated;
    }

    public DateTime getDateUpdated() {
        if (dateUpdated == null) {
            dateUpdated = new DateTime();
            save("dateUpdated");
        }
        return dateUpdated;
    }

    public String getDescriptionTag() {
        return descriptionTag;
    }

    public Link getEditRelativeLink() {
        return new Link(CategoryHandler.class, "edit", "category", this);
    }

    /**
     * Returns a mapping of attributes to values that are set on items in this category.
     * <p/>
     * It shows all the values for each attribute set on items that reside in this category.
     * <p/>
     * If an attribute value is set on a item but the item is not in this category then it does not return that value.
     * <p/>
     * It only include live items in its search
     *
     * @return
     */
    public MultiValueMap<Attribute, String> getFilters() {

        Query q = new Query(context, "select distinct a.id, av.value from # av join # a join (select ancestor from # where category=? union select ?) z "
                + "where a.filter=1 and av.attribute=a.id and a.category=z.ancestor order by a.position, a.name, av.value");

        q.setTable(AttributeValue.class);
        q.setTable(Attribute.class);
        q.setTable(CategoryAncestor.class);

        q.setParameter(this);
        q.setParameter(this);

        Attribute attribute = null;
        MultiValueMap<Attribute, String> map = new MultiValueMap(new LinkedHashMap());
        for (Row row : q.execute()) {

            if (attribute == null)
                attribute = row.getObject(0, Attribute.class);

            if (attribute == null)
                throw new RuntimeException("Invalid attribute data in database");

            if (attribute.getId() != row.getInt(0))
                attribute = row.getObject(0, Attribute.class);

            map.put(attribute, row.getString(1));
        }

        return map;
    }

    /**
     *
     */
    public String getFirstContent() {
        return getContentBlock().getContent();
    }

    public String getFooter() {
        return footer;
    }

    public String getForwardUrl() {
        return forwardUrl;
    }

    public ForwardWho getForwardWho() {
        return forwardWho == null ? ForwardWho.All : forwardWho;
    }

    public String getFriendlyUrl() {

        if (friendlyUrl == null || !friendlyUrl.endsWith("html")) {
            makeFriendlyUrl();
            save("friendlyUrl");
        }

        return friendlyUrl;
    }

    public String getFullName() {
        return fullName;
    }

    public String getFullName(boolean root, String delimiter) {

        String string = fullName;
        if (!root)
            string = string.replaceFirst(".*?\\|", "");

        return string.replace("|", delimiter);
    }

    public String getFullName(String delimiter) {
        return fullName == null ? null : fullName.replace("|", delimiter);
    }

    public String getHeader() {
        return header;
    }

    public int getImageLimit() {
        return 0;
    }

    public Map<String, String> getItemsOptions(RequestContext context) {
        Query q = new Query(context, "select i.friendlyUrl, i.name from # i join # ci on i.id=ci.item where ci.category=?");
        q.setTable(Item.class);
        q.setTable(CategoryItem.class);
        q.setParameter(this);

        return q.getMap(String.class, String.class, new LinkedHashMap());
    }

    /**
     * Returns all items regardless of status and regardless of type
     */
    public List<Item> getItems() {
        return getItems(null, null, 0);
    }

    /**
     *
     */
    public List<Item> getItems(int limit) {
        return getItems(null, null, limit);
    }

    /**
     * Returns all items in this category.
     */
    public List<Item> getItems(ItemType itemType, String status, int limit) {

        ItemSearcher searcher = getItemSearcher(itemType, status, limit);

        return searcher.getItems();
    }

    public Integer getItemsCount(ItemType itemType, String status, int limit) {
        ItemSearcher searcher = getItemSearcher(itemType, status, limit);

        return searcher.getItemsCount();
    }

    private ItemSearcher getItemSearcher(ItemType itemType, String status, int limit) {
        ItemSearcher searcher = new ItemSearcher(context);
        searcher.setSearchCategory(this);
        searcher.setStatus(status);
        searcher.setItemType(itemType);
        searcher.setLimit(limit);
        return searcher;
    }

    /**
     * Returns a list of item classes that are defined on items residing in this category.
     */
    public List<ItemType> getItemTypes() {
        Query q = new Query(context, "select ic.* from # ic join # i on ic.id=i.itemType join # ci on i.id=ci.item where ci.category=?");
        q.setTable(ItemType.class);
        q.setTable(Item.class);
        q.setTable(CategoryItem.class);
        q.setParameter(this);
        return q.execute(ItemType.class);
    }

    public String getKelkooCategory() {
        return kelkooCategory;
    }

    public String getKeyword() {
        return name;
    }

    public String getKeywords() {
        return keywords;
    }

    public String getLabel() {
        String string = getFullName(" > ");
        if (isForward()) {
            string = string + " -> " + forwardUrl;
        }

        if (string.length() > 100) {
            string = "..." + string.substring(string.length() - 100);
        }

        return string;
    }

    public LinkTag getLinkTag() {
        return new LinkTag(getUrl(), getName());
    }

    public int getLiveItemsCount() {
        return liveItemsCount;
    }

    public List<LogEntry> getLogEntries() {
        return LogEntry.get(context, this);
    }

    public String getLogId() {
        return getFullId();
    }

    public String getLogName() {
        return "Category #" + getIdString();
    }

    public String getName() {
        return name;
    }

    /**
     * Returns the name padded with spaces for each parent
     */
    public String getPaddedName() {
        return StringHelper.repeat("&nbsp;", parentCount * 5) + getName();
    }

    public Category getParent() {

        if (parent == null) {
            return null;
        }

        if (parent.getId() == getId()) {
            parent = getRoot(context);
            save("parent");
        }

        parent.pop();
        return parent;
    }

    /**
     * Returns the number of parents of this category
     */
    public int getParentCount() {
        return parentCount;
    }

    /**
     * Returns a List<Category> of all parents in order with the top most parent at position 0 and the immediate parent at position n
     */
    public List<Category> getParents(boolean includeRoot) {

        if (parents == null) {

            QueryBuilder b = new QueryBuilder(context);
            b.select("c.*");
            b.from("# c", Category.class);
            b.from("join # ca on c.id=ca.ancestor", CategoryAncestor.class);
            b.clause("ca.category=?", this);

            if (!includeRoot) {
                b.clause("c.parentCount>0");
            }

            b.order("c.fullname");
            parents = b.execute(Category.class);

        }

        return parents;
    }

    /**
     * Returns a list of parents with the immediate parent at position 0 and root or primary at position n
     */
    public List<Category> getParentsReversed(boolean includeRoot) {
        List<Category> parents = getParents(includeRoot);
        Collections.reverse(parents);
        return parents;
    }

    public PermissionsModule getPermissionsModule() {
        return PermissionsModule.get(context, this);
    }

    public int getPosition() {
        return position;
    }

    public List<Category> getPossibleParents() {

        List<Category> list = Category.get(context);
        list.remove(this);
        list.removeAll(getChildrenDeep());

        return list;
    }

    public TreeSet<Integer> getPriceBreaks() {

        if (priceBreakValues == null) {
            priceBreakValues = new TreeSet();
        }

        priceBreakValues.add(1);
        return priceBreakValues;
    }

    public List<Price> getPrices() {
        if (prices == null) {
            prices = SimpleQuery.execute(context, Price.class, "category", this);
        }
        return prices;
    }

    /**
     * Returns the top level parent of this category. If this category is already a root category then it returns itself.
     */
    public Category getPrimaryParent() {

        if (isRoot()) {
            return this;
        }

        if (isPrimary()) {
            return this;
        }

        return primaryParent.pop();
    }

    public String getRestrictionForwardUrl() {
        return restrictionForwardUrl;
    }

    public Search getSearch() {
        Search search = Search.get(context, this);
        return search == null ? new Search(context, this) : search;
    }

    public String getShopzillaCategory() {
        return shopzillaCategory;
    }

    public List<Category> getSubcategories() {
        return getChildren();
    }

    public Markup getSubcategoryListItemMarkup() {
        return (Markup) (subcategoryListItemMarkup == null ? null : subcategoryListItemMarkup.pop());
    }

    public Markup getSubcategoryMarkup() {
        return (Markup) (subcategoryMarkup == null ? null : subcategoryMarkup.pop());
    }

    public SubcategoryOrdering getSubcategoryOrdering() {
        return subcategoryOrdering == null ? SubcategoryOrdering.Alphabetical : subcategoryOrdering;
    }

    public List<String> getSuggestions() {
        if (suggestions == null) {
            suggestions = new ArrayList();
        }
        return suggestions;
    }

    public String getSummary() {
        return summary;
    }

    /**
     * Returns a usable summary even if the user has not entered a summary directly.
     */
    public String getSummary(int limit, String suffix) {

        if (summary == null) {

            return StringHelper.toSnippet(getContentBlock().getContentStripped(), limit, suffix);

        } else {

            return StringHelper.toSnippet(summary, limit, suffix);
        }
    }

    public TemplateConfig getTemplateConfig() {

        TemplateConfig tc = TemplateConfig.get(context, this);

        //no need to create empty template
        //default template later read
        /*
        if (template != null) {

            tc.setTemplate((Template) template.pop());
            tc.save();

            template = null;
            save("template");
        }
        */

        return tc;
    }

    public String getTitleTag() {
        return titleTag;
    }

    /**
     * Returns a trail of the category parent structure with primary at top and this category at end of list
     */
    public List<Category> getTrail() {

        if (isRoot()) {
            return Collections.emptyList();
        }

        List<Category> trail = new ArrayList();
        Category cat = this;
        while (cat != null) {

            if (cat.isRoot()) {
                break;
            }

            trail.add(0, cat);
            cat = cat.getParent();
        }

        return trail;
    }

    public String getTreeName(String string) {
        StringBuilder sb = new StringBuilder();
        for (int n = 0; n < parentCount; n++) {
            sb.append(string);
        }
        sb.append(name);
        return sb.toString();
    }

    public String getUrl() {
        return Config.getInstance(context).getUrl() + "/" + getFriendlyUrl();
    }

    public String getValue() {
        return getIdString();
    }

    public List<Video> getVideos() {
        return Video.get(context, this);
    }

    public List<Category> getVisibleChildren() {

        List<Category> children = getChildren();
        CollectionsUtil.filter(children, new Predicate<Category>() {

            public boolean accept(Category e) {
                return e.isVisible();
            }
        });

        return children;
    }

    public boolean hasAttachments() {
        return fileCount > 0;
    }

    public boolean hasAttributes() {
        return attributeCount > 0;
    }

    public boolean hasBackground() {
        return background != null;
    }

    public boolean hasBasketForwardCategory() {
        return basketForwardCategory != null;
    }

    public boolean hasChildren() {
        return childCount > 0;
    }

    public boolean hasCss() {
        return css != null;
    }

    public boolean hasCurrency() {
        return currency != null;
    }

    public boolean hasDescriptionTag() {
        return descriptionTag != null;
    }

    public boolean hasFooter() {
        return footer != null && CategoryModule.Headers.enabled(context);
    }

    public boolean hasForm() {
        Query q = new Query(context, "select count(*) from # where ownerCategory=? and form>0");
        q.setTable(FormBlock.class);
        q.setParameter(this);
        return q.getInt() > 0;
    }

    /**
     * Returns true if this category is a forwarding category.
     *
     * @return
     */
    public boolean hasForwardUrl() {
        return forwardUrl != null && CategoryModule.Forwards.enabled(context);
    }

    public boolean hasHeader() {
        return header != null && CategoryModule.Headers.enabled(context);
    }

    public boolean hasKelkooCategory() {
        return kelkooCategory != null;
    }

    public boolean hasKeywords() {
        return keywords != null;
    }

    /**
     * Returns true if this category contains live items of any type
     *
     * @return
     */
    public boolean hasLiveItems() {
        return liveItemsCount > 0;
    }

    /**
     *
     */
    public boolean hasPriceBreaks() {
        return getPriceBreaks().size() > 1;
    }

    public boolean hasShopzillaCategory() {
        return shopzillaCategory != null;
    }

    public boolean hasSubcategories() {
        return childCount > 0;
    }

    /**
     *
     */
    public boolean hasSubcategoryMarkup() {
        return subcategoryMarkup != null;
    }

    public boolean hasSummary() {
        return summary != null;
    }

    public boolean hasTitleTag() {
        return titleTag != null;
    }

    public final boolean isAdvanced() {
        return advanced;
    }

    public boolean isAdvancedEditor() {
        return !simpleEditor;
    }

    public boolean isAtAttachmentLimit() {
        return false;
    }

    public boolean isAutoContentLinking() {

        if (!Seo.getInstance(context).isAutoContentLinking()) {
            return false;
        }

        if (Seo.getInstance(context).isAutoContentLinkingOverride()) {
            return autoContentLinking;
        }

        return true;
    }

    public boolean isBranch() {
        return childCount > 0;
    }

    public boolean isChild() {
        return !isRoot();
    }

    /**
     * Returns true if this category is a child, immediate or otherwise, of the category param. Is the same as c.isParentOf(this)
     */
    public boolean isChildOf(Category ancestor) {
        Query q = new Query(context, "select count(*) from # where category=? and ancestor=?");
        q.setTable(CategoryAncestor.class);
        q.setParameter(this);
        q.setParameter(ancestor);
        return q.getInt() > 0;
    }

    public boolean isContentLinkingKeyword() {

        if (!Seo.getInstance(context).isAutoContentLinkingOverride()) {
            return true;
        }

        return contentLinkingKeyword;
    }

    public boolean isDeletable() {
        return !isRoot() && !hasChildren();
    }

    public boolean isForum() {
        return forum;
    }

    private boolean isForward() {
        return forwardUrl != null;
    }

    public boolean isFriendlyUrlLocked() {
        return friendlyUrlLocked;
    }

    public boolean isHidden() {

        // root should never be hidden
        if (isRoot()) {
            return false;
        }

        if (!hidden) {
            return false;
        }

        // only hidden if also enabled in cat settings
        return CategorySettings.getInstance(context).isHidden();
    }

    public boolean isLeaf() {
        return childCount == 0;
    }

    public final boolean isManualItemsOrder() {
        return manualItemsOrder;
    }

    public boolean isMemberGroupPricing() {
        return PriceUtil.isMemberGroupPricing(memberGroupPricing, context);
    }

    public boolean isMessages() {
        return messages;
    }

    /**
     * Returns true if this category is a parent at any level of the category param
     */
    public boolean isParentOf(Category c) {
        return c.isChildOf(this);
    }

    public final boolean isPermissions() {

        // only children can have permissions
        return isChild() && permissions && Module.Permissions.enabled(context);
    }

    public boolean isPriceBreaks() {
        return getPriceBreaks().size() > 1;
    }

    public boolean isPrimary() {
        return parentCount == 1;
    }

    public boolean isRenderTags() {
        return renderTags;
    }

    /**
     * Returns true if this is the home page
     */
    public boolean isRoot() {
        return parent == null;
    }

    public boolean isSearchEngineUrlLocked() {
        return searchEngineUrlLocked;
    }

    public boolean isSearchWrapper() {
        return searchWrapper;
    }

    public boolean isSimpleEditor() {
        return simpleEditor;
    }

    public boolean isSimpleListings() {
        return false;
    }

    public final boolean isSupermanLock() {
        return supermanLock;
    }

    /**
     * Returns true if this category is not hidden from general browsing.
     */
    public boolean isVisible() {
        return !isHidden();
    }

    /**
     * Returns true if this category is visible and its parent is visible
     */
    public boolean isVisibleTrail() {

        if (isHidden()) {
            return false;
        }

        if (isRoot()) {
            return true;
        }

        return getParent().isVisibleTrail();
    }

    public void lockFriendlyUrl() {
        if (!friendlyUrlLocked) {
            friendlyUrlLocked = true;
            save();
        }
    }

    //	public void removePriceDefinition(PriceBand priceGroup, int qty, Money costPriceFrom, Money costPriceTo) {
    //		PriceUtil.removePriceDefinition(context, this, priceGroup, qty);
    //	}

    public void log(String message) {
        new LogEntry(context, this, message);
    }

    public void log(User user, String message) {
        new LogEntry(context, this, user, message);
    }

    private void makeFriendlyUrl() {

        friendlyUrl = Seo.getFriendlyUrl(getName()) + "-c" + getIdString() + ".html";

        //		friendlyUrl = "c/" + Seo.getFriendlyUrl(getName()) + "/" + getIdString() + ".html";

        //		StringBuilder sb = new StringBuilder();
        //		for (Category category : getParents(false)) {
        //			sb.append(Seo.getFriendlyUrl(category.getName()));
        //			sb.append("/");
        //		}
        //		sb.append(Seo.getFriendlyUrl(getName()));
        //		sb.append("/c");
        //		sb.append(getIdString());
        //		sb.append(".html");

        //		friendlyUrl = sb.toString();
    }

    public void mergeInto(Category mergeInto) {

        // move children
        for (Category child : getChildren()) {
            child.setParent(mergeInto);
        }

        // move items
        for (Item item : getItems()) {
            item.removeCategory(this);
            item.addCategory(mergeInto);
        }

        // update any feed mappings
        Query q = new Query(context, "update # set category=? where category=?");
        q.setTable(CategoryReference.class);
        q.setParameter(mergeInto);
        q.setParameter(this);
        q.run();

        // remove things
        removeImages();
        removeBlocks();
        removeFromSeaches();
        deleteSearch();
        removePrices();
        removeStats();

        // shift attachments
        try {
            moveAttachmentsTo(mergeInto);

        } catch (AttachmentExistsException e) {
            e.printStackTrace();

        } catch (AttachmentTypeException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();

        } catch (AttachmentLimitException e) {
            e.printStackTrace();
        }

        // shift attributes

        super.delete();
    }

    public void moveAttachmentsTo(AttachmentOwner target) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException {
        AttachmentUtil.moveAttachmentsTo(context, this, target);
    }

    /**
     * Moves all children from this category to the target as new parrent
     */
    public void moveChildrenTo(Category target) {

        for (Category child : getChildren()) {
            child.setParent(target);
        }

    }

    public void removeAttachment(Attachment file) {
        file.delete();
    }

    public void removeAttachments() {
        for (Attachment file : getAttachments())
            file.delete();
    }

    public void removeAttribute(Attribute attribute) {
        attribute.delete();
        setAttributeCount();
        save();
    }

    private void removeAttributes() {

        for (Attribute attribute : getAttributes())
            attribute.delete();
    }

    public void removeBlock(Block block) {
        block.delete();
    }

    private void removeBlocks() {
        for (Block block : getBlocks()) {
            removeBlock(block);
        }
    }

    private void removeFromSeaches() {
        new Query(context, "update # set searchCategory=0 where searchCategory=?").setParameter(this).setTable(Search.class).run();
    }

    private void removeItems() {

        for (Item item : getItems())
            item.removeCategory(this);
    }

    public void removePrices() {
        PriceUtil.removePriceDefinitions(context, this);
    }

    private void removeStats() {

        // remove from page counter
        new Query(context, "delete from # where category=?").setTable(PageHitCounterMonthly.class).setParameter(this).run();

    }

    public void removeVideo(Video video) {
        video.delete();
        this.videoCount = Video.getCount(context, this);
        save();
    }

    public void rename(String target, String replacment) {

        Pattern pattern = Pattern.compile(target, Pattern.LITERAL | Pattern.CASE_INSENSITIVE);
        this.name = pattern.matcher(name).replaceAll(replacment);

        save("name");

        doDependancies();
    }

    public void resetSearchEngineFriendlyUrl() {
        friendlyUrl = null;
    }

    @Override
    public synchronized void save() {
        this.dateUpdated = new DateTime();
        super.save();

        // clear category hmtl cache

        SubcategoriesBlock.clearHtmlCache();
    }

    @Override
    public void schemaInit(RequestContext context) {

        /*
           * Check that root exists.
           */
        if (checkRoot(context)) {
            new DefaultInstaller(context).install();
        }
    }

    public void setAdvanced(boolean advanced) {
        this.advanced = advanced;
    }

    public void setAttachmentCount() {
        this.fileCount = getAttachments().size();
    }

    public void setAttributeCount() {
        this.attributeCount = getAttributes().size();
    }

    public void setAttributeCount(int i) {
        attributeCount = i;
    }

    public void setAutoContentLinking(boolean autoContentLinking) {
        this.autoContentLinking = autoContentLinking;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public final void setBasketForwardCategory(Category basketForwardCategory) {
        this.basketForwardCategory = basketForwardCategory;
    }

    private void setChildCount() {

        this.childCount = SimpleQuery.count(context, Category.class, "parent", this);
        logger.fine("[Category] new child count=" + childCount);
        childrenCache = null;
        save("childCount");
    }

    public void setContent(String string) {
        ContentBlock cb = getContentBlock();
        cb.setContent(string);
        cb.save();
    }

    public void setContentLinkingKeyword(boolean contentLinkingKeyword) {
        this.contentLinkingKeyword = contentLinkingKeyword;
    }

    public final void setCss(String css) {
        this.css = css;
    }

    public final void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public void setDescriptionTag(String descriptionTag) {
        this.descriptionTag = descriptionTag;
    }

    public void setFileCount(int i) {
        this.fileCount = i;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public void setForum(boolean forum) {
        this.forum = forum;
    }

    public void setForwardUrl(String s, User user) {

        if (s != null) {

            String url = Config.getInstance(context).getUrl() + "/";

            if (s.startsWith(url)) {
                s = s.substring(url.length());
            }
        }

        if (ObjectUtil.equal(forwardUrl, s)) {
            return;
        }

        this.forwardUrl = s;
        log(user, "Forward url changed to " + s);
    }

    public void setForwardWho(ForwardWho forwardWho) {
        this.forwardWho = forwardWho;
    }

    public void setFriendlyUrl(String s) {
        this.friendlyUrl = (s == null ? null : s.trim());
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public void setKelkooCategory(String kelkooCategory) {
        this.kelkooCategory = kelkooCategory;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public void setLiveCount() {

        Query q = new Query(context, "select count(*) from # i join # ci on i.id=ci.item where ci.category=? and i.status=?");
        q.setTable(Item.class);
        q.setTable(CategoryItem.class);
        q.setParameter(this);
        q.setParameter("live");

        liveItemsCount = q.getInt();
        save("liveItemsCount");
    }

    public final void setManualItemsOrder(boolean manualItemsOrder) {
        this.manualItemsOrder = manualItemsOrder;
    }

    public void setMemberGroupPricing(boolean b) {

        this.memberGroupPricing = b;

        /*
           * If we are disabling member group pricing then we should remove all sale prices for member groups other than default
           */
        if (!b) {
            Query q = new Query(context, "delete from # where category=? and memberGroup>0");
            q.setTable(Price.class);
            q.setParameter(this);
            q.run();
        }
    }

    public void setMessages(boolean messages) {
        this.messages = messages;
    }

    public void setName(String newName) {

        if (newName.equals(name)) {
            return;
        }

        /*
           * Strip pipes from name as we use them in the full name special field
           */
        this.name = newName.replace("|", "").trim();
        this.fullName = name;

        // if friendly urls are not locked then set to be recalculated
        if (!friendlyUrlLocked) {
            friendlyUrl = null;
        }

        save();

        // clear content links
        ContentBlock.clear(context);

        if (isPersisted()) {

            // update all items in here with new category strings name
            for (Item item : getItems()) {
                item.setCategoryInfo();
                item.save();
            }

            doDependancies();
        }

    }

    public boolean setParent(Category newParent) {

        // we cannot set a parent on the special root cat
        if (isRoot()) {
            logger.fine("[Category] cannot set parent on root");
            return false;
        }

        // make sure new parent and old category are not the same
        if (newParent.equals(parent)) {
            logger.fine("[Category] old / new parents are equal");
            return false;
        }

        // ensure that this new parent is not equal to this category
        if (equals(newParent)) {
            logger.fine("[Category] parent is itself!");
            return false;
        }

        // ensure this new parent is not already a child of this category
        if (newParent.isChildOf(this)) {
            logger.fine("[Category] new parent is child of this!");
            return false;
        }

        // populate old parent
        this.parent.pop();

        // store old parent so we can update it after
        Category old = parent;

        // update parent
        parent = newParent;
        save("parent");

        // clear category html cache

        SubcategoriesBlock.clearHtmlCache();

        // update child counts on both old parent and new parent
        parent.setChildCount();
        old.setChildCount();

        parents = null;

        doDependancies();

        return true;
    }

    public final void setPermissions(boolean p) {
        this.permissions = p;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setPriceBreaks(Collection<Integer> breaks) {

        this.priceBreakValues = new TreeSet<Integer>();
        this.priceBreakValues.addAll(breaks);

        PriceUtil.syncPriceBreaks(context, this, breaks);
        save();
    }

    public final void setRenderTags(boolean renderTags) {
        this.renderTags = renderTags;
    }

    public void setRestrictionForwardUrl(String restrictionForwardUrl) {
        this.restrictionForwardUrl = restrictionForwardUrl;
    }

    public void setSearchEngineUrlLocked(boolean searchEngineUrlLocked) {
        this.searchEngineUrlLocked = searchEngineUrlLocked;
    }

    public void setSearchWrapper(boolean b) {
        searchWrapper = b;
    }

    public void setSellPrice(PriceBand priceBand, int qty, Amount amount) {
        new Price(context, this, priceBand, qty, amount);
    }

    public void setShopzillaCategory(String shopzillaCategory) {
        this.shopzillaCategory = shopzillaCategory;
    }

    public void setSimpleEditor(boolean simpleEditor) {
        this.simpleEditor = simpleEditor;
    }

    public void setSimpleListings() {
    }

    public void setSubcategoryListItemMarkup(Markup subcategoryListItemMarkup) {
        this.subcategoryListItemMarkup = subcategoryListItemMarkup;
    }

    public void setSubcategoryMarkup(Markup subcategoryMarkup) {
        this.subcategoryMarkup = subcategoryMarkup;
    }

    public boolean setSubcategoryOrdering(SubcategoryOrdering so) {

        if (this.subcategoryOrdering == so) {
            return false;
        }

        this.subcategoryOrdering = so;
        return true;
    }

    public void setSuggestions(List<String> c) {

        if (c.equals(this.suggestions)) {
            return;
        }

        this.suggestions = c;
        ContentBlock.clear(context);
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public final void setSupermanLock(boolean supermanLock) {
        this.supermanLock = supermanLock;
    }

    public void setTitleTag(String titleTag) {
        this.titleTag = titleTag;
    }

    public void setUseManualSort(boolean useManualSort) {
        this.useManualSort = useManualSort;
    }

    public boolean useManualSort() {
        return useManualSort;
    }

    public boolean isComments() {
        return Block.contains(CommentPostBlock.class, getBlocks());
    }

    public Amount getDiscount() {
        return discount;
    }

    public void setDiscount(Amount discount) {
        this.discount = discount;
        save("discount");
    }

    public boolean isHideDiscountInfo() {
        return hideDiscountInfo;
    }

    public void setHideDiscountInfo(boolean hideDiscountInfo) {
        this.hideDiscountInfo = hideDiscountInfo;
        save("hideDiscountInfo");
    }

    public List<String> getContentLinkingKeywords() {
        if (contentLinkingKeywords == null) {
            contentLinkingKeywords = new ArrayList();
        }
        return contentLinkingKeywords;
    }

    public void setContentLinkingKeywords(List<String> c) {

        if (c.equals(this.contentLinkingKeywords)) {
            return;
        }

        this.contentLinkingKeywords = c;
//        ContentBlock.clear(context);
    }

    public boolean useImageDescriptions() {
        return false;
    }

    public void wordReplace(String from, String to) {

        name = name.replace(from, to);
        ContentBlock cb = getContentBlock();
        cb.replace(from, to);
        cb.save();
    }

    public boolean isSibling(Category c2) {
        return ObjectUtil.equal(parent, c2.parent);
    }

    public ItemsBlock getItemsBlock() {
        List<Block> blocks = getBlocks();
        for (Block block : blocks) {
            if (block instanceof ItemsBlock) {
                return (ItemsBlock) block;
            }
        }
        return null;
    }

    /**
     *
     */
    public String[] getParentNames() {
        return fullName.split("\\|");
    }

    public void addComment(RequestContext context, String title, String commentString) {
        Comment comment = new Comment(context, null);
        comment.setAwaitingModeration(false);
        comment.setCategory(this);
        comment.setTitle(title);
        comment.setComment(commentString);
        comment.save();
    }

}

//	/**
//	 * Returns attribute value filters for this category narrowed by the currently selected attribute values.
//	 * 
//	 * @param attributeValues
//	 * @return
//	 */
//	public List<AttributeFilter> getFilters(MultiHashMap<Attribute, String> attributeValues, List<Integer> ignoreAttributes) {
//
//		StringBuilder sb = new StringBuilder();
//		sb.append("select distinct a.id, av.value from # a join # av on a.id=av.attribute join # i on i.id=av.item "
//				+ "join # ci on i.id=ci.item where a.filter=1 and i.status=? and ci.category=? "
//				+ "and (a.category=? or a.category in (select ancestor from # where category=?)) ");
//
//		if (attributeValues == null)
//			attributeValues = EmptyMap;
//
//		if (ignoreAttributes == null)
//			ignoreAttributes = Collections.emptyList();
//
//		if (attributeValues.valuesSize() > 0 || ignoreAttributes.size() > 0) {
//			sb.append("and a.id not in (");
//			for (int n = 0; n < attributeValues.valuesSize() + ignoreAttributes.size(); n++) {
//				if (n > 0)
//					sb.append(",");
//				sb.append("?");
//			}
//			sb.append(") ");
//		}
//
//		if (attributeValues.valuesSize() > 0) {
//
//			sb.append("and av.item in (select item from (");
//
//			for (int n = 0; n < attributeValues.valuesSize(); n++) {
//
//				if (n > 0)
//					sb.append(" UNION ALL ");
//				sb.append(" select item from # av where av.attribute=? and av.value like ? ");
//			}
//
//			sb.append(" ) Y group by item having count(*)=");
//			sb.append(attributeValues.valuesSize());
//			sb.append(" ) ");
//
//		}
//
//		sb.append("order by a.id, av.value");
//
//		Query q = new Query(sb);
//		q.setTable(Attribute.class);
//		q.setTable(AttributeValue.class);
//		q.setTable(Item.class);
//		q.setTable(CategoryItem.class);
//		q.setTable(CategoryAncestor.class);
//
//		// set status param
//		q.setParameter(Item.Status.Live);
//		q.setParameter(this);
//
//		// set params for attributes categories lookup
//		q.setParameter(this);
//		q.setParameter(this);
//
//		if (ignoreAttributes.size() > 0)
//			for (Integer i : ignoreAttributes)
//				q.setParameter(i);
//
//		if (attributeValues.valuesSize() > 0) {
//
//			for (Map.Entry<Attribute, String> entry : attributeValues.entrySet()) {
//				q.setParameter(entry.getKey());
//			}
//
//			for (Map.Entry<Attribute, String> entry : attributeValues.entrySet()) {
//				q.setTable(AttributeValue.class);
//				q.setParameter(entry.getKey());
//				q.setParameter(entry.getValue());
//			}
//		}
//
//		q.setDebug();
//
//		List<Row> rows = q.execute();
//
//		if (rows.size() == 0)
//			return Collections.emptyList();
//
//		List<AttributeFilter> list = new ArrayList<AttributeFilter>();
//		AttributeFilter filter = null;
//
//		for (Row row : rows) {
//
//			if (filter == null || filter.getAttribute().getId() != row.getInt(0))
//				filter = new AttributeFilter(row.getObject(0, Attribute.class));
//
//			filter.addValue(row.getString(1));
//			if (filter.getValues().size() == 2)
//				list.add(filter);
//		}
//
//		return list;
//	}