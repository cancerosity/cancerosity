package org.sevensoft.ecreator.model.bookings;

import org.sevensoft.commons.skint.Amount;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 18 Mar 2007 16:55:41
 *
 */
@Table("bookings_discounts")
public class BookingDiscount extends EntityObject {

	private Amount	amount;
	private Item	item;
	private int		nights;

	protected BookingDiscount(RequestContext context) {
		super(context);
	}

	public BookingDiscount(RequestContext context, Item item, int nights, Amount discount) {
		super(context);
		this.item = item;
		this.nights = nights;
		this.amount = discount;
	}

	public final Amount getAmount() {
		return amount;
	}

	public final Item getItem() {
		return item.pop();
	}

	public final int getNights() {
		return nights;
	}

	public final void setAmount(Amount discount) {
		this.amount = discount;
	}

	public final void setNights(int nights) {
		this.nights = nights;
	}

}
