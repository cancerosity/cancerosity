package org.sevensoft.ecreator.model.bookings;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.iface.admin.ecom.bookings.BookingHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.support.AttributeMessageSupport;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.orders.VatHelper;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentException;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.*;
import org.sevensoft.ecreator.model.ecom.payments.processors.Processor;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorException;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

import java.io.IOException;
import java.util.List;

/**
 * @author sks 27 Dec 2006 10:40:13
 *         <p/>
 *         An actual booking made by an account
 */
@Table("bookings")
public class Booking extends AttributeMessageSupport {

    public enum Sort {
        Newest, Oldest, CustomerName;
    }

    public BookingStatus status;

    private Item item;

    private double vatRate;

    /**
     * The account used to make this booking
     */
    private Item account;

    /**
     * Address for the customer on this booking
     */
    private Address address;

    /**
     * The start date of this booking
     */
    private Date start;

    /**
     * The end date of this booking
     */
    private Date end;

    /**
     *
     */
    private DateTime datePlaced;

    /**
     * Number of adults / children booked on this booking
     */
    private int adults, children;

    /**
     *
     */
    private String optionsString;

    /**
     *
     */
    private Money optionsChargesEx;

    /**
     *
     */
    private Money itemChargeEx;

    /**
     *
     */
    private PaymentType paymentType;

    /**
     *
     */
    private Card card;

    /**
     *
     */
    private Money totalInc;

    /**
     *
     */
    private Money totalVat;

    /**
     *
     */
    private Money totalEx;

    /**
     *
     */
    private String paymentReference;

    /**
     *
     */
    private User confirmationPerson;


    /**
     *
     */
    private DateTime confirmationTime;


    protected Booking(RequestContext context) {
        super(context);
    }

    public Booking(RequestContext context, Item item, Item account, Date start, Date end, int adults, int children) {
        super(context);

        logger.fine("[Booking] Creating booking");

        this.item = item;
        this.account = account;
        this.start = start;
        this.end = end;
        this.adults = adults;
        this.children = children;

        this.datePlaced = new DateTime();
        this.status = BookingStatus.UNCONFIRMED;

        this.vatRate = 17.5;

        resetTotals();

        this.itemChargeEx = item.getBookingSetup().getChargeEx(start, end);
        this.optionsChargesEx = new Money(0);

        save();

        recalc();

        logger.fine("[Booking] changing status to booked on dates from=" + start + ", to=" + end);

        Date date = start;
        int n = 0;
        while (date.isBefore(end)) {

            BookingSlot slot = item.getBookingSetup().getBookingSlot(date);
            logger.fine("[Booking] getting slot for date=" + date + ", slot=" + slot);

            if (slot != null) {
                slot.setStatus(BookingSlot.Status.Reserved);
                slot.incBookings(adults + children);
                slot.save();
            }

            if (n > 1000) {
                break;
            }

            n++;

            date = date.nextDay();
        }
    }

    public void addOptions(List<BookingOption> options) {

        logger.fine("[Booking] calling add options=" + options);

        StringBuilder sb = new StringBuilder();
        this.optionsChargesEx = new Money(0);

        for (BookingOption option : options) {
            sb.append(option.getName() + " at \243" + option.getChargeEx() + "\n");
            this.optionsChargesEx = this.optionsChargesEx.add(option.getChargeEx());

            logger.fine("[Booking] adding option, name=" + option.getName() + ", charge=" + option.getChargeEx());
        }

        this.optionsString = sb.toString();

        recalc();
    }

    public void clearBookingSlots() {
        Date date = start;
        Date endPeriod = end.nextDay();
        while (!date.equals(endPeriod)) {
            BookingSlot slot = item.getBookingSetup().getBookingSlot(date);
            if (slot != null) {
                if (slot.getBookings() > 0) {
                    slot.decBookings(adults + children);
                }
                slot.setStatus(BookingSlot.Status.Open);
                slot.save();
            }
            date = date.nextDay();
        }
    }

    @Override
    public synchronized boolean delete() {

        clearBookingSlots();

        return super.delete();
    }

    public Item getAccount() {
        return account.pop();
    }

    public Address getAddress() {
        return address == null ? null : (Address) address.pop();
    }

    public final int getAdults() {
        return adults;
    }

    public List<Attribute> getAttributes() {
        return BookingSettings.getInstance(context).getAttributes();
    }

    public final Card getCard() {
        return (Card) (card == null ? null : card.pop());
    }

    public final int getChildren() {
        return children;
    }

    public final DateTime getDatePlaced() {
        return datePlaced;
    }

    public Date getEnd() {
        return end;
    }

    public Item getItem() {
        return item.pop();
    }

    public final Money getItemChargeEx() {
        return itemChargeEx;
    }

    public final Money getItemChargeExWithCommission() {
        return applyCommision(itemChargeEx);
    }

    public final Money getItemChargeInc() {
        return VatHelper.inc(context, getItemChargeExWithCommission(), vatRate);
    }

    public Money getItemChargeVat() {
        return VatHelper.vat(context, getItemChargeExWithCommission(), vatRate);
    }

    @Override
    public String getMessageSubject() {
        return "Booking ";
    }

    /**
     * Returns the number of nights on this booking
     */
    public int getNights() {
        return end.getDaysBetween(start);
    }

    public final Money getOptionsChargesEx() {
        return optionsChargesEx;
    }

    public final Money getOptionsChargesInc() {
        return VatHelper.inc(context, applyCommision(optionsChargesEx), vatRate);
    }

    public Money getOptionsChargesVat() {
        return VatHelper.vat(context, applyCommision(optionsChargesEx), vatRate);
    }

    public final String getOptionsString() {
        return optionsString;
    }

    public List<Payment> getPayments() {
        return SimpleQuery.execute(context, Payment.class, "booking", this);
    }

    public final PaymentType getPaymentType() {
        return paymentType;
    }

    public Date getStart() {
        return start;
    }

    public BookingStatus getStatus() {
        return status;
    }

    public void setStatus(BookingStatus status) {
        this.status = status;
    }

    public int getStay() {
        return start.getDaysBetween(end);
    }

    public Money getTotalEx() {
        return totalEx;
    }

    public Money getTotalInc() {
        return totalInc;
    }

    public Money getTotalVat() {
        return totalVat;
    }

    public double getVatRate() {
        return vatRate;
    }

    /**
     *
     */
    public boolean hasCard() {
        return card != null;
    }

    public boolean hasOptions() {
        return optionsString != null;
    }

    private void recalc() {

        logger.fine("[Booking] recalc");

        resetTotals();

        this.totalEx = applyCommision(itemChargeEx.add(optionsChargesEx));
        this.totalVat = getItemChargeVat().add(getOptionsChargesVat());
        this.totalInc = getItemChargeInc().add(getOptionsChargesInc());

        logger.fine("[Booking] totalEx=" + totalEx + ", totalVat=" + totalVat + ", totalInc=" + totalInc);

        save();
    }


    public Money getCommissionValue() {
        int commission = getItem().getCommission();
        Money charge = itemChargeEx.add(optionsChargesEx);
        return commission == 0 ? new Money(0) : charge.multiply(commission / 100d);
    }

    private Money applyCommision(Money charge) {
        if (charge == null) {
            return null;
        }
        int commission = getItem().getCommission();
        return ComissionHelper.inc(charge, commission);
    }

    private void resetTotals() {

        this.totalEx = new Money(0);
        this.totalVat = new Money(0);
        this.totalInc = new Money(0);
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public final void setAdults(int adults) {
        this.adults = adults;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public final void setChildren(int children) {
        this.children = children;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public void setStart(Date start) {
        this.start = start;
    }


    public final void setVatRate(double vatRate) {
        this.vatRate = vatRate;
    }

    public String getPaymentReference() {
        return paymentReference;
    }

    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    public User getConfirmationPerson() {
        return confirmationPerson;
    }

    public void setConfirmationPerson(User confirmationPerson) {
        this.confirmationPerson = confirmationPerson;
    }

    public DateTime getConfirmationTime() {
        return confirmationTime;
    }

    public void setConfirmationTime(DateTime confirmationTime) {
        this.confirmationTime = confirmationTime;
    }

    public Payment transactCard() throws PaymentException, CscException, CardException, CardTypeException, ExpiryDateException, IssueNumberException,
            StartDateException, IOException, CardNumberException, ProcessorException, ExpiredCardException {

        // we must have a card to transact
        if (!hasCard()) {
            logger.fine("[Booking] cannot transact, no card");
            throw new PaymentException("No card details set");
        }

        // cannot transact if we do not know payment type
        if (paymentType == null) {
            logger.fine("[Booking] cannot auto transact an order with null payment type");
            throw new PaymentException("No payment type set");
        }

        // payment type must be credit card
        if (paymentType != PaymentType.CardTerminal) {
            logger.fine("[Booking] cannot auto transact a non credit card order");
            throw new PaymentException("Payment type is not suitable");
        }

        // we must have an available processor
        Processor processor = PaymentSettings.getInstance(context).getDefaultProcessor();
        if (processor == null) {
            logger.fine("[Booking] no available processor");
            throw new PaymentException("No available processor");
        }

        Payment payment = processor.transact(getAccount(), getCard(), getAddress(), getTotalInc());
        logger.fine("[Booking] auto transaction order=" + this + ", payment=" + payment);

        payment.setBooking(this);

        // return payment object
        return payment;

    }

    /**
     * @throws SmtpServerException
     * @throws EmailAddressException
     */
    public void emailAccountNotification() throws EmailAddressException, SmtpServerException {

        Config config = Config.getInstance(context);

        StringBuilder sb = new StringBuilder();
        sb.append("Thank you for your booking\n\n");

        sb.append(getItem().getItemType().getName() + ": " + getItem().getName() + "\n");
        sb.append("From " + start.toString("dd.MMM.yyyy") + " to " + end.toString("dd.MMM.yyyy") + "\n\n");

        sb.append("Options:\n");
        sb.append(optionsString + "\n\n");

        sb.append("Total cost: " + getTotalInc() + "\n");

        Email e = new Email();
        e.setSubject("New booking");
        e.setBody(sb);
        e.setFrom(config.getServerEmail());
        e.setTo(getAccount().getEmail());
        new EmailDecorator(context, e).send(config.getSmtpHostname());
    }

    /**
     * @throws SmtpServerException
     * @throws EmailAddressException
     */
    public void sendOwnerNotification() throws EmailAddressException, SmtpServerException {

        Config config = Config.getInstance(context);

        StringBuilder sb = new StringBuilder();
        sb.append("New booking\n\n");

        sb.append(getItem().getItemType().getName() + ": " + getItem().getName() + "\n");
        sb.append("From " + start.toString("dd.MMM.yyyy") + " to " + end.toString("dd.MMM.yyyy") + "\n\n");

        sb.append("Options:\n");
        sb.append(optionsString + "\n\n");

        sb.append("Total cost: " + getTotalInc() + "\n");

        Email e = new Email();
        e.setSubject("New booking");
        e.setBody(sb);
        e.setFrom(config.getServerEmail());
        e.setTo(getItem().getOwner().getEmail());
        new EmailDecorator(context, e).send(config.getSmtpHostname());
    }

    public void emailThirdPartyNotifications() throws EmailAddressException, SmtpServerException {

        Config config = Config.getInstance(context);
        BookingSettings bookingSettings = BookingSettings.getInstance(context);

        StringBuilder sb = new StringBuilder();
        sb.append("A new booking has been received\n\n");
        sb.append("Click here to view\n" + new Link(BookingHandler.class, null, "booking", this) + "\n\n");

        sb.append(getItem().getItemType().getName() + ": " + getItem().getName() + "\n");
        sb.append("From " + start.toString("dd.MMM.yyyy") + " to " + end.toString("dd.MMM.yyyy") + "\n\n");

        sb.append("Options:\n");
        sb.append(optionsString + "\n\n");

        sb.append("Total cost: " + getTotalInc() + "\n");

        Email e = new Email();
        e.setSubject("New booking");
        e.setBody(sb);
        e.setFrom(config.getServerEmail());
        e.setRecipients(bookingSettings.getThirdPartyEmailNotifications());
        new EmailDecorator(context, e).send(config.getSmtpHostname());
    }

    public Item getPropertyOwner() {
        return item.getAccount();
    }
}