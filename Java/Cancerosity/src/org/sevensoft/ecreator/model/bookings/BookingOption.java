package org.sevensoft.ecreator.model.bookings;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.orders.VatHelper;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 18 Feb 2007 17:03:57
 *
 */
@Table("bookings_options")
public class BookingOption extends EntityObject implements Positionable {

	public enum Type {
		Check
	}

	private Item	item;
	private String	name;
	private Type	type;
	private Money	chargeEx;
	private int		position;

	protected BookingOption(RequestContext context) {
		super(context);
	}

	public BookingOption(RequestContext context, Item item, String name) {
		super(context);

		this.item = item;
		this.type = Type.Check;
		this.name = name;
		this.chargeEx = new Money(0);

		save();
	}

	public Money getChargeEx() {
		return chargeEx;
	}

	public Money getChargeInc() {
		return VatHelper.inc(context, chargeEx, 17.5);
	}

	public Item getItem() {
		return item.pop();
	}

	public String getName() {
		return name;
	}

	public int getPosition() {
		return position;
	}

	public Type getType() {
		return type;
	}

	public void setChargeEx(Money chargeEx) {
		this.chargeEx = chargeEx;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setType(Type type) {
		this.type = type;
	}

}
