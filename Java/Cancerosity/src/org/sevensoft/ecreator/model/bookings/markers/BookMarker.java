package org.sevensoft.ecreator.model.bookings.markers;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.iface.frontend.bookings.BookingHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

import java.util.Map;

/**
 * @author sks 19 Mar 2007 14:56:42
 */
public class BookMarker extends MarkerHelper implements IItemMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

        if (!Module.Bookings.enabled(context)) {
            return null;
        }

        return super.link(context, params, item.getUrl(), null);

/*        
        Date start = (Date) context.getAttribute("bookingStart");
        Date end = (Date) context.getAttribute("bookingEnd");

        if (start == null) {
            start = new Date();
        }

        if (end == null) {
            end = new Date().addDays(3);
        }

        Link link;
        String directBooking = item.getAttributeValue("Direct booking");
        if (Boolean.valueOf(directBooking)) {
            link = new Link(BookingHandler.class, "start", "item", item, "start", start, "end", end);
        } else {
            String thirdPartyUrl = item.getAttributeValue("3rd party URL");
            if (thirdPartyUrl != null) {
                if (!thirdPartyUrl.contains("http://")) {
                    thirdPartyUrl = "http://".concat(thirdPartyUrl);
                }
            } else {
                thirdPartyUrl = "";
            }
            link = new Link(thirdPartyUrl);
            params.put("target", "_blank");
        }
        return super.link(context, params, link.toString(), null);
*/
    }

    public Object getRegex() {
        return "booking_book";
    }

}
