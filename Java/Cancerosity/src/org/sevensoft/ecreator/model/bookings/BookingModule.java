package org.sevensoft.ecreator.model.bookings;

import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.misc.agreements.Agreement;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 19 Feb 2007 08:53:11
 *
 */
@Table("bookings_module")
public class BookingModule extends EntityObject {

	public static BookingModule get(RequestContext context, ItemType itemType) {
		BookingModule module = SimpleQuery.get(context, BookingModule.class, "itemType", itemType);
		return module == null ? new BookingModule(context, itemType) : module;
	}

	private ItemType	itemType;
	private Agreement	agreement;

	protected BookingModule(RequestContext context) {
		super(context);
	}

	public BookingModule(RequestContext context, ItemType itemType) {
		super(context);
		this.itemType = itemType;
		save();
	}

	public final Agreement getAgreement() {
		return (Agreement) (agreement == null ? null : agreement.pop());
	}

	public ItemType getItemType() {
		return itemType.pop();
	}

	public boolean hasAgreement() {
		return agreement != null;
	}

	public final void setAgreement(Agreement agreement) {
		this.agreement = agreement;
	}

}
