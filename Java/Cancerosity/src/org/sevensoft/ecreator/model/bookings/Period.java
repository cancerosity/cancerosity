package org.sevensoft.ecreator.model.bookings;


import org.sevensoft.commons.samdate.Date;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * User: MeleshkoDN
 * Date: 11.09.2007
 * Time: 15:34:26
 */
@Table("periods")
public class Period extends EntityObject {


    public Period(RequestContext context) {
        super(context);
    }

    private String name;
    private Date startDate;
    private Date endDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isWeekly() {
        return endDate.getDaysBetween(startDate) >= 7;
    }

}
