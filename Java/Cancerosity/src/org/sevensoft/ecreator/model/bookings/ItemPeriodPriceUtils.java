package org.sevensoft.ecreator.model.bookings;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.List;

/**
 * User: MeleshkoDN
 * Date: 13.09.2007
 * Time: 16:36:07
 */
public class ItemPeriodPriceUtils {

    public static ItemPeriodPrice getEnclosingPeriod(RequestContext context, Item item, Date start, Date end) {
        Query q = new Query(context, "SELECT i.* FROM # i JOIN # p ON p.id=i.period WHERE i.item=? AND (p.startDate < ? AND p.endDate > ?)");
        q.setTable(ItemPeriodPrice.class);
        q.setTable(Period.class);
        q.setParameter(item);
        q.setParameter(start);
        q.setParameter(end);
        return q.get(ItemPeriodPrice.class);
    }

    public static ItemPeriodPrice getIntersectingPeriod(RequestContext context, Item item, Date date) {
        Query q = new Query(context, "SELECT i.* FROM # i JOIN # p ON p.id=i.period WHERE i.item=? AND (p.startDate < ? AND p.endDate > ?)");
        q.setTable(ItemPeriodPrice.class);
        q.setTable(Period.class);
        q.setParameter(item);
        q.setParameter(date);
        q.setParameter(date);
        return q.get(ItemPeriodPrice.class);
    }

    public static List<ItemPeriodPrice> getInnerPeriods(RequestContext context, Item item, Date start, Date end) {
        Query q = new Query(context, "SELECT i.* FROM # i JOIN # p ON p.id=i.period WHERE i.item=? AND (p.startDate >= ? AND p.endDate <= ?) ORDER BY p.startDate ASC");
        q.setTable(ItemPeriodPrice.class);
        q.setTable(Period.class);
        q.setParameter(item);
        q.setParameter(start);
        q.setParameter(end);
        return q.execute(ItemPeriodPrice.class);
    }

}
