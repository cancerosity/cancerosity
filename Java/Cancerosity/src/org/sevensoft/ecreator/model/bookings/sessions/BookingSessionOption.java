package org.sevensoft.ecreator.model.bookings.sessions;

import org.sevensoft.ecreator.model.bookings.BookingOption;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 21 Mar 2007 07:40:24
 *
 */
@Table("bookings_sessions_options")
public class BookingSessionOption extends EntityObject {

	private String		sessionId;
	private BookingOption	option;

	protected BookingSessionOption(RequestContext context) {
		super(context);
	}

	public BookingSessionOption(RequestContext context, BookingOption option) {
		super(context);

		this.sessionId = context.getSessionId();
		this.option = option;

		logger.fine("[BookingSessionOption] creating booking session option for option=" + option + ", sessionId=" + context.getSessionId());

		save();
	}

}
