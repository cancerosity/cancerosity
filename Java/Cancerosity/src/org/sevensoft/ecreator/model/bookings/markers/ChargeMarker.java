package org.sevensoft.ecreator.model.bookings.markers;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.bookings.BookingSetup;
import org.sevensoft.ecreator.model.bookings.ComissionHelper;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * @author sks 19 Mar 2007 14:12:10
 */
public class ChargeMarker extends MarkerHelper implements IItemMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

        Date start = (Date) context.getAttribute("bookingStart");
        Date end = (Date) context.getAttribute("bookingEnd");

        if (start == null || end == null) {
            return null;
        }

        String sym = (String) context.getAttribute("currencySymbol");
        BookingSetup setup = item.getBookingSetup();
        Money charge = setup.getChargeEx(start, end);
        int commission = item.getCommission();
        Money comissionInc = ComissionHelper.inc(charge, commission);
        return super.string(context, params, sym + comissionInc);
    }

    @Override
    public String getDescription() {
        return "Shows the total charge to the customer for this stay";
    }

    public Object getRegex() {
        return "booking_charge";
    }

}
