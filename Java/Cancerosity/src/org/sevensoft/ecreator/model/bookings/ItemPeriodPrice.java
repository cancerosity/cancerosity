package org.sevensoft.ecreator.model.bookings;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * User: MeleshkoDN
 * Date: 12.09.2007
 * Time: 11:06:47
 */
@Table("item_period_prices")
public class ItemPeriodPrice extends EntityObject {

    public ItemPeriodPrice(RequestContext context) {
        super(context);
    }

    private Period period;
    private Item item;
    private Money dailyPrice;
    private Money weeklyPrice;

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Money getDailyPrice() {
        return dailyPrice;
    }

    public void setDailyPrice(Money dailyPrice) {
        this.dailyPrice = dailyPrice;
    }

    public Money getWeeklyPrice() {
        return weeklyPrice;
    }

    public void setWeeklyPrice(Money weeklyPrice) {
        this.weeklyPrice = weeklyPrice;
    }


}
