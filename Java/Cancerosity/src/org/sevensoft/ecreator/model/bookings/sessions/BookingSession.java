package org.sevensoft.ecreator.model.bookings.sessions;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.ecom.bookings.BookingNotificationEmails;
import org.sevensoft.ecreator.iface.frontend.bookings.BookingHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.support.AttributeSupport;
import org.sevensoft.ecreator.model.bookings.*;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.orders.VatHelper;
import org.sevensoft.ecreator.model.ecom.payments.PaymentException;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.*;
import org.sevensoft.ecreator.model.ecom.payments.forms.able2buy.CreditGroup;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorException;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author sks 15 Feb 2007 17:03:30
 */
@Table("bookings_sessions")
public class BookingSession extends AttributeSupport {

    public static BookingSession get(RequestContext context, String sessionId) {
        BookingSession session = SimpleQuery.get(context, BookingSession.class, "sessionId", sessionId);
        return session == null ? new BookingSession(context, sessionId) : session;
    }

    /**
     *
     */
    private Item item;

    /**
     *
     */
    private String sessionId;

    /**
     *
     */
    private Date start;

    /**
     *
     */
    private boolean offeredOptions;

    /**
     *
     */
    private Item account;

    /**
     * Name of account to create
     */
    private String accountName;

    /**
     * Email of account to create
     */
    private String accountEmail;

    private int children;

    private int adults;

    private Date end;

    private Card card;

    private PaymentType paymentType;

    private CreditGroup creditGroup;

    private Address address;

    private Booking booking;

    private boolean offeredAttributes;

    private String referrer;

    private BookingSession(RequestContext context) {
        super(context);
    }

    public BookingSession(RequestContext context, String sessionId) {
        super(context);
        this.sessionId = sessionId;

        save();
    }

    public void addOption(BookingOption option) {
        new BookingSessionOption(context, option);
    }

    public CreditGroup getAble2BuyCreditGroup() {
        return (CreditGroup) (creditGroup == null ? null : creditGroup.pop());
    }

    public final Item getAccount() {
        return (Item) (account == null ? null : account.pop());
    }

    public final String getAccountEmail() {
        return accountEmail;
    }

    public final String getAccountName() {
        return accountName;
    }

    public final Address getAddress() {
        return (Address) (address == null ? null : address.pop());
    }

    public final int getAdults() {
        return adults;
    }

    public List<Attribute> getAttributes() {
        return BookingSettings.getInstance(context).getAttributes();
    }

    public final Booking getBooking() {
        return (Booking) (booking == null ? null : booking.pop());
    }

    public BookingModule getBookingModule() {
        return getItem().getItemType().getBookingModule();
    }

    public Card getCard() {
        return (Card) (card == null ? null : card.pop());
    }

    public Money getChargeEx() {

        BookingSetup setup = getItem().getBookingSetup();
        Money amount = setup.getChargeEx(start, end);
        amount = amount.add(getOptionsChargesEx());

        return amount;
    }

    public Money getChargeInc() {
        return VatHelper.inc(context, getChargeEx(), 17.5);
    }

    public Money getChargeVat() {
        return VatHelper.vat(context, getChargeEx(), 17.5);
    }

    public final int getChildren() {
        return children;
    }

    public final CreditGroup getCreditGroup() {
        return creditGroup;
    }

    public final Date getEnd() {
        return end;
    }

    public Item getItem() {
        return item.pop();
    }

    public List<BookingOption> getOptions() {
        Query q = new Query(context, "select bo.* from # bo join # bso on bo.id=bso.option where bso.sessionId=?");
        q.setTable(BookingOption.class);
        q.setTable(BookingSessionOption.class);
        q.setParameter(context.getSessionId());
        return q.execute(BookingOption.class);
    }

    private Money getOptionsChargesEx() {

        Money money = new Money(0);
        for (BookingOption option : getOptions()) {
            money = money.add(option.getChargeEx());
        }
        return money;
    }

    private Money getOptionsChargesInc() {
        return VatHelper.inc(context, getOptionsChargesEx(), 17.5);
    }

    public String getOptionsString(String sep) {

        List<String> list = new ArrayList();

        for (BookingOption option : getOptions()) {
            list.add(option.getName() + " at \243" + option.getChargeInc());
        }

        return StringHelper.implode(list, sep);
    }

    public Item getPayableAccount() {
        return getAccount();
    }

    public Address getPaymentAddress() {
        return (Address) (address == null ? null : address.pop());
    }

    public Money getPaymentAmount() {
        return getChargeInc();
    }

    public String getPaymentDescription() {
        return "Booking";
    }

    public String getPaymentFailureUrl(PaymentType paymentType) {
        return Config.getInstance(context).getUrl() + "/" + new Link(BookingHandler.class, "cancelled");
    }

    public String getPaymentSuccessUrl(PaymentType paymentType) {
        return Config.getInstance(context).getUrl() + "/" + new Link(BookingHandler.class, "completed");
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public String getReferrer() {
        return referrer;
    }

    public String getSessionId() {
        return sessionId;
    }

    public Date getStart() {
        return start;
    }

    public boolean hasAccount() {
        return account != null;
    }

    public boolean hasBooking() {
        return booking != null;
    }

    public boolean hasCard() {
        return card != null;
    }

    public boolean hasEmail() {
        return accountEmail != null;
    }

    public boolean hasEnd() {
        return end != null;
    }

    public boolean hasItem() {
        return item != null;
    }

    public boolean hasName() {
        return accountName != null;
    }

    public boolean hasPaymentType() {
        return paymentType != null;
    }

    public boolean hasStart() {
        return start != null;
    }

    public final boolean isOfferedAttributes() {
        return offeredAttributes;
    }

    public boolean isOfferedOptions() {
        return offeredOptions;
    }

    public void removeOptions() {
        SimpleQuery.delete(context, BookingSessionOption.class, "sessionId", context.getSessionId());
    }

    public void reset() {
        this.paymentType = null;
        this.card = null;
        this.creditGroup = null;
    }

    public final void setAccount(Item account) {

        if (ObjectUtil.equal(this.account, account)) {
            return;
        }

        this.account = account;
        save();
    }

    public final void setAccountEmail(String email) {
        this.accountEmail = email;
    }

    public final void setAccountName(String name) {
        this.accountName = name;
    }

    public final void setAddress(Address address) {
        this.address = address;
    }

    public final void setAdults(int adults) {
        this.adults = adults;
    }

    public void setCard(Card card) {
        this.card = card;
        save("card");
    }

    public final void setChildren(int children) {
        this.children = children;
    }

    public final void setEnd(Date end) {
        this.end = end;
    }

    public void setItem(Item item) {
        this.item = item;
        save("item");
    }

    /**
     *
     */
    public void setOfferedAttributes(boolean b) {
        this.offeredAttributes = b;
    }

    public final void setOfferedOptions(boolean offeredOptions) {
        this.offeredOptions = offeredOptions;
        save("offeredOptions");
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
        save("paymentType");
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Booking toBooking() throws PaymentException, CscException, CardException, CardTypeException, ExpiryDateException, IssueNumberException,
            StartDateException, IOException, CardNumberException, ProcessorException, ExpiredCardException {

        logger.fine("[BookingSession] creating booking");

        if (account == null) {

            account = new Item(context, ItemType.getAccount(context), accountName, "LIVE", null);

            account.setEmail(accountEmail);
            account.save();
        }

        booking = new Booking(context, getItem(), (Item) account.pop(), start, end, adults, children);
        booking.addOptions(getOptions());

        booking.setAddress(getAddress());
        booking.setPaymentType(getPaymentType());

        logger.fine("[BookingSession] setting card=" + getCard());
        booking.setCard(getCard());

        booking.setAdults(getAdults());
        booking.setChildren(getChildren());

        booking.save();

        // transact

        /*if (booking.hasCard()) {
            booking.transactCard();
        }*/

        save();

        // send notification to owner
        /* try {
            booking.sendOwnerNotification();
        } catch (EmailAddressException e) {
            e.printStackTrace();
        } catch (SmtpServerException e) {
            e.printStackTrace();
        }*/

        /*try {
            booking.emailThirdPartyNotifications();
        } catch (EmailAddressException e) {
            e.printStackTrace();
        } catch (SmtpServerException e) {
            e.printStackTrace();
        }*/
        BookingNotificationEmails.informOwnerOfBooking(context, booking);
        BookingNotificationEmails.informClientOfBooking(context, booking);
        return booking;
    }

    /**
     *
     */
    public int getStay() {
        return start.getDaysBetween(end);
    }
}
