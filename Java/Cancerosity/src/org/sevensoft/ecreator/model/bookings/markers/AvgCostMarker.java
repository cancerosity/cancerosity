package org.sevensoft.ecreator.model.bookings.markers;

import java.util.Map;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.bookings.BookingSetup;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 19 Mar 2007 14:12:10
 *
 */
public class AvgCostMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		if (!Module.Bookings.enabled(context)) {
			return null;
		}

		Date start = (Date) context.getAttribute("bookingStart");
		Date end = (Date) context.getAttribute("bookingEnd");

		if (start == null) {
			start = new Date();
		}

		if (end == null) {
			end = new Date().addDays(3);
		}

		String sym = (String) context.getAttribute("currencySymbol");
		BookingSetup setup = item.getBookingSetup();
		Money charge = setup.getChargeEx(start, end);

		Money avg = charge.divide(start.getDaysBetween(end));

		return super.string(context, params, sym + avg);
	}

	@Override
	public String getDescription() {
		return "Shows the average cost per night for the stay searched for";
	}

	public Object getRegex() {
		return "booking_avgcost";
	}

}
