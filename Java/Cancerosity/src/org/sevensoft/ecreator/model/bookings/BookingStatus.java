package org.sevensoft.ecreator.model.bookings;

/**
 * User: MeleshkoDN
 * Date: 10.09.2007
 * Time: 16:02:27
 */
public enum BookingStatus {

    UNCONFIRMED("Unconfirmed"), CONFIRMED("Confirmed"), PAID("Paid"), CANCELLED("Cancelled"),;

    private final String name;

    BookingStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return getName();
    }
}
