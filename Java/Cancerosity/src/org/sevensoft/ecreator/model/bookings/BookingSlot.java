package org.sevensoft.ecreator.model.bookings;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.List;

/**
 * @author sks 27 Dec 2006 10:32:24
 *         <p/>
 *         The status for a particular date
 */
@Table("bookings_slots")
public class BookingSlot extends EntityObject {

    public enum Status {

        /**
         * The booking date is open for booking subject to limits
         */
        Open,

        /**
         * Only available as part of a block booking
         */
//		Block;
        Reserved,
        Booked,
        Unavailable
    }

    /**
     *
     */
    private Item item;

    /**
     *
     */
    private Date date;

    /**
     *
     */
    private Status status;

    /**
     *
     */
    private Money rate;

    private int bookings;

    protected BookingSlot(RequestContext context) {
        super(context);
    }

    public BookingSlot(RequestContext context, Item item, Date date) {
        super(context);

        this.item = item;
        this.date = date;
        this.status = Status.Open;

        save();
    }

    public void decBookings(int i) {
        this.bookings = bookings - i;
    }

    public int getBookings() {
        return bookings;
    }

    public Date getDate() {
        return date;
    }

    public Item getItem() {
        return item.pop();
    }

    public final Money getRate() {
        return rate;
    }

    public Status getStatus() {
        return status == null ? Status.Open : status;
    }

    public boolean hasRate() {
        return rate != null && getRate().isPositive();
    }

    public void incBookings(int i) {
        this.bookings = bookings + i;
    }

    public final void setRate(Money rate) {
        this.rate = rate;
    }

    public void setStatus(Status status) {
        this.status = status;
        save();
    }

    public static void setStatus(Status newStatus, Item item, Date start, Date end) {
        List<Date> days = Date.getDaysBetween(start, end);
        for (Date date : days) {
            BookingSlot slot = item.getBookingSetup().getBookingSlot(date);
            if (slot != null) {
                slot.setStatus(newStatus);
                slot.save();
            }
        }
    }
}
