package org.sevensoft.ecreator.model.bookings;

import org.sevensoft.commons.skint.Money;

/**
 * User: MeleshkoDN
 * Date: 02.10.2007
 * Time: 17:21:01
 */
public class ComissionHelper {
    public static Money inc(Money money, int comission) {

        if (money == null) {
            return null;
        }

        if (comission == 0) {
            return money;
        }

        return money.multiply(comission / 100d + 1d);
    }

}
