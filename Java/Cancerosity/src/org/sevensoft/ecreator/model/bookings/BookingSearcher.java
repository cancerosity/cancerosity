package org.sevensoft.ecreator.model.bookings;

import java.util.List;

import org.sevensoft.ecreator.model.bookings.Booking.Sort;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.QueryBuilder;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 22 Aug 2006 12:11:44
 *
 */
public class BookingSearcher {

	private String			status;
	private Sort			sort;
	private String			name;
	private String			email;
	private String			address;
	private String			postcode;
	private int				start, limit;
	private final RequestContext	context;
	private String			bookingId;

	public BookingSearcher(RequestContext context) {
		this.context = context;
	}

	public List<Booking> execute() {

		QueryBuilder b = new QueryBuilder(context);
		b.select("b.*");
		b.from("# b", Booking.class);
		b.from("join # i on b.account=i.id", Item.class);

		if (bookingId != null) {
			b.clause("b.id like ?", "%" + bookingId + "%");
		}

		if (name != null) {
			for (String s : name.split("\\s")) {
				b.clause("i.name like ?", "%" + s.trim() + "%");
			}
		}

		if (email != null) {
			b.clause("i.email like ?", "%" + email.trim() + "%");
		}

		if (address != null) {
			b.from("join # da on b.deliveryAddress=da.id", Address.class);
			b.clause("da.address like ?", "%" + address + "%");
		}

		if (postcode != null) {
			b.from("join # da on b.deliveryAddress=da.id", Address.class);
			b.clause("da.postcode like ?", "%" + postcode + "%");
		}

		if (status != null) {
			b.clause("b.status=?", status);
		} else {
			b.clause("b.status!=?", "Cancelled");
		}

		if (sort != null)

			switch (sort) {

			default:
			case Oldest:
				b.order("i.id asc");
				break;

			case CustomerName:
				b.order("i.name asc");
				break;

			case Newest:
				b.order("i.id desc");
				break;

			}

		b.order("i.id");

		return b.execute(Booking.class, start, limit);
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public final void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public void setSort(Sort sort) {
		this.sort = sort;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
