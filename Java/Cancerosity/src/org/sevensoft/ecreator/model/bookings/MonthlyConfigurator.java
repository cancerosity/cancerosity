package org.sevensoft.ecreator.model.bookings;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.JavascriptLinkTag;
import org.sevensoft.jeezy.http.util.Keypad;
import org.sevensoft.jeezy.http.util.Link;

import java.util.List;
import java.util.Map;

/**
 * @author sks 28 Dec 2006 15:11:25
 */
public class MonthlyConfigurator {

    private final Date month;
    private final RequestContext context;
    private final BookingSetup setup;
    private final List<BookingSlot> dates;
    private final Item item;
    private final Class<? extends Handler> handler;
    private final Map<Date, BookingSlot> datesMap;

    public MonthlyConfigurator(RequestContext context, Date month, BookingSetup setup, Item item, Class handler) {
        this.context = context;
        this.setup = setup;
        this.month = month.beginMonth();
        this.dates = setup.getBookingDates();
        this.datesMap = setup.getDatesMap(dates);
        this.item = item;
        this.handler = handler;
    }

    @Override
    public String toString() {

        Keypad keypad = new Keypad(7);
        keypad.setCaption(month.toString("MMMMMM yyyy"));
        keypad.setTableClass("ec_bkmonth");
        keypad.setCellspacing(2);
        keypad.setCellVAlign("top");
        keypad.setCellAlign("center");
        keypad.addObject("MON", "heading");
        keypad.addObject("TUE", "heading");
        keypad.addObject("WED", "heading");
        keypad.addObject("THU", "heading");
        keypad.addObject("FRI", "heading");
        keypad.addObject("SAT", "heading");
        keypad.addObject("SUN", "heading");
        keypad.setDefaultClass("empty");

        // fast forward to the correct day of the week
        int day = Date.Monday;
        while (month.getDayOfWeek() != day) {
            keypad.addObject(" ", "empty");
            day = Date.nextDay(day);
        }

        Date date = month;
        // for each day in the month render it
        while (date.isBefore(month.nextMonth())) {

            final Date date2 = date;

            int dayOfMonth = date.getDayOfMonth();

            //			Link link = new Link(BookingSetupHandler.class, "changeDate", "item", item, "date", date.getTimestamp());
            //			LinkTag linkTag = new LinkTag(link, dayOfMonth);

            BookingSlot bookingDate = datesMap.get(date);

            Link link = new Link(handler, "popup", "date", date, "item", item);
            final JavascriptLinkTag popuplink = new JavascriptLinkTag(context, link, dayOfMonth, 400, 300);
            popuplink.setOnMouseOver("var e = document.getElementById('boxover" + date.getTimestamp() + "'); if( e != null ){" +
                    "e.style.display='block'; e.style.top = findPosY(e) + 5 + 'px'; e.style.left = findPosX(e) + 30 + 'px'; }");
            popuplink.setOnMouseOut("var e = document.getElementById('boxover" + date.getTimestamp() + "'); " +
                    "if (e != null) {e.style.display='none';}");

            if (bookingDate == null) {

                keypad.addObject(popuplink, "disabled");

            } else {

                final String rateString;
                if (bookingDate.hasRate()) {
                    rateString = bookingDate.getRate().toString();
                } else {
                    rateString = "Use default";
                }

                final Object boxover = new Object() {

                    @Override
                    public String toString() {

                        StringBuilder sb = new StringBuilder();
                        sb
                                .append("<div class='boxover' id='boxover" +
                                        date2.getTimestamp() +
                                        "' style='text-align: left; display: none; position: absolute; background: white; border: 1px solid black; padding: 4px 8px; font-size: 12px;'>Rate: " +
                                        rateString + "</div>");

                        sb.append(popuplink);

                        return sb.toString();
                    }

                };

                /* if (bookingDate.getBookings() > 0) {

                  keypad.addObject(boxover, "booked");

              } else {*/

                switch (bookingDate.getStatus()) {
                    case Booked:
                        keypad.addObject(boxover, "booked");
                        break;
                    case Unavailable:
                        keypad.addObject(boxover, "unavailable");
                        break;

                    case Reserved:
                        keypad.addObject(boxover, "reserved");
                        break;

                    case Open:
                        keypad.addObject(boxover, "open");
                        break;
                    default:
                }
//                }
            }

            if (dates.contains(date)) {

            } else {

            }
            date = date.nextDay();
        }

        return keypad.toString();

    }
}
