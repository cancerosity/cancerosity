package org.sevensoft.ecreator.model.bookings.markers;

import java.util.Map;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 19 Mar 2007 14:12:10
 *
 */
public class NightsMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		Date start = (Date) context.getAttribute("bookingStart");
		Date end = (Date) context.getAttribute("bookingEnd");

		if (start == null || end == null) {
			return null;
		}

		int nights = end.getDaysBetween(start);
		if (nights == 0) {
			return null;
		}

		return super.string(context, params, nights);

	}

	@Override
	public String getDescription() {
		return "Shows the number of nights searched for";
	}

	public Object getRegex() {
		return "booking_nights";
	}
}
