package org.sevensoft.ecreator.model.bookings;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.Day;
import org.sevensoft.commons.samdate.Month;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.bookings.BookingSlot.Status;
import org.sevensoft.ecreator.model.ecom.orders.VatHelper;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sks 30 Dec 2006 17:31:09
 *         <p/>
 *         Contains booking details for a particular item
 */
@Table("bookings_setup")
public class BookingSetup extends EntityObject {

    public static BookingSetup get(RequestContext context, Item item) {
        BookingSetup setup = SimpleQuery.get(context, BookingSetup.class, "item", item);
        return setup == null ? new BookingSetup(context, item) : setup;
    }

    private Map<Integer, Amount> discounts;

    private Item item;

    /**
     * Max number of adults per item
     * Zero means no limit (up to max occupants obviously)
     */
    private int maxAdults;

    /**
     * Max number of occupants each item
     * (Zero means no limit)
     */
    private int maxOccupants;

    /**
     * Max number of children in this item
     * Zero means no limit (up to max occupants obviously)
     */
    private int maxChildren;

    /**
     * Max number of dates one can book in a row
     */
    private int maxNights;

    /**
     * Total number of items available to book, ie if this was rooms then a qty of 3 would mean 3 rooms
     */
    private int qty;

    /**
     * The default rate
     */
    private Money rate;

    protected BookingSetup(RequestContext context) {
        super(context);
    }

    public BookingSetup(RequestContext context, Item item) {
        super(context);
        this.item = item;
        save();
    }

    public BookingSlot addBookingSlot(Date date) {
        return new BookingSlot(context, item, date);
    }

    public void addDiscount(int nights, Amount discount) {
        new BookingDiscount(context, item, nights, discount);
    }

    public void addOption(String name) {
        new BookingOption(context, item, name);
    }

    public void applyToDates(Date sourceDate, Month month, Day day) {

        BookingSlot bookingDate = getBookingSlot(sourceDate);
        BookingSlot.Status status;
        Money rate;

        if (bookingDate == null) {

            status = null;
            rate = null;

        } else {

            rate = bookingDate.getRate();
            status = bookingDate.getStatus();

        }

        Date date = sourceDate.beginYear();
        for (int n = 0; n < date.getDaysInYear(); n++) {

            if (day == null || date.getDay() == day) {

                if (month == null || date.getMonth() == month) {

                    if (status == null) {
                        removeBookingDate(date);

                    } else {

                        BookingSlot slot = getBookingSlot(date);
                        if (slot == null) {
                            slot = addBookingSlot(date);
                        }

                        slot.setStatus(status);
                        slot.setRate(rate);

                        slot.save();
                    }
                }
            }

            date = date.nextDay();
        }
    }

    /*public void changeDate(Date date) {

        BookingSlot bd = getBookingSlot(date);
        if (bd == null) {

            addBookingSlot(date);

        } else {

            switch (bd.getStatus()) {

                default:
                    break;

                case Block:
                    removeBookingDate(date);
                    break;

                case Open:
                    bd.setStatus(BookingSlot.Status.Block);

            }

        }
    }*/

    public BookingSlot getBookingSlot(Date date) {
        return SimpleQuery.get(context, BookingSlot.class, "date", date, "item", item);
    }

    public List<BookingSlot> getBookingDates() {
        return SimpleQuery.execute(context, BookingSlot.class, "item", item);
    }

    public Money getChargeInc(Date start, Date end) {
        Money ex = getChargeEx(start, end);
        return VatHelper.inc(context, ex, 17.5);
    }

    public Money getChargeEx_old(Date start, Date end) {

        Money charge = new Money(0);

        int nights = start.getDaysBetween(end);
        logger.fine("[BookingSetup] calculating charge, nights=" + nights);

        // for each booking date, get rate, or use default
        for (int n = 0; n < nights; n++) {

            BookingSlot bookingDate = getBookingSlot(start);
            if (bookingDate != null && bookingDate.hasRate() && bookingDate.getStatus() == BookingSlot.Status.Open) {

                charge = charge.add(bookingDate.getRate());

            } else if (hasRate()) {

                charge = charge.add(rate);
            }

            start = start.nextDay();
        }

        return charge;
    }

    public Money getChargeEx(Date start, Date end) {

        Money charge = new Money(0);
        List<Date> days = Date.getDaysBetween(start, end);
        int nights = days.size();
        logger.fine("[BookingSetup] calculating charge, nights=" + nights);
        if (nights < 7) {
            charge = calculateDaily(days, charge);
        } else {
            ItemPeriodPrice enclosingPeriod = ItemPeriodPriceUtils.getEnclosingPeriod(context, item, start, end);
            if (enclosingPeriod != null) {
                charge = doCalculate(days, charge, enclosingPeriod, start, end);
            } else {
                ItemPeriodPrice leftPeriod = ItemPeriodPriceUtils.getIntersectingPeriod(context, item, start);
                if (leftPeriod != null) {
                    charge = doCalculate(days, charge, leftPeriod, start, leftPeriod.getPeriod().getEndDate());
                }
                ItemPeriodPrice rightPeriod = ItemPeriodPriceUtils.getIntersectingPeriod(context, item, end);
                if (rightPeriod != null) {
                    charge = doCalculate(days, charge, rightPeriod, rightPeriod.getPeriod().getStartDate(), end);
                }
                List<ItemPeriodPrice> innerPeriods = ItemPeriodPriceUtils.getInnerPeriods(context, item, start, end);
                for (ItemPeriodPrice innerPeriod : innerPeriods) {
                    charge = doCalculate(days, charge, innerPeriod, innerPeriod.getPeriod().getStartDate(), innerPeriod.getPeriod().getEndDate());
                }
                charge = calculateDaily(days, charge);
            }
        }
        return charge;
    }

    private Money doCalculate(List<Date> allDates, Money charge, ItemPeriodPrice period, Date start, Date end) {
        List<Date> days = Date.getDaysBetween(start, end);
        while (days.size() >= 7) {
            List<Date> week = days.subList(0, 7);
            charge = charge.add(period.getWeeklyPrice());
            allDates.removeAll(week);
            days = days.subList(7, days.size());
        }
        charge = calculateDaily(days, charge);
        allDates.removeAll(days);
        return charge;
    }

    private Money calculateDaily(List<Date> days, Money charge) {
        for (Date day : days) {
            BookingSlot bookingDate = getBookingSlot(day);
            if (bookingDate != null && bookingDate.hasRate() && bookingDate.getStatus() == BookingSlot.Status.Open) {
                charge = charge.add(bookingDate.getRate());
            } else if (hasRate()) {
                charge = charge.add(rate);
            }
        }
        return charge;
    }


    public Map<Date, BookingSlot> getDatesMap(List<BookingSlot> dates) {

        Map<Date, BookingSlot> datesMap = new HashMap();

        for (BookingSlot date : dates) {
            datesMap.put(date.getDate(), date);
        }

        return datesMap;
    }

    public final List<BookingDiscount> getDiscounts() {
        return SimpleQuery.execute(context, BookingDiscount.class, "item", item);
    }

    public Item getItem() {
        return item.pop();
    }

    public final int getMaxAdults() {
        return maxAdults;
    }

    public final int getMaxChildren() {
        return maxChildren;
    }

    public int getMaxNights() {
        return maxNights;
    }

    public final int getMaxOccupants() {
        return maxOccupants;
    }

    public List<BookingOption> getOptions() {
        return SimpleQuery.execute(context, BookingOption.class, "item", item);
    }

    public final int getQty() {
        return qty;
    }

    public final Money getRate() {
        return rate;
    }

    private boolean hasRate() {
        return rate != null && rate.isPositive();
    }

    public void removeAll(Date year) {
        Query q = new Query(context, "delete from # where item=? and date>=? and date<?");
        q.setTable(BookingSlot.class);
        q.setParameter(item);
        q.setParameter(year.beginYear());
        q.setParameter(year.nextYear());
        q.run();
    }

    public void removeBookingDate(Date date) {
        SimpleQuery.delete(context, BookingSlot.class, "item", item, "date", date);
    }

    /**
     * Remove all booking dates between this date range (inclusive)
     */
    public void removeDates(Date start, Date end) {
        Query q = new Query(context, "delete from # where date>=? and date<=?");
        q.setTable(BookingSlot.class);
        q.setParameter(start);
        q.setParameter(end);
        q.run();
    }

    public void removeDiscount(BookingDiscount discount) {
        discount.delete();
    }

    public void removeOption(BookingOption option) {
        option.delete();
    }

    public final void setDiscounts(Map<Integer, Amount> discounts) {
        this.discounts = discounts;
    }

    public final void setMaxAdults(int maxAdults) {
        this.maxAdults = maxAdults;
    }

    public final void setMaxChildren(int maxChildren) {
        this.maxChildren = maxChildren;
    }

    public void setMaxNights(int maxNights) {
        this.maxNights = maxNights;
    }

    public final void setMaxOccupants(int maxOccupants) {
        this.maxOccupants = maxOccupants;
    }

    public final void setQty(int qty) {
        this.qty = qty;
    }

    public void setRate(Date date, Money rate) {

        BookingSlot bookingDate = getBookingSlot(date);
        if (bookingDate == null) {
            return;
        }

        bookingDate.setRate(rate);
        bookingDate.save();
    }

    public final void setRate(Money rate) {
        this.rate = rate;
    }

    public void setStatus(Date day, Status status) {

        if (status == null) {

            removeBookingDate(day);

        } else {

            BookingSlot date = getBookingSlot(day);

            if (date == null) {
                date = addBookingSlot(day);
            }

            date.setStatus(status);
            date.save();
        }
    }

}
