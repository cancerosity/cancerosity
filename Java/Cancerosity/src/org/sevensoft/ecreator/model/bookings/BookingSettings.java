package org.sevensoft.ecreator.model.bookings;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.ecreator.model.attributes.support.AttributeOwnerSupport;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Mar 2007 11:52:17
 *
 */
@Table("bookings_settings")
@Singleton
public class BookingSettings extends AttributeOwnerSupport {

	public static BookingSettings getInstance(RequestContext context) {
		return EntityObject.getSingleton(context, BookingSettings.class);
	}

	private String		initialStatus;
	private String		emailInstructions;
	private List<String>	referrers;
	private String		completedText;
	private List<String>	thirdPartyEmailNotifications;

	protected BookingSettings(RequestContext context) {
		super(context);
	}

	public String getCompletedText() {
		return completedText == null ? "Booking completed thank you.<br/><br/>Your have booked [item] for [stay] nights from [start] to [end].<br/><br/>"
				+ "Your booking reference is <strong>[id]</strong>.<br/><br/><a href='#' onclick='window.print();'>Click here</a> to print this page."
				: completedText;
	}

	public String getCompletedTextRendered(Booking booking) {

		String string = getCompletedText();

		string = string.replace("[id]", booking.getIdString());
		string = string.replace("[name]", booking.getAccount().getName());
		string = string.replace("[email]", booking.getAccount().getEmail());
		string = string.replace("[start]", booking.getStart().toString("dd-MMM-yyyy"));
		string = string.replace("[end]", booking.getEnd().toString("dd-MMM-yyyy"));
		string = string.replace("[item]", booking.getItem().getName());
		string = string.replace("[stay]", String.valueOf(booking.getStay()));

		return string;
	}

	public String getEmailInstructions() {
		return emailInstructions == null ? "Your email address is used to acknowledge the order and to contact you if there is a problem."
				: emailInstructions;
	}

	public final String getInitialStatus() {
		return initialStatus;
	}

	public final List<String> getReferrers() {
		if (referrers == null) {
			referrers = new ArrayList();
		}
		return referrers;
	}

	public List<String> getStatuses() {
		Query q = new Query(context, "select distinct status from # order by status");
		q.setTable(Booking.class);
		return q.getStrings();
	}

	public boolean hasReferrers() {
		return false;
	}

	public final void setCompletedText(String completedText) {
		this.completedText = completedText;
	}

	public final void setInitialStatus(String initialStatus) {
		this.initialStatus = initialStatus;
	}

	public final void setReferrers(List<String> referrers) {
		this.referrers = referrers;
	}

	public List<String> getThirdPartyEmailNotifications() {
		if (thirdPartyEmailNotifications == null) {
			thirdPartyEmailNotifications = new ArrayList();
		}
		return thirdPartyEmailNotifications;
	}

	public void setThirdPartyEmailNotifications(List<String> thirdPartyEmailNotifications) {
		this.thirdPartyEmailNotifications = thirdPartyEmailNotifications;
	}
}
