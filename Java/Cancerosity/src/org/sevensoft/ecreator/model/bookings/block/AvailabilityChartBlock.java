package org.sevensoft.ecreator.model.bookings.block;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.iface.admin.ecom.bookings.AvailabilityChartBlockHandler;
import org.sevensoft.ecreator.iface.frontend.bookings.BookingHandler;
import org.sevensoft.ecreator.model.bookings.BookingSetup;
import org.sevensoft.ecreator.model.bookings.BookingSlot;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Label;

import java.util.List;
import java.util.Map;

/**
 * @author sks 15 Feb 2007 16:10:28
 *         <p/>
 *         Shows the availability of a month at a glance
 */
@Table("blocks_availabilitychart")
@Label("Availability chart")
@HandlerClass(AvailabilityChartBlockHandler.class)
public class AvailabilityChartBlock extends Block {

    public AvailabilityChartBlock(RequestContext context) {
        super(context);
    }

    public AvailabilityChartBlock(RequestContext context, BlockOwner owner, int objId) {
        super(context, owner, 0);
    }

    @Override
    public Object render(RequestContext context) {

        Item item = (Item) context.getAttribute("item");
        if (item == null) {
            logger.fine("[AvailabilityChartBlock] no item, exiting");
            return null;
        }

        Date year = new Date().beginYear();
        StringBuilder sb = new StringBuilder();

        BookingSetup bookingSetup = item.getBookingSetup();
        List<BookingSlot> dates = bookingSetup.getBookingDates();
        Map<Date, BookingSlot> datesMap = bookingSetup.getDatesMap(dates);

        sb.append("<style>");
        sb.append("table.ec_availabilitychart caption { padding: 5px; font-family: Arial; font-size: 16px; font-weight: bold; text-align: left; } ");
        sb.append("table.ec_availabilitychart { font-family: Arial; border-collapse: collapse; } ");
        sb.append("table.ec_availabilitychart td { border: 1px solid #eeeeee; padding: 2px;} ");
        sb.append("table.ec_availabilitychart td.month { font-weight: bold; } ");
        sb.append("table.ec_availabilitychart td.day { text-align: center; width: 15px; font-weight: bold; } ");
        sb.append("table.ec_availabilitychart td.open { background: #b8e39c; text-align: center; } ");
        sb.append("table.ec_availabilitychart td.reserved { background: #ff69b4; text-align: center; } ");
        sb.append("table.ec_availabilitychart td.unavailable { background: #4f4f4f; text-align: center; } ");
        sb.append("table.ec_availabilitychart td.booked { background: #faa9a5; text-align: center; } ");
        sb.append("</style>");

        sb.append(new TableTag("ec_availabilitychart").setCaption(year.toString("yyyy") + " availability"));

        // do header days
        sb.append("<tr>");
        sb.append("<td></td>");
        for (int n = 1; n <= 31; n++) {
            sb.append("<td class='day'>" + n + "</td>");
        }
        sb.append("</tr>");

        Date month = year;
        Date lastAvailableDay = null;
        for (int n = 0; n < 12; n++) {
            for (int m = 0; m < 31; m++) {
                if (m < month.getDaysInMonth()) {
                    BookingSlot date = datesMap.get(month.addMonths(n).addDays(m));
                    if (!(date == null || date.getBookings() > 0)) {
                        lastAvailableDay = date.getDate();
                    }
                }
            }
        }

        for (int n = 0; n < 12; n++) {

            sb.append("<tr>");
            sb.append("<td class='month'>");
            sb.append(month.toString("MMM"));
            sb.append("</td>");

            Date day = month;
            for (int m = 0; m < 31; m++) {

                if (m < month.getDaysInMonth()) {

                    BookingSlot date = datesMap.get(day);
                    if (date == null || date.getBookings() > 0) {

                        sb.append("<td class='booked'>B</td>");

                    } else {

                        switch (date.getStatus()) {

                            default:
                                break;

                            case Unavailable:
                                sb.append("<td class='unavailable'>A</td>");
                                break;

                            case Reserved:
                                sb.append("<td class='reserved'>A</td>");
                                break;

                            case Open:
//                                sb.append("<td class='open'>" + new LinkTag(BookingHandler.class, "start", "A", "item", item, "date", day) + "</td>");
                                sb.append("<td class='open'>" + new LinkTag(BookingHandler.class, "start", "A", "item", item, "start", day, "end", lastAvailableDay) + "</td>");
                                break;
                        }
                    }

                    day = day.nextDay();

                } else {

                    sb.append("<td class='empty'> </td>");
                }
            }

            sb.append("</tr>");
            month = month.nextMonth();
        }

        sb.append("</table>");

        return sb.toString();
    }

}
