package org.sevensoft.ecreator.model.sitemap.marker;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.sitemap.blocks.SitemapBlock;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * User: Tanya
 * Date: 22.12.2010
 */
public class SitemapHtmlMarker extends MarkerHelper implements IGenericMarker{

    public Object generate(RequestContext context, Map<String, String> params) {
        return new SitemapBlock(context).render(context);
    }

     public Object getRegex() {
        return "html_sitemap";
    }
}
