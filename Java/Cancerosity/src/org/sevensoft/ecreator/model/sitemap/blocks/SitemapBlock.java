package org.sevensoft.ecreator.model.sitemap.blocks;

import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.sitemap.SitemapCategory;
import org.sevensoft.ecreator.model.sitemap.SitemapSettings;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.db.annotations.Table;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;

/**
 * User: Tanya
 * Date: 16.11.2010
 */
@Table("blocks_sitemap")
@Label("Site map")
public class SitemapBlock extends Block {

    private transient SitemapSettings sitemapSettings;

    public SitemapBlock(RequestContext context) {
        super(context);
        this.sitemapSettings = SitemapSettings.getInstance(context);
    }

    public SitemapBlock(RequestContext context, BlockOwner owner, int objId) {
        super(context, owner, objId);
        this.sitemapSettings = SitemapSettings.getInstance(context);
    }

    public Object render(RequestContext context) {
        if (!Module.SiteMap.enabled(context)) {
            return null;
        }
        List<Category> categoryList = SitemapCategory.getCategories(context);

        Category category = Category.getRoot(context);
        StringBuilder sb = new StringBuilder();
        sb.append("<div id='ec_sitemap'>");

        if (sitemapSettings.isShowHiddenCategories()) {
            categoryRender(sb, category.getChildren(), 1, "parent", categoryList);
        } else {
            categoryRender(sb, category.getVisibleChildren(), 1, "parent", categoryList);
        }

        sb.append("</div>");

        return sb.toString();
    }

    private void categoryRender(StringBuilder sb, List<Category> children, int inline, String clazz, List<Category> categoryList) {
        for (Category c : children) {
            if (!categoryList.contains(c)) {
                continue;
            }
            if (c.isHidden() && !sitemapSettings.isShowHiddenCategories()) {
                continue;
            }

            sb.append("<li>");

            sb.append(new LinkTag(c.getUrl(), c.getName()).setClass(clazz).setId("category_inner_" + c.getIdString()));

            if (sitemapSettings.isShowItems())
                itemsRender(sb, c);


            sb.append("<ul class='inline" + inline + "'>");
            categoryRender(sb, c.getChildren(), inline + 1, "category_inner_" + inline, categoryList);
            sb.append("</ul>");
        }
    }

    private void itemsRender(StringBuilder sb, Category c) {
        Map<String, String> categoryItems = c.getItemsOptions(context);

        for (Map.Entry<String, String> obj : categoryItems.entrySet()) {
            sb.append("<li>");
            sb.append(new LinkTag(obj.getKey(), obj.getValue()).setClass("items_inner"));
        }

        List<Item> wrappedItems = new ArrayList<Item>();
        if (c.isSearchWrapper()) {
            ItemSearcher searcher;
            searcher = c.getSearch().getSearcher();
            searcher.setStatus("Live");
            searcher.setSortType(SortType.Name);
            wrappedItems = searcher.getItems();
        }

        for (Item obj : wrappedItems) {
            sb.append("<li>");
            sb.append(new LinkTag(obj.getFriendlyUrl(), obj.getName()).setClass("items_inner"));
        }
    }
}
