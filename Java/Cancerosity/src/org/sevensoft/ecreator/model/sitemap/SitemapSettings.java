package org.sevensoft.ecreator.model.sitemap;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.sitemap.renderers.SitemapMarkupDefault;

/**
 * User: alchemist
 * Date: 16.11.2010
 */

@Table("settings_sitemap")
@Singleton
public class SitemapSettings extends EntityObject{

    protected SitemapSettings(RequestContext context) {
        super(context);
    }

    public static SitemapSettings getInstance(RequestContext context) {
        return getSingleton(context, SitemapSettings.class);
    }

    private Markup markup;

    private boolean showItems;

    private boolean showHiddenCategories;

    public Markup getMarkup() {
        if (markup == null) {
            markup = new Markup(context, "Sitemap markup", new SitemapMarkupDefault());
            save();
        }
        return markup.pop();
    }

    public void setMarkup(Markup markup) {
        this.markup = markup;
    }

    public boolean isShowItems() {
        return showItems;
    }

    public void setShowItems(boolean showItems) {
        this.showItems = showItems;
    }

    public boolean isShowHiddenCategories() {
        return showHiddenCategories;
    }

    public void setShowHiddenCategories(boolean showHiddenCategories) {
        this.showHiddenCategories = showHiddenCategories;
    }
}
