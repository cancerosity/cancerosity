package org.sevensoft.ecreator.model.sitemap;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.containers.boxes.BoxWhere;

import java.util.List;

/**
 * User: Tanya
 * Date: 16.11.2010
 */

@Table("sitemap_categories")
public class SitemapCategory extends EntityObject {

    private Category category;

    protected SitemapCategory(RequestContext context) {
        super(context);
    }

    public SitemapCategory(RequestContext context, Category category) {
        super(context);
        this.category = category;

        save();
    }

    public Category getCategory() {
        return category.pop();
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public static List<Category> getCategories(RequestContext context) {
        Query q = new Query(context, "select c.* from # c join # sc on c.id=sc.category order by c.fullname");
        q.setTable(Category.class);
        q.setTable(SitemapCategory.class);
        return q.execute(Category.class);
//        return new Query(context, " select category from #").setTable(SitemapCategory.class).execute(Category.class);
    }

    public static List<SitemapCategory> getSitemapCategories(RequestContext context) {
        Query q = new Query(context, "select sc.* from # sc join # c on sc.category=c.id order by c.fullname ");
        q.setTable(SitemapCategory.class);
        q.setTable(Category.class);
        return q.execute(SitemapCategory.class);
//        return new Query(context, " select category from #").setTable(SitemapCategory.class).execute(Category.class);
    }
}
