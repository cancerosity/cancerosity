package org.sevensoft.ecreator.model.sitemap.renderers;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * User: Tanya
 * Date: 16.11.2010
 */
public class SitemapMarkupDefault implements MarkupDefault {

    public String getBetween() {
        return null;
    }

    public String getBody() {
        return null;
    }

    public String getEnd() {
        return null;
    }

    public String getStart() {
        return null;
    }

    public int getTds() {
        return 0;
    }

    public String getCss() {
        StringBuilder sb = new StringBuilder();
        sb.append("#ec_sitemap .parent { display: block; width: 100%; margin-top: 30px; clear: both; " +
                "border-bottom: 1px solid #cccccc; line-height: 30px; font-family: 'Segoe UI', Arial, Helvetica, sans-serif; font-size: 24px; " +
                "color: #000000; text-decoration: none; }\n");
        sb.append("#ec_sitemap .parent:hover { color: #555555; text-decoration: none; }\n");
        sb.append("#ec_sitemap .parent { background: transparent url(/files/graphics/misc/parent.gif) no-repeat scroll right center; }\n\n");

        sb.append("#ec_sitemap .inline1 { list-style: none; margin: 0; padding: 0; }\n");
        sb.append("#ec_sitemap .inline1 li { margin: 20px 0 0 0; padding: 0 0 2px 0; font-size: 12px; display: block; width: 50%; float: left; }\n");
        sb.append("#ec_sitemap .inline1 a { font-weight: bold; text-decoration: none; }\n");
        sb.append("#ec_sitemap .inline1 a:hover { color: #555555; text-decoration: none; }\n\n");

        sb.append("#ec_sitemap .inline2 { list-style: none; margin: 0; padding: 0; }\n");
        sb.append("#ec_sitemap .inline2 li {  display: block; width: 100%; float: none; margin: 0; padding: 2px 0; font-size: 11px; }\n");
        sb.append("#ec_sitemap .inline2 a { font-weight: normal; color: #555555; text-decoration: none; }\n");
        sb.append("#ec_sitemap .inline2 a:hover { color: #555555; text-decoration: none; }\n");

        return sb.toString();
    }

    public String getName() {
        return null;
    }
}
