package org.sevensoft.ecreator.model.design.generators.titles.s;

import org.sevensoft.ecreator.model.design.generators.titles.TitlesGenerator;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.jeezy.http.RequestContext;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * @author sks 4 Apr 2007 11:41:15
 */
public class SevensoftPostItGen extends TitlesGenerator {

    public void generate(RequestContext context, File file, String title) throws IOException {

        // load background
        File backgroundFile = ResourcesUtils.getRealTemplateTitle("_background.gif");
        BufferedImage backgroundImage = ImageIO.read(backgroundFile);
        Graphics2D g2 = (Graphics2D) backgroundImage.getGraphics();

        // our font does not support anything other than alphas
        title = title.replace("&", " and ").replaceAll("[':;`\"]", "");

        Font font;
        try {
            font = Font.createFont(Font.TRUETYPE_FONT, ResourcesUtils.getRealFont("SCRAOTC_.TTF"));
        } catch (FontFormatException e) {
            throw new IOException(e.toString());
        }
        font = font.deriveFont(20f);
        g2.setFont(font);

        FontMetrics metrics = g2.getFontMetrics();

        // work out sizes for the string
        Rectangle2D r = metrics.getStringBounds(title, g2);

        // get sizes
        int width = (int) r.getWidth() + 5;

        BufferedImage image = new BufferedImage(width, (int) (r.getHeight() + 10), BufferedImage.TYPE_INT_RGB);
        g2 = (Graphics2D) image.getGraphics();

        // tile background
        int x = 0;
        while (x <= width) {
            g2.drawImage(backgroundImage, x, 0, null);
            x = x + backgroundImage.getWidth();
        }

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        Color color = new Color(68, 68, 68);

        g2.setColor(color);
        g2.setFont(font);
        g2.drawString(title, 0, (int) r.getHeight());

        // write file out

        ImageIO.write(image, "gif", file);

    }
}
