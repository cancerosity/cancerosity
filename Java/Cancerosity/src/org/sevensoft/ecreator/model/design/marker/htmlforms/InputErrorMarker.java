package org.sevensoft.ecreator.model.design.marker.htmlforms;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;

/**
 * @author sks 27 Jul 2006 18:50:21
 *
 */
public class InputErrorMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		String id = params.get("id");
		if (id == null) {
			return null;
		}

		return new ErrorTag(context, "data_" + id, "<br/>");
	}

	public Object getRegex() {
		return "error";
	}

}
