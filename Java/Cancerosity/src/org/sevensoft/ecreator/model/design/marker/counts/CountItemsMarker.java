package org.sevensoft.ecreator.model.design.marker.counts;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.ICategoryMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.EntityObject;

/**
 * @author sks 04-Apr-2006 12:41:24
 *
 */
public final class CountItemsMarker implements IGenericMarker, ICategoryMarker {

    public Object generate(RequestContext context, Map<String, String> params, Category category) {
        Integer count = 0;
        ItemType itemType = null;

        if (params.containsKey("categoryEsc")){
            return generate(context, params);
        }
        
        if (params.containsKey("itemtype")) {
            itemType = EntityObject.getInstance(context, ItemType.class, params.get("itemtype"));
        }

        count = category.getItemsCount(itemType, "Live", 0);
        return "<span class='count_items'>" + count + "</span>";
    }

    public Object generate(RequestContext context, Map<String, String> params) {
        Integer count = 0;
        if (params.containsKey("itemtype")) {
            count = new Query(context, "SELECT COUNT(*) FROM # WHERE itemType=? AND status=?").setTable(Item.class).setParameter(params.get("itemtype").trim()).setParameter("LIVE").getInt();
        }
        return "<span class='count_items'>" + count + "</span>";
    }

	public Object getRegex() {
		return "count_items";
	}
}
