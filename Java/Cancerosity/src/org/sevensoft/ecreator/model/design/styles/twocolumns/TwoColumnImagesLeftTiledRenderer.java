package org.sevensoft.ecreator.model.design.styles.twocolumns;

import java.util.List;

import org.sevensoft.ecreator.model.design.styles.StyleRenderer;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 29 Apr 2007 14:35:50
 *
 */
public class TwoColumnImagesLeftTiledRenderer implements StyleRenderer {

	public String render(String content, List<Img> images) {

		StringBuilder sb = new StringBuilder();
		sb.append("<!-- start style renderer -->\n");

		sb.append(new TableTag("ecTwoColumnsStyle"));
		sb.append("<tr><td class='images' valign='top' width='200'>");

		for (Img img : images) {

			sb.append(img.getFullsizeImageTag(200, 0));
		}

		sb.append("<td><td class='content'>");
		sb.append(content);
		sb.append("</td></tr>");
		sb.append("</table>");

		sb.append("<!-- end style renderer -->\n");

		return sb.toString();
	}
}
