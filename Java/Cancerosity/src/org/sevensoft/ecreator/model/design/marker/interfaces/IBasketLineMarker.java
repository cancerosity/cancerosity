package org.sevensoft.ecreator.model.design.marker.interfaces;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.Marker;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 28 Jun 2006 11:51:51
 *
 */
public interface IBasketLineMarker extends Marker {

	public Object generate(RequestContext context, Map<String, String> params, BasketLine line);
}
