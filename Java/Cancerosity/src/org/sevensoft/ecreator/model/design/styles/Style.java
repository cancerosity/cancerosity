package org.sevensoft.ecreator.model.design.styles;

import java.util.List;

import org.sevensoft.ecreator.model.design.styles.onecolumn.ZigZagStyleRenderer;
import org.sevensoft.ecreator.model.design.styles.twocolumns.TwoColumnImagesLeftTiledRenderer;
import org.sevensoft.ecreator.model.media.images.Img;

/**
 * @author sks 29 Apr 2007 14:35:05
 * 
 * Styles are for laying out content
 *
 */
public enum Style {

	ZigZagImages("Zig zag images", new ZigZagStyleRenderer()),

	TwoColumnImagesLeftTiled("Two columns; images left tiled", new TwoColumnImagesLeftTiledRenderer());

	private final String		toString;
	private final StyleRenderer	styleRenderer;

	Style(String toString, StyleRenderer styleRenderer) {
		this.toString = toString;
		this.styleRenderer = styleRenderer;
	}

	public final StyleRenderer getStyleRenderer() {
		return styleRenderer;
	}

	@Override
	public String toString() {
		return toString;
	}

	public String render(String content, List<Img> images) {
		return styleRenderer.render(content, images);
	}

}
