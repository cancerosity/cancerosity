package org.sevensoft.ecreator.model.design.marker.interfaces;

import java.util.Map;

import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.design.marker.Marker;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 12 Jul 2006 09:17:36
 *
 */
public interface IAttachmentMarker extends Marker {

	public Object generate(RequestContext context, Map<String, String> params, Attachment attachment);
}
