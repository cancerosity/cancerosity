package org.sevensoft.ecreator.model.design.util;

/**
 * @author sks 22 Sep 2006 19:46:01
 * 
 * An object implements grid cell if it can be laid out in a grid via a grid renderer
 *
 */
public interface GridCellOld {

	/**
	 * 
	 */
	public int getCellSpan();

	/**
	 * 
	 */
	public int getCellsPerRow();

	/**
	 * 
	 */
	public Object getGridContents();

	/**
	 * 
	 */
	public String getGridDescription();

	/**
	 * 
	 */
	public String getGridLabel();

	/**
	 * 
	 */
	public String getGridRowId();

}
