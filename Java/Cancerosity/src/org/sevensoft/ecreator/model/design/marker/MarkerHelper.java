package org.sevensoft.ecreator.model.design.marker;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.extras.languages.Language;
import org.sevensoft.ecreator.model.extras.languages.tables.Translation;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Link;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author sks 22 Nov 2006 14:42:01
 */
public abstract class MarkerHelper implements Marker {

    protected static final Logger logger = Logger.getLogger("markers");

    public String getDescription() {
        return "";
    }

    public String getParams() {
        return "";
    }

    private String getPopupCode(Map<String, String> params, String url) {

        List<String> args = new ArrayList<String>();

        if (params.containsKey("width")) {
            args.add("width=" + params.get("width"));
        }

        if (params.containsKey("height")) {
            args.add("height=" + params.get("height"));
        }

        if (params.containsKey("scrollbars")) {
            args.add("scrollbars=" + params.get("scrollbars"));
        }

        if (params.containsKey("resize")) {
            args.add("resize=" + params.get("resize"));
        }

        String windowName = params.get("windowName");
        if (windowName == null) {
            windowName = "popupwindow";
        }

        return "window.open('" + url + "','" + windowName + "', '" + StringHelper.implode(args, ",", true) + "'); return false;";

    }

    public Object link(RequestContext context, Map<String, String> params, Link link) {
        return link(context, params, link, null);
    }

    public Object link(RequestContext context, Map<String, String> params, Link link, String clazz) {
        return link(context, params, context.getContextPath() + "/" + link, clazz);
    }


    protected Object link(RequestContext context, Map<String, String> params, String url, String clazz) {

        String align = params.get("align");
        String onClick = params.get("onclick");
        String id = params.get("id");
        if (params.containsKey("class") || clazz == null) {
            clazz = params.get("class");
        }
        String suffix = params.get("suffix");
        String prefix = params.get("prefix");
        String tag = params.get("tag");
        String src = params.get("src");
        String srchover = params.get("srchover");
        String target = params.get("target");
        Object trail=null;
        if (params.containsKey("trail")){
           trail=context.getAttribute("trailCategory");
        }

        String label = params.get("label");
        if (label != null) {

            ButtonTag buttonTag = new ButtonTag(url, label);

            if (clazz != null) {
                buttonTag.setClass(clazz);
            }

            if (align != null) {
                buttonTag.setAlign(align);
            }

            if (id != null) {
                buttonTag.setId(id);
            }

            if (onClick != null) {
                buttonTag.setOnClick(onClick);
            }

            if (trail != null) {
                buttonTag.addParameter("trailCategory", trail);
            }
            return buttonTag;
        }

        if (src != null) {

            ImageTag imageTag = new ImageTag(src);
            if (srchover != null) {
                imageTag.setOnMouseOver("this.src='" + srchover + "'");
                imageTag.setOnMouseOut("this.src='" + src + "'");
            }

            LinkTag linkTag;

            if (params.containsKey("popup")) {

                linkTag = new LinkTag("#", imageTag);
                linkTag.setOnClick(getPopupCode(params, url));
                return linkTag;

            } else {

                linkTag = new LinkTag(url, imageTag);

                if (target != null) {
                    linkTag.setTarget(target);
                }

                if (onClick != null) {
                    linkTag.setOnClick(onClick);
                }

            }

            if (clazz != null) {
                linkTag.setClass(clazz);
            }

            if (align != null) {
                linkTag.setAlign(align);
            }

            if (id != null) {
                linkTag.setId(id);
            }

            return linkTag;
        }

        String text = params.get("text");
        if (text != null) {

            // check for languages and if enabled resolve text
            text = Translation.translate(context, (Language) context.getAttribute("language"), text);

            if (tag == null) {
                tag = "span";
            }

            StringBuilder sb = new StringBuilder();
            sb.append("<" + tag);

            if (clazz != null) {
                sb.append(" class='" + clazz + "'");
            }

            if (id != null) {
                sb.append(" id='" + id + "'");
            }

            sb.append(">");

            if (prefix != null) {
                prefix = Translation.translate(context, (Language) context.getAttribute("language"), prefix);
                sb.append(prefix);
            }

            LinkTag linkTag;

            if (params.containsKey("popup")) {

                linkTag = new LinkTag("#", text);
                linkTag.setOnClick(getPopupCode(params, url));

            } else {

                linkTag = new LinkTag(url, text);

                if (target != null) {
                    linkTag.setTarget(target);
                }

                if (onClick != null) {
                    linkTag.setOnClick(onClick);
                }

            }


                if (trail != null) {
                    linkTag.setParameter("trailCategory", trail);
                }

            sb.append(linkTag);

            if (suffix != null) {
                suffix = Translation.translate(context, (Language) context.getAttribute("language"), suffix);
                sb.append(suffix);
            }

            sb.append("</" + tag + ">");

            return sb;
        }

        logger.fine("[" + getClass().getSimpleName() + "] no text, src or label provided");
        return null;
    }

    protected Object string(RequestContext context, Map<String, String> params, Object toString) {

        if (toString == null) {
            return null;
        }

        String string = toString.toString();
        if (string == null) {
            return null;
        }

        logger.fine("[MarkerHelper] generate marker string for string=" + string);

        String clazz = params.get("class");
        String tag = params.get("tag");

        if (tag == null) {
            tag = "span";
        }

        String suffix = params.get("suffix");
        String prefix = params.get("prefix");

        StringBuilder sb = new StringBuilder();

        if (clazz != null) {
            sb.append("<" + tag + " class=\"" + clazz + "\">");
        }

        if (prefix != null) {
            prefix = Translation.translate(context, (Language) context.getAttribute("language"), prefix);
            sb.append(prefix);
        }

        sb.append(string);

        if (suffix != null) {
            suffix = Translation.translate(context, (Language) context.getAttribute("language"), suffix);
            sb.append(suffix);
        }

        if (clazz != null) {
            sb.append("</" + tag + ">");
        }

        return sb;

    }
}
