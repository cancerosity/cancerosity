package org.sevensoft.ecreator.model.design.template.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.modules.Modules;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 7 Jun 2006 11:04:05
 *
 */
public class CopyrightTag implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {
		return Modules.getInstance(context).getCopyrightMessageRendered();
	}

	public Object getRegex() {
		return "copyright";
	}

}
