package org.sevensoft.ecreator.model.design.generators.boxes;

import java.io.File;
import java.io.IOException;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 29 Apr 2007 18:38:03
 *
 */
public interface CategoryBoxImagesGenerator {

	public abstract void generate(RequestContext context, File file, Category category) throws IOException;

}
