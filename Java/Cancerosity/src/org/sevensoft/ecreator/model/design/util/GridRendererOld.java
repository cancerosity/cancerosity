package org.sevensoft.ecreator.model.design.util;

import java.util.List;

import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 22 Sep 2006 19:44:38
 *
 */
public class GridRendererOld {

	private List<? extends GridCellOld>	cells;
	private String				title;
	private int					cellcount;

	/**
	 * Prefix for the row id, appended with the id of the object
	 */
	private String				rowIdPrefix;

	public GridRendererOld(List<? extends GridCellOld> bags) {
		this.cells = bags;
		this.cellcount = bags.get(0).getCellsPerRow();
	}

	private void row(StringBuilder sb, GridCellOld cell) {

		sb.append("<tr");

		if (rowIdPrefix != null) {
			sb.append(" id=\"");
			sb.append(rowIdPrefix);
			sb.append(cell.getGridRowId());
			sb.append("\"");
		}

		sb.append(">");
	}

	public void setRowIdPrefix(String rowIdPrefix) {
		this.rowIdPrefix = rowIdPrefix;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		TableTag table = new TableTag("form");

		if (title != null) {
			table.setCaption(title);
		}

		sb.append(table.setWidth("100%"));

		//			sb.append("<tr><td colspan='" + cellcount + "'>* fields are mandatory</td></tr>");

		int n = 0;
		for (GridCellOld cell : cells) {

			if (n == 0) {
				row(sb, cell);
			}

			String label = cell.getGridLabel();
			if (label != null) {

				sb.append("<td class='key' valign='top' align='right' width='180'>");
				sb.append(label);

				String desc = cell.getGridDescription();

				if (desc != null) {
					sb.append("<div class='description'>" + desc + "</div>");
				}

				sb.append("</td>");

				n++;
			}

			if (n == cellcount) {
				n = 0;
				sb.append("</tr>");
				row(sb, cell);
			}

			{

				sb.append("<td class='input' colspan='" + cell.getCellSpan() + "'>");
				sb.append(cell.getGridContents());
				sb.append("</td>");

				n += cell.getCellSpan();
			}

			if (n >= cellcount) {
				n = 0;
				sb.append("</tr>");
			}
		}

		sb.append("</table>");

		return sb.toString();
	}

}
