package org.sevensoft.ecreator.model.design.marker;

/**
 * @author sks 17 May 2006 14:23:14
 *
 */
public interface Marker {

	public Object getRegex();

}
