package org.sevensoft.ecreator.model.design.markup;

/**
 * @author Dmitry Lebedev
 * @version: $id$
 * Date: 05.03.2009
 * Time 14:47:46
 */
public abstract class MarkupCssClassDefault {

    public abstract String getCssClass();

    public abstract int getCycleRows();
} 
