package org.sevensoft.ecreator.model.design.styles;

import java.util.List;

import org.sevensoft.ecreator.model.media.images.Img;

/**
 * @author sks 29 Apr 2007 14:35:58
 *
 */
public interface StyleRenderer {

	/**
	 * 
	 */
	String render(String content, List<Img> images);

}
