package org.sevensoft.ecreator.model.design.markup;

/**
 * @author sks 17 Sep 2006 00:56:36
 *
 */
public interface Wrap<E> {

	String post(E obj);

	String pre(E obj);
}
