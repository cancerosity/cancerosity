package org.sevensoft.ecreator.model.design.marker.generic;

import java.util.Map;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 17 May 2006 14:20:18
 *
 */
public final class DateMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		String format = params.get("format");
		if (format == null) {
			format = "dd-MMM-yyyy";
		}

		return super.string(context, params, new Date().toString(format));
	}

	public Object getRegex() {
		return "date";
	}
}