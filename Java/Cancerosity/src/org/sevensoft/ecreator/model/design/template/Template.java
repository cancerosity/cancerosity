package org.sevensoft.ecreator.model.design.template;

import org.jdom.JDOMException;
import org.sevensoft.commons.simpleio.FileExistRule;
import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.commons.simpleio.SimpleZip;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.marker.MarkerRenderer;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.system.config.TemplateSession;
import org.sevensoft.ecreator.model.system.installer.Installer;
import org.sevensoft.ecreator.model.system.installer.InstallerException;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author sks 6 Jun 2006 17:49:32
 */
@Table("templates")
public class Template extends EntityObject implements Selectable {

    public static List<Template> get(RequestContext context) {

        Query q = new Query(context, "select * from # order by name");
        q.setTable(Template.class);
        return q.execute(Template.class);
    }

    public static Template getDefault(RequestContext context) {

        Query q = new Query(context, "select * from # where dfault=1");
        q.setTable(Template.class);
        return q.get(Template.class);
    }

    /*
      * the name of this template
      */
    private String name;

    /*
      * The top part of the template
      */
    private String before;

    /*
      * Bottom
      */
    private String after;

    /**
     * Goes into the head part of the html
     */
    private String head;

    /**
     *
     */
    private String css;

    /*
      * Is this template the default template
      */
    private boolean dfault;

    private User createdBy;

    private String headBackend;

    public Template(RequestContext context) {
        super(context);
    }

    /**
     * Create a new template from the contents of this zip file
     *
     * @throws InstallerException
     */
    public Template(RequestContext context, File file, User user2) throws IOException, InstallerException {
        super(context);

        createdBy = user2;

        /*
           * Copy all contents to template data
           */
        SimpleZip.extractToDir(file, ResourcesUtils.getRealTemplateDataDir(), true, null, FileExistRule.Overwrite);

        /*
           * Load up data for before and after
           */
        before = SimpleFile.readString(ResourcesUtils.getRealTemplateData("top.txt"));
        after = SimpleFile.readString(ResourcesUtils.getRealTemplateData("bottom.txt"));
        try {
            head = SimpleFile.readString(ResourcesUtils.getRealTemplateData("head.txt"));
        } catch (IOException e) {
            e.printStackTrace();
            logger.warning(e.toString());
        }

        //			STRING BOXESTXT = SIMPLEFILE.READSTRING(CONTEXT.GETREALFILE(CONFIG.TEMPLATEDATAPATH + "/BOXES.TXT"));
        //
        //			THIS.PANELS = NEW TREESET();
        //			FOR (STRING PANEL : BOXESTXT.SPLIT("\N")) {
        //				PANELS.ADD(PANEL);
        //			}

        // delete init files
        ResourcesUtils.getRealTemplateData("top.txt").delete();
        ResourcesUtils.getRealTemplateData("bottom.txt").delete();
        ResourcesUtils.getRealTemplateData("head.txt").delete();
        ResourcesUtils.getRealTemplateData("boxes.txt").delete();

        name = file.getName().replace(".zip", "");

        /*
           * Check for the presence of an install.xml file which we will use to setup the site
           */
        File installFile = ResourcesUtils.getRealTemplateData("install.xml");
        logger.fine("[Template] looking for install file:" + installFile);

        if (installFile.exists()) {

            try {
                new Installer(context).install(installFile);
                installFile.delete();
            } catch (JDOMException e) {
                throw new InstallerException(e);
            }

        }

        //  chmod all files
//        try {
//            exec("chmod -R 777 " + context.getRealFile(Config.TemplateDataPath).getAbsolutePath());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        save();

        TemplateSettings.getInstance(context).setTemplatesCount();
    }

    /**
     * Creates a new blank template with this name
     */
    public Template(RequestContext context, String name, User user) {

        super(context);
        this.name = name;
        this.createdBy = user;

        this.before = "<table class='main'>\n<tr><td class='left' valign='top'>[boxes?name=left]</td><td class='center'  valign='top'>";
        this.after = "</td><td class='right' valign='top'>[boxes?name=right]</td></tr>\n</table>\n</body></html>";

        if (SimpleQuery.count(context, Template.class, "dfault", 1) == 0) {
            dfault = true;
        }

        save();

        TemplateSettings.getInstance(context).setTemplatesCount();
    }

    @Override
    public synchronized boolean delete() {

        new Query(context, "update # set template=0 where template=?").setTable(Category.class).setParameter(this).run();
        new Query(context, "update # set template=0 where template=?").setTable(ItemType.class).setParameter(this).run();

        boolean b = super.delete();
        TemplateSettings.getInstance(context).setTemplatesCount();
        return b;
    }

    public final String getAfter() {
        return after;
    }

    public final String getBefore() {
        return before;
    }

    public String getCss() {
        return css;
    }

    public String getHead() {
        return head;
    }

    public String getHeadBackend() {
        return headBackend;
    }

    public String getLabel() {
        return getName();
    }

    public final String getName() {
        return name;
    }

    public String getValue() {
        return getIdString();
    }

    public boolean hasCss() {
        return css != null;
    }

    public boolean hasHead() {
        return head != null;
    }

    public boolean hasHeadBackend() {
        return headBackend != null;
    }

    public final boolean isDefault() {
        return dfault;
    }

    public CharSequence renderHead(RequestContext context) {

        if (!hasHead()) {
            return "";
        }

        return MarkerRenderer.render(context, head);
    }

    public CharSequence renderHeadBackend(RequestContext context) {

        if (!hasHeadBackend()) {
            return "";
        }

        return MarkerRenderer.render(context, headBackend);
    }

    public CharSequence renderAfter(RequestContext context) {

        if (after == null) {
            return null;
        }

        return MarkerRenderer.render(context, after);
    }

    public CharSequence renderBefore(RequestContext context) {

        if (before == null) {
            return null;
        }

        return MarkerRenderer.render(context, before);
    }

    public void replace(String from, String to) {

        if (css != null) {
            css = css.replace(from, to);
        }

        if (before != null) {
            before = before.replace(from, to);
        }

        if (after != null) {
            after = after.replace(from, to);
        }

    }

    public final void setAfter(String after) {
        this.after = after;
    }

    public final void setBefore(String before) {
        this.before = before;
    }

    public void setCss(String css) throws IOException {

        if (ObjectUtil.equal(this.css, css)) {
            return;
        }

        this.css = css;
        TemplateUtil.saveCss(context);
    }

    public final void setDefault(boolean b) {

        if (b == dfault)
            return;

        // deselect everything else if setting this to default
        if (b) {
            for (Template template : Template.get(context)) {
                template.setDefault(false);
                template.save();
            }
        }

        this.dfault = b;
        save();

    }

    public void setHead(String head) {
        this.head = head;
    }

    public void setHeadBackend(String headBackend) {
        this.headBackend = headBackend;
    }

    public final void setName(String name) {
        this.name = name;
    }

    public static Template getFromSession(RequestContext context) {
        Query q = new Query(context, "select ts.* from # ts where ts.sessionId=?");
        q.setTable(TemplateSession.class);
        q.setParameter(context.getSessionId());
        TemplateSession session = q.get(TemplateSession.class);
        Template template = null;
        if (session != null) {
            template = session.getTemplate();
        }
        return template;
    }
}
