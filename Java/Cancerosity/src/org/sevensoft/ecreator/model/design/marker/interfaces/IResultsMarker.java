package org.sevensoft.ecreator.model.design.marker.interfaces;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.Marker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Results;

/**
 * @author sks 28 Jul 2006 07:20:20
 *
 */
public interface IResultsMarker extends Marker {

	public Object generate(RequestContext context, Map<String, String> params, Results results);
}
