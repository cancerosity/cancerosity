package org.sevensoft.ecreator.model.design.template.markers;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategorySettings;
import org.sevensoft.ecreator.model.design.generators.titles.TitlesGenerator;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * @author sks 7 Jun 2006 11:02:55
 */
public class PageTitleTag implements IGenericMarker {

    public Object generate(RequestContext context, Map<String, String> params) {

        String tag = params.get("tag");
        boolean exhome = params.containsKey("exhome");
        boolean excats = params.containsKey("excats");
        boolean exitems = params.containsKey("exitems");
        String exclude = params.get("exclude");
        String clazz = params.get("class");
        CategorySettings categorySettings = CategorySettings.getInstance(context);
        boolean images = categorySettings.hasTitlesGenerator();

        Category category = (Category) context.getAttribute("category");
        if (category != null) {

            if (excats) {
                return null;
            }

            // check to exclude root
            if (exhome) {
                if (category.isRoot()) {
                    return null;
                }
            }

            if (exclude != null) {
                String[] excludes = exclude.split("\\D");
                for (String id : excludes) {
                    if (category.getIdString().equals(id)) {
                        return null;
                    }
                }
            }
        }

        Item item = (Item) context.getAttribute("item");
        if (item != null) {

            if (exitems) {
                return null;
            }
        }

        StringBuilder sb = new StringBuilder();
        if (tag != null) {
            sb.append("<");
            sb.append(tag);
            sb.append(">");
        }

        String title = (String) context.getAttribute("title");

        // if images is enabled show image instead of text
        if (images) {

            // replace title spaces with underscores and append gif
            String filename = title.replaceAll("\\s", "_").replaceAll("[\\?|:/]", "");
            filename = filename.toLowerCase();
            filename = filename + ".gif";

            // check file exists
            File file = ResourcesUtils.getRealTemplateTitle(filename);
            if (!file.exists()) {

                /*
                     * write out new graphic
                     * 1. Get dir for the category graphics
                     */

                // get generator class
                TitlesGenerator gen;
                try {
                    gen = (TitlesGenerator) Class.forName(categorySettings.getTitlesGenerator()).newInstance();
                } catch (InstantiationException e1) {
                    throw new RuntimeException(e1);
                } catch (IllegalAccessException e1) {
                    throw new RuntimeException(e1);
                } catch (ClassNotFoundException e1) {
                    throw new RuntimeException(e1);
                }

                try {

                    gen.generate(context, file, title);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            ImageTag imageTag = new ImageTag(Config.TemplateTitlesPath + "/" + filename);
            imageTag.setAlt(title);
            imageTag.setTitle(title);
            imageTag.setClass(clazz);

            sb.append(imageTag);

        } else {
            sb.append(title);
        }

        if (tag != null) {
            sb.append("</");
            sb.append(tag);
            sb.append(">");
        }

        return sb.toString();
    }

    public Object getRegex() {
        return "title";
    }

}
