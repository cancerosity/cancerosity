package org.sevensoft.ecreator.model.design.marker.htmlforms;

import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.design.marker.interfaces.IFormMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.submit.ImageSubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;

import java.util.Map;

/**
 * @author sks 31-Mar-2006 15:23:32
 */
public final class SubmitMarker implements IGenericMarker, IFormMarker, IItemMarker {

    public Object generate(RequestContext context, Map<String, String> params) {

        String src = params.get("src");
        if (src != null) {

            String align = params.get("align");
            String srchover = params.get("srchover");

            if (!src.startsWith("template-data/")) {
                src = "template-data/" + src;
            }

            ImageSubmitTag tag = new ImageSubmitTag(src);

            if (srchover != null) {
                tag.setOnMouseOver("this.src='" + srchover + "'");
                tag.setOnMouseOut("this.src='" + src + "'");
            }

            if (align != null) {
                tag.setAlign(align);
            }

            return tag;
        }

        String label = params.get("label");
        if (label != null) {
            return new SubmitTag(label);
        }

        return null;
    }

    public Object generate(RequestContext context, Map<String, String> params, Form form) {

        String label = params.get("label");
        if (label == null) {
            label = form.getSubmitButtonText();
        }

        return new SubmitTag(label);
    }

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
        String formId = params.get("form");
        Form form = Form.getInstance(context, Form.class, formId);
        if (form == null)
            return null;

        return generate(context, params, form);
    }

    public Object getRegex() {
        return "submit";
    }
}