package org.sevensoft.ecreator.model.design.marker.interfaces;

import java.util.Map;

import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.design.marker.Marker;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 5 Jul 2006 13:00:05
 *
 */
public interface IFormMarker extends Marker {

	public Object generate(RequestContext context, Map<String, String> params, Form form);

}
