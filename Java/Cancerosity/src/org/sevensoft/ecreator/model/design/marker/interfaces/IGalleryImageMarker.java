package org.sevensoft.ecreator.model.design.marker.interfaces;

import org.sevensoft.ecreator.model.design.marker.Marker;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * User: Tanya
 * Date: 29.04.2011
 */
public interface IGalleryImageMarker extends Marker {

    public Object generate(RequestContext context, Map<String, String> params, Img image);
}
