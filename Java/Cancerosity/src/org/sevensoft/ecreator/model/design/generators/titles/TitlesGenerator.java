package org.sevensoft.ecreator.model.design.generators.titles;

import java.io.File;
import java.io.IOException;

import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 4 Apr 2007 11:40:53
 *
 */
public abstract class TitlesGenerator {

	public abstract void generate(RequestContext context, File file, String name) throws IOException;

}
