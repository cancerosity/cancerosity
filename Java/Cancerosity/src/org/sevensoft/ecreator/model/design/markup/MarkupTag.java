package org.sevensoft.ecreator.model.design.markup;

import org.sevensoft.ecreator.iface.admin.misc.html.EditMarkupButton;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 5 Sep 2006 18:11:46
 *
 */
public class MarkupTag {

	private final RequestContext	context;
	private final String		param;
	private final String		any;
	private final Markup		markup;
	private final String		id;

	public MarkupTag(RequestContext context, String string, Markup markup, String any, String id) {
		this.context = context;
		this.param = string;
		this.markup = markup;
		this.any = any;
		this.id = id;
	}

	public MarkupTag(RequestContext context, String name, Markup markup, String any) {
		this(context, name, markup, any, name);
	}

	@Override
	public String toString() {

		SelectTag tag = new SelectTag(context, param, markup);
		if (any != null)
			tag.setAny(any);
		tag.addOptions(Markup.get(context));

		if (id == null) {
			return tag.toString();
		}

		tag.setId(id);
		return tag + " " + new EditMarkupButton(id);
	}
}
