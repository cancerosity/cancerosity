package org.sevensoft.ecreator.model.design.marker.generic;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 23 Aug 2006 14:19:55
 *
 */
public class PrinterFriendlyMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		MiscSettings miscSettings = MiscSettings.getInstance(context);
		if (!miscSettings.isPrinterFriendly()) {
			return null;
		}

		Link link = null;

		Item item = (Item) context.getAttribute("item");
		if (item != null) {
			link = new Link(ItemHandler.class, "printer", "item", item);
		}

		if (link == null) {
			link = (Link) context.getAttribute("link");
		}

		if (link == null) {
			return null;
		}

		if (params.containsKey("exhome") && context.containsAttribute("category") && ((Category) context.getAttribute("category")).isRoot()) {
			return null;
		}

		link.addParameter("printer", 1);
		return super.link(context, params, link, "print_friendly");
	}

	public Object getRegex() {
		return "printer_friendly";
	}

}
