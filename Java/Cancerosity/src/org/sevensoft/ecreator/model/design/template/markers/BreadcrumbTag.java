package org.sevensoft.ecreator.model.design.template.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.Trail;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 7 Jun 2006 10:56:28
 *
 */
public class BreadcrumbTag implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		Trail trail = (Trail) context.getAttribute("_trail");
		if (trail == null)
			return null;

		if (params.containsKey("exhome")) {
			Category category = (Category) context.getAttribute("category");
			if (category != null && category.isRoot())
				return null;
		}

		boolean skipindex = params.containsKey("skipindex");

		boolean current = params.containsKey("current");
		String separator = params.get("sep");
		if (separator == null)
			separator = ">";

		return trail.render(separator, current, skipindex);
	}

	public Object getRegex() {
		return "breadcrumb";
	}
}
