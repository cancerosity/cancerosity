package org.sevensoft.ecreator.model.design.template;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 21 Nov 2006 15:43:35
 *
 */
@Singleton
@Table("templates_settings")
public class TemplateSettings extends EntityObject {

	public static TemplateSettings getInstance(RequestContext context) {
		return EntityObject.getSingleton(context, TemplateSettings.class);
	}

	private List<String>	panels;
	private int			templatesCount;

	private TemplateSettings(RequestContext context) {
		super(context);
	}

	public List<String> getPanels() {

		if (panels == null) {
			panels = new ArrayList();
		}

		if (panels.isEmpty()) {

			panels.add("left");
			panels.add("right");
		}

		return panels;
	}

	public boolean hasMultipleTemplates() {
		return templatesCount == 0 || templatesCount > 1;
	}

	public void setPanels(Collection<String> c) {

		this.panels = new ArrayList();

		if (c != null) {

			for (String panel : c) {
				panels.add(panel.toLowerCase());
			}

		}
	}

	public void setTemplatesCount() {
		templatesCount = SimpleQuery.count(context, Template.class);
		save();
	}
}
