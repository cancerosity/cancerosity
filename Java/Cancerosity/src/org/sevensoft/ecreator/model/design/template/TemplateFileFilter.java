package org.sevensoft.ecreator.model.design.template;

import java.io.File;
import java.io.FileFilter;

/**
 * @author sks 27-Jan-2006 12:10:38
 *
 */
public class TemplateFileFilter implements FileFilter {

	public boolean accept(File file) {

		String name = file.getName();

		if (name.equalsIgnoreCase("preview.gif"))
			return false;

		if (name.equalsIgnoreCase("thumbs.db"))
			return false;

		return true;
	}
}