package org.sevensoft.ecreator.model.design.marker.interfaces;

import org.sevensoft.ecreator.model.design.marker.Marker;
import org.sevensoft.ecreator.model.comments.Comment;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * User: Tanya
 * Date: 29.07.2010
 */
public interface ICommentMarker extends Marker {

    public Object generate(RequestContext context, Map<String, String> params, Comment comment);

}

