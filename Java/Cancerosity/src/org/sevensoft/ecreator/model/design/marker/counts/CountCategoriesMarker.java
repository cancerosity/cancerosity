package org.sevensoft.ecreator.model.design.marker.counts;

import java.util.Map;

import org.sevensoft.ecreator.model.categories.CategorySettings;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 17 May 2006 14:34:13
 *
 */
public class CountCategoriesMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {
		return "<span class='count_categories'>" + CategorySettings.getInstance(context).getCategoryCount() + "</span>";
	}

	public Object getRegex() {
		return "count_categories";
	}

	@Override
	public String toString() {
		return "Displays the number of categories created";
	}

}
