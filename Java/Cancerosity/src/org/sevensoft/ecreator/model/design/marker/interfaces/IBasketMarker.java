package org.sevensoft.ecreator.model.design.marker.interfaces;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.Marker;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 28 Jun 2006 23:45:20
 *
 */
public interface IBasketMarker extends Marker {

	public abstract Object generate(RequestContext context, Map<String, String> params, Basket basket);

}
