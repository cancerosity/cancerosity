package org.sevensoft.ecreator.model.design.marker.dep;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 8 Jan 2007 20:53:16
 * 
 * This just stops old markers from appearing as content
 *
 */
public class DeprecatedTagHolders implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {
		return null;
	}

	public Object getRegex() {
		return new String[] { "member", "comparison_check" };
	}

}
