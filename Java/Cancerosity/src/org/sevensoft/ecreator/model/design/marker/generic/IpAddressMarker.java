package org.sevensoft.ecreator.model.design.marker.generic;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 03-Apr-2006 11:14:29
 *
 */
public class IpAddressMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {
		return context.getRemoteIp();
	}

	public Object getRegex() {
		return "ipaddress";
	}
}