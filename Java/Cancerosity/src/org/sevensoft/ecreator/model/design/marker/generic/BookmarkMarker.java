package org.sevensoft.ecreator.model.design.marker.generic;

import java.util.Map;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 03-Apr-2006 14:27:16
 *
 */
public class BookmarkMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		boolean current = params.containsKey("current");

		String url = params.get("url");
		String title = params.get("title");

		if (title == null) {
			title = Company.getInstance(context).getName();
		}

		if (current) {

			Item item = (Item) context.getAttribute("item");
			Category category = (Category) context.getAttribute("category");
			if (category != null) {
				url = category.getUrl();
			}

			if (item != null) {
				url = item.getUrl();
			}
		}

		if (url == null) {
			url = Config.getInstance(context).getUrl();
		}

		String onclick = "if (window.external) window.external.AddFavorite('" + url + "', '" + title +
				"'); else window.alert('Your browser does not support this function'); return false; ";

		params.put("onclick", onclick);

		return super.link(context, params, "#", null);
	}

	public Object getRegex() {
		return "add_bookmark";
	}

}
