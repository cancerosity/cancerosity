package org.sevensoft.ecreator.model.design.styles.onecolumn;

import java.util.List;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.design.styles.StyleRenderer;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.http.html.ImageTag;

/**
 * @author sks 29 Apr 2007 15:19:03
 *
 */
public class ZigZagStyleRenderer implements StyleRenderer {

	public String render(String content, List<Img> images) {

		int wordsPerImage = 100;

		StringBuilder sb = new StringBuilder();
		sb.append("<div class='ecStylesZigZag'>");

		// every 50 words put in an image
		List<String> words = StringHelper.explodeStrings(content, "\\s");
		int w = 0;
		int n = 0;
		for (String word : words) {

			if (w % wordsPerImage == 0) {

				if (images.size() > 0) {

					ImageTag imageTag = images.remove(0).getFullsizeImageTag(200, 0);

					if (n % 2 == 0) {

						sb.append(imageTag.setAlign("left"));

					} else {

						sb.append(imageTag.setAlign("right"));
					}

					n++;
				}
			}

			sb.append(word);
			sb.append(" ");

			w++;

		}

		sb.append(content);

		sb.append("</div>");
		return sb.toString();
	}
}
