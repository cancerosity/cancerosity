package org.sevensoft.ecreator.model.design.markup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.ecreator.model.design.marker.MarkerRenderer;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.KeypadDep;

/**
 * @author sks 31-Jan-2006 20:32:22
 *
 */
public class MarkupRenderer {

	private static final Logger		logger	= Logger.getLogger("ecreator");

	private final RequestContext		context;
	private final int				tds;
	private final String			end;
	private final String			between;
	private final String			start;
	private final String			body;
	private List	objs;
	private String				containerClass;
	private String				containerId;
	private String				containerAlign;
	private Wrap				wrap;
	private int					cycleRows;
	private String				caption;
	private String				location;
	private int					x, y;

	public MarkupRenderer(RequestContext context, Markup markup) {

		this.context = context;

		this.start = markup.getStart();
		this.body = markup.getBody();
		this.end = markup.getEnd();
		this.between = markup.getBetween();
		this.tds = markup.getTds();
		this.containerId = markup.getContainerId();
		this.containerClass = markup.getContainerClass();
		this.containerAlign = markup.getContainerAlign();
		this.cycleRows = markup.getCycleRows();
		this.caption = markup.getCaption();
	}

	public MarkupRenderer(RequestContext context, String body) {

		this.context = context;
		this.body = body;

		this.end = null;
		this.start = null;
		this.between = null;
		this.tds = 0;
	}

	public MarkupRenderer(RequestContext context, String start, String body, String end, String between, int tds) {

		this.context = context;
		this.start = start;
		this.body = body;
		this.end = end;
		this.between = between;
		this.tds = tds;
	}

	public void addWrap(Wrap wrap) {
		this.wrap = wrap;
	}

	public String getContainerAlign() {
		return containerAlign;
	}

	private Collection<String> getRenders() {

		if (body == null) {
			return Collections.emptyList();
		}

		List<String> renders = new ArrayList();
		int n = 1;
		for (Object obj : objs) {

			StringBuilder sb = new StringBuilder();

			String string = body.replace("${bodycount}", String.valueOf(n));
			string = MarkerRenderer.render(context, string, obj, location, x, y);
			if (string == null) {
				continue;
			}

			if (wrap != null) {
				String pre = wrap.pre(obj);
				if (pre != null) {
					sb.append(pre);
				}
			}

			sb.append(string);

			if (wrap != null) {
				String post = wrap.post(obj);
				if (post != null) {
					sb.append(post);
				}
			}

			renders.add(sb.toString());
			n++;
		}
		return renders;
	}

	public void setBody(Object obj) {
		this.objs = Collections.singletonList(obj);
	}

	public void setBodyObjects(Collection objs) {
		this.objs = new ArrayList();
		this.objs.addAll(objs);
	}

	public void setContainerAlign(String containerAlign) {
		this.containerAlign = containerAlign;
	}

	public void setLocation(String location, int x, int y) {
		this.location = location;
		this.x = x;
		this.y = y;

		if (location != null) {
			logger.fine("[MarkupRenderer] setLocation=" + location + ", x=" + x + ",y=" + y);
		}
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder("\n");

		Object obj;
		if (objs != null && objs.size() == 1) {
			obj = objs.get(0);
		} else {
			obj = null;
		}

		// add in start markup if used
		if (start != null) {
			sb.append(MarkerRenderer.render(context, start, obj, location, x, y));
		}

		if (objs != null && objs.size() > 0) {

			// if tds is bigger than zero then generate keypad
			if (tds > 0) {

				KeypadDep keypad = new KeypadDep(getRenders(), tds);
				keypad.setTableClass(containerClass == null ? "container" : containerClass);
				keypad.setTableAlign(containerAlign);
				if (caption != null) {
					keypad.setCaption(caption);
				}
				if (containerId != null) {
					keypad.setTableId(containerId);
				}
				keypad.setCycleRows(cycleRows);

				sb.append(keypad);

				// ... otherwise do in line
			} else {

				Iterator<String> iter = getRenders().iterator();

				while (iter.hasNext()) {

					sb.append(iter.next());

					if (iter.hasNext()) {

						sb.append("\n");

						if (between != null) {
							sb.append(between);
							sb.append("\n");
						}
					}
				}
			}

		} else {

			sb.append(MarkerRenderer.render(context, body));
		}

		// include end markup if used
		if (end != null) {
			sb.append(MarkerRenderer.render(context, end, obj, location, x, y));
		}

		sb.append("\n");
		return sb.toString();
	}
}
