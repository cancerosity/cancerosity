package org.sevensoft.ecreator.model.design.template;

import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.commons.superstrings.comparators.NaturalFileComparator;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.media.images.ImageFiletypeFilter;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

import java.io.*;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author sam2 Nov 12, 2003 2:00:43 AM
 */
public class TemplateUtil {

    private static Logger logger = Logger.getLogger("ecreator");

    /**
     * Copy this file to the template-data directory.
     */
    @SuppressWarnings("null")
    public static void copyFile(File templateDataDir, File file) throws FileNotFoundException, IOException {

        if (file.isFile()) {

            FileChannel in = null, out = null;
            try {

                in = new FileInputStream(file).getChannel();
                out = new FileOutputStream(templateDataDir.getPath() + "/" + file.getName()).getChannel();

                long size = in.size();
                MappedByteBuffer buf = in.map(FileChannel.MapMode.READ_ONLY, 0, size);

                out.write(buf);

            } finally {

                if (in != null)
                    in.close();

                if (out != null)
                    out.close();
            }
        }
    }

    /**
     * Clear out all template-data data
     */
    private static void emptyDataDir(File dataDir) {

        if (dataDir.exists())
            for (File file : dataDir.listFiles())
                file.delete();
        else
            dataDir.mkdir();
    }

    /**
     * Returns a map of names mapped to colours
     */
    public static Map<String, String> getColors(RequestContext context) {

        File stylesFile = ResourcesUtils.getRealTemplateData("styles.css");
        if (stylesFile.exists()) {

            Map<String, String> map = new TreeMap<String, String>();

            String styles = null;
            try {
                styles = SimpleFile.readString(stylesFile);
            } catch (IOException e) {
                e.printStackTrace();
                logger.warning(e.toString());
            }

            Pattern pattern = Pattern.compile(":\\s*?([#a-zA-Z0-9]+);\\s*?/\\*\\s*(.*?)\\s*\\*/");
            Matcher matcher = pattern.matcher(styles);
            while (matcher.find()) {

                String code = matcher.group(1);
                String name = matcher.group(2);

                map.put(name, code);
            }

            return map;
        }

        return Collections.emptyMap();
    }

    /**
     * Return's a list of images inside the template-data dir, used for overwriting with users own images
     *
     * @param context RequestContext
     * @return List
     */
    public static List<String> getImages(RequestContext context) {
        return getImages(context, ResourcesUtils.getRealTemplateDataDir());
    }

    /**
     * Return's a list of images inside the template-data dir, used for overwriting with users own images
     *
     * @param context RequestContext
     * @return List
     */
    public static List<String> getLogo(RequestContext context) {
        return getImages(context, ResourcesUtils.getRealEmailLogoDir());
    }

    /**
     * Return's a list of images inside the template-data dir, used for overwriting with users own images
     *
     * @param context RequestContext
     * @return List
     */
    public static List<String> getImages(RequestContext context, File dir) {

        if (dir.exists()) {
            String[] files = dir.list(new ImageFiletypeFilter());
            return Arrays.asList(files);
        }

        return Collections.emptyList();
    }

    /**
     * Return's a list of all ather files except images inside the template-data dir
     *
     * @param context
     * @return List<String> filenames
     */
    public static List<String> getNotImages(RequestContext context) {

        File file = ResourcesUtils.getRealTemplateDataDir();
        if (file.exists()) {
            String[] files = file.list(new ImageFiletypeFilter(false));
            return Arrays.asList(files);
        }

        return Collections.emptyList();
    }

    public static Map<String, String> getPresetOptions(RequestContext context) {

        File[] files = getPresets(context);
        if (files == null)
            return Collections.emptyMap();

        Map<String, String> options = new LinkedHashMap();
        for (File file : files) {

            if (file.getName().endsWith(".zip")) {
                options.put(file.getName(), file.getName().replace(".zip", ""));
            }

            // only show zips and remove zip ending
        }

        return options;
    }

    /**
     * Returns a list of files, which are all the preset template zips
     */
    public static File[] getPresets(RequestContext context) {

        File templatesDir = ResourcesUtils.getRealTemplateStoreDir();
        if (templatesDir.exists()) {
            File[] files = templatesDir.listFiles();
            Arrays.sort(files, NaturalFileComparator.instance);
            return files;
        }
        return null;
    }

    /**
     * Returns a map of all CSS values that are marked with a name
     */
    public static Map<String, String> getStyleSheet(RequestContext context) {

        File templateCssFile = ResourcesUtils.getRealTemplateData("template.css");
        if (templateCssFile.exists()) {

            logger.fine("[template helper] parsing stylesheet: " + templateCssFile);

            try {

                String stylesheet = SimpleFile.readString(templateCssFile);
                logger.fine("[template helper] file size " + stylesheet.length());

                Map<String, String> map = new TreeMap();

                Pattern pattern = Pattern.compile("([^\\s]*)/\\*\\s*(.*?)\\s*\\*/");
                Matcher matcher = pattern.matcher(stylesheet);
                while (matcher.find()) {

                    logger.fine(matcher.group());
                    String value = matcher.group(1);
                    String name = matcher.group(2);

                    logger.fine("[template helper] stylesheet pair name=" + name + " value=" + value);

                    map.put(name, value);
                }

                logger.fine("[template helper] returning stylesheet map: " + map);
                return map;

            } catch (IOException e) {
                e.printStackTrace();
                logger.warning(e.toString());
            }

        }

        return Collections.emptyMap();
    }

    /**
     * Create the css files for all markup and templates
     */
    public static void saveCss(RequestContext context) throws IOException {

        // get all markups, each markup will be included in every css file
        List<Markup> markups = Markup.get(context);

        for (Template template : Template.get(context)) {

            //create the auto css file
            File file = ResourcesUtils.getRealTemplateData("auto-" + template.getId() + ".css");

            StringBuilder sb = new StringBuilder();

            // write out css from template
            if (template.hasCss()) {

                sb.append("/* template: " + template.getName() + " #" + template.getId() + " */\n\n");
                sb.append(template.getCss());
                sb.append("\n\n\n");

            }

            // write out markup css

            for (Markup markup : markups) {

                if (markup.hasCss()) {

                    sb.append("/* markup: " + markup.getName() + " #" + markup.getId() + " */\n\n");
                    sb.append(markup.getCss());
                    sb.append("\n\n\n");

                }
            }

            //			String css = ForumSettings.getInstance(context).getCss();
            //			if (css != null) {
            //
            //				sb.append("/* forum css */\n\n");
            //				sb.append(css);
            //				sb.append("\n\n\n");
            //			}

            // write out CSS file
            try {
                SimpleFile.writeString(sb, file);
            } catch (IOException e) {
                e.printStackTrace();
                logger.warning(e.toString());
            }

        }

        // delete the old auto.css file if it exists
        File autoCss = ResourcesUtils.getRealTemplateData("auto.css");
        if (autoCss.exists()) {
            autoCss.delete();
        }

    }

    public static void saveStyleSheet(RequestContext context, Map<String, String> stylesMap) throws IOException {

        File templateCssFile = ResourcesUtils.getRealTemplateData("template.css");
        if (templateCssFile.exists()) {

            logger.fine("[template helper] saving to stylesheet: " + templateCssFile);

            String stylesheet = SimpleFile.readString(templateCssFile);
            for (Map.Entry<String, String> entry : stylesMap.entrySet()) {

                String value = entry.getValue();
                value = value.replaceAll("[^0-9A-Za-z#]", "");

                stylesheet = stylesheet.replaceAll("[^\\s]*/\\*\\s*" + entry.getKey() + "\\s*\\*/", value + "/*" + entry.getKey() + "*/");
            }

            SimpleFile.writeString(stylesheet, templateCssFile);
        }
    }

    public static void setColors(RequestContext context, Map<String, String> colors) throws FileNotFoundException, IOException {

        File stylesFile = ResourcesUtils.getRealTemplateData("styles.css");
        String styles = null;
        try {
            styles = SimpleFile.readString(stylesFile);
        } catch (IOException e) {
            e.printStackTrace();
            logger.warning(e.toString());
        }

        for (Map.Entry<String, String> entry : colors.entrySet()) {

            // Pattern pattern = Pattern.compile("(:|\\s)([#a-zA-Z0-9]+)\\s*?;\\s*?/\\*" + entry.getKey() + "\\*/");
            // Pattern pattern = Pattern.compile(":\\s*?(" + entry.getValue() + ");\\s*?/\\*\\s*(" + entry.getKey() + ")s*\\*/");
            Pattern pattern = Pattern.compile(":\\s*?([#a-zA-Z0-9]+);\\s*?/\\*\\s*(" + entry.getKey() + ")\\s*\\*/");

            Matcher matcher = pattern.matcher(styles);
            // System.out.println("before: " + styles);
            styles = matcher.replaceAll(": " + entry.getValue() + "; /*" + entry.getKey() + "*/");
            // System.out.println("after: " + styles);
        }

        BufferedWriter writer = new BufferedWriter(new FileWriter(stylesFile));
        writer.write(styles);
        writer.close();
    }

    public static void setTemplate(String path, RequestContext context) throws FileNotFoundException, IOException {

        // get real path to template data
        File templateDataDir = ResourcesUtils.getRealTemplateDataDir();

        // empty Data Dir
        emptyDataDir(templateDataDir);

        // copy across template files except for preview gif and thumbs file on XP.
        File templateSourceDir = context.getRealFile(path);
        if (templateSourceDir.exists()) {

            FileFilter fileFilter = new TemplateFileFilter();

            for (File file : templateSourceDir.listFiles(fileFilter))
                copyFile(templateDataDir, file);

            // chown directory, will throw exception on windows so catch
            try {
                EntityObject.exec("chmod -R 777 " + ResourcesUtils.getRealTemplateDataDir());
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

}