package org.sevensoft.ecreator.model.design.marker.generic;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 30 Jan 2007 15:32:32
 *
 */
public class EmailMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		String subject = params.get("subject");
		if (subject == null) {
			return null;
		}

		String email = params.get("email");
		if (email == null) {
			return null;
		}

		if ("item".equals(subject)) {
			subject = item.getName();
		} else if ("attribute".equals(subject)) {
			subject = item.getAttributeValue(Integer.parseInt(params.get("id")));
		}

		String body = params.get("body");

		StringBuilder sb = new StringBuilder();
		sb.append("mailto:" + email + "?subject=" + subject);

		if (body != null) {
			sb.append("&body=" + body);
		}

		return super.link(context, params, sb.toString(), null);

	}

	@Override
	public String getDescription() {
		return "Creates a link to an email. Use subject=item or subject=attribute&id=xx to change subject";
	}

	public Object getRegex() {
		return new String[] { "email" };
	}

}
