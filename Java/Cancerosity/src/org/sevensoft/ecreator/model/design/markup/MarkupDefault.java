package org.sevensoft.ecreator.model.design.markup;

/**
 * @author sks 15 Aug 2006 09:29:02
 *
 */
public interface MarkupDefault {

	public String getBetween();

	public String getBody();

	public String getEnd();

	public String getStart();

	public int getTds();
	
	public String getCss();

	public String getName();
}
