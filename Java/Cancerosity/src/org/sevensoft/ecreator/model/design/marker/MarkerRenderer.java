package org.sevensoft.ecreator.model.design.marker;

import org.sevensoft.ecreator.model.accounts.login.markers.*;
import org.sevensoft.ecreator.model.accounts.markers.*;
import org.sevensoft.ecreator.model.accounts.profile.markers.ProfileEditMarker;
import org.sevensoft.ecreator.model.accounts.profile.markers.ProfileImagesMarker;
import org.sevensoft.ecreator.model.accounts.profile.markers.ProfileViewMarker;
import org.sevensoft.ecreator.model.accounts.registration.markers.RegisterMarker;
import org.sevensoft.ecreator.model.accounts.registration.markers.RegistrationItemTypeMarker;
import org.sevensoft.ecreator.model.accounts.subscriptions.markers.*;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.downloads.markers.DownloadsMarker;
import org.sevensoft.ecreator.model.attachments.markers.*;
import org.sevensoft.ecreator.model.attributes.markers.*;
import org.sevensoft.ecreator.model.bookings.markers.AvgCostMarker;
import org.sevensoft.ecreator.model.bookings.markers.BookMarker;
import org.sevensoft.ecreator.model.bookings.markers.ChargeMarker;
import org.sevensoft.ecreator.model.bookings.markers.NightsMarker;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.markers.*;
import org.sevensoft.ecreator.model.containers.boxes.markers.BoxesLeftMarker;
import org.sevensoft.ecreator.model.containers.boxes.markers.BoxesMarker;
import org.sevensoft.ecreator.model.containers.boxes.markers.BoxesRightMarker;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.markers.*;
import org.sevensoft.ecreator.model.crm.forms.markers.submissions.FormSubmissionMarker;
import org.sevensoft.ecreator.model.crm.forms.markers.submissions.SubmissionsMarker;
import org.sevensoft.ecreator.model.crm.forum.markers.AccountPostsMarker;
import org.sevensoft.ecreator.model.design.marker.counts.CountCategoriesMarker;
import org.sevensoft.ecreator.model.design.marker.counts.CountItemsMarker;
import org.sevensoft.ecreator.model.design.marker.dep.DeprecatedTagHolders;
import org.sevensoft.ecreator.model.design.marker.generic.*;
import org.sevensoft.ecreator.model.design.marker.htmlforms.InputErrorMarker;
import org.sevensoft.ecreator.model.design.marker.htmlforms.SubmitMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.*;
import org.sevensoft.ecreator.model.design.template.markers.*;
import org.sevensoft.ecreator.model.design.template.markers.dates.YearMarker;
import org.sevensoft.ecreator.model.ecom.credits.markers.BonusCreditsMarker;
import org.sevensoft.ecreator.model.ecom.credits.markers.CreditPageMarker;
import org.sevensoft.ecreator.model.ecom.credits.markers.CreditsLeftMarker;
import org.sevensoft.ecreator.model.ecom.delivery.markers.DeliveryChargeMarker;
import org.sevensoft.ecreator.model.ecom.delivery.markers.DeliveryDetailsMarker;
import org.sevensoft.ecreator.model.ecom.delivery.markers.DeliveryRateSelectorMarker;
import org.sevensoft.ecreator.model.ecom.delivery.markers.DeliveryRatesMarker;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.ecom.orders.markers.*;
import org.sevensoft.ecreator.model.ecom.payments.forms.able2buy.markers.Able2BuyCreditTermsMarker;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.ecom.shopping.markers.CheckoutMarker;
import org.sevensoft.ecreator.model.ecom.shopping.markers.OrderHistoryMarker;
import org.sevensoft.ecreator.model.ecom.shopping.markers.OrderStatusMarker;
import org.sevensoft.ecreator.model.ecom.shopping.markers.QuickOrderMarker;
import org.sevensoft.ecreator.model.ecom.shopping.markers.scripts.CheckoutScriptMarker;
import org.sevensoft.ecreator.model.ecom.shopping.markers.basket.*;
import org.sevensoft.ecreator.model.ecom.shopping.markers.ordering.BuyMarker;
import org.sevensoft.ecreator.model.ecom.shopping.markers.ordering.MultiBuyMarker;
import org.sevensoft.ecreator.model.ecom.shopping.markers.ordering.QtyMarker;
import org.sevensoft.ecreator.model.ecom.shopping.markers.totals.*;
import org.sevensoft.ecreator.model.extras.languages.markers.LanguagesBarMarker;
import org.sevensoft.ecreator.model.extras.links.ExternalLinksMarker;
import org.sevensoft.ecreator.model.extras.mapping.google.markers.GoogleMapMarker;
import org.sevensoft.ecreator.model.extras.meetups.markers.RsvpMarker;
import org.sevensoft.ecreator.model.extras.meetups.markers.RsvpRemainingMarker;
import org.sevensoft.ecreator.model.extras.ratings.markers.AvgRatingMarker;
import org.sevensoft.ecreator.model.extras.ratings.markers.RatingAddMarker;
import org.sevensoft.ecreator.model.extras.ratings.markers.RatingsTableMarker;
import org.sevensoft.ecreator.model.extras.tellfriend.TellFriendMarker;
import org.sevensoft.ecreator.model.extras.rss.markers.RssImageMarker;
import org.sevensoft.ecreator.model.interaction.LastActiveMarker;
import org.sevensoft.ecreator.model.interaction.blocks.markers.BlockAddMarker;
import org.sevensoft.ecreator.model.interaction.blocks.markers.BlockIfMarker;
import org.sevensoft.ecreator.model.interaction.blocks.markers.BlocksMarker;
import org.sevensoft.ecreator.model.interaction.buddies.markers.BuddiesInclusionMarker;
import org.sevensoft.ecreator.model.interaction.buddies.markers.BuddiesMarker;
import org.sevensoft.ecreator.model.interaction.buddies.markers.BuddyMarker;
import org.sevensoft.ecreator.model.interaction.pm.markers.ComposePmMarker;
import org.sevensoft.ecreator.model.interaction.pm.markers.MessagesCountMarker;
import org.sevensoft.ecreator.model.interaction.pm.markers.MessagesMarker;
import org.sevensoft.ecreator.model.interaction.sms.ComposeSmsMarker;
import org.sevensoft.ecreator.model.interaction.userplane.chat.markers.UserplaneChatMarker;
import org.sevensoft.ecreator.model.interaction.userplane.messenger.markers.UserplaneMessengerMarker;
import org.sevensoft.ecreator.model.interaction.userplane.recorder.markers.UserplaneRecorderMarker;
import org.sevensoft.ecreator.model.interaction.userplane.recorder.markers.UserplaneViewerMarker;
import org.sevensoft.ecreator.model.interaction.views.markers.ViewsCountMarker;
import org.sevensoft.ecreator.model.interaction.views.markers.ViewsMarker;
import org.sevensoft.ecreator.model.interaction.views.markers.ViewsSinceMarker;
import org.sevensoft.ecreator.model.interaction.winks.markers.WinkMarker;
import org.sevensoft.ecreator.model.interaction.winks.markers.WinksMarker;
import org.sevensoft.ecreator.model.interaction.winks.markers.WinksSinceMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ibex.markers.IbexMarker;
import org.sevensoft.ecreator.model.items.alternatives.marker.AlternativesMarker;
import org.sevensoft.ecreator.model.items.favourites.markers.AddFavouriteMarker;
import org.sevensoft.ecreator.model.items.favourites.markers.FavouritesMarker;
import org.sevensoft.ecreator.model.items.favourites.markers.RemoveFavouriteMarker;
import org.sevensoft.ecreator.model.items.favourites.markers.FavouriteAddMarker;
import org.sevensoft.ecreator.model.items.favourites.markers.FavouriteRemoveMarker;
import org.sevensoft.ecreator.model.items.listings.markers.ListingsAddMarker;
import org.sevensoft.ecreator.model.items.listings.markers.ListingsMarker;
import org.sevensoft.ecreator.model.items.listings.markers.ListingsExpiryDateMarker;
import org.sevensoft.ecreator.model.items.markers.*;
import org.sevensoft.ecreator.model.items.options.markers.OptionInputMarker;
import org.sevensoft.ecreator.model.items.options.markers.OptionNameMarker;
import org.sevensoft.ecreator.model.items.options.markers.OptionsMarker;
import org.sevensoft.ecreator.model.items.options.markers.OptionsListMarker;
import org.sevensoft.ecreator.model.items.pricing.markers.*;
import org.sevensoft.ecreator.model.items.reviews.markers.ReviewsCountMarker;
import org.sevensoft.ecreator.model.items.reviews.markers.ReviewsMarker;
import org.sevensoft.ecreator.model.items.stock.markers.StockMarker;
import org.sevensoft.ecreator.model.marketing.newsletter.markers.NewsletterMarker;
import org.sevensoft.ecreator.model.marketing.newsletter.markers.NewsletterItemsMarker;
import org.sevensoft.ecreator.model.marketing.sms.markers.SmsMarker;
import org.sevensoft.ecreator.model.media.images.markers.FullsizeImageMarker;
import org.sevensoft.ecreator.model.media.images.markers.ImageBrowserMarker;
import org.sevensoft.ecreator.model.media.images.markers.ImagesMarker;
import org.sevensoft.ecreator.model.media.images.markers.RandomImageMarker;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.media.images.galleries.markers.*;
import org.sevensoft.ecreator.model.misc.content.markers.ContentMarker;
import org.sevensoft.ecreator.model.misc.content.markers.SummaryMarker;
import org.sevensoft.ecreator.model.misc.currencies.CurrenciesMarker;
import org.sevensoft.ecreator.model.misc.currencies.CurrentPageMarker;
import org.sevensoft.ecreator.model.misc.location.markers.DistanceMarker;
import org.sevensoft.ecreator.model.misc.location.markers.LocationMarker;
import org.sevensoft.ecreator.model.misc.location.markers.MultimapMarker;
import org.sevensoft.ecreator.model.misc.sitemap.SitemapMarker;
import org.sevensoft.ecreator.model.products.accessories.marker.AccessoriesBlurbMarker;
import org.sevensoft.ecreator.model.products.accessories.marker.AccessoriesCheckMarker;
import org.sevensoft.ecreator.model.products.accessories.marker.AccessoriesMarker;
import org.sevensoft.ecreator.model.search.markers.*;
import org.sevensoft.ecreator.model.search.markers.attributes.SearchAttributeMarker;
import org.sevensoft.ecreator.model.search.markers.control.SearchExactMarker;
import org.sevensoft.ecreator.model.search.markers.control.SearchExcludeAccountMarker;
import org.sevensoft.ecreator.model.search.markers.control.SearchExcludeAccountTypeMarker;
import org.sevensoft.ecreator.model.search.markers.control.SearchResultsPerPageMarker;
import org.sevensoft.ecreator.model.search.markers.location.SearchDistanceMarker;
import org.sevensoft.ecreator.model.search.markers.location.SearchLocationMarker;
import org.sevensoft.ecreator.model.search.markers.results.EndResultMarker;
import org.sevensoft.ecreator.model.search.markers.results.StartResultMarker;
import org.sevensoft.ecreator.model.search.markers.results.TotalResultsMarker;
import org.sevensoft.ecreator.model.search.markers.text.SearchDetailsMarker;
import org.sevensoft.ecreator.model.search.markers.text.SearchKeywordsMarker;
import org.sevensoft.ecreator.model.search.markers.text.SearchNameMarker;
import org.sevensoft.ecreator.model.system.company.markers.*;
import org.sevensoft.ecreator.model.system.config.markers.SiteNameMarker;
import org.sevensoft.ecreator.model.bespoke.ipoints.IPointsMarker;
import org.sevensoft.ecreator.model.comments.Comment;
import org.sevensoft.ecreator.model.comments.markers.*;
import org.sevensoft.ecreator.model.sitemap.marker.SitemapHtmlMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Urls;
import org.sevensoft.jeezy.http.util.Results;

import java.util.*;
import java.util.logging.Logger;

/**
 * @author sks 30-Mar-2006 13:21:32
 */
@SuppressWarnings("null")
public abstract class MarkerRenderer {

    private static SortedSet<String> TagStrings;

    /**
     * Map of reg exp's to markers
     */
    private final static Map<String, Marker> MarkerRegExMap;

    private static final Logger logger = Logger.getLogger("markers");

    static {

        TagStrings = new TreeSet<String>();
        MarkerRegExMap = new HashMap<String, Marker>();

        Set<Marker> set = new HashSet<Marker>() {

            @Override
            public boolean add(Marker e) {

                if (contains(e)) {
                    throw new RuntimeException("Markers set contains duplicate: " + e);
                }

                return super.add(e);
            }

        };

        accessories(set);
        account(set);
        accountLinks(set);
        attachments(set);
        attributes(set);

        boxes(set);
        bookings(set);

        company(set);
        counts(set);
        categories(set);

        credits(set);

        dep(set);
        date(set);
        delivery(set);

        favourites(set);
        forms(set);
        forum(set);

        generic(set);

        htmlforms(set);

        images(set);
        interaction(set);
        ipoints(set);
        item(set);

        location(set);
        listings(set);
        login(set);

        mailing(set);
        map(set);
        members(set);
        meetups(set);

        options(set);
        orders(set);

        pricing(set);

        ratings(set);
        results(set);

        search(set);
        stock(set);
        shopping(set);
        submissions(set);
        subscription(set);

        template(set);
        totals(set);

        userplane(set);

        comments(set);

        for (Marker marker : set) {

            String[] regs = null;
            Object regex = marker.getRegex();
            if (regex instanceof String) {

                regs = new String[]{(String) regex};

            } else if (regex instanceof String[]) {

                regs = (String[]) regex;
            }

            for (String regexp : regs) {

                regexp = regexp.replaceAll("\\(.*$", "");

                TagStrings.add(regexp);

                if (MarkerRegExMap.containsKey(regexp)) {
                    throw new RuntimeException("Tag already present: " + marker + " and " + MarkerRegExMap.get(regexp));
                }
                MarkerRegExMap.put(regexp, marker);
            }

        }
    }

    private static void accessories(Set<Marker> set) {

        set.add(new AccessoriesMarker());
        set.add(new AccessoriesBlurbMarker());
        set.add(new AccessoriesCheckMarker());

    }

    private static void account(Set<Marker> set) {
        set.add(new LoginMarker());
//        set.add(new RegisterMarker());
        set.add(new RegistrationItemTypeMarker());
        set.add(new AccountMarker());
        set.add(new AccountStatusMarker());
        set.add(new AccountNameMarker());
        set.add(new AccountEmailMarker());

        set.add(new AccountItemsMarker());
    }

    private static void accountLinks(Set<Marker> set) {

//        set.add(new AccountNameMarker());
        set.add(new DownloadsMarker());
        set.add(new OrderHistoryMarker());

//        set.add(new SubscriptionRenewalMarker());

        set.add(new ProfileViewMarker());
        set.add(new ProfileEditMarker());
        set.add(new ProfileImagesMarker());
    }

    private static void attachments(Set<Marker> set) {

        set.add(new AttachmentFileTypeMarker());
        set.add(new AttachmentLinkMarker());
        set.add(new AttachmentSizeMarker());
        set.add(new AttachmentNameMarker());
        set.add(new AttachmentsMarker());

    }

    private static void attributes(Set<Marker> set) {

        set.add(new AttributeValueMarker());
        set.add(new AttributesSpanMarker());
        set.add(new AttributesTableMarker());
        set.add(new AttributesSummarySpanMarker());
        set.add(new AttributesSummaryTableMarker());
        set.add(new AttributesMarker());
    }

    private static void bookings(Set<Marker> set) {
        set.add(new ChargeMarker());
        set.add(new AvgCostMarker());
        set.add(new NightsMarker());
        set.add(new BookMarker());
    }

    private static void boxes(Set<Marker> set) {
        set.add(new BoxesLeftMarker());
        set.add(new BoxesRightMarker());
        set.add(new BoxesMarker());
    }

    private static void categories(Set<Marker> set) {
        set.add(new CategoriesMarker());
        set.add(new CategoriesSubMarker());
        set.add(new CategoryMarker());
        set.add(new CategoriesDropDownMarker());
    }

    private static void company(Set<Marker> set) {
        set.add(new CompNameMarker());
        set.add(new CompTelephoneMarker());
        set.add(new CompEmailMarker());
        set.add(new CountryMarker());
        set.add(new CompAddressMarker());
        set.add(new CompAddressLabelMarker());
        set.add(new CompVatRegMarker());
        set.add(new CompNoMarker());
        set.add(new CompFaxMarker());
        set.add(new SiteNameMarker());
    }

    private static void counts(Set<Marker> set) {

        set.add(new CountItemsMarker());
        set.add(new CountCategoriesMarker());

    }

    private static void credits(Set<Marker> set) {
        set.add(new CreditsLeftMarker());
        set.add(new BonusCreditsMarker());
        set.add(new CreditPageMarker());

    }

    private static void date(Set<Marker> set) {
        set.add(new DateMarker());
    }

    private static void delivery(Set<Marker> set) {
        set.add(new DeliveryRatesMarker());
        set.add(new DeliveryChargeMarker());
        set.add(new DeliveryDetailsMarker());
        set.add(new DeliveryRateSelectorMarker());
    }

    private static void dep(Set<Marker> set) {
        set.add(new DeprecatedTagHolders());
    }

    private static void favourites(Set<Marker> set) {

        set.add(new FavouritesMarker());
//        set.add(new AddFavouriteMarker());   //not actual
//        set.add(new RemoveFavouriteMarker());//not actual
        set.add(new FavouriteAddMarker());
        set.add(new FavouriteRemoveMarker());
    }

    private static void forms(Set<Marker> set) {
        set.add(new FormFieldMarker());
        set.add(new FormMarker());
        set.add(new FormLineAddMarker());
        set.add(new FormSubmissionMarker());
        set.add(new FormPanelMarker());
        set.add(new CaptchaPanelMarker());
    }

    private static void generic(Set<Marker> set) {

        set.add(new HomeMarker());
        set.add(new SitemapMarker());
        set.add(new SitemapHtmlMarker());

        set.add(new IdMarker());
        set.add(new IpAddressMarker());
        set.add(new CurrenciesMarker());
        set.add(new CurrentPageMarker());
        set.add(new LanguagesBarMarker());

        set.add(new BookmarkMarker());
        set.add(new TellFriendMarker());
        set.add(new PrinterFriendlyMarker());
        set.add(new YearMarker());

        set.add(new EmailMarker());

        set.add(new ChildMarker());

        set.add(new IbexMarker());

        set.add(new ErrorMessageMarker());

    }

    public static Collection<Marker> getMarkers() {
        Collection values = MarkerRegExMap.values();
        return values;
    }

    public static Map<String, String> getParams(String string) {
        return Urls.getParams(string);
    }

    public static SortedSet<String> getTagStrings() {
        return TagStrings;
    }

    private static void htmlforms(Set<Marker> set) {
        set.add(new SubmitMarker());
        set.add(new InputErrorMarker());
    }

    private static void images(Set<Marker> set) {

        set.add(new ImagesMarker());
        set.add(new FullsizeImageMarker());
        set.add(new RandomImageMarker());
        set.add(new ImageBrowserMarker());
        set.add(new RssImageMarker());

        set.add(new GalleryImageMarker());
        set.add(new ImageCaptionMarker());
        set.add(new ImageDateMarker());
        set.add(new ImageDescriptionMarker());
        set.add(new PrintImageMarker());
    }

    private static void interaction(Set<Marker> set) {

        set.add(new BuddyMarker());
        set.add(new BuddiesInclusionMarker());
        set.add(new BuddiesMarker());

        set.add(new ComposeSmsMarker());

        set.add(new ComposePmMarker());
        set.add(new MessagesMarker());
        set.add(new MessagesCountMarker());

        set.add(new WinkMarker());
        set.add(new WinksSinceMarker());
        set.add(new WinksMarker());

        set.add(new ViewsMarker());
        set.add(new ViewsSinceMarker());
        set.add(new ViewsCountMarker());

        set.add(new BlockAddMarker());
        set.add(new BlocksMarker());
        set.add(new BlockIfMarker());
    }

    private static void ipoints(Set<Marker> set) {
        set.add(new IPointsMarker());
    }

    private static void item(Set<Marker> set) {

        set.add(new DateCreatedMarker());

        set.add(new SummaryMarker());
        set.add(new ContentMarker());

        set.add(new AlternativesMarker());

        set.add(new MultimapMarker());
        set.add(new ItemMarker());
        set.add(new ItemTypeMarker());

        set.add(new ReferenceMarker());

        set.add(new LastUpdatedMarker());
        set.add(new AgeMarker());

        set.add(new ItemUrlMarker());

        set.add(new ItemFamilyTableHeaderMarker());

        set.add(new PriorityMarker());

        set.add(new ImagePathMarker());
        set.add(new ThumbnailPathMarker());

    }

    private static void listings(Set<Marker> set) {
        set.add(new ListingsAddMarker());
        set.add(new ListingsMarker());
        set.add(new ListingsExpiryDateMarker());
    }

    private static void location(Set<Marker> set) {
//        set.add(new MultimapMarker());
        set.add(new DistanceMarker());
        set.add(new LocationMarker());
        set.add(new ExternalLinksMarker());
    }

    private static void login(Set<Marker> set) {

        set.add(new ForgottenPasswordMarker());
        set.add(new LoginEmailMarker());
        set.add(new LogoutMarker());
        set.add(new LoginStartMarker());
        set.add(new LoginEndMarker());
        set.add(new LoginPasswordMarker());
        set.add(new RegisterMarker());
    }

    private static void mailing(Set<Marker> set) {
        set.add(new SmsMarker());
        set.add(new NewsletterMarker());
        set.add(new NewsletterItemsMarker());
    }

    private static void map(Set<Marker> set) {
        set.add(new GoogleMapMarker());
    }

    private static void meetups(Set<Marker> set) {

        set.add(new RsvpMarker());
        set.add(new RsvpRemainingMarker());

    }

    private static void members(Set<Marker> set) {

        set.add(new LastActiveMarker());
    }

    private static void forum(Set<Marker> set) {
        set.add(new AccountPostsMarker());
    }

    private static void options(Set<Marker> set) {
        set.add(new OptionsMarker());
        set.add(new OptionNameMarker());
        set.add(new OptionInputMarker());
        set.add(new OptionsListMarker());
    }

    private static void orders(Set<Marker> set) {
        set.add(new DeliveryAddressMarker());
        set.add(new BillingAddressMarker());
        set.add(new OrderTotalMarker());

        set.add(new LineSellLineMarker());
        set.add(new LineSellUnitMarker());
        set.add(new OrderIdMarker());
        set.add(new OrderAccountNameMarker());
        set.add(new OrderAccountIdMarker());
        set.add(new CustomerReferenceMarker());
        set.add(new LineQtyMarker());
        set.add(new LineDescriptionMarker());
        set.add(new LineOptionsDescriptionMarker());
        set.add(new OrderDateMarker());
        set.add(new LinesMarker());
        set.add(new OrderDiscountMarker());
        set.add(new OrderLineIdMarker());
        set.add(new OrderLineAttributeMarker());
        set.add(new OrderVoucherMarker());

        set.add(new InvoiceHeaderMarker());
        set.add(new InvoiceFooterMarker());
        set.add(new InvoiceBillingMarker());
        set.add(new InvoiceDeliveryMarker());
        set.add(new IdOrderMarker());

        set.add(new InvoiceLinesAttributesHeaderMarker());
        set.add(new InvoiceLinesAttributesMarker());
        set.add(new InvoiceDeliveryLinesMarker());

    }

    private static void pricing(Set<Marker> set) {

        set.add(new RrpMarker());
        set.add(new RrpDiscountMarker());
        set.add(new SellPriceMarker());
        set.add(new OriginalPriceMarker());
        set.add(new OrigPriceForCategoryDiscountModuleMarker());
        set.add(new DiscountMarker());
        set.add(new PriceBreaksMarker());
        set.add(new DiscountPercentageMarker());

        set.add(new Able2BuyCreditTermsMarker());
    }

    private static void ratings(Set<Marker> set) {

        set.add(new RatingsTableMarker());
        set.add(new RatingAddMarker());
        set.add(new ReviewsMarker());
        set.add(new ReviewsCountMarker());
        set.add(new AvgRatingMarker());

    }

    /**
     * if no object is supplied we are rendering generic only
     */
    public static String render(RequestContext context, String markup) {
        return render(context, markup, null);
    }

    public static String render(RequestContext context, String markup, Object obj) {
        return render(context, markup, obj, null, 0, 0);
    }

    private static String getRegExEntry(String markup, String regexp) {
        int startIndex = markup.indexOf("[" + regexp + "]");
        if (startIndex == -1) {
            startIndex = markup.indexOf("[" + regexp + "?");
        }
        if (startIndex == -1) {
            return null;
        }
        int endIndex = startIndex;
        int bracketCount = 1;
        while (bracketCount != 0) {
            endIndex++;
            if (markup.charAt(endIndex) == ']') {
                bracketCount--;
            } else if (markup.charAt(endIndex) == '[') {
                bracketCount++;
            }
        }
        return markup.substring(startIndex, endIndex + 1);
    }

    private static String getParamString(String template) {
        int start = template.indexOf("?");
        if (start == -1) {
            return null;
        }
        return template.substring(start + 1, template.length() - 1);
    }

    public static String render(RequestContext context, String markup, Object obj, String location, int x, int y) {

        if (markup == null) {
            return markup;
        }

        for (Map.Entry<String, Marker> entry : MarkerRegExMap.entrySet()) {

            String regexp = entry.getKey();
            Marker marker = entry.getValue();

            String template;
            while ((template = getRegExEntry(markup, regexp)) != null) {

                Map<String, String> params = new HashMap<String, String>();
                String paramsString = getParamString(template);
                if (paramsString != null) {
                    if (paramsString.contains("[") && paramsString.contains("]")) {
                        paramsString = render(context, paramsString, obj, location, x, y);
                    }
                    params = MarkerRenderer.getParams(paramsString);
                }

                Object replacement = null;

                logger.fine("[MarkerRenderer] marker=" + marker.getClass().getSimpleName() + ", obj=" + obj);

                if (marker instanceof ICategoryMarker && obj instanceof Category) {
                    replacement = ((ICategoryMarker) marker).generate(context, params, (Category) obj);
                } else if (marker instanceof IItemMarker && obj instanceof Item) {
                    replacement = ((IItemMarker) marker).generate(context, params, (Item) obj, location, x, y);
                } else if (marker instanceof IOrderMarker && obj instanceof Order) {
                    replacement = ((IOrderMarker) marker).generate(context, params, (Order) obj);
                } else if (marker instanceof IBasketMarker && obj instanceof Basket) {
                    replacement = ((IBasketMarker) marker).generate(context, params, (Basket) obj);
                } else if (marker instanceof IBasketLineMarker && obj instanceof BasketLine) {
                    replacement = ((IBasketLineMarker) marker).generate(context, params, (BasketLine) obj);
                } else if (marker instanceof IAttachmentMarker && obj instanceof Attachment) {
                    replacement = ((IAttachmentMarker) marker).generate(context, params, (Attachment) obj);
                } else if (marker instanceof IFormMarker && obj instanceof Form) {
                    replacement = ((IFormMarker) marker).generate(context, params, (Form) obj);
                } else if (marker instanceof IOrderLineMarker && obj instanceof OrderLine) {
                    replacement = ((IOrderLineMarker) marker).generate(context, params, (OrderLine) obj);
                } else if (marker instanceof IResultsMarker && obj instanceof Results) {
                    replacement = ((IResultsMarker) marker).generate(context, params, (Results) obj);
                } else if (marker instanceof IItemMarker && marker instanceof AttributeValueMarker && obj instanceof BasketLine) {
                    replacement = ((IItemMarker) marker).generate(context, params, ((BasketLine) obj).getItem(), location, x, y);
                } else if (marker instanceof IGenericMarker) {
                    replacement = ((IGenericMarker) marker).generate(context, params);
                } else if (marker instanceof ICommentMarker && obj instanceof Comment) {
                    replacement = ((ICommentMarker) marker).generate(context, params, (Comment) obj);
                }else if (marker instanceof IGalleryImageMarker && obj instanceof Img) {
                    replacement = ((IGalleryImageMarker) marker).generate(context, params, (Img) obj);
                }

                if (replacement != null) {
                    replacement = replacement.toString();
                }

                if (replacement == null) {
                    replacement = "";

                }
                /*else {
                    replacement = Matcher.quoteReplacement(replacement.toString());
                }*/

                markup = markup.replace(template, replacement.toString());
            }
        }

        return markup;
    }

    private static void results(Set<Marker> set) {

        set.add(new StartResultMarker());
        set.add(new EndResultMarker());
        set.add(new TotalResultsMarker());

    }

    private static void search(Set<Marker> set) {

        set.add(new SearchKeywordsMarker());
        set.add(new SearchNameMarker());
        set.add(new SearchLocationMarker());
        set.add(new SearchCategoriesMarker());
        set.add(new SearchSortMarker());
        set.add(new SearchEndMarker());
        set.add(new SearchAttributeMarker());
        set.add(new SearchDistanceMarker());
        set.add(new SearchAgeMarker());
        set.add(new SearchItemTypeMarker());
        set.add(new SearchStartMarker());
        set.add(new SearchIdMarker());
        set.add(new SearchDetailsMarker());
        set.add(new SavedSearchesMarker());
        set.add(new SearchExactMarker());

        set.add(new SearchFieldMarker());

        // controls
        set.add(new SearchExcludeAccountMarker());
        set.add(new SearchExcludeAccountTypeMarker());
        set.add(new SearchResultsPerPageMarker());
    }

    private static void shopping(Set<Marker> set) {

        set.add(new BasketItemsMarker());
        set.add(new BasketTotalMarker());

        set.add(new VoucherTag());
        set.add(new BasketLinesMarker());

        set.add(new CheckoutMarker());
        set.add(new BasketMarker());
        set.add(new OrderStatusMarker());
        set.add(new QuickOrderMarker());

        set.add(new BuyMarker());
        set.add(new MultiBuyMarker());
        set.add(new QtyMarker());

        set.add(new CheckoutScriptMarker());

        set.add(new BasketItemRemove());

    }

    private static void stock(Set<Marker> set) {
        set.add(new StockMarker());
    }

    private static void submissions(Set<Marker> set) {
        set.add(new SubmissionsMarker());
    }

    private static void subscription(Set<Marker> set) {
        set.add(new SubscriptionLevelMarker());
        set.add(new SubscriptionCurrentMarker());
        set.add(new SubscriptionRenewalMarker());
        set.add(new SubscriptionExpiryMarker());
        set.add(new SubscriptionCancelMarker());
        set.add(new SubscriptionDaysMarker());
    }

    private static void template(Set<Marker> set) {

        set.add(new BreadcrumbTag());
        set.add(new MetaTitleTag());
        set.add(new CopyrightTag());
        set.add(new CreditsTag());
        set.add(new MetaDescriptionTag());
        set.add(new MetaKeywordsTag());
        set.add(new PageTitleTag());

    }

    private static void totals(Set<Marker> set) {
        set.add(new SubtotalMarker());
        set.add(new TotalMarker());
        set.add(new VatMarker());
        set.add(new ItemsSubtotalMarker());
        set.add(new ItemsTotalMarker());

    }

    private static void userplane(Set<Marker> set) {
        set.add(new UserplaneMessengerMarker());
        set.add(new UserplaneChatMarker());
        set.add(new UserplaneViewerMarker());
        set.add(new UserplaneRecorderMarker());
    }

    private static void comments(Set<Marker> set) {
        set.add(new CommentMarker());
        set.add(new CommentAuthorMarker());
        set.add(new CommentContentMarker());
        set.add(new CommentTitleMarker());
        set.add(new CommentDateMarker());

        set.add(new CommentAddMarker());
    }

}
