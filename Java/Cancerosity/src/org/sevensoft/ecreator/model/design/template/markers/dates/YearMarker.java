package org.sevensoft.ecreator.model.design.template.markers.dates;

import java.util.Map;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 17 Jan 2007 16:47:59
 *
 */
public class YearMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {
		return new Date().toString("yyyy");
	}

	public Object getRegex() {
		return "year";
	}

}
