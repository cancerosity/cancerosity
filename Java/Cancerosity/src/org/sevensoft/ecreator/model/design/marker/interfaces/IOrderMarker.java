package org.sevensoft.ecreator.model.design.marker.interfaces;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.Marker;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 28 Jun 2006 11:46:11
 *
 */
public interface IOrderMarker extends Marker {

	public abstract Object generate(RequestContext context, Map<String, String> params, Order order);
}
