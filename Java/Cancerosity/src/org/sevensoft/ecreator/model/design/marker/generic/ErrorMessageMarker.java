package org.sevensoft.ecreator.model.design.marker.generic;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * User: Tanya
 * Date: 12.07.2012
 */
public class ErrorMessageMarker implements IGenericMarker{

    public Object generate(RequestContext context, Map<String, String> params) {
        String id = params.get("id");

        if (context.hasErrors()){
            return context.getError(id);
        }


        return null;
    }

    public Object getRegex() {
        return "error_message";
    }
}
