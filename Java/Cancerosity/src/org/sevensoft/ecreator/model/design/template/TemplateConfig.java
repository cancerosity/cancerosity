package org.sevensoft.ecreator.model.design.template;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 25 Jan 2007 12:23:13
 *
 */
@Table("categories_templates")
public class TemplateConfig extends EntityObject {

	public static TemplateConfig get(RequestContext context, Category category) {
		TemplateConfig config = SimpleQuery.get(context, TemplateConfig.class, "category", category);
        return config;
//		return config == null ? new TemplateConfig(context, category) : config;  //no need to create empty template
	}

	public static TemplateConfig get(RequestContext context, ItemType itemType) {
		TemplateConfig config = SimpleQuery.get(context, TemplateConfig.class, "itemType", itemType);
        return config;
//		return config == null ? new TemplateConfig(context, itemType) : config; //no need to create empty template
	}

    public static TemplateConfig create(RequestContext context, Category category) {
        return new TemplateConfig(context, category);
    }

    public static TemplateConfig create(RequestContext context, ItemType itemType) {
        return new TemplateConfig(context, itemType);
    }

	public static Template getApplicableTemplate(Category category) {

		TemplateConfig config = category.getTemplateConfig();

        if (config != null && config.hasTemplate()) {
            return config.getTemplate();
        }

		for (Category parent : category.getParents(true)) {

			config = parent.getTemplateConfig();
            if (config != null && config.isCategoryInheritance() && config.hasTemplate()) {
				return config.getTemplate();
			}
		}

		return null;
	}

	public static Template getApplicableTemplate(Item item) {

		TemplateConfig config = item.getItemType().getTemplateConfig();

		// firstly check for a template set directly on this item type
		if (config != null && config.hasTemplate()) {
			return config.getTemplate();
		}

		// then check for a template for each of the parent categories
		for (Category category : item.getCategories()) {

			config = category.getTemplateConfig();
			if (config != null && config.isItemInheritance() && config.hasTemplate()) {
				return config.getTemplate();
			}
		}

		// then check each of the parents of the primary cats
		for (Category category : item.getCategories()) {

			for (Category parent : category.getParents(true)) {

				config = parent.getTemplateConfig();
				if (config != null && config.isCategoryInheritance() && config.isItemInheritance() && config.hasTemplate()) {
					return config.getTemplate();
				}
			}
		}

		return null;
	}

	/**
	 * The template to use
	 */
	private Template	template;

	/**
	 * The owning category
	 */
	private Category	category;

	/**
	 * Apply this template to subcategories
	 */
	private boolean	categoryInheritance;

	/**
	 * Apply this template to subcategories
	 */
	private boolean	itemInheritance;

	private ItemType	itemType;

	protected TemplateConfig(RequestContext context) {
		super(context);
	}

	public TemplateConfig(RequestContext context, Category category) {
		super(context);

		this.category = category;
		save();
	}

	public TemplateConfig(RequestContext context, ItemType itemType) {
		super(context);

		this.itemType = itemType;
		save();
	}

	public final Category getCategory() {
		return category.pop();
	}

	public final Template getTemplate() {
		return (Template) (template == null ? null : template.pop());
	}

	public boolean hasTemplate() {
		return template != null;
	}

	public final boolean isCategoryInheritance() {
		return categoryInheritance;
	}

	public final boolean isItemInheritance() {
		return itemInheritance;
	}

	public final void setCategoryInheritance(boolean inheritance) {
		this.categoryInheritance = inheritance;
	}

	public final void setItemInheritance(boolean itemInheritance) {
		this.itemInheritance = itemInheritance;
	}

	public final void setTemplate(Template template) {
		this.template = template;
	}
}
