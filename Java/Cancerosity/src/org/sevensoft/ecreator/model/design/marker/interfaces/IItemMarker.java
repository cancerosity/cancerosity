package org.sevensoft.ecreator.model.design.marker.interfaces;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.Marker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Apr 2006 17:33:24
 *
 */
public interface IItemMarker extends Marker {

	public abstract Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y);

}
