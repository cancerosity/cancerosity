package org.sevensoft.ecreator.model.design.marker.generic;

import java.util.Map;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.ICategoryMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderMarker;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 28 Jun 2006 12:42:57
 *
 */
public class IdMarker extends MarkerHelper implements IItemMarker, ICategoryMarker, IOrderMarker {

	public Object generate(RequestContext context, Map<String, String> params, Category category) {
		return super.string(context, params, category.getIdString());
	}

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
		return super.string(context, params, item.getIdString());
	}

	public Object generate(RequestContext context, Map<String, String> params, Order order) {
		return super.string(context, params, order.getIdString());
	}

	public Object getRegex() {
		return "id";
	}

}
