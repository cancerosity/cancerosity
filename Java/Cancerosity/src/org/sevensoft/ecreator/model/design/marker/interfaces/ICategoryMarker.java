package org.sevensoft.ecreator.model.design.marker.interfaces;

import java.util.Map;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.marker.Marker;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Apr 2006 17:33:24
 *
 */
public interface ICategoryMarker extends Marker {

	public abstract Object generate(RequestContext context, Map<String, String> params, Category category);

}
