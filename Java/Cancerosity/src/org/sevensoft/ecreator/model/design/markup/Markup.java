package org.sevensoft.ecreator.model.design.markup;

import java.io.IOException;
import java.util.List;

import org.sevensoft.ecreator.model.accounts.AccountModule;
import org.sevensoft.ecreator.model.accounts.AccountSettings;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategorySettings;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.design.template.TemplateUtil;
import org.sevensoft.ecreator.model.ecom.shopping.ShoppingSettings;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.highlighted.blocks.HighlightedItemsBlock;
import org.sevensoft.ecreator.model.items.highlighted.boxes.HighlightedItemsBox;
import org.sevensoft.ecreator.model.items.options.OptionSet;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 31 Jul 2006 11:01:10
 *
 */
@Table("markup")
public class Markup extends EntityObject implements Selectable, Logging {

	public static List<Markup> get(RequestContext context) {
		return SimpleQuery.execute(context, Markup.class, "name");
	}

	public static Markup getByName(RequestContext context, String name) {

		if (name == null) {
			return null;
		}

		return new Query(context, "select * from # where name like ?").setParameter(name).setTable(Markup.class).get(Markup.class);
	}

	private String	body, start, end, between;
	private int		tds;
	private String	name;
	private String	containerId, containerClass;
	private String	css;
	private String	containerAlign;
	private int		perPage;
	private String	caption;
    private int vmarquee;

	/**
	 * Number of rows to alternative. Will alternative row id from 1 to n
	 */
	private int		cycleRows;

	private Markup(RequestContext context) {
		super(context);
	}

	public Markup(RequestContext context, MarkupDefault d) {
		this(context, d.getName(), d.getStart(), d.getBody(), d.getEnd(), d.getBetween(), d.getTds());

		try {
			setCss(d.getCss());
			save();
		} catch (IOException e) {
			e.printStackTrace();
            logger.warning(e.toString());
		}
	}

	public Markup(RequestContext context, String newMarkupName) {
		super(context);
		name = newMarkupName;
		save();
	}

	public Markup(RequestContext context, String name, MarkupDefault d) {

		this(context, null, d.getStart(), d.getBody(), d.getEnd(), d.getBetween(), d.getTds(), d.getCss());
		this.name = name;

		save();
	}

	public Markup(RequestContext context, String name, String start, String body, String end, String between, int tds) {
		super(context);

		this.start = start;
		this.body = body;
		this.end = end;
		this.between = between;
		this.tds = tds;
		this.name = name;

		save();
	}

    public Markup(RequestContext context, String name, String start, String body, String end, String between, int tds, String css) {
		super(context);

		this.start = start;
		this.body = body;
		this.end = end;
		this.between = between;
		this.tds = tds;
		this.name = name;
        this.css = css;

        save();
	}

    @Override
	public synchronized boolean delete() {

		// delete from category settings
		new Query(context, "update # set subcategoriesMarkup=0 where subcategoriesMarkup=?").setTable(CategorySettings.class).setParameter(this).run();
		new Query(context, "update # set siblingsMarkup=0 where siblingsMarkup=?").setTable(CategorySettings.class).setParameter(this).run();

		// delete from blocks
		for (Class clazz : Block.getClasses()) {
			try {
				new Query(context, "update # set markup=0 where markup=?").setTable(clazz).setParameter(this).run();
			} catch (RuntimeException e) {
				e.printStackTrace();
                logger.warning(e.toString());
			}
		}

		new Query(context, "update # set accountMarkup=0 where accountMarkup=?").setTable(AccountModule.class).setParameter(this).run();
		new Query(context, "update # set loginMarkup=0 where loginMarkup=?").setTable(AccountSettings.class).setParameter(this).run();

		// delete from item types
		new Query(context, "update # set listMarkup=0 where listMarkup=?").setTable(ItemType.class).setParameter(this).run();
		new Query(context, "update # set viewMarkup=0 where viewMarkup=?").setTable(ItemType.class).setParameter(this).run();
		new Query(context, "update # set printerMarkup=0 where printerMarkup=?").setTable(ItemType.class).setParameter(this).run();

		new Query(context, "update # set listItemMarkup=0 where listItemMarkup=?").setTable(Category.class).setParameter(this).run();

		new Query(context, "update # set optionsMarkup=0 where optionsMarkup=?").setTable(OptionSet.class).setParameter(this).run();
		new Query(context, "update # set basketMarkup=0 where basketMarkup=?").setTable(ShoppingSettings.class).setParameter(this).run();
		new Query(context, "update # set basketLineMarkup=0 where basketLineMarkup=?").setTable(ShoppingSettings.class).setParameter(this).run();

		new Query(context, "update # set markup=0 where markup=?").setTable(Form.class).setParameter(this).run();

		HighlightedItemsBlock.clearHtmlCache();
		HighlightedItemsBox.clearHtmlCache();

		return super.delete();
	}

	public String getBetween() {
		return between;
	}

	public String getBody() {
		return body;
	}

	public String getCaption() {
		return caption;
	}

	public String getContainerAlign() {
		return containerAlign;
	}

	public String getContainerClass() {
		return containerClass;
	}

	public String getContainerId() {
		return containerId;
	}

	public String getCss() {
		return css;
	}

	public int getCycleRows() {
		return cycleRows < 1 ? 1 : cycleRows;
	}

	public String getEnd() {
		return end;
	}

	public String getLabel() {
		return getName() + " #" + getIdString();
	}

	public List<LogEntry> getLogEntries() {
		return LogEntry.get(context, this);
	}

	public String getLogId() {
		return getFullId();
	}

	public String getLogName() {
		return "Markup #" + getId();
	}

	public String getName() {
		return name;
	}

	public final int getPerPage() {
		return perPage;
	}

	public String getStart() {
		return start;
	}

	public int getTds() {
		return tds;
	}

	public String getValue() {
		return getIdString();
	}

	public boolean hasCss() {
		return css != null;
	}

	public boolean hasPerPage() {
		return perPage > 0;
	}

	public void log(String message) {
		new LogEntry(context, this, message);
	}

	public void log(User user, String message) {
		new LogEntry(context, this, user, message);
	}

	public void replace(String from, String to) {

		if (css != null) {
			css = css.replace(from, to);
		}

		if (start != null) {
			start = start.replace(from, to);
		}

		if (end != null) {
			end = end.replace(from, to);
		}

		if (body != null) {
			body = body.replace(from, to);
		}

		if (between != null) {
			between = between.replace(from, to);
		}
	}

	@Override
	public synchronized void save() {
		HighlightedItemsBlock.clearHtmlCache();
		HighlightedItemsBox.clearHtmlCache();

		super.save();
	}

	public void setBetween(String between) {
		this.between = between;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public void setContainerAlign(String containerAlign) {
		this.containerAlign = containerAlign;
	}

	public void setCss(String css) throws IOException {

		if (ObjectUtil.equal(this.css, css)) {
			return;
		}

		this.css = css;
		TemplateUtil.saveCss(context);
	}

	public void setCycleRows(int alternateRows) {
		this.cycleRows = alternateRows;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public void setName(String name) {
		this.name = name;
	}

	public final void setPerPage(int perPage) {
		this.perPage = perPage;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public void setTableClass(String containerClass) {
		this.containerClass = containerClass;
	}

	public void setTableId(String containerId) {
		this.containerId = containerId;
	}

	public void setTds(int tds) {
		this.tds = tds;
	}

    public int getVmarquee() {
        return vmarquee;
    }

    public void setVmarquee(int vmarquee) {
        this.vmarquee = vmarquee;
    }

    public static int getMaxVmarquee(RequestContext context) {
        Query q = new Query(context, "select max(vmarquee) from # ");
        q.setTable(Markup.class);
        return q.getInt();
    }

    public Markup updateFrorScrollerBox(HighlightedItemsBox box) {
        if (getVmarquee() == 0) {

            int max = Markup.getMaxVmarquee(context);
            setVmarquee(max + 1);
        }

        String start = getStart();
        if (start.indexOf("vmarquee") == -1) {

            StringBuilder sb = new StringBuilder(start);
            sb.append("<div class='autoScroller-container' style='height:");
            sb.append(box.getScrollerHeight());
            sb.append("px;overflow:auto'><div id='vmarquee");
            sb.append(getVmarquee());
            sb.append("'>");

            setStart(sb.toString());

            setEnd(getEnd() + "</div></div>");
        } else {
            String newstart = start.replaceAll("vmarquee[\\d]+", "vmarquee" + getVmarquee())
                    .replaceAll("'autoScroller-container' style='height:[\\d]+px", "'autoScroller-container' style='height:" + box.getScrollerHeight()+"px");
            setStart(newstart);
        }
        save();
        return this;
    }
}
