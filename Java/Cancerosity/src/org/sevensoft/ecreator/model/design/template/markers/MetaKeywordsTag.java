package org.sevensoft.ecreator.model.design.template.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 7 Jun 2006 11:00:20
 *
 */
public class MetaKeywordsTag implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		String keywords = (String) context.getAttribute("keywords");
		return keywords;
	}

	public Object getRegex() {
		return "meta_keywords";
	}

}
