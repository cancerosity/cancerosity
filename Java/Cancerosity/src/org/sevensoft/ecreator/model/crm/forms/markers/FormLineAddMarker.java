package org.sevensoft.ecreator.model.crm.forms.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;

/**
 * @author sks 27 Nov 2006 14:50:04
 *
 */
public class FormLineAddMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		Form form = EntityObject.getInstance(context, Form.class, params.get("form"));
		if (form == null)
			return null;

		StringBuilder sb = new StringBuilder();
		sb.append(new HiddenTag("form", form));

		return sb;
	}

	public Object getRegex() {
		return "form_add";
	}

}
