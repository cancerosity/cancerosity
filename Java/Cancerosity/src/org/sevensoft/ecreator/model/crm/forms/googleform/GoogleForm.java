package org.sevensoft.ecreator.model.crm.forms.googleform;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.crm.forms.FormField;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.misc.Page;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.httpc.HttpMethod;

import java.util.List;
import java.util.Map;
import java.text.MessageFormat;
import java.io.IOException;

/**
 * User: Tanya
 * Date: 14.11.2011
 */
@Table("forms_google")
public class GoogleForm extends EntityObject {

    private static final String GOOGLE_FORM = "https://spreadsheets.google.com/spreadsheet/formResponse";
    private String formkey;
    private Form form;
    private int pageColumn;
    private int siteColumn;

    public GoogleForm(RequestContext context) {
        super(context);
    }

    public GoogleForm(RequestContext context, Form form) {
        super(context);
        this.form = form;
        save();
    }

    public static GoogleForm get(RequestContext context, Form form) {
        GoogleForm googleForm = SimpleQuery.get(context, GoogleForm.class, "form", form);
        if (googleForm == null) {
            googleForm = new GoogleForm(context, form);
        }
        return googleForm;
    }

    public List<GoogleFormField> getFields() {
        return SimpleQuery.execute(context, GoogleFormField.class, "googleForm", this);
    }

    public GoogleFormField addField(FormField formField) {
        GoogleFormField field = new GoogleFormField(context, this, formField);
        return field;
    }

    public String getFormkey() {
        return formkey;
    }

    public void setFormkey(String formkey) {
        this.formkey = formkey;
    }

    public Form getForm() {
        return form == null ? null : (Form) form.pop();
    }

    public void setForm(Form form) {
        this.form = form;
    }

    public int getPageColumn() {
        return pageColumn;
    }

    public void setPageColumn(int pageColumn) {
        this.pageColumn = pageColumn;
    }

    public boolean hasPageColumn() {
        return pageColumn > 0;
    }

    public int getSiteColumn() {
        return siteColumn;
    }

    public void setSiteColumn(int siteColumn) {
        this.siteColumn = siteColumn;
    }

    public boolean hasSiteColumn() {
        return siteColumn > 0;
    }

    public static void post(RequestContext context, Form form, Map<FormField, String> data, Page page) throws IOException {
        GoogleForm googleForm = GoogleForm.get(context, form);
        if (googleForm.getFormkey() == null)
            return;
        HttpClient hc = new HttpClient(GOOGLE_FORM, HttpMethod.Post);
        hc.setParameter("formkey", googleForm.getFormkey());
        for (GoogleFormField formField : googleForm.getFields()) {
            if (formField.hasGoogleFormColumn() && data.containsKey(formField.getFormField())) {
                hc.setParameter(MessageFormat.format("entry.{0}.single", formField.getGoogleFormColumn() - 1), data.get(formField.getFormField()));
            }

        }
        if (googleForm.hasPageColumn()) {
            hc.setParameter(MessageFormat.format("entry.{0}.single", googleForm.getPageColumn() - 1), page.getName());
        }
        if (googleForm.hasSiteColumn()) {
            hc.setParameter(MessageFormat.format("entry.{0}.single", googleForm.getSiteColumn() - 1), Config.getInstance(context).getUrl());
        }

        hc.connect();

        System.out.println(hc.getResponseString());
    }
}
