package org.sevensoft.ecreator.model.crm.forms.blocks;

import org.sevensoft.ecreator.iface.frontend.crm.form.FormHandler;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.design.marker.MarkerRenderer;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 21 Jun 2006 11:09:56
 *
 */
@Table("blocks_forms")
@Label("Form")
public class FormBlock extends Block {

	private Form	form;

	protected FormBlock(RequestContext context) {
		super(context);
	}

	public FormBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, 0);

		this.form = EntityObject.getInstance(context, Form.class, objId);
		if (form != null) {
			setName("Form: " + form.getName());
			save();
		}
	}

	public FormBlock(RequestContext context, Category category, Form quoteForm) {
		this(context, category, quoteForm.getId());
	}

	public Form getForm() {
		return form.pop();
	}

	@Override
	public Object render(RequestContext context) {

		logger.fine("[FormBlock] rendering form=" + form);

		Item account = (Item) context.getAttribute("account");
		Category category = (Category) context.getAttribute("category");
		Item item = (Item) context.getAttribute("item");

		/*
		 * Check for permission on form
		 */
		if (!PermissionType.ViewForm.check(context, account, getForm())) {

			String restrictionForwardUrl = form.getRestrictionForwardUrl();
			if (restrictionForwardUrl == null) {
				return null;
			} else {
				return new ExternalRedirect(restrictionForwardUrl);
			}
		}

		StringBuilder sb = new StringBuilder();

		if (form.hasHeader()) {
			sb.append(MarkerRenderer.render(context, getForm().getHeader()));
		}

        FormTag formTag = new FormTag(FormHandler.class, "submit", "multi");
        formTag.setId(form.getCssId());
        formTag.setClass(form.getCssClass());
        sb.append(formTag);
		sb.append(new HiddenTag("form", getForm()));
		sb.append(new HiddenTag("category", category));
		sb.append(new HiddenTag("item", item));

		sb.append(getForm().render());

		sb.append("</form>");

		if (form.hasFooter()) {
			sb.append(MarkerRenderer.render(context, getForm().getFooter()));
		}

		return sb.toString();

	}

}
