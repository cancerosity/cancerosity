package org.sevensoft.ecreator.model.crm.forum;

import java.io.IOException;

import org.sevensoft.ecreator.model.design.template.TemplateUtil;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 06-Jul-2005 14:24:47
 * 
 */
@Table("settings_forums")
@Singleton
public class ForumSettings extends EntityObject {

	public static ForumSettings getInstance(RequestContext context) {
		return EntityObject.getSingleton(context, ForumSettings.class);
	}

	/**
	 * Block emails in all post in all forums
	 */
	private boolean	blockEmails;

	private String	css;

	/**
	 * Info that appears at the top of the board page.
	 */
	private String	boardHeader;

	public ForumSettings(RequestContext context) {
		super(context);
	}

	public String getBoardHeader() {
		return boardHeader;
	}

	public String getCss() {
		return css;
	}

	public boolean hasBoardHeader() {
		return boardHeader != null;
	}

	public final boolean isBlockEmails() {
		return blockEmails;
	}

	public final void setBlockEmails(boolean blockEmails) {
		this.blockEmails = blockEmails;
	}

	public void setBoardHeader(String boardHeader) {
		this.boardHeader = boardHeader;
	}

	public void setCss(String css) throws IOException {
		this.css = css;
		TemplateUtil.saveCss(context);
	}
}
