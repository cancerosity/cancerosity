package org.sevensoft.ecreator.model.crm.forum.box;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 24 Jul 2006 23:56:27
 *
 */
@Path("admin-posts-box.do")
public class LatestPostsBoxHandler extends BoxHandler {

	private LatestPostsBox	box;

	public LatestPostsBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {
	}

}
