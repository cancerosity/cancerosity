package org.sevensoft.ecreator.model.crm.forum;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.List;

/**
 * @author sks 22-Feb-2006 17:55:48
 */
@Table("forums_topics")
public class Topic extends EntityObject {

    /**
     * Member who created this topic
     */
    private Item creator;

    /**
     *
     */
    private DateTime dateCreated, dateUpdated;

    /**
     * closed if the topic no longer accepts messages.
     */
    private boolean closed;

    /**
     * Title of topic, or set to item name if not a stand alone topic
     */
    private String title;

    /**
     * The item this message is tied to if any.
     */
    private Item item;

    private int postCount;

    private Forum forum;

    private int views;

    private String author;

    private String description;

    private DateTime lastPostDate;

    private String lastPostAuthor;

    public Topic(RequestContext context) {
        super(context);
    }

    public Topic(RequestContext context, Forum forum, String title, Item member) {
        super(context);

        this.forum = forum;
        this.title = title;
        this.dateCreated = new DateTime();

        this.creator = member;
        this.author = member.getDisplayName();

        save();
    }

    Topic(RequestContext context, Item item) {

        super(context);

        /*
           * Get the forum for the item type of this item
           */
        this.forum = item.getItemType().getForum();

        this.item = item;
        this.dateCreated = new DateTime();
        this.title = item.getName();

        // only set author if this item has an account assigned
        if (item.hasAccount()) {
            this.creator = item.getAccount();
            this.author = creator.getDisplayName();
        } else {
            this.author = Company.getInstance(context).getName();
        }

        save();
    }

    public Post addPost(Item poster, String content) {

        if (isClosed())
            return null;

        /*
           * Check that the preceeding post isn't the same as this - if it is do not post
           */
        Post post = getLastPost();
        if (post != null && post.getContent().equals(content)) {
            return null;
        }

        post = new Post(context, this, poster, content);
        setPostCount();
        setLastPost(post);
        save();

        getForum().update();

        return post;
    }

    public void close() {
        this.closed = true;
        save();
    }

    @Override
    public synchronized boolean delete() {

        removePosts();
        return super.delete();
    }

    public String getAuthor() {
        return author;
    }

    public final Item getCreator() {
        return (Item) (creator == null ? null : creator.pop());
    }

    public DateTime getDateCreated() {
        return dateCreated;
    }

    public DateTime getDateUpdated() {
        return dateUpdated;
    }

    public String getDescription() {
        return description;
    }

    public Forum getForum() {
        return forum.pop();
    }

    public Item getItem() {
        return (Item) (item == null ? null : item.pop());
    }

    private Post getLastPost() {

        Query q = new Query(context, "select * from # where topic=? order by id desc");
        q.setTable(Post.class);
        q.setParameter(this);
        return q.get(Post.class);
    }

    public String getLastPostAuthor() {
        Post lastPost = getLastPost();
        return lastPost != null ? lastPost.getAuthor() : "";
    }

    public DateTime getLastPostDate() {
        Post lastPost = getLastPost();
        return lastPost != null ? lastPost.getDate() : null;
    }

    public List<Post> getMessages() {

        Query q = new Query(context, "select * from # where topic=? order by id");
        q.setTable(Post.class);
        q.setParameter(this);
        return q.execute(Post.class);
    }

    public int getPostCount() {
        return postCount;
    }

    /**
     * Returns a list of all posters
     */
    private List<Item> getPosters() {

        Query q = new Query(context, "select distinct m.* from # m join # p on m.id=p.member where p.topic=? order by m.id");
        q.setTable(Item.class);
        q.setTable(Post.class);
        q.setParameter(this);
        return q.execute(Item.class);
    }

    /**
     * Returns a list of the email addresses of all members who have posted in this topic
     */
    public List<String> getPostersEmails() {

        Query q = new Query(context, "select distinct email from # m join # p on m.id=p.poster where p.topic=?");
        q.setTable(Item.class);
        q.setTable(Post.class);
        q.setParameter(this);
        return q.getStrings();
    }

    public List<Post> getPosts() {
        Query q = new Query(context, "select * from # where topic=? order by id");
        q.setParameter(this);
        q.setTable(Post.class);
        return q.execute(Post.class);
    }

    public int getReplyCount() {
        return postCount - 1;
    }

    public String getTitle() {
        return title;
    }

    /**
     * @return
     */
    public int getViews() {
        return views;
    }

    public boolean hasCreator() {
        return creator != null;
    }

    public boolean hasDescription() {
        return description != null;
    }

    /**
     * Is this topic assigned to an item ?
     */
    public boolean hasItem() {
        return item != null;
    }

    public boolean hasLastPost() {
//		return lastPostAuthor != null;
        return getLastPost() != null;
    }

    /**
     * Returns true if this topic has posts
     */
    public boolean hasPosts() {
        return postCount > 0;
    }

    public boolean isClosed() {
        return closed;
    }

    public boolean isOpen() {
        return !closed;
    }

    /**
     * Send a private message to all contributors to this topic from the owner of this topic.
     */
    public int messageAll(String body) {

        int n = 0;
        for (Item poster : getPosters()) {

            if (getCreator().equals(poster))
                continue;

            //			getCreator().getInteraction().sendMessage(poster, body);
            n++;
        }

        return n;
    }

    /**
     *
     */
    public void open() {
        this.closed = false;
        save();
    }

    /**
     * @param post
     */
    public void removePost(Post post) {

        post.delete();
        setPostCount();

        if (postCount == 0) {

            delete();

        } else {

            setLastPost();
            save();
        }
    }

    private void removePosts() {
        SimpleQuery.delete(context, Post.class, "topic", this);
    }

    @Override
    public synchronized void save() {
        dateUpdated = new DateTime();
        super.save();
    }

    public void sendEmailToAll(String content) throws EmailAddressException, SmtpServerException {

        Email msg = new Email();
        msg.setSubject("Email about: " + getTitle());
        msg.setBody(content);

        /*
           * Set the main to email
           */
        msg.setTo(getCreator().getEmail());

        for (String email : getPostersEmails())
            msg.addBcc(email);

        new EmailDecorator(context, msg).send(Config.getInstance(context).getSmtpHostname());
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private void setLastPost() {
        setLastPost(getLastPost());
    }

    private void setLastPost(Post post) {
        this.lastPostAuthor = post.getAuthor();
        this.lastPostDate = post.getDate();
    }

    private void setPostCount() {
        this.postCount = new Query(context, "select count(*) from # where topic=?").setTable(Post.class).setParameter(this).getInt();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Increase view count by 1
     */
    public void viewed() {
        views++;
        save();

        getForum().viewed();
    }

}