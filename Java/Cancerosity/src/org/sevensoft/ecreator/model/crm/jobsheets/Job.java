package org.sevensoft.ecreator.model.crm.jobsheets;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.iface.admin.crm.jobsheets.JobHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.crm.messages.MessageSupport;
import org.sevensoft.ecreator.model.crm.messages.Msg;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 10 Oct 2006 17:28:13
 *
 */
@Table("jobsheets_jobs")
public class Job extends MessageSupport implements Positionable {

	public enum Priority {
		Green, Amber, Red;
	}

	public enum Status {
		Outstanding, Completed;
	}

	public enum SortType {
		Jobsheet, Title;
	}

	/**
	 * This job can be assigned to an item
	 */
	private Item	item;

	/**
	 * 
	 */
	private DateTime	dateCreated;

	/**
	 * the title / name of this job
	 */
	private String	name;
	/**
	 * 
	 */
	private int		position;
	/**
	 * 
	 */
	private Status	status;

	/**
	 * 
	 */
	private Jobsheet	jobsheet;

	/**
	 * The user that created this job
	 */
	private User	user;

	/**
	 * The user this job is assigned to
	 */
	private User	owner;

	/**
	 * 
	 */
	private Priority	priority;

	/**
	 * The auto job that created this job
	 */
	private AutoJob	autoJob;

	protected Job(RequestContext context) {
		super(context);
	}

	public Job(RequestContext context, Jobsheet jobsheet, User user, String name) {

		super(context);

		this.jobsheet = jobsheet;
		this.user = user;
		this.name = name;
		this.status = Status.Outstanding;
		this.priority = Priority.Green;

		this.dateCreated = new DateTime();
		save();

		Config config = Config.getInstance(context);

		/*
		 * Check for jobsheet emails
		 */
		if (jobsheet != null) {

			if (jobsheet.hasCreationEmails()) {

				Email e = new Email();
				e.setRecipients(jobsheet.getCreationEmails());
				e.setFrom(config.getServerEmail());
				e.setSubject("New job created");
				e.setBody("A new job has been created.\n\nYou can view it here:\n" + getUrl());

				try {
					new EmailDecorator(context, e).send(config.getSmtpHostname());
				} catch (EmailAddressException e1) {
					e1.printStackTrace();
				} catch (SmtpServerException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	@Override
	public Msg addMessage(String author, String email, String body) {

		Msg msg = super.addMessage(author, email, body);
		status = Status.Outstanding;
		save();

		emailRecipients(msg);
		return msg;
	}

	@Override
	public Msg addMessage(User user, String content) {

		Msg msg = super.addMessage(user, content);
		status = Status.Outstanding;
		save();

		emailRecipients(msg);

		return msg;
	}

	private void emailRecipients(Msg msg) {

		Set<User> contributors = super.getContributors();
		contributors.remove(msg.getUser());

		logger.fine("emailing contributors:" + contributors);

		for (User con : contributors) {

			// no point messaging ourselves!
			if (con.equals(msg.getUser())) {
				continue;
			}

			if (con.hasEmail()) {

				// email everyone who has replied with your new message
				Email email = new Email();
				email.setSubject("A new message has been added");
				email.setBody("Hello,\n\n" + con.getName() + " has added a message to job:\n" + name + "\n\nClick here to view:\n" + getUrl() +
						"\n\nThe message was:\n" + msg.getBody());
				email.setTo(con.getEmail());

				email.setFrom(Config.getInstance(context).getServerEmail());
				try {
					new EmailDecorator(context, email).send(Config.getInstance(context).getSmtpHostname());
				} catch (EmailAddressException e) {
					e.printStackTrace();
				} catch (SmtpServerException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 
	 */
	private void follows() {

		// get any jobs that follow this auto job
		if (autoJob == null)
			return;

		List<AutoJob> follows = SimpleQuery.execute(context, AutoJob.class, "follows", autoJob);

		for (AutoJob follow : follows) {
			follow.create(item);
		}
	}

	public List<Attribute> getAttributes() {
		return getJobsheet().getAttributes();
	}

	public AutoJob getAutoJob() {
		return (AutoJob) (autoJob == null ? null : autoJob.pop());
	}

	public String getCreatedBy() {
		return user == null ? "System" : getUser().getName();
	}

	public DateTime getDateCreated() {
		return dateCreated;
	}

	public Item getItem() {
		return (Item) (item == null ? null : item.pop());
	}

	public Jobsheet getJobsheet() {
		return (Jobsheet) (jobsheet == null ? null : jobsheet.pop());
	}

	/**
	 * For a jobsheet return any external addresses used
	 */
	@Override
	public Collection<String> getMessageEmails() {

		Set<String> emails = new HashSet();
		for (Msg msg : getMessages()) {
			if (msg.hasEmail()) {
				emails.add(msg.getEmail());
			}
		}

		return emails;
	}

	public String getMessageSubject() {
		return "Jobsheet: " + getName();
	}

	public String getName() {
		return name == null || name.length() == 0 ? "No subject" : name;
	}

	public final User getOwner() {
		return (User) (owner == null ? null : owner.pop());
	}

	public int getPosition() {
		return position;
	}

	public Priority getPriority() {
		return priority == null ? Priority.Green : priority;
	}

	public Status getStatus() {
		return status == null ? Status.Outstanding : status;
	}

	private String getUrl() {
		return Config.getInstance(context).getUrl() + "/" + new Link(JobHandler.class, null, "job", this);
	}

	public User getUser() {
		return (User) (user == null ? null : user.pop());
	}

	public boolean hasItem() {
		return item != null;
	}

	/**
	 * 
	 */
	public boolean hasJobsheet() {
		return jobsheet != null;
	}

	public boolean hasOwner() {
		return owner != null;
	}

	public boolean isCompleted() {
		return status == Status.Completed;
	}

	public boolean isOutstanding() {
		return getStatus() == Status.Outstanding;
	}

	private void sendCompletedEmail() {

		Config config = Config.getInstance(context);

		Set<String> emails = new HashSet();

		//		if (getJobsheet().isCompletionEmailToContributors()) {
		for (User con : super.getContributors()) {
			if (con.hasEmail()) {
				emails.add(con.getEmail());
			}
		}
		//		}

		for (String email : getJobsheet().getCompletionEmails()) {
			emails.add(email);
		}

		for (String email : emails) {

			Email e = new Email();
			e.setTo(email);
			e.setSubject("Job completed");
			e.setBody("A job has been completed:\n" + name + "\n\nTo view this job click here:\n" + config.getUrl() + "/" +
					new Link(JobHandler.class, null, "job", this));

			e.setFrom(config.getServerEmail());

			try {

				new EmailDecorator(context, e).send(config.getSmtpHostname());

			} catch (EmailAddressException ex) {
				ex.printStackTrace();

			} catch (SmtpServerException ex) {
				ex.printStackTrace();
			}

		}
	}

	public void setAutoJob(AutoJob job) {
		this.autoJob = job;
	}

	public void setDateCreated(DateTime dateCreated) {
		this.dateCreated = dateCreated;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public void setJobsheet(Jobsheet newJobsheet) {

		Jobsheet old = jobsheet;

		this.jobsheet = newJobsheet;
		save();

		if (old != null) {
			old.setJobCounts();
		}

		if (newJobsheet != null) {
			newJobsheet.setJobCounts();
		}

	}

	public void setName(String name) {
		this.name = name;
	}

	public final void setOwner(User owner) {
		this.owner = owner;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public void setStatus(Status s) {

		if (status == s) {
			return;
		}

		this.status = s;
		save();

		getJobsheet().setJobCounts();

		if (isCompleted()) {

			// email all contributors status has changed
			sendCompletedEmail();

			// check for auto job follow on
			follows();

		}
	}
}
