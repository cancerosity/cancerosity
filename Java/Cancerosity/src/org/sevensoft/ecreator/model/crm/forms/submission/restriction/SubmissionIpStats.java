package org.sevensoft.ecreator.model.crm.forms.submission.restriction;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.samdate.Date;

/**
 * User: Tanya
 * Date: 29.02.2012
 */
@Table("forms_submissions_ip_stats")
public class SubmissionIpStats extends EntityObject {


    public static SubmissionIpStats get(RequestContext context, String ipAddress) {
        SubmissionIpStats ipStats = SimpleQuery.get(context, SubmissionIpStats.class, "ipAddress", ipAddress, "lastSent", new Date());
        return ipStats == null ? new SubmissionIpStats(context, ipAddress) : ipStats;
    }

    private String ipAddress;
    private Date lastSent;
    private int sentCount;

    public SubmissionIpStats(RequestContext context) {
        super(context);
    }

    public SubmissionIpStats(RequestContext context, String ipAddress) {
        super(context);
        this.ipAddress = ipAddress;
        this.lastSent = new Date();
        save();
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Date getLastSent() {
        return lastSent;
    }

    public void setLastSent(Date lastSent) {
        this.lastSent = lastSent;
    }

    public int getSentCount() {
        return sentCount;
    }

    public void setSentCount(int sentCount) {
        this.sentCount = sentCount;
    }

    public void countIncrease() {
        sentCount++;
        save("sentCount");
    }
}
