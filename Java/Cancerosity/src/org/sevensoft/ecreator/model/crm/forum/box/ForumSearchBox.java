package org.sevensoft.ecreator.model.crm.forum.box;

import org.sevensoft.ecreator.iface.frontend.forum.PostSearchHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 10 May 2006 10:57:52
 *
 */
@Table("boxes_forums_search")
@Label("Forum search")
@HandlerClass(ForumSearchBoxHandler.class)
public class ForumSearchBox extends Box {

	public ForumSearchBox(RequestContext context) {
		super(context);
	}

	public ForumSearchBox(RequestContext context, boolean b) {
		super(context, "Right");
	}

	@Override
	protected String getCssIdDefault() {
		return "forum-search";
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	@Override
	public String render(RequestContext context) {

		StringBuilder sb = new StringBuilder();
		sb.append(getTableTag());
		sb.append(new FormTag(PostSearchHandler.class, "search", "GET"));

		sb.append("<tr><td>" + new TextTag(context, "keywords", 20) + "</td></tr>");
		sb.append("<br/>");
		sb.append("<tr><td>" + new TextTag(context, "author", 12) + "</td></tr>");

		SelectTag ageTag = new SelectTag(context, "age");
		ageTag.addOption("1", "Today or newer");
		ageTag.addOption("7", "1 week ago");
		ageTag.addOption("", "2 weeks ago");
		ageTag.addOption("", "1 month ago");
		ageTag.addOption("", "6 months ago");
		ageTag.addOption("", "12 months ago");
		ageTag.addOption("", "All posts");

		sb.append("<tr><td>" + ageTag + "</td></tr>");

		sb.append("<tr><td>" + new SubmitTag("Search") + "</td></tr>");

		sb.append("</form>");
		sb.append("</table>");

		return sb.toString();
	}

}
