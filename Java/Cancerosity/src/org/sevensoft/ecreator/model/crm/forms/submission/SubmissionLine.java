package org.sevensoft.ecreator.model.crm.forms.submission;

import java.util.List;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Nov 2006 13:04:13
 *
 */
@Table("forms_submissions_lines")
public class SubmissionLine extends EntityObject {

	@Index
	private Submission	submission;

	@Index
	private Item		item;

	/**
	 * Qty of items
	 */
	private int			qty;

	private int			optionCount;

	protected SubmissionLine(RequestContext context) {
		super(context);
	}

	public SubmissionLine(RequestContext context, Submission submission, Item item) {
		super(context);

		this.submission = submission;
		this.item = item;
		this.qty = 1;

		save();
	}

	public void addOption(ItemOption itemOption, String value) {
		new SubmissionLineOption(context, this, itemOption, value);
		setOptionCount();
	}

	public Item getItem() {
		return (Item) (item == null ? null : item.pop());
	}

	public List<SubmissionLineOption> getOptions() {
		return SimpleQuery.execute(context, SubmissionLineOption.class, "line", this);
	}

	public int getQty() {
		return qty;
	}

	public Submission getSubmission() {
		return submission.pop();
	}

	/**
	 * 
	 */
	public boolean hasOptions() {
		return optionCount > 1;
	}

	/**
	 * 
	 */
	private void setOptionCount() {
		this.optionCount = getOptions().size();
		save();
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

}
