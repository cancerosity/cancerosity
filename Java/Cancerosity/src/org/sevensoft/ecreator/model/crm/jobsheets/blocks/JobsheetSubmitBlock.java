package org.sevensoft.ecreator.model.crm.jobsheets.blocks;

import org.sevensoft.ecreator.iface.admin.crm.jobsheets.JobsheetSubmitBlockHandler;
import org.sevensoft.ecreator.iface.frontend.crm.jobsheets.JobsheetSubmitHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.crm.jobsheets.Jobsheet;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 12 Mar 2007 13:01:37
 *
 */
@Table("blocks_jobsheets_submit")
@Label("Jobsheet submit")
@HandlerClass(JobsheetSubmitBlockHandler.class)
public class JobsheetSubmitBlock extends Block {

	private Jobsheet	jobsheet;

	public JobsheetSubmitBlock(RequestContext context) {
		super(context);
	}

	public JobsheetSubmitBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, 0);
	}

	public final Jobsheet getJobsheet() {
		return (Jobsheet) (jobsheet == null ? null : jobsheet.pop());
	}

	@Override
	public Object render(RequestContext context) {

		if (jobsheet == null) {
			return null;
		}

		StringBuilder sb = new StringBuilder();

		sb.append(new TableTag("ec_jobsheet_submit"));
		sb.append(new FormTag(JobsheetSubmitHandler.class, null, "multi"));
		sb.append(new HiddenTag("jobsheet", getJobsheet()));
		sb.append(new HiddenTag("category", getOwnerCategory()));

		sb.append("<tr><td>Name</td><td>" + new TextTag(context, "name", 30) + new ErrorTag(context, "name", "<br/>") + "</td></tr>");
		sb.append("<tr><td>Email</td><td>" + new TextTag(context, "email", 30) + new ErrorTag(context, "email", "<br/>") + "</td></tr>");

		sb.append("<tr><td>Message</td><td>" + new TextAreaTag(context, "body", 60, 5) + new ErrorTag(context, "body", "<br/>") + "</td></tr>");

		sb.append("<tr><td></td><td>" + new SubmitTag("Submit") + "</td></tr>");

		sb.append("</form>");
		sb.append("</table>");

		return sb;
	}

	public final void setJobsheet(Jobsheet jobsheet) {
		this.jobsheet = jobsheet;
	}

}
