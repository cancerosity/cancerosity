package org.sevensoft.ecreator.model.crm.jobsheets;

import java.util.List;

import org.sevensoft.commons.samdate.Period;
import org.sevensoft.ecreator.model.crm.jobsheets.Job.Priority;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 6 Nov 2006 16:33:13
 * 
 * Job automatically made when another job is completed or item type made
 *
 */
@Table("jobs_auto")
public class AutoJob extends EntityObject implements Selectable {

	/**
	 * 
	 */
	public static List<AutoJob> get(RequestContext context) {
		return SimpleQuery.execute(context, AutoJob.class);
	}

	public static List<AutoJob> get(RequestContext context, ItemType itemType) {
		return SimpleQuery.execute(context, AutoJob.class, "itemType", itemType);
	}

	/**
	 * This job should be created when an item of this item type is created
	 */
	private ItemType	itemType;

	/**
	 * This auto job should be created when this follows job has been completed
	 */
	private AutoJob	follows;

	/**
	 * Name of the job to create
	 */
	private String	name;

	private String	content;

	private Jobsheet	jobsheet;

	private Priority	priority;

	private Period	deadline;

	@SuppressWarnings("unused")
	private AutoJob(RequestContext context) {
		super(context);
	}

	public AutoJob(RequestContext context, String name) {
		super(context);
		this.name = name;
		this.priority = Priority.Green;

		save();
	}

	/**
	 * Create the job from this auto job
	 */
	public Job create(Item item) {

		if (jobsheet == null)
			return null;

		// if item is not null then append item to the name
		String s = name;
		if (item != null) {
			s = s + " - " + item.getName();
		}

		Job job = getJobsheet().addJob(null, s);

		if (content != null) {
			job.addMessage("System", null, getContent());
		}
		job.setPriority(getPriority());
		job.setAutoJob(this);
		job.setItem(item);

		job.save();

		return job;
	}

	@Override
	public synchronized boolean delete() {

		new Query(context, "update # set follows=0 where follows=?").setTable(AutoJob.class).setParameter(this).run();
		new Query(context, "update # set autoJob=0 where autoJob=?").setTable(Job.class).setParameter(this).run();

		return super.delete();
	}

	public String getContent() {
		return content;
	}

	public Period getDeadline() {
		return deadline;
	}

	public AutoJob getFollows() {
		return (AutoJob) (follows == null ? null : follows.pop());
	}

	public ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public Jobsheet getJobsheet() {
		return (Jobsheet) (jobsheet == null ? null : jobsheet.pop());
	}

	public String getLabel() {
		return getName();
	}

	public String getName() {
		return name;
	}

	public Priority getPriority() {
		return priority == null ? Priority.Green : priority;
	}

	public String getValue() {
		return getIdString();
	}

	public boolean hasJobsheet() {
		return jobsheet != null;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setDeadline(Period deadline) {
		this.deadline = deadline;
	}

	public void setFollows(AutoJob f) {
		this.follows = f;
	}

	public void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	public void setJobsheet(Jobsheet jobsheet) {
		this.jobsheet = jobsheet;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

}
