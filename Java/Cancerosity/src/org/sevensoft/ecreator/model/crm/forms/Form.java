package org.sevensoft.ecreator.model.crm.forms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.net.MalformedURLException;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.model.accounts.permissions.Permissions;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.crm.captcha.Captcha;
import org.sevensoft.ecreator.model.crm.forms.blocks.FormBlock;
import org.sevensoft.ecreator.model.crm.forms.box.FormBox;
import org.sevensoft.ecreator.model.crm.forms.panels.FormPanel;
import org.sevensoft.ecreator.model.crm.forms.submission.Submission;
import org.sevensoft.ecreator.model.crm.forms.submission.SubmissionData;
import org.sevensoft.ecreator.model.crm.forms.submission.restriction.SubmissionIpStats;
import org.sevensoft.ecreator.model.crm.forms.submission.restriction.SubmissionRestrictionModule;
import org.sevensoft.ecreator.model.crm.forms.googleform.GoogleForm;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.marketing.newsletter.Subscriber;
import org.sevensoft.ecreator.model.misc.Page;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.stats.submissions.FormSubmissionCounter;
import org.sevensoft.ecreator.model.stats.submissions.SubmissionRecord;
import org.sevensoft.ecreator.model.stats.submissions.SubmissionsCounter;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 07-Jul-2005 16:59:54
 * 
 */
@Table("forms")
public class Form extends EntityObject implements Selectable, Permissions, Logging {

	public static List<Form> get(RequestContext context) {
		Query q = new Query(context, "select * from # order by name");
		q.setTable(Form.class);
		return q.execute(Form.class);
	}

	/*
	 * Number of categories this form is in
	 */
	private int			categoryCount;

	private boolean		captcha;

	/**
	 * Custom markup to override the standard renderer
	 */
	private Markup		markup;

	private Date		dateCreated, dateUpdated;

	private String		name, submitButtonText, submissionMessage, submissionEmailMessage;

	/**
	 * The emails of the people who should receive notification of the form being submitted.
	 * 
	 */
	private List<String>	emails;

	@Deprecated
	private String		recipient;

	/**
	 * BC - should be emailField really. Used when an email is rquired - for sending replies and signing people up.
	 */
	private FormField		emailResponseField;

	private List<String>	smsNumbers;

	/**
	 * Forward this user to an URL when the form is submitted rather than showing completed message.
	 */
	private String		submissionForward;

	/**
	 * 
	 */
	private boolean		permissions;

	/*
	 * Use the simple html editor for this form
	 */
	private boolean		simpleEditor;

	/**
	 * Show this form at checkout
	 */
	private boolean		checkout;

	/**
	 * Cc submission to the account who listed the item
	 */
	private boolean		ccItemListers;

    /**
     * a record which will be added to mail notification sent to listing owner on form submission
     */
    private String itemListersMailHeader;
    private String itemListersMailFooter;
	/**
	 * Content at the top of a form
	 */
	private String		header;

	/**
	 * Require an account before this form can be sent and attach the submission to the account of the person who is logged in.
	 */
	private boolean		accountRequired;

	private String		footer;

	private boolean		lineQuantities;

	private String		restrictionForwardUrl;

	/**
	 * Script that will be added to the form submission page
	 */
	private String		submissionScript;

	private String		cssId;

	private String		cssClass;

    /**
     * when set to Yes will include the form ID of the submission into the emai
     */
    private boolean		includeSubmId;


    private String trIdFirst;

    private String trIdLast;

	public Form(RequestContext context) {
		super(context);
	}

	public Form(RequestContext context, String string) {
		super(context);

		this.name = string;

		this.dateCreated = new Date();
		this.dateUpdated = new Date();

		save();
	}

	public FormField addField(String name, FieldType type) {
		FormField field = new FormField(context, this, name, type);
		return field;
	}

	public FormLine addItem(Item item, Map<ItemOption, String> options, Map<ItemOption, Upload> optionUploads) {

		FormLine line = new FormLine(context, this, item);
		line.addOptions(options);

		try {
			line.addAttachments(optionUploads.values());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (AttachmentLimitException e) {
			e.printStackTrace();
		} catch (AttachmentExistsException e) {
			e.printStackTrace();
		} catch (AttachmentTypeException e) {
			e.printStackTrace();
		}

		return line;
	}

	public void addNewsletterSignUp(Newsletter newsletter) {
		new NewsletterSignup(context, newsletter, this);
	}

    public void checkErrors(LinkedHashMap<FormField, String> data, String captchaResponse) {
        checkErrors(data, captchaResponse, null);
    }

    public void checkErrors(LinkedHashMap<FormField, String> data, String captchaResponse, LinkedHashMap<FormField, String> duplicateData) {

		for (FormField field : getFields()) {

			String value = data.get(field);
			if (!field.isValid(value)) {

				logger.fine("[FormHandler] field value not valid value=" + value + ", field=" + field);

				if (field.hasErrorMessage()) {

					logger.fine("[FormHandler] setting custom error message param=" + field.getParamName() + ", message=" + field.getErrorMessage());
					context.setError(field.getParamName(), field.getErrorMessage());

				} else {

					logger.fine("[FormHandler] setting standard error param=" + field.getParamName());
					context.setError(field.getParamName(), "Field must be completed");

				}

			}

            //check repeat Email
            if (field.isRepeatEmail()) {
                if (duplicateData != null) {
                    String duplicate = duplicateData.get(field);
                    if (value != null && !value.equalsIgnoreCase(duplicate)) {

                        context.setError(field.getParamName(), " do not match");
                        context.setError(field.getRepeatParamName(), " do not match");
                    }
                }
            }
		}

		// check for captcha
		if (isCaptcha()) {

            // get captcha for session
            List<Captcha> list = Captcha.getList(context, this);
            boolean responceCorrect = false;
            for (Captcha c : list) {
                String word = c.getWord();
                if (word.equalsIgnoreCase(captchaResponse)) {
                    responceCorrect = true;
                }
            }

            if (!responceCorrect) {
                context.setError("captchaResponse", "Invalid response to verification code.");

            }
		}
	}

	public Form copy() throws CloneNotSupportedException {

		Form form = (Form) super.clone();
		form.name = name + " (copy)";
		form.permissions = false;
		form.save();

		for (FormField field : getFields()) {
			field.copyTo(form);
		}

		return form;
	}

	@Override
	public synchronized boolean delete() {

		SimpleQuery.delete(context, FormField.class, "form", this);
		SimpleQuery.delete(context, FormBlock.class, "form", this);
		SimpleQuery.delete(context, FormBox.class, "form", this);
		SimpleQuery.delete(context, SubmissionsCounter.class, "form", this);
		SimpleQuery.delete(context, NewsletterSignup.class, "form", this);

		SimpleQuery.delete(context, FormLine.class, "form", this);
		new Query(context, "update # set form=0 where form=?").setTable(Submission.class).setParameter(this).run();

		return super.delete();
	}

	public int getCategoryCount() {
		return categoryCount;
	}

	public List<Form> getCheckout(RequestContext context) {
		return SimpleQuery.execute(context, Form.class, "checkout", true);
	}

	public String getCssClass() {
		return cssClass;
	}

	public final String getCssId() {
		return cssId;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public FormField getEmailField() {
		return (FormField) (emailResponseField == null ? null : emailResponseField.pop());
	}

	public List<String> getEmails() {
		if (emails == null) {
			emails = new ArrayList();
			if (recipient != null) {
				for (String string : recipient.split("\n")) {
					emails.add(string);
				}
			}
		}
		return emails;
	}

	public FormField getField(int id) {
		for (FormField field : getFields()) {
			if (field.getId() == id) {
				return field;
			}
		}
		return null;
	}

	public List<FormField> getFields() {
		return SimpleQuery.execute(context, FormField.class, "form", this);
	}

	public String getFooter() {
		return footer;
	}

	public String getHeader() {
		return header;
	}

	public String getLabel() {
		return getName();
	}

	/**
	 * Returns the form lines on this form for this session id
	 */
	public List<FormLine> getLines(String sessionId) {

		Query q = new Query(context, "select * from # where form=? and sessionId=? order by position");
		q.setParameter(this);
		q.setParameter(sessionId);
		q.setTable(FormLine.class);
		return q.execute(FormLine.class);
	}

	public List<LogEntry> getLogEntries() {
		return null;
	}

	public String getLogId() {
		return "form#" + getId();
	}

	public String getLogName() {
		return "Form #" + getId() + " - " + name;
	}

	public Markup getMarkup() {
		return (Markup) (markup == null ? null : markup.pop());
	}

	public String getName() {
		return name;
	}

	public List<NewsletterSignup> getNewsletterSignUps() {
		return SimpleQuery.execute(context, NewsletterSignup.class, "form", this);
	}

	public List<Category> getPages() {

		Query q = new Query(context, "select * from # where form=?");
		q.setTable(Category.class);
		q.setParameter(this);
		return q.execute(Category.class);
	}

	public String getRestrictionForwardUrl() {
		return restrictionForwardUrl;
	}

	public final List<String> getSmsNumbers() {
		if (smsNumbers == null) {
			smsNumbers = new ArrayList();
		}
		return smsNumbers;
	}

	public String getSubmissionForward() {
		return submissionForward;
	}

	public String getSubmissionMessage() {
		return submissionMessage == null ? "Thank you, the form has been submitted." : submissionMessage;
	}

    public String getSubmissionEmailMessage() {
        return submissionEmailMessage == null ? getSubmissionMessage(): submissionEmailMessage;
    }

    public String getSubmissionMessageBr() {
		return HtmlHelper.nl2br(getSubmissionMessage());
	}

	public String getSubmissionScript() {
		return submissionScript;
	}

	/**
	 * 
	 */
	public List<Submission> getSubmissions() {
		return SimpleQuery.execute(context, Submission.class, "form", this);
	}

	public String getSubmitButtonText() {
		return submitButtonText == null ? "Submit form" : submitButtonText;
	}

	public String getValue() {
		return getIdString();
	}

	public boolean hasEmailField() {
		return emailResponseField != null;
	}

	public boolean hasEmails() {
		return getEmails().size() > 0;
	}

	public boolean hasFooter() {
		return footer != null;
	}

	public boolean hasHeader() {
		return header != null;
	}

	/**
	 * Returns true if this form is displayed in any blocks owned by item types
	 */
	public boolean hasItemTypes() {
		Query q = new Query(context, "select count(*) from # where ownerItemType>0 and form=?");
		q.setTable(FormBlock.class);
		q.setParameter(this);
		return q.getInt() > 0;
	}

	public boolean hasMarkup() {
		return markup != null;
	}

	public boolean hasSmsNumbers() {
		return getSmsNumbers().size() > 0;
	}

	public boolean hasSubmissionForward() {
		return submissionForward != null;
	}

	public boolean isAccountRequired() {
		return accountRequired;
	}

	public boolean isAdvancedEditor() {
		return !isSimpleEditor();
	}

	public final boolean isCaptcha() {
		return captcha && Module.Captcha.enabled(context);
	}

	public boolean isCcItemListers() {
		return ccItemListers;
	}

    public String getItemListersMailFooter() {
        return itemListersMailFooter == null ? "" : itemListersMailFooter;
    }

    public String getItemListersMailHeader() {
        return itemListersMailHeader == null ? "" : itemListersMailHeader;
    }

    public final boolean isCheckout() {
		return checkout;
	}

	public boolean isEnoteOpen() {
		return true;
	}

	public boolean isLineQuantities() {
		return lineQuantities;
	}

	public boolean isNoGuestLogin() {
		return false;
	}

	public boolean isPermissions() {
		return permissions;
	}

	public boolean isSimpleEditor() {
		return simpleEditor;
	}

    public boolean isIncludeSubmId() {
        return includeSubmId;
    }

    public String getTrIdFirst() {
        return trIdFirst == null ? "first" : trIdFirst;
    }

    public String getTrIdLast() {
        return trIdLast == null ? "last" : trIdLast;
    }

    public void log(String message) {
		new LogEntry(context, this, message);
	}

	public void log(User user, String message) {
		new LogEntry(context, this, user, message);
	}

	public void removeField(FormField field) {
		field.delete();
	}

	public void removeLine(FormLine line) {
		line.delete();
	}

	public Object render() {

		logger.fine("[Form] rendering form, markup=" + getMarkup());

		if (markup == null) {

			return new FormPanel(context, this);

		} else {

			MarkupRenderer r = new MarkupRenderer(context, getMarkup());
			r.setBody(this);
			return r;
		}
	}

	@Override
	protected void schemaInit(RequestContext context) {

		// ensure we have at least one form
		if (SimpleQuery.count(context, Form.class) == 0) {
			FormPreset.ContactForm.create(context, "Contact us");
		}
	}

	public void setAccountRequired(boolean accountRequired) {
		this.accountRequired = accountRequired;
	}

	public final void setCaptcha(boolean captcha) {
		this.captcha = captcha;
	}

	public void setCcItemListers(boolean b) {
		this.ccItemListers = b;
	}

    public void setItemListersMailFooter(String itemListersMailFooter) {
        this.itemListersMailFooter = itemListersMailFooter;
    }

    public void setItemListersMailHeader(String itemListersMailHeader) {
        this.itemListersMailHeader = itemListersMailHeader;
    }

    public final void setCheckout(boolean checkout) {
		this.checkout = checkout;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	public final void setCssId(String cssId) {
		this.cssId = cssId;
	}

	public void setEmailField(FormField emailResponseField) {
		this.emailResponseField = emailResponseField;
	}

	public final void setEmails(List<String> emails) {
		this.emails = emails;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	public void setHeader(String topContent) {
		this.header = topContent;
	}

	public void setLineQuantities(boolean lineQuantities) {
		this.lineQuantities = lineQuantities;
	}

	public void setMarkup(Markup markup) {
		this.markup = markup;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPermissions(boolean p) {
		this.permissions = p;
	}

	public void setRestrictionForwardUrl(String restrictionForwardUrl) {
		this.restrictionForwardUrl = restrictionForwardUrl;
	}

	public void setSimpleEditor(boolean simpleEditor) {
		this.simpleEditor = simpleEditor;
	}

	public final void setSmsNumbers(List<String> smsNumbers) {
		this.smsNumbers = smsNumbers;
	}

	public void setSubmissionForward(String submissionForward) {
		this.submissionForward = submissionForward;
	}

	public void setSubmissionMessage(String message) {
		this.submissionMessage = message;
	}

    public void setSubmissionEmailMessage(String submissionEmailMessage) {
        this.submissionEmailMessage = submissionEmailMessage;
    }

    public void setSubmissionScript(String submissionScript) {
		this.submissionScript = submissionScript;
	}

	public void setSubmitButtonText(String submitButtonText) {
		this.submitButtonText = submitButtonText;
	}

    public void setIncludeSubmId(boolean includeSubmId) {
        this.includeSubmId = includeSubmId;
    }

    public void setTrIdFirst(String trIdFirst) {
        this.trIdFirst = trIdFirst.trim();
    }

    public void setTrIdLast(String trIdLast) {
        this.trIdLast = trIdLast.trim();
    }

    /**
	 * @param owner
	 *              - the owner of this submission, or null if it is a generic form submission
	 * @param item
	 *              - the page this form was submitted on
	 * @param category
	 *              - if submitted on a category
	 */
	public Submission submit(SubmissionOwner owner, final Map<FormField, String> data, List<Upload> uploads, String ipAddress, Page page) {

		logger.fine("[Form] Submitting form");

        if (Module.FormSubmissionRestriction.enabled(context)){
            SubmissionRestrictionModule module = SubmissionRestrictionModule.getInstance(context);
            if (module.getSubmissionsAllowed() > 0) {
                SubmissionIpStats ipStats = SubmissionIpStats.get(context, ipAddress);
                if (ipStats.getSentCount() >= module.getSubmissionsAllowed()) {
                    return null;
                } else {
                    ipStats.countIncrease();
                }
            }
        }

		Item account = (Item) context.getAttribute("account");
		Submission submission = new Submission(context, this, owner, data, ipAddress, page);

		log("Form submitted");
		submission.log("Created");

		try {
			submission.addAttachments(uploads);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (AttachmentLimitException e) {
			e.printStackTrace();
		} catch (AttachmentExistsException e) {
			e.printStackTrace();
		} catch (AttachmentTypeException e) {
			e.printStackTrace();
		}

		if (accountRequired) {
			logger.fine("[Form] submitting with account=" + account);
			submission.setAccount(account);
		}

		if (hasEmails()) {

			logger.fine("[Form] submitting via email");

			try {
				submission.emailSubmission(getEmails());
			} catch (EmailAddressException e) {
				e.printStackTrace();
			} catch (SmtpServerException e) {
				e.printStackTrace();
			}
		}

		if (hasSmsNumbers() && Module.Sms.enabled(context)) {

			logger.fine("[Form] submitting via sms");
			submission.sms(getSmsNumbers());
		}

		/*
		 * If we have set an email response field we will try to send the customer the submission message.
		 */
		// if (hasEmailField()) {

		logger.fine("[Form] Sending msg to email response field: " + data.get(getEmailField()));

		try {

			Email email = new Email();
			email.setFrom(Company.getInstance(context).getName(), Config.getInstance(context).getServerEmail());

            email.setBody(getSubmissionEmailMessage());

            if (data.get(getEmailField()) != null) {
                email.setTo(data.get(getEmailField()));
            } else {
                for (SubmissionData submissionData : submission.getData()) {
                    if (submissionData.getName().startsWith("Email")) {
                        email.setTo(submissionData.getValue());
                    }
                }
            }
            
			email.setSubject("Form completed");
			if (uploads != null) {
				for (Upload upload : uploads) {
					email.addAttachment(upload.getFile(), upload.getFilenameSafe());
				}
			}
			new EmailDecorator(context, email).send(Config.getInstance(context).getSmtpHostname());

		} catch (EmailAddressException e) {
			e.printStackTrace();
		} catch (SmtpServerException e) {
			e.printStackTrace();
		}

		if (isCcItemListers()) {

			if (page instanceof Item) {

				Item item = (Item) page;

				if (item.hasAccount() && item.getAccount().hasEmail()) {
                    emailSubmission(submission, item.getAccount().getEmail());
                } else if (item.getItemType().isAccount(context) && item.hasEmail()) {
                    emailSubmission(submission, item.getEmail());
                }

			}
		}

		/*
		 * Add in any newsletter signups
		 */

		List<NewsletterSignup> newsletterSignUps = getNewsletterSignUps();
		if (newsletterSignUps.size() > 0) {

			String emailValue = data.get(getEmailField());
			if (emailValue != null) {
				Subscriber sub = Subscriber.get(context, emailValue, true);

				sub.validate();

				for (NewsletterSignup signup : newsletterSignUps) {

					signup.getNewsletter().subscribe(sub);

				}
			}
		}
		// }

		if (Module.Sms.enabled(context)) {

			if (page instanceof Item) {

				List<String> smsNumbers = new ArrayList<String>();
				Item listing = (Item) page;
				Item ownerAccount = listing.getAccount();

				if (ownerAccount != null) {
					if (ownerAccount.hasMobilePhone()) {
						smsNumbers.add(account.getMobilePhone());
					} else if (ownerAccount.getAttributeValue("Mobile") != null && !ownerAccount.getAttributeValue("Mobile").equals("")
							&& ownerAccount.getAttributeValue("Mobile").length() != 0) {
						smsNumbers.add(ownerAccount.getAttributeValue("Mobile"));
					}

					if (smsNumbers.size() != 0) {
						submission.sms(smsNumbers);
					}
                } else if (listing.getItemType().isAccount(context)) {
                    if (listing.hasMobilePhone()) {
                        smsNumbers.add(listing.getMobilePhone());
                    } else if (listing.getAttributeValue("Mobile") != null && !listing.getAttributeValue("Mobile").equals("")
                            && listing.getAttributeValue("Mobile").length() != 0) {
                        smsNumbers.add(listing.getAttributeValue("Mobile"));
                    }
                }

                submission.sms(smsNumbers);
			}

		}

		new SubmissionRecord(context, page, this, ipAddress);

		if (page instanceof Item) {
			FormSubmissionCounter.update(context, (Item) page);
		}

		/*
		 * Update general submission stats
		 */
		SubmissionsCounter.update(context, this);

        if (Module.GoogleForm.enabled(context)) {
            logger.fine("GoogleForm.post data to google spreadsheet");
            try {
                GoogleForm.post(context, this, data, page);
            } catch (MalformedURLException e) {
                logger.warning("GoogleForm.post Url error =" + e.toString());
            } catch (IOException e) {
                logger.warning("GoogleForm.post exc =" + e.toString());
            }
        }

		return submission;
	}

    private void emailSubmission(Submission submission, String email) {
        try {
            submission.emailSubmission(email);
        } catch (EmailAddressException e) {
            e.printStackTrace();
        } catch (SmtpServerException e) {
            e.printStackTrace();
        }
    }
}
