package org.sevensoft.ecreator.model.crm.forms;

import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 10 Oct 2006 13:57:18
 * 
 * Show the custom
 *
 */
@Table("forms_newslettersignup")
public class NewsletterSignup extends EntityObject implements Positionable {

	/**
	 * 
	 */
	private Newsletter	newsletter;
	
	/**
	 * 
	 */
	private Form		form;
	
	/**
	 * 
	 */
	private int			position;

	/**
	 * Optional means this will be shown to the customer. 
	 * If non optional then you will be auto signed up without being asked!
	 */
	private boolean		optional;

	NewsletterSignup(RequestContext context) {
		super(context);
	}

	public NewsletterSignup(RequestContext context, Newsletter newsletter, Form form) {
		super(context);

		this.newsletter = newsletter;
		this.form = form;

		save();
	}

	public Form getForm() {
		return form.pop();
	}

	public Newsletter getNewsletter() {
		return newsletter.pop();
	}

	public int getPosition() {
		return position;
	}

	public boolean isOptional() {
		return optional;
	}

	public void setOptional(boolean optional) {
		this.optional = optional;
	}

	public void setPosition(int position) {
		this.position = position;
	}

}
