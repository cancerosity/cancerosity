package org.sevensoft.ecreator.model.crm.forum.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.forum.PostSearchHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 30 Jun 2007 13:05:16
 *
 */
public class AccountPostsMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		return super.link(context, params, new Link(PostSearchHandler.class, "search", "account", item));
	}

	public Object getRegex() {
		return "forum_account_posts";
	}

}
