package org.sevensoft.ecreator.model.crm.forms.export;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.FormField;
import org.sevensoft.ecreator.model.crm.forms.submission.Submission;
import org.sevensoft.ecreator.model.crm.forms.submission.SubmissionData;
import org.sevensoft.jeezy.http.RequestContext;

import com.ricebridge.csvman.CsvSaver;

/**
 * @author sks 22 May 2007 02:04:32
 *
 */
public class SubmissionExporter {

	private RequestContext		context;
	private List<FormField>		fields;
	private List<Submission>	submissions;
	private List<String>		fieldNames;
	private final Form		form;

	public SubmissionExporter(RequestContext context, Form form, List<FormField> fields) {
		super();
		this.context = context;
		this.form = form;
		this.fields = fields;
		this.fieldNames = new ArrayList<String>();
		for (FormField field : fields) {
			this.fieldNames.add(field.getName());
		}
		this.submissions = form.getSubmissions();
	}

    public SubmissionExporter(RequestContext context, Form form, List<FormField> fields, List<Submission> submissions) {
        super();
        this.context = context;
        this.form = form;
        this.fields = fields;
        this.fieldNames = new ArrayList<String>();
        this.submissions = submissions;
        for (FormField field : fields) {
            this.fieldNames.add(field.getName());
        }
    }

    public void export(CsvSaver saver) {

        for (Submission submission : submissions) {

            List<String> list = new ArrayList<String>();
            
            list.add(form.getIdString());
            list.add(submission.getDate().toDateTimeString());

            if (form.isIncludeSubmId()){
                list.add(String.valueOf(submission.getId()));
            }
            list.add(submission.getName());
            HashMap<String, String> map = new HashMap<String, String>();

            for (SubmissionData data : submission.getData()) {
                map.put(data.getName(), data.getValue());
            }

            for (String fieldName : fieldNames) {
                if (map.containsKey(fieldName)) {
                    list.add(map.get(fieldName));
                } else {
                    list.add(" ");
                }
            }

            String[] row = list.toArray(new String[list.size()]);
            saver.next(row);
        }
    }

    public void header(CsvSaver saver) {
        List<String> headers = new ArrayList<String>();
        headers.add("Form number");
        headers.add("Date of submission");
        if (form.isIncludeSubmId()) {
            headers.add("Submission number");
        }
        headers.add("Submission name");
        for (String fieldName : fieldNames) {
            headers.add(fieldName);
        }
        String[] headerRow = headers.toArray(new String[headers.size()]);
        saver.next(headerRow);
    }
}
