package org.sevensoft.ecreator.model.crm.forms.submission.restriction;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * User: Tanya
 * Date: 29.02.2012
 */
@Table("forms_submissions_restriction_module")
@Singleton
public class SubmissionRestrictionModule extends EntityObject {

    private int submissionsAllowed;
    private String customMessage;

    public SubmissionRestrictionModule(RequestContext context) {
        super(context);
    }

    public static SubmissionRestrictionModule getInstance(RequestContext context) {
        return getSingleton(context, SubmissionRestrictionModule.class);
    }

    public int getSubmissionsAllowed() {
        return submissionsAllowed;
    }

    public void setSubmissionsAllowed(int submissionsAllowed) {
        this.submissionsAllowed = submissionsAllowed;
    }

    public String getCustomMessage() {
        return customMessage == null ? "You can only send " + getSubmissionsAllowed() + " forms per day" : customMessage;
    }

    public void setCustomMessage(String customMessage) {
        this.customMessage = customMessage;
    }
}
