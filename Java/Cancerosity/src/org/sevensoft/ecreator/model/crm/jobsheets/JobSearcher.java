package org.sevensoft.ecreator.model.crm.jobsheets;

import java.util.List;

import org.sevensoft.ecreator.model.crm.jobsheets.Job.SortType;
import org.sevensoft.ecreator.model.crm.jobsheets.Job.Status;
import org.sevensoft.ecreator.model.crm.messages.Msg;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.StringQueryBuilder;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 26 Mar 2007 11:45:29
 *
 */
public class JobSearcher {

	private String		author;
	private String		content;
	private RequestContext	context;
	private int			limit;
	private int			start;
	private Status		status;
	private User		user;
	private User		owner;
	private boolean		unassigned;
	private String		name;
	private SortType		sortType;
	private Jobsheet		jobsheet;

	public JobSearcher(RequestContext context) {
		this.context = context;
	}

	public List<Job> execute() {
		return (List<Job>) run(false);
	}

	public final String getAuthor() {
		return author;
	}

	public final String getContent() {
		return content;
	}

	public Object run(boolean count) {

		StringQueryBuilder b = new StringQueryBuilder(context);
		if (count) {
			b.append("select count(*) ");
		} else {
			b.append("select j.* ");
		}

		b.append(" from # j ");
		b.addTable(Job.class);
		if (content != null) {
			b.append(" join # m on j.id=m.job ");
			b.addTable(Msg.class);
		}

		b.append(" where 1=1 ");

		if (author != null) {
			b.append(" and m.author like ? ", "%" + author + "%");
		}

		if (content != null) {
			b.append(" and m.content like ? ", "%" + content + "%");
		}

		if (owner != null) {
			b.append(" and j.owner=? ", owner);
		}

		if (status != null) {
			b.append(" and j.status=?", status);
		}

		if (name != null) {
			b.append(" and j.name=?", "%" + name + "%");
		}

		if (unassigned) {
			b.append(" and j.owner=0");
		}

		if (jobsheet != null) {
			b.append(" and j.jobsheet=?", jobsheet);
		}

		if (sortType != null) {

			switch (sortType) {

			case Jobsheet:
				b.append(" order by j.jobsheet ");
				break;

			case Title:
				b.append(" order by j.title ");
				break;
			}

		}

		if (count) {
			return b.toQuery().getInt();

		} else {
			return b.toQuery().execute(Job.class, start, limit);
		}

	}

	public final void setAuthor(String author) {
		this.author = author;
	}

	public final void setContent(String content) {
		this.content = content;
	}

	public void setJobsheet(Jobsheet jobsheet) {
		this.jobsheet = jobsheet;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public void setSort(SortType sortType) {
		this.sortType = sortType;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public void setName(String title) {
		this.name = title;
	}

	public void setUnassigned() {
		this.unassigned = true;
	}

	public int size() {
		return (Integer) run(true);
	}
}