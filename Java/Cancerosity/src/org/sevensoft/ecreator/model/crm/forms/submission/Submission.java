package org.sevensoft.ecreator.model.crm.forms.submission;

import java.io.IOException;
import java.io.File;
import java.lang.reflect.Member;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.AttachmentFilter;
import org.sevensoft.ecreator.model.attachments.AttachmentOwner;
import org.sevensoft.ecreator.model.attachments.AttachmentUtil;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.FormField;
import org.sevensoft.ecreator.model.crm.forms.FormLine;
import org.sevensoft.ecreator.model.crm.forms.FormLineOption;
import org.sevensoft.ecreator.model.crm.forms.SubmissionOwner;
import org.sevensoft.ecreator.model.crm.messages.MessageSupport;
import org.sevensoft.ecreator.model.crm.messages.Msg;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.marketing.sms.SmsNumberException;
import org.sevensoft.ecreator.model.marketing.sms.SmsSenderException;
import org.sevensoft.ecreator.model.marketing.sms.SmsSettings;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsCreditsException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayAccountException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayMessagingException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsMessageException;
import org.sevensoft.ecreator.model.misc.Page;
import org.sevensoft.ecreator.model.misc.geoip.GeoIp;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.iface.admin.crm.forms.SubmissionHandler;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * 
 * A submission of a form. Can be attached to various objects or standalone for simple form submission
 * 
 * @author sks 4 Aug 2006 09:02:38
 *
 */
@Table("forms_submissions")
public class Submission extends MessageSupport implements AttachmentOwner, Logging {

	public static List<Submission> get(RequestContext context) {
		Query q = new Query(context, "select * from # order by id desc");
		q.setTable(Submission.class);
		return q.execute(Submission.class);
	}

    public static List<Submission> getByCategory(RequestContext context, Category category, int limit) {
        Query q = new Query(context, "select * from # where category=? order by id desc");
        q.setTable(Submission.class);
        q.setParameter(category);
        return q.execute(Submission.class, limit);
    }

    /**
	 * 
	 */
	private String	subject;

	/**
	 * 
	 */
	private DateTime	date;

	private Category	category;

	/**
	 * 
	 */
	private String	status;

	/**
	 * This was the item the form was submitted on
	 */
	private Item	item;

	/**
	 * This is the account who SUBMITTED the form
	 */
	private Item	account;

	private int		attachmentCount;

	private String	ipAddress;

	/**
	 * Submission country
	 */
	private Country	country;

	private Basket	basket;

	private Form	form;

	private Order	order;

	/**
	 * Name of form
	 */
	private String	name;

	private String	pageName;

	private Submission(RequestContext context) {
		super(context);
	}

	public boolean hasCountry() {
		return country != null;
	}

	public Submission(RequestContext context, Form form, SubmissionOwner owner, Map<FormField, String> data, String ipAddress, Page page) {
		super(context);

		this.status = "New";

		if (owner instanceof Order) {
			this.order = (Order) owner;
		} else if (owner instanceof Basket) {
			this.basket = (Basket) owner;
		}

		this.form = form;
		this.subject = form.getName();
		this.name = form.getName();

		this.date = new DateTime();
		this.ipAddress = ipAddress;
		this.country = GeoIp.lookup(context, ipAddress);

		if (page instanceof Item) {

			this.item = (Item) page;
			this.pageName = page.getName();
		}

		if (page instanceof Category) {

			this.category = (Category) page;
			this.pageName = page.getName();
		}

		save();

		for (Map.Entry<FormField, String> entry : data.entrySet()) {

			FormField field = entry.getKey();
			String value = entry.getValue();

			new SubmissionData(context, this, field.getName(), value);
		}

		for (FormLine line : form.getLines(context.getSessionId())) {
			addLine(line);
		}
	}

	public boolean acceptedFiletype(String filename) {
		return new AttachmentFilter().accept(filename);
	}

	public Attachment addAttachment(Attachment attachment) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException {
		return new Attachment(context, this, attachment);
	}

    public Attachment addAttachment(File file, String fileName) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException {
        return null;
    }

    public Attachment addAttachment(Upload upload, boolean sample) throws IOException, AttachmentLimitException, AttachmentExistsException,
			AttachmentLimitException, AttachmentTypeException {
		return new Attachment(context, this, upload, false);
	}

	public Attachment addAttachment(URL url) throws IOException, AttachmentTypeException, AttachmentExistsException, AttachmentLimitException {
		return new Attachment(context, this, url, false);
	}

	public final void addAttachments(Collection<Upload> uploads) throws IOException, AttachmentLimitException, AttachmentExistsException,
			AttachmentTypeException {
		for (Upload upload : uploads)
			addAttachment(upload, false);
	}

	public SubmissionLine addLine(FormLine formLine) {

		Item item = formLine.getItem();

		SubmissionLine line = addLine(item);
		line.setQty(formLine.getQty());

		for (FormLineOption option : formLine.getOptions()) {
			line.addOption(option.getItemOption(), option.getValue());
		}

		line.save();
		return line;
	}

	public SubmissionLine addLine(Item item) {
		return new SubmissionLine(context, this, item);
	}

	public Msg addMessage(String author, String email, String body) {
		return new Msg(context, this, author, email, body);
	}

	public Msg addMessage(User user, String body) {
		return new Msg(context, this, user, body);
	}

	public void copyAttachmentsTo(AttachmentOwner target) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException {
		AttachmentUtil.copyAttachmentsTo(target, this, context);
	}

	@Override
	public synchronized boolean delete() {
		removeMessages();
		removeData();
		return super.delete();
	}

	public void emailSubmission(List<String> recipients) throws EmailAddressException, SmtpServerException {

		logger.fine("[Submission] Emailing submission to recipients=" + recipients);

		Email msg = new Email();
		msg.setBody(getMessageBody());
		msg.setSubject("Form submitted: " + getName());
		msg.setRecipients(recipients);
		final Config config = Config.getInstance(context);
		msg.setFrom(config.getServerEmail());
        if (getAttachmentCount() != 0) {
            for (Attachment attachment : getAttachments()) {
                msg.addAttachment(attachment.getFile());
            }
        }
        new EmailDecorator(context, msg).send(config.getSmtpHostname());
	}

	public void emailSubmission(String email) throws EmailAddressException, SmtpServerException {
		emailSubmission(Collections.singletonList(email));
	}

	public int getAttachmentCount() {
		return attachmentCount;
	}

	public int getAttachmentLimit() {
		return 0;
	}

	public List<Attachment> getAttachments() {
		return AttachmentUtil.getAttachments(context, this);
	}

	public final Category getCategory() {
		return (Category) (category == null ? null : category.pop());
	}

	public Member getCreator() {
		return (Member) (account == null ? null : account.pop());
	}

	public List<SubmissionData> getData() {
		return SimpleQuery.execute(context, SubmissionData.class, "submission", this);
	}

	public DateTime getDate() {
		return date;
	}

	public final String getIpAddress() {
		return ipAddress;
	}

	public final Item getItem() {
		return (Item) (item == null ? null : item.pop());
	}

	public List<SubmissionLine> getLines() {
		return SimpleQuery.execute(context, SubmissionLine.class, "submission", this);
	}

	private String getMessageBody() {

		/*
		 * Add in the form details as the initial message
		 */
		StringBuilder sb = new StringBuilder();
        sb.append(form.getItemListersMailHeader());

		sb.append("Form submitted: " + name + "\n");
        if (form.isIncludeSubmId())
            sb.append("Submission ID: " + getId() + "\n");
        sb.append("Time: " + date.toString("dd-MMM-yyyy HH:mm") + "\n");

		if (pageName != null) {

			sb.append("Page: " + pageName + "\n");
			if (item != null) {
				sb.append(getItem().getUrl() + "\n");
			}
			if (category != null) {
                sb.append(Config.getInstance(context).getUrl() + "/" + new LinkTag(SubmissionHandler.class, "view", getId(), "submission", this).getUrl() + "\n");
			}
		}

		sb.append("\n");

		for (SubmissionData data : getData()) {

			sb.append(data.getName() + ": " + data.getValue());
			sb.append("\n");
		}

        sb.append(form.getItemListersMailFooter());

		return sb.toString();
	}

	public String getMessageSubject() {
		return subject;
	}

	public String getMessageSubjectPrefix() {
		return "sub";
	}

	public final String getName() {
		return name;
	}

	public String getPageName() {
		return pageName == null ? "" : pageName;
	}

	private String getSmsBody() {

		/*
		 * Add in the form details as the initial message
		 */
		StringBuilder sb = new StringBuilder();

		sb.append(name + "\r\n");

		if (item != null) {
			sb.append("Page: " + item.getIdString() + "\r\n");
		} else if (pageName != null) {
            sb.append("Page: " + pageName + "\r\n");
        }

		for (SubmissionData data : getData()) {

			sb.append(data.getName() + ": " + data.getValue());
			sb.append("\r\n");
		}

		return sb.toString();
	}

	public String getStatus() {
		return status;
	}

	public String getSubject() {
		return subject;
	}

	public boolean hasAttachments() {
		return attachmentCount > 0;
	}

	public boolean hasCategory() {
		return category != null;
	}

	public boolean hasItem() {
		return item != null;
	}

	public boolean hasMember() {
		return account != null;
	}

	public boolean hasName() {
		return name != null;
	}

	public boolean hasPageName() {
		return pageName != null;
	}

	public boolean isAtAttachmentLimit() {
		return false;
	}

	public void moveAttachmentsTo(AttachmentOwner target) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException {
		AttachmentUtil.moveAttachmentsTo(context, this, target);
	}

	public void removeAttachment(Attachment a) {
		AttachmentUtil.removeAttachment(this, a);
	}

	public void removeAttachments() {
		AttachmentUtil.removeAttachments(this);
	}

	private void removeData() {
		SimpleQuery.delete(context, SubmissionData.class, "submission", this);
	}

	public void setAccount(Item acc) {
		this.account = acc;
		save("account");
	}

	public void setAttachmentCount() {
		this.attachmentCount = getAttachments().size();
		save();
	}

	public final void setName(String name) {
		this.name = name;
	}

	public void setOrder(Order order) {
		this.order = order;
		save("order");
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void sms(List<String> smsNumbers) {

		SmsSettings smsSettings = SmsSettings.getInstance(context);

		String msg = getSmsBody();
		// restrict msg to 160
		if (msg.length() > 160) {
			msg = msg.substring(0, 160);
		}

		try {

			smsSettings.sendMessage(msg, smsNumbers);

		} catch (SmsGatewayException e) {
			e.printStackTrace();
		} catch (SmsMessageException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SmsGatewayAccountException e) {
			e.printStackTrace();
		} catch (SmsGatewayMessagingException e) {
			e.printStackTrace();
		} catch (SmsCreditsException e) {
			e.printStackTrace();
		} catch (SmsNumberException e) {
			e.printStackTrace();
		} catch (SmsSenderException e) {
			e.printStackTrace();
		}

	}

	public final Country getCountry() {
		return country;
	}

	public List<LogEntry> getLogEntries() {
		return null;
	}

	public String getLogId() {
		return getFullId();
	}

	public String getLogName() {
		return "Submission #" + getId();
	}

	public void log(String message) {
		new LogEntry(context, this, null, message);
	}

	public void log(User user, String message) {
		new LogEntry(context, this, user, message);
	}

    public void removeBasket() {
        this.basket = null;
    }
}
