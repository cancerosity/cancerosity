package org.sevensoft.ecreator.model.crm.forms.submission;

import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Nov 2006 17:41:33
 *
 */
@Table("forms_submissions_lines_options")
public class SubmissionLineOption extends EntityObject {

	private SubmissionLine	line;
	private String		name;
	private String		value;

	private SubmissionLineOption(RequestContext context) {
		super(context);
	}

	public SubmissionLineOption(RequestContext context, SubmissionLine line, ItemOption itemOption, String value) {
		super(context);

		this.line = line;
		this.name = itemOption.getName();
		this.value = value;

		save();
	}

	public SubmissionLine getLine() {
		return line.pop();
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

}
