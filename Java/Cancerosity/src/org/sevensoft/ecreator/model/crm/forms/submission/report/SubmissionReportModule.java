package org.sevensoft.ecreator.model.crm.forms.submission.report;

import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.*;

/**
 * User: Tanya
 * Date: 03.11.2011
 */
@Table("forms_submissions_report_module")
@Singleton
public class SubmissionReportModule extends EntityObject {

    private List<String> emails;
    private String reportTitle;
    private SortedSet<Integer> forms;


    public SubmissionReportModule(RequestContext context) {
        super(context);
    }

    public static SubmissionReportModule getInstance(RequestContext context) {
        return getSingleton(context, SubmissionReportModule.class);
    }

    public List<String> getEmails() {
        if (emails == null) {
            emails = new ArrayList();

        }
        return emails;
    }

    public final void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public String getReportTitle() {
        return reportTitle;
    }

    public void setReportTitle(String reportTitle) {
        this.reportTitle = reportTitle;
    }

    public Set<Integer> getForms() {
        if (forms == null)
            return Collections.emptySet();
        return forms;
    }

    public void setForms(Collection<Integer> c) {
        this.forms = new TreeSet<Integer>();
        this.forms.addAll(c);
    }

}
