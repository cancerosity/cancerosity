package org.sevensoft.ecreator.model.crm.forum.box;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 2 Aug 2006 11:28:23
 *
 */
@Path("admin-forum-searchbox.do")
public class ForumSearchBoxHandler extends BoxHandler {

	public ForumSearchBoxHandler(RequestContext context) {
		super(context);
	}

	private ForumSearchBox	box;

	@Override
	protected void specifics(StringBuilder sb) {
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {
	}

}
