package org.sevensoft.ecreator.model.crm.forms.panels;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.model.crm.captcha.Captcha;
import org.sevensoft.ecreator.model.crm.captcha.CaptchaPanel;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.FormField;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.util.Grid;

/**
 * @author sks 31 Jan 2007 09:10:24
 *
 */
public class FormPanel extends Panel {

	private static final Logger	logger	= Logger.getLogger("ecreator");

	private Form			form;
	private List<FormField>		fields;
	private List<String>		sections;

	public FormPanel(RequestContext context, Form form) {
		super(context);

		this.form = form;
		this.fields = form.getFields();

		logger.fine("[FormPanel] fields=" + fields);

		// get sections
		sections = Grid.getSections(fields);

		logger.fine("[FormPanel] sections=" + sections);
	}

	@Override
	public void makeString() {

		for (String section : sections) {

			// get fields for this section
			List<FormField> cells = Grid.getCells(fields, section);

			logger.fine("[FormPanel] cells=" + cells);

			if (section == null) {
				section = "General details";
			}
			sb.append(new FormFieldSet(section));

			Grid grid = new Grid(cells.get(0).getGridColumns());
			grid.setTrIdFirst(form.getTrIdFirst());
            grid.setTrIdLast(form.getTrIdLast());

            grid.addCells(cells);

			sb.append(grid);
			sb.append("</fieldset>");

		}

		if (form.isCaptcha()) {

			try {
				Captcha c = new Captcha(context, context.getSessionId(), form);
				sb.append(new CaptchaPanel(context, c, "Enter the digits shown below to help us combat spam"));
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		sb.append("<div class='ec_form_commands'>" + new SubmitTag(form.getSubmitButtonText()).setClass("submit") + "</div>");
	}
}
