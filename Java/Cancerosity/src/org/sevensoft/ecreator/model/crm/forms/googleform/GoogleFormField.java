package org.sevensoft.ecreator.model.crm.forms.googleform;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.crm.forms.FormField;

/**
 * User: Tanya
 * Date: 14.11.2011
 */
@Table("forms_fields_google")
public class GoogleFormField extends EntityObject {

    private GoogleForm googleForm;
    private FormField formField;
    private int googleFormColumn;

    public GoogleFormField(RequestContext context) {
        super(context);
    }

    public GoogleFormField(RequestContext context, GoogleForm googleForm, FormField formField) {
        super(context);
        this.googleForm = googleForm;
        this.formField = formField;
        save();
    }

    public GoogleForm getGoogleForm() {
        return googleForm == null ? null : (GoogleForm) googleForm.pop();
    }

    public void setGoogleForm(GoogleForm googleForm) {
        this.googleForm = googleForm;
    }

    public FormField getFormField() {
        return formField == null ? null : (FormField) formField.pop();
    }

    public void setFormField(FormField formField) {
        this.formField = formField;
    }

    public int getGoogleFormColumn() {
        return googleFormColumn;
    }

    public void setGoogleFormColumn(int googleFormColumn) {
        this.googleFormColumn = googleFormColumn;
    }

    public boolean hasGoogleFormColumn() {
        return googleFormColumn > 0;
    }
}
