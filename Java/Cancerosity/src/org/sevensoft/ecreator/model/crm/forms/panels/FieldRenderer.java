package org.sevensoft.ecreator.model.crm.forms.panels;

import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.misc.FieldInputCell;
import org.sevensoft.ecreator.model.crm.forms.FormField;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;

/**
 * @author sks 14 Mar 2007 06:57:58
 *
 */
public class FieldRenderer extends Panel {

	private FormField	field;

	public FieldRenderer(RequestContext context, FormField field) {
		super(context);
		this.field = field;
	}

	@Override
	public void makeString() {

		switch (field.getType()) {

		default:

			ErrorTag errorTag = new ErrorTag(context, field.getParamName(), "<br/>");
			final FieldInput fieldInput = new FieldInput(context, field);

			FieldInputCell cell = new FieldInputCell(field.getName(), field.getDescription(), fieldInput, errorTag);
			sb.append(cell);

            if (field.isRepeatEmail()) {
                ErrorTag errorTag1 = new ErrorTag(context, field.getRepeatParamName(), "<br/>");
                final FieldInput fieldInput1 = new FieldInput(context, field);

                fieldInput1.setFieldName(field.getRepeatParamName());

                FieldInputCell cell1 = new FieldInputCell("Repeat " + field.getName(), field.getDescription(), fieldInput1, errorTag1);
                sb.append(cell1);
            }

            break;

		case Description:

			sb.append("<div class='text'>" + HtmlHelper.nl2br(field.getName()) + "</div>");
			break;

		case Header:
			sb.append("<div class='text'><b>" + field.getName() + "</b></div>");
			break;

		case Separator:
			sb.append("<hr size='1'/>");
			break;

		}
	}

}
