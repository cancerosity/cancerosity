package org.sevensoft.ecreator.model.crm.forms.box;

import org.sevensoft.ecreator.iface.admin.crm.forms.boxes.FormBoxHandler;
import org.sevensoft.ecreator.iface.frontend.crm.form.FormHandler;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.FormField;
import org.sevensoft.ecreator.model.crm.captcha.Captcha;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.bool.RadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.util.Label;

import java.io.IOException;

/**
 * @author sks 10-Jan-2006 14:55:02
 *
 */
@Table("boxes_forms")
@Label("Form")
@HandlerClass(FormBoxHandler.class)
public class FormBox extends Box {

	private Form	form;

	protected FormBox(RequestContext context) {
		super(context);
	}

	public FormBox(RequestContext context, String location) {
		super(context, location);
	}

	@Override
	protected String getCssIdDefault() {
		return "form";
	}

	public Form getForm() {
		return (Form) (form == null ? null : form.pop());
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	public String render(RequestContext context) {

		if (!Module.Forms.enabled(context)) {
			return null;
		}

		if (form == null) {
			return null;
		}

		form.pop();

		Item account = (Item) context.getAttribute("account");
		Item item = (Item) context.getAttribute("item");
		Category category = (Category) context.getAttribute("category");

		/*
		 * Check for permission on form
		 */
		if (!PermissionType.ViewForm.check(context, account, getForm())) {
			return null;
		}

		StringBuilder sb = new StringBuilder();

		sb.append(getTableTag());
		sb.append("<tr><td>");

		sb.append(new FormTag(FormHandler.class, "submit", "post"));
		sb.append(new HiddenTag("form", form));
		sb.append(new HiddenTag("item", item));
		sb.append(new HiddenTag("category", category));

		for (FormField field : form.getFields()) {

			String fieldName = field.getParamName();
			ErrorTag errorTag = new ErrorTag(context, fieldName, "<br/>");

			switch (field.getType()) {

			default:
				break;

			case Attachment:
				sb.append("<div class='label'>" + field.getName() + "</div>");
				sb.append("<div class='input'>" + new FileTag("upload") + "</div>");
				break;

			case TickBox:
				sb.append("<div class='input'>");
				sb.append(new CheckTag(context, fieldName, "true", false));
				sb.append(field.getName());
				sb.append("</div>");
				break;

			case Radio:
				sb.append("<div class='label'>" + field.getName() + "</div>");

				sb.append("<div class='input'>");
				for (String option : field.getOptions()) {
					sb.append(option);
					sb.append(" ");
					sb.append(new RadioTag(context, fieldName, option));
				}

				sb.append(errorTag);
				sb.append("</div>");
				break;

			case Text:

				sb.append("<div class='label'>" + field.getName() + "</div>");

				sb.append("<div class='input'>");
				if (field.getHeight() == 1) {
					sb.append(new TextTag(context, fieldName, field.getWidth()));
				} else {
					sb.append(new TextAreaTag(context, fieldName, field.getWidth(), field.getHeight()));
				}
				sb.append(errorTag);
				sb.append("</div>");

                if (field.isRepeatEmail()) {
                    String repeatFieldName = field.getRepeatParamName();
                    sb.append("<div class='label'>Repeat " + field.getName() + "</div>");

                    sb.append("<div class='input'>");
                    if (field.getHeight() == 1) {
                        sb.append(new TextTag(context, repeatFieldName, field.getWidth()));
                    } else {
                        sb.append(new TextAreaTag(context, repeatFieldName, field.getWidth(), field.getHeight()));
                    }

                    ErrorTag errorTag1 = new ErrorTag(context, repeatFieldName, "<br/>");

                    sb.append(errorTag1);
                    sb.append("</div>");
                }

				break;

			case DropDownMenu:

				sb.append("<div class='label'>" + field.getName() + "</div>");

				SelectTag tag = new SelectTag(context, fieldName);
				tag.addOptions(field.getOptions());

				sb.append("<div class='input'>");
				sb.append(tag);
				sb.append(errorTag);
				sb.append("</div>");

				break;
			}

		}

        if (form.isCaptcha()) {

            try {
                Captcha captcha = new Captcha(context, context.getSessionId(), form);
                String text = "Enter the digits shown below to help us combat spam";

                sb.append(new HiddenTag("captcha", captcha));

                sb.append("<div class='label'>" + text + "</div>");
                sb.append("<div class='input'>" + new ImageTag(Config.CaptchaPath + "/" + captcha.getFile().getName()) + "</div>");

                sb.append("<div class='label'>Enter code as seen in image</div>");
                sb.append("<div class='input'>" + new TextTag(context, "captchaResponse"));
                sb.append(new ErrorTag(context, "captchaResponse", " "));
                sb.append("</div>");

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

		sb.append("<div class='submit'>" + new SubmitTag(form.getSubmitButtonText()) + "</div>");

		sb.append("</form>");

		sb.append("</td></tr>");
		sb.append("<tr><td class='bottom'></td></tr>");
		sb.append("</table>");

		return sb.toString();
	}

	@Override
	public void setObjId(int objId) {
		this.form = EntityObject.getInstance(context, Form.class, objId);
		this.name = "Form: " + form.getName();
		save();
	}
}
