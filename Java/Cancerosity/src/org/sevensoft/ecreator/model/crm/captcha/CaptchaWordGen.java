package org.sevensoft.ecreator.model.crm.captcha;

import java.util.Locale;

import org.sevensoft.commons.superstrings.RandomHelper;

import com.octo.captcha.component.word.wordgenerator.WordGenerator;

/**
 * @author sks 4 Apr 2007 17:50:11
 *
 */
final class CaptchaWordGen implements WordGenerator {

	private String	word;

	public String getLastWord() {
		return word;
	}

	public String getWord(Integer arg0) {
		word = RandomHelper.getRandomDigits(arg0);
		return word;
	}

	public String getWord(Integer arg0, Locale arg1) {
		word = RandomHelper.getRandomDigits(arg0);
		return word;
	}
}