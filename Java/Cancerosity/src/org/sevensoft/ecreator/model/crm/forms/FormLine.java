package org.sevensoft.ecreator.model.crm.forms;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.attachments.AttachmentSupport;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 23 Nov 2006 17:54:28
 *
 */
@Table("forms_lines")
public class FormLine extends AttachmentSupport implements Positionable {

	@Index
	private Item	item;

	/**
	 * The form this item is attached to
	 */
	@Index
	private Form	form;

	private int		qty;

	@Index
	private String	sessionId;

	/**
	 * Date this line was added
	 */
	private Date	date;

	private int		position;

	/**
	 * String containing details of the options on this order
	 */
	private String	options;

	protected FormLine(RequestContext context) {
		super(context);
	}

	FormLine(RequestContext context, Form form, Item item) {
		super(context);
		this.form = form;

		this.date = new Date();

		this.sessionId = context.getSessionId();
		this.item = item;

		this.qty = 1;

		save();
	}

	public int getAttachmentLimit() {
		return 0;
	}

	public Date getDate() {
		return date;
	}

	public Form getForm() {
		return form.pop();
	}

	public Item getItem() {
		return item.pop();
	}

	public List<FormLineOption> getOptions() {
		return SimpleQuery.execute(context, FormLineOption.class, "line", this);
	}

	public int getPosition() {
		return position;
	}

	public int getQty() {
		return qty;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void addOptions(Map<ItemOption, String> options) {

		Set<Entry<ItemOption, String>> entrySet = options.entrySet();
		for (Map.Entry<ItemOption, String> entry : entrySet) {
			new FormLineOption(context, this, entry.getKey(), entry.getValue());
		}
	}

	public void setOptions(String options) {
		this.options = options;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

}
