package org.sevensoft.ecreator.model.crm.forms.submission;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Sep 2006 06:48:41
 * 
 * Data for the general form fields
 *
 */
@Table("forms_submissions_data")
public class SubmissionData extends EntityObject {

	private Submission	submission;
	private String		value;
	private String		name;

	public SubmissionData(RequestContext context) {
		super(context);
	}

	public SubmissionData(RequestContext context, Submission submission, String name, String value) {
		super(context);

		this.submission = submission;
		this.name = name;
		this.value = value;

		save();
	}

	public String getName() {
		return name;
	}

	public Submission getSubmission() {
		return submission.pop();
	}

	public String getValue() {
		return value;
	}

}
