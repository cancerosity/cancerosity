package org.sevensoft.ecreator.model.crm.forms.markers.submissions;

import java.util.Map;

import org.sevensoft.ecreator.iface.admin.reports.submissions.SubmissionsHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 20 Sep 2006 06:40:36
 *
 * A link to the my submissions page
 */
public class SubmissionsMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		return super.link(context, params, new Link(SubmissionsHandler.class), "account_link");
	}

	public Object getRegex() {
		return "submissions";
	}

}
