package org.sevensoft.ecreator.model.crm.mailboxes;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;

import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.crm.jobsheets.Jobsheet;
import org.sevensoft.ecreator.model.crm.messages.MessageOwner;
import org.sevensoft.ecreator.model.crm.messages.MessageUtil;
import org.sevensoft.ecreator.model.crm.messages.Msg;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 04-Mar-2006 13:14:46
 *
 */
@Table("mailboxes")
public class Mailbox extends EntityObject {

	public static List<Mailbox> get(RequestContext context) {
		return SimpleQuery.execute(context, Mailbox.class);
	}

	public static Mailbox getDefault(RequestContext context) {
		return SimpleQuery.get(context, Mailbox.class);
	}

	private String	hostname, username, password;

	private User	owner;

	private String	autoResponse;

	private boolean	enabled;

	private String	autoResponderFrom;

	/**
	 * If not null, then we will create any new messages as a job in this jobsheet
	 */
	private Jobsheet	createInJobsheet;

	public Mailbox(RequestContext context) {
		super(context);
	}

	public Mailbox(RequestContext context, String hostname) {
		super(context);
		this.hostname = hostname;
		save();
	}

	/**
	 * @param contentType
	 * @return
	 */
	private Map<String, String> getAttributes(String contentType) {

		Map<String, String> attributes = new HashMap<String, String>();
		for (String attribute : contentType.split(";")) {

			String[] s = attribute.split("=");
			if (s.length == 1)
				attributes.put("contentType", s[0]);
			else if (s.length == 2)
				attributes.put(s[0].trim(), s[1].trim());
		}

		return attributes;
	}

	private String getAutoResponderFrom() {
		if (autoResponderFrom == null)
			return Config.getInstance(context).getServerEmail();
		else
			return autoResponderFrom;
	}

	public final String getAutoResponse() {
		return autoResponse;
	}

	public final Jobsheet getCreateInJobsheet() {
		return (Jobsheet) (createInJobsheet == null ? null : createInJobsheet.pop());
	}

	public String getHostname() {
		return hostname;
	}

	public final User getOwner() {
		return (User) (owner == null ? null : owner.pop());
	}

	public String getOwnerName() {
		return owner == null ? "None" : getOwner().getName();
	}

	public String getPassword() {
		return password;
	}

	public String getUsername() {
		return username;
	}

	public boolean hasAutoResponse() {
		return autoResponse != null;
	}

	public boolean hasOwner() {
		return owner != null;
	}

	/**
	 * 
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Processes this mailbox for new messages and returns thr number of emails picked up
	 * 
	 */
	int process() throws MessagingException, IOException {

		logger.fine("[Mailbox] Hostname: " + hostname);
		logger.fine("[Mailbox] username: " + username);
		logger.fine("[Mailbox] password: " + password);

		Folder folder = null;
		Store store = null;

		// Get session
		Session session = Session.getDefaultInstance(new Properties(), null);

		// Get the store
		store = session.getStore("pop3");
		store.connect(hostname, username, password);
		logger.fine("[Mailbox] Connecting to store");

		// Get folder
		folder = store.getFolder("INBOX");
		logger.fine("[Mailbox] Getting folder: INBOX");

		int n = 0;
		if (folder != null) {

			try {

				folder.open(Folder.READ_WRITE);
				logger.fine("[Mailbox] Opening folder");

				final Message[] messages = folder.getMessages();

				logger.fine("[Mailbox] Folder contains " + messages.length + " messages");

				for (javax.mail.Message message : messages) {

					processMessage(message);
					n++;
				}

			} finally {

				folder.close(true);
				logger.fine("[Mailbox] Closing folder and deleting read messages");

				store.close();
				logger.fine("[Mailbox] Closing store");

			}
		}

		return n;
	}

	private void processAttachment(Object content, Msg msg, String filename) {

		logger.fine("[Mailbox] Processing attachment");

		/*
		 * Check for invalid attachment filetype
		 */
		if (filename.endsWith(".exe") || filename.endsWith(".sh") || filename.endsWith(".bat")) {

			logger.warning("[Mailbox] Rejecting enote attachment: " + filename);

			return;
		}

		/*
		 * Strip out the surrounding quotes that tend to be on most filenames on attachments sent from most clients
		 */
		filename = removeQuotes(filename);

		logger.fine("[Mailbox] Using attachment filename: " + filename);

		final InputStream in;

		if (content instanceof InputStream) {

			in = (InputStream) content;

		} else if (content instanceof String) {

			in = new ByteArrayInputStream(content.toString().getBytes());

		} else {

			logger.fine("[Mailbox] Unsupported content class for attachments: " + content.getClass().getName());

			return;
		}

		try {

			msg.addAttachment(in, filename);
			logger.fine("[Mailbox] Attachment created: " + filename);

		} catch (AttachmentExistsException e) {

			e.printStackTrace();
			logger.warning(e.toString());

		} catch (AttachmentTypeException e) {

			e.printStackTrace();
			logger.fine(e.toString());

		} catch (IOException e) {
			e.printStackTrace();
			logger.fine(e.toString());

		} catch (AttachmentLimitException e) {
			e.printStackTrace();
		}
	}

	private void processBody(Object content, String contentType, Msg msg) throws IOException {

		logger.fine("[Mailbox] Processing body");

		final String body;

		/*
		 * Get content first
		 */
		if (content instanceof String) {

			logger.fine("[Mailbox] String detected");

			body = (String) content;

		} else if (content instanceof InputStream) {

			logger.fine("[Mailbox] InputStream detected");

			body = SimpleFile.readString((InputStream) content);

		} else {

			logger.fine("[Mailbox] Unsupported content class for attachments: " + content.getClass().getName());

			return;

		}

		/*
		 * We have body content now stored in body ref. Check for content type now, if html we don't want
		 * to override text if its already there.
		 */

		if ("text/html".equals(contentType)) {

			/*
			 * For html content only show this as the body if we do not have a body already
			 */
			if (!msg.hasBody()) {

				String stripped = HtmlHelper.stripHtml(body, " ");

				msg.setBody(stripped);
				msg.save();
			}

		} else if ("text/plain".equals(contentType)) {

			msg.setBody(body);
			msg.save();

		} else {

			logger.fine("[Mailbox] Unsupported content type when setting body: " + contentType);

			return;

		}
	}

	private void processMessage(Message message) throws MessagingException, IOException {

		final Config config = Config.getInstance(context);

		logger.fine("[Mailbox] ----Processing message: " + message + "----");

		try {

			InternetAddress[] sender = (InternetAddress[]) message.getFrom();
			logger.fine("[Mailbox] sender=" + sender);

			if (sender != null && sender.length > 0) {

				String senderEmail = sender[0].getAddress();
				String senderName = sender[0].getPersonal();
				String subject = message.getSubject();

				logger.fine("[Mailbox] senderEmail=" + senderEmail);
				logger.fine("[Mailbox] senderName=" + senderName);
				logger.fine("[Mailbox] subject=" + subject);

				if (subject == null) {
					subject = "-No subject-";
				}

				if (senderEmail == null || senderEmail.length() < 8) {
					logger.fine("invalid sender email");
					throw new MessagingException("Invalid sender email");
				}

				senderEmail = senderEmail.trim().toLowerCase();

				if (senderName == null || senderName.length() == 0) {
					senderName = senderEmail;
				} else {
					senderName = senderName.trim() + " <" + senderEmail + ">";
				}

				/* 
				 * try to get existing message owner  from modified subject
				 * othewise create new job in jobsheet and set the job as the owner
				 */
				MessageOwner owner;

				Msg leadMsg = MessageUtil.getFromSubject(subject, context);
				if (leadMsg == null) {

					logger.fine("[Mailbox] msg not found");

					if (createInJobsheet != null) {

						owner = getCreateInJobsheet().addJob(null, subject);
						logger.fine("[Mailbox] created job=" + owner);

					} else {

						logger.fine("[Mailbox] no create jobsheet set, so skipping new msg");
						owner = null;
					}

				} else {

					owner = leadMsg.getOwner();
					logger.fine("[Mailbox] owner retrieved=" + owner);

				}

				/*
				 * add new message if owner was found
				 */
				if (owner != null) {

					// obviously this messag should be public
					Msg msg = owner.addMessage(senderName, senderEmail, null);

					try {
						processPart(message, msg);
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
						msg.setBody("Error - email content could not be read\n\n" + e.getMessage());
					}

					if (hasAutoResponse()) {

						logger.fine("[Mailbox] Sending auto response");

						Email e = new Email();

						e.setFrom(getAutoResponderFrom());
						e.setSubject(msg.getModifiedSubject());
						e.setBody(getAutoResponse());
						e.setTo(senderEmail);

						try {
							new EmailDecorator(context, e).send(config.getSmtpHostname());

						} catch (EmailAddressException e1) {

							e1.printStackTrace();
							logger.fine("[Mailbox] Could not send auto response: " + e1.getMessage());

						} catch (SmtpServerException e1) {

							e1.printStackTrace();
							logger.fine("[Mailbox] Could not send auto response: " + e1.getMessage());
						}

					}
				}
			}

		} finally {

			message.setFlag(Flags.Flag.DELETED, true);

			logger.fine("[Mailbox] ----end of message----");
		}
	}

	private void processPart(Part part, Msg msg) throws MessagingException, IOException {

		logger.fine("[Mailbox] Processing part: " + part);

		String contentType = part.getContentType();
		logger.fine("[Mailbox] Content type: " + contentType);

		if (contentType == null) {
			return;
		}

		Object content = part.getContent();
		if (content == null) {

			logger.fine("[Mailbox] Content is null, skipping");
			return;
		}

		Map<String, String> attributes = getAttributes(contentType);
		contentType = attributes.get("contentType");

		logger.fine("[Mailbox] Content class: " + content.getClass().getName());

		/*
		 * If we have a multipart message then we need to loop back over each body part again
		 */
		if (content instanceof MimeMultipart) {

			logger.fine("[Mailbox] Processing MimeMultipart");

			for (int m = 0; m < ((MimeMultipart) content).getCount(); m++) {
				BodyPart bp = ((MimeMultipart) content).getBodyPart(m);
				processPart(bp, msg);
			}
			logger.fine("[Mailbox] End of multipart\n");

		} else {

			/*
			 * We check for filename - if present then this is an attachment
			 */

			String filename = attributes.get("name");
			if (filename == null)
				processBody(content, contentType, msg);
			else
				processAttachment(content, msg, filename);

		}
	}

	private String removeQuotes(String filename) {

		if (filename.startsWith("\""))
			filename = filename.substring(1);

		if (filename.endsWith("\""))
			filename = filename.substring(0, filename.length() - 1);

		return filename;
	}

	public void setAutoResponderFrom(String autoResponderFrom) {
		this.autoResponderFrom = autoResponderFrom;
	}

	public final void setAutoResponse(String autoResponse) {
		this.autoResponse = autoResponse;
	}

	public final void setCreateInJobsheet(Jobsheet createInJobsheet) {
		this.createInJobsheet = createInJobsheet;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "Mailbox " + super.toString() + " hostname=" + hostname + ", username=" + username;
	}

}
