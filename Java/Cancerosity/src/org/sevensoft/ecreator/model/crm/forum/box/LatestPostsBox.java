package org.sevensoft.ecreator.model.crm.forum.box;

import org.sevensoft.ecreator.iface.frontend.forum.PostSearchHandler;
import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.crm.forum.Forum;
import org.sevensoft.ecreator.model.crm.forum.Post;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 12-Feb-2006 09:21:34
 *
 */
@Label("Forum posts")
@Table("boxes_forum_posts_latest")
@HandlerClass(LatestPostsBoxHandler.class)
public class LatestPostsBox extends Box {

	/**
	 * The forum the posts should be taken from, or null if it is across the site.
	 */
	private Forum	forum;

	public LatestPostsBox(RequestContext context) {
		super(context);
	}

	public LatestPostsBox(RequestContext context, String location, boolean b) {
		super(context, location);
	}

	@Override
	protected String getCssIdDefault() {
		return "latest-posts";
	}

	public final Forum getForum() {
		return (Forum) (forum == null ? null : forum.pop());
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	public String render(RequestContext context) {

		if (!Module.Forum.enabled(context)) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		sb.append(getTableTag());

		for (Post message : Forum.getLatest(context, getForum(), 8)) {

			sb.append("<tr><td class='post'>");

			sb.append("<div class='name'>" + new LinkTag(ItemHandler.class, null, message.getTopic().getTitle(), "topic", message.getTopic()) + "</div>");
			sb.append("<div class='date'>" + message.getDate().toString("dd/MM/yyyy hh:mma") + "</div>");
			sb.append("<div class='content'>" + message.getContent(100) + "</div>");

			sb.append("</td></tr>");
		}

		sb.append("<tr><td class='bottom'>" + new LinkTag(PostSearchHandler.class, "latest", "Show more recent messages") + "</td></tr>");
		sb.append("</table>");

		return sb.toString();
	}

	public final void setForum(Forum forum) {
		this.forum = forum;
	}
}
