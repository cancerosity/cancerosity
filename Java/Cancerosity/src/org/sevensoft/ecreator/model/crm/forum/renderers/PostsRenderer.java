package org.sevensoft.ecreator.model.crm.forum.renderers;

import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.iface.EcreatorRenderer;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionHandler;
import org.sevensoft.ecreator.iface.frontend.forum.TopicHandler;
import org.sevensoft.ecreator.iface.frontend.interaction.pm.PmHandler;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.crm.forum.Post;
import org.sevensoft.ecreator.model.crm.forum.Topic;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 30-Jan-2006 14:32:53
 *
 */
public class PostsRenderer extends EcreatorRenderer {

	private Topic	topic;

	public PostsRenderer(RequestContext context, Topic topic, int page) {
		super(context);
		this.topic = topic;
	}

	@Override
	public String toString() {

		sb.append("<style>");
		sb.append("table.forum_post { width: 100%; font-family: Arial; font-size: 12px; color: black; margin-bottom: 10px; }");
		sb.append("table.forum_post td, table.item_messages th { padding: 3px; } ");
		sb.append("table.forum_post th.date { width: 200px; } ");
		sb.append("table.forum_post th { background: #e1e1e1; }");
		sb.append("table.forum_post td { background: #f5f5f5; }");
		sb.append("</style>");

		if (topic.hasPosts()) {

			for (Post post : topic.getPosts()) {

				sb.append(new TableTag("forum_post", 2, 0));
				sb.append("<tr>");
				sb.append("<th align='left'>");

				if (post.hasAccount()) {

					sb.append(new LinkTag(post.getAccount().getUrl(), post.getAuthor()));

				} else {

					sb.append(post.getAuthor());

				}

				//				if (member != null && member.equals(topic.getMember())) {
				//
				//					sb.append(" ");
				//					sb.append(new EmailTag(post.getMember().getEmail()));
				//				}

				sb.append("</th>");
				sb.append("<th align='right' class='date'>Posted: " + post.getDate().toString("HH:mm dd/MM/yyyy") + "</th>");
				sb.append("</tr>");

				sb.append("<tr><td colspan='2' class='content'>" + HtmlHelper.nl2br(post.getContent()) + "</td></tr>");
				sb.append("</table>");
			}

		}

		/*
		 * If logged in member is the owner then show member controls
		 */
		if (account != null && account.equals(topic.getCreator())) {

			sb.append(new TableTag("topic_posts"));
			sb.append("<tr><th>Topic owner controls</th></tr>");

			if (topic.isOpen()) {

				sb.append("<tr><td align='center'>" + new LinkTag(TopicHandler.class, "close", "Close this topic", "topic", topic) +
						" - members will not be able to add anymore replies.</td></tr>");

			} else {

				sb.append("<tr><td align='center'>" + new LinkTag(TopicHandler.class, "open", "Open this topic", "topic", topic) +
						" - members will be able to add new replies.</td></tr>");
			}

			sb.append("<tr><td align='center'>" + new LinkTag(PmHandler.class, "compose", "Send message", "topic", topic) +
					" - send a private message to all the members who have contributed to this topic.</td></tr>");

			sb.append("</table>");

		}
		
		Config config = Config.getInstance(context);

		/*
		 * If topic is still open then show add message options
		 */
		if (topic.isOpen()) {

			sb.append(new FormTag(TopicHandler.class, "addPost", "post"));
			sb.append(new HiddenTag("topic", topic));
			sb.append(new TableTag("topic_posts"));

			sb.append("<tr><th>Join the discussion - add your message!</th></tr>");
			sb.append("<tr><td align='center'>");

			if (account == null) {

				sb.append(new LinkTag(LoginHandler.class, null, "Login") + " to post a message.");

			} else if (config.getUrl().contains("overfiftiesfriends.co.uk") && !account.getSubscription().hasSubscriptionLevel()) {

				sb.append("Your membership does not allow you to perform this action. Please " +
						new LinkTag(SubscriptionHandler.class, null, "click here") + " to upgrade");

			} else if (PermissionType.PostReply.check(context, account, topic.getForum())) {

				sb.append(new TextAreaTag(context, "content", 50, 6) + "<br/>" + new SubmitTag("Add message"));

			} else {

				sb.append("Your membership does not allow you to perform this action. Please " +
						new LinkTag(SubscriptionHandler.class, null, "click here") + " to upgrade to Full membership");
			}

			sb.append("</td></tr>");
			sb.append("</table>");

			sb.append("</form>");

		} else {

			sb.append(new TableTag("topic_posts"));
			sb.append("<tr><th>Topic closed - no new posts can be made!</th></tr>");
			sb.append("</table>");

		}

		return sb.toString();
	}
}
