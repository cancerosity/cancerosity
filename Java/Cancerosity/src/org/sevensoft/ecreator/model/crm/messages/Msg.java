package org.sevensoft.ecreator.model.crm.messages;

import java.util.List;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.AttachmentSupport;
import org.sevensoft.ecreator.model.crm.forms.submission.Submission;
import org.sevensoft.ecreator.model.crm.jobsheets.Job;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 24-Oct-2005 20:55:18
 * 
 */
@Table("messages")
public class Msg extends AttachmentSupport {

	private Order		order;
	private DateTime		date;

	/**
	 * A string of the author, either email sender, or user name
	 */
	private String		author;

	/**
	 * The actual message 
	 */
	private String		body;

	/**
	 * The email address if this was received via email
	 */
	private String		email;

	/**
	 * The user who added this message if it was a message added via admin
	 */
	private User		user;

	/**
	 * Owner
	 */
	private Submission	submission;

	/**
	 * The owner item 
	 */
	private Item		item;

	/**
	 * Flags that this message has been emailed out and is no longer just internal.
	 */
	private boolean		isPublic;

	/**
	 *  Owner
	 */
	private Job			job;

	public Msg(RequestContext context) {
		super(context);
	}

	public Msg(RequestContext context, MessageOwner owner, String body) {
		super(context);

		if (owner instanceof Order) {
			this.order = (Order) owner;
		}

		else if (owner instanceof Submission) {
			this.submission = (Submission) owner;
		}

		else if (owner instanceof Job) {
			this.job = (Job) owner;
		}

		else if (owner instanceof Item) {
			this.item = (Item) owner;
		}

		else {
			throw new RuntimeException();
		}

		this.body = body;
		this.date = new DateTime();

	}

	public Msg(RequestContext context, MessageOwner owner, String author, String email, String body) {

		this(context, owner, body);
		this.author = author;
		this.email = email;

		/*
		 * This message has come from external so not much point it being internal !
		 */
		this.isPublic = true;

		save();

		owner.setLastMessageDate();
		owner.setLastMessageAuthor();
		owner.setMessageCount();
	}

	public Msg(RequestContext context, MessageOwner owner, User user, String body) {

		this(context, owner, body);
		this.user = user;
		this.author = user.getName();

		save();

		owner.setLastMessageDate();
		owner.setLastMessageAuthor();
		owner.setMessageCount();
	}

	public int getAttachmentLimit() {
		return 0;
	}

	public String getAuthor() {
		return author;
	}

	public String getBody() {
		return body;
	}

	public DateTime getDate() {
		return date;
	}

	public String getEmail() {
		return email;
	}

	public final Item getItem() {
		return (Item) (item == null ? null : item.pop());
	}

	public Job getJob() {
		return (Job) (job == null ? null : job.pop());
	}

	/**
	 * Returns the subject with the added message ID so the system knows which message owner to retrieve to assign the message to
	 * 
	 * We will always use the first message in the thread
	 */
	public String getModifiedSubject() {

		MessageOwner owner = getOwner();
		List<Msg> messages = owner.getMessages();

		Msg msg = messages.get(0);
		return "[#" + msg.getId() + "] " + owner.getMessageSubject();
	}

	public Order getOrder() {
		return (Order) (order == null ? null : order.pop());
	}

	public MessageOwner getOwner() {

		if (hasOrder()) {
			return getOrder();
		}

		if (hasJob()) {
			return getJob();
		}

		if (hasSubmission()) {
			return getSubmission();
		}

		return null;
	}

	public Submission getSubmission() {
		return (Submission) (submission == null ? null : submission.pop());
	}

	public User getUser() {
		return (User) (user == null ? null : user.pop());
	}

	public boolean hasBody() {
		return body != null;
	}

	public boolean hasEmail() {
		return email != null;
	}

	public boolean hasItem() {
		return item != null;
	}

	public boolean hasJob() {
		return job != null;
	}

	public boolean hasOrder() {
		return order != null;
	}

	public boolean hasSubmission() {
		return submission != null;
	}

	public boolean hasUser() {
		return user != null;
	}

	public boolean isPrivate() {
		return !isPublic();
	}

	public final boolean isPublic() {
		return isPublic;
	}

	private void sendEmail() throws EmailAddressException, SmtpServerException {

		if (!hasUser()) {
			return;
		}

		Config config = Config.getInstance(context);

		Email e = new Email();
		e.setSubject(getModifiedSubject());
		e.setBody(body);
		e.setFrom(getUser().getName(), config.getMailboxEmail());

		for (String email : getOwner().getMessageEmails()) {
			e.setTo(email);
		}

		for (Attachment a : getAttachments()) {
			e.addAttachment(a.getFile());
		}

		new EmailDecorator(context, e).send(Config.getInstance(context).getSmtpHostname());
	}

	public void setBody(String body) {
		this.body = body;
	}

	public final void setPublic(boolean pub) throws EmailAddressException, SmtpServerException {

		if (this.isPublic == pub) {
			return;
		}

		if (!hasUser()) {
			return;
		}

		this.isPublic = pub;
		save();

		sendEmail();
	}
}
