package org.sevensoft.ecreator.model.crm.forms.googleform;

import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.httpc.HttpMethod;
import org.sevensoft.commons.samdate.Date;

import java.io.IOException;

/**
 * User: Tanya
 * Date: 14.11.2011
 */
public class GoogleFormTest {

    public static void main(String[] arg) throws IOException {

        HttpClient hc = new HttpClient("https://spreadsheets.google.com/spreadsheet/formResponse", HttpMethod.Post);
        hc.setParameter("formkey", "dFJ2U1dXdk1DcWhnM0xWejBrRzNVQ3c6MQ");
        hc.setParameter("entry.0.single", "14/11/2011");
        hc.setParameter("entry.1.single", "http://localhost");

        // hc.setParameter("fldname", "smith");
        // hc.setParameter("fldemail", "hotmail.com");

        hc.setParameter("entry.2.single", "Model");
         hc.setParameter("entry.3.single", "tanya r");
         hc.setParameter("entry.7.single", "roshkovska@gmail.com");

        hc.connect();

        System.out.println(hc.getResponseString());

    }

}
