package org.sevensoft.ecreator.model.crm.captcha;

import com.octo.captcha.component.image.backgroundgenerator.BackgroundGenerator;
import com.octo.captcha.component.image.backgroundgenerator.GradientBackgroundGenerator;
import com.octo.captcha.component.image.fontgenerator.DeformedRandomFontGenerator;
import com.octo.captcha.component.image.fontgenerator.FontGenerator;
import com.octo.captcha.component.image.textpaster.RandomTextPaster;
import com.octo.captcha.component.image.textpaster.TextPaster;
import com.octo.captcha.component.image.wordtoimage.ComposedWordToImage;
import com.octo.captcha.component.image.wordtoimage.WordToImage;
import com.octo.captcha.engine.image.ListImageCaptchaEngine;
import com.octo.captcha.image.gimpy.GimpyFactory;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.samdate.Date;
import org.apache.commons.io.filefilter.AgeFileFilter;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.FileFilter;
import java.util.*;

/**
 * @author sks 4 Apr 2007 17:51:41
 */
@Table("captchas")
public class Captcha extends EntityObject {

    public static Captcha get(RequestContext context, Form form) {
        return SimpleQuery.get(context, Captcha.class, "sessionId", context.getSessionId(), "form", form);
    }

    public static java.util.List<Captcha> getList(RequestContext context, Form form) {
        return SimpleQuery.execute(context, Captcha.class, "sessionId", context.getSessionId(), "form", form);
    }

    private transient File file;

    private String word;
    private String sessionId;
    private Form form;
    private String formInst;  // in case of a form appears on 1 page 2 times

    private Captcha(RequestContext context) {
        super(context);
    }

    public Captcha(RequestContext context, String sessionId, Form form) throws IOException {
        super(context);
        this.sessionId = sessionId;
        this.form = form;
        this.formInst = form.toString();

        final CaptchaWordGen wordGenerator = new CaptchaWordGen();
        ListImageCaptchaEngine engine = new ListImageCaptchaEngine() {

            @Override
            protected void buildInitialFactories() {

                TextPaster textPaster = new RandomTextPaster(new Integer(6), new Integer(6), Color.white);

                BackgroundGenerator backgroundGenerator = new GradientBackgroundGenerator(280, 100, Color.WHITE, Color.black);
                FontGenerator fontGenerator = new DeformedRandomFontGenerator(30, 50);

                WordToImage wordToImage = new ComposedWordToImage(fontGenerator, backgroundGenerator, textPaster);

                addFactory(new GimpyFactory(wordGenerator, wordToImage));
            }
        };

        BufferedImage image = engine.getNextImageCaptcha().getImageChallenge();

        file = File.createTempFile("captcha", ".jpeg", ResourcesUtils.getRealCaptchasDir());
        file.deleteOnExit();

        ImageIO.write(image, "jpeg", file);

        word = wordGenerator.getLastWord();

        // wipe off previous for this session
        new Query(context, "delete from #  where sessionId=? and form=? and formInst!=?" )
                .setTable(Captcha.class)
                .setParameter(sessionId)
                .setParameter(form)
                .setParameter(formInst).run();

        // delete old captchas files
        FileFilter filter = new AgeFileFilter(new Date().previousWeek().getTimestamp(), true);
        File[] filesToDelete = ResourcesUtils.getRealCaptchasDir().listFiles(filter);
        java.util.List<File> files = Arrays.asList(filesToDelete);
        for (File delete : files) {
            delete.delete();
        }

        save();
    }

    public File getFile() {
        return file;
    }

    public String getWord() {
        return word;
    }

}
