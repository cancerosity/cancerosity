package org.sevensoft.ecreator.model.crm.forms.markers.submissions;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.crm.form.FormHandler;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 30 Jan 2007 16:25:40
 *
 */
public class FormSubmissionMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		Form form = EntityObject.getInstance(context, Form.class, params.get("id"));

		Link link = new Link(FormHandler.class, null, "form", form, "referenceItem", item);
		return super.link(context, params, link, "form_link");
	}

	@Override
	public String getDescription() {
		return "Makes a link through to a form with this item set as the source";
	}

	public Object getRegex() {
		return "form_submission";
	}

}
