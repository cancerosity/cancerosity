package org.sevensoft.ecreator.model.crm.forms.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.captcha.Captcha;
import org.sevensoft.ecreator.model.crm.captcha.CaptchaPanel;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;
import java.io.IOException;

/**
 * User: Tanya
 * Date: 13.06.2012
 */
public class CaptchaPanelMarker extends MarkerHelper implements IItemMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
        String formId = params.get("form");
        Form form = Form.getInstance(context, Form.class, formId);
        if (form == null)
            return null;

        try {
            Captcha c = new Captcha(context, context.getSessionId(), form);
            return new CaptchaPanel(context, c, "Enter the digits shown below to help us combat spam");
        } catch (IOException e) {
        }

        return null;
    }

    public Object getRegex() {
        return "captcha_panel";
    }
}
