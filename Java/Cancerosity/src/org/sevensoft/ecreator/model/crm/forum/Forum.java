package org.sevensoft.ecreator.model.crm.forum;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.accounts.permissions.Permissions;
import org.sevensoft.ecreator.model.crm.forum.box.LatestPostsBox;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

import java.util.List;

/**
 * @author sks 03-Apr-2006 22:04:21
 */
@Table("forums")
public class Forum extends EntityObject implements Selectable, Positionable, Permissions {

    /**
     * Returns all forums ordered by category and name
     */
    public static List<Forum> get(RequestContext context) {
        Query q = new Query(context, "select * from # order by position");
        q.setTable(Forum.class);
        return q.execute(Forum.class);
    }

    public static List<Post> getLatest(RequestContext context, Forum forum, int i) {

        if (forum == null) {

            Query q = new Query(context, "select * from # order by id desc");
            q.setTable(Post.class);
            return q.execute(Post.class, i);

        } else {

            Query q = new Query(context, "select p.* from # p join # t on p.topic=t.id where t.forum=? order by p.id desc");
            q.setTable(Post.class);
            q.setTable(Topic.class);
            q.setParameter(forum);
            return q.execute(Post.class, i);

        }
    }

    private int position;

    /**
     * Stop members from putting email addresses in posts.
     */
    private boolean blockEmails;

    /**
     * Name of this forum
     */
    private String name;

    /**
     *
     */
    private String lastPostTopicTitle;

    /**
     *
     */
    private String category;

    /**
     *
     */
    private String lastPostAuthor;

    /**
     * Last time a post was made
     */
    private DateTime lastPostDate;

    /**
     *
     */
    private int postCount;

    /**
     * No posts can be made in here if the forum is locked
     */
    private boolean locked;

    /**
     *
     */
    private int topicCount;

    private String description;

    private int views;

    /**
     * Enable access control
     */
    private boolean permissions;

    private String restrictionForwardUrl;

    public Forum(RequestContext context) {
        super(context);
    }

    public Forum(RequestContext context, String name) {
        super(context);
        this.name = name;

        save();
    }

    public Topic addTopic(Item item) {

        Topic topic = new Topic(context, item);
        setTopicCount();

        return topic;
    }

    public Topic addTopic(String title, Item poster, String body) {

        Topic topic = new Topic(context, this, title, poster);
        setTopicCount();

        topic.addPost(poster, body);
        setLastPost();

        return topic;
    }

    @Override
    public synchronized boolean delete() {

        // remove this forum as the one where item topics go
        new Query(context, "update # set forum=0 where forum=?").setParameter(this).setTable(ItemType.class).run();

        // Remove box
        new Query(context, "delete from # where forum=?").setTable(LatestPostsBox.class).setParameter(this).run();

        return super.delete();
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    public String getLabel() {
        return getName();
    }

    public String getLastPostAuthor() {
        Post lastPost = getLastPost();
        return lastPost != null ? lastPost.getAuthor() : "";
    }

    public DateTime getLastPostDate() {
        Post lastPost = getLastPost();
        return lastPost != null ? lastPost.getDate() : null;
    }

    public String getLastPostTopicTitle() {
        Post lastPost = getLastPost();
        return lastPost != null ? lastPost.getTopic().getTitle() : "";
    }

    public String getName() {
        return name;
    }

    public int getPosition() {
        return position;
    }

    public int getPostCount() {
        return postCount;
    }

    public String getRestrictionForwardUrl() {
        return restrictionForwardUrl;
    }

    public int getTopicCount() {
        return topicCount;
    }

    public List<Topic> getTopics(int start, int limit) {

        Query q = new Query(context, "select * from # where forum=? order by lastPostDate desc");
        q.setTable(Topic.class);
        q.setParameter(this);
        return q.execute(Topic.class);
    }

    public String getValue() {
        return getIdString();
    }

    public int getViews() {
        return views;
    }

    /**
     *
     */
    public boolean hasCategory() {
        return category != null;
    }

    public boolean hasDescription() {
        return description != null;
    }

    public boolean hasLastPost() {
//        return lastPostDate != null;
        return getLastPost() != null;
    }

    public boolean isBlockEmails() {
        return blockEmails;
    }

    public boolean isLocked() {
        return locked;
    }

    public boolean isPermissions() {
        return permissions;
    }

    public void setBlockEmails(boolean blockEmailInPosts) {
        this.blockEmails = blockEmailInPosts;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Post getLastPost() {
        Query q = new Query(context, "select p.* from # p join # t on p.topic=t.id where t.forum=? order by p.id desc");
        q.setTable(Post.class);
        q.setTable(Topic.class);
        q.setParameter(this);
        return q.get(Post.class);
    }

    private void setLastPost() {

        Post post = getLastPost();
        if (post == null) {
            return;
        }
        lastPostAuthor = post.getAuthor();
        lastPostDate = post.getDate();
        lastPostTopicTitle = post.getTopic().getTitle();
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPermissions(boolean p) {
        this.permissions = p;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setPostCount() {

        Query q = new Query(context, "select count(p.id) from # t join # p on t.id=p.topic where t.forum=?");
        q.setTable(Topic.class);
        q.setTable(Post.class);
        q.setParameter(this);
        this.postCount = q.getInt();
    }

    public void setRestrictionForwardUrl(String restrictionForwardUrl) {
        this.restrictionForwardUrl = restrictionForwardUrl;
    }

    private void setTopicCount() {

        Query q = new Query(context, "select count(*) from # where forum=?").setTable(Topic.class).setParameter(this);
        this.topicCount = q.getInt();

        save();
    }

    public void update() {

        setPostCount();
        setLastPost();

        save();
    }

    public void viewed() {
        views++;
        save();
    }
}