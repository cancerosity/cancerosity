package org.sevensoft.ecreator.model.crm.forms.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IFormMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class FormMarker extends MarkerHelper implements IFormMarker, IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Form form) {
		return super.string(context, params, form.getName());
	}

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
        String formId = params.get("id");
        Form form = Form.getInstance(context, Form.class, formId);
        if (form == null)
            return null;

        return generate(context, params, form);
    }

	public Object getRegex() {
		return "form";
	}
}
