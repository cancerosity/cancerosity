package org.sevensoft.ecreator.model.crm.captcha;

import java.io.IOException;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.misc.FieldInputCell;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 4 Apr 2007 17:55:12
 *
 */
public class CaptchaPanel extends Panel {

	private final String	text;
	private final Captcha	captcha;

	public CaptchaPanel(RequestContext context, Captcha captcha, String text) throws IOException {
		super(context);
		this.captcha = captcha;
		this.text = text;
	}

	@Override
	public void makeString() {

		sb.append(new HiddenTag("captcha", captcha));
		sb.append(new FormFieldSet("Verification code"));

		if (text != null) {
			sb.append("<div class='text'>" + text + "</div>");
		}

		sb.append("<div class='text'>" + new ImageTag(Config.CaptchaPath + "/" + captcha.getFile().getName()) + "</div>");

		sb.append(new FieldInputCell("Enter code as seen in image", null, new TextTag(null, "captchaResponse", 20), new ErrorTag(context,
				"captchaResponse", " ")));
		sb.append("</fieldset>");
	}
}
