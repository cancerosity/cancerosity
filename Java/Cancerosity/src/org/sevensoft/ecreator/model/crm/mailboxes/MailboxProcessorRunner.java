package org.sevensoft.ecreator.model.crm.mailboxes;

import java.io.IOException;
import java.util.logging.Logger;

import javax.mail.MessagingException;

import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 04-Mar-2006 13:12:03
 *
 */
public class MailboxProcessorRunner implements Runnable {

	private static Logger			logger	= Logger.getLogger("ecreator");
	private MailboxProcessorResult	result;
	private RequestContext			context;

	public MailboxProcessorRunner(RequestContext context) {
		this.context = context;
	}

	public MailboxProcessorResult getResult() {
		return result;
	}

	/*
	 * @see java.lang.Runnable#run()
	 */
	public void run() {

		if (!Module.CrmMailboxes.enabled(context)) {
			return;
		}

		logger.fine("[MailboxProcessorRunner] running mailbox processor");

		result = new MailboxProcessorResult();

		try {

			for (Mailbox mailbox : Mailbox.get(context)) {

				if (mailbox.isEnabled()) {

					logger.fine("[MailboxProcessorRunner] checking mailbox=" + mailbox);

					result.addMailbox(1);
					result.addMessages(mailbox.process());
				}
			}

		} catch (MessagingException e) {

			e.printStackTrace();
			result.addError(e);

		} catch (IOException e) {

			e.printStackTrace();
			result.addError(e);

		}

	}
}
