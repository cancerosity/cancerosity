package org.sevensoft.ecreator.model.crm.messages;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 21 Aug 2006 19:40:49
 *
 */
public abstract class MessageSupport extends EntityObject implements MessageOwner {

	private int		messageCount;
	private String	lastMessageAuthor;
	private DateTime	lastMessageDate;

	public MessageSupport(RequestContext context) {
		super(context);
	}

	public Msg addMessage(String author, String email, String body) {
		return new Msg(context, this, author, email, body);
	}

	public Msg addMessage(User user, String content) {

		Msg message = new Msg(context, this, user, content);
		return message;
	}

	public Set<User> getContributors() {
		return MessageUtil.getContributors(this);
	}

	public final Msg getLastMessage() {
		return MessageUtil.getLastMessage(context, this);
	}

	public final String getLastMessageAuthor() {
		return lastMessageAuthor;
	}

	public final DateTime getLastMessageDate() {
		return lastMessageDate;
	}

	public final int getMessageCount() {
		return messageCount;
	}

	public Collection<String> getMessageEmails() {
		return null;
	}

	public final List<Msg> getMessages() {
		return MessageUtil.getMessages(context, this);
	}

	public abstract String getMessageSubject();

	public final List<Msg> getPublicMessages() {
		return MessageUtil.getPublicMessages(context, this);
	}

	public final boolean hasMessages() {
		return messageCount > 0;
	}

	public final void removeMessages() {
		MessageUtil.removeMessages(context, this);
	}

	public final void setLastMessageAuthor() {
		this.lastMessageAuthor = MessageUtil.getLastMessageAuthor(context, this);
	}

	public final void setLastMessageDate() {
		this.lastMessageDate = MessageUtil.getLastMessageDate(context, this);
	}

	public final void setMessageCount() {
		this.messageCount = getMessages().size();
		save();
	}

}
