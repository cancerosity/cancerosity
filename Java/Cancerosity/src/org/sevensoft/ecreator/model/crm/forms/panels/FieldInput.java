package org.sevensoft.ecreator.model.crm.forms.panels;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.crm.forms.FormField;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.bool.RadioTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.util.Keypad;
import org.sevensoft.jeezy.http.util.Renderer;

/**
 * @author sks 24 Aug 2006 10:59:53
 *
 */
public class FieldInput {

	private final FormField		field;
	private final RequestContext	context;
	private String		fieldName;
	private final Item		item;
	private final Category		category;
    private String value;

	public FieldInput(RequestContext context, FormField field) {
		this.context = context;
		this.field = field;
		this.fieldName = field.getParamName();
		this.item = (Item) context.getAttribute("item");
		this.category = (Category) context.getAttribute("category");
	}

    public void setValue(String value) {
        this.value = value;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
	public String toString() {

		Keypad keypad;

		switch (field.getType()) {

		case Attachment:

			return new FileTag("uploads").toString();

		case DropDownMenu:

			SelectTag tag = new SelectTag(context, fieldName);
			tag.setClass("styled");
			if (field.isOptional()) {
				tag.setAny("-None-");
			} else if (field.hasInstruction()) {
				tag.setAny(field.getInstruction());
			}
			tag.addOptions(field.getOptions());

			return tag.toString();

		case Radio:

			keypad = new Keypad(field.getCols());
			keypad.setTableClass("radios");
			keypad.setObjects(field.getOptions());
			keypad.setRenderer(new Renderer<String>() {

				public Object render(String option) {
					return new RadioTag(context, fieldName, option) + " " + option;
				}
			});

			return keypad.toString();

		case TickBox:

			return new CheckTag(context, fieldName, "true", false).setAlign("absmiddle").setClass("check").toString();

		case TickBoxes:

			keypad = new Keypad(field.getCols());
			keypad.setTableClass("checks");
			keypad.setObjects(field.getOptions());
			keypad.setRenderer(new Renderer<String>() {

				public Object render(String obj) {
					return new CheckTag(context, fieldName, obj, false) + " " + obj;
				}
			});

			return keypad.toString();

		default:
		case Text:

			if (field.getHeight() == 1) {

				return new TextTag(context, fieldName, value, field.getWidth()).setClass("text").toString();

			} else {

				return new TextAreaTag(context, fieldName, value, field.getWidth(), field.getHeight()).setClass("text").toString();

			}

		}

	}
}
