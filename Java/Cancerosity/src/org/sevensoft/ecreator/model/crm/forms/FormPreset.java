package org.sevensoft.ecreator.model.crm.forms;

import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 10-Apr-2006 17:52:26
 *
 */
public enum FormPreset {

	ContactForm() {

		public Form create(RequestContext context, String name) {

			Form form = new Form(context, name);

			form.addField("Name", FieldType.Text);

			FormField field = form.addField("Address", FieldType.Text);
			field.setHeight(4);
			field.save();

			form.addField("Telephone", FieldType.Text);
			form.addField("Email", FieldType.Text);
			form.addField("Your message", FieldType.Text);

			return form;

		}

		@Override
		public String toString() {
			return "Contact us form";
		}

	},

	QuoteForm() {

		public Form create(RequestContext context, String name) {

			Form form = new Form(context, name);

			form.addField("Your name", FieldType.Text);

			FormField field = form.addField("Address", FieldType.Text);
			field.setHeight(4);
			field.save();

			form.addField("Telephone", FieldType.Text);
			form.addField("Email", FieldType.Text);
			form.addField("Services required", FieldType.Text);

			return form;

		}

		@Override
		public String toString() {
			return "Quote form";
		}

	};

	public abstract Form create(RequestContext context, String name);
}
