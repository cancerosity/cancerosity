package org.sevensoft.ecreator.model.crm.forms;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.crm.forms.panels.FieldRenderer;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Cell;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 07-Jul-2005 17:00:33
 * 
 */
@Table("forms_fields")
public class FormField extends EntityObject implements Positionable, Selectable, Cell {

	private Form		form;

	/**
	 * Can this field be left blank ?
	 */
	private boolean		mandatory;

	/**
	 * Reg exp this field must match if not optional
	 */
	private String		regExp;

	/**
	 * Name of this field
	 */
	private String		name;

	/**
	 * List of options for this field
	 */
	private List<String>	options;

	private int			position;

	private String		section;

	private int			cellSpan;

	/**
	 * Field type
	 */
	private FieldType		type;

	/**
	 * Rows and cols for weight / height of elements
	 */
	private int			width, height;

	/**
	 * Customise this error message for non optional fields
	 */
	private String		errorMessage;

	/**
	 * 
	 */
	private String		instruction;

	/**
	 * Number of cols for tickboxes 
	 */
	private int			cols;

	private int			gridColumns;

	private String		description;

    private boolean repeatEmail;

	public FormField(RequestContext context) {
		super(context);
	}

	public FormField(RequestContext context, Form form, String name, FieldType type) {
		super(context);

		this.form = form;
		this.name = name.trim();
		this.type = type;
		this.options = new ArrayList<String>();

		if (type == FieldType.Text) {
			height = 1;
			width = 30;
		}

		save();
	}

	/**
	 * @throws CloneNotSupportedException 
	 * 
	 */
	public void copyTo(Form form) throws CloneNotSupportedException {

		FormField field = (FormField) super.clone();
		field.form = form;
		field.save();
	}

	@Override
	public synchronized boolean delete() {

		// remove email response field
		Query q = new Query(context, "update # set emailResponseField=0 where emailResponseField=?");
		q.setTable(Form.class);
		q.setParameter(this);
		q.run();

		return super.delete();
	}

	public Object getCellRenderer() {
		return new FieldRenderer(context, this);
	}

	public final int getCellSpan() {
		return cellSpan < 1 ? 1 : cellSpan;
	}

	public int getCols() {
		return cols < 1 ? 3 : cols;
	}

	public String getDescription() {
		return description;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public Form getForm() {
		return form.pop();
	}

	public int getGridColumns() {
		return gridColumns < 1 ? 1 : gridColumns;
	}

	public int getHeight() {
		
		if (height < 1) {
			return 1;
		}

		if (height > 30) {
			return 30;
		}

		return height;
	}

	public String getInstruction() {
		return instruction;
	}

	public String getLabel() {
		return getName();
	}

	public String getName() {
		return name;
	}

	public List<String> getOptions() {

		if (options == null) {
			options = new ArrayList();
		}

		return options;
	}

	public String getOptionsString() {
		return StringHelper.implode(getOptions(), "\n", true);
	}

	public String getParamName() {
		return "data_" + getId();
	}

    public String getRepeatParamName() {
        return "data_1_" + getId();
    }

	public int getPosition() {
		return position;
	}

	public String getRegExp() {
		return regExp;
	}

	public final String getSection() {
		return section;
	}

	public FieldType getType() {
		if (type == null) {
			type = FieldType.Text;
		}
		return type;
	}

	public String getValue() {
		return getIdString();
	}

	public int getWidth() {

		if (width < 4) {
			return 4;
		}

		if (width > 100) {
			return 100;
		}

		return width;
	}

	public boolean hasErrorMessage() {
		return errorMessage != null;
	}

	public boolean hasInstruction() {
		return instruction != null;
	}

	public boolean hasRegExp() {
		return regExp != null;
	}

	public boolean hasSection() {
		return section != null;
	}

	public boolean isDropDownMenu() {
		return type == FieldType.DropDownMenu;
	}

	public boolean isMandatory() {

		switch (getType()) {

		case Description:
		case Header:
		case TickBox:
		case Separator:
		case Attachment:
			return false;

		case Email:

		case DropDownMenu:
		case Radio:
		case Text:
			return mandatory;

		case TickBoxes:
			// always optional if we only have one option
			return getOptions().size() > 1 && mandatory;

		}

		return false;
	}

	public boolean isOptional() {
		return !isMandatory();
	}

	/**
	 * 
	 */
	public boolean isSeparator() {
		return getType() == FieldType.Separator;
	}

	/**
	 * 
	 */
	public boolean isText() {
		return getType() == FieldType.Text;
	}

	/**
	 * Returns true if this value is valid for this field.
	 * 
	 * Takes into account if the field is optional, and if the field has a reg ex set
	 */
	public boolean isValid(String value) {

		// if value is null then check if this field can be blank
		if (value == null) {

			return isOptional();
		}

		/*
		 * Check reg exp if applicable
		 */
		if (regExp == null)
			return true;

		Pattern pattern = Pattern.compile(regExp, Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
		return pattern.matcher(value).matches();
	}

	public boolean offerOptional() {

		switch (getType()) {

		default:
			return false;

		case Radio:
		case Text:
		case DropDownMenu:
		case TickBoxes:
			return true;
		}
	}

    public boolean isRepeatEmail() {
        return repeatEmail;
    }

    public void removeNewsletterSignUp(Newsletter newsletter) {
		SimpleQuery.delete(context, NewsletterSignup.class, "field", this, "newsletter", newsletter);
	}

	public void removeNewsletterSignUps() {
		SimpleQuery.delete(context, NewsletterSignup.class, "field", this);
	}

	public final void setCellSpan(int span) {
		this.cellSpan = span;
	}

	public void setCols(int cols) {
		this.cols = cols;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public final void setGridColumns(int gridColumns) {
		this.gridColumns = gridColumns;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	public void setName(String s) {
		name = s.trim();
	}

	public void setOptional(boolean b) {
		this.mandatory = !b;
	}

	public void setOptionsString(String string) {

		getOptions().clear();
		if (string == null) {
			return;
		}

		for (String option : string.split("\n")) {

			option = option.trim();
			if (option.length() > 0) {
				options.add(option);
			}
		}
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setRegExp(String regExp) {
		this.regExp = regExp;
	}

	public final void setSection(String section) {
		this.section = section;
	}

	public void setType(FieldType t) {
		this.type = t;
	}

	public void setWidth(int w) {
		this.width = w;
	}

	public boolean useCols() {

		switch (getType()) {

		default:
			return false;

		case TickBoxes:
		case Radio:
			return true;
		}
	}

	public boolean useDimensions() {

		switch (getType()) {

		default:
			return false;

		case Text:
			return true;
		}
	}

	public boolean useOptions() {

		switch (getType()) {

		default:
			return false;

		case DropDownMenu:
		case TickBoxes:
		case Radio:
			return true;
		}
	}

	
	public final void setDescription(String description) {
		this.description = description;
	}

    public void setRepeatEmail(boolean repeatEmail) {
        this.repeatEmail = repeatEmail;
    }
}
