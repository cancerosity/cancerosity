package org.sevensoft.ecreator.model.crm.messages.panels;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.extras.messages.MessageHandler;
import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.model.crm.messages.MessageOwner;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 20 Apr 2006 12:00:40
 *
 */
public class MessagesReplyPanel extends Panel {

	private final MessageOwner	owner;
	private final String		linkback;
	private final boolean		customer;
	private final boolean		form;

	/**
	 * Set customer to true if this is to be displayed on the frontend
	 * Set form to true if you want to wrap this panel in a form to add a message
	 * the link should be a linkback after adding a message
	 */
	public MessagesReplyPanel(RequestContext context, boolean customer, MessageOwner owner, Link link, boolean form) {

		super(context);
		this.customer = customer;

		this.owner = owner;
		this.form = form;

		if (link == null) {
			this.linkback = null;
		} else {
			this.linkback = link.toString();
		}
	}

	@Override
	public void makeString() {

		sb.append(new AdminTable("Add message"));

		if (form) {
			
			if (customer) {
				sb.append(new FormTag(CategoryHandler.class, "addMessage", "post"));
			} else {
				sb.append(new FormTag(MessageHandler.class, "addMessage", "post"));
			}

			if (linkback != null) {
				sb.append(new HiddenTag("linkback", linkback));
			}

			sb.append(new HiddenTag(StringHelper.toCamelCase(owner.getClass()), owner));
		}

		sb.append("<tr><td>" + new TextAreaTag(context, "body", 80, 5) + "</td></tr>");

		if (form) {
			sb.append("<tr><td>" + new SubmitTag("Add message") + "</td></tr>");
		}

		sb.append("</form>");

		sb.append("</table>");
	}

}
