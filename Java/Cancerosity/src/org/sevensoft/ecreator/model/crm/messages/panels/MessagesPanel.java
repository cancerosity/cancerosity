package org.sevensoft.ecreator.model.crm.messages.panels;

import java.util.Collections;
import java.util.List;

import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.iface.admin.ecom.orders.panels.AttachmentsLinePanel;
import org.sevensoft.ecreator.iface.admin.extras.messages.MessageHandler;
import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.model.crm.messages.MessageOwner;
import org.sevensoft.ecreator.model.crm.messages.Msg;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 20 Apr 2006 09:42:18
 * Renders all messages in the enote
 */
public class MessagesPanel extends Panel {

	private final MessageOwner	owner;
	private final boolean		showVis;
	private final boolean		customer;
	private List<Msg>			messages;

	public MessagesPanel(RequestContext context, MessageOwner owner, boolean showVisibility, boolean customer) {

		super(context);

		this.owner = owner;
		this.showVis = showVisibility;
		this.customer = customer;

		if (customer) {
			this.messages = owner.getPublicMessages();
		} else {
			this.messages = owner.getMessages();
		}
	}

	@Override
	public void makeString() {

		if (messages.isEmpty()) {
			return;
		}

		Collections.reverse(messages);

		int n = messages.size();
		for (Msg msg : messages) {

			sb.append(new TableTag("msg", "center"));
			sb.append("<tr>");

			final String titleClass;
			if (msg.isPrivate()) {
				titleClass = "title_int";
			} else {
				titleClass = "title_ext";
			}

			sb.append("<td rowspan='3' class='number' align='center' valign='top'>" + n);

			if (msg.hasUser()) {

				if (msg.getUser().hasAnyImage()) {

					Img image = msg.getUser().getAnyImage();
					if (image != null) {
						int[] d = Img.getDimensionsToFit(50, 50, image, true);

						sb.append(new ImageTag(image.getThumbnailPath(), 0, d[0], d[1]));
					}
				}
			}

			sb.append("</td>");
			sb.append("<td class=\"" + titleClass + "\"><b>" + msg.getAuthor() + "</b> - " + msg.getDate().toString("HH:mm, EEE dd MMM yyyy") + "</td>");
			sb.append("<td class=\"" + titleClass + "\" align='right'>");

			if (showVis && !customer) {

				if (msg.isPrivate()) {

					sb.append("<b>Private Message</b> | ");
					sb.append(new LinkTag(MessageHandler.class, "setPublic", "Change to public", "message", msg));

				} else {

					sb.append(" <b>Public Message</b>");
				}
			}

			sb.append("</td></tr>");

			sb.append("<tr><td class='attachments' colspan='2' align='right'>");

			/*
			 * File attachments row
			 */
			if (msg.hasAttachments()) {
				AttachmentsLinePanel panel = new AttachmentsLinePanel(context, msg);
				sb.append(panel);
			}

			if (msg.hasUser() && !customer) {

				sb.append(new FormTag(MessageHandler.class, "addAttachment", "multi"));
				sb.append(new HiddenTag("message", msg));

				sb.append(new FileTag("upload") + " " + new SubmitTag("Upload attachment"));
				sb.append("</form>");
			}

			sb.append("</td></tr>");

			sb.append("<tr><td colspan='2' class='body'>" + HtmlHelper.nl2br(HtmlHelper.convertLinks(msg.getBody())) + "</td></tr>");

			sb.append("</table>");

			n--;

		}
	}
}
