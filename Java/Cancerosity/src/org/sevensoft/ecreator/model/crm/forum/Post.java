package org.sevensoft.ecreator.model.crm.forum;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 19-Jan-2006 17:53:45
 *
 */
@Table("forums_posts")
public class Post extends EntityObject {

	/**
	 * 
	 */
	private String	content, author;
	/**
	 * 
	 */
	private DateTime	date;

	/**
	 * Member who posted this, member for BC
	 */
	private Item	account;
	/**
	 * 
	 */
	private Topic	topic;
	/**
	 * 
	 */
	private boolean	approved;

	public Post(RequestContext context) {
		super(context);
	}

	public Post(RequestContext context, Topic topic, Item account, String c) {
		super(context);

		this.content = c;
		this.account = account;
		this.author = account.getDisplayName();
		this.date = new DateTime();
		this.approved = true;
		this.topic = topic;

		/*
		 * If block email in posts is enabled then we should strip them out.
		 */
		if (topic.getForum().isBlockEmails()) {
			content = StringHelper.stripEmails(c);
		}

		save();
	}

	public Item getAccount() {
		return (Item) (account == null ? null : account.pop());
	}

	public String getAuthor() {
		return author;
	}

	public String getContent() {
		return content;
	}

	public String getContent(int i) {
		return StringHelper.toSnippet(content, i);
	}

	public DateTime getDate() {
		return date;
	}

	public String getSummary(int i) {
		return StringHelper.toSnippet(content, i);
	}

	public Topic getTopic() {
		return topic.pop();
	}

	/**
	 * 
	 */
	public boolean hasAccount() {
		return account != null;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
