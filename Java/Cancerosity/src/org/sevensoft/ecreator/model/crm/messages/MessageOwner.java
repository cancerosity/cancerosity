package org.sevensoft.ecreator.model.crm.messages;

import java.util.Collection;
import java.util.List;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.system.users.User;

/**
 * @author sks 4 Aug 2006 09:05:18
 *
 */
public interface MessageOwner {

	public Msg addMessage(String author, String email, String body);

	public Msg addMessage(User user, String body);

	public int getId();

	public Msg getLastMessage();

	public String getLastMessageAuthor();

	public DateTime getLastMessageDate();

	public int getMessageCount();

	public Collection<String> getMessageEmails();

	public List<Msg> getMessages();

	public String getMessageSubject();

	public List<Msg> getPublicMessages();

	public boolean hasMessages();

	public void removeMessages();

	public void setLastMessageAuthor();

	public void setLastMessageDate();

	public void setMessageCount();

}
