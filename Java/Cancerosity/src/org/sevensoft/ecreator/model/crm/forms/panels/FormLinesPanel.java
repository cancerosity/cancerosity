package org.sevensoft.ecreator.model.crm.forms.panels;

import org.sevensoft.ecreator.iface.frontend.crm.form.FormHandler;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.FormLine;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 27 Nov 2006 10:49:30
 *
 */
public class FormLinesPanel {

	private final Form		form;
	private final RequestContext	context;

	public FormLinesPanel(RequestContext context, Form form) {
		this.context = context;
		this.form = form;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(new TableTag("form_lines"));

		sb.append(new FormTag(FormHandler.class, "updateLines", "post"));
		sb.append(new HiddenTag("form", form));

		sb.append("<tr>");
		sb.append("<th colspan='2'>Description</th>");
		if (form.isLineQuantities()) {
			sb.append("<th>Qty</th>");
		}
		sb.append("</tr>");

		for (FormLine line : form.getLines(context.getSessionId())) {

			sb.append("<tr>");
			sb.append("<td>" + new LinkTag(FormHandler.class, "removeLine", "x", "line", line) + "</td>");
			sb.append("<td>" + new LinkTag(line.getItem().getUrl(), line.getItem().getName()) + "</td>");

			if (form.isLineQuantities()) {
				sb.append("<td>" + new TextTag(context, "qtys_" + line.getId(), line.getQty(), 4) + "</td>");
			}

			sb.append("</tr>");
		}

		sb.append("<tr><td colspan='3'>" + new SubmitTag("Change quantities") + "</td></tr>");

		sb.append("</form>");
		sb.append("</table>");

		return sb.toString();
	}
}
