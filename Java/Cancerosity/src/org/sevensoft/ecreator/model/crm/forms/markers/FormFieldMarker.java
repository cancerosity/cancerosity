package org.sevensoft.ecreator.model.crm.forms.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.FormField;
import org.sevensoft.ecreator.model.crm.forms.panels.FieldInput;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IFormMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 5 Jul 2006 13:00:44
 *
 */
public class FormFieldMarker extends MarkerHelper implements IFormMarker, IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Form form) {

		String id = params.get("id");
		FormField field = form.getField(Integer.parseInt(id.trim()));

        return generateField(context, params, field);
	}

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
        String id = params.get("id");
        FormField field = FormField.getInstance(context, FormField.class, id);
        
        return generateField(context, params, field);
    }

    private Object generateField(RequestContext context, Map<String, String> params, FormField field) {
        if (field == null)
            return null;

        if (params.containsKey("name"))
            return field.getName();

        FieldInput fieldInput = new FieldInput(context, field);

        if (params.containsKey("initial"))
            fieldInput.setValue(params.get("initial"));

        return fieldInput;
	}

	public Object getRegex() {
		return "field";
	}
}
