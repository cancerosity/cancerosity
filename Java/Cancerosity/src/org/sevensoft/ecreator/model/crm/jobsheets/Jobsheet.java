package org.sevensoft.ecreator.model.crm.jobsheets;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.ecreator.model.attributes.support.AttributeOwnerSupport;
import org.sevensoft.ecreator.model.crm.jobsheets.Job.Status;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 18 Jul 2006 11:17:53
 *
 */
@Table("jobsheets")
public class Jobsheet extends AttributeOwnerSupport implements Selectable, Positionable {

	public static List<Jobsheet> get(RequestContext context) {
		return SimpleQuery.execute(context, Jobsheet.class);
	}

	/**
	 * The name of htis jobsheet
	 */
	private String		name;

	private int			outstandingJobCount, completedJobCount;

	private int			redStatusDays, amberStatusDays;

	/**
	 * Emails to be notified when a job is created in this job sheet
	 */
	private List<String>	creationEmails;

	/**
	 * Emails to be notified when a job is completed in this job sheet
	 */
	private List<String>	completionEmails;

	private int			position;

	private boolean		completionEmailToContributors;

	public Jobsheet(RequestContext context) {
		super(context);
	}

	public Jobsheet(RequestContext context, String name) {
		super(context);

		this.name = name;
		this.completionEmailToContributors = true;

		save();
	}

	public Job addJob(User user, String name) {

		Job job = new Job(context, this, user, name);
		setJobCounts();

		return job;

	}

	@Override
	public synchronized boolean delete() {

		for (Job job : getJobs())
			job.delete();

		return super.delete();
	}

	public int getAmberStatusDays() {
		return amberStatusDays;
	}

	public int getCompletedJobCount() {
		return completedJobCount;
	}

	public List<String> getCompletionEmails() {
		if (completionEmails == null) {
			completionEmails = new ArrayList();
		}
		return completionEmails;
	}

	public List<String> getCreationEmails() {
		if (creationEmails == null) {
			creationEmails = new ArrayList();
		}
		return creationEmails;
	}

	public List<Job> getJobs() {
		Query q = new Query(context, "select * from # where jobsheet=? order by lastMessageDate ");
		q.setParameter(this);
		q.setTable(Job.class);
		return q.execute(Job.class);
	}

	public List<Job> getJobs(Status status) {
		Query q = new Query(context, "select * from # where jobsheet=? and status=? order by position");
		q.setTable(Job.class);
		q.setParameter(this);
		q.setParameter(status);
		return q.execute(Job.class);
	}

	public String getLabel() {
		return getName();
	}

	public String getName() {
		return name;
	}

	public Job getNextJob() {
		Query q = new Query(context, "select * from # where jobsheet=? and status=? order by position");
		q.setTable(Job.class);
		q.setParameter(this);
		q.setParameter(Job.Status.Outstanding);
		return q.get(Job.class);
	}

	public int getOutstandingJobCount() {
		return outstandingJobCount;
	}

	public int getPosition() {
		return position;
	}

	public int getRedStatusDays() {
		return redStatusDays;
	}

	public String getValue() {
		return getIdString();
	}

	public boolean hasCreationEmails() {
		return !getCreationEmails().isEmpty();
	}

	public final boolean isCompletionEmailToContributors() {
		return completionEmailToContributors;
	}

	public void removeJob(Job job) {
		job.delete();
		setJobCounts();
	}

	public void setAmberStatusDays(int amberStatusDays) {
		this.amberStatusDays = amberStatusDays;
	}

	public void setCompletionEmails(List<String> completionEmails) {
		this.completionEmails = completionEmails;
	}

	public final void setCompletionEmailToContributors(boolean completionEmailToContributors) {
		this.completionEmailToContributors = completionEmailToContributors;
	}

	public void setCreationEmails(List<String> e) {
		this.creationEmails = new ArrayList();
		if (e != null) {
			this.creationEmails.addAll(e);
		}
	}

	void setJobCounts() {
		this.outstandingJobCount = SimpleQuery.count(context, Job.class, "jobsheet", this, "status", Job.Status.Outstanding);
		this.completedJobCount = SimpleQuery.count(context, Job.class, "jobsheet", this, "status", Job.Status.Completed);
		save();
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setRedStatusDays(int redStatusDays) {
		this.redStatusDays = redStatusDays;
	}

}
