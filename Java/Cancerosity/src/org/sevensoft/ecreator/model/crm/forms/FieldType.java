package org.sevensoft.ecreator.model.crm.forms;

/**
 * @author sks 6 Oct 2006 13:37:25
 *
 */
public enum FieldType {

	DropDownMenu("Drop down menu"), Separator, Text, TickBox("Tick box"), Radio("Radio options"), Header, Description(), TickBoxes("Tick boxes"), Attachment(
			"Attachment"), Email("Email");

	private final String	toString;

	private FieldType() {
		this.toString = name();
	}

	private FieldType(String toString) {
		this.toString = toString;
	}

	@Override
	public String toString() {
		return toString;
	}
}