package org.sevensoft.ecreator.model.crm.forms;

import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Nov 2006 17:33:30
 *
 */
@Table("forms_lines_options")
public class FormLineOption extends EntityObject implements Positionable {

	@Index
	private FormLine		line;

	@Index
	private ItemOption	itemOption;
	private String		value;
	
	private int			position;

	protected FormLineOption(RequestContext context) {
		super(context);
	}

	public FormLineOption(RequestContext context, FormLine line, ItemOption key, String value) {
		super(context);

		this.line = line;
		this.itemOption = key;
		this.value = value;
		
		save();
	}

	public ItemOption getItemOption() {
		return itemOption.pop();
	}

	public FormLine getLine() {
		return line.pop();
	}

	public int getPosition() {
		return position;
	}

	public String getValue() {
		return value;
	}

	public void setPosition(int position) {
		this.position = position;
	}

}
