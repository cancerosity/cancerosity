package org.sevensoft.ecreator.model.crm.forum;

import java.util.List;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.jeezy.db.QueryBuilder;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 30 Oct 2006 18:57:47
 *
 */
public class PostSearcher {

	private RequestContext	context;

	/**
	 * Lmit to posts by this member
	 */
	private Item		poster;

	private String		author;

	/**
	 * Title / subject of topic this post is in
	 */
	private String		title;

	private int			limit;

	private int			start;

	/**
	 * Limit to posts in this forum
	 */
	private Forum		forum;

	private String		keywords;

	private SortType		sortType;

	private Item		account;

	public PostSearcher(RequestContext context) {
		this.context = context;
	}

	public List<Post> execute() {

		QueryBuilder b = new QueryBuilder(context);
		b.select("p.*");
		b.from("# p", Post.class);
		b.from("join # t on p.topic=t.id", Topic.class);

		if (forum != null) {
			b.clause("t.forum=?", forum);
		}

		if (keywords != null) {
			b.clause("p.content like ?", "%" + keywords + "%");
		}

		if (poster != null) {
			b.clause("p.poster=?", poster);
		}

		if (account != null) {
			b.clause("p.account=?", account);
		}

		if (author != null) {
			b.clause("p.author like ?", "%" + author + "%");
		}

		if (title != null) {
			b.clause("t.title like ?", "%" + title + "%");
		}

		if (sortType != null) {
			switch (sortType) {
			default:
				b.order("p.id desc");
				break;
			case Newest:
				b.order("p.id desc");
				break;
			}
		}

		return b.execute(Post.class, start, limit);

	}

	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * 
	 */
	public void setForum(Forum forum) {
		this.forum = forum;
	}

	/**
	 * 
	 */
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public void setPoster(Item p) {
		this.poster = p;
	}

	public void setSortType(SortType sortType) {
		this.sortType = sortType;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 
	 */
	public void setAccount(Item account) {
		this.account = account;
	}
}
