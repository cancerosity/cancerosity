package org.sevensoft.ecreator.model.crm.forms.submission.report;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.submission.Submission;

import java.util.*;

/**
 * User: Tanya
 * Date: 03.11.2011
 */
public class SubmissionReportEmail extends Email {

    private RequestContext context;
    private SubmissionReportModule module;
    private Config config;

    public SubmissionReportEmail(RequestContext context) {
        this.context = context;
        this.module = SubmissionReportModule.getInstance(context);
        this.config = Config.getInstance(context);

        setFrom(config.getServerEmail());
        setRecipients(module.getEmails());
        setSubject(module.getReportTitle() + " for " + new Date().previousDay().toDateString() + " from " + config.getUrl());
        setBody(getBody(), "text/html");
    }

    public String getBody() {

        StringBuilder sb = new StringBuilder();
        int total = 0;

        Set<Integer> set = module.getForms();
        Iterator<Integer> iter = set.iterator();
        final Map<Form, Integer> forms = new MultiValueMap<Form, Integer>();
        while (iter.hasNext()) {
            Integer nextId = iter.next();
            Form next = Form.getInstance(context, Form.class, nextId);
            Integer count = getCount(context, next.getId());
            forms.put(next, count);
            total = total + count;
        }
        List<Form> list = new ArrayList<Form>(forms.keySet());
        Collections.sort(list, new Comparator<Form>() {

            public int compare(Form o1, Form o2) {
                return forms.get(o2).compareTo(forms.get(o1));
            }
        });

        sb.append("<b>Yesterday you received a total of <span style='color: red;'>");
        sb.append(total);
        sb.append(" forms</span> from <br>");
        sb.append(config.getUrl() + "</b><br><br><br>");


//        forms.sort
        sb.append("<b><table border='0'>");
        for (Form form : list) {
            sb.append("<tr>");
            sb.append("<td width='300' nowrap>");
            sb.append(form.getName());
            sb.append(" - </td>");
            sb.append("<td width='100'>Total:");
            sb.append("</td>");
            sb.append("<td>");
            sb.append(forms.get(form));
            sb.append("</td>");
            sb.append("</tr>");
        }
        sb.append("</table></b>");
        sb.append("<br><br>");
        sb.append("<span style='font-size: 200%;'>TOTAL AMOUNT = </span><span style='color: red; font-size: 200%;'>");
        sb.append(total);
        sb.append("</span><br><br>");
        sb.append("------------------------------------------<br><br>");
        sb.append("BREAKDOWN OF PAGES SECTION<br><br>");

        for (Form form : list) {
            sb.append("<b>");
            sb.append(form.getName());
            sb.append(" - Total - ");
            sb.append(forms.get(form));
            sb.append("</b><br>");

            sb.append("Page On Site Form Was Located:<br>");
            for (String page : getPageDistinct(context, form.getId())) {
                sb.append(page);
                sb.append(" - ");
                sb.append(getPageCount(context, form.getId(), page));
                sb.append("<br>");
            }
            sb.append("<br>");

        }

        return sb.toString();
    }

    private static int getPageCount(RequestContext context, int formId, String pageName) {
        Query q = new Query(context, "select count(*) from # where date>=? and date<=? and form=? and pageName=?");
        q.setTable(Submission.class);
        q.setParameter(new DateTime(new Date().previousDay()).beginDay());
        q.setParameter(new DateTime(new Date()).beginDay());
        q.setParameter(formId);
        q.setParameter(pageName);
        return q.getInt();
    }

    private static List<String> getPageDistinct(RequestContext context, int formId) {
        Query q = new Query(context, "select distinct(pageName) from # where date>=? and date<=? and form=?");
        q.setTable(Submission.class);
        q.setParameter(new DateTime(new Date().previousDay()).beginDay());
        q.setParameter(new DateTime(new Date()).beginDay());
        q.setParameter(formId);
        return q.getStrings();
    }

    private static int getCount(RequestContext context, int formId) {
        Query q = new Query(context, "select count(*) from # where date>=? and date<=? and form=?");
        q.setTable(Submission.class);
        q.setParameter(new DateTime(new Date().previousDay()).beginDay());
        q.setParameter(new DateTime(new Date()).beginDay());
        q.setParameter(formId);
        return q.getInt();
    }

    private static int getCountTotal(RequestContext context) {
        Query q = new Query(context, "select count(*) from # where date>=? and date<=?");
        q.setTable(Submission.class);
        q.setParameter(new DateTime(new Date().previousDay()).beginDay());
        q.setParameter(new DateTime(new Date()).beginDay());
        return q.getInt();
    }
}
