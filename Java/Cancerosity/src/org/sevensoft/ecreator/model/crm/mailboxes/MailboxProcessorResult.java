package org.sevensoft.ecreator.model.crm.mailboxes;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sks 18 May 2006 22:58:42
 *
 */
public class MailboxProcessorResult {

	private int			mailboxes;
	private int			messages;
	private List<String>	errors;

	public MailboxProcessorResult() {
		this.errors = new ArrayList<String>();
	}

	/**
	 * @param e
	 */
	public void addError(Exception e) {
		errors.add(e.getMessage());
	}

	public void addMailbox(int i) {
		mailboxes += i;
	}

	
	public final List<String> getErrors() {
		return errors;
	}

	public void addMessages(int i) {
		messages += i;
	}

	public final int getMailboxes() {
		return mailboxes;
	}

	public final int getMessages() {
		return messages;
	}

}
