package org.sevensoft.ecreator.model.crm.messages;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27-Mar-2006 07:36:07
 *
 */
public class MessageUtil {

	private static Pattern	subjectPattern	= Pattern.compile("\\[#(\\d*?)]");
	private static Logger	logger		= Logger.getLogger("ecreator");

	/**
	 * Returns a list of Users that have added messages to this message owner
	 */
	public static Set<User> getContributors(MessageOwner owner) {

		Set<User> contributors = new HashSet();
		for (Msg msg : owner.getMessages()) {
			if (msg.hasUser()) {
				contributors.add(msg.getUser());
			}
		}

		return contributors;
	}

	public static Msg getFromSubject(String subject, RequestContext context) {

		Matcher matcher = subjectPattern.matcher(subject);
		if (matcher.find()) {

			String id = matcher.group(1);
			logger.fine("[MessageUtil] email id=" + id);

			return EntityObject.getInstance(context, Msg.class, id);

		} else {

			return null;
		}
	}

	public static Msg getLastMessage(RequestContext context, MessageOwner owner) {

		List<Msg> messages = getMessages(context, owner);
		if (messages.isEmpty()) {
			return null;
		}

		return messages.get(messages.size() - 1);
	}

	public static String getLastMessageAuthor(RequestContext context, MessageOwner owner) {
		List<Msg> messages = getMessages(context, owner);
		return messages.isEmpty() ? null : messages.get(messages.size() - 1).getAuthor();
	}

	public static DateTime getLastMessageDate(RequestContext context, MessageOwner owner) {
		List<Msg> messages = getMessages(context, owner);
		return messages.isEmpty() ? null : messages.get(0).getDate();
	}

	public static List<Msg> getMessages(RequestContext context, MessageOwner owner) {

		Query q = new Query(context, "select * from # where `" + owner.getClass().getSimpleName() + "`=? order by id");
		q.setParameter(owner);
		q.setTable(Msg.class);
		return q.execute(Msg.class);

	}

	public static List<Msg> getPublicMessages(RequestContext context, MessageOwner owner) {
		List<Msg> msgs = getMessages(context, owner);
		CollectionsUtil.filter(msgs, new Predicate<Msg>() {

			public boolean accept(Msg e) {
				return e.isPublic();
			}
		});
		return msgs;
	}

	public static void removeMessage(Msg msg) {

		MessageOwner owner = msg.getOwner();

		msg.delete();

		owner.setMessageCount();
	}

	/**
	 * 
	 */
	public static void removeMessages(RequestContext context, MessageOwner owner) {
		for (Msg message : getMessages(context, owner)) {
			message.delete();
		}
		owner.setMessageCount();
	}

}
