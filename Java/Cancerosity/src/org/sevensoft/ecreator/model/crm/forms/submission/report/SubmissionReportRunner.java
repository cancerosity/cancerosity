package org.sevensoft.ecreator.model.crm.forms.submission.report;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.smail.Email;

import java.util.logging.Logger;

/**
 * User: Tanya
 * Date: 03.11.2011
 */
public class SubmissionReportRunner implements Runnable {

    private Logger logger = Logger.getLogger("cron");

    private final RequestContext context;

    public SubmissionReportRunner(RequestContext context) {
        this.context = context;
    }

    public void run() {
        if (Email.sendEmail(context, new SubmissionReportEmail(context))) {
            logger.fine("[SubmissionReportRunner] send report email");
        } else {
            logger.fine("[SubmissionReportRunner] REPORT EMAIL WAS NOT SENT");
        }
    }
}
