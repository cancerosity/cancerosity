package org.sevensoft.ecreator.model.crm.forms.submission.report.renderers;

import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.util.Keypad;
import org.sevensoft.jeezy.http.util.Renderer;

import java.util.logging.Logger;
import java.util.List;

/**
 * User: Tanya
 * Date: 03.11.2011
 */
public class SubmissionFormsListRenderer {

    private static Logger logger = Logger.getLogger("ecreator");
    private RequestContext context;

    private List<Form> formsAll;
    private List<Integer> values;
    private int cols;
    private String parameter;

    public SubmissionFormsListRenderer(RequestContext context, List<Integer> values) {
        this.context = context;
        this.formsAll = Form.get(context);
        this.values = values;
        this.cols = 3;
        this.parameter = "forms";
    }

    @Override
    public String toString() {
        logger.fine("RevealOptionsSelectionRenderer{" +
                "formsAll=" + formsAll +
                ", context=" + context +
                ", values=" + values +
                ", parameter='" + parameter + '\'' +
                '}');


        Keypad keypad = new Keypad(cols);

        keypad.setObjects(formsAll);

        keypad.setRenderer(new Renderer<Form>() {

            public Object render(Form form) {

                boolean checked = false;
                if (values != null) {
                    checked = values.contains(form.getId());
                }

                String param = "formsOptionCheckTag" + form.getId();

                CheckTag checkTag = new CheckTag(context, parameter, form.getValue(), checked);
                checkTag.setId(param);

                StringBuilder sb = new StringBuilder();
                sb.append(checkTag);
//                sb.append(" #");
//                sb.append(form.getValue());
//                sb.append(" - ");
                sb.append(form.getName());

                return sb.toString();
            }

        });

        return keypad.toString();
    }
}
