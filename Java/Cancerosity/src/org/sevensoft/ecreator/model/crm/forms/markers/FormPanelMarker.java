package org.sevensoft.ecreator.model.crm.forms.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.panels.FormPanel;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * User: Tanya
 * Date: 13.06.2012
 */
public class FormPanelMarker extends MarkerHelper implements IItemMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
        String formId = params.get("form");
        Form form = Form.getInstance(context, Form.class, formId);
        if (form == null)
            return null;

        return new FormPanel(context, form);
    }

    public Object getRegex() {
        return "form_panel";
    }
}
