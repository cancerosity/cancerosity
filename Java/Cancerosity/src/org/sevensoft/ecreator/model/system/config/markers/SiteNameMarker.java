package org.sevensoft.ecreator.model.system.config.markers;

import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * User: Tanya
 * Date: 25.01.2011
 */
public class SiteNameMarker implements IGenericMarker {

    public Object generate(RequestContext context, Map<String, String> params) {
        return Config.getInstance(context).getUrl();
    }

    public Object getRegex() {
        return "site_name";
    }
}