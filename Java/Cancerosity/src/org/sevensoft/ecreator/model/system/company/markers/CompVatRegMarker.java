package org.sevensoft.ecreator.model.system.company.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 06-Apr-2006 08:49:08
 *
 */
public class CompVatRegMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Company.getInstance(context).isVatRegistered())
			return null;

		String prefix = params.get("prefix");
		String suffix = params.get("suffix");

		StringBuilder sb = new StringBuilder();
		if (prefix != null)
			sb.append(prefix);

		sb.append(Company.getInstance(context).getVatNumber());

		if (suffix != null)
			sb.append(suffix);

		return sb.toString();
	}

	public Object getRegex() {
		return "comp_vatreg";
	}
}