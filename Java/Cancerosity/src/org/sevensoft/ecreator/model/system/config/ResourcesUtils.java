package org.sevensoft.ecreator.model.system.config;

import java.io.File;
import java.util.Arrays;

/**
 * User: MeleshkoDN Date: 29.10.2007 Time: 10:59:25
 */
public class ResourcesUtils {

	public static File getRealAttachment(String attachment) {
		return new File(getRealAttachmentsPath() + File.separator + attachment);
	}

	public static File getRealAttachmentsDir() {
		return new File(getRealAttachmentsPath());
	}

	public static String getRealAttachmentsPath() {
		return getRealPathToResource(Config.AttachmentsPath);
	}

	public static File getRealCaptcha(String captcha) {
		return new File(getRealCaptchasPath() + File.separator + captcha);
	}

	public static File getRealCaptchasDir() {
		return new File(getRealCaptchasPath());
	}

	public static String getRealCaptchasPath() {
		return getRealPathToResource(Config.CaptchaPath);
	}

	public static File getRealFeedData(String feedDataFile) {
		return new File(getRealFeedDataPath() + File.separator + feedDataFile);
	}

	public static File getRealFeedDataDir() {
		return new File(getRealFeedDataPath());
	}

	public static String getRealFeedDataPath() {
		return getRealPathToResource(Config.FeedDataPath);
	}

	public static File getRealFont(String fontFile) {
		return new File(getRealFontsPath() + File.separator + fontFile);
	}

	public static File getRealFontsDir() {
		return new File(getRealFontsPath());
	}

	public static String getRealFontsPath() {
		return getRealPathToResource(Config.FontsPath);
	}

	public static File getRealImage(String image) {
		return new File(getRealImagesPath() + File.separator + image);
	}

	public static File getRealImagesDir() {
		return new File(getRealImagesPath());
	}

	public static String getRealImagesPath() {
		return getRealPathToResource(Config.ImagesPath);
	}

	public static File getRealMedia(String mediaFile) {
		return new File(getRealMediaPath() + File.separator + mediaFile);
	}

	public static File getRealMediaDir() {
		return new File(getRealMediaPath());
	}

	public static String getRealMediaPath() {
		return getRealPathToResource(Config.MediaPath);
	}

	private static String getRealPathToResource(String resource) {
		return JNDIUtils.getResourcesPath() + File.separator + resource;
	}

	public static File getRealPdf(String attachment) {
		return new File(getRealPdfsPath() + File.separator + attachment);
	}

	public static File getRealPdfsDir() {
		return new File(getRealPdfsPath());
	}

	public static String getRealPdfsPath() {
		return getRealPathToResource(Config.PdfsPath);
	}

	public static File getRealTemplateData(String templateData) {
		return new File(getRealTemplateDataPath() + File.separator + templateData);
	}

	public static File getRealTemplateDataDir() {
		return new File(getRealTemplateDataPath());
	}

	public static String getRealTemplateDataPath() {
		return getRealPathToResource(Config.TemplateDataPath);
	}

	public static File getRealTemplateStore(String templateStoreFile) {
		return new File(getRealTemplateStorePath() + File.separator + templateStoreFile);
	}

	public static File getRealTemplateStoreDir() {
		return new File(getRealTemplateStorePath());
	}

	public static String getRealTemplateStorePath() {
		return JNDIUtils.getTemplateStorePath() + File.separator + Config.TemplateStorePath;
	}

	public static File getRealTemplateTitle(String templateTitleFile) {
		return new File(getRealTemplateTitlesPath() + File.separator + templateTitleFile);
	}

	public static File getRealTemplateTitlesDir() {
		return new File(getRealTemplateTitlesPath());
	}

	public static String getRealTemplateTitlesPath() {
		return getRealPathToResource(Config.TemplateTitlesPath);
	}

	public static File getRealThumbnail(String thumbnail) {
		return new File(getRealThumbnailsPath() + File.separator + thumbnail);
	}

	public static File getRealThumbnailsDir() {
		return new File(getRealThumbnailsPath());
	}

	public static String getRealThumbnailsPath() {
		return getRealPathToResource(Config.ThumbnailsPath);
	}

	public static File getRealTmp(String tmpFile) {
		return new File(getRealTmpPath() + File.separator + tmpFile);
	}

	public static File getRealTmpDir() {
		return new File(getRealTmpPath());
	}

	public static String getRealTmpPath() {
		return getRealPathToResource(Config.TmpPath);
	}

	public static File getRealUserData(String userDataFile) {
		return new File(getRealUserDataPath() + File.separator + userDataFile);
	}

	public static File getRealUserDataDir() {
		return new File(getRealUserDataPath());
	}

	public static String getRealUserDataPath() {
		return getRealPathToResource(Config.UserDataPath);
	}

	public static File getRealVideo(String video) {
		return new File(getRealVideoPath() + File.separator + video);
	}

	public static String getRealVideoPath() {
		return getRealPathToResource(Config.VideosPath);
	}

	public static File getRealVideosDir() {
		return new File(getRealVideoPath());
	}

    public static String getRealEmailLogoPath() {
        return getRealPathToResource(Config.EmailLogoPath);
    }

    public static File getRealEmailLogoDir() {
		return new File(getRealEmailLogoPath());
	}

    public static File getRealEmailLogo(String filename) {
		return new File(getRealEmailLogoPath() + File.separator + filename);
	}

    public static String getRealSuperIPsFilePath() {
        return JNDIUtils.getSuperIPsPath() + File.separator + Config.SuperIPsFile;
    }

    public static File getRealSuperIPsFile() {
        return new File(getRealSuperIPsFilePath());
    }

    public static File recreatePath(File path){
        if (path.exists()) {
            java.util.List<File> files = Arrays.asList(path.listFiles());
            for (File delete : files) {
                delete.delete();
            }
        } else
            path.mkdir();

        return path;
    }

    
}
