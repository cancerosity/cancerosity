package org.sevensoft.ecreator.model.system.modules;

import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 22 Jun 2006 23:08:33
 */
public enum Module {

    MemberBlocking("Member blocking") {

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.MemberProfiles};
        }
    },

    Captcha() {
    },

    SavedSearches("Saved searches") {

        @Override
        public String getDescription() {
            return "Allow users to save searches so they can re-run previous searches";
        }

        @Override
        public boolean beta() {
            return true;
        }
    },

    SearchAlerts("Search alerts") {

        @Override
        public boolean beta() {
            return true;
        }

        @Override
        public String getDescription() {
            return "Users save a search and when new items are added that match they are emailed or sent an SMS";
        }
    },

    GoogleAnalytics("Google Analytics") {

        @Override
        public String getDescription() {
            return "Enable use of google analytics integration";
        }
    },

    KeywordSuggestions("Keyword suggestions") {

        @Override
        public boolean beta() {
            return true;
        }
    },

    Galleries() {

        @Override
        public String getDescription() {
            return "Automatic photo galleries";
        }
    },

    /**
     * Availability and stock info
     */
    Availabilitity("Stock and availability") {

        @Override
        public String getDescription() {
            return "Track stock, set in / out stock messages, and configure stock alerts for items";
        }

    },
    Credits() {

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Accounts};
        }

        @Override
        public boolean beta() {
            return true;
        }
    },

    /**
     * Allow categories to wrap searches for use as containers
     */
    WrappedSearches("Search wrappers") {

        @Override
        public String getDescription() {
            return "Automatic inclusion of items in categories via pre-defined searches";
        }
    },

    Videos() {

    },

    Calculators() {

        @Override
        public String getDescription() {
            return "Build calculators that can be assigned to a category";
        }

    },

    Favourites() {

    },

    ProfileSms() {

        @Override
        public boolean beta() {
            return super.beta();
        }

        @Override
        public String getDescription() {
            return "Allows users on the site to send members an SMS via their profile";
        }

    },

    Vouchers() {

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Pricing};
        }

        @Override
        public boolean beta() {
            return true;
        }
    },

    ListingVouchers("Listing package voucher") {

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Vouchers, Module.Listings};
        }

        @Override
        public boolean beta() {
            return true;
        }
    },
    Accessories() {

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Shopping};
        }
    },
    Alternatives() {

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Shopping};
        }
    },

    Newsfeed() {

        @Override
        public String getDescription() {
            return "Import and display of RSS newsfeeds";
        }
    },

    /**
     * Access levels for user groups
     */
    Privileges("Privileges") {

        @Override
        public String getDescription() {
            return "Restrict admin access to individual user accounts";
        }
    },

    /**
     * Allow normal users to edit markup
     */
    UserMarkup("User markup") {

        @Override
        public String getDescription() {
            return "Allow admin users access to the template / markup system";
        }

    },

    AttachmentPayment("Attachment payments") {

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Credits};
        }
    },

    Affiliates() {

        @Override
        public boolean beta() {
            return true;
        }

        @Override
        public String getDescription() {
            return "Track click throughs and conversions from your affiliated sites";
        }
    },
    Calendars() {

        @Override
        public String getDescription() {
            return "Show items on calendars via date attributes";
        }
    },

    Meetups() {

        @Override
        public boolean beta() {
            return true;
        }
    },

    CategoryAttributes("Category attributes") {

        @Override
        public String getDescription() {
            return "Enable attributes in categories";
        }
    },

    UserItemTypes("User item types") {

        @Override
        public String getDescription() {
            return "Let admin users have access to create / delete / edit item types";
        }

    },
    Jobsheets() {

        @Override
        public String getDescription() {
            return "Task based jobsheets";
        }
    },

    Bookings() {

        @Override
        public boolean beta() {
            return true;
        }

        @Override
        public String getDescription() {
            return "Date based booking system";
        }

    },

    UserplaneMessenger("Userplane Messenger") {

        @Override
        public String getDescription() {
            return "Userplane messenger integration";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Accounts};
        }
    },

    UserFeeds("User feeds") {

        @Override
        public String getDescription() {
            return "Allow users to run and edit feeds";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Feeds};
        }

    },

    RssExport() {

        @Override
        public String getDescription() {
            return "Export items as RSS feed";
        }

    },

    Shopping() {

        @Override
        public String getDescription() {
            return "Enable the checkout and basket system for purchasing products";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Pricing};
        }

    },

    NewsletterTemplates("Newsletter templates") {

        @Override
        public String getDescription() {
            return "Create pre-defined HTML templates which will auto wrap around a newsletter when sending";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Newsletter};
        }
    },

    NewsletterExamples("Newsletter examples") {

        @Override
        public String getDescription() {
            return "Save the newsletter and load previously saved newsletters to be loaded in";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Newsletter};
        }
    },
    NewsletterNewItems("Newsletter with items") {

        @Override
        public String getDescription() {
            return "Email new items to subscribers";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Newsletter};
        }
    },
    Currencies() {

        @Override
        public String getDescription() {
            return "Create multiple currencies and allow users to select between on the front end";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Pricing};
        }

    },

    Permissions() {

        @Override
        public String getDescription() {
            return "Access control for categories, items, etc, via permission masks";
        }
    },

    Subscriptions() {

        @Override
        public String getDescription() {
            return "Allow users to pay for different levels of account";
        }

    },

    SubscriptionDeleter() {

        @Override
        public String getDescription() {
            return "Delete listings of the account with expired subscription";
        }

        @Override
        Module[] requiresModule() {
           return new Module[]{Module.Subscriptions};
        }
    },

    Listings() {

        @Override
        public String getDescription() {
            return "Enable users to add items via frontend listing process";
        }

    },
    ListingSms("Listing SMS") {
        @Override
        public String getDescription() {
            return "Ability to send SMS from listing view page";
        }
    },

    /**
     * Allows users view listing statistic
     */
    ListingFrontendStats("Listing frontend statistic") {

        @Override
        public String getDescription() {
            return "Show Listing Statistic at User's accounts";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Listings};
        }
    },

    ListingAttachments("Listing attachments") {

        @Override
        public String getDescription() {
            return "Upload attachments as part of the add listing process";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Listings};
        }

    },

    /**
     *
     */
    Languages() {

        @Override
        public String getDescription() {
            return "Transaction between contents on categories and items";
        }

    },

    Forum() {

    },

    Pricing() {

        @Override
        public String getDescription() {
            return "Use cost price, sell prices and VAT rates on items";
        }

    },
    Buddies() {

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.MemberProfiles};
        }
    },
    Nudges("Winks") {

        @Override
        public String getDescription() {
            return "Allow users to send a wink to each other, which just notifies the other user they have been contacted or noticed.";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.MemberProfiles};
        }
    },

    Buyers() {

        @Override
        public String getDescription() {
            return "Enable additional buyers to be tagged to a user account by the user, so they can control spending in a department, etc";
        }

    },

    OrderExport() {

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Shopping};
        }
    },

    /**
     * Member messages between members
     */
    PrivateMessages("Private messages") {

        @Override
        public String getDescription() {
            return "Users can send messages to other users through the site";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.MemberProfiles};
        }

    },
    MemberViewers("Member views") {

        @Override
        public String getDescription() {
            return "Shows users how many people have viewed their profile";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.MemberProfiles};
        }

    },
    Polls() {

        @Override
        public String getDescription() {
            return "Create polls for users to vote on";
        }
    },

    Feeds() {

        @Override
        public String getDescription() {
            return "Activate feeds for automatic import and export of item data";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Pricing};
        }
    },

    /**
     * Allows members to view / search profiles of other members.
     * Basically creates an interactive site.
     */
    MemberProfiles("Member profiles") {

        @Override
        public String getDescription() {
            return "Allow accounts to be viewed on the front end of the site";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Accounts};
        }

    },
    Banners() {

        @Override
        public boolean beta() {
            return true;
        }

    },
    Promotions() {

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Shopping};
        }

        @Override
        public boolean beta() {
            return true;
        }
    },
    Ratings() {

        @Override
        public boolean beta() {
            return true;
        }
    },
    Forms() {

        @Override
        public String getDescription() {
            return "Enable creation of HTML forms for feedback, etc";
        }
    },
    FormsReport("Forms Report Module") {

        @Override
        public String getDescription() {
            return "Enable sending reports of submission forms";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Forms};
        }
    },
    FormsExportHeaders() {

        @Override
        public String getDescription() {
            return "Enable creation CSV Header";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Forms};
        }
    },
    FormSubmissionRestriction("Form Submission IP Restrictions") {

        @Override
        public String getDescription() {
            return "Deny sending more than X form submissions from an IP per day";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Forms};
        }
    },
    Sms() {

        @Override
        public String getDescription() {
            return "SMS (Text message) bulletins";
        }
    },
    HolidayReminders("Holiday reminders") {

        @Override
        public String getDescription() {
            return "Holiday Reminders";
        }
    },
    Newsletter() {

        @Override
        public String getDescription() {
            return "Email newsletters";
        }

    },
    Wizards() {

        @Override
        public String getDescription() {
            return "Enable interactive search wizards for finding items by attribute";
        }

    },
    ListingModeration("Listing moderation") {

        @Override
        public String getDescription() {
            return "Allow listings to be moderated by an admin user before they appear on the site";
        }

    },
    ListingEdit("Listing edit") {

        @Override
        public String getDescription() {
            return "Allow listing owner to preview and alter not moderated listings";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Listings, Module.ListingModeration};
        }
    },
    AttachmentPreviews("Attachment previews") {

        @Override
        public String getDescription() {
            return "Create previews of attachments (MP3 only)";
        }

    },
    Delivery() {

        @Override
        public String getDescription() {
            return "Enable delivery options for shopping system";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Shopping};
        }

    },
    OrderHistory("Order history") {

        @Override
        public String getDescription() {
            return "Let customers see their order history, with invoice re-print, etc";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Shopping};
        }
    },
    QuickOrdering("Quick ordering") {

        @Override
        public String getDescription() {
            return "Allow customers to shop by entering many product codes directly into a form";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Shopping};
        }
    },

    GoogleMap("Google map") {

        @Override
        public String getDescription() {
            return "Shows locations of items as a block";
        }

        @Override
        public boolean beta() {
            return false;
        }

    },

    GoogleMapV3("Google map V3") {

        @Override
        public String getDescription() {
            return "Shows locations of items as a block";
        }

        @Override
        public boolean beta() {
            return true;
        }

    },

    MemberImages("Member images") {

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.MemberProfiles};
        }

    },
    Agreements() {

        @Override
        public String getDescription() {
            return "Enable agreements / terms to be used when registering, checking out, subscribing, adding a listing, etc";
        }
    },

    Licenses() {

        @Override
        public boolean beta() {
            return true;
        }
    },

    UserImages,

    ItemExport() {

    },

    UserplaneChat("Userplane chat") {

        @Override
        public String getDescription() {
            return "Enable the userplane.com webchat module";
        }

    },

    UserplaneRecorder, Login() {

    @Override
    public String getDescription() {
        return "Allow users to login through an account item type";
    }

},

    Accounts() {

        @Override
        public String getDescription() {
            return "Enable accounts that users can login to via their password";
        }
    },

    InstantBuy("Instant buy") {

        @Override
        public boolean beta() {
            return true;
        }
    },

    TellFriend("Tell a friend") {

        @Override
        public boolean beta() {
            return true;
        }
    },

    Relations() {

        @Override
        public boolean beta() {
            return true;
        }
    },

    CrmMailboxes("Crm Mailboxes") {

        @Override
        public boolean beta() {
            return true;
        }
    },

    TagClouds("Tag clouds") {

    },
    GoogleAdwords("Google adwords") {

        @Override
        public String getDescription() {
            return "Easy creation of adwords in a block or box";
        }

    },

    Reviews("Reviews") {

        @Override
        public boolean beta() {
            return true;
        }
    },
    Branches("Branches") {

        @Override
        public boolean beta() {
            return true;
        }
    },

    SearchFilters("Search filters"), SmsCredits() {

    @Override
    public boolean beta() {
        return true;
    }
},
    PriceWatch() {

        @Override
        public boolean beta() {
            return true;
        }
    },
    ExpiryBots() {

        @Override
        public boolean beta() {
            return true;
        }
    },
    Suppliers() {

        @Override
        public boolean beta() {
            return true;
        }
    },
    FilesRenameBots("Files rename bots") {

        @Override
        public String getDescription() {
            return "Allow users to rename image and pdf files to the actual name of the parent";
        }

        @Override
        public boolean beta() {
            return true;
        }
    },
    GoogleCheckout("Google Checkout Payment type") {

        @Override
        public boolean beta() {
            return true;
        }

        @Override
        public String getDescription() {
            return "Google Checkout Payment type. It requires one of Modules: Shopping, Listings, Subscriptions, Credits";
        }
    },
    PayPalExpress("PayPal Express Checkout") {

        @Override
        public String getDescription() {
            return "PayPal Express Payment type.";
        }

    },
    PayPalProExpress("PayPal Pro. Express Checkout") {

        @Override
        public String getDescription() {
            return "PayPal Payment Pro. Express Payment type.";
        }

    },
    PayPalProHosted("PayPal Pro Hosted") {

        @Override
        public String getDescription() {
            return "PayPal Payment Pro. Hosted Solution.";
        }

    },
    Ipoints("Ipoints module") {
        @Override
        public String getDescription() {
            return "Ipoints module";
        }
    },
    ManualSort("Manual sort") {

        @Override
        public String getDescription() {
            return "Manual sort in categories";
        }
    },
    StatsBlock("Stats center switch off") {
        @Override
        public String getDescription() {
            return "Module for switching off Stats center";
        }
    },
    EditInMass("Edit Item values") {

        @Override
        public String getDescription() {
            return "The ability to update item values through product list";
        }
    },
    Comments() {

        @Override
        public String getDescription() {
            return "The ability for visitors to a website to be able to leave comments on a category or an item";
        }

    },
    NoFollow() {

        @Override
        public String getDescription() {
            return "Stops the search engines following the link attribute value";
        }
    },
    TinyMCEStandard("TinyMCE Standard") {

        @Override
        public String getDescription() {
            return "Editor includes standard plugins and buttons that comes with the TinyMCE package.";
        }
    },
    TinyMCEFull("TinyMCE Full") {

        @Override
        public String getDescription() {
            return "Editor includes all plugins and buttons that comes with the TinyMCE package.";
        }
    },
    SocialNetworkFeed("Social Network Item Type Feed") {

        @Override
        public String getDescription() {
            return "Turn this on and have the ability to feed the contents of a listing to social network sites, Twitter, Facebook.";
        }
    },
    CategoryAndItemDiscount("Category And Item Discount") {

        @Override
        public String getDescription() {
            return " The facility to put a percentage against a category of products and to be able to take the percentage off, without adjusting the selling price itself";
        }
    },
    MultiItemsAddition("Multiple Items Addition") {

        @Override
        public String getDescription() {
            return "Let admin users get 5 empty items that can be filled in and updated all in one go";
        }

        @Override
        public boolean beta() {
            return true;
        }
    },
    SiteMap("Site map") {

        @Override
        public String getDescription() {
            return "Customised sitemap add on";
        }

        @Override
        public boolean beta() {
            return true;
        }
    },
    IBex("iBex. Internet Booking Exchange") {

        @Override
        public String getDescription() {
            return "Web based booking system";
        }

        @Override
        public boolean beta() {
            return true;
        }
    },
    LoginOuter("Login Outer") {

        @Override
        public String getDescription() {
            return "Outer login functionality";
        }

        @Override
        public boolean beta() {
            return true;
        }
    },
    Facebook("Facebook") {
        @Override
        public boolean beta() {
            return false;
        }
        @Override
        public String getDescription() {
            return "A Module required for all kinds of Facebook integration";
        }

    },
    FacebookApplications("Facebook Application") {
        @Override
        public boolean beta() {
            return true;
        }
        @Override
        public String getDescription() {
            return "It allows you to create a custom site template for facebook applications";
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Facebook};
        }
    },
    FacebookLogin("Facebook login") {

        @Override
        public String getDescription() {
            return "Facebook login inregration";
        }

        @Override
        public boolean beta() {
            return true;
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.LoginOuter};
        }
    },
    FacebookPhotos("Facebook Photos Likes") {

        @Override
        public String getDescription() {
            return "Facebook Photo Likes inregration";
        }

        @Override
        public boolean beta() {
            return true;
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Facebook};
        }
    },
    FacebookOpenGraphTags("Facebook Open Graph Tags") {
        @Override
        public boolean beta() {
            return true;
        }
        @Override
        public String getDescription() {
            return ""/*"It allows you to create a custom site template for facebook applications"*/;
        }

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Facebook};
        }
    },
    OrdersRemoval("Order Removal") {

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Shopping};
        }

        @Override
        public boolean beta() {
            return true;
        }

        @Override
        public String getDescription() {
            return "Customers can remove orders: completed, with a date range";
        }
    },
    ListingsFeaturedExclude("Exclude featured listings") {

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.Listings};
        }

        @Override
        public boolean beta() {
            return true;
        }
        
        @Override
        public String getDescription() {
            return "a module which allows you to exclude featured listings within the main category block" +
                    " so that if a listing is set to 'Featured' it does not display in the main category item block";
        }
    },
    GoogleAnalyticsTracking("GA: Tracking") {

        @Override
        Module[] requiresModule() {
            return new Module[]{};
        }

        @Override
        public boolean beta() {
            return true;
        }

        @Override
        public String getDescription() {
            return "Google Analytics Tracking";
        }
    },
    GATrackingCodeEcommerce("GA: Ecommerce Tracking") {

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.GoogleAnalyticsTracking};
        }

        @Override
        public boolean beta() {
            return true;
        }

        @Override
        public String getDescription() {
            return "Google Analytics can report ecommerce activity for your website";
        }
    },
    GATrackingOutboundLinks("GA: Outbound Clicks Tracking") {

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.GoogleAnalyticsTracking};
        }

        @Override
        public boolean beta() {
            return true;
        }

        @Override
        public String getDescription() {
            return "Google Analytics can track clicks on links that lead away from your site";
        }
    },
    GATrackingEmailLinks("GA: Email Link Clicks Tracking") {

        @Override
        Module[] requiresModule() {
            return new Module[]{Module.GoogleAnalyticsTracking};
        }

        @Override
        public boolean beta() {
            return true;
        }

        @Override
        public String getDescription() {
            return "Google Analytics can track clicks on links that lead away from your site";
        }
    },
    EmailLogo("Emails with LOGO") {

        @Override
        public boolean beta() {
            return true;
        }

        @Override
        public String getDescription() {
            return "Add LOGO to all the outgoing emails";
        }
    },
    CardsavePayment("Cardsave payment") {

        @Override
        public String getDescription() {
            return "Redirect/Hosted Cardsave payment";
        }

    },
    GoogleForm("Google Form Integration") {

        @Override
        public String getDescription() {
            return "Enable use of google form integration";
        }
    },
    CookieControl("Cookie Control") {
        @Override
        public String getDescription() {
            return "Cookie Control civicuk.com";
        }
    },
    CronStop("Cron stop") {
        @Override
        public String getDescription() {
            return "None of Cron tasks will be executed";
        }
    };

    private final String toString;

    private Module() {
        this.toString = name();
    }

    private Module(String toString) {
        this.toString = toString;
    }

    public boolean beta() {
        return false;
    }

    public final boolean enabled(RequestContext context) {
        return Modules.getInstance(context).getModules().contains(this);
    }

    public String getDescription() {
        return null;
    }

    Module[] requiresModule() {
        return null;
    }

    @Override
    public final String toString() {
        return toString;
    }

}
