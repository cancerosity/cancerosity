package org.sevensoft.ecreator.model.system.installer;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.superstrings.RandomHelper;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategorySettings;
import org.sevensoft.ecreator.model.categories.Category.SubcategoryOrdering;
import org.sevensoft.ecreator.model.categories.blocks.ItemsBlock;
import org.sevensoft.ecreator.model.containers.boxes.shortcuts.LatestNewsBoxShortcut;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.FormPreset;
import org.sevensoft.ecreator.model.crm.forms.blocks.FormBlock;
import org.sevensoft.ecreator.model.items.ItemTypePreset;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.ecreator.model.marketing.newsletter.blocks.NewsletterBlock;
import org.sevensoft.ecreator.model.marketing.newsletter.box.NewsletterBox;
import org.sevensoft.ecreator.model.marketing.sms.blocks.SmsBlock;
import org.sevensoft.ecreator.model.marketing.sms.box.SmsBox;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.search.forms.FieldType;
import org.sevensoft.ecreator.model.search.forms.SearchField;
import org.sevensoft.ecreator.model.search.forms.SearchForm;
import org.sevensoft.ecreator.model.search.forms.blocks.SearchBlock;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHitCounterDaily;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.modules.Modules;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 21 Oct 2006 08:43:58
 *
 */
public class DefaultInstaller {

	private final RequestContext	context;
	private MiscSettings		miscSettings;

	public DefaultInstaller(RequestContext context) {
		this.context = context;
		this.miscSettings = MiscSettings.getInstance(context);
	}

	public void install() {

		// enable default modules
		Modules modules = Modules.getInstance(context);

		modules.addModule(Module.Newsfeed);
		modules.addModule(Module.Forms);
		modules.addModule(Module.Galleries);
		modules.addModule(Module.Newsletter);
		modules.addModule(Module.Sms);

		miscSettings.setSimpleMode(true);
		miscSettings.save();

		{

			Category ourServices = new Category(context, "Our Services", null);
			new Category(context, "Test Category 1", ourServices);
			new Category(context, "Test Category 2", ourServices);

		}

		{

			Category aboutUs = new Category(context, "About Us", null);
			new Category(context, "Company History", aboutUs);
			Category contactDetails = new Category(context, "Contact Details", aboutUs);

			Form contactForm = FormPreset.ContactForm.create(context, "Contact form");
			new FormBlock(context, contactDetails, contactForm);

		}

		{

			// set home page to manual
			CategorySettings cs = CategorySettings.getInstance(context);
			cs.setSubcategoryOrdering(true);
			cs.save();

			Category root = Category.getRoot(context);
			root.setSubcategoryOrdering(SubcategoryOrdering.Manual);
			root.save();

		}

		// create news category
		{
			Category newsCategory = new Category(context, "News", null);
			newsCategory.setHidden(true);
			newsCategory.setAdvanced(true);
			newsCategory.save();

			// set items order
			ItemsBlock block = newsCategory.getItemsBlock();
			block.setSort(SortType.Newest);

		}

		// create newsletter category
		{

			Category newsletterCategory = new Category(context, "Newsletter", null);
			newsletterCategory.setHidden(true);
			newsletterCategory.setAdvanced(true);
			newsletterCategory.save();

			new NewsletterBlock(context, newsletterCategory, 0);

		}

		// create sms category
		{

			Category smsCategory = new Category(context, "Sms Bulletin", null);
			smsCategory.setHidden(true);
			smsCategory.setAdvanced(true);
			smsCategory.save();

			new SmsBlock(context, smsCategory, 0);

		}

		/// hidden search cat
		{
			Category searchCategory = new Category(context, "Search", null);
			searchCategory.setAdvanced(true);
			searchCategory.setHidden(true);
			searchCategory.save();

			SearchBlock block = new SearchBlock(context, searchCategory, 0);
			SearchForm form = block.getSearchForm();
			SearchField field = form.addInputField(FieldType.Keywords);
			field.setName("Keywords");
			field.save();

			field = form.addInputField(FieldType.Categories);
			field.setName("Categories");
			field.save();

		}

		ItemTypePreset.Listing.create(context);
		ItemTypePreset.NewsArticle.create(context);

		//		{
		//
		//			// create featured products pages
		//			Category featuredProducts = new Category(context, "Featured products");
		//			featuredProducts.setHidden(false);
		//			featuredProducts.save();
		//
		//			HighlightedItemsBlock block = new HighlightedItemsBlock(context, featuredProducts, 0);
		//
		//			Search search = block.getSearch();
		//			search.setLimit(20);
		//			search.setItemType(products);
		//			search.setMethod(HighlightMethod.Featured);
		//			search.save();
		//
		//		}
		//
		//		{
		//
		//			// create latest products pages
		//			Category latestProducts = new Category(context, "Latest products");
		//			latestProducts.setHidden(false);
		//			latestProducts.save();
		//
		//			HighlightedItemsBlock block = new HighlightedItemsBlock(context, latestProducts, 0);
		//
		//			Search search = block.getSearch();
		//			search.setLimit(20);
		//			search.setItemType(products);
		//			search.setMethod(HighlightMethod.Latest);
		//			search.save();
		//
		//		}

		// latest news sidebox
		new LatestNewsBoxShortcut().install(context, "Right");

		//		 create a sidebox for the newsletter
		new NewsletterBox(context, "Right");

		//		 create a sidebox for sms
		new SmsBox(context, "Right");

		// create back dated stats
		Date date = new Date();
		for (int n = 0; n < 31; n++) {

			int total = Math.abs(RandomHelper.getRandomNumber(200, 500));
			int unique = total / 3;

			SiteHitCounterDaily daily = new SiteHitCounterDaily(context, date);
			daily.setUnique(unique);
			daily.setTotal(total);
			daily.save();

			date = date.removeDays(1);
		}
	}
}
