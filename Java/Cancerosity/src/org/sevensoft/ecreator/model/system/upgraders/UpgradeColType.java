package org.sevensoft.ecreator.model.system.upgraders;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.ecreator.model.misc.content.ContentBlock;
import org.sevensoft.ecreator.model.items.Item;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
public class UpgradeColType {

    private RequestContext context;
    private String columnType;

    public UpgradeColType(RequestContext context, String columnType) {
		this.context = context;
        this.columnType = columnType;
    }

    public void run() {
        new Query(context, "ALTER TABLE # MODIFY content " + columnType + ";").setTable(ContentBlock.class).run();
        new Query(context, "ALTER TABLE # MODIFY contentstripped  " + columnType + ";").setTable(ContentBlock.class).run();
        new Query(context, "ALTER TABLE # MODIFY contentlinked  " + columnType + ";").setTable(ContentBlock.class).run();
    }

    public void runItem() {
        new Query(context, "ALTER TABLE # MODIFY content " + columnType + ";").setTable(Item.class).run();
        new Query(context, "ALTER TABLE # MODIFY contentstripped  " + columnType + ";").setTable(Item.class).run();
    }
}
