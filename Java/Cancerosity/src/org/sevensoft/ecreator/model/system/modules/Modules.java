package org.sevensoft.ecreator.model.system.modules;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Set;
import java.util.TreeSet;

/**
 * @author sks 22-Jul-2005 09:42:11
 */
@Table("settings_modules")
@Singleton
public class Modules extends EntityObject {

    private static final String COPYRIGHT = "Copyright \251 [site] [year]";

    private static final String POWERED_BY = "<a href=\"http://www.7soft.co.uk\">Web design</a> by 7Soft.co.uk";

    public static Modules getInstance(RequestContext context) {
        return getSingleton(context, Modules.class);
    }

    private TreeSet<Module> modules;
    private int categoryLimit;
    private int itemsLimit;
    private String copyrightMessage;
    private String poweredByMessage;

    public Modules(RequestContext context) {
        super(context);
    }

    public void addModule(Module module) {

        if (getModules().contains(module)) {
            return;
        }

        Module[] requires = module.requiresModule();
        if (requires != null) {
            for (Module require : requires) {
                addModule(require);
            }
        }

        getModules().add(module);
        save();
    }

    public int getCategoryLimit() {
        return categoryLimit;
    }

    public String getCopyrightMessage() {
        return copyrightMessage == null ? Modules.COPYRIGHT : copyrightMessage;
    }

    public String getCopyrightMessageRendered() {
        return getCopyrightMessage().replace("[site]", Company.getInstance(context).getName()).replace("[year]", String.valueOf(new Date().getYear()));
    }

    public int getItemsLimit() {
        return itemsLimit;
    }

    public final Set<Module> getModules() {

        if (modules == null) {
            modules = new TreeSet();
        }

        return modules;
    }

    public String getPoweredByMessage() {
        return poweredByMessage == null ? Modules.POWERED_BY : poweredByMessage;
    }

    public String getPoweredByMessageRendered() {
        return getPoweredByMessage();
    }

    public boolean hasCategoryLimit() {
        return categoryLimit > 0;
    }

    public boolean hasItemsLimit() {
        return itemsLimit > 0;
    }

    private boolean hasVettingListingPackage() {
        Query q = new Query(context, "select count(*) from # where vetting=1");
        q.setTable(ListingPackage.class);
        return q.getInt() > 0;
    }

    public void removeModule(Module module) {
        getModules().remove(module);
    }

    public void setCategoryLimit(int categoryLimit) {
        this.categoryLimit = categoryLimit;
    }

    public void setCopyrightMessage(String copyrightMessage) {
        this.copyrightMessage = copyrightMessage;
    }

    public void setItemsLimit(int itemsLimit) {
        this.itemsLimit = itemsLimit;
    }

    public void setPoweredByMessage(String poweredByMessage) {
        this.poweredByMessage = poweredByMessage;
    }

    @Override
    protected void singletonInit(RequestContext context) {

        addModule(Module.Pricing);
        addModule(Module.Newsletter);
        addModule(Module.Newsfeed);
        addModule(Module.Availabilitity);
        addModule(Module.Sms);
        addModule(Module.Polls);
        addModule(Module.Alternatives);
        addModule(Module.Forms);
        save();
    }

}
