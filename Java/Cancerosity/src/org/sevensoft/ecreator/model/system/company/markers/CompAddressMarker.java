package org.sevensoft.ecreator.model.system.company.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 03-Apr-2006 11:00:39
 *
 */
public class CompAddressMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Company.getInstance(context).hasAddress()) {
			return null;
		}

		return super.string(context, params, Company.getInstance(context).getAddress());

	}

	public Object getRegex() {
		return "comp_address";
	}
}