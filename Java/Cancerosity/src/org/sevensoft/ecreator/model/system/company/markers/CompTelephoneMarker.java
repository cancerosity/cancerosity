package org.sevensoft.ecreator.model.system.company.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 03-Apr-2006 11:44:41
 *
 */
public class CompTelephoneMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		Company company = Company.getInstance(context);
		if (!company.hasTelephone())
			return null;

		String prefix = params.get("prefix");
		String suffix = params.get("suffix");

		StringBuilder sb = new StringBuilder();
		if (prefix != null)
			sb.append(prefix);

		sb.append(Company.getInstance(context).getTelephone());

		if (suffix != null)
			sb.append(suffix);

		return sb.toString();
	}

	public Object getRegex() {
		return "comp_telephone";
	}
}