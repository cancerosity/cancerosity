package org.sevensoft.ecreator.model.system.users;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27-Oct-2004 16:21:38
 */
@Table("users_sessions")
public class UserSession extends EntityObject {

	@Index()
	private String	sessionId;

	private long	timestamp;

	@Index()
	private User	user;


    public static UserSession get(RequestContext context, String sessionId) {
        return SimpleQuery.get(context, UserSession.class, "sessionId", sessionId);
    }

	public UserSession(RequestContext context) {
		super(context);
	}

	public UserSession(RequestContext context, User user, String sessionId) {
		super(context);

		this.user = user;
		this.sessionId = sessionId;
		this.timestamp = System.currentTimeMillis();

		save();
	}

	public User getUser() {
		return user.pop();
	}


}
