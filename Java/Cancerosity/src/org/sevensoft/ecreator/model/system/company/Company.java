package org.sevensoft.ecreator.model.system.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author Sam 09-Sep-2003 15:32:56
 */
@Table("settings_site")
@Singleton
public class Company extends EntityObject {

	public static final int	E_EXEMPT			= 1;
	public static final int	FEB_MAY_AUG_NOV		= 2;
	public static final int	JAN_APRIL_JULY_OCT	= 1;
	public static final int	LTD				= 2;
	public static final int	MARCH_JUNE_SEP_DEC	= 3;
	public static final int	PARTNERSHIP			= 3;
	public static final int	R_REDUCED			= 1;
	public static final int	S_STANDARD			= 1;
	public static final int	SOLE_TRADER			= 1;
	public static final int	VAT_ACCURAL			= 2;
	public static final int	VAT_CASH			= 1;
	public static final int	Z_ZERO_RATED		= 1;

	public static Company getInstance(RequestContext context) {
		return getSingleton(context, Company.class);
	}

	private String	companyNumber, postcode, email, telephone, fax, address, name, vatNumber;
	private Country	country;

	public Company(RequestContext context) {
		super(context);
	}

	public String getAddress() {
		return address;
	}

	private String getAddress(String seperator) {
		return address == null ? null : address.replace("\n", seperator);
	}

	public String getAddressCommas() {
		return getAddress(", ");
	}

	public String getAddressLabel(String sep) {

		if (address == null)
			return null;

		StringBuilder sb = new StringBuilder();
		sb.append(getAddress(sep));

		if (postcode != null) {
			sb.append(sep);
			sb.append(postcode);
		}

		sb.append(sep);
		sb.append(getCountry().getName());

		return sb.toString();
	}

	public String getCompanyNumber() {
		return companyNumber;
	}

	public String getContactEmail() {
		return email;
	}

	public Country getCountry() {
		return country == null ? Country.UK : country;
	}

	public String getEmail() {
		return email;
	}

	public String getFax() {
		return fax;
	}

	public String getLabel(String sep) {

		List<String> list = new ArrayList();
		list.add(address);

		if (postcode != null)
			list.add(postcode);

		if (country != null)
			list.add(country.toString());

		if (telephone != null)
			list.add(telephone);

		if (email != null)
			list.add(email);

		return StringHelper.implode(list, sep);
	}

	public String getMultimapUrl() {
		return "http://www.multimap.com/map/browse.cgi?client=europe&pc=" + postcode + "&cname=Great+Britain";
	}

	public String getName() {
		return name == null ? "Your Site" : name;
	}

	public String getName(int i) {
		return StringHelper.toSnippet(name, i);
	}

	public String getPostcode() {
		return postcode;
	}

	public String getTelephone() {
		return telephone;
	}

	public String getVatNumber() {
		return vatNumber;
	}

	public boolean hasAddress() {
		return address != null;
	}

	public boolean hasCompanyNumber() {
		return companyNumber != null;
	}

	public boolean hasEmail() {
		return email != null;
	}

	public boolean hasFax() {
		return fax != null;
	}

	public boolean hasPostcode() {
		return postcode != null;
	}

	public boolean hasTelephone() {
		return telephone != null;
	}

	public boolean isVatRegistered() {
		return vatNumber != null;
	}

	public void setAddress(String s) {
		address = (s == null ? null : s.trim());
	}

	public void setCompanyNumber(String s) {
		companyNumber = (s == null ? null : s.trim());
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public void setEmail(String s) {
		this.email = (s == null ? null : s.trim());
	}

	public void setFax(String s) {
		this.fax = (s == null ? null : s.trim());
	}

	public void setName(String s) {
		this.name = (s == null ? null : s.trim());
	}

	public void setPostcode(String s) {
		this.postcode = (s == null ? null : s.trim().toUpperCase());
	}

	public void setTelephone(String s) {
		this.telephone = (s == null ? null : s.trim());
	}

	public void setVatNumber(String s) {
		this.vatNumber = (s == null ? null : s.trim());
	}

	@Override
	protected void singletonInit(RequestContext context) {
		this.email = "sales@yourdomain.com";
		this.name = "Your Site";
		this.telephone = "01234 567890";
		this.address = "Number 2, Your Street, Smallville, Countyshire";
		this.postcode = "TS19 7SN";
		this.fax = null;
		this.country = Country.UK;
	}

	/**
	 * 
	 */
	public String getGoogleMapUrl() {
		return "http://www.google.co.uk/maps?q=" + postcode;
	}

    public Locale getLocale() {
        return new Locale(getCountry().getLanguage(), getCountry().getIsoAlpha2(), "");
    }

}