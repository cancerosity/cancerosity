package org.sevensoft.ecreator.model.system.messages;

import java.util.List;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 18 Aug 2006 17:33:35
 *
 */
@Table("system_messages")
public class SystemMessage extends EntityObject {

	public static List<SystemMessage> get(RequestContext context, int limit) {
		Query q = new Query(context, "select * from # order by id desc");
		q.setTable(SystemMessage.class);
		return q.execute(SystemMessage.class, limit);
	}

	private String	content;
	private DateTime	date;

	private SystemMessage(RequestContext context) {
		super(context);
	}

	public SystemMessage(RequestContext context, String content) {
		super(context);

		this.date = new DateTime();
		this.content = content;

		save();
	}

	public String getContent() {
		return content;
	}

	public DateTime getDate() {
		return date;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

}
