package org.sevensoft.ecreator.model.system.config;

import org.sevensoft.commons.superstrings.english.Pluraliser;
import org.sevensoft.ecreator.model.design.marker.MarkerRenderer;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 09-Oct-2005 19:32:12
 * 
 */
@Table("settings_captions")
@Singleton
public class Captions extends EntityObject {

	public static Captions getInstance(RequestContext context) {
		return getSingleton(context, Captions.class);
	}

	private String	checkoutCaption;

	/**
	 * The message that is displayed when you add an item to a basket
	 */
	private String	addBasketItemCaption;

	/**
	 * The message that is displayed when you clear the basket
	 */
	private String	emptyBasketCaption;

	/**
	 * The message that is displayed when the user clicks update on their basket
	 */
	private String	basketUpdatedCaption;

	/**
	 * 
	 */
	private String	accountCaption;

	/**
	 * 
	 */
	private String	categoriesCaption;

	/**
	 * 
	 */
	private String	deliveryOptionsCaption;

	private String	loginPageCaption, registrationPageCaption;

	/**
	 * 
	 */
	private String	chatCaption;

	/**
	 */
	private String	buddiesCaption;

	/**
	 */
	private String	imagesCaption;

	/**
	 */
	private String	messagesCaption;

	/**
	 */
	private String	nudgesCaption;

	/**
	 */
	private String	viewsCaption;

	private String	checkoutDeliveryTitle;

	private String	shoppingBasketCaption;

	private String	downloadsCaption;

	private String	orderHistoryCaption;

	private String	bidsCaption;

	private String	myBidsCaption;

	private String	myReturnsCaption;

	private String	newsletterCaption;

	private String	subscriptionCaption;

	private String	attachmentsCaption;

	private String	buddiesWhoCaptionPlural;

	private String	eCardCaption;

	private String	basketClearedCaption;

	private String	buyersCaption;

	private String	creditsCaption;

	private String	branch;

    private String  orderStatusCaption;

    private String favouritesCaption;

    private String listingsCaption;

	public Captions(RequestContext context) {
		super(context);
	}

	public String getAccountCaption() {
		return accountCaption == null ? "My Account" : accountCaption;
	}

	public String getAddBasketItemCaption() {
		return addBasketItemCaption == null ? "You have added '[item?link=1]' to your basket. Please <a href='javascript:history.go(-1)' title='go back'>click here</a> to continue shopping."
				: addBasketItemCaption;
	}

	public String getAddBasketItemCaptionRendered(RequestContext context, Item item) {

		String string = getAddBasketItemCaption();
		string = MarkerRenderer.render(context, string, item);

		return string;
	}

	public String getAddedToBuddiesCaptionPlural() {
		return buddiesWhoCaptionPlural == null ? "Members who have added me" : buddiesWhoCaptionPlural;
	}

	public String getAttachmentsCaption() {
		return attachmentsCaption == null ? "Attachment" : attachmentsCaption;
	}

	public String getAttachmentsCaptionPlural() {
		return Pluraliser.toPlural(getAttachmentsCaption());
	}

	public String getBasketClearedCaption() {
		return basketClearedCaption == null ? "Your basket has been cleared" : basketClearedCaption;
	}

	public String getBasketUpdatedCaption() {
		return basketUpdatedCaption == null ? "Basket updated" : basketUpdatedCaption;
	}

	public String getBidsCaption() {
		return bidsCaption == null ? "Bid" : bidsCaption;
	}

	public String getBidsCaptionPlural() {
		return Pluraliser.toPlural(getBidsCaption());
	}

	public final String getBranch() {
		return branch;
	}

	/**
	 * 
	 */
	public String getBranchCaption() {
		return branch == null ? "Branch" : branch;
	}

	public String getBuddiesCaption() {
		return buddiesCaption == null ? "Buddy" : buddiesCaption;
	}

	public String getBuddiesCaptionPlural() {
		return Pluraliser.toPlural(getBuddiesCaption());
	}

	public String getBuyersCaption() {
		return buyersCaption == null ? "Additional buyers" : buyersCaption;
	}

	public String getBuyersCaptionPlural() {
		return Pluraliser.toPlural(getBuyersCaption());
	}

	public final String getCategoriesCaption() {
		return categoriesCaption == null ? "Category" : categoriesCaption;
	}

	public final String getCategoriesCaptionPlural() {
		return Pluraliser.toPlural(getCategoriesCaption());
	}

	public String getChatCaption() {
		return chatCaption == null ? "Instant Chat" : chatCaption;
	}

	public String getChatCaptionPlural() {
		return Pluraliser.toPlural(getChatCaption());
	}

	public final String getCheckoutCaption() {
		return checkoutCaption == null ? "Checkout" : checkoutCaption;
	}

	public String getCheckoutDeliveryTitle() {
		return checkoutDeliveryTitle == null ? "Checkout - Delivery options" : checkoutDeliveryTitle;
	}

	public String getCreditsCaption() {
		return creditsCaption == null ? "Credit" : creditsCaption;
	}

	public String getCreditsCaptionPlural() {
		return Pluraliser.toPlural(getCreditsCaption());
	}

	public String getDeliveryOptionsCaption() {
		return deliveryOptionsCaption == null ? "Delivery Option" : deliveryOptionsCaption;
	}

	public String getDeliveryOptionsCaptionPlural() {
		return Pluraliser.toPlural(getDeliveryOptionsCaption());
	}

	public String getDownloadsCaption() {
		return downloadsCaption == null ? "My downloads" : downloadsCaption;
	}

	public String getDownloadsCaptionPlural() {
		return Pluraliser.toPlural(getDownloadsCaption());
	}

	public String getECardCaption() {
		return eCardCaption == null ? "eCard" : eCardCaption;
	}

	public String getECardCaptionPlural() {
		return Pluraliser.toPlural(getECardCaption());
	}

	public String getEmptyBasketCaption() {
		return emptyBasketCaption == null ? "Your basket is empty" : emptyBasketCaption;
	}

	public String getImagesCaption() {
		return imagesCaption == null ? "Image" : imagesCaption;
	}

	public String getImagesCaptionPlural() {
		return Pluraliser.toPlural(getImagesCaption());
	}

	public final String getLoginPageCaption() {
		return loginPageCaption == null ? "Login" : loginPageCaption;
	}

	public String getMyBidsCaption() {
		return myBidsCaption == null ? "My bids" : myBidsCaption;
	}

	public String getMyReturnsCaption() {
		return myReturnsCaption == null ? "My returns" : myReturnsCaption;
	}

	public String getMySavedSearchesCaption() {
		return "My saved searches";
	}

	public String getNewsletterCaption() {
		return newsletterCaption == null ? "Newsletter" : newsletterCaption;
	}

	public String getOrderHistoryCaption() {
		return orderHistoryCaption == null ? "Order history" : orderHistoryCaption;
	}

    public String getOrderStatusCaption() {
        return orderStatusCaption == null ? "Order status" : orderStatusCaption;
    }

	public String getPmCaption() {
		return messagesCaption == null ? "Member Message" : messagesCaption;
	}

	public String getPmCaptionPlural() {
		return Pluraliser.toPlural(getPmCaption());
	}

	public final String getRegistrationPageCaption() {
		return registrationPageCaption == null ? "Register" : registrationPageCaption;
	}

	public final String getShoppingBasketCaption() {
		return shoppingBasketCaption == null ? "Shopping basket" : shoppingBasketCaption;
	}

	public String getSidebarSearchCaption() {
		return "Quick search";
	}

	public String getSubscriptionCaption() {
		return subscriptionCaption == null ? "Subscription" : subscriptionCaption;
	}

	public String getSubscriptionCaptionPlural() {
		return Pluraliser.toPlural(getSubscriptionCaption());
	}

	public String getViewsCaption() {
		return viewsCaption == null ? "Viewer" : viewsCaption;
	}

	public String getViewsCaptionPlural() {
		return Pluraliser.toPlural(getViewsCaption());
	}

	public String getWinksCaption() {
		return nudgesCaption == null ? "Nudge" : nudgesCaption;
	}

	public String getWinksCaptionPlural() {
		return Pluraliser.toPlural(getWinksCaption());
	}

    public String getFavouritesCaption() {
        return favouritesCaption == null ? "Wishlist" : favouritesCaption;
    }

    public String getListingsCaption() {
        return listingsCaption== null ? "Listing" : listingsCaption;
    }

    public void setAccountCaption(String accountCaption) {
		this.accountCaption = accountCaption;
	}

	public void setAddBasketItemCaption(String addBasketItemCaption) {
		this.addBasketItemCaption = addBasketItemCaption;
	}

	public void setAttachmentsCaption(String attachmentsCaption) {
		this.attachmentsCaption = attachmentsCaption;
	}

	public final void setBasketClearedCaption(String basketClearedCaption) {
		this.basketClearedCaption = basketClearedCaption;
	}

	public void setBasketUpdatedCaption(String basketUpdatedCaption) {
		this.basketUpdatedCaption = basketUpdatedCaption;
	}

	public void setBidsCaption(String bidsCaption) {
		this.bidsCaption = bidsCaption;
	}

	public final void setBranch(String branch) {
		this.branch = branch;
	}

	public void setBuddiesCaption(String buddiesCaption) {
		this.buddiesCaption = buddiesCaption;
	}

	public final void setBuddiesWhoCaptionPlural(String buddiesWhoCaptionPlural) {
		this.buddiesWhoCaptionPlural = buddiesWhoCaptionPlural;
	}

	public final void setBuyersCaption(String buyersCaption) {
		this.buyersCaption = buyersCaption;
	}

	public final void setCategoriesCaption(String categoriesCaption) {
		this.categoriesCaption = categoriesCaption;
	}

	public void setChatCaption(String chatCaption) {
		this.chatCaption = chatCaption;
	}

	public final void setCheckoutCaption(String checkoutCaption) {
		this.checkoutCaption = checkoutCaption;
	}

	public void setCheckoutDeliveryTitle(String checkoutDeliveryTitle) {
		this.checkoutDeliveryTitle = checkoutDeliveryTitle;
	}

	public final void setCreditsCaption(String creditsCaption) {
		this.creditsCaption = creditsCaption;
	}

	public void setDeliveryOptionsCaption(String deliveryOptionsCaption) {
		this.deliveryOptionsCaption = deliveryOptionsCaption;
	}

	public void setDownloadsCaption(String downloadsCaption) {
		this.downloadsCaption = downloadsCaption;
	}

	public final void setECardCaption(String cardCaption) {
		eCardCaption = cardCaption;
	}

	public void setEmptyBasketCaption(String emptyBasketCaption) {
		this.emptyBasketCaption = emptyBasketCaption;
	}

	public void setImagesCaption(String imagesCaption) {
		this.imagesCaption = imagesCaption;
	}

	public final void setLoginPageCaption(String loginPageCaption) {
		this.loginPageCaption = loginPageCaption;
	}

	public void setMessagesCaption(String messagesCaption) {
		this.messagesCaption = messagesCaption;
	}

	public void setMyBidsCaption(String myBidsCaption) {
		this.myBidsCaption = myBidsCaption;
	}

	public void setMyReturnsCaption(String myReturnsCaption) {
		this.myReturnsCaption = myReturnsCaption;
	}

	public void setNudgesCaption(String nudgesCaption) {
		this.nudgesCaption = nudgesCaption;
	}

	public void setOrderHistoryCaption(String orderHistoryCaption) {
		this.orderHistoryCaption = orderHistoryCaption;
	}

    public void setOrderStatusCaption(String orderStatusCaption) {
        this.orderStatusCaption = orderStatusCaption;
    }

	public final void setRegistrationPageCaption(String registrationPageCaption) {
		this.registrationPageCaption = registrationPageCaption;
	}

	public final void setShoppingBasketCaption(String shoppingBasketCaption) {
		this.shoppingBasketCaption = shoppingBasketCaption;
	}

	public void setSubscriptionCaption(String subscriptionCaption) {
		this.subscriptionCaption = subscriptionCaption;
	}

	public void setViewsCaption(String viewsCaption) {
		this.viewsCaption = viewsCaption;
	}

    public void setFavouritesCaption(String favouritesCaption) {
        this.favouritesCaption = favouritesCaption;
    }

    public void setListingsCaption(String listingsCaption) {
        this.listingsCaption = listingsCaption;
    }
}