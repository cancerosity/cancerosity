package org.sevensoft.ecreator.model.system.users;

import java.util.logging.Logger;

import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 17 Aug 2006 17:28:55
 *
 * Deletes all sessions older than a week.
 */
public class UserSessionBot implements Runnable {

	private Logger	logger	= Logger.getLogger("cron");

	private RequestContext	context;

	public UserSessionBot(RequestContext context) {
		this.context = context;
	}

	public void run() {

		logger.config("[UserSessionRunner] user sessions reaper");

		Query q = new Query(context, "delete from # where timestamp<?");
		q.setTable(UserSession.class);
		q.setParameter(System.currentTimeMillis() - (1000l * 60 * 60 * 24 * 7));
		q.run();

	}

}
