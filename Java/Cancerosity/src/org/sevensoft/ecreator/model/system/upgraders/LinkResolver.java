package org.sevensoft.ecreator.model.system.upgraders;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author sam
 * 
 *         This class will change all old style local links to links on the new site.
 * 
 *         Will change both absolute and relative urls into relative urls
 * 
 * 
 */
public class LinkResolver {

	private String	string;

	public String getString() {
		return string;
	}

	public void resolve() {

		Pattern p = Pattern.compile("href\\s*?=\\s*?(\"|')?http://www.peakdistrictonline.co.uk/?", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(string);

		string = m.replaceAll("href='/");

	}

	public void setString(String string) {
		this.string = string;
	}
}
