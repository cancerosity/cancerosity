package org.sevensoft.ecreator.model.system.config;

import java.io.InputStream;
import java.util.Properties;

public class MissingUrlUtil {
	
	private static final Properties URL_ENTRIES = new Properties();
	
	//load urls
	static {
		try {
			InputStream in = MissingUrlUtil.class.getResourceAsStream("/MissingUrls.properties");
			URL_ENTRIES.load(in);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * if the url is a missing url,return the redirect url
	 * if not a missing,return null
	 * @return 
	 */
	public static String isMissingUrl(String url){
		 return URL_ENTRIES.get(url)  == null ? null : URL_ENTRIES.get(url).toString();
	}
}
