package org.sevensoft.ecreator.model.system.company.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.EmailTag;

/**
 * @author sks 03-Apr-2006 11:44:45
 *
 */
public class CompEmailMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		Company company = Company.getInstance(context);
		if (!company.hasEmail())
			return null;

		boolean link = params.containsKey("link");
		if (link) {
			return super.string(context, params, new EmailTag(company.getEmail()));
		} else {
			return super.string(context, params, company.getEmail());
		}
	}

	public Object getRegex() {
		return "comp_email";
	}
}