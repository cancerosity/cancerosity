package org.sevensoft.ecreator.model.system.upgraders;

import java.util.Arrays;
import java.util.Collection;

import junit.framework.TestCase;

/**
 * @author Stephen K Samuel samspade79@gmail.com 24 Oct 2009 10:46:29
 * 
 */
public class AddressSlurperTest extends TestCase {

	private Collection<String>	towns		= Arrays.asList(new String[] { "qq", "BUXton", "DoVEDale", "34534%", "ASHWELL", "matlock" });

	private String			title		= "Peak District Cottages - Cotterill Farm Cottages - Biggin by Hartington - Buxton";
	private String			address	= "Cotterill Farm, Biggin-By-Hartington, Buxton, Derbyshire";
	private String			title2	= "Peak District Cottages - The Barn at Smalldale Hall - Nr Castleton";
	private String			address2	= "Smalldale, Bradwell, Nr Castleton, Derbyshire";
	private String			desc		= "<span style=\"font-size: x-small; font-family: verdana,geneva;\"><br />Superbly situated very close to the beautiful valley of Dovedale in a lovely part of the Peak District National Park, the four cottages are located between the highly acclaimed village of Hartington and its smaller neighbour, Biggin-by-Hartington. <br /></span><span style=\"font-size: x-small; font-family: verdana,geneva;\"><br />Cotterill Farm is in the heart of the white Peak area of the peak district with the attractive market towns of Bton, Ashbourne, Bakewell and Leek all 11 miles away at various points of the compass. The NON-working farm consists of 12 acres of land, with two wild flower meadows and is surrounded by a nature reserve through which a footpath leads on its way to the River Dove. There are three cycle trails on average 15 miles length within 3 miles and there is excellent walking from the cottages with numerous footpaths and bridleways heading off in all directions. <br /></span><span style=\"font-size: x-small; font-family: verdana,geneva;\"><br />Our cottages have consistently achieved the grading of 4 Stars from the English Tourism Council (and under the old grading system 'Highly Commended'), but despite this we have constantly striven to improve our facilities. <br /></span><span style=\"font-size: x-small; font-family: verdana,geneva;\">";

	public void testAddress() {

		AddressSlurper s = new AddressSlurper();
		s.setTowns(towns);

		// should find buxton from address
		String town = s.getTown(title, address, desc);
		assertEquals("BUXton", town);

		// should find buxton from title
		town = s.getTown(title2, address, desc);
		assertEquals("BUXton", town);

		// should find buxton from title
		town = s.getTown(title, address2, desc);
		assertEquals("BUXton", town);

		// should find dovedale from description
		town = s.getTown(title2, address2, desc);
		assertEquals("DoVEDale", town);

		// should find dovedale from description
		town = s.getTown(null, null, desc);
		assertEquals("DoVEDale", town);

		// should find nothing 
		town = s.getTown(null, null, "crpa crap crap");
		assertEquals(null, town);
	}
}
