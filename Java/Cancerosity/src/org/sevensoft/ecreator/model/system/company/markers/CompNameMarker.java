package org.sevensoft.ecreator.model.system.company.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 03-Apr-2006 11:42:24
 *
 */
public class CompNameMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {
		return Company.getInstance(context).getName();
	}

	public Object getRegex() {
		return "comp_name";
	}
}