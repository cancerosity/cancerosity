package org.sevensoft.ecreator.model.system.upgraders;

import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.logging.Logger;

/**
 * User: MeleshkoDN
 * Date: 10.09.2007
 * Time: 10:11:10
 */
public class Upgrade7to8 {
    private static final Logger logger = Logger.getLogger("ecreator");

    private RequestContext context;

    public Upgrade7to8(RequestContext context) {
        this.context = context;
    }

    public void updateBookingAttributes() {
        ItemType itemType = (new Query(context, "select * from # where name='Accommodation'")).setTable(ItemType.class).get(ItemType.class);
        Attribute directBooking = itemType.addAttribute("Direct booking", AttributeType.Boolean);
        directBooking.setDisplayable(false);
        directBooking.setDefaultNew("true");
        directBooking.save();
        Attribute thirdPartyUrl = itemType.addAttribute("3rd party URL", AttributeType.Text);
        thirdPartyUrl.setDisplayable(false);
        thirdPartyUrl.save();
        Attribute comission = itemType.addAttribute("Commission", AttributeType.Text);
        comission.setDisplayable(false);
        comission.setDefaultNew("10");
        comission.save();
    }

    private void updateEventAttributes() {
        ItemType itemType = (new Query(context, "select * from # where name='Event'")).setTable(ItemType.class).get(ItemType.class);
        Attribute enableRecurrence = itemType.addAttribute("Recurrence", AttributeType.Boolean);
        enableRecurrence.setDefaultNew("false");
        enableRecurrence.save();
        Attribute repeatTerm = itemType.addAttribute("Repeat term", AttributeType.Numerical);
        repeatTerm.save();
        Attribute repeatOption = itemType.addAttribute("Repeat option", AttributeType.Text);
        repeatOption.save();
        Attribute nextStartDate = itemType.addAttribute("Next date start", AttributeType.Date);
        nextStartDate.save();
        Attribute nextEndDate = itemType.addAttribute("Next date end", AttributeType.Date);
        nextEndDate.save();
    }

    public void upgradePaymentSettings() {
        PaymentSettings paymentSettings = PaymentSettings.getInstance(context);
        paymentSettings.addPaymentType(PaymentType.ManualPayments);
        paymentSettings.save();
    }

    public void run() {
//        updateBookingAttributes();
//        upgradePaymentSettings();
        updateEventAttributes();
    }


}
