package org.sevensoft.ecreator.model.system.installer;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.accounts.login.boxes.LoginBox;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeOwner;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategoryModule;
import org.sevensoft.ecreator.model.categories.CategorySettings;
import org.sevensoft.ecreator.model.categories.box.CategoryBox;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.containers.boxes.misc.CustomBox;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.marketing.newsletter.box.NewsletterBox;
import org.sevensoft.ecreator.model.marketing.sms.box.SmsBox;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.search.forms.FieldType;
import org.sevensoft.ecreator.model.search.forms.SearchField;
import org.sevensoft.ecreator.model.search.forms.SearchForm;
import org.sevensoft.ecreator.model.search.forms.boxes.ItemSearchBox;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.modules.Modules;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author sks 3 Nov 2006 14:06:56
 */
public class Installer {

    private Logger logger = Logger.getLogger("ecreator");

    private final RequestContext context;
    private final Modules modules;
    private final Config config;

    public Installer(RequestContext context) {
        this.context = context;
        this.modules = Modules.getInstance(context);
        this.config = Config.getInstance(context);
    }

    private void attribute(AttributeOwner owner, Element element) {

        String attributeName = element.getChildTextTrim("name");
        AttributeType type = AttributeType.valueOf(element.getChildTextTrim("type"));

        Attribute attribute = owner.addAttribute(attributeName, type);
        if (element.getChild("width") != null) {

            int width = Integer.parseInt(element.getChildTextTrim("width"));
            attribute.setWidth(width);
        }

        if (element.getChild("height") != null) {

            int height = Integer.parseInt(element.getChildTextTrim("height"));
            attribute.setHeight(height);
        }

        attribute.save();
    }

    private void block(Category category, Element e) {

        String type = e.getAttributeValue("type");

        if ("highlighted-items".equalsIgnoreCase(type)) {

            // I only want one set of highlighted items on the front page

            //			HighlightedItemsBlock block = new HighlightedItemsBlock(context, category, 0);
            //			String method = e.getChildText("method");
            //			ItemType itemType = ItemType.getByName(context, e.getChildTextTrim("itemtype"));
            //			String limit = e.getChildText("limit");
            //
            //			if (method != null) {
            //
            //				Search search = block.getSearch();
            //
            //				search.setMethod(HighlightMethod.valueOf(method));
            //				search.setItemType(itemType);
            //
            //				if (limit != null) {
            //					try {
            //						search.setLimit(Integer.parseInt(limit));
            //					} catch (NumberFormatException e1) {
            //						e1.printStackTrace();
            //					}
            //				}
            //
            //				search.save();
            //			}

        } else if ("ticker".equalsIgnoreCase(type)) {

        }
    }

    private void box(Element element) {

        String type = element.getAttributeValue("type");
        String name = element.getChildTextTrim("name");
        String caption = element.getChildTextTrim("caption");
        boolean border = "true".equals(element.getChildTextTrim("border"));
        String location = element.getChildTextTrim("location");
        String content = element.getChildTextTrim("content");
        // String css = element.getChildTextTrim("css");
        String cssId = element.getChildTextTrim("css-id");
        String cssClass = element.getChildTextTrim("css-class");
        boolean simpleEditor = "true".equals(element.getChildText("simple-editor"));

        logger.config("[Installer] box type=" + type + ", name=" + name + ", location=" + location);

        if (location == null)
            location = "left";

        // wipe all existing boxes

        Box box = null;
        if (type.equalsIgnoreCase("custom")) {

            CustomBox customBox = new CustomBox(context, location);
            customBox.setContent(content);
            box = customBox;

        } else if (type.equalsIgnoreCase("login")) {

            LoginBox loginBox = SimpleQuery.get(context, LoginBox.class);
            if (loginBox == null)
                loginBox = new LoginBox(context, location);
            box = loginBox;

        } else if (type.equalsIgnoreCase("newsletter")) {

            NewsletterBox newsletterBox = SimpleQuery.get(context, NewsletterBox.class);
            if (newsletterBox == null)
                newsletterBox = new NewsletterBox(context, location);
            box = newsletterBox;

        } else if (type.equalsIgnoreCase("sms")) {

            SmsBox smsBox = SimpleQuery.get(context, SmsBox.class);
            if (smsBox == null)
                smsBox = new SmsBox(context, location);

            box = smsBox;

        } else if (type.equalsIgnoreCase("search")) {

            ItemSearchBox searchBox = new ItemSearchBox(context, location);

            // process form fields from form element
            List<Element> forms = element.getChildren("form");
            for (Element e : forms) {
                searchForm(searchBox, e);
            }

            box = searchBox;

        } else if (type.equalsIgnoreCase("highlighted-items")) {

            //			HighlightedItemsBox highlightedBox = new HighlightedItemsBox(context, location);
            //
            //			String method = element.getChildText("method");
            //			ItemType itemType = null;
            //			String itemTypeName = element.getChildTextTrim("itemtype");
            //			if (itemTypeName != null)
            //				itemType = ItemType.getByName(context, itemTypeName);
            //
            //			String limit = element.getChildText("limit");
            //
            //			if (method != null) {
            //
            //				Search search = highlightedBox.getSearch();
            //
            //				search.setMethod(HighlightMethod.valueOf(method));
            //				search.setItemType(itemType);
            //
            //				if (limit != null) {
            //					try {
            //						search.setLimit(Integer.parseInt(limit));
            //					} catch (NumberFormatException e1) {
            //						e1.printStackTrace();
            //					}
            //				}
            //
            //				search.save();
            //			}
            //
            //			box = highlightedBox;

        } else if (type.equalsIgnoreCase("highlighted-members")) {

            //			HighlightedMembersBox itemsBox = new HighlightedMembersBox(context, location);
            //			box = itemsBox;

        } else if (type.equalsIgnoreCase("categories")) {

            CategoryBox categoryBox = SimpleQuery.get(context, CategoryBox.class);
            if (categoryBox == null)
                categoryBox = new CategoryBox(context, location);

            box = categoryBox;

        }

        if (box != null) {

            if (name != null)
                box.setName(name);

            if (caption != null)
                box.setCaptionText(caption);

            box.setBorder(border);
            box.setSimpleEditor(simpleEditor);
            box.setCssClass(cssClass);
            box.setCssId(cssId);

            // markup
            {
                String markupName = element.getChildTextTrim("markup");
                logger.config("[Installer] box markupName=" + markupName);

                Markup markup = Markup.getByName(context, markupName);
                if (markup != null) {

                    box.setMarkup(markup);

                } else {

                    logger.config("[Installer] markup not located");

                }
            }

            box.save();
        }

    }

    /**
     *
     */
    private void category(Element element) {

        String name = element.getChildTextTrim("name");
        logger.config("[Installer] category name=" + name);

        Category parent = null;
        if (element.getChild("parent") != null) {
            parent = Category.getByName(context, element.getChildTextTrim("parent"));
        }

        logger.config("[Installer] parent=" + parent);

        Category category;
        // check for existing cat
        if (name.equalsIgnoreCase("root")) {

            category = Category.getRoot(context);
            logger.config("[Installer] using root");

        } else {

            category = Category.getByName(context, name);
            if (category == null) {
                category = new Category(context, name, parent);
                logger.config("[Installer] created new category=" + category);
            } else {

                logger.config("[Installer] located category=" + category);
            }
        }

        if (element.getChild("content") != null) {

            category.getContentBlock().setContent(element.getChildTextTrim("content"));
            category.getContentBlock().save();
        }

        if (element.getChild("image") != null) {

            CategorySettings cs = CategorySettings.getInstance(context);
            cs.addModule(CategoryModule.Images);
            cs.save();

            // move image to images folder
            String image = element.getChildTextTrim("image");

            logger.config("[Installer] adding image to category filename=" + image);

            File src = ResourcesUtils.getRealTemplateData(image);
            logger.config("[Installer] image file=" + src);

            if (src.exists()) {

                try {

//					File dest = context.getRealFile("images/" + image);
                    File dest = ResourcesUtils.getRealImage(image);

                    SimpleFile.move(src, dest);
                    category.addImage(image, true, true, false);

                } catch (IOException e1) {
                    e1.printStackTrace();

                } catch (ImageLimitException e1) {
                    e1.printStackTrace();
                }
            }
        }

        if (element.getChild("hidden") != null) {
            boolean hidden = "true".equalsIgnoreCase(element.getChildTextTrim("hidden"));
            category.setHidden(hidden);
            category.save();
        }

        // process blocks on this category
        List<Element> elements = element.getChildren("block");
        for (Element e : elements) {
            block(category, e);
        }
    }

    private void config(Element element) {

        //		boolean advanced = "false".equalsIgnoreCase(element.getChildTextTrim("jimmode"));

    }

    public void install(File file) throws JDOMException, IOException {

        logger.config("[Installer] installing from file=" + file);

        String data = SimpleFile.readString(file);

        Document doc = new SAXBuilder().build(new StringReader(data));
        Element root = doc.getRootElement();

        logger.config(root.toString());

        // modules
        List<Element> elements = root.getChildren("markup");
        for (Element element : elements) {
            markup(element);
        }

        // modules
        elements = root.getChildren("module");
        for (Element element : elements) {
            module(element);
        }

        // categories
        elements = root.getChildren("category");
        for (Element element : elements) {
            category(element);
        }

        // boxes
        elements = root.getChildren("box");
        for (Element element : elements) {
            box(element);
        }

        // general config
        elements = root.getChildren("config");
        for (Element element : elements) {
            config(element);
        }
    }

    private void markup(Element element) {

        String name = element.getAttributeValue("name");
        if (name == null)
            return;

        logger.config("[Installer] markup name=" + name);

        String caption = element.getChildTextTrim("caption");
        String body = element.getChildTextTrim("body");
        String start = element.getChildTextTrim("start");
        String end = element.getChildTextTrim("end");
        String between = element.getChildTextTrim("between");
        String tds = element.getChildTextTrim("tds");
        String tableId = element.getChildTextTrim("table-id");
        String cssClass = element.getChildTextTrim("table-class");
        String cycleRows = element.getChildTextTrim("cycle-rows");

        Markup markup = new Markup(context, name);
        logger.config("[Installer] markup created=" + markup);

        markup.setBody(body);
        markup.setEnd(end);
        markup.setStart(start);
        markup.setBetween(between);
        markup.setTableId(tableId);
        markup.setTableClass(cssClass);
        markup.setCaption(caption);
        if (tds != null) {
            try {
                markup.setTds(Integer.parseInt(tds));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        if (cycleRows != null) {
            try {
                markup.setCycleRows(Integer.parseInt(cycleRows));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        markup.save();
    }

    private void module(Element element) {

        logger.config("[Installer] module");

        try {

            Module module = Module.valueOf(element.getValue());
            modules.addModule(module);
            modules.save();

        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private void searchForm(ItemSearchBox searchBox, Element e) {

        logger.config("[Installer] searchForm");

        SearchForm form = searchBox.addSearchForm();
        String submitImage = e.getChildTextTrim("submit-image");
        form.setSubmitSrc(submitImage);

        String submitLabel = e.getChildTextTrim("submit-label");
        form.setSubmitLabel(submitLabel);

        String itemTypeText = e.getChildTextTrim("itemtype");
        if (itemTypeText != null) {
            ItemType itemType = ItemType.getByName(context, itemTypeText);
            form.setItemType(itemType);
        }

        List<Element> elements = e.getChildren("field");
        for (Element element : elements) {
            searchFormField(form, element);
        }

        form.save();
    }

    private void searchFormField(SearchForm form, Element e) {

        FieldType type = FieldType.valueOf(e.getAttributeValue("type"));
        String name = e.getChildTextTrim("name");

        SearchField field = form.addInputField(type);
        field.setName(name);

        String ranges = e.getChildText("ranges");
        if (ranges != null) {
            field.setRanges(StringHelper.explodeStrings(ranges, "\\D"));
        }

        field.save();
    }
}
