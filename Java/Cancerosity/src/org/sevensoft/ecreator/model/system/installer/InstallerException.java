package org.sevensoft.ecreator.model.system.installer;


/**
 * @author sks 14 Nov 2006 18:41:59
 *
 */
public class InstallerException extends Exception {

	public InstallerException() {
	}

	public InstallerException(String message) {
		super(message);
	}

	public InstallerException(Throwable cause) {
		super(cause);
	}

	public InstallerException(String message, Throwable cause) {
		super(message, cause);
	}

}
