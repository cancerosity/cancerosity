package org.sevensoft.ecreator.model.system.company.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 03-Apr-2006 11:44:45
 *
 */
public class CountryMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {
		return Company.getInstance(context).getCountry().toString();
	}

	public Object getRegex() {
		return "comp_country";
	}
}