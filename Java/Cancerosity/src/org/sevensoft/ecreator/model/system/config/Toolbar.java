package org.sevensoft.ecreator.model.system.config;

import org.sevensoft.ecreator.iface.admin.AdminLoginHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.AbstractTag;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 6 May 2006 09:11:32
 */
public class Toolbar {

    private static final String ControlPanelGif = "/files/graphics/admin/toolbar_controlpanel.gif";
    private static final String ControlPanelHoverGif = "/files/graphics/admin/toolbar_controlpanel_hover.gif";
    private static final String FrontPageGif = "/files/graphics/admin/toolbar_frontpage.gif";
    private static final String FrontPageHoverGif = "/files/graphics/admin/toolbar_frontpage_hover.gif";
    private static final String EditGif = "/files/graphics/admin/toolbar_edit.gif";
    private static final String EditHoverGif = "/files/graphics/admin/toolbar_edit_hover.gif";
    private static final String ValidateGif = "/files/graphics/admin/toolbar_validate.gif";
    private static final String ValidateHoverGif = "/files/graphics/admin/toolbar_validate_hover.gif";
    private static final String ViewOnSiteGif = "/files/graphics/admin/toolbar_viewsite.gif";
    private static final String ViewOnSiteHoverGif = "/files/graphics/admin/toolbar_viewsite_hover.gif";

    private User user;
    private Config config;
    private Company company;
    private Category category;
    private Item item;
    private String view;
    private final RequestContext context;

    public Toolbar(User user, RequestContext context) {

        this.user = user;
        this.context = context;
        this.config = Config.getInstance(context);
        this.company = Company.getInstance(context);

        this.item = (Item) context.getAttribute("item");
        this.category = (Category) context.getAttribute("category");
        if (context.containsAttribute("view")) {
            view = context.getAttribute("view").toString();
        }

    }

    /**
     *
     */
    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        sb.append(new TableTag("cpbar", "center").setId("cp_toolbar"));
        sb.append("<tr>\n<td class='cpleft' valign='middle'>");
        sb.append("<span>");

        if (config.isDemo()) {
            sb.append("Demo site: ");
        } else if (config.isBuild()) {
            sb.append("Build site: ");
        }

        sb.append(company.getName());

        if (MiscSettings.getInstance(context).isOffline()) {
            sb.append(" (SITE HIDDEN)");
        }

        sb.append("</span> &nbsp; &nbsp; ");

        if (user == null) {

            sb.append("You are not logged in");

        } else {

            sb.append("You are logged in as " + user.getName() + " " +
                    new LinkTag(context.getContextPath() + "/" + new Link(AdminLoginHandler.class, "logout"), "Logout"));
            sb.append(" &nbsp; ");

        }

        if (User.isSuperIp(context)) {

            sb.append(" [superman] [");

            //			if (User.isSuperman(context)) {
            //
            //				sb.append(new LinkTag(context.getContextPath() + "/" + new Link(DashboardHandler.class, "supermanOff"), "on"));
            //
            //			} else {
            //
            //				sb.append(new LinkTag(context.getContextPath() + "/" + new Link(DashboardHandler.class, "supermanOn"), "off"));
            //			}
            //
            //			sb.append("]");

            sb.append(context.getServerIp());

            String server = Servers.getServerName(context.getServerIp());
            if (server != null) {
                sb.append(" " + server);
            }

            sb.append("] [Ecreator 8." + context.getServletAttribute("version") + "] ");
        }

        sb.append("</td>\n");
        sb.append("<td align='right' class='cpright' valign='middle'>");

        if (item != null) {

            if (User.isSuperIp(context)) {

                AbstractTag imageTag = new ImageTag(context.getContextPath() + ValidateGif).setAlign("middle");
                imageTag.setOnMouseOver("this.src='" + context.getContextPath() + ValidateHoverGif + "'");
                imageTag.setOnMouseOut("this.src='" + context.getContextPath() + ValidateGif + "'");

                LinkTag linkTag = new LinkTag("http://validator.w3.org/check?uri=" + item.getUrl(), imageTag);
                sb.append(linkTag);
            }

            {
                AbstractTag imageTag = new ImageTag(context.getContextPath() + EditGif).setAlign("middle");
                imageTag.setOnMouseOver("this.src='" + context.getContextPath() + EditHoverGif + "'");
                imageTag.setOnMouseOut("this.src='" + context.getContextPath() + EditGif + "'");

                LinkTag linkTag = new LinkTag(context.getContextPath() + "/" + new Link(ItemHandler.class, "edit"), imageTag, "item", item);
                sb.append(linkTag);
            }
        }

        if (category != null) {

            if (User.isSuperIp(context)) {

                AbstractTag imageTag = new ImageTag(context.getContextPath() + ValidateGif).setAlign("middle");
                imageTag.setOnMouseOver("this.src='" + context.getContextPath() + ValidateHoverGif + "'");
                imageTag.setOnMouseOut("this.src='" + context.getContextPath() + ValidateGif + "'");

                LinkTag linkTag = new LinkTag("http://validator.w3.org/check?uri=" + category.getUrl(), imageTag);
                sb.append(linkTag);
            }

            {

                AbstractTag imageTag = new ImageTag(context.getContextPath() + EditGif).setAlign("middle");
                imageTag.setOnMouseOver("this.src='" + context.getContextPath() + EditHoverGif + "'");
                imageTag.setOnMouseOut("this.src='" + context.getContextPath() + EditGif + "'");

                LinkTag linkTag = new LinkTag(context.getContextPath() + "/" + new Link(CategoryHandler.class, "edit"), imageTag, "category", category);
                sb.append(linkTag);

            }
        }

        if (view != null) {

            AbstractTag imageTag = new ImageTag(context.getContextPath() + ViewOnSiteGif).setAlign("middle");
            imageTag.setOnMouseOver("this.src='" + context.getContextPath() + ViewOnSiteHoverGif + "'");
            imageTag.setOnMouseOut("this.src='" + context.getContextPath() + ViewOnSiteGif + "'");

            LinkTag linkTag = new LinkTag(view, imageTag);
            sb.append(linkTag);
        }

        // FRONT PAGE
        {
            AbstractTag imageTag = new ImageTag(context.getContextPath() + FrontPageGif).setAlign("middle");
            imageTag.setOnMouseOver("this.src='" + context.getContextPath() + FrontPageHoverGif + "'");
            imageTag.setOnMouseOut("this.src='" + context.getContextPath() + FrontPageGif + "'");

            LinkTag linkTag = new LinkTag(context.getContextPath() + "/", imageTag);
            sb.append(linkTag);
        }

        // CP
        {
            AbstractTag imageTag = new ImageTag(context.getContextPath() + ControlPanelGif).setAlign("middle");
            imageTag.setOnMouseOver("this.src='" + context.getContextPath() + ControlPanelHoverGif + "'");
            imageTag.setOnMouseOut("this.src='" + context.getContextPath() + ControlPanelGif + "'");

            LinkTag linkTag = new LinkTag(context.getContextPath() + "/" + new Link(DashboardHandler.class), imageTag);
            sb.append(linkTag);
        }

        sb.append("</td></tr>\n");
        sb.append("</table>\n\n");

        return sb.toString();
    }
}
