package org.sevensoft.ecreator.model.system.users.privileges;

import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 11 Jul 2006 15:53:25
 *
 */
public enum Privilege {

	ViewOrder("View order") {

		@Override
		public String getDescription() {
			return "Allow this user to view order details";
		}
	},

	EditOrder("Edit order") {

		@Override
		public String getDescription() {
			return "Allow this user to edit order details";
		}
	},

	EditMember("Edit member") {

		@Override
		public String getDescription() {
			return "Allow this user to edit member details";
		}
	},

	ViewMember("View member") {

		@Override
		public String getDescription() {
			return "Allow this user to view member details";
		}
	},

	ViewOwnerItems(),

	ViewAllItems(),

	ViewCreatedItems(),

	Reports() {
	},

	CreateItem("Create item") {

		@Override
		public String getDescription() {
			return "Allow this user to create new items";
		}
	},

	CreateCategory("Create category") {

		@Override
		public String getDescription() {
			return "Allow this user to create new categories";
		}
	},

	EditAllItems("Edit all items") {

		@Override
		public String getDescription() {
			return "Allow this user to edit items";
		}

	},

	EditOwnerItems(),

	EditCreatedItems(),

	EditCategory("Edit category") {
	},

	DeleteItem("Delete item") {
	},

	DeleteCategory("Delete category") {

		@Override
		public String getDescription() {
			return "Allow this user to delete categories";
		}

	},

	OrderPayment() {
	},

	CardDetails() {
	},

	ComposeNewsletter() {

		@Override
		public String getDescription() {
			return "Allow this user to compose and send newsletters";
		}

	},

	EditSms() {
	},

	ComposeSms() {
	};

	private final String	toString;

	Privilege() {
		this.toString = name();
	}

	Privilege(String toString) {
		this.toString = toString;
	}

	public String getDescription() {
		return null;
	}

	public final boolean hasDescription() {
		return getDescription() != null;
	}

	@Override
	public String toString() {
		return toString;
	}

	public final boolean yes(User user, RequestContext context) {

		// if privileges are disabled then the user has rights to everything
		if (!Module.Privileges.enabled(context))
			return true;

		// if the user is an admin then they have rights to all
		if (user.isAdministrator())
			return true;

		// otherwise check for presence of priviledge
		return user.hasPrivilege(this);
	}
}
