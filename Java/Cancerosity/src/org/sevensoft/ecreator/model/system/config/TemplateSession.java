package org.sevensoft.ecreator.model.system.config;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.design.template.Template;

/**
 * User: Tanya
 * Date: 11.10.2012
 */
@Table("templates_session")
public class TemplateSession extends EntityObject {

    private String sessionId;
    private Template template;

    public static TemplateSession get(RequestContext context, String sessionId) {
        TemplateSession session = getExistent(context, sessionId);
        if (session == null) {
            session = new TemplateSession(context, sessionId);
        }
        return session;
    }

    public static TemplateSession getExistent(RequestContext context, String sessionId) {
        return SimpleQuery.get(context, TemplateSession.class, "sessionId", sessionId);
    }

    public TemplateSession(RequestContext context) {
        super(context);
    }

    public TemplateSession(RequestContext context, String sessionId) {
        super(context);
        this.sessionId = sessionId;
        save();
    }

    public Template getTemplate() {
        return template == null ? null : (Template) template.pop();
    }

    public void setTemplate(Template template) {
        this.template = template;
        save("template");
    }

}
