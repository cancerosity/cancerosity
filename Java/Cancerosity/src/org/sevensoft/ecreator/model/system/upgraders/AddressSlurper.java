package org.sevensoft.ecreator.model.system.upgraders;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Stephen K Samuel samspade79@gmail.com 24 Oct 2009 10:43:31
 * 
 *         This class is designed to detect the town of a listing from the details in the address, title, or description - in that order. The field
 *         will be checked against a given list of towns
 * 
 */
public class AddressSlurper {

	private Collection<String>	towns	= new ArrayList();

	public void setTowns(Collection<String> c) {
		this.towns = c;
	}

	public String getTown(String title, String address, String description) {

		// first check address
		if (address != null) {
			address = address.toLowerCase();
			for (String town : towns) {
				if (address.contains(town.toLowerCase())) {
					return town;
				}
			}
		}

		// next check title
		if (title != null) {
			title = title.toLowerCase();
			for (String town : towns) {
				if (title.contains(town.toLowerCase())) {
					return town;
				}
			}
		}

		// finally check description
		if (description != null) {
			description = description.toLowerCase();
			for (String town : towns) {
				if (description.contains(town.toLowerCase())) {
					return town;
				}
			}
		}

		return null;
	}

}
