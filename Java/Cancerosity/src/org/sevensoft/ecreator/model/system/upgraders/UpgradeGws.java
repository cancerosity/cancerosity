package org.sevensoft.ecreator.model.system.upgraders;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategoryModule;
import org.sevensoft.ecreator.model.categories.CategorySettings;
import org.sevensoft.ecreator.model.categories.blocks.SubcategoriesBlock;
import org.sevensoft.ecreator.model.categories.box.CategoryBox;
import org.sevensoft.ecreator.model.categories.deleters.CategoryExterminator;
import org.sevensoft.ecreator.model.ecom.shopping.box.BasketBox;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.ItemTypePreset;
import org.sevensoft.ecreator.model.items.advanced.ItemExterminator;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.marketing.newsletter.Subscriber;
import org.sevensoft.ecreator.model.marketing.newsletter.box.NewsletterBox;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.misc.content.ContentBlock;
import org.sevensoft.ecreator.model.search.forms.boxes.ItemSearchBox;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.modules.Modules;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.Row;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Required database columns
 * <p/>
 * <p/>
 * categories categories_contents categories_products category_headersfooters content_texts products products_prices newsletter
 * <p/>
 * and the title tags one !
 * 
 * @author sks 02-Aug-2005 14:20:36
 */
public class UpgradeGws {

	private static final int	RootId	= 100000;

	/**
	 * db prefix
	 */
	private final String		prefix;

	/**
	 * Flags if we should import images too
	 */
	private boolean			images;

	private Config			config;

	/**
	 * Url to copy images from
	 */
	private final String		url;

	private Logger			logger	= Logger.getLogger("ecreator");

	private final RequestContext	context;

	public UpgradeGws(RequestContext context, String prefix, String url, boolean images) {

		this.context = context;
		context.disableCache();
		this.prefix = prefix;

		this.images = images;
		this.config = Config.getInstance(context);

		if (url != null) {

			if (!url.startsWith("http"))
				url = "http://" + url;

			if (url.endsWith("/"))
				url = url.substring(0, url.length() - 1);
		}

		this.url = url;

		logger.config(url);
		logger.config("[UpgradeGws] images=" + images);
		logger.config("[UpgradeGws] prefix=" + prefix);
	}

	// public void checkGwsContent() {
	//
	// System.out.println("Checking GWS content");
	//
	// Query q = new Query("select categoryId, content from " + this.prefix + "categories_contents c join " + this.prefix
	// + "content_texts t on c.typeId=t.textId where c.type='text'");
	// Map<String, String> category_contents = q.getMap(String.class, String.class);
	//
	// System.out.println(category_contents.size());
	//
	// for (Category category : Category.get()) {
	//
	// if (category.getContent() == null || category.getContent().equals("") || category.getContent().equals("<br/><br/>")) {
	//
	// String content = category_contents.get(category.getIdString());
	// if (content != null && content.length() > 10) {
	//
	// content = contentReplacement(content);
	//
	// System.out.println("Found new content for " + category.getId() + " " + category.getName());
	//
	// category.setContent(content);
	// category.save();
	// }
	// }
	// }
	// }

	private void categories() {

		logger.config("[UpgradeGws] deleting all existing categories");

		// wipe all existing categories
		new CategoryExterminator(context).delete();

		logger.config("[UpgradeGws] moving root to id: " + RootId);

		/*
		 * move ID of root to 100000 to avoid potential clashes GWS had a special value of 0 for root as it was a pseudo category, whereas root is
		 * now a category proper.
		 */
		Query q = new Query(context, "update # set id=?");
		q.setTable(Category.class);
		q.setParameter(RootId);
		q.run();

		// create maps for header / footers
		Map<Integer, String> headers = new HashMap<Integer, String>();
		Map<Integer, String> footers = new HashMap<Integer, String>();

		// get all headers / footers and put into a hashmap mapped against the category id
		try {

			logger.config("[UpgradeGws] retrieving footer / header cache");

			q = new Query(context, "select categoryId, cheader, cfooter from " + prefix + "category_headersfooters");
			for (Row row : q.execute()) {

				int id = row.getInt(0);

				{

					String header = row.getString(1);
					if (header != null) {

						header = new IntercodeProcessor(context).process(header);
						header = header.trim();

						if (header.length() > 2) {
							logger.config("[gws] header found for cat: " + id);
							headers.put(id, header);
						}
					}
				}

				{

					String footer = row.getString(2);
					if (footer != null) {

						footer = new IntercodeProcessor(context).process(footer);
						footer = footer.trim();

						if (footer.length() > 2) {
							logger.config("[gws] footer found for cat: " + id);
							footers.put(id, footer);
						}
					}

				}
			}

		} catch (RuntimeException e) {
			e.printStackTrace();
		}

		logger.config("[UpgradeGws] footers size: " + footers.size());
		logger.config("[UpgradeGws] headers size: " + headers.size());

		if (headers.size() > 0 || footers.size() > 0) {

			// if we have headers or footers definied then ensure we enable headers on site.
			CategorySettings cs = CategorySettings.getInstance(context);
			cs.addModule(CategoryModule.Headers);
			cs.save();
		}

		logger.config("[UpgradeGws] retreiving all categories");

		// get all categories and create them
		q = new Query(context, "select c.categoryId, c.parentId, c.name, t.content from " + prefix + "categories c left outer join " + prefix
				+ "categories_contents cc on c.categoryId=cc.categoryId left outer join " + prefix
				+ "content_texts t on cc.typeId=t.textId group by c.categoryId");

		int n = 0;
		for (Row row : q.execute()) {

			int categoryId = row.getInt(0);
			if (categoryId == 0)
				continue;

			int parentId = row.getInt(1);
			String name = row.getString(2);
			String content = row.getString(3);

			logger.fine("processing category name=" + name + ", id=" + categoryId + ", parent=" + parentId);

			// the GWS database does not store empty strings as nulls, so if the content is less than 2 we want this to be null.
			if (content != null) {

				content = content.trim();
				if (content.length() < 2)
					content = null;
				else
					content = new IntercodeProcessor(context).process(content);
			}

			String header = headers.get(categoryId);
			String footer = footers.get(categoryId);

			logger.fine("[UpgradeGws] header=" + header);
			logger.fine("[UpgradeGws] footer=" + footer);

			/*
			 * If the parent is zero, then we must replace that parent with the root Id
			 */
			if (parentId == 0) {
				parentId = RootId;
				logger.fine("[UpgradeGws] replacing 0 parentid with root");
			}

			/*
			 * remove z index from category names if present12/10/06 - turns out hte Z index can be anything, in any case!, so now stripping out
			 * anything in angle brackets
			 */
			name = name.replaceAll("<.{1,3}>", "");

			// create category and give same id as before
			q = new Query(context, "insert into # (id, name, fullName, parent) values (?, ?, ?, ?)");
			q.setTable(Category.class);

			q.setParameter(categoryId);
			q.setParameter(name);

			// set full name as just name for now, the init dependancy sweep will sort it properly later
			q.setParameter(name);
			q.setParameter(parentId);
			q.run();

			// retreive category to set remaining values via method call
			Category category = EntityObject.getInstance(context, Category.class, categoryId);

			if (category == null) // error saving, should not error
				throw new RuntimeException("BUG category is null: " + categoryId);

			logger.fine("category retrieved for further processing name=" + category.getName() + ", category=" + category);

			// create subcategories block and content block
			new ContentBlock(context, category, 0);
			new SubcategoriesBlock(context, category, 0);

			if (header != null)
				category.setHeader(header);

			if (footer != null)
				category.setFooter(footer);

			category.save();

			if (content != null) {
				logger.config("setting category content cat=" + categoryId + ", length=" + content.length() + " bytes");
				category.getContentBlock().setContent(content);
			}

			n++;
		}

		logger.config("categories processed: " + n);

		// for all categories created we need to set a position. Easiest way is to set to the id then they are all unique.
		logger.config("updating category positions, setting all cats to pos=id");
		new Query(context, "update # set position=id").setTable(Category.class).run();

		// re init categories
		Category root = Category.getRoot(context);
		logger.config("re init cat dependancies root=" + root);
		root.doDependancies();

		// get special content for root page and set content
		q = new Query(context, "select t.content from " + prefix + "content_texts t join " + prefix
				+ "categories_contents cc on t.textId=cc.typeId where cc.categoryId=0");
		String content = q.getString();
		if (content != null) {

			content = content.trim();
			content = new IntercodeProcessor(context).process(content);
			if (content.length() < 2)
				content = null;
		}

		if (content != null) {
			logger.config("setting root content to: " + content.length() + " bytes");
			logger.fine(content);
			root.getContentBlock().setContent(content);
		}

		// delete orphaned categories - strange ones that seem to languish nowhere on gws (possibly not deleted when parent was deleted)
		// they will have a parent count of 0 because our system did not found them on the init dependancy sweep, but will have a parent set
		// to a non existant cat
		new Query(context, "delete from # where parentCount=0 and parent>0").setTable(Category.class).run();
		logger.config("[UpgradeGws] orphaned cats deleted");
	}

	private void categoryTitleTags() {

		logger.config("[UpgradeGws] retrieving tags");

		Query q = new Query(context, "select categoryId, pagetitle, pagedesc, pagekeywords from " + prefix + "titletags");
		for (Row row : q.execute()) {

			Category category;
			if (row.getInt(0) == 0)
				category = Category.getRoot(context);
			else
				category = row.getObject(0, Category.class);

			if (category != null) {

				String titleTag = row.getString(1);
				if (titleTag != null)
					category.setTitleTag(titleTag);

				String descTag = row.getString(2);
				if (descTag != null)
					category.setDescriptionTag(descTag);

				String keywords = row.getString(3);
				if (keywords != null)
					category.setKeywords(keywords);

				category.save();

			}
		}
	}

	private void images(Item item, String... images) {

		for (String image : images) {

			try {

				item.addImage(image, true, false, false);
				logger.config("[UpgradeGws] image added from local filesystem");

			} catch (IOException e) {

				logger.config("[UpgradeGws] image not found locally");

				logger.fine("null==" + url + " ? " + (null == url));

				// if file not found then we'll try to download directly from the old site, if the url to their site was provided.
				if (url != null) {

					String imageUrl = url + "/images/" + image;
					logger.config("[UpgradeGws] attempting to download image from: " + imageUrl);

					try {

						String filename = Img.download(context, imageUrl);

						if (filename != null) {
							logger.config("[UpgradeGws] Image downloaded to: " + filename);
							item.addImage(filename, true, false, false);
						}

					} catch (IllegalArgumentException e1) {
						logger.config(e1.toString());

					} catch (IOException e1) {
						logger.config(e1.toString());

					} catch (ImageLimitException e1) {
						logger.config(e1.toString());
					}
				}

			} catch (ImageLimitException e) {
				e.printStackTrace();
			}
		}
	}

	private void news() {

		// create the news item type

		ItemType itemType = ItemType.getByName(context, "News");
		if (itemType == null) {
			itemType = ItemTypePreset.NewsArticle.create(context);
		}

		logger.config("[UpgradeGws] news itemtype=" + itemType);
		Attribute authorAttribute = itemType.getAttribute("Author");

		Query q = new Query(context, "select poster, date, newsHeadline, news from " + prefix + "news");
		for (Row row : q.execute()) {

			try {

				String poster = row.getString(0);
				DateTime date = new DateTime(row.getString(1), "yyyy-MM-dd HH:mm");
				String headline = row.getString(2);
				String content = row.getString(3);

				Item item = new Item(context, itemType, headline, "Live", null);
				item.setContent(content);
				item.setDateCreated(date);
				item.save();

				item.setAttributeValue(authorAttribute, poster);

			} catch (ParseException e) {
				e.printStackTrace();
			}

		}

	}

	private void newsletter() {

		logger.config("[UpgradeGws] doNewsletter");

		List<Newsletter> newsletters = Newsletter.get(context);

		/*
		 * Add emails to default newsletter
		 */
		Query q = new Query(context, "select sEmail from " + prefix + "newsletter");
		for (Row row : q.execute()) {

			String email = row.getString(0);
			if (email != null) {

				email = email.trim();

				if (email.length() > 2) {
					logger.config("[UpgradeGws]Adding email: " + email);
				}

				Subscriber sub = Subscriber.get(context, email, true);

				sub.subscribe(newsletters);

			}
		}
	}

	private void options() {

		logger.config("[UpgradeGws] processing product options");

		// get all options froom prefix_productoptionlist table
		Query q = new Query(context, " select nid, nproductid, stitle, stype from " + prefix + "productoptionlist");
		for (Row row : q.execute()) {

			int id = row.getInt(0);
			Item item = EntityObject.getInstance(context, Item.class, row.getInt(1));
			String name = row.getString(2);
			boolean optional = "o".equalsIgnoreCase(row.getString(3));

			logger.config("[UpgradeGws] option row: id=" + id + ", item=" + item + ", name=" + name + ", optional=" + optional);

			if (item == null || name == null)
				continue;

			ItemType itemType = item.getItemType();
			itemType.addModule(ItemModule.Options);
			itemType.save();

			ItemOption option = item.getOptionSet().addOption(name);
			option.setType(ItemOption.Type.Selection);
			option.setOptional(optional);
			option.save();

			// get options for this option

			Query q2 = new Query(context, " select sdescription, npriceadjustment from " + prefix + "productoptionlistitems where nProductOptionListId=?");
			q2.setParameter(id);
			for (Row row2 : q2.execute()) {

				String text = row2.getString(0);
				Money price = new Money(row2.getDouble(1) * 100);

				ItemOptionSelection selection = option.addSelection(text);
				if (selection != null) {
					selection.setSalePrice(price);
					selection.save();
				}
			}
		}
	}

	private void products() {

		logger.config("[gws] processing products");

		Modules modules = Modules.getInstance(context);
		modules.addModule(Module.Shopping);
		modules.addModule(Module.Pricing);
		modules.addModule(Module.Delivery);
		modules.addModule(Module.Availabilitity);
		modules.save();

		/*
		 * Delete existing items and types
		 */
		logger.config("[gws] wiping existing items and item types");
		new ItemExterminator(context, null).delete();
		new Query(context, "delete from #").setTable(ItemType.class).run();

		/*
		 * Create a generic item type called products for now Really, GWS is used for listing and products but there was never any distinction -
		 * if we call them products, we can always rename them later to something more appropriate !
		 */
		ItemType itemType = new ItemType(context, "Product");
		Attribute weightAttribute = itemType.getAttribute("Weight");

		// build a map of all product -> category mappings
		MultiValueMap<Integer, Category> categoryProductsMap = new MultiValueMap(new HashMap());
		Query q = new Query(context, "select productId, categoryId from " + prefix + "categories_products");
		for (Row row : q.execute()) {

			int productId = row.getInt(0);
			int categoryId = row.getInt(1);

			Category category;
			if (categoryId == 0) {
				category = Category.getRoot(context);
			} else {
				category = EntityObject.getInstance(context, Category.class, categoryId);
			}

			if (category != null) {
				categoryProductsMap.put(productId, category);
			}
		}

		logger.config("categoryProductsMap size: " + categoryProductsMap.size());

		// get product info and create
		q = new Query(context, "select productId, name, description, rrp, vatRate, discontinued, image1, image2, leadtime, buy, weight from " + prefix
				+ "products");

		int n = 0;
		for (Row row : q.execute()) {

			final int id = row.getInt(0);
			String name = row.getString(1);
			String description = row.getString(2);
			if (description != null) {
				description = description.trim();

				// the GWS database does not store empty strings as nulls, so if the content is less than 2 this is probably a null value
				// really.
				if (description.length() < 2) {

					logger.fine("description length is 0 or 1, assuming null");
					description = null;

				} else {

					description = new IntercodeProcessor(context).process(description);

				}
			}

			Money rrp = new Money(row.getDouble(3) * 100);
			logger.config("rrp=" + rrp);

			double vatRate = row.getDouble(4);
			logger.config("vatRate=" + vatRate);

			boolean discontinued = row.getBoolean(5);
			logger.config("discontinued=" + discontinued);

			/*
			 * GWS stores 2 images, 1 and 2.
			 */
			String image1 = row.getString(6);
			if (image1 != null) {
				image1 = image1.trim();
				if (image1.length() < 2)
					image1 = null;
			}
			logger.config("image1=" + image1);

			String image2 = row.getString(7);
			if (image2 != null) {
				image2 = image2.trim();
				if (image2.length() < 2)
					image2 = null;
			}
			logger.config("image2=" + image2);

			String leadtime = row.getString(8);

			// 12/10/05 found some sites with html in leadtime so strip it out.
			if (leadtime != null)
				leadtime = leadtime.replaceAll("<.*?>", "");
			if (leadtime != null)
				leadtime = leadtime.trim();

			// if leadtime is less than 2 then just null it, same as for description
			if (leadtime != null && leadtime.length() < 2)
				leadtime = null;

			logger.config("[gws] leadtime=" + leadtime);

			// buy flag determines if the product is orderable via basket
			boolean buy = row.getBoolean(9);
			logger.config("[gws] buy=" + buy);

			double weight;
			try {
				weight = row.getDouble(10);
			} catch (RuntimeException e) {
				weight = 0;
			}
			logger.config("[gws] weight=" + weight);

			// get price for product from separate prices table
			q = new Query(context, "select price from " + prefix + "products_prices where productId=?");
			q.setParameter(id);
			String price = q.getString();

			logger.config("price=" + price);

			// insert the item directly into database to keep existing id.
			q = new Query(context, "insert into # (id, itemType, status, name) values (?, ?, ?, 'new item') ");
			q.setTable(Item.class);
			q.setParameter(id);
			q.setParameter(itemType);
			q.setParameter(discontinued ? "Disabled" : "Live");
			q.run();

			// retrieve product to set other values properly through methods
			Item item = EntityObject.getInstance(context, Item.class, id);

			if (item == null) {
				throw new RuntimeException("Item is null: " + id);
			}

			if (description != null) {
				item.setContent(description);
			}

			item.setName(name);

			/*
			 * vat rate was 1.175 in GWS. So if the vat rate is > 0 then set it to our standard rate rather than using the rate in GWS. We
			 * should only need 17.5 and 0 anyway
			 */
			if (vatRate == 0) {
				item.setVatRate(0, true);
			} else {
				item.setVatRate(17.5, true);
			}

			if (leadtime != null) {

				/*
				 * Enable stock if not already
				 */
				if (!itemType.getModules().contains(ItemModule.Stock)) {

					itemType.addModule(ItemModule.Stock);
					itemType.save();

					logger.config("enabled stock module");

				}

				item.setOutStockMsg(leadtime);
			}

			if (price != null) {

				/*
				 * Enable pricing if not already and ordering
				 */
				if (!itemType.getModules().contains(ItemModule.Pricing)) {

					itemType.addModule(ItemModule.Pricing);

					itemType.save();

					logger.config("enabled Pricing module");
				}

				if (!itemType.getModules().contains(ItemModule.Ordering)) {

					itemType.addModule(ItemModule.Ordering);

					itemType.save();

					logger.config("enabled ordering module");
				}

				item.setSellPrice(null, 1, Amount.parse(price));

				if (rrp.isGreaterThan(0)) {

					if (!itemType.isRrp()) {

						itemType.setRrp(true);
						itemType.save();

						logger.config("[gws] enabled rrps");
					}

					item.setRrp(rrp);
				}
			}

			if (weight > 0) {

				item.setAttributeValue(weightAttribute, String.valueOf(weight));
			}

			item.save();

			if (images)
				images(item, image1, image2);

			// add categories if appropriate;
			List<Category> categories = categoryProductsMap.list(item.getId());
			if (categories != null) {
				logger.config("Adding product to categories: " + categories);
				item.addCategories(categories);
			}

			n++;
		}

		logger.config("items processed: " + n);
	}

	private void productTitleTags() {

		logger.config("retrieving product tags");

		try {

			Query q = new Query(context, "select productId, pagetitle, pagedesc, pagekeywords from " + prefix + "titletags_products");
			for (Row row : q.execute()) {

				Item item = row.getObject(0, Item.class);
				if (item != null) {

					String titleTag = row.getString(1);
					if (titleTag != null)
						item.setTitleTag(titleTag);

					String descTag = row.getString(2);
					if (descTag != null)
						item.setDescriptionTag(descTag);

					String keywords = row.getString(3);
					if (keywords != null)
						item.setKeywords(keywords);

					item.save();

				}
			}

		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	public void runCategories() {

		// disable cache
		context.disableCache();

		categories();
		categoryTitleTags();

	}

	public void runProducts() {

		// disable cache
		context.disableCache();

		products();
		options();
		productTitleTags();
	}

	public void runOther() {

		// disable cache
		context.disableCache();

		// create a 2 depth category sidebox
		if (SimpleQuery.count(context, CategoryBox.class) == 0) {

			CategoryBox box = new CategoryBox(context, "Left");
			box.setDepth(2);
			box.save();
		}

		// create a right sidebox basket summary and search box
		if (SimpleQuery.count(context, ItemSearchBox.class) == 0) {
			new ItemSearchBox(context, "Right");
		}

		if (SimpleQuery.count(context, BasketBox.class) == 0) {
			new BasketBox(context, "Right");
		}

		if (SimpleQuery.count(context, NewsletterBox.class) == 0) {
			new NewsletterBox(context, "Right");
		}

		// newsletter();

		news();
	}

	public void run() {

		// disable cache
		context.disableCache();

		categories();
		categoryTitleTags();

		products();
		options();
		productTitleTags();

		// create a 2 depth category sidebox
		if (SimpleQuery.count(context, CategoryBox.class) == 0) {

			CategoryBox box = new CategoryBox(context, "Left");
			box.setDepth(2);
			box.save();
		}

		// create a right sidebox basket summary and search box
		if (SimpleQuery.count(context, ItemSearchBox.class) == 0) {
			new ItemSearchBox(context, "Right");
		}

		if (SimpleQuery.count(context, BasketBox.class) == 0) {
			new BasketBox(context, "Right");
		}

		if (SimpleQuery.count(context, NewsletterBox.class) == 0) {
			new NewsletterBox(context, "Right");
		}

		// newsletter();

		news();

	}

}
