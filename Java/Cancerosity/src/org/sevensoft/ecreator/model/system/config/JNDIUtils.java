package org.sevensoft.ecreator.model.system.config;

import java.io.File;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * User: MeleshkoDN Date: 23.10.2007 Time: 11:50:55
 */
public class JNDIUtils {

	public static final String	USER_DATA_PATH	= "user_data_storage";
	public static final String	SITE_DIR		= "site_dir";

	private static String		resourcesDir;
	private static String		siteDir;

	static {
		try {
			Context ctx = (Context) (new InitialContext()).lookup("java:/comp/env");
			resourcesDir = (String) ctx.lookup(USER_DATA_PATH);
			siteDir = (String) ctx.lookup(SITE_DIR);
		} catch (NamingException e) {
			//e.printStackTrace();
			//added by by Bing
			System.err.println(e.getMessage());
			resourcesDir = "D:/FreeLancer/ECreator/user_data_storage";
			siteDir = "site_dir";
		}
	}

	public static String getResourcesDir() {
		return resourcesDir;
	}

	public static String getResourcesPath() {
		return resourcesDir + File.separator + siteDir;
	}

	public static String getSiteDir() {
		return siteDir;
	}

    public static String getTemplateStorePath() {
        return getResourcesDir();
    }

    public static String getSuperIPsPath() {
        return getResourcesDir();
    }
}
