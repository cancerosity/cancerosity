package org.sevensoft.ecreator.model.system.upgraders;

import junit.framework.TestCase;

/**
 * @author Stephen K Samuel samspade79@gmail.com 24 Oct 2009 10:36:42
 * 
 */
public class LinkResolverTest extends TestCase {

	private String	string1	= "<font size=\"2\"><a href='http://www.peakdistrictonline.co.uk/index.php\" class='path'>Home</a> > <a href='http://www.peakdistrictonline.co.uk/content.php?categoryId=26' class='path'>Peak District Villages</a> > Buxton ";
	private String	string2	= "<font size=\"2\"><a href='/index.php\" class='path'>Home</a> > <a href='/content.php?categoryId=26' class='path'>Peak District Villages</a> > Buxton ";

	public void testResolver() {

		LinkResolver r = new LinkResolver();
		r.setString(string1);
		r.resolve();

		String output = r.getString();
		assertEquals(string2, output);

	}

}
