package org.sevensoft.ecreator.model.system.config;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.accounts.registration.RegistrationSession;
import org.sevensoft.ecreator.model.accounts.sessions.AccountSession;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.blocks.SubcategoriesBlock;
import org.sevensoft.ecreator.model.categories.search.CategorySearcher;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.bots.BasketReaperBot;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.listings.sessions.ListingSession;
import org.sevensoft.ecreator.model.misc.content.ContentBlock;
import org.sevensoft.ecreator.model.misc.location.Pin;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.modules.Modules;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.Row;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

import java.awt.*;
import java.io.File;
import java.util.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Sam 09-SepCo-2003 15:32:56
 */
@Table("settings_installation")
@Singleton
public class Config extends EntityObject {

    public enum SecureMode {
        None, All, AsRequired
    }

    public enum SoftwareVersion {
        Brochure, Full
    }

    public enum Status {
        Live, Demo, Build;
    }


    public static final String TemplateDataPath = "template-data";

    public static final String AttachmentsPath = "WEB-INF/files";

    public static final String FilesPath = "files";

    public static final String ImagesPath = "images";

    public static final String UserDataPath = "user-data";

    public static final String ThumbnailsPath = "thumbnails";

    public static final String TemplateStorePath = "template-store";

    public static final String MediaPath = "media";

    public static final String FeedDataPath = "feed-data";

    public static final String TmpPath = "tmp";

    public static final String PostcodesPath = "WEB-INF/postcodes";

    public static final String FontsPath = "WEB-INF/fonts";

    public static final String TemplateTitlesPath = "template-data/generated-titles";

    public static final String VideosPath = "videos";

    public static final String CaptchaPath = "captchas";

    public static final String FlashLargePath = "flashlarge";

    public static final String FlashSmallPath = "flashsmall";

    public static final String SatNavPath = "sat_nav";

    public static final String PdfsPath = "pdfs";

    public static final String ArticlesPath = "Articles";

    public static final String SitePath = "Site";

    public static final String StoragePath = "Storage";

    public static final String DownloadsPath = "Downloads";

    public static final String UploadsPath = "Uploads";

    public static final String NewsLetterPath = "Newsletter";

    public static final String TinyMCETemplatesPath = "tinymce-templates";

    public static final String EmailLogoPath = "template-data/email-logo";

    public static final String SuperIPsFile = "superips.txt";

    /**
     *
     */
    public static String[] getInitDirs() {

        String[] dirs = new String[]{Config.TmpPath, Config.TemplateTitlesPath, Config.ImagesPath, Config.ThumbnailsPath, Config.TemplateDataPath,
                Config.AttachmentsPath, Config.FontsPath, Config.CaptchaPath, Config.UserDataPath, Config.FeedDataPath, Config.PostcodesPath,
                Config.VideosPath, Config.TinyMCETemplatesPath, Config.EmailLogoPath};

        return dirs;
    }

    public static Config getInstance(RequestContext context) {
        return getSingleton(context, Config.class);
    }

    private SoftwareVersion softwareVersion;

    private SecureMode secureMode;

    private String domain;

    /**
     * Additional ips that can be added for superman access
     */
    private Set<String> supermanIps;

    private String smtpHostname;

    private String serverEmail;

    private String url;

    /**
     * Milliseconds to keep session action for
     */
    private long sessionExpiryMillis;

    /**
     * Superman offline mode
     */
    private boolean offline;

    /**
     *
     */
    private String offlineBody;

    @Deprecated
    private boolean secure;

    private Status status;

    private long lastAccessTimestamp;

    private String secureUrl;

    private int thumbnailHeight;

    private int thumbnailWidth;

    /*
      * Pad the thumbnails out to the thumbnail width
      */
    @Default("1")
    private boolean thumbnailPadding;

    private String thumbnailBackground;

    private int maxImageWidth;

    private int maxImageHeight;

    /**
     * Company that owns the software or the brand name
     */
    private String developers;

    private String serverName;

    private boolean thumbnailAutoCrop;

    private String mailboxEmail;

    private String resourcePath;

    private String filePath;

    private boolean useNewsContent;

    private boolean useHighlightedItemsCache;

    private boolean wwwRedirection;

    @Default("1")
    private boolean useMessages;

    private String username;
    private String password;

    public Config(RequestContext context) {
        super(context);
    }

    /**
     * Renames all categories to title case
     */
    public void categoriesToTitleCase() {

        for (Category category : Category.get(context)) {

            String name = category.getName();
            String titleCase = StringHelper.toTitleCase(name);
            if (!name.equals(titleCase)) {

                category.setName(titleCase);
                category.save();
            }
        }
    }

    public void clearBaskets() {
        new BasketReaperBot(context).run();
    }

    public void clearCostPrices() {
        new Query(context, "update # set costPrice=-1").setTable(Item.class).run();
    }

    public void clearSessions() {
        new Query(context, "delete from #").setTable(AccountSession.class).run();
    }

    public void createAccountsFromOrders() {

        ItemType accountType = ItemType.getAccount(context);

        Query q = new Query(context, "select account, deliveryAddress from orders_copy group by account");
        q.setTable(Order.class);
        for (Row row : q.execute()) {

            int accountId = row.getInt(0);
            Address address = row.getObject(1, Address.class);

            String name;
            if (address == null) {
                name = "unknown";
            } else {
                name = address.getName();
            }
            if (name == null) {
                name = "unknown";
            }

            Query q2 = new Query(context, "insert into items (id, itemType, name, status) values (?, ?, ?, ?)");
            q2.setParameter(accountId);
            q2.setParameter(accountType);
            q2.setParameter(name);
            q2.setParameter("live");
            q2.run();

            Item account = EntityObject.getInstance(context, Item.class, accountId);
            account.setBalance();
        }
    }

    public void deleteMultipleContentBlocks() {

        CategorySearcher searcher = new CategorySearcher(context);
        for (Category category : searcher) {

            List<Block> blocks = category.getBlocks();
            // filter out non content
            CollectionsUtil.filter(blocks, new Predicate<Block>() {

                public boolean accept(Block e) {
                    return e instanceof ContentBlock;
                }
            });

            // remove first one
            if (blocks.isEmpty()) {
                continue;
            }

            blocks.remove(0);
            for (Block block : blocks) {
                block.delete();
            }
        }
    }

    public String getAttachmentsPath() {
        return AttachmentsPath;
    }

    public int getBasketCount() {
        return new Query(context, "select count(*) from #").setTable(Basket.class).getInt();
    }

    public String getDevelopers() {
        return developers == null ? "7soft eCreator" : developers;
    }

    public String getDomain() {
        return domain;
    }

    public String getFeedDataPath() {
        return FeedDataPath;
    }

    public String getFilePath() {
        return filePath;
    }

    public List<String> getFontNames() {

        Font[] fonts = getFonts();
        List<String> names = new ArrayList(fonts.length);
        for (Font font : fonts) {
            names.add(font.getName());
        }

        return names;
    }

    public Font[] getFonts() {

        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Font[] fonts = env.getAllFonts();

        Arrays.sort(fonts, new Comparator<Font>() {

            public int compare(Font o1, Font o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        return fonts;
    }

    public String getGraphicPath(String filename) {

        File file = ResourcesUtils.getRealTemplateData(filename);
        if (file.exists()) {
            return Config.TemplateDataPath + "/" + filename;
        }

        return null;

    }

    public String getImagesPath() {
        return ImagesPath;
    }

    public final long getLastAccessTimestamp() {
        return lastAccessTimestamp;
    }

    public int getListingSessionCount() {
        return new Query(context, "select count(*) from #").setTable(ListingSession.class).getInt();
    }

    public String getMailboxEmail() {
        return mailboxEmail == null ? getServerEmail() : mailboxEmail;
    }

    public final int getMaxImageHeight() {
        return maxImageHeight;
    }

    public final int getMaxImageWidth() {
        return maxImageWidth;
    }

    public String getOfflineBody() {
        return offlineBody;
    }

    public int getPaymentSessionCount() {
        return new Query(context, "select count(*) from #").setTable(FormSession.class).getInt();
    }

    /* public File getRealAttachmentsFile() {
        return new File(getRealAttachmentsPath());
    }*/

    /* public String getRealAttachmentsPath() {
        return context.getRealPath(AttachmentsPath);
    }*/

    /* public String getRealFeedDataPath() {
        return context.getRealPath(getFeedDataPath());
    }*/

    /*public final String getRealFilesDir() {
        return context.getRealPath(FilesPath);
    }*/

    /*public File getRealImagesFile() {
        return new File(getRealImagesPath());
    }*/

//    public String getRealImagesPath() {
////        return context.getRealPath(ImagesPath);
//        return JNDIUtils.getResourcesPath() + File.separator + ImagesPath;
//    }

    /* public File getRealPostcodesDir() {
        return new File(getRealPostcodesPath());
    }

    public String getRealPostcodesPath() {
        return context.getRealPath(PostcodesPath);
    }*/

    /*public File getRealTemplateDataDir() {
        return new File(getRealTemplateDataPath());
    }*/

    /*public String getRealTemplateDataPath() {
        return context.getRealPath(TemplateDataPath);
    }*/

    /*public File getRealThumbnailsDir() {
        return new File(getRealThumbnailsPath());
    }*/

    /*public String getRealThumbnailsPath() {
        return context.getRealPath(ThumbnailsPath);
    }*/

    /*public String getRealUserDataPath() {
        return context.getRealPath(UserDataPath);
    }*/

    /*public String getRealVideosPath() {
        return context.getRealPath(Config.VideosPath);
    }*/

    public int getRegistrationSessionCount() {
        return new Query(context, "select count(*) from #").setTable(RegistrationSession.class).getInt();
    }

    public SecureMode getSecureMode() {

        if (secureMode == null) {

            if (secure) {
                secureMode = SecureMode.AsRequired;
            } else {
                secureMode = SecureMode.None;
            }

            save();
        }

        return secureMode;
    }

    public String getSecureUrl() {
        return secureUrl;
    }

    public String getServerEmail() {
        return serverEmail;
    }

    /**
     * Returns the name of the server, not including http or path info
     */
    public String getServerName() {
        return serverName;
    }

    /**
     * Returns the url but excludes any servlet path info
     */
    public String getServerUrl() {

        return null;

    }

    public int getSessionCount() {
        return SimpleQuery.count(context, AccountSession.class);
    }

    public long getSessionExpiryMillis() {
        return sessionExpiryMillis;
    }

    public String getSmtpHostname() {
        return smtpHostname == null ? "localhost" : smtpHostname;
    }

    public final SoftwareVersion getSoftwareVersion() {
        return softwareVersion == null ? SoftwareVersion.Full : softwareVersion;
    }

    public final Status getStatus() {
        return status == null ? Status.Live : status;
    }

    public final Set<String> getSupermanIps() {

        if (supermanIps == null) {
            supermanIps = new HashSet();
        }
        return supermanIps;
    }

    public String getTemplateDataPath() {
        return TemplateDataPath;
    }

    public String getTemplateTitlesPath() {
        return TemplateTitlesPath;
    }

    

    public final String getThumbnailBackground() {
        return thumbnailBackground;
    }

    public int getThumbnailHeight() {
        return thumbnailHeight < 10 ? 120 : thumbnailHeight;
    }

    public String getThumbnailsPath() {
        return ThumbnailsPath;
    }

    public int getThumbnailWidth() {
        return thumbnailWidth < 10 ? 160 : thumbnailWidth;
    }

    /**
     * returns the complete url including http and www and any sevlet path, but excluding a trailing slash
     */
    public String getUrl() {
        return url == null ? null : url.trim();
    }

    public String getUserDataPath() {
        return UserDataPath;
    }

    /**
     * Returns true if this graphic exists in the template-data directory
     */
    public boolean hasGraphic(String filename) {

        File file = ResourcesUtils.getRealTemplateData(filename);
        return file.exists();

    }

    public boolean hasImageConstraints() {
        return maxImageHeight > 0 || maxImageWidth > 0;
    }

    public boolean hasOfflineBody() {
        return offlineBody != null;
    }

    public void init(RequestContext context) {

        logger.config("[Config] init(context)");

        // always reset domain, it will never be set to anything manually
        domain = context.getRequest().getServerName().trim().toLowerCase();
        if (domain.startsWith("www.")) {
            domain = domain.substring(4);
        }
        logger.config("[Config] domain:" + domain);

        // same for url
        int serverPort = context.getRequest().getLocalPort();
        if (serverPort == 80 || serverPort == 443) {
            url = "http://" + context.getRequest().getServerName() + context.getRequest().getContextPath();
            secureUrl = "https://" + context.getRequest().getServerName() + context.getRequest().getContextPath();
        } else {
            url = "http://" + context.getRequest().getServerName() + ":" + serverPort + context.getRequest().getContextPath();
            secureUrl = "https://" + context.getRequest().getServerName() + ":" + serverPort + context.getRequest().getContextPath();
        }

        logger.config("[Config] url:" + url);
        logger.config("[Config] secureUrl:" + secureUrl);

        // only update the email address if it does not end with the right domain, this allows us to make perm changes
        if (serverEmail == null || !serverEmail.endsWith(domain)) {
            serverEmail = "DoNotReply@" + domain;
        }
        logger.config("[Config] serverEmail:" + serverEmail);

        serverName = context.getRequest().getServerName();
        logger.config("[Config] serverName:" + serverName);

        if (smtpHostname == null) {
            smtpHostname = "localhost";
        }

        resourcePath = JNDIUtils.getResourcesPath();

        logger.config("[Config] smtpHostname:" + smtpHostname);
        save();
    }

    /**
     * Copy weights from field in items to attribute
     */
    public void initWeights() {

        Query q = new Query(context, "select id, weight from #");
        q.setTable(Item.class);
        for (Row row : q.execute()) {

            Item item = row.getObject(0, Item.class);
            ItemType itemType = item.getItemType();
            Attribute attribute = itemType.getAttribute("Weight");
            if (attribute == null)
                attribute = itemType.addAttribute("Weight", AttributeType.Numerical);

            int weight = row.getInt(1);

            if (weight > 0) {
                item.setAttributeValue(attribute, String.valueOf(weight));
            }
        }
    }

    public boolean isBuild() {
        return getStatus() == Status.Build;
    }

    public boolean isDemo() {
        return getStatus() == Status.Demo;
    }

    public boolean isHiglightedItemsCache() {
        return useHighlightedItemsCache;
    }

    public boolean isLive() {
        return getStatus() == Status.Live;
    }

    public boolean isLocations() {
        return SimpleQuery.count(context, Pin.class) > 1000;
    }

    public boolean isLogging() {
        return Logger.getLogger("ecreator").getLevel() == Level.FINEST;
    }

    public boolean isOffline() {
        return offline;
    }

    public boolean isSecured() {
        switch (getSecureMode()) {

            case All:
            case AsRequired:
                return true;

            default:
            case None:
                return false;
        }
    }

    public boolean isThumbnailAutoCrop() {
        return thumbnailAutoCrop;
    }

    public final boolean isThumbnailPadding() {
        return thumbnailPadding;
    }

    public int recreateSubcategoryBlocks() {

        SimpleQuery.delete(context, SubcategoriesBlock.class);

        CategorySearcher searcher = new CategorySearcher(context);
        int n = 0;
        for (Category category : searcher) {

            if (category.isRoot()) {
                continue;
            }

            Block block = new SubcategoriesBlock(context, category, 0);
            block.setPosition(-10);
            block.save();

            n++;

        }

        Block.clearClasses();

        return n;
    }

    public void removeDuplicatedContentBlocks() {

        // get all categories with more than one content block
        Query q = new Query(context, "select cb.category from # cb group by cb.category having count(*) > 1");
        q.setTable(ContentBlock.class);
        for (Row row : q.execute()) {

            Category category = row.getObject(0, Category.class);
            List<ContentBlock> blocks = category.getContentBlocks();
            if (blocks.size() > 1) {
                blocks.remove(0);
                for (Block block : blocks)
                    block.delete();
            }

        }
    }

    public void resetBalances() {

        for (Item account : Item.get(context, null, null, 0, 0)) {
            account.setBalance();
        }
    }

    /**
     * Sends account email with details of the 'admin' account
     */
    public void sendAccountEmail(String email) throws EmailAddressException, SmtpServerException {

        // get any admin account for email content
        User admin = User.getAdmin(context);

        Email msg = new AccountEmail(admin, context);
        msg.setTo(email);

        new EmailDecorator(context, msg).send(Config.getInstance(context).getSmtpHostname());
    }

    public void setDevelopers(String developers) {
        this.developers = developers;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public void setHighlightedItemsCache(boolean useHighlightedItemsCache) {
        this.useHighlightedItemsCache = useHighlightedItemsCache;
    }

    public void setLastAccessTimestamp() {
        this.lastAccessTimestamp = System.currentTimeMillis();
        save("lastAccessTimestamp");
    }

    public final void setLastAccessTimestamp(long lastAccessTimestamp) {
        this.lastAccessTimestamp = lastAccessTimestamp;
    }

    public final void setMailboxEmail(String mailboxEmail) {
        this.mailboxEmail = mailboxEmail;
    }

    public final void setMaxImageHeight(int maxImageHeight) {
        this.maxImageHeight = maxImageHeight;
    }

    public final void setMaxImageWidth(int maxImageWidth) {
        this.maxImageWidth = maxImageWidth;
    }

    public void setOffline(boolean offline) {
        this.offline = offline;
    }

    public void setOfflineBody(String offlineBody) {
        this.offlineBody = offlineBody;
    }

    public void setSecureMode(SecureMode secureMode) {
        this.secureMode = secureMode;
    }

    public void setServerEmail(String serverEmail) {
        this.serverEmail = serverEmail;
    }

    public void setSessionExpiryMillis(long sessionExpiryMillis) {
        this.sessionExpiryMillis = sessionExpiryMillis;
    }

    public void setSmtpHostname(String smtpHostname) {
        this.smtpHostname = smtpHostname;
    }

    public final void setSoftwareVersion(SoftwareVersion version) {

        if (softwareVersion == version) {
            return;
        }

        this.softwareVersion = version;
        Modules modules = Modules.getInstance(context);

        switch (softwareVersion) {

            case Brochure:

                modules.addModule(Module.Galleries);
                modules.addModule(Module.Forms);
                break;

            case Full:
                break;
        }

        modules.save();
    }

    public final void setStatus(Status status) {
        this.status = status;
    }

    public final void setSupermanIps(Collection<String> supermanIps) {
        this.supermanIps = new HashSet();
        this.supermanIps.addAll(supermanIps);
    }

    public void setThumbnailAutoCrop(boolean thumbnailAutoCrop) {
        this.thumbnailAutoCrop = thumbnailAutoCrop;
    }

    public void setThumbnailBackground(String thumbnailBackground) {
        this.thumbnailBackground = thumbnailBackground;
    }

    public final void setThumbnailHeight(int thumbnailHeight) {
        this.thumbnailHeight = thumbnailHeight;
    }

    public final void setThumbnailPadding(boolean thumbnailPadding) {
        this.thumbnailPadding = thumbnailPadding;
    }

    public final void setThumbnailWidth(int thumbnailWidth) {
        this.thumbnailWidth = thumbnailWidth;
    }

    public final void setUrl(String url) {
        this.url = url;
    }

    public void setUseNewsContent(boolean useNewsContent) {
        this.useNewsContent = useNewsContent;
    }

    @Override
    protected void singletonInit(RequestContext context) {

        this.smtpHostname = "localhost";
        this.status = Status.Demo;
        this.thumbnailWidth = 160;
        this.thumbnailHeight = 160;
        // this.thumbnailPadding = true;
        this.thumbnailBackground = "#FFFFFF";

        this.maxImageHeight = 640;
        this.maxImageWidth = 640;

        this.softwareVersion = SoftwareVersion.Full;

    }

    public void testMailServer(String to) throws EmailAddressException, SmtpServerException {

        Email email = new Email();
        email.setBody("Testing mail server");
        email.setTo(to);
        email.setFrom(getServerEmail());
        email.setSubject("Mail test");
        email.send(smtpHostname, username, password);
    }

    /**
     *
     */
    public boolean isBlackRound() {
        return url.contains("blacknround");
    }

    /**
     *
     */
    public boolean isBusClub() {
        return url.contains("businessclub");
    }

    /**
     *
     */
    public boolean isToys() {
        return url.contains("toysgamesgifts");
    }

    public String getResourcePath() {
        return resourcePath;
    }

    public boolean useNewsContent() {
        return useNewsContent;
    }

    public boolean isWwwRedirection() {
        return wwwRedirection;
    }

    public void setWwwRedirection(boolean wwwRedirection) {
        this.wwwRedirection = wwwRedirection;
    }

    public boolean isUseMessages() {
        return useMessages;
    }

    public void setUseMessages(boolean useMessages) {
        this.useMessages = useMessages;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}