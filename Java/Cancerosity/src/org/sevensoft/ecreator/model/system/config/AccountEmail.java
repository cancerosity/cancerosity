package org.sevensoft.ecreator.model.system.config;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 23-Sep-2005 11:28:18
 * 
 */
public class AccountEmail extends Email {

	public AccountEmail(User admin, RequestContext context) {

		Config config = Config.getInstance(context);
		Company company = Company.getInstance(context);

		if (admin == null)
			return;

		setSubject("Your control panel details");

		StringBuilder sb = new StringBuilder();
		sb.append("Hello!,\n\nThis is an email with the login details to the control panel for:\n");
		sb.append(config.getUrl());

		sb.append("\n\nSite name: " + company.getName() + "\n");
		sb.append("Control panel: " + config.getUrl() + "/" + new Link(DashboardHandler.class) + "\n");
		sb.append("Username: " + admin.getUsername() + "\n");
		sb.append("Password: " + admin.getPassword() + "\n");

		setBody(sb);
		
		setFrom("7soft.co.uk", config.getServerEmail());
	}

}
