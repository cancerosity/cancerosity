package org.sevensoft.ecreator.model.system.config;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sks 21 Jan 2007 19:39:26
 *
 */
public class Servers {

	private static Map<String, String>	Servers;

	static {

		Servers = new HashMap();

		Servers.put("88.208.202.21", "Aero");
		Servers.put("88.208.202.22", "Aero");
		Servers.put("88.208.201.46", "Aero");
		Servers.put("88.208.201.47", "Aero");

		Servers.put("88.208.195.49", "Boost");
		Servers.put("88.208.195.50", "Boost");

		Servers.put("88.208.218.56", "Crunchie");
		Servers.put("88.208.218.57", "Crunchie");
		Servers.put("88.208.218.58", "Crunchie");
		Servers.put("88.208.218.59", "Crunchie");

		Servers.put("213.171.222.10", "DoubleDecker");
		Servers.put("213.171.222.11", "DoubleDecker");
		Servers.put("213.171.221.168", "DoubleDecker");
		Servers.put("213.171.222.145", "DoubleDecker");

		Servers.put("213.171.220.34", "Flake");
		Servers.put("213.171.220.35", "Flake");
		Servers.put("213.171.220.36", "Flake");
		Servers.put("213.171.220.37", "Flake");
		Servers.put("213.171.220.38", "Flake");
		Servers.put("213.171.220.39", "Flake");
		Servers.put("213.171.220.40", "Flake");
		Servers.put("213.171.221.145", "Flake");

		Servers.put("213.171.206.123", "Mars");
		Servers.put("213.171.206.124", "Mars");
		Servers.put("213.171.207.124", "Mars");

		Servers.put("88.208.231.79", "MilkyWay");
		Servers.put("88.208.231.80", "MilkyWay");

		Servers.put("217.174.248.220", "Picnic");
		Servers.put("217.174.248.221", "Picnic");
		Servers.put("217.174.249.10", "Picnic");
		Servers.put("217.174.248.172", "Picnic");
		Servers.put("217.174.248.173", "Picnic");

		Servers.put("88.208.201.65", "Rolo");
		Servers.put("88.208.201.155", "Rolo");

		Servers.put("88.208.218.52", "Snickers");
		Servers.put("88.208.218.53", "Snickers");
		Servers.put("88.208.218.54", "Snickers");
		Servers.put("88.208.218.55", "Snickers");

		Servers.put("88.208.235.10", "Topic");
		Servers.put("88.208.233.38", "Toberlone");

		Servers.put("88.208.200.117", "Yorkie");
		Servers.put("88.208.200.118", "Yorkie");
		Servers.put("88.208.200.173", "Yorkie");
		Servers.put("88.208.200.174", "Yorkie");
		Servers.put("88.208.200.219", "Yorkie");
		Servers.put("88.208.200.218", "Yorkie");
		Servers.put("88.208.201.82", "Yorkie");
		Servers.put("88.208.201.83", "Yorkie");
		Servers.put("88.208.200.196", "Yorkie");
		Servers.put("88.208.200.197", "Yorkie");
		Servers.put("88.208.200.235", "Yorkie");
		Servers.put("88.208.200.236", "Yorkie");
	}

	public static String getServerName(String ip) {
		return Servers.get(ip);
	}
}
