package org.sevensoft.ecreator.model.system.upgraders;

import java.util.List;

import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.attributes.AttributeValue;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.blocks.SubcategoriesBlock;
import org.sevensoft.ecreator.model.categories.box.CategoryBox;
import org.sevensoft.ecreator.model.categories.search.CategorySearcher;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.boxes.misc.CustomBox;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.ItemTypePreset;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.modules.Modules;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.Row;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 10 Oct 2006 07:52:47
 *
 */
public class Upgrade2 {

	private final RequestContext	context;
	private final Modules		modules;

	public Upgrade2(RequestContext context) {
		this.context = context;
		this.modules = Modules.getInstance(context);
	}

	/**
	 * Convert availability from field to attribute 
	 */
	public void availabilityToAttribute() {

		try {

			Query q = new Query(context, "select id, availability from items where availability is not null");
			q.setTable(Item.class);
			for (Row row : q.execute()) {

				Item item = row.getObject(0, Item.class);
				String a = row.getString(1);

				Attribute attribute = item.getItemType().getAttribute("availability attribute");
				if (attribute == null) {
					attribute = item.getItemType().addAttribute("availability attribute", AttributeType.Text);
				}

				item.setAttributeValue(attribute, a);

				new Query(context, "update # set availability = null where id=?").setTable(Item.class).setParameter(item.getId()).run();

			}

		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	private void customBoxes() {

		// remove all non custom boxes 
		try {

			new Query(context, "delete from boxes where type != 'custom' ").run();

			// drop custom boxes table and rename boxes to it
			new Query(context, "drop table #").setTable(CustomBox.class).run();
			new Query(context, "alter table boxes rename to #").setTable(CustomBox.class).run();
			new Query(context, "update # set caption=name").setTable(CustomBox.class).run();

		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	private void listings() {

		Query q;
		try {

			// do listings
			q = new Query(context, "select count(*) from # where type='Listing' and status!=?");
			q.setTable(Item.class);
			q.setParameter("Deleted");
			if (q.getInt() > 0) {

				ItemType itemType = ItemTypePreset.Listing.create(context);
				Attribute addressAttribute = itemType.getAttribute("address");
				Attribute postcodeAttribute = itemType.getAttribute("postcode");
				Attribute emailAttribute = itemType.getAttribute("email");
				Attribute telephoneAttribute = itemType.getAttribute("telephone");
				Attribute websiteAttribute = itemType.getAttribute("website");

				q = new Query(context, "update # set itemType=?, type = null where type='Listing'");
				q.setTable(Item.class);
				q.setParameter(itemType);
				q.run();

				// copy all address, postcode, email, telephone and web data across
				q = new Query(context, "select id, address, postcode, email, telephone, website from # where itemType=?");
				q.setTable(Item.class);
				q.setParameter(itemType);
				for (Row row : q.execute()) {

					Item item = EntityObject.getInstance(context, Item.class, row.getInt(0));
					String address = row.getString(1);
					String postcode = row.getString(2);
					String email = row.getString(3);
					String telephone = row.getString(4);
					String website = row.getString(5);

					item.addAttributeValue(websiteAttribute, website);
					item.addAttributeValue(addressAttribute, address);
					item.addAttributeValue(telephoneAttribute, telephone);
					item.addAttributeValue(postcodeAttribute, postcode);
					item.addAttributeValue(emailAttribute, email);
				}
			}
		} catch (RuntimeException e1) {
			e1.printStackTrace();
		}
	}

	public void members() {

		// get item type for members
		ItemType accountType = ItemType.getAccount(context);

		Query q = new Query(context, "select id, name, email, passwordHash, status from members");
		for (Row row : q.execute()) {

			int memberId = row.getInt(0);
			String name = row.getString(1);
			String email = row.getString(2);
			String passwordHash = row.getString(3);
			String status = row.getString(4);
			if ("active".equalsIgnoreCase(status)) {
				status = "LIVE";
			}

			Item account = new Item(context, accountType, name, status, null);
			account.setEmail(email);
			account.setPasswordHash(passwordHash);
			account.save();

			try {

				// update attribute values with new id
				q = new Query(context, "update # set item=? where member=?");
				q.setTable(AttributeValue.class);
				q.setParameter(account);
				q.setParameter(memberId);
				q.run();

			} catch (RuntimeException e7) {
				e7.printStackTrace();
			}

			try {
				// update images
				q = new Query(context, "update # set item=? where member=?");
				q.setTable(Img.class);
				q.setParameter(account);
				q.setParameter(memberId);
				q.run();
			} catch (RuntimeException e7) {
				e7.printStackTrace();
			}

			try {
				// update orders
				q = new Query(context, "update # set account=? where member=?");
				q.setTable(Order.class);
				q.setParameter(account);
				q.setParameter(memberId);
				q.run();
			} catch (RuntimeException e7) {
				e7.printStackTrace();
			}

			try {
				// update addresses
				q = new Query(context, "update # set account=? where member=?");
				q.setTable(Address.class);
				q.setParameter(account);
				q.setParameter(memberId);
				q.run();
			} catch (RuntimeException e7) {
				e7.printStackTrace();
			}

			try {
				// update cards
				q = new Query(context, "update # set account=? where member=?");
				q.setTable(Card.class);
				q.setParameter(account);
				q.setParameter(memberId);
				q.run();
			} catch (RuntimeException e6) {
				e6.printStackTrace();
			}

			try {
				// update payments
				q = new Query(context, "update # set account=? where member=?");
				q.setTable(Payment.class);
				q.setParameter(account);
				q.setParameter(memberId);
				q.run();
			} catch (RuntimeException e6) {
				e6.printStackTrace();
			}

			// update member balance
			account.setBalance();
		}

		new Query(context, "delete from members").run();
	}

	public void news() {

		ItemType news = ItemTypePreset.NewsArticle.create(context);
		Attribute authorAttribute = news.getAttribute("author");

		try {

			Query q = new Query(context, "select author, content, date, name from newsbulletins");
			for (Row row : q.execute()) {

				String author = row.getString(0);
				String content = row.getString(1);
				long timestamp = row.getLong(2);
				String name = row.getString(3);

				Item item = new Item(context, news, name, "Live", null);
				item.setAttributeValue(authorAttribute, author);
				item.setContent(content);
				item.save();

				new Query(context, "update # set dateCreated=? where id=?").setTable(Item.class).setParameter(timestamp).setParameter(item.getId())
						.run();

			}

		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Change prices from inline to the separate prices table
	 */
	public void prices() {

		Query q = new Query(context, "select id, salePrice from # where salePrice > 0 limit 200");
		q.setTable(Item.class);
		List<Row> rows;
		while ((rows = q.execute()).size() > 0) {

			for (Row row : rows) {

				Item item = EntityObject.getInstance(context, Item.class, row.getInt(0));
				if (item == null) {
					continue;
				}

				String salePrice = row.getString(1);
				if (salePrice == null) {
					continue;
				}

				item.setSellPrice(null, 1, Amount.parse(salePrice));

				new Query(context, "update # set saleprice=0 where id=?").setParameter(item).setTable(Item.class).run();
			}
		}

	}

	private void products() {

		try {

			Query q = new Query(context, "select count(*) from # where type='Product' and status!=?");
			q.setTable(Item.class);
			q.setParameter("Deleted");
			if (q.getInt() > 0) {

				modules.addModule(Module.Pricing);
				modules.addModule(Module.Shopping);
				modules.addModule(Module.Delivery);
				modules.addModule(Module.Availabilitity);
				modules.save();

				// create item type for products
				ItemType itemType = ItemTypePreset.Product.create(context);

				// create attributes for mpn / website
				Attribute mpnAttribute = itemType.getAttribute("Manuf part number", AttributeType.Text);
				Attribute manufAttribute = itemType.getAttribute("Manufacturer", AttributeType.Selection);
				Attribute websiteAttribute = itemType.getAttribute("Website", AttributeType.Link);
				Attribute weightAttribute = itemType.getAttribute("Weight", AttributeType.Numerical);

				q = new Query(context, "update # set itemType=?, type = null where type='Product'");
				q.setTable(Item.class);
				q.setParameter(itemType);
				q.run();

				// copy all manufacturer, mpn and website data
				q = new Query(context,
						"select id, website, modelNumber, salePrice, costPrice, availability, rrp, supplierSku, manufacturer, weight from # where itemType=?");
				q.setTable(Item.class);
				q.setParameter(itemType);
				for (Row row : q.execute()) {

					Item item = EntityObject.getInstance(context, Item.class, row.getInt(0));
					String website = row.getString(1);
					String mpn = row.getString(2);
					Money salePrice = row.getMoney(3);
					Money costPrice = row.getMoney(4);
					String availability = row.getString(5);
					Money rrp = row.getMoney(6);

					int manuf = row.getInt(8);
					double weight = row.getInt(9);

					item.addAttributeValue(websiteAttribute, website);
					item.addAttributeValue(mpnAttribute, mpn);
					item.addAttributeValue(weightAttribute, String.valueOf(weight));

					if (costPrice != null) {

						if (!itemType.isCostPricing()) {
							itemType.setCostPricing(true);
							itemType.save();
						}

						item.setOurCostPrice(costPrice);
					}

					if (salePrice != null) {
						item.setSellPrice(null, 1, new Amount(salePrice));
					}

					item.setOutStockMsg(availability);
					item.setRrp(rrp);
					item.save();

					if (manuf > 0) {

						String manufacturer = new Query(context, "select name from manufacturers where id=?").setParameter(manuf).getString();
						item.addAttributeValue(manufAttribute, manufacturer);

					}
				}

			}
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	public void run() {

		modules.addModule(Module.Newsletter);
		modules.addModule(Module.Sms);
		modules.addModule(Module.Forms);
		modules.save();

		new Query(context, "delete from boxes where type='ListingDetails' ").run();

		new Query(context, "delete from boxes where type='ProductDetails' ").run();

		news();

		members();

		listings();

		products();

		customBoxes();

		subcatBlocks();

		// enable category boxes
		new CategoryBox(context, "Left");

		// update category counts
		for (Category category : Category.get(context)) {
			category.setLiveCount();
		}

		resetFriendlyUrls();

		/*
		 * I want to re-create all subcategory blocks
		 */
		Config config = Config.getInstance(context);

		config.recreateSubcategoryBlocks();
		context.getSchemaValidator().clear();
	}

	/**
	 * 
	 */
	private void resetFriendlyUrls() {

		try {
			new Query(context, "update # set friendlyUrl=null").setTable(Item.class).run();

		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		try {
			new Query(context, "update # set friendlyUrl=null").setTable(Category.class).run();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	private void subcatBlocks() {

		SimpleQuery.delete(context, SubcategoriesBlock.class);

		CategorySearcher searcher = new CategorySearcher(context);
		int n = 0;
		for (Category category : searcher) {

			if (category.isRoot()) {
				continue;
			}

			Block block = new SubcategoriesBlock(context, category, 0);
			block.setPosition(-10);
			block.save();

			n++;

		}
	}
}
