package org.sevensoft.ecreator.model.system.upgraders;

import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.Row;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author Stephen K Samuel stephenksamuel@gmail.com Jul 10, 2010 3:11:02 PM
 * 
 */
public class PdoAttributeUpgrader {

	/**
	 * The ItemType of the listings class
	 */
	private ItemType			itemType;

	private final RequestContext	context;

	public PdoAttributeUpgrader(RequestContext context) {
		this.context = context;
	}

	public int upgrade() {

		Attribute websiteAttribute = itemType.getAttribute("Website", AttributeType.Text);
		Attribute postcodeAttribute = itemType.getAttribute("Postcode", AttributeType.Text);
		Attribute addressAttribute = itemType.getAttribute("Address", AttributeType.Text);
		Attribute emailAttribute = itemType.getAttribute("Email", AttributeType.Text);
		Attribute telephoneAttribute = itemType.getAttribute("Telephone", AttributeType.Text);

		int k = 0;
		Query query = new Query(context, "select productId, telephone, email, website, postcode, address from pdo_products");
		for (Row row : query.execute()) {

			int productId = row.getInt(0);
			String telephone = row.getString(1);
			String email = row.getString(2);
			String website = row.getString(3);
			String postcode = row.getString(4);
			String address = row.getString(5);

			Item item = EntityObject.getInstance(context, Item.class, productId);
			item.setAttributeValue(telephoneAttribute, telephone);
			item.setAttributeValue(emailAttribute, email);
			item.setAttributeValue(addressAttribute, address);
			item.setAttributeValue(postcodeAttribute, postcode);
			item.setAttributeValue(websiteAttribute, website);
			k++;
		}

		return k;
	}

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }
}
