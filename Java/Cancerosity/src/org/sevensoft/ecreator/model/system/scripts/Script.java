package org.sevensoft.ecreator.model.system.scripts;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * User: Tanya
 * Date: 24.06.2011
 */
@Table("settings_scripts")
@Singleton
public class Script extends EntityObject {

    protected Script(RequestContext context) {
        super(context);
    }

    public static Script getInstance(RequestContext context) {
        return getSingleton(context, Script.class);
    }

    @Default("1")
    private boolean loadjQuery = true;

    private boolean loadjQuery144 = false;

    private boolean loadjQueryUi = false;

    @Default("1")
    private boolean loadThickbox = true;

    private boolean AC_RunActiveContent = false;

    private boolean loadEasyflashuploader = false;

    private boolean loadCookieControl;

    private boolean loadCookieControlWithRevoke;

    private boolean loadIosFiletag;

    public boolean isLoadjQuery() {
        return loadjQuery;
    }

    public void setLoadjQuery(boolean loadjQuery) {
        this.loadjQuery = loadjQuery;
    }

    public boolean isLoadjQuery144() {
        return loadjQuery144;
    }

    public void setLoadjQuery144(boolean loadjQuery144) {
        this.loadjQuery144 = loadjQuery144;
    }

    public boolean isLoadjQueryUi() {
        return loadjQueryUi;
    }

    public void setLoadjQueryUi(boolean loadjQueryUi) {
        this.loadjQueryUi = loadjQueryUi;
    }

    public boolean isLoadThickbox() {
        return loadThickbox;
    }

    public void setLoadThickbox(boolean loadThickbox) {
        this.loadThickbox = loadThickbox;
    }

    public boolean isAC_RunActiveContent() {
        return AC_RunActiveContent;
    }

    public void setAC_RunActiveContent(boolean AC_RunActiveContent) {
        this.AC_RunActiveContent = AC_RunActiveContent;
    }

    public boolean isLoadEasyflashuploader() {
        return loadEasyflashuploader;
    }

    public void setLoadEasyflashuploader(boolean loadEasyflashuploader) {
        this.loadEasyflashuploader = loadEasyflashuploader;
    }

    public boolean isLoadCookieControl() {
        return loadCookieControl;
    }

    public void setLoadCookieControl(boolean loadCookieControl) {
        this.loadCookieControl = loadCookieControl;
    }

    public boolean isLoadCookieControlWithRevoke() {
        return loadCookieControlWithRevoke;
    }

    public void setLoadCookieControlWithRevoke(boolean loadCookieControlWithRevoke) {
        this.loadCookieControlWithRevoke = loadCookieControlWithRevoke;
    }

    public boolean isLoadIosFiletag() {
        return loadIosFiletag;
    }

    public void setLoadIosFiletag(boolean loadIosFiletag) {
        this.loadIosFiletag = loadIosFiletag;
    }
}
