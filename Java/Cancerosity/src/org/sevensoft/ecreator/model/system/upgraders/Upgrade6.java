package org.sevensoft.ecreator.model.system.upgraders;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.accounts.AccountModule;
import org.sevensoft.ecreator.model.accounts.registration.RegistrationModule;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionModule;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeValue;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.template.Template;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.delivery.Delivery;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.interaction.buddies.Buddy;
import org.sevensoft.ecreator.model.interaction.pm.PrivateMessage;
import org.sevensoft.ecreator.model.interaction.views.View;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.ItemTypePreset;
import org.sevensoft.ecreator.model.items.favourites.Favourite;
import org.sevensoft.ecreator.model.items.highlighted.blocks.HighlightedItemsBlock;
import org.sevensoft.ecreator.model.items.highlighted.boxes.HighlightedItemsBox;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;
import org.sevensoft.ecreator.model.items.listings.ListingPackageAccess;
import org.sevensoft.ecreator.model.items.listings.ListingPackageCategory;
import org.sevensoft.ecreator.model.items.listings.ListingRate;
import org.sevensoft.ecreator.model.items.pricing.Price;
import org.sevensoft.ecreator.model.items.stock.StockModule;
import org.sevensoft.ecreator.model.items.stock.StockModule.StockControl;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.marketing.newsletter.Subscriber;
import org.sevensoft.ecreator.model.marketing.newsletter.Subscription;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.misc.agreements.Agreement;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.ecreator.model.search.forms.SearchForm;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.modules.Modules;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.Row;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Dec 2006 21:35:08
 *
 */
public class Upgrade6 {

	private static final Logger	logger	= Logger.getLogger("ecreator");

	private RequestContext		context;

	public Upgrade6(RequestContext context) {
		this.context = context;
	}

	private void attachments() {
		try {
			new Query(context, "update # set attachmentcount=filecount where attachmentcount=0 ").setTable(Item.class).run();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	private void attributeChanges() {
		try {
			new Query(context, "update # set displayable = 0 where priv=1 ").setTable(Attribute.class).run();
			new Query(context, "update # set displayable = 1 where priv=0 ").setTable(Attribute.class).run();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	private void blocks() {

		for (Class clazz : Block.getClasses()) {

			Query q;
			try {
				q = new Query(context, "update # set ownerCategory=category where category>0");
				q.setTable(clazz);
				q.run();
			} catch (RuntimeException e1) {
				e1.printStackTrace();
			}

			try {
				q = new Query(context, "update # set visible=visible2");
				q.setTable(clazz);
				q.run();
			} catch (RuntimeException e) {
				e.printStackTrace();
			}
		}
	}

	private void boxes() {

		for (Class clazz : Box.classes) {

			try {
				new Query(context, "update # set `where`=1 where advanced=1").setTable(clazz).run();
			} catch (RuntimeException e) {
				e.printStackTrace();
			}

			try {
				new Query(context, "update # set displayOnAllCategories=displayAllCategories, "
						+ "displayOnAllItems=displayAllItems, displayOnBasket=displayBasket, "
						+ "displayOnCheckout=displayCheckout, displayOnHome=displayHome, "
						+ "displayOnItemsInCategories=displayItemsInCategories, "
						+ "displayToAllAccounts=displayMembers, displayToGuests=displayGuest").setTable(clazz).run();
			} catch (RuntimeException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 */
	public void createMembers() {

		// Map is item type name to item type map
		Map<String, ItemType> map = new HashMap();

		for (ItemType itemType : ItemType.getAccounts(context)) {
			map.put(itemType.getName(), itemType);
		}

		Query q = new Query(context, "select id, name, displayName, email, passwordHash, status, x, y, location, lastActiveDate, referrer, ipAddress, "
				+ "subscriptionExpiryDate, subscription, dateregistered from members");
		for (Row row : q.execute()) {

			int memberId = row.getInt(0);
			String name = row.getString(1);
			String displayName = row.getString(2);
			String email = row.getString(3);
			String passwordHash = row.getString(4);
			String status = row.getString(5);

			int x = row.getInt(6);
			int y = row.getInt(7);
			String location = row.getString(8);

			long lastActive = row.getLong(9);

			String referrer = row.getString(10);
			String ipAddress = row.getString(11);
			long dateRegistered = row.getLong(14);

			// convert active -> live and inactive to disabled
			if ("active".equalsIgnoreCase(status)) {
				status = "Live";
			}

			// get first member group for this member
			q = new Query(context, "select mg.name from members_groups mg join members_groups_join mgj on mg.id=mgj.memberGroup " + "where mgj.member=?");
			q.setParameter(memberId);
			Row row2 = q.get();

			if (row2 != null) {

				String memberGroupName = row2.getString(0);
				logger.config("[Upgrade6to7] member group name=" + memberGroupName);

				ItemType itemType = map.get(memberGroupName);
				logger.config("[Upgrade6to7] item type=" + itemType);

				if (itemType != null) {

					Item account = new Item(context, itemType, name, status, null);
					account.setEmail(email);
					account.setDisplayName(displayName);
					account.setPasswordHash(passwordHash);
					account.setX(x);
					account.setY(y);
					account.setLocation(location);
					account.setLastActive(lastActive);
					account.setReferrer(referrer);
					if (dateRegistered > 0) {
						account.setDateCreated(new DateTime(dateRegistered));
					}
					account.setIpAddress(ipAddress);
					account.save();

					try {
						// update attributes with new id
						q = new Query(context, "update # set item=? where member=?");
						q.setTable(AttributeValue.class);
						q.setParameter(account);
						q.setParameter(memberId);
						q.run();
					} catch (RuntimeException e7) {
						e7.printStackTrace();
					}

					try {
						// update images
						q = new Query(context, "update # set item=? where member=?");
						q.setTable(Img.class);
						q.setParameter(account);
						q.setParameter(memberId);
						q.run();
					} catch (RuntimeException e7) {
						e7.printStackTrace();
					}

					try {
						// update orders
						q = new Query(context, "update # set account=? where member=?");
						q.setTable(Order.class);
						q.setParameter(account);
						q.setParameter(memberId);
						q.run();
					} catch (RuntimeException e7) {
						e7.printStackTrace();
					}

					try {
						// update favourites
						q = new Query(context, "update # set account=? where member=?");
						q.setTable(Favourite.class);
						q.setParameter(account);
						q.setParameter(memberId);
						q.run();
					} catch (RuntimeException e7) {
						e7.printStackTrace();
					}

					try {
						// update addresses
						q = new Query(context, "update # set account=? where member=?");
						q.setTable(Address.class);
						q.setParameter(account);
						q.setParameter(memberId);
						q.run();
					} catch (RuntimeException e7) {
						e7.printStackTrace();
					}

					try {
						// update cards
						q = new Query(context, "update # set account=? where member=?");
						q.setTable(Card.class);
						q.setParameter(account);
						q.setParameter(memberId);
						q.run();
					} catch (RuntimeException e6) {
						e6.printStackTrace();
					}

					try {
						// update attachments
						q = new Query(context, "update # set item=? where member=?");
						q.setTable(Attachment.class);
						q.setParameter(account);
						q.setParameter(memberId);
						q.run();
					} catch (RuntimeException e6) {
						e6.printStackTrace();
					}

					try {
						// update cards
						q = new Query(context, "update # set account=? where member=?");
						q.setTable(Payment.class);
						q.setParameter(account);
						q.setParameter(memberId);
						q.run();
					} catch (RuntimeException e6) {
						e6.printStackTrace();
					}

					account.setAttachmentCount();
					account.setImageInfo();

					try {
						new Query(context, "update # set account=? where member=?").setTable(View.class).setParameter(account).setParameter(
								memberId).run();
					} catch (RuntimeException e5) {
						e5.printStackTrace();
					}

					try {
						new Query(context, "update # set viewer=? where viewer=?").setTable(View.class).setParameter(account)
								.setParameter(memberId).run();
					} catch (RuntimeException e4) {
						e4.printStackTrace();
					}

					try {
						new Query(context, "update # set account=? where member=?").setTable(Buddy.class).setParameter(account).setParameter(
								memberId).run();
					} catch (RuntimeException e3) {
						e3.printStackTrace();
					}

					try {
						new Query(context, "update # set buddy=? where buddy=?").setTable(Buddy.class).setParameter(account).setParameter(memberId)
								.run();
					} catch (RuntimeException e2) {
						e2.printStackTrace();
					}

					try {
						new Query(context, "update # set recipient=? where recipient=?").setTable(PrivateMessage.class).setParameter(account)
								.setParameter(memberId).run();
					} catch (RuntimeException e1) {
						e1.printStackTrace();
					}

					try {
						new Query(context, "update # set sender=? where sender=?").setTable(PrivateMessage.class).setParameter(account)
								.setParameter(memberId).run();
					} catch (RuntimeException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	private void deliveryCharges() {

		try {

			Query q = new Query(context, "select id, deliverySurcharge from # where deliverySurcharge>0");
			q.setTable(Item.class);
			for (Row row : q.execute()) {

				Item item = row.getObject(0, Item.class);
				Money deliverySurcharge = row.getMoney(1);
				final Delivery delivery = item.getDelivery();
				delivery.setDeliverySurcharge(deliverySurcharge);
				delivery.save();
			}

		} catch (RuntimeException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 */
	private void favourites() {
		try {
			Query q = new Query(context, "update # set header=content where header is not null");
			q.setTable(Favourite.class);
			q.run();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	public void highlightedMemberBoxes() {

		try {
			Query q = new Query(context,
					"select cssid, cssclass, name, location, markup, border, css, captiontext, visible, id from boxes_highlighted_members");
			for (Row row : q.execute()) {

				String cssId = row.getString(0);
				String cssclass = row.getString(1);
				String name = row.getString(2);
				String location = row.getString(3);
				Markup markup = row.getObject(4, Markup.class);
				boolean border = "1".equals(row.getString(5));
				String captionText = row.getString(7);
				boolean visible = "1".equals(row.getString(8));
				int id = row.getInt(9);

				HighlightedItemsBox box = new HighlightedItemsBox(context, location);
				box.setVisible(visible);
				box.setBorder(border);
				box.setCssClass(cssclass);
				box.setCssId(cssId);
				box.setMarkup(markup);
				box.setCaptionText(captionText);
				box.setName(name);
				box.save();

				// update search to use this new box
				new Query(context, "update # set highlightedItemsBox=? where highlightedMembersBox=?").setTable(Search.class).setParameter(box)
						.setParameter(id).run();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	private void imagesToModule() {

		try {

			// select all item types where images=1 and add it in as a module
			Query q = new Query(context, "select * from # where images=1");
			q.setTable(ItemType.class);
			for (ItemType itemType : q.execute(ItemType.class)) {
				itemType.addModule(ItemModule.Images);
			}

		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	private void moveMembers() {

		Query q;
		try {
			q = new Query(context, "select count(*) from # where modules like '%Members%'");
			q.setTable(Modules.class);
			if (q.getInt() == 0) {
				return;
			}
		} catch (RuntimeException e8) {
			e8.printStackTrace();
		}

		Modules modules = Modules.getInstance(context);
		modules.addModule(Module.Accounts);
		modules.save();

		// create an item type for each member group that isn't the guest account
		q = new Query(context,
				"select id, name, profileMarkup, listMarkup, signup, imageholder, agreement, loginforwardurl from members_groups where guests=0");
		for (Row row : q.execute()) {

			int id = row.getInt(0);
			String name = row.getString(1);
			Markup profileMarkup = row.getObject(2, Markup.class);
			Markup listMarkup = row.getObject(3, Markup.class);
			boolean signup = row.getBoolean(4);
			String imageHolder = row.getString(5);
			Agreement agreement = row.getObject(6, Agreement.class);
			String loginForwardUrl = row.getString(7);
			//			boolean registrationEmail = "1".equals(row.getString(8));
			//			String registrationEmailContent = row.getString(9);
			//			String registrationContent = row.getString(10);

			logger.config("[Upgrade6to7] creating item type from member group name=" + name);

			ItemType itemType = ItemTypePreset.Account.create(context);
			itemType.setName(name);
			itemType.setListMarkup(listMarkup);
			itemType.setViewMarkup(profileMarkup);
			itemType.setHidden(true);

			if (profileMarkup != null) {

				profileMarkup.setBody(profileMarkup.getBody().replace("[member", "[item"));
				profileMarkup.save();
			}

			if (listMarkup != null) {

				listMarkup.setBody(listMarkup.getBody().replace("[member", "[item"));
				listMarkup.save();
			}

			itemType.addModule(ItemModule.Account);
			itemType.setImageHolder(imageHolder);

			if (signup) {

				itemType.addModule(ItemModule.Registrations);

				RegistrationModule module = itemType.getRegistrationModule();
				module.setAgreement(agreement);
				module.save();
			}

			AccountModule module = itemType.getAccountModule();
			module.setLoginForwardUrl(loginForwardUrl);
			module.save();

			//			RegistrationModule rm = itemType.getRegistrationModule();
			//			rm.setRegistrationEmail(registrationEmail);
			//			rm.setRegistrationContent(registrationContent);
			//			rm.setRegistrationEmailContent(registrationEmailContent);
			//			rm.save();

			itemType.save();

			// subscription modules
			try {
				new Query(context, "update # set itemType=? where memberGroup=?").setTable(SubscriptionModule.class).setParameter(itemType)
						.setParameter(id).run();
			} catch (RuntimeException e) {
				e.printStackTrace();
			}

			// update searches that use member groups
			try {
				new Query(context, "update # set itemType=? where memberGroup=?").setTable(SearchForm.class).setParameter(itemType).setParameter(id)
						.run();
			} catch (RuntimeException e) {
				e.printStackTrace();
			}

			try {
				new Query(context, "update # set itemType=? where memberGroup=?").setTable(Search.class).setParameter(itemType).setParameter(id).run();
			} catch (RuntimeException e) {
				e.printStackTrace();
			}

		}

		createMembers();

		// delete members to ensure this is an indemopent operation

		//		new Query(context, "drop table members").run();
		//		new Query(context, "drop table members_groups");
		//		new Query(context, "drop table members_groups_joins");
	}

	private void moveSearchOwner() {

		try {

			Query q = new Query(context, "select id, highlighteditemsblock from # where highlighteditemsblock>0");
			q.setTable(Search.class);
			for (Row row : q.execute()) {

				int id = row.getInt(0);
				HighlightedItemsBlock block = row.getObject(1, HighlightedItemsBlock.class);

				new Query(context, "update # set owner=? where id=?").setParameter(block.getFullId()).setParameter(id).setTable(Search.class).run();
			}

		} catch (RuntimeException e2) {
			e2.printStackTrace();
		}

		try {

			Query q = new Query(context, "select id, highlighteditemsbox from # where highlighteditemsbox>0");
			q.setTable(Search.class);
			for (Row row : q.execute()) {

				int id = row.getInt(0);
				HighlightedItemsBox box = row.getObject(1, HighlightedItemsBox.class);

				new Query(context, "update # set owner=? where id=?").setParameter(box.getFullId()).setParameter(id).setTable(Search.class).run();
			}

		} catch (RuntimeException e1) {
			e1.printStackTrace();
		}

		try {

			Query q = new Query(context, "select id, category from # where category>0");
			q.setTable(Search.class);
			for (Row row : q.execute()) {

				int id = row.getInt(0);
				Category category = row.getObject(1, Category.class);

				new Query(context, "update # set owner=? where id=?").setParameter(category.getFullId()).setParameter(id).setTable(Search.class).run();
			}

		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	public void newsletterSubscribers() {

		// put each subscriber in the default newsletter
		Newsletter nl = Newsletter.getDefault(context);

		for (Subscriber s : Subscriber.get(context)) {
			new Subscription(context, nl, s);
		}
	}

	private void orderableToModule() {

		try {

			// select all item types where images=1 and add it in as a module
			Query q = new Query(context, "select * from # where orderable=1");
			q.setTable(ItemType.class);
			for (ItemType itemType : q.execute(ItemType.class)) {
				itemType.addModule(ItemModule.Ordering);
			}

		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	public void prices() {

		try {

			Query q = new Query(context, "select item, qty, saleprice, markup from items_saleprices");
			for (Row row : q.execute()) {

				int item = row.getInt(0);
				int qty = row.getInt(1);
				Money saleprice = row.getMoney(2);
				double markup = row.getDouble(3);

				Amount amount;
				if (markup > 0) {
					amount = new Amount(markup);
				} else {
					amount = new Amount(saleprice);
				}

				q = new Query(context, "insert into # (item, qty, amount) values (?,?,?)");
				q.setTable(Price.class);
				q.setParameter(item);
				q.setParameter(qty);
				q.setParameter(amount.toString());
				q.run();
			}

		} catch (RuntimeException e) {
			e.printStackTrace();
		}

		try {
			new Query(context, "update # set genericSellPrice=0").setTable(Item.class).run();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}

		try {
			new Query(context, "update # set costPrice=0").setTable(Item.class).run();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void removeDefunctTables() {

		String[] oldTables = new String[] { "stats_hits", "stats_hits_categories", "stats_hits_daily_categories", "stats_hits_daily_items",
				"stats_hits_daily_site", "stats_hits_monthly_site", "stats_hits_items", "stats_impress", "stats_referrals", "stats_impressions",
				"stats_impress_daily", "storefinder_stores", "warehouses", "blocks_members_highlighted", "applications_fields",
				"applications_fields_values", "designs", "designs_elements", "designs_elements_data", "blocks_member", "blocks_highlighted_members",
				"members_blacklist", "members_subscriptions" };

		for (String table : oldTables) {
			try {
				new Query(context, "drop table `" + table + "`").run();
			} catch (RuntimeException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 */
	private void removeListingPackages() {
		try {
			SimpleQuery.delete(context, ListingPackage.class);
			SimpleQuery.delete(context, ListingPackageAccess.class);
			SimpleQuery.delete(context, ListingRate.class);
			SimpleQuery.delete(context, ListingPackageCategory.class);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	private void resetFriendlyUrls() {
		try {
			new Query(context, "update # set friendlyUrl=null").setTable(Item.class).run();

		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		try {
			new Query(context, "update # set friendlyUrl=null").setTable(Category.class).run();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	public void run() {

		attachments();
		imagesToModule();
		orderableToModule();
		blocks();
		searchSubscriptionToSubscriptionLevel();
		moveMembers();
		stockChanges();
		highlightedMemberBoxes();
		boxes();
		moveSearchOwner();
		attributeChanges();
		removeListingPackages();
		newsletterSubscribers();

		resetFriendlyUrls();

		Price.resetCachedSellPrices(context);
		prices();

		deliveryCharges();

		removeDefunctTables();
		favourites();

		template();
	}

	private void searchSubscriptionToSubscriptionLevel() {

		try {
			Query q = new Query(context, "update # set subscriptionLevel=subscription");
			q.setTable(Search.class);
			q.run();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 */
	private void stockChanges() {

		try {
			// get existing stock control
			for (ItemType itemType : ItemType.get(context)) {

				String string = new Query(context, "select stockControl from # where id=?").setTable(ItemType.class).setParameter(itemType).getString();
				if (string != null) {
					StockControl control = StockControl.valueOf(string);
					if (control != null) {
						StockModule module = itemType.getStockModule();
						module.setStockControl(control);
						module.save();
					}
				}
			}
		} catch (RuntimeException e1) {
			e1.printStackTrace();
		}

		try {
			try {
				new Query(context, "update # set outStockMsg=leadtime").setTable(Item.class).run();
			} catch (RuntimeException e) {
				e.printStackTrace();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	private void template() {

		try {
			for (Template template : Template.get(context)) {
				String before = template.getBefore();
				String after = template.getAfter();

				if (before != null) {
					before = before.replaceAll("\\[search_type.*?\\]", "");
				}

				if (after != null) {
					after = after.replaceAll("\\[search_type.*?\\]", "");
				}

				template.setBefore(before);
				template.setAfter(after);
				template.save();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

}
