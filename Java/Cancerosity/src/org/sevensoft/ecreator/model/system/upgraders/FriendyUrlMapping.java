package org.sevensoft.ecreator.model.system.upgraders;

import java.sql.SQLException;

import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.Row;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author Stephen K Samuel samspade79@gmail.com 24 Oct 2009 11:09:51
 * 
 *         This class will grab the friendly URLS for all the pages and create a redirect file
 * 
 */
public class FriendyUrlMapping {

	private final RequestContext	context;

	private static final int	HTTP_CODE	= 301;

	public FriendyUrlMapping(RequestContext context) {
		super();
		this.context = context;
	}

	public String generate() throws SQLException {

		StringBuilder sb = new StringBuilder();

		Query q = new Query(context, "select productId, seo_url from pdo_products");
		for (Row row : q.execute()) {

			if (row.getString(1) == null)
				continue;

			long id = row.getLong(0);
			String url = row.getString(1).replaceFirst("/l\\d+\\.html", "").replace("/", "-");
			String newURL = "/" + url + "-i" + id + ".html";

			sb.append(HTTP_CODE);
			sb.append("\t");
			sb.append(url);
			sb.append("\t");
			sb.append(newURL);
			sb.append("\n");
		}

		return sb.toString().trim();
	}

}
