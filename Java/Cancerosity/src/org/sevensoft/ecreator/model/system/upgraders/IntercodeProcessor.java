package org.sevensoft.ecreator.model.system.upgraders;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author Stephen K Samuel stephenksamuel@gmail.com Jul 9, 2010 2:31:32 PM
 * 
 */
public class IntercodeProcessor {

	private String			url;

	private final RequestContext	context;

	private static Logger		logger		= Logger.getLogger(IntercodeProcessor.class);

	private static final int	flags			= Pattern.DOTALL | Pattern.CASE_INSENSITIVE;

	private static final Pattern	BIG_HEADING		= Pattern.compile("\\[bigheading\\](.*?)\\[\\/bigheading\\]", flags);

	private static final Pattern	BOLD			= Pattern.compile("\\[bold\\](.*?)\\[\\/bold\\]", flags);
	private static final Pattern	BULLET		= Pattern.compile("\\[bullet\\](.*?)\\[\\/bullet\\]", flags);
	private static final Pattern	CENTER		= Pattern.compile("\\[center\\](.*?)\\[\\/center\\]", flags);
	private static final Pattern	EMAIL			= Pattern.compile("\\[email=(.*?)\\](.*?)\\[\\/email\\]", flags);
	private static final Pattern	EMAIL2		= Pattern.compile("\\[email\\](.*?)\\[\\/email\\]", flags);
	private static final Pattern	IMAGE			= Pattern.compile("\\[image\\](.*?)\\[\\/image\\]", flags);
	private static final Pattern	LINK			= Pattern.compile("\\[link\\](http:\\/\\/)?(.*?)\\[\\/link\\]", flags);
	private static final Pattern	LINK2			= Pattern.compile("\\[link=(http:\\/\\/)?(.*?)\\](.*?)\\[\\/link\\]", flags);
	private static final Pattern	MAP			= Pattern.compile("\\[map\\](.*?)\\[\\/map\\]", flags);
	private static final Pattern	SMALL_HEADING	= Pattern.compile("\\[smallheading\\](.*?)\\[\\/smallheading\\]", flags);

	public IntercodeProcessor(RequestContext context) {
		super();
		this.context = context;
	}

	public String process(String content) {

		if (content == null)
			return content;

		// some contents have escape on quotes, (\"), so remove them
		content = content.replace("\\\"", "\"").replace("\\'", "'");

		logger.debug("[UpgradeGws] stripping intercode and replacing with standard HTML");
		logger.debug("[UpgradeGws] content before: " + content);

		content = BOLD.matcher(content).replaceAll("<b>$1</b>");
		content = CENTER.matcher(content).replaceAll("<center>$1</center>");
		content = BULLET.matcher(content).replaceAll("<li>$1</li>");
		content = BIG_HEADING.matcher(content).replaceAll("<font size=\"4\"><b>$1</b></font>");
		content = SMALL_HEADING.matcher(content).replaceAll("<font size=\"3\"><b>$1</b></font>");
		content = LINK.matcher(content).replaceAll("<a href=\"http://$2\" target=\"_blank\">$2</a>");
		content = LINK2.matcher(content).replaceAll("<a href=\"http://$2\" target=\"_blank\">$3</a>");
		content = EMAIL.matcher(content).replaceAll("<a href=\"mailto:$1\">$2</a>");
		content = EMAIL2.matcher(content).replaceAll("<a href=\"mailto:$1\">$1</a>");
		content = MAP.matcher(content).replaceAll(
				"<a href=\"http://www.multimap.com/map/browse.cgi?client=public&db=pc&cidr_client=none&pc=$1&quicksearch=$1\">$1</a>");
		content = content.replace("\n", "<br/>");

		logger.debug("[UpgradeGws] content after: " + content);

		StringBuffer sb = new StringBuffer();
		Matcher matcher = IMAGE.matcher(content);
		while (matcher.find()) {

			logger.debug("Found matching image: group0: " + matcher.group(0) + " group1: " + matcher.group(1));

			String filename = matcher.group(1);
			filename = resolveImage(filename);
			if (filename == null)
				filename = "FILENOTFOUND";

			matcher.appendReplacement(sb, "<img src='images/" + filename + "' border='0'>");
		}
		matcher.appendTail(sb);

		content = sb.toString();

		Pattern pattern = Pattern.compile("<img src=\"images/(.+?)\"", Pattern.CASE_INSENSITIVE);
		matcher = pattern.matcher(content);

		sb = new StringBuffer();
		while (matcher.find()) {

			logger.debug("Found img tag: group0: " + matcher.group(0) + " group1: " + matcher.group(1));

			String filename = matcher.group(1);
			filename = resolveImage(filename);

			matcher.appendReplacement(sb, "<img src=\"images/" + filename + "\" ");
		}

		matcher.appendTail(sb);

		content = sb.toString();

		// remove any globalwebstore1/2 links
		content = content.replaceAll("\"http://www.globalwebstore1.co.uk/[a-zA-Z0-9]+/", "\"");
		content = content.replaceAll("\"http://www.globalwebstore2.co.uk/[a-zA-Z0-9]+/", "\"");

		return sb.toString();
	}

	/**
	 * Checks a given image filename and returns the correct filename for the image.
	 */
	private String resolveImage(String filename) {

		// check if image exists locally and if so just returns current filename
		File file = ResourcesUtils.getRealImage(filename);
		if (file.exists())
			return filename;

		// try the file but with lower case
		file = ResourcesUtils.getRealImage(filename.toLowerCase());
		if (file.exists())
			return filename;

		// image does not exist on filesystem so we will try to download it if url was supplied
		if (url != null) {

			String imageUrl = url + "/images/" + filename;
			try {

				filename = Img.download(context, imageUrl);
				return filename;

			} catch (IOException e) {

			}
		}

		// if we are here then the image doesn't exist as is and it could not be downloaded using the url supplied so try differnt ext's locally
		for (String ext : new String[] { "gif", "GIF", "jpg", "JPG" }) {

			file = ResourcesUtils.getRealImage(filename + "." + ext);
			if (file.exists())
				return filename + "." + ext;

		}

		// try differnt ext's for download
		for (String ext : new String[] { "gif", "GIF", "jpg", "JPG" }) {

			String imageUrl = url + "/images/" + filename + "." + ext;

			logger.debug("attemping to download image from: " + imageUrl);

			try {

				filename = Img.download(context, imageUrl);

				logger.debug("downloaded image to: " + filename);

				return filename;

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return null;
	}
}
