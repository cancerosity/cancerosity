package org.sevensoft.ecreator.model.system.users;

import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.sevensoft.commons.superstrings.RandomHelper;
import org.sevensoft.ecreator.model.crm.jobsheets.Job;
import org.sevensoft.ecreator.model.crm.jobsheets.JobSearcher;
import org.sevensoft.ecreator.model.crm.mailboxes.Mailbox;
import org.sevensoft.ecreator.model.media.images.ImageSupport;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.users.privileges.Privilege;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author Sam 09-Jul-2003 17:13:44
 */
@Table("users")
public class User extends ImageSupport implements Selectable {

	public static final String	SuperIpsKey		= "superips";

	public static final String	DeveloperIpsKey	= "developerips";

	/**
	 * Returns a list of active users
	 * 
	 */
	public static List<User> getActive(RequestContext context) {
		Query q = new Query(context, "select * from # where active=1 order by name");
		q.setTable(User.class);
		return q.execute(User.class);
	}

	/**
	 * Returns any available admin user
	 * 
	 */
	public static User getAdmin(RequestContext context) {
		Query q = new Query(context, "select * from # where administrator=1 and active=1");
		q.setTable(User.class);
		return q.get(User.class);
	}

	public static List<User> getAll(RequestContext context) {
		Query q = new Query(context, "select * from # order by name");
		q.setTable(User.class);
		return q.execute(User.class);
	}

	public static User getByLogin(RequestContext context, String username, String password) {

		Query q = new Query(context, "select * from # where username=? and password=?");
		q.setTable(User.class);
		q.setParameter(username);
		q.setParameter(password);
		return q.get(User.class);
	}

	public static User getByUsername(RequestContext context, String username) {

		Query q = new Query(context, "select * from # where username=?");
		q.setTable(User.class);
		q.setParameter(username);
		return q.get(User.class);
	}

	public static User getUserFromSession(RequestContext context, String sessionId) {

		Query q = new Query(context, "select u.* from # u join # s on u.id=s.user where s.sessionId=?");
		q.setTable(User.class);
		q.setTable(UserSession.class);
		q.setParameter(sessionId);
		User user = q.get(User.class);

		// if member is not null then update session
		if (user != null) {

			q = new Query(context, "update # set timestamp=? where sessionId=?");
			q.setTable(UserSession.class);
			q.setParameter(System.currentTimeMillis());
			q.setParameter(sessionId);
			q.run();
		}

		return user;
	}

	/**
	 * Returns true if this ip is in the superman list or if we are localhost for development
	 */
	public static boolean isSuperIp(RequestContext context) {

		if (context.isLocalhost()) {
			return true;
		}

        //todo moved to  context.isLocalhost() to be able use server txt files with set of superIPs
		// check super ips from file based on server
//		Set<String> supermenIps = (Set<String>) context.getServletAttribute(User.SuperIpsKey);
//		if (supermenIps != null) {
//			for (String ip : supermenIps) {
//				if (context.getRemoteIp().startsWith(ip)) {
//					return true;
//				}
//			}
//		}

		// check super ips from database ones
		if (Config.getInstance(context).getSupermanIps().contains(context.getRemoteIp())) {
			return true;
		}

		return false;
	}

	/**
	 * Returns true if we are currently set as superman
	 */
	public static boolean isSuperman(RequestContext context) {
		return context.containsAttribute("superman");
	}

	public static void logout(RequestContext context, String sessionId) {

		Query q = new Query(context, "delete from # where sessionId=?");
		q.setTable(UserSession.class);
		q.setParameter(sessionId);
		q.run();
	}

	/**
	 * This user is a sales person
	 */
	private boolean		salesPerson;

	/**
	 * This user is a crm user
	 */
	private boolean		crm;

	private boolean		active, administrator;

	private String		username, name, password;

	/**
	 * Do not send this user and emails from the system
	 */
	private boolean		disableEmails;

	/**
	 * These are the privileges a user does NOT have !
	 * I am using it this way because as we add more, they will default to being on, rather than off. 
	 */
	private Set<Privilege>	restrictions;

	private String		email;

	public User(RequestContext context) {
		super(context);
	}

	public User(RequestContext context, String username) {
		this(context, username, username, null);
		setPassword();

		save();
	}

	public User(RequestContext context, String name, String username, String password) {
		super(context);

		this.active = true;
		this.name = name;
		this.username = username;
		this.password = password;

		save();
	}

	public void addPrivilege(Privilege privilege) {
		getRestrictions().remove(privilege);
	}

	@Override
	public boolean delete() {

		if (administrator) {

			// we always require at least one admin
			Query q = new Query(context, "select count(*) from # where administrator=1 and active=1");
			q.setTable(User.class);
			if (q.getInt() < 2) {
				return false;
			}
		}

		active = false;
		administrator = false;
		save();

		/*
		 * Now no longer active so remove from mailboxes
		 */
		Query q = new Query(context, "update # set owner=0 where owner=?");
		q.setTable(Mailbox.class);
		q.setParameter(this);
		q.run();

		// logout sessions
		new Query(context, "delete from # where user=?").setTable(UserSession.class).setParameter(this).run();

		return true;
	}

	public String getAlt() {
		return null;
	}

	public String getEmail() {
		return email;
	}

	public int getImageLimit() {
		return 1;
	}

	public List<Job> getJobs() {

		JobSearcher searcher = new JobSearcher(context);
		searcher.setOwner(this);
		searcher.setStatus(Job.Status.Outstanding);
		searcher.setSort(Job.SortType.Jobsheet);

		return searcher.execute();
	}

	public String getLabel() {
		return getName();
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	public Set<Privilege> getPrivileges() {

		Set<Privilege> set = new TreeSet<Privilege>();
		set.addAll(EnumSet.allOf(Privilege.class));
		set.removeAll(getRestrictions());
		return set;
	}

	public Set<Privilege> getRestrictions() {
		if (restrictions == null) {
			restrictions = new HashSet();
		}
		return restrictions;
	}

	public String getType() {
		return administrator ? "Administration" : "User";
	}

	public String getUsername() {
		return username;
	}

	public String getValue() {
		return getIdString();
	}

	public boolean hasEmail() {
		return email != null;
	}

	public boolean hasPassword() {
		return password != null;
	}

	public boolean hasPrivilege(Privilege privilege) {

		if (getRestrictions().contains(privilege))
			return false;
		else
			return true;
	}

	public boolean isActive() {
		return active;
	}

	public boolean isAdministrator() {
		return administrator;
	}

	public final boolean isCrm() {
		return crm;
	}

	public final boolean isDisableEmails() {
		return disableEmails;
	}

	public final boolean isSalesPerson() {
		return salesPerson;
	}

	public void login(String sessionId) {

		logout(context, sessionId);
		new UserSession(context, this, sessionId);
	}

	public void removePrivilege(Privilege privilege) {
		getRestrictions().add(privilege);
	}

	@Override
	protected void schemaInit(RequestContext context) {

		// check for at least one administrator account
		Query q = new Query(context, "select count(*) from # where administrator=1 and active=1");
		q.setTable(User.class);
		if (q.getInt() == 0) {
			User administrator = new User(context, "admin", "admin", RandomHelper.getRandomDigits(4).toLowerCase());
			administrator.administrator = true;
			administrator.save();
		}
	}

	public void setAdministrator(boolean b) {

		if (administrator == b)
			return;

		if (!b) {

			// check we have at least one other admin
			if (SimpleQuery.count(context, User.class, "administrator", 1, "active", 1) > 0) {
                administrator = b;
                return;
			}
		}

		administrator = b;
	}

	public final void setCrm(boolean crm) {
		this.crm = crm;
	}

	public final void setDisableEmails(boolean disableEmails) {
		this.disableEmails = disableEmails;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPassword() {
		this.password = RandomHelper.getRandomDigits(4).toLowerCase();
	}

	public void setPassword(String p) {
		this.password = p.trim();

	}

	public void setPrivileges(Collection<Privilege> c) {

		// add all as restrictions, then remove any set as privileges
		this.restrictions = new HashSet();
		this.restrictions.addAll(Arrays.asList(Privilege.values()));

		if (c != null) {
			logger.fine("[User] removing privileges " + c);
			this.restrictions.removeAll(c);
		}
	}

	public final void setSalesPerson(boolean salesPerson) {
		this.salesPerson = salesPerson;
	}

	public void setUsername(String s) throws IllegalArgumentException {

		s = s.trim();

		if (s.equals(this.username)) {
			return;
		}

		Query q = new Query(context, "select count(*) from # where username=?");
		q.setTable(User.class);
		q.setParameter(s);
		if (q.getInt() > 0) {
			throw new IllegalArgumentException("Username already in use.");
		}

		this.username = s;
	}

	public boolean useImageDescriptions() {
		return false;
	}

}