package org.sevensoft.ecreator.model.login.facebook;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.attributes.Attribute;

/**
 * User: Tanya
 * Date: 01.02.2011
 */
@Table("settings_facebook_login")
@Singleton
public class FacebookLoginSettings extends EntityObject {

    protected FacebookLoginSettings(RequestContext context) {
        super(context);
    }

    public static FacebookLoginSettings getInstance(RequestContext context) {
        return getSingleton(context, FacebookLoginSettings.class);
    }

    private String appId;
    private ItemType registerAccountType;

    private Attribute realNameAttribute;
    private Attribute genderAttribute;
    private Attribute birthdayAttribute;
    private Attribute locationAttribute;
    private Attribute phoneAttribute;
    private Attribute anniversaryAttribute;
    private Attribute aboutMeAttribute;
    private Attribute imageAttribute;


    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public ItemType getRegisterAccountType() {
        return (ItemType) (registerAccountType == null ? null : registerAccountType.pop());
    }

    public ItemType getAccountType() {
        return getRegisterAccountType() != null ? getRegisterAccountType() : ItemType.getAccount(context);
    }

    public void setRegisterAccountType(ItemType registerAccountType) {
        this.registerAccountType = registerAccountType;
    }

    public Attribute getRealNameAttribute() {
        return realNameAttribute == null ? null : (Attribute) realNameAttribute.pop();
    }

    public void setRealNameAttribute(Attribute realNameAttribute) {
        this.realNameAttribute = realNameAttribute;
    }

    public Attribute getGenderAttribute() {
        return genderAttribute == null ? null : (Attribute) genderAttribute.pop();
    }

    public void setGenderAttribute(Attribute genderAttribute) {
        this.genderAttribute = genderAttribute;
    }

    public Attribute getBirthdayAttribute() {
        return birthdayAttribute == null ? null : (Attribute) birthdayAttribute.pop();
    }

    public void setBirthdayAttribute(Attribute birthdayAttribute) {
        this.birthdayAttribute = birthdayAttribute;
    }

    public Attribute getLocationAttribute() {
        return locationAttribute == null ? null : (Attribute) locationAttribute.pop();
    }

    public void setLocationAttribute(Attribute locationAttribute) {
        this.locationAttribute = locationAttribute;
    }

    public Attribute getPhoneAttribute() {
        return phoneAttribute == null ? null : (Attribute) phoneAttribute.pop();
    }

    public void setPhoneAttribute(Attribute phoneAttribute) {
        this.phoneAttribute = phoneAttribute;
    }

    public Attribute getAnniversaryAttribute() {
        return anniversaryAttribute == null ? null : (Attribute) anniversaryAttribute.pop();
    }

    public void setAnniversaryAttribute(Attribute anniversaryAttribute) {
        this.anniversaryAttribute = anniversaryAttribute;
    }

    public Attribute getAboutMeAttribute() {
        return aboutMeAttribute == null ? null : (Attribute) aboutMeAttribute.pop();
    }

    public void setAboutMeAttribute(Attribute aboutMeAttribute) {
        this.aboutMeAttribute = aboutMeAttribute;
    }

    public Attribute getImageAttribute() {
        return imageAttribute == null ? null : (Attribute) imageAttribute.pop();
    }

    public void setImageAttribute(Attribute imageAttribute) {
        this.imageAttribute = imageAttribute;
    }

    public String getLoginButtonCode() {
        return "<fb:login-button autologoutlink=\"true\" perms=\"email,user_birthday,status_update,publish_stream\"></fb:login-button>\n";
    }

}
