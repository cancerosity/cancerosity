package org.sevensoft.ecreator.model.login.facebook;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.config.Config;

/**
 * User: Tanya
 * Date: 21.02.2011
 */
public class FacebookIntegration {

    private final RequestContext context;
    private transient FacebookLoginSettings facebookLoginSettings;

    public FacebookIntegration(RequestContext context) {
        this.context = context;
        this.facebookLoginSettings = FacebookLoginSettings.getInstance(context);
    }

    @Override
    public String toString() {
        if (!Module.FacebookLogin.enabled(context)) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("<div id=\"fb-root\"></div>\n");
        sb.append("<script type=\"text/javascript\">\n");
        sb.append("window.fbAsyncInit = function() {\n");
        sb.append(" FB.init({appId: '" + FacebookLoginSettings.getInstance(context).getAppId() + "'," +
                " session : '" + context.getSessionId() + "'," +
                " status: true, cookie: true, xfbml: true" +
                ", domain: '" + Config.getInstance(context).getDomain() + "' " +
                "});\n\n");

        sb.append("      FB.Event.subscribe('auth.login', function(response) {\n");
//      sb.append(  "         // do something with response\n" );
        sb.append("             if(response.status == 'connected'){\n");
        sb.append("                 login(response);\n");
        sb.append("                 FB.api('/me', function(response) {\n" +
//                "      alert('You have successfully logged in, '+response.name+\"!\");\n" +
                "                   });\n");
        sb.append("             }\n");
        sb.append("     });\n");
        sb.append("     FB.Event.subscribe('auth.logout', function(response) {\n");
//        sb.append("       document.location.href=\"http://\"+document.location.host+\"/l.do?_a=logout&linkback=\"+document.location.pathname+document.location.search;\n");
//        sb.append("    alert('You have successfully logged out!');");
        sb.append("             return false;\n");
        sb.append("     });\n\n");

        sb.append("     FB.getLoginStatus(function(response) {\n");
        sb.append("             FB.api('/me', function(response) {\n" +
//                "               alert('Welcome, '+response.name+\"!\");\n" +
                "               });");
        sb.append("     });\n");
        sb.append(" };\n");
        sb.append("     (function() {\n");
        sb.append("         var e = document.createElement('script');\n");
        sb.append("         e.type = 'text/javascript';\n");
        sb.append("         e.src = document.location.protocol +\n");
        sb.append("            '//connect.facebook.net/en_US/all.js';\n");
        sb.append("         e.async = true;\n");
        sb.append("         document.getElementById('fb-root').appendChild(e);\n");
        sb.append("     }());\n");
        sb.append("      function login(response) {\n");
        sb.append("         var query = FB.Data.query('select name, email, hometown_location, sex, pic_square, uid from user where uid={0}', response.session.uid);\n");
        sb.append("         query.wait(function(rows) {\n");
        sb.append("             return document.location.href=\"http://\"+document.location.host+\"/l.do?_a=loginFacebook&email=\"+rows[0].email+\"&password=\"+rows[0].uid+\"&name=\"+rows[0].name+\"&linkback=\"+document.location.pathname+document.location.search;\n");
        sb.append("         });\n");
        sb.append("     };\n ");
        sb.append("</script>\n");

        return sb.toString();
    }
}