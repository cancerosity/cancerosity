package org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions;


/**
 * @author sks 25-Jul-2005 17:39:19
 *
 */
public class SmsGatewayException extends Exception {

	public SmsGatewayException() {
		super();
	}

	public SmsGatewayException(String message) {
		super(message);
	}

	public SmsGatewayException(String message, Throwable cause) {
		super(message, cause);
	}

	public SmsGatewayException(Throwable cause) {
		super(cause);
	}

}
