package org.sevensoft.ecreator.model.marketing.newsletter;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.ecreator.model.search.SearchOwner;
import org.sevensoft.ecreator.model.extras.reorder.ReorderOwner;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.items.highlighted.blocks.markup.HighlightedItemsBlockMarkup2;

import java.util.List;
import java.util.Set;

/**
 * User: Tanya
 * Date: 16.07.2012
 */
@Table("newsletters_options")
public class NewsletterOption extends EntityObject implements ReorderOwner, SearchOwner {

    private Newsletter newsletter;

    private String name;
    private boolean hidden;
    private String description;
    private String subject;
    private Search search;
    private Markup markup;
    private String autoResponder;

    private int position;


    public NewsletterOption(RequestContext context) {
        super(context);
    }

    public NewsletterOption(RequestContext context, Newsletter newsletter, String name) {
        super(context);
        this.newsletter = newsletter;
        this.name = name;
        this.search = new Search(context, this);

        save();
    }

    public static List<NewsletterOption> get(RequestContext context, Newsletter newsletter) {
        Query q = new Query(context, "select * from # where newsletter=? order by position");
        q.setParameter(newsletter);
        q.setTable(NewsletterOption.class);
        return q.execute(NewsletterOption.class);
    }

    public static List<NewsletterOption> get(RequestContext context) {
        Query q = new Query(context, "select * from # order by name");
        q.setTable(NewsletterOption.class);
        return q.execute(NewsletterOption.class);
    }

    public static List<NewsletterOption> getVisible(RequestContext context, Newsletter newsletter) {
        Query q = new Query(context, "select * from # where hidden=? and newsletter=? order by name");
        q.setTable(NewsletterOption.class);
        q.setParameter(0);
        q.setParameter(newsletter);
        return q.execute(NewsletterOption.class);
    }

    public static List<NewsletterOption> getVisible(RequestContext context) {
        Query q = new Query(context, "select * from # where hidden=? order by name");
        q.setTable(NewsletterOption.class);
        q.setParam(0);
        return q.execute(NewsletterOption.class);
    }

    public Newsletter getNewsletter() {
        return newsletter != null ? (Newsletter) newsletter.pop() : null;
    }

    public void setNewsletter(Newsletter newsletter) {
        this.newsletter = newsletter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean hasDescription() {
        return description != null;
    }

    public String getSubject() {
        return subject != null ? subject : getName();
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Search getSearch() {
        return search != null ? (Search) search.pop() : null;
    }

    public void setSearch(Search search) {
        this.search = search;
    }

    public Markup getMarkup() {

        HighlightedItemsBlockMarkup2 highlightedItemsBlockMarkup = new HighlightedItemsBlockMarkup2();

        if (markup == null) {
            markup = new Markup(context, getName() + " Markup", new HighlightedItemsBlockMarkup2());
            markup.setTableClass(highlightedItemsBlockMarkup.getCssClass());
            markup.save();
            save();
        }

        return markup.pop();
    }

    public void setMarkup(Markup markup) {
        this.markup = markup;
    }

    public String getAutoResponder() {
        return autoResponder;
    }

    public void setAutoResponder(String autoResponder) {
        this.autoResponder = autoResponder;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }


    public String getReorderName() {
        return getName();
    }

    @Override
    public boolean delete() {
        getSearch().delete();
        return super.delete();
    }

    /**
     * Subscribe a subscriber to this newsletter
     */
    public void subscribe(Subscriber subscriber) {
        new Subscription(context, this, subscriber);
    }

    /**
     * remove a subscriber
     */
    public void unsubscribe(Subscriber s) {
        SimpleQuery.delete(context, Subscription.class, "newsletterOption", this, "subscriber", s);
    }

    public static NewsletterOption getDefault(RequestContext context) {
        Query q = new Query(context, "select * from #");
        q.setTable(NewsletterOption.class);
        return q.get(NewsletterOption.class);
    }

    /**
     * Returns a set of validated email addresses for this collection of newsletters
     */
    public Set<String> getValidatedEmails() {

        String query = "select distinct s.email from # s join # s2 on s.id=s2.subscriber where s.validating=? and s2.newsletterOption=?";

        Query q = new Query(context, query);
        q.setTable(Subscriber.class);
        q.setTable(Subscription.class);
        q.setParameter(false);
        q.setParameter(this);

        return q.getStringsSet();
    }

}
