package org.sevensoft.ecreator.model.marketing.newsletter;

import java.util.List;

/**
 * @author sks 1 Feb 2007 17:31:55
 *
 */
public interface NewsletterOptable {

	public List<Newsletter> getNewsletterOptIns();

	public boolean isNewsletterOptIn(Newsletter newsletter);

	public void removeNewsletterOptIns();

	public void setNewsletterOptIns(List<Newsletter> newsletters);
}
