package org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions;


/**
 * @author sks 25-Jul-2005 17:38:09
 *
 */
public class SmsGatewayMessagingException extends Exception {

	public SmsGatewayMessagingException() {
		super();
	}

	public SmsGatewayMessagingException(String message) {
		super(message);
	}

	public SmsGatewayMessagingException(String message, Throwable cause) {
		super(message, cause);
	}

	public SmsGatewayMessagingException(Throwable cause) {
		super(cause);
	}

}
