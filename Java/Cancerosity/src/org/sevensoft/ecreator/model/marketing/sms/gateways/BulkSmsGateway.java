package org.sevensoft.ecreator.model.marketing.sms.gateways;

import java.io.IOException;
import java.util.logging.Logger;

import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.ecreator.model.marketing.sms.SmsSettings;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsCreditsException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayAccountException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayMessagingException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsMessageException;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 29-Jul-2005 07:27:10
 * 
 */
public class BulkSmsGateway extends SmsGatewayImpl {

	private static Logger	logger	= Logger.getLogger("ecreator");

	@Override
	public void send(RequestContext context, String number, String sender, CharSequence message) throws IOException, SmsMessageException,
			SmsGatewayMessagingException, SmsGatewayAccountException, SmsCreditsException, SmsGatewayException {

		Config config = Config.getInstance(context);

		if (message.length() > 160) {
			throw new SmsMessageException();
		}

		SmsSettings smsSettings = SmsSettings.getInstance(context);

		String username = smsSettings.getBulkSmsUsername();
		String password = smsSettings.getBulkSmsPassword();

		HttpClient hc = new HttpClient("http://www.bulksms.co.uk:5567/eapi/submission/send_sms/2/2.0");

		if (username == null && config.isDemo()) {
			username = SmsSettings.DEMO_SMS_USERNAME;
		}

		if (password == null && config.isDemo()) {
			password = SmsSettings.DEMO_SMS_PASSWORD;
		}

        if (username == null || password == null) {
            throw new SmsGatewayException("Invalid gateway username or password");
        }

        hc.setParameter("username", username);
		hc.setParameter("password", password);
		hc.setParameter("message", message.toString());

		if (number.startsWith("0")) {
			number = "44" + number.substring(1);
		}

		logger.fine("[BulkSmsGateway] username=" + username + ", password=" + password + ", number=" + number + ", message=" + message);

		hc.setParameter("msisdn", number);
		if (sender != null)
			hc.setParameter("sender", sender.length() > 11 ? sender.substring(0, 11) : sender);
		hc.connect();

		String response = hc.getResponseString();
		logger.fine("[BulkSmsGateway] response=" + response);

		String[] s = response.split("\\|");

		String code = s[0];

		if ("22".equals(code)) {
			throw new SmsGatewayException("Internal failure: " + s[1]);
		}

		if ("23".equals(code)) {
			throw new SmsGatewayAccountException("Authentication failure: " + s[1]);
		}

		if ("24".equals(code)) {
			throw new SmsGatewayMessagingException("Data validation failed: " + s[1]);
		}

		if ("25".equals(code)) {
			throw new SmsCreditsException("You do not have sufficient credits: " + s[1]);
		}

		if ("26".equals(code)) {
			throw new SmsCreditsException(s[1]);
		}

		if ("27".equals(code)) {
			throw new SmsCreditsException("You have exceeded your daily quota: " + s[1]);
		}

		if ("40".equals(code)) {
			throw new SmsGatewayException("Temporarily unavailable: " + s[1]);
		}
	}

}
