package org.sevensoft.ecreator.model.marketing.sms.credits;

import java.util.List;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 25 Apr 2007 11:02:49
 *
 */
@Table("sms_credits_packages")
public class SmsCreditPackage extends EntityObject implements Positionable {

	public static List<SmsCreditPackage> get(RequestContext context) {
		return SimpleQuery.execute(context, SmsCreditPackage.class);
	}

	private int		position;
	private int		credits;
	private Money	fee;

	private SmsCreditPackage(RequestContext context) {
		super(context);
	}

	public SmsCreditPackage(RequestContext context, int credits, Money fee) {
		super(context);
		
		this.credits = credits;
		this.fee = fee;

		save();
	}

	@Override
	public synchronized boolean delete() {
		new Query(context, "update # set smsCreditPackage=0 where smsCreditPackage=?").setTable(SmsCreditsSession.class).setParameter(this).run();
		return super.delete();
	}

	public final int getCredits() {
		return credits;
	}

	public Money getFee() {
		return fee;
	}

	public final int getPosition() {
		return position;
	}

	public final void setCredits(int credits) {
		this.credits = credits;
	}

	public final void setFee(Money fee) {
		this.fee = fee;
	}

	public final void setPosition(int position) {
		this.position = position;
	}

}
