package org.sevensoft.ecreator.model.marketing.sms.sync;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.marketing.sms.SmsBulletin;
import org.sevensoft.ecreator.model.marketing.sms.SmsRegistration;
import org.sevensoft.ecreator.model.marketing.sms.SmsSettings;
import org.sevensoft.jeezy.http.RequestContext;

import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.CsvSaver;
import com.ricebridge.csvman.CsvSpec;
import com.ricebridge.csvman.QuoteType;

/**
 * @author sks 25 Aug 2006 11:52:29
 *
 */
public class SmsCsvExporter {

	private final List<SmsBulletin>	bulletins;
	private final SmsSettings		settings;
	private final List<Attribute>		attributes;
	private List<SmsRegistration>		registrations;

	public SmsCsvExporter(RequestContext context) {
		this.settings = SmsSettings.getInstance(context);
		this.attributes = settings.getAttributes();
		this.bulletins = SmsBulletin.get(context);
		this.registrations = SmsRegistration.get(context);
	}

	public File export() throws IOException {

		File file = File.createTempFile("sms_csv", null);

		CsvManager csvman = new CsvManager();
		CsvSpec spec = csvman.getCsvSpec();
		spec.setQuote('"');
		spec.setDoubleQuote(true);
		spec.setQuoteType(QuoteType.All);
		spec.setSeparator(",");
		csvman.setCsvSpec(spec);

		CsvSaver saver = csvman.makeSaver(file);
		saver.begin();

		header(saver);

		for (SmsRegistration registration : registrations) {
			registration(registration, saver);
		}

		saver.end();

		return file;
	}

	private void header(CsvSaver saver) {

		List<String> header = new ArrayList();
		header.add("Number");

		if (bulletins.size() > 1) {
			header.add("Bulletin");
		}

		for (Attribute attribute : attributes) {
			header.add(attribute.getName());
		}

		saver.next(header.toArray(new String[header.size()]));
	}

	private void registration(SmsRegistration registration, CsvSaver saver) {

		List<String> row = new ArrayList();

		MultiValueMap<Attribute, String> attributeValues = registration.getAttributeValues();
		row.add(registration.getNumber());

		if (bulletins.size() > 1) {
			row.add(registration.getBulletin().getName());
		}

		for (Attribute attribute : attributes) {
			String value = attributeValues.get(attribute);
			row.add(value);
		}

		CollectionsUtil.replaceNulls(row, "");
		saver.next(row.toArray(new String[0]));
	}
}
