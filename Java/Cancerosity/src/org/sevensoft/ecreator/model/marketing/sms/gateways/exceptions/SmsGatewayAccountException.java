package org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions;


/**
 * @author sks 25-Jul-2005 17:37:00
 *
 */
public class SmsGatewayAccountException extends Exception {

	public SmsGatewayAccountException() {
		super();
	}

	public SmsGatewayAccountException(String message) {
		super(message);
	}

	public SmsGatewayAccountException(String message, Throwable cause) {
		super(message, cause);
	}

	public SmsGatewayAccountException(Throwable cause) {
		super(cause);
	}

}
