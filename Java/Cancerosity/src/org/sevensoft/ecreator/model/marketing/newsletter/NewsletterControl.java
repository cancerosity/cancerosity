package org.sevensoft.ecreator.model.marketing.newsletter;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.frontend.marketing.NewsletterHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.support.AttributeOwnerSupport;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.marketing.newsletter.templates.NewsletterTemplate;
import org.sevensoft.ecreator.model.marketing.newsletter.bots.NewsletterEmail;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

import java.util.*;

/**
 * @author sks 06-Jul-2005 14:24:47
 */
@Table("settings_newsletter")
@Singleton
public class NewsletterControl extends AttributeOwnerSupport {

    private static List<Newsletter> get(RequestContext context) {
        Query q = new Query(context, "select * from # order by name");
        q.setTable(Newsletter.class);
        return q.execute(Newsletter.class);
    }

    public static NewsletterControl getInstance(RequestContext context) {
        return getSingleton(context, NewsletterControl.class);
    }

    /**
     *
     */
    private String signupContent;

    /**
     * Content of the auto sidebar
     */
    private String sidebarContent;

    /**
     *
     */
    private boolean recipients;

    /**
     * Show opt in when registering as a member.
     */
    private boolean registrationOptIn;

    /**
     *
     */
    private String registrationOptInText;

    private boolean saveNewsletters;

    private boolean validateEmails;

    private boolean checkoutOptIn;

    public NewsletterControl(RequestContext context) {
        super(context);
    }

    public boolean checkCode(String code) {
        return true;
    }

    private Collection<String> getAccountEmails(ItemType accountType, MultiValueMap<Attribute, String> attributeValues) {

        ItemSearcher searcher = new ItemSearcher(context);

        searcher.setIncludeHidden(true);
        searcher.setSearchableOnly(false);
        searcher.setVisibleOnly(false);
        searcher.setAttributeValues(attributeValues);
        if (accountType != null) {
            searcher.setItemType(accountType);
        } else {
            searcher.setItemModule(ItemModule.Account);
        }
        searcher.setHasEmail(true);


        Collection<String> emails = new HashSet<String>();
        for (Item item : searcher) {
            emails.add(item.getEmail());
        }

        return emails;
    }

    public String getName() {
        return "Newsletter";
    }

    public String getRegistrationOptInText() {
        return registrationOptInText == null ? "Click here to receive our email newsletter. "
                + "You can unsubscribe at any time by following the link at the bottom of each newsletter we sent out." : registrationOptInText;
    }

    public String getSignupContent() {
        return signupContent == null ? "To subscribe to our newsletter we require a valid email address.<br/>"
                + "You can unsubscribe from the newsletter at any time using the link at the bottom of each email we sent out.<br/>"
                + "Please enter the email address you want to use to receive our newsletters in the box below." : signupContent;
    }

    /**
     * Returns total number of distint emails
     *
     * @return
     */
    public int getSubscriberCount() {
        Query q = new Query(context, "select count(distinct email) from #");
        q.setTable(Subscriber.class);
        return q.getInt();
    }

    /**
     * Returns a set of validated email addresses for this collection of newsletters
     */
    private Set<String> getValidatedEmails(Collection<Newsletter> newsletters) {

        String query = "select distinct s.email from # s join # s2 on s.id=s2.subscriber where s.validating=? " +
                StringHelper.repeat(" and s2.newsletter=?", newsletters.size());

        Query q = new Query(context, query);
        q.setTable(Subscriber.class);
        q.setTable(Subscription.class);
        q.setParameter(false);
        for (Newsletter newsletter : newsletters) {
            q.setParameter(newsletter);
        }

        return q.getStringsSet();
    }

    private Set<String> getValidatedEmails(Set<NewsletterOption> newsletterOptions) {
        Set<String> emails = new TreeSet();
        for (NewsletterOption option : newsletterOptions) {
            emails.addAll(option.getValidatedEmails());
        }
        return emails;
    }

    /**
     *
     */
    public boolean isCheckoutOptIn() {
        return checkoutOptIn;
    }

    public boolean isRecipients() {
        return recipients;
    }

    public final boolean isRegistrationOptIn() {
        return registrationOptIn;
    }

    public boolean isSaveNewsletters() {
        return saveNewsletters;
    }

    public boolean isValidateEmails() {
        return validateEmails;
    }

    @Override
    protected void schemaInit(RequestContext context) {

        /*
           * We need to make sure we have at least one newsletter created
           */

        List<Newsletter> newsletters = get(context);
        if (newsletters.isEmpty()) {
            new Newsletter(context, "Newsletter");
        }

        /*
           * Make sure all subscribers are in a newsletter
           */
        //		Query q = new Query(context, "update # set newsletter=? where newsletter=0");
        //		q.setTable(Subscriber.class);
        //		q.setParameter(n);
        //		q.run();
    }

//	/**
//	 * Sends out an access code to this email so they can access their preferences
//	 */
//	public void sendAccessCode(String email) throws EmailAddressException, SmtpServerException {
//
//		Subscriber s = Subscriber.get(context, email, false);
//		if (s == null) {
//			return;
//		}
//
//		Email e = new Email();
//		e.setSubject("Newsletter unsubscribe");
//		String link = Config.getInstance(context).getUrl() + "/ " + new Link(NewsletterHandler.class, "edit", "code", s.getCode());
//		e.setBody("Someone requested that this email address be unsubscribed from our newsletter(s). " +
//				"If this was not you, please ignore this email. Otherwise click here to unsubscribe.\n\n" + link);
//		e.setFrom(Config.getInstance(context).getServerEmail());
//		e.setTo(email);
//		e.send(Config.getInstance(context).getSmtpHostname());
//
//	}

    public void sendNewsletter(NewsletterTemplate template, Set<Newsletter> newsletters, Set<NewsletterOption> newsletterOptions, List<String> testers, boolean subscribers, boolean accounts, String subject, String body,
                               MultiValueMap<Attribute, String> attributeValues, ItemType accountType) throws SmtpServerException {

        if (newsletters.isEmpty() && newsletterOptions.isEmpty()) {
            logger.fine("[NewsletterSettings] No newsletters selected - exiting");
            return;
        }

        final Config config = Config.getInstance(context);
        final Company company = Company.getInstance(context);

        Set<String> recipients = new HashSet();
        if (testers == null || testers.isEmpty()) {

            if (newsletters.size() > 0) {

                if (subscribers) {
                    Collection<String> emails = getValidatedEmails(newsletters);
                    logger.fine("[NewsletterSettings] Adding subscribers, total=" + emails.size());
                    recipients.addAll(emails);
                }
            }

            if (newsletterOptions.size() > 0) {

                if (subscribers) {
                    Collection<String> emails = getValidatedEmails(newsletterOptions);
                    logger.fine("[NewsletterSettings] Adding subscribers, total=" + emails.size());
                    recipients.addAll(emails);
                }
//                context.setAttribute("newsletterOption", newsletterOptions.iterator().next());
            }

            if (accounts) {

                Collection<String> emails = getAccountEmails(accountType, attributeValues);
                logger.fine("[NewsletterSettings] Adding account emails total=" + emails.size());
                recipients.addAll(emails);
            }

        } else {

            logger.fine("[NewsletterSettings] Sending to testers addresses=" + testers);
            recipients.addAll(testers);
        }

        logger.config("[NewsletterSettings] Sending newsletter to " + newsletters.size() + " newsletters and " + recipients.size() + " recipients");
        logger.fine("[NewsletterSettings] Subject: " + subject);
        logger.fine("[NewsletterSettings] Body: " + body);
        logger.fine("[NewsletterSettings] Recipients: " + recipients);

        boolean html = true;

        /*
           *  if HTML we need to replace the relative links with absolute ones.
           */
        MarkupRenderer renderer = new MarkupRenderer(context, body);
        body = renderer.toString();

        if (html) {

            body = compileBody(body, config);
        }

        logger.fine("[NewsletterSettings] Email body:\n--------\n" + body + "\n----------\n");

        for (String recip : recipients) {

            logger.fine("recip=" + recip);

            String removalUrl = null;
            Subscriber sub = Subscriber.get(context, recip, false);
            if (sub != null) {
                removalUrl = config.getUrl() + "/" + new Link(NewsletterHandler.class, "edit", "e", sub.getEmail(), "c", sub.getCode());

            }

            logger.fine("removalUrl=" + removalUrl);

            Email msg = new Email();
            msg.setSubject(subject);

            StringBuilder sb = new StringBuilder(body);

            if (html) {

                if (removalUrl != null) {
                    sb.append("<br/><br/>To unsubscribe from this newsletter, <a href='" + removalUrl + "'>click here</a>");
                }

                /*
                     * Only apply a template if this is a HTML format newsletter
                     */
                if (template != null) {

                    if (template.hasTop()) {
                        logger.fine("[NewsletterSettings] ---template top---\n" + template.getTop() + "\n---");
                        sb.insert(0, template.getTop());
                    }

                    if (template.hasBottom()) {
                        logger.fine("[NewsletterSettings] ---template bottom---\n" + template.getBottom() + "\n---");
                        sb.append(template.getBottom());
                    }
                }

                msg.setBody(sb, "text/html");

            } else {

                if (removalUrl != null) {

                    sb.append("\n\nTo unsubscribe from this newsletter, click here\n" + removalUrl);
                }

                msg.setBody(sb);
            }

            msg.setFrom(company.getName(), config.getServerEmail());
            msg.setTo(recip);
            try {
                new EmailDecorator(context, msg).send(config.getSmtpHostname());
            } catch (EmailAddressException e) {
                logger.warning(e.toString());
            }
        }
    }

    private String compileBody(String body, Config config) {
        final String absoluteImagePath = config.getUrl() + "/" + config.getImagesPath();
        final String absoluteThumbnailPath = config.getUrl() + "/" + config.getThumbnailsPath();
        //			final String url = config.getUrl();

        logger.fine("[NewsletterSettings] Absolute image path: " + absoluteImagePath);
        logger.fine("[NewsletterSettings] Absolute thumbnail path: " + absoluteThumbnailPath);
        logger.fine("[NewsletterSettings] -----Content before------");
        logger.fine(body);

        body = body.toString().replace(" src=\"images/", " src=\"" + absoluteImagePath + "/");
        body = body.toString().replace(" src=\"images/", " src=\"" + absoluteThumbnailPath + "/");

        /*
        * change relative links to absolute. The content editor seems to strip the domain just leaving in the path. We need to add in the domain,
    * but not any path info eg the servlet path must not be added
    *
    * update 28/7
    * seems ff content editor strips out domain only and ie strips out everything just leaving the relative url
    *
    *
    */

        // if the urls begin with a slash then they need path of url adding only
        body = body.toString().replace("<a href=\"/", "<a href=\"http://" + config.getServerName() + "/");

        // if the urls begin with www. then we will just add in http://
        body = body.toString().replaceAll("<a href=\"www.", "<a href=\"http://www.");

        // if the urls do not being with http:// now then they need the full url prepending
        if (body.toString().indexOf("@") == -1) {
            body = body.toString().replaceAll("<a href=\"([^h][^t][^t][^p][^:][^/][^/])", "<a href=\"" + config.getUrl() + "/$1");
        }

        logger.fine("[NewsletterSettings] ----Content after-----");
        logger.fine(body);
        return body;
    }


    public final void setCheckoutOptIn(boolean checkoutOptIn) {
        this.checkoutOptIn = checkoutOptIn;
    }

    public void setRecipients(boolean recipients) {
        this.recipients = recipients;
    }

    public final void setRegistrationOptIn(boolean registrationOptIn) {
        this.registrationOptIn = registrationOptIn;
    }

    public void setRegistrationOptInText(String s) {
        this.registrationOptInText = s;
    }

    public void setSaveNewsletters(boolean saveNewsletters) {
        this.saveNewsletters = saveNewsletters;
    }

    public void setSignupContent(String signupContent) {
        this.signupContent = signupContent;
    }

    public void setValidateEmails(boolean b) {

        if (b == validateEmails) {
            return;
        }

        validateEmails = b;
        if (b == false) {
            validateAll();
        }
    }

    /**
     * Returns true if the new subscriber requires validation
     */
    public Subscriber subscribe(Collection<Newsletter> newsletters, String email, Map<Attribute, String> map) {

        if (newsletters == null) {
            newsletters = Collections.singleton(Newsletter.getDefault(context));
        }

        Subscriber s = Subscriber.get(context, email, true);
        s.setAttributeValues(map);
        s.subscribe(newsletters);

        /*
           * If we require validation  then generate code and send it out
           */
        if (s.isValidating()) {
            try {
                s.sendValidationCode();
            } catch (EmailAddressException e) {
                e.printStackTrace();
            } catch (SmtpServerException e) {
                e.printStackTrace();
            }
        }

        return s;
    }

    /**
     * Returns true if the new subscriber requires validation
     */
    public Subscriber subscribeOpt(Collection<NewsletterOption> newsletterOptions, String email, Map<Attribute, String> map) {

        if (newsletterOptions == null) {
            newsletterOptions = Collections.singleton(NewsletterOption.getDefault(context));
        }

        Subscriber s = Subscriber.get(context, email, true);
        s.setAttributeValues(map);
        s.subscribeOpt(newsletterOptions);

        /*
           * If we require validation  then generate code and send it out
           */
        if (s.isValidating()) {
            try {
                s.sendValidationCode();
            } catch (EmailAddressException e) {
                e.printStackTrace();
            } catch (SmtpServerException e) {
                e.printStackTrace();
            }
        }

        return s;
    }

    public void tidy() {

        new Query(context, "delete from # where length(email) < 8").setTable(Subscriber.class).run();
        new Query(context, "delete from # where email like '% %' ").setTable(Subscriber.class).run();
        new Query(context, "delete from # where email like '@%' ").setTable(Subscriber.class).run();
        new Query(context, "delete from # where email like '.%@' ").setTable(Subscriber.class).run();

        new Query(context, "delete from # where email like '.%' ").setTable(Subscriber.class).run();
        new Query(context, "delete from # where email like '%.' ").setTable(Subscriber.class).run();

        new Query(context, "delete from # where email like '%..%' ").setTable(Subscriber.class).run();
        new Query(context, "delete from # where instr(email, '@') = 0 ").setTable(Subscriber.class).run();
    }

    /**
     * Validates the subscriber that matches this email
     */
    public boolean validate(String email, String code) {

        Subscriber s = Subscriber.get(context, email, false);
        if (s == null) {
            return false;
        }

        if (s.getCode().equalsIgnoreCase(code)) {
            s.validate();
            return true;
        }

        return false;
    }

    private void validateAll() {
        new Query(context, "update # set validating=0").setTable(Subscriber.class).run();
    }

    public void sendNewsletter(NewsletterOption newsletterOption, NewsletterEmail email, Set<String> testerdEmails) {
        Set<Newsletter> newsletters = Collections.emptySet();
        Set<NewsletterOption> newsletterOptions = new HashSet();
        newsletterOptions.add(newsletterOption);
        List<String> testers = new ArrayList(newsletterOption.getValidatedEmails());
        try {
            sendNewsletter(null, newsletters, newsletterOptions, testers, true, false, newsletterOption.getSubject(), email.getBody(), null, null);
        } catch (SmtpServerException e) {
            logger.severe("[NewsletterControl] "+ e);
        }
    }
}