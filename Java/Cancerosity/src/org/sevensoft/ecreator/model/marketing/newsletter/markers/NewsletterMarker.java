package org.sevensoft.ecreator.model.marketing.newsletter.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.marketing.NewsletterHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 31-Mar-2006 15:23:07
 *
 */
public class NewsletterMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.Newsletter.enabled(context)) {
			return null;
		}

		return super.link(context, params, new Link(NewsletterHandler.class), "link_newsletter");

	}

	public Object getRegex() {
		return "newsletter";
	}
}