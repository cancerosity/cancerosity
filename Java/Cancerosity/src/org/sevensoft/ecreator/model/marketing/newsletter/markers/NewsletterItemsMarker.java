package org.sevensoft.ecreator.model.marketing.newsletter.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterOption;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;
import java.util.List;

/**
 * User: Tanya
 * Date: 29.11.2012
 */
public class NewsletterItemsMarker extends MarkerHelper implements IGenericMarker {

    public Object generate(RequestContext context, Map<String, String> params) {

        if (!Module.NewsletterNewItems.enabled(context)) {
            return null;
        }
        NewsletterOption option = null;
        if (context.containsAttribute("newsletterOption")) {
            option = (NewsletterOption) context.getAttribute("newsletterOption");
        } else return null;

        ItemSearcher searcher = option.getSearch().getSearcher();
        if (params.containsKey("limit")) {
            searcher.setLimit(Integer.valueOf(params.get("limit")));
        }
        searcher.setStatus("Live");
        List<Item> items = searcher.getItems();
        MarkupRenderer r = new MarkupRenderer(context, option.getMarkup());
        r.setBodyObjects(items);

        return r;
    }

    public Object getRegex() {
        return "newsletter_items";
    }
}