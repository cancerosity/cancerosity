package org.sevensoft.ecreator.model.marketing.newsletter;

import org.sevensoft.jeezy.http.RequestContext;

/**
 * User: Tanya
 * Date: 16.07.2012
 */
public class NewsletterOptionFactory {

    public static NewsletterOption createOption(RequestContext context, Newsletter newsletter){
       return new NewsletterOption(context, newsletter,"new " + newsletter.getName() + " option");
    }
}
