package org.sevensoft.ecreator.model.marketing.affiliates;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 26 Jan 2007 13:10:27
 *
 */
@Table("affiliates_settings")
@Singleton
public class AffiliateSettings extends EntityObject {

	public static AffiliateSettings getInstance(RequestContext context) {
		return EntityObject.getSingleton(context, AffiliateSettings.class);
	}

	protected AffiliateSettings(RequestContext context) {
		super(context);
	}

}
