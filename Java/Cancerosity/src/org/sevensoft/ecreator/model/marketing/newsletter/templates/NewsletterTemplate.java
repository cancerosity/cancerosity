package org.sevensoft.ecreator.model.marketing.newsletter.templates;

import java.util.List;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 1 Sep 2006 11:15:49
 *
 */
@Table("newsletters_templates")
public class NewsletterTemplate extends EntityObject implements Selectable {

	public static List<NewsletterTemplate> get(RequestContext context) {
		return SimpleQuery.execute(context, NewsletterTemplate.class);
	}

	private String	name;

	/**
	 * The surrounding contents
	 */
	private String	top, bottom;

	protected NewsletterTemplate(RequestContext context) {
		super(context);
	}

	public NewsletterTemplate(RequestContext context, String name) {
		super(context);
		this.name = name;
		save();
	}

	public String getBottom() {
		return bottom;
	}

	public String getName() {
		return name;
	}

	public String getTop() {
		return top;
	}

	public boolean hasBottom() {
		return bottom != null;
	}

	public boolean hasTop() {
		return top != null;
	}

	public void setBottom(String bottom) {
		this.bottom = bottom;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setTop(String top) {
		this.top = top;
	}

	public String getLabel() {
		return name;
	}

	public String getValue() {
		return getIdString();
	}

}
