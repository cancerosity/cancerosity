package org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions;


/**
 * @author sks 24-Jul-2005 11:03:24
 *
 */
public class SmsMessageException extends Exception {

	public SmsMessageException() {
		super();
	}

	public SmsMessageException(String message) {
		super(message);
	}

	public SmsMessageException(String message, Throwable cause) {
		super(message, cause);
	}

	public SmsMessageException(Throwable cause) {
		super(cause);
	}

}
