package org.sevensoft.ecreator.model.marketing.sms.credits;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.frontend.interaction.sms.credits.SmsCreditsPurchaseHandler;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.support.PayableSupport;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 25 Apr 2007 11:02:16
 *
 */
@Table("sms_credits_session")
public class SmsCreditsSession extends PayableSupport {

	public static SmsCreditsSession get(RequestContext context) {

		SmsCreditsSession s = SimpleQuery.get(context, SmsCreditsSession.class, "sessionId", context.getSessionId());
		return s == null ? new SmsCreditsSession(context, context.getSessionId()) : s;
	}

	private String			sessionId;
	private SmsCreditPackage	smsCreditPackage;
	private Item			account;

	private SmsCreditsSession(RequestContext context) {
		super(context);
	}

	private SmsCreditsSession(RequestContext context, String sessionId) {
		super(context);
		this.sessionId = sessionId;
		save();
	}

	/**
	 * 
	 */
	public void complete(Payment payment, String ipAddress) {
		getAccount().addSmsCredits(getSmsCreditPackage());
	}

	public final Item getAccount() {
		return account.pop();
	}

	public Item getPayableAccount() {
		return getAccount();
	}

	public Address getPaymentAddress() {
		return null;
	}

	public Money getPaymentAmount() {
		return getSmsCreditPackage().getFee();
	}

	public String getPaymentDescription() {
		return getSmsCreditPackage().getCredits() + " sms credits for \243" + getSmsCreditPackage().getFee();
	}

	public String getPaymentFailureUrl(PaymentType paymentType) {
		return Config.getInstance(context).getUrl() + "/" + new Link(SmsCreditsPurchaseHandler.class, "cancelled");
	}

	public String getPaymentSuccessUrl(PaymentType paymentType) {
		return Config.getInstance(context).getUrl() + "/" + new Link(SmsCreditsPurchaseHandler.class, "completed");
	}

    public String getBasketUrl() {
        return null;
    }

    public final String getSessionId() {
		return sessionId;
	}

	public final SmsCreditPackage getSmsCreditPackage() {
		return (SmsCreditPackage) (smsCreditPackage == null ? null : smsCreditPackage.pop());
	}

	/**
	 * 
	 */
	public boolean hasAccount() {
		return account != null;
	}

	public boolean hasSmsCreditsPackage() {
		return smsCreditPackage != null;
	}

	public final void setAccount(Item account) {
		this.account = account;
		save("account");
	}

	public final void setSmsCreditPackage(SmsCreditPackage smsCreditPackage) {
		this.smsCreditPackage = smsCreditPackage;
	}

}
