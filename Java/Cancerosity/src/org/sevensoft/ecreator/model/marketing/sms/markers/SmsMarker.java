package org.sevensoft.ecreator.model.marketing.sms.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.marketing.SmsHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 03-Apr-2006 11:41:39
 *
 */
public final class SmsMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.Sms.enabled(context)) {
			return null;
		}

		return super.link(context, params, new Link(SmsHandler.class), "link_sms");
	}

	public Object getRegex() {
		return "sms";
	}
}