package org.sevensoft.ecreator.model.marketing.newsletter.sync;

import java.io.File;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;

import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterControl;
import org.sevensoft.ecreator.model.marketing.newsletter.Subscriber;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterOption;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

import com.ricebridge.csvman.CsvLoader;
import com.ricebridge.csvman.CsvManager;

/**
 * @author sks 25 Jan 2007 08:38:33
 * 
 * Import emails into the newsletter
 *
 */
public class NewsletterCsvImporter {

	private RequestContext		context;
	private List<Newsletter>	newsletters;
    private List<NewsletterOption>	newsletterOptions;
	private NewsletterControl	settings;

	public NewsletterCsvImporter(RequestContext context) {
		this.context = context;
		this.newsletters = Newsletter.get(context);
        if (Module.NewsletterNewItems.enabled(context)) {
            this.newsletterOptions = NewsletterOption.get(context);
        } else {
            this.newsletterOptions = Collections.emptyList();
        }
		this.settings = NewsletterControl.getInstance(context);
	}

	/**
	 * Import the emails from this file, it should be a single field per record delimited file. Will assume csv for now
	 * 
	 * Returns the number of emails added
	 */
	public int run(File file) {

		CsvManager csvman = new CsvManager();
		csvman.setIgnoreEmptyLines(true);
		csvman.setIgnoreBadLines(true);
		csvman.setSeparator(",");

		CsvLoader loader = csvman.makeLoader(file);
		loader.begin();

		int n = 0;
        List header = new ArrayList();
        while (loader.hasNext()) {

            if (header.isEmpty()) {
                header.addAll(Arrays.asList(loader.next()));
            }

            String[] row = loader.next();
            String email = row[0];

            Subscriber subscriber = Subscriber.get(context, email, true);
            if (subscriber != null) {
                subscriber.validate();

                for (Newsletter newsletter : newsletters) {
                    int newsletterIndex = header.indexOf(newsletter.getName());
                    if (newsletterIndex != -1 && row.length > newsletterIndex) {
                        if (row[newsletterIndex] != null && row[newsletterIndex].equalsIgnoreCase("yes")) {
                            newsletter.subscribe(subscriber);
                        }
                    }
                }

                for (NewsletterOption newsletterOption : newsletterOptions) {
                    int newsletterIndex = header.indexOf(newsletterOption.getName());
                    if (newsletterIndex != -1 && row.length > newsletterIndex) {
                        if (row[newsletterIndex] != null && row[newsletterIndex].equalsIgnoreCase("yes")) {
                            newsletterOption.subscribe(subscriber);
                        }
                    }
                }

                n++;
            }
        }

        loader.end();
		return n;
	}
}
