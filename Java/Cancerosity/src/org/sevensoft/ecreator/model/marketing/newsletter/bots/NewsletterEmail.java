package org.sevensoft.ecreator.model.marketing.newsletter.bots;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.crm.forms.submission.report.SubmissionReportModule;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterOption;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;

import java.util.List;

/**
 * User: Tanya
 * Date: 29.11.2012
 */
public class NewsletterEmail extends Email {

    private RequestContext context;
    private NewsletterOption newsletterOption;
    private Newsletter newsletter;
    private SubmissionReportModule module;
    private Config config;

    public NewsletterEmail(RequestContext context, NewsletterOption newsletterOption) {
        this.context = context;
        this.newsletterOption = newsletterOption;
        this.newsletter = newsletterOption.getNewsletter();

        this.config = Config.getInstance(context);

        setFrom(config.getServerEmail());
        setSubject(newsletterOption.getSubject());
//        setBody(getBody(), "text/html");
    }

    public String getBody() {

        MarkupRenderer renderer = new MarkupRenderer(context, newsletter.getNewsletterContent());
        return renderer.toString();

    }
}