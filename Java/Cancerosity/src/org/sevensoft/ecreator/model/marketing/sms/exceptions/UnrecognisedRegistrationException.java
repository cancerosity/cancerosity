package org.sevensoft.ecreator.model.marketing.sms.exceptions;


/**
 * @author sks 25-Jul-2005 17:07:40
 *
 */
public class UnrecognisedRegistrationException extends Exception {

	public UnrecognisedRegistrationException() {
		super();
	}

	public UnrecognisedRegistrationException(String message) {
		super(message);
	}

	public UnrecognisedRegistrationException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnrecognisedRegistrationException(Throwable cause) {
		super(cause);
	}

}
