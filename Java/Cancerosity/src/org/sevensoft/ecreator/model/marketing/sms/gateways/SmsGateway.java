package org.sevensoft.ecreator.model.marketing.sms.gateways;

import java.io.IOException;

import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsCreditsException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayAccountException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayMessagingException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsMessageException;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 25-Jul-2005 11:45:32
 * 
 */
public enum SmsGateway {

	BulkSms(new BulkSmsGateway()), Clickatell(new ClickatellGateway());

	private SmsGatewayImpl	impl;

	private SmsGateway(SmsGatewayImpl impl) {
		this.impl = impl;
	}

	public void send(RequestContext context, String number, String sender, CharSequence message) throws IOException, SmsMessageException,
			SmsGatewayAccountException, SmsGatewayMessagingException, SmsGatewayException, SmsCreditsException {
		impl.send(context, number, sender, message);
	}

}
