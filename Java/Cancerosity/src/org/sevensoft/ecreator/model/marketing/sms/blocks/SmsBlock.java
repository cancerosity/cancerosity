package org.sevensoft.ecreator.model.marketing.sms.blocks;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.admin.marketing.sms.SmsBlockHandler;
import org.sevensoft.ecreator.iface.frontend.marketing.panels.SmsSubscribePanel;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 20 Apr 2007 10:30:12
 *
 */
@HandlerClass(SmsBlockHandler.class)
@Label("Sms")
@Table("sms_blocks")
public class SmsBlock extends Block {

	public SmsBlock(RequestContext context) {
		super(context);
	}

	public SmsBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, objId);
	}

	@Override
	public Object render(RequestContext context) {

		MultiValueMap<Attribute, String> attributeValues = (MultiValueMap<Attribute, String>) context.getAttribute("attributeValues");
		return new SmsSubscribePanel(context, attributeValues);
	}

}
