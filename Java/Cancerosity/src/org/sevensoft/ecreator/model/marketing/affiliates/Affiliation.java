package org.sevensoft.ecreator.model.marketing.affiliates;

import org.sevensoft.commons.skint.Money;


/**
 * @author sks 23 Feb 2007 15:26:47
 *
 */
public interface Affiliation {

	/**
	 * 
	 */
	Money getAffiliationValue();

}
