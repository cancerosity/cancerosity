package org.sevensoft.ecreator.model.marketing.sms;

/**
 * @author sks 7 Mar 2007 11:20:55
 *
 */
public class SmsNumberException extends Exception {

	public SmsNumberException() {
	}

	public SmsNumberException(String message) {
		super(message);
	}

	public SmsNumberException(String message, Throwable cause) {
		super(message, cause);
	}

	public SmsNumberException(Throwable cause) {
		super(cause);
	}

}
