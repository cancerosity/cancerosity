package org.sevensoft.ecreator.model.marketing.sms.sync;

import java.io.File;
import java.util.List;

import org.sevensoft.ecreator.model.marketing.sms.SmsBulletin;
import org.sevensoft.ecreator.model.marketing.sms.exceptions.ExistingRegistrationException;
import org.sevensoft.jeezy.http.RequestContext;

import com.ricebridge.csvman.CsvLoader;
import com.ricebridge.csvman.CsvManager;

/**
 * @author sks 25 Jan 2007 08:38:33
 * 
 * Import emails into the newsletter
 *
 */
public class SmsCsvImporter {

	private RequestContext		context;
	private List<SmsBulletin>	bulletins;

	public SmsCsvImporter(RequestContext context) {
		this.context = context;
		this.bulletins = SmsBulletin.get(context);
	}

	public int run(File file) {

		CsvManager csvman = new CsvManager();
		CsvLoader loader = csvman.makeLoader(file);

		loader.begin();

		int n = 0;
		while (loader.hasNext()) {

			String[] row = loader.next();
			String number = row[0];

			n++;

			for (SmsBulletin bulletin : bulletins) {
				try {
					bulletin.register(number);
				} catch (ExistingRegistrationException e) {
					e.printStackTrace();
				}
			}
		}

		loader.end();
		return n;
	}
}
