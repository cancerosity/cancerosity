package org.sevensoft.ecreator.model.marketing.newsletter;

import java.util.List;

import org.sevensoft.ecreator.model.accounts.registration.NewsletterOptIn;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.crm.forms.NewsletterSignup;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.marketing.newsletter.blocks.NewsletterBlock;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 5 Jun 2006 09:17:07
 *
 */
@Table("newsletters")
public class Newsletter extends EntityObject implements Positionable, Selectable {

    private String	autoResponder;
    private String newsletterContent;

	public static List<Newsletter> get(RequestContext context) {
		Query q = new Query(context, "select * from # order by name");
		q.setTable(Newsletter.class);
		return q.execute(Newsletter.class);
	}

	/**
	 * Return any category that has the sms block
	 */
	public static Category getCategory(RequestContext context) {

		NewsletterBlock block = SimpleQuery.get(context, NewsletterBlock.class);
		if (block == null) {

			Category category = new Category(context, "Newsletter", null);
			category.setHidden(true);
			category.setAdvanced(true);
			category.save();

			block = new NewsletterBlock(context, category, 0);

			return category;

		} else {

			return block.getOwnerCategory();
		}
	}

	public static Newsletter getDefault(RequestContext context) {
		Query q = new Query(context, "select * from #");
		q.setTable(Newsletter.class);
		return q.get(Newsletter.class);
	}

	/**
	 * The name of this newsletter
	 */
	private String	name;

	private String	description;

	private int		position;

	private Newsletter(RequestContext context) {
		super(context);
	}

	public Newsletter(RequestContext context, String string) {
		super(context);
		name = string;
		save();
	}

	public void addAutoItemType(ItemType it) {
		new AutoSignupNewsletter(context, it, this);
	}

	@Override
	public synchronized boolean delete() {

		SimpleQuery.delete(context, NewsletterOptIn.class, "newsletter", this);
		SimpleQuery.delete(context, NewsletterSignup.class, "newsletter", this);

		return super.delete();
	}

    public String getAutoResponder() {
        return autoResponder;
    }

    public String getNewsletterContent() {
        return newsletterContent;
    }

    public List<ItemType> getAutoMemberGroups() {

		Query q = new Query(context, "select it.* from # it join # auto on it.id=auto.itemType where auto.newsletter=?");
		q.setTable(ItemType.class);
		q.setTable(AutoSignupNewsletter.class);
		q.setParameter(this);
		return q.execute(ItemType.class);
	}

	/**
	 * 
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns a list of all emails in this newsletter.
	 */
	public List<String> getEmails() {

		Query q = new Query(context, "select email from # where newsletter=?");
		q.setParameter(this);
		q.setTable(Subscriber.class);
		return q.getStrings();
	}

	public String getLabel() {
		return getName();
	}

	public final String getName() {
		return name;
	}

	public int getPosition() {
		return position;
	}

	/**
	 * Returns the number of people validated in this newsletter.
	 */
	public int getSubscriberCount() {
		return new Query(context, "select count(*) from # where validating=0").setTable(Subscriber.class).getInt();
	}

	public Subscriber getSubscribers() {
		return null;
	}

	//	/**
	//	 * Returns true if new subscribers to this newsletter should be validated by email first
	//	 */
	//	public boolean isValidateEmails() {
	//		return validateEmails;
	//	}

	public String getValue() {
		return getIdString();
	}

	public boolean hasDescription() {
		return description != null;
	}

	/**
	 * Returns true if this subscriber is subscribed to this newsletter
	 */
	public boolean hasSubscriber(Subscriber s) {
		return s.getNewsletters().contains(this);
	}

	public void removeAutoItemType(ItemType it) {
		Query q = new Query(context, "delete from # where newsletter=? and itemType=?");
		q.setParameter(this);
		q.setParameter(it);
		q.setTable(AutoSignupNewsletter.class);
		q.run();
	}

    public void setAutoResponder(String autoResponder) {
        this.autoResponder = autoResponder;
    }

    public void setNewsletterContent(String newsletterContent) {
        this.newsletterContent = newsletterContent.trim();
    }

    public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Renames this newsletter
	 */
	public final void setName(String name) {
		this.name = name;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	/**
	 * Subscribe a subscriber to this newsletter
	 */
	public void subscribe(Subscriber subscriber) {
		new Subscription(context, this, subscriber);
	}

	/**
	 * remove a subscriber
	 */
	public void unsubscribe(Subscriber s) {
		SimpleQuery.delete(context, Subscription.class, "newsletter", this, "subscriber", s);
	}

//	public void validate(String email) {
//
//		Query q = new Query(context, "update # set validating=0 where email=? and newsletter=?");
//		q.setTable(Subscriber.class);
//		q.setParameter(email);
//		q.setParameter(this);
//		q.run();
//	}

	/**
	 * Mass validates all subscribers to this newsletter
	 */
	private void validateAll() {
		Query q = new Query(context, "update # set validating=0 where newsletter=?");
		q.setTable(Subscriber.class);
		q.setParameter(this);
		q.run();
	}

    public void addNewsletterOption(RequestContext context) {
        NewsletterOptionFactory.createOption(context, this);
    }

    public void removeNewletterOption(RequestContext context, NewsletterOption newsletterOption) {
        newsletterOption.delete();

    }
}
