package org.sevensoft.ecreator.model.marketing.sms.gateways;

import java.io.IOException;

import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.ecreator.model.marketing.sms.SmsSettings;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsCreditsException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayAccountException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayMessagingException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsMessageException;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 25-Jul-2005 11:46:00
 * 
 */
public class ClickatellGateway extends SmsGatewayImpl {

	@Override
	public void send(RequestContext context, String number, String sender, CharSequence message) throws IOException,
			SmsMessageException, SmsGatewayAccountException, SmsGatewayMessagingException, SmsGatewayException, SmsCreditsException {
		System.out.println("Sending message via clickatell");

		System.out.println("message length: " + message.length());
		System.out.println("number: " + number.length());
		System.out.println("sender: " + sender);
		System.out.println("message: " + message);

		if (message.length() > 160) {
			throw new SmsMessageException();
		}

		SmsSettings gatewaySettings = SmsSettings.getInstance(context);

		HttpClient http = new HttpClient("http://api.clickatell.com/http/sendmsg");

		http.setParameter("api_id", gatewaySettings.getClickatellApiId());
		http.setParameter("user", gatewaySettings.getClickatellUsername());
		http.setParameter("password", gatewaySettings.getClickatellPassword());

		http.setParameter("to", number);
		http.setParameter("text", message.toString());
		http.connect();

		String result = http.getResponseString();

		if (result.startsWith("ID:")) {
			return;
		}

		if (result.startsWith("ERR: 001")) {
			throw new SmsGatewayAccountException("Authentication failure");
		}

		if (result.startsWith("ERR: 004")) {
			throw new SmsGatewayAccountException("Account frozen");
		}

		if (result.startsWith("ERR: 002")) {
			throw new SmsGatewayAccountException("Unknown username or password");
		}

		if (result.startsWith("ERR: 114")) {
			throw new SmsGatewayMessagingException("Cannot route message");
		}

		if (result.startsWith("ERR: 105")) {
			throw new SmsGatewayMessagingException("Invalid Destination Address");
		}

		if (result.startsWith("ERR: 116")) {
			throw new SmsGatewayMessagingException("Invalid Unicode Data");
		}

		if (result.startsWith("ERR: 301")) {
			throw new SmsGatewayAccountException("No credit left");
		}

		if (result.startsWith("ERR: 302")) {
			throw new SmsGatewayAccountException("Max allowed credit");
		}

		throw new SmsGatewayException("Unknown error: " + result);
	}

}
