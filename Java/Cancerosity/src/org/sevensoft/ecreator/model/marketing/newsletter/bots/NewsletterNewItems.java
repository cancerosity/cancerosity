package org.sevensoft.ecreator.model.marketing.newsletter.bots;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterOption;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterControl;

import java.util.logging.Logger;
import java.util.List;

/**
 * User: Tanya
 * Date: 29.11.2012
 */
public class NewsletterNewItems implements Runnable {

    private Logger logger = Logger.getLogger("cron");

    private final RequestContext context;

    public NewsletterNewItems(RequestContext context) {
        this.context = context;
    }

    public void run() {
        NewsletterControl newsletterControl = NewsletterControl.getInstance(context);
        List<NewsletterOption> newsletterOptions = NewsletterOption.getVisible(context);
        for (NewsletterOption newsletterOption : newsletterOptions) {
            context.setAttribute("newsletterOption", newsletterOption);
            NewsletterEmail email = new NewsletterEmail(context, newsletterOption);
//            for (String to : newsletterOption.getValidatedEmails()) {
//                email.setTo(to);
                newsletterControl.sendNewsletter(newsletterOption, email, newsletterOption.getValidatedEmails());
//                if (Email.sendEmail(context, email)) {
//                    logger.fine("[NewsletterNewItems] send newsletter item email");
//                } else {
//                    logger.fine("[NewsletterNewItems] NEWSLETTER EMAIL WAS NOT SENT");
//                }
//            }
        }
    }
}
