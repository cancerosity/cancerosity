package org.sevensoft.ecreator.model.marketing.newsletter;

import java.util.List;

import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;

/**
 * @author sks 1 Feb 2007 17:31:17
 *
 */
public class NewsletterOptInPanel {

	private final List<Newsletter>	newsletters;
	private final RequestContext		context;
	private final NewsletterOptable	optable;
	private NewsletterControl		newsletterControl;

	public NewsletterOptInPanel(RequestContext context, NewsletterOptable optable, List<Newsletter> newsletters) {
		this.context = context;
		this.optable = optable;
		this.newsletters = newsletters;
		this.newsletterControl = NewsletterControl.getInstance(context);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(new FormFieldSet("Newsletter opt in"));

		if (newsletters.size() == 1) {

			sb.append("<div class='text'>");
			sb.append(new CheckTag(context, "newsletterOptIns", newsletters.get(0), optable.isNewsletterOptIn(newsletters.get(0))));
			sb.append(" ");
			sb.append(newsletterControl.getRegistrationOptInText());
			sb.append("</div>");

		} else {

			sb.append("<div class='text'>" + HtmlHelper.nl2br(newsletterControl.getRegistrationOptInText()) + "</div>");

			for (Newsletter newsletter : newsletters) {

				sb.append("<div>" + new CheckTag(context, "newsletterOptIns", newsletter, optable.isNewsletterOptIn(newsletter)));
				sb.append(" <b>" + newsletter.getName() + "</b></div>");

				if (newsletter.hasDescription()) {
					sb.append("<div>" + newsletter.getDescription() + "</div>");
				}
			}
		}

		sb.append("</fieldset>");

		return sb.toString();
	}

}
