package org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions;

/**
 * @author sks 30-Jul-2005 17:09:35
 * 
 */
public class SmsCreditsException extends Exception {

	public SmsCreditsException() {
		super();
	}

	public SmsCreditsException(String message) {
		super(message);
	}

	public SmsCreditsException(String message, Throwable cause) {
		super(message, cause);
	}

	public SmsCreditsException(Throwable cause) {
		super(cause);
	}

}
