package org.sevensoft.ecreator.model.marketing.affiliates;

import java.util.LinkedHashMap;
import java.util.Map;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 23 Feb 2007 15:15:59
 *
 */
@Table("affiliates_conversions")
public class Conversion extends EntityObject {

	public static Map<String, String> getTotals(RequestContext context, Date month) {
		Query q = new Query(context, "select affiliateId, sum(value) from # where time>=? and time<=? group by affiliateId");
		q.setTable(Conversion.class);
		q.setParameter(month.beginMonth());
		q.setParameter(month.endMonth());
		return q.getMap(String.class, Money.class, new LinkedHashMap());

	}

	private Money	value;
	private DateTime	time;
	private String	ipAddress;
	private String	affiliateId;
	private String	sessionId;

	protected Conversion(RequestContext context) {
		super(context);
	}

	public Conversion(RequestContext context, String affiliateId, Affiliation a) {
		super(context);

		this.affiliateId = affiliateId;
		this.time = new DateTime();
		this.value = a.getAffiliationValue();

		this.sessionId = context.getSessionId();
		this.ipAddress = context.getRemoteIp();

		save();
	}

}
