package org.sevensoft.ecreator.model.marketing.affiliates;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 23 Feb 2007 07:54:37
 *
 */
@Table("affiliates_clicks")
public class ClickThrough extends EntityObject {

	public static int count(RequestContext context, Date month) {
		Query q = new Query(context, "select count(*) from # where time>=? and time<=?");
		q.setTable(ClickThrough.class);
		q.setParameter(month.beginMonth());
		q.setParameter(month.endMonth());
		return q.getInt();
	}

	public static List<ClickThrough> get(RequestContext context, Date month, int start, int limit) {
		Query q = new Query(context, "select * from # where time>=? and time<=? order by id desc");
		q.setTable(ClickThrough.class);
		q.setParameter(month.beginMonth());
		q.setParameter(month.endMonth());
		return q.execute(ClickThrough.class, start, limit);
	}

	/**
	 * 
	 */
	public static ClickThrough get(RequestContext context, String sessionId, boolean converted) {
		return SimpleQuery.get(context, ClickThrough.class, "sessionId", sessionId, "converted", converted);
	}

	private String	affiliateId;
	private DateTime	time;
	private String	ipAddress;
	private String	landingPage;
	private String	referrer;
	private boolean	converted;
	private String	sessionId;

	protected ClickThrough(RequestContext context) {
		super(context);
	}

	public ClickThrough(RequestContext context, String affiliateId, String page) {
		super(context);

		this.affiliateId = affiliateId;
		this.landingPage = page;
		this.time = new DateTime();
		this.ipAddress = context.getRemoteIp();
		this.referrer = context.getReferrer();
		this.sessionId = context.getSessionId();

		save();
	}

	public String getAffiliateId() {
		return affiliateId;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public String getLandingPage() {
		return landingPage;
	}

	public String getReferrer() {
		return referrer;
	}

	public String getSessionId() {
		return sessionId;
	}

	public DateTime getTime() {
		return time;
	}

	/**
	 * 
	 */
	public boolean hasReferrer() {
		return referrer != null;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

}
