package org.sevensoft.ecreator.model.marketing.sms;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.sevensoft.ecreator.iface.frontend.marketing.SmsHandler;
import org.sevensoft.ecreator.model.attributes.support.AttributeOwnerSupport;
import org.sevensoft.ecreator.model.marketing.sms.exceptions.ExistingRegistrationException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.SmsGateway;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsCreditsException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayAccountException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayMessagingException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsMessageException;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 06-Jul-2005 14:24:47
 * 
 */
@Table("settings_sms")
@Singleton
public class SmsSettings extends AttributeOwnerSupport {

	public static final String	DEMO_SMS_PASSWORD	= "eltonjohn";
	public static final String	DEMO_SMS_USERNAME	= "global7";

	public static SmsSettings getInstance(RequestContext context) {
		return getSingleton(context, SmsSettings.class);
	}

	/**
	 * Show opt in when registering as a member.
	 * 
	 */
	private boolean		registrationOptIn;

	/**
	 * 
	 */
	private String		signupContent;

	private String		bulkSmsUsername, bulkSmsPassword;

	/**
	 * Details for clickatell 
	 */
	private String		clickatellApiId, clickatellUsername, clickatellPassword;

	/**
	 * 
	 */
	private SmsGateway	gateway;

	public SmsSettings(RequestContext context) {
		super(context);
	}

	public void addRegistration(String number, List<SmsBulletin> bulletins) throws ExistingRegistrationException {

		for (SmsBulletin bulletin : bulletins) {
			bulletin.register(number);
		}
	}

	public String getBulkSmsPassword() {
		return bulkSmsPassword;
	}

	public String getBulkSmsUsername() {
		return bulkSmsUsername;
	}

	public String getClickatellApiId() {
		return clickatellApiId;
	}

	public String getClickatellPassword() {
		return clickatellPassword;
	}

	public String getClickatellUsername() {
		return clickatellUsername;
	}

	public SmsGateway getGateway() {
		return gateway == null ? SmsGateway.BulkSms : gateway;
	}

	public String getRelativeUrl() {
		return new Link(SmsHandler.class).toString();
	}

	public String getSignupContent() {
		return signupContent == null ? "Subscribe to our <b>SMS bulletin</b> and keep up to date on all our latest news and offers directly to your mobile! Don't worry about the cost, this is a <b>free service</b>"
				: signupContent;
	}

	public String getUrl() {
		return Config.getInstance(context).getUrl() + "/" + getRelativeUrl();
	}

	public boolean hasSignupContent() {
		return signupContent != null;
	}

	public boolean isBulkSms() {
		return getGateway() == SmsGateway.BulkSms;
	}

	public boolean isClickatell() {
		return getGateway() == SmsGateway.Clickatell;
	}

	public final boolean isRegistrationOptIn() {
		return registrationOptIn;
	}

	public void removeRegistration(String number, List<SmsBulletin> bulletins) {

		number = number.replaceAll("\\D", "");
		for (SmsBulletin bulletin : bulletins) {
			bulletin.unregister(number);
		}

	}

	public void sendMessage(Collection<SmsBulletin> bulletins, String message) throws IOException, SmsMessageException, SmsGatewayAccountException,
			SmsGatewayMessagingException, SmsGatewayException, SmsCreditsException, SmsNumberException, SmsSenderException {

		Set<String> numbers = new HashSet();

		for (SmsBulletin bulletin : bulletins) {
			numbers.addAll(bulletin.getNumbers());
		}

		sendMessage(message, numbers);
	}

	public void sendMessage(String message, Collection<String> numbers) throws SmsGatewayException, SmsMessageException, IOException,
			SmsGatewayAccountException, SmsGatewayMessagingException, SmsCreditsException, SmsNumberException, SmsSenderException {

		logger.fine("[SmsSettings] sending message=" + message + ", numbers=" + numbers);

		SmsGateway gateway = getGateway();
		logger.fine("[SmsSettings] gateway=" + gateway);

		Sms sms = new Sms(context);
		sms.setMessage(message);
		sms.setSender(Company.getInstance(context).getName());

		for (String number : numbers) {
			logger.fine("[SmsSettings] sending message number=" + number);
            if (number != null && number.length() != 0) {
                sms.setNumber(number);
                sms.send();
            }
		}
	}

	public void setBulkSmsPassword(String s) {
		this.bulkSmsPassword = (s == null ? null : s.trim());
	}

	public void setBulkSmsUsername(String s) {
		this.bulkSmsUsername = (s == null ? null : s.trim());
	}

	public void setClickatellApiId(String s) {
		this.clickatellApiId = (s == null ? null : s.trim());
	}

	public void setClickatellPassword(String s) {
		this.clickatellPassword = (s == null ? null : s.trim());
	}

	public void setClickatellUsername(String s) {
		this.clickatellUsername = (s == null ? null : s.trim());
	}

	public void setGateway(SmsGateway gateway) {
		this.gateway = gateway;
	}

	public final void setRegistrationOptIn(boolean registrationOptIn) {
		this.registrationOptIn = registrationOptIn;
	}

	public void setSignupContent(String signupContent) {
		this.signupContent = signupContent;
	}

	/**
	 * @throws SmsSenderException 
	 * @throws SmsNumberException 
	 * @throws SmsCreditsException 
	 * @throws SmsGatewayMessagingException 
	 * @throws SmsGatewayAccountException 
	 * @throws IOException 
	 * @throws SmsMessageException 
	 * @throws SmsGatewayException 
	 * 
	 */
	public void sendMessage(String message, String number) throws SmsGatewayException, SmsMessageException, IOException, SmsGatewayAccountException,
			SmsGatewayMessagingException, SmsCreditsException, SmsNumberException, SmsSenderException {
		sendMessage(message, Collections.singleton(number));
	}

}
