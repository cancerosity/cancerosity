package org.sevensoft.ecreator.model.marketing.newsletter;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.system.config.Config;

/**
 * @author sks 22 Jan 2007 17:26:51
 * 
 * A subscriber subscribed to a particular newsletter
 *
 */
@Table("newsletters_subscription")
public class Subscription extends EntityObject {

	private Newsletter	newsletter;
    private NewsletterOption newsletterOption;
	private Subscriber	subscriber;

	protected Subscription(RequestContext context) {
		super(context);
	}

	public Subscription(RequestContext context, Newsletter newsletter, Subscriber subscriber) {
        super(context);

        if (!subscriber.getNewsletters().contains(newsletter)) {

            this.newsletter = newsletter;
            this.subscriber = subscriber;
            save();

            autoRespond(context, subscriber, newsletter.getAutoResponder());

        }
    }

    public Subscription(RequestContext context, NewsletterOption newsletterOption, Subscriber subscriber) {
        super(context);

        if (!subscriber.getNewslettersOptions().contains(newsletterOption)) {

            this.newsletterOption = newsletterOption;
            this.subscriber = subscriber;
            save();

            String autoResponder = newsletterOption.getAutoResponder() != null ? newsletterOption.getAutoResponder()
                    : newsletterOption.getNewsletter().getAutoResponder();
            autoRespond(context, subscriber, autoResponder);

        }
    }

    private void autoRespond(RequestContext context, Subscriber subscriber, String autoResponder) {
        if (autoResponder != null) {

            Email e = new Email();
            e.setBody(autoResponder, "text/html");
            e.setTo(subscriber.getEmail());
            e.setSubject("Newsletter autoresponder");
            e.setFrom(Config.getInstance(context).getServerEmail());
            try {
                new EmailDecorator(context, e).send(Config.getInstance(context).getSmtpHostname());
            } catch (EmailAddressException e1) {
                e1.printStackTrace();
            } catch (SmtpServerException e1) {
                e1.printStackTrace();
            }
        }
    }

}
