package org.sevensoft.ecreator.model.marketing.sms;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.support.AttributeSupport;
import org.sevensoft.ecreator.model.marketing.sms.exceptions.ExistingRegistrationException;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 24-Jul-2005 11:01:11
 * 
 */
@Table("sms_registrations")
public class SmsRegistration extends AttributeSupport {

	public static List<SmsRegistration> get(RequestContext context) {
		return SimpleQuery.execute(context, SmsRegistration.class);
	}

	private Date		date;
	private String		number;
	private SmsBulletin	bulletin;

	private SmsRegistration(RequestContext context) {
		super(context);
	}

	public SmsRegistration(RequestContext context, SmsBulletin bulletin, String number) throws ExistingRegistrationException {
		super(context);

		number = number.trim();
		number = number.replaceAll("\\D", "");

		if (SimpleQuery.count(context, SmsRegistration.class, "number", number, "bulletin", bulletin) == 0) {

			this.bulletin = bulletin;
			this.number = number;
			this.date = new Date();
			save();

		}

	}

	public List<Attribute> getAttributes() {
		return SmsSettings.getInstance(context).getAttributes();
	}

	public final SmsBulletin getBulletin() {
		return bulletin.pop();
	}

	public Date getDate() {
		return date;
	}

	public String getNumber() {
		return number;
	}

}
