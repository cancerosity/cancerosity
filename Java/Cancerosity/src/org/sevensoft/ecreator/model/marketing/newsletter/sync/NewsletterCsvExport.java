package org.sevensoft.ecreator.model.marketing.newsletter.sync;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterControl;
import org.sevensoft.ecreator.model.marketing.newsletter.Subscriber;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterOption;
import org.sevensoft.jeezy.http.RequestContext;

import com.ricebridge.csvman.CsvManager;

/**
 * @author sks 25 Aug 2006 11:52:29
 */
public class NewsletterCsvExport {

    private final List<Newsletter> newsletters;
    private final List<NewsletterOption> newsletterOptions;
    private final NewsletterControl control;
    private final List<Attribute> attributes;
    private final List<Subscriber> subscribers;

    public NewsletterCsvExport(RequestContext context) {
        this.control = NewsletterControl.getInstance(context);
        this.attributes = control.getAttributes();
        this.newsletters = Newsletter.get(context);
        // import all the subscriprions from the db
        this.newsletterOptions = NewsletterOption.get(context);
        this.subscribers = Subscriber.get(context);
    }

    public String export() {

        CsvManager csvman = new CsvManager();

        List<List<String>> rows = new ArrayList();
        List<String> row = new ArrayList();
        row.add("Email");

        for (Newsletter newsletter : newsletters) {
            row.add(newsletter.getName());
        }

        for (Attribute attribute : attributes) {
            row.add(attribute.getName());
        }

        for (NewsletterOption newsletterOption : newsletterOptions) {
            row.add(newsletterOption.getName());
        }

        rows.add(row);

        for (Subscriber subscriber : subscribers) {

            row = new ArrayList();
            List<Newsletter> subscriberNewsletters = subscriber.getNewsletters();
            List<NewsletterOption> subscriberNewsletterOptions = subscriber.getNewslettersOptions();
            MultiValueMap<Attribute, String> attributeValues = subscriber.getAttributeValues();
            row.add(subscriber.getEmail());

            for (Newsletter newsletter : newsletters) {
                if (subscriberNewsletters.contains(newsletter)) {
                    row.add("Yes");
                } else {
                    row.add("No");
                }
            }

            for (Attribute attribute : attributes) {
                String value = attributeValues.get(attribute);
                row.add(value);
            }

            for (NewsletterOption newsletterOption : newsletterOptions) {
                if (subscriberNewsletterOptions.contains(newsletterOption)) {
                    row.add("Yes");
                } else {
                    row.add("No");
                }
            }

            CollectionsUtil.replaceNulls(row, "");
            rows.add(row);
        }

        return csvman.saveAsListsToString(rows);
    }
}
