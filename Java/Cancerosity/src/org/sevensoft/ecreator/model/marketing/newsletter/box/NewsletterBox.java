package org.sevensoft.ecreator.model.marketing.newsletter.box;

import org.sevensoft.ecreator.iface.admin.marketing.newsletter.boxes.NewsletterBoxHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 03-Mar-2006 17:39:34
 *
 */
@Table("boxes_newsletter")
@Label("Newsletter signup")
@HandlerClass(NewsletterBoxHandler.class)
public class NewsletterBox extends Box {

	protected NewsletterBox(RequestContext context) {
		super(context);
	}

	public NewsletterBox(RequestContext context, String location) {
		super(context, location);

		Category category = Newsletter.getCategory(context);
		Link link = new Link(CategoryHandler.class, null, "category", category);

		this.content = new LinkTag(link, "Subscribe to our newsletter")
				+ " and you'll receive regular emails keeping you up to date on our latest news, special offers and exclusive member only deals!";

		save();
	}

	@Override
	protected String getCssIdDefault() {
		return "newsletter";
	}

	@Override
	public boolean hasEditableContent() {
		return true;
	}

	public String render(RequestContext context) {

		if (!Module.Newsletter.enabled(context)) {
			return null;
		}

		if (content == null) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		sb.append(getTableTag());

		sb.append("<tr><td>");
		sb.append(content);
		sb.append("</td></tr>");

		sb.append("<tr><td class='bottom'></td></tr>");
		sb.append("</table>");

		return sb.toString();
	}
}
