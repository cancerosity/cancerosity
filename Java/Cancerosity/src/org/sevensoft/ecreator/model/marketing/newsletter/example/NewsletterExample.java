package org.sevensoft.ecreator.model.marketing.newsletter.example;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.util.Selectable;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.List;

/**
 * Date: 14.06.2010
 */
@Table("newsletters_examples")
public class NewsletterExample extends EntityObject implements Selectable {

    private String name;
    private String content;

    protected NewsletterExample(RequestContext context) {
        super(context);
    }

    public NewsletterExample(RequestContext context, String name) {
        super(context);
        this.name = name;
        save();
    }

    public static List<NewsletterExample> get(RequestContext context) {
        return SimpleQuery.execute(context, NewsletterExample.class);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLabel() {
        return getName();
    }

    public String getValue() {
        return getContent();
    }
}
