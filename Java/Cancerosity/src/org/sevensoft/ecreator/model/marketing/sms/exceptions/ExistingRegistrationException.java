package org.sevensoft.ecreator.model.marketing.sms.exceptions;


/**
 * @author sks 25-Jul-2005 17:06:33
 *
 */
public class ExistingRegistrationException extends Exception {

	public ExistingRegistrationException() {
		super();
	}

	public ExistingRegistrationException(String message) {
		super(message);
	}

	public ExistingRegistrationException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExistingRegistrationException(Throwable cause) {
		super(cause);
	}

}
