package org.sevensoft.ecreator.model.marketing.newsletter;

import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 25 Aug 2006 14:46:38
 *
 */
@Table("newsletters_autosignups")
public class AutoSignupNewsletter extends EntityObject {

	/**
	 * 
	 */
	private ItemType		itemType;
	/**
	 * 
	 */
	private Newsletter	newsletter;

	private AutoSignupNewsletter(RequestContext context) {
		super(context);
	}

	AutoSignupNewsletter(RequestContext context, ItemType memberGroup, Newsletter newsletter) {
		super(context);
		this.itemType = memberGroup;
		this.newsletter = newsletter;

		save();
	}

}
