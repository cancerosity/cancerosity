package org.sevensoft.ecreator.model.marketing.newsletter.blocks;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.admin.marketing.newsletter.blocks.NewsletterBlockHandler;
import org.sevensoft.ecreator.iface.frontend.marketing.panels.NewsletterSubscribePanel;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 20 Apr 2007 10:30:12
 *
 */
@HandlerClass(NewsletterBlockHandler.class)
@Label("Newsletter")
@Table("newsletters_blocks")
public class NewsletterBlock extends Block {

	public NewsletterBlock(RequestContext context) {
		super(context);
	}

	public NewsletterBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, objId);
	}

	@Override
	public Object render(RequestContext context) {

		MultiValueMap<Attribute, String> attributeValues = (MultiValueMap<Attribute, String>) context.getAttribute("attributeValues");
		return new NewsletterSubscribePanel(context, attributeValues);

	}
}
