package org.sevensoft.ecreator.model.marketing.newsletter;

import java.util.Collection;
import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.commons.superstrings.RandomHelper;
import org.sevensoft.commons.validators.net.EmailValidator;
import org.sevensoft.ecreator.iface.frontend.marketing.NewsletterHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.support.AttributeSupport;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 05-Jul-2005 00:22:42
 * 
 * A subscriber to one or more emails
 * 
 * 
 */
@Table("newsletters_subscribers")
public class Subscriber extends AttributeSupport {

	public static List<Subscriber> get(RequestContext context) {
		return SimpleQuery.execute(context, Subscriber.class);
	}

	public static Subscriber get(RequestContext context, String email, boolean create) {
		
		Query q = new Query(context, "select * from # where email=?");
		q.setTable(Subscriber.class);
		q.setParameter(email);
		Subscriber sub = q.get(Subscriber.class);

        if (sub == null && create) {

            if (new EmailValidator().validate(email) != null)
                return null;

			sub = new Subscriber(context, email);
		}

		return sub;
	}

	public void sendValidationCode() throws EmailAddressException, SmtpServerException {

		String link = Config.getInstance(context).getUrl() + "/" + new Link(NewsletterHandler.class, "v", "c", code, "e", email);

		Email e = new Email();
		e.setSubject("Newsletter validation");
		e.setBody("Someone requested that this email be subscribed to our newsletter(s).\n\n" +
				"If this was not you, please ignore this email. Otherwise click the link below to confirm your subscription.\n" + link);
		e.setFrom(Config.getInstance(context).getServerEmail());
		e.setTo(email);
		new EmailDecorator(context, e).send(Config.getInstance(context).getSmtpHostname());
	}

	/**
	 * The email of the subscriber 
	 */
	private String	email;

	/**
	 * The date this email was added
	 */
	private Date	date;

	/**
	 * True if this subscriber has not yet validated their email
	 */
	private boolean	validating;

	/**
	 * Access code
	 */
	private String	code;

	private Subscriber(RequestContext context) {
		super(context);
	}

	public Subscriber(RequestContext context, String e) {
		super(context);

		this.email = e.trim().toLowerCase();
		this.code = RandomHelper.getRandomString(8);
		if (NewsletterControl.getInstance(context).isValidateEmails()) {
			this.validating = true;
		}
		this.date = new Date();

		save();
	}

	@Override
	public synchronized boolean delete() {
		removeAttributeValues();
		return super.delete();
	}

	public List<Attribute> getAttributes() {
		return NewsletterControl.getInstance(context).getAttributes();
	}

	public String getCode() {
		if (code == null) {
			code = RandomHelper.getRandomString(8);
			save("code");
		}
		return code;
	}

	public String getEmail() {
		return email;
	}

	/**
	 * Returns a list of newsletters this subscriber is subscribed to
	 */
	public List<Newsletter> getNewsletters() {
		Query q = new Query(context, "select n.* from # n join # s on n.id=s.newsletter where s.subscriber=?");
		q.setTable(Newsletter.class);
		q.setTable(Subscription.class);
		q.setParameter(this);
		return q.execute(Newsletter.class);
	}

    /**
	 * Returns a list of newsletters this subscriber is subscribed to
	 */
	public List<NewsletterOption> getNewslettersOptions() {
		Query q = new Query(context, "select no.* from # no join # s on no.id=s.newsletterOption where s.subscriber=?");
		q.setTable(NewsletterOption.class);
		q.setTable(Subscription.class);
		q.setParameter(this);
		return q.execute(NewsletterOption.class);
	}

	public boolean isValidated() {
		return validating = false;
	}

	public boolean isValidating() {
		return validating;
	}

	public void validate() {

		if (validating) {
			this.validating = false;
			save();
		}
	}

	/**
	 * 
	 */
	public void subscribe(Collection<Newsletter> newsletters) {
		for (Newsletter newsletter : newsletters) {
			newsletter.subscribe(this);
		}
	}

    public void subscribeOpt(Collection<NewsletterOption> newsletterOptions) {
		for (NewsletterOption newsletter : newsletterOptions) {
			newsletter.subscribe(this);
		}
	}

	/**
	 * unsubscribe from all newsletters
	 */
	public void unsubscribe() {
		SimpleQuery.delete(context, Subscription.class, "subscriber", this);

	}

}
