package org.sevensoft.ecreator.model.marketing.sms;

import java.util.List;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.marketing.sms.blocks.SmsBlock;
import org.sevensoft.ecreator.model.marketing.sms.exceptions.ExistingRegistrationException;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Sep 2006 22:56:32
 *
 * A instance of a bulletin
 */
@Table("sms_bulletins")
public class SmsBulletin extends EntityObject {

	public static List<SmsBulletin> get(RequestContext context) {
		return SimpleQuery.execute(context, SmsBulletin.class);
	}

	/**
	 * Return any category that has the sms block
	 */
	public static Category getCategory(RequestContext context) {

		SmsBlock block = SimpleQuery.get(context, SmsBlock.class);
		if (block == null) {

			Category category = new Category(context, "Sms Bulletin", null);
			category.setHidden(true);
			category.setAdvanced(true);
			category.save();

			new SmsBlock(context, category, 0);

			return category;

		} else {
			
			return block.getOwnerCategory();
		}
	}

	private String	name;

	private SmsBulletin(RequestContext context) {
		super(context);
	}

	public SmsBulletin(RequestContext context, String name) {
		super(context);
		this.name = name;

		save();
	}

	public final String getName() {
		return name;
	}

	public List<String> getNumbers() {
		Query q = new Query(context, "select number from # where bulletin=?");
		q.setTable(SmsRegistration.class);
		q.setParameter(this);
		return q.getStrings();
	}

	public List<SmsRegistration> getRegistrations() {
		return null;
	}

	public void register(String number) throws ExistingRegistrationException {
		new SmsRegistration(context, this, number);
	}

	@Override
	protected void schemaInit(RequestContext context) {
		super.schemaInit(context);

		// ensure we have at least one sms bulletin
		if (SimpleQuery.count(context, SmsBulletin.class) == 0) {
			new SmsBulletin(context, "Sms Bulletin");
		}
	}

	public final void setName(String name) {
		this.name = name;
	}

	public void unregister(String number) {
		SimpleQuery.delete(context, SmsRegistration.class, "number", number, "bulletin", this);
	}
}
