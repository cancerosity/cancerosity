package org.sevensoft.ecreator.model.marketing.sms;

/**
 * @author sks 7 Mar 2007 11:24:23
 *
 */
public class SmsSenderException extends Exception {

	public SmsSenderException() {
		super();
	}

	public SmsSenderException(String message) {
		super(message);
	}

	public SmsSenderException(String message, Throwable cause) {
		super(message, cause);
	}

	public SmsSenderException(Throwable cause) {
		super(cause);
	}

}
