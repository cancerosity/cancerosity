package org.sevensoft.ecreator.model.marketing.sms.gateways;

import java.io.IOException;

import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsCreditsException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayAccountException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayMessagingException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsMessageException;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 04-Jan-2006 12:00:01
 * 
 */
public abstract class SmsGatewayImpl {

	public abstract void send(RequestContext context, String number, String sender, CharSequence message) throws IOException, SmsMessageException,
			SmsGatewayAccountException, SmsGatewayMessagingException, SmsGatewayException, SmsCreditsException;

}
