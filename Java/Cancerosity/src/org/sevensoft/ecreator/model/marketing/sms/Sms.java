package org.sevensoft.ecreator.model.marketing.sms;

import java.io.IOException;

import org.sevensoft.ecreator.model.marketing.sms.gateways.SmsGateway;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsCreditsException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayAccountException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayMessagingException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsMessageException;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 7 Mar 2007 10:44:22
 *
 */
public class Sms {

	private String			number;
	private String			message;
	private final RequestContext	context;
	private String			sender;

	public Sms(RequestContext context) {
		this.context = context;
	}

	public void send() throws SmsMessageException, IOException, SmsGatewayAccountException, SmsGatewayMessagingException, SmsGatewayException,
			SmsCreditsException, SmsNumberException, SmsSenderException {

		if (message.length() > 160) {
			throw new SmsMessageException("Message is too long for sms text");
		}

		if (message == null) {
			throw new SmsMessageException("No message specified");
		}

		if (number == null) {
			throw new SmsNumberException("Invalid mobile number");
		}

		if (sender == null) {
			throw new SmsSenderException("Invalid sender name");
		}

		SmsGateway gateway = SmsSettings.getInstance(context).getGateway();
		gateway.send(context, number, sender, message);
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}
}
