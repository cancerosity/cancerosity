package org.sevensoft.ecreator.model.search.forms.boxes;

import org.sevensoft.ecreator.iface.admin.search.box.ContentSearchBoxHandler;
import org.sevensoft.ecreator.iface.frontend.search.ContentSearchHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 29 Dec 2006 15:41:31
 *
 */
@Table("boxes_search_content")
@Label("Content search")
@HandlerClass(ContentSearchBoxHandler.class)
public class ContentSearchBox extends Box {

	/**
	 * Text that appears above the search box
	 */
	private String	header;

	/**
	 * Change the submit button label
	 */
	private String	submitLabel;

	private String	footer;

	private int		resultsPerPage;

	public ContentSearchBox(RequestContext context) {
		super(context);
	}

	public ContentSearchBox(RequestContext context, String panel) {
		super(context, panel);
	}

	@Override
	protected String getCssIdDefault() {
		return "content_search";
	}

	public final String getFooter() {
		return footer;
	}

	public final String getHeader() {
		return header;
	}

	public final int getResultsPerPage() {
		return resultsPerPage;
	}

	public final String getSubmitLabel() {
		return submitLabel == null ? "Search" : submitLabel;
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	@Override
	public String render(RequestContext context) {

		StringBuilder sb = new StringBuilder();
		sb.append(getTableTag());

		sb.append("<tr><td>");

		sb.append(new FormTag(ContentSearchHandler.class, null, "get"));
		sb.append(new HiddenTag("resultsPerPage", resultsPerPage));

		if (header != null) {
			sb.append(header);
		}

		sb.append(new TextTag(context, "keywords", 20));
		sb.append(" ");
		sb.append(new SubmitTag(getSubmitLabel()));

		if (footer != null) {
			sb.append(footer);
		}

		sb.append("</form>");
		sb.append("</td></tr>");
		sb.append("</table>");

		return sb.toString();
	}

	public final void setFooter(String footer) {
		this.footer = footer;
	}

	public final void setHeader(String header) {
		this.header = header;
	}

	public final void setResultsPerPage(int resultsPerPage) {
		this.resultsPerPage = resultsPerPage;
	}

	public final void setSubmitLabel(String submitLabel) {
		this.submitLabel = submitLabel;
	}

}
