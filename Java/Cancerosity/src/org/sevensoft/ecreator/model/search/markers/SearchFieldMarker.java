package org.sevensoft.ecreator.model.search.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.search.forms.SearchField;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 24 Apr 2007 17:50:57
 *
 */
public class SearchFieldMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!params.containsKey("id")) {
			return null;
		}

		int id = Integer.parseInt(params.get("id"));
		SearchField field = EntityObject.getInstance(context, SearchField.class, id);

        if (field == null) {
            return null;
        }
        if (params.containsKey("plain")) {
            return super.string(context, params, field.renderPlain());
        } else {
            return field.render(false);
        }
	}

	public Object getRegex() {
		return "search_field";
	}

}
