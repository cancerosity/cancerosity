package org.sevensoft.ecreator.model.search.markers.text;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 31-Mar-2006 15:23:41
 *
 */
public final class SearchNameMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		int size = 20;
		if (params.containsKey("size"))
			size = Integer.parseInt(params.get("size").trim());

		return new TextTag(context, "name", size);
	}

	public Object getRegex() {
		return "search_name";
	}
}