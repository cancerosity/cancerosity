package org.sevensoft.ecreator.model.search.markers.text;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 17 Jul 2006 17:49:03
 *
 */
public class SearchDetailsMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		StringBuilder sb = new StringBuilder();

		int size;
		if (params.containsKey("size"))
			size = Integer.parseInt(params.get("size").trim());
		else
			size = 12;

		sb.append(new TextTag(context, "details", size));

		return sb;
	}

	public Object getRegex() {
		return "search_details";
	}

}
