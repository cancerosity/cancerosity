package org.sevensoft.ecreator.model.search;

import java.util.Map;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.frontend.search.ItemSearchHandler;
import org.sevensoft.ecreator.model.search.forms.SearchForm;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 6 Sep 2006 11:08:36
 *
 */
public class SearchSaveHeader {

	private final RequestContext				context;
	private final SearchForm				searchForm;
	private final MultiValueMap<String, String>	parameters;

	public SearchSaveHeader(RequestContext context, SearchForm searchForm, MultiValueMap<String, String> parameters) {
		this.context = context;
		this.searchForm = searchForm;
		this.parameters = parameters;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		Link link = new Link(ItemSearchHandler.class, "save");
		if (searchForm != null) {
			link.addParameter("searchForm", searchForm);
		}
		for (Map.Entry<String, String> entry : parameters.entrySet()) {
			link.addParameter(entry.getKey(), entry.getValue());
		}

		sb.append(new TableTag("ec_savesearch_header"));
		sb.append("<tr><td>To save this search so you can re-run it at any time " + new LinkTag(link, "click here") + "</td></tr>");
		sb.append("</table>");

		return sb.toString();
	}
}
