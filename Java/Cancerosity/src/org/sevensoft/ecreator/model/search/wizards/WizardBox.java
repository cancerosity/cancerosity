package org.sevensoft.ecreator.model.search.wizards;

import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 19 Aug 2006 20:46:02
 *
 */
@Table("boxes_wizards")
@Label("Wizard")
@HandlerClass(WizardBoxHandler.class)
public class WizardBox extends Box {

	private Wizard	wizard;

	public WizardBox(RequestContext context) {
		super(context);
	}

	public WizardBox(RequestContext context, String panel) {
		super(context, panel);
	}

	@Override
	protected String getCssIdDefault() {
		return "wizard_box";
	}

	public Wizard getWizard() {
		return wizard.pop();
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	@Override
	public String render(RequestContext context) {

		wizard.pop();

		logger.fine("[WizardBox] wizard=" + wizard);

		StringBuilder sb = new StringBuilder();

		sb.append(getTableTag());
		sb.append(new WizardRenderer(context, wizard));
		sb.append("</table>");

		return sb.toString();
	}

	@Override
	public void setObjId(int objId) {
		this.wizard = EntityObject.getInstance(context, Wizard.class, objId);
		this.name = "Wizard: " + wizard.getName();

		save();
	}
}
