package org.sevensoft.ecreator.model.search.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 03-Apr-2006 11:43:17
 *
 */
public class SearchEndMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {
		return "</form>";
	}

	public Object getRegex() {
		return "search_end";
	}
}