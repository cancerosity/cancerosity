package org.sevensoft.ecreator.model.search.comments;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.comments.box.CommentListBox;
import org.sevensoft.ecreator.model.comments.CommentsSearcher;
import org.sevensoft.ecreator.model.search.SearchOwner;
import org.sevensoft.commons.samdate.DateTime;

/**
 * User: Tanya
 * Date: 17.11.2011
 */
@Table("searcher_comments")
public class CommentSearch extends EntityObject {

    private String name;

    /**
     * Date this search was created on the system
     */
    private DateTime dateCreated;

    /**
     * Max comment to display
     */
    private int limit;

    /**
     * A name assigned by the user
     */
    private String customName;

    private CommentListBox commentListBox;

    private String owner;

    private boolean showOnlyChildren;


    public CommentSearch(RequestContext context) {
        super(context);
        this.dateCreated = new DateTime();
        logger.fine("created new comment search");
    }

    public CommentSearch(RequestContext context, SearchOwner owner) {

        this(context);
        this.owner = owner.getFullId();


        save();
        logger.fine("created new persisted search id=" + getId());
    }

    /**
     * Returns a search for this search owner.
     * Will create if required
     */
    public static CommentSearch get(RequestContext context, SearchOwner owner) {
        return SimpleQuery.get(context, CommentSearch.class, "owner", owner.getFullId());
    }

    public final String getCustomName() {
        return customName;
    }

    public DateTime getDateCreated() {
        return dateCreated;
    }

    public int getLimit() {
        return limit;
    }

    public String getName() {
        return name;
    }

    public CommentsSearcher getSearcher() {

        CommentsSearcher searcher = new CommentsSearcher(context);

        searcher.setLimit(getLimit());

        return searcher;
    }

    public boolean hasCustomName() {
        return customName != null;
    }


    public final void setCustomName(String customName) {
        this.customName = customName;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean hasCommentListBox() {
        return commentListBox != null;
    }

    public final CommentListBox getCommentListBox() {
        return (CommentListBox) (commentListBox == null ? null : commentListBox.pop());
    }

    public boolean isShowOnlyChildren() {
        return showOnlyChildren;
    }

    public void setShowOnlyChildren(boolean showOnlyChildren) {
        this.showOnlyChildren = showOnlyChildren;
    }


}
