package org.sevensoft.ecreator.model.search.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.search.SavedSearchesHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 31-Mar-2006 15:23:41
 *
 */
public final class SavedSearchesMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.SavedSearches.enabled(context)) {
			logger.fine("[SavedSearchesMarker] saved searches disabled, exiting");
			return null;
		}

		return super.link(context, params, new Link(SavedSearchesHandler.class), "account_link");
	}

	public Object getRegex() {
		return "searches_saved";
	}
}