package org.sevensoft.ecreator.model.search.markers.location;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 31-Mar-2006 15:23:41
 *
 */
public final class SearchLocationMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		int size;
		if (params.containsKey("size")) {
			size = Integer.parseInt(params.get("size").trim());
		} else {
			size = 20;
		}

		return new TextTag(context, "location", size);
	}

	public Object getRegex() {
		return "search_location";
	}
}