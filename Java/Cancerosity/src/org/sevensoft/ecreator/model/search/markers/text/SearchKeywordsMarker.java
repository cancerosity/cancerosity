package org.sevensoft.ecreator.model.search.markers.text;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 31-Mar-2006 15:23:41
 *
 */
public final class SearchKeywordsMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		int size;
		if (params.containsKey("size")) {
			size = Integer.parseInt(params.get("size").trim());
		} else {
			size = 20;
		}

		TextTag textTag = new TextTag(context, "keywords", size);

		if (params.containsKey("initial")) {

			String initial = params.get("initial");

			textTag.setOnFocus("if (this.value == '" + initial + "') this.value = ''; ");
			textTag.setValue(initial);
		}

		return textTag;
	}

	public Object getRegex() {
		return new String[] { "search_keywords", "csearch_keywords" };
	}
}