package org.sevensoft.ecreator.model.search.markers.results;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Results;

/**
 * @author sks 30 Apr 2007 07:28:28
 *
 */
public class TotalResultsMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		Results results = (org.sevensoft.jeezy.http.util.Results) context.getAttribute("results");
		if (results == null) {
			return null;
		}

		return super.string(context, params, results.getTotalResults());
	}

	public Object getRegex() {
		return "results_total";
	}

}
