package org.sevensoft.ecreator.model.search.markers.control;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;

/**
 * @author sks 6 Jul 2006 13:07:11
 *
 */
public class SearchExcludeAccountMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {
		return new HiddenTag("excludeAccount", true);
	}

	public Object getRegex() {
		return "search_exclude_account";
	}

}
