package org.sevensoft.ecreator.model.search.tags;

import org.sevensoft.ecreator.model.interaction.LastActive;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 6 Sep 2006 10:16:40
 *
 */
public class LastActiveTag {

	private final RequestContext	context;
	private final String		any;

	public LastActiveTag(RequestContext context, String any) {
		this.context = context;
		this.any = any;
	}

	@Override
	public String toString() {

		SelectTag lastActiveTag = new SelectTag(context, "lastActive");
		if (any != null) {
			lastActiveTag.setAny(any);
		}
		lastActiveTag.addOptions(LastActive.values());

		return lastActiveTag.toString();
	}

}
