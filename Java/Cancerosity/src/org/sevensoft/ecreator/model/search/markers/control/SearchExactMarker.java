package org.sevensoft.ecreator.model.search.markers.control;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;

/**
 * @author sks 24 Mar 2007 14:16:24
 *
 */
public class SearchExactMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {
		return new HiddenTag("exact", true);
	}

	@Override
	public String getDescription() {
		return "Use this marker to return the user to the search if it does not match a single result";
	}

	public Object getRegex() {
		return "search_exact";
	}

}
