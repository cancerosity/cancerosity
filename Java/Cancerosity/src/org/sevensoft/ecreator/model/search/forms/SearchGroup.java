package org.sevensoft.ecreator.model.search.forms;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
@Table("search_groups")
public class SearchGroup extends EntityObject {

    private SearchField searchField;
    private String name;
    private String values;

    public SearchGroup(RequestContext context) {
        super(context);
    }

    public SearchGroup(RequestContext context, SearchField searchField) {
        this(context);
        this.searchField = searchField;
        save();
    }

    public SearchField getSearchField() {
        return (SearchField) (searchField == null ? null : searchField.pop());
    }

    public void setSearchField(SearchField searchField) {
        this.searchField = searchField;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValues() {
        return values;
    }

    public void setValues(String values) {
        this.values = values;
    }

    public static Map<String, String> getMap(RequestContext context) {
        Query q = new Query(context, "select s.values, s.name from # s");
        q.setTable(SearchGroup.class);
        return q.getLinkedHashMap(String.class, String.class);
    }
}
