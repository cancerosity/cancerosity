package org.sevensoft.ecreator.model.search.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 17 Jul 2006 17:49:03
 *
 * Search by item id
 */
public class SearchIdMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		StringBuilder sb = new StringBuilder();

		int size;
		if (params.containsKey("size")) {
			size = Integer.parseInt(params.get("size").trim());
		} else {
			size = 12;
		}

		sb.append(new TextTag(context, "id", size));
		return sb;
	}

	public Object getRegex() {
		return "search_id";
	}

}
