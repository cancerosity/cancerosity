package org.sevensoft.ecreator.model.search.alerts;

import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 7 Mar 2007 10:14:00
 *
 */
public class SearchAlertRunner implements Runnable {

	private static Logger		logger	= Logger.getLogger("ecreator");

	private final RequestContext	context;
	private final Item		item;
	private int				n;

	public SearchAlertRunner(RequestContext context, Item item) {
		this.context = context;
		this.item = item;
	}

	public int getCount() {
		return n;
	}

	public void run() {

		n = 0;

		final List<SearchAlert> list = SearchAlert.get(context);
		logger.fine("[SearchAlertRunner] system has " + list.size() + " alerts");
		logger.fine("[SearchAlertRunner] checking alerts for item=" + item);

		for (SearchAlert alert : list) {
			alert.check(item);
			n++;
		}

	}

}
