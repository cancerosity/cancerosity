package org.sevensoft.ecreator.model.search;

/**
 * @author sks 2 Oct 2006 18:03:02
 *
 */
public interface SearchOwner {

	String getFullId();

}
