package org.sevensoft.ecreator.model.search.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.search.ItemSearchHandler;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;

/**
 * @author sks 03-Apr-2006 11:41:54
 *
 */
public class SearchStartMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {
		return new FormTag(ItemSearchHandler.class, null, "get");
	}

	public Object getRegex() {
		return new String[] { "search_start", "search_begin" };
	}
}