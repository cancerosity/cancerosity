package org.sevensoft.ecreator.model.search.wizards;

import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 19 Aug 2006 20:46:02
 *
 */
@Table("blocks_wizards")
@Label("Wizard")
public class WizardBlock extends Block {

	private Wizard	wizard;

	private WizardBlock(RequestContext context) {
		super(context);
	}

	public WizardBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, 0);

		this.wizard = EntityObject.getInstance(context, Wizard.class, objId);
		if (wizard != null)
			save();
	}

	public Wizard getWizard() {
		return wizard.pop();
	}

	@Override
	public Object render(RequestContext context) {

		if (wizard == null) {
			logger.fine("[WizardBlock] no wizard set on block");
		}

		wizard.pop();
		logger.fine("[WizardBlock] wizard=" + wizard);

		StringBuilder sb = new StringBuilder();
		sb.append(new TableTag("ec_wizard"));

		sb.append(new WizardRenderer(context, wizard));

		sb.append("</table>");

		return sb.toString();
	}
}
