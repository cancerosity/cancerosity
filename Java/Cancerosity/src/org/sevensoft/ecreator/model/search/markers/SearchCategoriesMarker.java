package org.sevensoft.ecreator.model.search.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 03-Apr-2006 11:41:46
 *
 */
public class SearchCategoriesMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		SelectTag tag = new SelectTag(context, "searchCategory");
		tag.setAny("-Any category-");

		String id = params.get("id");

		if (id == null) {

			tag.addOptions(Category.getItemContainersMap(context, 0, 500));

		} else {

			Map<String, String> options;
			options = Category.getItemContainersMap(context, id);
			tag.addOptions(options);
		}

		return super.string(context, params, tag);
	}

	public Object getRegex() {
		return "search_categories";
	}
}