package org.sevensoft.ecreator.model.search.content;

import java.util.logging.Logger;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;

/**
 * @author sks 13 Jan 2007 21:12:45
 *
 */
public class ContentSearchResult {

	private static Logger	logger	= Logger.getLogger("ecreator");

	private String		url;
	private String		name;
	private String		summary;

	public ContentSearchResult(Category category) {
		this(category.getUrl(), category.getName(), category.getContentBlock().getContentStripped());
	}

	public ContentSearchResult(Item item) {
		this(item.getFriendlyUrl(), item.getName(), item.getContentStripped());
	}

	private ContentSearchResult(String url, String name, String content) {
		this.url = url;
		this.name = name;
		this.summary = StringHelper.toSnippet(content, 200, "...");
		logger.fine("[ContentSearchResult] created: " + this);
	}

	public final String getName() {
		return name;
	}

	public final String getSummary() {
		return summary;
	}

	public final String getUrl() {
		return url;
	}

	public boolean hasSummary() {
		return summary != null;
	}

	@Override
	public String toString() {
		return "url=" + url + ", name=" + name + ", summary=" + summary;
	}
}
