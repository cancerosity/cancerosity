package org.sevensoft.ecreator.model.search.forms.blocks;

import org.sevensoft.ecreator.iface.admin.search.SearchBlockHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.search.forms.SearchField;
import org.sevensoft.ecreator.model.search.forms.SearchForm;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ImageSubmitTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 5 Sep 2006 15:51:11
 *
 */
@Table("blocks_search")
@HandlerClass(SearchBlockHandler.class)
@Label("Search block")
public class SearchBlock extends Block {

	private boolean	searchHeader;

	/**
	 * Page title for the search results page
	 */
	private String	searchResultsTitle;

	private SearchBlock(RequestContext context) {
		super(context);
	}

	public SearchBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, 0);
	}

	public SearchForm getSearchForm() {

		SearchForm form = SimpleQuery.get(context, SearchForm.class, "searchBlock", this);
		if (form == null) {
			form = new SearchForm(context, this);
		}

		return form;
	}

	public String getSearchResultsTitle() {
		return searchResultsTitle;
	}

	@Override
	public Object render(RequestContext context) {

		SearchForm form = getSearchForm();

		StringBuilder sb = new StringBuilder();

		form.renderFormTag(sb);
		sb.append(new HiddenTag("searchBlock", this));

		if (form.hasMarkup()) {

			MarkupRenderer r = new MarkupRenderer(context, form.getMarkup());
			sb.append(r);

		} else {

			renderDefault(sb);

		}

		sb.append("</form>");

		return sb.toString();
	}

	private void renderDefault(StringBuilder sb) {

		sb.append(new TableTag("form").setCaption("Search criteria"));

        SearchForm searchForm = getSearchForm();
        for (SearchField field : searchForm.getSearchFields()) {

			Object fieldRender = field.render(false);
			if (fieldRender != null) {
				sb.append("<tr>");
				sb.append("<td class='key'>" + field.getName() + "</td>");
				sb.append("<td class='input'>" + fieldRender + new ErrorTag(context, field.getParam(), "<br/>") + "</td>");
				sb.append("</tr>");
			}
		}

        sb.append("<tr><td></td><td>");
        if (searchForm.hasSubmitSrc()) {
            sb.append(new ImageSubmitTag("template-data/" + searchForm.getSubmitSrc()));
        } else {
            sb.append(new SubmitTag(searchForm.getSubmitLabel()));
        }
        sb.append("</td></tr>");
		sb.append("</table>");
	}

	public void setSearchHeader(boolean searchHeader) {
		this.searchHeader = searchHeader;
	}

	public void setSearchResultsTitle(String searchResultsTitle) {
		this.searchResultsTitle = searchResultsTitle;
	}

}
