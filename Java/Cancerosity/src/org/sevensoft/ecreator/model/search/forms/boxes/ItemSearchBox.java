package org.sevensoft.ecreator.model.search.forms.boxes;

import java.io.IOException;
import java.util.List;

import org.sevensoft.ecreator.iface.admin.search.box.ItemSearchBoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.extras.languages.Language;
import org.sevensoft.ecreator.model.extras.languages.tables.Translation;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.search.forms.FieldType;
import org.sevensoft.ecreator.model.search.forms.SearchField;
import org.sevensoft.ecreator.model.search.forms.SearchForm;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ImageSubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 13 Sep 2006 15:06:35
 *
 */
@Table("boxes_search")
@Label("Search")
@HandlerClass(ItemSearchBoxHandler.class)
public class ItemSearchBox extends Box {

	/**
	 *  This box will automatically submit straight to the search results and then the box can be used to filter results
	 */
	private boolean	autoSubmit;

	private ItemSearchBox(RequestContext context) {
		super(context);
	}

	public ItemSearchBox(RequestContext context, String location) {
		super(context, location);

		// create one default form with keywords on
		SearchForm form = addSearchForm();

		SearchField field = form.addInputField(FieldType.Keywords);
		field.setName("Enter keywords to search");
		field.save();
	}

	public SearchForm addSearchForm() {
		return new SearchForm(context, this);
	}

	@Override
	public boolean checkWhereOthers() {

		if (equals(context.getAttribute("searchBox"))) {
			return true;
		}

		return super.checkWhereOthers();
	}

	@Override
	public synchronized boolean delete() {

		removeSearchForms();
		return super.delete();
	}

	@Override
	protected String getCssIdDefault() {
		return "search_box";
	}

	public List<SearchForm> getSearchForms() {
		return SimpleQuery.execute(context, SearchForm.class, "searchBox", this);
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	public boolean isAutoSubmit() {
		return autoSubmit;
	}

	public void removeSearchForm(SearchForm form) {
		form.delete();
	}

	private void removeSearchForms() {
		for (SearchForm form : getSearchForms()) {
			form.delete();
		}
	}

	@Override
	public String render(RequestContext context) {

		final List<SearchForm> forms = getSearchForms();
		if (forms.isEmpty()) {
			return null;
		}

		if (autoSubmit) {

			// only autoSubmit if we are in a category
			if (context.getAttribute("category") != null) {

				Link link = forms.get(0).getLink();
				link.setParameter("searchBox", this);

				try {
					context.getResponse().sendRedirect(link.toString());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		StringBuilder sb = new StringBuilder();

		if (forms.size() > 1 || context.getRequest().getServletPath().equals("/s.do")) {

			/*
			 * Render script for showing / hiding divs
			 */
			sb.append("<script type=\"text/javascript\">\n");

			if (forms.size() > 1) {

				sb.append("function hideSearchForms() { \n");
				for (SearchForm searchForm : forms) {
					sb.append("	document.getElementById('" + searchForm.getFormId() + "div').style.display='none'; \n");
				}
				sb.append("}\n\n");

				sb.append("function showSearchForm(f) {\n");
				sb.append("	hideSearchForms(); ");
				sb.append("	document.getElementById(f).style.display=''; \n");
				sb.append("}\n\n");
			}

			// only render ajax searching if on the search page
			if (context.getRequest().getServletPath().equals("/s.do")) {
/*
				sb.append("	var searchSubmitOptions = { ");
				sb.append("		target:        '#searchResultsDiv', ");
				sb.append("		beforeSubmit:  showLoading, ");
				sb.append("		success:       finishedLoading ");
				sb.append("	}; \n\n");

				for (SearchForm searchForm : forms) {

					sb.append("	$(document).ready(function() {\n");
					sb.append("		$('#" + searchForm.getFormId() + "').ajaxForm(searchSubmitOptions);\n");
					sb.append("	}); \n\n");
				}

				sb.append("	function showLoading() { document.getElementById('searchResultsDiv').style.textAlign = 'center'; "
						+ "document.getElementById('searchResultsDiv').innerHTML = "
						+ "'<br/><br/><br/><br/><br/><img align=\"absmiddle\" src=\"files/graphics/misc/loadingAnimation.gif\"/> &nbsp; "
						+ "<span style=\"font-weight: bold; font-size: 18px; font-family: Verdana; color: #444444;\">Searching...</span>'; } ");

				sb.append("	function finishedLoading() { document.getElementById('searchResultsDiv').style.textAlign =  'left'; } \n\n");
*/
			}

			sb.append("</script>\n\n");

		}

		sb.append(getTableTag());

		/*
		 * Render a 'selector' first. 
		 * 
		 * This won't actually submit the item type to the form,
		 * but is just used to show / hide the various divs that contain the searches. Each div will
		 * contain a hidden tag used to send the item type to the search handler
		 * 
		 * Only show the selector if we have more than 1 search form
		 */

		if (forms.size() > 1) {

			sb.append("<tr><td class='type_selector'>");

			SelectTag tag = new SelectTag(null, "type_selector_unused");
			tag.setOnChange("showSearchForm(this.value)");
			for (SearchForm searchForm : forms) {
				tag.addOption(searchForm.getFormId() + "div", searchForm.getName());
			}

			sb.append(tag);

			sb.append("</td></tr>");

		}

		sb.append("<tr><td class='forms'>");

		// render each search form inside their own div, so we can hide / show them
		for (SearchForm searchForm : forms) {

            sb.append("<div id='" + searchForm.getFormId() + "div'>");
//			sb.append("<div id='" + searchForm.getFormId() + "div' style='display:none;'>");

			searchForm.renderFormTag(sb);
			sb.append(new HiddenTag("searchBox", this));

			sb.append(new TableTag("searchbox_form"));

			for (SearchField field : searchForm.getSearchFields()) {

				Object obj = field.render(context.getRequest().getServletPath().equals("/s.do"));
				if (obj != null) {

					sb.append("<tr><td class='label'>" + field.getName() + "</td></tr>");
					sb.append("<tr><td class='input'>" + obj + new ErrorTag(context, field.getParam(), "<br/>") + "</td></tr>");
				}
			}

			sb.append("<tr><td>");

			if (searchForm.isShowSubmit()) {

				if (searchForm.hasSubmitSrc()) {
					sb.append(new ImageSubmitTag("template-data/" + searchForm.getSubmitSrc()));
				} else {

					Language language = (Language) context.getAttribute("language");

					String submitLabel = searchForm.getSubmitLabel();
					if (language != Language.English) {
						submitLabel = Translation.translate(context, language, submitLabel);
					}

					sb.append(new SubmitTag(submitLabel));
				}
			}

			sb.append("</td></tr>");
			sb.append("</table>");

			sb.append("</form>");
			sb.append("</div>");

		}

		sb.append("</td></tr>");
		sb.append("<tr><td class='bottom'></td></tr>");
		sb.append("</table>");

		/*
		 * Do a script to just show first one initially if we have more than one form
		 */
		if (forms.size() > 1) {
			sb.append("<script type=\"text/javascript\"> showSearchForm('" + forms.get(0).getFormId() + "div'); </script> ");
		}

		return sb.toString();

	}

	public void setAutoSubmit(boolean autoSubmit) {
		this.autoSubmit = autoSubmit;
	}

}
