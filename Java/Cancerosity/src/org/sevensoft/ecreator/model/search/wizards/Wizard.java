package org.sevensoft.ecreator.model.search.wizards;

import java.util.Collections;
import java.util.List;

import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 28 Jul 2006 09:27:40
 *
 */
@Table("wizards")
public class Wizard extends EntityObject implements Selectable {

	public enum Results {

		/**
		 * Browse as search results
		 */
		Browse,

		/**
		 * Show results in select drop down, choose one and then proceed to item page
		 */
		DropDown;
	}

	public static List<Wizard> get(RequestContext context) {
		return SimpleQuery.execute(context, Wizard.class);
	}

	/**
	 * The item type of this wizard. Must be set
	 */
	private ItemType		itemType;

	/**
	 * The text that will be displayed as a link to restart the wizard
	 */
	private String		restartText;

	private AutoForward	autoForward;

	/**
	 * Limit attribute values to items in this category
	 */
	private Category		category;

	/**
	 * What to do when we have finished the wizard.
	 * If null then show all end results as a search.
	 * Otherwise perform this action
	 */
	private Results		results;

	/**
	 * The name for this wizard
	 */
	private String		name;

	private Wizard(RequestContext context) {
		super(context);
	}

	public Wizard(RequestContext context, String name) {
		super(context);

		this.name = name;
		this.restartText = "Click here to restart this wizard";

		save();
	}

	public WizardStep addStep() {
		return new WizardStep(context, this);
	}

	@Override
	public synchronized boolean delete() {
		removeSteps();
		SimpleQuery.delete(context, WizardBlock.class, "wizard", this);
		return super.delete();
	}

	/**
	 * Returns all attributes set on this wizard.
	 */
	public List<Attribute> getAttributes() {

		Query q = new Query(context, "select a.* from # a join # wa on a.id=wa.attriute where wa.wizard=? order by position");
		q.setParameter(this);
		q.setTable(Wizard.class);
		return q.execute(Attribute.class);
	}

	public final AutoForward getAutoForward() {
		return autoForward == null ? AutoForward.Never : autoForward;
	}

	public Category getCategory() {
		return (Category) (category == null ? null : category.pop());
	}

	public ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public String getLabel() {
		return getName();
	}

	public String getName() {
		return name;
	}

	public String getRestartText() {
		return restartText;
	}

	public Results getResults() {
		return results == null ? Results.DropDown : results;
	}

	public List<WizardStep> getSteps() {
		Query q = new Query(context, "select * from # where wizard=? order by position");
		q.setTable(WizardStep.class);
		q.setParameter(this);
		return q.execute(WizardStep.class);
	}

	/**
	 * Returns the steps up to this number
	 */
	public List<WizardStep> getSteps(int n) {

		List<WizardStep> steps = getSteps();
		if (steps.isEmpty()) {
			return null;
		}

		if (n == 0) {
			return Collections.singletonList(steps.get(0));
		}

		if (n >= steps.size()) {
			return steps;
		}

		return steps.subList(0, n + 1);
	}

	public String getValue() {
		return getIdString();
	}

	public boolean hasCategory() {
		return category != null;
	}

	public boolean hasItemType() {
		return itemType != null;
	}

	public boolean hasRestartText() {
		return restartText != null;
	}

	private void removeSteps() {
		SimpleQuery.delete(context, WizardStep.class, "wizard", this);
	}

	public final void setAutoForward(AutoForward autoForward) {
		this.autoForward = autoForward;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public void setItemType(ItemType i) {

		if (ObjectUtil.equal(itemType, i))
			return;

		this.itemType = i;
		removeSteps();
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRestartText(String restartText) {
		this.restartText = restartText;
	}

	public void setResults(Results results) {
		this.results = results;
	}

}
