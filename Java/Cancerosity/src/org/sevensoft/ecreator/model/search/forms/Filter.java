package org.sevensoft.ecreator.model.search.forms;

import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 18 Apr 2007 15:49:01
 *
 */
@Table("search_forms_filters")
public class Filter extends EntityObject implements Positionable {

	private Attribute		attribute;
	private SearchForm	searchForm;
	private int			position;

	protected Filter(RequestContext context) {
		super(context);
	}

	public Filter(RequestContext context, SearchForm form, Attribute attribute) {
		super(context);

		this.searchForm = form;
		this.attribute = attribute;

		save();
	}

	public final Attribute getAttribute() {
		return attribute.pop();
	}

	public int getPosition() {
		return position;
	}

	public final SearchForm getSearchForm() {
		return searchForm.pop();
	}

	public void setPosition(int position) {
		this.position = position;
	}

}
