package org.sevensoft.ecreator.model.search.emails;

import java.util.List;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.search.alerts.SearchAlert;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 15-Feb-2006 15:12:20
 *
 */
public class SearchResultsEmail extends Email {

	public SearchResultsEmail(SearchAlert alert, List<Item> items, RequestContext context) {

		setSubject("Search results");
		setTo("sks@global7.co.uk");
		setFrom("server@global7.co.uk");

		StringBuilder sb = new StringBuilder("Hi, ");
		//.append(search.getMember2().getName());
		sb.append("\n\n");
		sb.append("Here are the latest " + items.size() + " results from your search agent");

		//		if (alert.hasKeywords())
		//			sb.append("\nInterval: " + alert.getRunInterval());
		//
		//		if (alert.hasKeywords())
		//			sb.append("\nKeywords: " + alert.getKeywords());
		//
		//		if (alert.hasCategory())
		//			sb.append("\nFrom category: " + alert.getCategory().getName());

		//		if (search.hasAddress())
		//			sb.append("\nAddress matching: " + search.getAddress());

		sb.append("\n\nIf you do not want to receive any more search results then click here:\n");
		sb.append(Config.getInstance(context).getUrl() + "/");
		//		sb.append(new LinkTag(OldSearchHandler.class, "delsa", "remove", "sa", search, "email", search.getMember2().getEmail()).getUrl());
		sb.append("\n\n");

		for (Item item : items) {

			sb.append(item.getName());
			sb.append("\n");

			//			if (listing.hasAddress()) {
			//				sb.append(listing.getAddressLabel(", ", true));
			//				sb.append("\n");
			//			}

			if (item.hasCategory()) {
				sb.append("From: ");
				sb.append(item.getCategory().getName());
				sb.append("\n");
			}

			sb.append(item.getUrl());
			sb.append("\n\n\n");
		}

		sb.append("You can find more on our site:\n" + Config.getInstance(context).getUrl());

		sb.append("\n\nRegards,\nThe " + Company.getInstance(context).getName() + " team");

		setBody(sb);
		System.out.println(this);
	}

}
