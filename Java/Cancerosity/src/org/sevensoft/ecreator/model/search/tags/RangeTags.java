package org.sevensoft.ecreator.model.search.tags;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.ecreator.model.system.company.Company;

/**
 * @author sks 27 Sep 2006 11:54:06
 *
 */
public class RangeTags {

	private final Collection<String>	ranges;
	private final RequestContext		context;
	private boolean				autoSubmit;
	private boolean				fixedBands;
	private final String			fromParam;
	private final String			toParam;
	private boolean				optional;
	private String				any;
	private int					minDp;
	private int					maxDp;
	private boolean				grouping;
	private boolean				ajax;
	private String				formId;

	public RangeTags(RequestContext context, Collection<String> ranges, String fromParam, String toParam) {
		this.context = context;
		this.fromParam = fromParam;
		this.toParam = toParam;
        this.ranges = ranges;
		this.any = "Any";
	}

	private void options(SelectTag tag, List<String> list) {

		NumberFormat formatter = NumberFormat.getInstance(Company.getInstance(context).getLocale());
		formatter.setGroupingUsed(grouping);
		formatter.setMaximumFractionDigits(maxDp);
		formatter.setMinimumFractionDigits(minDp);

        tag.addOptions(list);
	}

	public void setAny(String string) {
		this.any = string;
	}

	public void setAutoSubmit(boolean autoSubmit) {
		this.autoSubmit = autoSubmit;
	}

	public void setFixedBands(boolean fixedBands) {
		this.fixedBands = fixedBands;
	}

	public void setGrouping(boolean grouping) {
		this.grouping = grouping;
	}

	public final void setMaxDp(int maxDp) {
		this.maxDp = maxDp;
	}

	public final void setMinDp(int minDp) {
		this.minDp = minDp;
	}

	public void setOptional(boolean optional) {
		this.optional = optional;
	}

	@Override
	public String toString() {

		if (ranges == null || ranges.size() < 3) {
			return null;
		}

		List<String> list = new ArrayList<String>(ranges);

		if (fixedBands) {

			SelectTag tag = new SelectTag(context, fromParam);

			if (autoSubmit) {

				if (ajax) {
					tag.setOnChange("$('#" + formId + "').ajaxSubmit(searchSubmitOptions);");
				} else {
					tag.setAutoSubmit(true);
				}
			}

			if (optional) {
				tag.setAny(any);
			}

			Iterator<String> iter = ranges.iterator();
			String a = iter.next();
			while (iter.hasNext()) {

				String b = iter.next();

				tag.addOption(a + "-" + b, a + " to " + b);

				a = b;
			}

			return tag.toString();

		} else {

			SelectTag fromTag = new SelectTag(context, fromParam);
			if (optional) {
				fromTag.addOption("*", any);
			}
			options(fromTag, list.subList(0, ranges.size() - 1));

			SelectTag toTag = new SelectTag(context, toParam);
			if (optional) {
				toTag.addOption("*", any);
			}
			options(toTag, list.subList(1, ranges.size()));

			if (autoSubmit) {

				if (ajax) {
					fromTag.setOnChange("$('#" + formId + "').ajaxSubmit(searchSubmitOptions);");
					toTag.setOnChange("$('#" + formId + "').ajaxSubmit(searchSubmitOptions);");
				} else {
					fromTag.setAutoSubmit(true);
					toTag.setAutoSubmit(true);
				}
			}

			return fromTag + " to " + toTag;
		}
	}

	/**
	 * 
	 */
	public void setAjax(boolean ajax, String formId) {
		this.ajax = ajax;
		this.formId = formId;
	}
}
