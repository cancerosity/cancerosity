package org.sevensoft.ecreator.model.search.markers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 6 Jul 2006 13:07:11
 *
 */
public class SearchItemTypeMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		boolean toggle = params.containsKey("toggle");
		String id = params.get("id");
		String ids = params.get("ids");
		String name = params.get("id");

		if (id != null) {

			return new HiddenTag("itemType", id);

		} else if (name != null) {

			ItemType itemType = ItemType.getByName(context, name);
			return itemType == null ? null : new HiddenTag("itemType", itemType);

		} else {

			StringBuilder sb = new StringBuilder();

			// get item types	
			List<ItemType> itemTypes;
			if (ids == null) {

				itemTypes = ItemType.getSearchable(context);

			} else {

				itemTypes = new ArrayList();

				for (int i : StringHelper.explodeIntegers(ids, ",")) {

					ItemType itemType = EntityObject.getInstance(context, ItemType.class, i);
					if (itemType != null) {
						itemTypes.add(itemType);
					}
				}
			}

			if (itemTypes.isEmpty()) {
				return null;
			}

			SelectTag tag = new SelectTag(null, "itemType", itemTypes.get(0));
			tag.addOptions(itemTypes);

			if (toggle) {

				sb.append("<script>");
				sb.append("function hideSearchFields() {");
				for (ItemType itemType : itemTypes) {
					sb.append("	var e = document.getElementById('searchdiv_" + itemType.getId() + "'); ");
					sb.append(" if (e != null) e.style.display = 'none'; ");
				}
				sb.append("}");

				sb.append("function showSearchFields(t) {");
				sb.append("	hideSearchFields(); ");
				sb.append("	document.getElementById('searchdiv_' + t).style.display = 'block'; ");
				sb.append("}");

				/*
				 * Register a function to load on onload
				 */
				sb.append("var z = window.onload; ");
				sb.append("window.onload = function() {  if (z) z();  hideSearchFields(); showSearchFields('" + itemTypes.get(0).getId() + "'); } ");

				sb.append("</script>");

				tag.setOnChange("showSearchFields(this.value);");

			}

			sb.append(tag);

			return sb;
		}
	}

	public Object getRegex() {
		return "search_itemtype";
	}

}
