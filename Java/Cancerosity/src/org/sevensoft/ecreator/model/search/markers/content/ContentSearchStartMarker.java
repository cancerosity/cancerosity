package org.sevensoft.ecreator.model.search.markers.content;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.search.ContentSearchHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;

/**
 * @author sks 14 Jan 2007 10:34:39
 *
 */
public class ContentSearchStartMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {
		return new FormTag(ContentSearchHandler.class, null, "get");
	}

	public Object getRegex() {
		return "csearch_start";
	}

}
