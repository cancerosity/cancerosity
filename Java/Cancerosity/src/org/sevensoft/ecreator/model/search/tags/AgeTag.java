package org.sevensoft.ecreator.model.search.tags;

import java.util.Collection;
import java.util.List;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 5 Sep 2006 20:13:03
 *
 */
public class AgeTag {

	private List<Integer>		ages;
	private final RequestContext	context;
	private final String		param;
	private String			any;
	private boolean			autoSubmit;
	private boolean			optional;

	public AgeTag(RequestContext context, String string, Collection<String> ages) {
		this.context = context;
		this.param = string;
		this.ages = StringHelper.getIntegersRanged(ages);
		this.any = "Any";
		this.optional = true;
	}

	public AgeTag(RequestContext context, String string, String ages) {
		this(context, string, StringHelper.explodeStrings(ages, ","));
	}

	public void setAny(String any) {
		this.any = any;
	}

	public void setAutoSubmit() {
		this.autoSubmit = true;
	}

	public void setOptional(boolean optional) {
		this.optional = optional;
	}

	@Override
	public String toString() {

		SelectTag tag = new SelectTag(context, param);

		if (autoSubmit)
			tag.setAutoSubmit();

		if (optional)
			tag.setAny(any);

		for (int age : ages)
			if (age == 1)
				tag.addOption("New today");
			else
				tag.addOption(age + " days or newer");

		return tag.toString();
	}

}
