package org.sevensoft.ecreator.model.search.markers.location;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.search.tags.DistanceTag;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 31-Mar-2006 15:23:41
 *
 */
public final class SearchDistanceMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!params.containsKey("distances")) {
			return null;
		}

		String any = params.get("any");
		if (any == null) {
			any = "Any distance";
		}

		DistanceTag tag = new DistanceTag(context, params.get("distances"));
		tag.setAny(any);
		return tag;
	}

	public Object getRegex() {
		return "search_distance";
	}
}