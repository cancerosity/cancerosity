package org.sevensoft.ecreator.model.search;

import java.util.*;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.attributes.support.AttributeSupport;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.ecom.promotions.Promotion;
import org.sevensoft.ecreator.model.interaction.LastActive;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.ItemTypeSettings;
import org.sevensoft.ecreator.model.items.highlighted.HighlightMethod;
import org.sevensoft.ecreator.model.items.highlighted.Ticker;
import org.sevensoft.ecreator.model.items.highlighted.blocks.HighlightedItemsBlock;
import org.sevensoft.ecreator.model.items.highlighted.boxes.HighlightedItemsBox;
import org.sevensoft.ecreator.model.items.sorts.ItemSort;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.ecreator.model.misc.location.Locatable;
import org.sevensoft.ecreator.model.misc.location.LocationUtil;
import org.sevensoft.ecreator.model.search.forms.SearchForm;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 12-Nov-2005 11:06:27
 * 
 * This class represents an item search on the system.
 * It can be persisted or created as a temporary search.
 * 
 * They are usually tagged to a member as personal saved searches but can be tagged to a category (like old style containers)
 * 
 */
@Table("searches")
public class Search extends AttributeSupport implements Locatable {

    public enum FeaturedMethod {
		Yes, No, Any
	}

	public static void delete(RequestContext context, SearchOwner owner) {
		SimpleQuery.delete(context, Search.class, owner.getClass().getSimpleName(), owner);
	}

	public static List<Search> get(RequestContext context) {
		Query q = new Query(context, "select * from #");
		q.setTable(Search.class);
		return q.execute(Search.class);
	}

	/**
	 * Returns a search for this search owner.
	 * Will create if required
	 */
	public static Search get(RequestContext context, SearchOwner owner) {
		Search search = SimpleQuery.get(context, Search.class, "owner", owner.getFullId());
		if (search == null) {
			search = new Search(context, owner);
		}
		return search;
	}

	public static List<Search> getActive(RequestContext context, Category category) {
		return Collections.emptyList();
	}

	public static List<Search> getScheduled(RequestContext context) {

		Query q = new Query(context, "select * from # where runInterval>0");
		q.setTable(Search.class);
		List<Search> searches = q.execute(Search.class);
		CollectionsUtil.filter(searches, new Predicate<Search>() {

			public boolean accept(Search e) {
				return e.isScheduled();
			}

		});
		return searches;
	}

	private HighlightedItemsBlock	highlightedItemsBlock;

	private String			email;

	private String			startsWith;

	private int				distance;

	/**
	 * 
	 */
	private boolean			noCategory;

	private boolean			noImages;

	private HighlightMethod		method;

	private boolean			multipleItemTypes;

	private FeaturedMethod		featuredMethod;

	/**
	 * Only items created on the system after this date.
	 * Use this field to search for say newly added items
	 */
	private Date			createdAfter;

	private int				start;

	private Ticker			ticker;

	/**
	 * 
	 */
	private String			name;

	/**
	 * Only items with an image
	 */
	private boolean			imageOnly;

	/**
	 * search any attribute, content and name
	 */
	private String			details;

	@Deprecated
	private boolean			featured;

	/**
	 * Id of the target object
	 */
	private int				wildcardId;

	/**
	 * The member who this search is owned by
	 */
	private Item			member;

	private Category			searchCategory;

	/**
	 * How often to run for an auto runner in hours
	 */
	private int				runInterval;

	private String			keywords;

	/**
	 * Exclude the item type of the currently active item
	 */
	private boolean			excludeAccountItemType;

	private boolean			excludeAccount;

	/**
	 * Category owner
	 */
	private Category			category;

	private int				minStock;

	/**
	 * The promotion owner
	 */
	private Promotion			promotion;

	private int				y;

	private int				x;

	private String			location;

	/**
	 * Date this search was created on the system
	 */
	private DateTime			dateCreated;

	/**
	 * Item type to search for if we are searching for single item types
	 */
	private ItemType			itemType;

	/**
	 * Last time this search was run.
	 */
	private Date			lastRuntime;

	/**
	 * Send only new items
	 * IE those items / members which are new since this search was last run
	 */
	private boolean			newOnly;

	/**
	 * Max items to display
	 */
	private int				limit;

	private LastActive		lastActive;

	/**
	 * Show members with this subscription only
	 */
	private SubscriptionLevel	subscriptionLevel;

	/**
	 * 
	 */
	private boolean			subcats;

	private int				age;

	private ItemSort			sort;

	private Money			sellPriceTo;

	private Money			sellPriceFrom;

	private SortType			sortType;

	private String			itemStatus;

	private int				joinedWithinDays;

	private SearchForm		searchForm;

	/**
	 * A name assigned by the user
	 */
	private String			customName;

	private boolean			inStockOnly;

	private HighlightedItemsBox	highlightedItemsBox;

	private boolean			recordingOnly;

	private String			owner;

	private Set<Integer>		itemTypes;

	private Date			bookingStart;

	private Date			bookingEnd;

	private Item			searchAccount;

    private List<Integer>			searchAccounts;

    /**
     * to display itemType(listing) in a box depending on which item of ItemType(listing owner)  page is shown
     */
    private ItemType			itemTypeDisplayed;

    private boolean currDateWrapper;

//    @Default("1")
    private boolean showOnlyChildren;

    private boolean showOnlyRelatives;

    public Search(RequestContext context) {
		super(context);

		this.dateCreated = new DateTime();
		logger.fine("created new transient search");
	}

	public Search(RequestContext context, SearchOwner owner) {

		super(context);

		this.dateCreated = new DateTime();
		this.owner = owner.getFullId();

		this.featuredMethod = FeaturedMethod.Any;

		save();
		logger.fine("created new persisted search id=" + getId());
	}

	@Override
	public synchronized boolean delete() {

		removeAttributeValues();
		return super.delete();
	}

	public int getAge() {
		return age;
	}

	/**
	 * Returns attributes from the item type if set
	 */
	public List<Attribute> getAttributes() {

		// get global attributes
		List<Attribute> attributes = ItemTypeSettings.getInstance(context).getAttributes();

		if (itemType != null) {
			attributes.addAll(getItemType().getAttributes());
		}
        List<Attribute> remove = new ArrayList<Attribute>();
        for (Attribute attribute : attributes) {
            if (attribute.isDublicate()) {
                remove.add(attribute);
            }
        }

        for (Attribute rem : remove) {
            attributes.remove(rem);
        }

        return attributes;
	}

	public Category getCategory() {
		return (Category) (category == null ? null : category.pop());
	}

	public int[] getCoords() {
		return new int[] { x, y };
	}

	public Date getCreatedAfter() {
		return createdAfter;
	}

	public final String getCustomName() {
		return customName;
	}

	public DateTime getDateCreated() {
		return dateCreated;
	}

	public String getDetails() {
		return details;
	}

	public int getDistance() {
		return distance;
	}

	public String getEmail() {
		return email;
	}

	public final FeaturedMethod getFeaturedMethod() {
		if (featuredMethod == null) {
			if (featured) {
				featuredMethod = FeaturedMethod.Yes;
			} else {
				featuredMethod = FeaturedMethod.Any;
			}
			save();
		}
		return featuredMethod;
	}

	public ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public final Set<Integer> getItemTypes() {
		if (itemTypes == null) {
			itemTypes = new HashSet();
		}
		return itemTypes;
	}

	public int getJoinedWithinDays() {
		return joinedWithinDays;
	}

	public String getKeywords() {
		return keywords;
	}

	public LastActive getLastActive() {
		return lastActive;
	}

	public Date getLastRuntime() {
		return lastRuntime;
	}

	public int getLimit() {
		return limit;
	}

	/**
	 * Returns a link to perform this search
	 */
	public Link getLink() {

		Link link = getSearcher().getLink();

		if (searchForm != null) {
			link.setParameter("searchForm", searchForm);
		}

		return link;
	}

	public String getLocation() {
		return location;
	}

	public HighlightMethod getMethod() {
		return method;
	}

	public int getMinStock() {
		return minStock;
	}

	public String getName() {
		return name;
	}

	public int getRunInterval() {
		return runInterval;
	}

	public Category getSearchCategory() {
		return (Category) (searchCategory == null ? null : searchCategory.pop());
	}

	public ItemSearcher getSearcher() {

		ItemSearcher searcher = new ItemSearcher(context);

		searcher.setAge(age);

        MultiValueMap<Attribute, String> attributeValues = getAttributeValues();

        if (isCurrDateWrapper()) {
            for (Map.Entry<Attribute, String> entry : attributeValues.entrySet()) {
                if (entry.getKey().getType() == AttributeType.Date) {
                    attributeValues.set(entry.getKey(), new Date().getTimestampString());
                }
            }
        }
        searcher.setAttributeValues(attributeValues);

        searcher.setSearchAccount(searchAccount);
        searcher.setSearchAccounts(searchAccounts);

		searcher.setCreatedAfter(createdAfter);

		searcher.setDetails(details);
		searcher.setDistance(getDistance());

		searcher.setEmail(email);

		searcher.setExcludeAccount(excludeAccount);
		searcher.setExcludeAccountItemType(excludeAccountItemType);

		searcher.setFeaturedMethod(featuredMethod);
		searcher.setImageOnly(imageOnly);

		searcher.setItemType(getItemType());
		searcher.setItemTypes(getItemTypes());

		searcher.setKeywords(getKeywords());
		searcher.setLastActive(getLastActive());
		searcher.setLimit(getLimit());
		searcher.setLocation(location, x, y);

		searcher.setName(name);
		searcher.setNoCategory(noCategory);
		searcher.setNoImages(noImages);

		searcher.setRecordingsOnly(isRecordingOnly());

		searcher.setSearchCategory(getSearchCategory());
		searcher.setStatus(itemStatus);
		searcher.setStartsWith(startsWith);
		searcher.setSellPriceFrom(sellPriceFrom);
		searcher.setSellPriceTo(sellPriceTo);

		// only use sort if it is not null
		if (sort != null) {
			
			searcher.setSort(getSort());
			
		} else if (sortType != null) {
			
			searcher.setSortType(sortType);
		}

		searcher.setStart(start);
		searcher.setSubcats(isSubcats());

		searcher.setMethod(method);

		return searcher;
	}

    public Item getSearchAccount() {
        return (Item) (searchAccount == null ? null : searchAccount.pop());
    }

    public List<Integer> getSearchAccounts() {
        if (searchAccounts == null) {
            searchAccounts = new ArrayList<Integer>();
        }
        if (searchAccounts.isEmpty() && getSearchAccount() != null) {
            searchAccounts.add(getSearchAccount().getId());
        }
        return searchAccounts;
    }

    public SearchForm getSearchForm() {
		return searchForm;
	}

	public Money getSellPriceFrom() {
		return sellPriceFrom;
	}

	public Money getSellPriceTo() {
		return sellPriceTo;
	}

	public ItemSort getSort() {
		return (ItemSort) (sort == null ? null : sort.pop());
	}

	public SortType getSortType() {
		return sortType;
	}

	public String getStartsWith() {
		return startsWith;
	}

	public String getStatus() {
		return itemStatus;
	}

	public String getString() {

		StringBuilder sb = new StringBuilder();
		sb.append("Searching for ");
		if (itemType == null) {
			sb.append(" all users ");
		} else {
			sb.append(getItemType().getNamePluralLower());
		}

		if (lastActive != null) {

			switch (lastActive) {

			case OnlineNow:
				sb.append(" who are online now ");
				break;

			default:
				sb.append(" last online " + lastActive);
			}

		}

		boolean and = false;
		boolean where = false;

		if (name != null) {

			if (!where) {
				sb.append(" where ");
				where = true;
			}

			sb.append(" name is \"");
			sb.append(name);
			sb.append("\"");
			and = true;
		}

		if (keywords != null) {

			if (!where) {
				sb.append(" where ");
				where = true;
			}

			if (and) {
				sb.append(" and ");
			}

			sb.append(" keywords are \"");
			sb.append(name);
			sb.append("\"");
			and = true;
		}

		if (details != null) {

			if (!where) {
				sb.append(" where ");
				where = true;
			}

			if (and) {
				sb.append(" and ");
			}

			sb.append(" description contains \"");
			sb.append(name);
			sb.append("\"");
			and = true;
		}

		if (imageOnly) {

			if (!where) {
				sb.append(" where ");
				where = true;
			}

			if (and) {
				sb.append(" and ");
			}

			sb.append(" has images");
			and = true;
		}

		for (Attribute attribute : getAttributeValues().keySet()) {

			if (!where) {
				sb.append(" where ");
				where = true;
			}

			if (and) {
				sb.append(" and ");
			}

			List<String> values = getAttributeValues(attribute);
			switch (attribute.getType()) {

			case Numerical:

				if (values.size() == 2) {

					String value1 = values.get(0);
					String value2 = values.get(1);

					// should never occur
					if (value1.equals("*") && value2.equals("*")) {

					} else if (value1.equals("*")) {

						sb.append(attribute.getName() + " is less than " + value2);

					} else if (value2.equals("*")) {

						sb.append(attribute.getName() + " is greater than " + value1);

					} else {

						sb.append(attribute.getName() + " is between " + value1 + " and " + value2);
					}

				} else {

					sb.append(attribute.getName() + " is " + values.get(0));
				}

				break;

			default:

				sb.append(attribute.getName() + " is ");

				Iterator<String> iter = values.iterator();
				while (iter.hasNext()) {
					sb.append(iter.next());
					if (iter.hasNext()) {
						sb.append(" or ");
					}
				}

			}

			and = true;

		}

		if (location != null) {

			if (distance > 0) {
				sb.append(" within " + distance + " miles of ");
			} else {
				sb.append(" and nearest city is ");
			}

			sb.append(location);
		}

		if (category != null) {
			sb.append(" in " + category.getName());
		}

		return sb.toString();
	}

	public SubscriptionLevel getSubscriptionLevel() {
		return (SubscriptionLevel) (subscriptionLevel == null ? null : subscriptionLevel.pop());
	}

	public final int getWildcardId() {
		return wildcardId;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public boolean hasCategory() {
		return category != null;
	}

	public boolean hasCustomName() {
		return customName != null;
	}

	public boolean hasDistance() {
		return distance > 0;
	}

	public boolean hasItemType() {
		return itemType != null;
	}

	public boolean hasKeywords() {
		return keywords != null;
	}

	public boolean hasLocation() {
		return location != null;
	}

	public boolean hasSearchCategory() {
		return searchCategory != null;
	}

	public final boolean isExcludeAccount() {
		return excludeAccount;
	}

	public final boolean isExcludeAccountItemType() {
		return excludeAccountItemType;
	}

	public boolean isImageOnly() {
		return imageOnly;
	}

	public boolean isInStockOnly() {
		return inStockOnly;
	}

	public final boolean isMultipleItemTypes() {
		return multipleItemTypes;
	}

	public boolean isNewOnly() {
		return newOnly;
	}

	public boolean isNoCategory() {
		return noCategory;
	}

	public boolean isNoImages() {
		return noImages;
	}

	public final boolean isRecordingOnly() {
		return recordingOnly;
	}

	public boolean isScheduled() {

		if (lastRuntime == null) {
			return true;
		}

		return lastRuntime.addDays(runInterval).getTimestamp() <= System.currentTimeMillis();
	}

	public boolean isSubcats() {
		return subcats;
	}

	public void removeLocation() {
		this.location = null;
		this.x = 0;
		this.y = 0;
	}

	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * 
	 */
	public void setBookingEnd(Date bookingEnd) {
		this.bookingEnd = bookingEnd;
	}

	/**
	 * 
	 */
	public void setBookingStart(Date bookingStart) {
		this.bookingStart = bookingStart;
	}

	public void setCreatedAfter(Date createdAfter) {
		this.createdAfter = createdAfter;
	}

	public final void setCustomName(String customName) {
		this.customName = customName;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public final void setExcludeAccount(boolean excludeActiveItem) {
		this.excludeAccount = excludeActiveItem;
	}

	public final void setExcludeAccountItemType(boolean excludeActiveItemType) {
		this.excludeAccountItemType = excludeActiveItemType;
	}

	public final void setFeaturedMethod(FeaturedMethod featuredMethod) {
		this.featuredMethod = featuredMethod;
	}

	public void setImageOnly(boolean imageOnly) {
		this.imageOnly = imageOnly;
	}

	public void setInStockOnly(boolean isInStockOnly) {
		this.inStockOnly = isInStockOnly;
	}

	public void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	public void setItemTypes(Set<Integer> itemTypes) {
		this.itemTypes = itemTypes;
	}

	public void setJoinedWithinDays(int joinedWithinDays) {
		this.joinedWithinDays = joinedWithinDays;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public void setLastActive(LastActive lastActive) {
		this.lastActive = lastActive;
	}

	public void setLastRuntime(Date lastRuntime) {
		this.lastRuntime = lastRuntime;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public void setLocation(String location) {
		LocationUtil.setLocation(context, this, location);
	}

	public void setLocation(String location, int x, int y) {

		location = null;
		x = 0;
		y = 0;

		save("location", "x", "y");
	}

	public void setMethod(HighlightMethod method) {
		this.method = method;
	}

	public void setMinStock(int minStock) {
		this.minStock = minStock;
	}

	public final void setMultipleItemTypes(boolean multipleItemTypes) {
		this.multipleItemTypes = multipleItemTypes;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNewOnly(boolean newOnly) {
		this.newOnly = newOnly;
	}

	public void setNoCategory(boolean noCategory) {
		this.noCategory = noCategory;
	}

	public void setNoImages(boolean noImages) {
		this.noImages = noImages;
	}

	public final void setRecordingOnly(boolean recordingOnly) {
		this.recordingOnly = recordingOnly;
	}

	public void setRunInterval(int runInterval) {
		this.runInterval = runInterval;
	}

	public void setSearchCategory(Category searchCategory) {
		this.searchCategory = searchCategory;
	}

	public void setSearchForm(SearchForm searchForm) {
		this.searchForm = searchForm;
	}

	public void setSellPriceFrom(Money sellPriceFrom) {
		this.sellPriceFrom = sellPriceFrom;
	}

	public void setSellPriceTo(Money sellPriceTo) {
		this.sellPriceTo = sellPriceTo;
	}

	public void setSort(ItemSort sort) {
		this.sort = sort;
		this.sortType = null;
	}

	public void setSortType(SortType sortType) {
		this.sortType = sortType;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public void setStartsWith(String letter) {
		this.startsWith = letter;
	}

	public void setStatus(String s) {
		this.itemStatus = s;
	}

	public void setSubcats(boolean subcats) {
		this.subcats = subcats;
	}

	public void setSubscriptionLevel(SubscriptionLevel subscription) {
		this.subscriptionLevel = subscription;
	}

	public boolean hasHighlightedItemsBox() {
		return highlightedItemsBox != null;
	}

	public boolean hasHighlightedItemsBlock() {
		return highlightedItemsBlock != null;
	}

	public final void setWildcardId(int wildcardId) {
		this.wildcardId = wildcardId;
	}

	public final HighlightedItemsBlock getHighlightedItemsBlock() {
		return (HighlightedItemsBlock) (highlightedItemsBlock == null ? null : highlightedItemsBlock.pop());
	}

	public final HighlightedItemsBox getHighlightedItemsBox() {
		return (HighlightedItemsBox) (highlightedItemsBox == null ? null : highlightedItemsBox.pop());
	}

	/**
	 * 
	 */
	public void setSearchAccount(Item searchAccount) {
		this.searchAccount = searchAccount;
	}

    public void setSearchAccounts(List<Integer> searchAccounts) {
        this.searchAccounts = searchAccounts;
    }

    public ItemType getItemTypeDisplayed() {
        return (ItemType) (itemTypeDisplayed == null ? null : itemTypeDisplayed.pop());
    }

    public void setItemTypeDisplayed(ItemType itemTypeDisplayed) {
        this.itemTypeDisplayed = itemTypeDisplayed;
    }

    public boolean isCurrDateWrapper() {
        return currDateWrapper;
    }

    public void setCurrDateWrapper(boolean currDateWrapper) {
        this.currDateWrapper = currDateWrapper;
    }

    public boolean isShowOnlyChildren() {
        return showOnlyChildren;
    }

    public void setShowOnlyChildren(boolean showOnlyChildren) {
        this.showOnlyChildren = showOnlyChildren;
    }

    public boolean isShowOnlyRelatives() {
        return showOnlyRelatives;
    }

    public void setShowOnlyRelatives(boolean showOnlyRelatives) {
        this.showOnlyRelatives = showOnlyRelatives;
    }
}
