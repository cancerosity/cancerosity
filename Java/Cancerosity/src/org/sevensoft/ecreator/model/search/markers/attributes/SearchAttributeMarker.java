package org.sevensoft.ecreator.model.search.markers.attributes;

import java.util.Map;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.search.ASelectionRenderer;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;

/**
 * @author sks 26 Apr 2006 13:53:01
 *
 */
public class SearchAttributeMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		String id = params.get("id");
		Attribute attribute = EntityObject.getInstance(context, Attribute.class, id);
		if (attribute == null) {
			return null;
		}

		// if we have the value 
		if (params.containsKey("value")) {
			return new HiddenTag(attribute.getParamName(), params.get("value"));
		}

		//		boolean hideYear = params.containsKey("hideyear");
		boolean hideTime = params.containsKey("hidetime");

		ASelectionRenderer a = new ASelectionRenderer(context, attribute);
		//		a.setHideYear(hideYear);
		a.setHideTime(hideTime);

		if (params.containsKey("startyear")) {
			a.setStartYear(Integer.parseInt(params.get("startyear").trim()));
		}

		if (params.containsKey("endyear")) {
			a.setEndYear(Integer.parseInt(params.get("endyear").trim()));
		}

		if (params.containsKey("range")) {
			a.setYearRange(Integer.parseInt(params.get("range").trim()));
		}

		if (params.containsKey("yearrange")) {
			a.setYearRange(Integer.parseInt(params.get("yearrange").trim()));
		}

		if (params.containsKey("ranges")) {
			a.setRanges(StringHelper.explodeStrings(params.get("ranges"), ","));
		}

		return a;
	}

	public Object getRegex() {
		return "search_attribute";
	}
}
