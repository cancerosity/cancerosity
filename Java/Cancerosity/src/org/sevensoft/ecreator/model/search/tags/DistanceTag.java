package org.sevensoft.ecreator.model.search.tags;

import java.util.Collection;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 13 Sep 2006 21:15:23
 *
 */
public class DistanceTag {

	private final RequestContext		context;
	private final Collection<Integer>	distances;
	private String				any;
	private boolean				autoSubmit;
	private boolean				optional;

	public DistanceTag(RequestContext context, Collection<Integer> distances) {
		this.context = context;
		this.distances = distances;

		this.any = "Any distance";
		this.optional = true;
	}

	public DistanceTag(RequestContext context, String distances) {
		this(context, StringHelper.explodeIntegers(distances, "\\D"));
	}

	public void setAny(String any) {
		this.any = any;
	}

	public void setAutoSubmit(boolean autoSubmit) {
		this.autoSubmit = autoSubmit;
	}

	public void setOptional(boolean optional) {
		this.optional = optional;
	}

	@Override
	public String toString() {

		SelectTag tag = new SelectTag(context, "distance");
		tag.setAutoSubmit(autoSubmit);

		if (optional)
			tag.setAny(any);

		for (Integer i : distances)
			tag.addOption(i, "Within " + i + " miles");

		return tag.toString();
	}

}
