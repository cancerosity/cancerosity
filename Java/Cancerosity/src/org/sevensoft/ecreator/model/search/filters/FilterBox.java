package org.sevensoft.ecreator.model.search.filters;

import java.util.Map;

import org.sevensoft.ecreator.iface.admin.search.box.FilterBoxHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 1 Mar 2007 17:45:18
 * 
 * A filter box allows us to narrow down results when we are browsing a category, or viewing search results
 *
 */
@Table("boxes_filters")
@Label("Filters")
@HandlerClass(FilterBoxHandler.class)
public class FilterBox extends Box {

	public FilterBox(RequestContext context) {
		super(context);
	}

	public FilterBox(RequestContext context, String panel) {
		super(context, panel);
	}

	@Override
	protected String getCssIdDefault() {
		return "searchFilters";
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	@Override
	public String render(RequestContext context) {

		Map<Attribute, String> filteredAttributes = (Map<Attribute, String>) context.getAttribute("filteredAttributes");

		StringBuilder sb = new StringBuilder();
		sb.append(super.getTableTag());

		// show filtered attributes
		sb.append("<tr><td>");

		sb.append(filteredAttributes);

		sb.append("</td></tr>");

		sb.append("</table>");
		return sb.toString();
	}

}
