package org.sevensoft.ecreator.model.search.wizards;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.frontend.search.ItemSearchHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 25 Mar 2007 08:36:08
 *
 */
public class WizardRenderer {

	private static final Logger				logger	= Logger.getLogger("ecreator");

	private final RequestContext				context;
	private final MultiValueMap<Attribute, String>	attributeValues;
	private final Wizard					wizard;

	public WizardRenderer(RequestContext context, Wizard wizard) {

		this.context = context;
		this.wizard = wizard;
		this.attributeValues = (MultiValueMap<Attribute, String>) context.getAttribute("wizardAttributeValues");

		logger.fine("[WizardRenderer] wizardAttributeValues=" + attributeValues);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		// check for item type
		if (!wizard.hasItemType()) {
			logger.fine("[WizardRenderer] no item type set on wizard");
			return sb.toString();
		}

		/*
		 * Make a map that will store all values for each attribute as we move along
		 */
		MultiValueMap<Attribute, String> steppedValues = new MultiValueMap(new HashMap());

		// get the wizard steps
		List<WizardStep> steps = wizard.getSteps();

		Link pagelink = (Link) context.getAttribute("pagelink");
		if (pagelink == null) {
			pagelink = new Link(CategoryHandler.class);
		}

		if (wizard.hasRestartText()) {
			sb.append("<tr><td colspan='2' class='restart'>" + new LinkTag(pagelink, wizard.getRestartText()) + "</td></tr>");
		}

		/* 
		 * if the size of our current attribute values is only one less than our steps, then that means that the next
		 * step is going to be our last one. Therefore point the form to the search page.
		 * Otherwise point the form back to the page we are on.
		 */
		if (attributeValues.size() == steps.size() - 1) {

			sb.append(new FormTag(ItemSearchHandler.class, null, "get"));
			sb.append(new HiddenTag("autoForward", wizard.getAutoForward()));

			// instruct our search to only search for this item type
			sb.append(new HiddenTag("itemType", wizard.getItemType()));

		} else {

			sb.append(new FormTag(pagelink.getHref(), null, "get"));
			for (Map.Entry<String, String> entry : pagelink.getParameters().entrySet()) {
				sb.append(new HiddenTag(entry));
			}
		}
        
        for (Map.Entry<Attribute, String> entry : attributeValues.entrySet()) {
            sb.append(new HiddenTag("wizardAttributeValues_" + entry.getKey().getId(), entry.getValue()));
        }

		/*
		 * Render each wizard step up to our current step.
		 * All steps after that should be disabled.
		 */
		boolean disabled = false;
		for (WizardStep step : steps) {

			// get attribute from wizard step
			Attribute attribute = step.getAttribute();

			if (disabled) {

				sb.append("<tr class='step_row'>");
				sb.append("<td class='text'>" + step.getDescription() + "</td>");
				sb.append("<td class='select'>" + new SelectTag(null, "none", "-Please select-").setDisabled(true) + "</td>");
				sb.append("</tr>");

			} else {

				String value;
				if (attributeValues == null) {
					value = null;
				} else {
					value = attributeValues.get(attribute);
				}

				if (value == null) {

					// get all applicable values for this step based on previously chosen values
					List<String> values = step.getAttributeValues(steppedValues);

					//				// ensure the value we have choosen is actually an applicable value, otherwise reset
					//				if (!values.contains(value)) {
					//					value = null;
					//				}

					logger.fine("[WizardRenderer] step=" + step + ", value=" + value);

					SelectTag selectTag = new SelectTag(context, "wizardAttributeValues_" + attribute.getId(), value);
					selectTag.addOptions(values);
					selectTag.setAutoSubmit(true);
					selectTag.setAny("--");

					// render the step
					sb.append("<tr>");
					sb.append("<td>" + step.getDescription() + "</td>");
					sb.append("<td>" + selectTag + "</td>");
					sb.append("</tr>");

					disabled = true;

				} else {

					// render the previously choosen option
					sb.append("<tr>");
					sb.append("<td>" + step.getDescription() + "</td>");
					sb.append("<td>" + value + "</td>");
					sb.append("</tr>");

					steppedValues.put(attribute, value);
				}
			}
		}

		// render submit button
		sb.append("<tr class='continue_row'>");
		sb.append("<td></td><td class='continue'>" + new SubmitTag("Continue") + "</td>");
		sb.append("</tr>");

		sb.append("</form>");

		return sb.toString();
	}
}
