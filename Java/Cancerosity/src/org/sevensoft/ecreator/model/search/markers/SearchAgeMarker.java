package org.sevensoft.ecreator.model.search.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.search.tags.AgeTag;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 31-Mar-2006 15:23:41
 *
 */
public final class SearchAgeMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		String ages = params.get("ages");
		if (ages == null) {
			return null;
		}

		AgeTag ageTag = new AgeTag(context, "age", ages);
		return ageTag;
	}

	public Object getRegex() {
		return "search_age";
	}
}
