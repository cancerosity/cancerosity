package org.sevensoft.ecreator.model.search.forms;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.sevensoft.ecreator.iface.frontend.search.ItemSearchHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.ItemTypeSettings;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.ecreator.model.search.forms.blocks.SearchBlock;
import org.sevensoft.ecreator.model.search.forms.boxes.ItemSearchBox;
import org.sevensoft.ecreator.model.search.wizards.AutoForward;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.misc.seo.Meta;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 5 Sep 2006 15:51:11
 *
 */
@Table("search_forms")
public class SearchForm extends EntityObject implements Meta {

	/**
	 * The main item type this search form will search for
	 */
	private ItemType		itemType;

	/**
	 * Filter this search
	 */
	private boolean		filter;

	/**
	 * 
	 */
	private ItemSearchBox	searchBox;

	private AutoForward	autoForward;

	private Markup		markup;

	/**
	 *  Category to search in
	 */
	private Category		searchCategory;

	/**
	 * Hide the submit tab. Useful if you have auto submit on all your fields.
	 */
	private boolean		hideSubmit;

	private boolean		multipleItemTypes;

	/**
	 * For using multiple item types
	 */
	private Set<Integer>	itemTypes;

	/**
	 * 
	 */
	private SearchBlock	searchBlock;

	@Default("1")
	private boolean		resultsTop;

	@Default("1")
	private boolean		resultsBottom;

	/**
	 * 
	 */
	private String		noResultsForwardUrl;

	/**
	 * Take location from the active location set
	 */
	private boolean		activeLocation;

	/**
	 * This text will appear when there are no results
	 */
	private String		noResultsText;

	/**
	 * Show sorts at top on results page 
	 */
	@Default("1")
	private boolean		sortsTop;

	/**
	 * Show sorts at bottom on results page
	 */
	private boolean		sortsBottom;

	/** 
	 * Number of items per page
	 */
	private int			resultsPerPage;

	/**
	 * Include google map of results
	 */
	private boolean		googleMap;

	private SortType		sortType;

	/**
	 * Used if sort type is attribute
	 */
	private Attribute		sortAttribute;

	/**
	 * List markup for the results
	 */
	private Markup		listMarkup;

	/**
	 * Submit button label
	 */
	private String		submitLabel;

	/**
	 * Submit button image
	 */
	private String		submitSrc;

	/**
	 * Override the normal 'search results' page title
	 */
	private String		resultsPageTitle;

	/**
	 * Allow searches from this from to be saved
	 */
	private boolean		savable;

	/**
	 * Search for other item types than the one of the current active item
	 */
	private boolean		excludeAccountItemType;

	private boolean		excludeAccount;

	private boolean		alerts;

    private Markup highlightedResultsMarkup;

    private int numberOfHighlightedResults;

    private boolean     includeSubcategories;

    private String titleTag;
    private String descriptionTag;
    private String keywords;

    /**
     * enable using Patent Category/Item template on the result page instead of a default one
     */
    private boolean parentTemplate;

    private SearchForm(RequestContext context) {
		super(context);
	}

	public SearchForm(RequestContext context, ItemSearchBox searchBox) {
		super(context);
		this.searchBox = searchBox;
		save();
	}

	public SearchForm(RequestContext context, SearchBlock searchBlock) {
		super(context);
		this.searchBlock = searchBlock;
		this.resultsTop = true;
		this.sortsTop = true;
		save();
	}

	public SearchField addInputField(FieldType fieldType) {
		SearchField f = new SearchField(context, this, fieldType);
		return f;
	}

	@Override
	public synchronized boolean delete() {
		removeFields();
		SimpleQuery.delete(context, Filter.class, "searchForm", this);
		return super.delete();
	}

	public Map<String, String> getApplicableCategories() {

		if (itemType != null) {
			return Category.getItemContainersMap(context, itemType);
		}

		return Collections.emptyMap();
	}

    public Map<String, Category> getApplicableCategoriesMap(String orderby) {

        if (itemType != null) {
            return Category.getItemContainersMap1(context, itemType, orderby);
        }

        return Collections.emptyMap();
    }

    /**
	 * Returns attributes applicable for this search form
	 */
	public List<Attribute> getAttributes() {

		logger.fine("[SearchForm] getting attributes");

		// get global attributes
		List<Attribute> attributes = ItemTypeSettings.getInstance(context).getAttributes();

		if (!multipleItemTypes) {
			if (itemType != null) {
				attributes.addAll(getItemType().getAttributes());
			}
		}

		return attributes;
	}

	public final AutoForward getAutoForward() {
		return autoForward;
	}

	//	public List<Attribute> getFilteredAttributes() {
	//		Query q = new Query(context, "select a.* from # a join # fa on a.id=fa.attribute where fa.form=?");
	//		q.setTable(Attribute.class);
	//		q.setTable(FilterAttribute.class);
	//		q.setParameter(this);
	//		return q.execute(Attribute.class);
	//	}

	/**
	 * 
	 */
	public List<Filter> getFilters() {
		return SimpleQuery.execute(context, Filter.class, "searchForm", this);
	}

	public String getFormId() {
		return "searchform" + getIdString();
	}

	public ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public final Set<Integer> getItemTypes() {
		if (itemTypes == null) {
			itemTypes = new HashSet();
		}
		return itemTypes;
	}

	/**
	 * Returns a link to the search form that will search for the defaults of this form
	 */
	public Link getLink() {

		Link link = new Link(ItemSearchHandler.class);
		link.setParameter("searchForm", this);

		if (hasItemType()) {
			link.setParameter("itemType", getItemType());
		}

		if (hasSortType()) {
			link.setParameter("sortType", getSortType());
		}

		if (hasSearchCategory()) {
			link.setParameter("searchCategory", getSearchCategory());
		}

		return link;
	}

	public Markup getListMarkup() {
		return (Markup) (listMarkup == null ? null : listMarkup.pop());
	}

	public final Markup getMarkup() {
		return (Markup) (markup == null ? null : markup.pop());
	}

    public String getDescriptionTag() {
        return descriptionTag;
    }

    public String getFriendlyUrl() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getKeywords() {
        return keywords;
    }

    /**
	 * Returns a name for the type of search.
	 * 
	 * If an Item type search then will return the name of the item type (plural)
	 * If a member search will return the name of the member group
	 */
	public String getName() {

		if (itemType != null) {
			return getItemType().getNamePlural();
		}

		return "Nothing";
	}

    public String getTitleTag() {
        return titleTag;
    }

    public boolean hasDescriptionTag() {
        return descriptionTag != null;
    }

    public boolean hasKeywords() {
        return keywords != null;
    }

    public boolean hasTitleTag() {
        return titleTag != null;
    }

    public void setDescriptionTag(String descriptionTag) {
        this.descriptionTag = descriptionTag;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public void setTitleTag(String titleTag) {
        this.titleTag = titleTag;
    }

    public final String getNoResultsForwardUrl() {
		return noResultsForwardUrl;
	}

	public final String getNoResultsText() {
		return noResultsText == null ? "No results" : noResultsText;
	}

	public String getResultsPageTitle() {
		return resultsPageTitle;
	}

	public int getResultsPerPage() {
		return resultsPerPage;
	}

	public SearchBlock getSearchBlock() {
		return (SearchBlock) (searchBlock == null ? null : searchBlock.pop());
	}

	public ItemSearchBox getSearchBox() {
		return (ItemSearchBox) (searchBox == null ? null : searchBox.pop());
	}

	public Category getSearchCategory() {
		return (Category) (searchCategory == null ? null : searchCategory.pop());
	}

	public List<SearchField> getSearchFields() {
		Query q = new Query(context, "select * from # where searchForm=? order by position");
		q.setTable(SearchField.class);
		q.setParameter(this);
		return q.execute(SearchField.class);
	}

	public final Attribute getSortAttribute() {
		return (Attribute) (sortAttribute == null ? null : sortAttribute.pop());
	}

	public SortType getSortType() {
		return sortType;
	}

	public String getSubmitLabel() {
		return submitLabel == null ? "Search" : submitLabel;
	}

	public String getSubmitSrc() {
		return submitSrc;
	}

	public boolean hasItemType() {
		return itemType != null;
	}

	public boolean hasMarkup() {
		return markup != null;
	}

	public boolean hasNoResultsForwardUrl() {
		return noResultsForwardUrl != null;
	}

	public boolean hasResultsPageTitle() {
		return resultsPageTitle != null;
	}

	public boolean hasSearchBlock() {
		return searchBlock != null;
	}

	public boolean hasSearchBox() {
		return searchBox != null;
	}

	public boolean hasSearchCategory() {
		return searchCategory != null;
	}

	public boolean hasSortType() {
		return sortType != null;
	}

	public boolean hasSubmitSrc() {
		return submitSrc != null;
	}

	public final boolean isActiveLocation() {
		return activeLocation;
	}

	public boolean isAlerts() {
		return alerts && Module.SearchAlerts.enabled(context);
	}

	public boolean isExcludeAccount() {
		return excludeAccount;
	}

	public final boolean isExcludeAccountItemType() {
		return excludeAccountItemType;
	}

	public final boolean isFilter() {
		return filter && itemType != null;
	}

	public boolean isHideSubmit() {
		return hideSubmit;
	}

    public boolean isIncludeSubcategories() {
        return includeSubcategories;
    }

	public final boolean isMultipleItemTypes() {
		return multipleItemTypes;
	}

	public final boolean isResultsBottom() {
		return resultsBottom;
	}

	public final boolean isResultsTop() {
		return resultsTop;
	}

	public final boolean isSavable() {
		return savable && Module.SavedSearches.enabled(context);
	}

	public boolean isShowSubmit() {
		return !isHideSubmit();
	}

	public final boolean isSortsBottom() {
		return sortsBottom;
	}

	public final boolean isSortsTop() {
		return sortsTop;
	}

    public boolean isParentTemplate() {
        return parentTemplate;
    }

    public void removeField(SearchField field) {
		field.delete();
	}

	private void removeFields() {
		SimpleQuery.delete(context, SearchField.class, "searchForm", this);
	}

	public void renderFormTag(StringBuilder sb) {

		sb.append(new FormTag(ItemSearchHandler.class, null, "get").setId(getFormId()));

		// only white label if we are already on the search page
//		if (context.getRequest().getServletPath().equals("/s.do")) {
//			sb.append(new HiddenTag("wl", true));
//		}

		sb.append(new HiddenTag("searchForm", this));
		if (excludeAccount) {
			sb.append(new HiddenTag("excludeAccount", true));
		}

		if (isExcludeAccountItemType()) {
			sb.append(new HiddenTag("excludeAccountItemType", true));
		}

		if (getResultsPerPage() > 0) {
			sb.append(new HiddenTag("resultsPerPage", getResultsPerPage()));
		}

		if (hasSortType()) {
			sb.append(new HiddenTag("sortType", getSortType()));
			sb.append(new HiddenTag("sortAttribute", getSortAttribute()));
		}

		if (hasSearchCategory()) {
			sb.append(new HiddenTag("searchCategory", getSearchCategory()));
		}

		if (hasItemType()) {
			sb.append(new HiddenTag("itemType", getItemType()));
		}

		if (isActiveLocation()) {
			sb.append(new HiddenTag("activeLocation", true));
		}
	}

	public final void setActiveLocation(boolean activeLocation) {
		this.activeLocation = activeLocation;
	}

	public final void setAlerts(boolean alerts) {
		this.alerts = alerts;
	}

	public final void setAutoForward(AutoForward autoForward) {
		this.autoForward = autoForward;
	}

	public final void setExcludeAccount(boolean b) {
		this.excludeAccount = b;
	}

	public final void setExcludeAccountItemType(boolean b) {
		this.excludeAccountItemType = b;
	}

	public final void setFilter(boolean filter) {
		this.filter = filter;
	}

	public void setHideSubmit(boolean hideSubmit) {
		this.hideSubmit = hideSubmit;
	}

	public void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	public final void setItemTypes(Set<Integer> itemTypes) {
		this.itemTypes = itemTypes;
	}

	public void setListMarkup(Markup listMarkup) {
		this.listMarkup = listMarkup;
	}

	public final void setMarkup(Markup markup) {
		this.markup = markup;
	}

	public final void setMultipleItemTypes(boolean multipleItemTypes) {
		this.multipleItemTypes = multipleItemTypes;
	}

	public final void setNoResultsForwardUrl(String noResultsForwardUrl) {
		this.noResultsForwardUrl = noResultsForwardUrl;
	}

	public final void setNoResultsText(String noResultsText) {
		this.noResultsText = noResultsText;
	}

	public final void setResultsBottom(boolean resultsBarBottom) {
		this.resultsBottom = resultsBarBottom;
	}

	public void setResultsPageTitle(String pageTitle) {
		this.resultsPageTitle = pageTitle;
	}

	public void setResultsPerPage(int resultsPerPage) {
		this.resultsPerPage = resultsPerPage;
	}

	public final void setResultsTop(boolean resultsBarTop) {
		this.resultsTop = resultsBarTop;
	}

	public final void setSavable(boolean savable) {
		this.savable = savable;
	}

	public void setSearchCategory(Category category) {
		this.searchCategory = category;
	}

	public final void setSortAttribute(Attribute sortAttribute) {
		this.sortAttribute = sortAttribute;
	}

	public final void setSortsBottom(boolean sortsBottom) {
		this.sortsBottom = sortsBottom;
	}

	public final void setSortsTop(boolean sortsTop) {
		this.sortsTop = sortsTop;
	}

	public void setSortType(SortType sortType) {
		this.sortType = sortType;
	}

	public void setSubmitLabel(String submitLabel) {
		this.submitLabel = submitLabel;
	}

	public void setSubmitSrc(String submitSrc) {
		this.submitSrc = submitSrc;
	}

	/**
	 * 
	 */
	public Filter addFilter(Attribute attribute) {
		return new Filter(context, this, attribute);
	}

	public final boolean isGoogleMap() {
		return googleMap && Module.GoogleMap.enabled(context);
	}

	public final void setGoogleMap(boolean googleMap) {
		this.googleMap = googleMap;
	}

    public Markup getHighlightedResultsMarkup() {
        return (Markup) (highlightedResultsMarkup == null ? null : highlightedResultsMarkup.pop());
    }

    public void setHighlightedResultsMarkup(Markup highlightedResultsMarkup) {
        this.highlightedResultsMarkup = highlightedResultsMarkup;
    }

    public void setIncludeSubcategories(boolean includeSubcategories) {
        this.includeSubcategories = includeSubcategories;
    }

    public int getNumberOfHighlightedResults() {
        return numberOfHighlightedResults;
    }

    public void setNumberOfHighlightedResults(int numberOfHighlightedResults) {
        this.numberOfHighlightedResults = numberOfHighlightedResults;
    }

    public void setParentTemplate(boolean parentTemplate) {
        this.parentTemplate = parentTemplate;
    }
}
