package org.sevensoft.ecreator.model.search.content;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.misc.content.ContentBlock;
import org.sevensoft.jeezy.db.Row;
import org.sevensoft.jeezy.db.StringQueryBuilder;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 29 Dec 2006 07:09:20
 *
 */
public class ContentSearcher {

	private static Logger		logger	= Logger.getLogger("ecreator");

	private final RequestContext	context;
	private final String		keywords;
	private int				start;
	private int				limit;

	public ContentSearcher(RequestContext context, String keywords) {
		this.context = context;
		this.keywords = keywords;
	}

	public List<ContentSearchResult> getResults() {
		return (List<ContentSearchResult>) run(false);
	}

	/**
	 * Return a map of results of id to name
	 */
	public Object run(boolean count) {

		StringQueryBuilder b = new StringQueryBuilder(context);
		if (count) {
			b.append("select count(*) ");
		} else {
			b.append("select type, id ");
		}

		b.append(" from ((select '1' type, c.id id from # c where 1=1 ");
		b.addTable(Category.class);

//        String string = keywords;
		String[] names = keywords.split("\\s");
		for (int n = 0; n < names.length; n++) {

            b.append(" and ");

			String keyword = "%" + names[n] + "%";
			b.append(" c.name like ? ", keyword);
		}
        b.append(" and ( c.hidden=0 ) ");
		b.append(" group by id ) union ( select '1' type, cb.ownerCategory id from # cb where 1=1 ");
		b.addTable(ContentBlock.class);

//		string = keywords.split("\\s");
//		for (int n = 0; n < string.length; n++) {

			b.append(" and ");

//			String keyword = "%" + string[n] + "%";
        String keyword = "%" + keywords + "%";
			b.append(" cb.contentStripped like ? ", keyword);
//		}
        b.append(" and (cb.visible=1 ) ");
		b.append(" group by id ) union ( select '2' type, i.id id from # i ");
		b.addTable(Item.class);

        b.append(" join # it on i.itemtype=it.id ");
        b.addTable(ItemType.class);

        b.append(" where 1=1 ");

//		string = keywords.split("\\s");
//		for (int n = 0; n < string.length; n++) {
        for (int n = 0; n < names.length; n++) {
            b.append(" and ");
            if (n == 0) {
                b.append("(");
            }
            String key = "%" + names[n] + "%";
//			String keyword = "%" + string[n] + "%";
            b.append(" i.name like ? ", key);
        }
        b.append(" or i.contentStripped like ?) ", keyword);

        b.append(" and ( i.status='Live') ");
        b.append(" and ( i.awaitingModeration=0 and i.awaitingValidation=0) ");
        b.append(" and ( it.hidden=0) ");
        b.append(" and ( it.hiddenCs=0) ");
		b.append(" group by id )) Z");

		if (count) {
			return b.toQuery().getInt();
		}

		List<ContentSearchResult> results = new ArrayList();
		List<Row> rows = b.toQuery().execute(start, limit);
		logger.fine("[ContentSearcher] rows=" + rows.size());
		for (Row row : rows) {

			switch (row.getInt(0)) {

			default:
			case 1:

				logger.fine("[ContentSearcher] getting category id=" + row.getInt(1));

				Category category = row.getObject(1, Category.class);
				if (category != null) {
					results.add(new ContentSearchResult(category));
				}
				break;

			case 2:

				logger.fine("[ContentSearcher] getting item id=" + row.getInt(1));

				Item item = row.getObject(1, Item.class);
				results.add(new ContentSearchResult(item));
				break;
			}
		}

		return results;
	}

	public final void setLimit(int limit) {
		this.limit = limit;
	}

	public final void setStart(int start) {
		this.start = start;
	}

	public int size() {
		return (Integer) run(true);
	}
}
