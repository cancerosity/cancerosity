package org.sevensoft.ecreator.model.search.forms;

/**
 * @author sks 18 Sep 2006 12:28:58
 *
 */
public enum FieldType {

	Id() {
	},
	Attribute() {

		@Override
		public boolean isUseAny() {
			return true;
		}
	},
	Keywords() {
	},
    GroupedKeywords() {
    },
    Name() {
	},

	Details() {
	},
	Categories() {

		@Override
		public boolean isUseAny() {
			return true;
		}
	},
	Age() {

		@Override
		public boolean isUseAny() {
			return true;
		}

		@Override
		public String toString() {
			return "Age of item";
		}

	},
	LastActive() {

		@Override
		public boolean isUseAny() {
			return true;
		}

		@Override
		public String toString() {
			return "Last active";
		}
	},
	BookingStart() {

		@Override
		public String toString() {
			return "Booking start";
		}
	},
	Duration() {

		@Override
		public String toString() {
			return "Booking duration";
		}
	},
	Location() {
	},
	Distance() {

		@Override
		public boolean isUseAny() {
			return true;
		}
	},
	ImagesOnly() {

		@Override
		public String toString() {
			return "Images only";
		}
	},
	Price() {

	},
	Sort() {

	},

	Towns() {

		@Override
		public boolean isUseAny() {
			return true;
		}

		@Override
		public String toString() {
			return "Towns";
		}
	},
	Reference() {

		@Override
		public boolean isUseAny() {
			return true;
		}

		@Override
		public String toString() {
			return "Reference";
		}
	},
	MajorTowns() {

		@Override
		public boolean isUseAny() {
			return true;
		}

		@Override
		public String toString() {
			return "Major towns";
		}

	};

	public boolean isUseAny() {
		return false;
	}
}