package org.sevensoft.ecreator.model.search.wizards;


/**
 * @author sks 25 Mar 2007 16:51:23
 *
 */
public enum AutoForward {

	Never, SingleResult, Always
}
