package org.sevensoft.ecreator.model.search.markers.control;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;

/**
 * @author sks 20 Apr 2007 11:04:00
 *
 */
public class SearchResultsPerPageMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {
		
		if (params.containsKey("resultsPerPage")) {
			
			return new HiddenTag("resultsPerPage", params.get("resultsPerPage"));
			
		} else {
			
			return null;
		}
	}

	public Object getRegex() {
		return "search_rpp";
	}

}
