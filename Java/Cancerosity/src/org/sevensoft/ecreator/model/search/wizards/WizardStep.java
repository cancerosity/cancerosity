package org.sevensoft.ecreator.model.search.wizards;

import java.util.List;
import java.util.Map;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeValue;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategoryItem;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 28 Jul 2006 09:28:32
 *
 */
@Table("wizards_steps")
public class WizardStep extends EntityObject implements Positionable {

	public enum Type {
		Attribute;
	}

	private Type	type;

	private Wizard	wizard;

	/**
	 * The attribute for this step 
	 */
	private Attribute	attribute;

	/**
	 *  Enter a description of this step that will appear to the customer, eg
	 * 'Choose your printer manufacturer'
	 */
	private String	description;

	/**
	 * The order these attributes will selected in
	 */
	private int		position;

	private WizardStep(RequestContext context) {
		super(context);
	}

	public WizardStep(RequestContext context, Wizard wizard) {
		super(context);

		this.wizard = wizard;
		this.type = Type.Attribute;
		save();
	}

	public Attribute getAttribute() {
		return (Attribute) (attribute == null ? null : attribute.pop());
	}

	/**
	 * Returns a list of applicable values for this step based on the category and attribute values passed in
	 */
	public List<String> getAttributeValues(MultiValueMap<Attribute, String> attributeValues) {

		if (attribute == null) {
			return null;
		}

		return getAttribute().getApplicableValues(getWizard().getCategory(), attributeValues);
	}

	/**
	 * Returns a list of categories applicable for items that have these attribute values
	 */
	public List<Category> getCategories(Map<Attribute, String> attributeValues) {

		if (attributeValues == null || attributeValues.isEmpty())
			return Category.getItemContainers(context);

		StringBuilder sb = new StringBuilder();
		sb.append("select distinct c.* from # c join # ci on c.id=ci.category join # i on ci.item=i.id join ( select item from # where ");

		for (int n = 0; n < attributeValues.size(); n++) {

			if (n > 0)
				sb.append(" or ");

			sb.append("(attribute=? and value=?)");
		}

		sb.append(" group by item having count(*) =? ) Z on i.id=Z.item where i.status=? order by c.name");

		Query q = new Query(context, sb);

		q.setTable(Category.class);
		q.setTable(CategoryItem.class);
		q.setTable(Item.class);
		q.setTable(AttributeValue.class);

		for (Map.Entry<Attribute, String> entry : attributeValues.entrySet()) {

			q.setParameter(entry.getKey());
			q.setParameter(entry.getValue());
		}

		q.setParameter(attributeValues.size());
		q.setParameter("Live");
		return q.execute(Category.class);
	}

	public String getDescription() {
		return description;
	}

	public int getPosition() {
		return position;
	}

	public Wizard getWizard() {
		return wizard.pop();
	}

	public boolean hasAttribute() {
		return attribute != null;
	}

	public boolean hasDescription() {
		return description != null;
	}

	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setPosition(int position) {
		this.position = position;
	}

}
