package org.sevensoft.ecreator.model.search.alerts;

import java.io.IOException;
import java.util.List;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.marketing.sms.Sms;
import org.sevensoft.ecreator.model.marketing.sms.SmsNumberException;
import org.sevensoft.ecreator.model.marketing.sms.SmsSenderException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsCreditsException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayAccountException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayMessagingException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsMessageException;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.ecreator.model.search.SearchOwner;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 5 Dec 2006 07:20:22
 * 
 * Will watch new items being created for matches and then email / sms if so
 *
 */
@Table("searches_alerts")
public class SearchAlert extends EntityObject implements SearchOwner {

	public static List<SearchAlert> get(RequestContext context) {
		return SimpleQuery.execute(context, SearchAlert.class);
	}

	/**
	 * 
	 */
	public static boolean hasAlerts(RequestContext context) {
		return SimpleQuery.count(context, SearchAlert.class) > 0;
	}

	private String	emailAddress, smsNumber;

	public SearchAlert(RequestContext context) {
		super(context);
	}

	/**
	 * Run this search alert to see if it matches the item param
	 */
	public void check(Item item) {

		// perform search for matching items
		ItemSearcher searcher = getSearch().getSearcher();
		if (searcher.contains(item)) {

			logger.fine("[SearchAlert] match found for alertid=" + this);

			if (emailAddress != null) {
				email(item);
			}

			if (smsNumber != null) {
				try {
					sms(item);
				} catch (SmsMessageException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (SmsGatewayAccountException e) {
					e.printStackTrace();
				} catch (SmsGatewayMessagingException e) {
					e.printStackTrace();
				} catch (SmsGatewayException e) {
					e.printStackTrace();
				} catch (SmsCreditsException e) {
					e.printStackTrace();
				} catch (SmsNumberException e) {
					e.printStackTrace();
				} catch (SmsSenderException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 
	 */
	private void email(Item item) {

		Config config = Config.getInstance(context);

		Email e = new Email();
		e.setTo(emailAddress);
		e.setFrom(config.getServerEmail());
		e.setSubject("Matching item found!");
		e.setBody(item.getUrl());
		logger.fine("[SearchAlert] email=" + e);

		try {

			new EmailDecorator(context, e).send(config.getSmtpHostname());

		} catch (EmailAddressException e1) {
			e1.printStackTrace();

		} catch (SmtpServerException e1) {
			e1.printStackTrace();
		}
	}

	public final String getEmailAddress() {
		return emailAddress;
	}

	public Search getSearch() {
		return Search.get(context, this);
	}

	public final String getSmsNumber() {
		return smsNumber;
	}

	public final void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public final void setSmsNumber(String smsNumber) {
		this.smsNumber = smsNumber;
	}

	private void sms(Item item) throws SmsMessageException, IOException, SmsGatewayAccountException, SmsGatewayMessagingException, SmsGatewayException,
			SmsCreditsException, SmsNumberException, SmsSenderException {

		Config config = Config.getInstance(context);

		Sms sms = new Sms(context);
		sms.setMessage("A new item called '" + item.getName() + " has been added that matches your saved search. Check out our site for full details. "
				+ config.getUrl());
		sms.setNumber(smsNumber);
		sms.setSender(Company.getInstance(context).getName());

		sms.send();

	}
}
