package org.sevensoft.ecreator.model.search.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.sorts.ItemSort;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 03-Apr-2006 11:41:56
 *
 */
public final class SearchSortMarker implements IGenericMarker {


	public Object generate(RequestContext context, Map<String, String> params) {

		SelectTag tag = new SelectTag(context, "sort");

		String itemTypeId = params.get("itemtype");
		ItemType itemType = EntityObject.getInstance(context, ItemType.class, itemTypeId);

		if (itemType == null) {
			tag.addOptions(ItemSort.get(context));
		} else {
			tag.addOptions(itemType.getSorts());
		}

		return tag.toString();

	}

	public Object getRegex() {
		return "search_sort";
	}
}