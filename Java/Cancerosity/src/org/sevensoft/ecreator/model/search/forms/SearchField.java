package org.sevensoft.ecreator.model.search.forms;

import java.util.*;

import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.SearchControl;
import org.sevensoft.ecreator.model.attributes.Attribute.DateSearchType;
import org.sevensoft.ecreator.model.attributes.renderers.search.ASelectionRenderer;
import org.sevensoft.ecreator.model.items.sorts.ItemSort;
import org.sevensoft.ecreator.model.misc.location.MajorTownsTag;
import org.sevensoft.ecreator.model.misc.location.TownsTag;
import org.sevensoft.ecreator.model.search.tags.AgeTag;
import org.sevensoft.ecreator.model.search.tags.DistanceTag;
import org.sevensoft.ecreator.model.search.tags.LastActiveTag;
import org.sevensoft.ecreator.model.search.tags.RangeTags;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.date.DateTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.commons.superstrings.StringHelper;

/**
 * @author sks 30 Aug 2006 14:01:57
 *
 */
@Table("search_forms_fields")
public class SearchField extends EntityObject implements Positionable {

	public enum DateMethod {
		DayMonthYear, DayMonth, QuickDates
	}

	/**
	 * @author sks 1 Mar 2007 19:09:59
	 * Style for displaying selection attributes
	 * 
	 */
	public enum SelectionMethod {
		DropDown, Check, Link
	}

	public static List<SearchField> get(RequestContext context, EntityObject obj) {

		Query q = new Query(context, "select * from # where " + obj.getClass().getSimpleName() + "=? order by position");
		q.setTable(SearchField.class);
		q.setParameter(obj);
		return q.execute(SearchField.class);
	}

	/**
	 * How we should render selection fields
	 */
	private SelectionMethod		selectionMethod;

	private FieldType			type;

	private int				position;

	private String			name;

	private SearchForm		searchForm;

	private Attribute			attribute;

	/**
	 * Automatically submit any selection or checkbox field
	 */
	private boolean			autoSubmit;

	/**
	 * Distance options for location searching
	 */
	private SortedSet<Integer>	distances;

	/**
	 * Ranges used for searching by number or price or age etc.
	 * should be called ranges or something now, but still priceRanges for BC**
	 */
	private List<String>		priceRanges;

	private SortedSet<Integer>	durations;

	@Deprecated
	private boolean			multiValues;

	private DateMethod		dateMethod;

	/**
	 * Width of the search box or select box
	 */
	private int				width;

	/**
	 * 
	 */
	private int				height;

	/**
	 * Is this field optional ?
	 */
	@Default("1")
	private boolean			optional;

	/**
	 * If true then we get from / to options, rather than a single option
	 */
	private DateSearchType		dateSearchType;

	/**
	 * Start and end years for selecting years on date inputs
	 */
	private int				startYear, endYear;

	/**
	 * Cols used on multi value selection types (for check boxes)
	 */
	private int				cols;

	/**
	 * The value of the 'optional' field when searching
	 */
	private String			any;

	private boolean			fixedBands;

	private String			value;

	/**
	 * Set to true for a hidden field
	 */
	private boolean			hidden;

	private int				yearRange;

	/**
	 * Control parameter 
	 */
	private SearchControl		searchControl;

    /**
     * display list of categories with padding
     */
    @Default("1")
    private boolean padding;

	private SearchField(RequestContext context) {
		super(context);
	}

	public SearchField(RequestContext context, SearchForm obj, FieldType type) {

		super(context);

		this.searchForm = obj;
		this.optional = true;

		this.type = type;
		this.name = type.name();

		this.height = 1;
		this.width = 18;

		save();
	}

	public String getAny() {
		return any == null ? "Any" : any;
	}

	public Attribute getAttribute() {
		return (Attribute) (attribute == null ? null : attribute.pop());
	}

	public int getCols() {
		return cols < 1 ? 3 : cols;
	}

	public DateMethod getDateMethod() {
		if (dateMethod == null) {
			dateMethod = DateMethod.DayMonthYear;
			save();
		}
		return dateMethod;
	}

	public final DateSearchType getDateSearchType() {
		return dateSearchType;
	}

	/**
	 * Returns a description of hte field - type + INFO (such as attribute name)
	 */
	public String getDescription() {

		String string = getFieldType().toString();
		if (attribute != null)
			string = string + ": " + getAttribute().getName();
		return string;
	}

	public SortedSet<Integer> getDistances() {
		if (distances == null)
			distances = new TreeSet();
		return distances;
	}

	public Set<Integer> getDurations() {
		if (durations == null) {
			durations = new TreeSet();
		}
		return durations;
	}

	public int getEndYear() {
		return endYear;
	}

	public FieldType getFieldType() {
		return type;
	}

	public int getHeight() {
		return height < 1 ? 1 : height;
	}

	public String getName() {
		return name;
	}

	public String getParam() {

		switch (getFieldType()) {

		case Duration:
			return "duration";

		case BookingStart:
			return "startDate";

		case MajorTowns:
			return "town";

		case Towns:
			return "town";

		case Location:
			return "location";

		case Sort:
			return "sort";

		case Id:
			return "id";

		case Age:
			return "age";

		case LastActive:
			return "lastActive";

		case Name:
			return "name";

		case Categories:
			return "searchCategory";

		case ImagesOnly:
			return "imageOnly";

		case Keywords:
			return "keywords";

		case Details:
			return "details";

		case Price:
			return "sellPriceFrom";

		case Reference:
			return "reference";

		case Attribute:

			if (attribute == null)
				return null;

			return getAttribute().getParamName();

		case Distance:
			return "distance";

        case GroupedKeywords:
            return "groupedKeywords";

        }

		return null;
	}

	public int getPosition() {
		return position;
	}

	public List<String> getRanges() {
		if (priceRanges == null)
			priceRanges = new ArrayList();
		return priceRanges;
	}

	public final SearchControl getSearchControl() {
		return searchControl;
	}

	public SearchForm getSearchForm() {
		return searchForm.pop();
	}

	public SelectionMethod getSelectionMethod() {

		if (selectionMethod == null) {

			if (multiValues) {
				selectionMethod = SelectionMethod.Check;
			} else {
				selectionMethod = SelectionMethod.DropDown;
			}
			save("selectionMethod");
		}

		return selectionMethod;
	}

	public int getStartYear() {
		return startYear;
	}

	public final String getValue() {
		return value;
	}

	public int getWidth() {
		return width < 1 ? 18 : width;
	}

	public final int getYearRange() {
		return yearRange;
	}

	public boolean hasAttribute() {
		return attribute != null;
	}

	public boolean isAttribute() {
		return getFieldType() == FieldType.Attribute;
	}

	public boolean isAutoSubmit() {
		return autoSubmit;
	}

	public boolean isFixedBands() {
		return fixedBands;
	}

	public final boolean isHidden() {
		return hidden;
	}

	/**
	 * 
	 */
	public boolean isInput() {
		return !isHidden();
	}

	public boolean isLocation() {
		return getFieldType() == FieldType.Location;
	}

	public boolean isOptional() {
		return optional;
	}

    public boolean isPadding() {
        return padding;
    }

    public Object renderPlain() {
        switch (getFieldType()) {

		default:
			return null;

		case Age:
			return context.getParameter("age");

		case Attribute:
            return StringHelper.implode(context.getParamValues(attribute.getParamName()), null);

		case Categories:
			return context.getParameter("searchCategory");

		case Details:
			return context.getParameter("details");

		case Keywords:
			return context.getParameter("keywords");

		case Distance:
			return context.getParameter("distance");

		case ImagesOnly:
			return context.getParameter("imagesOnly");

		case LastActive:
			return context.getParameter("lastActive");

		case Location:
			return context.getParameter("location");

		case Name:
			return context.getParameter("name");

        case GroupedKeywords:
			return context.getParameter("groupedKeywords");

        }
    }
    
	public Object render(boolean ajax) {
		return isHidden() ? renderHiddenField() : renderInputField(ajax);
	}

	private Object renderHiddenField() {

		switch (getFieldType()) {

		default:
			return null;

		case Age:
			return new HiddenTag("age", value);

		case Attribute:
			return new HiddenTag("attributeValues_" + attribute.getId(), value);

		case Categories:
			return new HiddenTag("searchCategory", value);

		case Details:
			return new HiddenTag("details", value);

		case Keywords:
			return new HiddenTag("keywords", value);

		case Distance:
			return new HiddenTag("distance", value);

		case ImagesOnly:
			return new HiddenTag("imagesOnly", value);

		case LastActive:
			return new HiddenTag("lastActive", value);

		case Location:
			return new HiddenTag("location", value);

		case Name:
			return new HiddenTag("name", value);

        case GroupedKeywords:
			return new HiddenTag("groupedKeywords", value);

        }
	}

	private Object renderInputField(boolean ajax) {

		switch (getFieldType()) {

		case Duration:
			SelectTag tag = new SelectTag(context, "duration");
			for (int duration : getDurations()) {
				tag.addOption(duration, duration + " nights");
			}
			return tag;

		case BookingStart:
			return new DateTag(context, "bookingStart");

		case Towns:

			TownsTag townsTag = new TownsTag(context);
			townsTag.setOptional(optional);
			townsTag.setAutoSubmit(autoSubmit);
			townsTag.setAny(getAny());
			return townsTag;

		case MajorTowns:

			MajorTownsTag majorTownsTag = new MajorTownsTag(context);
			majorTownsTag.setOptional(optional);
			majorTownsTag.setAutoSubmit(autoSubmit);
			majorTownsTag.setAny(getAny());
			return majorTownsTag;

		case Reference:
			TextTag referenceTag = new TextTag(context, "reference", getWidth());
			return referenceTag;

		case Sort:

			/*
			 * We can only render a sort if this is a search specifically set to a certain type
			 */
			List<ItemSort> sorts = null;
			if (getSearchForm().hasItemType()) {
				sorts = getSearchForm().getItemType().getSorts();
			}

			if (sorts == null || sorts.isEmpty()) {
				return null;
			}

			return new SelectTag(context, "sort", null, sorts, null);

		case Id:
			TextTag idTag = new TextTag(context, "id", 12);
			return idTag;

		case Age:

			AgeTag ageTag = new AgeTag(context, "age", getRanges());
			ageTag.setAny(getAny());
			ageTag.setOptional(isOptional());
			ageTag.setAutoSubmit();
			return ageTag;

		case Price:

			if (getRanges().size() < 2) {
				return null;
			}

			RangeTags priceTags = new RangeTags(context, getRanges(), "sellPriceFrom", "sellPriceTo");
			priceTags.setAutoSubmit(autoSubmit);
			priceTags.setFixedBands(fixedBands);
			priceTags.setOptional(optional);
			priceTags.setAny(getAny());
			return priceTags;

		case LastActive:

			return new LastActiveTag(context, any);

		case Location:

			TextTag locationTag = new TextTag(context, "location", getWidth());
			return locationTag;

		case Categories:

			SelectTag categoriesTag = new SelectTag(context, "searchCategory");
			if (optional) {
				categoriesTag.setAny(any);
			}
            categoriesTag.setWidth(width);
//            Map<String, String> categoriesMap = getSearchForm().getApplicableCategories();


            Map<String, Category> categoriesMap1 = getSearchForm().getApplicableCategoriesMap("c.fullname");
            Map<Integer, String> map2 = new LinkedHashMap<Integer, String>();

            for (Map.Entry<String, Category> entry : categoriesMap1.entrySet()) {
                Category category = entry.getValue();

                List<Category> parentsRev = category.getParents(false);
                for (Category parent : parentsRev) {
                    if (!map2.containsKey(parent.getId())) {
                        map2.put(parent.getId(), getOffset(parent.getParentCount()) + parent.getName());
                    }
                }

                map2.put(category.getId(), getOffset(category.getParentCount()) + category.getName());
            }
/*
            for (Map.Entry<String, Category> entry : categoriesMap1.entrySet()) {
                Category category = entry.getValue();

                Category parentCategory = category.getParent();
                if (parentCategory != null && !map2.containsKey(parentCategory.getId())) {
                    map2.put(parentCategory.getId(), parentCategory.getName());
                }
                if (!map2.containsKey(category.getId())) {
                    map2.put(category.getId(), " &nbsp; &nbsp; &nbsp; " + category.getName());
                }
            }
*/

            categoriesTag.addOptions(map2);
			categoriesTag.setAutoSubmit(autoSubmit);
			return categoriesTag;

		case ImagesOnly:

			BooleanRadioTag imagesOnlyTag = new BooleanRadioTag(context, "imageOnly", false);
			imagesOnlyTag.setAutoSubmit(autoSubmit);
			return imagesOnlyTag;

		case Keywords:

			TextTag keywordsTag = new TextTag(context, "keywords", getWidth());
			return keywordsTag;

		case Details:

			TextTag detailsTag = new TextTag(context, "details", getWidth());
			return detailsTag;

		case Name:

			TextTag nameTag = new TextTag(context, "name", getWidth());
			return nameTag;

		case Attribute:

			if (attribute == null) {
				return null;
			}

			ASelectionRenderer r = new ASelectionRenderer(context, getAttribute());
			r.setSelectionMethod(getSelectionMethod());
			r.setDateMethod(getDateMethod());
			r.setCols(getCols());

			r.setAjax(ajax, getSearchForm().getFormId());
			r.setAutoSubmit(autoSubmit);

			// ranges
			r.setRanges(getRanges());
			r.setFixedBands(fixedBands);

			r.setOptional(isOptional());

			r.setWidth(width);
			r.setAnyLabel(getAny());

			// date
			r.setDateSearchType(dateSearchType);
			r.setYearRange(yearRange);
			r.setStartYear(startYear);
			r.setEndYear(endYear);

			return r;

		case Distance:

			if (getDistances().isEmpty()) {
				return null;
			}

			DistanceTag distanceTag = new DistanceTag(context, getDistances());
			distanceTag.setOptional(optional);
			distanceTag.setAny(getAny());

			return distanceTag;

        case GroupedKeywords:

            SelectTag groupedKeywordsTag = new SelectTag(context, "groupedKeywords");
            if (optional) {
                groupedKeywordsTag.setAny(any);
            }
            groupedKeywordsTag.setWidth(width);

            Map<String, String> groupedKeywordsMap = SearchGroup.getMap(context);
            groupedKeywordsTag.addOptions(groupedKeywordsMap);
            groupedKeywordsTag.setAutoSubmit(autoSubmit);

            return groupedKeywordsTag;
        }

		return null;
	}

    private String getOffset(int count){
        StringBuilder sb = new StringBuilder();
        if (isPadding())
            for (int i = 0; i < count; i++) {
                sb.append("&nbsp;&nbsp;&nbsp;&nbsp;");
            }
        return sb.toString();
    }

	public void setAny(String any) {
		this.any = any;
	}

	public void setAttribute(Attribute attribute) {

		if (ObjectUtil.equal(this.attribute, attribute)) {
			return;
		}

		this.attribute = attribute;
		if (attribute != null) {
			this.name = attribute.getName();
		}
	}

	public void setAutoSubmit(boolean autoSubmit) {
		this.autoSubmit = autoSubmit;
	}

	public void setCols(int cols) {
		this.cols = cols;
	}

	public void setDateMethod(DateMethod dateMethod) {
		this.dateMethod = dateMethod;
	}

	public final void setDateSearchType(DateSearchType dateSearchType) {
		this.dateSearchType = dateSearchType;
	}

	public void setDistances(Collection<Integer> c) {
		this.distances = new TreeSet<Integer>();
		this.distances.addAll(c);
	}

	public void setDurations(Collection<Integer> c) {
		this.durations = new TreeSet();
		if (c != null) {
			this.durations.addAll(c);
		}
	}

	public void setFixedBands(boolean fixedBands) {
		this.fixedBands = fixedBands;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public final void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOptional(boolean optional) {
		this.optional = optional;
	}

    public void setPadding(boolean padding) {
        this.padding = padding;
    }

    public void setPosition(int position) {
		this.position = position;
	}

	public void setRanges(Collection<String> c) {
		this.priceRanges = new ArrayList();
		if (c != null) {
			this.priceRanges.addAll(c);
		}
	}

	public final void setSearchControl(SearchControl searchControl) {
		this.searchControl = searchControl;
	}

	public void setSelectionMethod(SelectionMethod selectionStyle) {
		this.selectionMethod = selectionStyle;
	}

	public void setType(FieldType type) {
		this.type = type;
	}

	public final void setValue(String value) {
		this.value = value;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setYearEnd(int yearEnd) {
		this.endYear = yearEnd;
	}

	public final void setYearRange(int yearRange) {
		this.yearRange = yearRange;
	}

	public void setYearStart(int yearStart) {
		this.startYear = yearStart;
	}

    public List<SearchGroup> getSearchGroup() {
        Query q = new Query(context, "select * from # where searchField=? ");
        q.setTable(SearchGroup.class);
        q.setParameter(this);
        return q.execute(SearchGroup.class);
    }

}
