package org.sevensoft.ecreator.model.media.images.galleries.markers;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGalleryImageMarker;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;

import java.util.Map;

/**
 * User: Tanya
 * Date: 29.04.2011
 */
public class PrintImageMarker implements IGalleryImageMarker {

    public Object generate(RequestContext context, Map<String, String> params, Img image) {

        ImageTag print = new ImageTag("files/js/utils/print.jpeg");
        print.setOnClick("printImage('" + image.getImagePath() + "'," + image.getWidth() + "," + image.getHeight() + ");");
        return print;
    }

    public Object getRegex() {
        return "print_image";
    }
}
