package org.sevensoft.ecreator.model.media.images.galleries.blocks;

import org.sevensoft.ecreator.iface.admin.media.galleries.GalleryBlockHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SlideshowGif;
import org.sevensoft.ecreator.iface.frontend.images.galleries.GalleryHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.media.images.galleries.*;
import org.sevensoft.ecreator.model.media.images.galleries.search.GallerySearchPanel;
import org.sevensoft.ecreator.model.media.images.galleries.blocks.markup.GalleryBlockMarkup;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.ImageOpenerTag;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;
import org.sevensoft.jeezy.http.util.Link;

import java.util.Arrays;

/**
 * @author sks 5 Nov 2006 13:43:46
 */
@Table("blocks_galleries")
@HandlerClass(GalleryBlockHandler.class)
@Label("Gallery")
public class GalleryBlock extends Block {

    private boolean showAll;

    private Gallery gallery;

    private boolean useGrid;

    private Markup markup;

    private int page;

    protected GalleryBlock(RequestContext context) {
        super(context);
    }

/*    public GalleryBlock(RequestContext context, BlockOwner owner, int objId) {
        super(context, owner, 0);

        // by default this gallery should show all albums
        this.showAll = true;
        save();
    }*/

    public GalleryBlock(RequestContext context, BlockOwner owner, Gallery gallery) {
        super(context, owner, 0);
        // by default this gallery should show all albums
        this.showAll = true;
        this.gallery = gallery;
        save();
    }

    public Gallery getGallery() {
        return (Gallery) (gallery == null ? null : gallery.pop());
    }

    public boolean isUseGrid() {
        return useGrid;
    }

    public void setUseGrid(boolean useGrid) {
        this.useGrid = useGrid;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public Markup getMarkup() {

        GalleryBlockMarkup highlightedItemsBlockMarkup = new GalleryBlockMarkup();

        if (markup == null) {
			markup = new Markup(context, getDefaultName(), new GalleryBlockMarkup());
            markup.setTableClass(highlightedItemsBlockMarkup.getCssClass());
            markup.setCycleRows(highlightedItemsBlockMarkup.getCycleRows());
            markup.save();
            save();
		}

        return markup.pop();
    }

    public void setMarkup(Markup markup) {
        this.markup = markup;
    }

    @Override
    public Object render(RequestContext context) {

/*        List<Gallery> albums = Gallery.get(context);
        if (albums.size() == 1) {

            Gallery g = albums.get(0);

            Results results = new Results(g.getApprovedImageCount(), 1, g.getResultsPerPage());
            return new GalleryGridRenderer(context, albums.get(0), results);

        } else {
            return new GalleriesRenderer(context, albums);
        }*/
        if (isUseGrid()) {
            StringBuilder sb = new StringBuilder();
            Gallery gall = getGallery();
            Results results = new Results(gall.getApprovedImageCount(), page, gall.getResultsPerPage());
            if (gall.isEnableSearchOption())
                sb.append(new GallerySearchPanel(context));
            if (gall.isEnableSortOption())
                sb.append(new GalleriesSortPanel(context, GalleryHandler.class, gall, gall.getSortType()));

//            sb.append(new ResultsControl(context, results, new Link(GalleryHandler.class, null, "gallery", gallery, "imageSortType", gall.getSortType())));
             sb.append(new ResultsControl(context, results, new Link(CategoryHandler.class, null, "category", context.getParameter("category"))));
            sb.append(new GalleryGridRenderer(context, gall, results, gall.getSortType(), getMarkup()));

            if (gall.isEnableSlideshow()) {
                ImageOpenerTag slideshow = new ImageOpenerTag(context, new Link(GallerySlideShowHandler.class, null, "gallery", gall), SlideshowGif.SRC, gall.getMaxImgWidth() + 50, gall.getMaxImgHeight() + 50);
                slideshow.setScrollbars(true);
                slideshow.setTarget("_blank");
                sb.append(slideshow);
            }
            return sb.toString();

        }  else
        return new GalleriesRenderer(context, Arrays.asList(getGallery()));
    }


}
