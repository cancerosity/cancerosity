package org.sevensoft.ecreator.model.media.images.galleries;

import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Keypad;
import org.sevensoft.jeezy.http.util.Renderer;

import java.util.List;
import java.util.logging.Logger;

/**
 * @author sks 18 Jun 2006 20:53:59
 */
public class GalleriesRenderer {

    private static final Logger logger = Logger.getLogger("ecreator");

    private final RequestContext context;
    private final List<Gallery> galleries;
    private final Keypad keypad;
    private final StringBuilder sb;

    public GalleriesRenderer(RequestContext c, List<Gallery> galleries) {

        this.context = c;
        this.galleries = galleries;
        this.sb = new StringBuilder();

        logger.fine("[GalleriesRenderer] galleries=" + galleries);

        sb.append("<style>");
        sb.append("table.ec_galleries div.ec_gallery_pic img { padding: 3px; border: 1px solid black; } ");
        sb.append("table.ec_galleries div.ec_gallery_name { margin-top: 5px; font-weight: bold; font-size: 15px;  } ");
        sb.append("table.ec_galleries div.ec_gallery_name a { color: #555555; font-family: Arial; text-decoration: none;} ");
        sb.append("table.ec_galleries div.ec_gallery_name a:hover { text-decoration: underline; } ");
        sb.append("table.ec_galleries div.ec_gallery_desc { margin-top: 5px; color: #555555; font-size: 13px; font-family: Arial; } ");
        sb.append("table.ec_galleries td { padding: 8px; } ");
        sb.append("</style>");

        keypad = new Keypad(3);
        keypad.setTableClass("ec_galleries");
        keypad.setCellVAlign("bottom");
        keypad.setTableAlign("center");
        keypad.setRenderer(new Renderer<Gallery>() {

            public Object render(Gallery g) {

                StringBuilder sb = new StringBuilder();

                if (g == null) {
                    sb.append("<h3><font color='red'>To administrator: Re-add gallery (gallery block) to this category!</font></h3>");
                    return sb;
                }

                String detailsString;
                if (g.hasApprovedImages()) {

                    Img img = g.getApprovedImage();
                    if (img != null) {

                        int width = g.getWidth();
                        int height = g.getHeight();
                        if (width == 0) {
                            width = Config.getInstance(context).getThumbnailWidth();
                        }
                        if (height == 0) {
                            height = Config.getInstance(context).getThumbnailHeight();
                        }

                        sb.append("<div class='ec_gallery_pic'>" +
                                new LinkTag(g.getFriendlyUrl(), g.getApprovedImage().getThumbnailTag(width, height)) + "</div>");

                    }

                    if (g.getImageAddedTime() == null) {

                        detailsString = g.getApprovedImageCount() + " images";

                    } else {

                        detailsString = g.getApprovedImageCount() + " images, last one added on " + g.getImageAddedTime().toString("dd-MMM-yyyy");
                    }

                } else {

                    detailsString = "0 images";
                }

                sb.append("<div class='ec_gallery_name'>" + new LinkTag(g.getFriendlyUrl(), g.getName()) + "</div>");
                sb.append("<div class='ec_gallery_details'>" + detailsString + "</div>");

                if (g.hasDescription()) {
                    sb.append("<div class='ec_gallery_desc'>" + g.getDescription() + "</div>");
                }

                return sb;
            }
        });
        keypad.setObjects(galleries);

        sb.append(keypad);
    }

    @Override
    public String toString() {
        return sb.toString();
    }
}
