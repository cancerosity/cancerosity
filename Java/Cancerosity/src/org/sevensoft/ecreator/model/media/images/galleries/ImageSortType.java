package org.sevensoft.ecreator.model.media.images.galleries;

import org.sevensoft.jeezy.http.util.Selectable;

/**
 * Created by IntelliJ IDEA.
 * User: MeleshkoDN
 * Date: 03.09.2007
 * Time: 19:47:27
 * To change this template use File | Settings | File Templates.
 */
public enum ImageSortType implements Selectable {

    NAME_ASC("caption"), NAME_DESC("caption"),
    DESCRIPTION_ASC("description"), DESCRIPTION_DESC("description"),
    DATE_ASC("date"), DATE_DESC("date"),
    SIZE_ASC("width"), SIZE_DESC("width"),;


    private final String column;


    ImageSortType(String column) {
        this.column = column;
    }

    public String getColumn() {
        return column;
    }

    public String getOrder() {
        return toString().split("_")[1];
    }


    public String getLabel() {
        return "by " + getColumn() + " (" + getOrder().toLowerCase() + ")";
    }

    public String getValue() {
        return toString();
    }

    public static ImageSortType getInverse(ImageSortType sortType) {
        switch (sortType) {
            case DATE_ASC:
                return DATE_DESC;
            case DATE_DESC:
                return DATE_ASC;
            case NAME_ASC:
                return NAME_DESC;
            case NAME_DESC:
                return NAME_ASC;
            case DESCRIPTION_ASC:
                return DESCRIPTION_DESC;
            case DESCRIPTION_DESC:
                return DESCRIPTION_ASC;
            case SIZE_ASC:
                return SIZE_DESC;
            case SIZE_DESC:
                return SIZE_ASC;
            default:
                throw new IllegalArgumentException();
        }
    }
}
