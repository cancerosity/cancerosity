package org.sevensoft.ecreator.model.media.images.galleries;

import org.sevensoft.ecreator.model.media.images.ImageSupport;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.ecreator.model.misc.seo.Meta;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.QueryBuilder;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

import java.util.List;

/**
 * @author sks 25-Oct-2005 13:30:23
 */
@Table("galleries")
public class Gallery extends ImageSupport implements Selectable, Meta {

    public enum Style {
        Grid;
    }

    public static List<Gallery> get(RequestContext context) {
        Query q = new Query(context, "select * from # order by name");
        q.setTable(Gallery.class);
        return q.execute(Gallery.class);
    }

    /*
      * For grid style, the rows and cols per page. rows * cols are the number of elements that will be rendered per page.
      */
    private int cols, rows;

    /**
     * A name for this gallery
     */
    private String name;

    private boolean showDateUploaded;

    /**
     *
     */
    private Style style;

    /**
     * A description of this album
     */
    private String description;

    /**
     * Dimensions of the thumbnails in grid mode
     */
    private int width, height;

    private String friendlyUrl;

    private boolean printable;

    private ImageSortType sortType;

    private boolean enableSlideshow;

    private int slideshowInterval;

    private boolean enableSortOption;

    @Default("1")
    private boolean enableSearchOption;

    private String descriptionTag;

    private String keywords;

    private String titleTag;

    public Gallery(RequestContext context) {
        super(context);
    }

    public Gallery(RequestContext context, String name) {
        super(context);

        this.name = name;
        this.style = Style.Grid;
        this.cols = 3;
        this.rows = 4;

        this.width = 160;
        this.height = 120;

        this.showDateUploaded = true;
        this.sortType = ImageSortType.DATE_DESC;
        save();
    }

    private void doFriendlyUrl() {

        friendlyUrl = Seo.getFriendlyUrl(getName()) + "-g" + getIdString() + ".html";
        save("friendlyUrl");
    }

    public int getCols() {
        return cols < 1 ? 4 : cols;
    }

    public final String getDescription() {
        return description;
    }

    public String getDescriptionTag() {
        return descriptionTag;
    }

    public final String getFriendlyUrl() {

        if (friendlyUrl == null) {
            doFriendlyUrl();
        }
        return friendlyUrl;
    }

    public String getKeywords() {
        return keywords;
    }

    public int getHeight() {
        return height;
    }

    public int getImageLimit() {
        return 0;
    }

    public String getLabel() {
        return getName();
    }

    public String getName() {
        return name;
    }

    public String getTitleTag() {
        return titleTag;
    }

    public boolean hasDescriptionTag() {
        return descriptionTag != null;
    }

    public boolean hasKeywords() {
        return keywords != null;
    }

    public boolean hasTitleTag() {
        return titleTag != null;
    }

    public void setDescriptionTag(String descriptionTag) {
        this.descriptionTag = descriptionTag;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public void setTitleTag(String titleTag) {
        this.titleTag = titleTag;   
    }

    public int getPages() {
        return getApprovedImageCount() / (getRows() * getCols());
    }

    public String getRelativeUrl() {
        return null;
    }

    /**
     *
     */
    public int getResultsPerPage() {
        return cols * rows;
    }

    public int getRows() {
        return rows < 1 ? 3 : rows;
    }

    public Style getStyle() {
        return style == null ? Style.Grid : style;
    }

    public String getUrl() {
        return Config.getInstance(context).getUrl() + "/" + getFriendlyUrl();
    }

    public String getValue() {
        return getIdString();
    }

    public int getWidth() {
        return width;
    }

    public boolean hasDescription() {
        return description != null;
    }

    public boolean isGrid() {
        return style == Style.Grid;
    }

    public final boolean isShowDateUploaded() {
        return showDateUploaded;
    }

    public boolean isSlideshow() {
        return false;//style == Style.Slideshow;
    }

    public void setCols(int cols) {
        this.cols = cols;
    }

    public final void setDescription(String description) {
        this.description = description;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public final void setShowDateUploaded(boolean showDateUploaded) {
        this.showDateUploaded = showDateUploaded;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public boolean useImageDescriptions() {
        return true;
    }

    public boolean isPrintable() {
        return printable;
    }

    public void setPrintable(boolean printable) {
        this.printable = printable;
    }

    public ImageSortType getSortType() {
        return sortType == null ? ImageSortType.DATE_DESC : sortType;
    }

    public void setSortType(ImageSortType sortType) {
        this.sortType = sortType;
    }

    public boolean isEnableSlideshow() {
        return enableSlideshow;
    }

    public void setEnableSlideshow(boolean enableSlideshow) {
        this.enableSlideshow = enableSlideshow;
    }

    public int getSlideshowInterval() {
        return slideshowInterval;
    }

    public void setSlideshowInterval(int slideshowInterval) {
        this.slideshowInterval = slideshowInterval;
    }

    public boolean isEnableSortOption() {
        return enableSortOption;
    }

    public void setEnableSortOption(boolean enableSortOption) {
        this.enableSortOption = enableSortOption;
    }

    public boolean isEnableSearchOption() {
        return enableSearchOption;
    }

    public void setEnableSearchOption(boolean enableSearchOption) {
        this.enableSearchOption = enableSearchOption;
    }

    public int getMaxImgWidth() {
        QueryBuilder q = new QueryBuilder(context);
        q.select("MAX(width)");
        q.from("#", Img.class);
        q.clause("gallery=?", this);
        q.clause("approved=?", 1);
        return q.toQuery().getInt();
    }

    public int getMaxImgHeight() {
        QueryBuilder q = new QueryBuilder(context);
        q.select("MAX(height)");
        q.from("#", Img.class);
        q.clause("gallery=?", this);
        q.clause("approved=?", 1);
        return q.toQuery().getInt();
    }

    public List<Img> getApprovedImages(ImageSortType sortType) {
        QueryBuilder q = new QueryBuilder(context);
        q.select("*");
        q.from("#", Img.class);
        q.clause("gallery=?", this);
        q.clause("approved=?", 1);
        q.order(" " + sortType.getColumn() + " " + sortType.getOrder());
        return q.toQuery().execute(Img.class);
    }

}