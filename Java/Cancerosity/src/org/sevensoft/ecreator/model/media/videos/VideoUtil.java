package org.sevensoft.ecreator.model.media.videos;

import java.io.IOException;
import java.util.List;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;

/**
 * @author sks 14 Feb 2007 12:48:26
 *
 */
public class VideoUtil {

	public static void save(RequestContext context, VideoOwner owner, List<Upload> videoUploads) {

		if (videoUploads != null) {

			for (Upload upload : videoUploads) {

				try {
					owner.addVideo(upload);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

}
