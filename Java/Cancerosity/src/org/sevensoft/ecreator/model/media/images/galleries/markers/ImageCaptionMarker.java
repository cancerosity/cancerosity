package org.sevensoft.ecreator.model.media.images.galleries.markers;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGalleryImageMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;
import java.util.List;

/**
 * User: Tanya
 * Date: 29.04.2011
 */
public class ImageCaptionMarker extends MarkerHelper implements IGalleryImageMarker, IItemMarker {

    public Object generate(RequestContext context, Map<String, String> params, Img image) {
        return super.string(context, params, image.hasCaption() ? image.getCaption() : image.getFilename());
    }

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
        if (item.hasApprovedImages()) {

            if (params.containsKey("limit")) {
                return generate(context, params, item.getApprovedImage());
            } else {
                List<Img> list = item.getApprovedImages();
                StringBuilder path = new StringBuilder();
                for (int i = 0; i < list.size(); i++) {
                    path.append(generate(context, params, list.get(i)));
                    if (i < list.size() - 1)
                        path.append(", ");
                }

                return path.toString();
            }
        }
        return null;
    }

    public Object getRegex() {
        return "image_caption";
    }

}
