package org.sevensoft.ecreator.model.media.images;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.ecreator.model.accounts.registration.RegistrationSession;
import org.sevensoft.ecreator.model.advertising.banners.Banner;
import org.sevensoft.ecreator.model.attributes.AttributeOption;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.listings.sessions.ListingSession;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.ecreator.model.media.images.boxes.ImageBox;
import org.sevensoft.ecreator.model.media.images.galleries.Gallery;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.model.extras.facebook.FacebookOpenGraphTags;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.ImageTag;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.List;
import java.util.Iterator;

/**
 * @author sks 25-Jul-2004 18:49:55
 */
@Table("images")
public class Img extends EntityObject implements Positionable, Comparable<Img> {

    public static BufferedImage autocrop(BufferedImage image) {

        logger.fine("auto cropping image whitesize, image=" + image);

        /*
           * Find vertical drop
           *
           */
        int t = 0, b = image.getHeight() - 1, l = 0, r = image.getWidth() - 1;

        while (checkHorizontal(image, t)) {
            t++;
        }
        while (checkHorizontal(image, b)) {
            b--;
        }
        while (checkVertical(image, l)) {
            l++;
        }
        while (checkVertical(image, r)) {
            r--;
        }

        logger.fine("new dimensions: x=" + l + "y=" + t + "w=" + (r - l) + "h=" + (b - t));

        //no need to resize as image less then thumb should be
        if (b == -1 || t == -1)
            return image;

        return image.getSubimage(l, t, r - l, b - t);
    }

    private static boolean checkHorizontal(BufferedImage image, int y) {

        for (int x = 0; x < image.getWidth(); x++) {

            try {
                int c = image.getRGB(x, y);
                if (c != -1)
                    return false;
            } catch (ArrayIndexOutOfBoundsException e) {
                logger.warning(image + " " + e);
                return false;
            } catch (RuntimeException e) {
            }

        }

        return true;
    }

    private static boolean checkVertical(BufferedImage image, int x) {

        for (int y = 0; y < image.getHeight(); y++) {

            try {
                int c = image.getRGB(x, y);
                if (c != -1)
                    return false;
            } catch (ArrayIndexOutOfBoundsException e) {
                logger.warning(image + " " + e);
                return false;
            } catch (RuntimeException e) {
            }

        }

        return true;
    }

    public static String download(RequestContext context, String imageUrl) throws FileNotFoundException, IOException {
        return download(context, imageUrl, ResourcesUtils.getRealImagesPath());
    }
    /**
     * Returns the local filename of the downloaded remote file.
     */

    public static String download(RequestContext context, String imageUrl, String imagesDir) throws FileNotFoundException, IOException {
        String fn = null;
        try {
            BufferedImage image = ImageIO.read(new URL(imageUrl));


            String filename = imageUrl;
            // get last filename bit
            filename = filename.replaceAll(".*/", "");
            filename = filename.replaceAll(".*\\\\", "");
            // replace spaces with underscore
            filename = filename.replaceAll("\\s", "_");

            // if no extension then do not save.
            if (!filename.contains(".")) {
                throw new IOException("invalid remote filename - does not contain . so skipping for security concerns");
            }

            // replace file extension with .jpg
            filename = filename.substring(0, filename.lastIndexOf('.')) + ".jpg";

            fn = SimpleFile.getUniqueFilename(filename, imagesDir);
            if (fn == null) {
                throw new IOException("could not write unique filename");
            }

            ImageIO.write(image, "jpg", new FileOutputStream(imagesDir + "/" + fn));
        } catch (IllegalArgumentException e) {
            logger.warning("Downloading image: " + imageUrl + "  " + e);

        }
        return fn;
    }

    public static List<Img> get(RequestContext context) {
        return SimpleQuery.execute(context, Img.class);
    }

    public static List<Img> get(RequestContext context, int start, int limit) {
        Query q = new Query(context, "select * from # order by id");
        q.setTable(Img.class);
        return q.execute(Img.class, start, limit);
    }

    public static List<Img> getByFilename(RequestContext context, String filename) {
        return SimpleQuery.execute(context, Img.class, "filename", filename);
    }

    /**
     *
     */
    public static int[] getDimensionsToCover(int targetWidth, int targetHeight, Img image) {
        return getDimensionsToCover(targetWidth, targetHeight, image.getWidth(), image.getHeight());
    }

    /**
     * Returns the dimensions to scale the source image to in order for as much as possible of the image to be displayed, yet still covering the entire
     * target bound without tiling. The returned dimensions may lend to cropping of the x or y. Aspect ratio will be constant.
     */
    public static int[] getDimensionsToCover(int targetWidth, int targetHeight, int srcWidth, int srcHeight) {

        if (targetHeight == 0) {
            targetHeight = srcHeight;
        }

        if (targetWidth == 0) {
            targetWidth = srcWidth;
        }

        double targetAspect = (double) targetWidth / (double) targetHeight;
        double srcAspect = (double) srcWidth / (double) srcHeight;

        double scaleFactor;

        // if the src aspect is larger than hte target aspect then we want to scale x
        if (srcAspect > targetAspect) {

            scaleFactor = (double) srcWidth / (double) targetWidth;

        } else {

            scaleFactor = (double) srcHeight / (double) targetHeight;
        }

        return new int[]{(int) (srcWidth / scaleFactor), (int) (srcHeight / scaleFactor)};
    }

    public static int[] getDimensionsToFit(int targetWidth, int targetHeight, Img image, boolean useThumbnail) {

        if (image.hasThumbail() && useThumbnail) {
            return getDimensionsToFit(targetWidth, targetHeight, image.getThumbnailWidth(), image.getThumbnailHeight());
        } else {
            return getDimensionsToFit(targetWidth, targetHeight, image.getWidth(), image.getHeight());
        }
    }

    /**
     * Returns the dimensions that will enable the source image to fit inside the target bounding box in as big as possible.
     */
    public static int[] getDimensionsToFit(int targetWidth, int targetHeight, int sourceWidth, int sourceHeight) {

        // if target height is zero then we don't care how high the image is, so use the src width
        if (targetHeight == 0) {
            targetHeight = sourceHeight;
        }

        // if target width is zero then we don't care how wide it is, so use the src width
        if (targetWidth == 0) {
            targetWidth = sourceWidth;
        }

        double w = sourceWidth, h = sourceHeight;

        if (sourceWidth > targetWidth) {

            w = targetWidth;

            double reduction = targetWidth / (double) sourceWidth;
            h = sourceHeight * reduction;
        }

        if (h > targetHeight) {

            h = targetHeight;

            double reduction = targetHeight / (double) sourceHeight;
            w = sourceWidth * reduction;
        }

        return new int[]{(int) w, (int) h};
    }

    public static int getImageCount(File imagesRootDir) {
        return getImageFilenames(imagesRootDir, null).length;
    }

    public static String[] getImageFilenames(File imagesRootDir, final String phrase) {

        return imagesRootDir.list(new FilenameFilter() {

            public boolean accept(File dir, String name) {
                name = name.toLowerCase();
                return (phrase == null || name.startsWith(phrase.toLowerCase()))
                        && (name.endsWith(".jpg") || name.endsWith(".gif") || name.endsWith(".png") || name.endsWith(".jpeg"));
            }
        });
    }

    public static List<Img> getModeration(RequestContext context) {
        return SimpleQuery.execute(context, Img.class, "approved", 0);
    }

    public static boolean hasImages(RequestContext context) {

        File imageRoot = ResourcesUtils.getRealImagesDir();
        if (imageRoot.exists()) {

            File[] filenames = imageRoot.listFiles();
            if (filenames != null && filenames.length > 0)
                return true;
        }

        return false;
    }

    private static BufferedImage pad(BufferedImage source, int tw, int th) {

        // source dimensions
        int sw = source.getWidth();
        int sh = source.getHeight();

        // if source is larger than target we cannot pad it so return source
        if (sw > tw || sh > th)
            return source;

        // create new buffered image to hold the larger padded image
        BufferedImage target = new BufferedImage(tw, th, BufferedImage.TYPE_INT_RGB);

        // get graphics for target image
        Graphics2D g = target.createGraphics();

        // fill background with bg colour (defaults to white)
        g.setColor(Color.white);
        g.fillRect(0, 0, tw, th);

        // get co-ords for drawing image in centre
        int x = (tw - sw) / 2;
        int y = (th - sh) / 2;

        // draw source in centre of target
        g.drawImage(source, x, y, null);

        return target;
    }

    public static void regenerateImages(RequestContext context) {
        Config config = Config.getInstance(context);
        if (!config.hasImageConstraints())
            return;

        List<Img> images = Img.get(context);
        for (Img image : images) {
            try {
                image.resize(images.size() < 1000);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void regenerateThumbnails(RequestContext context) {

        for (Img image : Img.get(context)) {
            try {
                image.generateThumbnail(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Removes all images except the first one
     */
    public static void removeMultipleImages(RequestContext context) {

        Query q = new Query(context, "select * from # where imageCount>1");
        q.setTable(Item.class);
        for (Item item : q.execute(Item.class)) {

            List<Img> images = item.getApprovedImages();
            for (int n = 1; n < images.size(); n++)
                item.removeImage(images.get(n));

        }
    }

    private static BufferedImage resize(BufferedImage source, int tw, int th, boolean smoothing) {

        logger.fine("[Img] resizing image, source=" + source + ", tw=" + tw + ", th=" + th + ", smoothing=" + smoothing);

        // source dimensions
        int sw = source.getWidth();
        int sh = source.getHeight();

        int[] d = getDimensionsToFit(tw, th, sw, sh);

        logger.fine("[Img] target dimensions w=" + d[0] + " h=" + d[1]);

        // 	 create new buffered image to hold the resized image
        BufferedImage target = new BufferedImage(d[0], d[1], BufferedImage.TYPE_INT_RGB);

        //		 get graphics for target image
        Graphics2D g = target.createGraphics();

        // fill background with bg colour (defaults to white)
        g.setColor(Color.white);
        g.fillRect(0, 0, d[0], d[1]);

        /*
           * Resized image
           */
        Image scaled;
        // only perform smooth resize if smoothing param is set
        if (smoothing) {
            scaled = source.getScaledInstance(d[0], d[1], java.awt.Image.SCALE_AREA_AVERAGING);
        } else {
            scaled = source.getScaledInstance(d[0], d[1], java.awt.Image.SCALE_DEFAULT);
        }

        g.drawImage(scaled, 0, 0, null);

        return target;
    }

    private ImageType imageType;

    /**
     * The fullsize filename relative to images directory
     */
    private String filename;

    private User user;

    /**
     * True if this image is moderated
     */
    @Default("1")
    private boolean approved;

    /**
     *
     */
    private String thumbnail;

    private Banner banner;

    /**
     * A description of this image
     */
    private String description;

    /**
     * The date this image holder was created on the system
     */
    private DateTime date;

    /**
     *
     */
    private int position, width, height;

    private Gallery gallery;

    /**
     * Original url downloaded from - so we know to re-use rather than duplicating
     */
    private String url;

    @Index()
    private Item item;

    /**
     * The owning category
     */
    @Index()
    private Category category;

    /**
     *
     */
    private int thumbnailHeight;

    /**
     *
     */
    private int thumbnailWidth;

    /**
     *
     */
    @Index()
    private ListingSession listingSession;

    /**
     *
     */
    @Index()
    private ItemOptionSelection itemOptionSelection;

    /**
     *
     */
    @Index()
    private RegistrationSession registrationSession;

    /**
     *
     */
    @Index()
    private AttributeOption attributeOption;

    private String alt;

    private String name;

    private ImageBox imageBox;

    private String caption;

    private FacebookOpenGraphTags facebookOpenGraphTags;

    public Img(RequestContext context) {
        super(context);
    }

    /**
     * Save this image object out as a file and then tag to this owner
     *
     * @param autoApprove
     */
    public Img(RequestContext context, ImageOwner owner, BufferedImage awtImage, String filename, boolean useThumb, boolean smoothing, boolean autoApprove)
            throws IOException, ImageLimitException {

        super(context);

        // write out image to the filename param
        ImageIO.write(awtImage, "jpeg", new FileOutputStream(ResourcesUtils.getRealImagesPath() + File.separator + filename));

        init(context, owner, filename, useThumb, smoothing, autoApprove);
    }

    public Img(RequestContext context, ImageOwner owner, File file, boolean useThumb, boolean smoothing, boolean autoApprove) throws FileNotFoundException,
            ImageLimitException, IOException {
        super(context);
        init(context, owner, file.getAbsolutePath(), useThumb, smoothing, autoApprove);
    }

    /**
     * Copy this existing image to this owner
     *
     * @param autoApprove
     */
    public Img(RequestContext context, ImageOwner owner, Img image, boolean useThumb, boolean autoApprove) throws IOException, ImageLimitException {
        this(context, owner, image.getFilename(), useThumb, true, autoApprove, image.getCaption());
    }

    /**
     * Add the image located at the filename
     *
     * @param autoApprove
     */
    public Img(RequestContext context, ImageOwner owner, String filename, boolean useThumb, boolean smoothing, boolean autoApprove)
            throws FileNotFoundException, IOException, ImageLimitException {

        super(context);
        init(context, owner, filename, useThumb, smoothing, autoApprove);
    }

    public Img(RequestContext context, ImageOwner owner, String filename, boolean useThumb, boolean smoothing, boolean autoApprove, String caption)
            throws FileNotFoundException, IOException, ImageLimitException {

        this(context, owner, filename, useThumb, smoothing, autoApprove);
        setCaption(caption);
    }

    /**
     * Get the image at the specified upload and then write it out to a proper file and add to the owner
     *
     * @param autoApprove
     */
    public Img(RequestContext context, ImageOwner owner, Upload upload, boolean useThumb, boolean smoothing, boolean autoApprove) throws IOException,
            ImageLimitException {

        super(context);

        String filename = upload.writeUnique(ResourcesUtils.getRealImagesPath());
        init(context, owner, filename, useThumb, smoothing, autoApprove);
    }

    /**
     * Download the image located at the URL param and write it out to a unique filename, and then add it to the owner
     *
     * @param autoApprove
     */
    public Img(RequestContext context, ImageOwner owner, URL url, boolean useThumb, boolean smoothing, boolean autoApprove) throws IOException,
            ImageLimitException {

        super(context);

        /*
           * Check if we have downloaded this image before
           */
        Img img = SimpleQuery.get(context, Img.class, "url", url.toString());
        if (img != null) {

            init(context, owner, img.getFilename(), useThumb, smoothing, autoApprove);

        } else {

            String filename = Img.download(context, url.toExternalForm());
            this.url = url.toString();

            init(context, owner, filename, useThumb, smoothing, autoApprove);
        }
    }

    public void approve() {

        this.approved = true;
        save();

        getOwner().setImageInfo();
        getOwner().imageApproved(this);
    }

    private BufferedImage checkSize(boolean smoothing, File file) throws IOException {

        // load image for dimensions
        BufferedImage image = readToRGB(file);

        if (image == null)
            throw new IOException("Invalid image");

        /*
           * Resize image if over max sizes
           */
        Config config = Config.getInstance(context);
        if (config.hasImageConstraints()) {

            if (image.getWidth() > config.getMaxImageWidth() || image.getHeight() > config.getMaxImageHeight()) {

                // resize image
                image = resize(image, config.getMaxImageWidth(), config.getMaxImageHeight(), smoothing);

                // save resized image back out
                ImageIO.write(image, SimpleFile.getExtension(file), file);
            }
        }
        return image;
    }

    @Override
    public synchronized boolean delete() {

        super.delete();

        try {

            final ImageOwner owner = getOwner();
            owner.setImageInfo();

        } catch (RuntimeException e) {
            e.printStackTrace();
        }

        return true;
    }

    private boolean exists() {
        return ResourcesUtils.getRealImage(filename).exists();
    }

    private void generateThumbnail(boolean smoothing) throws IOException {
        generateThumbnail(smoothing, filename);
    }

    private void generateThumbnail(boolean smoothing, String thumbnailFilename) throws IOException {

        Config config = Config.getInstance(context);

        try {

            // load full size file
            File realImage = ResourcesUtils.getRealImage(filename);
            if (!realImage.exists() || realImage.length() == 0) {
                logger.fine("No such file or its length=0: " + realImage.toString());
                return;
            }
            
            BufferedImage source = readToRGB(realImage);

            // crop image down to white space
            BufferedImage t = autocrop(source);

            // resize
            t = resize(t, config.getThumbnailWidth(), config.getThumbnailHeight(), smoothing);

            // pad out if set in config
            if (config.isThumbnailPadding()) {
                t = pad(t, config.getThumbnailWidth(), config.getThumbnailHeight());
            }

            /*
                * write out
                */
            ImageIO.write(t, "jpeg", getThumbnailFile(thumbnailFilename));

            thumbnail = getThumbnailFile(thumbnailFilename).getName();
            thumbnailWidth = t.getWidth();
            thumbnailHeight = t.getHeight();
            save();

        } catch (IOException e) {

            e.printStackTrace();

            removeThumbnail();
        }
    }

    private BufferedImage readToRGB(File file) throws IOException{
        BufferedImage source;
        try {
            source = ImageIO.read(file);
        } catch (IllegalArgumentException e) {
            source = readPNG(file);
        } catch (IOException e) {
            source = readPNG(file);
        }
        return source;
    }

    private BufferedImage readPNG(File file) throws IOException {
        BufferedImage source;
        ImageInputStream input = ImageIO
                .createImageInputStream(file);
        Iterator<ImageReader> readers = ImageIO.getImageReaders(input);
        ImageReader reader = null;
        while (readers.hasNext()) {
            reader = (ImageReader) readers.next();
            if (reader.canReadRaster())
                break;
        }

        if (reader == null)
            throw new IOException("no reader found");
        // Set the input.
        reader.setInput(input);
        int w = reader.getWidth(0);
        int h = reader.getHeight(0);
        source = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        Image intImage = Toolkit.getDefaultToolkit().createImage(file.getAbsolutePath());
        new ImageIcon(intImage);
        Graphics2D g = source.createGraphics();
        g.drawImage(intImage, 0, 0, null);
        g.dispose();
        return source;
    }

    public String getAlt() {
        return alt;
    }

    public final AttributeOption getAttributeOption() {
        return attributeOption.pop();
    }

    private ImageOwner getBanner() {
        return (ImageOwner) (banner == null ? null : banner.pop());
    }

    public int getBoundedHeight(int width, int height) {

        if (width > width || height > height) {

            final double ratio = (double) width / (double) height;

            if (ratio < 1.3333333333333)
                return 120;

            else
                return (int) (160d * ratio);
        }

        return height;
    }

    public int getBoundedWidth(int width, int height) {

        if (width > width || height > height) {

            final double ratio = (double) width / (double) height;

            if (ratio < 1.3333333333333)
                return (int) (120d * ratio);

            else
                return 160;

        }

        return width;
    }

    public final String getCaption() {
        return caption;
    }

    public Category getCategory() {
        return category.pop();
    }

    public DateTime getDate() {
        if (date == null) {
            date = new DateTime();
            save();
        }
        return date;
    }

    public String getDescription() {
        return description;
    }

    private File getFile() {
        return ResourcesUtils.getRealImage(filename);
    }

    public String getFilename() {
        return filename;
    }

    public ImageTag getFullsizeImageTag(int maxWidth, int maxHeight) {
        int[] d = Img.getDimensionsToFit(maxWidth, maxHeight, width, height);
        return new ImageTag(getPath(), 0, d[0], d[1]);
    }

    public Gallery getGallery() {
        return (Gallery) (gallery == null ? null : gallery.pop());
    }

    public int getHeight() {
        return height;
    }

    /**
     * Returns an AWT image object for the full size image represented by this.
     */
    public BufferedImage getImage() throws IOException {
        File image = ResourcesUtils.getRealImage(filename);
        logger.fine("[Image] returning real file=" + image);
        return ImageIO.read(image);
    }

    public final ImageBox getImageBox() {
        return (ImageBox) (imageBox == null ? null : imageBox.pop());
    }

    public String getImagePath() {
        return getPath();
    }

    public String getFullImagePath() {
        return Config.getInstance(context).getUrl() + "/" + Config.getInstance(context).getImagesPath() + "/" + filename;
    }

    public ImageTag getImageTag() {
        return new ImageTag(getPath());
    }

    /**
     * Returns an Img Tag for this Img which is bounded by the dimensions
     */
    public ImageTag getImageTag(int maxWidth, int maxHeight) {

        int[] d = Img.getDimensionsToFit(maxWidth, maxHeight, this, true);
        return new ImageTag(getThumbnailPath(), 0, d[0], d[1]);
    }

    public final ImageType getImageType() {
        if (imageType == null) {
            imageType = getItem().getItemType().getMediaModule().getImageTypes().get(0);
            save("imageType");
        }
        return imageType.pop();
    }

    public Item getItem() {
        return (Item) (item == null ? null : item.pop());
    }

    public ItemOptionSelection getItemOptionSelection() {
        return (ItemOptionSelection) (itemOptionSelection == null ? null : itemOptionSelection.pop());
    }

    public final ListingSession getListingSession() {
        return listingSession;
    }

    public String getName() {
        if (name == null)
            return getFilename();
        return name;
    }

    public ImageOwner getOwner() {

        if (hasItem()) {
            return getItem();
        }

        if (hasGallery()) {
            return getGallery();
        }

        if (hasCategory()) {
            return getCategory();
        }

        if (hasListingProcess()) {
            return getListingSession();
        }
        if (hasRegistrationSession()) {
            return getRegistrationSession();
        }

        if (hasAttributeOption()) {
            return getAttributeOption();
        }

        if (hasItemOptionSelection()) {
            return getItemOptionSelection();
        }

        if (hasUser()) {
            return getUser();
        }

        if (hasBanner()) {
            return getBanner();
        }

        if (hasImageBox()) {
            return getImageBox();
        }

        if (hasFacebookOpenGraphTags()) {
            return getFacebookOpenGraphTags();
        }

        throw new RuntimeException("Unrecognised type of image owner: " + this);
    }

    public String getPath() {
        return context.getContextPath() + "/" + Config.getInstance(context).getImagesPath() + "/" + filename;

    }

    public int getPosition() {
        return position;
    }

    /*private File getRealThumbnailFile() {
        return context.getRealFile(Config.ThumbnailsPath + "/" + filename);
    }

    private String getRealThumbnailPath() {
        return context.getRealPath(Config.ThumbnailsPath + "/" + filename);
    }*/

    public final RegistrationSession getRegistrationSession() {
        return registrationSession.pop();
    }

    private File getThumbnailFile() {

        return getThumbnailFile(filename);
    }

    private File getThumbnailFile(String filename) {

        /*
           * our thumbnails will always be jpegs
           */

        String thumbFilename = getThumbnailFileName(filename);

        return ResourcesUtils.getRealThumbnail(thumbFilename);
    }

    private String getThumbnailFileName(String filename) {
        int dot = filename.lastIndexOf('.');
        String thumbFilename = filename.substring(0, dot) + ".jpg";
        return thumbFilename;
    }

    public String getThumbnailFilename() {
        return thumbnail;
    }

    public final int getThumbnailHeight() {

        if (thumbnailHeight == 0 && hasThumbail()) {
            setThumbnailDimensions();
        }

        return thumbnailHeight;
    }

    public String getThumbnailPath() {

        StringBuilder sb = new StringBuilder();
        sb.append(context.getContextPath());
        sb.append("/");

        if (hasThumbail()) {
            sb.append(Config.getInstance(context).getThumbnailsPath() + "/" + thumbnail);
        } else {
            sb.append(Config.getInstance(context).getImagesPath() + "/" + filename);
        }

        return sb.toString();

    }

    public ImageTag getThumbnailTag() {
        return new ImageTag(getThumbnailPath());
    }

    public ImageTag getThumbnailTag(int i, int j) {

        int[] d = getDimensionsToFit(i, j, getThumbnailWidth(), getThumbnailHeight());
        return new ImageTag(getThumbnailPath(), 0, d[0], d[1]);
    }

    public final int getThumbnailWidth() {

        if (thumbnailWidth == 0 && hasThumbail()) {
            setThumbnailDimensions();
        }

        return thumbnailWidth;
    }

    /**
     * Returns the full url to this image's full size
     */
    public String getUrl() {
        return Config.getInstance(context).getUrl() + "/" + Config.getInstance(context).getImagesPath() + "/" + filename;
    }

    private ImageOwner getUser() {
        return (ImageOwner) (user == null ? null : user.pop());
    }

    public int getWidth() {
        return width;
    }

    public boolean hasAttributeOption() {
        return attributeOption != null;
    }

    private boolean hasBanner() {
        return banner != null;
    }

    public boolean hasCaption() {
        return caption != null;
    }

    private boolean hasCategory() {
        return category != null;
    }

    public boolean hasDescription() {
        return description != null;
    }

    public boolean hasGallery() {
        return gallery != null;
    }

    private boolean hasImageBox() {
        return imageBox != null;
    }

    public boolean hasItem() {
        return item != null;
    }

    private boolean hasItemOptionSelection() {
        return itemOptionSelection != null;
    }

    private boolean hasListingProcess() {
        return listingSession != null;
    }

    public boolean hasRegistrationSession() {
        return registrationSession != null;
    }

    public boolean hasThumbail() {
        return thumbnail != null;
    }

    /**
     *
     */
    private boolean hasUser() {
        return user != null;
    }

    public boolean hasFacebookOpenGraphTags() {
        return facebookOpenGraphTags!=null;
    }

    public FacebookOpenGraphTags getFacebookOpenGraphTags() {
        return facebookOpenGraphTags!=null? (FacebookOpenGraphTags) facebookOpenGraphTags.pop() : null;
    }

    private void init(RequestContext context, ImageOwner owner, String filename, boolean useThumb, boolean smoothing, boolean autoApprove)
            throws ImageLimitException, FileNotFoundException, IOException {

        if (owner.isAtImageLimit()) {
            throw new ImageLimitException("You have reached your image limit");
        }

        if (filename == null || filename.length() < 5)
            return;

//        String imagesPath = Config.getInstance(context).getRealImagesPath();

        // check that file exists
        File file = ResourcesUtils.getRealImage(filename);

        if (!file.exists())
            throw new FileNotFoundException("File not found: " + filename);

        else if (owner instanceof Gallery) {
            this.gallery = (Gallery) owner;
        } else if (owner instanceof Item) {
            this.item = (Item) owner;
        } else if (owner instanceof Category) {
            this.category = (Category) owner;
        } else if (owner instanceof ListingSession) {
            this.listingSession = (ListingSession) owner;
        } else if (owner instanceof RegistrationSession) {
            this.registrationSession = (RegistrationSession) owner;
        } else if (owner instanceof AttributeOption)
            this.attributeOption = (AttributeOption) owner;

        else if (owner instanceof ItemOptionSelection)
            this.itemOptionSelection = (ItemOptionSelection) owner;

        else if (owner instanceof ImageBox) {
            this.imageBox = (ImageBox) owner;
        } else if (owner instanceof Banner) {
            this.banner = (Banner) owner;
        } else if (owner instanceof User) {
            this.user = (User) owner;
        } else if (owner instanceof FacebookOpenGraphTags) {
            this.facebookOpenGraphTags = (FacebookOpenGraphTags) owner;
        } else {
            throw new RuntimeException("Unrecognised image owner");
        }

        // check if we are moderating
        if (autoApprove || !owner.isModeratingImages()) {

            this.approved = true;

        } else {

            this.approved = false;
        }

        BufferedImage image = checkSize(smoothing, file);

        this.filename = filename;
        this.width = image.getWidth();
        this.height = image.getHeight();
        this.date = new DateTime();
        save();

        // save image count before thumbnail in case of thumbnail errors.
        owner.setImageInfo();
        owner.save();

        // do we want a thumbnail
        if (useThumb) {
            String fn = SimpleFile.getUniqueFilename(getThumbnailFileName(filename), ResourcesUtils.getRealThumbnailsPath());
            // check if thumbnail already exists otherwise generate
            if (getThumbnailFile(fn).exists()) {
                
                thumbnail = getThumbnailFile().getName();
                setThumbnailDimensions();

            } else {

                generateThumbnail(smoothing, fn);
            }
        }
    }

    public final boolean isApproved() {
        return approved;
    }

    public boolean isThumbnailed() {
        return hasThumbail();
    }

    public void reject() {
        getOwner().imageRejected(this);
        getOwner().removeImage(this);
    }

    private void removeThumbnail() {

        thumbnailWidth = 0;
        thumbnailHeight = 0;
        thumbnail = null;

        save();
    }

    /**
     * Resizes the image this img object represents if required by the current defaults set in config.
     *
     * @throws IOException
     */
    private void resize(boolean smoothing) throws IOException {

        File file = getFile();
        if (!file.exists() || file.length() == 0) {
            logger.fine("No such file or its length=0: " + file.toString());
            return;
        }

        BufferedImage image = checkSize(smoothing, file);

        width = image.getWidth();
        height = image.getHeight();
        save();
    }

    /**
     *
     */
    public void setAlt(String alt) {
        this.alt = alt;
    }

    public final void setCaption(String caption) {
        this.caption = caption;
        save("caption");
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param filename The filename to set.
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    public final void setImageType(ImageType imageType) {
        this.imageType = imageType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    private void setThumbnailDimensions() {

        if (thumbnail == null)
            return;

        File thumbnailFile = getThumbnailFile();

        try {

            BufferedImage image = ImageIO.read(thumbnailFile);
            thumbnailWidth = image.getWidth();
            thumbnailHeight = image.getHeight();
            save();

        } catch (IOException e) {

            removeThumbnail();
        }

    }

    /**
     *
     */
    public void setThumbnailFilename(String thumbnailFilename) {
        thumbnail = thumbnailFilename;
    }

    private boolean thumbnailExists() {
        return ResourcesUtils.getRealThumbnail(filename).exists();
    }

    /**
     * Checks the image referred to by this image exists. If not it deletes it.
     * Returns true if the image does exist.
     */
    boolean verify() {

        logger.fine("verifying image " + filename);

        if (exists()) {

            logger.fine("image exists, now checking thumbnail");

            if (!thumbnailExists()) {

                logger.fine("image exists, but thumbnail does not, regenerating");

                try {

                    generateThumbnail(true);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            getOwner().setImageInfo();
            getOwner().save();

            return true;

        } else {

            logger.fine("image not found on file system, deleting");

            delete();

            return false;

        }

    }

    public int compareTo(Img o) {
        if (position < o.position) {
            return -1;
        }

        if (position > o.position) {
            return 1;
        }
        return 0;
    }

    @Override
    public Img clone() throws CloneNotSupportedException {
        return (Img) super.clone();
    }

    public String getExtension() {
        return SimpleFile.getExtension(filename);
    }
}