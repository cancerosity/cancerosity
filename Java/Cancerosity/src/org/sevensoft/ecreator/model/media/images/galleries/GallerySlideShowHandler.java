package org.sevensoft.ecreator.model.media.images.galleries;

import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.StringResult;

import javax.servlet.ServletException;
import java.util.List;

/**
 * @author dme 05 Sep 2007 20:53:59
 */
@Path("gallery-slideshow.do")
public class GallerySlideShowHandler extends FrontendHandler {

    private Gallery gallery;


    public GallerySlideShowHandler(RequestContext context) {
        super(context);
    }

    @Override
    public Object main() throws ServletException {
        StringBuilder sb = new StringBuilder();

        sb.append("<html>\n");
        sb.append("<head>\n");
        sb.append("<TITLE>Gallery Slideshow</TITLE>\n");
        sb.append("<SCRIPT TYPE=\"text/javascript\" SRC=\"files/js/utils/slideshow.js\">\n");
        sb.append("<!--\n");
        sb.append("// JavaScript Slide Show\n");
        sb.append("// -->\n");
        sb.append("</SCRIPT>\n");
        sb.append("<SCRIPT TYPE=\"text/javascript\">\n");
        sb.append("<!--\n");
        sb.append("ss = new slideshow(\"ss\");\n");
        sb.append("ss.timeout = " + gallery.getSlideshowInterval() + ";\n");

        List<Img> images = gallery.getApprovedImages(ImageSortType.NAME_ASC);

        for (Img image : images) {
            sb.append("s = new slide();\n");
            sb.append("s.src = \"" + image.getImagePath() + "\";\n");
            sb.append("s.link = \"" + image.getPath() + "\";\n");
            sb.append("s.title = \"" + (image.hasCaption() ? image.getCaption() : image.getFilename()) + "\";\n");
            sb.append("s.text = \"<b>Description:</b> " + (image.hasDescription() ? image.getDescription().replaceAll("[\r\n]", " ").replaceAll("\"", "'") : "(empty)") + "\";\n");
            sb.append("ss.add_slide(s);\n");
        }

        sb.append("for (var i=0; i < ss.slides.length; i++) {\n");
        sb.append("\n");
        sb.append("  s = ss.slides[i];\n");
        sb.append("  s.target = \"ss_popup\";\n");
        sb.append("  s.attr = \"width=320,height=420,resizable=yes,scrollbars=yes\";\n");
        sb.append("\n");
        sb.append("}\n");
        sb.append("//-->\n");
        sb.append("</SCRIPT>\n");
        sb.append("</head>\n");
        sb.append("<BODY ONLOAD=\"ss.restore_position('SS_POSITION');ss.update();\"\n");
        sb.append("ONUNLOAD=\"ss.save_position('SS_POSITION');\">\n");
        sb.append("\n");
        sb.append("<DIV ID=\"slideshow\">\n");
        sb.append("\n");
        sb.append("<FORM ID=\"ss_form\" NAME=\"ss_form\" ACTION=\"\" METHOD=\"GET\">\n");
        sb.append("\n");
        sb.append("<DIV ID=\"ss_controls\">\n");
        sb.append("\n");
        sb.append("<INPUT TYPE=\"button\" VALUE=\"&lt;prev\" onclick=\"ss.previous()\">\n");
        sb.append("\n");
        sb.append("<INPUT TYPE=\"button\" VALUE=\"start\" onclick=\"ss.next();ss.play()\">\n");
        sb.append("\n");
        sb.append("<INPUT TYPE=\"button\" VALUE=\"pause\" onclick=\"ss.pause()\">\n");
        sb.append("\n");
        sb.append("<INPUT TYPE=\"button\" VALUE=\"next&gt;\" onclick=\"ss.next()\">\n");

        sb.append("<!--\n" +
                "The following select list will be modified with the slide titles.\n" +
                "-->\n" +
                "<select ID=\"ss_select\" name=\"ss_select\" onchange=\"ss.goto_slide(this.selectedIndex)\">\n" +
                "  <OPTION>\n" +
                "  <OPTION>\n" +
                "  <OPTION>\n" +
                "  <OPTION>\n" +
                "  <OPTION>\n" +
                "</SELECT>\n");

        sb.append("</DIV>\n");


        sb.append("<DIV ID=\"ss_img_div\">\n" +
                "\n" +
                "<A ID=\"ss_img_link\" HREF=\"javascript:ss.hotlink()\"><IMG\n" +
                "ID=\"ss_img\" NAME=\"ss_img\" SRC=\"\"\n" +
                "STYLE=\"filter:progid:DXImageTransform.Microsoft.Fade();\"\n" +
                "ALT=\"Slideshow image\"></A>\n" +
                "\n" +
                "</DIV>\n" +
                "\n" +
                "<DIV ID=\"ss_text\">\n");
        sb.append("<SCRIPT type=\"text/javascript\">\n" +
                "<!--\n" +
                "\n" +
                "// The contents of this DIV will be overwritten by browsers\n" +
                "// that support innerHTML.\n" +
                "//\n" +
                "// For browsers that do not support innerHTML, we will display\n" +
                "// a TEXTAREA element to hold the slide text.\n" +
                "// Note however that if the slide text contains HTML, then the\n" +
                "// HTML codes will be visible in the textarea.\n" +
                "\n" +
                "document.write('<TEXTAREA ID=\"ss_textarea\" NAME=\"ss_textarea\" ROWS=\"6\" COLS=\"40\" WRAP=\"virtual\"><\\/TEXTAREA>\\n');\n" +
                "\n" +
                "ss.textarea = document.ss_form.ss_textarea;\n" +
                "\n" +
                "//-->\n" +
                "</SCRIPT>\n" +
                "\n" +
                "</DIV>\n" +
                "\n" +
                "</FORM>\n" +
                "\n" +
                "</DIV>\n" +
                "\n" +
                "<SCRIPT TYPE=\"text/javascript\">\n" +
                "<!--\n" +
                "\n" +
                "// Finish defining and activating the slideshow\n" +
                "\n" +
                "// Set up the select list with the slide titles\n" +
                "function config_ss_select() {\n" +
                "  var selectlist = document.ss_form.ss_select;\n" +
                "  selectlist.options.length = 0;\n" +
                "  for (var i = 0; i < ss.slides.length; i++) {\n" +
                "    selectlist.options[i] = new Option();\n" +
                "    selectlist.options[i].text = (i + 1) + '. ' + ss.slides[i].title;\n" +
                "  }\n" +
                "  selectlist.selectedIndex = ss.current;\n" +
                "}\n" +
                "\n" +
                "// If you want some code to be called before or\n" +
                "// after the slide is updated, define the functions here\n" +
                "\n" +
                "ss.pre_update_hook = function() {\n" +
                "  return;\n" +
                "}\n" +
                "\n" +
                "ss.post_update_hook = function() {\n" +
                "  // For the select list with the slide titles,\n" +
                "  // set the selected item to the current slide\n" +
                "  document.ss_form.ss_select.selectedIndex = this.current;\n" +
                "  return;\n" +
                "}\n" +
                "\n" +
                "if (document.images) {\n" +
                "\n" +
                "  // Tell the slideshow which image object to use\n" +
                "  ss.image = document.images.ss_img;\n" +
                "\n" +
                "  // Tell the slideshow the ID of the element\n" +
                "  // that will contain the text for the slide\n" +
                "  ss.textid = \"ss_text\";\n" +
                "\n" +
                "  // Randomize the slideshow?\n" +
                "  // ss.shuffle();\n" +
                "\n" +
                "  // Set up the select list with the slide titles\n" +
                "  config_ss_select();\n" +
                "\n" +
                "  // Update the image and the text for the slideshow\n" +
                "  ss.update();\n" +
                "\n" +
                "  // Auto-play the slideshow\n" +
                "  //ss.play();\n" +
                "}\n" +
                "\n" +
                "//-->\n" +
                "</SCRIPT>\n" +
                "\n" +
                "\n" +
                "<SCRIPT TYPE=\"text/javascript\">\n" +
                "<!--\n" +
                "\n" +
                "// Make the slideshow accessible to search engines\n" +
                "// and non-javascript browsers.\n" +
                "//\n" +
                "// The following function is normally left commented out.\n" +
                "// It is used only to produce some HTML that you can copy\n" +
                "// into the NOSCRIPT section below.\n" +
                "// The HTML produced will contain all of the slideshow images,\n" +
                "// plus the text for each image.\n" +
                "\n" +
                "// document.write(\"<PRE>\" + ss.noscript() + \"<\\/PRE>\");\n" +
                "\n" +
                "//-->\n" +
                "</SCRIPT>\n" +
                "\n" +
                "<NOSCRIPT>\n" +
                "\n" +
                "<!--\n" +
                "This is a version of the slideshow for search engines\n" +
                "and non-javascript browsers\n" +
                "-->\n" +
                "\n" +
                "</NOSCRIPT>\n" +
                "\n" +
                "</BODY>\n");
        sb.append("</html>\n");
        return new StringResult(sb, "text/html");
    }
}