package org.sevensoft.ecreator.model.media.images.emails;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.media.images.ImageSettings;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 25 Jan 2007 15:59:19
 *
 */
public class ApprovedEmail extends Email {

	private RequestContext	context;
	private ImageSettings	imageSettings;
	private Config		config;
	private Item		account;

	public ApprovedEmail(RequestContext context, Item account) {

		this.context = context;
		this.config = Config.getInstance(context);
		this.imageSettings = ImageSettings.getInstance(context);
		this.account = account;

		setSubject("Image approved");
		setBody(imageSettings.getApprovedEmailBody());
		setFrom(config.getServerEmail());
		setTo(account.getEmail());
	}

    @Override
	public void send(String hostname) throws EmailAddressException, SmtpServerException {
		new EmailDecorator(context, this).send(hostname);
	}

}
