package org.sevensoft.ecreator.model.media.images.galleries.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGalleryImageMarker;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.superstrings.StringHelper;

import java.util.Map;

/**
 * User: Tanya
 * Date: 29.04.2011
 */
public class ImageDescriptionMarker extends MarkerHelper implements IGalleryImageMarker {

    public Object generate(RequestContext context, Map<String, String> params, Img image) {
        int limit = 0;
        if (params.containsKey("limit")){
            limit=Integer.valueOf(params.get("limit"));
        }
        return super.string(context, params, StringHelper.toSnippet(image.getDescription(), limit));
    }

    public Object getRegex() {
        return "image_description";
    }


}
