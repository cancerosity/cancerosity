package org.sevensoft.ecreator.model.media.images.galleries.search;

import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.media.images.galleries.Gallery;
import org.sevensoft.ecreator.model.media.images.galleries.ImageSortType;
import org.sevensoft.jeezy.db.StringQueryBuilder;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.List;

/**
 * @author dme 02 Sep 2007 10:38:09
 */
public class GalleryImageSearcher {

    private final RequestContext context;
    private int limit;
    private int start;
    private String keywords;
    private Gallery gallery;


    public GalleryImageSearcher(RequestContext context) {
        this.context = context;
    }

    public List<Img> execute(ImageSortType sortType) {
        return (List<Img>) run(false, sortType);
    }

    public String getKeywords() {
        return keywords != null ? keywords.trim().toLowerCase() : null;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Gallery getGallery() {
        return gallery;
    }

    public void setGallery(Gallery gallery) {
        this.gallery = gallery;
    }

    public final int getLimit() {
        return limit;
    }

    public final int getStart() {
        return start;
    }

    private Object run(boolean count, ImageSortType sortType) {

        StringQueryBuilder b = new StringQueryBuilder(context);
        if (count) {
            b.append("SELECT count(*)");
        } else {
            b.append("SELECT *");
        }
        b.append(" FROM # ");
        b.addTable(Img.class);
        b.append(" WHERE 1=1 ");

        if (getGallery() != null) {
            b.append(" AND gallery=?", getGallery());
        }
        if (getKeywords() != null) {
            b.append(" AND ( LOWER(caption) LIKE ?", "%" + getKeywords() + "%");
            b.append(" OR LOWER(filename) LIKE ?", "%" + getKeywords() + "%");
            b.append(" OR LOWER(description) LIKE ?", "%" + getKeywords() + "%");
            b.append(" )");
        }
        if (sortType != null) {
            b.append(" ORDER BY " + sortType.getColumn() + " " + sortType.getOrder());
        } else {
            b.append(" ORDER BY date DESC");
        }

        if (count) {
            return b.toQuery().getInt();
        } else {
            return b.toQuery().execute(Img.class, start, limit);
        }
    }


    public final void setLimit(int limit) {
        this.limit = limit;
    }


    public final void setStart(int start) {
        this.start = start;
    }

    public int size() {
        return (Integer) run(true, null);
    }

}