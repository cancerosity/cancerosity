package org.sevensoft.ecreator.model.media.videos.panels;

import java.util.List;

import org.sevensoft.ecreator.iface.EcreatorRenderer;
import org.sevensoft.ecreator.iface.admin.media.videos.VideoHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.media.videos.Video;
import org.sevensoft.ecreator.model.media.videos.VideoOwner;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 27-Mar-2006 18:19:15
 *
 */
public class VideoAdminPanel extends EcreatorRenderer {

	private List<Video>		videos;
	private final VideoOwner	owner;

	public VideoAdminPanel(RequestContext context, VideoOwner owner) {
		super(context);

		this.owner = owner;
		this.videos = owner.getVideos();
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(new AdminTable("Videos"));

		sb.append("<tr>");
		sb.append("<th width='60'>Position</th>");
		sb.append("<th>File</th>");
		sb.append("<th>Size</th>");
		sb.append("<th width='10'> </th>");
		sb.append("</tr>");

		PositionRender pr = new PositionRender(VideoHandler.class, "video");

		for (Video video : videos) {

			sb.append("<tr>");

			sb.append("<td width='60'>" + pr.render(video) + "</td>");

			sb.append("<td>" + new LinkTag(VideoHandler.class, null, new SpannerGif(), "video", video) + " ");
			sb.append(video.getFilename());
			sb.append("</td>");

			sb.append("<td>" + video.getSize() + "</td>");

			sb.append("<td width='10'>" + new LinkTag(VideoHandler.class, "removeVideo", new DeleteGif(), "video", video) + "</td>");
			sb.append("</tr>");

		}

		/*
		 * Upload image from hard drive. Show file inputs (number to be determined by setting in misc settings
		 */
		sb.append("<tr>");
		sb.append("<td colspan='4'>Upload video ");

		sb.append(new FileTag("videoUploads", 60));
		sb.append(new SubmitTag("Upload"));

		sb.append("</td></tr>");

		sb.append("</table>");

		return sb.toString();
	}
}
