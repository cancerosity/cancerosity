package org.sevensoft.ecreator.model.media.images.boxes;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.iface.admin.containers.boxes.ImageBoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.ImageOwner;
import org.sevensoft.ecreator.model.media.images.ImageUtil;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 4 Dec 2006 09:59:23
 *
 */
@Table("boxes_images")
@Label("Sidebox image")
@HandlerClass(ImageBoxHandler.class)
public class ImageBox extends Box implements ImageOwner {

	private int		pendingImageCount;
	private String	url;
	private boolean	newWindow;
	private int		imageCount;
	private long	imageUpdateTimestamp;

	protected ImageBox(RequestContext context) {
		super(context);
	}

	public ImageBox(RequestContext context, String panel) {
		super(context, panel);
	}

	public final Img addImage(Img image) throws IOException, ImageLimitException {
		return new Img(context, this, image, true, true);
	}

	public final Img addImage(String filename, boolean useThumb, boolean smoothing, boolean autoApprove) throws IOException, ImageLimitException {
		return new Img(context, this, filename, useThumb, smoothing, autoApprove);
	}

	public final Img addImage(Upload upload, boolean useThumb, boolean smoothing, boolean autoApprove) throws IOException, ImageLimitException {
		return new Img(context, this, upload, useThumb, smoothing, autoApprove);
	}

	public final Img addImage(URL url, boolean useThumb, boolean smoothing, boolean autoApprove) throws IOException, ImageLimitException {
		return new Img(context, this, url, useThumb, smoothing, autoApprove);
	}

    public Img addImage(Upload imageUpload, boolean useThumb, boolean smoothing, boolean autoApprove, String caption) throws IOException, ImageLimitException {
        return new Img(context, this, imageUpload, useThumb, smoothing, autoApprove);
    }

    public final void copyImagesTo(ImageOwner target) throws IOException, ImageLimitException {
		ImageUtil.copyImagesTo(this, target);
	}

	@Override
	public synchronized boolean delete() {
		removeImages();
		return super.delete();
	}

	public List<Img> getAllImages() {
		return ImageUtil.getAllImages(context, this);
	}

	public int getAllImagesCount() {
		return ImageUtil.getAllImagesCount(this);
	}

	public String getAlt() {
		return null;
	}

	public Img getAnyImage() {
		return ImageUtil.getAnyImage(context, this);
	}

	public final Img getApprovedImage() {
		return ImageUtil.getApprovedImage(this);
	}

	public int getApprovedImageCount() {
		return imageCount;
	}

	public final List<Img> getApprovedImages() {
		return ImageUtil.getApprovedImages(context, this);
	}

	@Override
	protected String getCssIdDefault() {
		return "image_box";
	}

	public DateTime getImageAddedTime() {
		return null;
	}

	public int getImageLimit() {
		return 1;
	}

	public String getImagePlaceholder() {
		return null;
	}

	public final long getImageUpdateTimestamp() {
		return imageUpdateTimestamp;
	}

	public final int getPendingImageCount() {
		return pendingImageCount;
	}

	public final String getUrl() {
		return url;
	}

	public boolean hasAnyImage() {
		return ImageUtil.hasAnyImage(this);
	}

	public final boolean hasApprovedImages() {
		return imageCount > 0;
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	public boolean hasImagePlaceholder() {
		return false;
	}

	public final boolean hasMoreApprovedImages() {
		return ImageUtil.hasMoreApprovedImages(this);
	}

	public boolean hasUrl() {
		return url != null;
	}

	public void imageApproved(Img img) {
	}

	public void imageCallback(Img image) {
	}

	public void imageRejected(Img img) {
	}

	public final boolean isAtImageLimit() {
		return ImageUtil.isAtImageLimit(this);
	}

	public boolean isModeratingImages() {
		return false;
	}

	public boolean isNewWindow() {
		return newWindow;
	}

	public final void moveImagesTo(ImageOwner target) throws IOException, ImageLimitException {
		ImageUtil.moveImagesTo(this, target);
	}

	public final void removeImage(Img image) {
		ImageUtil.removeImage(this, image);
	}

	public final void removeImage(Img image, boolean deleteFile, boolean deleteThumbnail) {
		ImageUtil.removeImage(this, image, deleteFile, deleteThumbnail);
	}

	public final void removeImages() {
		ImageUtil.removeImages(this);
	}

	@Override
	public String render(RequestContext context) {

		if (!hasAnyImage()) {
			return null;
		}

		Img image = getAnyImage();
		if (image == null) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		sb.append(getTableTag());
		sb.append("<tr><td>");

		if (url == null) {

			sb.append(image.getImageTag());

		} else {
			
			LinkTag linkTag = new LinkTag(url, image.getImageTag());

			if (newWindow) {

				linkTag.setTarget("_blank");

			} 
			
			sb.append(linkTag);
		}

		sb.append("</td></tr>");
		sb.append("</table>");

		return sb.toString();
	}

	public final void setImageInfo() {
		this.imageCount = getApprovedImages().size();
		this.pendingImageCount = getAllImages().size() - imageCount;
		save();
	}

	public final void setImageUpdateTimestamp() {
		this.imageUpdateTimestamp = System.currentTimeMillis();
	}

	public final void setNewWindow(boolean newWindow) {
		this.newWindow = newWindow;
	}

	public final void setUrl(String url) {
		this.url = url;
	}

	public boolean useImageDescriptions() {
		return false;
	}

}
