package org.sevensoft.ecreator.model.media.images;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.simpleio.FileExistRule;
import org.sevensoft.commons.simpleio.SimpleZip;
import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.attributes.AttributeOption;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.ecreator.model.media.images.galleries.Gallery;
import org.sevensoft.ecreator.model.misc.content.ContentBlock;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.design.template.TemplateUtil;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.Row;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.image.BufferedImage;
import java.awt.*;

/**
 * @author sks 09-Jan-2006 17:10:22
 */
public class ImageUtil {

	private static Logger	logger	= Logger.getLogger("ecreator");

    public static Img addImage(RequestContext context, ImageOwner owner, Upload upload, boolean useThumb, boolean smoothing, boolean autoApprove)
            throws IOException, ImageLimitException {
        return addImage(context, owner, upload, useThumb, smoothing, autoApprove, null);
    }

	public static Img addImage(RequestContext context, ImageOwner owner, Upload upload, boolean useThumb, boolean smoothing, boolean autoApprove, String caption)
			throws IOException, ImageLimitException {

		File file = upload.getFile();
		logger.fine("[ImageUtil] upload file=" + file + ", origfilename=" + upload.getFilename());
		if (upload.getFilename().toLowerCase().endsWith(".zip")) {

			List<File> files = SimpleZip.extractToDir(file, ResourcesUtils.getRealImagesPath(), true, null, FileExistRule.Rename);

			logger.fine("[ImageUtil] extracted files=" + files);
			Img img = null;

			for (File extractedFile : files) {

                logger.fine("[ImageUtil] adding extracted file=" + extractedFile);
                img = new Img(context, owner, extractedFile.getName(), useThumb, smoothing, autoApprove);
                img.setCaption(caption);
			}

			return img;

		} else {

            Img img = new Img(context, owner, upload, useThumb, smoothing, autoApprove);
            img.setCaption(caption);
            return img;
		}
	}

	/**
	 * Delete all unused images / thumbnails from the image / thumbnail directories.
	 * <p/>
	 * Returns number of images deleted
	 * 
	 * @param context
	 */
	public static int clean(RequestContext context) {

		Query q = new Query(context, "select filename from #");
		q.setTable(Img.class);
		List<String> filenames = q.getStrings();

		Pattern pattern = Pattern.compile("src=(?:'|\")images/(.+?)(?:'|\")", Pattern.CASE_INSENSITIVE);

		/* Get all filenames from item or category contents */
		q = new Query(context, "select content from # union select content from #");
		q.setTable(ContentBlock.class);
		q.setTable(Item.class);

		for (String string : q.getStrings()) {

			if (string == null)
				continue;

			Matcher matcher = pattern.matcher(string);
			while (matcher.find()) {

				String filename = matcher.group(1);
				// System.out.println("Adding filename: " + filename);
				filenames.add(filename);
			}
		}

		int n = 0;

		for (File file : ResourcesUtils.getRealImagesDir().listFiles(new FilenameFilter() {

			public boolean accept(File dir, String name) {
				name = name.toLowerCase();
				return name.endsWith(".jpg") || name.endsWith(".png") || name.endsWith(".gif");
			}
		})) {

			String name = file.getName();
			// System.out.println(filenames);
			// System.out.println(name);
			if (!filenames.contains(name)) {

				// System.out.println(file);
				file.delete();
				n++;
			}
		}

		filenames = null;

		q = new Query(context, "select thumbnail from #");
		q.setTable(Img.class);
		List<String> thumbnails = q.getStrings();

		for (File file : ResourcesUtils.getRealThumbnailsDir().listFiles(new FilenameFilter() {

			public boolean accept(File dir, String name) {
				name = name.toLowerCase();
				return name.endsWith(".jpg") || name.endsWith(".png") || name.endsWith(".gif");
			}
		})) {

			String name = file.getName();
			if (!thumbnails.contains(name)) {

				file.delete();
				n++;
			}
		}

		return n;
	}

	public static void copyImagesTo(ImageOwner src, ImageOwner target) throws IOException, ImageLimitException {

		for (Img image : src.getAllImages()) {
			target.addImage(image);
		}
	}

    public static void copyImageTo(String imageFilename, File destFile) throws IOException {

        File originalImage = ResourcesUtils.getRealImage(imageFilename);
        
        TemplateUtil.copyFile(destFile, originalImage);
    }

	public static List<Img> getAllImages(RequestContext context, ImageOwner owner) {
		Query q = new Query(context, "select * from # where " + owner.getClass().getSimpleName() + "=? order by position");
		q.setTable(Img.class);
		q.setParameter(owner);
		return q.execute(Img.class);
	}

	public static int getAllImagesCount(ImageOwner owner) {
		return owner.getPendingImageCount() + owner.getApprovedImageCount();
	}

	public static Img getAnyImage(RequestContext context, ImageOwner owner) {
		List<Img> images = getAllImages(context, owner);
		return images.isEmpty() ? null : images.get(0);
	}

	public static Img getApprovedImage(ImageOwner owner) {

		List<Img> images = owner.getApprovedImages();
		if (images.isEmpty()) {
			return null;
		}
		return images.get(0);
	}

	public static List<Img> getApprovedImages(RequestContext context, ImageOwner owner) {

		List<Img> imgs = new ArrayList(owner.getAllImages());

		CollectionsUtil.filter(imgs, new Predicate<Img>() {

			public boolean accept(Img e) {
				return e.isApproved();
			}
		});

		Collections.sort(imgs);

		return imgs;
	}

	public static Img getRandomImage(ImageOwner owner) {

		List<Img> images = owner.getApprovedImages();
		if (images.isEmpty()) {
			return null;
		}

		Collections.shuffle(images);
		return images.get(0);
	}

	public static boolean hasAnyImage(ImageOwner owner) {
		return owner.getAllImagesCount() > 0;
	}

	public static boolean hasMoreApprovedImages(ImageOwner owner) {
		return owner.getApprovedImageCount() > 1;
	}

	public static boolean hasMoreImages(ImageOwner owner) {
		return owner.getApprovedImageCount() > 1;
	}

	public static boolean isAtImageLimit(ImageOwner owner) {

		if (owner.getImageLimit() == 0)
			return false;

		return owner.getPendingImageCount() + owner.getApprovedImageCount() >= owner.getImageLimit();
	}

	public static void moveImagesTo(ImageOwner src, ImageOwner target) throws IOException, ImageLimitException {

		copyImagesTo(src, target);
		src.removeImages();
	}

	public static void prepopulate(RequestContext context, List<Item> items) {

		String itemsQueryString = StringHelper.repeat("item=?", items.size(), " or ");

		Query q = new Query(context, "select * from # where " + itemsQueryString + " order by position");
		q.setTable(Img.class);
		q.setParameters(items);
		for (Img img : q.execute(Img.class)) {
			img.getItem().prepopulate(img);
		}
	}

	public static int randomImages(Upload upload, RequestContext context, ItemType itemType, Category searchCategory) throws IOException, ImageLimitException {

		// save images out to zip, getting list of image filenames
		List<File> filenames = SimpleZip.extractToDir(upload.getFile(), ResourcesUtils.getRealImagesPath(), true, null, FileExistRule.Rename);

		logger.fine("zip extracted, filenames=" + filenames);

		ItemSearcher searcher = new ItemSearcher(context);
		searcher.setNoImages(true);
		searcher.setLimit(200);
		searcher.setSearchCategory(searchCategory);
		searcher.setItemType(itemType);

		int n = 0;
		for (Item item : searcher) {

			String filename = filenames.get(n % filenames.size()).getName();
			logger.fine("[ImageUtil] processing pos=" + n % filenames.size() + ", item=" + item + ", filename" + filename);
			item.addImage(filename, true, true, false);

			n++;
		}

		return n;
	}

	public static void removeImage(ImageOwner owner, Img image) {
		removeImage(owner, image, false, false);

	}

	public static void removeImage(ImageOwner owner, Img image, boolean deleteFile, boolean deleteThumbnail) {

		image.delete();
		owner.setImageInfo();
	}

	public static void removeImage(ImageOwner owner, String filename) {
		for (Img img : owner.getAllImages()) {
			if (img.getFilename().equals(filename)) {
				removeImage(owner, img, false, false);
			}
		}
	}

	public static void removeImages(ImageOwner owner) {

		for (Img image : owner.getAllImages()) {
			removeImage(owner, image, false, false);
		}

		owner.setImageInfo();
		owner.save();
	}

	public static void resetImageCounts(RequestContext context) {

		Class[] classes = new Class[] { Item.class, Category.class, Gallery.class, AttributeOption.class, ItemOptionSelection.class };

		for (Class clazz : classes) {

			new Query(context, "update # set imageCount=0").setTable(clazz).run();
			Query q = new Query(context, "select " + clazz.getSimpleName() + ", count(*) from # group by " + clazz.getSimpleName());
			q.setTable(Img.class);
			for (Row row : q.execute()) {

				int id = row.getInt(0);
				int count = row.getInt(1);

				Query q2 = new Query(context, "update # set imageCount=? where id=?");
				q2.setTable(clazz);
				q2.setParameter(count);
				q2.setParameter(id);
				q2.run();
			}
		}
	}

	public static void save(RequestContext context, ImageOwner owner, List<Upload> imageUploads, List<String> imageUrls, String imageFilename,
			boolean autoApprove) throws ImageLimitException, IOException {

		if (imageUploads != null) {

			for (Upload upload : imageUploads) {
				owner.addImage(upload, true, true, autoApprove);
			}
		}

		if (imageUrls != null) {

			for (String imageUrl : imageUrls) {

				try {

					String fn = Img.download(context, imageUrl);
					owner.addImage(fn, true, true, autoApprove);

				} catch (FileNotFoundException e) {
					e.printStackTrace();
                    logger.warning(imageUrl);
                    logger.warning(e.toString());

				} catch (IOException e) {
					e.printStackTrace();
                    logger.warning(imageUrl);
                    logger.warning(e.toString());
				}
			}
		}

		if (imageFilename != null) {

			try {

				owner.addImage(imageFilename, true, true, true);
				// owner.log(user, "Image added: " + img.getFilename());

			} catch (IOException e) {
				e.printStackTrace();
                logger.warning("Owner: " + owner.toString() + ", Image: " + imageFilename);
                logger.warning(e.toString());

			} catch (ImageLimitException e) {
				e.printStackTrace();
			}
		}
	}

	/**
     *
     */
	private static void stripWhitespace(File file) {

		logger.fine("[ImageUtil] checking file=" + file);
		String renamed = file.getName().replaceAll("\\s", "_");

		if (!renamed.equals(file.getName())) {
			final File renamedFile = new File(file.getParentFile().getAbsolutePath() + "/" + renamed);
			logger.fine("[ImageUtil] renamed file=" + renamedFile);
			file.renameTo(renamedFile);
		}
	}

	private static void stripWhitespace(List<File> files) {

		for (File file : files) {
			stripWhitespace(file);
		}
	}

	public static void stripWhitespace(RequestContext context) {

		Config config = Config.getInstance(context);

		for (File file : ResourcesUtils.getRealImagesDir().listFiles()) {
			stripWhitespace(file);
		}

		for (File file : ResourcesUtils.getRealThumbnailsDir().listFiles()) {
			stripWhitespace(file);
		}
	}

	public static int upload(RequestContext context, Upload upload) throws IOException {

		Config config = Config.getInstance(context);

		int i = 0;

		File file = upload.getFile();
		if (file.getName().toLowerCase().endsWith(".zip")) {

			ImageFiletypeFilter filter = new ImageFiletypeFilter();

			logger.fine("[ImageUtil] Detected zip file: " + upload.getFilename());
			List<File> files = SimpleZip.extractToDir(file, ResourcesUtils.getRealImagesDir(), true, filter, FileExistRule.Overwrite);
			stripWhitespace(files);

			i = i + files.size();

		} else {

			logger.fine("[ImageUtil] Writing single image: " + upload.getFilename());
			String filename = upload.writeUnique(ResourcesUtils.getRealImagesPath());
			logger.fine("[ImageUtil] Image written to: " + filename);

			i = 1;
		}

		return i;
	}

	/**
	 * Deletes any image instances for filenames that no longer exist
	 */
	public static void verify(RequestContext context) {

		logger.config("[ImageUtil] running image repair");

		/*
		 * Reset all image counts as we will reset these next
		 */
		new Query(context, "update # set imageCount=0").setTable(Item.class).run();
		new Query(context, "update # set imageCount=0").setTable(Gallery.class).run();
		new Query(context, "update # set imageCount=0").setTable(Category.class).run();
		new Query(context, "update # set imageCount=0").setTable(ItemOptionSelection.class).run();

		logger.config("reset image counts on owners of images");

		/*
		 * For all images verify and set image count again
		 */
		int n = 0;
		List<Img> images;
		while ((images = Img.get(context, n, 500)).size() > 0) {

			logger.config("[ImageUtil] processing " + images.size() + " images");

			for (Img image : images) {
				image.verify();
			}

			n += 500;
		}

	}

}
