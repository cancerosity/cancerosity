package org.sevensoft.ecreator.model.media.images;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 25 Jan 2007 12:21:46
 *
 */
@Singleton
@Table("images_settings")
public class ImageSettings extends EntityObject {

	public static ImageSettings getInstance(RequestContext context) {
		return EntityObject.getSingleton(context, ImageSettings.class);
	}

	/**
	 * Send out unapproved email
	 */
	private boolean	sendApprovalEmails;

	/**
	 * 
	 */
	private String	rejectedEmailBody;

	/**
	 * 
	 */
	private String	approvedEmailBody;

	/**
	 * Content to show after an image is pending upload
	 */
	private String	pendingContent;

    private String formatsNote;

	protected ImageSettings(RequestContext context) {
		super(context);
	}

	public final String getApprovedEmailBody() {
		return approvedEmailBody;
	}

	public final String getPendingContent() {
		return pendingContent;
	}

	public final String getRejectedEmailBody() {
		return rejectedEmailBody;
	}

	public boolean hasApprovedEmailBody() {
		return approvedEmailBody != null;
	}

	public boolean hasPendingContent() {
		return pendingContent != null;
	}

	public boolean hasRejectedEmailContent() {
		return rejectedEmailBody != null;
	}

	public final boolean isSendApprovalEmails() {
		return sendApprovalEmails;
	}

	public final void setApprovedEmailBody(String approvedEmailContent) {
		this.approvedEmailBody = approvedEmailContent;
	}

	public void setPendingContent(String approvalConfirmationContent) {
		this.pendingContent = approvalConfirmationContent;
	}

	public final void setRejectedEmailBody(String unapprovedEmailContent) {
		this.rejectedEmailBody = unapprovedEmailContent;
	}

	public final void setSendApprovalEmails(boolean sendUnapprovedEmail) {
		this.sendApprovalEmails = sendUnapprovedEmail;
	}

    public String getFormatsNote() {
        return formatsNote !=null? formatsNote: "You can upload images in GIF, JPEG or PNG format - "
					+ "most digital cameras automatically use one of these formats.<br/>"
					+ "Do not worry about the size of the images - our site will automatically resize any images that are too large.";
    }

    public void setFormatsNote(String formatsNote) {
        this.formatsNote = formatsNote;
    }
}
