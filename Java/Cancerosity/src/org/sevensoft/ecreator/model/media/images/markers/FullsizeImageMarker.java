package org.sevensoft.ecreator.model.media.images.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.marker.interfaces.ICategoryMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.media.images.ImageOwner;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
@Deprecated
public class FullsizeImageMarker implements IItemMarker, ICategoryMarker {

	public Object generate(RequestContext context, Map<String, String> params, Category category) {
		return renderImage(context, params, category);
	}

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
		return renderImage(context, params, item);
	}

	public Object getRegex() {
		return "fullsize";
	}

	private Object renderImage(RequestContext context, Map<String, String> params, ImageOwner owner) {

		Img image = owner.getApprovedImage();
		if (image == null) {
			return null;
		}

		boolean lightbox = params.containsKey("lightbox");
        boolean altParam = params.containsKey("alt");
		String w = params.get("w");
		String h = params.get("h");
		String align = params.get("align");
		String title = params.get("title");

		int width;
		int height;

		if (w == null) {
			width = 0;
		} else {
			width = Integer.parseInt(w.trim());
		}

		if (h == null) {
			height = 0;
		} else {
			height = Integer.parseInt(h.trim());
		}

		int[] d = Img.getDimensionsToFit(width, height, image, false);

		ImageTag imageTag = new ImageTag(image.getPath(), 0, d[0], d[1]);
		imageTag.setId("fullsize_image");
		if (align != null) {
			imageTag.setAlign(align);
		}

        String altValue = null;
        if (image.getCaption() != null) {
            altValue = image.getCaption();
        } else if (image.getDescription() != null) {
            altValue = image.getDescription();
        } else if (owner.getAlt() != null) {
            altValue = owner.getAlt();
        } else if (altParam) {
            altValue = params.get("alt");
        }
        if (altValue != null) {
            imageTag.setAlt(altValue);
            imageTag.setTitle(altValue);
        }

//        if (altParam) {
//            imageTag.setAlt(params.get("alt"));
//        } else if (image.getDescription() != null) {
//            imageTag.setAlt(image.getDescription());
//        } else {
//            imageTag.setAlt(owner.getAlt());
//        }
//        imageTag.setTitle(imageTag.getAlt());


		if (lightbox) {

			LinkTag linkTag = new LinkTag(image.getPath(), imageTag);
			linkTag.setRel(String.valueOf(owner.getId()));

			if (title == null) {
				title = owner.getAlt();
			}

			if (title != null) {
				linkTag.setTitle(title);
			}

			linkTag.setClass("thickbox");

			return linkTag;

		}

		return "<span class='image'>" + imageTag + "</span>";
	}
}
