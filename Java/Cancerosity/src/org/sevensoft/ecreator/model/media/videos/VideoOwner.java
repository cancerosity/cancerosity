package org.sevensoft.ecreator.model.media.videos;

import java.io.IOException;
import java.util.List;

import org.sevensoft.jeezy.http.Upload;

/**
 * @author sks 14 Feb 2007 12:29:09
 *
 */
public interface VideoOwner {

	public Video addVideo(String filename) throws IOException;

	public Video addVideo(Upload upload) throws IOException;

	public List<Video> getVideos();

	public void removeVideo(Video video);

}
