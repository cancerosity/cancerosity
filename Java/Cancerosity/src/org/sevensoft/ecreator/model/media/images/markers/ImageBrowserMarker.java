package org.sevensoft.ecreator.model.media.images.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.images.ImageBrowserHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 21 Jun 2006 23:39:30
 *
 */
public class ImageBrowserMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

        if (item.getAllImages().isEmpty()) {
            return null;
        }

        if (!params.containsKey("width")) {
			params.put("width", "620");
		}

		if (!params.containsKey("height")) {
			params.put("height", "550");
		}

		params.put("popup", "true");

		return super.link(context, params, new Link(ImageBrowserHandler.class, null, "item", item), "image_browser");
	}

	public Object getRegex() {
		return "image_browser";
	}
}
