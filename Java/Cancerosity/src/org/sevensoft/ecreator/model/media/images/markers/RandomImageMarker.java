package org.sevensoft.ecreator.model.media.images.markers;

import org.sevensoft.commons.superstrings.RandomHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Map;

/**
 * @author sks 21 Jun 2006 23:39:30
 */
public class RandomImageMarker implements IGenericMarker {

    public Object generate(RequestContext context, Map<String, String> params) {

        final String prefix = params.get("prefix");
        if (prefix == null)
            return null;

        File file = ResourcesUtils.getRealImagesDir();
        String[] filenames = file.list(new FilenameFilter() {

            public boolean accept(File dir, String name) {
                return name.startsWith(prefix);
            }

        });

        if (filenames == null || filenames.length == 0)
            return null;

        int n = Math.abs(RandomHelper.getRandomNumber(0, filenames.length - 1));
        return new ImageTag(Config.getInstance(context).getImagesPath() + "/" + filenames[n]);
    }

    public Object getRegex() {
        return "random_image";
    }

}
