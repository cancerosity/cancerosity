package org.sevensoft.ecreator.model.media.images.markers;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketLineMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.ICategoryMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.media.images.ImageOwner;
import org.sevensoft.ecreator.model.media.images.ImageType;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.links.JavascriptLinkTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.links.RelImageTag;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Shows the images for this object.
 */
public class ImagesMarker implements IItemMarker, ICategoryMarker, IBasketLineMarker, IGenericMarker {

    private static final Logger logger = Logger.getLogger("markers");

    private Object doTag(RequestContext context, Map<String, String> params, ImageOwner owner, String url, String imageHolder) {

        logger.fine("[ImagesMarker] generating tag for " + owner);

        int[] dimensions = getDimensions(params);
        boolean fullsize = params.containsKey("fullsize");

        if (owner.hasApprovedImages()) {

            int limit = params.containsKey("limit") ? Integer.parseInt(params.get("limit").trim()) : 0;
            int pos = 0;
            if (params.containsKey("pos")) {
                pos = Integer.parseInt(params.get("pos").trim());
            }

            StringBuilder sb = new StringBuilder();

            List<Img> images;
            if (params.containsKey("imagetype") && owner instanceof Item) {

                ImageType imageType = EntityObject.getInstance(context, ImageType.class, params.get("imagetype"));
                images = ((Item) owner).getApprovedImages(imageType);

            } else {

                images = owner.getApprovedImages();
            }

            // if position > 0 then we want to only show that image
            if (pos > 0 && pos <= images.size()) {
                images = images.subList(pos - 1, pos);
            }

            int n = 1;
            for (Img image : images) {

                int[] d;
                if (params.containsKey("cover")) {
                    d = Img.getDimensionsToCover(dimensions[0], dimensions[1], image);
                } else {
                    d = Img.getDimensionsToFit(dimensions[0], dimensions[1], image, true);
                }

                String thumbnailPath = fullsize ? image.getPath() : image.getThumbnailPath();
                sb.append(doTag(context, params, owner, url, thumbnailPath, image.getPath(), d[0], d[1], image.getWidth(),
                        image.getHeight(), image.getDescription(), owner.getAlt(), image.getCaption()));

                n++;

                if (limit > 0 && n > limit) {
                    break;
                }
            }

            return sb;

        } else {

            if (imageHolder == null) {

                logger.fine("[ImagesMarker] owner has no images and has no placeholder");
                return null;
            }

            // load image holder to get dimensions
            BufferedImage image;
            try {
                image = ImageIO.read(ResourcesUtils.getRealTemplateData(imageHolder));
            } catch (IOException e) {
                e.printStackTrace();
                logger.warning(imageHolder);
                logger.warning(e.toString());
                return null;
            }

            int[] d;
            if (params.containsKey("cover")) {
                d = Img.getDimensionsToCover(dimensions[0], dimensions[1], image.getWidth(), image.getHeight());
            } else {
                d = Img.getDimensionsToFit(dimensions[0], dimensions[1], image.getWidth(), image.getHeight());
            }

            ImageTag imageTag = new ImageTag(Config.TemplateDataPath + "/" + imageHolder, 0, d[0], d[1]);

            String alt = null;
            if (owner.getAlt() != null) {
                alt = owner.getAlt();
            } else if (params.containsKey("alt")) {
                alt = params.get("alt");
            }
            if (alt != null) {
                imageTag.setAlt(alt);
                imageTag.setTitle(alt);
            }

            boolean link = params.containsKey("link");
            if (link) {

                String clazz = params.get("class");

                LinkTag linkTag = new LinkTag(url, imageTag);
                if (clazz != null) {
                    linkTag.setClass(clazz);
                }

                return linkTag;
            }

            return imageTag;

        }
    }

    private int[] getDimensions(Map<String, String> params) {

        int width, height;

        if (params.containsKey("w")) {
            String value = params.get("w").trim();
            //temporary hack
            Pattern p = Pattern.compile("\\d+");      // the expression
            Matcher m = p.matcher(value);       // the source
            if (m.find()) {
                width = Integer.parseInt(m.group());
            } else {
                width = 0;
            }

        } else if (params.containsKey("width")) {
            width = Integer.parseInt(params.get("width").trim());

        } else {
            width = 0;
        }

        if (params.containsKey("h")) {
            height = Integer.parseInt(params.get("h").trim());

        } else if (params.containsKey("height")) {
            height = Integer.parseInt(params.get("height").trim());

        } else {
            height = 0;
        }

        return new int[]{width, height};
    }

    private Object doTag(RequestContext context, Map<String, String> params, ImageOwner owner, String url, String thumbnailPath, String fullsizePath, int w,
                         int h, int fw, int fh, String description, String alt, String imgCaption) {

        boolean enlarge = params.containsKey("enlarge");
        boolean link = params.containsKey("link");
        boolean gallery = params.containsKey("gallery");
        boolean thickbox = params.containsKey("lightbox") || params.containsKey("thickbox");
        boolean desc = params.containsKey("desc");
        boolean imglink = params.containsKey("imglink");
        boolean relimglink = params.containsKey("relimglink");
        boolean altParam = params.containsKey("alt");
        boolean caption = params.containsKey("caption");

        String title = params.get("title");
        String clazz = params.get("class");
        String container = params.get("container");
        String fullsizeImageId = "fullsize_image";
        Object trail=null;
        if (params.containsKey("trail")){
           trail=context.getAttribute("trailCategory");
        }

        StringBuilder sb = new StringBuilder();

        if (container != null) {
            sb.append("<").append(container).append(">");
        }

        if (caption){
            sb.append("<div class='ec_img'>");
        }
        ImageTag imageTag = new ImageTag(thumbnailPath, 0, w, h);
        String altValue = null;
        if (imgCaption != null) {
            altValue = imgCaption;
        } else if (description != null) {
            altValue = description;
        } else if (alt != null) {
            altValue = alt;
        } else if (altParam) {
            altValue = params.get("alt");
        }
//        if (altParam) {
//            altValue = params.get("alt");
//        } else if (description != null) {
//            altValue = description;
//        } else if (alt != null) {
//            altValue = alt;
//        }
        if (altValue != null) {
            imageTag.setAlt(altValue);
            imageTag.setTitle(altValue);
        }

        // 10/10/06 if lightbox then use thickbox to show the lightbox effect ! (its coool :)
        if (thickbox) {

            LinkTag linkTag = new LinkTag(fullsizePath, imageTag);
            linkTag.setRel(String.valueOf(owner.getId()));

            if (title == null) {

                if (link && url != null) {

                    title = "<a href='" + url + "' title='" + altValue + "'>" + altValue + "</a>".replace("<", "&lt;").replace(">", "&gt;");

                } else {

                    title = altValue;
                }
            }

            if (title != null) {
                linkTag.setTitle(title);
            }

            linkTag.setClass("thickbox");

            sb.append(linkTag);
        }

        // if link then make a link to the image owner
        else if (link) {

            LinkTag linkTag = new LinkTag(url, imageTag);
            if (clazz != null) {
                linkTag.setClass(clazz);
            }

            String target = params.get("target");
            if (target != null) {
                linkTag.setTarget(target);
            }
            if (trail != null) {
                linkTag.setParameter("trailCategory", trail);
            }
            if (altValue!=null){
                linkTag.setAlt(alt);
                linkTag.setTitle(alt);
            }

            sb.append(linkTag);

        }

        /*
           * if enlarge then make a link to the bigger version, using javascript to open up the window
           */
        else if (enlarge) {

            JavascriptLinkTag linkTag = new JavascriptLinkTag(context, fullsizePath, imageTag, fw + 20, fh + 20);
            if (clazz != null) {
                linkTag.setClass(clazz);
            }

            sb.append(linkTag);

        }

        // if gallery then make the image change the fullsize id
        else if (gallery) {

            LinkTag linkTag = new LinkTag("#", imageTag);
            linkTag.setOnClick("document.getElementById('" + fullsizeImageId + "').src = '" + fullsizePath + "'; return false;");

            sb.append(linkTag);
        }

        else if (imglink) {

            LinkTag linkTag = new LinkTag(fullsizePath, imageTag);
            if (clazz != null) {
                linkTag.setClass(clazz);
            }

            if (altValue!=null){
//                linkTag.setAlt(altValue);  //alt is not supported by <a/> 
                linkTag.setTitle(altValue);
            }

            sb.append(linkTag);

        }

        else if (relimglink) {
            StringBuilder sb1 = new StringBuilder(imageTag.toString());
            if (params.containsKey("content") && params.containsKey("contentblock")){
                sb1.append("<").append(params.get("contentblock")).append(">");
                sb1.append(params.get("content"));
                sb1.append("</").append(params.get("contentblock")).append(">");
            }
            RelImageTag relTag = new RelImageTag(sb1);      //todo
            relTag.setRel(fullsizePath);

            sb.append(relTag);
        }

        // ... otherwise just plonk in a thumbnail
        else {

            sb.append(imageTag);
        }

        if (caption &&  imgCaption!= null) {
            sb.append("<div class='ec_imgcaption'>" + imgCaption + "</div>");
        }

        if (desc && description != null) {
            sb.append("<div class='image_desc'>" + description + "</div>");
        }

        if (caption){
            sb.append("</div>");
        }

        if (container != null) {
            sb.append("</").append(container).append(">");
        }

        return sb;
    }

    public Object generate(RequestContext context, Map<String, String> params, BasketLine line) {

        if (line.hasItem()) {

            return doTag(context, params, line.getItem(), line.getItem().getUrl(), null);

        } else {
            return null;
        }
    }

    public Object generate(RequestContext context, Map<String, String> params, Category category) {
        return doTag(context, params, category, category.getUrl(), null);
    }

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

        if (params.containsKey("video-override") && item.hasApprovedRecording()) {
            return null;
        }

        // if we contain the param sublinkonly then only link if we are subscribed
        if (params.containsKey("sublinkonly")) {

            if (!item.getSubscription().hasSubscriptionLevel()) {
                params.remove("link");
            }
        }

        if (params.containsKey("account")) {
            item = item.getAccount();
            if (item == null) {
                return null;
            }
        }

        return doTag(context, params, item, item.getUrl(), item.getItemType().getImageHolder());
    }

    public Object generate(RequestContext context, Map<String, String> params) {
        if (context.containsAttribute("item")) {
            return generate(context, params, (Item) context.getAttribute("item"), null, 0, 0);
        } else if (context.containsAttribute("category")) {
            return generate(context, params, (Category) context.getAttribute("category"));
        }
        return null;
    }

    public Object getRegex() {
        return new String[]{"thumbnails", "images", "image", "thumbnail"};
    }
}
