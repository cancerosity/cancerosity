package org.sevensoft.ecreator.model.media.images.galleries;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * User: MeleshkoDN
 * Date: 20.09.2007
 * Time: 16:56:41
 */
public class GalleriesSortPanel extends Panel {

    private Gallery gallery;
    private ImageSortType currentSortType;
    private Class<? extends Handler> handler;

    protected GalleriesSortPanel(RequestContext context) {
        super(context);
    }

    public GalleriesSortPanel(RequestContext context, Class<? extends Handler> handler, Gallery gallery, ImageSortType currentSortType) {
        this(context);
        this.gallery = gallery;
        this.currentSortType = currentSortType;
        this.handler = handler;
    }

    public void makeString() {

        LinkTag byName = new LinkTag(new Link(handler, null, "gallery", gallery, "imageSortType", prepareSortType(ImageSortType.NAME_ASC)), "Name" + " " + getOrderImage(ImageSortType.NAME_ASC));
        LinkTag byDate = new LinkTag(new Link(handler, null, "gallery", gallery, "imageSortType", prepareSortType(ImageSortType.DATE_ASC)), "Date" + " " + getOrderImage(ImageSortType.DATE_ASC));
        LinkTag byDescription = new LinkTag(new Link(handler, null, "gallery", gallery, "imageSortType", prepareSortType(ImageSortType.DESCRIPTION_ASC)), "Description" + " " + getOrderImage(ImageSortType.DESCRIPTION_ASC));
        LinkTag bySize = new LinkTag(new Link(handler, null, "gallery", gallery, "imageSortType", prepareSortType(ImageSortType.SIZE_ASC)), "Size" + " " + getOrderImage(ImageSortType.SIZE_ASC));
        sb.append(new TableTag("ec_page_control"));
        sb.append("<tr>");
        sb.append("<td>" + byName + "</td>");
        sb.append("<td>" + byDate + "</td>");
        sb.append("<td>" + byDescription + "</td>");
        sb.append("<td>" + bySize + "</td>");
        sb.append("</tr>");
        sb.append("</table>");
        sb.append("<br/>");
        sb.append("<br/>");
    }

    private ImageSortType prepareSortType(ImageSortType sortType) {
        if (currentSortType != null && sortType == currentSortType) {
            return ImageSortType.getInverse(sortType);
        } else {
            return sortType;
        }
    }

    private String getOrderImage(ImageSortType sortType) {
        if (currentSortType != null && prepareSortType(sortType) == prepareSortType(currentSortType)) {
            return currentSortType.getOrder().equals("ASC") ? new ImageTag("files/graphics/admin/sort-asc.gif").toString() : new ImageTag("files/graphics/admin/sort-desc.gif").toString();
        } else {
            return "";
        }
    }
}
