package org.sevensoft.ecreator.model.media.images;

import java.io.File;
import java.io.FilenameFilter;
import java.util.HashSet;
import java.util.Set;

/**
 * @author sks 27-Feb-2006 07:53:23
 *
 */
public class ImageFiletypeFilter implements FilenameFilter {

	private static Set<String>	AcceptedFileTypes;
    private boolean acceptImages;

    public ImageFiletypeFilter() {
        this(true);
    }

    public ImageFiletypeFilter(boolean acceptImages) {
        this.acceptImages = acceptImages;
    }

    static {

		AcceptedFileTypes = new HashSet<String>();
		AcceptedFileTypes.add("jpeg");
		AcceptedFileTypes.add("jpg");
		AcceptedFileTypes.add("gif");
		AcceptedFileTypes.add("png");
	}

	public boolean accept(File dir, String name) {

		if (!name.contains("."))
			return false;

		String ext = name.substring(name.lastIndexOf('.') + 1);

        if (acceptImages)
            return AcceptedFileTypes.contains(ext.toLowerCase());
        else
            return !AcceptedFileTypes.contains(ext.toLowerCase());
            
	}
}
