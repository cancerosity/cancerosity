package org.sevensoft.ecreator.model.media;

import java.util.Collections;
import java.util.List;

import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.media.images.ImageType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 31 Mar 2007 23:02:15
 *
 */
@Table("media_modules")
public class MediaModule extends EntityObject {

	public static MediaModule get(RequestContext context, ItemType itemType) {
		MediaModule module = SimpleQuery.get(context, MediaModule.class, "itemType", itemType);
		return module == null ? new MediaModule(context, itemType) : module;
	}

	private ItemType		itemType;
	private List<String>	imageTypes;

	private MediaModule(RequestContext context) {
		super(context);
	}

	private MediaModule(RequestContext context, ItemType itemType) {
		super(context);
		this.itemType = itemType;

		save();
	}

	public ImageType addImageType(String name) {
		return new ImageType(context, itemType, name);
	}

	public final List<ImageType> getImageTypes() {
		List<ImageType> types = SimpleQuery.execute(context, ImageType.class, "itemType", itemType);
		if (types.isEmpty()) {
			ImageType imageType = new ImageType(context, itemType, "Images");
			return Collections.singletonList(imageType);
		}
		return types;
	}

	public final ItemType getItemType() {
		return itemType.pop();
	}

	public void removeImageType(ImageType imageType) {
		imageType.delete();
	}

	public final void setImageTypes(List<String> imageTypes) {
		this.imageTypes = imageTypes;
	}

}
