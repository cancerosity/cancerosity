package org.sevensoft.ecreator.model.media.images.bot;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;

import javax.imageio.ImageIO;
import java.util.List;
import java.util.logging.Logger;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
public class ImagesRenameBot implements Runnable {

    private static Logger logger = Logger.getLogger("cron");

    private RequestContext context;

    public ImagesRenameBot(RequestContext context) {
        this.context = context;
    }

    public void run() {
        List<Img> images = Img.get(context);
        if (images != null && !images.isEmpty()) {
            for (Img image : images) {
                if (image.getItem() != null) {
                    String itemName = image.getItem().getName().replaceAll("/", "-");
                    if (itemName.length() > 50) {
                        itemName = itemName.substring(0, 50);
                    }
                    if (!itemName.equalsIgnoreCase(image.getName().substring(0, image.getName().lastIndexOf(".")))) {
                        Img newImage = null;
                        try {
                            newImage = image.clone();
                            String extension = image.getExtension();
                            String filename = itemName + "." + extension;
                            String path = ResourcesUtils.getRealImagesPath() + "/" + filename;
                            ImageIO.write(image.getImage(), extension, new FileOutputStream(path));
                            File file = new File(path);
                            if (!file.exists())
                                throw new FileNotFoundException("File not found: " + filename);
                            newImage.setName(filename);
                            newImage.setFilename(filename);
                            newImage.save();
                            image.delete();

                        } catch (CloneNotSupportedException e) {
                            logger.fine("[ImagesRenameBot] clone not suppoted " + e);
                        } catch (FileNotFoundException e) {
                            logger.fine("[ImagesRenameBot] file not found " + e);
                        } catch (IOException e) {
                            logger.fine("[ImagesRenameBot] " + e);
                        }
                    }
                }
            }
        }
    }
}
