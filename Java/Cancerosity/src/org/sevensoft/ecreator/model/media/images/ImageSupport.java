package org.sevensoft.ecreator.model.media.images;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;

import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * @author sks 21 Aug 2006 16:30:50
 */
public abstract class ImageSupport extends EntityObject implements ImageOwner {

    /**
     * Pending images
     */
    private int pendingImageCount;

    private long imageUpdateTimestamp;

    /**
     * Approved images, BC
     */
    private int imageCount;

    private DateTime imageAddedTime;

    private transient List<Img> cachedImages;

    public ImageSupport(RequestContext context) {
        super(context);
    }

    public final Img addImage(Img image) throws IOException, ImageLimitException {
        cachedImages = null;
        return new Img(context, this, image, true, image.isApproved());
    }

    public final Img addImage(String filename, boolean useThumb, boolean smoothing, boolean autoApprove) throws IOException, ImageLimitException {
        cachedImages = null;
        return new Img(context, this, filename, useThumb, smoothing, autoApprove);
    }

    public final Img addImage(Upload upload, boolean useThumb, boolean smoothing, boolean autoApprove) throws IOException, ImageLimitException {
        cachedImages = null;
        return ImageUtil.addImage(context, this, upload, useThumb, smoothing, autoApprove);
    }

    public final Img addImage(URL url, boolean useThumb, boolean smoothing, boolean autoApprove) throws IOException, ImageLimitException {
        cachedImages = null;
        return new Img(context, this, url, useThumb, smoothing, autoApprove);
    }

    public Img addImage(Upload imageUpload, boolean useThumb, boolean smoothing, boolean autoApprove, String caption) throws IOException, ImageLimitException {
        cachedImages = null;
        return ImageUtil.addImage(context, this, imageUpload, useThumb, smoothing, autoApprove, caption);
    }

    public final void copyImagesTo(ImageOwner target) throws IOException, ImageLimitException {
        ImageUtil.copyImagesTo(this, target);
    }

    @Override
    public synchronized boolean delete() {
        removeImages();
        cachedImages = null;
        return super.delete();
    }

    public List<Img> getAllImages() {
        if (cachedImages == null) {
            cachedImages = ImageUtil.getAllImages(context, this);
        }
        return cachedImages;
    }

    public int getAllImagesCount() {
        return ImageUtil.getAllImagesCount(this);
    }

    public String getAlt() {
        return null;
    }

    public Img getAnyImage() {
        return ImageUtil.getAnyImage(context, this);
    }

    public final Img getApprovedImage() {
        return ImageUtil.getApprovedImage(this);
    }

    public final int getApprovedImageCount() {
        return imageCount;
    }

    public final List<Img> getApprovedImages() {
        return ImageUtil.getApprovedImages(context, this);
    }

    public DateTime getImageAddedTime() {
        return imageAddedTime;
    }

    public String getImagePlaceholder() {
        return null;
    }

    public final long getImageUpdateTimestamp() {
        return imageUpdateTimestamp;
    }

    public final int getPendingImageCount() {
        return pendingImageCount;
    }

    public boolean hasAnyImage() {
        return ImageUtil.hasAnyImage(this);
    }

    public final boolean hasApprovedImages() {
        return imageCount > 0;
    }

    public boolean hasImagePlaceholder() {
        return false;
    }

    public final boolean hasMoreApprovedImages() {
        return ImageUtil.hasMoreApprovedImages(this);
    }

    public void imageApproved(Img img) {
    }

    public void imageRejected(Img img) {
    }

    public final boolean isAtImageLimit() {
        return ImageUtil.isAtImageLimit(this);
    }

    public boolean isModeratingImages() {
        return false;
    }

    public final void moveImagesTo(ImageOwner target) throws IOException, ImageLimitException {
        ImageUtil.moveImagesTo(this, target);
        cachedImages = null;
    }

    public final void removeImage(Img image) {
        ImageUtil.removeImage(this, image);
        cachedImages = null;
    }

    public final void removeImage(Img image, boolean deleteFile, boolean deleteThumbnail) {
        ImageUtil.removeImage(this, image, deleteFile, deleteThumbnail);
        cachedImages = null;
    }

    public final void removeImages() {
        ImageUtil.removeImages(this);
        cachedImages = null;
    }

    public final void setImageInfo() {

        this.imageCount = getApprovedImages().size();
        this.pendingImageCount = getAllImages().size() - imageCount;
        this.imageAddedTime = new DateTime();

        save();
    }

    public final void setImageUpdateTimestamp() {
        this.imageUpdateTimestamp = System.currentTimeMillis();
    }

    public boolean useImageDescriptions() {
        return false;
    }

}
