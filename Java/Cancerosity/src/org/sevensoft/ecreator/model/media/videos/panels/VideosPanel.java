package org.sevensoft.ecreator.model.media.videos.panels;

import org.sevensoft.ecreator.model.media.videos.Video;
import org.sevensoft.ecreator.model.media.videos.VideoOwner;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 14 Feb 2007 11:58:31
 *
 */
public class VideosPanel {

	private final RequestContext	context;
	private final VideoOwner	owner;

	public VideosPanel(RequestContext context, VideoOwner owner) {
		this.context = context;
		this.owner = owner;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		for (Video video : owner.getVideos()) {
			sb.append(video.render());
		}

		return sb.toString();
	}

}
