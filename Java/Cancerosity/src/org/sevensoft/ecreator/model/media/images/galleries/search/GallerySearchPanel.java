package org.sevensoft.ecreator.model.media.images.galleries.search;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.media.images.galleries.Gallery;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: MeleshkoDN
 * Date: 03.09.2007
 * Time: 14:03:51 *
 */
public class GallerySearchPanel extends Panel {

    private List<Gallery> galleries;

    public GallerySearchPanel(RequestContext context) {
        super(context);
        galleries = SimpleQuery.execute(context, Gallery.class);
    }

    private SelectTag getSelectGalleriesTag() {
        SelectTag galleriesTag = new SelectTag(context, "gallery");
        galleriesTag.setLabelComparator();
        galleriesTag.setAny("-Any-");
        if (Module.Galleries.enabled(context)) {
            for (Gallery gallery : galleries) {
                galleriesTag.addOption(gallery, gallery.getName());
            }
        }
        return galleriesTag;
    }

    public void makeString() {

        sb.append(new FormTag(GalleryImageSearchHandler.class, null, "POST"));
        sb.append(new AdminTable("Search"));
        sb.append("<tr>");
        sb.append("<td>" + getSelectGalleriesTag() + "</td>");
        sb.append("<td>" + new TextTag(context, "keywords", 30) + "</td>");
        sb.append("<td>" + new SubmitTag("Search") + "</td>");
        sb.append("</table>");
        sb.append("</form>");
    }
}
