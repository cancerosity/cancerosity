package org.sevensoft.ecreator.model.media.images.galleries.search;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.media.images.galleries.GalleriesSortPanel;
import org.sevensoft.ecreator.model.media.images.galleries.Gallery;
import org.sevensoft.ecreator.model.media.images.galleries.ImageSortType;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.*;

import javax.servlet.ServletException;
import java.util.List;

/**
 * View search image results
 *
 * @author dme 3 Sep 2007 14:34:43
 */
@Path("gallery-image-search.do")
public class GalleryImageSearchHandler extends FrontendHandler {

    private static final int LIMIT = 5;

    private int page;

    private String keywords;
    private Gallery gallery;

    private ImageSortType imageSortType;

    private final int height;
    private final int width;

    public GalleryImageSearchHandler(RequestContext context) {
        super(context);
        width = Config.getInstance(context).getThumbnailWidth();
        height = Config.getInstance(context).getThumbnailHeight();
    }

    @Override
    public Object main() throws ServletException {

        GalleryImageSearcher searcher = new GalleryImageSearcher(context);
        searcher.setGallery(gallery);
        searcher.setKeywords(keywords);

        int count = searcher.size();

        final Results results = new Results(count, page, LIMIT);

        searcher.setStart(results.getStartIndex());
        searcher.setLimit(LIMIT);
        final List<Img> images = searcher.execute(imageSortType);

        FrontendDoc doc = new FrontendDoc(context, "Search results");

        doc.addHead(getHead());

        doc.addBody(new Body() {

            @Override
            public String toString() {
                sb.append(new GallerySearchPanel(context));
                sb.append(new GalleriesSortPanel(context, GalleryImageSearchHandler.class, gallery, imageSortType));
                sb.append(new ResultsControl(context, results, new Link(GalleryImageSearchHandler.class, null, "keywords", keywords, "gallery", gallery, "imageSortType", imageSortType)));

                Keypad r = new Keypad(1);
                r.setTableClass("ec_gallery");
                r.setCellVAlign("top");
                r.setTableAlign("center");
                r.setObjects(images);
                r.setRenderer(new Renderer<Img>() {
                    public Object render(Img img) {
                        StringBuilder sb = new StringBuilder();

                        LinkTag linkTag = new LinkTag(img.getPath(), img.getThumbnailTag(width, height));
                        linkTag.setRel(String.valueOf(img.getId()));

                        linkTag.setClass("thickbox");
                        if (img.hasDescription()) {
                            linkTag.setTitle(img.getDescription());
//                            linkTag.setAlt(img.getDescription()); //alt is not supported by <a/>
                        }

                        sb.append("<div class='ec_gallery_img'>" + linkTag + "</div>");

                        sb.append("<div class='ec_gallery_img_name'> Name: " + (img.hasCaption() ? highlightKeywords(img.getCaption(), keywords) : "(empty)") + "</div>");

                        if (img.hasDescription()) {
                            sb.append("<div class='ec_gallery_img_desc'> Description: " + highlightKeywords(StringHelper.toSnippet(img.getDescription(), 200, "..."), keywords) + "</div>");
                        } else {
                            sb.append("<div class='ec_gallery_img_desc'> Description: " + "(empty)" + " </div>");
                        }

                        sb.append("<div class='ec_gallery_img_filename'> Filename: " + highlightKeywords(img.getFilename(), keywords) + "</div>");
                        sb.append("<div class='ec_gallery_img_size'> Size: " + img.getWidth() + "x" + img.getHeight() + "</div>");

                        if (gallery == null || gallery.isShowDateUploaded()) {
                            sb.append("<div class='ec_gallery_img_date'> Date: " + img.getDate().toString("dd MMM yyyy") + "</div>");
                        }

                        if (gallery == null || gallery.isPrintable()) {
                            ImageTag print = new ImageTag("files/js/utils/print.jpeg");
                            print.setOnClick("printImage('" + img.getImagePath() + "'," + img.getWidth() + "," + img.getHeight() + ");");
                            sb.append(print);
                        }
                        return sb;
                    }
                });
                sb.append(r);

                return sb.toString();
            }

        });
        return doc;
    }

    private String highlightKeywords(String initialString, String keywords) {
        if (keywords == null) {
            return initialString;
        }
        if (initialString != null) {
            StringBuffer sb = new StringBuffer(initialString);
            String lowerCasedInitialString = initialString.toLowerCase();
            String lowerCasedKeywords = keywords.toLowerCase();
            int previousIndex = 0;

            while ((previousIndex = lowerCasedInitialString.indexOf(lowerCasedKeywords, previousIndex)) != -1) {
                sb.insert(previousIndex, "<b>");
                int offset = previousIndex + "<b>".length() + keywords.length();
                sb.insert(offset, "</b>");
                previousIndex += offset;
                lowerCasedInitialString = sb.toString().toLowerCase();
            }
            initialString = sb.toString();
        }
        return initialString;
    }

    private String getHead() {
        StringBuffer head = new StringBuffer();
        head.append("<style>");
        head.append("table.ec_gallery { margin-top: 10px; width: 100%; background: #fbfbfb; } ");
        head.append("table.ec_gallery td { padding: 8px; border: 2px solid white;  font-size: 11px; } ");
        head.append("table.ec_gallery div.ec_gallery_img img { padding: 3px; border: 1px solid black; } ");
        head.append("table.ec_gallery div.ec_gallery_img_name { margin-top: 5px; color: #555555; font-size: 15px; font-family: Arial; } ");
        head.append("table.ec_gallery div.ec_gallery_img_date { margin-top: 5px; font-size: 10px; font-weight: bold; } ");
        head.append("table.ec_gallery div.ec_gallery_img_desc { margin-top: 5px; font-size: 11px; } ");
        head.append("table.ec_gallery div.ec_gallery_img_filename { margin-top: 5px; font-size: 11px; } ");
        head.append("table.ec_gallery div.ec_gallery_img_size { margin-top: 5px; font-size: 11px; } ");
        head.append("</style>");
        return head.toString();
    }
}