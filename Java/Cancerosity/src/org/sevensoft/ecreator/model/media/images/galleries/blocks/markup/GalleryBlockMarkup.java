package org.sevensoft.ecreator.model.media.images.galleries.blocks.markup;

import org.sevensoft.ecreator.model.design.markup.MarkupCssClassDefault;
import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * User: Tanya
 * Date: 28.04.2011
 */
public class GalleryBlockMarkup extends MarkupCssClassDefault implements MarkupDefault {

    public String getBetween() {
        return null;
    }

    public String getBody() {
        StringBuilder sb = new StringBuilder();

        sb.append("<div class='ec_gallery_img'>[gallery_image?class=thickbox&link=1]</div>");
        sb.append("<div class='ec_gallery_img_name'>[image_caption]</div>");
        sb.append("<div class='ec_gallery_img_date'>Date: [image_date?format=dd MMM yyyy]</div>");
        sb.append("<div class='ec_gallery_img_desc'>[image_description?suffix=...]</div>");
        sb.append("[print_image]");

        return sb.toString();
    }

    public String getEnd() {
        return null;
    }

    public String getStart() {
        StringBuilder sb = new StringBuilder();
        sb.append("<style>");
        sb.append("table.ec_gallery { margin-top: 10px; width: 100%; background: #fbfbfb; } ");
        sb.append("table.ec_gallery td { padding: 8px; border: 2px solid white;  font-size: 11px; } ");
        sb.append("table.ec_gallery div.ec_gallery_img img { padding: 3px; border: 1px solid black; } ");
        sb.append("table.ec_gallery div.ec_gallery_img_name { margin-top: 5px; color: #555555; font-size: 15px; font-family: Arial; font-weight: bold; } ");
        sb.append("table.ec_gallery div.ec_gallery_img_date { margin-top: 5px; font-size: 10px; font-weight: bold; } ");
        sb.append("table.ec_gallery div.ec_gallery_img_desc { margin-top: 5px; font-size: 11px; } ");
        sb.append("</style>");
        return sb.toString();

    }

    public int getTds() {
        return 0;
    }

    public String getCss() {
        return null;
    }

    public String getName() {
        return "Gallery block markup";
    }

    public String getCssClass() {
        return null;
    }

    public int getCycleRows() {
        return 0;
    }
}
