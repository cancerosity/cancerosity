package org.sevensoft.ecreator.model.media.images;


/**
 * @author sks 05-Oct-2005 00:44:49
 *
 */
public class ImageLimitException extends Exception {

	public ImageLimitException() {
		super();
	}

	public ImageLimitException(String message) {
		super(message);
	}

	public ImageLimitException(String message, Throwable cause) {
		super(message, cause);
	}

	public ImageLimitException(Throwable cause) {
		super(cause);
	}

}
