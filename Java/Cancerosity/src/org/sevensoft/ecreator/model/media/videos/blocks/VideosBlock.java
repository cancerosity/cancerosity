package org.sevensoft.ecreator.model.media.videos.blocks;

import org.sevensoft.ecreator.iface.admin.media.videos.VideosBlockHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.media.videos.VideoOwner;
import org.sevensoft.ecreator.model.media.videos.panels.VideosPanel;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 4 Apr 2007 15:20:27
 *
 */
@Table("videos_block")
@HandlerClass(VideosBlockHandler.class)
@Label("Videos")
public class VideosBlock extends Block {

	public VideosBlock(RequestContext context) {
		super(context);
	}

	public VideosBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, objId);
	}

	@Override
	public Object render(RequestContext context) {

		if (!Module.Videos.enabled(context)) {
			return null;
		}

		VideoOwner owner;
		// get videos for this owner
		if (context.containsAttribute("category")) {

			owner = (Category) context.getAttribute("category");

		} else if (context.containsAttribute("item")) {

			owner = (Item) context.getAttribute("item");

		} else {
			return null;
		}

		return new VideosPanel(context, owner);
	}
}
