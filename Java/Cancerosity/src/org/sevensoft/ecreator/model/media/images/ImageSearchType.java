package org.sevensoft.ecreator.model.media.images;

/**
 * @author sks 2 Feb 2007 16:56:38
 *
 */
public enum ImageSearchType {

	ImagesOnly() {

		@Override
		public String toString() {
			return "Images only";
		}

	},
	NoImages() {

		@Override
		public String toString() {
			return "No images";
		}

	}

}
