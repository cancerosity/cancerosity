package org.sevensoft.ecreator.model.media.images;

import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 1 Apr 2007 00:01:02
 *
 */
@Table("images_types")
public class ImageType extends EntityObject implements Positionable, Selectable {

	private String	name;
	private ItemType	itemType;
	private int		position;

	protected ImageType(RequestContext context) {
		super(context);
	}

	public ImageType(RequestContext context, ItemType itemType, String name) {
		super(context);
		this.itemType = itemType;
		this.name = name;

		save();
	}

	public final ItemType getItemType() {
		return itemType.pop();
	}

	public String getLabel() {
		return getName();
	}

	public final String getName() {
		return name;
	}

	public final int getPosition() {
		return position;
	}

	public String getValue() {
		return getIdString();
	}

	public final void setName(String name) {
		this.name = name;
	}

	public final void setPosition(int position) {
		this.position = position;
	}

}
