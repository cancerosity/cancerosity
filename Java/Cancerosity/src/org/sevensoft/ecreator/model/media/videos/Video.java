package org.sevensoft.ecreator.model.media.videos;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * @author sks 14 Feb 2007 11:37:33
 */
@Table("videos")
public class Video extends EntityObject implements Positionable {

    public static List<Video> get(RequestContext context, VideoOwner owner) {
        return SimpleQuery.execute(context, Video.class, owner.getClass().getSimpleName(), owner);
    }

    public static int getCount(RequestContext context, VideoOwner owner) {
        return SimpleQuery.count(context, Video.class, owner.getClass().getSimpleName(), owner);
    }

    private Category category;

    /**
     * Filename to video
     */
    private String filename;

    /**
     * Auto play ?
     */
    private boolean autoStart;

    /**
     * Show controls ?
     */
    private boolean controls;

    /**
     * Owner item
     */
    private Item item;

    /**
     * Position obviously
     */
    private int position;

    /**
     * Size of the embedded video
     */
    private int width, height;

    protected Video(RequestContext context) {
        super(context);
    }

    public Video(RequestContext context, VideoOwner owner, String filename) throws FileNotFoundException {
        super(context);

        if (owner instanceof Item) {
            this.item = (Item) owner;
        } else if (owner instanceof Category) {
            this.category = (Category) owner;
        } else {
            throw new RuntimeException("Unsuppported video owner");
        }

        this.filename = filename;

        // check filename exists

        File file = getFile();
        if (!file.exists()) {
            throw new FileNotFoundException();
        }

        this.autoStart = true;
        this.controls = true;

        save();
    }

    public Video(RequestContext context, VideoOwner owner, Upload upload) throws IOException {
        super(context);

        if (owner instanceof Item) {
            this.item = (Item) owner;
        } else if (owner instanceof Category) {
            this.category = (Category) owner;
        } else {
            throw new RuntimeException("Unsuppported video owner");
        }

        this.filename = upload.writeUnique(ResourcesUtils.getRealVideoPath());

        this.autoStart = true;
        this.controls = true;

        save();
    }

    public final Category getCategory() {
        return (Category) (category == null ? null : category.pop());
    }

    public File getFile() {
        return ResourcesUtils.getRealVideo(filename);
    }

    public String getFilename() {
        return filename;
    }

    public int getHeight() {
        return height < 10 ? 256 : height;
    }

    public Item getItem() {
        return (Item) (item == null ? null : item.pop());
    }

    public VideoOwner getOwner() {

        if (item != null) {
            return getItem();
        }

        if (category != null) {
            return getCategory();
        }

        return null;
    }

    public String getPath() {
        return Config.VideosPath + "/" + filename;
    }

    public int getPosition() {
        return position;
    }

    public long getSize() {
        return getFile().length();
    }

    public long getSizeKb() {
        return getSize() / 1000;
    }

    public int getWidth() {
        return width < 10 ? 300 : width;
    }

    public boolean hasCategory() {
        return category != null;
    }

    public boolean hasItem() {
        return item != null;
    }

    public boolean isAutoStart() {
        return autoStart;
    }

    private String isAutoStart1() {
        return autoStart ? "1" : "0";
    }

    public boolean isControls() {
        return controls;
    }

    private String isControls1() {
        return controls ? "1" : "0";
    }

    public String render() {

        StringBuilder sb = new StringBuilder();

        sb.append("<div>");
        sb.append("<object id='MediaPlayer1' CLASSID='CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95' "
                + "codebase='http://activex.microsoft.com/activex/controls/mplayer/ en/nsmp2inf.cab#Version=5,1,52,701'");

        sb.append("standby='Loading Microsoft Windows\256 Media Player components...' TYPE='application/x-oleobject' width='" + getWidth() + "' height='"
                + getHeight() + "'>");

        sb.append("<param name='fileName' value='" + getPath() + "'>");
        sb.append("<param name='animationatStart' value='true'>");
        sb.append("<param name='transparentatStart' value='true'>");
        sb.append("<param name='autoStart' value='" + isAutoStart() + "'>");
        sb.append("<param name='showControls' value='" + isControls() + "'>");
        sb.append("<param name='Volume' value='-20'>");

        sb.append("<embed type='application/x-mplayer2' pluginspage='http://www.microsoft.com/Windows/MediaPlayer/' " + "src='" + getPath() + "' "
                + "name='MediaPlayer1' width=" + getWidth() + " height=" + getHeight() + " autostart=" + isAutoStart1() + " " + "showcontrols="
                + isControls1() + " volume=-20>");

        sb.append("</object>");
        sb.append("</div>");

        return sb.toString();
    }

    public void setAutoStart(boolean autoStart) {
        this.autoStart = autoStart;
    }

    public void setControls(boolean controls) {
        this.controls = controls;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setWidth(int width) {
        this.width = width;
    }

}
