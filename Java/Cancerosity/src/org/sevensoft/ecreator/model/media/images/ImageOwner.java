package org.sevensoft.ecreator.model.media.images;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.jeezy.http.Upload;

/**
 * @author sks 04-Nov-2005 09:57:02
 * 
 */
public interface ImageOwner {

	public Img addImage(Img image) throws IOException, ImageLimitException;

	public Img addImage(String filename, boolean useThumb, boolean smoothing, boolean autoApprove) throws IOException, ImageLimitException;

	public Img addImage(Upload upload, boolean useThumb, boolean smoothing, boolean autoApprove) throws IOException, ImageLimitException;

	public Img addImage(URL url, boolean useThumb, boolean smoothing, boolean autoApprove) throws IOException, ImageLimitException;

    public Img addImage(Upload imageUpload, boolean useThumb, boolean smoothing, boolean autoApprove, String caption) throws IOException, ImageLimitException;

	public void copyImagesTo(ImageOwner target) throws IOException, ImageLimitException;

	/**
	 * Returns all images regardless of moderation
	 */
	public List<Img> getAllImages();

	public int getAllImagesCount();

	public String getAlt();

	/**
	 * Returns any image, approved or not
	 */
	public Img getAnyImage();

	/**
	 * Returns the first moderated image on this owner which should be cached
	 */
	public Img getApprovedImage();

	/**
	 * Returns the number of moderated images on this owner
	 */
	public int getApprovedImageCount();

	/**
	 * Returns all moderated images
	 */
	public List<Img> getApprovedImages();

	public int getId();

	public DateTime getImageAddedTime();

	/**
	 * Returns the max number of images this owner can take
	 */
	public int getImageLimit();

	public String getImagePlaceholder();

	public long getImageUpdateTimestamp();

	/**
	 * Returns the number of pending images on this owner
	 */
	public int getPendingImageCount();

	/**
	 * Returns true if this owner has any image, moderated or unmoderated
	 */
	public boolean hasAnyImage();

	/**
	 * Returns true if this owner has at least 1 moderated image
	 */
	public boolean hasApprovedImages();

	public boolean hasImagePlaceholder();

	/**
	 * Returns true if this owner has more than 1 moderated image
	 */
	public boolean hasMoreApprovedImages();

	/**
	 * image approved callback
	 */
	public void imageApproved(Img img);

	/**
	 * 
	 */
	public void imageRejected(Img img);

	public boolean isAtImageLimit();

	/**
	 * Returns true if this owner requires images to be moderated first
	 */
	public boolean isModeratingImages();

	public void moveImagesTo(ImageOwner target) throws IOException, ImageLimitException;

	public void removeImage(Img image);

	public void removeImage(Img image, boolean deleteFile, boolean deleteThumbnail);

	public void removeImages();

	public void save();

	public void setImageInfo();

	public void setImageUpdateTimestamp();

	public boolean useImageDescriptions();

}
