package org.sevensoft.ecreator.model.media.images.galleries.markers;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGalleryImageMarker;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import java.util.Map;

/**
 * User: Tanya
 * Date: 29.04.2011
 */
public class GalleryImageMarker extends MarkerHelper implements IGalleryImageMarker {

    public Object generate(RequestContext context, Map<String, String> params, Img image) {

//        if (g.getWidth() == 0) {
//            width = Config.getInstance(context).getThumbnailWidth();
//        } else {
//            width = g.getWidth();
//        }
//
//        if (g.getHeight() == 0) {
//            height = Config.getInstance(context).getThumbnailHeight();
//        } else {
//            height = g.getHeight();
//        }
        if (params.containsKey("link")) {
            LinkTag linkTag = new LinkTag(image.getPath(), image.getThumbnailTag(/*width, height*/));
            linkTag.setRel(String.valueOf(image.getId()));

            linkTag.setClass(params.get("thickbox"));
            if (image.hasDescription()) {
                linkTag.setTitle(image.getDescription());
//                linkTag.setAlt(image.getDescription());      //alt is not supported by <a/>
            }
            return linkTag;
        }
        return null;
     }

    public Object getRegex() {
        return "gallery_image";
    }
}
