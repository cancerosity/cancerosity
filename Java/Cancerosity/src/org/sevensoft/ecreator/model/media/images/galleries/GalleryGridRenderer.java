package org.sevensoft.ecreator.model.media.images.galleries;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Keypad;
import org.sevensoft.jeezy.http.util.Renderer;
import org.sevensoft.jeezy.http.util.Results;

import java.util.List;
import java.util.logging.Logger;

/**
 * @author sks 18 Jun 2006 20:53:59
 */
public class GalleryGridRenderer {

    private static final Logger logger = Logger.getLogger("ecreator");

    private RequestContext context;
    private final Gallery gallery;
    private final Results results;
    private List<Img> images;
    private StringBuilder sb;

    private int height;
    private int width;

    public GalleryGridRenderer(final RequestContext context, Gallery g, Results results, ImageSortType sortType) {

        this.context = context;
        this.gallery = g;
        this.results = results;
        this.images = gallery.getApprovedImages(sortType);
        this.images = results.subList(images);
        this.sb = new StringBuilder();

        if (g.getWidth() == 0) {
            width = Config.getInstance(context).getThumbnailWidth();
        } else {
            width = g.getWidth();
        }

        if (g.getHeight() == 0) {
            height = Config.getInstance(context).getThumbnailHeight();
        } else {
            height = g.getHeight();
        }

        logger.fine("[GalleryGridRenderer] gallery grid renderer");

        sb.append("<style>");
        sb.append("table.ec_gallery { margin-top: 10px; width: 100%; background: #fbfbfb; } ");
        sb.append("table.ec_gallery td { padding: 8px; border: 2px solid white;  font-size: 11px; } ");
        sb.append("table.ec_gallery div.ec_gallery_img img { padding: 3px; border: 1px solid black; } ");
        sb.append("table.ec_gallery div.ec_gallery_img_name { margin-top: 5px; color: #555555; font-size: 15px; font-family: Arial; font-weight: bold; } ");
        sb.append("table.ec_gallery div.ec_gallery_img_date { margin-top: 5px; font-size: 10px; font-weight: bold; } ");
        sb.append("table.ec_gallery div.ec_gallery_img_desc { margin-top: 5px; font-size: 11px; } ");
        sb.append("</style>");


        Keypad r = new Keypad(gallery.getCols());
        r.setTableClass("ec_gallery");
        r.setCellVAlign("top");
        r.setTableAlign("center");
        r.setObjects(images);
        r.setRenderer(new Renderer<Img>() {

            public Object render(Img img) {

                StringBuilder sb = new StringBuilder();

                LinkTag linkTag = new LinkTag(img.getPath(), img.getThumbnailTag(width, height));
                linkTag.setRel(String.valueOf(img.getId()));

                linkTag.setClass("thickbox");
                if (img.hasDescription()) {
                    linkTag.setTitle(img.getDescription());
//                    linkTag.setAlt(img.getDescription()); //alt is not supported by <a/>
                }

                sb.append("<div class='ec_gallery_img'>" + linkTag + "</div>");

                if (img.hasCaption()) {
                    linkTag.setLabel(img.getCaption());
                } else {
                    linkTag.setLabel(img.getFilename());
                }

                sb.append("<div class='ec_gallery_img_name'>" + linkTag + "</div>");

                if (gallery.isShowDateUploaded()) {
                    sb.append("<div class='ec_gallery_img_date'>Date: " + img.getDate().toString("dd MMM yyyy") + "</div>");
                }

                if (img.hasDescription()) {
                    sb.append("<div class='ec_gallery_img_desc'>" + StringHelper.toSnippet(img.getDescription(), 200, "...") + "</div>");
                }
                if (gallery.isPrintable()) {
                    ImageTag print = new ImageTag("files/js/utils/print.jpeg");
                    print.setOnClick("printImage('" + img.getImagePath() + "'," + img.getWidth() + "," + img.getHeight() + ");");
                    sb.append(print);
                }
                return sb;
            }
        });

        sb.append(r);
    }

    public GalleryGridRenderer(final RequestContext context, Gallery gall, Results results, final ImageSortType sortType, final Markup markup) {
                this.context = context;
        this.gallery = gall;
        this.results = results;
        this.images = gallery.getApprovedImages(sortType);
        this.images = results.subList(images);
        this.sb = new StringBuilder();

        logger.fine("[GalleryGridRenderer] gallery grid renderer");

        Keypad r = new Keypad(gallery.getCols());
        r.setTableClass("ec_gallery");
        r.setCellVAlign("top");
        r.setTableAlign("center");
        r.setObjects(images);
        r.setRenderer(new Renderer<Img>() {

            public Object render(Img img) {

                StringBuilder sb = new StringBuilder();
                MarkupRenderer r = new MarkupRenderer(context, markup);
                r.setBody(img);
                sb.append(r);

                return sb;
            }
        });

        sb.append(r);
    }

    @Override
    public String toString() {
        return sb.toString();
    }
}
