package org.sevensoft.ecreator.model.media.images.galleries;

import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 18 Jun 2006 20:53:59
 *
 */
public class GallerySlideshowRenderer extends Body {

	private Gallery		gallery;
	private RequestContext	context;
	private int			count;
	private int			previous, next;

	public GallerySlideshowRenderer(RequestContext context, Gallery gallery) {
		
		this.context = context;
		this.gallery = gallery;

		if (count < 0 || count > gallery.getApprovedImageCount())
			count = 1;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(new TableTag("gallery_grid"));
		sb.append("<tr>");
		sb.append("<td>");

		Img image = gallery.getApprovedImages().get(count - 1);
		sb.append(image.getImageTag());

		sb.append("</td></tr>");
		sb.append("<tr><td>");
		sb.append(new LinkTag("#", "Previous", count, previous));
		sb.append("</td></td>");
		sb.append(new LinkTag("#", "Next", count, next));
		sb.append("</td></tr>");
		sb.append("</table>");

		return sb.toString();
	}
}
