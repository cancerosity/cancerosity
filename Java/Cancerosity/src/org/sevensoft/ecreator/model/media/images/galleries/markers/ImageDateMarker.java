package org.sevensoft.ecreator.model.media.images.galleries.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGalleryImageMarker;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * User: Tanya
 * Date: 29.04.2011
 */
public class ImageDateMarker extends MarkerHelper implements IGalleryImageMarker {

    public Object generate(RequestContext context, Map<String, String> params, Img image) {
        String format = params.get("format");
        if (format == null) {
            format = "dd MMM yyyy";
        }
        return super.string(context, params, image.getDate().toString(format));
    }

    public Object getRegex() {
        return "image_date";
    }
}
