package org.sevensoft.ecreator.model.items.renderers.presets;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * @author sks 11 Oct 2006 12:21:11
 *
 */
public class AlternativeItemsStyle1 implements MarkupDefault {

	public String getBetween() {
		return null;
	}

	public String getCss() {
		return null;
	}

	public String getBody() {

		return "table.ec_altcon caption { font-weight: bold; text-align: left; padding: 10px; margin-top: 15px; border-top: 1px solid #cccccc; }" +

		"[thumbnail?link=1&w=80&h=80]<br/>[item&link=1][ratings_avg]"
				+ "[pricing_rrp?prefix=List price: ][pricing_sell?prefix=Our price: ][stock?prefix=Availability: ]";
	}

	public String getEnd() {
		return null;
	}

	public String getName() {
		return "Alternatives style 1";
	}

	public String getStart() {
		return null;
	}

	public int getTds() {
		return 4;
	}

}
