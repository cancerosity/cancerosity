package org.sevensoft.ecreator.model.items.highlighted.boxes;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * @author sks 16 Nov 2006 11:10:04
 *
 */
public class LatestNewsMarkup1 implements MarkupDefault {

	public String getBetween() {
		return null;
	}

	public String getCss() {
		return null;
	}

	public String getBody() {

		StringBuilder sb = new StringBuilder();

		sb.append("<div class='headline'>[item?link=1]</div>");
		sb.append("<div class='date'>[date_created]</div>");
		sb.append("<div class='snippet'>[summary?max=150]</div>");

		return sb.toString();
	}

	public String getEnd() {
		return null;
	}

	public String getName() {
		return null;
	}

	public String getStart() {

		StringBuilder sb = new StringBuilder();

		sb.append("<style>");
		sb.append("div.headline { font-size: 11px; font-weight: bold; padding-bottom: 2px; }");
		sb.append("div.date { font-size: 10px; color: #777777; padding-bottom: 3px; }");
		sb.append("div.snippet { font-size: 11px; border-bottom: 1px solid #999999; padding-bottom: 3px; margin-bottom: 5px; }");
		sb.append("</style>");

		return sb.toString();
	}

	public int getTds() {
		return 0;
	}

}
