package org.sevensoft.ecreator.model.items.listings;

import org.sevensoft.commons.samdate.formatters.DateFormatter;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.orders.VatHelper;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 28 Aug 2006 10:08:00
 * 
 * Set up different cost options on a listing package
 *
 */
@Table("listings_packages_rates")
public class ListingRate extends EntityObject implements Selectable {

	/**
	 * How much this listing costs in cash
	 */
	private Money		fee;

	/**
	 * How many credits will be used for this listing package
	 */
	private int			credits;

	/**
	 * The length of time a listing paid for by this rate will stay live for.
	 * A value of zero means a lifetime listing
	 */
	private int			period;

	/**
	 * The listing package that this rate is for
	 */
	private ListingPackage	listingPackage;

	/**
	 * 
	 */
	private double		vatRate;

	protected ListingRate(RequestContext context) {
		super(context);
	}

	public ListingRate(RequestContext context, ListingPackage listingPackage) {
		super(context);

		this.listingPackage = listingPackage;
		this.fee = new Money(1000);
		this.period = 30;
		this.vatRate = 17.5;

		save();
	}

    public ListingRate(RequestContext context, ListingPackage listingPackage, ListingRate rate) {
		super(context);

		this.listingPackage = listingPackage;
		this.fee = rate.getFeeEx();
		this.period = rate.getPeriod();
		this.vatRate = 17.5;
        this.credits = rate.getCredits();

		save();
	}

	public int getCredits() {
		return credits;
	}

	public String getDescription() {
		if (isFree()) {
			return "Free for " + getPeriodDescription();
		} else {
			return Currency.getDefault(context).getSymbol() + getFeeEx() + " for " + getPeriodDescription();
		}
	}

	public Money getFeeEx() {
		return fee;
	}

	/**
	 * 
	 */
	public Money getFeeInc() {
		return VatHelper.inc(context, getFeeEx(), vatRate);
	}

	public String getLabel() {
		return getDescription();
	}

	public ListingPackage getListingPackage() {
		return listingPackage.pop();
	}

	public int getPeriod() {
		return period;
	}

	public String getPeriodDescription() {

		if (period == 0) {
			return "Lifetime listing";
		}

		return DateFormatter.daysToString(period);
	}

	public String getValue() {
		return getIdString();
	}

	public double getVatRate() {
		return vatRate;
	}

	public boolean hasFee() {
		return fee.isPositive();
	}

	public boolean isFree() {
		return fee.isZero();
	}

	public void setCredits(int credits) {
		this.credits = credits;
	}

	public void setFeeEx(Money fee) {
		this.fee = fee;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public void setVatRate(double vatRate) {
		this.vatRate = vatRate;
	}
}
