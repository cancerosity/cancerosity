package org.sevensoft.ecreator.model.items.favourites;

import java.util.Collection;
import java.util.List;
import java.io.IOException;

import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.renderers.ListMarkupDefault;
import org.sevensoft.ecreator.model.items.favourites.box.FavouritesBox;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 23 Aug 2006 12:07:51
 *
 */
@Table("items_favourites_groups")
public class FavouritesGroup extends EntityObject implements Selectable {

	public static List<FavouritesGroup> get(RequestContext context) {
		return SimpleQuery.execute(context, FavouritesGroup.class, "name");
	}

	public static FavouritesGroup getByName(RequestContext context, String name) {
		return SimpleQuery.get(context, FavouritesGroup.class, "name", name);
	}

	private String	name;

	/**
	 * Custom markup if we want to show customised information about these favourites.
	 */
	private Markup	markup;

	/**
	 * Allow users to rename favourites.
	 */
	private boolean	rename;

	/**
	 * The item type to add to this favourites group
	 */
	private ItemType	itemType;

	private String	footer;

	private String	header;

	protected FavouritesGroup(RequestContext context) {
		super(context);
	}

	public FavouritesGroup(RequestContext context, String name) {
		super(context);

		this.name = name;
		save();
	}

	public void addAttribute(Attribute attribute) {
		new FavouritesGroupAttribute(context, this, attribute);
	}

	public boolean contains(Item account, Item item) {
		return SimpleQuery.count(context, Favourite.class, "account", account, "item", item, "favouritesGroup", this) > 0;
	}

	@Override
	public synchronized boolean delete() {

		SimpleQuery.delete(context, FavouritesGroupAttribute.class, "favouritesGroup", this);

		removeFavourites();
		removeBox();
		return super.delete();
	}

	public void email(Collection<String> emails) {
	}

	public List<Attribute> getAttributes() {

		Query q = new Query(context, "select a.* from # a join # fga on a.id=fga.attribute where fga.favouritesGroup=?");
		q.setTable(Attribute.class);
		q.setTable(FavouritesGroupAttribute.class);
		q.setParameter(this);
		return q.execute(Attribute.class);
	}

	/**
	 * 
	 */
	public String getFooter() {
		return footer;
	}

	public String getHeader() {
		return header;
	}

	public ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public String getLabel() {
		return getName();
	}

    public Markup getMarkup() {
        if (markup == null) {
            ListMarkupDefault markupDefault = new ListMarkupDefault();
            markup = new Markup(context, "Favourites list markup", markupDefault);
            try {
                markup.setCss(markupDefault.getCss());
            } catch (IOException e) {
                e.printStackTrace();
            }
            markup.setTableClass(markupDefault.getCssClass());
            markup.setCycleRows(markupDefault.getCycleRows());
            markup.save();
            save();
        }
        return markup.pop();
    }

	public String getName() {
		return name;
	}

	public String getValue() {
		return getIdString();
	}

	public boolean hasFooter() {
		return footer != null;
	}

	/**
	 * 
	 */
	public boolean hasHeader() {
		return header != null;
	}

	public boolean hasItemType() {
		return itemType != null;
	}

	public boolean isRename() {
		return rename;
	}

	public void removeAttribute(Attribute attribute) {
		SimpleQuery.delete(context, FavouritesGroupAttribute.class, "attribute", attribute, "favouritesGroup", this);
	}

	private void removeBox() {
		new Query(context, "update # set favouritesGroup=0 where favouritesGroup=?").setTable(FavouritesBox.class).setParameter(this).run();
	}

	public void removeFavourite(Item account, Item item) {
		SimpleQuery.delete(context, Favourite.class, "account", account, "item", item);
	}

	private void removeFavourites() {

		Query q = new Query(context, "delete from # where favouritesGroup=?");
		q.setTable(Favourite.class);
		q.setParameter(this);
		q.run();
	}

	public final void setFooter(String footer) {
		this.footer = footer;
	}

	public final void setHeader(String header) {
		this.header = header;
	}

	public void setItemType(ItemType itemType) {

		if (ObjectUtil.equal(this.itemType, itemType))
			return;

		/*
		 * If change the item type then reset attributes
		 */
		SimpleQuery.delete(context, FavouritesGroupAttribute.class, "favouritesGroup", this);
		this.itemType = itemType;
	}

	public void setMarkup(Markup markup) {
		this.markup = markup;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRename(boolean personalNames) {
		this.rename = personalNames;
	}

}
