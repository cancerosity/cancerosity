package org.sevensoft.ecreator.model.items.listings.sessions;

import java.util.List;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.frontend.items.listings.ListingPaymentHandler;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.forms.able2buy.CreditGroup;
import org.sevensoft.ecreator.model.ecom.vouchers.Voucher;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;
import org.sevensoft.ecreator.model.items.listings.ListingRate;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.Captions;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 26 May 2006 00:21:07
 *
 */
@Table("listings_session_payment")
public class ListingPaymentSession extends EntityObject implements Payable {

	public static void delete(RequestContext context) {
		SimpleQuery.delete(context, ListingPaymentSession.class, "sessionId", context.getSessionId());
	}

	public static ListingPaymentSession get(RequestContext context) {

		ListingPaymentSession s = SimpleQuery.get(context, ListingPaymentSession.class, "sessionId", context.getSessionId());
		if (s == null)
			s = new ListingPaymentSession(context, true);

		return s;
	}

	/**
	 * Tied to user by session id
	 */
	private String		sessionId;

	/**
	 * The item we are paying for
	 */
	private Item		item;

	/**
	 * Payment method to be used 
	 */
	private PaymentType	paymentType;

	/**
	 * The rate object we have choosen.
	 */
	private ListingRate	listingRate;

	/**
	 * If we have agreed to terms
	 */
	private boolean		agreed;

	private Item		account;

    /**
     * voucher applied during listing setup
     */
    private Voucher voucher;

	public ListingPaymentSession(RequestContext context) {
		super(context);
	}

	public ListingPaymentSession(RequestContext context, boolean b) {
		super(context);
		this.sessionId = context.getSessionId();
		save();
	}

	public CreditGroup getAble2BuyCreditGroup() {
		return null;
	}

	public Card getCard() {
		return null;
	}

	public Item getItem() {
		return (Item) (item == null ? null : item.pop());
	}

	public ListingRate getListingRate() {
		return (ListingRate) (listingRate == null ? null : listingRate.pop());
	}

	public final Item getAccount() {
		return account.pop();
	}

	/**
	 * Returns the applicable listing rates for the package set on this item
	 */
	public List<ListingRate> getListingRates() {
		ListingPackage lp = getItem().getListing().getListingPackage();
		return lp == null ? null : lp.getListingRates();
	}

	public Item getPayableAccount() {
		return getAccount();
	}

	public Address getPaymentAddress() {
		return null;
	}

    public Money getPaymentAmount() {
        Money discount = null;
        if (item.getListing().hasVoucher()) {
            discount = item.getListing().getVoucher().getDiscount(getListingRate().getFeeInc());
        }
        return getListingRate().getFeeInc().minus(discount);
    }

	public String getPaymentDescription() {
        return getListingRate().getPeriodDescription() + " " + Captions.getInstance(context).getListingsCaption() + " for '" + getItem().getName() + "'";
	}

	public String getPaymentFailureUrl(PaymentType paymentType) {
		return Config.getInstance(context).getUrl() + "/" + new Link(ListingPaymentHandler.class);
	}

	public String getPaymentSuccessUrl(PaymentType paymentType) {
		return Config.getInstance(context).getUrl() + "/" + new Link(ListingPaymentHandler.class, "completed");
	}

	public final PaymentType getPaymentType() {
		return paymentType;
	}

    public String getBasketUrl() {
        return null;
    }

    public final String getSessionId() {
		return sessionId;
	}

	public boolean hasItem() {
		return item != null;
	}

	public boolean hasListingRate() {
		return listingRate != null;
	}

	public boolean hasPaymentType() {
		return paymentType != null;
	}

	public final boolean isAgreed() {
		return agreed;
	}

    public Voucher getVoucher() {
        return voucher == null ? null : (Voucher) voucher.pop();
    }

    /**
	 * This method will list a payment after payment has been received
	 */
	public boolean list(Payment payment, String ipAddress) {

		if (listingRate == null)
			return false;

		if (item == null)
			return false;

		getItem().getListing().list(getListingRate(), payment, ipAddress);
		return true;
	}

	public final void setAgreed(boolean agreed) {
		this.agreed = agreed;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public void setListingRate(ListingRate listingRate) {
		this.listingRate = listingRate;
	}

	public final void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	
	public final void setAccount(Item account) {
		this.account = account;
		save("account");
	}

    public void setVoucher(Voucher voucher) {
        this.voucher = voucher;
    }
}
