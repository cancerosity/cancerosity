package org.sevensoft.ecreator.model.items.favourites.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.items.favourites.FavouritesHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.favourites.FavouritesGroup;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.db.EntityObject;

/**
 * @author sks 03-Apr-2006 14:27:16
 *
 */
public class AddFavouriteMarker extends MarkerHelper implements IItemMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

        String group = params.get("group");
        if (group == null) {

            String name = params.get("name");
            if (name == null) {
                return null;
            }

            FavouritesGroup g = FavouritesGroup.getByName(context, name);
            if (g == null) {
                return null;
            } else {
                group = g.getIdString();
            }
        }

        FavouritesGroup favouritesGroup = EntityObject.getInstance(context, FavouritesGroup.class, group);
        if (favouritesGroup.contains((Item) context.getAttribute("account"), item)) {
            return null;
        }

        return super.link(context, params, new Link(FavouritesHandler.class, "add", "item", item, "favouritesGroup", group), null);
    }

	public Object getRegex() {
		return "add_favourites";
	}

}
