package org.sevensoft.ecreator.model.items.adminsearch;

import java.util.List;

import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 9 Jan 2007 15:05:17
 * 
 * Determines which fields appear in search results in admin
 *
 */
@Table("items_types_fields")
public class AdminField extends EntityObject implements Positionable {

	public static List<AdminField> get(RequestContext context, ItemType itemType) {
		return SimpleQuery.execute(context, AdminField.class, "itemType", itemType);
	}

	private int			position;
	private ItemType		itemType;
	private AdminFieldType	fieldType;
	private Attribute		attribute;
    private boolean searchResultsEditable;

	protected AdminField(RequestContext context) {
		super(context);
	}

	public AdminField(RequestContext context, ItemType itemType, AdminFieldType fieldType) {
		super(context);
		this.itemType = itemType;
		this.fieldType = fieldType;
		save();
	}

	public Attribute getAttribute() {
		return (Attribute) (attribute == null ? null : attribute.pop());
	}

	public AdminFieldType getFieldType() {
		return fieldType;
	}

	public ItemType getItemType() {
		return itemType;
	}

    public boolean isSearchResultsEditable() {
        return searchResultsEditable;
    }

    public String getName() {
		
		switch (getFieldType()) {

		case Attribute:
			if (attribute == null) {
				return "None";
			} else {
				return getAttribute().getName();
			}

		default:
			return fieldType.toString();
		}
	}

	public int getPosition() {
		return position;
	}

	public final void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

	public final void setFieldType(AdminFieldType fieldType) {
		this.fieldType = fieldType;
	}

    public void setSearchResultsEditable(boolean searchResultsEditable) {
        this.searchResultsEditable = searchResultsEditable;
    }

    public void setPosition(int position) {
		this.position = position;
	}

}
