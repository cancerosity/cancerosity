package org.sevensoft.ecreator.model.items.pricing;

import java.util.Collection;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 8 Oct 2006 14:59:39
 *
 */
@Table("prices_settings")
@Singleton
public class PriceSettings extends EntityObject implements Priceable {

	public enum PriceRounding {
		None() {

			@Override
			public Money round(Money cheapest, int roundto) {
				return cheapest;
			}

		},
		Up() {

			@Override
			public Money round(Money cheapest, int roundto) {

				if (roundto == 0)
					return cheapest;

				int j = cheapest.getAmount() / roundto;
				return new Money((j + 1) * roundto);
			}
		},
		Nearest() {

			@Override
			public Money round(Money cheapest, int roundto) {

				if (roundto == 0)
					return cheapest;

				int r = cheapest.getAmount() % roundto;

				if (r < roundto / 2)
					return Down.round(cheapest, roundto);
				else
					return Up.round(cheapest, roundto);

			}
		},
		Down() {

			@Override
			public Money round(Money cheapest, int roundto) {

				if (roundto == 0)
					return cheapest;

				int i = cheapest.getAmount() / roundto;
				return new Money(i * roundto);

			}
		};

		public abstract Money round(Money cheapest, int roundTo);
	}

	public enum PricingType {
		Markup, Margin
	}

	public static PriceSettings getInstance(RequestContext context) {
		return EntityObject.getSingleton(context, PriceSettings.class);
	}

	/**
	 * Price breaks for price settings here
	 */
	private TreeSet<Integer>	priceBreaks;

	/**
	 * Price breaks that are used if categories and items do not have more specific price breaks set
	 */
	private TreeSet<Integer>	genericPriceBreaks;

	/**
	 * If we are using markup or margin when calculating percentage based pricing
	 */
	private PricingType		pricingType;

	/**
	 * This discount applies to all products across the entire site
	 */
	private Amount			globalDiscount;

	/**
	 * Enables price bands across all pricing
	 */
	private boolean			usePriceBands;

	/**
	 * Enables price breaks
	 */
	private boolean			usePriceBreaks;

	private transient List<Price>	prices;

	private boolean			collectionCharges;

	private PriceSettings(RequestContext context) {
		super(context);
	}

	public void clearPriceCaches() {
		PriceUtil.clearPriceCaches(context);
	}

	public final TreeSet<Integer> getGenericPriceBreaks() {
		return genericPriceBreaks;
	}

	public final Amount getGlobalDiscount() {
		return globalDiscount;
	}

	public SortedSet<Integer> getPriceBreaks() {

		if (priceBreaks == null) {
			priceBreaks = new TreeSet();
		}

		priceBreaks.add(1);
		return priceBreaks;
	}

	public Price getPriceDefinition(PriceBand memberGroup, int qty, Money costPriceFrom, Money costPriceTo) {
		return null;
	}

	public List<Price> getPrices() {
		if (prices == null) {
			prices = SimpleQuery.execute(context, Price.class, "priceSettings", this);
		}
		return prices;
	}

	public PricingType getPricingType() {
		return pricingType == null ? PricingType.Markup : pricingType;
	}

	/**
	 * Returns true if cost pricing is enabled on any item type
	 */
	public boolean isCostPricing() {

		for (ItemType itemType : ItemType.get(context)) {
			if (itemType.isCostPricing()) {
				return true;
			}
		}

		return false;
	}

	public boolean isPriceBreaks() {
		return getPriceBreaks().size() > 1;
	}

	public final boolean isUsePriceBands() {
		return usePriceBands;
	}

	public final boolean isUsePriceBreaks() {
		return usePriceBreaks;
	}

	public void removePrices() {
		PriceUtil.removePriceDefinitions(context, this);
	}

	public final void setGlobalDiscount(Amount globalDiscount) {
		this.globalDiscount = globalDiscount;
	}

	public void setPriceBreaks(Collection<Integer> breaks) {

		this.priceBreaks = new TreeSet<Integer>();
		this.priceBreaks.addAll(breaks);

		PriceUtil.syncPriceBreaks(context, this, breaks);
		save();
	}

	public void setPricingType(PricingType pt) {

		if (pt == pricingType) {
			return;
		}

		this.pricingType = pt;
		clearPriceCaches();
	}

	public void setSellPrice(PriceBand priceBand, int qty, Amount amount) {
		new Price(context, this, priceBand, qty, amount);
	}

	public final void setUsePriceBands(boolean usePriceBands) {
		this.usePriceBands = usePriceBands;
	}

	public final void setUsePriceBreaks(boolean usePriceBreaks) {
		this.usePriceBreaks = usePriceBreaks;
	}

	/**
	 * 
	 */
	public void setCollectionCharges(boolean collectionCharges) {
		this.collectionCharges = collectionCharges;
	}

	public boolean isCollectionCharges() {
		return collectionCharges;
	}

}
