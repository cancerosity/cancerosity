package org.sevensoft.ecreator.model.items.listings.bots;

import org.apache.commons.lang.time.DateUtils;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.listings.Listing;
import org.sevensoft.ecreator.model.items.listings.ListingSettings;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

/**
 * @author sks 05-Dec-2005 22:11:22
 */
public class ListingExpiryBot implements Runnable {

    private Logger logger = Logger.getLogger("cron");
    private RequestContext context;

    public ListingExpiryBot(RequestContext context) {
        this.context = context;
    }

    /**
     * Runs through all member listings that are live and checks they have not yet expired.
     */
    public void run() {
        logger.config("Listing Expiry Reminder Bot");
        ListingSettings ls = ListingSettings.getInstance(context);
        Set<Integer> expiryReminderDays = ls.getExpiryReminderDays();
        if (expiryReminderDays.isEmpty()) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        Date now = new Date();
        for (Integer reminderDay : expiryReminderDays) {
            sb.append(DateUtils.truncate(now, Calendar.DATE).getTime() + reminderDay * DateUtils.MILLIS_IN_DAY).append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        Query q = new Query(context, "SELECT i.* FROM # i JOIN # l ON i.id=l.item WHERE l.expirydate IN (" + sb + ") and " +
                "i.status like 'Live'");
        q.setTable(Item.class);
        q.setTable(Listing.class);
        List<Item> expiringItems = q.execute(Item.class);
        for (Item item : expiringItems) {
            try {
                item.getListing().sendEmailExpiryReminder();
            } catch (EmailAddressException e) {
                e.printStackTrace();
            } catch (SmtpServerException e) {
                e.printStackTrace();
            }
        }

        logger.severe("[ListingExpiryBot] expiring items number: " + expiringItems.size());
    }
}
