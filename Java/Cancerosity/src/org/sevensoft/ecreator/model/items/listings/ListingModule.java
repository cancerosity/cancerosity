package org.sevensoft.ecreator.model.items.listings;

import java.util.List;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Jan 2007 11:40:03
 *
 */
@Table("listings_module")
public class ListingModule extends EntityObject {

	public static ListingModule get(RequestContext context, ItemType itemType) {
		ListingModule module = SimpleQuery.get(context, ListingModule.class, "itemType", itemType);
		return module == null ? new ListingModule(context, itemType) : module;
	}

	/**
	 * Returns true if the email address has not been used by another account
	 */
	public static boolean isUniqueEmail(RequestContext context, String email) {
		return SimpleQuery.count(context, Item.class, "email", email) == 0;
	}

	private ItemType	itemType;

	private int		maxListings;

	private ListingModule(RequestContext context) {
		super(context);
	}

	private ListingModule(RequestContext context, ItemType it) {
		super(context);
		this.itemType = it;

		save();
	}

	public ListingPackage addListingPackage(String name) {
		return new ListingPackage(context, itemType, name);
	}

	public final ItemType getItemType() {
		return itemType.pop();
	}

	/**
	 * Returns all listing packages that are defined for this item type
	 */
	public List<ListingPackage> getListingPackages() {
		return SimpleQuery.execute(context, ListingPackage.class, "itemType", itemType, "deleted", 0);
	}

	public final int getMaxListings() {
		return maxListings;
	}

	public void removeListingPackage(ListingPackage listingPackage) {
		listingPackage.delete();
	}

	public final void setMaxListings(int maxListings) {
		this.maxListings = maxListings;
	}

}
