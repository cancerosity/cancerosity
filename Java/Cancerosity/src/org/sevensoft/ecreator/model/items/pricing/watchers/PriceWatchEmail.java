package org.sevensoft.ecreator.model.items.pricing.watchers;

import java.util.List;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 18-Sep-2005 00:12:19
 * 
 */
public class PriceWatchEmail extends Email {
    private RequestContext context;

	public PriceWatchEmail(Item item, List<String> priceWatchEmails, RequestContext context) {

		final Config config = Config.getInstance(context);
		final Company company = Company.getInstance(context);

		StringBuilder sb = new StringBuilder();
		sb.append("Hi, you asked us to tell you when the price changed on\n");
		sb.append(item.getName());
		sb.append(".\n\n");
		sb.append("The new price of is \243");
	//	sb.append(item.getSalePriceInc());
		sb.append("\n\nClick here to view\n");
		sb.append(item.getUrl());
		setBody(sb);

		setSubject("Price change on " + item.getName());

		setFrom(company.getName(), config.getServerEmail());
		setTo(config.getServerEmail());

		for (String string : priceWatchEmails)
			addBcc(string);
	}

    @Override
	public void send(String hostname) throws EmailAddressException, SmtpServerException {
		new EmailDecorator(context, this).send(hostname);
	}

}
