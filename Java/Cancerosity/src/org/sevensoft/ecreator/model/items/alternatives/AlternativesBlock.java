package org.sevensoft.ecreator.model.items.alternatives;

import java.util.List;

import org.sevensoft.ecreator.iface.admin.items.modules.alternatives.AlternativesBlockHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 18 Apr 2007 10:18:31
 *
 */
@Table("blocks_alternatives")
@Label("Alternatives")
@HandlerClass(AlternativesBlockHandler.class)
public class AlternativesBlock extends Block {

	public AlternativesBlock(RequestContext context) {
		super(context);
	}

	public AlternativesBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, objId);

		save();
	}

	@Override
	public Object render(RequestContext context) {

		Item item = (Item) context.getAttribute("item");
		if (item == null) {
			return null;
		}

		AlternativesModule module = item.getItemType().getAlternativesModule();

		List<Item> alternatives = item.getAutoAlternatives(module.getAlternativesQty());
		if (alternatives == null || alternatives.isEmpty()) {
			logger.fine("[AlternativesBlock]no alternatives found, exiting");
			return null;
		}

		logger.fine("[AlternativesBlock] found " + alternatives.size() + " alts");

		StringBuilder sb = new StringBuilder();

		String caption = module.getAlternativesCaption();

		if (caption != null) {

			sb.append("<div class='alternatives_caption'>");
			sb.append(caption);
			sb.append("</div>");
		}

		MarkupRenderer r = new MarkupRenderer(context, module.getAlternativesMarkup());
		r.setBodyObjects(alternatives);
		sb.append(r);

		return sb;
	}

}
