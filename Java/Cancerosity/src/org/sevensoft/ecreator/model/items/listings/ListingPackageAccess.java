package org.sevensoft.ecreator.model.items.listings;

import org.sevensoft.ecreator.model.accounts.permissions.AccessGroup;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 30 May 2006 15:21:00
 * 
 * When can we access this listing package ?
 *
 */
@Table("listings_packages_access")
public class ListingPackageAccess extends EntityObject {

	private ListingPackage		listingPackage;
	private ItemType			itemType;
	private SubscriptionLevel	subscriptionLevel;

	public ListingPackageAccess(RequestContext context) {
		super(context);
	}

	public ListingPackageAccess(RequestContext context, ListingPackage listingPackage, ItemType itemType) {
		super(context);

		if (!listingPackage.getAccountItemTypes().contains(itemType)) {

			this.listingPackage = listingPackage;
			this.itemType = itemType;

			save();
		}
	}

	public ListingPackageAccess(RequestContext context, ListingPackage listingPackage, SubscriptionLevel subscriptionLevel) {
		super(context);

		if (!listingPackage.getSubscriptions().contains(subscriptionLevel)) {

			this.listingPackage = listingPackage;
			this.subscriptionLevel = subscriptionLevel;

			save();
		}
	}

	public final ItemType getItemType() {
		return itemType.pop();
	}

	public final ListingPackage getListingPackage() {
		return listingPackage.pop();
	}

	public final AccessGroup getSubscriptionLevel() {
		return subscriptionLevel.pop();
	}

}
