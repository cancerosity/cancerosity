package org.sevensoft.ecreator.model.items.favourites.markers;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.favourites.Favourite;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.iface.frontend.items.favourites.FavouritesnewHandler;

import java.util.Map;

/**
 * User: Tanya
 * Date: 23.08.2010
 */
public class FavouriteAddMarker extends MarkerHelper implements IItemMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
        Item account = (Item) context.getAttribute("account");
        if (account == null) {
            return null;
        }

        if (Favourite.contains(context, account, item)) {
            return null;
        }

        return super.link(context, params, new Link(FavouritesnewHandler.class, "add", "item", item), null);
    }

    public Object getRegex() {
        return "favourites_add";
    }
}
