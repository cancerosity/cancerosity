package org.sevensoft.ecreator.model.items.advanced;

import java.io.IOException;
import java.util.List;

import org.sevensoft.commons.skint.Amount;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 17 Mar 2007 10:23:57
 *
 */
public class ItemActioner {

	public enum Action {

		MoveCategory("Move to category"), AddCategory("Add category"), CopyImages("Copy images"), RemoveCategory("Remove category"), RemoveAllCategories(
				"Remove all categories"), SetStatus("Change status"), SetVatRate("Set vat rate"), CopyOptions("Copy item options"), CopyContent(
				"Copy content"), TitleCase("Rename to title case"),  SetSellPrice("Set standard sell price"), CopyItem(
				"Copy item with find/replace"), AttributeValue("Set attribute value");
		
		// RemoveImages("Remove all images"),

		private final String	toString;

		private Action() {
			this.toString = name();
		}

		private Action(String toString) {
			this.toString = toString;
		}

		@Override
		public String toString() {
			return toString;
		}
	}

	private final ItemSearcher	searcher;
	private final RequestContext	context;
	private int				count;

	public ItemActioner(RequestContext context, ItemSearcher searcher) {
		this.context = context;
		this.searcher = searcher;
	}

	public void addCategory(Category category) {

		count = 0;

		for (Item item : searcher) {
			item.addCategory(category);
			count++;
		}
	}

	public void copyContent(Item sourceItem) {

		count = 0;

		String content = sourceItem.getContent();

		for (Item item : searcher) {

			item.setContent(content);
			item.save();

			count++;
		}

	}

	public void copyImages(Item src) throws IOException, ImageLimitException {

		count = 0;

		List<Img> images = src.getAllImages();
		if (images.isEmpty()) {
			return;
		}

		for (Item item : searcher) {
			for (Img img : images) {
				item.addImage(img);
			}
			count++;
		}
	}

	public void copyItem(Item sourceItem, String target, String replacement) throws CloneNotSupportedException {

		count = 0;

		for (Item item : searcher) {
			item.clone(target, replacement);
			count++;
		}
	}

	public void copyOptions(Item src) {

		count = 0;

		List<ItemOption> options = src.getOptionSet().getOptions();
		if (options.isEmpty()) {
			return;
		}

		for (Item item : searcher) {
			for (ItemOption option : options) {
				option.copyTo(item);
			}
			count++;
		}

	}

	public final int getCount() {
		return count;
	}

	public void moveCategory(Category actionCategory) {

		count = 0;

		for (Item item : searcher) {
			item.removeCategories();
			item.addCategory(actionCategory);

			count++;
		}

	}

	public void removeCategories() {

		count = 0;

		for (Item item : searcher) {
			item.removeCategories();
			count++;
		}
	}

	public void removeCategory(Category category) {

		count = 0;

		for (Item item : searcher) {
			item.removeCategory(category);
			count++;
		}
	}

	public void removeImages() {

		count = 0;

		for (Item item : searcher) {
			item.removeImages();
			count++;
		}
	}

	public void setAttributeValue(Attribute attribute, String value) {

		count = 0;

		for (Item item : searcher) {
			item.setAttributeValue(attribute, value);
			count++;
		}

	}

	public void setSellPrice(Amount amount) {

		count = 0;

		for (Item item : searcher) {
			item.setSellPrice(amount);
			count++;
		}
	}

	public void setStatus(String status) {

		count = 0;

		for (Item item : searcher) {

			item.setStatus(status);
			item.save();

			count++;
		}
	}

	public void setVatRate(double vatRate) {

		count = 0;

		for (Item item : searcher) {

			item.setVatRate(vatRate, true);

			count++;
		}
	}

	public void titleCase() {

		count = 0;

		for (Item item : searcher) {
			item.setTitleCase();
			item.save();

			count++;
		}
	}
}
