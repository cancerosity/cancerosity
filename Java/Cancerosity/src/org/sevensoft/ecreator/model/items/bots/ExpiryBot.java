package org.sevensoft.ecreator.model.items.bots;

import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.Period;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeValue;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.misc.Interval;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 7 Sep 2006 15:38:15
 *
 * Expire items based on date attributes
 */
@Table("items_bots_expiry")
public class ExpiryBot extends EntityObject {

	public static List<ExpiryBot> get(RequestContext context) {
		Query q = new Query(context, "select * from #");
		q.setTable(ExpiryBot.class);
		return q.execute(ExpiryBot.class);
	}

	public static List<ExpiryBot> get(RequestContext context, Interval interval) {
		return SimpleQuery.execute(context, ExpiryBot.class, "interval", interval);
	}

	public static List<ExpiryBot> get(RequestContext context, ItemType itemType) {
		return SimpleQuery.execute(context, ExpiryBot.class, "itemType", itemType);
	}

	@SuppressWarnings("hiding")
	private Logger	logger	= Logger.getLogger("cron");
	private ItemType	itemType;
	private Attribute	attribute;
	private Period	period;
	private Interval	interval;

	public ExpiryBot(RequestContext context) {
		super(context);
	}

	public ExpiryBot(RequestContext context, ItemType itemType) {
		super(context);
		this.itemType = itemType;
		save();
	}

	public Attribute getAttribute() {
		return (Attribute) (attribute == null ? null : attribute.pop());
	}

	public List<Attribute> getAttributes() {

		List<Attribute> attributes = getItemType().getAttributes();
		CollectionsUtil.filter(attributes, new Predicate<Attribute>() {

			public boolean accept(Attribute e) {
				switch (e.getType()) {

				default:
					return false;

				case Date:
				case DateTime:
					return true;

				}
			}
		});
		return attributes;
	}

	public final Interval getInterval() {
		return interval == null ? Interval.Manual : interval;
	}

	public ItemType getItemType() {
		return itemType.pop();
	}

	public Period getPeriod() {
		return period;
	}

	public boolean hasAttribute() {
		return attribute != null;
	}

	public boolean hasPeriod() {
		return period != null;
	}

	public int run() {

		// if no period defined then exit
		if (period == null) {
			logger.warning("[ExpiryBot] No period defined for this expiry bot: " + period);
			return 0;
		}

		Query q = new Query(context, "select i.* from # i join # av on i.id=av.item where av.attribute=? and av.value<=?");
		q.setTable(Item.class);
		q.setTable(AttributeValue.class);
		q.setParameter(attribute);

		switch (getAttribute().getType()) {

		default:
			logger.warning("[ExpiryBot] expiry bot attribute is not date botid=" + getId() + ", attribute=" + getAttribute() + ", type="
					+ getAttribute().getType());
			return 0;

		case Date:
			q.setParameter(period.remove(new Date()));
			break;

		case DateTime:
			q.setParameter(period.remove(new Date()));
			break;

		}

		int n = 0;
		for (Item item : q.execute(Item.class)) {
			item.delete();
			n++;
		}

		logger.config("[ExpiryBot] expiry bot deleted " + n + " items (botid=" + getId() + ")");

		return n;
	}

	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

	public final void setInterval(Interval interval) {
		this.interval = interval;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	@Override
	public String toString() {
		return ObjectUtil.fieldsToString(this);
	}

}
