package org.sevensoft.ecreator.model.items.pricing.panels;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.model.items.pricing.PriceSettings;
import org.sevensoft.ecreator.model.items.pricing.Priceable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 18 Jan 2007 12:48:07
 */
public class PriceBreaksPanel {

    private final PriceSettings priceSettings;
    private final RequestContext context;
    private final Priceable priceable;
    private final int span;

    public PriceBreaksPanel(RequestContext context, Priceable priceable, int span) {
        this.context = context;
        this.priceable = priceable;
        this.span = span;
        this.priceSettings = PriceSettings.getInstance(context);
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        if (priceSettings.isUsePriceBreaks()) {
            sb.append(new AdminRow("Price breaks", "Setup the desired price breaks here.", new TextTag(context, "priceBreaks", StringHelper.implode(
                    priceable.getPriceBreaks(), " ", true), 20)).setSpan(span));
        }

        return sb.toString();
    }

}
