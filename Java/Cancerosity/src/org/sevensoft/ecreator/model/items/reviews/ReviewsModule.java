package org.sevensoft.ecreator.model.items.reviews;

import org.sevensoft.ecreator.model.attributes.support.AttributeOwnerSupport;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 16 Jan 2007 15:20:03
 *
 */
@Table("reviews_module")
public class ReviewsModule extends AttributeOwnerSupport {

	public static ReviewsModule get(RequestContext context, ItemType itemType) {
		ReviewsModule module = SimpleQuery.get(context, ReviewsModule.class, "itemType", itemType);
		return module == null ? new ReviewsModule(context, itemType) : module;
	}

	private ItemType	itemType;
	private String	rejectionEmailText;
	private String	approvalEmailText;

	private ReviewsModule(RequestContext context) {
		super(context);
	}

	public ReviewsModule(RequestContext context, ItemType itemType) {
		super(context);
		this.itemType = itemType;
		save();
	}

	public final String getApprovalEmailText() {
		return approvalEmailText;
	}

	public final ItemType getItemType() {
		return itemType.pop();
	}

	public final String getRejectionEmailText() {
		return rejectionEmailText;
	}

	public final void setApprovalEmailText(String approvalEmailText) {
		this.approvalEmailText = approvalEmailText;
	}

	public final void setRejectionEmailText(String rejectionEmailText) {
		this.rejectionEmailText = rejectionEmailText;
	}

}
