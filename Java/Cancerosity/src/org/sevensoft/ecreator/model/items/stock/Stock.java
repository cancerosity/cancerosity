package org.sevensoft.ecreator.model.items.stock;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 21 Jan 2007 14:04:44
 *
 */
@Table("stock")
public class Stock extends EntityObject {

	public static Stock get(RequestContext context, Item item) {
		Stock stock = SimpleQuery.get(context, Stock.class, "item", item);
		return stock == null ? new Stock(context, item) : stock;
	}

	private Item	item;

	protected Stock(RequestContext context) {
		super(context);
	}

	public Stock(RequestContext context, Item item) {
		super(context);
		this.item = item;
		save();
	}

	public final Item getItem() {
		return item;
	}

}
