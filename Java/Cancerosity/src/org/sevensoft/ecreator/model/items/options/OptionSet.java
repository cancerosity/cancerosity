package org.sevensoft.ecreator.model.items.options;

import java.util.List;
import java.util.ArrayList;

import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 25 Aug 2006 14:27:44
 *
 */
@Table("items_options_set")
public class OptionSet extends EntityObject {

	/**
	 * Minimum number of options that must be set, regardless of what they are
	 */
	private int		minimumOptionSelection;

	/**
	 * Assigned to this item
	 */
	private Item	item;

	/**
	 * Markup object for options
	 */
	private Markup	optionsMarkup;

    /**
	 * Assigned to this itemType
	 */
	private ItemType itemType;

	private OptionSet(RequestContext context) {
		super(context);
	}

	public OptionSet(RequestContext context, Item item) {
		super(context);
		this.item = item;
		save();
	}

    public OptionSet(RequestContext context, ItemType itemType) {
        this(context);
        this.itemType = itemType;
        save();
    }

    public ItemOption addOption(String name) {

		ItemOption option = new ItemOption(context, getItem(), name);

		item.setOptionCount();
		save();

		return option;
	}

    public ItemOption addOption(String name, ItemType itemType) {

        ItemOption option = new ItemOption(context, itemType, name);

//		item.setOptionCount();
//		save();

        return option;
    }

	public void copyOptionsTo(Item target) {

		if (equals(target)) {
			return;
		}

		for (ItemOption option : getOptions()) {
			option.copyTo(target);
		}

	}

	@Override
	public synchronized boolean delete() {

		for (ItemOption option : getOptions())
			option.delete();

		return super.delete();
	}

	public Item getItem() {
		return item.pop();
	}

	public int getMinimumOptionSelection() {
		return minimumOptionSelection;
	}

	/**
	 * Returns the option with this id
	 */
	public ItemOption getOption(int i) {
		for (ItemOption option : getOptions())
			if (option.getId() == i) {
				return option;
			}
		return null;
	}

    public List<ItemOption> getOptions() {
        if (item != null) {
            return SimpleQuery.execute(context, ItemOption.class, "item", item);
        } else if (itemType != null) {
            return SimpleQuery.execute(context, ItemOption.class, "itemType", itemType);
        }

        return new ArrayList<ItemOption>();
    }

	public Markup getOptionsMarkup() {
		return optionsMarkup == null ? null : optionsMarkup;
	}

	public boolean hasOptionsMarkup() {
		return optionsMarkup != null;
	}

	public void removeOption(ItemOption option) {

		option.delete();
		getItem().setOptionCount();
		save();
	}

    public void removeITypeOption(ItemOption option) {

		option.delete();
	}

	public void removeOptions() {
		SimpleQuery.delete(context, ItemOption.class, "item", item);

		getItem().setOptionCount();
		save();
	}

	public void setMinimumOptionSelection(int minimumOptionSelection) {
		this.minimumOptionSelection = minimumOptionSelection;
	}

	public void setOptionsMarkup(Markup m) {
		this.optionsMarkup = m;
	}

    public void addOption(ItemOption addPresetOption, Item item) {
        addPresetOption.copyTo(item);
    }
}
