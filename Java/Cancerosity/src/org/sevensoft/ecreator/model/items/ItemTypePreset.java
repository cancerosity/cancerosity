package org.sevensoft.ecreator.model.items;

import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategorySettings;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.extras.links.LinkType;
import org.sevensoft.ecreator.model.items.sorts.ItemSort;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.modules.Modules;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 03-Apr-2006 10:37:30
 */
public enum ItemTypePreset {

    Account() {

        public ItemType create(RequestContext context) {

            ItemType type = new ItemType(context, "Account");
            type.setSearchable(false);
            type.setStatusControl(false);
            type.setFeatured(false);
            type.setHidden(true);
            type.setOverrideTags(false);
            type.save();

            type.addModule(ItemModule.Account);
            type.addModule(ItemModule.Registrations);
            type.removeModule(ItemModule.Images);
            type.removeModule(ItemModule.Content);
            type.removeModule(ItemModule.Categories);
            type.save();

            return type;
        }
    },

    NewsArticle() {

        public ItemType create(RequestContext context) {

            CategorySettings cs = CategorySettings.getInstance(context);
            cs.setHidden(true);
            cs.save();

            Category news = Category.getByName(context, "news");
            if (news == null) {
                news = new Category(context, "News", null);
                news.setHidden(true);
                news.save();
            }

            ItemType type = new ItemType(context, "News Article");
            type.setDefaultCategory(news);
            type.setOverrideTags(false);
            type.setNameCaption("Headline");

            type.setFeatured(false);
            type.setSearchable(false);
            type.setStatusControl(false);

            type.addModule(ItemModule.Images);
            type.removeModule(ItemModule.Categories);

            ItemSort sort = type.addSort();
            sort.setType(SortType.Newest);
            sort.save();

            Attribute authorAttribute = type.addAttribute("Author", AttributeType.Text);

            StringBuilder sb = new StringBuilder();
            sb.append("<style>\n");
            sb.append("table.news_bulletins { width: 100%; font-family: Verdana; margin-bottom: 10px; } \n");
            sb.append("table.news_bulletins td.headline { color: #304b61; font-size: 15px; font-weight: bold; } \n");
            sb.append("table.news_bulletins td.headline a { color: #304b61; text-decoration: underline; } \n");
            sb.append("table.news_bulletins td.headline a:hover { color: #304b61; text-decoration: none; } \n");
            sb.append("table.news_bulletins td.date { padding: 2px 0 0 5px; font-family: Tahoma; color: #666666; font-size: 11px; } \n");
            sb.append("table.news_bulletins td.content { font-family: Tahoma; padding: 5px; }\n");
            sb.append("table.news_bulletins td.content a { font-size: 11px; color: #6095c1; font-weight: bold; text-decoration: underline;}\n");
            sb.append("table.news_bulletins td.content a:hover { color: #6095c1; text-decoration: none; }\n");
            sb.append("</style>\n\n\n");

            //			type.setListMarkup(new Markup(sb.toString()));

            sb = new StringBuilder();
            sb.append(new TableTag("news_bulletins"));
            sb.append("\n\n<tr><td class='headline'>[item?link=1]</td></tr>\n");
            sb.append("<tr><td class='date'>Posted by [attribute?id=" + authorAttribute.getId()
                    + "] on [date_created?format=EEEEE, dd MMMMM yyyy hh:mma]</td></tr>\n");
            sb.append("<tr><td class='content'>[summary?max=400&link=read more]</td></tr>\n");

            //			if (bulletin.getContent().length() > 400) {
            //
            //				sb.append(" <span class='more'>[ ");
            //				sb.append(new LinkTag(NewsHandler.class, null, "read more", "bulletin", bulletin));
            //				sb.append(" ]</span>");
            //			}

            sb.append("</table>");

            //			type.setListMarkupBody(sb.toString());

            sb = new StringBuilder();
            sb.append("<style>\n");
            sb.append("table.news_bulletin { width: 100%; font-family: Verdana; margin-bottom: 10px; } \n");
            sb.append("table.news_bulletin td.date { padding: 2px 0 0 5px; font-family: Tahoma; color: #666666; font-size: 11px; } \n");
            sb.append("table.news_bulletin td.content { font-family: Tahoma; padding: 5px; }\n");
            sb.append("table.news_bulletin td.content a { font-size: 11px; color: #6095c1; font-weight: bold; text-decoration: underline;}\n");
            sb.append("table.news_bulletin td.content a:hover { color: #6095c1; text-decoration: none; }\n");
            sb.append("</style>\n\n\n");

            sb.append(new TableTag("news_bulletin"));
            sb.append("\n<tr><td class='date'>Posted on [date_created?format=EEEEE, dd MMMMM yyyy hh:mma] by [attribute?id=" + authorAttribute.getId()
                    + "]</td></tr>\n");
            sb.append("<tr><td class='content'>[content]</td></tr>\n");
            sb.append("</table>");

            type.setViewMarkup(new Markup(context, "News view markup", null, sb.toString(), null, null, 0));

            type.save();

            return type;
        }

        @Override
        public String toString() {
            return "News Article";
        }
    },

    Product() {

        public ItemType create(RequestContext context) {

            Modules modules = Modules.getInstance(context);
            modules.addModule(Module.Shopping);
            modules.addModule(Module.Availabilitity);
            modules.addModule(Module.Pricing);

            modules.save();

            ItemType itemType = new ItemType(context, "Product");
            itemType.setSearchable(true);
            itemType.addModule(ItemModule.Pricing);
            itemType.setVat(true);
            itemType.setDefaultVatRate(17.5);
            itemType.addModule(ItemModule.Stock);
            itemType.addModule(ItemModule.Accessories);
            itemType.addModule(ItemModule.Ordering);
            itemType.addModule(ItemModule.Images);
            itemType.setRrp(true);

            itemType.save();

            itemType.addAttribute("Manufacturer", AttributeType.Text);
            itemType.addAttribute("Manuf part number", AttributeType.Text);

            Attribute websiteAttribute = itemType.addAttribute("Website", AttributeType.Link);
            websiteAttribute.setLinkNewWindow(true);
            websiteAttribute.save();

            return itemType;
        }

    },
    Book() {

        public ItemType create(RequestContext context) {

            Modules modules = Modules.getInstance(context);
            modules.addModule(Module.Shopping);
            modules.addModule(Module.Availabilitity);
            modules.addModule(Module.Pricing);
            modules.save();

            ItemType itemType = new ItemType(context, "Book");
            itemType.setSearchable(true);
            itemType.setVat(true);
            itemType.setDefaultVatRate(0);
            itemType.addModule(ItemModule.Ordering);
            itemType.addModule(ItemModule.Stock);
            itemType.addModule(ItemModule.Pricing);
            itemType.addModule(ItemModule.Images);
            itemType.setRrp(true);

            itemType.setNameCaption("Title");
            itemType.save();

            itemType.addAttribute("Author");
            itemType.addAttribute("ISBN", AttributeType.Text);

            return itemType;
        }

    },
    ListingAdvanced() {

        @Override
        public ItemType create(RequestContext context) {

            ItemType itemType = new ItemType(context, "Listing");
            itemType.addModule(ItemModule.Images);
            itemType.save();

            Attribute addressAttribute = itemType.addAttribute("Address", AttributeType.Text);
            addressAttribute.setWidth(40);
            addressAttribute.setHeight(4);
            addressAttribute.save();

            itemType.addAttribute("District", AttributeType.Text);
            itemType.addAttribute("Town", AttributeType.Text);
            itemType.addAttribute("County", AttributeType.Text);
            itemType.addAttribute("Postcode", AttributeType.Postcode);
            itemType.addAttribute("Country", AttributeType.Text);
            itemType.addAttribute("Telephone", AttributeType.Text);
            itemType.addAttribute("Mobile", AttributeType.Text);
            itemType.addAttribute("Fax", AttributeType.Text);
            itemType.addAttribute("Email", AttributeType.Email);

            Attribute websiteAttribute = itemType.addAttribute("Website", AttributeType.Link);
            websiteAttribute.setLinkNewWindow(true);
            websiteAttribute.save();

            itemType.addAttribute("Opening Times", AttributeType.Text);
            itemType.addAttribute("Type", AttributeType.Selection);

            return itemType;
        }

    },
    Listing() {

        @Override
        public ItemType create(RequestContext context) {

            ItemType itemType = new ItemType(context, "Listing");
            itemType.addModule(ItemModule.Images);
            itemType.save();

            Attribute addressAttribute = itemType.addAttribute("Address", AttributeType.Text);
            addressAttribute.setWidth(40);
            addressAttribute.setHeight(4);
            addressAttribute.save();

            itemType.addAttribute("Postcode", AttributeType.Text);
            itemType.addAttribute("Email", AttributeType.Email);
            itemType.addAttribute("Telephone", AttributeType.Text);

            Attribute websiteAttribute = itemType.addAttribute("Website", AttributeType.Link);
            websiteAttribute.setLinkNewWindow(true);
            websiteAttribute.save();

            itemType.addExternalLink(LinkType.GoogleStreet);
            itemType.addExternalLink(LinkType.MultimapSatellite);

            return itemType;
        }

    },
    Event() {

        @Override
        public ItemType create(RequestContext context) {

            ItemType itemType = new ItemType(context, "Event");
            itemType.addModule(ItemModule.Images);
            itemType.save();

            itemType.addAttribute("Date start", AttributeType.Date);
            itemType.addAttribute("Date end", AttributeType.Date);
            itemType.addAttribute("Time", AttributeType.Text);

            Attribute websiteAttribute = itemType.addAttribute("Website", AttributeType.Link);
            websiteAttribute.setLinkNewWindow(true);
            websiteAttribute.save();

            itemType.addAttribute("Promoter/Organiser", AttributeType.Text);
            itemType.addAttribute("Contact", AttributeType.Text);
            itemType.addAttribute("Contact telephone", AttributeType.Text);
            itemType.addAttribute("Contact email", AttributeType.Email);

            Attribute orgWebsiteAttribute = itemType.addAttribute("Organiser's website", AttributeType.Link);
            orgWebsiteAttribute.setLinkNewWindow(true);
            orgWebsiteAttribute.save();

            itemType.addAttribute("Event type", AttributeType.Selection);
            itemType.addAttribute("Venue", AttributeType.Text);
            itemType.addAttribute("Address", AttributeType.Text);
            itemType.addAttribute("District", AttributeType.Text);
            itemType.addAttribute("Town", AttributeType.Text);
            itemType.addAttribute("County", AttributeType.Text);
            itemType.addAttribute("Postcode", AttributeType.Postcode);
            itemType.addAttribute("Country", AttributeType.Text);
            itemType.addAttribute("Venue contact", AttributeType.Text);
            itemType.addAttribute("Venue telephone", AttributeType.Text);
            itemType.addAttribute("Venue email", AttributeType.Email);

            return itemType;
        }

    },
    Venues() {

        @Override
        public ItemType create(RequestContext context) {

            ItemType itemType = new ItemType(context, "Venue");
            itemType.addModule(ItemModule.Images);
            itemType.save();

            Attribute addressAttribute = itemType.addAttribute("Address", AttributeType.Text);
            addressAttribute.setWidth(40);
            addressAttribute.setHeight(4);
            addressAttribute.save();

            itemType.addAttribute("District", AttributeType.Text);
            itemType.addAttribute("Town", AttributeType.Text);
            itemType.addAttribute("County", AttributeType.Text);
            itemType.addAttribute("Postcode", AttributeType.Postcode);
            itemType.addAttribute("Country", AttributeType.Text);
            itemType.addAttribute("Contact", AttributeType.Text);

            itemType.addAttribute("Telephone", AttributeType.Text);
            itemType.addAttribute("Mobile", AttributeType.Text);
            itemType.addAttribute("Fax", AttributeType.Text);
            itemType.addAttribute("Email", AttributeType.Email);

            Attribute websiteAttribute = itemType.addAttribute("Website", AttributeType.Link);
            websiteAttribute.setLinkNewWindow(true);
            websiteAttribute.save();

            itemType.addAttribute("Opening times", AttributeType.Text);
            itemType.addAttribute("Type", AttributeType.Selection);

            return itemType;
        }

    },
    Music() {

        public ItemType create(RequestContext context) {

            Modules modules = Modules.getInstance(context);
            modules.addModule(Module.Pricing);
            modules.addModule(Module.Shopping);
            modules.addModule(Module.Availabilitity);

            ItemType itemType = new ItemType(context, "Music");
            itemType.setSearchable(true);

            itemType.setVat(true);
            itemType.setDefaultVatRate(17.5);
            itemType.addModule(ItemModule.Ordering);
            itemType.addModule(ItemModule.Pricing);
            itemType.addModule(ItemModule.Images);
            itemType.setRrp(true);
            itemType.setNameCaption("Title");
            itemType.save();

            itemType.addAttribute("Artist");
            itemType.addAttribute("Label");

            Attribute attribute = itemType.addAttribute("Catalogue number");

            attribute = itemType.addAttribute("Format", AttributeType.Selection);
            attribute.addOption("CD");
            attribute.addOption("DVD");
            attribute.addOption("Vinyl 7\"");
            attribute.addOption("Vinyl 12\"");
            attribute.addOption("Cassette");
            attribute.addOption("VHS");

            attribute = itemType.addAttribute("Type", AttributeType.Selection);
            attribute.addOption("Album");
            attribute.addOption("Single");
            attribute.addOption("Compilation");
            attribute.addOption("EP");

            return itemType;
        }

    },
    Job() {

        public ItemType create(RequestContext context) {

            ItemType type = new ItemType(context, "Job");
            type.addModule(ItemModule.Images);
            type.setSearchable(true);
            type.save();

            Attribute attribute = type.addAttribute("Salary", AttributeType.Numerical);
            attribute.save();

            attribute = type.addAttribute("Location", AttributeType.Text);
            attribute.setLocation(true);

            return type;
        }

    },
    Property() {

        public ItemType create(RequestContext context) {

            ItemType type = new ItemType(context, "Property");
            type.addModule(ItemModule.Images);
            type.setSearchable(true);
            type.setPriceCaption("Asking price");
            type.save();

            Attribute attribute = type.addAttribute("Property Type", AttributeType.Selection);

            attribute.addOption("Detached");
            attribute.addOption("Semi");
            attribute.addOption("Terraced");
            attribute.addOption("Bungalow");

            attribute = type.addAttribute("Bedrooms", AttributeType.Selection);

            attribute.addOption("Studio");
            attribute.addOption("1");
            attribute.addOption("2");
            attribute.addOption("3");
            attribute.addOption("4");
            attribute.addOption("5+");

            return type;
        }

    };

    public abstract ItemType create(RequestContext context);
}
