package org.sevensoft.ecreator.model.items.highlighted;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * User: alchemist
 * Date: 01.03.2012
 */
@Table("tickers_settings")
@Singleton
public class TickerSettings extends EntityObject {

    private int interval;

    public TickerSettings(RequestContext context) {
        super(context);
    }

    public static TickerSettings getInstance(RequestContext context) {
        return getSingleton(context, TickerSettings.class);
    }

    public int getInterval() {
        return interval == 0 ? 5000 : interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }
}
