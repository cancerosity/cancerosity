package org.sevensoft.ecreator.model.items.export;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 23 Jan 2007 16:45:42
 * 
 * Export data as an email
 *
 */
public class ItemEmailExporter {

	private static final Logger			logger	= Logger.getLogger("ecreator");
	private Item					item;
	private MultiValueMap<Attribute, String>	attributeValues;
	private List<Attribute>				attributes;
	private String					separator;
	private Config					config;
	private final List<String>			recipients;

	public ItemEmailExporter(RequestContext context, Item item, List<String> recipients) {
		this.item = item;
		this.recipients = recipients;
		this.attributes = item.getAttributes();
		this.attributeValues = item.getAttributeValues();
		this.separator = ":";
		this.config = Config.getInstance(context);
	}

	public void email() throws EmailAddressException, SmtpServerException {

		StringBuilder sb = new StringBuilder();

		sb.append("Name" + separator + item.getName() + "\n");

		if (item.hasEmail()) {
			sb.append("Email" + separator + item.getEmail() + "\n");
		}

		sb.append("Date created" + separator + item.getDateCreated().toString("dd-MMM-yyyy HH:mm") + "\n");
		sb.append("Id" + separator + item.getIdString() + "\n");

		if (item.hasReference()) {
			sb.append("Reference" + separator + item.getIdString() + "\n");
		}

		for (Map.Entry<String, String> entry : getFields().entrySet()) {
			sb.append(entry.getKey() + separator + entry.getValue() + "\n");
		}

		Email e = new Email();
		e.setRecipients(recipients);
		e.setSubject(item.getItemType().getName() + " data export");
		e.setBody(sb);
		e.setFrom(config.getServerEmail());

		logger.fine("[ItemEmailExporter] email=" + e);

		new EmailDecorator(RequestContext.getInstance(), e).send(config.getSmtpHostname());
	}

	public Map<String, String> getFields() {

		Map<String, String> map = new LinkedHashMap();
		for (Attribute attribute : attributes) {
			String value = attributeValues.get(attribute);
			if (value == null) {
				value = "";
			}
			map.put(attribute.getName(), value);
		}
		return map;
	}
}
