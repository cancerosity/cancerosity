package org.sevensoft.ecreator.model.items.renderers;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * @author sks 27 Dec 2006 13:26:05
 *
 */
public class PrinterMarkupDefault implements MarkupDefault {

	public String getBetween() {
		return null;
	}

	public String getCss() {
		return null;
	}

	public String getBody() {

		StringBuilder sb = new StringBuilder();

		sb.append("<table cellspacing='0' cellpadding='0' class='item_print'>\n");
        sb.append("<tr class='printnotes'><td><div id='go_back'><a href='#' title='Go Back' " +
                "onclick='javascript:window.history.go(-1)'>Continue Browsing �</a></div></td><td " +
                "align='right'><div id='njh_date'></div></td></tr>\n");
        sb.append("<tr class='head'><td><div class='comp_name'>[comp_name]</div></td><td align='right' " +
                "valign='top'><div class='comp_address'>[comp_address_label]<br />[comp_email]</div><div " +
                "class='comp_tel'>[comp_telephone?prefix=Tel: ]</div></td></tr>\n");
        sb.append("<tr><td colspan='2'><span id='item_url'>[item_url]</span><h1>[item]</h1></td>\n");
        sb.append("<tr><td class='image' align='left' valign='top'>[fullsize?w=300&h=300]</td><td class='details' " +
                "valign='top' align='left' width='100%'>\n");
        sb.append("<div class='pricing'><h2>Product Details:</h2>\n");
        sb.append("[pricing_sell?inc=1&prefix=Price: ]\n");
        sb.append("[pricing_rrp] [pricing_rrp_discount?prefix=- you save &suffix=!]\n");
        sb.append("[pricing_sell?prefix=Ex VAT: ]\n");
        sb.append("[pricing_original?inc=1&prefix=Original price: ]\n");
        sb.append("[pricing_discount?inc=1&prefix=Discount price: ]\n");
        sb.append("</div>\n");
        sb.append("[stock?prefix=Availability: &text=true]\n");
        sb.append("[accessories]\n");
        sb.append("[options?printer=1&]\n");
        sb.append("<!-- <table class='ordering' cellspacing='0' cellpadding='0'><tr><td class='quan' " +
                "align='left'>[ordering_qty]</td><td class='add' align='left'>\n");
        sb.append("[ordering_buy?label=Add to basket]</td></tr></table> -->[attributes_table]\n");
        sb.append("</td></tr>\n");
        sb.append("<tr><td colspan='2'><div class='description'><h2>Product Description:</h2></div>\n");
        sb.append("[content]</td></tr>\n");
        sb.append("</table>\n");
        sb.append("[alternatives]");

        return sb.toString();
	}

	public String getEnd() {

        StringBuilder sb = new StringBuilder();
        sb.append("<script type='text/javascript'>function dateInit() {\n");
        sb.append("var njhdate = document.getElementById('njh_date')\n;");
        sb.append("njhdate.innerHTML = Date();\n");
        sb.append("}\n");
        sb.append("dateInit();</script>\n");

        return sb.toString();
	}

	public String getName() {
		return null;
	}

	public String getStart() {

		StringBuilder sb = new StringBuilder();

		sb.append("<style type='text/css'>\n");

        sb.append("h1 { font-family: 'Segoe UI', 'Trebuchet MS', Arial, Helvetica, Verdana; font-size: 24px; " +
                "color: #006699; margin: 0; padding: 0; padding-top: 30px; padding-bottom: 30px; text-align: center; }\n");
        sb.append("h2 { font-family: 'Segoe UI', 'Trebuchet MS', Arial, Helvetica, Verdana; font-size: 14px; " +
                "color: #1b0055; margin: 0; padding: 0; }\n");
        sb.append("div.comp_name { font-family: 'Segoe UI', 'Trebuchet MS', Arial, Helvetica, Verdana; font-size: " +
                "32px; font-weight: bold; color: #006699; }\n");
        sb.append("div.comp_address { text-align: right; font-family: 'Segoe UI', 'Trebuchet MS', Arial, Helvetica, " +
                "Verdana; font-size: 12px; font-weight: bold; }\n");
        sb.append("div.comp_tel { text-align: right; font-family: 'Segoe UI', 'Trebuchet MS', Arial, Helvetica, " +
                "Verdana; font-size: 12px; font-weight: bold; }\n");
        sb.append("div.ec_printitem_name { font-family: Verdana; font-size: 13px; font-weight: bold; }\n");
        sb.append("table.item_print { width: 640px; margin: 0 auto; font-family: Verdana, sans-serif; font-size: 12px; color: #333333; }\n");
        sb.append("table.item_print td.image img { border: 3px solid #cccccc; margin-right: 30px; }\n");
        sb.append("table.item_print div.pricing span { display: block; font-family: Arial, Helvetica, sans-serif; }\n");
        sb.append("table.item_print div.pricing { margin-bottom: 10px; }\n");
        sb.append("table.item_print div.pricing span.price_inc { font-size: 16px; color: #990000; font-weight: bold; }\n");
        sb.append("table.item div.pricing span.rrp { display: inline; font-size: 13px; color: orange; font-weight: bold; }\n");
        sb.append("table.item_print div.pricing span.rrp_discount { display: inline; font-size: 13px; color: #990000; font-weight: bold; }\n");
        sb.append("table.item_print div.pricing span.price { font-size: 12px; color: #555555; }\n");
        sb.append("table.item_print div.pricing span.price_original { font-size: 12px; color: orange; font-weight: bold; }\n");
        sb.append("table.item div.pricing span.price_discount { font-size: 12px; color: #ee0000; font-weight: bold; }\n");
        sb.append("table.item span.stock { display: block; margin-bottom: 10px; font-size: 12px; font-weight: bold; color: #777777; }\n");
        sb.append("table.attributes { margin-bottom: 10px; margin-top: 2px; font-family: Arial, Helvetica, sans-serif; font-size: 13px; }\n");
        sb.append("table.attributes td.attribute-label { font-weight: bold; padding-bottom: 3px; padding-top: 1px; }\n");
        sb.append("table.attributes td.attribute-value { padding-left: 5px; padding-bottom: 3px; padding-top: 1px; }\n");
        sb.append("table.options { margin-bottom: 10px; }\n");
        sb.append("table.options td.label { font-weight: bold; padding-right: 5px; padding-top: 3px; }\n");
        sb.append("table.options td.input select,table.options td.input option { margin: 1px 0; font-family: Verdana, sans-serif; font-size: 11px; }\n");
        sb.append("table.item_print div.description { border-top: 1px solid #cccccc; padding: 30px 0; margin-top: 30px; font-size: 12px; }\n");
        sb.append("tr.printnotes td { padding-bottom: 10px; }\n");
        sb.append("tr.head td { padding-top: 20px; border-top: 1px dotted #dddddd; padding-bottom: 30px; border-bottom: 1px solid #1b0055; }\n");
        sb.append("div#go_back,div#njh_date { font-family: \"Segoe UI\", \"Trebuchet MS\", Arial, Helvetica, sans-serif; font-size: 12px; color: #333333; }\n");
        sb.append("div#go_back a { color: #990000; text-decoration: none; }\n");
        sb.append("div#go_back a:hover { color: #333333; text-decoration: none; }\n");
        sb.append("span#item_url { font-family: 'Segoe UI', 'Trebuchet MS', Arial, Helvetica, Verdana; font-style: italic; }\n");
        sb.append("table.ec_options { background: #eeeeee; border: 5px solid #eeeeee; margin: 10px 0; font-family: Arial, Helvetica, sans-serif; font-size: 13px; }\n");
        sb.append("table.ec_options td.ec_option_label { padding-right: 20px; font-weight: bold; }\n");
        sb.append("table.ec_options td span.option_value { font-style: italic; }\n");
        sb.append("@media print {\n");
        sb.append("div.go_back { display: none; }\n");
        sb.append("}</style>");

        return sb.toString();
	}

	public int getTds() {
		return 0;
	}

}
