package org.sevensoft.ecreator.model.items;

import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 16 Jul 2006 09:57:40
 *
 */
public enum ItemModule {

	ProfileImages() {
	},

	TellFriend() {

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.TellFriend.enabled(context);
		}
	},

//	Relations() {
//
//		@Override
//		public boolean isAvailable(RequestContext context) {
//			return Module.Relations.enabled(context);
//		}
//	},

	SmsCredits() {

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.SmsCredits.enabled(context);
		}
	},

	Agreements() {

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.Agreements.enabled(context);
		}
	},
	Buyers() {

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.Buyers.enabled(context);
		}
	},
	Bookings() {

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.Bookings.enabled(context);
		}
	},

	Account() {

		@Override
		public String getDescription() {
			return "Turn this item type into a member account";
		}

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.Accounts.enabled(context);
		}
	},

	Subscriptions() {

		@Override
		public String getDescription() {
			return "Create subscription levels for accounts of this type";
		}

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.Subscriptions.enabled(context);
		}
	},

	Listings() {

		@Override
		public String getDescription() {
			return "Users can add items of this item through the front end";
		}

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.Listings.enabled(context);
		}
	},

	Messages() {

		@Override
		public String getDescription() {
			return "Allow messages to be added to this item by users";
		}
	},

	Login() {

		@Override
		public String getDescription() {
			return "Allow users to login to this member account with a password through the front end";
		}

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.Login.enabled(context);
		}
	},

	Delivery() {

		@Override
		public String getDescription() {
			return "Enable per item delivery rates for items of this type";
		}

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.Delivery.enabled(context);
		}
	},

	Attachments() {

		@Override
		public String getDescription() {
			return "Attachments can be uploaded to items of this type";
		}
	},

	Alternatives() {

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.Alternatives.enabled(context);
		}

	},

	Accessories() {

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.Accessories.enabled(context);
		}
	},

	Stock() {

		@Override
		public String getDescription() {
			return "Enable stock levels and in / out stock messages on this item type";
		}

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.Availabilitity.enabled(context);
		}

	},
	Ratings() {

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.Ratings.enabled(context);
		}
	},

	Reviews() {

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.Reviews.enabled(context);
		}

	},
	Forms() {

		@Override
		public String getDescription() {
			return "Allow forms to be assigned to this item type";
		}
	},

	Pricing() {

		@Override
		public String getDescription() {
			return "Enable sell price, cost price and vat rates on this item type";
		}

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.Pricing.enabled(context);
		}
	},
	Options() {

		@Override
		public String getDescription() {
			return "Extra options can be added to this item for ordering purposes, eg colour, size";
		}
	},

	Ordering() {

		@Override
		public String getDescription() {
			return "Allow this item type to be ordered via the shopping system";
		}

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.Pricing.enabled(context) && Module.Shopping.enabled(context);
		}
	},
	Registrations() {

		@Override
		public String getDescription() {
			return "Users can register as a member of this account type via the front end";
		}
	},
	Images() {

		@Override
		public String getDescription() {
			return "Allow images to be uploaded to this item";
		}

	},

    IgnoreItemsInFeed() {

        @Override
        public String getDescription() {
            return "Allow items to be excluded from Google base feed";
        }

        @Override
        public boolean isAvailable(RequestContext context) {
            return Module.Feeds.enabled(context);
        }
    },

    Profile, HeadersFooters, Videos, Meetups() {

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.Meetups.enabled(context) && Module.Accounts.enabled(context);
		}
	},

    Comments() {

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.Comments.enabled(context) && Module.Accounts.enabled(context) ;
		}
	},
    SocialNetworkFeed() {

        @Override
        public boolean isAvailable(RequestContext context) {
            return Module.SocialNetworkFeed.enabled(context) && Module.Listings.enabled(context);
        }
    },

	MultiItemsAddition("Multiple Items Addition") {

		@Override
		public String getDescription() {
			return "Let admin users get 5 empty items that can be filled in and updated all in one go";
		}

		@Override
		public boolean isAvailable(RequestContext context) {
			return Module.MultiItemsAddition.enabled(context);
		}
	},
    IBex("iBex. Internet Booking Exchange") {

        @Override
        public String getDescription() {
            return "Web based booking system";
        }

        @Override
        public boolean isAvailable(RequestContext context) {
            return Module.IBex.enabled(context);
        }
    },
	Content, Categories, Branches, Relations, Credits, MyShoppingBox;

	private final String	toString;

	private ItemModule() {
		this.toString = name();

	}

	private ItemModule(String toString) {
		this.toString = toString;
	}

	public final boolean enabled(RequestContext context, Item item) {
        return item != null && enabled(context, item.getItemType());
	}

	public final boolean enabled(RequestContext context, ItemType itemType) {
        return itemType != null && itemType.getModules().contains(this) && isAvailable(context);
    }

	/**
	 * 
	 */
	public String getDescription() {
		return null;
	}

	public boolean isAvailable(RequestContext context) {
		return true;
	}

	@Override
	public final String toString() {
		return toString;
	}

}
