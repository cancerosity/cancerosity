package org.sevensoft.ecreator.model.items.reviews;

import java.util.List;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 22 Mar 2007 16:40:31
 *
 * Contains methods for reviews on an item
 */
@Table("reviews_delegates")
public class ReviewsDelegate extends EntityObject {

	public static ReviewsDelegate get(RequestContext context, Item item) {
		ReviewsDelegate del = SimpleQuery.get(context, ReviewsDelegate.class, "item", item);
		return del == null ? new ReviewsDelegate(context, item) : del;
	}

	private Item	item;
	private int		reviewsCount;

	private ReviewsDelegate(RequestContext context) {
		super(context);
	}

	public ReviewsDelegate(RequestContext context, Item item) {
		super(context);

		this.item = item;

		save();
	}

	public Review addReview(String author, String content) {

		Review review = new Review(context, item, author, content);

		return review;
	}

	public List<Review> getReviews() {
		Query q = new Query(context, "select * from # where item=? order by date desc");
		q.setTable(Review.class);
		q.setParameter(item);
		return q.execute(Review.class);
	}

}
