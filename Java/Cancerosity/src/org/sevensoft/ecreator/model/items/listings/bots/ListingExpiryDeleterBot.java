package org.sevensoft.ecreator.model.items.listings.bots;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.listings.Listing;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.samdate.Date;


import java.util.List;
import java.util.logging.Logger;

/**
 * @author LebedevDN
 * @version: $id$
 */
public class ListingExpiryDeleterBot implements Runnable {

    private Logger logger = Logger.getLogger("cron");
    private RequestContext context;

    public ListingExpiryDeleterBot(RequestContext context) {
        this.context = context;
    }

    public void run() {
        logger.config("Listing Expiry Deleter Bot");
        Query q = new Query(context, "SELECT i.* FROM # i JOIN # l ON i.id=l.item WHERE i.status like 'Live' and l.expirydate > 0 AND l.expirydate < ?");
        q.setTable(Item.class);
        q.setTable(Listing.class);
        q.setParameter(new Date().getTimestamp());
        List<Item> expiringItems = q.execute(Item.class);
        for (Item item : expiringItems) {
            item.setStatus("Disabled");
            item.setListingPaymentDone(false);
        }
        logger.severe("[ListingExpiryDeleterBot] items removed: " + expiringItems.size());
    }
}
