package org.sevensoft.ecreator.model.items.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.ICategoryMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.db.EntityObject;

/**
 * @author sks 23 Apr 2007 12:54:13
 *
 */
public class LastUpdatedMarker extends MarkerHelper implements IItemMarker, ICategoryMarker, IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

        if (params.containsKey("format")) {
            return super.string(context, params, item.getDateUpdated().toString(params.get("format")));
        }
        String lastUpdated = item.getLastUpdatedString();
        return super.string(context, params, lastUpdated);
	}

    public Object generate(RequestContext context, Map<String, String> params, Category category) {

        String id = params.get("id");
        if (id != null) {
            category = EntityObject.getInstance(context, Category.class, id);
        }

        String name = params.get("name");
        if (name != null) {
            category = Category.getByName(context, name);
        }

        if (category == null) {
            category = (Category) context.getAttribute("category");
        }
        logger.fine("[CategoryMarker] category=" + category);

        if (category == null) {
            return null;
        }
        String format = params.get("format");
        if (format == null) {
            format = "EEE dd/MM/yyyy";
        }

        if (params.containsKey("class")) {
            params.put("class", "date_created_category");
        }

        return super.string(context, params, category.getDateUpdated().toString(format));
    }

    public Object generate(RequestContext context, Map<String, String> params) {
        if (context.containsAttribute("item")) {
            return generate(context, params, (Item) context.getAttribute("item"), null, 0, 0);
        }
        return generate(context, params, null);
    }

    public Object getRegex() {
        return "last_updated";
    }

}
