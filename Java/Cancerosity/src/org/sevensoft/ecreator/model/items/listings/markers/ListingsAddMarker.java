package org.sevensoft.ecreator.model.items.listings.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.items.listings.AddListingHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 26 Jun 2006 09:53:32
 *
 */
public class ListingsAddMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.Listings.enabled(context)) {
			return null;
		}

		Link link = new Link(AddListingHandler.class, "start");
		if (params.containsKey("lp")) {
			link.setParameter("listingPackage", params.get("lp"));
		}

		if (params.containsKey("listingpackage")) {
			link.setParameter("listingPackage", params.get("listingpackage"));
		}

		return super.link(context, params, link, "account_link");

	}

	public Object getRegex() {
		return new String[] { "listings_add", "account_listings_add" };
	}

}
