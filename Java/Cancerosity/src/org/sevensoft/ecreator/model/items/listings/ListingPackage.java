package org.sevensoft.ecreator.model.items.listings;

import java.util.*;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.ecreator.model.accounts.permissions.AccessGroup;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.misc.agreements.Agreement;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.config.Captions;
import org.sevensoft.ecreator.model.extras.reorder.ReorderOwner;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 29-Nov-2005 16:40:04
 */
@Table("listings_packages")
public class ListingPackage extends EntityObject implements Selectable, Positionable, Comparable<ListingPackage>, ReorderOwner {

	public static SortedSet<ListingPackage> filter(SortedSet<ListingPackage> packages, final Category category) {

		CollectionsUtil.filter(packages, new Predicate<ListingPackage>() {

			public boolean accept(ListingPackage e) {
				return e.accepts(category);
			}
		});
		return packages;
	}

	public static SortedSet<ListingPackage> filter(SortedSet<ListingPackage> packages, final ItemType itemType) {

		CollectionsUtil.filter(packages, new Predicate<ListingPackage>() {

			public boolean accept(ListingPackage e) {
				return true;
			}
		});
		return packages;
	}

	public static List<ListingPackage> get(RequestContext context) {
		Query q = new Query(context, "select * from # where deleted=0 and itemType>0 order by position");
		q.setTable(ListingPackage.class);
		return q.execute(ListingPackage.class);
	}

    public static Map<String, String> getSelectionMap(RequestContext context) {
        Query q = new Query(context, "select id, name from # where deleted=0 and itemType>0 order by position");
        q.setTable(ListingPackage.class);
        return q.getMap(String.class, String.class, new LinkedHashMap());
    }

	public static SortedSet<Category> getCategories(Collection<ListingPackage> packages) {

		SortedSet<Category> categories = new TreeSet<Category>();

		/*
		 * Iterate over each listing package and add in the applicable categories.
		 */
		for (ListingPackage listingPackage : packages) {
			categories.addAll(listingPackage.getCategories());
		}

		return categories;
	}

	public static List<ListingPackage> getGuest(RequestContext context) {
		return SimpleQuery.execute(context, ListingPackage.class, "guest", true, "deleted", 0);
	}

	/**
	 * 
	 */
	private boolean	moderateImages;

	/**
	 * Allow users to add listings without registering, simply adding their email
	 * and then being sent a login to change the details.
	 */
	private boolean	guest;

	/**
	 * Automatically set the created listing to featured
	 */
	private boolean	featured;

	/**
	 * Name this package
	 */
	private String	name;

	/**
	 * Allow the lister to specify delivery rates;
	 */
	private boolean	deliveryRates;

	/**
	 * By default delivery rates are not optional
	 */
	private boolean	deliveryRatesOptional;

	/**
	 * Require validation of anon email address before the listing can go live
	 */
	private boolean	validateGuest;

	/**
	 * Limit the number of categories listings using this package can choose
	 */
	private int		categoryLimit;

	/**
	 * We cannot delete packages if they are in use by another listing, so we must flag as deleted.
	 */
	private boolean	deleted;

	/**
	 * Flag this to yes and emails cannot be included in the content of an item of this type.
	 */
	private boolean	blockEmailsInContent;

	private Agreement	agreement;

	/**
	 * 
	 */
	private int		position;

	private int		minImages;

	private int		minCharacters;

	private int		minAttachments;

	private int		maxCategories;

	/**
	 * Max number of attachments
	 * Zero to delete
	 */
	private int		maxAttachments;

	/**
	 * If this item type has prices modules, then can the user add a price ?
	 */
	private boolean	prices;

	/**
	 * Increase sale price by this commission
	 */
	private Amount	commission;

	/*
	 *Flag to true if these listings should be moderated before they go live
	 */
	private boolean	moderation;

	/**
	 * All the user to add a stock level to their listing.
	 */
	private boolean	stock;

	/**
	 * Limit the number of images a listing of this package can add
	 */
	private int		maxImages;

	/**
	 * Limit the number of characters in a listing
	 */
	private int		maxCharacters;

	/**
	 * Limit the number of words in a listing
	 */
	private int		maxWords;

	/*
	 *Message to be displayed when the listing is completed.
	 */
	private String	completedMessage;

	private String	confirmButtonLabel;

	/**
	 * If flagged then this package will allow users to list in all categories on the site.
	 */
	private boolean	allCategories;

	/**
	 * If true then this package will allow users to list in any category where the category has
	 * a flag set specifying it as a listable category
	 */
	private boolean	listableCategories;

	/**
	 * Comments used to describe this package to customers.
	 */
	private String	description;

	private String	detailPagesHeader, detailPagesFooter;

	/**
	 * Ask for categories before anything else
	 */
	private boolean	categoriesFirst;

	/**
	 * Allow the name of items to be changed after they have been created
	 */
	private boolean	nameChange;

	/**
	 * The item type we can list using this package
	 */
	private ItemType	itemType;

	private boolean	useCategories;

	private boolean	prioritised;

	private String	deliveryRatesHeader;

    private boolean allowDescription;

    private boolean allowGuestToEdit;

    private String forwardUrl;

    @Default("1")
    private boolean	listingUpdate;

    private String nameDescription;
    private String contentSectionLabel;
    private String contentFieldLabel;
    private String contentFieldDescription;


    public ListingPackage(RequestContext context) {
		super(context);
	}

	public ListingPackage(RequestContext context, ItemType itemType, String name) {
		super(context);

		this.itemType = itemType;
		this.name = name;
		this.allCategories = true;

		save();
	}

	public boolean accepts(Category category) {

		List<Category> categories = getCategories();
		return categories.isEmpty() || categories.contains(category);
	}

    public void addAccessItemType(List<ItemType> types) {
        for (ItemType type : types) {
            addAccess(type);
        }
    }

    public void addAccess(ItemType type) {
        new ListingPackageAccess(context, this, type);
    }

    public void addAccessSubscriptionLevel(List<SubscriptionLevel> subscriptionLevels) {
        for (SubscriptionLevel subscriptionLevel : subscriptionLevels) {
            addAccess(subscriptionLevel);
        }
    }

    public void addAccess(SubscriptionLevel subscriptionLevel) {
        new ListingPackageAccess(context, this, subscriptionLevel);
    }

	public void addCategories(List<Category> categories) {
		for (Category category : categories) {
			new ListingPackageCategory(context, this, category);
		}
	}

	public void addCategory(Category category) {
		addCategories(Collections.singletonList(category));
	}

    public void addListingRates(List<ListingRate> rates) {
        for (ListingRate rate : rates) {
            new ListingRate(context, this, rate);
        }
    }

    public void addListingRate(ListingRate rate) {
        addListingRates(Collections.singletonList(rate));
    }

    public ListingRate addListingRate() {
        return new ListingRate(context, this);
    }

	public int compareTo(ListingPackage o) {
		return name.toLowerCase().compareTo(o.getName().toLowerCase());
	}

	@Override
	public synchronized boolean delete() {

		// delete assigend item types and categories
		new Query(context, "delete from # where listingPackage=?").setTable(ListingPackageCategory.class).setParameter(this).run();
		new Query(context, "delete from # where listingPackage=?").setTable(ListingPackageAccess.class).setParameter(this).run();

		// now deactivate
		deleted = true;

		save();
		return true;
	}

	public List<ItemType> getAccountItemTypes() {

		Query q = new Query(context, "select it.* from # it join # a on it.id=a.itemType where a.listingPackage=?");
		q.setTable(ItemType.class);
		q.setTable(ListingPackageAccess.class);
		q.setParameter(this);
		return q.execute(ItemType.class);
	}

	public final Agreement getAgreement() {
		return (Agreement) (agreement == null ? null : agreement.pop());
	}

	/**
	 * 
	 */
	public List<Category> getAvailableCategories() {

		if (isAllCategories()) {
			return Category.get(context);

		} else {
			return getCategories();
		}
	}

	/**
	 * Returns a list of categories this package has been explicity set on
	 * 
	 */
	public List<Category> getCategories() {

		Query q = new Query(context, "select c.* from # c join # lpc on c.id=lpc.category where lpc.listingPackage=? order by c.fullname");
		q.setTable(Category.class);
		q.setTable(ListingPackageCategory.class);
		q.setParameter(this);
		return q.execute(Category.class);
	}

	public int getCategoryLimit() {
		return categoryLimit < 1 ? 1 : categoryLimit;
	}

	public ListingRate getCheapestRate() {

		ListingRate cheapest = null;
		for (ListingRate rate : getListingRates()) {
			if (cheapest == null || rate.getFeeEx().isLessThan(cheapest.getFeeEx())) {
				cheapest = rate;
			}
		}
		return cheapest;
	}

	public Amount getCommission() {
		return commission;
	}

	public final String getCompletedMessage() {
        return completedMessage == null ? "Your " + Captions.getInstance(context).getListingsCaption() + " has been created, thank you." : completedMessage;
	}

	public final String getConfirmButtonLabel() {
        return confirmButtonLabel == null ? "Complete " + Captions.getInstance(context).getListingsCaption() : confirmButtonLabel;
	}

	public String getDeliveryRatesHeader() {
		return deliveryRatesHeader;
	}

	public final String getDescription() {
		return description;
	}

	public final String getDetailPagesFooter() {
		return detailPagesFooter;
	}

	public final String getDetailPagesHeader() {
		return detailPagesHeader;
	}

    public String getNameDescription() {
        return nameDescription == null ? "Enter the name of the " + itemType.getNameLower() + " to create." : nameDescription;
    }

    public String getContentSectionLabel() {
        return contentSectionLabel == null ? itemType.getName() + " description" : contentSectionLabel;
    }

    public String getContentFieldLabel() {
        return contentFieldLabel == null ? "Enter a description" : contentFieldLabel;
    }

    public String getContentFieldDescription() {
        if (contentFieldDescription == null) {
            StringBuilder desc = new StringBuilder();
            desc.append("Enter a general description of your " + itemType.getNameLower() + ".<br/>");
            if (hasMaxCharacters()) {
                desc.append(" You are limited to " + getMaxCharacters() +
                        " characters. We will tell you if you have gone over this limit.<br/>");
            }
            desc.append("HTML code is not allowed in your description except for &lt;b&gt; and &lt;i&gt; tags. Anything else will be stripped out by our editor.");

            contentFieldDescription = desc.toString();

        }
        return contentFieldDescription;
    }

    public ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public String getLabel() {
		return getName();
	}

	public List<ListingRate> getListingRates() {
		Query q = new Query(context, "select * from # where listingPackage=? order by fee");
		q.setParameter(this);
		q.setTable(ListingRate.class);
		return q.execute(ListingRate.class);
	}

	public int getMaxAttachments() {

		if (maxAttachments == 0) {
			return 0;
		}

		if (!Module.ListingAttachments.enabled(context)) {
			return 0;
		}

		return maxAttachments;
	}

	public final int getMaxCategories() {
		return maxCategories < 1 ? 1 : maxCategories;
	}

	public final int getMaxCharacters() {
		return maxCharacters;
	}

	public final int getMaxImages() {
		return maxImages;
	}

	public int getMaxWords() {
		return maxWords;
	}

	public final int getMinAttachments() {
		return minAttachments;
	}

	public final int getMinCharacters() {
		return minCharacters;
	}

	public final int getMinImages() {
		return minImages;
	}

	public String getName() {
		return name;
	}

	public int getPosition() {
		return position;
	}

	public List<SubscriptionLevel> getSubscriptions() {
		Query q = new Query(context, "select s.* from # s join # lpo on s.id=lpo.subscriptionLevel where lpo.listingPackage=?");
		q.setTable(SubscriptionLevel.class);
		q.setTable(ListingPackageAccess.class);
		q.setParameter(this);
		return q.execute(SubscriptionLevel.class);
	}

	public String getValue() {
		return getIdString();
	}

	public boolean hasAgreement() {
		return agreement != null;
	}

	public boolean hasCommission() {
		return commission != null;
	}

	/**
	 * 
	 */
	public boolean hasDeliveryRatesHeader() {
		return deliveryRatesHeader != null;
	}

	/**
	 * 
	 */
	public boolean hasDescription() {
		return description != null;
	}

	public boolean hasDetailsPageFooter() {
		return detailPagesFooter != null;
	}

	public boolean hasDetailsPageHeader() {
		return detailPagesHeader != null;
	}

	/**
	 * Returns true if this package has at least one fee based rate 
	 */
	public boolean hasFeeRates() {
		for (ListingRate rate : getListingRates()) {
			if (rate.hasFee())
				return true;
		}
		return false;
	}

	public boolean hasMaxCharacters() {
		return maxCharacters > 0;
	}

	public boolean hasRates() {
		return !isFree();
	}

	public boolean isAllCategories() {
		return allCategories;
	}

    public boolean isAllowDescription() {
        return allowDescription;
    }

    public boolean isAllowGuestToEdit() {
        return allowGuestToEdit;
    }

    public boolean isAttachments() {
		return getMaxAttachments() > 0;
	}

	public final boolean isBlockEmailsInContent() {
		return blockEmailsInContent;
	}

	public boolean isCategories() {
		return getItemType().isCategories() && (allCategories || getCategories().size() > 0);
	}

	public boolean isCategoriesFirst() {
		return categoriesFirst;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public final boolean isDeliveryRates() {
		return deliveryRates;
	}

	public final boolean isDeliveryRatesOptional() {
		return deliveryRatesOptional;
	}

	public final boolean isFeatured() {
		return featured;
	}

	public boolean isFree() {
		return getListingRates().isEmpty();
	}

	public boolean isGuest() {
		return guest;
	}

	/**
	 * Returns true if we are allowed to add images to our listing using this package.
	 * 
	 */
	public boolean isImages() {
		return maxImages > 0 && getItemType().getModules().contains(ItemModule.Images);
	}

	public boolean isListableCategories() {
		return listableCategories;
	}

	public final boolean isModerateImages() {
		return moderateImages;
	}

	public final boolean isModeration() {
		return moderation;
	}

	public boolean isNameChange() {
		return nameChange;
	}

	public final boolean isPrices() {
		return prices && getItemType().getModules().contains(ItemModule.Pricing);
	}

	public boolean isPrioritised() {
		return prioritised;
	}

	public final boolean isStock() {
		return stock && getItemType().getModules().contains(ItemModule.Stock);
	}

	public boolean isValidateGuest() {
		return validateGuest;
	}

    public String getForwardUrl() {
        return forwardUrl;
    }

    public boolean hasForwardUrl() {
        return forwardUrl != null;
    }

    public void removeAccess() {
		SimpleQuery.delete(context, ListingPackageAccess.class, "listingPackage", this);
	}

	public void removeAccess(AccessGroup subscriptionLevel) {
		SimpleQuery.delete(context, ListingPackageAccess.class, "listingPackage", this, "subscriptionLevel", subscriptionLevel);
	}

	public void removeAccess(ItemType itemType) {
		SimpleQuery.delete(context, ListingPackageAccess.class, "listingPackage", this, "itemType", itemType);
	}

	public void removeCategories() {
		SimpleQuery.delete(context, ListingPackageCategory.class, "listingPackage", this);
	}

	public void removeCategory(Category category) {
		SimpleQuery.delete(context, ListingPackageCategory.class, "listingPackage", this, "category", category);
	}

	public void removeRate(ListingRate listingRate) {
		listingRate.delete();
	}

	public final void setAgreement(Agreement agreement) {
		this.agreement = agreement;
	}

	public void setAllCategories(boolean allCategories) {
		this.allCategories = allCategories;
	}

    public void setAllowDescription(boolean allowDescription) {
        this.allowDescription = allowDescription;
    }

    public void setAllowGuestToEdit(boolean allowGuestToEdit) {
        this.allowGuestToEdit = allowGuestToEdit;
    }

    public final void setBlockEmailsInContent(boolean blockEmailsInContent) {
		this.blockEmailsInContent = blockEmailsInContent;
	}

	public void setCategoriesFirst(boolean categoriesFirst) {
		this.categoriesFirst = categoriesFirst;
	}

	public void setCategoryLimit(int categoryLimit) {
		this.categoryLimit = categoryLimit;
	}

	public void setCommission(Amount commission) {
		this.commission = commission;
	}

	public final void setCompletedMessage(String completedMessage) {
		this.completedMessage = completedMessage;
	}

	public final void setConfirmButtonLabel(String confirmButtonLabel) {
		this.confirmButtonLabel = confirmButtonLabel;
	}

	public final void setDeliveryRates(boolean deliveryRates) {
		this.deliveryRates = deliveryRates;
	}

	public void setDeliveryRatesHeader(String deliveryRatesHeader) {
		this.deliveryRatesHeader = deliveryRatesHeader;
	}

	public final void setDeliveryRatesOptional(boolean deliveryRatesOptional) {
		this.deliveryRatesOptional = deliveryRatesOptional;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public final void setDetailPagesFooter(String detailsPageFooter) {
		this.detailPagesFooter = detailsPageFooter;
	}

	public final void setDetailPagesHeader(String detailsPageHeader) {
		this.detailPagesHeader = detailsPageHeader;
	}

    public void setNameDescription(String nameDescription) {
        this.nameDescription = nameDescription;
    }

    public void setContentSectionLabel(String contentSectionLabel) {
        this.contentSectionLabel = contentSectionLabel;
    }

    public void setContentFieldLabel(String contentFieldLabel) {
        this.contentFieldLabel = contentFieldLabel;
    }

    public void setContentFieldDescription(String contentFieldDescription) {
        this.contentFieldDescription = contentFieldDescription;
    }

    public final void setFeatured(boolean featured) {
		this.featured = featured;
	}

	public void setGuest(boolean anonymous) {
		this.guest = anonymous;
	}

	public void setListableCategories(boolean listableCategories) {
		this.listableCategories = listableCategories;
	}

	public void setMaxAttachments(int attachments) {
		this.maxAttachments = attachments;
	}

	public final void setMaxCategories(int maxCategories) {
		this.maxCategories = maxCategories;
	}

	public final void setMaxCharacters(int maxCharacters) {
		this.maxCharacters = maxCharacters;
	}

	public final void setMaxImages(int maxImages) {
		this.maxImages = maxImages;
	}

	public void setMaxWords(int maxWords) {
		this.maxWords = maxWords;
	}

	public final void setMinAttachments(int minAttachments) {
		this.minAttachments = minAttachments;
	}

	public final void setMinCharacters(int minCharacters) {
		this.minCharacters = minCharacters;
	}

	public final void setMinImages(int minImages) {
		this.minImages = minImages;
	}

	public final void setModerateImages(boolean moderateImages) {
		this.moderateImages = moderateImages;
	}

	public final void setModeration(boolean vetting) {
		this.moderation = vetting;
	}

	public void setName(String name) {
		this.name = name;
	}

	public final void setNameChange(boolean nameChange) {
		this.nameChange = nameChange;
	}

	public void setPosition(int position) {
		this.position = position;
	}

    public String getReorderName() {
        return getName();
    }

    public final void setPrices(boolean prices) {
		this.prices = prices;
	}

	public final void setPrioritised(boolean prioritised) {
		this.prioritised = prioritised;
	}

	public final void setStock(boolean stock) {
		this.stock = stock;
	}

	public void setUseCategories(boolean useCategories) {
		this.useCategories = useCategories;
	}

	public void setValidateGuest(boolean validateAnon) {
		this.validateGuest = validateAnon;
	}

    public void setForwardUrl(String forwardUrl) {
        this.forwardUrl = forwardUrl;
    }

    public boolean isListingUpdate() {
        return listingUpdate;
    }

    public void setListingUpdate(boolean listingUpdate) {
        this.listingUpdate = listingUpdate;
    }

    public ListingPackage clone() throws CloneNotSupportedException {
        return clone(name + " (copy)");
    }

    public ListingPackage clone(String name) throws CloneNotSupportedException {

        ListingPackage copy = (ListingPackage) super.clone();

        copy.name = name;
        copy.save();

        // copy categories
        copy.addCategories(getCategories());
        copy.addListingRates(getListingRates());
        copy.addAccessItemType(getAccountItemTypes());
        copy.addAccessSubscriptionLevel(getSubscriptions());
        
        copy.save();

        return copy;
    }
}
