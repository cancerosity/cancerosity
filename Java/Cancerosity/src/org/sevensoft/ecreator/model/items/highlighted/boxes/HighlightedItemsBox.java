package org.sevensoft.ecreator.model.items.highlighted.boxes;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.sevensoft.ecreator.iface.admin.items.highighted.box.HighlightedItemsBoxHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.panels.OrderingForm;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.highlighted.boxes.markup.HighlightedItemsBoxMarkup1;
import org.sevensoft.ecreator.model.items.highlighted.boxes.markup.HighlightedItemsBoxMarkup2;
import org.sevensoft.ecreator.model.items.highlighted.boxes.markup.HighlightedItemsBoxMarkup3;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.ecreator.model.media.images.ImageUtil;
import org.sevensoft.ecreator.model.misc.CacheContainer;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.ecreator.model.search.SearchOwner;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 17-Mar-2006 11:32:21
 *
 */
@Table("boxes_highlighted_items")
@Label("Highlighted items")
@HandlerClass(HighlightedItemsBoxHandler.class)
public class HighlightedItemsBox extends Box implements SearchOwner {

	public enum Style {
		Static, Scroller, Typewriter;
	}

	public static Map<Integer, CacheContainer>	htmlCache;

	static {
		htmlCache = new HashMap();
	}

	/**
	 * 
	 */
	public static void clearHtmlCache() {
		htmlCache.clear();
	}

	/**
	 * Applet headline font size
	 */
	private String	appletHeadlineSize;

	/**
	 * Applet headline font family
	 */
	private String	appletHeadlineFamily;

	/**
	 * Applet summary font size
	 */
	private String	appletSummarySize;

	/**
	 * 
	 */
	private String	appletSummaryFamily;

	/**
	 * 
	 */
	private String	appletSummaryColor;

	/**
	 * 
	 */
	private String	appletBackgroundColor;

	/**
	 * 
	 */
	private String	appletHeadlineColor;

	/**
	 * 
	 */
	private Style	style;

	/**
	 * 
	 */
	private String	appletHeadlineHighlightColor;

	/**
	 * 
	 */
	private String	appletSummaryHighlightColor;

	/**
	 * Link to the category not the item when in applet
	 */
	private boolean	appletLinkToCategory;

    private String scrollerHeight;

    private String scrollerSpeed;

	protected HighlightedItemsBox(RequestContext context) {
		super(context);
	}

	public HighlightedItemsBox(RequestContext context, String location) {
		super(context, location);
	}

	@Override
	public synchronized boolean delete() {
		htmlCache.remove(getId());
		return super.delete();
	}

	public final String getAppletBackgroundColor() {
		return appletBackgroundColor == null ? "FFFFFF" : appletBackgroundColor;
	}

	public final String getAppletHeadlineColor() {
		return appletHeadlineColor == null ? "990000" : appletHeadlineColor;
	}

	public final String getAppletHeadlineFamily() {
		return appletHeadlineFamily == null ? "Arial" : appletHeadlineFamily;
	}

	public final String getAppletHeadlineHighlightColor() {
		return appletHeadlineHighlightColor == null ? "990000" : appletHeadlineHighlightColor;
	}

	public final String getAppletHeadlineSize() {
		return appletHeadlineSize == null ? "12" : appletHeadlineSize;
	}

	public final String getAppletSummaryColor() {
		return appletSummaryColor == null ? "000000" : appletSummaryColor;
	}

	public final String getAppletSummaryFamily() {
		return appletSummaryFamily == null ? "Arial" : appletSummaryFamily;
	}

	public final String getAppletSummaryHighlightColor() {
		return appletSummaryHighlightColor == null ? "000000" : appletSummaryHighlightColor;
	}

	public final String getAppletSummarySize() {
		return appletSummarySize == null ? "11" : appletSummarySize;
	}

	@Override
	protected String getCssIdDefault() {
		return "highlighted_items";
	}

	@Override
	public Markup getMarkup() {

		if (markup == null) {

			markup = new Markup(context, new HighlightedItemsBoxMarkup1());
			markup.setName("Highlighted items box markup");
			save();
		}
		return markup.pop();
	}

	public final Search getSearch() {

		Search search = Search.get(context, this);
		if (search == null) {

			search = new Search(context, this);
			search.setLimit(5);

			search.save();
		}

		logger.fine("[HighlightedItemsBox] search=" + search);

		return search;
	}

	public final Style getStyle() {
		return style == null ? Style.Static : style;
	}

	@Override
	public Map<Class, String> getStyles() {

		Map<Class, String> map = new LinkedHashMap<Class, String>();
		map.put(HighlightedItemsBoxMarkup1.class, "Style 1");
		map.put(HighlightedItemsBoxMarkup2.class, "Style 2");
		map.put(HighlightedItemsBoxMarkup3.class, "Style 3");

		return map;
	}

	@Override
	public final boolean hasEditableContent() {
		return false;
	}

	public final boolean isApplet() {
//		return getStyle() == Style.Scroller || getStyle() == Style.Typewriter;
        return isTypewriter();
	}

	public boolean isAppletLinkToCategory() {
		return appletLinkToCategory;
	}

	public final boolean isScroller() {
		return getStyle() == Style.Scroller;
	}

	public final boolean isStatic() {
		return getStyle() == Style.Static;
	}

	public final boolean isTypewriter() {
		return getStyle() == Style.Typewriter;
	}

	public String render(RequestContext context) {

		logger.fine("[HighlightedItemsBox] rendering=" + name);

		// check html cache first
		if (htmlCache.containsKey(getId())) {
			CacheContainer container = htmlCache.get(getId());
			if (container.isExpired()) {
				htmlCache.remove(getId());
			} else {
				return container.getCachedContent();
			}
		}

        Search search = getSearch();
		ItemSearcher searcher = search.getSearcher();

		// override status
		searcher.setStatus("Live");

		// ensure a sensible bound on items
		if (searcher.getLimit() < 1 || searcher.getLimit() > 30) {
			searcher.setLimit(30);
		}

		searcher.setAccount((Item) context.getAttribute("account"));

        if (search.getItemTypeDisplayed() != null) {
            Item pageOwner = (Item) context.getAttribute("item");
            if (pageOwner != null && pageOwner.getItemType().equals(search.getItemTypeDisplayed())) {
                searcher.setSearchAccount(pageOwner);
            }
        }

        //todo Can override the prev code block
        if (search.isShowOnlyChildren()) {
            searcher.setSearchAccount((Item) context.getAttribute("item"));
        }

        //todo Can override the prev code block
        if (search.isShowOnlyRelatives()) {
            Item pageOwner = (Item) context.getAttribute("item");
            if (pageOwner != null)
                searcher.setSearchAccount(pageOwner.getAccount());
        }

		List<Item> items = searcher.getItems();
		if (items.isEmpty()) {
			return null;
		}

		// prepopulate
		ImageUtil.prepopulate(context, items);
//		AttributeUtil.prepopulate(context, items);    TODO

		logger.fine("[HighlightedItemsBox] items=" + items);

		StringBuilder sb = new StringBuilder("<!-- start highlighted items box -->");
		sb.append(getTableTag());

		sb.append("<tr><td class='middle'>\n");
		sb.append(new OrderingForm(context));

		if (isTypewriter()) {
			renderAppletTypewriter(sb, items);
		} else if (isScroller()) {
//			renderAppletScroller(sb, items);
            sb.append("<script type=\"text/javascript\"> $(document).ready(function(){autoScroller('vmarquee").append(getMarkup().getVmarquee()).append("'" + ", ").append(getScrollerSpeed()).append(")}) </script>");
            renderStatic(context, sb, items);
        }

		else {
			renderStatic(context, sb, items);
		}

		sb.append("</form>");
		sb.append("</td></tr>\n");
		sb.append("<tr><td class='bottom'>&nbsp;</td></tr>\n");

		sb.append("</table>\n");

		sb.append("<!-- end highlighted items box -->");

		String html = sb.toString();

		// put this box in the cache		
		if (searcher.isExcludeAccount()) {
			return html;
		}

		if (searcher.isExcludeAccountItemType()) {
			return html;
		}

		if (searcher.getSortType() == SortType.Random) {
			return html;
		}

        if (search.isShowOnlyChildren()) {
            return html;
        }

		htmlCache.put(getId(), new CacheContainer(html));
		return html;
	}

	protected void renderAppletItems(StringBuilder sb, List objs) {

		int n = 1;
		for (Object obj : objs) {

			Item item = (Item) obj;

			sb.append("<param name='target_frame" + n + "' value='_self'>\n");

			sb.append("<param name='title" + n + "' value='");
			sb.append(item.getName().replace("'", "\""));
			sb.append("'>\n");

			if (item.hasContent()) {

				sb.append("<param name='text" + n + "' value='");
				sb.append(item.getContentStripped(150).replace("'", "\""));
				sb.append("'>\n");

			} else {

				sb.append("<param name='text" + n + "' value=''>\n");
			}

			String url;
			if (isAppletLinkToCategory() && item.hasCategory())
				url = item.getCategory().getUrl();
			else
				url = item.getUrl();

			sb.append("<param name='link" + n + "' value='" + url + "'>");

			n++;

		}

	}

    @Deprecated
	protected void renderAppletScroller(StringBuilder sb, List objs) {

		sb.append("<tr><td align='center'>");
		sb.append("<applet code='advnewsscroll.class' codebase='files/applets/' width='140' height='205' MAYSCRIPT>\n");
		sb.append("<param name='info' value='Applet by Gokhan Dagli,www.appletcollection.com'>\n");
		sb.append("<param name='regcode' value='coghkr5u8'>\n");

		sb.append("<param name='input_text' value='from_parameters'>\n");

		sb.append("<param name='text_file' value=''>\n");
		sb.append("<param name='bgimage' value=''>\n");

		sb.append("<param name='scroll_mode' value='scroll'>\n");

		sb.append("<param name='image_xposition' value='135'>\n");
		sb.append("<param name='image_yposition' value='166'>\n");

		sb.append("<param name='bgcolor' value='");
		sb.append(getAppletBackgroundColor());
		sb.append("'>\n");

		sb.append("<param name='title_color' value='");
		sb.append(getAppletHeadlineColor());
		sb.append("'>\n");

		sb.append("<param name='text_color' value='");
		sb.append(getAppletSummaryColor());
		sb.append("'>\n");

		sb.append("<param name='highlight_title_color' value='");
		sb.append(getAppletHeadlineHighlightColor());
		sb.append("'>\n");

		sb.append("<param name='highlight_text_color' value='");
		sb.append(getAppletSummaryHighlightColor());
		sb.append("'>\n");

		sb.append("<param name='title_align' value='left'>\n");
		sb.append("<param name='text_align' value='left'>\n");

		sb.append("<param name='title_font_type' value='" + getAppletHeadlineFamily() + "'>\n");
		sb.append("<param name='title_font_size' value='" + getAppletHeadlineSize() + "'>\n");
		sb.append("<param name='title_font_style' value='1'>\n");

		sb.append("<param name='text_font_type' value='" + getAppletSummaryFamily() + "'>\n");
		sb.append("<param name='text_font_size' value='" + getAppletSummarySize() + "'>\n");
		sb.append("<param name='text_font_style' value='0'>\n");

		sb.append("<param name='title_underline' value='no'>\n");
		sb.append("<param name='text_underline' value='no'>\n");

		sb.append("<param name='scroll_delay' value='15'>\n");
		sb.append("<param name='pause' value='2000'>\n");
		sb.append("<param name='vertical_space' value='20'>\n");
		sb.append("<param name='title_linespace' value='0'>\n");
		sb.append("<param name='text_linespace' value='0'>\n");

		renderAppletItems(sb, objs);

		sb.append("</applet>\n");
	}

	protected void renderAppletTypewriter(StringBuilder sb, List objs) {

		sb.append("<tr><td align='center'>");

		sb.append("<applet code='advtypew3.class' codebase='files/applets/' width='140' height='205' MAYSCRIPT>\n");
		sb.append("<param name='info' value='Applet by Gokhan Dagli,www.appletcollection.com'>\n");
		sb.append("<param name='regcode' value='coghkr5u8'>\n");

		sb.append("<param name='input_text' value='from_parameters'>\n");

		sb.append("<param name='text_file' value=''>\n");
		sb.append("<param name='bgimage' value=''>\n");

		sb.append("<param name='image_xposition' value='135'>\n");
		sb.append("<param name='image_yposition' value='166'>\n");

		sb.append("<param name='bgcolor' value='");
		sb.append(getAppletBackgroundColor());
		sb.append("'>\n");

		sb.append("<param name='title_color' value='");
		sb.append(getAppletHeadlineColor());
		sb.append("'>\n");

		sb.append("<param name='text_color' value='");
		sb.append(getAppletSummaryColor());
		sb.append("'>\n");

		sb.append("<param name='highlight_title_color' value='");
		sb.append(getAppletHeadlineHighlightColor());
		sb.append("'>\n");

		sb.append("<param name='highlight_text_color' value='");
		sb.append(getAppletSummaryHighlightColor());
		sb.append("'>\n");

		sb.append("<param name='title_align' value='left'>\n");
		sb.append("<param name='text_align' value='left'>\n");
		sb.append("<param name='title_font_type' value='" + getAppletHeadlineFamily() + "'>\n");
		sb.append("<param name='title_font_size' value='" + getAppletHeadlineSize() + "'>\n");
		sb.append("<param name='title_font_style' value='1'>\n");

		sb.append("<param name='text_font_type' value='" + getAppletSummaryFamily() + "'>\n");
		sb.append("<param name='text_font_size' value='" + getAppletSummarySize() + "'>\n");
		sb.append("<param name='text_font_style' value='0'>\n");
		sb.append("<param name='scroll_delay' value='15'>\n");
		sb.append("<param name='type_delay' value='50'>\n");
		sb.append("<param name='vertical_space' value='20'>\n");
		sb.append("<param name='title_linespace' value='0'>\n");
		sb.append("<param name='text_linespace' value='0'>\n");

		renderAppletItems(sb, objs);

		sb.append("</applet>\n");

	}

	private void renderStatic(RequestContext context, StringBuilder sb, List<Item> items) {

		MarkupRenderer r = new MarkupRenderer(context, getMarkup());
		r.setBodyObjects(items);
		sb.append(r);
	}

	public final void setAppletBackgroundColor(String appletBackgroundColor) {
		this.appletBackgroundColor = appletBackgroundColor;
	}

	public final void setAppletHeadlineColor(String appletHeadlineColor) {
		this.appletHeadlineColor = appletHeadlineColor;
	}

	public final void setAppletHeadlineFamily(String appletHeadlineFamily) {
		this.appletHeadlineFamily = appletHeadlineFamily;
	}

	public final void setAppletHeadlineHighlightColor(String appletHeadlineHighlightColor) {
		this.appletHeadlineHighlightColor = appletHeadlineHighlightColor;
	}

	public final void setAppletHeadlineSize(String appletHeadlineSize) {
		this.appletHeadlineSize = appletHeadlineSize;
	}

	public void setAppletLinkToCategory(boolean appletLinkToCategory) {
		this.appletLinkToCategory = appletLinkToCategory;
	}

	public final void setAppletSummaryColor(String appletSummaryColor) {
		this.appletSummaryColor = appletSummaryColor;
	}

	public final void setAppletSummaryFamily(String appletSummaryFamily) {
		this.appletSummaryFamily = appletSummaryFamily;
	}

	public final void setAppletSummaryHighlightColor(String appletSummaryHighlightColor) {
		this.appletSummaryHighlightColor = appletSummaryHighlightColor;
	}

	public final void setAppletSummarySize(String appletSummarySize) {
		this.appletSummarySize = appletSummarySize;
	}

	public final void setStyle(Style style) {
		this.style = style;
	}

	public final boolean useHtmlEditor() {
		return false;
	}

    public String getScrollerHeight() {
        return scrollerHeight == null ? "150" : scrollerHeight;
    }

    public void setScrollerHeight(String scrollerHeight) {
        this.scrollerHeight = scrollerHeight;
    }

    public String getScrollerSpeed() {
        return scrollerSpeed == null ? "1" : scrollerSpeed;
    }

    public void setScrollerSpeed(String scrollerSpeed) {
        this.scrollerSpeed = scrollerSpeed;
    }
}
