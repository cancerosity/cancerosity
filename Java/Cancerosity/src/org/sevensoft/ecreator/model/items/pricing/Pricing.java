package org.sevensoft.ecreator.model.items.pricing;

import java.util.List;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.pricing.watchers.PriceWatch;
import org.sevensoft.ecreator.model.items.pricing.watchers.PriceWatchEmail;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Apr 2007 08:28:56
 *
 */
@Table("prices_pricing")
public class Pricing extends EntityObject {

	public static Pricing get(RequestContext context, Item item) {
		Pricing module = SimpleQuery.get(context, Pricing.class, "item", item);
		return module == null ? new Pricing(context, item) : module;
	}

	private Item	item;

	/**
	 * A charge added for collecting this item. EX vat of course
	 */
	private Money	collectionCharge;

    private Amount discount;

	private Pricing(RequestContext context) {
		super(context);
	}

	private Pricing(RequestContext context, Item item) {
		super(context);
		this.item = item;
	}

	public void addPriceWatch(String email) {
		new PriceWatch(context, item, email);
	}

	/**
	 * Sends out email or sms to all people watching the price on this item
	 */
	public void doPriceWatch() {

		try {

			new PriceWatchEmail(item, getPriceWatchEmails(), context).send(Config.getInstance(context).getSmtpHostname());

		} catch (EmailAddressException e) {
			e.printStackTrace();

		} catch (SmtpServerException e) {
			e.printStackTrace();
		}
	}

	public final Money getCollectionCharge() {
		return collectionCharge;
	}

	public final Item getItem() {
		return item;
	}

	public List<String> getPriceWatchEmails() {

		Query q = new Query(context, "select email from # where product=?");
		q.setParameter(this);
		q.setTable(PriceWatch.class);
		return q.getStrings();
	}

	public boolean hasCollectionCharge() {
		return collectionCharge != null && collectionCharge.isPositive() && PriceSettings.getInstance(context).isCollectionCharges();
	}

	public boolean hasPromotionPrice() {
		return false;
	}

	/**
	 * Removes the price watcher tagged to this email
	 * 
	 */
	public void removePriceWatch(String email) {
		SimpleQuery.delete(context, PriceWatch.class, "item", this, "email", email);
	}

	/**
	 * Removes ALL price watchers
	 */
	public void removePriceWatchers() {
		SimpleQuery.delete(context, PriceWatch.class, "item", this);
	}

	public final void setCollectionCharge(Money collectionChargeEx) {
		this.collectionCharge = collectionChargeEx;
	}

    public Amount getDiscount() {
        return discount;
    }

    public void setDiscount(Amount discount) {
        this.discount = discount;
        save("discount");
    }
}
