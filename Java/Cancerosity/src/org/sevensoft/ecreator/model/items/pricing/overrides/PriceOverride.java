package org.sevensoft.ecreator.model.items.pricing.overrides;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 16 Aug 2006 20:15:58
 *
 */
@Table("items_prices_overrides")
public class PriceOverride extends EntityObject {

	private Item	account;
	private Item	item;
	private Money	price;

	protected PriceOverride(RequestContext context) {
		super(context);
	}

	public PriceOverride(RequestContext context, Item account, Item item, Money price) {

		super(context);

		this.account = account;
		this.item = item;
		this.price = price;

		save();
	}

	public Item getAccount() {
		return account.pop();
	}

	public Item getItem() {
		return item.pop();
	}

	public Money getPrice() {
		return price;
	}

}
