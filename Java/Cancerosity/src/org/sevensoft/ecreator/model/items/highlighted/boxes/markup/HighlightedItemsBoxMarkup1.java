package org.sevensoft.ecreator.model.items.highlighted.boxes.markup;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 6 Nov 2006 07:23:14
 *
 */
public class HighlightedItemsBoxMarkup1 implements MarkupDefault {

	public String getBetween() {
		return null;
	}

    public String getCss() {
        StringBuilder sb = new StringBuilder();
        sb.append("table.sidebar#highlighted_items { padding: 0px; }                                                                  ");
        sb.append("table.sidebar#highlighted_items td { padding: 0 5px; }                                                             ");
        sb.append("table.sidebar#highlighted_items td.bottom { padding: 3px 0px 3px 3px; font-size: 9px; }                            ");
        sb.append("table.sidebar#highlighted_items td.bottom a { text-decoration: none; }                                             ");
        sb.append("table.sidebar#highlighted_items table { margin: 5px 0px 0px 0px; padding: 0; border-bottom: 1px solid #bbc6e0; }   ");
        sb.append("table.sidebar#highlighted_items td.image,                                                                          ");
        sb.append("table.sidebar#highlighted_items td.details { background-color: #f3f4f8; padding-top: 5px; }                        ");
        sb.append("table.sidebar#highlighted_items td.image { padding: 0; }                                                           ");
        sb.append("table.sidebar#highlighted_items td.image img { border: 1px solid #bbc6e0; }                                        ");
        sb.append("table.sidebar#highlighted_items td.details { padding: 3px 2px 3px 0px; font-size: 10px; color: #636365; }          ");
        sb.append("table.sidebar#highlighted_items td.details span.price { color: #cc0033; font-weight: bold; }                       ");
        sb.append("table.sidebar#highlighted_items td.details a { font-size: 10px; color: black; }                                    ");
        sb.append("table.sidebar#highlighted_items td.details a:hover { color: #636365; text-decoration: none; }                      ");
        return sb.toString();
    }

	public String getBody() {

		StringBuilder sb = new StringBuilder();

		sb.append(new TableTag("inner", "center").setStyle("width: 100%"));
		sb.append("\n<tr><td class='image' align='center' valign='top'>[thumbnail?w=120&h=120&link=1&limit=1]</td></tr>\n");

		sb.append("<tr><td class='details' valign='top'>\n\n");
		sb.append("[item?link=1]\n");
		sb.append("[member?link=1]\n");
		sb.append("[pricing_sell?inc=1]\n");
		sb.append("[summary?max=50]\n\n");

		sb.append("</td></tr>");
		sb.append("</table>");

		return sb.toString();
	}

	public String getEnd() {
		return null;
	}

	public String getName() {
		return "Highlighted items box style 1";
	}

	public String getStart() {
        /*
		return "<style> table.sidebar#highlighted_items { padding: 0px; }\r\n" + " \r\n" + "table.sidebar#highlighted_items td {\r\n"
				+ " padding: 0 5px;\r\n" + "}\r\n" + " \r\n" + "table.sidebar#highlighted_items td.bottom {\r\n" + " padding: 3px 0px 3px 3px;\r\n"
				+ " font-size: 9px;\r\n" + "}\r\n" + " \r\n" + "table.sidebar#highlighted_items td.bottom a {\r\n" + " text-decoration: none;\r\n"
				+ "}\r\n" + " \r\n" + "table.sidebar#highlighted_items table {\r\n" + " margin: 5px 0px 0px 0px;\r\n" + " padding: 0;\r\n"
				+ " border-bottom: 1px solid #bbc6e0;\r\n" + "}\r\n" + " \r\n" + "table.sidebar#highlighted_items td.image,\r\n"
				+ "table.sidebar#highlighted_items td.details {\r\n" + " background-color: #f3f4f8;\r\n" + " padding-top: 5px;\r\n" + "}\r\n" + " \r\n"
				+ "table.sidebar#highlighted_items td.image img {\r\n" + " border: 1px solid #bbc6e0;\r\n" + " }\r\n" + " \r\n"
				+ "table.sidebar#highlighted_items td.details {\r\n" + " padding: 3px 2px 3px 0px;\r\n" + " font-size: 10px;\r\n"
				+ " color: #636365;\r\n" + "}\r\n" + " \r\n" + "table.sidebar#highlighted_items td.details span.price {\r\n" + " color: #cc0033;\r\n"
				+ " font-weight: bold;\r\n" + "}\r\n" + " \r\n" + "table.sidebar#highlighted_items td.details a {\r\n" + " font-size: 10px;\r\n"
				+ " color: black;\r\n" + "}\r\n" + " \r\n" + "table.sidebar#highlighted_items td.details a:hover {\r\n" + " font-size: 10px;\r\n"
				+ " color: #636365;\r\n" + " text-decoration: none;\r\n" + "} </style>";
				*/
        return null;
	}

	public int getTds() {
		return 0;
	}

}
