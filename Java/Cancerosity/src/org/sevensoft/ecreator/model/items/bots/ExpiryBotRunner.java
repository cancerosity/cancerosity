package org.sevensoft.ecreator.model.items.bots;

import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.ecreator.model.misc.Interval;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 7 Sep 2006 16:13:24
 *
 */
public class ExpiryBotRunner implements Runnable {

	private static Logger		logger	= Logger.getLogger("cron");
	private final RequestContext	context;
	private final List<ExpiryBot>	bots;

	public ExpiryBotRunner(RequestContext context, Interval interval) {
		this.context = context;
		this.bots = ExpiryBot.get(context, interval);
	}

	public void run() {

		logger.config("[ExpiryBotRunner] running");
		for (ExpiryBot bot : bots) {
			bot.run();
		}
	}

}
