package org.sevensoft.ecreator.model.items.options;

import java.util.List;

import org.sevensoft.ecreator.model.attributes.support.AttributeOwnerSupport;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 22 Nov 2006 21:56:48
 * 
 * A group of options that can be applied to an item
 *
 */
@Table("options_groups")
public class OptionGroup extends AttributeOwnerSupport implements Selectable {

	public static List<OptionGroup> get(RequestContext context) {
		return SimpleQuery.execute(context, OptionGroup.class);
	}

	/**
	 * The name of this option group
	 */
	private String	name;

	/**
	 * If null then will use the default attributes renderer
	 */
	private Markup	markup;

	protected OptionGroup(RequestContext context) {
		super(context);
	}

	public OptionGroup(RequestContext context, String name) {
		super(context);
		this.name = name;
		save();
	}

	public String getLabel() {
		return name;
	}

	public final Markup getMarkup() {
		return (Markup) (markup == null ? null : markup.pop());
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return getIdString();
	}

	public final void setMarkup(Markup markup) {
		this.markup = markup;
	}

	public void setName(String name) {
		this.name = name;
	}

}
