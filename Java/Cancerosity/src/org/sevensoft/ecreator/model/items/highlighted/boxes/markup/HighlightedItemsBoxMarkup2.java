package org.sevensoft.ecreator.model.items.highlighted.boxes.markup;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * @author sks 15 Nov 2006 20:23:37
 *
 */
public class HighlightedItemsBoxMarkup2 implements MarkupDefault {

	public String getCss() {
		return null;
	}

	public String getBetween() {
		return null;
	}

	public String getBody() {
		return null;
	}

	public String getEnd() {
		return null;
	}

	public String getName() {
		return "Highlighted items style 2";
	}

	public String getStart() {
		return null;
	}

	public int getTds() {
		return 0;
	}

}
