package org.sevensoft.ecreator.model.items.stock;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 4 Sep 2006 17:18:54
 * 
 * Contains details of stock inputs and manual adjustments
 *
 */
@Table("items_stock_audit")
public class Audit extends EntityObject {

	/**
	 * Stock after adjustment
	 */
	private int		stock;

	/**
	 * Adjustment level
	 */
	private int		adjustment;

	private DateTime	date;

	/**
	 * Item we are auditing
	 */
	private Item	item;

	/**
	 * User who did manual adjustment
	 */
	private User	user;

	/**
	 * A message detailing stock adjustment
	 */
	private String	message;

	private Audit(RequestContext context) {
		super(context);
	}

	public Audit(RequestContext context, Item item, int stock, int adjustment, String message) {
		super(context);
		this.item = item;
		this.stock = stock;
		this.adjustment = adjustment;
		this.message = message;

		this.date = new DateTime();
		save();
	}

	public int getAdjustment() {
		return adjustment;
	}

	public DateTime getDate() {
		return date;
	}

	public Item getItem() {
		return item;
	}

	public String getMessage() {
		return message;
	}

	public int getStock() {
		return stock;
	}

	public User getUser() {
		return user;
	}

}
