package org.sevensoft.ecreator.model.items.pricing.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class DiscountMarker implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
        Item account = (Item) context.getAttribute("account");

        if (!PermissionType.ItemPriceSummary.check(context, account, item.getItemType())) {
            return null;
        }

		//		if (!item.getItemType().isPromotionPrices())
		//			return null;

		if (!item.getPricing().hasPromotionPrice()) {
			return null;
		}

		//		Currency currency = (Currency) context.getAttribute("currency");
		String currencySymbol = (String) context.getAttribute("currencySymbol");

		//		Money discount = item.getPromotionDiscount(currency);
		//		if (discount == null)
		//			return null;

		String prefix = params.get("prefix");
		String suffix = params.get("suffix");

		StringBuilder sb = new StringBuilder();
		sb.append("<span class='price_discount'>");

		if (prefix != null)
			sb.append(prefix);

		sb.append("<span class='thediscount'>");
		sb.append(currencySymbol);
		//		sb.append(discount);
		sb.append("</span>");

		if (suffix != null)
			sb.append(suffix);

		sb.append("</span>");
		return sb.toString();
	}

	public Object getRegex() {
		return "pricing_discount";
	}

}
