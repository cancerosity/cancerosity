// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   Exporter.java

package org.sevensoft.ecreator.model.items.export;

import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.CsvSaver;
import com.ricebridge.csvman.CsvSpec;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

@Table("exporter")
public class Exporter extends EntityObject {

	private static final Logger	logger	= Logger.getLogger(Exporter.class.getName());
	private String			separator;
	private String			endLine;
	private ItemType			itemType;

	public Exporter(RequestContext context) {
		super(context);
		separator = ",";
		endLine = "\n";
	}

	public ExportField addField() {
		ExportField field = new ExportField(context, this);
		int nextPosition = new Query(context, "select max(position) from # where exporter=?").setTable(ExportField.class).setParameter(this).getInt() + 1;
		field.setPosition(nextPosition);
		field.setFieldType(ExportFieldType.Name);
		field.save();
		return field;
	}

	public synchronized boolean delete() {
		deleteFields();
		return super.delete();
	}

	private void deleteFields() {
		ExportField field;
		for (Iterator i = getFields().iterator(); i.hasNext(); field.delete())
			field = (ExportField) i.next();

	}

	public File export() throws IOException {
		CsvManager csvman = new CsvManager();
		CsvSpec spec = csvman.getCsvSpec();
		logger.fine((new StringBuilder()).append("using separator=").append(getSeparator()).toString());
		spec.setSeparator(separator);
		logger.fine((new StringBuilder()).append("using endLine=").append(getEndLine()).toString());
		spec.setEndOfLine(endLine);
		csvman.setCsvSpec(spec);
		File file = File.createTempFile("itemexport", ".csv");
		file.deleteOnExit();
		logger.fine((new StringBuilder()).append("temp file created=").append(file).toString());
		ItemSearcher searcher = new ItemSearcher(context);
		searcher.setVisibleOnly(false);
		searcher.setIncludeHidden(true);
		searcher.setStatus("LIVE");
		logger.fine((new StringBuilder()).append("using item type=").append(itemType.pop()).toString());
		searcher.setItemType(itemType);
		CsvSaver saver = csvman.makeSaver(file);
		saver.begin();
		String row[];
		List<ExportField> fields = getFields();
		row = makeCapRow(fields);
		saver.next(row);
		for (Iterator i = searcher.iterator(); i.hasNext(); saver.next(row)) {
			Item item = (Item) i.next();
			logger.fine((new StringBuilder()).append("logging item=").append(item).toString());
			row = makeRow(fields, item);
		}

		saver.end();
		return file;
	}

	public String getEndLine() {
		if (endLine == null)
			endLine = "\n";
		return endLine;
	}

	public List<ExportField> getFields() {
		return SimpleQuery.execute(context, ExportField.class, "exporter", this);
	}

	public ItemType getItemType() {
		return (ItemType) (itemType != null ? itemType.pop() : null);
	}

	public String getSeparator() {
		if (separator == null)
			separator = ",";
		return separator;
	}

	private String[] makeCapRow(List<ExportField> fields) {
		String row[] = new String[fields.size()];
		int n = 0;
		for (ExportField field : fields) {
			row[n] = field.getName();
			n++;
		}

		return row;
	}

	private String[] makeRow(List<ExportField> fields, Item item) {
		String row[] = new String[fields.size()];
		int n = 0;
		for (ExportField field : fields) {
			row[n] = field.export(item);
			n++;
		}

		return row;
	}

	public void setEndLine(String endLine) {
		this.endLine = endLine;
	}

	public void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}
}
