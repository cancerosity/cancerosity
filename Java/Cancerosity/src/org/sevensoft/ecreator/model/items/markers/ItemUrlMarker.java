package org.sevensoft.ecreator.model.items.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * @author sks 7 Oct 2007 19:00:27
 */
public class ItemUrlMarker extends MarkerHelper implements IItemMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
        String url = item.getUrl();
        return super.string(context, params, url);
    }

    public Object getRegex() {
        return "item_url";
    }

}
