package org.sevensoft.ecreator.model.items.export;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.RequestContext;

import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.CsvSaver;

/**
 * @author sks 15 Nov 2006 10:18:09
 *
 */
public class ItemCsvExporter {

	private ItemType		itemType;
	private RequestContext	context;
	private List<Attribute>	attributes;
	private int			size;
	private String		separator;

	public ItemCsvExporter(RequestContext context, ItemType itemType) {
		this.context = context;
		this.itemType = itemType;
		this.attributes = itemType.getAttributes();
		this.size = attributes.size() + 6;
		this.separator = ",";
	}

	/**
	 * Export all items of this item type to the temp file that will be returned 
	 * 
	 */
	public File export() throws IOException {

		File file = File.createTempFile("item_export", null);
		file.deleteOnExit();

		CsvManager csvman = new CsvManager();
		csvman.setSeparator(separator);
		CsvSaver saver = csvman.makeSaver(file);

		final ItemSearcher searcher = new ItemSearcher(context);
		searcher.setItemType(itemType);
		searcher.setStatus("Live");

		saver.begin();
		saver.next(getHeader());

		for (Item item : searcher) {

			String[] row = new String[size];

			// name
			row[0] = item.getName();

			// date created
			row[1] = item.getDateCreated().toString("dd-MM-yy HH:mm");

			if (item.getItemType().isContent()) {
				row[2] = item.getContent();
			} else {
				row[2] = null;
			}

			if (item.getItemType().isCategories()) {
				row[3] = item.getCategoryNames();
			} else {
				row[3] = null;
			}

			// date updated
			row[4] = item.getDateUpdated().toString("dd-MM-yy HH:mm");

			if (ItemModule.Pricing.enabled(context, item)) {
				row[5] = item.getSellPrice().toEditString();
			} else {
				row[5] = null;
			}

			int n = 6;
			for (Attribute attribute : attributes) {
				row[n] = StringHelper.implode(item.getAttributeValues(attribute), ", ");
				n++;
			}

			CollectionsUtil.replaceNulls(row, "");
            HtmlHelper.replaseSymbolsWithUTF(row);
			saver.next(row);
		}

		saver.end();
		return file;
	}

	/**
	 * 
	 */
	private String[] getHeader() {

		String[] row = new String[size];

		row[0] = "Name";
		row[1] = "Date created";
		row[2] = "Description";
		row[3] = "Categories";
		row[4] = "Date updated";
		row[5] = "Sell price";

		int n = 6;
		for (Attribute attribute : attributes) {
			row[n] = attribute.getName();
			n++;
		}

		CollectionsUtil.replaceNulls(row, "");
		return row;
	}
}
