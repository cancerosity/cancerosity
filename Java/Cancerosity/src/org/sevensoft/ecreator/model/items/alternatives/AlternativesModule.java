package org.sevensoft.ecreator.model.items.alternatives;

import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 11 Oct 2006 16:57:46
 * 
 * A 'group' of relations with a name, eg, alternatives or local or related or 'things you might like'
 *
 */
@Table("items_modules_alternatives")
public class AlternativesModule extends EntityObject {

	/**
	 * 
	 */
	private ItemType	itemType;

	/**
	 * **BC
	 */
	private String	alternativesCaption;

	/****BC
	 * 
	 */
	private Markup	alternativesMarkup;

	/**
	 * How many relations to show in items of this type
	 * **BC
	 */
	private int		alternativesQty;

    private Attribute alternativesAttribute;

	private AlternativesModule(RequestContext context) {
		super(context);
	}

	public AlternativesModule(RequestContext context, ItemType itemType) {
		super(context);
		this.itemType = itemType;

		save();
	}

	public String getAlternativesCaption() {
		return alternativesCaption;
	}

	public Markup getAlternativesMarkup() {

		if (alternativesMarkup == null) {

			alternativesMarkup = new Markup(context, new AltBlockMarkupDefault());
			alternativesMarkup.setName("Default alternatives markup for " + itemType.getName());
			alternativesMarkup.save();

			save();
		}

		return alternativesMarkup.pop();
	}

	public int getAlternativesQty() {
		return alternativesQty;
	}

	public boolean isAlternatives() {
		return alternativesQty > 0;
	}

    public Attribute getAlternativesAttribute() {
        return alternativesAttribute == null ? null : (Attribute) alternativesAttribute.pop();
    }

    public void setAlternativesCaption(String alternativesCaption) {
		this.alternativesCaption = alternativesCaption;
	}

	public void setAlternativesMarkup(Markup alternativesMarkup) {
		this.alternativesMarkup = alternativesMarkup;
	}

	public void setAlternativesQty(int i) {
		this.alternativesQty = i;
	}

    public void setAlternativesAttribute(Attribute alternativesAttribute) {
        this.alternativesAttribute = alternativesAttribute;
    }
}
