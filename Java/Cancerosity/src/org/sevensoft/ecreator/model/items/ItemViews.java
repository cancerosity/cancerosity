package org.sevensoft.ecreator.model.items;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 7 Jan 2007 09:43:51
 *
 */
@Table("items_views")
public class ItemViews extends EntityObject {

	public static ItemViews get(RequestContext context) {
		return SimpleQuery.get(context, ItemViews.class, "sessionId", context.getSessionId(), "date", new Date());
	}

	private Date	date;
	private String	sessionId;
	private int		views;

	protected ItemViews(RequestContext context) {
		super(context);
	}

	public ItemViews(RequestContext context, String sessionId) {
		super(context);
		this.sessionId = sessionId;
		this.views = 1;
		this.date = new Date();
		save();
	}

	public String getSessionId() {
		return sessionId;
	}

	public int getViews() {
		return views;
	}

	public void inc() {
		this.views++;
		save();
	}

	public void setViews(int views) {
		this.views = views;
	}

}
