package org.sevensoft.ecreator.model.items.pricing.overrides;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 16 Aug 2006 20:18:18
 *
 */
@Table("items_prices_requests")
public class PriceRequest extends EntityObject {

	private Item	account;
	private Item	item;
	private Money	price;
	private String	message;

	private PriceRequest(RequestContext context) {
		super(context);
	}

	public PriceRequest(RequestContext context, Item account, Item item, Money price) {
		super(context);
		this.account = account;
		this.item = item;
		this.price = price;

		save();
	}

	public Item getAccount() {
		return account;
	}

	public Item getItem() {
		return item;
	}

	public Money getPrice() {
		return price;
	}

}
