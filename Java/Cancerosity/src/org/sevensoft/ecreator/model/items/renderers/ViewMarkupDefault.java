package org.sevensoft.ecreator.model.items.renderers;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * @author sks 26-Jan-2006 15:56:32
 *
 */
public class ViewMarkupDefault implements MarkupDefault {

	public String getBetween() {
		return null;
	}

	public String getCss() {
		StringBuilder sb = new StringBuilder();

        sb.append("table.item { width: 100%; font-family: \"Segoe UI\", \"Trebuchet MS\", Arial, Helvetica, sans-serif; font-size: 12px; color: #333333; }\n");
                sb.append("table.item select, table.item option { font-weight: bold; color: #333333; }\n");
                sb.append("table.item td { text-align: left; vertical-align: top; }\n");
                sb.append("table.item td.image { width: 170px; vertical-align: top; text-align: center; padding-right: 15px; }\n");
                sb.append("table.item td.image div.imgbrowser { height: 16px; overflow: hidden; margin-bottom: 5px; }\n");
                sb.append("table.item td.image div.smthumb img { margin-right: 3px; margin-bottom: 3px; }\n");
                sb.append("div.socialnet { width: 152px; margin: 16px auto 25px auto; }\n");
                sb.append("div#speccontainer { width: 360px; background: #f9f9f9; background: #ffffff url(../files/graphics/markup/default/gradient2.gif) repeat-x scroll left top; padding: 20px 25px; margin-top: -10px; }\n");
                sb.append("table.item div.pricing { font-weight: bold; color: #4d9dcb; }\n");
                sb.append("table.item div.pricing span.price_inc { font-size: 18px; color: #ff9211; }\n");
                sb.append("table.item div.pricing span.price { display: block; }\n");
                sb.append("table.item div.pricing span.inc { font-size: 12px; color: #4d9dcb; }\n");
                sb.append("table.item div.pricing span.rrp { display: block; font-weight: normal; }\n");
                sb.append("table.item span.stock { display: block; font-weight: bold; background: transparent url(../files/graphics/markup/default/stock.gif) no-repeat scroll left center; padding: 4px 0px 3px 25px; margin-top: 5px; }\n");
                sb.append("table.ordering { margin: 15px 0; }\n");
                sb.append("table.ordering td.quan { padding-right: 10px; }\n");
                sb.append("div#breadcrumb div.trail { display: none; }\n");
                sb.append("div.trail { margin-bottom: 20px; padding: 10px 0; border-bottom: 1px dotted #999999; font-family: Verdana, sans-serif; font-size: 10px; }\n");
                sb.append("span.goback { display: block; position: relative; top: 26px; left: 330px; font-family: Verdana, sans-serif; font-size: 10px; }\n");
                sb.append("span.goback a, div.trail a { color: #454545; text-decoration: none; text-transform: uppercase; }\n");
                sb.append("span.goback a:hover, div.trail a:hover { color: #454545; text-decoration: underline; }\n");
                sb.append("div.trail a.current, div.trail a.current:hover { display: block; font-weight: bold; }\n");
                sb.append("div.item_details { font-size: 14px; padding-bottom: 5px; border-bottom: 1px dotted #999999; margin: 10px 0; }\n");
                sb.append("div.item_details span.desc { display: block; float: right; font-size: 11px; margin-top: 3px; }\n");
                sb.append("table.attributes td.attribute-label { padding-right: 20px; }\n");
                sb.append("div.refer { margin-top: 10px; }\n");
                sb.append("div.refer img { margin-bottom: -5px; margin-right: 5px; }\n");
                sb.append("div.imgbrowser { margin-top: 4px; font-weight: bold; }\n");
                sb.append("div.imgbrowser img { margin-bottom: -3px; margin-right: 5px; }\n");
                sb.append("table.ec_external_links { margin: 10px 0; }\n");
                sb.append("table.ec_external_links td { padding-right: 10px; }\n");
                sb.append("table.options { height: 50px; margin-top: 15px; margin-bottom: 5px; background: #dddddd url(../files/graphics/markup/default/options.png) no-repeat scroll right 5px; border-top: 5px solid #dddddd; border-bottom: 5px solid #dddddd; border-right: 10px solid #dddddd; }\n");
                sb.append("table.options td { padding: 5px 10px; }\n");
                sb.append("table.options td.label { padding-top: 7px; }\n");
                sb.append("table.options td.input { padding-right: 45px; }\n");
                sb.append("table.options td.input select, table.options td.input option { font-family: \"Segoe UI\", \"Trebuchet MS\", Arial, Helvetica, sans-serif; font-size: 12px; color: #333333; }\n");
                sb.append("table.item_accessories td { vertical-align: middle; }\n");
                sb.append("table.item_accessories td.image { padding-bottom: 5px; padding-right: 5px; }\n");
                sb.append("table.item_accessories td.detail { padding-left: 10px; }\n");
                sb.append(".ec_img { float: left; }");


        sb.append("table.item { width: 100%; font-family: \"Segoe UI\", \"Trebuchet MS\", Arial, Helvetica, sans-serif; font-size: 12px; color: #333333; }\n");
        sb.append("table.item select, table.item option { font-weight: bold; color: #333333; }\n");
        sb.append("table.item td { text-align: left; vertical-align: top; }\n");
        sb.append("table.item td.image { text-align: center; padding-right: 15px; }\n");
        sb.append("div.socialnet { width: 152px; margin: 15px auto 25px auto; }\n");
        sb.append("div#speccontainer { width: 360px; background: #f9f9f9; background: #ffffff url(../files/graphics/markup/default/gradient2.gif) repeat-x scroll left top; padding: 20px 25px; margin-top: -10px; }\n");
        sb.append("table.item div.pricing { font-weight: bold; color: #4d9dcb; }\n");
        sb.append("table.item div.pricing span.price_inc { font-size: 18px; color: #ff9211; }\n");
        sb.append("table.item div.pricing span.price { display: block; }\n");
        sb.append("table.item div.pricing span.inc { font-size: 12px; color: #4d9dcb; }\n");
        sb.append("table.item div.pricing span.rrp { display: block; font-weight: normal; }\n");
        sb.append("table.item span.stock { display: block; font-weight: bold; background: transparent url(../files/graphics/markup/default/stock.gif) no-repeat scroll left center; padding: 4px 0px 3px 25px; margin-top: 5px; }\n");
        sb.append("table.ordering { margin: 15px 0; }\n");
        sb.append("table.ordering td.quan { padding-right: 10px; }\n");
        sb.append("div#breadcrumb div.trail { display: none; }\n");
        sb.append("div.trail { margin-bottom: 20px; padding: 10px 0; border-bottom: 1px dotted #999999; font-family: Verdana, sans-serif; font-size: 10px; }\n");
        sb.append("span.goback { display: block; position: relative; top: 26px; left: 330px; font-family: Verdana, sans-serif; font-size: 10px; }\n");
        sb.append("span.goback a, div.trail a { color: #454545; text-decoration: none; text-transform: uppercase; }\n");
        sb.append("span.goback a:hover, div.trail a:hover { color: #454545; text-decoration: underline; }\n");
        sb.append("div.trail a.current, div.trail a.current:hover { display: block; font-weight: bold; }\n");
        sb.append("div.item_details { font-size: 14px; padding-bottom: 5px; border-bottom: 1px dotted #999999; margin: 10px 0; }\n");
        sb.append("div.item_details span.desc { display: block; float: right; font-size: 11px; margin-top: 3px; }\n");
        sb.append("table.attributes td.attribute-label { padding-right: 20px; }\n");
        sb.append("div.refer { margin-top: 10px; }\n");
        sb.append("div.refer img { margin-bottom: -5px; margin-right: 5px; }\n");
        sb.append("div.imgbrowser { margin-top: 4px; font-weight: bold; }\n");
        sb.append("div.imgbrowser img { margin-bottom: -3px; margin-right: 5px; }\n");
        sb.append("table.ec_external_links { margin: 10px 0; }\n");
        sb.append("table.ec_external_links td { padding-right: 10px; }\n");
        sb.append("table.options { height: 50px; margin-top: 15px; margin-bottom: 5px; background: #dddddd url(../files/graphics/markup/default/options.png) no-repeat scroll right 5px; border-top: 5px solid #dddddd; border-bottom: 5px solid #dddddd; border-right: 10px solid #dddddd; }\n");
        sb.append("table.options td { padding: 5px 10px; }\n");
        sb.append("table.options td.label { padding-top: 7px; }\n");
        sb.append("table.options td.input { padding-right: 45px; }\n");
        sb.append("table.options td.input select, table.options td.input option { font-family: \"Segoe UI\", \"Trebuchet MS\", Arial, Helvetica, sans-serif; font-size: 12px; color: #333333; }\n");
        sb.append("table.item_accessories td { vertical-align: middle; }\n");
        sb.append("table.item_accessories td.image { padding-bottom: 5px; padding-right: 5px; }\n");
        sb.append("table.item_accessories td.detail { padding-left: 10px; }\n");
        sb.append(".ec_img { float: left; }\n");
        return sb.toString();
	}

	public String getBody() {

        StringBuilder sb = new StringBuilder();

        sb.append("<div id=\"breadcrumb\">[breadcrumb?sep= � ]</div>\n");
        sb.append("<table cellspacing='0' cellpadding='0' class='item' width='100%'>\n");
        sb.append("<tr><td class='image'><a class=\"thickbox\" href=\"[image_url?limit=1]\" title=\"[title]\">[thumbnail?w=160&h=160&limit=1]</a>\n");
        sb.append("<div class=\"imgbrowser\"><a href=\"[image_url?limit=1]\" title=\"[title]\" class=\"thickbox\" rel=\"gallery\"><img src=\"./files/graphics/markup/default/zoom.gif\" alt='+' /> Click to enlarge images</a>\n");
        sb.append("</div><div class=\"smthumb\">[thumbnail?w=50&h=50&imglink=1&class=thickbox\" rel=\"gallery]</div></td>\n");
        sb.append("<td class='details' valign='top'><span class=\"goback\"><a href=\"javascript:history.go(-1)\" title=\"Go back\">Go back �</a></span>\n");
        sb.append("<div id=\"speccontainer\">\n");
        sb.append("<!-- AddThis Button BEGIN -->\n");
        sb.append("<div class=\"addthis_toolbox addthis_default_style \">\n");
        sb.append("<a class=\"addthis_button_facebook_like\"></a>\n");
        sb.append("<a class=\"addthis_button_tweet\"></a>\n");
        sb.append("<a class=\"addthis_counter addthis_pill_style\"></a>\n");
        sb.append("</div>\n");
        sb.append("<script type=\"text/javascript\" src=\"http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4ea74c2571544e52\"></script>\n");
        sb.append("<!-- AddThis Button END -->\n");
        sb.append("<div class='pricing'>\n");
        sb.append("[pricing_sell?inc=1&suffix=<span class=\"inc\"> inc VAT</span>]\n");
        sb.append("[pricing_sell?suffix= ex VAT]\n");
        sb.append("[pricing_rrp?prefix=<span class=\"discount\">RRP: ] [pricing_rrp_discount?prefix=- you save &suffix=!</span>]\n");
        sb.append("[pricing_original?inc=1&prefix=Original price: ]\n");
        sb.append("[pricing_discount?inc=1&prefix=Discount price: ]\n");
        sb.append("[pricing_able2buy?popup=1&text=Click here for example credit options]\n");
        sb.append("</div>\n");
        sb.append("[stock?prefix=Availability: &text=true]\n");
        sb.append("[options]\n");
        sb.append("<table class=\"ordering\" cellspacing=\"0\" cellpadding=\"0\"><tr><td class=\"quan\" align=\"left\">[ordering_qty]</td><td class=\"add\" align=\"left\">[ordering_buy?src=./files/graphics/markup/default/add_to_basket.gif]</td></tr></table>\n");
        sb.append("<div class=\"item_details\">Details:</div>\n");
        sb.append("[attributes_table]\n");
        sb.append("[external_links]\n");
        sb.append("</div>\n");
        sb.append("</td></tr>\n");
        sb.append("<tr><td colspan=\"2\">\n");
        sb.append("<div class=\"item_details\">Description:</div>\n");
        sb.append("[content]\n");
        sb.append("</td></tr>\n");
        sb.append("<tr><td colspan=\"2\">[accessories]</td></tr>\n");
        sb.append("</table>\n");
        sb.append("[alternatives]\n");
        sb.append("[attachments]");


/*      sb.append("<div id='breadcrumb'>[breadcrumb?sep= � ]</div>");
        sb.append("<table cellspacing='0' cellpadding='0' class='item' width='100%'>");
        sb.append("<tr><td class='image' width='170' valign='top'>");
        sb.append("<a href=\"#\" onclick=\"window.open('image-browser.do?item=[id]','popupwindow', 'width=620,height=550'); return false;\">[thumbnail?w=160&h=160&limit=1]</a>");
        sb.append("<div class=\"imgbrowser\"><a href=\"#\" onclick=\"window.open('image-browser.do?item=[id]','popupwindow', 'width=620,height=550'); return false;\"><img src=\"./files/graphics/markup/default/zoom.gif\" alt='+' /></a>");
        sb.append("[image_browser?text=Click for more images]</div>");
        sb.append("<script type=\"text/javascript\" src=\"http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4d4bd3f76765874d\"></script>");
        sb.append("<!-- AddThis Button END -->");
        sb.append("</div>                                ");
        sb.append("</td><td class='details' valign='top'>");
        sb.append("<span class=\"goback\"><a href=\"javascript:history.go(-1)\" title=\"Go back\">Go back �</a></span>");
        sb.append("<div id=\"speccontainer\">");
        sb.append("<!-- AddThis Button BEGIN -->");
        sb.append("<div class=\"addthis_toolbox addthis_default_style \">");
        sb.append("<a class=\"addthis_button_facebook_like\" fb:like:layout=\"button_count\"></a>");
        sb.append("<a class=\"addthis_button_tweet\"></a>");
        sb.append("<a class=\"addthis_counter addthis_pill_style\"></a>");
        sb.append("</div>");
        sb.append("<script type=\"text/javascript\" src=\"http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4d4bd56d52d7e04f\"></script>");
        sb.append("<!-- AddThis Button END -->");
        sb.append("<div class='pricing'>");
        sb.append("[pricing_sell?inc=1&suffix=<span class=\"inc\"> inc VAT</span>][pricing_sell?suffix= ex VAT][pricing_rrp?prefix=<span class=\"discount\">RRP: ] [pricing_rrp_discount?prefix=- you save &suffix=!</span>]");
        sb.append("[pricing_original?inc=1&prefix=Original price: ]");
        sb.append("[pricing_discount?inc=1&prefix=Discount price: ]");
        sb.append("[pricing_able2buy?popup=1&text=Click here for example credit options]");
        sb.append("</div>");
        sb.append("[stock?prefix=Availability: &text=true]");
        sb.append("[options]");
        sb.append("<table class=\"ordering\" cellspacing=\"0\" cellpadding=\"0\"><tr><td class=\"quan\" align=\"left\">[ordering_qty]</td><td class=\"add\" align=\"left\">[ordering_buy?src=./files/graphics/markup/default/add_to_basket.gif]</td></tr></table>");
        sb.append("<div class=\"item_details\">Details:</div>");
        sb.append("[attributes_table]");
        sb.append("[external_links]");
        sb.append("</div>");
        sb.append("</td></tr>");
        sb.append("<tr><td colspan=\"2\"><div class=\"item_details\">Description:</div>");
        sb.append("[content]</td></tr>");
        sb.append("<tr><td colspan=\"2\">[accessories]</td></tr>");
        sb.append("</table>");
        sb.append("[alternatives]");
        sb.append("[attachments]");
*/
/*
        sb.append("[breadcrumb?sep= � ]");
        sb.append("<table cellspacing='0' cellpadding='0' class='item' width='100%'>");
        sb.append("<tr><td class='image' width='170' valign='top'>");
        sb.append("<a href='#' onclick='window.open('image-browser.do?item=[id]','popupwindow', " +
                "'width=620,height=550'); return false;'>[thumbnail?w=160&h=160&limit=1]</a>" +
                "<div class='imgbrowser'><a href='#' onclick='window.open('image-browser.do?item=[id]','popupwindow', " +
                "'width=620,height=550'); return false;'><img src='./files/graphics/markup/default/zoom.gif' alt='+' /></a> ");
        sb.append("[image_browser?text=Click for more images]</div>");
        sb.append("<div class='refer'>[referafriend?src=./files/graphics/markup/default/tell_a_friend.gif]<strong> " +
                "[referafriend?text=Tell a friend]</strong></div>");
        sb.append("<div class='socialnet'><strong>Bookmark & share:</strong><br /><br /><a target='_blank' " +
                "href='http://del.icio.us/post?url=[item_url]&title=[title]'><img " +
                "src='./files/graphics/markup/default/delicious.gif' alt='Bookmark with Del.icio.us' " +
                "title='Bookmark with Del.icio.us'></a> <a target='_blank' " +
                "href='http://digg.com/submit?url=[item_url]&title=[title]'><img " +
                "src='./files/graphics/markup/default/digg.gif' alt='Digg This!' title='Digg This!'></a>");
        sb.append("<a target='_blank' href='http://reddit.com/submit?url=[item_url]&title=[title]'><img " +
                "src='./files/graphics/markup/default/reddit.gif' alt='Post to Reddit' title='Post to Reddit'></a> " +
                "<a target='_blank' href='http://www.facebook.com/sharer.php?u=[item_url]'><img " +
                "src='./files/graphics/markup/default/facebook.gif' alt='Share on Facebook' title='Share on Facebook'></a>");
        sb.append("<a target='_blank' href='http://www.stumbleupon.com/submit?url=[item_url]&title=[title]'><img " +
                "src='./files/graphics/markup/default/stumbleupon.gif' alt='Post to StumbleUpon' " +
                "title='Post to StumbleUpon'></a>");
        sb.append("<a target='_blank' href='http://www.kaboodle.com/za/additem?get=1&url=[item_url]&title=[title]'>" +
                "<img src='./files/graphics/markup/default/kaboodle.gif' alt='Post to Kaboodle' title='Post to Kaboodle'></a>");
        sb.append("<a target='_blank' href='http://myweb2.search.yahoo.com/myresults/bookmarklet?t=[title]&u=[item_url]'>" +
                "<img src='./files/graphics/markup/default/yahoo.gif' alt='Bookmark with Yahoo' title='Bookmark with Yahoo'></a> ");
        sb.append("a target='_blank' href='http://www.google.com/bookmarks/mark?op=add&bkmk=[item_url]&title=[title]'>" +
                "<img src='./files/graphics/markup/default/google.gif' alt='Bookmark with Google' " +
                "title='Bookmark with Google'></a>");
        sb.append("<script>function bebo_click() {u=location.href;t=document.title;" +
                "window.open('http://www.bebo.com/c/share?Url='+encodeURIComponent(u)+'&Title='+encodeURIComponent(t)+" +
                "'&MID=8974376238&TUUID=fc7850b8-964c-47bd-8a91-db1d2a5cad3c','sharer','toolbar=0,status=0,width=626," +
                "height=436');return false;}</script>");
        sb.append("<style> html .b_share_link { padding:4px 0 0 20px; height:16px; background:" +
                "url(./files/graphics/markup/default/bebo.gif) no-repeat top left; }</style>");
        sb.append("<a href='http://www.bebo.com/c/share?Url=<url>' onclick='return bebo_click()' target='_blank' " +
                "class='b_share_link'></a> <a href='http://twitthis.com/twit?url=[item_url]&title=[title]' " +
                "title='Share on Twitter'><img src='./files/graphics/markup/default/twitter.gif' alt='Share on Twitter' /></a>");
        sb.append("<script type='text/javascript'>");
        sb.append("function GetThis(T, C, U, L) {");
        sb.append("var targetUrl = 'http://www.myspace.com/index.cfm?fuseaction=postto&' + 't=' + encodeURIComponent(T)" +
                " + '&c=' + encodeURIComponent(C) + '&u=' + encodeURIComponent(U) + '&l=' + L;");
        sb.append("window.open(targetUrl); }");
        sb.append("</script>");
        sb.append("<a href='javascript:GetThis('[title] on [comp_name]', 'I saw this link: [item_url] and just had to " +
                "share it!', 'http://www.myspace.com', 3)'><img src='./files/graphics/markup/default/myspace.gif' " +
                "border='0' alt='Post to MySpace' /></a></div>");
        sb.append("</td><td class='details' valign='top'>");
        sb.append("<div id=\"speccontainer\"><div class='pricing'>");
        sb.append("[pricing_sell?inc=1&suffix=<span class='inc'> inc VAT</span>][pricing_sell?suffix= ex VAT]" +
                "[pricing_rrp?prefix=<span class='discount'>RRP: ]");
        sb.append("[pricing_rrp_discount?prefix=- you save &suffix=!</span>]");
        sb.append("[pricing_original?inc=1&prefix=Original price: ]");
        sb.append("[pricing_discount?inc=1&prefix=Discount price: ]");
        sb.append("[pricing_able2buy?popup=1&text=Click here for example credit options]");
        sb.append("</div>");
        sb.append("[stock?prefix=Availability: &text=true]");
        sb.append("[accessories]");
        sb.append("[options]");
        sb.append("<table class='ordering' cellspacing='0' cellpadding='0'><tr><td class='quan' align='left'>" +
                "[ordering_qty]</td><td class='add' align='left'>" +
                "[ordering_buy?src=./files/graphics/markup/default/add_to_basket.gif]</td></tr></table>");
        sb.append("<div class=\"item_details\">Details:</div>");
        sb.append("[attributes_table]");
        sb.append("[external_links]");
        sb.append("</div>");
        sb.append("</td></tr>");
        sb.append("<tr><td colspan='2'><div class='item_details'>Description:</div>");
        sb.append("[content]</td></tr>");
        sb.append("</table>");
        sb.append("[alternatives]");
        sb.append("[attachments]");
*/

        /*sb.append("<div id='breadcrumb'>[breadcrumb?sep= � ]</div>\n");
        sb.append("<table cellspacing='0' cellpadding='0' class='item' width='100%'>\n");
        sb.append("<tr><td class='image' width='170' valign='top'>\n");
        sb.append("<a href=\"#\" onclick=\"window.open('image-browser.do?item=[id]','popupwindow', 'width=620,height=550'); return false;\">[thumbnail?w=160&h=160&limit=1]</a>\n" +
                "<div class=\"imgbrowser\"><a href=\"#\" onclick=\"window.open('image-browser.do?item=[id]','popupwindow', 'width=620,height=550'); return false;\"><img src=\"./files/graphics/markup/default/zoom.gif\" alt='+' /></a>\n");
        sb.append("[image_browser?text=Click for more images]</div>\n");
        sb.append("<div class=\"refer\">[referafriend?src=./files/graphics/markup/default/tell_a_friend.gif]<strong> [referafriend?text=Tell a friend]</strong></div>\n");
        sb.append("<div class=\"socialnet\"><strong>Bookmark & share:</strong><br /><br /><a target=\"_blank\" href=\"http://del.icio.us/post?url=[item_url]&title=[title]\"><img src=\"./files/graphics/markup/default/delicious.gif\" " +
                "alt=\"Bookmark with Del.icio.us\" title=\"Bookmark with Del.icio.us\"></a> <a target=\"_blank\" href=\"http://digg.com/submit?url=[item_url]&title=[title]\"><img src=\"./files/graphics/markup/default/digg.gif\" alt=\"Digg This!\" " +
                "title=\"Digg This!\"></a> <a target=\"_blank\" href=\"http://reddit.com/submit?url=[item_url]&title=[title]\"><img src=\"./files/graphics/markup/default/reddit.gif\" alt=\"Post to Reddit\" title=\"Post to Reddit\"></a> " +
                "<a target=\"_blank\" href=\"http://www.facebook.com/sharer.php?u=[item_url]\"><img src=\"./files/graphics/markup/default/facebook.gif\" alt=\"Share on Facebook\" title=\"Share on Facebook\"></a> <a target=\"_blank\" " +
                "href=\"http://www.stumbleupon.com/submit?url=[item_url]&title=[title]\"><img src=\"./files/graphics/markup/default/stumbleupon.gif\" alt=\"Post to StumbleUpon\" title=\"Post to StumbleUpon\"></a> <a target=\"_blank\" " +
                "href=\"http://www.kaboodle.com/za/additem?get=1&url=[item_url]&title=[title]\"><img src=\"./files/graphics/markup/default/kaboodle.gif\" alt=\"Post to Kaboodle\" title=\"Post to Kaboodle\"></a> <a target=\"_blank\" " +
                "href=\"http://myweb2.search.yahoo.com/myresults/bookmarklet?t=[title]&u=[item_url]\"><img src=\"./files/graphics/markup/default/yahoo.gif\" alt=\"Bookmark with Yahoo\" title=\"Bookmark with Yahoo\"></a> <a target=\"_blank\" " +
                "href=\"http://www.google.com/bookmarks/mark?op=add&bkmk=[item_url]&title=[title]\"><img src=\"./files/graphics/markup/default/google.gif\" alt=\"Bookmark with Google\" title=\"Bookmark with Google\"></a></div>\n");
        sb.append("</td><td class='details' valign='top'><span class=\"goback\"><a href=\"javascript:history.go(-1)\" title=\"Go back\">Go back �</a></span>\n");
        sb.append("<div id=\"speccontainer\"><div class='pricing'>\n");
        sb.append("[pricing_sell?inc=1&suffix=<span class=\"inc\"> inc VAT</span>][pricing_sell?suffix= ex VAT][pricing_rrp?prefix=<span class=\"discount\">RRP: ] [pricing_rrp_discount?prefix=- you save &suffix=!</span>]\n");
        sb.append("[pricing_original?inc=1&prefix=Original price: ]\n");
        sb.append("[pricing_discount?inc=1&prefix=Discount price: ]\n");
        sb.append("[pricing_able2buy?popup=1&text=Click here for example credit options]\n");
        sb.append("</div>\n");
        sb.append("[stock?prefix=Availability: &text=true]\n");
        sb.append("[options]\n");
        sb.append("<table class=\"ordering\" cellspacing=\"0\" cellpadding=\"0\"><tr><td class=\"quan\" align=\"left\">[ordering_qty]</td><td class=\"add\" align=\"left\">[ordering_buy?src=./files/graphics/markup/default/add_to_basket.gif]</td></tr></table>\n");
        sb.append("<div class=\"item_details\">Details:</div>\n");
        sb.append("[attributes_table]\n");
        sb.append("[external_links]\n");
        sb.append("</div>\n");
        sb.append("</td></tr>\n");
        sb.append("<tr><td colspan=\"2\"><div class=\"item_details\">Description:</div>\n");
        sb.append("[content]</td></tr>\n");
        sb.append("<tr><td colspan=\"2\">[accessories]</td></tr>\n");
        sb.append("</table>\n");
        sb.append("[alternatives]\n");
        sb.append("[attachments]\n");*/
        /*sb.append("<table cellspacing='0' cellpadding='0' class='item' width='100%'>\n");
		sb.append("<tr><td class='image' width='170' align='center' valign='top'>\n");
		sb.append("[thumbnail?w=160&h=160&limit=1]\n");
		sb.append("<div>[image_browser?text=Click for more images]</div>\n");
		sb.append("</td><td class='details' valign='top'>\n");
		sb.append("<div class='pricing'>\n");
		sb.append("[pricing_sell?inc=1&prefix=Price: ]\n");
		sb.append("[pricing_rrp] [pricing_rrp_discount?prefix=- you save &suffix=!]\n");
		sb.append("[pricing_sell?prefix=Ex VAT: ]\n");
		sb.append("[pricing_original?inc=1&prefix=Original price: ]\n");
		sb.append("[pricing_discount?inc=1&prefix=Discount price: ]\n");
		sb.append("[pricing_able2buy?popup=1&text=Click here for example credit options]\n");
		sb.append("</div>\n");
		sb.append("[stock?prefix=Availability: &text=true]\n");
		sb.append("[accessories]\n");
		sb.append("[options]\n");
		sb.append("<table class=\"ordering\" cellspacing=\"0\" cellpadding=\"0\"><tr><td class=\"quan\" align=\"left\">[ordering_qty]</td>"
				+ "<td class=\"add\" align=\"left\">[ordering_buy?label=Add to basket]</td></tr></table>");
		sb.append("[attributes_table]\n");
		sb.append("[external_links]\n");
		sb.append("</td></tr>\n");
		sb.append("<tr><td colspan=\"2\"><div class=\"description\"><b>Description</b></div>\n");
		sb.append("[content]</td></tr>\n");
		sb.append("</table>\n");
		sb.append("[alternatives]\n");
		sb.append("[attachments]\n");*/

		return sb.toString();
	}

	public String getEnd() {
		return null;
	}

	public String getName() {
		return null;
	}

	public String getStart() {

		/*StringBuilder sb = new StringBuilder();

		sb.append("<style>");
		sb.append("table.item { font-family: Verdana, sans-serif; font-size: 11px; color: #333333; }");
		sb.append("table.item td.image img { border: 0; }");
		sb.append("table.item td.image div { padding-top: 3px; margin-bottom: 10px; }");
		sb
				.append("table.item td.image div a { font-family: Verdana, sans-serif; font-size: 11px; padding-left: 15px; background: transparent url(template-data/view_imgzoom.gif) no-repeat left bottom; }");
		sb.append("table.item div.pricing span { display: block; font-family: Arial, Helvetica, sans-serif; }");
		sb.append("table.item div.pricing { margin-bottom: 10px; }");
		sb.append("table.item div.pricing span.price_inc { font-size: 16px; color: #ee0000; font-weight: bold; }");
		sb.append("table.item div.pricing span.rrp { display: inline; font-size: 13px; color: orange; font-weight: bold; }");
		sb.append("table.item div.pricing span.rrp_discount { display: inline; font-size: 13px; color: #ee0000; font-weight: bold; }");
		sb.append("table.item div.pricing span.price { font-size: 12px; color: #777777; }");
		sb.append("table.item div.pricing span.price_original { font-size: 12px; color: orange; font-weight: bold; }");
		sb.append("table.item div.pricing span.price_discount { font-size: 12px; color: #ee0000; font-weight: bold; }");
		sb.append("table.item span.stock { display: block; margin-bottom: 10px; font-size: 12px; font-weight: bold; color: #777777; }");
		sb.append("table.attributes { margin-bottom: 10px; margin-top: 2px; *//*border-top: 1px solid #cccccc; border-bottom: 1px solid #cccccc;*//* }");
		sb.append("table.attributes td.attribute-label { font-weight: bold; padding-bottom: 3px; padding-top: 1px; }");
		sb.append("table.attributes td.attribute-value { padding-left: 5px; padding-bottom: 3px; padding-top: 1px; }");
		sb.append("table.options { margin-bottom: 10px; }");
		sb.append("table.options td.label { font-weight: bold; padding-right: 5px; padding-top: 3px; }");
		sb.append("table.options td.input select,");
		sb.append("table.options td.input option { margin: 1px 0; font-family: Verdana, sans-serif; font-size: 11px; }");
		sb.append("table.ec_external_links { margin-bottom: 10px; }");
		sb.append("table.ec_external_links td { font-weight: bold; padding-right: 5px; }");
		sb.append("table.ec_external_links td a { font-weight: normal; }");
		sb.append("table.ordering { margin-bottom: 10px; }");
		sb.append("table.ordering td.quan { padding-right: 5px; }");
		sb.append("table.ordering td.quan select,");
		sb.append("table.ordering td.quan option { font-family: Verdana, sans-serif; font-size: 11px; font-weight: bold; }");
		sb.append("table.item div.description { border-bottom: 1px solid #cccccc; padding-bottom: 2px; margin-bottom: 5px; }");
		sb.append("</style>");*/

		return null;
	}

	public int getTds() {
		return 0;
	}
}