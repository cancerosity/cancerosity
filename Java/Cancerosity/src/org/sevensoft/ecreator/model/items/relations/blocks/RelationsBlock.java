package org.sevensoft.ecreator.model.items.relations.blocks;

import java.util.List;

import org.sevensoft.ecreator.iface.admin.items.modules.relations.RelationsBlockHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.panels.OrderingForm;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.relations.RelationGroup;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 2 Apr 2007 07:59:55
 *
 */
@Table("blocks_relations")
@HandlerClass(RelationsBlockHandler.class)
@Label("Relations block")
public class RelationsBlock extends Block {

	private RelationGroup	relationGroup;
	private int			limit;
	private Markup		markup;

	public RelationsBlock(RequestContext context) {
		super(context);
	}

	public RelationsBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, objId);

		this.relationGroup = EntityObject.getInstance(context, RelationGroup.class, objId);
		this.limit = 4;
		setName(relationGroup.getName());

		save();
	}

	private List<Item> getItems() {

		Item item = (Item) context.getAttribute("item");

		List<Item> items = relationGroup.getRelations(item);

		if (limit > 0) {
			if (items.size() > limit) {
				items = items.subList(0, limit);
			}
		}

		return items;
	}

	public final int getLimit() {
		return limit;
	}

	public final Markup getMarkup() {
		return markup.pop();
	}

	public final RelationGroup getRelationGroup() {
		return relationGroup.pop();
	}

	@Override
	public Object render(RequestContext context) {

		List<Item> items = getItems();
		if (items.isEmpty()) {
			return null;
		}

		StringBuilder sb = new StringBuilder();

		if (Module.Shopping.enabled(context)) {
			sb.append(new OrderingForm(context));
		}

		MarkupRenderer r = new MarkupRenderer(context, getMarkup());
		r.setBodyObjects(items);
		sb.append(r);

		if (Module.Shopping.enabled(context)) {
			sb.append("</form>");
		}

		return sb.toString();
	}

	public final void setLimit(int limit) {
		this.limit = limit;
	}

	public final void setMarkup(Markup markup) {
		this.markup = markup;
	}

}
