package org.sevensoft.ecreator.model.items.favourites.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.items.favourites.FavouritesHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.favourites.FavouritesGroup;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.db.EntityObject;

/**
 * @author sks 25 Sep 2006 10:47:19
 *
 */
public class RemoveFavouriteMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		String group = params.get("group");
		if (group == null) {
			return null;
		}

        FavouritesGroup favouritesGroup = EntityObject.getInstance(context, FavouritesGroup.class, group);
        if (!favouritesGroup.contains((Item) context.getAttribute("account"), item)) {
            return null;
        }

		Link link = new Link(FavouritesHandler.class, "remove", "item", item, "favouritesGroup", group);
		return super.link(context, params, link, null);
	}

	public Object getRegex() {
		return "remove_favourites";
	}

}
