package org.sevensoft.ecreator.model.items.options.renderers;

import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.util.Keypad;
import org.sevensoft.jeezy.http.util.Renderer;

import java.util.logging.Logger;
import java.util.List;

/**
 * User: Tanya
 * Date: 03.10.2011
 */
public class RevealOptionsSelectionRenderer {

    private static Logger logger = Logger.getLogger("ecreator");

    private ItemOption itemOption; //todo attribute

    private RequestContext context;

    private List<Integer> values;

    private int cols;

    private String parameter;

    public RevealOptionsSelectionRenderer(RequestContext context, ItemOption option, List<Integer> values) {
        this.context = context;
        this.itemOption = option;
        this.values = values;
        this.cols = 3;
        this.parameter = "revealIds";
    }

    @Override
    public String toString() {
        logger.fine("RevealOptionsSelectionRenderer{" +
                "itemOption=" + itemOption +
                ", context=" + context +
                ", values=" + values +
                ", parameter='" + parameter + '\'' +
                '}');


        Keypad keypad = new Keypad(cols);
        List<ItemOption> list = null;
        if (itemOption.getItem() != null) {
            list = itemOption.getItem().getOptionSet().getOptions();

        } else if (itemOption.getItemType() != null) {
            list = itemOption.getItemType().getOptionSet().getOptions();
        }
        if (list == null) {
            return null;
        }
        if (list.contains(itemOption)) {
            list.remove(itemOption);
        }

        keypad.setObjects(list);

        keypad.setRenderer(new Renderer<ItemOption>() {

            public Object render(ItemOption option) {

                boolean checked = false;
                if (values != null) {
                    checked = values.contains(option.getId());
                }

                String param = "revealedOptionCheckTag" + option.getId();

                CheckTag checkTag = new CheckTag(context, parameter, option.getValue(), checked);
                checkTag.setId(param);

                StringBuilder sb = new StringBuilder();
                sb.append(checkTag);
                sb.append(" #");
                sb.append(option.getValue());
                sb.append(" - ");
                sb.append(option.getName());

                return sb.toString();
            }

        });

        return keypad.toString();
    }
}
