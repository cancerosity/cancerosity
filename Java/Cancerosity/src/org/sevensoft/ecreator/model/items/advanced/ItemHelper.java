package org.sevensoft.ecreator.model.items.advanced;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.pricing.Price;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 6 Mar 2007 09:41:14
 *
 */
public class ItemHelper {

	public static void moveSellToCost(RequestContext context) {

		for (Item item : new ItemSearcher(context)) {

			Money money = item.getSellPrice();
			item.setOurCostPrice(money);

		}
	}

    public static void copyRrpToPrice(RequestContext context) {
        for (Item item : new ItemSearcher(context)) {
            if (item.getStandardPrice() == null) {
                Money money = item.getRrp();
                item.setStandardPrice(new Amount(money));
            }
        }
    }

	/**
	 * Wipe all sell prices set directly on items 
	 * @param context 
	 */
	public static void wipeItemSellPrices(RequestContext context) {

		// remove from price tables
		Query q = new Query(context, "delete from # where account=0 and item>0");
		q.setTable(Price.class);
		q.run();

		// update caches
		new Query(context, "update # set genericSellPrice=0").setTable(Item.class).run();

	}
}
