package org.sevensoft.ecreator.model.items.reviews;

import java.util.List;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.attributes.Attributable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.support.AttributeSupport;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 11 Aug 2006 18:54:03
 * 
 * An individual review by a member on this item
 *
 */
@Table("reviews")
public class Review extends AttributeSupport implements Attributable {

	/**
	 * The item this review is for
	 */
	private Item	item;

	/**
	 * The account who did the review
	 */
	private Item	account;

	/**
	 * The date of the review
	 */
	private DateTime	date;

	private boolean	approved;

	/**
	 * The content of the review
	 */
	private String	content;

	private String	author;

	private Review(RequestContext context) {
		super(context);
	}

	public Review(RequestContext context, Item item, Item acc, String content) {
		this(context, item, acc.getName(), content);

		this.account = acc;
		save();
	}

	public Review(RequestContext context, Item item, String author, String content) {
		super(context);

		this.date = new DateTime();
		this.item = item;
		this.author = author;
		this.content = content;

		save();
	}

	/**
	 * 
	 */
	public void approve() {
	}

	public Item getAccount() {
		return account.pop();
	}

	public List<Attribute> getAttributes() {
		return getItem().getItemType().getReviewsModule().getAttributes();
	}

	public final String getAuthor() {
		return author;
	}

	public String getContent() {
		return content;
	}

	public DateTime getDate() {
		return date;
	}

	public Item getItem() {
		return item.pop();
	}

	public final boolean isApproved() {
		return approved;
	}

	public final void setApproved(boolean approved) {
		this.approved = approved;
	}

	public final void setAuthor(String author) {
		this.author = author;
	}

	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * 
	 */
	public void reject() {
	}

}
