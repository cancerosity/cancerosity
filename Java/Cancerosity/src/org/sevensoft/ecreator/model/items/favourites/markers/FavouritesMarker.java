package org.sevensoft.ecreator.model.items.favourites.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.items.favourites.FavouritesHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 03-Apr-2006 14:27:16
 *
 */
public class FavouritesMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		String id = params.get("id");
		if (id == null) {
			id = params.get("group");
		}

		if (id == null) {
			return null;
		}

		return super.link(context, params, new Link(FavouritesHandler.class, null, "favouritesGroup", id), "account_link");

	}

	public Object getRegex() {
		return "favourites";
	}

}
