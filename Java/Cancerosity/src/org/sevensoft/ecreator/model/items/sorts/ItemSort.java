package org.sevensoft.ecreator.model.items.sorts;

import java.util.List;

import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 24 Jul 2006 18:12:04
 *
 */
@Table("items_sorts")
public class ItemSort extends EntityObject implements Positionable, Selectable {

	/**
	 * Returns all sorts across the entire site.
	 */
	public static List<ItemSort> get(RequestContext context) {
		Query q = new Query(context, "select * from # order by position, id");
		q.setTable(ItemSort.class);
		return q.execute(ItemSort.class);
	}

	private Attribute	attribute;

	/**
	 * BC should eb called sortType now
	 */
	private SortType	key;
	private int		position;
	private String	name;
	private ItemType	itemType;
	private boolean	dfault;
	private boolean	reverse;

	@Override
	public synchronized boolean delete() {

		Query q = new Query(context, "update # set itemsort=0 where itemsort=?");
		q.setTable(Category.class);
		q.setParameter(this);
		q.run();

		// remove from saved searches
		q = new Query(context, "update # set sort=0 where sort=?");
		q.setTable(Search.class);
		q.setParameter(this);
		q.run();

		return super.delete();
	}

	public ItemSort(RequestContext context) {
		super(context);
	}

	public ItemSort(RequestContext context, ItemType itemType) {
		super(context);

		this.itemType = itemType;
		this.key = SortType.Name;
		this.name = "new sort";

		save();
	}

	public Attribute getAttribute() {
		return (Attribute) (attribute == null ? null : attribute.pop());
	}

	public ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public String getLabel() {
		return name;
	}

	public String getName() {
		return name;
	}

	public int getPosition() {
		return position;
	}

	public SortType getType() {
		return key == null ? SortType.Name : key;
	}

	public String getValue() {
		return getIdString();
	}

	public boolean hasAttribute() {
		return attribute != null;
	}

	public boolean hasItemType() {
		return itemType != null;
	}

	public boolean isAvailable(String sessionId, String location) {

		switch (getType()) {

		default:
			return true;

		case Distance:
			return location != null;

		}
	}

	public boolean isDefault() {
		return dfault;
	}

	public boolean isReverse() {
		return reverse;
	}

	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

	public void setDefault(boolean dfault) {
		this.dfault = dfault;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setReverse(boolean n) {
		this.reverse = n;
	}

	public void setType(SortType type) {

		if (ObjectUtil.equal(this.key, type)) {
			return;
		}

		this.key = type;
	}
}
