package org.sevensoft.ecreator.model.items.sorts;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.RadioTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

import java.util.Collection;

/**
 * @author sks 09-Feb-2006 17:28:36
 */
public class FrontSortRenderer {

    private final Link link;
    private final Collection<ItemSort> sorts;
    private final ItemSort currentSort;
    private final RequestContext context;
    private final String location;

    public FrontSortRenderer(RequestContext context, Link tag, Collection<ItemSort> sorts, ItemSort currentSort, String location) {

        this.context = context;
        this.link = tag;
        this.sorts = sorts;
        this.currentSort = currentSort;
        this.location = location;
    }

    @Override
    public String toString() {

        if (sorts.size() < 2) {
            return null;
        }

        StringBuilder sb = new StringBuilder();

        sb.append("<style>");
        sb.append("table.sort { width: 100%; font-family: Verdana; color: #333333; font-size: 11px; font-weight: bold; } ");
        sb.append("table.sort td { padding: 6px 0; border-bottom: 1px dotted #999999;}");
        sb.append("</style>");

        sb.append(new TableTag("sort", "center"));
        sb.append("<tr><td>Sort by: ");

        for (ItemSort sort : sorts) {

            if (sort.isAvailable(context.getSessionId(), location)) {

                link.setParameter("sort", sort);
                link.setParameter("page", 1);

                RadioTag radioTag = new RadioTag(context, "sort", sort, sort.equals(currentSort));
                radioTag.setOnClick("window.location='" + link.toString() + "'");

                sb.append(radioTag);
                sb.append(sort.getName());

                sb.append(" ");

            }
        }

        sb.append("</td></tr>");
        sb.append("</table>");

        return sb.toString();
    }
}
