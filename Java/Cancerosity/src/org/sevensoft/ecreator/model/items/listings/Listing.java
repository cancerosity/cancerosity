package org.sevensoft.ecreator.model.items.listings;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.commons.superstrings.RandomHelper;
import org.sevensoft.ecreator.iface.admin.items.listings.ListingModerationHandler;
import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.ecreator.iface.frontend.items.listings.ListingValidationHandler;
import org.sevensoft.ecreator.iface.frontend.items.listings.EditListingHandler;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.vouchers.Voucher;
import org.sevensoft.ecreator.model.ecom.vouchers.Vouchable;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.accounts.registration.EmailTagParser;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 27 Dec 2006 18:16:07
 */
@Table("listings")
public class Listing extends EntityObject implements Logging, Vouchable {

    /**
     *
     */
    public static Listing get(RequestContext context, Item item) {
        Listing listing = SimpleQuery.get(context, Listing.class, "item", item);
        return listing == null ? new Listing(context, item) : listing;
    }

    private Item item;

    /**
     * This is the expiry date of member listings
     */
    @Index()
    private Date expiryDate;

    private boolean introductoryListingOffered;

    /**
     * Flag to true when setting to lifetime listing
     */
    private boolean lifetimeListing;

    /**
     * The listing package used to list this item
     */
    private ListingPackage listingPackage;

    /**
     * Code used to check validation
     */
    private String validationCode;

    /**
     * voucher applied during listing setup
     */
    private Voucher voucher;

    protected Listing(RequestContext context) {
        super(context);
    }

    public Listing(RequestContext context, Item item) {
        super(context);

        this.item = item;
        this.validationCode = RandomHelper.getRandomString(6);

        save();
    }

    public void approved() throws EmailAddressException, SmtpServerException {

        item.setAwaitingModeration(false);
        item.save();

        if (item.isDisabled()) {
            if (expiryDate == null || expiryDate.isFuture()) {
                item.setStatus("Live");
            }
        }

        Listing listing = item.getListing();
        listing.emailApproved();
    }

    /**
     * Send email to admins with notification of the listing
     */
    public void emailAdmin() throws SmtpServerException {

        ListingSettings ls = ListingSettings.getInstance(context);
        Company company = Company.getInstance(context);
        Config config = Config.getInstance(context);

        if (ls.hasNotificationEmails()) {

            logger.fine("[Item] emailListingNotification=" + ls.getNotificationEmails());

            for (String email : ls.getNotificationEmails()) {

                StringBuilder sb = new StringBuilder();
                sb.append("A listing called '" + item.getName() + "' has been added or re-listed.\n");
                sb.append("You can edit the listing here:\n");
                sb.append(config.getUrl() + "/" + new Link(EditListingHandler.class, "main", "item", this));

                if (item.isAwaitingModeration()) {
                    sb.append("\n\nThis item is awaiting moderation, you can view the moderation queue here:\n");
                    sb.append(config.getUrl() + "/" + new Link(ListingModerationHandler.class));
                }

                Email e = new Email();
                e.setSubject("New listing on " + company.getName());
                e.setBody(sb);
                e.setFrom(config.getServerEmail());
                e.setTo(email);
                try {
                    new EmailDecorator(context, e).send(config.getSmtpHostname());
                } catch (EmailAddressException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public void emailApproved() throws EmailAddressException, SmtpServerException {

        Config config = Config.getInstance(context);
        Company company = Company.getInstance(context);

        Email e = new Email();

        String body = item.emailTagsParse(item.getItemType().getAcceptListingEmailContent());
        e.setBody(body);

//        StringBuilder sb = new StringBuilder();
//        sb.append("Hello,\n\nGood news! Your posted " + item.getItemTypeNameLower()
//                + " has been approved for inclusion on our site.\n\nYou can view it by clicking this link:\n");
//        sb.append(item.getUrl());
//        sb.append("\n\n\n" + company.getName());

//        e.setBody(sb);

        e.setSubject(item.getItemTypeNameLower() + " approved");
        if (getItem().getItemType().isAccount(context)) {
            e.setTo(getItem().getEmail());
        } else {
            e.setTo(getItem().getAccount().getEmail());
        }
        e.setFrom(config.getServerEmail());
        new EmailDecorator(context, e).send(config.getSmtpHostname());
    }

    /**
     * Send an email to the account holder of this listing with a link to edit it
     * using the special auto login page
     */
    public void emailDetails() throws EmailAddressException, SmtpServerException {

        Config config = Config.getInstance(context);

        Item account = getItem().getAccount();

        // generate new password to edit this listing for the account
        String password = account.setPassword();

        Email e = new Email();
        e.setTo(account.getEmail());

        StringBuilder sb = new StringBuilder();

        sb.append("Hello,\n\nTo modify or delete your listing, click this link:\n");
        sb.append(Config.getInstance(context).getUrl() + "/" + new Link(ItemHandler.class, null, "item", item, "p", password));

        e.setFrom(config.getServerEmail());
        e.setSubject("Link to edit your listing");
        e.setBody(sb);

        logger.fine("[Listing] listing access email=" + e);
        new EmailDecorator(context, e).send(config.getSmtpHostname());
    }

    public void emailRejected() throws EmailAddressException, SmtpServerException {

        Config config = Config.getInstance(context);
        Company company = Company.getInstance(context);

        Email e = new Email();

        String body = item.emailTagsParse(item.getItemType().getRejectListingEmailContent());
        e.setBody(body);

//        StringBuilder sb = new StringBuilder();
//        sb.append("Hello,\n\nUnfortunately your recent posting '" + item.getName() + "' has been rejected by our admin team.\nPlease try adding another.");
//        sb.append("\n\n\n" + company.getName());

//        e.setBody(sb);

        e.setSubject(item.getItemTypeNameLower() + " rejected");
        if (getItem().getItemType().isAccount(context)) {
            e.setTo(getItem().getEmail());
        } else {
            e.setTo(getItem().getAccount().getEmail());
        }
        e.setFrom(config.getServerEmail());
        new EmailDecorator(context, e).send(config.getSmtpHostname());
    }

    public void emailValidation() throws EmailAddressException, SmtpServerException {

        Config config = Config.getInstance(context);
        Company company = Company.getInstance(context);

        Email e = new Email();

        StringBuilder sb = new StringBuilder();
        sb
                .append("Hello,\n\nThank you for adding a listing to our site.\n\nBefore it becomes visible to other users you must confirm your email address by clicking the link below:\n");
        sb.append(config.getUrl() + "/" + new Link(ListingValidationHandler.class, null, "item", item, "code", validationCode));
        sb.append("\n\n\n" + company.getName());

        e.setBody(sb);

        e.setSubject("Listing email verification");
        e.setTo(getItem().getAccount().getEmail());
        e.setFrom(config.getServerEmail());
        new EmailDecorator(context, e).send(config.getSmtpHostname());
    }

    public void sendEmailExpiryReminder() throws EmailAddressException, SmtpServerException {

        Config config = Config.getInstance(context);

        Email e = new Email();

        e.setBody(EmailTagParser.parse(context, getItem(), ListingSettings.getInstance(context).getExpiryReminderBody(), null), "text/html");

        e.setSubject("Listing expiry reminder");
        Item account = getItem().getAccount();
        if (account == null) {
            return;
        }
        e.setTo(account.getEmail());

        e.setFrom(config.getServerEmail());
        new EmailDecorator(context, e).send(config.getSmtpHostname());

        log("Listing expiry reminder. Notification was sent to "+ account.getEmail());
    }

    /**
     * Returns the expiry date for this listing.
     */
    public Date getExpiryDate() {
        return expiryDate;
    }

    /**
     * Number of days before this listing will expire
     */
    public int getExpiryDays() {

        if (expiryDate == null || expiryDate.isPast())
            return 0;

        return expiryDate.getDaysBetween(new Date());
    }

    public String getExpiryStatus() {

        if (item.isDisabled()) {
            return null;
        }

        if (expiryDate == null)
            return "Lifetime listing";

        /*
           * If expiry date is past then show when it expired
           */
        if (expiryDate.isPast())
            return "Expired on " + expiryDate.toString("dd/MM/yyyy");

        /*
           * ...otherwise we have a listing that will expire in the future.
           */
        return "Listed until " + expiryDate.toString("dd/MM/yyyy");

    }

    public final Item getItem() {
        return item.pop();
    }

    public ListingPackage getListingPackage() {
        return (ListingPackage) (listingPackage == null ? null : listingPackage.pop());
    }

    /**
     * Returns a status appropriate for listing details, ie, more informative to the customer.
     */
    public String getListingStatus() {

        if (item.isAwaitingModeration()) {
            return "Awaiting moderation";
        }

        if (item.isAwaitingValidation()) {
            return "Awaiting validation";
        }

        /*
           * If the listing has an expiry date in the past then it has expired
           */
        if (hasExpired()) {
            return "Expired on " + expiryDate.toString("dd-MMM-yyyy");
        }

        if (isExpiring()) {
            return "Listed until " + expiryDate.toString("dd-MMM-yyyy");
        }

        if (item.isDisabled()) {
            return "Not listed yet";
        }

        return "Lifetime listing";
    }

    public String getStatusLong() {

        if (item.isAwaitingValidation()) {
            return "This " + item.getItemTypeNameLower() + " is awaiting validation of your email address. "
                    + "It will appear on the site once you click the link in the email you were sent after you completed the listing.";
        }

        if (item.isAwaitingModeration()) {
            return "This " + item.getItemTypeNameLower()
                    + " is awaiting moderation. It will appear on the site once it has been approved by a member of the admin team.";
        }

        /*
           * If the listing has an expiry date in the past then it has expired
           */
        if (hasExpired()) {
            return "This " + item.getItemTypeNameLower() + " expired on " + expiryDate.toString("dd-MMM-yyyy") + ".";
        }

        if (isExpiring()) {
            return "This " + item.getItemTypeNameLower() + " will be listed until " + expiryDate.toString("dd-MMM-yyyy") + ".";
        }

        if (item.isDisabled()) {
            return "This " + item.getItemTypeNameLower() + " is disabled.";
        }

        return "This " + item.getItemTypeNameLower() + " is live.";
    }

    public final String getValidationCode() {
        return validationCode;
    }

    public Voucher getVoucher() {
        return voucher == null ? null : (Voucher) voucher.pop();
    }

    public boolean hasVoucher() {
        return getVoucher() != null;
    }

    /**
     * Returns true if this listing has already expired.
     */
    public boolean hasExpired() {
        return expiryDate != null && expiryDate.isPast();
    }

    /**
     * Returns true if this listing has an expiry date
     */
    public boolean hasExpiryDate() {
        return expiryDate != null;
    }

    public boolean hasListingPackage() {
        return listingPackage != null;
    }

    /**
     * Returns true if this listing will exipre in the future.
     */
    public boolean isExpiring() {

        /*
           * If no member set then it is internal so will never expire
           */
        if (!item.hasAccount()) {
            return false;
        }

        /*
           * If no expiry date set then it will not expire
           */
        if (expiryDate == null)
            return false;

        /*
           * Otherwise return if the expiry date is yet to come, ie future or today.
           *
           */
        return !expiryDate.isPast();
    }

    public boolean isLifetimeListing() {
        return lifetimeListing;
    }

    /**
     * This item is being listed for the duration  of the listing rate.
     * Payment contains details of the transaction.
     */
    public void list(ListingRate rate, Payment payment, String ipAddress) {

        /*
           * Ensure payment amount is correct if the listing package has a fee.
           */
        if (rate != null && rate.hasFee()) {

            if (payment == null) {
                logger.fine("[Item] no payment parameter for listing with rate with fee");
                return;
            }

            Money fee = rate.getFeeEx();
            Money paid = payment.getAmount();

            if (!fee.equals(paid)) {

                logger.warning("[Item] listing payment does not tally with amount set on rate");
                return;
            }

            /*
                * if a payment was made then create an order
                */
            Order order = item.addOrder(null, ipAddress);
            order.addListingPackage(rate, item);
            order.addPayment(payment);

            logger.fine("[Item] order created for payment: " + order);

        }

        /*
           * Set introduction to true so that this item cannot use it again
           */
        introductoryListingOffered = true;

        /*
           * if period is zero then we have a lifetime listing
           * Otherwise extend expiry date.
           *
           */
        if (rate == null || rate.getPeriod() == 0) {

            expiryDate = null;
            lifetimeListing = true;

            logger.fine("[Item] setting lifetime listing");

        } else {

            /*
                * If no expiry date set then start from tomorrow
                */
            boolean expiryTomorrow = false;
            if (expiryDate == null) {
                expiryDate = new Date().addDays(1);
                expiryTomorrow = true;
            }

            if (expiryTomorrow && rate.getPeriod() != 0) {
                expiryDate = expiryDate.addDays(rate.getPeriod() - 1);
            } else if (!expiryTomorrow) {
                expiryDate = expiryDate.addDays(rate.getPeriod());
            }
            lifetimeListing = false;
            logger.fine("[Item] setting expiry date=" + expiryDate);
        }

        save();

        if (!item.getStatus().equalsIgnoreCase("Live")) {

            logger.fine("[Item] setting listing status to live, current status=" + item.getStatus());
            item.setStatus("Live");
        }

        /*
           * Send notifications to anyone in the notify list.
           */

        try {
            emailAdmin();
        } catch (SmtpServerException e) {
            e.printStackTrace();
            logger.warning(e.toString());
        }

    }

    public void rejected() throws EmailAddressException, SmtpServerException {

        Listing listing = item.getListing();
        listing.emailRejected();

        item.setAwaitingModeration(false);

        item.delete();
    }

    public void setExpiryDate(Date d) {
        this.expiryDate = d;
        this.lifetimeListing = false;
    }

    public void setLifetimeListing() {
        this.expiryDate = null;
        this.lifetimeListing = true;
    }

    public final void setLifetimeListing(boolean lifetimeListing) {
        this.lifetimeListing = lifetimeListing;
    }

    public void setListingPackage(ListingPackage lp) {
        this.listingPackage = lp;
    }

    public void setVoucher(Voucher voucher) {
        this.voucher = voucher;
        save("voucher");
    }

    public boolean validate(String code) {

        if (validationCode.equals(code)) {

            getItem().setAwaitingValidation(false);
            getItem().save();

            return true;

        } else {

            return false;
        }
    }

    public String getLogName() {
        return getItem().getName() + " #" + getIdString();
    }

    public void log(String message) {
        new LogEntry(context, this, message);
    }

    public void log(User user, String message) {
        new LogEntry(context, this, user, message);
    }

    /**
     * not supported by listings packages vouchers
     * @return
     */
    public Money getVoucherTotal() {
        return new Money(0);
    }

    public Item getCustomer() {
        return getItem().getAccount();
    }

    /**
     * not supported by listings packages vouchers
     * @return
     */
    public int getVoucherQty() {
        return 1;
    }
}