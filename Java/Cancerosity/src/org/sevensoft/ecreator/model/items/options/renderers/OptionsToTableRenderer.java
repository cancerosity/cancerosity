package org.sevensoft.ecreator.model.items.options.renderers;

import org.sevensoft.ecreator.iface.EcreatorRenderer;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.table.TableTag;

import java.util.List;

/**
 * User: Dmitry Lebedev
 * Date: 19.06.2009
 * Time: 11:48:40
 */
public class OptionsToTableRenderer extends EcreatorRenderer {

    private final Item item;
    private final List<ItemOption> options;

    public OptionsToTableRenderer(RequestContext context, Item item, List<ItemOption> options) {
        super(context);
        this.item = item;
        this.options = options;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(new TableTag("ec_options", 0, 0));
            for (ItemOption option : options) {
                sb.append("<tr>");
                sb.append("<td class='ec_option_label' valign='top'><b>" + option.getName() + ":</b></td>");
                sb.append("<td class='ec_option_value'>");
                for (ItemOptionSelection selection : option.getSelections()) {
                    sb.append("<span class='option_value'>" + selection.getLabel() + "</span>, ");
                }
                sb.append("</td>");
                sb.append("</tr>");
            }
        sb.append("</table>");
        return sb.toString();
    }
}
