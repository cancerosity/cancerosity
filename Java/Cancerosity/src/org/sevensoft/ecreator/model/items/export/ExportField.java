// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   ExportField.java

package org.sevensoft.ecreator.model.items.export;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;

@Table("exporter_fields")
public class ExportField extends EntityObject implements Positionable {


    private Exporter exporter;
    private ExportFieldType fieldType;
    private int position;
    private Attribute attribute;
    private boolean fullImageUrl;

    public ExportField(RequestContext context) {
        super(context);
    }

    public ExportField(RequestContext context, Exporter exporter) {
        super(context);
        this.exporter = exporter;
    }

    public String export(Item item) {
        if (fieldType == null)
            return null;

        switch (fieldType) {
            default:
                throw new RuntimeException("Field error");

            case Attribute:
                return item.getAttributeValue(attribute);

            case Content:
                return item.getContent();

            case CostPrice:
                Money costPrice = item.getOurCostPrice();
                if (costPrice == null)
                    return null;
                else
                    return costPrice.toEditString();

            case DescriptionTag:
                return item.getDescriptionTag();

            case Image:
                Img img = item.getApprovedImage();
                if (img == null)
                    return null;
               else {
                    if (isFullImageUrl()) {
                        return img.getFullImagePath();
                    } else {
                        return img.getFilename();
                    }
                }

            case Keywords:
                return item.getKeywords();

            case SellingPrice:
                Money sellPrice = item.getSellPrice();
                if (sellPrice == null)
                    return null;
                else
                    return sellPrice.toEditString();

            case Name:
                return item.getName();

            case Reference:
                return item.getReference();

            case RRP:
                Money rrp = item.getRrp();
                if (rrp == null)
                    return null;
                else
                    return rrp.toEditString();

            case TitleTag:
                return item.getTitleTag();

            case ItemUrl:
                return item.getUrl();
        }
    }

    public Attribute getAttribute() {
        return (Attribute) (attribute != null ? attribute.pop() : null);
    }

    public Exporter getExporter() {
        return (Exporter) exporter.pop();
    }

    public ExportFieldType getFieldType() {
        return fieldType;
    }

    public int getPosition() {
        return position;
    }

    public boolean isFullImageUrl() {
        return fullImageUrl;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    public void setFieldType(ExportFieldType exportFieldType) {
        fieldType = exportFieldType;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setFullImageUrl(boolean fullImageUrl) {
        this.fullImageUrl = fullImageUrl;
    }

    public String getName() {
        if (fieldType == null)
            return null;
        if (fieldType == ExportFieldType.Attribute) {
            return attribute == null ? "Attribute: not defined" : attribute.getName();
        }

        return fieldType.toString();
    }
}
