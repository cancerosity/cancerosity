package org.sevensoft.ecreator.model.items.listings.emails;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 16-Nov-2005 01:04:27
 * 
 */
public class ListingExpiredEmail extends Email {

	public ListingExpiredEmail(Item item, RequestContext context) {

		setSubject("Expiry warning: " + item.getName());
		setFrom(Company.getInstance(context).getName(), Config.getInstance(context).getServerEmail());
		setBody("Your listing has expired bud");
		setTo("sks@7soft.co.uk");
	}

}
