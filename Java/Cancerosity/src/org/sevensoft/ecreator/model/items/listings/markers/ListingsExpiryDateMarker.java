package org.sevensoft.ecreator.model.items.listings.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * User: Tanya
 * Date: 04.02.2011
 */
public class ListingsExpiryDateMarker extends MarkerHelper implements IItemMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

        if (!Module.Listings.enabled(context)) {
            return null;
        }

        if (!ItemModule.Listings.enabled(context, item)) {
            return null;
        }
        String format = params.get("format");
        if (format == null) {
//            format = " EEEE dd MMMM yyyy HH:mm ";
            format = "EEEEE, dd MMMMM yyyy hh:mma";
        }

        if (item.getListing() != null && item.getListing().getExpiryDate() != null) {
            return super.string(context, params, item.getListing().getExpiryDate().toString(format));
        }
        return null;
    }


    public Object getRegex() {
        return "listing_expiry";
    }


}
