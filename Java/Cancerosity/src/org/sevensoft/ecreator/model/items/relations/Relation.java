package org.sevensoft.ecreator.model.items.relations;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Dec 2006 14:54:14
 *
 */
@Table("relations")
public class Relation extends EntityObject {

	private Item		item;
	private Item		relation;
	private RelationGroup	relationGroup;

	protected Relation(RequestContext context) {
		super(context);
	}

	Relation(RequestContext context, RelationGroup g, Item item, Item r) {
		super(context);

		this.relationGroup = g;
		this.item = item;
		this.relation = r;

		save();
	}
}
