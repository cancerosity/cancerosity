package org.sevensoft.ecreator.model.items;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.samdate.QuickDate;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.frontend.search.ItemSearchHandler;
import org.sevensoft.ecreator.model.accounts.subscriptions.Subscription;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeValue;
import org.sevensoft.ecreator.model.attributes.SearchControl;
import org.sevensoft.ecreator.model.bookings.BookingSlot;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategoryItem;
import org.sevensoft.ecreator.model.categories.db.CategoryAncestor;
import org.sevensoft.ecreator.model.interaction.Interaction;
import org.sevensoft.ecreator.model.interaction.LastActive;
import org.sevensoft.ecreator.model.items.highlighted.HighlightMethod;
import org.sevensoft.ecreator.model.items.sorts.ItemSort;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.ecreator.model.media.images.ImageSearchType;
import org.sevensoft.ecreator.model.search.Search.FeaturedMethod;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.db.StringQueryBuilder;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

import java.text.ParseException;
import java.util.*;
import java.util.logging.Logger;

/**
 * @author sks 12-Nov-2005 11:06:27
 *         <p/>
 *         This class represents a search on the system.
 *         It can be persisted or created as a temporary search.
 *         <p/>
 *         They are usually tagged to a member as personal saved searches but can be tagged to a category (like old style containers)
 */
public class ItemSearcher implements Iterable<Item> {

    private static Logger logger = Logger.getLogger("ecreator");

    private String email;

    private String startsWith;

    private MultiValueMap<Attribute, String> attributeValues;

    private int distance;

    /**
     *
     */
    private boolean noCategory;

    private boolean noImages;

    /**
     * Only items created on the system after this date.
     * Use this field to search for say newly added listings which do not have a date themselves
     */
    private Date createdAfter;
    private int start;

    /**
     *
     */
    private String name;

    /**
     * Only items with an image
     */
    private boolean imageOnly;

    /**
     * search any attribute, content and name
     */
    private String details;

    private transient Item account;

    /**
     *
     */
    private boolean excludeAccountItemType;

    /**
     *
     */
    private boolean excludeAccount;

    private Attribute sortAttribute;

    private boolean sortReverse;

    private Category searchCategory;

    private String keywords;

    private int y;

    private int x;

    private String location;

    private int minStock;

    /**
     * Item type to search for
     */
    private ItemType itemType;

    /**
     * Max items to display
     */
    private int limit;

    private LastActive lastActive;

    /**
     * Show members with this subscription only
     */
    private SubscriptionLevel subscriptionLevel;

    /**
     *
     */
    private boolean subcats;

    private int age;

    private ItemSort sort;

    private Money sellPriceTo;

    private Money sellPriceFrom;

    private SortType sortType;

    private String status;

    private final RequestContext context;

    private Date joinedAfter;

    /**
     * Id of the product to search for
     */
    private String id;

    private String displayName;

    private boolean visibleOnly;

    private int seed;

    private boolean inStockOnly;

    /**
     * Only include items / members that have recordings
     */
    private boolean recordingsOnly;

    private boolean prioritised;

    private Set<Integer> itemTypes;

    private boolean includeHidden;

    private boolean awaitingModeration;

    private boolean hasEmail;

    private String reference;

    private ItemModule itemModule;

    private Date bookingEnd;

    private Date bookingStart;

    private int feedCountBelow;

    private String feedSrc;

    private FeaturedMethod featuredMethod;

    private boolean searchableOnly;

    private int itemTypeId;

    private Map<Attribute, SearchControl> searchControls;

    private Item searchAccount;

    private List<Integer> searchAccounts;

    private boolean excludeFromGoogleFeed;

    private String groupedKeywords;

    private boolean calendar;

    private List<Integer> excludeItemsIds;

    /**
     * Create a new, non persisted search
     */
    public ItemSearcher(RequestContext context) {

        this.context = context;
        this.visibleOnly = true;
        this.includeHidden = false;
        this.searchableOnly = true;
    }

    /**
     * Returns true if this searcher would return this item if it was asked to search
     */
    public boolean contains(Item item) {
        Integer integer = (Integer) execute(0, 1, true, item.getId());
        return integer.intValue() == 1;
    }

    private Object execute(int start, int limit, boolean count, int exactId) {

        // I want to limit searching to a max of 100 results to keep database fast
        // we should never need more than 100 results per set.
//        if (limit < 1 || limit > 1000) {
//            limit = 200;
//        }

        StringQueryBuilder b = new StringQueryBuilder(context);

        if (count) {

            b.append("select count(*) from ( ");

        }

        b.append("select distinct i.* ");

        if (location != null) {

            b.append(", pow(pow(i.x - " + x + ", 2) + pow(i.y- " + y + ", 2), 0.5) / 1000 / 1.6093 dist ");

        } else {

            b.append(", 0 dist ");
        }

        b.append(" from # i join # it on i.itemType=it.id ");
        b.addTable(Item.class);
        b.addTable(ItemType.class);

        // if interactions join on interaction
        if (recordingsOnly) {

            b.append(" join # interaction on i.id=interaction.account ");
            b.addTable(Interaction.class);
        }

        logger.fine("[ItemSearcher] attributeValues= " + attributeValues);

        if (attributeValues != null && !attributeValues.isEmpty()) {

            if (calendar) {
                b.append("join (select itm.* FROM # itm join # av1 on itm.id=av1.item \n" +
                        "join # av2 on itm.id=av2.item WHERE (");
                b.addTable(Item.class);
                b.addTable(AttributeValue.class);
                b.addTable(AttributeValue.class);
            } /*else {
                b.append(" join (select item, count(*) c from # av WHERE ");
                b.addTable(AttributeValue.class);
            }*/

            Iterator<Attribute> attributeValuesIterator = attributeValues.keySet().iterator();
            while (attributeValuesIterator.hasNext()) {

                // new
                if (!calendar) {
                    b.append(" join (select item from # av WHERE ");
                    b.addTable(AttributeValue.class);
                }
                //  end new
                Attribute attribute = attributeValuesIterator.next();
                List<String> values = attributeValues.list(attribute);

                logger.fine("[ItemSearcher] attribute: " + attribute.getType() + " values=" + values);

                switch (attribute.getType()) {

                    case Numerical:

                        SearchControl searchControl = attribute.getSearchControl();
                        if (searchControl == null) {

                            if (values.size() == 2) {

                                String min = values.get(0);
                                String max = values.get(1);

                                logger.fine("[ItemSearcher] numerical attribute search between bounds " + min + " to " + max);

                                if (min.equals("*") && max.equals("*")) {

                                    logger.fine("[ItemSearcher] numerical attributes are both wildcard, so searching for all values");
                                    b.append(" (av.attribute=? and av.value is not null) ", attribute);

                                } else if (min.equals("*")) {

                                    try {
                                        logger.fine("[ItemSearcher] min is wildcard, so searching with max bound only");
                                        if (max.indexOf(".") == -1) {
                                            int maxInt = Integer.parseInt(max);
                                            b.append(" (av.attribute=? and av.value<=" + maxInt + ") ", attribute);
                                        } else {
                                            double maxDouble = Double.parseDouble(max);
                                            b.append(" (av.attribute=? and av.value<=" + maxDouble + ") ", attribute);
                                        }

                                    } catch (NumberFormatException e) {
                                        logger.warning("[" + ItemSearcher.class + "] incorrect numer format of parameter min="
                                                + max + "  " + e);
                                        b.append(" (1=1) ");
                                    }

                                } else if (max.equals("*")) {

                                    try {

                                        logger.fine("[ItemSearcher] max is wildcard, so searching with min bound only");
                                        if (min.indexOf(".") == -1) {
                                            int minInt = Integer.parseInt(min);
                                            b.append(" (av.attribute=? and av.value>=" + minInt + ") ", attribute);

                                        } else {
                                            double minDouble = Double.parseDouble(min);
                                            b.append(" (av.attribute=? and av.value>=" + minDouble + ") ", attribute);
                                        }

                                    } catch (NumberFormatException e) {
                                        logger.warning("[" + ItemSearcher.class + "] incorrect numer format of parameter min="
                                                + min + "  " + e);
                                        b.append(" (1=1) ");
                                    }

                                } else {

                                    // I have noticed the re-ordering of numerical bounds.
                                    // Lets ensure they are always in meaningful low to high order
                                    try {
                                        if (max.indexOf(".") != -1 || min.indexOf(".") != -1) {
                                            double maxDouble = Double.parseDouble(max);
                                            double minDouble = Double.parseDouble(min);

                                            if (minDouble > maxDouble) {

                                                double t = minDouble;
                                                minDouble = maxDouble;
                                                maxDouble = t;
                                            }
                                        } else {
                                            int maxInt = Integer.parseInt(max);
                                            int minInt = Integer.parseInt(min);

                                            if (minInt > maxInt) {

                                                int t = minInt;
                                                minInt = maxInt;
                                                maxInt = t;
                                            }
                                        }

                                        logger.fine("[ItemSearcher] searching for max and min bounds");
                                        b.append(" (av.attribute=? and av.value>=" + min + " and av.value<=" + max + " ) ", attribute);

                                    } catch (NumberFormatException e) {
                                        logger.warning("[" + ItemSearcher.class + "] incorrect numer format of parameter min="
                                                + min + " or max=" + max + "  " + e);
                                        b.append(" (1=1) ");
                                    }

                                }

                            } else if (values.size() == 1) {

                                try {

                                    // ensure value is numeric
                                    int i = Integer.parseInt(values.get(0));
                                    b.append(" (av.attribute=? and av.value=" + i + ") ", attribute);

                                } catch (NumberFormatException e) {
                                    // e.printStackTrace();
                                    b.append(" (1=1) ");
                                }
                            }

                        } else {

                            try {

                                // ensure value is numeric
                                int intValue = Integer.parseInt(values.get(0));

                                switch (attribute.getSearchControl()) {

                                    default:
                                    case Min:
                                        b.append(" (av.attribute=? and av.value>=" + intValue + ") ", attribute);
                                        break;

                                    case Max:
                                        b.append(" (av.attribute=? and av.value<=" + intValue + ") ", attribute);
                                        break;

                                }

                            } catch (NumberFormatException e) {
                                // e.printStackTrace();
                                b.append(" (1=1) ");
                            }

                        }

                        break;

                        /*
                       * If a single value then search for that DAY, if 2 values then search in that date range
                       */
                    case Date:
                    case DateTime:

                        try {

                            if (values.size() == 2) {

                                String v1 = values.get(0);
                                String v2 = values.get(1);

                                if ("*".equals(v1) && "*".equals(v2)) {

                                    logger.fine("[ItemSearcher] dates are both wildcard, so searching for all values");
                                    b.append(" (av.attribute=? and av.value is not null) ", attribute);

                                } else if ("*".equals(v1)) {

                                    // before date
                                    logger.fine("[ItemSearcher] from is wildcard, so searching before date");

                                    DateTime before = new DateTime(v2);
                                    b.append(" (av.attribute=? and av.value<=" + before.getTimestampString() + ") ", attribute);

                                } else if ("*".equals(v2)) {

                                    // after date
                                    logger.fine("[ItemSearcher] to is wildcard, so searching after date");

                                    DateTime before = new DateTime(v1);
                                    b.append(" (av.attribute=? and av.value>=" + before.getTimestampString() + ") ", attribute);

                                } else {

                                    // between dates
                                    logger.fine("[ItemSearcher] searching between dates");

                                    DateTime from = new DateTime(v1);
                                    DateTime to = new DateTime(v2);

                                    b
                                            .append(" (av.attribute=? and av.value>=? and av.value<?) ", attribute, from.getTimestamp(), to
                                                    .getTimestamp());
                                }

                            } else if (values.size() == 1) {

                                String v1 = values.get(0);
                                if ("*".equals(v1)) {

                                    logger.fine("[ItemSearcher] date is wildcard so searching for all dates");
                                    b.append(" (av.attribute=? and av.value is not null) ", attribute);

                                } else {

                                    try {

                                        // lets see if the value is a quick date
                                        QuickDate quickDate = QuickDate.valueOf(v1);

                                        Date[] dates = quickDate.getDates();
                                        Date from = dates[0];
                                        Date to = dates[1];

                                        // INCLUSIVE VALUES
                                        b.append(" (av.attribute=? and av.value>=? and av.value<=?) ", attribute, from.getTimestamp(), to
                                                .getTimestamp());

                                    } catch (RuntimeException e) {

                                        Date date = new Date(v1);
                                        if (!calendar) {
                                            if (attribute.getName().equalsIgnoreCase("date start")) {
                                                b.append(" (av.attribute=? and av.value>=?) ", attribute, date.getTimestamp());
                                            } else if (attribute.getName().equalsIgnoreCase("date end")) {
                                                b.append(" (av.attribute=? and av.value<=?) ", attribute, date.getTimestamp());
                                            } else {
                                                b.append(" (av.attribute=? and av.value>=? and av.value<?) ", attribute, date.getTimestamp(), date
                                                        .nextDay().getTimestamp());
                                            }
                                        } else {
                                            if (attribute.getName().equalsIgnoreCase("Date start") || attribute.isDateStart()) {
                                                b.append(" av1.attribute=? and av1.value<? ", attribute, date.nextDay().getTimestamp());
                                            }
                                            if (attribute.getName().equalsIgnoreCase("Date end") || attribute.isDateEnd()) {
                                                b.append(" av2.attribute=? and av2.value>=? ", attribute, date.getTimestamp());
                                            }

                                            if (attributeValuesIterator.hasNext()) {
                                                b.append(" and ");
                                            }
                                        }
                                    }

                                }

                            }

                        } catch (ParseException e1) {
                            // e1.printStackTrace();
                        }

                        break;

                    default:
                    case Text:
                    case Selection:
                    case Postcode:

                        b.append(" (av.attribute=? and (", attribute);

                        Iterator<String> valuesIterator = values.iterator();
                        while (valuesIterator.hasNext()) {

                            String value = valuesIterator.next();

                            // selections should always search exactly so if this isn't selection then wildcard
                            if (!attribute.isSelection()) {

                                // only wildcard if we don't wrap in "" as that will be used for an exact search
                                if (value.startsWith("\"") && value.endsWith("\"")) {
                                    value = value.substring(1, value.length() - 1);
                                } else {
                                    value = "%" + value + "%";
                                }

                                logger.fine("[ItemSearcher] attribute not selection, wildcarding value=" + value);
                            }

                            b.append("av.value like ?", value);

                            if (valuesIterator.hasNext()) {
                                if (attribute.isSelection()) {
                                    b.append(" or ");
                                } else {
                                    b.append(" and ");
                                }
                            }
                        }
                        b.append(") and av.item!=0 )");
//                        b.append("))");
                        break;
                }
                //new
                if (!calendar) {
                    b.append(" ) Z" + attribute.getId() + " on i.id=Z" + attribute.getId() + ".item ");
//                b.addParameter(attributeValues.size());

//                if (attributeValuesIterator.hasNext() && !calendar) {
//                    b.append(" or ");
//                }
                }
               //end new

            }

            if (calendar) {
                b.append(" )) Z on i.id=Z.id ");
            } /*else {
                b.append(" group by item having c >= ?) Z on i.id=Z.item ");
                b.addParameter(attributeValues.size());
            }*/
        }

        if (sortType == SortType.Attribute && sortAttribute != null) {
            b.append(" join # av on i.id=av.item ");
            b.addTable(AttributeValue.class);
        }

        if (searchCategory != null) {

            if (!subcats || searchCategory.isChild()) {

            }

            // dont do a subcat search if root and subcats is set
            if (searchCategory.isRoot() && subcats) {

            } else {

                b.append(" join # ci on i.id=ci.item ");
                b.addTable(CategoryItem.class);

            }
        }

        if (details != null /*&& attributeValues != null && !attributeValues.isEmpty()*/) {
            b.append(" left outer join # avd on i.id=avd.item ");
            b.addTable(AttributeValue.class);
        }

        if (!count && sortType == SortType.SubscriptionLevel) {
            b.append(" join # sub on i.id=sub.account ");
            b.addTable(Subscription.class);
        }

        if (bookingStart != null && bookingEnd != null) {
//            b.append(" join (select item, count(*) c from # slot "
//                    + "where date>=? and date<? and bookings=0 group by item having c=?) B on i.id=B.item ");
            b.append(" join (select item, count(*) c from # slot "
                    + "where date>=? and date<? and status=? group by item having c=?) B on i.id=B.item ");
            b.addTable(BookingSlot.class);
            b.addParameter(bookingStart);
            b.addParameter(bookingEnd);
            b.addParameter(BookingSlot.Status.Open);
            b.addParameter(bookingStart.getDaysBetween(bookingEnd));
        }

        // END JOINS

        // START CLAUSES
        b.append(" WHERE 1=1 ");

        if (excludeAccount) {
            logger.fine("[ItemSearcher] excludeAccount=" + excludeAccount + ", account=" + account);
            if (account != null) {
                b.append(" and i.id !=? ", account.getId());
            }
        }

        if (excludeAccountItemType) {
            if (account != null) {
                b.append(" and i.itemType!=? ", account.getItemType().getId());
            }
        }

        if (itemType != null) {

            b.append(" and i.itemType=? ", itemType);

        } else if (itemTypeId > 0) {

            b.append(" and i.itemType=? ", itemTypeId);

        } else if (itemTypes != null) {

            // make a copy of item types
            List<Integer> itemTypesCopy = new ArrayList(itemTypes);
            if (excludeAccountItemType && account != null) {
                itemTypesCopy.remove(new Integer(account.getItemType().getId()));
            }

            if (itemTypesCopy.size() > 0) {

                b.append(" and (");

                Iterator<Integer> iter = itemTypesCopy.iterator();
                while (iter.hasNext()) {

                    b.append(" i.itemType=? ", iter.next());
                    if (iter.hasNext()) {
                        b.append(" OR ");
                    }

                }

                b.append(" ) ");
            }

        }

        if (minStock > 0) {
            b.append(" and i.ourStock>" + minStock);
        }

        if (searchAccount != null) {
            b.append(" and i.account=?", searchAccount);
        } else if (searchAccounts != null && !searchAccounts.isEmpty()) {
            b.append(" and (");

            Iterator<Integer> iter = searchAccounts.iterator();
            while (iter.hasNext()) {

                b.append(" i.account=? ", iter.next());
                if (iter.hasNext()) {
                    b.append(" OR ");
                }

            }

            b.append(" ) ");
        }

        if (recordingsOnly) {
            b.append(" and interaction.recordingsCount>0 ");
        }

        if (exactId > 0) {
            b.append(" and i.id=? ", exactId);
        }

        if (visibleOnly) {
            b.append(" and i.awaitingModeration=0 and i.awaitingValidation=0 ");
        }

        if (excludeFromGoogleFeed) {
            b.append(" and i.excludeFromGoogleFeed=0");
        }

        if (sortType == SortType.Attribute && sortAttribute != null) {
            b.append(" and av.attribute=? ", sortAttribute);
        }

        if (featuredMethod != null) {

            switch (featuredMethod) {

                default:
                case Any:
                    break;

                case No:
                    b.append(" and i.featured=0 ");
                    break;

                case Yes:
                    b.append(" and i.featured=1 ");
                    break;
            }
        }

        if (awaitingModeration) {
            b.append(" and i.awaitingModeration=1 ");
        }

        if (feedSrc != null) {
            b.append(" and i.feedSrc=? ", feedSrc);
        }

        if (feedCountBelow > 0) {
            b.append(" and i.feedCount<? ", feedCountBelow);
        }

        /*
           * If category is null, then this is a generic search, so we want to limit to items that are searchable only.
           * Otherwise when in a category, the searchable flag makes no difference
           *
           */
        if (searchCategory == null) {
            if (itemType == null) {
                if (itemTypes == null) {
                    if (searchableOnly) {
                        b.append(" and it.searchable=1 ");
                    }
                }
            }
        }

        if (name != null) {

            for (String string : name.split("\\s")) {

                if (string.startsWith("-")) {

                    string = "%" + string.substring(1) + "%";
                    b.append(" i.name not like ? ", string);

                } else {
                    for (String str : name.split("\\s")) {
                        string = "%" + str + "%";
                        b.append(" and i.name like ? ", string);
                    }

                    string = "%" + string + "%";
                    b.append(" and i.name like ? ", string);

                }
            }

        }

        if (displayName != null) {

            for (String string : name.split("\\s")) {

                if (string.startsWith("-")) {

                    string = "%" + string.substring(1) + "%";
                    b.append(" i.displayName not like ? ", string);

                } else {

                    string = "%" + string + "%";
                    b.append(" and i.displayName like ? ", string);

                }
            }

        }

        if (keywords != null) {

            for (String string : keywords.split("\\s")) {

                if (string.startsWith("-")) {

                    string = "%" + string.substring(1) + "%";
                    b.append(" and (i.name not like ? and i.content not like ?) ", string, string);

                } else {

                    string = "%" + string + "%";
                    b.append(" and (i.name like ? or i.content like ?) ", string, string);

                }
            }
        }

        if (groupedKeywords != null) {
            b.append(" and (");
            for (String string : groupedKeywords.split(",")) {
                string = "%" + string + "%";
                b.append(" i.name like ? or i.content like ? or ", string, string);

            }
            b.append(" 1!=1 )");
        }

        if (reference != null) {
            b.append(" and i.reference like ? ", "%" + reference + "%");
        }

        if (details != null) {
            if (details.split("\\s").length > 1) {

                b.append("and ((");

                for (String key : details.split("\\s")) {
                    key = "%" + key + "%";
                    b.append(" i.name like ? and ", key);
                }
                b.append(" 1=1) ");
                String string = "%" + details + "%";
                b.append(" or (i.content like ? ) or (avd.value like ?))", string, string);

            } else {

//            for (String detail : details.split("\\s")) {

                String string = "%" + details + "%";
                b.append(" and (i.name like ? or i.content like ?  ", string, string);
//                if (attributeValues != null && !attributeValues.isEmpty()) {
                b.append("or avd.value like ?)", string);
//                } else {
//                    b.append(")");
//                }
//            }
            }
        }

        if (sellPriceFrom != null) {
            if (sellPriceFrom.isPositive()) {
                if (Company.getInstance(context).isVatRegistered())
                    b.append(" and (genericSellPrice * ((vatRate/100)+1))>= ? ", sellPriceFrom);
                else
                    b.append(" and genericSellPrice >= ? ", sellPriceFrom);
            }
        }

        if (sellPriceTo != null) {
            if (sellPriceTo.isPositive()) {
                if (Company.getInstance(context).isVatRegistered())
                    b.append(" and (genericSellPrice * ((vatRate/100)+1)) <= ? ", sellPriceTo);
                else
                    b.append(" and genericSellPrice <= ? ", sellPriceTo);
            }
        }

        if (searchCategory != null) {

            if (searchCategory.isRoot() && subcats) {

            } else if (subcats) {

//                b.append(" and ci.category in (select category from # where ancestor=? union select ?) ", searchCategory, searchCategory);
                b.append(" and (ci.category in (select category from # where ancestor=?) or ci.category=?) ", searchCategory, searchCategory);
                b.addTable(CategoryAncestor.class);

            } else {

                b.append(" and ci.category=? ", searchCategory);
            }
        }

        if (itemModule != null) {
            b.append(" and it.modules like ? ", "%" + itemModule.name() + "%");
        }

        if (noImages) {
            b.append(" and i.imageCount=0 ");
        }

        if (noCategory) {
            b.append(" and i.categoryCount=0 ");
        }

        if (imageOnly) {
            b.append(" and i.imageCount>0 ");
        }

        if(inStockOnly) {
            b.append(" and i.availablestock>0 ");
        }

        if (age > 0) {
            b.append(" and i.dateCreated >=? ", new Date().removeDays(age));
        }

        if (subscriptionLevel != null) {
            b.append(" and sub.subscriptionLevel=?", subscriptionLevel);
        }

        if (status != null) {
            logger.fine("[ItemSearcher] setting item status=" + status);
            b.append(" and i.status=? ", status);
        }

        if (!includeHidden) {
            b.append(" and it.hidden=0 ");
        }

        if (lastActive != null) {

            switch (lastActive) {

                case OnlineNow:

                    long timeout = LastActive.getOnlineNowTimeout(context);

                    b.append(" and i.lastActive>=?");
                    b.addParameter(System.currentTimeMillis() - timeout);
                    break;

                case Today:
                    b.append(" and i.lastActive>=?");
                    b.addParameter(new Date());
                    break;

                case Within24Hours:
                    b.append(" and i.lastActive>=?");
                    b.addParameter(System.currentTimeMillis() - (1000l * 60 * 60 * 24));
                    break;

                case Within3Days:
                    b.append(" and i.lastActive>=?");
                    b.addParameter(new Date().removeDays(3));
                    break;

                case Week:
                    b.append(" and i.lastActive>=?");
                    b.addParameter(new Date().removeDays(7));
                    break;

                case Fortnight:
                    b.append(" and i.lastActive>=?");
                    b.addParameter(new Date().removeDays(14));
                    break;

                case Month:
                    b.append(" and i.lastActive>=?");
                    b.addParameter(new Date().removeMonths(1));
                    break;

                case OverMonth:
                    b.append(" and i.lastActive<=?");
                    b.addParameter(new Date().removeMonths(1));
                    break;

            }
        }

        if (createdAfter != null) {
            b.append(" and i.dateCreated>=? ", createdAfter);
        }

        if (joinedAfter != null) {
            b.append(" and i.joinedAfter>=? ", joinedAfter);
        }

        if (hasEmail) {
            b.append(" and i.email is not null ");
        }

        if (id != null) {

            id = id.replace("*", "%");
            b.append(" and i.id like ? ", id);
        }

        if (email != null) {
            b.append(" and i.email like ?", "%" + email + "%");
        }

        if (startsWith != null) {
            b.append(" and i.name like ? ", startsWith + "%");
        }

        if (sortType != null && sortType.equals(SortType.Random)) {
            if (excludeItemsIds != null && !excludeItemsIds.isEmpty()) {
                b.append(" and i.id not in ( ");
                for (Integer idNot : excludeItemsIds) {
                    b.append(idNot + ", ");
                }
                b.append(excludeItemsIds.get(0) + ") ");
            }
        }

        if (x > 0 && y > 0 && distance > 0 && location != null) {
            b.append(" group by i.id ");
            b.append(" having dist <= ? ", distance);
        }

        /*
           * No need to sort when simply returning a count!
           */
        if (!count) {

            b.append(" order by ");

            if (prioritised) {
                b.append(" i.prioritised desc, ");
            }

            logger.fine("[ItemSearcher] sortType=" + sortType);

            if (sortType == null) {

                b.append(" i.name ");

            } else {

                switch (sortType) {

                    case SubscriptionLevel:
                        b.append(" sub.position desc ");
                        break;

                    case Reference:

                        if (sortReverse) {
                            b.append(" i.reference desc ");
                        } else {
                            b.append(" i.reference ");
                        }
                        break;

                    case Distance:

                        if (sortReverse) {
                            b.append(" dist desc ");
                        } else {
                            b.append(" dist asc ");
                        }
                        break;

                    case Email:
                        if (sortReverse) {
                            b.append(" i.email desc ");
                        } else {
                            b.append(" i.email ");
                        }
                        break;

                    case Name:
                        if (sortReverse) {
                            b.append(" i.name desc ");
                        } else {
                            b.append(" i.name ");
                        }
                        break;

                    case Random:
                        if (seed == 0) {
                            b.append(" rand() ");
                        } else {
                            b.append(" rand(" + seed + ") ");
                        }
                        break;

                    case Status:
                        b.append(" i.status ");
                        break;

                    case Oldest:
                        b.append(" i.id ");
                        break;

                    case Newest:
                        b.append(" i.id desc ");
                        break;

                    case LastActive:
                        b.append(" i.lastActive desc ");
                        break;

                    case Attribute:

                        if (sortAttribute == null) {

                            b.append(" i.name ");

                        } else {

                            switch (sortAttribute.getType()) {

                                case Numerical:

                                    if (sortReverse) {
                                        b.append(" av.value + 0 desc ");
                                    } else {
                                        b.append(" av.value + 0 ");
                                    }
                                    break;

                                default:

                                    if (sortReverse) {
                                        b.append(" av.value desc ");
                                    } else {
                                        b.append(" av.value ");
                                    }

                            }
                        }
                        break;

                    case Availability:
                        b.append(" i.availableStock desc, i.name ");
                        break;

                    case Price:
                        if (sortReverse) {
                            b.append(" i.genericsellprice desc, i.name ");
                        } else {
                            b.append(" i.genericsellprice asc, i.name ");
                        }
                }
            }

        }

        logger.fine(b.toString());

        if (count) {

            b.append(") C ");

            int i = b.toQuery().getInt();
            logger.fine("[ItemSearcher] counted " + i + " objects");
            return i;

        }

        List objs = b.toQuery().execute(Item.class, start, limit);
//        List returnObjs = new ArrayList();
//        if (context.getReferrer() != null && context.getReferrer().indexOf("rallyinguk") != -1 && context.getReferrer().indexOf("admin") == -1) {
//            for (Object object : objs) {
//                Item obj = (Item) object;
//                if (obj.getListing() != null && (obj.getListing().getExpiryDate() == null || obj.getListing().getExpiryDate().getTimestamp() >= new Date().getTimestamp())) {
//                    returnObjs.add(obj);
//                }
//            }
//            objs = returnObjs;
//        }

        logger.fine("[ItemSearcher] " + objs.size() + " objects retrieved");

        return objs;
    }

    public int getCount() {
        return size();
    }

    public Item getItem() {
        limit = 1;
        List<Item> items = getItems();
        return items.isEmpty() ? null : items.get(0);
    }

    public List<Item> getItems() {
        return (List<Item>) execute(start, limit, false, 0);
    }

     public Integer getItemsCount() {
        return (Integer) execute(start, limit, true, 0);
    }

    public final String getKeywords() {
        return keywords;
    }

    public String getGroupedKeywords() {
        return groupedKeywords;
    }

    public int getLimit() {
        return limit;
    }

    public Link getLink() {

        Link link = new Link(ItemSearchHandler.class);

        link.setParameters("attributeValues", attributeValues);

        if (location != null) {
            link.setParameter("location", location);
            link.setParameter("x", x);
            link.setParameter("y", y);
        }

        if (details != null) {
            link.setParameter("details", details);
        }

        if (distance > 0) {
            link.setParameter("distance", distance);
        }

        if (excludeAccount) {
            link.setParameter("excludeAccount", true);
        }

        if (excludeAccountItemType) {
            link.setParameter("excludeAccountItemType", true);
        }

        if (email != null) {
            link.setParameter("email", email);
        }

        if (age > 0) {
            link.setParameter("age", age);
        }

        if (createdAfter != null) {
            link.setParameter("createdAfter", createdAfter);
        }

        if (joinedAfter != null) {
            link.setParameter("joinedAfter", joinedAfter);
        }

        if (featuredMethod != null) {
            link.setParameter("featuredMethod", featuredMethod);
        }

        if (imageOnly) {
            link.setParameter("imageOnly", imageOnly);
        }

        if (inStockOnly) {
            link.setParameter("inStockOnly", inStockOnly);
        }

        if (itemType != null) {
            link.setParameter("itemType", itemType);
        }

        if (keywords != null) {
            link.setParameter("keywords", keywords);
        }

        if (groupedKeywords != null) {
            link.setParameter("groupedKeywords", groupedKeywords);
        }

        if (lastActive != null) {
            link.setParameter("lastActive", lastActive);
        }

        if (limit > 0) {
            link.setParameter("limit", limit);
        }

        if (minStock > 0) {
            link.setParameter("minStock", minStock);
        }

        if (name != null) {
            link.setParameter("name", name);
        }

        if (noImages) {
            link.setParameter("noImages", noImages);
        }

        if (reference != null) {
            link.setParameter("reference", reference);
        }

        if (seed > 0) {
            link.setParameter("seed", seed);
        }

        if (sellPriceFrom != null) {
            if (sellPriceFrom.isPositive()) {
                link.setParameter("sellPriceFrom", sellPriceFrom);
            }
        }

        if (sellPriceTo != null) {
            if (sellPriceTo.isPositive()) {
                link.setParameter("sellPriceTo", sellPriceTo);
            }
        }

        if (searchCategory != null) {
            link.setParameter("searchCategory", searchCategory);
        }

        if (sortType != null) {
            link.setParameter("sortType", sortType);
        }

        return link;
    }

    public Set<String> getSearchParameters() {

//        Link link = new Link(ItemSearchHandler.class);
        Set<String> params = new HashSet<String>();

        if (!attributeValues.isEmpty()) {
            Iterator<Attribute> attributeValuesIterator = attributeValues.keySet().iterator();
            while (attributeValuesIterator.hasNext()) {

                Attribute attribute = attributeValuesIterator.next();
                String value = attributeValues.get(attribute);

                params.add(attribute.getName() + ": " + value);
            }

        }

//        if (location != null) {
//            params.add("Location: " + location + "; X: " + x + "; Y: " + y);
//        }

        if (details != null) {
            params.add("Details: " + details);
        }

//        if (distance > 0) {
//            link.setParameter("distance", distance);
//        }

        if (email != null) {
            params.add("Email: " + email);
        }


        if (inStockOnly) {
            params.add("In Stock Only: " + inStockOnly);
        }

        if (keywords != null) {
            params.add("Keywords: " + keywords);
        }


        if (minStock > 0) {
            params.add("Min Stock: " + minStock);
        }

        if (name != null) {
            params.add("Name: " + name);
        }

        if (sellPriceFrom != null) {
            if (sellPriceFrom.isPositive()) {
                params.add("Price from: " + sellPriceFrom);
            }
        }

        if (sellPriceTo != null) {
            if (sellPriceTo.isPositive()) {
                params.add("Price to: " + sellPriceTo);
            }
        }

        if (searchCategory != null) {
            params.add("Category: " + searchCategory.getName());
        }

//        if (sortType != null) {
//            params.add("Sort: " + sortType);
//
//        }

        return params;
    }

    public final String getName() {
        return name;
    }

    public MultiValueMap<String, String> getParameters() {
        return getLink().getParameters();
    }

    public final int getSeed() {
        return seed;
    }

    public final SortType getSortType() {
        return sortType;
    }

    public final String getStatus() {
        return status;
    }

    public boolean isCalendar() {
        return calendar;
    }

    public final boolean isExcludeAccount() {
        return excludeAccount;
    }

    public final boolean isExcludeAccountItemType() {
        return excludeAccountItemType;
    }

    public boolean isSubcats() {
        return subcats;
    }

    public boolean isVisibleOnly() {
        return visibleOnly;
    }

    /**
     * Use the iterator to iterate through all items without limit
     */
    public Iterator<Item> iterator() {

        return new Iterator<Item>() {

            private int n = 0;
            private List<Item> items;

            public boolean hasNext() {

                limit = limit == 0 ? 200 : limit;
                
                if (items == null) {
                    items = ((List<Item>) execute(n, limit, false, 0));
                    n = n + 200;
                }

                return items.size() > 0;
            }

            public Item next() {
                Item item = items.remove(0);
                if (items.isEmpty()) {
                    items = null;
                }
                return item;
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        };

    }

    public void prioritise() {
        this.prioritised = true;
    }

    public void removeLocation() {
        this.location = null;
        this.x = 0;
        this.y = 0;
    }

    public final void setAccount(Item active) {
        this.account = active;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setAttributeValues(MultiValueMap<Attribute, String> attributeValues) {
        this.attributeValues = attributeValues;
    }

    public void setAwaitingModeration(boolean b) {
        this.awaitingModeration = b;
    }

    public void setBookingEnd(Date bookingEnd) {
        this.bookingEnd = bookingEnd;
    }

    public void setBookingStart(Date bookingStart) {
        this.bookingStart = bookingStart;
    }

    public void setCreatedAfter(Date createdAfter) {
        this.createdAfter = createdAfter;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public final void setExcludeAccount(boolean excludeActiveItem) {
        this.excludeAccount = excludeActiveItem;
    }

    public final void setExcludeAccountItemType(boolean excludeActiveItemType) {
        this.excludeAccountItemType = excludeActiveItemType;
    }

    public void setFeaturedMethod(FeaturedMethod featuredMethod) {
        this.featuredMethod = featuredMethod;
    }

    public void setFeedCountBelow(int i) {
        this.feedCountBelow = i;
    }

    public void setFeedSrc(String feedSrc) {
        this.feedSrc = feedSrc;
    }

    public void setHasEmail(boolean b) {
        this.hasEmail = b;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImageOnly(boolean imageOnly) {
        this.imageOnly = imageOnly;
    }

    public void setImageSearchType(ImageSearchType imageSearchType) {

        if (imageSearchType == null) {
            this.imageOnly = false;
            this.noImages = false;
        }

        if (imageSearchType == ImageSearchType.ImagesOnly) {
            this.imageOnly = true;
            this.noImages = false;
        }

        if (imageSearchType == ImageSearchType.NoImages) {
            this.imageOnly = false;
            this.noImages = true;
        }
    }

    public final void setIncludeHidden(boolean hidden) {
        this.includeHidden = hidden;
    }

    public void setItemModule(ItemModule itemModule) {
        this.itemModule = itemModule;
    }

    public void setItemType(int itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public void setItemTypes(Set<Integer> itemTypes) {
        this.itemTypes = itemTypes;
    }

    public void setIsCalendar(boolean calendar) {
        this.calendar = calendar;
    }

    public void setJoinedAfter(Date joinedAfter) {
        this.joinedAfter = joinedAfter;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public void setGroupedKeywords(String groupedKeywords) {
        this.groupedKeywords = groupedKeywords;
    }

    public void setLastActive(LastActive lastActive) {
        this.lastActive = lastActive;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setLocation(String location, int x, int y) {

        this.location = location;
        this.x = x;
        this.y = y;

    }

    public void setMethod(HighlightMethod method) {

        if (method == null) {
            return;
        }

        switch (method) {

            case Featured:

                // for featured set featured to true and sort by random
                sortType = SortType.Random;
                featuredMethod = FeaturedMethod.Yes;
                break;

            default:

            case Popular:
            case Latest:
                sortType = SortType.Newest;
                break;

            case Random:
                sortType = SortType.Random;
                break;

        }
    }

    public void setMinStock(int i) {
        minStock = i;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNoCategory(boolean noCategory) {
        this.noCategory = noCategory;
    }

    public void setNoImages(boolean noImages) {
        this.noImages = noImages;
    }

    public final void setRecordingsOnly(boolean recordingsOnly) {
        this.recordingsOnly = recordingsOnly;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public void setSearchableOnly(boolean b) {
        this.searchableOnly = b;
    }

    /**
     *
     */
    public void setSearchAccount(Item searchAccount) {
        this.searchAccount = searchAccount;
    }

    public void setSearchCategory(Category searchCategory) {
        this.searchCategory = searchCategory;
        context.setAttribute("trailCategory", searchCategory);
    }

    /**
     *
     */
    public void setSearchControls(Map<Attribute, SearchControl> searchControls) {
        this.searchControls = searchControls;
    }

    public final void setSeed(int seed) {
        this.seed = seed;
    }

    public void setSellPriceFrom(Money sellPriceFrom) {
        this.sellPriceFrom = sellPriceFrom;
    }

    public void setSellPriceTo(Money sellPriceTo) {
        this.sellPriceTo = sellPriceTo;
    }

    public void setSort(ItemSort sort) {

        if (sort == null) {
            return;
        }

        if (sort.getType() == SortType.Attribute && !sort.hasAttribute()) {
            return;
        }

        this.sortAttribute = sort.getAttribute();
        this.sortType = sort.getType();
        this.sortReverse = sort.isReverse();

        logger.fine("[ItemSearcher] setting item sort, sortType=" + sortType + ", reverse=" + sortReverse + ", attribute=" + sortAttribute);
    }

    public final void setSortAttribute(Attribute sortAttribute) {
        this.sortAttribute = sortAttribute;
    }

    public void setSortType(SortType sortType) {
        this.sortType = sortType;
        logger.fine("[ItemSearcher] setting sortType=" + sortType);
    }

    public void setStart(int start) {
        this.start = start;
    }

    public void setStartsWith(String letter) {
        this.startsWith = letter;
    }

    public final void setStatus(String status) {
        this.status = status;
    }

    public void setSubcats(boolean subcats) {
        this.subcats = subcats;
    }

    public void setVisibleOnly(boolean v) {
        this.visibleOnly = v;
    }

    public void setStockOnly(boolean inStockOnly) {
        this.inStockOnly = inStockOnly;
    }

    public void setExcludeItemsIds(List<Integer> excludeItemsIds) {
        this.excludeItemsIds = excludeItemsIds;
    }

    public int size() {
        Integer integer = (Integer) execute(0, 0, true, 0);
        return integer.intValue();
    }


    public boolean isPrioritised() {
        return prioritised;
    }


    public void setPrioritised(boolean prioritised) {
        this.prioritised = prioritised;
    }

    public void setExcludeFromGoogleFeed(boolean excludeFromGoogleFeed) {
        this.excludeFromGoogleFeed = excludeFromGoogleFeed;
    }

    public void setSearchAccounts(List<Integer> searchAccounts) {
        this.searchAccounts = searchAccounts;
    }
}
