package org.sevensoft.ecreator.model.items.renderers;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
public class FamilyHeaderMarkupDefault implements MarkupDefault {

    public String getBetween() {
        return null;
    }

    public String getCss() {
        return null;
    }

    public String getBody() {

        StringBuilder sb = new StringBuilder();

        sb.append("[item_family_header?id=8&name=&class=items_list&tag=th]");
        sb.append("<th class='items_list'>Part Name</th>");
        sb.append("[item_family_header?id=17&name=&class=items_list&tag=th]");
        sb.append("[item_family_header?id=18&name=&class=items_list&tag=th]");
        sb.append("[item_family_header?id=19&name=&class=items_list&tag=th]");
        sb.append("[item_family_header?id=12&name=&class=items_list&tag=th]");

        return sb.toString();
    }

    public String getEnd() {
        return null;
    }

    public String getName() {
        return null;
    }

    public String getStart() {

        StringBuilder sb = new StringBuilder();

        sb.append("<style>\n");

        sb.append("table.items_list * { font-family: Tahoma; font-size: 12px; } \n");
        sb.append("table.items_list { width: 100%;border-bottom: 1px solid #cccccc; } \n");

        sb.append("</style>");

        return sb.toString();
    }

    public int getTds() {
        return 0;
    }
}