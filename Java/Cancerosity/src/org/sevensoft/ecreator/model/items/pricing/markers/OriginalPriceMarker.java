package org.sevensoft.ecreator.model.items.pricing.markers;

import java.util.Map;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class OriginalPriceMarker implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
        Item account = (Item) context.getAttribute("account");

        if (!PermissionType.ItemPriceSummary.check(context, account, item.getItemType())) {
            return null;
        }
		//		if (!item.getItemType().isPromotionPrices())
		//			return null;

		if (!item.getPricing().hasPromotionPrice()) {
			return null;
		}

		Currency currency = (Currency) context.getAttribute("currency");
		String currencySymbol = (String) context.getAttribute("currencySymbol");

		//		/*
		//		 * No point showing the original price if there is no discount - it would be the same price !
		//		 */
		//		Money discount = item.getPromotionDiscount(currency);
		//		if (discount == null || discount.isZero())
		//			return null;

		// get our original price
		Money originalSellPrice;
		if (params.containsKey("inc"))
			originalSellPrice = item.getOriginalSellPriceInc(currency);
		else
			originalSellPrice = item.getOriginalSellPrice(currency);

		String prefix = params.get("prefix");
		String suffix = params.get("suffix");

		StringBuilder sb = new StringBuilder();
		sb.append("<span class='price_original'>");

		if (prefix != null)
			sb.append(prefix);

		sb.append("<span class='theprice'>");
		sb.append(currencySymbol);
		sb.append(originalSellPrice);
		sb.append("</span>");

		if (suffix != null)
			sb.append(suffix);

		sb.append("</span>");
		return sb.toString();
	}

	public Object getRegex() {
		return "pricing_original";
	}

}
