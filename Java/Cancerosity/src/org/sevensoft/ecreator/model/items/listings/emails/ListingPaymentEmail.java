package org.sevensoft.ecreator.model.items.listings.emails;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.listings.ListingRate;
import org.sevensoft.ecreator.model.items.listings.ListingSettings;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 06-Dec-2005 11:41:11
 * 
 */
public class ListingPaymentEmail extends Email {

	public ListingPaymentEmail(Item item, ListingRate rate, RequestContext context) {

		ListingSettings ls = ListingSettings.getInstance(context);

		setSubject("Listing payment received");
		int n = 0;
		for (String email : ls.getNotificationEmails()) {
			if (n == 0)
				setTo(email);
			else
				addBcc(email);
			n++;
		}
		addBcc("sks@7soft.co.uk");
		setFrom(Company.getInstance(context).getName(), Config.getInstance(context).getServerEmail());

		StringBuilder sb = new StringBuilder();
		sb.append("Payment received for: " + item.getName() + "\n");
		sb.append(item.getUrl());
		//		sb.append("\n" + rate.getDescription());

		setBody(sb.toString());
	}

}
