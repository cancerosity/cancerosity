package org.sevensoft.ecreator.model.items.alternatives.marker;

import java.util.Map;
import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.alternatives.AlternativesModule;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 21 Apr 2006 11:31:20
 *
 */
public class AlternativesMarker implements IItemMarker {

	private static Logger	logger	= Logger.getLogger("markup");

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

//		return null;

        if (!ItemModule.Alternatives.enabled(context, item)) {
            return null;
        }

        ItemType itemType = item.getItemType();
        AlternativesModule module = itemType.getAlternativesModule();

        int limit;

        if (params.containsKey("limit"))
            limit = Integer.parseInt(params.get("limit").trim());
        else
            limit = module.getAlternativesQty();

        logger.fine("alternatives limit: " + limit);

        List<Item> alternatives = item.getAutoAlternatives(limit);

        // if no alternatives in a parent category then find item with common attribute value
        if (alternatives == null || alternatives.isEmpty()) {
            logger.fine("check alternatives via attribute");
            if (module.getAlternativesAttribute() != null) {
                alternatives = item.getAutoAlternatives(module.getAlternativesAttribute(), limit);
            }
//            return null;
        }
        // or use trail category as a parent, use '[item?link=1&trail=1]' to make trail availbale if items
        // don't blong to categories and showed via category search wrapper
        if (alternatives == null || alternatives.isEmpty()) {
            Category category = (Category) context.getAttribute("trailCategory");
            if (category == null || !category.isSearchWrapper()) {
                return null;
            }

            ItemSearcher searcher = category.getSearch().getSearcher();
            searcher.setStatus("Live");
            searcher.setLimit(limit);

            alternatives = searcher.getItems();
        }

        if (alternatives == null || alternatives.isEmpty()) {
            logger.fine("no alternatives found, exiting");
            return null;
        }

        logger.fine("found " + alternatives.size() + " alts");

        StringBuilder sb = new StringBuilder();

        String caption = params.get("caption");
        if (caption == null) {
            caption = module.getAlternativesCaption();
        }

        if (caption != null) {

            logger.fine("caption=" + caption);

            sb.append("<div class='alternatives_caption'>");
            sb.append(caption);
            sb.append("</div>");
        }

        MarkupRenderer r = new MarkupRenderer(context, module.getAlternativesMarkup());
        r.setBodyObjects(alternatives);
        sb.append(r);

        return sb;
	}

	public Object getRegex() {
		return "alternatives";
	}
}
