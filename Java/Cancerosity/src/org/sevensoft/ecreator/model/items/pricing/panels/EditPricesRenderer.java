package org.sevensoft.ecreator.model.items.pricing.panels;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.pricing.Price;
import org.sevensoft.ecreator.model.items.pricing.PriceBand;
import org.sevensoft.ecreator.model.items.pricing.PriceSettings;
import org.sevensoft.ecreator.model.items.pricing.PriceUtil;
import org.sevensoft.ecreator.model.items.pricing.Priceable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 8 Oct 2006 14:49:03
 * 
 * Renders a table for entering price definitions
 *
 */
public class EditPricesRenderer {

	private Logger				logger	= Logger.getLogger("ecreator");

	private final RequestContext		context;
	private final List<PriceBand>		priceBands;
	private final Collection<Integer>	priceBreaks;

	/**
	 * The object that these price definitions are defined on.
	 */
	private final Priceable			priceable;
	private final PriceSettings		priceSettings;
	private int					span;
	private final List<Price>		prices;

	private boolean				usePriceBands;
	private boolean				usePriceBreaks;

	public EditPricesRenderer(RequestContext context, Priceable pricable) {

		this.context = context;
		this.priceable = pricable;
		this.priceSettings = PriceSettings.getInstance(context);

		// get price breaks
		this.priceBreaks = pricable.getPriceBreaks();
		this.span = priceBreaks.size();

		// get all prices
		this.prices = pricable.getPrices();

		/*
		 * Get our price bands, and include null for use as the standard price
		 */
		this.priceBands = new ArrayList();
		this.priceBands.add(0, null);

		if (priceSettings.isUsePriceBands()) {

			this.priceBands.addAll(PriceBand.get(context));
			this.usePriceBands = true;
		}

		if (priceSettings.isUsePriceBreaks() && priceBreaks.size() > 1) {
			this.usePriceBreaks = true;
		}
	}

	private void header(StringBuilder sb) {

		if (usePriceBands || usePriceBreaks) {

			sb.append("<tr>");

			sb.append("<th width='150'></th>");

			if (usePriceBreaks) {

				for (int qty : priceable.getPriceBreaks()) {
					sb.append("<th>Qty up to " + qty + "</th>");
				}

			} else {

				sb.append("<th>Price</th>");
			}

		}

		sb.append("</tr>");
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(new AdminTable("Prices"));

		if (priceSettings.isUsePriceBreaks()) {
			sb.append(new PriceBreaksPanel(context, priceable, span));
		}

		header(sb);

		int n = 0;
		for (PriceBand priceBand : priceBands) {

			sb.append("<tr>");

			sb.append("<td width='150'><b>");

			// if we have price bands then get an appropriate name for the band
			if (usePriceBands) {

				// if we have the null member group then show "standard price"
				if (priceBand == null) {

					sb.append("Standard price band");

				} else {

					// otherwise display price band name
					sb.append(priceBand.getName());
				}

			} else {

				sb.append("Price");

			}

			sb.append("</b></td>");

			for (int qty : priceBreaks) {

				Price price = PriceUtil.getPrice(prices, priceBand, qty);
				String priceString = PriceUtil.getPriceString(price);

				String calculatedPrice = "";
				if (priceable instanceof Item && price != null) {

					Item item = (Item) priceable;
					Money costPrice = item.getCostPrice();
					if (costPrice != null && costPrice.isPositive()) {
						calculatedPrice = price.calculateSellPrice(costPrice).toString();
					}
				}

				sb.append("<td>" + new TextTag(context, "prices_" + n, priceString, 8) + " " + calculatedPrice + "</td>");
				n++;
			}

			sb.append("</tr>");

		}

		sb.append(new HiddenTag("priceRows", n));
		sb.append("</table>");

		return sb.toString();
	}
}
