package org.sevensoft.ecreator.model.items;

import java.util.List;

import org.sevensoft.ecreator.model.attributes.support.AttributeOwnerSupport;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 19 Dec 2006 14:31:30
 *
 */
@Table("items_types_settings")
@Singleton
public class ItemTypeSettings extends AttributeOwnerSupport implements Logging {

	public static ItemTypeSettings getInstance(RequestContext context) {
		return EntityObject.getSingleton(context, ItemTypeSettings.class);
	}

	public ItemTypeSettings(RequestContext context) {
		super(context);
	}

	public List<LogEntry> getLogEntries() {
		return LogEntry.get(context, this);
	}

	public String getLogId() {
		return getFullId();
	}

	public String getLogName() {
		return "Item Type Settings";
	}

	public void log(String message) {
		new LogEntry(context, this, message);
	}

	public void log(User user, String message) {
		new LogEntry(context, this, user, message);
	}

}
