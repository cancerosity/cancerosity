package org.sevensoft.ecreator.model.items.pricing.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.pricing.PriceSettings;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.skint.Amount;

import java.util.Map;

/**
 * User: Tanya
 * Date: 30.08.2010
 */
public class DiscountPercentageMarker extends MarkerHelper implements IItemMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
        if (!Module.CategoryAndItemDiscount.enabled(context)) {
            return null;
        }
        if (item.getCategoryWithDiscount().isHideDiscountInfo()) {
            return null;
        }

        PriceSettings ps = PriceSettings.getInstance(context);

        return super.string(context, params, item.getDiscount(ps));
    }

    public Object getRegex() {
        return "percentage_discount";
    }
}
