// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   ExportFieldType.java

package org.sevensoft.ecreator.model.items.export;


public enum ExportFieldType {


    Name("Name", 0), Reference("Reference", 1), RRP("RRP", 2), Attribute("Attribute", 3), SellingPrice("SellingPrice", 4), CostPrice("CostPrice", 5), Content("Content", 6),
    Image("Image", 7), Keywords("Keywords", 8), DescriptionTag("DescriptionTag", 9), TitleTag("TitleTag", 10), ItemUrl("Item URL", 11);

    private final String fieldName;
    private final int position;

    private ExportFieldType(String fieldName, int position) {
        this.fieldName = fieldName;
        this.position = position;
    }

}
