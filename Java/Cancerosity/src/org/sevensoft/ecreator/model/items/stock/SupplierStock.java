package org.sevensoft.ecreator.model.items.stock;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.ecom.suppliers.Supplier;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 6 Aug 2006 17:30:57
 *
 */
@Table("items_stock")
public class SupplierStock extends EntityObject {

	private int		stock;
	private Item	item;
	private Supplier	supplier;
	private DateTime	time;

	public SupplierStock(RequestContext context) {
		super(context);
	}

	public SupplierStock(RequestContext context, Item item, Supplier supplier, int stock) {
		super(context);

		this.item = item;
		this.stock = stock;
		this.supplier = supplier;

		this.time = new DateTime();

		save();
	}

	public Item getItem() {
		return item.pop();
	}

	public int getStock() {
		return stock;
	}

	public Supplier getSupplier() {
		return supplier.pop();
	}

	public DateTime getTime() {
		return time;
	}

	public boolean hasTime() {
		return time != null;
	}

}
