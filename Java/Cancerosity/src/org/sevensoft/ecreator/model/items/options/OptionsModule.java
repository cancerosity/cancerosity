package org.sevensoft.ecreator.model.items.options;

import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Nov 2006 06:57:06
 *
 */
@Table("items_modules_options")
public class OptionsModule extends EntityObject {

	private ItemType	itemType;

	private boolean	displayExVat;

	private OptionsModule(RequestContext context) {
		super(context);
	}

	public OptionsModule(RequestContext context, ItemType type) {
		super(context);
		this.itemType = type;
	}

	public boolean isDisplayExVat() {
		return displayExVat;
	}

	public void setDisplayExVat(boolean displayExVat) {
		this.displayExVat = displayExVat;
	}

}
