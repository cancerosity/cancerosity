package org.sevensoft.ecreator.model.items.highlighted;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 18-Mar-2006 00:44:17
 *
 */
public enum HighlightMethod {

	/**
	 * 
	 */
	Featured,

	/**
	 * 
	 */
	Latest,

	/**
	 * 
	 */
	Random,

	/**
	 * 
	 */
	Popular;

	public static Set<HighlightMethod> getItem(RequestContext context) {

		Set<HighlightMethod> set = new TreeSet<HighlightMethod>(new Comparator<HighlightMethod>() {

			public int compare(HighlightMethod o1, HighlightMethod o2) {
				return o1.name().compareTo(o2.name());
			}
		});
		set.add(Featured);
		set.add(Random);
		set.add(Popular);
		set.add(Latest);

		return set;
	}

	public static Set<HighlightMethod> getMember() {

		Set<HighlightMethod> set = new TreeSet<HighlightMethod>(new Comparator<HighlightMethod>() {

			public int compare(HighlightMethod o1, HighlightMethod o2) {
				return o1.name().compareTo(o2.name());
			}
		});
		set.add(Featured);
		set.add(Random);
		set.add(Popular);
		set.add(Latest);

		return set;
	}
}
