package org.sevensoft.ecreator.model.items.reviews;

import java.util.List;

import org.sevensoft.ecreator.iface.frontend.items.ReviewsHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 22 Mar 2007 17:59:47
 *
 */
@Table("blocks_reviews")
@Label("Reviews")
public class ReviewsBlock extends Block {

	private int	perPage;

	public ReviewsBlock(RequestContext context) {
		super(context);
	}

	public ReviewsBlock(RequestContext context, BlockOwner owner, int i) {
		super(context, owner, 0);
	}

	public final int getPerPage() {
		return perPage;
	}

	@Override
	public Object render(RequestContext context) {

		if (!Module.Reviews.enabled(context)) {
			return null;
		}

		Item item = (Item) context.getAttribute("item");
		if (item == null) {
			return null;
		}

		StringBuilder sb = new StringBuilder();

		sb.append("<div class='ec_reviews_title'>Reviews</div>");

		ReviewsDelegate del = item.getReviewsDelegate();
		List<Review> reviews = del.getReviews();
		for (Review review : reviews) {
			sb.append("<div class='ec_review'>" + review.getContent() + "</div>");
		}

		sb.append("<div class='ec_reviews_add'>Add your own review " + new LinkTag(ReviewsHandler.class, null, "click here", "item", item) + "</div>");
		return sb;
	}

	public final void setPerPage(int perPage) {
		this.perPage = perPage;
	}
}
