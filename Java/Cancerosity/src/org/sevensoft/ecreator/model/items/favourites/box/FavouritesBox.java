package org.sevensoft.ecreator.model.items.favourites.box;

import java.util.List;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Transformer;
import org.sevensoft.ecreator.iface.admin.items.modules.favourites.FavouritesBoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.favourites.Favourite;
import org.sevensoft.ecreator.model.items.favourites.FavouritesGroup;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 22 Aug 2006 23:45:00
 *
 */
@Table("boxes_favourites")
@Label("Favourites")
@HandlerClass(FavouritesBoxHandler.class)
public class FavouritesBox extends Box {

	/**
	 * The group of favourites to show
	 */
	private FavouritesGroup	favouritesGroup;

	public FavouritesBox(RequestContext context) {
		super(context);
	}

	public FavouritesBox(RequestContext context, String location) {
		super(context, location);
	}

	@Override
	protected String getCssIdDefault() {
		return null;
	}

	public FavouritesGroup getFavouritesGroup() {
		return (FavouritesGroup) (favouritesGroup == null ? null : favouritesGroup.pop());
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	@Override
	public String render(RequestContext context) {

		if (!Module.Favourites.enabled(context)) {
			return null;
		}

		Item account = (Item) context.getAttribute("account");
		if (account == null) {
			return null;
		}

//		if (favouritesGroup == null) {
//			return null;
//		}

//		List<Favourite> favourites = account.getFavourites(getFavouritesGroup());
        List<Favourite> favourites = Favourite.get(context, account);
		if (favourites.isEmpty()) {
			return null;
		}

		if (hasMarkup()) {

			List<Item> items = CollectionsUtil.transform(favourites, new Transformer<Favourite, Item>() {

				public Item transform(Favourite obj) {
					return obj.getItem();
				}
			});

			MarkupRenderer r = new MarkupRenderer(context, getMarkup());
			r.setBodyObjects(items);
			return r.toString();

		} else {

			StringBuilder sb = new StringBuilder();
			sb.append(getTableTag());

			for (Favourite favourite : favourites) {

				sb.append("<tr><td>");
				sb.append(new LinkTag(favourite.getItem().getUrl(), favourite.getName()));
				sb.append("</td></tr>");
			}

			sb.append("<tr><td class='bottom'>&nbsp;</td></tr>");
			sb.append("</table>");
			return sb.toString();
		}
	}

	public void setFavouritesGroup(FavouritesGroup favouritesGroup) {
		this.favouritesGroup = favouritesGroup;
	}

}
