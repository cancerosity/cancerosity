package org.sevensoft.ecreator.model.items.ibex;

import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.attributes.Attribute;

/**
 * User: Tanya
 * Date: 04.03.2011
 */
@Table("settings_ibex")
@Singleton
public class IbexSettings extends EntityObject{

    protected IbexSettings(RequestContext context) {
        super(context);
    }

    public static IbexSettings getInstance(RequestContext context){
        return getSingleton(context, IbexSettings.class);
    }

    private String op;
    private String cyid;
    private int frameWidth;
    private int frameHeight;
    private Attribute propertyId;

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public String getCyid() {
        return cyid;
    }

    public void setCyid(String cyid) {
        this.cyid = cyid;
    }

    public int getFrameWidth() {
        return frameWidth == 0 ? 770 : frameWidth;
    }

    public void setFrameWidth(int frameWidth) {
        this.frameWidth = frameWidth;
    }

    public int getFrameHeight() {
        return frameHeight == 0 ? 650 : frameHeight;
    }

    public void setFrameHeight(int frameHeight) {
        this.frameHeight = frameHeight;
    }

    public Attribute getPropertyId() {
        return propertyId == null ? null : (Attribute) propertyId.pop();
    }

    public void setPropertyId(Attribute propertyId) {
        this.propertyId = propertyId;
    }
}
