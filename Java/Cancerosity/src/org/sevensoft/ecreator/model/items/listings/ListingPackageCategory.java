package org.sevensoft.ecreator.model.items.listings;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 07-Mar-2006 09:41:14
 * 
 * A category we can list an item in for this listing package
 *
 */
@Table("listings_packages_categories")
public class ListingPackageCategory extends EntityObject {

	private ListingPackage	listingPackage;
	private Category		category;

	public ListingPackageCategory(RequestContext context) {
		super(context);
	}

	public ListingPackageCategory(RequestContext context, ListingPackage rate, Category category) {
		super(context);

		Query q = new Query(context, "select count(*) from # where listingPackage=? and category=?");
		q.setTable(ListingPackageCategory.class);
		q.setParameter(rate);
		q.setParameter(category);
		if (q.getInt() == 0) {

			this.listingPackage = rate;
			this.category = category;

			save();
		}
	}

	public Category getCategory() {
		return category.pop();
	}

	public ListingPackage getFee() {
		return listingPackage.pop();
	}

}
