package org.sevensoft.ecreator.model.items.pricing.costs;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.suppliers.Supplier;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 6 Aug 2006 17:27:25
 *
 */
@Table("items_prices_cost")
public class SupplierCostPrice extends EntityObject {

	@Index()
	private Item	item;

	@Index()
	private Supplier	supplier;

	private Money	costPrice;

	/**
	 * The datetime this cost price was created
	 */
	private DateTime	time;

	private SupplierCostPrice(RequestContext context) {

		super(context);
	}

	public SupplierCostPrice(RequestContext context, Item item, Supplier supplier, Money money) {

		super(context);

		this.item = item;
		this.supplier = supplier;
		this.costPrice = money;
		this.time = new DateTime();

		save();
	}

	public Money getCostPrice() {
		return costPrice;
	}

	public Item getItem() {
		return item.pop();
	}

	public Supplier getSupplier() {
		return supplier.pop();
	}

	public DateTime getTime() {
		return time;
	}

	/**
	 * @return
	 */
	public boolean hasTime() {
		return time != null;
	}

	public void setCostPrice(Money costPrice) {
		this.costPrice = costPrice;
	}

}
