package org.sevensoft.ecreator.model.items.relations;

import java.util.List;

import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 28 Mar 2007 15:35:14
 *
 */
@Table("relations_module")
public class RelationsModule extends EntityObject {

	public static RelationsModule get(RequestContext context, ItemType itemType) {
		RelationsModule module = SimpleQuery.get(context, RelationsModule.class, "itemType", itemType);
		return module == null ? new RelationsModule(context, itemType) : module;
	}

	private ItemType	itemType;

	protected RelationsModule(RequestContext context) {
		super(context);
	}

	public RelationsModule(RequestContext context, ItemType itemType) {
		super(context);
		this.itemType = itemType;

		save();
	}

	public List<RelationGroup> getGroups() {
		return SimpleQuery.execute(context, RelationGroup.class, "itemType", itemType);
	}

	public void removeGroup(RelationGroup group) {
		group.delete();
	}

}
