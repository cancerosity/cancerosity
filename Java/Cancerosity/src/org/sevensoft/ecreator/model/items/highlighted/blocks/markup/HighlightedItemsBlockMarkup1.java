package org.sevensoft.ecreator.model.items.highlighted.blocks.markup;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 12-Jan-2006 15:01:12
 *
 */
public class HighlightedItemsBlockMarkup1 implements MarkupDefault {

	public String getBetween() {
		return null;
	}

	public String getBody() {

		StringBuilder sb = new StringBuilder();

		sb.append("<div class='highlighted'>\n\n");

		sb.append(new TableTag("hi").setCaption("In [category]"));
		sb.append("<tr><td class='image' valign='middle' align='center'>[thumbnail?w=160&h=120&link=1&limit=1]</td></tr>\n");

		sb.append("<tr><td class='details' valign='bottom'>\n");
		sb.append("<div class='name'>[item?link=1]</div>\n");
		sb.append("[pricing_sell?inc=1]\n");
		sb.append("[stock?label=Availability: &text=true]\n");
		sb.append("[ordering_buy]\n");
		sb.append("</td></tr>\n");
		sb.append("</table>\n\n");

		sb.append("</div>");

		return sb.toString();
	}

	public String getEnd() {
		return null;
	}

	public String getName() {
		return "Highlighted items block style 1";
	}

	public String getStart() {

		StringBuilder sb = new StringBuilder();

		sb.append("<style>\n");
		sb.append("div.highlighted { margin: 5px; height: 250px; width: 200px; border: 1px solid #cccccc; padding: 5px; float: left; } \n");
		sb.append("table.hi { width: 100%; } \n");
		sb.append("table.hi caption { background: #555555; color: white; padding: 3px 6px; font-size: 14px; font-weight: bold; } \n");
		sb.append("table.hi div.name { font-weight: bold; font-size: 12px; margin-bottom: 8px; } \n");
		sb.append("table.hi div.price { color: #cc0000; font-weight: bold; font-size: 16px; } \n");
		sb.append("table.hi div.availability { font-size: 11px; } \n");
		sb.append("table.hi div.buy { text-align: right; } \n");
		sb.append("table.hi input { border: 1px solid #333333; color: #333333; margin: 4px; padding: 2px 3px; "
				+ "background: white; font-size: 11px; font-weight: bold; }\n");
		sb.append("table.hi td.image { padding: 5px 0; height: 130px; }\n");
		sb.append("table.hi td.image img { border: 0; }\n");
		sb.append("</style>\n");

		return sb.toString();
	}

	public int getTds() {
		return 0;
	}

	public String getCss() {
		return null;
	}
}
