package org.sevensoft.ecreator.model.items.adminsearch;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.AbstractTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 9 Jan 2007 15:05:48
 *
 */
public enum AdminFieldType {

	Name() {

		@Override
		public SortType getSortType() {
			return SortType.Name;
		}

		@Override
		public String getText(Item item, Attribute attribute) {

			StringBuilder sb = new StringBuilder();
			sb.append(new LinkTag(ItemHandler.class, "edit", item.getName(), "item", item).toString());

			if (item.getItemType().isCategories()) {
				sb.append("<br/>");
				sb.append(item.getCategoryNames());
			}

			return sb.toString();
		}
	},
	Image() {

		@Override
		public String getText(Item item, Attribute attribute) {
			Img img = item.getAnyImage();
			if (img == null) {
				return null;
			} else {
				return img.getThumbnailTag(80, 80).toString();
			}
		}
	},
	SellPrice() {

		@Override
		public SortType getSortType() {
			return SortType.Price;
		}

		@Override
		public String getText(Item item, Attribute attribute) {

			Money salePrice = item.getSellPrice();
			if (salePrice.isZero()) {
				return "-";
			} else {
				return salePrice.toString();
			}

		}

		@Override
		public String toString() {
			return "Sell price";
		}

        @Override
        public AbstractTag getTag(RequestContext context, String value) {
            return new TextTag(context, "sellPrice", value, 12);
        }
    },
	CostPrice() {

		@Override
		public String getText(Item item, Attribute attribute) {
			return item.getCostPrice().toString();
		}

		@Override
		public String toString() {
			return "Cost price";
		}

        @Override
        public AbstractTag getTag(RequestContext context, String value) {
            return new TextTag(context, "costPrice", value, 12);
        }
    },
	Images() {

		@Override
		public String getText(Item item, Attribute attribute) {
			return String.valueOf(item.getAllImagesCount());
		}
	},
	Attachments() {

		@Override
		public String getText(Item item, Attribute attribute) {
			return String.valueOf(item.getAttachmentCount());
		}
	},
	Summary() {

		@Override
		public String getText(Item item, Attribute attribute) {
			return item.getSummary();
		}

        @Override
        public AbstractTag getTag(RequestContext context, String value) {
            return new TextTag(context, "summary", value, 50);
        }
    },
	Id() {

		@Override
		public SortType getSortType() {
			return SortType.Oldest;
		}

		@Override
		public String getText(Item item, Attribute attribute) {
			return new LinkTag(ItemHandler.class, "edit", item.getIdString(), "item", item).toString();
		}
	},
	Reference() {

		@Override
		public SortType getSortType() {
			return SortType.Reference;
		}

		@Override
		public String getText(Item item, Attribute attribute) {
			return item.getReference();
		}

//        @Override
//        public AbstractTag getTag(RequestContext context, String value) {
//            return new TextTag(context, "reference", value, 30);
//        }
    },
	DateCreated() {

		@Override
		public SortType getSortType() {
			return SortType.Newest;
		}

		@Override
		public String getText(Item item, Attribute attribute) {
			return item.getDateCreated().toString("dd-MMM-yyyy");
		}

		@Override
		public String toString() {
			return "Date created";
		}
	},
	Status() {

		@Override
		public SortType getSortType() {
			return SortType.Status;
		}

		@Override
		public String getText(Item item, Attribute attribute) {
			return item.getStatus();
		}

        @Override
        public AbstractTag getTag(RequestContext context, String value) {
            SelectTag tag = new SelectTag(context, "status", value);
            tag.addOption("LIVE");
            tag.addOption("DISABLED");
            tag.addOption("DELETED");
            return tag;
        }
    },
	Attribute() {

		@Override
		public String getText(Item item, Attribute attribute) {
			return item.getAttributeValue(attribute);
		}
	},
    Stock() {

        public String getText(Item item, Attribute attribute) {
            return item.getOurStockString();
        }

        @Override
        public AbstractTag getTag(RequestContext context, String value) {
            return new TextTag(context, "ourStock", value, 5);
        }
    };

	public SortType getSortType() {
		return null;
	}

	public abstract String getText(Item item, Attribute attribute);

    public AbstractTag getTag(RequestContext context, String value){
        return null;
    }
}
