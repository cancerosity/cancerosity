package org.sevensoft.ecreator.model.items.renderers;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * User: Tanya
 * Date: 01.03.2012
 */
public class TickerMarkupDefault implements MarkupDefault {
    
    public String getBetween() {
        return null;
    }

    public String getBody() {
        StringBuilder sb = new StringBuilder();
        sb.append("    <li>\n" +
                "        [thumbnail?w=60&h=60&link=1&limit=1&class=tickthumb] - [summary] - [item?link=1&text=read more...]\n" +
                "    </li>\n" );
        return sb.toString();
    }

    public String getEnd() {
        return "</ul>";
    }

    public String getStart() {
        return "<ul id=\"ec_ticker\" class=\"ticker\">\n";
    }

    public int getTds() {
        return 0;
    }

    public String getCss() {
        StringBuilder sb = new StringBuilder();
        sb.append(".ticker { width: 500px; height: 40px; overflow: hidden; border: 1px solid #DDD; margin: 0; padding: 0; list-style: none; border-radius: 5px; \tbox-shadow: 0px 0px 5px #DDD; }\n");
        sb.append(".ticker li { height: 30px; border-bottom: 1px dotted #DDD; padding: 5px; margin: 0px 5px; }");
        return sb.toString();
    }

    public String getName() {
        return "Ticker Markup Default";
    }
}
