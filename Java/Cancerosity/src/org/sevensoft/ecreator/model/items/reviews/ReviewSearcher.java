package org.sevensoft.ecreator.model.items.reviews;

import java.util.List;

import org.sevensoft.jeezy.db.QueryBuilder;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 24 Mar 2007 10:06:50
 *
 */
public class ReviewSearcher {

	private final RequestContext	context;
	private boolean			approvedOnly;
	private boolean			unapprovedOnly;
	private String			name;
	private String			author;
	private int	limit;
	private int	start;

	public ReviewSearcher(RequestContext context) {
		this.context = context;
	}

	public List<Review> execute() {

		QueryBuilder b = new QueryBuilder(context);
		b.select("r.*");
		b.from("r", Review.class);

		if (approvedOnly) {
			b.clause("r.approved=1");
		}
		if (unapprovedOnly) {
			b.clause("r.approved=0");
		}

		return b.toQuery().execute(Review.class);
	}

	public final String getAuthor() {
		return author;
	}

	public final String getName() {
		return name;
	}

	public final boolean isApprovedOnly() {
		return approvedOnly;
	}

	public final boolean isUnapprovedOnly() {
		return unapprovedOnly;
	}

	public final void setApprovedOnly(boolean approvedOnly) {
		this.approvedOnly = approvedOnly;
	}

	public final void setAuthor(String author) {
		this.author = author;
	}

	public final void setName(String name) {
		this.name = name;
	}

	public final void setUnapprovedOnly(boolean unapprovedOnly) {
		this.unapprovedOnly = unapprovedOnly;
	}

	public int size() {
		return 0;
	}

	/**
	 * 
	 */
	public void setLimit(int limit) {
		this.limit = limit;
	}

	/**
	 * 
	 */
	public void setStart(int start) {
		this.start = start;
	}
}
