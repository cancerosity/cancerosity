package org.sevensoft.ecreator.model.items.pricing;

import java.util.Collection;

import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.pricing.PriceSettings.PricingType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * NEW 08/10/06 sks
 * 
 * This class represents a how to calculate a price for a given item
 * Prices can be calculated absolutely (a fixed value which is irrelevent of a cost) or a percentage (which is calculated from the cost of the item).
 * 
 * Price definitions can be assigned to an individual item, an individual member, a member group, a category, or across the entire site.
 * 
 * Price definitions always have a price break set, defaulting to 1 if not explicity set. Obviously a price break of 1 means it will apply to all items regardless of qty!
 * 
 * Prices set on the guest group should instead be set on null group.
 * 
 * Price definitions take priority in the following order
 *  
 * Member level definitions
 * Item level definitions
 * Member Group definitions
 * Category definitions
 * Global definitions 
 * 
 * There are several parameters that can qualify the price to be given, and these vary dependant on the type of owner.
 * 
 * Owner type 		| Cost price constraints	| price breaks	| member groups
 * --------------------------------------------------------------------------------------
 * Member			| Yes						| Yes			| No
 * Item				| No						| Yes			| Yes
 * Category		| Yes						| Yes			| Yes
 * Global			| Yes						| Yes			| Yes
 * 
 * 
 * 
 * 
 * TO DO 
 * 
 * Somehow include attributes as part of the pricing definition at the category, member group and global levels
 * 
 * @author sks 04-Aug-2005 12:29:26
 * 
 */
@Table("prices")
public class Price extends EntityObject {

	public static void resetCachedSellPrices(RequestContext context) {
		new Query(context, "update # set genericSellPrice=0").setTable(Item.class).run();
	}

	/**
	 * We can assign this to price settings for global pricing
	 */
	private PriceSettings	priceSettings;

	/**
	 * 
	 */
	private Amount		amount;

	/**
	 * PriceGroup for this price or null for blank 
	 * BC
	 */
	@Index()
	private PriceBand		priceBand;

	/**
	 *  Price break - minimum quantity needed to be purchased for this price definition to be valid
	 */
	@Index()
	private int			qty;

	/**
	 * The owning category for a category price definition
	 */
	@Index()
	private Category		category;

	/**
	 * Individual item pricing
	 */
	@Index()
	private Item		item;

	//	/**
	//	 *  Cost price constraints
	//	 */
	//	private Money		costPriceFrom, costPriceTo;

	/**
	 * Tie this price to a specific account
	 */
	private Item		account;

	public Price(RequestContext context) {
		super(context);
	}

	/**
	 * Individual pricing for this account
	 */
	public Price(RequestContext context, Item item, Item account, int qty, Amount amount) {

		super(context);

		this.qty = qty;
		this.amount = amount;
		this.item = item;
		this.account = account;

		save();
	}

	public Price(RequestContext context, Priceable p, PriceBand priceBand, int qty, Amount amount) {

		super(context);

		this.priceBand = priceBand;
		this.qty = qty;
		this.amount = amount;

		if (p instanceof Item) {
			this.item = (Item) p;
		}

		else if (p instanceof Category) {
			this.category = (Category) p;
		}

		else if (p instanceof PriceSettings) {
			this.priceSettings = (PriceSettings) p;
		}

		save();

		p.clearPriceCaches();
	}

	/**
	 * Return the price for the given cost price
	 * 
	 */
	public Money calculateSellPrice(Money costPrice) {

		PricingType pricingType = PriceSettings.getInstance(context).getPricingType();
		logger.fine("[Price] calculating sell price for cost=" + costPrice + ", pricingType=" + pricingType + ", amount=" + amount);

		if (costPrice == null) {
			costPrice = new Money(0);
		}

		if (amount.isFixed()) {
			return amount.getFixed();
		}

		switch (pricingType) {

		case Margin:
			return amount.calculateMargin(costPrice);

		default:
		case Markup:
			return costPrice.add(amount.calculate(costPrice));
		}
	}

	public Amount getAmount() {
		return amount;
	}

	//	public Money getCostPriceFrom() {
	//		return costPriceFrom;
	//	}
	//
	//	public Money getCostPriceTo() {
	//		return costPriceTo;
	//	}

	public Item getItem() {
		return (Item) (item == null ? null : item.pop());
	}

	public PriceBand getPriceBand() {
		return (PriceBand) (priceBand == null ? null : priceBand.pop());
	}

	public int getQty() {
		return qty;
	}

	/**
	 * Check this price definition is valid for the parameters
	 */
	public boolean isValid(Collection<PriceBand> priceGroups, int q, Money cp) {

		// check we have bought enough qty
		if (qty > q)
			return false;

		// check we are in an appropriate group
		if (priceBand != null && !priceGroups.contains(priceBand)) {
			return false;
		}

		return true;
	}

	/**
	 *  Returns true if this price definition is valid for this item and qty
	 */
	public boolean isValid(int q) {

		//		if (costPriceFrom != null && costPriceFrom.isPositive() && costPrice.isLessThan(costPriceFrom)) {
		//			return false;
		//		}
		//
		//		if (costPriceTo != null && costPriceTo.isPositive() && costPrice.isGreaterThan(costPriceTo)) {
		//			return false;
		//		}

		if (q > 0 && q < qty) {
			return false;
		}

		return true;
	}

}