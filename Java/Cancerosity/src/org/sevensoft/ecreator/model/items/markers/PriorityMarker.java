package org.sevensoft.ecreator.model.items.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
public class PriorityMarker extends MarkerHelper implements IItemMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

        boolean priority = item.isPrioritised();
        return priority ? "_prioritised" : "";
    }

    public String getDescription() {
        return "Use it along with any class name. I.e. if 'table class='items_list[priority]'' for prioritised items you get 'table class='items_list_prioritised'' ";
    }

    public Object getRegex() {
        return "priority";
    }
}

