package org.sevensoft.ecreator.model.items.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.db.EntityObject;

import java.util.Map;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
public class ItemFamilyTableHeaderMarker extends MarkerHelper implements IItemMarker, IGenericMarker {


    public Object generate(RequestContext context, Map<String, String> params) {

        String id = params.get("id");

        Attribute attribute = EntityObject.getInstance(context, Attribute.class, id);
        if (attribute == null) {
            logger.fine("[AttributeValueMarker] no attribute found for id=" + id);
            return null;
        }

        StringBuilder sb = new StringBuilder();

        if (params.containsKey("name")) {

            sb.append("<span class='attribute-name'>");
            sb.append(attribute.getName());
            sb.append("</span>");

        }

        return super.string(context, params, sb.toString());

    }

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
        return generate(context, params);
    }

    public Object getRegex() {
        return "item_family_header";
    }


}
