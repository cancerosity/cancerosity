package org.sevensoft.ecreator.model.items.options;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.comparators.NaturalStringComparator;
import org.sevensoft.ecreator.model.ecom.orders.VatHelper;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLineOption;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 21-Oct-2005 12:16:45
 * 
 */
@Table("items_options")
public class ItemOption extends EntityObject implements Positionable, Selectable {

    public enum PriceType {
		Letter, Line, Phrase, Word;
	}

	public enum Type {
		Selection, Text, YesNo("Yes / No"), Upload, FlashUpload, List;

		private String	toString;

		private Type() {
			this.toString = name();
		}

		private Type(String string) {
			this.toString = string;
		}

		@Override
		public String toString() {
			return toString;
		}
	}

	/**
	 * The name of this option
	 */
	private String		name;

	/**
	 * Does this option have to be selected ?
	 */
	private boolean		optional;

	/**
	 * Overrides the price rather than adding to the price
	 */
	private boolean		priceOverride;

	/**
	 * 
	 */
	private boolean		nameOrder;

	/**
	 * 
	 */
	private int			position, wordLimit, letterLimit;

	/**
	 * The extra charge imposed for selecting this option.
	 */
	private Money		price;

	/**
	 * The item this option is applied to
	 */
	private Item		item;

    /**
     * The itemType this option is applied to
     */
    private ItemType itemType;

    /**
	 * An option template can be applied to many items at once
	 */
	private OptionGroup	optionTemplate;

	/**
	 * 
	 */
	private PriceType		priceMethod;

	/**
	 * Appears in the selection box before you choose an option
	 */
	private String		intro;

	/**
	 * Type of option
	 */
	private Type		type;

	private int			height, width;

	/**
	 * Should we use images for this selection based option ?
	 */
	private boolean		images;

    private boolean     stockOverride;

    private ItemOption parent;

    public ItemOption(RequestContext context) {
		super(context);
	}

	public ItemOption(RequestContext context, Item item, String name) {
		super(context);

		this.name = name;
		this.type = Type.Selection;
		this.optional = true;
		this.item = item;

		save();
	}

    public ItemOption(RequestContext context, ItemType itemType, String name) {
		super(context);

		this.name = name;
		this.type = Type.Selection;
		this.optional = true;
		this.itemType = itemType;

		save();
	}

	public ItemOption(RequestContext context, OptionGroup t, String name) {
		super(context);

		this.optionTemplate = t;
		this.name = name;
		this.type = Type.Selection;
		this.optional = true;

		save();
	}

	public ItemOptionSelection addSelection(String text) {

		if (text == null) {
			return null;
		}

		text = text.trim();

		if (text.length() == 0) {
			return null;
		}

		return new ItemOptionSelection(context, this, text.trim());
	}

    public ItemOptionSelection addSelectionCopy(ItemOptionSelection itemOptionSelection) {
        ItemOptionSelection newSelection = new ItemOptionSelection(context);

        return null;  //To change body of created methods use File | Settings | File Templates.
    }

	private void clearBaskets() {

		// remove all basket lines that have this option and recalc baskets
		Query q = new Query(context, "update # b join # bl on b.id=bl.basket join # blo on bl.id=blo.line set b.recalc=1 where blo.itemOption=?");
		q.setTable(Basket.class);
		q.setTable(BasketLine.class);
		q.setTable(BasketLineOption.class);
		q.setParameter(this);
		q.run();

		q = new Query(context, "delete from # where itemOption=?");
		q.setTable(BasketLineOption.class);
		q.setParameter(this);
		q.run();
	}

	/**
	 * @param target
	 */
	public void copyTo(Item target) {

		ItemOption newOption = new ItemOption(context, target, name);

		newOption.letterLimit = letterLimit;
		newOption.optional = optional;
		newOption.position = position;
		newOption.price = price;
		newOption.priceMethod = priceMethod;
		newOption.type = type;
		newOption.wordLimit = wordLimit;
		newOption.intro = intro;
		newOption.height = height;
		newOption.width = width;
		newOption.nameOrder = nameOrder;
		newOption.images = images;
        newOption.parent = this;
		newOption.save();

		// copy selections
		for (ItemOptionSelection selection : getSelections()) {
			selection.copyTo(newOption);
		}

		target.setOptionCount();
		target.save();
	}

	@Override
	public synchronized boolean delete() {

		removeSelections();
		clearBaskets();

		return super.delete();
	}

	public int getHeight() {
		return (height < 1 || height > 20) ? 1 : height;
	}

	public String getIntro() {
		return intro;
	}

    public Item getItem() {
        return item != null ? (Item) item.pop() : null;
    }

    public ItemType getItemType() {
        return itemType != null ? (ItemType) itemType.pop() : null;
    }

    public int getLetterLimit() {
		return letterLimit;
	}

	public String getName() {
		return name;
	}

	public OptionsModule getOptionsModule() {
		return getItem().getItemType().getOptionsModule();
	}

	public OptionGroup getOptionTemplate() {
		return (OptionGroup) (optionTemplate == null ? null : optionTemplate.pop());
	}

	public int getPosition() {
		return position;
	}

	public Money getPrice() {
		return price;
	}

	public Money getPriceInc() {
		return getPrice().add(getPriceVat());
	}

	public PriceType getPriceMethod() {
		return priceMethod == null ? PriceType.Phrase : priceMethod;
	}

	public Money getPriceVat() {
		return VatHelper.vat(context, price, getItem().getVatRate());
	}

	public ItemOptionSelection getSelectionFromId(int id) {

		for (ItemOptionSelection selection : getSelections()) {

			if (selection.getId() == id) {
				return selection;
			}

		}

		return null;
	}

	public List<ItemOptionSelection> getSelections() {

		switch (getType()) {

		default:
		case Text:
		case YesNo:
			return Collections.emptyList();

		case Selection:
        case List:

			if (nameOrder) {

				List<ItemOptionSelection> selections = SimpleQuery.execute(context, ItemOptionSelection.class, "itemOption", this);
				Collections.sort(selections, new Comparator<ItemOptionSelection>() {

					public int compare(ItemOptionSelection o1, ItemOptionSelection o2) {
						return NaturalStringComparator.instance.compare(o1.getText(), o2.getText());
					}
				});
				return selections;

			} else {

				Query q = new Query(context, "select * from # where itemOption=? order by position");
				q.setParameter(this);
				q.setTable(ItemOptionSelection.class);
				return q.execute(ItemOptionSelection.class);

			}
		}
	}

	public String getSelectionsString() {

		StringBuilder sb = new StringBuilder();
		Iterator<ItemOptionSelection> iter = getSelections().iterator();
		if (!iter.hasNext()) {
			return "[no selections set]";
		}

		while (iter.hasNext()) {
			sb.append(iter.next().getText());
			if (iter.hasNext()) {
				sb.append("; ");
			}
		}
		return sb.toString();
	}

	public Type getType() {
		return type == null ? Type.Selection : type;
	}

	public int getWidth() {
		return (width < 1 || width > 120) ? 20 : width;
	}

	public int getWordLimit() {
		return wordLimit;
	}

    public ItemOption getParent() {
        return parent;
    }

    public boolean hasIntro() {
		return intro != null;
	}

	public boolean hasItem() {
		return item != null;
	}

	public boolean hasPrice() {
		return !price.isZero();
	}

	public boolean isImages() {
		return images && isSelection();
	}

	public boolean isNameOrder() {
		return nameOrder;
	}

	public boolean isOptional() {
		return optional;
	}

	public final boolean isPriceOverride() {
		return priceOverride;
	}

    public boolean isSelection() {
        return getType() == Type.Selection || getType() == Type.List;
    }

    public boolean isStockOverride() {
        return stockOverride;
    }

    public boolean isText() {
		return getType() == Type.Text;
	}

	public void removeSelection(ItemOptionSelection selection) {
		selection.delete();
	}

	private void removeSelections() {

		Query q = new Query(context, "delete from # where itemOption=?");
		q.setParameter(this);
		q.setTable(ItemOptionSelection.class);
		q.run();
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public void setImages(boolean images) {
		this.images = images;
	}

	public void setIntro(String initial) {
		this.intro = initial;
	}

	public void setLetterLimit(int letterLimit) {
		this.letterLimit = letterLimit;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNameOrder(boolean manualOrder) {
		this.nameOrder = manualOrder;
	}

	public void setOptional(boolean optional) {
		this.optional = optional;
	}

	public void setPosition(int position) {
		this.position = position;

	}

	public void setPrice(Money price) {
		this.price = price;
	}

	public void setPriceMethod(PriceType priceMethod) {
		this.priceMethod = priceMethod;
	}

	public final void setPriceOverride(boolean priceOverride) {
		this.priceOverride = priceOverride;
	}

    public void setStockOverride(boolean stockOverride) {
        this.stockOverride = stockOverride;
    }

    public void setType(Type t) {

		if (type == t) {
			return;
		}

		this.type = t;

		clearBaskets();

		if (isText()) {
			removeSelections();
			width = 20;
			height = 1;
		}
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setWordLimit(int wordLimit) {
		this.wordLimit = wordLimit;
	}

    public String getLabel() {
        return getName();
    }

    public String getValue() {
        return getIdString();
    }

    public List<ItemOption> getRelated(RequestContext context) {
        Query q = new Query(context, "select * from # where parent=?");
        q.setTable(ItemOption.class);
        q.setParameter(this);
        return q.execute(ItemOption.class);
    }

    public void savePrice() {
        save("price");
    }
}
