package org.sevensoft.ecreator.model.items.ibex.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.iface.frontend.items.ibex.IbexPopupHandler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

import java.util.Map;

/**
 * User: Tanya
 * Date: 22.03.2011
 */
public class IbexMarker extends MarkerHelper implements IItemMarker {

    public Object getRegex() {
        return "ibex_popup";
    }

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
        if (!Module.IBex.enabled(context)) {
            return null;
        }

        return super.link(context, params, new Link(IbexPopupHandler.class, null, "item", item));
    }
}
