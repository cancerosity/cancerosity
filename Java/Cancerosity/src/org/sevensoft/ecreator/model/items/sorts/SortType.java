package org.sevensoft.ecreator.model.items.sorts;

import java.util.Set;
import java.util.TreeSet;

import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 16 Nov 2006 15:22:32
 *
 */
public enum SortType {

	Name() {

	},
	Newest() {

	},
	Oldest() {

	},
	Random() {

	},
	Price() {

		@Override
		public String toString() {
			return "Selling price";
		}

	},
	Availability() {

	},
	Distance() {

	},
	Attribute() {

	},
	LastActive() {

	},
	Status() {

	},

	Email() {

	},

	Reference(),

	SubscriptionLevel;

	public static Set<SortType> getSortTypes(RequestContext context) {

		Set<SortType> set = new TreeSet();

		set.add(Name);
		set.add(Newest);
		set.add(Oldest);
		set.add(Random);
		set.add(Status);
		set.add(Reference);

		if (Module.Subscriptions.enabled(context)) {
			set.add(SubscriptionLevel);
		}

		if (Module.Pricing.enabled(context)) {
			set.add(Price);
		}

		if (Module.Availabilitity.enabled(context)) {
			set.add(Availability);
		}

		if (Module.Accounts.enabled(context)) {
			set.add(LastActive);
			set.add(Email);
		}

		return set;
	}

}