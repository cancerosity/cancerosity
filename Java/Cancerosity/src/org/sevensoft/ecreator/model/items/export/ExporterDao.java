// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   ExporterDao.java

package org.sevensoft.ecreator.model.items.export;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.List;


public class ExporterDao {

    private RequestContext context;

    public ExporterDao(RequestContext context) {
        this.context = context;
    }

    public List<Exporter> findAll() {
        return SimpleQuery.execute(context, Exporter.class);
    }

    public Exporter load(int id) {
        return EntityObject.getInstance(context, Exporter.class, id);
    }
}
