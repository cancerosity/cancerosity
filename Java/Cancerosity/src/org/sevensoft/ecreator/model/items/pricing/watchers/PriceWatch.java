package org.sevensoft.ecreator.model.items.pricing.watchers;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 18-Sep-2005 00:07:31
 * 
 */
@Table("items_prices_watchers")
public class PriceWatch extends EntityObject {

	private String	email;
	private Item	item;

	public PriceWatch(RequestContext context) {
		super(context);
	}

	public PriceWatch(RequestContext context, Item item, String email) {
		super(context);

		Query q = new Query(context, "select count(*) from # where item=? and email=?");
		q.setTable(PriceWatch.class);
		q.setParameter(item);
		q.setParameter(email);

		if (q.getInt() == 0) {

			this.item = item;
			this.email = email;

			save();
		}
	}

	public String getEmail() {
		return email;
	}

	public Item getItem() {
		return item.pop();
	}

}
