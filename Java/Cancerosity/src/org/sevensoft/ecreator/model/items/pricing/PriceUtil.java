package org.sevensoft.ecreator.model.items.pricing;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.sevensoft.commons.skint.Amount;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 21 Jul 2006 14:03:01
 *
 */
public class PriceUtil {

	private static Logger	logger	= Logger.getLogger("ecreator");

	public static void clearPriceCaches(RequestContext context) {
		// reset ALL cached sell prices
		new Query(context, "update # set genericSellPrice=0").setTable(Item.class).run();
	}

	/**
	 * Returns the exact price definition for this band and qty from the list of prices passed in
	 */
	public static Price getPrice(List<Price> prices, PriceBand priceBand, int qty) {

		for (Price price : prices) {

			if (price.getQty() == qty) {

				if (ObjectUtil.equal(priceBand, price.getPriceBand())) {
					return price;
				}
			}
		}

		return null;
	}

	/**
	 * 
	 */
	public static String getPriceString(Price price) {

		if (price == null) {
			return "-";
		} else {
			return price.getAmount().toString();
		}
	}

	//	public static void removePriceDefinition(RequestContext context, Priceable priceable, PriceBand priceBand, int qty) {
	//
	//		Query q = new Query(context, "delete from # where " + priceable.getClass().getSimpleName() + "=? and priceBand=? and qty=?");
	//
	//		q.setTable(Price.class);
	//		q.setParameter(priceable);
	//		q.setParameter((priceBand == null ? 0 : priceBand.getId()));
	//		q.setParameter(qty);
	//
	//		q.run();
	//
	//	}

	/**
	 * Returns true if we should show the advanced prices panel for this priceable
	 * @param context 
	 */
	public static boolean isAdvancedPricing(Priceable p, RequestContext context) {

		PriceSettings priceSettings = PriceSettings.getInstance(context);

		if (priceSettings.isUsePriceBreaks()) {
			return true;
		}

		if (priceSettings.isUsePriceBands() && PriceBand.get(context).size() > 0) {
			return true;
		}

		return false;
	}

	public static boolean isMemberGroupPricing(boolean memberGroupPricing, RequestContext context) {
		return memberGroupPricing && PriceBand.get(context).size() > 1;
	}

	public static boolean isPriceBreaks(Priceable priceable, RequestContext context) {
		return priceable.getPriceBreaks().size() > 1;
	}

	//	public static void savePrices(RequestContext context, Priceable priceable, String priceBreaksString, String costConstraintsString, int rows,
	//			Map<Integer, PriceBand> memberGroupMap, Map<Integer, Money> costPriceFromMap, Map<Integer, Money> costPriceToMap,
	//			MultiValueMap<Integer, String> priceMap) {
	//
	//		logger.fine("saveprices for priceable=" + priceable);
	//		logger.fine("[priceBreakString] " + priceBreaksString);
	//		logger.fine("[costConstraintsString] " + costConstraintsString);
	//		logger.fine("[memberGroupMap] " + memberGroupMap);
	//		logger.fine("[costPriceFromMap] " + costPriceFromMap);
	//		logger.fine("[costPriceToMap] " + costPriceToMap);
	//		logger.fine("[priceMap] " + priceMap);
	//
	//		List<Money> costConstraints = new ArrayList();
	//		for (String string : StringHelper.explodeStrings(costConstraintsString, "\\s")) {
	//			costConstraints.add(new Money(string));
	//		}
	//		priceable.setCostConstraintBands(costConstraints);
	//		syncCostConstraintBands(context, priceable, costConstraints);
	//
	//		logger.fine("processing " + rows + " rows");
	//		for (int n = 0; n < rows; n++) {
	//
	//			logger.fine("processor price row: " + n);
	//
	//			PriceBand memberGroup = memberGroupMap.get(n);
	//			Money costPriceFrom = costPriceFromMap.get(n);
	//			Money costPriceTo = costPriceToMap.get(n);
	//
	//			if (costPriceTo == null) {
	//				costPriceTo = new Money(0);
	//			}
	//
	//			if (costPriceTo.isLessThan(costPriceFrom)) {
	//
	//				logger.fine("cost price to is less than cost price from! resetting both to zero");
	//				costPriceTo = new Money(0);
	//				costPriceFrom = new Money(0);
	//			}
	//
	//			logger.fine("price row " + n + ": group=" + memberGroup + " costPriceFrom=" + costPriceFrom + ", costPriceTo=" + costPriceTo);
	//
	//			List<String> priceStrings = priceMap.list(n);
	//			logger.fine("prices for row[" + n + "]: " + priceStrings);
	//
	//			if (priceStrings != null) {
	//
	//				//				logger.fine("priceBreaks.size==priceStrings.size - " + priceBreaks.size() + "==" + priceStrings.size());
	//				//				for (int qty : priceBreaks) {
	//				//
	//				//					logger.fine("qty=" + qty);
	//				//					if (priceStrings.isEmpty()) {
	//				//						logger.fine("no more price strings left, exiting loop");
	//				//						break;
	//				//					}
	//				//
	//				//					setSellPrice(context, priceable, memberGroup, qty, priceStrings.remove(0), false, costPriceFrom, costPriceTo);
	//				//				}
	//			}
	//		}
	//
	//	}

	//	public static void setSellPrice(RequestContext context, Priceable pricable, PriceBand group, int qty, String priceString, boolean removeDefinition,
	//			Money costPriceFrom, Money costPriceTo) {
	//
	//		logger.fine("[PriceUtil] setting price for priceable=" + pricable + " to " + priceString);
	//
	//		// remove price def for this group and qty
	//		if (removeDefinition) {
	//			logger.fine("Removing price def");
	//			pricable.removePriceDefinition(group, qty, null, null);
	//		}
	//
	//		// if the price is null then exit
	//		if (priceString == null) {
	//			logger.fine("no price set, exiting");
	//			return;
	//		}
	//
	//		// if priceString is - then just exit, that is our place holder value
	//		if (priceString.equals("-")) {
	//			logger.fine("price is place holder, exiting");
	//			return;
	//		}
	//
	//		if (qty < 1)
	//			qty = 1;
	//
	//		// parse string to get pricing
	//		Amount amount = Amount.parse(priceString);
	//		if (amount == null)
	//			return;
	//
	//		double markup = amount.getPercentage();
	//		Money fixedPrice = amount.getFixed();
	//
	//		logger.fine("[markup] " + markup + " [fixed] " + fixedPrice);
	//
	//		if (markup == 0 && fixedPrice.isZero()) {
	//			logger.fine("definition works out at zero, skipping creation");
	//			return;
	//		}
	//
	//		//		new Price(context, pricable, group, qty, fixedPrice, markup, costPriceFrom, costPriceTo);
	//		//
	//		//		// flag baskets
	//		//		pricable.flagBaskets();
	//	}

	/**
	 * Remove all price definitions for this priceable
	 */
	public static void removePriceDefinitions(RequestContext context, Priceable priceable) {

		Query q = new Query(context, "delete from # where " + priceable.getClass().getSimpleName() + "=?");
		q.setTable(Price.class);
		q.setParameter(priceable);
		q.run();

	}

	public static void savePrices(RequestContext context, Priceable priceable, Map<Integer, String> prices) {

		// remove all existing price definitions as we are going to re-save the entire lot
		priceable.removePrices();

		// get price bands and include null at first position for standard pricing
		final List<PriceBand> priceBands = PriceBand.get(context);
		priceBands.add(0, null);

		logger.fine("[PriceUtil] saving prices=" + prices);

		int n = 0;
		for (PriceBand band : priceBands) {

			logger.fine("[PriceUtil] price band=" + band);

			for (int qty : priceable.getPriceBreaks()) {

				String string = prices.get(n);
				Amount amount = Amount.parse(string);

				logger.fine("[PriceUtil] sell price n=" + n + ", band=" + band + ", qty=" + qty + ", string=" + string);

				if (amount != null) {
					priceable.setSellPrice(band, qty, amount);
				}

				n++;
			}

		}
	}

	//	/**
	//	 * Removes any cost constraints for this priceable that are not in the new list of cost constraints
	//	 */
	//	private static void syncCostConstraintBands(RequestContext context, Priceable priceable, Collection<Money> costConstraintBands) {
	//
	//		if (costConstraintBands == null || costConstraintBands.isEmpty())
	//			return;
	//
	//		StringBuilder sb = new StringBuilder("delete from # where ");
	//		sb.append(priceable.getClass().getSimpleName());
	//		sb.append("=? ");
	//		for (int n = 0; n < costConstraintBands.size(); n++)
	//			sb.append(" and costPriceTo!=? ");
	//
	//		Query q = new Query(context, sb);
	//		q.setTable(Price.class);
	//		q.setParameter(priceable);
	//		for (Money m : costConstraintBands)
	//			q.setParameter(m);
	//	}

	public static void syncPriceBreaks(RequestContext context, Priceable pricable, Collection<Integer> breaks) {

		// remove all prices defined on qtys no longer in our price break list
		StringBuilder sb = new StringBuilder("delete from # where " + pricable.getClass().getSimpleName() + "=? and qty!=1 ");
		for (int n = 0; n < breaks.size(); n++) {
			sb.append(" and qty !=? ");
		}

		Query q = new Query(context, sb.toString());
		q.setTable(Price.class);
		q.setParameter(pricable);
		for (int i : breaks) {
			q.setParameter(i);
		}
		q.run();
	}

}
