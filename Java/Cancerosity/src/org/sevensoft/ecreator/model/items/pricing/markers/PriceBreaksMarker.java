package org.sevensoft.ecreator.model.items.pricing.markers;

import java.util.List;
import java.util.Map;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.pricing.Price;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 28 Nov 2006 17:38:19
 * 
 * Shows the prices for the different price breaks for this item
 *
 */
public class PriceBreaksMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		Item account = (Item) context.getAttribute("account");

        if (!PermissionType.ItemPriceSummary.check(context, account, item.getItemType())) {
            return null;
        }

		Money costPrice = item.getCostPrice();

        if (params.containsKey("price_max")) {
            account = null;
        }
        
		List<Price> prices = item.getApplicablePrices(account, 0);

		/*
		 * If we have prices for this member then generate the table
		 */
		if (prices != null && prices.size() > 0) {

			StringBuilder sb = new StringBuilder();
			sb.append(new TableTag("ec_pricebreaks"));
			sb.append("<tr>");
			for (Price price : prices) {
				sb.append("<th>" + price.getQty() + "+</th>");
			}
			sb.append("</tr><tr>");
			for (Price price : prices) {
				sb.append("<td>" + price.calculateSellPrice(costPrice) + "</td>");
			}
			sb.append("</tr>");
			sb.append("</table>");
			return sb.toString();
		}

		return null;
	}

	public Object getRegex() {
		return "pricing_breaks";
	}

}
