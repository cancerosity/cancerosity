package org.sevensoft.ecreator.model.items.highlighted.blocks.markup;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;
import org.sevensoft.ecreator.model.design.markup.MarkupCssClassDefault;

/**
 * @author sks 12-Jan-2006 15:01:12
 *
 */
public class HighlightedItemsBlockMarkup2 extends MarkupCssClassDefault implements MarkupDefault {

	public String getBetween() {
		return null;
	}

	public String getCss() {

        StringBuilder sb = new StringBuilder();

        sb.append("table.njh_hi_default { margin-top: 10px; width: 100%; font-family: 'Segoe UI', 'Trebuchet MS', " +
                "Arial, Helvetica, sans-serif; font-size: 12px; color: #333333; }\n");
        sb.append("table.njh_hi_default td { padding: 3px; text-align: left; }\n");
        sb.append("table.njh_hi_default tr.row0 td table.item_h { background: #ffffff " +
                "url(../files/graphics/markup/default/gradient1.gif) repeat-x scroll left bottom; }\n");
        sb.append("table.item_h { width: 100%; height: 320px; border: 1px solid #dddddd; margin-bottom: 10px; }\n");
        sb.append("table.item_h td { padding: 4px; }\n");
        sb.append("table.item_h td.image { text-align: center; height: 168px; }\n");
        sb.append("table.item_h td.title { padding: 0 4px; font-size: 13px; font-weight: bold; color: #cc0000; text-align: left; }\n");
        sb.append("div.summary_h { margin-top: 10px; margin-bottom: 24px; }\n");
        sb.append("table.item_h div.pricing { margin-top: -5px; text-align: right; font-weight: bold; color: #4d9dcb; }\n");
        sb.append("table.item_h div.pricing span.inc { padding-left: 10px; }\n");
        sb.append("table.item_h div.pricing span.price_inc { font-size: 20px; color: #cc0000; }\n");
        sb.append("table.item_h div.pricing span.rrp { display: block; font-weight: normal; }\n");
        sb.append("table.item_h span.stock { display: block; float: left; text-align: left; font-weight: bold; " +
                "background: transparent url(../files/graphics/markup/default/stock.gif) no-repeat scroll left center; " +
                "padding: 3px 0px 2px 25px; width: 75px; margin-top: 5px; color: #555555; }");        

        return sb.toString();
	}

	public String getBody() {

		StringBuilder sb = new StringBuilder();

		sb.append("<table class='item_h' cellspacing='0' cellpadding='0'><tr><td " +
                "class='image'>[image?w=160&h=160&link=1&limit=1]</td></tr>\n");
        sb.append("<tr><td class='title'>[item?link=1]</td></tr>\n");
        sb.append("<tr><td valign='bottom'>\n");
        sb.append("<div class='summary_h'>[summary?max=100]</div>\n");
        sb.append("<div class='pricing'>[stock?text=true]<!-- [pricing_sell?prefix=ex " +
                "VAT: &suffix=<span class='inc'>inc VAT: </span>] -->[pricing_sell?inc=1][pricing_rrp?prefix=<span " +
                "class='discount'>RRP: ] [pricing_rrp_discount?prefix=- you save &suffix=!</span>]</div>\n");
        sb.append("<div class='basket'>[item?link=1&src=./files/graphics/markup/default/more_info.gif] " +
                "[ordering_buy?src=./files/graphics/markup/default/add.gif]</div>\n");
        sb.append("<!-- <div class='distance'>[distance?suffix= miles<br />from ][location]</div> -->\n");
        sb.append("</td></tr></table>");

        return sb.toString();
	}

	public String getEnd() {
		return null;
	}

	public String getName() {
		return "Highlighted items block style 2";
	}

	public String getStart() {
		return null;
	}

	public int getTds() {
		return 3;
	}

     public String getCssClass() {
        StringBuilder sb = new StringBuilder();
        sb.append("njh_hi_default");
        return sb.toString();
    }

    public int getCycleRows() {
        return 0;
    }
}
