package org.sevensoft.ecreator.model.items.stock;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 22 Dec 2006 15:25:35
 * 
 * Stock system
 *
 */
@Table("stock_module")
public class StockModule extends EntityObject {

	public enum StockCalculationSystem {

		/**
		 * Ignore stock from suppliers
		 */
		Internal,

		/**
		 * Takes the highest stock level from between us and our suppliers.
		 */
		Highest,

		/**
		 * Takes an average of stock from all suppliers.
		 */
		Average,

		/**
		 * Adds all stock from all suppliers to our own stock
		 */
		Cumulative
	}

	public enum StockControl {

		None,

		/**
		 * simple enter an availability string
		 */
		String,

		/**
		 * Yes or no
		 */
		Boolean,

		/**
		 * Manually updated stock levels
		 */
		Manual,

		/**
		 * Real time updated stock levels
		 */
		RealTime;
	}

	public static StockModule get(RequestContext context, ItemType itemType) {
		StockModule module = SimpleQuery.get(context, StockModule.class, "itemType", itemType);
		return module == null ? new StockModule(context, itemType) : module;
	}

	private String				outStockIcon, inStockIcon;

	private ItemType				itemType;

	private String				inStockMsgDefault;

	private String				inStockMsgOverride;

	/**
	 * The emails to be notified when stock reaches notify level
	 */
	private List<String>			stockNotifyEmails;

	private StockControl			stockControl;

	private List<String>			stockDisplayRanges;

	private StockCalculationSystem	stockSystem;

	/*
	 *Allows member to set backorders on each item;
	 */
	private boolean				backordersOverride;

	/**
	 * Override the availability message by setting one here
	 */
	private String				outStockMsgOverride;

	/*
	 * 
	 */
	private String				outStockMsgDefault;

	/*
	 * Default backorders. Any items created will use this value for their backorders value
	 */
	private boolean				defaultBackorders;

	private boolean	buyQtyRestriction;

	private StockModule(RequestContext context) {
		super(context);
	}

	public StockModule(RequestContext context, ItemType itemType) {
		super(context);
		this.itemType = itemType;
		this.defaultBackorders = true;
		this.outStockMsgDefault = "Out of stock";
		this.inStockMsgDefault = "In stock";
		save();
	}

	public final String getInStockIcon() {
		return inStockIcon;
	}

	public String getInStockIconPath() {
		return Config.TemplateDataPath + "/" + inStockIcon;
	}

	public final String getInStockMsgDefault() {
		return inStockMsgDefault == null ? "[stock] in stock" : inStockMsgDefault;
	}

	public final String getInStockMsgOverride() {
		return inStockMsgOverride;
	}

	public final ItemType getItemType() {
		return itemType.pop();
	}

	public final String getOutStockIcon() {
		return outStockIcon;
	}

	public String getOutStockIconPath() {
		return Config.TemplateDataPath + "/" + outStockIcon;
	}

	public String getOutStockMsgDefault() {
		return outStockMsgDefault == null ? "Out of stock" : outStockMsgDefault;
	}

	public String getOutStockMsgOverride() {
		return outStockMsgOverride;
	}

	public final StockControl getStockControl() {

		if (!Module.Availabilitity.enabled(context)) {
			return StockControl.None;
		}

		if (!ItemModule.Stock.enabled(context, getItemType())) {
			return StockControl.None;
		}

		return stockControl == null ? StockControl.None : stockControl;
	}

	public List<String> getStockDisplayRanges() {
		if (stockDisplayRanges == null) {
			stockDisplayRanges = new ArrayList();
		}
		return stockDisplayRanges;
	}

	public String getStockDisplayRangesString() {
		return StringHelper.implode(getStockDisplayRanges(), ", ", true);
	}

	public final List<String> getStockNotifyEmails() {
		if (stockNotifyEmails == null) {
			stockNotifyEmails = new ArrayList();
		}
		return stockNotifyEmails;
	}

	public StockCalculationSystem getStockSystem() {
		return stockSystem == null ? StockCalculationSystem.Highest : stockSystem;
	}

	public boolean hasInStockIcon() {
		return inStockIcon != null;
	}

	public boolean hasOutStockIcon() {
		return outStockIcon != null;
	}

	public boolean hasOutStockMsgOverride() {
		return outStockMsgOverride != null;
	}

	public boolean hasStockDisplayRanges() {
		return getStockDisplayRanges().size() > 0;
	}

	public boolean hasStockNotifyEmails() {
		return getStockNotifyEmails().size() > 0;
	}

	public final boolean isBackordersOverride() {
		return backordersOverride;
	}

	public boolean isBuyQtyRestriction() {
		return buyQtyRestriction;
	}

	public final boolean isDefaultBackorders() {
		return defaultBackorders;
	}

	public boolean isStockControl() {
		return getStockControl() != StockControl.None && ItemModule.Stock.enabled(context, itemType);
	}

	/**
	 * Returns true if an item type uses a stock control method that uses individual stock levels
	 */
	public boolean isStockLevels() {

		switch (getStockControl()) {

		case Manual:
		case RealTime:
			return true;

		case Boolean:
		case String:
		case None:
			return false;
		}

		assert false;
		return false;
	}

	/**
	 * Sets the backorders field of all items of this item type to the boolean parameter. 
	 */
	public void resetBackorders(boolean backorders) {

		Query q = new Query(context, "update # set backorders=? where itemType=?");
		q.setTable(Item.class);
		q.setParameter(backorders);
		q.setParameter(getItemType());
		q.run();
	}

	/**
	 * Sets the leadtime of all items of this item type to the string parameter. 
	 */
	public void resetLeadtime(String leadtime) {

		Query q = new Query(context, "update # set leadtime=? where itemType=?");
		q.setTable(Item.class);
		q.setParameter(leadtime);
		q.setParameter(getItemType());
		q.run();
	}

	/**
	 * Sets all stock to the int parameter for all items of this item type
	 */
	public void resetStock(int resetStock) {

		switch (getStockControl()) {

		default:
		case None:
		case String:
		case Boolean:
			break;

		case Manual:
		case RealTime:

			Query q = new Query(context, "update # set ourStock=?, availableStock=? where itemType=?");
			q.setTable(Item.class);
			q.setParameter(resetStock);
			q.setParameter(resetStock);
			q.setParameter(getItemType());
			q.run();

		}

	}

	public final void setBackordersOverride(boolean backordersOverride) {
		this.backordersOverride = backordersOverride;
	}

	public final void setDefaultBackorders(boolean defaultBackorders) {
		this.defaultBackorders = defaultBackorders;
	}

	public final void setInStockIcon(String inStockIcon) {
		this.inStockIcon = inStockIcon;
	}

	public final void setInStockMsgDefault(String inStockMsgDefault) {
		this.inStockMsgDefault = inStockMsgDefault;
	}

	public final void setInStockMsgOverride(String inStockMsgOverride) {
		this.inStockMsgOverride = inStockMsgOverride;
	}

	public final void setOutStockIcon(String outStockIcon) {
		this.outStockIcon = outStockIcon;
	}

	public void setOutStockMsgDefault(String s) {
		this.outStockMsgDefault = s;
	}

	public void setOutStockMsgOverride(String s) {
		this.outStockMsgOverride = s;
	}

	public final void setStockControl(StockControl stockSystem) {
		this.stockControl = stockSystem;
	}

	public void setStockDisplayRanges(String ranges) {

		getStockDisplayRanges().clear();
		if (ranges == null)
			return;

		for (String range : ranges.split("[^\\d-]")) {

			range = range.trim();
			if (range.length() > 0) {
				getStockDisplayRanges().add(range);
			}
		}
	}

	public final void setStockNotifyEmails(List<String> notifyEmails) {
		this.stockNotifyEmails = notifyEmails;
	}

	public void setStockSystem(StockCalculationSystem stockSystem) {
		this.stockSystem = stockSystem;
	}

	
	public final void setBuyQtyRestriction(boolean buyQtyRestriction) {
		this.buyQtyRestriction = buyQtyRestriction;
	}
}
