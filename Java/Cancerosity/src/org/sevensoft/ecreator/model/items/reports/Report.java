package org.sevensoft.ecreator.model.items.reports;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeValue;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 9 Nov 2006 10:07:10
 *
 * Need 3 fields for a bar chart
 * 
 * 1. X axis - any groupable field
 * 2. Y Axis - numerical field for sum
 * 3. Range - must be date in order to get a range. Will default to a date created
 * 
 * Then optional values to filter down by
 *
 */
@Table("items_reports")
public class Report extends EntityObject {

	/**
	 * 
	 */
	public static List<Report> get(RequestContext context) {
		return SimpleQuery.execute(context, Report.class);
	}

	/**
	 * Item type to run report on
	 */
	private ItemType	itemType;

	/**
	 * 
	 */
	private Attribute	x;

	/**
	 * 
	 */
	private Attribute	y;

	/**
	 * Constraints on date - if not set then defaults to date created
	 */
	private Attribute	dateRange;

	private String	name;

	private String	yLabel;

	private String	xLabel;

	private Sort	sort;

	private Report(RequestContext context) {
		super(context);
	}

	public Report(RequestContext context, ItemType itemType, String string) {
		super(context);
		this.itemType = itemType;
		name = string;
		save();
	}

	public Map getDataSet(Date month) {

		if (dateRange == null) {

			String string = "select av1.value x , sum(av2.value) y from # av1 join # av2 on av1.item=av2.item join # i on av1.item=i.id "
					+ "where av1.attribute=? and av2.attribute=? and i.dateCreated>=? and i.dateCreated<? group by av1.value";

			switch (getSort()) {

			default:
			case XAsc:
				string = string + " order by x asc ";
				break;

			case XDesc:
				string = string + " order by x desc ";
				break;

			case YAsc:
				string = string + " order by y asc ";
				break;

			case YDesc:
				string = string + " order by y desc ";
				break;
			}

			Query q = new Query(context, string);
			q.setTable(AttributeValue.class);
			q.setTable(AttributeValue.class);
			q.setTable(Item.class);
			q.setParameter(x);
			q.setParameter(y);
			q.setParameter(month.beginMonth());
			q.setParameter(month.endMonth());

			return q.getMap(String.class, Double.class, new LinkedHashMap());

		} else {

			String string = "select av1.value x, sum(av2.value) y from # av1 join # av2 on av1.item=av2.item join # av3 on av1.item=av3.item "
					+ "where av1.attribute=? and av2.attribute=? and av3.attribute=? and av3.value>=? and av3.value<? group by av1.value";

			switch (getSort()) {

			default:
			case XAsc:
				string = string + " order by x asc ";
				break;

			case XDesc:
				string = string + " order by x desc ";
				break;

			case YAsc:
				string = string + " order by y asc ";
				break;

			case YDesc:
				string = string + " order by y desc ";
				break;
			}

			Query q = new Query(context, string);
			q.setTable(AttributeValue.class);
			q.setTable(AttributeValue.class);
			q.setTable(AttributeValue.class);
			q.setParameter(x);
			q.setParameter(y);
			q.setParameter(dateRange);
			q.setParameter(month.beginMonth());
			q.setParameter(month.endMonth());

			return q.getMap(String.class, Double.class, new LinkedHashMap());

		}
	}

	public Attribute getDateRange() {
		return (Attribute) (dateRange == null ? null : dateRange.pop());
	}

	public ItemType getItemType() {
		return itemType.pop();
	}

	public String getName() {
		return name;
	}

	/**
	 * 
	 */
	public Sort getSort() {
		return sort == null ? Sort.XAsc : sort;
	}

	public Attribute getX() {
		return (Attribute) (x == null ? null : x.pop());
	}

	/**
	 * 
	 */
	public String getXLabel() {

		if (xLabel != null)
			return xLabel;

		if (x != null)
			return getX().getName();

		return null;
	}

	public Attribute getY() {
		return (Attribute) (y == null ? null : y.pop());
	}

	/**
	 * 
	 */
	public String getYLabel() {

		if (yLabel != null)
			return yLabel;

		if (y != null)
			return getY().getName();

		return null;

	}

	public void setDateRange(Attribute dateRange) {
		this.dateRange = dateRange;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSort(Sort sort) {
		this.sort = sort;
	}

	public void setX(Attribute x) {
		this.x = x;
	}

	public void setXLabel(String label) {
		xLabel = label;
	}

	public void setY(Attribute y) {
		this.y = y;
	}

	public void setYLabel(String label) {
		yLabel = label;
	}
}
