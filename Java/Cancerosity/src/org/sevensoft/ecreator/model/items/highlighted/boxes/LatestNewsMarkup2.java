package org.sevensoft.ecreator.model.items.highlighted.boxes;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * User: Tanya
 * Date: 24.05.2011
 */
public class LatestNewsMarkup2 implements MarkupDefault {

    public String getBetween() {
        return null;
    }

    public String getCss() {

        StringBuilder sb = new StringBuilder();
        sb.append("/* Scroller Box */\n" +
                "#scroller_container {\n" +
                " width: 150px;\n" +
                " height: 200px;\n" +
                " overflow: hidden;\n" +
                "}\n" +
                "/* Scroller Box */\n" +
                "\n" +
                "/* CSS Hack Safari */\n" +
                "#dummy {;# }\n" +
                "\n" +
                "#scroller_container {\n" +
                " overflow: auto;\n" +
                "}");
        return sb.toString();
    }

    public String getBody() {
        StringBuilder sb = new StringBuilder();

        sb.append("<div class='headline'>[item?link=1]</div>");
        sb.append("<div class='date'>[date_created]</div>");
        sb.append("<div class='snippet'>[summary?max=150]</div>");

        return sb.toString();
    }

    public String getEnd() {
        StringBuilder sb = new StringBuilder();
        sb.append("</div>\n" +
                "</div>");
        return sb.toString();
    }

    public String getName() {
        return "Auto scroller";
    }

    public String getStart() {

        StringBuilder sb = new StringBuilder();

        sb.append("<div id=\"scroller_container\">\n" +
                " <div class=\"jscroller2_up\">");

        return sb.toString();
    }

    public int getTds() {
        return 0;
    }

}
