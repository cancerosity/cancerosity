package org.sevensoft.ecreator.model.items.pricing;

import java.util.List;
import java.util.SortedSet;

import org.sevensoft.commons.skint.Amount;

/**
 * @author sks 21 Jul 2006 14:06:16
 * 
 * This interface is to be implemented by any object that can own price definitions
 *
 */
public interface Priceable {

	/**
	 * 
	 */
	public void clearPriceCaches();

	//	public void removePrice(PriceBand priceBand, int qty);

	/**
	 * Returns the price breaks defined on this priceable
	 * Always returns a set containing at least the integer 1
	 */
	public SortedSet<Integer> getPriceBreaks();

	/**
	 * Returns all price definitions set on this priceable
	 */
	public List<Price> getPrices();

	/**
	 * Returns true if this priceable is using price breaks
	 */
	public boolean isPriceBreaks();

	/**
	 * Remove all price definitions
	 */
	public void removePrices();

	public void save();

	/**
	 * Set a price definition for this instance for the price band and qty
	 */
	public void setSellPrice(PriceBand priceBand, int qty, Amount amount);

}
