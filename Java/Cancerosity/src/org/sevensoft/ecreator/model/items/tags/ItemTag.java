package org.sevensoft.ecreator.model.items.tags;

import org.sevensoft.ecreator.iface.admin.items.search.ItemSearchAjaxHandler;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 24 Apr 2007 07:32:15
 * 
 * Used for searching and entering an item
 */
public class ItemTag {

	private RequestContext	context;
	private String		name;
	private Item		item;
	private ItemType		itemType;
	private String		value;
	private int			id;
	private int			size;

	public ItemTag(RequestContext context, String name, Item item, int size) {
		this.context = context;
		this.name = name;
		this.item = item;
		this.size = size;
		if (item == null) {
			value = "";
			id = 0;
		} else {
			value = item.getName();
			id = item.getId();
		}
	}

	public final void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	@Override
	public String toString() {

		Link link = new Link(ItemSearchAjaxHandler.class, "csv", "itemType", itemType);

		String clearId = name + "Clear";
		String searchId = name + "Search";
		String textId = name + "Text";
		String selectId = name + "Select";
		String hiddenId = name + "Hidden";

		StringBuilder sb = new StringBuilder();
		sb.append("<script>");

		sb.append("	function search() {");

		sb.append("	var clear = document.getElementById('" + clearId + "'); ");
		sb.append("	var search = document.getElementById('" + searchId + "'); ");
		sb.append("	var text = document.getElementById('" + textId + "'); ");
		sb.append("	var select = document.getElementById('" + selectId + "'); ");
		sb.append("	var h = document.getElementById('" + hiddenId + "'); ");

		sb.append("	text.style.display = 'none';	");
		sb.append("	select.style.display = '';	");
		sb.append("	clear.style.display = '';	");
		sb.append("	search.style.display = 'none';	");
		sb.append("	if (h != null) { h.name = ''; } ");

		sb.append("	var url = '" + link + "&name=' + text.value; ");

		sb.append("	ajax(url, searchCallback); ");
		sb.append("}");

		sb.append("	function searchCallback() { ");
		sb.append("		if (ajaxRequest.readyState == 4) { ");
		sb.append("			var select = document.getElementById('" + selectId + "'); ");
		sb.append("			select.options.length = 0;	");
		sb.append("			var records =  ajaxRequest.responseText.split('\\n');  ");
		sb.append("			for (n=0; n < records.length; n++) { ");
		sb.append("				var fields = records[n].split(','); ");
		sb.append("				select.options[n] = new Option(fields[1], fields[0]);	");
		sb.append("			}");
		sb.append("		}");
		sb.append("	}");

		sb.append("	function clearSearch() {");

		sb.append("		var clear = document.getElementById('" + clearId + "'); ");
		sb.append("		var search = document.getElementById('" + searchId + "'); ");
		sb.append("		var text = document.getElementById('" + textId + "'); ");
		sb.append("		var select = document.getElementById('" + selectId + "'); ");
		sb.append("		var h = document.getElementById('" + hiddenId + "'); ");

		sb.append("		text.style.display = '';	");
		sb.append("		select.style.display = 'none';	");
		sb.append("		clear.style.display = 'none';	");
		sb.append("		search.style.display = '';	");

		sb.append("		if (h != null) { h.name = '" + name + "'; } ");
		sb.append("		text.value = '" + value + "'; ");

		sb.append(" 	} ");

		sb.append("</script> ");

		HiddenTag hiddenTag = new HiddenTag(name, item);
		hiddenTag.setId(hiddenId);
		sb.append(hiddenTag);

		TextTag textTag = new TextTag(context, "ad", value, size);
		textTag.setId(textId);
		sb.append(textTag);

		sb.append(" ");

		SelectTag selectTag = new SelectTag(context, name);
		selectTag.setStyle("display: none;");
		selectTag.setId(selectId);
		sb.append(selectTag);

		sb.append(" ");

		ButtonTag search = new ButtonTag("Search");
		search.setOnClick("search();");
		search.setId(searchId);
		sb.append(search);

		sb.append(" ");

		ButtonTag clear = new ButtonTag("Reset");
		clear.setOnClick("clearSearch();");
		clear.setId(clearId);
		clear.setStyle("display: none;");

		sb.append(clear);

		return sb.toString();
	}
}
