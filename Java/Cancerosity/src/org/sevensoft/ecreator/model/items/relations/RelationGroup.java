package org.sevensoft.ecreator.model.items.relations;

import java.util.List;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Dec 2006 14:53:43
 *
 */
@Table("relations_groups")
public class RelationGroup extends EntityObject {

	public static List<RelationGroup> get(RequestContext context) {
		return SimpleQuery.execute(context, RelationGroup.class);
	}

	/**
	 * If reciprocal relations added one way are auto added in reverse 
	 */
	private boolean	reciprocal;

	/**
	 * Name of this group
	 */
	private String	name;

	/**
	 * Item type owner
	 */
	private ItemType	itemType;

	protected RelationGroup(RequestContext context) {
		super(context);
	}

	public RelationGroup(RequestContext context, ItemType itemType, String name) {
		super(context);

		this.itemType = itemType;
		this.name = name;

		save();
	}

	public void addRelation(Item item, Item relation) {
		new Relation(context, this, item, relation);
		if (isReciprocal()) {
			new Relation(context, this, relation, item);
		}
	}

	public final ItemType getItemType() {
		return itemType.pop();
	}

	public final String getName() {
		return name;
	}

	/**
	 * Returns all items associated with this item
	 */
	public List<Item> getRelations(Item item) {

		Query q = new Query(context, "select i.* from # i join # r on i.id=r.relation where r.relationGroup=? and r.item=?");

		q.setTable(Item.class);
		q.setTable(Relation.class);
		q.setParameter(this);
		q.setParameter(item);

		return q.execute(Item.class);
	}

	public final boolean isReciprocal() {
		return reciprocal;
	}

	public void removeRelation(Item item, Item relation) {

		removeRelation(item, relation, 1);
		if (reciprocal) {
			removeRelation(relation, item, 1);
		}
	}

	private void removeRelation(Item item, Item relation, int i) {

		Query q = new Query(context, "delete from # where relationGroup=? and item=? and relation=?");
		q.setTable(Relation.class);
		q.setParameter(this);
		q.setParameter(item);
		q.setParameter(relation);
		q.run();
	}

	public final void setName(String name) {
		this.name = name;
	}

	public final void setReciprocal(boolean b) {
		this.reciprocal = b;
	}

}
