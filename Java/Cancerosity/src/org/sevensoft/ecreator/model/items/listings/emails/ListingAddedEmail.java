package org.sevensoft.ecreator.model.items.listings.emails;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;

import java.util.logging.Logger;

/**
 * User: Tanya
 * Date: 26.11.2010
 */
public class ListingAddedEmail extends Email implements Runnable {

    protected static Logger logger = Logger.getLogger("email");

    private RequestContext context;
    private ItemType itemType;
    private Company company;
    private Config config;

    public ListingAddedEmail(RequestContext context, Item item, Item account) {
        this(context);
        this.itemType = item.getItemType();

        setTo(account.getEmail());
        setSubject(item.getItemTypeNameLower() + " added");
        setBody(item.emailTagsParse(itemType.getAddListingEmailContent()));
        setFrom(config.getServerEmail());

    }

    public ListingAddedEmail(RequestContext context) {
        this.context = context;
        this.company = Company.getInstance(context);
        this.config = Config.getInstance(context);
    }

    public void run() {
        try {
            new EmailDecorator(context, this).send(config.getSmtpHostname());
        } catch (EmailAddressException e) {
            logger.warning("[ListingAddedEmail] " + e);
        } catch (SmtpServerException e) {
            logger.warning("[ListingAddedEmail] " + e);
        }
    }

}
