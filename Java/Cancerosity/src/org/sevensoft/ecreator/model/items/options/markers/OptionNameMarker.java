package org.sevensoft.ecreator.model.items.options.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 26 Jul 2006 11:59:08
 *
 */
public class OptionNameMarker implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		if (!ItemModule.Options.enabled(context, item))
			return null;

		String id = params.get("id");
		if (id == null)
			return null;

		// get option
		ItemOption option = item.getOptionSet().getOption(Integer.parseInt(id.trim()));
		if (option == null)
			return null;

		StringBuilder sb = new StringBuilder();
		sb.append("<span class='item-opt-name'>");
		sb.append(option.getName());
		sb.append("</span>");

		return sb.toString();
	}

	public Object getRegex() {
		return "option_name";
	}

}
