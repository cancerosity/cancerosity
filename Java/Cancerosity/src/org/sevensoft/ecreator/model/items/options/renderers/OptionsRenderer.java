package org.sevensoft.ecreator.model.items.options.renderers;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;

import org.sevensoft.ecreator.iface.EcreatorRenderer;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 20-Mar-2006 14:00:21
 *
 */
public class OptionsRenderer extends EcreatorRenderer {

	private static final Logger		logger	= Logger.getLogger("ecreator");

	private final Item			item;
	private final List<ItemOption>	options;
    private List<Integer>				hiddenOptions;

	public OptionsRenderer(RequestContext context, Item item, List<ItemOption> options) {
		super(context);
		logger.fine("[OptionsMarker] created item=" + item + ", options=" + options);
		this.item = item;
		this.options = options;
        this.hiddenOptions = new ArrayList();
		for (ItemOption option : options) {
			if (option.isSelection()) {
				for (ItemOptionSelection selection : option.getSelections()) {
					for (Integer id : selection.getRevealIds()) {
						this.hiddenOptions.add(id);
					}
				}
			}
		}
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(new TableTag("options"));

		for (ItemOption option : options) {

			logger.fine("[OptionsRenderer] rendering option=" + option);

			sb.append("<tr  ");

            if (hiddenOptions.contains(option.getId())) {
                sb.append(" style=\"display: none\"");
            }

            sb.append(" id=\"" + getInputTrId(option) + "\"");

            sb.append(">");



			final String paramName = "options_" + option.getIdString();

			sb.append("<td class='label' valign='top'>");
			sb.append(option.getName());
			sb.append("</td><td class='input'>");

			sb.append(new OptionInputRenderer(context, option));

			sb.append(new ErrorTag(context, paramName, "<br/>"));
			sb.append("</td></tr>");
		}

		sb.append("</table>");

		return sb.toString();
	}

    public static String getInputName(int id) {
		return "options_" + id;
	}

    public static String getInputTrId(int id) {
		return "inputTr" + id;
	}

	public static String getInputTrId(ItemOption option) {
		return getInputTrId(option.getId());
	}
}
