package org.sevensoft.ecreator.model.items.alternatives;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 23-Mar-2006 07:17:38
 *
 */
public class AltBlockMarkupDefault implements MarkupDefault {

	public String getBetween() {
		return null;
	}

	public String getCss() {
		return null;
	}

	public String getBody() {

		StringBuilder sb = new StringBuilder();
		sb.append(new TableTag("alternative"));

		sb.append("\n\n<tr><td class='image' align='center' valign='middle' width='90' height='90'>[thumbnail?w=90&h=90&limit=1&link=1]</td></tr>\n");
		sb.append("<tr><td class='details' align='center' valign='top'>\n");
		sb.append("[item?link=1]<br/>\n");
		sb.append("[pricing_sell?inc=1&prefix=Our price: ]<br/>\n");
		sb.append("[stock?label=Availability: ]<br/>\n");
		sb.append("</td></tr>\n\n");

		sb.append("</table>");

		return sb.toString();
	}

	public String getEnd() {
		return null;
	}

	public String getName() {
		return "Default alternatives markup";
	}

	public String getStart() {
		StringBuilder sb = new StringBuilder();

		sb.append("<style>\n");
		sb.append("table.alternative div.name { font-size: 12px; font-family: Tahoma; font-weight: bold; color: #333333; margin: 3px;  } \n");
		sb.append("table.alternative div.name a { color: #333333; text-decoration: underline; } \n");
		sb.append("table.alternative div.name a:hover { color: #666666; text-decoration: none;  } \n");
		sb.append("table.alternative div.price { font-size: 14px; font-family: Tahoma; font-weight: bold; color: #cc0000; } \n");
		sb.append("</style>\n");

		return sb.toString();
	}

	public int getTds() {
		return 4;
	}
}
