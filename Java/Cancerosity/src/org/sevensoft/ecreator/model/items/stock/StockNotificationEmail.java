package org.sevensoft.ecreator.model.items.stock;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 28 Dec 2006 13:10:46
 *
 */
public class StockNotificationEmail extends Email {
    private RequestContext context;
    private transient Config config;
	public StockNotificationEmail(RequestContext context, Item item) {
        this.context = context;
		config = Config.getInstance(context);

		StockModule module = item.getItemType().getStockModule();

		super.setFrom(config.getServerEmail());
		super.setRecipients(module.getStockNotifyEmails());
		super.setSubject("Stock notification");
		super.setBody("You asked to be notified when stock dropped below " + item.getStockNotifyLevel() + " on '" + item.getName()
				+ "'.\n\nThe current stock stands at: " + item.getOurStock() + "\n\nClick here to view the item:\n" + item.getEditUrl());
	}

    @Override
	public void send(String hostname) throws EmailAddressException, SmtpServerException {
		new EmailDecorator(context, this).send(hostname);
	}
}
