package org.sevensoft.ecreator.model.items.highlighted.blocks;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.ecreator.iface.admin.items.highighted.block.HighlightedItemsBlockHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.panels.OrderingForm;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.highlighted.blocks.markup.HighlightedItemsBlockMarkup1;
import org.sevensoft.ecreator.model.items.highlighted.blocks.markup.HighlightedItemsBlockMarkup2;
import org.sevensoft.ecreator.model.media.images.ImageUtil;
import org.sevensoft.ecreator.model.misc.CacheContainer;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.ecreator.model.search.SearchOwner;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 26 Jun 2006 16:58:18
 *
 */
@Table("blocks_highlighted_items")
@Label("Highlighted items")
@HandlerClass(HighlightedItemsBlockHandler.class)
public class HighlightedItemsBlock extends Block implements SearchOwner {

	public static Map<Integer, CacheContainer>	htmlCache;

	static {
		htmlCache = new HashMap();
	}

	/**
	 * 
	 */
	public static void clearHtmlCache() {
		htmlCache.clear();
	}
	
	/**
	 * Markup used to render block
	 */
	private Markup	markup;

	/**
	 * Displays a div caption above the block
	 */
	private String	caption;

	protected HighlightedItemsBlock(RequestContext context) {
		super(context);
	}

	public HighlightedItemsBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, 0);
	}

	public Markup getMarkup() {

        HighlightedItemsBlockMarkup2 highlightedItemsBlockMarkup = new HighlightedItemsBlockMarkup2();

        if (markup == null) {
			markup = new Markup(context, getDefaultName(), new HighlightedItemsBlockMarkup2());
            markup.setTableClass(highlightedItemsBlockMarkup.getCssClass());
            markup.save();
            save();
		}

		return markup.pop();
	}

	public Search getSearch() {

		Search search = SimpleQuery.get(context, Search.class, "owner", getFullId());
		if (search == null) {

			search = new Search(context, this);
			search.setLimit(5);
			search.save();
		}

		return search;
	}

	@Override
	public Map<Class, String> getStyles() {

		Map<Class, String> map = new LinkedHashMap<Class, String>();
		map.put(HighlightedItemsBlockMarkup1.class, "Style 1");
		map.put(HighlightedItemsBlockMarkup2.class, "Style 2");

		return map;
	}

	public Object render(final RequestContext context) {

        Config config = Config.getInstance(context);

		// check html cache first
        if (!this.context.getParamNames().contains("chgcur") && config.isHiglightedItemsCache()) {
            if (htmlCache.containsKey(getId())) {
                CacheContainer container = htmlCache.get(getId());
                if (container.isExpired()) {
                    htmlCache.remove(getId());
                }
                else {
                    return container.getCachedContent();
                }
            }
        }

		ItemSearcher searcher = getSearch().getSearcher();
		searcher.setStatus("live");
		searcher.setVisibleOnly(true);

		// remove who we are logged in as
		searcher.setExcludeAccount(true);
		searcher.setAccount((Item) context.getAttribute("account"));

        if (getSearch().isInStockOnly()) {
            searcher.setStockOnly(true);
        }

        List<Item> items = searcher.getItems();
		if (items.isEmpty()) {
			return null;
		}

		// prepopulate
		ImageUtil.prepopulate(context, items);
//		AttributeUtil.prepopulate(context, items);

		logger.fine("[HighlightedBlock] items=" + items);

		final Item active = (Item) context.getAttribute("account");
		logger.fine("[HighlightedBlock] context active=" + active);

		/*
		 * Strip out any non vetted items
		 */
		CollectionsUtil.filter(items, new Predicate<Item>() {

			public boolean accept(Item e) {

				return e.isVisible();

			}
		});

		logger.fine("filtered items awaiting moderation");

		/* 
		 * strip out any items we do not have permission to view.
		 */
		CollectionsUtil.filter(items, new Predicate<Item>() {

			public boolean accept(Item e) {
				return PermissionType.ItemSummary.check(context, active, e.getItemType());
			}
		});

		logger.fine("filtered items without permissions");

		StringBuilder sb = new StringBuilder();

		sb.append(new OrderingForm(context));

		MarkupRenderer r = new MarkupRenderer(context, getMarkup());
		r.setBodyObjects(items);
		sb.append(r);

		sb.append("</form>");

		String html = sb.toString();
		htmlCache.put(getId(), new CacheContainer(html));

		return html;
	}

	public void setMarkup(Markup markup) {
		this.markup = markup;
	}

}
