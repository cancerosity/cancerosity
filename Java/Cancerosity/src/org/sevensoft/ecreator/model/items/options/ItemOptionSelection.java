package org.sevensoft.ecreator.model.items.options;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLineOption;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.ImageSupport;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

import java.io.IOException;
import java.util.*;

/**
 * @author sks 11-Oct-2005 10:38:33
 */
@Table("items_options_selections")
public class ItemOptionSelection extends ImageSupport implements Positionable, Selectable {

    private ItemOption itemOption;
    private int position;
    private Money salePrice, costPrice, optionRrp;
    private String text;
    private double vatRate;
    private int stock;
    private String productCode;
    private ItemOptionSelection parent;
    private SortedSet<Integer> revealIds;

    public ItemOptionSelection(RequestContext context) {
        super(context);
    }

    public ItemOptionSelection(RequestContext context, ItemOption option, String t) {
        super(context);

        this.itemOption = option;
        this.text = t;

        // NEED TO get vat rat from product I think
        //option can be created as Preset for an itemType
        if (option.getItem() != null) {
            this.vatRate = option.getItem().getVatRate();
        }

        save();
    }

    public static ItemOptionSelection getSelectionByProductCode(RequestContext context, String productCode) {
        Query q = new Query(context, "select * from # where productcode=?");
        q.setTable(ItemOptionSelection.class);
        q.setParameter(productCode);
        return q.get(ItemOptionSelection.class);
    }

    /**
     * @param target
     */
    public void copyTo(ItemOption target) {

        ItemOptionSelection newSelection = new ItemOptionSelection(context, target, text);

        newSelection.salePrice = salePrice;
        newSelection.costPrice = costPrice;
        newSelection.vatRate = vatRate;
        newSelection.revealIds = revealIds;
        newSelection.parent = this;

        // copy images
        try {
            copyImagesTo(newSelection);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ImageLimitException e) {
            e.printStackTrace();
        }

        newSelection.save();
    }

    @Override
    public synchronized boolean delete() {

        // recalc baskets
        Query q = new Query(context, "update # b join # bl on b.id=bl.basket join # blo on bl.id=blo.line set b.recalc=1 where blo.itemOption=?");
        q.setTable(Basket.class);
        q.setTable(BasketLine.class);
        q.setTable(BasketLineOption.class);
        q.setParameter(getItemOption());
        q.run();

        return super.delete();
    }

    public Money getCostPrice() {
        return costPrice;
    }

    public String getDescription() {

        if (salePrice == null || salePrice.isZero())
            return text;

        StringBuilder sb = new StringBuilder(text);

        if (salePrice.isGreaterThan(0)) {

            if (itemOption != null && itemOption.isPriceOverride()) {
                sb.append(" - Price is \243");
            } else {
                sb.append(" - extra charge of \243");
            }

        } else {

            sb.append(" - discount of \243");
        }

        if (getOptionsModule().isDisplayExVat()) {

            sb.append(getSalePrice());

        } else {

            sb.append(getSalePriceInc());
        }

        return sb.toString();
    }

    /**
     *
     */
    private OptionsModule getOptionsModule() {
        return getItemOption().getOptionsModule();
    }

    public int getImageLimit() {
        return 1;
    }

    public ItemOption getItemOption() {
        return itemOption.pop();
    }

    public String getLabel() {
        return getDescription();
    }

    public ItemOption getOption() {
        return itemOption.pop();
    }

    public int getPosition() {
        return position;
    }

    public String getProductCode () {
        return productCode;
    }

    public Money getSalePrice() {
        return salePrice;
    }

    public Money getSalePriceInc() {
        return salePrice.add(getSalePriceVat());
    }

    public Money getSalePriceVat() {
        if (getItemOption().getItem() != null && vatRate != getItemOption().getItem().getVatRate()) {
            vatRate = getItemOption().getItem().getVatRate();
            save();
        }
        if (vatRate == 0)
            return new Money(0);
        else
            return getSalePrice().multiply(vatRate / 100d);
    }

    public Money getPrice() {
        return getOptionsModule().isDisplayExVat() ? getSalePrice() : getSalePriceInc();
    }

    public Money getOptionRrp() {
        return optionRrp;
    }

    public Money getOptionRrpEx() {
        return optionRrp;
    }

    public Money getOptionRrpInc() {
        return optionRrp.add(getOptionRrpVat());
    }

    public Money getOptionRrpVat() {
        if (getItemOption().getItem() != null && vatRate != getItemOption().getItem().getVatRate()) {
            vatRate = getItemOption().getItem().getVatRate();
            save();
        }
        if (vatRate == 0)
            return new Money(0);
        else
            return getOptionRrp().multiply(vatRate / 100d);
    }

    public Money getRrp() {
        return getOptionsModule().isDisplayExVat() ? getOptionRrpEx() : getOptionRrpInc();
    }

    public int getStock() {
        return stock;
    }

    public String getText() {
        return text;
    }

    public String getValue() {
        return getIdString();
    }

    public ItemOptionSelection getParent() {
        return parent;
    }

    public Set<Integer> getRevealIds() {
		if (revealIds == null)
			return Collections.emptySet();
		return revealIds;
	}

    public void setCostPrice(Money costPrice) {
        this.costPrice = costPrice;
    }

    public void setSalePrice(Money price) {
        this.salePrice = price;
    }

    public void setOptionRrp(Money optionRrp) {
        this.optionRrp = optionRrp;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void setText(String s) {
        this.text = (s == null ? "Option" : s.trim());
    }

    public void setPosition(int position) {
        this.position = position;
    }

     public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public void setRevealIds(Collection<Integer> c) {
		this.revealIds = new TreeSet<Integer>();
		this.revealIds.addAll(c);
	}

    public boolean hasRevealIds() {
        return getRevealIds().size() > 0;
    }

    public List<ItemOptionSelection> getRelated(RequestContext context) {
        Query q = new Query(context, "select * from # where parent=?");
        q.setTable(ItemOptionSelection.class);
        q.setParameter(this);
        return q.execute(ItemOptionSelection.class);
    }

    public void saveSalePrice() {
        save("salePrice");
    }

    public void saveRrp() {
        save("optionRrp");
    }
}
