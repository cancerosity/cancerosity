package org.sevensoft.ecreator.model.items.listings;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.Captions;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sam2 Nov 12, 2003 2:00:43 AM
 */
@Table("settings_listings")
@Singleton
public class ListingSettings extends EntityObject {

	public enum CategorySelectionStyle {
		Flat, Navigation
	}

	public static ListingSettings getInstance(RequestContext context) {
		return EntityObject.getSingleton(context, ListingSettings.class);
	}

	/**
	 * A set of days when reminders are sent out
	 */
	private SortedSet<Integer>		expiryReminderDays;

    private String		expiryReminderBody;

	/**
	 * A collection of emails that will be notified each time a listing is made.
	 */
	private Set<String>			notificationEmails;

	/**
	 * 
	 */
	private CategorySelectionStyle	categorySelectionStyle;

	private String				notificationBody;

	private String	confirmationHeader, confirmationFooter;

    private String choosePackageText;

	public ListingSettings(RequestContext context) {
		super(context);
	}

	public void emailNotification(Item item) throws EmailAddressException, SmtpServerException {

		Config config = Config.getInstance(context);

		if (!hasNotificationEmails()) {
			return;
		}

		Email e = new Email();
		e.setFrom(config.getServerEmail());
		e.setBody(getNotificationBodyRendered(item));
		e.setRecipients(getNotificationEmails());
		e.setSubject("Listing created");
		new EmailDecorator(context, e).send(config.getSmtpHostname());
	}

	public final CategorySelectionStyle getCategorySelectionStyle() {
		return categorySelectionStyle == null ? CategorySelectionStyle.Flat : categorySelectionStyle;
	}

	public String getConfirmationFooter() {
		return confirmationFooter;
	}

	public String getConfirmationHeader() {
		return confirmationHeader;
	}

	public SortedSet<Integer> getExpiryReminderDays() {
		if (expiryReminderDays == null) {
			expiryReminderDays = new TreeSet();
		}
		return expiryReminderDays;
	}

    public String getExpiryReminderBody() {
        if (expiryReminderBody ==null){            //item.getName           expiryDate.toString("dd-MMM-yyyy")
            expiryReminderBody = "Your listing ([name]) will expire on [listing_expiry_date]. " +
                                        //item.getUrl()
                "Please click <a href=\"[itemurl]\">here</a> to return to" +
                " the site and view your listing.";
        }
        return expiryReminderBody;
    }

    public final String getNotificationBody() {
		return notificationBody == null ? "A new listing has been created called:\n'[name]'\n\nYou can view it here:\n[url]" : notificationBody;
	}

	private Object getNotificationBodyRendered(Item item) {
		String string = getNotificationBody();
		string = string.replace("[url]", item.getUrl());
		string = string.replace("[name]", item.getName());
		return string;
	}

	public final Set<String> getNotificationEmails() {
		if (notificationEmails == null) {
			notificationEmails = new HashSet();
		}
		return notificationEmails;
	}

	public boolean hasConfirmationFooter() {
		return confirmationFooter != null;
	}

	public boolean hasConfirmationHeader() {
		return confirmationHeader != null;
	}

	public boolean hasNotificationEmails() {
		return !getNotificationEmails().isEmpty();
	}

    public final String getChoosePackageText() {
        return choosePackageText != null ? choosePackageText : "Please choose the package that you wish to use for your " +
                Captions.getInstance(context).getListingsCaption().toLowerCase() + "." +
                " The lowest costs are displayed for each package along with a description of what that package offers.";
    }

    public final void setCategorySelectionStyle(CategorySelectionStyle categorySelectionStyle) {
		this.categorySelectionStyle = categorySelectionStyle;
	}

	public void setConfirmationFooter(String confirmationFooter) {
		this.confirmationFooter = confirmationFooter;
	}

	public void setConfirmationHeader(String confirmationHeader) {
		this.confirmationHeader = confirmationHeader;
	}

	public void setExpiryReminderDays(Collection<Integer> c) {
		this.expiryReminderDays = new TreeSet();
		if (c != null) {
			this.expiryReminderDays.addAll(c);
		}
	}

    public void setExpiryReminderBody(String expiryReminderBody) {
        this.expiryReminderBody = expiryReminderBody;
    }

	public final void setNotificationBody(String notificationBody) {
		this.notificationBody = notificationBody;
	}

	public void setNotificationEmails(Collection<String> s) {
		this.notificationEmails = new TreeSet<String>();
		this.notificationEmails.addAll(s);
	}

    public final void setChoosePackageText(String choosePackageText) {
        this.choosePackageText = choosePackageText;
    }
}