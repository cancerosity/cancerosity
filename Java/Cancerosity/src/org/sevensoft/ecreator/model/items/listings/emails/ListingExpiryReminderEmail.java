package org.sevensoft.ecreator.model.items.listings.emails;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 16-Nov-2005 01:04:27
 * 
 */
public class ListingExpiryReminderEmail extends Email {
    private RequestContext context;

	public ListingExpiryReminderEmail(Item item, RequestContext context) {
        this.context = context;
		setSubject("Expiry warning: " + item.getName());
		setFrom(Company.getInstance(context).getName(), Config.getInstance(context).getServerEmail());
		setBody("Just a gentle reminder than your listing will expire soon");
		setTo("sks@7soft.co.uk");
	}

    @Override
	public void send(String hostname) throws EmailAddressException, SmtpServerException {
		new EmailDecorator(context, this).send(hostname);
	}

}
