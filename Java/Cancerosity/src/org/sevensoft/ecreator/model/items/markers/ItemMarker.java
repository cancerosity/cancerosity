package org.sevensoft.ecreator.model.items.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketLineMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class ItemMarker extends MarkerHelper implements IItemMarker, IBasketLineMarker {

	public Object generate(RequestContext context, Map<String, String> params, BasketLine line) {

		if (line.hasItem()) {

			if (!params.containsKey("text")) {
				params.put("text", line.getItem().getDisplayName());
			}

			return super.link(context, params, line.getItem().getUrl(), null);

		} else {

			params.remove("link");
			return line.getName();
		}
	}

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		if (params.containsKey("account")) {
			item = item.getAccount();
			if (item == null) {
				return null;
			}
		}

		// if we contain the param sublinkonly then only link if we are subscribed
		if (params.containsKey("sublinkonly")) {

			if (!item.getSubscription().hasSubscriptionLevel()) {
				params.remove("link");
			}
		}

		// put in default class name if present
		if (!params.containsKey("class")) {
			params.put("class", "name");
		}

		// if no text set or label then default to display name
		if (!params.containsKey("text")) {
			params.put("text", item.getDisplayName());
		}

		if (params.containsKey("link")) {

			if (params.get("link").equals("category")) {

				if (item.hasCategory()) {

					return super.link(context, params, item.getCategory().getUrl(), null);

				} else {

					return null;
				}

			} else {

				return super.link(context, params, item.getUrl(), null);
			}

		} else {

			return super.string(context, params, item.getDisplayName());

		}
	}

	public Object getRegex() {
		return new String[] { "item" };
	}
}
