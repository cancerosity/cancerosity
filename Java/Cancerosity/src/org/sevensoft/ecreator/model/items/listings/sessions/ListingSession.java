package org.sevensoft.ecreator.model.items.listings.sessions;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.ecreator.model.attributes.support.AttributeImageAttachmentSupport;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.categorisable.Categories;
import org.sevensoft.ecreator.model.categories.categorisable.CategoryJoin;
import org.sevensoft.ecreator.model.categories.categorisable.CategoryUtil;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.vouchers.Voucher;
import org.sevensoft.ecreator.model.extras.meetups.Meetup;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.listings.Listing;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;
import org.sevensoft.ecreator.model.items.listings.ListingSettings;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 26 May 2006 00:21:07
 *
 */
@Table("listings_session")
public class ListingSession extends AttributeImageAttachmentSupport implements Categories {

	public static void delete(RequestContext context) {
		SimpleQuery.delete(context, ListingSession.class, "sessionId", context.getSessionId());
	}

    public static ListingSession get(RequestContext context) {
        ListingSession listingSession = SimpleQuery.get(context, ListingSession.class, "sessionId", context.getSessionId(), "completed", false);
        return listingSession == null ? new ListingSession(context, context.getSessionId()) : listingSession;
    }

	/**
	 * Tied to user by session id
	 */
	private String			sessionId;

	/**
	 * The content and name of the item to create
	 */
	private String			name, content;

	/**
	 * Set to true if the user has agreed to the agreement
	 */
	private boolean			agreed;

	/**
	 * The listing package currently choosen
	 */
	private ListingPackage		listingPackage;

	private String			email;

	private String			password;

	private Item			item;

	private Item			account;

	private Money			price;

	private int				stock;

	private String			outStockMsg;

	/**
	 * Set to true when we are listing as a guest
	 */
	private boolean			guest;

	private Map<Integer, Money>	deliveryRates;

	private Card			card;

	private boolean			offeredImages;

	private boolean			offeredAttachments;

	private String			accountName;

	private int				maxGuests;

	private boolean			privateGuests;

    private boolean completed;

	protected ListingSession(RequestContext context) {
		super(context);
	}

	public ListingSession(RequestContext context, String sessionId) {
		super(context);
		this.sessionId = sessionId;
		save();
	}

	public void addCategories(List<Category> categories) {
		for (Category c : categories) {
			addCategory(c);
		}
	}

	public void addCategory(Category category) {
		new CategoryJoin(context, this, category);
	}

	@Override
	public synchronized boolean delete() {
		reset();
		return super.delete();
	}

	public String getAlt() {
		return null;
	}

	public int getAttachmentLimit() {

		if (hasListingPackage()) {
			return getListingPackage().getMaxAttachments();
		}

		return 0;
	}

	public List<Attribute> getAttributes() {
		return AttributeUtil.getListableAttributes(getItemType());
	}

	public Card getCard() {
		return card;
	}

	/**
	 * These are the selected categories for this session
	 */
	public List<Category> getCategories() {
		return CategoryUtil.getCategories(context, this);
	}

	public Category getCategory() {
		return CategoryUtil.getCategory(this);
	}

	public final String getContent() {
		return content;
	}

	public final Map<Integer, Money> getDeliveryRates() {
		return deliveryRates;
	}

	public String getEmail() {
		return email;
	}

	public int getImageLimit() {
		return hasListingPackage() ? getListingPackage().getMaxImages() : 1;
	}

	public final Item getItem() {
		return (Item) (item == null ? null : item.pop());
	}

	public ItemType getItemType() {
		if (listingPackage == null) {
			return null;
		}
		return getListingPackage().getItemType();
	}

	public List<Category> getListingCategories() {
		return getListingPackage().getCategories();
	}

	/**
	 * Returns the currently selected listing package
	 * or if we are editing an item then it will return the listing package for that item
	 */
	public final ListingPackage getListingPackage() {
		return (ListingPackage) (listingPackage == null ? null : listingPackage.pop());
	}

	/**
	 * Returns a list of all listing packages available for this account or if not logged in, then
	 * anonymous packages
	 */
	public Collection<ListingPackage> getListingPackages() {

		if (account == null) {
			logger.fine("[ListingSession] getting packages for guest");
			return ListingPackage.getGuest(context);
		} else {
			logger.fine("[ListingSession] getting packages for account=" + account);
			return account.getListingPackages();
		}
	}

	public final String getName() {
		return name;
	}

	public String getOutStockMsg() {
		return outStockMsg;
	}

	public final String getPassword() {
		return password;
	}

	public Money getPrice() {
		return price;
	}

	public final String getSessionId() {
		return sessionId;
	}

	public int getStock() {
		return stock;
	}

    public Item getAccount() {
        return account;
    }

    public boolean hasAccount() {
		return account != null;
	}

	public boolean hasCategory() {
		return CategoryUtil.hasCategory(context, this);
	}

	public boolean hasItem() {
		return item != null;
	}

	public boolean hasListingPackage() {
		return listingPackage != null;
	}

	public boolean hasPrice() {
		return price != null && price.isPositive();
	}

	/**
	 * Returns true if this session has the basic required details - name, content and categories
	 */
	public boolean hasRequiredDetails() {

		if (name == null) {
			logger.fine("[ListingSession] name not set");
			return false;
		}

		if (getItemType().isContent()) {
            if (listingPackage.isAllowDescription()) {
                if (content == null) {
                    logger.fine("[ListingSession] content not set");
                    return false;
                }
            }
        }

		// if we are using categories then ensure we have at least one
		if (getItemType().isCategories()) {

			if (getListingPackage().isCategories()) {

				if (getCategories().isEmpty()) {

					logger.fine("[ListingSession] no category/categories set");
					return false;
				}
			}
		}

		return true;
	}

	public final boolean isAgreed() {
		return agreed;
	}

	/**
	 * 
	 */
	public boolean isOfferedAttachments() {
		return offeredAttachments;
	}

	/**
	 * 
	 */
	public boolean isOfferedImages() {
		return offeredImages;
	}

    public boolean isCompleted() {
        return completed;
    }

    public void removeCategories() {
		CategoryUtil.removeCategories(context, this);
	}

	public void removeCategory(Category category) {
		CategoryUtil.removeCategory(context, this, category);
	}

	private void reset() {

		account = null;
		agreed = false;
		outStockMsg = "Now out of stock";
		content = null;
		email = null;
		name = null;
		item = null;
		listingPackage = null;
		price = null;
		sessionId = null;
		stock = 0;

		removeImages();
		removeAttributeValues();
	}

	public void setAccount(Item a) {

		if (ObjectUtil.equal(account, a)) {
			return;
		}

		this.account = a;

		// clear data that might be invalid now
        //not actual since listingSession is completeda and created new one if previous was completed by customer
//		this.email = null;
//		this.name = null;
//
//		this.listingPackage = null;
//		this.agreed = false;
//
//		// clear attributes
//		removeAttributeValues();
//		// remove imgaes
//		removeImages();
//		// remove attachments
//		removeAttachments();

		save("account");
	}

	/**
	 * 
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public void setAgreed(boolean b) {
		agreed = b;
	}

	public void setCategories(List<Category> categories) {
		CategoryUtil.setCategories(this, categories);
	}

	public void setCategory(Category category) {
		CategoryUtil.setCategory(this, category);
	}

	public final void setContent(String content) {
		this.content = content;
	}

	public void setDeliveryRates(Map<Integer, Money> deliveryRates) {
		this.deliveryRates = deliveryRates;
	}

	public final void setEmail(String email) {
		this.email = email;
	}

	public final void setListingPackage(ListingPackage listingPackage) {

		if (ObjectUtil.equal(this.listingPackage, listingPackage)) {
			return;
		}

		this.listingPackage = listingPackage;
		this.agreed = false;
		this.item = null;

		removeImages();
		removeAttachments();
	}

	public void setMaxGuests(int maxGuests) {
		this.maxGuests = maxGuests;
	}

	public final boolean setName(String name) {
		this.name = name;
		return true;
	}

	public final void setOfferedAttachments(boolean offeredAttachments) {
		this.offeredAttachments = offeredAttachments;
		save();
	}

	public final void setOfferedImages(boolean offeredImages) {
		this.offeredImages = offeredImages;
		save();
	}

	public final void setOutStockMsg(String availability) {
		this.outStockMsg = availability;
	}

	public final void setPassword(String password) {
		this.password = password;
	}

	public final void setPrice(Money price) {
		this.price = price;
	}

	public void setPrivateGuests(boolean privateGuests) {
		this.privateGuests = privateGuests;
	}

	public final void setStock(int stock) {
		this.stock = stock;
	}

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public Item toItem() {

		// create the item but in disabled state
        item = new Item(context, getItemType(), name, getListingPackage().hasRates() ? "Disabled" : "Live", null);

		/*
		 * If we are logged in then we need to set the owner of this item to the
		 * logged in account, otherwise create a hidden one
		 */
		if (account == null) {

			Item hiddenAccount = new Item(context, ItemType.getAccount(context), accountName, "Live", null);
			hiddenAccount.setEmail(email);
			hiddenAccount.save();

			item.setAccount(hiddenAccount);

		} else {

			item.setAccount(account);
		}

		item.addCategories(getCategories());
		if (deliveryRates != null) {
			item.setDeliveryRates(deliveryRates);
		}

		if (listingPackage.isFeatured()) {
			logger.fine("[ListingSession] package is featured");
			item.setFeatured(true);
		}

        if (listingPackage.isPrioritised()) {
            logger.fine("[ListingSession] package is featured");
            item.setPrioritised(true);
        }

		if (item.getItemType().isBlockEmailsInContent()) {

			content = StringHelper.stripEmails(content);
			logger.fine("[ListingSession] blocking emails in content, new content=" + content);
		}

		item.setContent(content);

		if (listingPackage.isPrices()) {

			logger.fine("[ListingSession] setting prices, price=" + price);

			Money m = price;
			if (listingPackage.hasCommission()) {
				m = m.add(listingPackage.getCommission().calculate(m));
			}

			item.setSellPrice(null, 1, new Amount(m));
		}

		if (listingPackage.isStock()) {

			logger.fine("[ListingSession] setting stock, stock=" + price);
			item.setOurStock(stock);
		}

		// check for meetup details
		if (ItemModule.Meetups.enabled(context, getItemType())) {
			
			Meetup meetup = item.getMeetup();
			meetup.setPrivateGuests(privateGuests);
			meetup.setMaxGuests(maxGuests);
			meetup.save();
		}

		/*
		 * If we are not logged in check if we are validating guests
		 */
		if (account == null && getListingPackage().isValidateGuest()) {

			logger.fine("[ListingSession] package is set to validate guest, setting to awaiting validation");
			item.setAwaitingValidation(true);
		}

		// MODERATION CHECK
		if (listingPackage.isModeration()) {
			logger.fine("[ListingSession] package is moderated, setting to awaiting moderation");
			item.setAwaitingModeration(true);
		}

		item.save();

        this.setCompleted(true);
        this.save("completed");

		// set listing details
		Listing listing = item.getListing();
		listing.setListingPackage(getListingPackage());
		listing.save();

		try {

			copyAttachmentsTo(item);

		} catch (AttachmentExistsException e1) {
			e1.printStackTrace();

		} catch (AttachmentTypeException e1) {
			e1.printStackTrace();

		} catch (IOException e1) {
			e1.printStackTrace();

			logger.warning(e1.toString());
		} catch (AttachmentLimitException e) {
			e.printStackTrace();
		}

		copyAttributeValuesTo(item);

		try {

			copyImagesTo(item);

		} catch (IOException e) {
			e.printStackTrace();
			logger.warning(e.toString());

		} catch (ImageLimitException e) {
			e.printStackTrace();
		}

		/*
		 * If this is a guest listing, 
		 * then we need to send an email to the owner with a special url to come and modify the listing
		 */
		if (account == null) {

			try {
				logger.fine("[ListingSession] sending access email");
				listing.emailDetails();

			} catch (EmailAddressException e) {
				e.printStackTrace();

			} catch (SmtpServerException e) {
				e.printStackTrace();
			}

			// send email to verify email address if set in listing pacakge
			if (listingPackage.isValidateGuest()) {

				try {
					listing.emailValidation();
				} catch (EmailAddressException e) {
					e.printStackTrace();
				} catch (SmtpServerException e) {
					e.printStackTrace();
				}

			}
		}

		ListingSettings listingSettings = ListingSettings.getInstance(context);
		if (listingSettings.hasNotificationEmails()) {

			try {
				logger.fine("[ListingSession] sending notification email");
				listingSettings.emailNotification(item);
			} catch (EmailAddressException e) {
				e.printStackTrace();
			} catch (SmtpServerException e) {
				e.printStackTrace();
			}

		}

		return item;
	}

	public boolean useImageDescriptions() {
		return false;
	}

}
