package org.sevensoft.ecreator.model.items.markers;

import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;
import java.util.List;

/**
 * User: Tanya
 * Date: 06.08.2010
 */
public class ImagePathMarker extends MarkerHelper implements IItemMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

        if (item.hasApprovedImages()) {

            if (params.containsKey("limit")) {
                return item.getApprovedImage().getPath();
            } else {
                List<Img> list = item.getApprovedImages();
                StringBuilder path = new StringBuilder();
                for (int i = 0; i < list.size(); i++) {
                    path.append(list.get(i).getPath());
                    if (i < list.size() - 1)
                        path.append(", ");
                }

                return path.toString();
            }
        }
        return null;
    }

    public Object getRegex() {
        return "image_url";  //To change body of implemented methods use File | Settings | File Templates.
    }
}
