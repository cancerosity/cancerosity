package org.sevensoft.ecreator.model.items.pricing;

import java.util.List;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 21 Dec 2006 16:55:04
 * 
 * A band for individual pricing
 *
 */
@Table("prices_bands")
public class PriceBand extends EntityObject implements Selectable {

	public static List<PriceBand> get(RequestContext context) {
		return SimpleQuery.execute(context, PriceBand.class);
	}

	private String	name;

	protected PriceBand(RequestContext context) {
		super(context);
	}

	public PriceBand(RequestContext context, String name) {
		super(context);
		this.name = name;

		save();
	}

	@Override
	public synchronized boolean delete() {
		SimpleQuery.delete(context, Price.class, "priceBand", this);
		return super.delete();
	}

	public String getLabel() {
		return getName();
	}

	public final String getName() {
		return name;
	}

	public String getValue() {
		return getIdString();
	}

	public final void setName(String name) {
		this.name = name;
	}
}
