package org.sevensoft.ecreator.model.items.favourites;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.List;

/**
 * @author sks 10-Feb-2006 07:02:09
 *
 */
@Table("items_favourites")
public class Favourite extends EntityObject {

	/**
	 * 
	 */
	private Item		item;

	/**
	 * 
	 */
	private Item		account;

	/**
	 * The group of favourites this item is tagged to, eg, Wishlist or planner
	 */
	private FavouritesGroup	favouritesGroup;

	/**
	 * We can override the name of the favourite to give it a friendly name, like downstairs printer instead of Samsung ML1250 etc.
	 */
	private String		name;

	public Favourite(RequestContext context) {
		super(context);
	}

    public Favourite(RequestContext context, Item account, Item item) {
        this(context, null, account, item);
    }
	public Favourite(RequestContext context, FavouritesGroup favouritesGroup, Item account, Item item) {
		super(context);

        if (favouritesGroup != null) {
            if (favouritesGroup.contains(account, item))
                return;
        } else {
            if (Favourite.contains(context, account, item))
                return;
        }

		this.account = account;
		this.item = item;
		this.favouritesGroup = favouritesGroup;
		this.name = item.getName();

		save();

	}

    public static List<Favourite> get(RequestContext context, Item account) {

        Query q = new Query(context, "select * from # where account=?");
        q.setTable(Favourite.class);
        q.setParameter(account);
        return q.execute(Favourite.class);
    }

    public static boolean contains(RequestContext context, Item account, Item item) {
		return SimpleQuery.count(context, Favourite.class, "account", account, "item", item) > 0;
	}

	public FavouritesGroup getFavouritesGroup() {
		return favouritesGroup.pop();
	}

	public Item getItem() {
		return item.pop();
	}

	public Item getAccount() {
		return account.pop();
	}

	public String getName() {
		if (name == null) {
			name = getItem().getName();
			save();
		}
		return name;
	}

	public void setFavouritesGroup(FavouritesGroup favouritesGroup) {
		this.favouritesGroup = favouritesGroup;
	}

	public void setName(String name) {
		this.name = name;
	}

    public static void removeFavourite(RequestContext context, Item account, Item item) {
		SimpleQuery.delete(context, Favourite.class, "account", account, "item", item);
    }
}
