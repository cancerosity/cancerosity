package org.sevensoft.ecreator.model.items.pricing.markers;

import java.util.Map;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class RrpDiscountMarker implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		if (!item.getItemType().isRrp())
			return null;

		Item member = (Item) context.getAttribute("account");
		Currency currency = (Currency) context.getAttribute("currency");
		String currencySymbol = (String) context.getAttribute("currencySymbol");

        if (!PermissionType.ItemPriceSummary.check(context, member, item.getItemType())) {
            return null;
        }

		if (!item.hasRrpDiscount(member))
			return null;

		String prefix = params.get("prefix");
		String suffix = params.get("suffix");

        if (params.containsKey("price_max")) {
            member = null;
        }

        Money discount;
        if (params.containsKey("ex")) {
            discount = item.getRrpDiscountEx(member, 1, currency);
        } else {
            discount = item.getRrpDiscount(member, 1, currency);
        }

		StringBuilder sb = new StringBuilder();
		sb.append("<span class='rrp_discount'>");

		if (prefix != null)
			sb.append(prefix);

		sb.append(currencySymbol);
		sb.append(discount);

		if (suffix != null)
			sb.append(suffix);

		sb.append("</span>");
		return sb.toString();
	}

	public Object getRegex() {
		return "pricing_rrp_discount";
	}

}
