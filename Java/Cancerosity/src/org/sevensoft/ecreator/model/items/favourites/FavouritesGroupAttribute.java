package org.sevensoft.ecreator.model.items.favourites;

import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 25 Sep 2006 15:32:39
 *
 */
@Table("items_favourites_groups_attribute")
public class FavouritesGroupAttribute extends EntityObject {

	private Attribute		attribute;
	private FavouritesGroup	favouritesGroup;

	public FavouritesGroupAttribute(RequestContext context) {
		super(context);
	}

	public FavouritesGroupAttribute(RequestContext context, FavouritesGroup favouritesGroup, Attribute attribute) {
		super(context);

		this.favouritesGroup = favouritesGroup;
		this.attribute = attribute;

		save();
	}

	public Attribute getAttribute() {
		return attribute.pop();
	}

	public FavouritesGroup getFavouritesGroup() {
		return favouritesGroup.pop();
	}

}
