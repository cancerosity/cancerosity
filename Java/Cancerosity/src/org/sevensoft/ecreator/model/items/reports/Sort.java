package org.sevensoft.ecreator.model.items.reports;

/**
 * @author sks 9 Nov 2006 11:45:37
 *
 */
public enum Sort {

	XDesc, XAsc, YAsc, YDesc;

}
