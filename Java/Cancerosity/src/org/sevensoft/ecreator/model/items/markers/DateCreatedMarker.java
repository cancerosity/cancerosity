package org.sevensoft.ecreator.model.items.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.ICategoryMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.db.EntityObject;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class DateCreatedMarker extends MarkerHelper implements IItemMarker, ICategoryMarker, IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		String format = params.get("format");
		if (format == null) {
			format = "EEE dd/MM/yyyy";
		}

		if (params.containsKey("class")) {
			params.put("class", "date_created");
		}

		return super.string(context, params, item.getDateCreated().toString(format));
	}

    public Object generate(RequestContext context, Map<String, String> params, Category category) {
        String id = params.get("id");
        if (id != null) {
            category = EntityObject.getInstance(context, Category.class, id);
        }

        String name = params.get("name");
        if (name != null) {
            category = Category.getByName(context, name);
        }

        if (category == null) {
            category = (Category) context.getAttribute("category");
        }
        logger.fine("[CategoryMarker] category=" + category);

        if (category == null) {
            return null;
        }
        String format = params.get("format");
        if (format == null) {
            format = "EEE dd/MM/yyyy";
        }

        if (params.containsKey("class")) {
            params.put("class", "date_created_category");
        }

        return super.string(context, params, category.getDateCreated().toString(format));
    }

    public Object generate(RequestContext context, Map<String, String> params) {
        return generate(context, params, null);
    }

    public Object getRegex() {
		return new String[] { "date_created", "join_date" };
	}
}
