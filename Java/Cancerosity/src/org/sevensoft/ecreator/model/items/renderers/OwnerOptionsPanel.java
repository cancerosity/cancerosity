package org.sevensoft.ecreator.model.items.renderers;

import org.sevensoft.ecreator.iface.EcreatorRenderer;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.items.modules.options.ItemOptionsHandler;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.OptionSet;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.db.positionable.PositionRender;

/**
 * User: Tanya
 * Date: 23.12.2010
 */
public class OwnerOptionsPanel extends EcreatorRenderer {
    private Item item;
    private ItemType itemType;

    public OwnerOptionsPanel(RequestContext context) {
        super(context);
    }

    public OwnerOptionsPanel(RequestContext context, Item item) {
        this(context);
        this.item = item;
    }

    public OwnerOptionsPanel(RequestContext context, ItemType itemType) {
        this(context);
        this.itemType = itemType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(new AdminTable("Options"));

        sb.append("<tr>");
        sb.append("<th width='120'>Position</td>");
        sb.append("<th>Name</td>");
        sb.append("<th>Selections</td>");
        sb.append("<th width='60'>Edit</td>");
        sb.append("<th width='60'>Remove</td>");
        if (itemType != null) {
            sb.append("<th width='60'>Remove Related</td>");
            sb.append("<th width='60'>Add</td>");
        }
        sb.append("</tr>");

        PositionRender pr = new PositionRender(ItemOptionsHandler.class, "option");

        OptionSet optionSet = item !=null? item.getOptionSet(): itemType.getOptionSet();

        for (ItemOption option : optionSet.getOptions()) {

            sb.append("<tr>");
            sb.append("<td width='120'>" + pr.render(option) + "</td>");
            sb.append("<td>" + option.getName() + "</td>");
            sb.append("<td>");

            if (option.isSelection())
                sb.append(option.getSelectionsString());

            sb.append("&nbsp;</td>");
            sb.append("<td width='60'>" + new ButtonTag(ItemOptionsHandler.class, null, "Edit", "option", option) + "</td>");
            sb.append("<td width='60'>" +
                    new ButtonTag(ItemOptionsHandler.class, "delete", "Delete", "option", option)
                            .setConfirmation("Are you sure you want to delete this option?") + "</td>");
            if (itemType != null) {
                sb.append("<td width='60'>" +
                    new ButtonTag(ItemOptionsHandler.class, "deleteRelated", "Delete", "option", option)
                            .setConfirmation("Are you sure you want to delete all the related options?") + "</td>");
                sb.append("<td width='60'>" + new ButtonTag(ItemOptionsHandler.class, "addEverywhere", "Add to " + itemType.getNamePlural(), "option", option).
                        setConfirmation("Are you sure want to add the option to all the " + itemType.getNamePluralLower() + " across the system?") + "</td>");
            }
            sb.append("</tr>");

        }

        sb.append("<tr><td colspan='7'>Add new options<br/> " + new TextAreaTag(context, "newOptions", 40, 3) + "</td></tr>");
        sb.append("</table>");
        return sb.toString();
    }
}
