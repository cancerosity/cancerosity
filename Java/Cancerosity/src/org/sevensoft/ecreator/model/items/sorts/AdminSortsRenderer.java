package org.sevensoft.ecreator.model.items.sorts;

import java.util.List;

import org.sevensoft.ecreator.iface.admin.items.sorts.SortHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 17 Sep 2006 17:44:50
 *
 */
public class AdminSortsRenderer {

	private final RequestContext	context;
	private final ItemType		itemType;
	private final List<ItemSort>	sorts;

	public AdminSortsRenderer(RequestContext context, ItemType itemType) {
		this.context = context;
		this.itemType = itemType;
		this.sorts = itemType.getSorts();
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		PositionRender pr = new PositionRender(SortHandler.class, "sort");

		sb.append(new AdminTable("Sorts"));

		for (ItemSort sort : sorts) {

			sb.append("<tr>");
			sb.append("<td>" + pr.render(sort) + "</td>");
			sb.append("<td>" + new LinkTag(SortHandler.class, "edit", new SpannerGif(), "sort", sort) + " " + sort.getName() + "</td>");
			sb.append("<td width='150'>"
					+ new ButtonTag(SortHandler.class, "delete", "Remove", "sort", sort)
							.setConfirmation("Are you sure you want to delete this sort?") + "</td>");
			sb.append("</tr>");
		}

		sb.append("<tr><td colspan='3'>Add sort " + new ButtonTag(SortHandler.class, "addSort", "Submit", "itemType", itemType) + "</td></tr>");

		sb.append("</table>");

		return sb.toString();
	}

}
