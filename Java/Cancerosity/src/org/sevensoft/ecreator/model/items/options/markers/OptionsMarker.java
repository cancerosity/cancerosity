package org.sevensoft.ecreator.model.items.options.markers;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.io.File;

import org.sevensoft.ecreator.iface.admin.attachments.AttachmentHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketLineMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLineOption;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.renderers.OptionsRenderer;
import org.sevensoft.ecreator.model.items.options.renderers.OptionsToTableRenderer;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.ImageTag;

/**
 * @author sks 21 Apr 2006 11:31:20
 *
 */
public class OptionsMarker extends MarkerHelper implements IItemMarker, IBasketLineMarker {

	public Object generate(RequestContext context, Map<String, String> params, BasketLine line) {

		String sep = params.get("sep");
		String between = params.get("between");
        File file = null;

        StringBuilder sb = new StringBuilder();
		Iterator<BasketLineOption> iter = line.getOptions().iterator();
		while (iter.hasNext()) {

			BasketLineOption option = iter.next();

			sb.append("<span class='blo'>");
			sb.append("<span class='blo-name'>");
			sb.append(option.getName());
			sb.append("</span>");

			if (sep != null)
				sb.append(sep);

			sb.append("<span class='blo-value'>");

			if (option.hasAttachments()) {

                file = option.getAttachments().get(0).getImageFile();
                sb.append(new LinkTag(AttachmentHandler.class, null, option.getValue(), "attachment", option.getAttachments().get(0)));

			} else {

				sb.append(option.getValue());

			}

			sb.append("</span>");
			sb.append("</span>");

			if (iter.hasNext() && between != null)
				sb.append(between);
		}

        if (file != null) {
            String imgSrc = context.getContextPath() + "/" + Config.getInstance(context).getImagesPath() +
                    file.getPath().replaceAll("[\\x00-\\x7F]+images", "").replace('\\', '/');
            sb.append(new ImageTag(imgSrc, 0, 60, 60));
        }

        return sb.toString();
	}

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

        String printer = params.get("printer");

        if (printer == null) {
            if (!ItemModule.Options.enabled(context, item)) {
                logger.fine("[OptionsMarker] options disabled");
                return null;
            }

            /*
            * If no options to display then exit
*/
            List<ItemOption> options = item.getOptionSet().getOptions();
            if (options.isEmpty()) {
                logger.fine("[OptionsMarker] no options on item");
                return null;
            }

            logger.fine("[OptionsMarker] options=" + options);

            /*
            * Get the customised options markup if it is present
*/
            Markup markup = item.getOptionSet().getOptionsMarkup();

            /*
            * If the customised markup does not exist then render using the default renderer
*/
            if (markup == null) {
                logger.fine("[OptionsMarker] using default renderer");
                return new OptionsRenderer(context, item, options);
            }

            /*
            * ... Othewise render using our custom markup
*/
            logger.fine("[OptionsMarker] using markup=" + markup);
            MarkupRenderer r = new MarkupRenderer(context, markup);
            r.setBody(item);

            return r;
        } else {

            if (!ItemModule.Options.enabled(context, item)) {
                logger.fine("[OptionsMarker] options disabled");
                return null;
            }

            /*
            * If no options to display then exit
*/
            List<ItemOption> options = item.getOptionSet().getOptions();
            if (options.isEmpty()) {
                logger.fine("[OptionsMarker] no options on item");
                return null;
            }

            return new OptionsToTableRenderer(context, item, options);
        }
    }

	public Object getRegex() {
		return "options";
	}

}
