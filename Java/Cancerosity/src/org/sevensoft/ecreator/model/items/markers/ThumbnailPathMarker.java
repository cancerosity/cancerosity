package org.sevensoft.ecreator.model.items.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.ICategoryMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;
import java.util.List;

/**
 * User: Tanya
 * Date: 25.01.2011
 */
public class ThumbnailPathMarker extends MarkerHelper implements IItemMarker, ICategoryMarker, IGenericMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

        if (item.hasApprovedImages()) {

            if (params.containsKey("limit")) {
                if (item.getApprovedImage() != null) {
                    return item.getApprovedImage().getThumbnailPath();
                }
            } else {
                List<Img> list = item.getApprovedImages();
                StringBuilder path = new StringBuilder();
                for (int i = 0; i < list.size(); i++) {
                    path.append(list.get(i).getThumbnailPath());
                    if (i < list.size() - 1)
                        path.append(", ");
                }

                return path.toString();
            }
        }
        return "";
    }

    public Object generate(RequestContext context, Map<String, String> params, Category category) {
        if (category.hasApprovedImages()) {

            if (params.containsKey("limit")) {
                if (category.getApprovedImage() != null) {
                    return category.getApprovedImage().getThumbnailPath();
                }
            } else {
                List<Img> list = category.getApprovedImages();
                StringBuilder path = new StringBuilder();
                for (int i = 0; i < list.size(); i++) {
                    path.append(list.get(i).getThumbnailPath());
                    if (i < list.size() - 1)
                        path.append(", ");
                }

                return path.toString();
            }
        }
        return "";
    }

    public Object generate(RequestContext context, Map<String, String> params) {
        if (context.containsAttribute("item")) {
            return generate(context, params, (Item) context.getAttribute("item"), null, 0, 0);
        } else if (context.containsAttribute("category")) {
            return generate(context, params, (Category) context.getAttribute("category"));
        }
        return null;
    }

    public Object getRegex() {
        return "thumbnail_url";  //To change body of implemented methods use File | Settings | File Templates.
    }
}