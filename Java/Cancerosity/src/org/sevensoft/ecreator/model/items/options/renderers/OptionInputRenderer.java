package org.sevensoft.ecreator.model.items.options.renderers;

import org.sevensoft.ecreator.iface.frontend.items.ItemOptionImagesHandler;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.ecreator.model.items.options.OptionSet;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.samspade79.ecreator.uploader.FlashUploadObject;

import java.util.LinkedHashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.List;

/**
 * @author sks 26 Jul 2006 12:03:05
 *
 */
public class OptionInputRenderer {

	private final ItemOption	option;
	private final RequestContext	context;
	private final OptionSet		optionSet;

	public OptionInputRenderer(RequestContext context, ItemOption option) {
		this.context = context;
		this.option = option;
		this.optionSet = option.getItem().getOptionSet();
	}

	@Override
	public String toString() {

		final String paramName = "options_" + option.getIdString();

		StringBuilder sb = new StringBuilder();

		switch (option.getType()) {

        case FlashUpload:

			sb.append(new FlashUploadObject());
			break;

        case Upload:

			FileTag tag = new FileTag("optionUploads_" + option.getId());
			sb.append(tag);
			break;

		case Selection:

            appendSelection(paramName, sb);

			break;

        case List:

            appendList(paramName, sb);
            appendList1(paramName, sb);

            break;

		case Text:

			if (option.hasPrice()) {

				sb.append("Enter " + option.getName() + " at an extra cost of \243");

				if (option.getOptionsModule().isDisplayExVat()) {

					sb.append(option.getPrice());

				} else {

					sb.append(option.getPriceInc());
				}

			}

			if (option.getHeight() == 1) {
				sb.append(new TextTag(context, paramName, option.getWidth()));

			} else {
				sb.append(new TextAreaTag(context, paramName, option.getWidth(), option.getHeight()));
			}

			break;

		case YesNo:

			sb.append(new CheckTag(context, paramName, "true", false));
			break;
		}

		return sb.toString();
	}

    private void appendList(String paramName, StringBuilder sb) {
        sb.append("\n     <div class='" + paramName + "'>\n");

        for (ItemOptionSelection selection : option.getSelections()) {
            sb.append("         \n<div class='pricing_holder' name='pricing_holder' id='pricing_holder_" + selection.getIdString() + "'>\n");
            sb.append("             <span class='pricing_0_text'>");
            sb.append(selection.getText());
            sb.append("</span> \n");
            sb.append("             <span class='pricing_0_sellprice'>\n");
            sb.append("                 <em class='Bolden'>|</em>\n");
            sb.append("                 <em class='Diminish'> Now \243");
            sb.append(selection.getPrice());
            sb.append("</em>\n");
            sb.append("             </span> \n");
            if (selection.getRrp().isGreaterThan(0)) {
                sb.append("             <span class='pricing_0_rrp'>");
                sb.append("                 <em class='Bolden'> - </em>\n");
                sb.append("                 <em class='Diminish'>Was \243");
                sb.append(selection.getRrp());
                sb.append("</em>\n");
                sb.append("             </span> \n");
            }
            sb.append("         </div> \n");

        }
        sb.append("     </div>\n");


    }

    private void appendList1(String paramName, StringBuilder sb) {
        SelectTag selectTag = new SelectTag(context, paramName);

        if (option.hasIntro() || option.isOptional()) {

            if (option.hasIntro()) {
                selectTag.setAny(option.getIntro());
            } else {
                selectTag.setAny("-Choose optional " + option.getName() + "-");
            }
        }

        for (ItemOptionSelection selection : option.getSelections()) {
            selectTag.addOption(selection, selection.getText());
        }

        selectTag.setOnChange("showSelection();");
        sb.append(selectTag);
        sb.append("\n<script type=\"text/javascript\">\nfunction showSelection(){\n");
        sb.append("     if (this.document.activeElement.value != '') {\n" +
                "           for (var i=0; i< this.document.activeElement.options.length; i++){\n" +
                "               var k = document.getElementById('pricing_holder_'+this.document.activeElement.options[i].value);\n" +
                "               if (k != null) k.style.display = 'none';\n" +
                "           }"+
                "           var c = document.getElementById('pricing_holder_' + this.document.activeElement.value);\n" +
                "           if (c != null) c.style.display = '';\n" +
                "       }" +
                "       else {" +
                 "           for (var i=0; i< this.document.activeElement.options.length; i++){\n" +
                "               var k = document.getElementById('pricing_holder_'+this.document.activeElement.options[i].value);\n" +
                "               if (k != null) k.style.display = '';\n" +
                "           }\n" +
                "       }\n"+
                "   }\n");
                sb.append("</script>\n ");

        if (option.isImages()) {

            ButtonTag buttonTag = new ButtonTag("View images");
            buttonTag.setOnClick("window.open('" + new Link(ItemOptionImagesHandler.class, null, "option", option)
                    + "', 'optionimages', 'width=400, height=500, scrollbars=yes'); ");

            sb.append(buttonTag);
        }

    }

    private void appendSelection(String paramName, StringBuilder sb) {
        SelectTag selectTag = new SelectTag(context, paramName);

        if (option.hasIntro() || option.isOptional()) {

            if (option.hasIntro()) {
                selectTag.setAny(option.getIntro());
            } else {
                selectTag.setAny("-Choose optional " + option.getName() + "-");
            }
        }

        MultiValueMap<String, Integer> revealMap = new MultiValueMap(new LinkedHashMap());

        for (ItemOptionSelection selection : option.getSelections()) {

            if (selection.hasRevealIds()) {
//				reveals = true;
                revealMap.putAll(selection.getValue(), selection.getRevealIds());
            }
            selectTag.addOption(selection, selection.getDescription());
        }

        /*
           * Do reveal javascrips
       */
        if (revealMap.size() > 0) {


            String functionName = "optionRevealer" + option.getId();
            String functionName2 = "clearOptionsValues()";

            selectTag.setOnChange(functionName + "(this.options[this.selectedIndex].value); " + functionName2);

            context.setAttribute("optSection",
                    "var op=this.document.getElementsByName('" + selectTag.getName() + "')[0].options; \n" +
                            "var e=op[op.selectedIndex].value; \n" +
                            "if (e ==null) e='" + revealMap.keySet().toArray()[0] + "'; \n" +
                            functionName + "(e); \n");

            sb.append("<script>");
            sb.append("function " + functionName + "(value) { ");

            // clear dependant rows
            for (String id : new HashSet<String>(revealMap.keySet())) {
                sb.append(" e = document.getElementById('" + id + "'); if (e != null) e.style.display='none'; \n");
            }
            for (int id : new HashSet<Integer>(revealMap.values())) {
                sb.append(" e = document.getElementById('" + OptionsRenderer.getInputTrId(id) + "'); if (e != null) e.style.display='none';\n ");
            }

            for (Map.Entry<String, List<Integer>> entry : revealMap.entryListSet()) {

                String value = entry.getKey();
                List<Integer> ids = entry.getValue();

                sb.append("if (value == '" + value + "') {\n");

                sb.append(" e = document.getElementById('" + value + "'); if (e != null) e.style.display=''; \n");
                for (int id : ids) {
                    sb.append(" e = document.getElementById('" + OptionsRenderer.getInputTrId(id) + "'); if (e != null) e.style.display=''; \n");
                }

                sb.append(" } \n");
            }

            sb.append(" } \n");
            sb.append("</script>\n");

            sb.append("<script>\n");
            sb.append("function " + functionName2 + " { \n");


            for (int id : new HashSet<Integer>(revealMap.values())) {
                sb.append(" e = document.getElementsByName('" + OptionsRenderer.getInputName(id) + "')[0]; if (e != null) e.value=''; \n");
            }

            sb.append(" } \n");
            sb.append("</script>\n");

        }

        sb.append(selectTag);

        if (option.isImages()) {

            ButtonTag buttonTag = new ButtonTag("View images");
            buttonTag.setOnClick("window.open('" + new Link(ItemOptionImagesHandler.class, null, "option", option)
                    + "', 'optionimages', 'width=400, height=500, scrollbars=yes'); ");

            sb.append(buttonTag);
        }
    }
}
