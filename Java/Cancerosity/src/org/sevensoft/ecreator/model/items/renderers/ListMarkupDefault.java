package org.sevensoft.ecreator.model.items.renderers;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;
import org.sevensoft.ecreator.model.design.markup.MarkupCssClassDefault;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 26-Jan-2006 15:56:32
 *
 */
public class ListMarkupDefault extends MarkupCssClassDefault implements MarkupDefault {

	public String getBetween() {
		return null;
	}

	public String getCss() {
		StringBuilder sb = new StringBuilder();

        sb.append("table.njh_list_default { margin-top: 10px; width: 100%; font-family: \"Segoe UI\", \"Trebuchet MS\", Arial, Helvetica, sans-serif; font-size: 12px; color: #333333; }\n");
        sb.append("table.njh_list_default td { padding: 0; text-align: left; }\n");
        sb.append("table.njh_list_default tr.row0 td table.item_l { background: #ffffff url(../files/graphics/markup/default/gradient1.gif) repeat-x scroll left bottom; }\n");
        sb.append("table.njh_list_default tr.row1 td table.item_l { background: #ffffff url(../files/graphics/markup/default/gradient2.gif) repeat-x scroll left top; }\n");
        sb.append("table.item_l { height: 120px; border: 1px solid #dddddd; margin-bottom: 10px; }\n");
        sb.append("table.item_l td { padding: 12px 0; }\n");
        sb.append("table.item_l div.thumb { width: 100px; text-align: center; margin: 0 10px; }\n");
        sb.append("table.item_l div.thumb img { padding: 2px; border: 1px solid #999999; }\n");
        sb.append("table.item_l div.title { font-size: 14px; font-weight: bold; color: #cc0000; text-align: left; padding-left: 10px; }\n");
        sb.append("div.summary { padding-left: 10px; margin-top: 10px; }");
        sb.append("table.item_l div.pricing { margin-top: -5px; text-align: right; font-weight: bold; color: #4d9dcb; }\n");
        sb.append("table.item_l div.pricing span.inc { padding-left: 10px; }\n");
        sb.append("table.item_l div.pricing span.price_inc { font-size: 18px; color: #ff9211; }\n");
        sb.append("table.item_l div.pricing span.rrp { display: block; width: 240px; font-weight: normal; }\n");
        sb.append("table.item_l span.stock { display: block; text-align: right; font-weight: bold; background: transparent url(../files/graphics/markup/default/stock.gif) no-repeat scroll right center; padding: 4px 25px 3px 0; width: 215px; margin-top: 5px; }\n");
        sb.append("div.basket { width: 133px; float: right; margin-left: 10px; padding-top: 10px; }\n");
        sb.append("div.distance {clear: both; padding-top: 10px; text-align: center; margin-right: 10px; }\n");

        return sb.toString();
	}

	public String getBody() {

		StringBuilder sb = new StringBuilder();

//		sb.append(new TableTag("items_list", "center"));
        sb.append("<table class='item_l' cellspacing='0' cellpadding='0'><tr><td width='100%' valign='top'>\n");

        sb.append("<div class='title'>[item?link=1]</div>\n");
        sb.append("<div class='summary'>[summary?max=200]</div>\n");
        sb.append("</td><td class='ordering' valign='top'>\n");
        sb.append("<div class='pricing'><!-- [pricing_sell?prefix=ex VAT: &suffix=<span class='inc'>inc VAT: </span>] -->[pricing_sell?inc=1][pricing_rrp?prefix=<span class='discount'>RRP: ] [pricing_rrp_discount?prefix=- you save &suffix=!</span>]</div>\n");
        sb.append("[stock?text=true]\n");
        sb.append("<div class='basket'>[item?link=1&src=./files/graphics/markup/default/more_info.gif] [ordering_buy?src=./files/graphics/markup/default/add.gif]</div>\n");
        sb.append("<div class='distance'>[distance?suffix= miles<br />from ][location]</div>\n");
        sb.append("</td><td><div class='thumb'>[image?w=90&h=90&link=1&limit=1]</div></td></tr></table>\n");
        /*sb.append("\n<tr>\n");
		sb.append("<td class='image' width='100' align='center' valign='top'>[image?w=90&h=90&link=1&limit=1]</td>\n");
		sb.append("<td class='details' valign='top'>\n");

		sb.append(new TableTag("item_title"));
		sb.append("\n<tr><td>\n");
		sb.append("[item?link=1]\n");
		sb.append("</td><td align='right'>\n");

		sb.append("</td></tr>\n");
		sb.append("</table>\n");

		sb.append("[distance?suffix= miles ] [location?prefix=from ] \n");
		sb.append("[attributes_summary_span]\n");
		sb.append("[summary?max=200]\n");

		sb.append(new TableTag("ordering"));
		sb.append("\n<tr><td valign='top'>\n");

		sb.append("[pricing_sell?inc=1&prefix=Our price: ]\n");
		sb.append("[stock?label=Availability: &text=true]\n");

		sb.append("</td><td align='right' width='200'>\n");

		sb.append("[ordering_qty] [ordering_buy]\n");

		sb.append("</td></tr>\n");
		sb.append("</table>\n");

		sb.append("</td><td>\n\n");
		sb.append("[comparison_check]");
		sb.append("</td></tr>\n");
		sb.append("</table>\n");*/

		return sb.toString();
	}

	public String getEnd() {
		return null;
	}

	public String getName() {
		return null;
	}

	public String getStart() {

		/*StringBuilder sb = new StringBuilder();

		sb.append("<style>\n");

		sb.append("table.items_list * { font-family: Tahoma; font-size: 12px; } \n");
		sb.append("table.items_list { width: 100%;border-bottom: 1px solid #cccccc; } \n");
		sb.append("table.items_list td { padding: 15px 3px; }\n");

		sb.append("table.items_list td.image img { border: 1px solid #999999; padding: 3px; }\n ");

		// title box
		sb.append("table.items_list table.item_title { font-family: Tahoma; width: 100%; font-size: 12px;}\n ");
		sb.append("table.items_list table.item_title td { padding: 0; }\n ");

		// name styles
		sb.append("table.items_list span.link_name { }\n");
		sb.append("table.items_list span.link_name a { color: #333333; text-decoration: underline; font-weight: bold; }\n");
		sb.append("table.items_list span.link_name a:hover { color: #666666;  text-decoration: none; }\n");

		// member
		sb.append("table.items_list span.member { font-size: 12px; color: #666666; }\n");
		sb.append("table.items_list span.member a { color: #666666;  text-decoration: underline; }\n");
		sb.append("table.items_list span.member a:hover { color: #666666;  text-decoration: none; }\n");

		sb.append("table.items_list span.distance { font-size: 11px; color: #666666; font-weight: bold; margin-bottom: 1px; }\n");
		sb.append("table.items_list span.attribute { font-size: 11px; color: #666666; margin-bottom: 1px; }\n");
		sb.append("table.items_list span.attribute-value { color: #333333; padding-right: 10px; } ");

		// content
		sb.append("table.items_list span.content { font-size: 11px; margin-top: 6px; color: #666666; }\n");
		sb.append("table.items_list span.content a.more { font-weight: bold; font-weight: normal; text-decoration: underline; color: #333333; }\n ");
		sb.append("table.items_list span.content a.more:hover { font-weight: normal; text-decoration: none; }\n ");

		sb.append("table.items_list span.summary { display: block; padding: 3px 0; } ");

		// ordering box
		sb.append("table.items_list table.ordering { margin-top: 10px; font-family: Tahoma; font-size: 11px; width: 100%; } \n");
		sb.append("table.items_list table.ordering td { padding: 0; } \n");

		sb.append("table.items_list span.availability { color: #666666; }\n");
		sb.append("table.items_list span.price { color: #cc0000; font-weight: bold; margin-bottom: 1px; }\n");

		// buy button
		sb.append("table.items_list span.basket input.qty { border: 1px solid #666666; padding: 2px 3px; font-size: 11px; }\n ");
		sb.append("table.items_list span.basket input.button { font-family: Tahoma; padding: 1px 3px; font-size: 11px; }\n");

		sb.append("</style>");*/

		return null;
	}

	public int getTds() {
		return 1;
	}

    public String getCssClass() {
        StringBuilder sb = new StringBuilder();
        sb.append("njh_list_default");
        return sb.toString();
    }

    public int getCycleRows() {
        return 2;
    }

}
