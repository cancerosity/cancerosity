package org.sevensoft.ecreator.model.items.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 23 Apr 2007 12:54:13
 *
 */
public class AgeMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		String age = item.getAgeString();
		return super.string(context, params, age);
	}

	public Object getRegex() {
		return "age";
	}

}
