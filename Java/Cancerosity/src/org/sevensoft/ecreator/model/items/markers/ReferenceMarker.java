package org.sevensoft.ecreator.model.items.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 8 Feb 2007 17:18:31
 *
 */
public class ReferenceMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		if (!item.getItemType().isReferences()) {
			return null;
		}

		String ref = item.getReference();
		if (ref == null) {
			return null;
		}

		return super.string(context, params, ref);
	}

	@Override
	public String getDescription() {
		return "Displays the reference of the item, the reference is used for synchronization with external databases.";
	}

	public Object getRegex() {
		return "item_reference";
	}

}
