package org.sevensoft.ecreator.model.items.renderers;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
public class FamilyItemsMarkupDefault implements MarkupDefault {

    public String getBetween() {
        return null;
    }

    public String getCss() {
        return null;
    }

    public String getBody() {

        StringBuilder sb = new StringBuilder();

        sb.append("<tr>");
        sb.append("[attribute?id=8&tag=td]");
        sb.append("[item?link=1&tag=td]");
        sb.append("[attribute?id=17&tag=td]");
        sb.append("[attribute?id=18&tag=td]");
        sb.append("[attribute?id=19&tag=td]");
        sb.append("[attribute?id=12&tag=td]");
        sb.append("</tr>");

        return sb.toString();
    }

    public String getEnd() {
        return null;
    }

    public String getName() {
        return null;
    }

    public String getStart() {

        StringBuilder sb = new StringBuilder();

        sb.append("<style>\n");

        sb.append("table.items_list * { font-family: Tahoma; font-size: 12px; } \n");
        sb.append("table.items_list { width: 100%;border-bottom: 1px solid #cccccc; } \n");

        sb.append("</style>");

        return sb.toString();
    }

    public int getTds() {
        return 0;
    }
}
