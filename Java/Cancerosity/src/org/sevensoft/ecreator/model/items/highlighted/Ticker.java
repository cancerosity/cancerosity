package org.sevensoft.ecreator.model.items.highlighted;

import java.util.List;

import org.sevensoft.ecreator.iface.admin.categories.blocks.TickerBlockHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.renderers.TickerMarkupDefault;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.ecreator.model.search.SearchOwner;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 12 Jul 2006 16:48:00
 *
 */
@Table("tickers")
@Label("Ticker")
@HandlerClass(TickerBlockHandler.class)
public class Ticker extends Block implements SearchOwner {

    @Deprecated
	private String	font;

    @Deprecated
	private String	fontSize;

	@Deprecated
    private String	highlightFontColor;

	@Deprecated
    private String	fontColor;

    @Deprecated
	private String	backgroundColor;

    @Deprecated
	private String	borderColor;

    @Deprecated
	private String	pauseTime;

    @Deprecated
	private String	highlightBorderColor;

    private Markup markup;

	private Ticker(RequestContext context) {
		super(context);
	}

	public Ticker(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, 0);
	}

	public String getBackgroundColor() {
		return backgroundColor == null ? "FFFFFF" : backgroundColor;
	}

	public String getBorderColor() {
		return borderColor == null ? "999999" : borderColor;
	}

	public String getFont() {
		return font == null ? "Arial" : font;
	}

	public String getFontColor() {
		return fontColor == null ? "000000" : fontColor;
	}

	public String getFontSize() {
		return fontSize == null ? "12" : fontSize;
	}

	public String getHighlightBorderColor() {
		return highlightBorderColor == null ? "000030" : highlightBorderColor;
	}

	public String getHighlightFontColor() {
		return highlightFontColor == null ? "0000E0" : highlightFontColor;
	}

	private String getPauseTime() {
		return pauseTime == null ? "3000" : pauseTime;
	}

	public Search getSearch() {
		return Search.get(context, this);
	}

    public Markup getMarkup() {
        if (markup == null) {
			markup = new Markup(context, new TickerMarkupDefault());
			save();
		}

		return markup.pop();

    }

	@Override
	public Object render(RequestContext context) {

		List<Item> items = getSearch().getSearcher().getItems();
		if (items.size() == 0) {
			logger.fine("[Ticker] no items, exiting");
			return null;
		}

		logger.fine("[Ticker] rendering items=" + items);

		StringBuilder sb = new StringBuilder();

        Markup markup = getMarkup();
        MarkupRenderer r = new MarkupRenderer(context, markup);
        r.setLocation(null, 0, 0);
        r.setBodyObjects(items);
        sb.append(r);

        sb.append("\n<script type='text/javascript'>\n");
        sb.append("    function tick(){\n");
        sb.append("        $('#ec_ticker li:first').slideUp( function () { $(this).appendTo($('#ec_ticker')).slideDown(); });\n");
        sb.append("    }\n");
        sb.append("    setInterval(function(){ tick () }, " + TickerSettings.getInstance(context).getInterval() + ");\n");
        sb.append("</script>");
/*

		sb.append("<div class='newsticker' align='center'>");
		sb.append("<applet code='advnewsticker2.class' codebase='files/applets/' width='400' height='20'>");
		sb.append("<param name='applet_width' value='400'>");
		sb.append("<param name='applet_height' value='20'>");
		sb.append("<param name='regcode' value='coghkr5u8'>");
		sb.append("<param name='info' value='Applet by Gokhan Dagli,www.appletcollection.com'>");
		sb.append("<param name='input_text' value='from_parameters'>");
		sb.append("<param name='text_file' value=''>");
		sb.append("<param name='bgcolor' value='" + getBackgroundColor() + "'>");
		sb.append("<param name='text_color' value='" + getFontColor() + "'/>");
		sb.append("<param name='highlight_text_color' value='" + getHighlightFontColor() + "'/>");
		sb.append("<param name='font_type' value='" + getFont() + "'>");
		sb.append("<param name='font_size' value='" + getFontSize() + "'>");
		sb.append("<param name='font_style' value='0'>");
		sb.append("<param name='underline' value='no'>");
		sb.append("<param name='border_thickness' value='0'>");
		sb.append("<param name='border_color' value='" + getBorderColor() + "'>");
		sb.append("<param name='highlight_border_color' value='" + getHighlightBorderColor() + "'>");
		sb.append("<param name='text_align' value='center'>");
		sb.append("<param name='xoffset' value='5'>");
		sb.append("<param name='yoffset' value='0'>");
		sb.append("<param name='pause_time' value='" + getPauseTime() + "'>");
		sb.append("<param name='hscroll_delay' value='8'>");
		sb.append("<param name='vscroll_delay' value='20'>");
		sb.append("<param name='typewriter_delay' value='30'>");
		sb.append("<param name='fade_delay' value='25'>");
		sb.append("<param name='effects' value='custom'>");
		sb.append("<param name='custom_in_effects' value='4'>");
		sb.append("<param name='custom_out_effects' value='1'>");

		int n = 1;
		for (Item item : items) {

			sb.append("<param name='text" + n + "' value='" + item.getName() + "'/>\n");
			sb.append("<param name='link" + n + "' value='" + item.getUrl() + "'/>\n");
			sb.append("<param name='target_frame" + n + "' value='_self'/>\n");

			n++;
		}
		sb.append("</applet></div>");
*/

		return sb.toString();
	}

	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public void setBorderColor(String borderColor) {
		this.borderColor = borderColor;
	}

	public void setFont(String font) {
		this.font = font;
	}

	public void setFontColor(String fontColor) {
		this.fontColor = fontColor;
	}

	public void setFontSize(String fontSize) {
		this.fontSize = fontSize;
	}

	public void setHighlightBorderColor(String highlightBorderColor) {
		this.highlightBorderColor = highlightBorderColor;
	}

	public void setHighlightFontColor(String highlightTextColor) {
		this.highlightFontColor = highlightTextColor;
	}

	public void setPauseTime(String pauseTime) {
		this.pauseTime = pauseTime;
	}

    public void setMarkup(Markup markup) {
        this.markup = markup;
    }
}
