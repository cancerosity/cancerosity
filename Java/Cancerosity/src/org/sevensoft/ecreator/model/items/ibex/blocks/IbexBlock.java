package org.sevensoft.ecreator.model.items.ibex.blocks;

import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ibex.IbexSettings;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.iface.admin.items.ibex.IbexBlockHandler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.db.annotations.Table;

/**
 * User: Tanya
 * Date: 06.01.2011
 */
@Table("blocks_ibex")
@Label("iBex. Internet Booking Exchange")
@HandlerClass(IbexBlockHandler.class)
public class IbexBlock extends Block {

    public IbexBlock(RequestContext context) {
        super(context);
    }

    public IbexBlock(RequestContext context, BlockOwner owner, int objId) {
        super(context, owner, objId);
    }

    public Object render(RequestContext context) {
        context.setAttribute("ibex", true);

        Item item = (Item) context.getAttribute("item");

        Attribute ibexAttribute = item.getIbexAttribute();
        String attValue = item.getAttributeValue(ibexAttribute);
        String rnid = ibexAttribute.getOptionFromValue(attValue).getRnid();
        IbexSettings ibexSettings = IbexSettings.getInstance(context);
        String pid = item.getAttributeValue(ibexSettings.getPropertyId());

        if (ibexSettings.getOp() == null || pid ==null){
            return "";
        }

//        StringBuilder messageAgent = new StringBuilder();
//        messageAgent.append("http%3A//");
//        messageAgent.append(context.getRequest().getServerName());
//        int port = context.getRequest().getServerPort();
//        if (port != 80 && port != 443) {
//            messageAgent.append("%3A");
//            messageAgent.append(port);
//        }
//        messageAgent.append("/files/js/ibex/SeekomMessageAgent.html");

//        String messageAgentUrl = "http%3A//"+ context.getRequest().getServerName()+"/files/js/ibex/SeekomMessageAgent.html";     //todo change
/*        StringBuilder dynamicBookingURL = new StringBuilder();
        dynamicBookingURL.append("http://www.seekom.com/accommodation/PropertySearch.php?op=");
        dynamicBookingURL.append(ibexSettings.getOp());
        dynamicBookingURL.append("&cyid=");
        dynamicBookingURL.append(ibexSettings.getCyid());
        dynamicBookingURL.append("&rnid=");
        dynamicBookingURL.append(rnid);
        dynamicBookingURL.append("&lnid=0&hostma=");
        dynamicBookingURL.append(messageAgent);*/

          StringBuilder dynamicBookingURL = new StringBuilder();
        dynamicBookingURL.append("http://www.seekom.com/accommodation/Property.php?op=");
        dynamicBookingURL.append(ibexSettings.getOp());
        dynamicBookingURL.append("&pid=");
        dynamicBookingURL.append(pid);
//        dynamicBookingURL.append("&cyid=");
//        dynamicBookingURL.append(ibexSettings.getCyid());
//        dynamicBookingURL.append("&rnid=");
//        dynamicBookingURL.append(rnid);
//        dynamicBookingURL.append("&lnid=0&hostma=");
//        dynamicBookingURL.append(messageAgent);

//### Retrieve the Seekom Region ID from your own look up table
//// sample uses dorset
//$rnid = 655;
//
//### Enter the absolute URL of your SeekomMessageAgent.html file, with the ":" encoded as %3A
////example - change this if you need to
//$messageAgentURL = "http%3A//www.just-caravanning.co.uk/SeekomMessageAgent.html";
//
//### Then we set up the iframe source
//$dynamicBookingURL = "http://www.seekom.com/accommodation/PropertySearch.php?op=sunshine&cyid=111&rnid=";
//$dynamicBookingURL .=$rnid;
//$dynamicBookingURL .="&lnid=0&hostma=".$messageAgentURL;


        return
                "<iframe id=\"bookingPage\" onload= \"SeekomFrame.SetHeight(this)\"\n" +
                        "   src=\"" + dynamicBookingURL + "\"\n" +
                        "        width=\"" + ibexSettings.getFrameWidth() + "px\"\n" +
                        "        height=\"" + ibexSettings.getFrameHeight() + "px\"\n" +
                        "        frameborder=\"no\"\n" +
                        "        scrolling=\"auto\"\n" +
                        "        marginwidth=\"0\" marginheight=\"0\" vspace=\"0\" hspace=\"0\"\n" +
                        "        style=\"overflow-y: visible;\" >\n" +
                        "</iframe>";
    }
}
