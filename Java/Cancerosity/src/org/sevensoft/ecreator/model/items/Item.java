package org.sevensoft.ecreator.model.items;

import org.jdom.JDOMException;
import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.collections.Transformer;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.commons.superstrings.RandomHelper;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.superstrings.comparators.NaturalStringComparator;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.frontend.account.PasswordHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.model.accounts.buyers.BuyerAddress;
import org.sevensoft.ecreator.model.accounts.buyers.BuyerConfig;
import org.sevensoft.ecreator.model.accounts.login.PasswordHelper;
import org.sevensoft.ecreator.model.accounts.permissions.AccessGroup;
import org.sevensoft.ecreator.model.accounts.permissions.AccessGroupItem;
import org.sevensoft.ecreator.model.accounts.permissions.Domain;
import org.sevensoft.ecreator.model.accounts.registration.RegistrationModule;
import org.sevensoft.ecreator.model.accounts.registration.emails.ConfirmationEmail;
import org.sevensoft.ecreator.model.accounts.sessions.AccountSession;
import org.sevensoft.ecreator.model.accounts.subscriptions.Subscription;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionModule;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.downloads.Download;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.ecreator.model.attributes.AttributeValue;
import org.sevensoft.ecreator.model.attributes.support.AttributeImageAttachmentSupport;
import org.sevensoft.ecreator.model.bookings.BookingSetup;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategoryItem;
import org.sevensoft.ecreator.model.categories.DiscountComparator;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.crm.forms.submission.Submission;
import org.sevensoft.ecreator.model.crm.forum.Forum;
import org.sevensoft.ecreator.model.crm.forum.Post;
import org.sevensoft.ecreator.model.crm.forum.Topic;
import org.sevensoft.ecreator.model.crm.jobsheets.AutoJob;
import org.sevensoft.ecreator.model.crm.jobsheets.Job;
import org.sevensoft.ecreator.model.crm.messages.MessageOwner;
import org.sevensoft.ecreator.model.crm.messages.MessageUtil;
import org.sevensoft.ecreator.model.crm.messages.Msg;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.*;
import org.sevensoft.ecreator.model.ecom.branches.Branch;
import org.sevensoft.ecreator.model.ecom.credits.CreditStore;
import org.sevensoft.ecreator.model.ecom.delivery.Delivery;
import org.sevensoft.ecreator.model.ecom.delivery.DeliveryOption;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.ecom.orders.VatHelper;
import org.sevensoft.ecreator.model.ecom.payments.Adjustment;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.cards.CardType;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.*;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorException;
import org.sevensoft.ecreator.model.ecom.payments.results.AvsCheck;
import org.sevensoft.ecreator.model.ecom.promotions.PromotionItem;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.ecom.shopping.ShoppingSettings;
import org.sevensoft.ecreator.model.ecom.suppliers.Sku;
import org.sevensoft.ecreator.model.ecom.suppliers.Supplier;
import org.sevensoft.ecreator.model.ecom.vouchers.Voucher;
import org.sevensoft.ecreator.model.ecom.vouchers.VoucherUse;
import org.sevensoft.ecreator.model.extras.meetups.Meetup;
import org.sevensoft.ecreator.model.extras.ratings.Rating;
import org.sevensoft.ecreator.model.extras.ratings.RatingType;
import org.sevensoft.ecreator.model.extras.shoppingbox.ShoppingBox;
import org.sevensoft.ecreator.model.feeds.refreshers.MidwichRefresher;
import org.sevensoft.ecreator.model.interaction.Interaction;
import org.sevensoft.ecreator.model.interaction.LastActive;
import org.sevensoft.ecreator.model.interaction.buddies.Buddy;
import org.sevensoft.ecreator.model.interaction.pm.PrivateMessage;
import org.sevensoft.ecreator.model.interaction.userplane.recorder.Recording;
import org.sevensoft.ecreator.model.interaction.views.View;
import org.sevensoft.ecreator.model.interaction.views.ViewCount;
import org.sevensoft.ecreator.model.interaction.winks.Wink;
import org.sevensoft.ecreator.model.items.favourites.Favourite;
import org.sevensoft.ecreator.model.items.favourites.FavouritesGroup;
import org.sevensoft.ecreator.model.items.highlighted.blocks.HighlightedItemsBlock;
import org.sevensoft.ecreator.model.items.highlighted.boxes.HighlightedItemsBox;
import org.sevensoft.ecreator.model.items.listings.Listing;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;
import org.sevensoft.ecreator.model.items.listings.ListingRate;
import org.sevensoft.ecreator.model.items.listings.emails.ListingExpiryReminderEmail;
import org.sevensoft.ecreator.model.items.listings.sessions.ListingPaymentSession;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.OptionGroup;
import org.sevensoft.ecreator.model.items.options.OptionSet;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.ecreator.model.items.pricing.*;
import org.sevensoft.ecreator.model.items.pricing.costs.SupplierCostPrice;
import org.sevensoft.ecreator.model.items.pricing.overrides.PriceOverride;
import org.sevensoft.ecreator.model.items.reviews.ReviewsDelegate;
import org.sevensoft.ecreator.model.items.stock.Audit;
import org.sevensoft.ecreator.model.items.stock.StockModule;
import org.sevensoft.ecreator.model.items.stock.StockNotificationEmail;
import org.sevensoft.ecreator.model.items.stock.SupplierStock;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.marketing.newsletter.Subscriber;
import org.sevensoft.ecreator.model.marketing.sms.credits.SmsCreditPackage;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.ImageSettings;
import org.sevensoft.ecreator.model.media.images.ImageType;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.media.images.emails.ApprovedEmail;
import org.sevensoft.ecreator.model.media.images.emails.RejectedEmail;
import org.sevensoft.ecreator.model.media.videos.Video;
import org.sevensoft.ecreator.model.media.videos.VideoOwner;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.misc.Page;
import org.sevensoft.ecreator.model.misc.agreements.Agreement;
import org.sevensoft.ecreator.model.misc.agreements.AgreementOwner;
import org.sevensoft.ecreator.model.misc.agreements.AgreementUtil;
import org.sevensoft.ecreator.model.misc.content.ContentBlock;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.misc.location.Locatable;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.misc.seo.Meta;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.ecreator.model.products.accessories.Accessory;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.ecreator.model.search.SearchOwner;
import org.sevensoft.ecreator.model.search.alerts.SearchAlert;
import org.sevensoft.ecreator.model.stats.StatablePage;
import org.sevensoft.ecreator.model.stats.vouchers.VoucherUser;
import org.sevensoft.ecreator.model.stats.downloads.DownloadCounterMonthly;
import org.sevensoft.ecreator.model.stats.downloads.DownloadCounterWeekly;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.model.comments.Comment;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.QueryBuilder;
import org.sevensoft.jeezy.db.Row;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Selectable;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author sks 13-Aug-2005 18:08:54
 *
 */

/**
 * @author sks 3 May 2007 15:08:43
 */
@Table("items")
public final class Item extends AttributeImageAttachmentSupport implements BlockOwner, StatablePage, Locatable, Comparable<Item>, Meta, MessageOwner,
        AgreementOwner, Priceable, VideoOwner, Logging, SearchOwner, Selectable, Page {

    public static void clearCachedPrices(RequestContext context) {
        new Query(context, "update # set genericSellPrice=0, costPrice=-1").setTable(Item.class).run();

    }

    public static List<Item> get(RequestContext context, ItemType itemType, String status, int start, int limit) {

        QueryBuilder b = new QueryBuilder(context);
        b.select("*");
        b.from("#", Item.class);

        if (itemType != null) {
            b.clause("itemType=?", itemType);
        }

        if (status != null) {
            b.clause("status=?", status);
        }

        b.order("name");
        b.limit(start, limit);

        return b.execute(Item.class);
    }

    public static List<Item> get(RequestContext context, ItemType itemType, String status) {
        return get(context, itemType, status, "name");
    }

    public static List<Item> get(RequestContext context, ItemType itemType, String status, String orderby) {
        QueryBuilder b = new QueryBuilder(context);
        b.select("*");
        b.from("#", Item.class);

        if (itemType != null) {
            b.clause("itemType=?", itemType);
        }

        if (status != null) {
            b.clause("status=?", status);
        }

        b.order(orderby);

        return b.execute(Item.class);
    }

    public static List<Item> get(RequestContext context, ItemType itemType, String status, Item account) {
        QueryBuilder b = new QueryBuilder(context);
        b.select("*");
        b.from("#", Item.class);

        if (itemType != null) {
            b.clause("itemType=?", itemType);
        }

        if (status != null) {
            b.clause("status=?", status);
        }

        if (account != null) {
            b.clause("account=?", account);
        } else {
            b.clause("account=?", 0);
        }

        b.order("dateCreated DESC");

        return b.execute(Item.class);
    }

    public static List<Item> getAccounts(RequestContext context) {
        Query q = new Query(context, "SELECT i.* FROM # i JOIN # it ON i.itemType=it.id WHERE it.modules LIKE ? ORDER BY name");
        q.setTable(Item.class);
        q.setTable(ItemType.class);
        q.setParameter("%" + ItemModule.Account + "%");
        return q.execute(Item.class);
    }

    /**
     * is used for selections, don't load unnecessary params
     *
     * @param context RequestContext
     * @return Map<ID, NAME>
     */
    public static Map getAccountsForOptions(RequestContext context) {
        Query q = new Query(context, "SELECT i.id, i.name FROM # i JOIN # it ON i.itemType=it.id WHERE it.modules LIKE ? ORDER BY name");
        q.setTable(Item.class);
        q.setTable(ItemType.class);
        q.setParameter("%" + ItemModule.Account + "%");
        return q.getMap(Integer.class, String.class, new LinkedHashMap());
    }

    public static Item getByEmail(RequestContext context, String email) {
        return SimpleQuery.get(context, Item.class, "email", email);
    }

    public static Item getByEmail(RequestContext context, String email, ItemType accountType) {
        return SimpleQuery.get(context, Item.class, "email", email, "itemType", accountType);
    }

    public static Item getByLogin(RequestContext context, int id, String passwordPlain) {

        String hash = PasswordHelper.generatePasswordHash(passwordPlain);
        return SimpleQuery.get(context, Item.class, "id", id, "passwordHash", hash);
    }

    public static Item getByLogin(RequestContext context, String email, String passwordPlain) {

        String hash = PasswordHelper.generatePasswordHash(passwordPlain);
        logger.fine("[Item] getting by login, email=" + email + ", hash=" + hash);
        return SimpleQuery.get(context, Item.class, "email", email, "passwordHash", hash);
    }

    public static Item getByName(RequestContext context, String name) {
        Query q = new Query(context, "select * from # where name=?");
        q.setParameter(name);
        q.setTable(Item.class);
        return q.get(Item.class);
    }

    public static Item getByRef(RequestContext context, ItemType itemType, String ref) {

        QueryBuilder b = new QueryBuilder(context);
        b.select("i.*");
        b.from("# i", Item.class);

        if (itemType != null) {
            b.clause("i.itemType=?", itemType);
        }

        b.clause("i.reference=?", ref);

        return b.toQuery().get(Item.class);
    }

    public static Map<String, String> getSelectionMap(RequestContext context) {
        Query q = new Query(context, "select id, name from # where status=? order by name");
        q.setTable(Item.class);
        q.setParameter("Live");
        return q.getMap(String.class, String.class, new LinkedHashMap());
    }

    public static List<Item> getShopzilla(RequestContext context) {

        Query q = new Query(context, "select distinct i.* from # i join # ci on i.id=ci.item join # c on ci.category=c.id "
                + "where c.shopzillaCategory is not null and i.status=? order by i.name ");
        q.setTable(Item.class);
        q.setTable(CategoryItem.class);
        q.setTable(Category.class);
        q.setParameter("Live");
        return q.execute(Item.class);
    }

    /**
     * Returns true if this name already exists for another item
     */
    public static boolean isNameExisting(RequestContext context, String name) {
        return new Query(context, "select count(*) from # where name=?").setTable(Item.class).setParameter(name).getInt() > 0;
    }

    public static boolean isUniqueEmail(RequestContext context, String email) {
        return SimpleQuery.count(context, Item.class, "email", email) == 0;
    }

    /**
     * Checks this email is unique discounting any email set on the item param,
     * as that will be the current item, and they can set their email to themselves (keep same)
     */
    public static boolean isUniqueEmail(RequestContext context, String email, Item account) {

        Query q = new Query(context, "select count(*) from # where email=? and id!=?");
        q.setTable(Item.class);
        q.setParameter(email.toLowerCase());
        q.setParameter(account);
        return q.getInt() == 0;
    }

    public static void repair(RequestContext context) {

        for (Item item : Item.get(context, null, null, 0, 0)) {

            item.genericSellPrice = null;
            item.setCategoryInfo();

            item.save();
        }

    }

    public static final String FAMILY_ATTRIBUTE = "Family";

    /*
      * When flagged allows users to order an item that is out of stock
      */
    @Default("1")
    private boolean backorders;

    private Agreement agreement;

    /**
     * Unit of issue
     */
    private int unit;

    /**
     *
     */
    @Index()
    private DateTime dateCreated;

    /**
     * Code used to validate accounts by email
     */
    @Index()
    private String confirmationCode;

    /**
     * The src of the feed that created this item - useful if we want to 'undo' feed creations
     * We can also use it to wipe out items that are not present in the next run of the feed - but this is not useful from multiple sourced items
     * <p/>
     * This string is just the id@classname of the feed
     */
    private String feedSrc;

    /**
     * Our cost price we have set manually
     */
    private Money ourCostPrice;

    /**
     * Referance for this item other than the auto id generated by the db.
     * Used for synching with another database.
     */
    @Index()
    private String reference;

    /**
     * email for logging in
     */
    private String email;

    /**
     * An iteration counter for the feed so we know which items we can delete.
     * The feed interation is increased by the feed each time and this iteration is set here so we know when it was last updated
     */
    private int feedCount;

    private String lastMessageAuthor;

    private DateTime lastMessageDate;

    /**
     * Number of saved searches
     */
    private int searchCount;

    private Money balance;

    /**
     * `
     */
    private DateTime dateUpdated;

    /**
     * The definition class for this item
     */
    @Index()
    private ItemType itemType;

    /**
     * Meta tags
     */
    private String descriptionTag, titleTag;

    /**
     * Used for keywords for the page, but also used for searching by keyword
     */
    @Index()
    private String keywords;

    /**
     * If flagged to true, then this friendly Url will not change anymore, unless manually changed.
     */
    private boolean friendlyUrlLocked;

    private int stockNotifyLevel;

    /**
     * Is this a featured item ?
     */
    @Index()
    private boolean featured;

    private int orderQtyMin;

    private int orderQtyMax;

    /**
     * The multiples that must be bought. IE, if set to 2 then must be bought in 2s.
     */
    private int orderQtyMultiple;

    /**
     * The content stripped of any html tags
     */
    private String contentStripped;

    /**
     * Content of this item
     */
    @Index()
    private String content;

    /**
     * Set to true when we are waiting the user to validate his email address before this listing can go live.
     */
    @Index()
    private boolean awaitingValidation;

    /**
     * The vat rate of this item across all services
     */
    private double vatRate;

    private int accessoryCount;

    /**
     * RRP of the product item
     */
    private Money rrp;

    private String location;

    private int postCount;

    private String summary;

    /**
     * A shortened version of the name used by the basket screens. Will default to full name if null
     */
    private String shortName;

    /**
     * The generic selling price of the standard pricing for qty 1.
     */
    private Money genericSellPrice;

    /**
     * Set to true when this item cannot be ordered but the item type is an orderable type.
     */
    private boolean brochure;

    /**
     * Flagged to true if this listing is awaiting moderation or this is an account then it is awaiting approval
     */
    @Index()
    private boolean awaitingModeration;

    /**
     * The grid ref of this item
     */
    @Index()
    private int x;

    @Index()
    private int y;

    private int optionCount;

    /**
     *
     */
    @Index()
    private String status;

    /**
     * Our internal stock
     */
    private int ourStock;

    /**
     * Availabilty of item when out of stock
     */
    private String outStockMsg;

    /**
     * Name of this item
     */
    @Index()
    private String name;

    private User owner;

    private List<String> suggestions;

    /**
     * A password used when logging in to this listing.
     */
    private String passwordHash;

    /**
     *
     */
    private transient List<Category> categoriesCache;

    /**
     * The first category this item is in - cached here purely for speed.
     */
    private Category categoryCached;

    /**
     *
     */
    @Index()
    protected int categoryCount;

    /**
     *
     */
    private String categoryFullName;

    /**
     *
     */
    private String categoryIds;

    /**
     *
     */
    private String categoryName;

    /**
     * The cost price we are to use, calculated
     */
    private Money costPrice;

    /**
     * If this item account is actually a buying account for another main user then this is a link to the user who owns the account
     */
    private Item buyerFor;

    private Branch branch;

    /**
     * Use an option group for options
     */
    private OptionGroup optionGroup;

    private TreeSet<Integer> priceBreaks;

    private boolean endOfLine;

    /**
     * The account that listed this item
     */
    private Item account;

    /**
     * The price band assigned to this account item
     */
    private PriceBand priceBand;

    private int messageCount;

    /**
     * Timestamp this account was last used
     */
    private long lastActive;

    /**
     * The user that created this item
     */
    private User createdBy;

    /**
     * Cached category names
     */
    private String categoryNames;

    /**
     * Code is set when we want to reset the users password.
     */
    private String resetCode;

    private String displayName;

    /**
     * The ip address used to register this account
     */
    private String ipAddress;

    /**
     * If prioritised then it comes first when ordering
     */
    @Index()
    private boolean prioritised;

    /**
     * This is the number of stocks available depending on the stock system
     */
    private int availableStock;

    /**
     * A cache of rating averages
     */
    private Map<Integer, Integer> ratingAverages;

    /**
     * The generic average combined from the other averages
     */
    private int ratingAverage;

    private String friendlyUrl;

    private int pendingReviewsCount;

    private int approvedReviewsCount;

    private String referrer;

    private int videoCount;

    private int smsCredits;

    private String inactiveMessage;

    private boolean excludeFromGoogleFeed;

    private long openRangeProductId;

    private String mobilePhone;

    @Default("1")
    private boolean comments = true;

    // Module.CategoryAndItemDiscount  Discount has already been applied
    //so discounts should not be used
    private boolean discountApplied;

    private boolean listingPaymentDone;

    private boolean showOptionsList;

    protected Item(RequestContext context) {
        super(context);
    }

    public Item(RequestContext context, ItemType itemType, String name, String status, User createdBy) {
        super(context);

        this.status = status;
        this.createdBy = createdBy;
        this.dateCreated = new DateTime();
        this.itemType = itemType;

        this.rrp = new Money(0);

        this.vatRate = itemType.getDefaultVatRate();

        StockModule module = getItemType().getStockModule();
        this.outStockMsg = module.getOutStockMsgDefault();
        this.backorders = module.isDefaultBackorders();

        if (MiscSettings.getInstance(context).isTitleCase()) {
            this.name = StringHelper.toTitleCase(name);
        } else {
            this.name = name.trim();
        }

        if (itemType.getModules().contains(ItemModule.Account)) {
            setPassword();
        }

        this.balance = new Money(0);
        this.lastActive = System.currentTimeMillis();

//        this.comments = CommentsSettings.getInstance(context).isAddToCategories();

        save();

        makeFriendlyUrl();

        // clear content links
//        ContentBlock.clear(context);

        if (itemType.hasDefaultContent()) {
            setContent(itemType.getDefaultContent());
        }

        // if item type has default category then bung us in !
        if (itemType.hasDefaultCategory()) {
            addCategory(itemType.getDefaultCategory());
        }

        // get attributes from item type and check for default values
        // we don't need to get attributes from categories because I don't want to set default values for category attributes
        AttributeUtil.setDefaults(this, itemType.getAttributes());

        /*
           * Check for auto jobs
           */
        if (Module.Jobsheets.enabled(context)) {

            for (AutoJob autojob : AutoJob.get(context, itemType)) {
                autojob.create(this);
            }
        }

        /*
           * Check for email notifications
           */
        for (String email : itemType.getEmails()) {
            try {
                sendCreationEmail(email);
            } catch (EmailAddressException e) {
                e.printStackTrace();
            } catch (SmtpServerException e) {
                e.printStackTrace();
            }
        }

        /*
           * Auto signup account newsletters
           */
        if (email != null) {

            List<Newsletter> newsletters = getItemType().getRegistrationModule().getAutoSignupNewsletters();
            if (newsletters.size() > 0) {
                Subscriber sub = Subscriber.get(context, email, true);
                sub.subscribe(newsletters);
            }
        }

//		/*
//		 * Check if we have any registration credit packages
//		 */
//		if (Module.Credits.enabled(context)) {
//
//			List<CreditPackage> packages = CreditPackage.getRegistration(context);
//			for (CreditPackage creditPackage : packages) {
//				logger.fine("[Item] detected registration credit package: " + creditPackage);
//				getCreditStore().credit(creditPackage, null, null);
//			}
//
//		}

        if (itemType.hasInitialTopicContent()) {

            logger.fine("[Item] detected initial topic content");
            addPost(account, itemType.getInitialTopicContent());
        }

        // clear cached higlighted
        HighlightedItemsBlock.clearHtmlCache();
        HighlightedItemsBox.clearHtmlCache();

        if (itemType.getModules().contains(ItemModule.MyShoppingBox)) {
            new ShoppingBox(context, this);
        }
    }

    public void addAccessGroup(AccessGroup ag) {
        new AccessGroupItem(context, ag, this);
    }

    public void addAccessory(Item accessory) {
        new Accessory(context, this, accessory);
        setAccessoryCount();
        save();
    }

    public Address addAddress(String contactName, String addressLine1, String addressLine2, String addressLine3, String postTown, String county,
                              String postCode, Country country, String telephone1, String telephone2, String telephone3, String instructions) throws PostCodeException,
            ContactNameException, TownException, CountryException, AddressLineException {

        try {

            Address a = new Address(context, this, contactName, addressLine1, addressLine2, addressLine3, postTown, county, postCode, country);
            a.setTelephone1(telephone1);
            a.setTelephone2(telephone2);
            a.setTelephone3(telephone3);
            a.setInstructions(instructions);
            a.save();

            return a;

        } catch (PostCodeException e) {
            throw e;

        } catch (ContactNameException e) {
            throw e;

        } catch (TownException e) {
            throw e;

        } catch (CountryException e) {
            throw e;

        } catch (AddressLineException e) {
            throw e;
        }
    }

    public Address addAddress(String contactName, String companyName, String[] address, String town, String county, String postcode, Country country,
                              String telephone1, String telephone2, String telephone3, String instructions) throws PostCodeException, ContactNameException, TownException,
            CountryException, AddressLineException {

        return addAddress(contactName, address[0], (address.length > 1 ? address[1] : null), (address.length > 2 ? address[2] : null), town, county,
                postcode, country, telephone1, telephone2, telephone3, instructions);
    }

    public Adjustment addAdjustment(Date date, Money amount) {
        Adjustment a = new Adjustment(context, this, date, amount);
        setBalance();

        return a;
    }

    public Item addBuyer(String name) {
        Item item = new Item(context, getItemType(), name, "Live", null);
        item.setBuyerFor(this);
        return item;
    }

    public Card addCard(String ipAddress, CardType cardType, String cardHolder, String cardNumber, String expiryDate, String startDate, String issueNumber,
                        String csc, String cardIssuer) throws IssueNumberException, ExpiryDateException, ExpiredCardException, CardNumberException, IOException,
            CardTypeException, StartDateException, CardHolderException, CscException, CardException, ProcessorException {

        Card card = new Card(context, this, ipAddress, cardType, cardHolder, cardNumber, expiryDate, startDate, issueNumber, csc, cardIssuer);
        return card;
    }

    public final void addCategories(Collection<Category> categories) {

        List<Category> existingCategories = getCategories();

        for (Category category : categories) {

            if (!existingCategories.contains(category)) {

                new CategoryItem(context, this, category);
                category.setLiveCount();

            }
        }

        clearCategoryCaches();
        setCategoryInfo();
        save();
    }

    public final void addCategory(Category c) {
        addCategories(Collections.singleton(c));
    }

    public Download addDownload(Attachment attachment) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException {

        Download download = new Download(context, this, attachment);

        DownloadCounterMonthly.process(context, new Date(), attachment, false);
        DownloadCounterWeekly.process2(context, new Date(), attachment, false);

        return download;
    }

    public Favourite addFavourite(FavouritesGroup favouritesGroup, Item item) {
        return new Favourite(context, favouritesGroup, this, item);
    }

    public Favourite addFavourite(Item item) {
        return new Favourite(context, this, item);
    }

    public Msg addMessage(String author, String email, String body) {
        return new Msg(context, this, author, email, body);
    }

    public Msg addMessage(User user, String content) {
        return new Msg(context, this, user, content);
    }

    public Order addOrder(User user, String ip) {

        Order order = new Order(context, this, PaymentType.Cash, null, ip);
        order.setSalesPerson(user);
        order.save();

        if (user == null) {
            order.log("Order created");
        } else {
            order.log(user, "Order created");
        }

        return order;
    }

    public Payment addPayment(PaymentType paymentType, Money money, Date date) {

        Payment payment = new Payment(context, this, money, paymentType);
        payment.setDate(date);

        setBalance();
        return payment;
    }

    public Post addPost(Item poster, String content) {

        Post post = new Post(context, getTopic(true), poster, content);
        setPostCount();
        save();

        return post;
    }

    public Rating addRating(Item account, RatingType ratingType, int rating) {
        Rating r = new Rating(context, this, account, ratingType, rating);
        calculateRatingAverages();
        return r;
    }

    public Search addSearch(int interval) {

        Search search = new Search(context, this);

        searchCount = getSearches().size();
        save();

        return search;
    }

    public void addSmsCredits(SmsCreditPackage p) {
        this.smsCredits = smsCredits + p.getCredits();
        save("smsCredits");
    }

    /**
     * Adds new stock into the system.
     * <p/>
     * This will increase our own internal stock level
     * <p/>
     * Will need to check on any outstanding orders before increasing free stock level
     *
     * @param q
     */
    public void addStock(int q) {
        this.ourStock += q;
        save();

        new Audit(context, this, ourStock, q, "Stock level manually adjusted by " + q);
    }

    public Video addVideo(String filename) throws IOException {
        return new Video(context, this, filename);
    }

    public Video addVideo(Upload upload) throws IOException {
        return new Video(context, this, upload);
    }

    /**
     * Allocate stock from our own internal levels
     */
    public int allocate(OrderLine line, int request) {

        StockModule module = getItemType().getStockModule();
        switch (module.getStockControl()) {

            case None:
            case String:
                return request;

            case Boolean:
                if (ourStock > 1) {
                    return request;
                } else {
                    return 0;
                }

            case Manual:
                return request;

            case RealTime:

                int allocated;

              if (isBackorders()) {

                    allocated = request;
                    ourStock = ourStock - allocated;
                    save();

                } else {

                    // if we have more stock than what is required then return requested amount and reduce stock levels
                    if (request <= ourStock) {

                        allocated = request;

                        ourStock = ourStock - allocated;
                        save();

                    } else {

                        //.. otherwise return all we have left, which will either be just enough, or not quite enough. Either way, stock will be stock,
                        // and we should run any zero stock methods

                        allocated = ourStock;
                        ourStock = 0;
                        save();

                    }
                }

                // check if we have gone below notify level
                if (ourStock <= stockNotifyLevel) {
                    try {
                        emailStockNotifications();
                    } catch (EmailAddressException e) {
                        e.printStackTrace();
                    } catch (SmtpServerException e) {
                        e.printStackTrace();
                    }
                }

                new Audit(context, this, ourStock, request, request + " allocated to order " + line.getOrderId());

                // recalculate available stock
                calculateAvailableStock();

                return allocated;
        }

        assert false;
        return 0;
    }

    public void approveAccount() {
        awaitingModeration = false;
        save();

        RegistrationModule module = getItemType().getRegistrationModule();
        module.approve(this);
    }

    /**
     * Sets the available stock cache levels
     */
    public void calculateAvailableStock() {

        /*
           * Returns the amount of stock
           */
        StockModule module = getItemType().getStockModule();
        switch (module.getStockControl()) {

            case Boolean:
                this.availableStock = ourStock;
                break;

            case String:
            case None:
                this.availableStock = 0;
                break;

            case Manual:
            case RealTime:

                this.availableStock = ourStock;

                switch (module.getStockSystem()) {

                    // for internal just leave stock as our stock
                    case Internal:
                        break;

                        // with cumulative add in all supplier stocks to our stock
                    case Cumulative:

                        for (SupplierStock supplierStock : getSupplierStocks()) {
                            this.availableStock += supplierStock.getStock();
                        }

                        break;

                        // for average, add in supplier stocks and get average
                    case Average:

                        int n = 1;
                        for (SupplierStock supplierStock : getSupplierStocks()) {
                            this.availableStock += supplierStock.getStock();
                            n++;
                        }
                        this.availableStock = availableStock / n;
                        break;

                    case Highest:

                        for (SupplierStock supplierStock : getSupplierStocks()) {

                            if (supplierStock.getStock() > this.availableStock) {
                                this.availableStock = supplierStock.getStock();
                            }
                        }

                        break;
                }
                break;
        }

        save();
    }

    private void calculateCostPrice() {

        switch (getItemType().getCostPriceSystem()) {

            default:
            case Cheapest:

                // get all cost prices and return the cheapest, start with our cost price.
                costPrice = ourCostPrice;

                for (SupplierCostPrice supply : getCostPrices()) {

                    // ignore zero cost prices
                    if (supply.getCostPrice() == null || supply.getCostPrice().isZero()) {
                        continue;
                    }

                    if (costPrice == null || costPrice.isZero() || supply.getCostPrice().isLessThan(costPrice)) {
                        costPrice = supply.getCostPrice();
                    }
                }

                if (costPrice == null) {
                    costPrice = new Money(0);
                }
        }

        save("costPrice");
    }

    private void calculateRatingAverages() {

        double cumulativeAverage = 0;
        double n = 0;

        ratingAverages = new HashMap<Integer, Integer>();
        for (RatingType ratingType : getItemType().getRatingModule().getRatingTypes()) {

            int average = getAverage(ratingType);
            ratingAverages.put(ratingType.getId(), average);

            cumulativeAverage += average;
            n++;
        }

        ratingAverage = (int) Math.round(cumulativeAverage / n);

        logger.fine("[Item] Averages: " + ratingAverage + " " + ratingAverages);

        save();
    }

    public final void clearCategoryCaches() {
        categoryCached = null;
        categoriesCache = null;
    }

    public void clearPriceCaches() {
        genericSellPrice = new Money(0);
        save("genericSellPrice");
        Basket.flagItem(context, this);
    }

    public Item clone() throws CloneNotSupportedException {
        return clone(name + " (copy)");
    }

    public Item clone(String name) throws CloneNotSupportedException {

        Item copy = (Item) super.clone();

        copy.dateCreated = new DateTime();

        copy.name = name;
        copy.categoryNames = null;
        copy.categoryIds = null;
        copy.categoryFullName = null;
        copy.categoryName = null;

        copy.accessoryCount = 0;

        // remove cached sell price
        copy.genericSellPrice = null;

        copy.friendlyUrl = null;
        copy.friendlyUrlLocked = false;

        copy.save();

        // copy categories
        copy.addCategories(getCategories());

        // images
        try {
            copyImagesTo(copy);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ImageLimitException e) {
            e.printStackTrace();
        }

        // attribute values
        copy.setAttributeValues(getAttributeValues());

        // attachments
        try {
            copyAttachmentsTo(copy);

        } catch (AttachmentExistsException e) {
            e.printStackTrace();

        } catch (AttachmentTypeException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();

        } catch (AttachmentLimitException e) {
            e.printStackTrace();
        }

        // copy options
        for (ItemOption option : getOptionSet().getOptions()) {
            option.copyTo(copy);
        }
        copy.setOptionCount();

        copy.save();

        copyPricingTo(copy);

        return copy;
    }

    public Item clone(String target, String replacement) throws CloneNotSupportedException {

        Pattern pattern = Pattern.compile(target, Pattern.LITERAL | Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(name);

        String newName = matcher.replaceAll(Matcher.quoteReplacement(replacement));

        return clone(newName);
    }

    public int compareTo(Item item) {
        return NaturalStringComparator.instance.compare(getName().toLowerCase(), item.getName().toLowerCase());
    }

    /**
     * Confirm code matches
     */
    public boolean confirm(String code) {

        if (code == null)
            return false;

        if (code.equals(confirmationCode)) {
            confirmationCode = null;
            save();
            return true;
        }

        return false;
    }

    private Money convert(Money money, Currency currency) {
        return Currency.convert(money, currency);
    }

    private void copyPricingTo(Item copy) {

        copy.setPriceBreaks(getPriceBreaks());
        copy.setOurCostPrice(getOurCostPrice());
        copy.save();

        for (Price price : getPrices()) {
            copy.setSellPrice(price.getPriceBand(), price.getQty(), price.getAmount());
        }
    }

    @Override
    public synchronized boolean delete() {

        removeAccessories();
        removeBasketLines();
        removeCategories();
        removeImages();
        removePrices();
        removeStocks();
        removeCostPrices();
        removeSkus();
        removeAttachments();
        removeAttributeValues();

        removeRatings();
        removeOptions();
        removeFavourites();
        removeFromPromotions();

        // Private messsages
        SimpleQuery.delete(context, PrivateMessage.class, "sender", this);
        SimpleQuery.delete(context, PrivateMessage.class, "recipient", this);
        SimpleQuery.delete(context, Meetup.class, "item", this);

        // remove from winks
        SimpleQuery.delete(context, Wink.class, "recipient", this);
        SimpleQuery.delete(context, Wink.class, "sender", this);

        // remove from views
        SimpleQuery.delete(context, View.class, "viewer", this);
        SimpleQuery.delete(context, View.class, "account", this);
        SimpleQuery.delete(context, ViewCount.class, "viewer", this);
        SimpleQuery.delete(context, ViewCount.class, "account", this);

        // remove from buddies
        SimpleQuery.delete(context, Buddy.class, "account", this);
        SimpleQuery.delete(context, Buddy.class, "buddy", this);

        // remove from assocations
        Query q = new Query(context, "delete av.* from # av join # a on av.attribute=a.id where a.type=? and av.value=?");
        q.setTable(AttributeValue.class);
        q.setTable(Attribute.class);
        q.setParameter(AttributeType.Association);
        q.setParameter(getIdString());
        q.run();

        // attribute associate values
        SimpleQuery.delete(context, AttributeValue.class, "value", getIdString());

        // sessions
        SimpleQuery.delete(context, AccountSession.class, "item", this);

        // recordings
        SimpleQuery.delete(context, Recording.class, "account", this);

        // remove form listing sesions
        SimpleQuery.delete(context, ListingPaymentSession.class, "item", this);

        //remove from voucherUses
        SimpleQuery.delete(context, VoucherUser.class, "account", this);
        
        Class[] updates = new Class[]{Job.class, Submission.class};
        for (Class clazz : updates) {
            new Query(context, "update # set item=0 where item=?").setTable(clazz).setParameter(this).run();
        }

        /*
           * If the item has been used it CANNOT be deleted.
           */
        if (isUsed()) {

            this.status = "Deleted";

            /*
                * Remove topic if one is set
                */
            Topic topic = getTopic(false);
            if (topic != null)
                topic.delete();

            super.save();
            return true;

        } else {

            super.delete();
            return true;

        }
    }

    /**
     * This method will run all search alerts to see if they match this new item
     */
    public void doSearchAlerts() {

        for (SearchAlert alert : SearchAlert.get(context)) {
            alert.check(this);
        }
    }

    public void download(Attachment attachment) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException {
        new Download(context, this, attachment);
    }

    /**
     * Send an email to the customer with a new password.
     */
    public void emailNewPassword() throws EmailAddressException, SmtpServerException {

        if (email == null)
            return;

        Config config = Config.getInstance(context);

        String password = setPassword();
        Email e = new Email();

        e.setSubject("Account password");
        e.setBody("We have generated a password for you to use to log into your account. Your password is: " + password + "\n\n");
        e.setTo(email);
        e.setFrom(config.getServerEmail());
        new EmailDecorator(context, e).send(config.getSmtpHostname());
    }

    /**
     * Send an email with details of this item to the associated owner
     *
     * @throws SmtpServerException
     * @throws EmailAddressException
     */
    public void emailOwner(User user) throws EmailAddressException, SmtpServerException {

        if (owner == null) {
            return;
        }

        Config config = Config.getInstance(context);

        Email e = new Email();
        e.setBody(user.getName() + " has sent you details of a " + getItemType().getNameLower() + ".\nYou can view it here:\n" + config.getUrl() + "/" +
                new Link(org.sevensoft.ecreator.iface.admin.items.ItemHandler.class, "edit", "item", this));
        e.setTo(getOwner().getEmail());
        e.setSubject("Please view this " + getItemType().getNameLower());
        e.setFrom(config.getServerEmail());

        new EmailDecorator(context, e).send(config.getSmtpHostname());
    }

    /**
     * @throws SmtpServerException
     * @throws EmailAddressException
     */
    private void emailStockNotifications() throws EmailAddressException, SmtpServerException {
        Config config = Config.getInstance(context);
        new StockNotificationEmail(context, this).send(config.getSmtpHostname());
    }

    /**
     *
     */
    public String emailTagsParse(String body) {

        // accounts name
        body = body.replace("[name]", getName());

        body = body.replace("[url]", getUrl());

        //		if (subscriptionLevel != null) {
        //
        //			// number of days until expiry
        //			body = body.replace("[expiry_days]", String.valueOf(getSubscription().getSubscriptionRemainingDays()));
        //
        //			// actual date of expiry
        //			body = body.replace("[expiry_date]", getSubscriptionExpiryDate().toString("dd-MMM-yyyy"));
        //
        //			// link to renewal
        //			body = body.replace("[subscription_url]", Config.getInstance(context).getUrl() + "/" + new Link(SubscriptionHandler.class));
        //
        //		}

        // accounts email
        if (email != null) {
            body = body.replace("[email]", email);
            body = body.replace("[login]", email);
        }

        body = body.replace("[site]", Company.getInstance(context).getName());
        body = body.replace("[id]", getIdString());
        body = body.replace("[loginurl]", getLoginUrl());

        if (body.contains("[password]")) {

            String password = setPassword();
            body = body.replace("[password]", password);

        }

        body = body.replace("[changepassword]", Config.getInstance(context).getUrl() + "/" + new Link(PasswordHandler.class));

        return body;
    }

    public void flagBaskets() {
        Basket.flagItem(context, this);
    }

    public void generateDisplayName() {

        List<String> attributeNames = new ArrayList();
        for (Map.Entry<Attribute, String> entry : getAttributeValues().entrySet()) {

            if (entry.getKey().isDisplayName()) {
                if (entry.getValue() != null && entry.getValue().length() > 0) {
                    attributeNames.add(entry.getValue());
                }
            }
        }

        StringBuilder sb = new StringBuilder();
        if (attributeNames.isEmpty()) {

            sb.append(getName());
            logger.fine("[Item] attributeNames is empty, name=" + name + " " + this);

        } else {

            sb.append(StringHelper.implode(attributeNames, ", ", true));
            logger.fine("[Item] attributeNames=" + attributeNames);
        }

        if (getItemType().getAccountModule().isShowAccountId()) {

            sb.append(" (ID: ");
            sb.append(getIdString());
            sb.append(")");

        }

        displayName = sb.toString();
        logger.fine("[Item] generating display name=" + displayName);
        save();
    }

    public List<AccessGroup> getAccessGroups() {

        Query q = new Query(context, "select ag.* from # ag join # agi on ag.id=agi.accessGroup where agi.item=?");
        q.setTable(AccessGroup.class);
        q.setTable(AccessGroupItem.class);
        q.setParameter(this);
        return q.execute(AccessGroup.class);
    }

    public List<Item> getAccessories() {

        Query q = new Query(context, "select i.* from # i join # a on i.id=a.accessory where a.item=? and i.status=?");
        q.setTable(Item.class);
        q.setTable(Accessory.class);
        q.setParameter(this);
        q.setParameter("Live");
        return q.execute(Item.class);
    }

    public Item getAccessory() {
        List<Item> accessories = getAccessories();
        return accessories.isEmpty() ? null : accessories.get(0);
    }

    /**
     *
     */
    public Item getAccount() {
        return (Item) (account == null ? null : account.pop());
    }

    /**
     * Returns all items that have account prices for this account
     */
    public List<Item> getAccountPricedItems() {

        Query q = new Query(context, "select i.* from # i join # p on i.id=p.item where p.account=? group by i.id");

        q.setTable(Item.class);
        q.setTable(Price.class);

        q.setParameter(this);

        return q.execute(Item.class);
    }

    /**
     * Returns addresses that are active on this account
     */
    public List<Address> getAddresses() {

        if (isBuyer()) {

            // get allowed addresses
            Query q = new Query(context, "select a.* from # a join # ba on a.id=ba.address where ba.buyer=? and a.active=1");
            q.setTable(Address.class);
            q.setTable(BuyerAddress.class);
            q.setParameter(this);
            return q.execute(Address.class);

        } else {

            return SimpleQuery.execute(context, Address.class, "account", this, "active", true);
        }
    }

    /**
     * Returns all addresses, active and deleted, for this account
     */
    public List<Address> getAddressesAll() {
        return SimpleQuery.execute(context, Address.class, "account", this);
    }

    public List<Adjustment> getAdjustments() {
        return SimpleQuery.execute(context, Adjustment.class, "account", this);
    }

    private String getAdminUrl() {
        return Config.getInstance(context).getUrl() + "/" + new Link(org.sevensoft.ecreator.iface.admin.items.ItemHandler.class, "edit", "item", this);
    }

    /**
     * Returns number of days since this item was created
     */
    public int getAge() {

        long t = System.currentTimeMillis() - getDateCreated().getTimestamp();

        // convert to days
        t = t / (1000l * 60 * 60 * 24);

        if (t == 0) {
            return 1;
        } else {
            return (int) t;
        }
    }

    /**
     *
     */
    public String getAgeString() {

        int age = getAge();

        if (age == 1) {
            return "1 day";
        } else {
            return age + " days";
        }
    }

    public Agreement getAgreement() {
        return agreement.pop();
    }

    public List<Img> getAllImages(final ImageType imageType) {
        return (List<Img>) CollectionsUtil.filter(getAllImages(), new Predicate<Img>() {

            public boolean accept(Img e) {
                return e.getImageType().equals(imageType);
            }
        });
    }

    public String getAlt() {
        return name;
    }

    /**
     * Returns the applicable prices to use for this account by searching the price hierarchy in order
     */
    public List<Price> getApplicablePrices(final Item account, final int qty) {

        // check for account prices first
        List<Price> prices = getPrices();
        //there is only statdard price, so the rest prices must be calculated with standard price band
        boolean reloadPrices = pricesRecalculated(prices);
        if (reloadPrices) {
            prices = getPrices();
        }
        if (account != null && account.getPriceBand() != null) {
            List<Price> priceBandPrices = new ArrayList<Price>();
            for (Price price : prices) {
                PriceBand priceBand = price.getPriceBand();
                if (priceBand != null && priceBand.equals(account.getPriceBand()) && price.getAmount() != null) {
                    priceBandPrices.add(price);
                }
            }
            if (!priceBandPrices.isEmpty()) {
                prices = priceBandPrices;
            } else {
                CollectionsUtil.filter(prices, new Predicate<Price>() {
                    public boolean accept(Price p) {
                        return p.getPriceBand() == null;
                    }
                });
            }
        } else {
            CollectionsUtil.filter(prices, new Predicate<Price>() {
                public boolean accept(Price p) {
                    return p.getPriceBand() == null;
                }
            });
        }
        logger.fine("[Item] getting prices defined on this item: " + prices);

        if (!PriceUtil.isAdvancedPricing(this, context)) {
            CollectionsUtil.filter(prices, new Predicate<Price>() {

                public boolean accept(Price e) {
                    return e.equals(getStandardPrice());
                }
            });
        }

        // if no definitions defined on this item, we will search categories until we find one with prices
        if (prices.isEmpty()) {

            Iterator<Category> iter = getCategories().iterator();
            while (iter.hasNext() && prices.isEmpty()) {

                final Category category = iter.next();
                prices = category.getPrices();
                logger.fine("[Item] prices from category=" + category + " prices=" + prices);
            }
        }

        // if still none, check from global pricing
        if (prices.isEmpty()) {
            prices = PriceSettings.getInstance(context).getPrices();
            logger.fine("[Item] global pricing=" + prices);
        }

        /*
           * Filter out any invalid qty breaks
           */
        CollectionsUtil.filter(prices, new Predicate<Price>() {

            public boolean accept(Price e) {
                return e.isValid(qty);
            }
        });
//        if (!PriceUtil.isAdvancedPricing(this, context)) {
//            CollectionsUtil.filter(prices, new Predicate<Price>() {
//
//                public boolean accept(Price e) {
//                    return e.equals(getStandardPrice());
//                }
//            });
//        }

        return prices;
    }

    private boolean pricesRecalculated(List<Price> prices) {
        boolean reloadPrices = false;
        if (PriceSettings.getInstance(context).isUsePriceBreaks()) {
            List<Price> priceList = PriceSettings.getInstance(context).getPrices();
            if (!prices.isEmpty() && prices.size() != priceList.size()) {
                Amount standardPrice = getStandardPrice(prices).getAmount();
                if (getPriceBreaks() != null && getPriceBreaks().equals(PriceSettings.getInstance(context).getPriceBreaks())) {

//                    List<Price> priceList = PriceSettings.getInstance(context).getPrices();
                    for (Price p : priceList) {
                        boolean priceExist = isAddPrice(prices, p);
                        if (!priceExist) {   //because we've already have value for standard price (band=null) and qty=1
                            Money priceDiscount = standardPrice.calculateDiscount(p.getAmount());
                            setSellPrice(p.getPriceBand(), p.getQty(), new Amount(priceDiscount.getAmount()));
                            reloadPrices = true;
                        }
                    }
                }
            }
        }
        return reloadPrices;
    }

    private Price getStandardPrice(List<Price> prices) {
        for (Price p : prices) {
            if (p.getPriceBand() == null && p.getQty() == 1) {
                return p;
            }
        }
        return prices.get(0);
    }

    private boolean isAddPrice(List<Price> prices, Price p) {
        boolean priceExist = false;
        for (Price p1 : prices) {
            if (p1.getQty() == p.getQty() && p1.getPriceBand() == p.getPriceBand()) {
                priceExist = true;
            }
        }
        return priceExist;
    }

    public List<Img> getApprovedImages(final ImageType imageType) {

        return (List<Img>) CollectionsUtil.filter(getApprovedImages(), new Predicate<Img>() {

            public boolean accept(Img e) {
                return e.getImageType().equals(imageType);
            }
        });
    }

    public final int getApprovedReviewsCount() {
        return approvedReviewsCount;
    }

    public int getAttachmentLimit() {
        return 0;
    }

    public Attribute getAttributeFromId(String id) {

        for (Attribute attribute : getAttributes()) {
            if (attribute.getIdString().equals(id)) {
                return attribute;
            }
        }
        return null;
    }

    /**
     * Returns a list of all attributes applicable to this item. Includes attributes from any categories and of course the item class.
     */
    public List<Attribute> getAttributes() {

        // add in global item attributes
        List<Attribute> attributes = ItemTypeSettings.getInstance(context).getAttributes();

        // add in item type attributes
        attributes.addAll(getItemType().getAttributes());

        // add in attributes from all categories
        if (hasCategories()) {

            for (Category category : getCategories()) {

                for (Attribute attribute : category.getAttributes()) {

                    if (!attributes.contains(attribute)) {

                        attributes.add(attribute);
                    }
                }
            }
        }

        return attributes;
    }

    public List<Audit> getAuditTrail() {
        Query q = new Query(context, "select * from # order by id desc limit 50");
        q.setTable(Audit.class);
        q.setParameter(this);
        return q.execute(Audit.class);
    }

    /**
     * Returns a list of alternative items suggested by the system.
     * <p/>
     * It just selects other random items in the same category.
     */
    public List<Item> getAutoAlternatives(int limit) {

        if (!hasCategories()) {
            return null;
        }

        Query q = new Query(context,
                "select i.* from # i join # ci on i.id=ci.item where ci.category=? and i.id!=? and i.status=? and i.itemType=? order by rand()");

        q.setTable(Item.class);
        q.setTable(CategoryItem.class);

        q.setParameter(getCategory());
        q.setParameter(this);
        q.setParameter("Live");
        q.setParameter(itemType);

        return q.execute(Item.class, limit);
    }

    /**
     * Returns a list of alternative items suggested by the system.
     * <p/>
     * It just selects other random items with the same attribute value.
     */
    public List<Item> getAutoAlternatives(Attribute alternativesAttribute, int limit) {
        ItemSearcher searcher = getItemsAttributeSearcher(alternativesAttribute, limit);
        return searcher.getItems();
    }

    private ItemSearcher getItemsAttributeSearcher(Attribute alternativesAttribute, int limit) {
        ItemSearcher searcher = new ItemSearcher(context);

        MultiValueMap<Attribute, String> attributeValues = new MultiValueMap();

        for (String entry : getAttributeValues(alternativesAttribute)) {
            attributeValues.put(alternativesAttribute, entry);
        }

        searcher.setAttributeValues(attributeValues);

        searcher.setStatus("Live");
        searcher.setLimit(limit);

        return searcher;
    }

    /**
     * Returns the availability message which is the current stock status as a string, or leadtimes, etc.
     *
     * @return
     */
    public String getAvailability() {

        StockModule module = getItemType().getStockModule();

        switch (module.getStockControl()) {

            /*
            * When stock control is not used return null
            */
            case None:
                return null;

            case String:

                return getOutStockMsg();

                /*
                 * For cases where stock is used.
                 */

            case Boolean:

                if (isAvailable()) {

                    String string = module.getInStockMsgDefault();

                    // remove [stock] tag for boolean
                    return string.replace("[stock]", "");

                } else {

                    return getOutStockMsg();

                }

            case Manual:
            case RealTime:

                if (isAvailable()) {

                    /*
                      * If we have stock ranges then show the appropriate range.
                      */
                    if (module.hasStockDisplayRanges()) {

                        int max = 0;
                        List<String> ranges = module.getStockDisplayRanges();
                        for (String range : ranges) {

                            String[] split = range.split("-");
                            if (split.length == 2) {

                                int from = Integer.parseInt(split[0]);
                                int to = Integer.parseInt(split[1]);

                                int overriddenStock = 0;
                                boolean stockOverrided = false;
                                for (ItemOption option : this.getOptionSet().getOptions()) {
                                    if (option.isStockOverride()) {
                                        stockOverrided = true;
                                        for (ItemOptionSelection selection : option.getSelections()) {
                                            overriddenStock = overriddenStock + selection.getStock();
                                        }
                                        break;
                                    }
                                }

                                if (!stockOverrided) {
                                    if (from <= availableStock && to >= availableStock) {
                                        return from + " - " + to + " in stock";
                                    }
                                } else {
                                    if (from <= overriddenStock && to >= overriddenStock) {
                                        return from + " - " + to + " in stock";
                                    }
                                }

                                if (to > max) {
                                    max = to;
                                }
                            }
                        }

                        return max + "+ in stock";

                    } else {

                        String string = module.getInStockMsgDefault();

                        int overriddenStock = 0;
                        boolean stockOverrided = false;
                        for (ItemOption option : this.getOptionSet().getOptions()) {
                            if (option.isStockOverride()) {
                                stockOverrided = true;
                                for (ItemOptionSelection selection : option.getSelections()) {
                                    overriddenStock = overriddenStock + selection.getStock();
                                }
                                break;
                            }
                        }

                        if (stockOverrided) {
                            return string.replace("[stock]", String.valueOf(overriddenStock));
                        } else {
                            return string.replace("[stock]", String.valueOf(availableStock));
                        }

                    }

                } else {

                    return getOutStockMsg();

                }
        }

        assert false;
        return null;
    }

    /**
     * Returns the number of stock available to us, as determined by the stock system
     */
    public int getAvailableStock() {
        return availableStock;
    }

    private int getAverage(RatingType ratingType) {

        Query q = new Query(context, "select sum(rating), count(*) from # where item=? and ratingType=?");
        q.setTable(Rating.class);
        q.setParameter(this);
        q.setParameter(ratingType);
        Row row = q.get();

        return (int) Math.round(row.getDouble(0) / row.getDouble(1));
    }

    public List<AvsCheck> getAvsChecks() {

        Query q = new Query(context, "select * from # where account=? order by date");
        q.setTable(AvsCheck.class);
        q.setParameter(this);
        return q.execute(AvsCheck.class);
    }

    @Deprecated
    public Money getBalance() {
        return balance;
    }

    public List<Block> getBlocks() {

        List<Block> blocks = Block.get2(context, this);
        return blocks;
    }

    public BookingSetup getBookingSetup() {
        return BookingSetup.get(context, this);
    }

    public final Branch getBranch() {
        return (Branch) (branch == null ? null : branch.pop());
    }

    public BuyerConfig getBuyerConfig() {
        return BuyerConfig.get(context, this);
    }

    /**
     * Returns the account that owns this buyer
     */
    public final Item getBuyerFor() {
        return (Item) (buyerFor == null ? null : buyerFor.pop());
    }

    public List<Card> getCards() {
        return SimpleQuery.execute(context, Card.class, "account", this, "active", true);
    }

    /**
     * Returns a list of all categories this item is directly placed in.
     */
    public final List<Category> getCategories() {

        if (categoriesCache == null) {

            Query q = new Query(context, "select c.* from # c join # co on c.id=co.category where co." + getClass().getSimpleName() + "=? order by co.id");
            q.setTable(Category.class);
            q.setTable(CategoryItem.class);
            q.setParameter(this);
            categoriesCache = q.execute(Category.class);

        }

        return categoriesCache;
    }

    /**
     * Returns the first category for this item. Categories are in order of when they were added like a normal List.
     */
    public final Category getCategory() {

        if (categoryCached == null) {

            List<Category> categories = getCategories();
            categoryCached = categories.isEmpty() ? null : categories.get(0);
        }

        return (Category) (categoryCached == null ? null : categoryCached.pop());
    }

    public final int getCategoryCount() {
        return categoryCount;
    }

    public final String getCategoryFullName(String sep, boolean includeRoot) {

        if (categoryName == null && categoryCount > 0) {
            setCategoryInfo();
            save();
        }

        if (categoryFullName == null) {
            return null;
        }

        String string = categoryFullName;
        // remove first entry if not requiring root
        if (!includeRoot) {
            string = categoryFullName.replaceFirst(".*?\\|", "");
        }

        return string.replace("|", sep);
    }

    public final String getCategoryName() {
        return categoryName == null ? " Not in any categories" : categoryName;
    }

    public final String getCategoryNames() {
        return categoryNames == null ? "Not in any categories" : categoryNames;
    }

    public final String getCategoryIds() {
        return categoryIds;
    }

    private Money getCheapestPrice(List<Price> prices, int qty) {

        // if no valid prices return zero money
        if (prices.isEmpty()) {
            return new Money(0);
        }

        Money costPrice = getCostPrice();
        Money sellPrice = null;
        for (Price price : prices) {

            if (price.getQty() <= qty) {

                Money money = price.calculateSellPrice(costPrice);
                if (sellPrice == null || money.isLessThan(sellPrice)) {
                    sellPrice = money;
                }
            }
        }

        if (sellPrice == null) {
            return new Money(0);
        }

        return sellPrice;
    }

    private Amount getMaxCategoryDiscount(List<Category> parentCategories) {

        if (parentCategories.isEmpty()) {
            return null;
        }

        Amount maxDiscount = new Amount(0);
        for (Category category : parentCategories) {

            Amount discount = category.getDiscount();
            if (discount != null) {

                if (maxDiscount.isLessThan(discount)) {
                    maxDiscount = discount;
                }
            }
        }
        if (maxDiscount.equals(new Amount(0))) return null;

        return maxDiscount;
    }

    private Amount getCategoryDiscount(Category category) {

        if (category == null) {
            return null;
        }

        Amount maxDiscount = new Amount(0);
        Amount discount = category.getDiscount();

        if (discount != null) {
            maxDiscount = discount;
        }
        return maxDiscount.equals(new Amount(0))? null: maxDiscount;
    }

    public Category getCategoryWithDiscount() {
        List<Category> parentCategories = getCategories();
        if (parentCategories.isEmpty()) {
            return null;
        }

        Amount maxDiscount = new Amount(0);
        Collections.sort(parentCategories, new DiscountComparator());
        Collections.reverse(parentCategories);
        return parentCategories.get(0);
    }

    public List<Order> getCompletedOrders() {
        return SimpleQuery.execute(context, Order.class, "account", this, "completed", true);
    }

    public String getConfirmationCode() {
        return confirmationCode;
    }

    public String getContent() {
        return content;
    }

    public String getContent(int i) {
        return StringHelper.toSnippet(content, i);
    }

    public String getContentStripped() {
        return contentStripped;
    }

    public String getContentStripped(int maxChars) {
        return getContentStripped(maxChars, null);
    }

    public String getContentStripped(int maxChars, String suffix) {
        return StringHelper.toSnippet(contentStripped, maxChars, suffix);
    }

    public Set<User> getContributors() {
        return MessageUtil.getContributors(this);
    }

    public int[] getCoords() {
        return new int[]{x, y};
    }

    /**
     * Returns the applicable cost price rate we should use in calculations
     */
    public Money getCostPrice() {

        /*
           * If cost pricing is disabled then return zero
           */
        if (!getItemType().isCostPricing()) {
            return new Money(0);
        }

        if (costPrice == null || costPrice.isZero()) {
            calculateCostPrice();
        }

        return costPrice;
    }

    /**
     * Returns the cost price for this supplier or null if this supplier has no cost set
     */
    public Money getCostPrice(Supplier supplier) {
        return SimpleQuery.getMoney(context, SupplierCostPrice.class, "costprice", "item", this, "supplier", supplier);
    }

    /**
     * Returns all cost prices for other suppliers, ie, does not include our generic cost price.
     */
    public List<SupplierCostPrice> getCostPrices() {
        return SimpleQuery.execute(context, SupplierCostPrice.class, "item", this);
    }

    public Collection<Supplier> getCostSuppliers() {
        Query q = new Query(context, "select sup.* from # sup join # stk on sup.id=stk.supplier where stk.item=?");
        q.setTable(Supplier.class);
        q.setTable(SupplierCostPrice.class);
        q.setParameter(this);
        return q.execute(Supplier.class);
    }

    public final User getCreatedBy() {
        return (User) (createdBy == null ? null : createdBy.pop());
    }

    public CreditStore getCreditStore() {
        CreditStore c = SimpleQuery.get(context, CreditStore.class, "account", this);
        return c == null ? new CreditStore(context, this) : c;
    }

    public DateTime getDateCreated() {
        if (dateCreated == null) {
            dateCreated = new DateTime();
            save("dateCreated");
        }
        return dateCreated;
    }

    public DateTime getDateUpdated() {
        if (dateUpdated == null) {
            dateUpdated = new DateTime();
            save("dateUpdated");
        }
        return dateUpdated;
    }

    public Delivery getDelivery() {
        return Delivery.get(context, this);
    }

    public String getDescriptionTag() {
        return descriptionTag;
    }

    /**
     * Returns the display name to show on the account screens, to hide users real names
     */
    public String getDisplayName() {

        /*
           * If the accounts module is not active on this item then we will not
           * use display names, as there is no need - this is designed as a feature
           * for member sites
           */

        if (ItemModule.Account.enabled(context, this)) {

            if (displayName == null) {
                generateDisplayName();
            }

            return displayName;
        }

        return name;
    }

    public Collection<Domain> getDomains() {

        Set<Domain> domains = new HashSet();

        domains.addAll(getAccessGroups());

        SubscriptionLevel level = getSubscription().getSubscriptionLevel();
        if (level != null) {
            domains.add(level);
        }

        domains.add(getItemType());

        return domains;
    }

    public List<Download> getDownloads() {
        Query q = new Query(context, "select * from # where account=? and expired=0 order by id desc");
        q.setTable(Download.class);
        q.setParameter(this);
        return q.execute(Download.class);
    }

    /**
     * Returns the attributes a user can edit in their profile
     */
    public List<Attribute> getEditableAttributes() {

        List<Attribute> attributes = getAttributes();
        CollectionsUtil.filter(attributes, new Predicate<Attribute>() {

            public boolean accept(Attribute e) {
                return e.isEditable();
            }
        });
        return attributes;
    }

    public Link getEditRelativeLink() {
        return new Link(ItemHandler.class, "edit", "item", this);
    }

    public String getEditUrl() {
        return Config.getInstance(context).getUrl() + "/" + new Link(ItemHandler.class, "edit", "item", this);
    }

    public String getEmail() {
        return email;
    }

    /**
     * Returns a list of items which are set as favourites for this favourite group
     */
    public List<Favourite> getFavourites(FavouritesGroup favouritesGroup) {

        Query q = new Query(context, "select * from # where favouritesGroup=? and account=?");
        q.setTable(Favourite.class);
        q.setParameter(favouritesGroup);
        q.setParameter(this);
        return q.execute(Favourite.class);
    }

    public String getFeedSrc() {
        return feedSrc;
    }

    public String getFirstName() {

        /*
           * Remove salutation
           */
        Pattern pattern = Pattern.compile("(mrs|miss|mr|ms|dr|prof)\\s", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(name);
        String string = matcher.replaceFirst("");

        /*
           * Split name on spaces and return first entry.
           */

        String[] names = string.split("\\s");
        return names[0];
    }

    public final String getFriendlyUrl() {

        if (friendlyUrl == null || !friendlyUrl.endsWith("html")) {
            makeFriendlyUrl();
            save("friendlyUrl");
        }
        return friendlyUrl;
    }

    public int getImageCount(ImageType imageType) {
        return getAllImages(imageType).size();
    }

    public String getImagePlaceholder() {
        return getItemType().getImageHolder();
    }

    public String getInactiveMessage() {
        return inactiveMessage;
    }

    public Interaction getInteraction() {
        return Interaction.get(context, this);
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public List<Item> getItems() {
        Query q = new Query(context, "select * from # where account=? and status!=?");
        q.setTable(Item.class);
        q.setParameter(this);
        q.setParameter("deleted");
        return q.execute(Item.class);
    }

    public List<Item> getItemsOrdered(String order) {
        Query q = new Query(context, "select * from # where account=? and status!=?");
        q.setTable(Item.class);
        q.setParameter(this);
        q.setParameter("deleted");
        q.order(order);
        return q.execute(Item.class);
    }

    public ItemType getItemType() {
        return (ItemType) (itemType == null ? null : itemType.pop());
    }

    public String getItemTypeName() {
        return getItemType().getName();
    }

    public String getItemTypeNameLower() {
        return getItemTypeName().toLowerCase();
    }

    public String getKeyword() {
        return name;
    }

    public String getKeywords() {
        return keywords;
    }

    public String getLabel() {
        return getName();
    }

    public LastActive getLastActive() {

        if (lastActive == 0) {
            return LastActive.OverMonth;
        }

        // if userplane is activated, I want the time out to be 30 seconds otherwise set the timeout for online now to 5 minutes
        long onlineNowTimeout = LastActive.getOnlineNowTimeout(context);

        if (lastActive > System.currentTimeMillis() - onlineNowTimeout) {
            return LastActive.OnlineNow;
        }

        Date today = new Date();

        if (today.equals(new Date(lastActive))) {
            return LastActive.Today;
        }

        if ((System.currentTimeMillis() - 1000l * 60 * 60 * 24) < lastActive) {
            return LastActive.Within24Hours;
        }

        if (today.removeDays(3).isBefore(lastActive)) {
            return LastActive.Within3Days;
        }

        if (today.removeDays(7).isBefore(lastActive)) {
            return LastActive.Week;
        }

        if (today.removeDays(14).isBefore(lastActive)) {
            return LastActive.Fortnight;
        }

        if (today.removeMonths(1).isBefore(lastActive)) {
            return LastActive.Month;
        }

        return LastActive.OverMonth;
    }

    public DateTime getLastActiveTime() {
        return new DateTime(lastActive);
    }

    public final Msg getLastMessage() {
        return MessageUtil.getLastMessage(context, this);
    }

    public final String getLastMessageAuthor() {
        return lastMessageAuthor;
    }

    public final DateTime getLastMessageDate() {
        return lastMessageDate;
    }

    public String getLastName() {

        String[] names = name.split("\\s");
        return names[names.length - 1];
    }

    public Order getLastOrder() {
        Query q = new Query(context, "select * from # where account=? order by id desc");
        q.setTable(Order.class);
        q.setParameter(this);
        return q.get(Order.class);
    }

    public Payment getLastPayment() {
        Query q = new Query(context, "select * from # where account=? order by date desc");
        q.setTable(Payment.class);
        q.setParameter(this);
        return q.get(Payment.class);
    }

    /**
     * Returns a string this item was last updated
     */
    public String getLastUpdatedString() {

        long t = System.currentTimeMillis() - getDateUpdated().getTimestamp();

        // convert to hours
        t = t / (1000l * 60 * 60);

        if (t == 0) {
            return "Under 1 hour";
        } else if (t < 24) {
            return t + " hours ago";
        } else {
            return t / 24 + " days ago";
        }

    }

    public Listing getListing() {
        return Listing.get(context, this);
    }

    /**
     * Return all listing packages available to this account.
     */
    public SortedSet<ListingPackage> getListingPackages() {

        SortedSet<ListingPackage> packages = new TreeSet();

        // add in all packages that are set to be accessed by this account type
        packages.addAll(getItemType().getAccessableListingPackages());

        /*
           * Add in all packages from our subscription if we have one
           */
        if (getSubscription().hasSubscriptionLevel()) {
            packages.addAll(getSubscription().getSubscriptionLevel().getListingPackages());
        }

        return packages;
    }

    public List<Item> getListings() {
        return SimpleQuery.execute(context, Item.class, "account", this);
    }

    /**
     * Returns a list of subscription levels for this account
     */
    public List<SubscriptionLevel> getLiveSubscriptionLevels() {

        if (!Module.Subscriptions.enabled(context)) {
            return Collections.emptyList();
        }

        SubscriptionModule module = getItemType().getSubscriptionModule();
        return module.getLiveSubscriptionLevels();
    }

    public String getLocation() {
        return location;
    }

    public List<LogEntry> getLogEntries() {
        return LogEntry.get(context, this);
    }

    public String getLogId() {
        return getFullId();
    }

    public String getLoginUrl() {
        return Config.getInstance(context).getUrl() + "/" + LoginHandler.class.getAnnotation(Path.class).value()[0];
    }

    public String getLogName() {
        return getItemType().getName() + " #" + getIdString();
    }

    /**
     * Margin is the percentage of the sale price that is profit. Some programs seem to call this retail markup ?
     */
    public Money getMargin() {
        return (getSellPrice().subtract(getCostPrice())).divide(getSellPrice().getAmount());
    }

    /**
     * Markup is the percentage increase from cost to sale price. Some programs seem to call this simple markup ?
     */
    public Money getMarkup() {
        return (getSellPrice().subtract(getCostPrice())).divide(getSellPrice().getAmount());
    }

    public Meetup getMeetup() {
        return Meetup.get(context, this);
    }

    public int getMessageCount() {
        return messageCount;
    }

    public Collection<String> getMessageEmails() {
        return null;
    }

    public final List<Msg> getMessages() {
        return MessageUtil.getMessages(context, this);
    }

    public String getMessageSubject() {
        return getName();
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public String getName() {
        return name == null ? "No name" : name;
    }

    public int getOptionCount() {
        return optionCount;
    }

    public final OptionGroup getOptionGroup() {
        return (OptionGroup) (optionGroup == null ? null : optionGroup.pop());
    }

    /**
     * Returns the option set for this item
     */
    public OptionSet getOptionSet() {

        OptionSet b = SimpleQuery.get(context, OptionSet.class, "item", this);
        if (b == null) {
            b = new OptionSet(context, this);
        }

        return b;
    }

    /**
     * Return a list of accessories that can be ordered for this account.
     */
    public List<Item> getOrderableAccessories(Item account) {

        List<Item> accessories = getAccessories();
        Iterator<Item> iter = accessories.iterator();
        while (iter.hasNext()) {

            Item accessory = iter.next();
            if (!accessory.isOrderable(account)) {
                iter.remove();
            }
        }

        return accessories;
    }

    public int getOrderCount() {
        Query q = new Query(context, "select count(*) from # where account=? and status!='Cancelled'");
        q.setTable(Order.class);
        q.setParameter(this);
        return q.getInt();
    }

    public int getOrderQtyMax() {
        return orderQtyMax;
    }

    public int getOrderQtyMin() {
        return orderQtyMin;
    }

    public int getOrderQtyMultiple() {
        return orderQtyMultiple;
    }

    public List<Order> getOrders() {
        return getOrders(0, 0);
    }

    /**
     * Returns all non cancelled orders
     */
    public List<Order> getOrders(int start, int limit) {
        Query q = new Query(context, "select * from # where account=? and cancelled=0 order by id");
        q.setTable(Order.class);
        q.setParameter(this);
        return q.execute(Order.class, start, limit);
    }

    /**
     * Returns all orders including cancelled ones.
     */
    public List<Order> getOrdersAll(int start, int limit) {
        Query q = new Query(context, "select * from # where account=? order by id");
        q.setTable(Order.class);
        q.setParameter(this);
        q.setLimit(start, limit);
        return q.execute(Order.class);
    }

    public Money getOriginalSellPrice(Currency currency) {
        return getSellPrice(null, 1, currency, true);
    }

    public Money getOriginalSellPriceInc(Currency currency) {
        return inc(getSellPrice(null, 1, currency, true));
    }

    public Money getOriginalSellPriceVat(Currency currency) {
        return vat(getSellPrice(null, 1, currency, true));
    }

    public Money getOurCostPrice() {
        return ourCostPrice;
    }

    public int getOurStock() {
        return ourStock;
    }

    public String getOurStockString() {
        return String.valueOf(ourStock);
    }

    /**
     * Returns the out of stock msg, aka leadtime
     */
    public String getOutStockMsg() {

        StockModule module = getItemType().getStockModule();

        if (module.hasOutStockMsgOverride()) {
            return module.getOutStockMsgOverride();
        }

        if (outStockMsg == null) {
            return module.getOutStockMsgDefault();
        }

        return outStockMsg;
    }

    public final User getOwner() {
        return (User) (owner == null ? null : owner.pop());
    }

    public final String getPasswordHash() {
        return passwordHash;
    }

    /**
     * Returns all payments made by this account
     *
     * @return
     */
    public List<Payment> getPayments() {
        return SimpleQuery.execute(context, Payment.class, "account", this);
    }

    public final int getPendingReviewsCount() {
        return pendingReviewsCount;
    }

    /**
     * Returns the value of an attribute called postcode
     */
    private String getPostcode() {
        return getAttributeValue("postcode", true);
    }

    public int getPostCount() {
        return postCount;
    }

    public List<Post> getPosts() {
        return getTopic(true).getPosts();
    }

    public final PriceBand getPriceBand() {
        return (PriceBand) (priceBand == null ? null : priceBand.pop());
    }

    public SortedSet<Integer> getPriceBreaks() {
        if (priceBreaks == null || priceBreaks.isEmpty()) {
            priceBreaks = new TreeSet();
            priceBreaks.addAll(PriceSettings.getInstance(context).getPriceBreaks());
        }
//        priceBreaks.add(1);
        return priceBreaks;
    }

    public List<Item> getPriceMovers(int i) {
        return null;
    }

    /**
     * Returns a price override for this item for the account param
     */
    public PriceOverride getPriceOverride(Item account) {
        return null;
    }

    /**
     * Returns all prices definied precisely on this item
     */
    public List<Price> getPrices() {
        return getPrices(null);
    }

    /**
     * Returns prices definied on this item for the account param
     */
    public List<Price> getPrices(Item account) {

        Query q = new Query(context, "select * from # where item=? and account=? order by qty");
        q.setTable(Price.class);
        q.setParameter(this);
        q.setParameter(account == null ? 0 : account.getId());
        return q.execute(Price.class);
    }

    public Pricing getPricing() {
        return Pricing.get(context, this);
    }

    public final List<Msg> getPublicMessages() {
        return MessageUtil.getPublicMessages(context, this);
    }

    /**
     * Returns the last /limit/ number of items bought by this customer in the form of order line objects
     * No object will have a repeated item object.
     */
    public List<OrderLine> getPurchaseHistory(int limit) {

        Query q = new Query(context, "select l.* from # l join # o on l.ord=o.id where l.item>0 and o.account=? group by l.item order by l.ord desc");
        q.setTable(OrderLine.class);
        q.setTable(Order.class);
        q.setParameter(this);
        return q.execute(OrderLine.class, limit);
    }

    public int getRatingAverage() {
        return ratingAverage;
    }

    public Map<Integer, Integer> getRatingAverages() {
        if (ratingAverages == null)
            return Collections.emptyMap();
        return ratingAverages;
    }

    public String getReference() {
        return reference;
    }

    public String getResetCode() {
        return resetCode;
    }

    public ReviewsDelegate getReviewsDelegate() {
        return ReviewsDelegate.get(context, this);
    }

    /**
     * delegates to getRrp(Money money) with money = null
     */
    public Money getRrp() {
        return getRrp(null);
    }

    public Money getRrp(Currency currency) {

        Money money = rrp;
        if (money == null) {
            money = new Money(0);
        }

        return convert(money, currency);
    }

    public Money getRrpDiscount(Item account, int qty, Currency currency) {
        return getRrpInc(currency).subtract(getSellPriceInc(account, qty, currency));
    }

    public Money getRrpDiscountEx(Item account, int qty, Currency currency) {
        return getRrp(currency).subtract(getSellPrice());
    }

    public String getRrpDiscountPercentage(Item account, int qty, Currency currency) {

        if (rrp == null) {
            return "0%";
        }

        double r = rrp.getAmount();
        double p = getSellPriceInc(account, qty, currency).getAmount();

        return new DecimalFormat().format((r - p) / r * 100) + "%";
    }

    public Money getRrpInc(Currency currency) {
        return getRrp(currency).add(getRrpVat(currency));
    }

    public Money getRrpVat(Currency currency) {

        if (Company.getInstance(context).isVatRegistered()) {
            return getRrp(currency).multiply(vatRate / 100d);

        } else {
            return new Money(0);
        }
    }

    /**
     * Returns all this accounts saved searches
     */
    public List<Search> getSearches() {
        return SimpleQuery.execute(context, Search.class, "owner", getFullId());
    }

    /**
     * Returns the sale price for no group and qty 1
     */
    public Money getSellPrice() {
        return getSellPrice(null, 1, null, false);
    }

    /**
     * Convenience method to return the price of this item for this account for qty 1, standard currency
     */
    public Money getSellPrice(Item active) {
        return getSellPrice(account, 1, null, false);
    }

    /**
     * Returns the lowest price for this account (or guest if account is null) for this qty and convert to currency if not null.
     * Use original means return the non promotional price
     */
    public Money getSellPrice(Item account, int qty, Currency currency, boolean useOriginal) {

        // logger.fine("[item] getting sell price for item=" + this + ", account=" + account + ", qty=" + qty + ", currency=" + currency);

        if (!Module.Pricing.enabled(context)) {
            //	logger.fine("[item] pricing module disabled, returning zero money");
            return new Money(0);
        }

        if (!ItemModule.Pricing.enabled(context, this)) {
            //	logger.fine("[item] pricing item module disabled, returning zero money");
            return new Money(0);
        }

        // qty less than 1 is nonsense
        if (qty < 1) {
            qty = 1;
        }

        final PriceSettings ps = PriceSettings.getInstance(context);
        Money sellPrice = null;

        // is this price the standard band (no level, qty 1)
        boolean generic;

        // if we are getting the price for qty 1 and standard band then set generic
        if (qty == 1 || !PriceUtil.isAdvancedPricing(this, context)) {

            // must be standard price for guests!
            if (account == null) {
                generic = true;
            }

            // else it all boils down to if we have a price band or not
            else {
                generic = (account.getPriceBand() == null);
            }

        } else {
            generic = false;
        }

        logger.fine("[Item] generic pricing=" + generic);

        /*if (generic) {

            if (genericSellPrice != null && genericSellPrice.isPositive()) {

                logger.fine("[Item] using generic sell price: " + genericSellPrice);
                sellPrice = genericSellPrice;
            }
        }*/


        List<Price> prices = getApplicablePrices(account, qty);
        sellPrice = getCheapestPrice(prices, qty);
        sellPrice = sellPrice.multiply(getUnit());
        logger.fine("[item] using sell price=" + sellPrice);

        if (generic) {
            genericSellPrice = sellPrice;
            save("genericSellPrice");
            logger.fine("[item] saving generated price as generic=" + genericSellPrice);
        }


        Amount discount = getDiscount(ps);

        if (discount != null) {
            logger.fine("[Item] discounting item by " + discount);
            Money discounted = discount.calculate(sellPrice);
            if (discounted.isLessThan(sellPrice)) {
                sellPrice = sellPrice.minus(discount.calculate(sellPrice));
//                if (sellPrice.isNegative()) {
//                    sellPrice = new Money(0);
//                }
            } else {
                sellPrice = discounted;
            }
        }

        return convert(roundPrice(sellPrice), currency);
    }

    /**
     * DISCOUNTS
     * 1. item discount, if empty then 2
     * 2. chipest category discount, if ampty then 3
     * 3. global discount
     *
     * @param ps Global PriceSettings
     * @return discount
     */
    public Amount getDiscount(PriceSettings ps) {
        Amount discount = null;
        Amount itemDiscount = null;
        Amount categoryDiscount = null;

        if (isDiscountApplied()) {
            return discount;
        }

        if (Module.CategoryAndItemDiscount.enabled(context)) {
            itemDiscount = getPricing().getDiscount();
            Category cat = getCategoryWithDiscount();
            categoryDiscount = getCategoryDiscount(cat);
        }
        Amount globalDiscount = ps.getGlobalDiscount();

        if (itemDiscount != null) {
            discount = itemDiscount;
        } else if (categoryDiscount != null) {
            /**
             * if we have categories discounts get the chipest one
             */
            discount = categoryDiscount;
        } else if (globalDiscount != null) {
            /*
            * if we have a site wide discount, then apply it now
            */
            discount = globalDiscount;

        }
        return discount;
    }

    public Money getSellPriceInc() {
        return getSellPriceInc(null, 1, null);
    }

    public Money getSellPriceInc(Item m, int qty, Currency currency) {
        return inc(getSellPrice(m, qty, currency, false));
    }

    public Money getSellPriceVat(Item account, int qty, Currency currency) {
        return vat(getSellPrice(account, qty, currency, false));
    }

    public String getShopzillaCategory() {

        for (Category category : getCategories()) {
            if (category.hasShopzillaCategory()) {
                return category.getShopzillaCategory();
            }
        }

        return null;
    }

    public String getShortName() {
        return shortName == null || !getItemType().isShortNames() ? getName() : shortName;
    }

    public List<Supplier> getSkuSuppliers() {
        Query q = new Query(context, "select sup.* from # sup join # stk on sup.id=stk.supplier where stk.item=?");
        q.setTable(Supplier.class);
        q.setTable(Sku.class);
        q.setParameter(this);
        return q.execute(Supplier.class);
    }

    public final int getSmsCredits() {
        return smsCredits;
    }

    /**
     * Returns the standard price def set directly on this item
     */
    public Price getStandardPrice() {

        Price price = SimpleQuery.get(context, Price.class, "item", this, "account", 0, "qty", 1);
        return price;
    }

    public Money getStandardPriceInc(Currency currency) {
        Price price = getStandardPrice();
        if (price != null) {
            Money m = inc(getStandardPrice().getAmount().getFixed());
            return convert(roundPrice(m), currency);
        } else return null;
    }

    public Money getStandardPrice(Currency currency) {
        Price price = getStandardPrice();
        if (price != null) {
            Money m = getStandardPrice().getAmount().getFixed();
            return convert(roundPrice(m), currency);
        } else return null;
    }

    /**
     * Before returning the status we should check the status of the listing.
     */
    public String getStatus() {
        return status == null ? null : status.toUpperCase();
    }

    /**
     * Returns the stock available at this supplier
     */
    public int getStock(Supplier supplier) {

        Query q = new Query(context, "select stock from # where item=? and supplier=?");
        q.setTable(SupplierStock.class);
        q.setParameter(this);
        q.setParameter(supplier);
        return q.getInt();
    }

    public final int getStockNotifyLevel() {
        return stockNotifyLevel;
    }

    /**
     * Returns a collection of all suppliers that provide stock to this item.
     */
    public Collection<Supplier> getStockSuppliers() {
        Query q = new Query(context, "select sup.* from # sup join # stk on sup.id=stk.supplier where stk.item=?");
        q.setTable(Supplier.class);
        q.setTable(SupplierStock.class);
        q.setParameter(this);
        return q.execute(Supplier.class);
    }

    public List<Submission> getSubmissions() {

        Query q = new Query(context, "select * from # where account=? and item>0 order by id desc ");
        q.setParameter(this);
        q.setTable(Submission.class);
        return q.execute(Submission.class);
    }

    public Subscription getSubscription() {
        return Subscription.get(context, this);
    }

    public final List<String> getSuggestions() {
        if (suggestions == null) {
            suggestions = new ArrayList();
        }
        return suggestions;
    }

    public String getSummary() {
        return summary;
    }

    /**
     * Returns a usable summary even if the user has not entered a summary directly.
     */
    public String getSummary(int limit, String suffix) {

        if (summary == null) {

            return StringHelper.toSnippet(getContentStripped(), limit, suffix);

        } else {

            return StringHelper.toSnippet(summary, limit, suffix);
        }
    }

    public String getSupplierSku(Supplier supplier) {
        Query q = new Query(context, "select sku from # where item=? and supplier=?");
        q.setTable(Sku.class);
        q.setParameter(this);
        q.setParameter(supplier);
        return q.getString();
    }

    public String getMemberDescription(int limit, String suffix) {
        String memberDesription = getAttributeValue("About me");
        return memberDesription != null ? StringHelper.toSnippet(memberDesription, limit, suffix) : "";
    }

    public List<Sku> getSupplierSkus() {
        return SimpleQuery.execute(context, Sku.class, "item", this);
    }

    /**
     * Returns a list of the stock objects for each supplier.
     */
    public List<SupplierStock> getSupplierStocks() {
        return SimpleQuery.execute(context, SupplierStock.class, "item", this);
    }

    public String getTitleTag() {
        return titleTag;
    }

    public Topic getTopic(boolean create) {

        Topic topic = SimpleQuery.get(context, Topic.class, "item", this);

        if (topic == null && create) {
            Forum forum = getItemType().getForum();
            topic = forum.addTopic(this);
        }

        return topic;
    }

    public String getTrailName() {

        StringBuilder sb = new StringBuilder(50);
        Category category = getCategory();
        if (category != null) {
            sb.append(category.getFullName(" > "));
            sb.append(" > ");
        }

        sb.append(this.name);
        return sb.toString();
    }

    public final int getUnit() {
        return unit < 1 ? 1 : unit;
    }

    public String getUrl() {
        return Config.getInstance(context).getUrl() + "/" + getFriendlyUrl();
    }

    public static String getUrl(RequestContext context, String friendlyUrl, String key) {
        if (friendlyUrl == null) {
            Item item = Item.getByName(context, key);
            return item.getUrl();
        }
        return Config.getInstance(context).getUrl() + "/" + friendlyUrl;
    }

    public String getValue() {
        return getIdString();
    }

    public double getVatRate() {

        if (!Company.getInstance(context).isVatRegistered()) {
            return 0;
        }

        if (!getItemType().isVat()) {
            return 0;
        }

        return vatRate;
    }

    public String getVatRateString() {
        return String.valueOf(vatRate);
    }

    public int getVideoCount() {
        return videoCount;
    }

    public List<Video> getVideos() {
        return Video.get(context, this);
    }

    public List<SubscriptionLevel> getVisibleSubscriptionLevels() {

        if (!Module.Subscriptions.enabled(context)) {
            return Collections.emptyList();
        }

        SubscriptionModule module = getItemType().getSubscriptionModule();
        return module.getLiveSubscriptionLevels();
    }

    public int getVoucherUses(Voucher voucher) {
//        return SimpleQuery.count(context, VoucherUse.class, "account", this, "voucher", voucher);
        return SimpleQuery.count(context, VoucherUser.class, "account", this, "voucher", voucher);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean hasAccessories() {
        return accessoryCount > 0 && ItemModule.Accessories.enabled(context, this);
    }

    public boolean hasAccount() {
        return account != null;
    }

    public boolean hasAddresses() {
        return getAddresses().size() > 0;
    }

    public boolean hasAgreement() {
        return agreement != null;
    }

    public boolean hasApprovedRecording() {
        return SimpleQuery.count(context, Recording.class, "account", this, "status", "approved") > 0;
    }

    public boolean hasBbcWeatherLink() {
        return getPostcode() != null;
    }

    public boolean hasCategories() {
        return categoryCount > 0;
    }

    public boolean hasCategory() {
        return categoryCount > 0;
    }

    public boolean hasContent() {
        return content != null;
    }

    public boolean hasDescriptionTag() {
        return descriptionTag != null;
    }

    public boolean hasDownloads() {
        return getDownloads().size() > 0;
    }

    public boolean hasEmail() {
        return email != null;
    }

    public boolean hasExternalAgreement() {
        return AgreementUtil.hasExternalAgreement(this);
    }

    public boolean hasFavourite(Item item) {
        return false;
    }

    public boolean hasImagePlaceholder() {
        return getItemType().getImageHolder() != null;
    }

    public boolean hasInlineAgreement() {
        return AgreementUtil.hasInlineAgreement(this);
    }

    public boolean hasKeywords() {
        return keywords != null;
    }

    public boolean hasLeadtime() {

        if (getItemType().getStockModule().hasOutStockMsgOverride()) {
            return true;
        }

        return outStockMsg != null;
    }

    /**
     * Returns true if there is at least one listing package this account can use
     */
    public boolean hasListingPackages() {
        return getListingPackages().size() > 0;
    }

    /**
     * Returns true if this account as listings tagged to it
     */
    public boolean hasListings() {
        return SimpleQuery.count(context, Item.class, "account", this) > 0;
    }

    /**
     * Returns true if this account has subscriptions available to him.
     */
    public boolean hasLiveSubscriptionLevels() {
        return getLiveSubscriptionLevels().size() > 0;
    }

    public boolean hasLocation() {
        return location != null;
    }

    public boolean hasMessages() {
        return messageCount > 0;
    }

    public boolean hasMobilePhone() {
        return mobilePhone != null;
    }

    public boolean hasMultimapAerialPhoto() {
        return getPostcode() != null;
    }

    public boolean hasMultimapLink() {
        return getAttributeValue("postcode", true) != null;
    }

    public boolean hasOptionGroup() {
        return optionGroup != null;
    }

    public boolean hasOptions() {
        return optionCount > 0;
    }

    public boolean hasOrderQtyMax() {
        return orderQtyMax > 0;
    }

    public boolean hasOrderQtyMin() {
        return orderQtyMin > 0;
    }

    public boolean hasOwner() {
        return owner != null;
    }

    public boolean hasPassword() {
        return passwordHash != null;
    }

    public boolean hasPosts() {
        return postCount > 0;
    }

    public boolean hasPriceBand() {
        return priceBand != null;
    }

    public boolean hasPriceBreaks() {
        return getPriceBreaks().size() > 1;
    }

    public boolean hasReference() {
        return reference != null;
    }

    public boolean hasResetCode() {
        return resetCode != null;
    }

    /**
     * Returns true if this product has an RRP set and rrp is enabled.
     *
     * @return
     */
    public boolean hasRrp() {
        return rrp.isGreaterThan(0) && getItemType().isRrp();
    }

    public boolean hasRrpDiscount(Item account) {
        Money salePrice = getSellPrice(account, 1, null, false);
        return salePrice.isLessThan(rrp) && getItemType().isRrp();
    }

    public boolean hasSalePrice(Item acc) {
        return ItemModule.Pricing.enabled(context, this) && getSellPrice(acc, 1, null, false).isGreaterThan(0);
    }

    public boolean hasSearchAgents() {
        return searchCount > 0;
    }

    public boolean hasShortName() {
        return shortName != null;
    }

    public boolean hasSmsCredits() {
        return smsCredits > 0;
    }

    public boolean hasSummary() {
        return summary != null && getItemType().isSummaries();
    }

    public boolean hasTitleTag() {
        return titleTag != null;
    }

    /**
     * Returns true if this item has a vat rate set
     */
    public boolean hasVatRate() {
        return vatRate > 0 && Company.getInstance(context).isVatRegistered();
    }

    public boolean hasVisibleSubscriptionLevels() {
        return getVisibleSubscriptionLevels().size() > 0;
    }

    public boolean hasItemType() {
        return itemType != null;
    }

    @Override
    public void imageApproved(Img img) {

        ImageSettings imageSettings = ImageSettings.getInstance(context);
        if (imageSettings.isSendApprovalEmails() && imageSettings.hasApprovedEmailBody() && hasEmail()) {
            try {
                new ApprovedEmail(context, this).send(Config.getInstance(context).getSmtpHostname());
            } catch (EmailAddressException e) {
                e.printStackTrace();
            } catch (SmtpServerException e) {
                e.printStackTrace();
            }
        }
        super.imageApproved(img);
    }

    @Override
    public void imageRejected(Img img) {

        ImageSettings imageSettings = ImageSettings.getInstance(context);
        if (imageSettings.isSendApprovalEmails() && imageSettings.hasApprovedEmailBody() && hasEmail()) {
            try {
                new RejectedEmail(context, this).send(Config.getInstance(context).getSmtpHostname());
            } catch (EmailAddressException e) {
                e.printStackTrace();
            } catch (SmtpServerException e) {
                e.printStackTrace();
            }
        }
        super.imageRejected(img);
    }

    private Money inc(Money money) {

        if (!getItemType().isVat()) {
            return money;
        }

        return VatHelper.inc(context, money, vatRate);
    }

    private boolean isAccountPricing() {
        return false;
    }

    /**
     * Returns true if this item is available in stock either internally or with a supplier.
     */
    public boolean isAvailable() {

        /*
           * If stock is not enabled then always available
           */
        if (!hasStockModule()) {
            return true;
        }

        StockModule module = getItemType().getStockModule();
        switch (module.getStockControl()) {

            /*
            * If stock is disabled or a single text string then always return true;
            */
            case None:
            case String:
                return true;

                /*
                 * For cases where stock is used we require at least one in stock
                 */
            case Boolean:
            case Manual:
            case RealTime:
                int overriddenStock = 0;
                boolean stockOverrided = false;
                for (ItemOption option : this.getOptionSet().getOptions()) {
                    if (option.isStockOverride()) {
                        stockOverrided = true;
                        for (ItemOptionSelection selection : option.getSelections()) {
                            overriddenStock = overriddenStock + selection.getStock();
                        }
                        break;
                    }
                }

                if (stockOverrided) {
                    return overriddenStock > 0;
                } else {
                    return availableStock > 0;
                }
        }

        assert false;
        return false;
    }

    public final boolean isAwaitingModeration() {
        return awaitingModeration;
    }

    public final boolean isAwaitingValidation() {
        return awaitingValidation;
    }

    public final boolean isBackorders() {

        /*
           * If stock is not enabled then always available
           */
        if (!hasStockModule())
            return true;

        StockModule module = getItemType().getStockModule();
        switch (module.getStockControl()) {

            case None:
            case String:
                return true;

            case RealTime:
            case Manual:
            case Boolean:
                return backorders;
        }

        assert false;
        return false;
    }

    public boolean hasStockModule() {
        return ItemModule.Stock.enabled(context, this);
    }

    public final boolean isBrochure() {
        return brochure;
    }

    public boolean isBuyer() {
        return buyerFor != null;
    }

    public boolean isConfirmed() {
        return confirmationCode == null;
    }

    public boolean isDeleted() {
        return "deleted".equalsIgnoreCase(status);
    }

    public boolean isDisabled() {
        return "disabled".equalsIgnoreCase(status);
    }

    public final boolean isEndOfLine() {
        return endOfLine;
    }

    public boolean isFeatured() {
        return featured;
    }

    public boolean isFriendlyUrlLocked() {
        return friendlyUrlLocked;
    }

    public boolean isLive() {
        return "live".equalsIgnoreCase(status);
    }

    @Override
    public boolean isModeratingImages() {
        return getItemType().isModerateImages();
    }

    public boolean isOnline() {
        return getLastActive() == LastActive.OnlineNow;
    }

    /**
     * An item is orderable for an account if it has a sale price, and ordering module is enabled on the item type
     *
     * @return
     */
    public boolean isOrderable(Item active) {

        /*
           * Shopping module must be enabled to allow ordering
           */
        if (!Module.Shopping.enabled(context)) {
            logger.fine("[Item] no sell price, so not orderable");
            return false;
        }

        /*
           * If this item is set to be a brochure item then it cannot be orderable
           */
        if (brochure) {
            logger.fine("[Item] brochurable, so not orderable");
            return false;
        }

        /*
           * Only live items can be ordered
           */
        if (!isLive()) {
            logger.fine("[Item] not live, so not orderable");
            return false;
        }

        // check this item type has ordering enabled
        if (!ItemModule.Ordering.enabled(context, this)) {
            logger.fine("[Item] ordering is disabled, so not orderable");
            return false;
        }

        /*
           * The item must have a sell price above zero.
           */
        if (!getSellPrice(active, 1, null, false).isPositive()) {
            logger.fine("[Item] no sell price, so not orderable");
            return false;
        }

        /*
           * if the item is not available and backorders is disabled then we cannot order
           */
        if (!isAvailable() && !isBackorders()) {
            logger.fine("[Item] not available and no backorders, so not orderable");
            return false;
        }

        return true;
    }

    public boolean isPasswordMatch(String password) {
        return PasswordHelper.isPlainTextMatch(password, passwordHash);
    }

    public boolean isPlainTextPasswordMatch(String t) {
        return PasswordHelper.isPlainTextMatch(t, passwordHash);
    }

    public boolean isPriceBreaks() {
        return PriceUtil.isPriceBreaks(this, context);
    }

    public final boolean isPrioritised() {
        return prioritised;
    }

    /**
     * An item is 'used' if:
     * <p/>
     * - it is present on an order
     * - it is present on a PO
     * - has invoices (is an account)
     * - has orders (is an account)
     * - has payments
     */
    private boolean isUsed() {

        if (SimpleQuery.count(context, OrderLine.class, "item", this) > 0) {
            return true;
        }

        if (SimpleQuery.count(context, Order.class, "account", this) > 0) {
            return true;
        }

        if (SimpleQuery.count(context, Payment.class, "account", this) > 0) {
            return true;
        }

        return false;
    }

    /**
     *
     */
    public boolean isVisible() {
        return !isAwaitingModeration() && !isAwaitingValidation() && !getItemType().isHidden();
    }

    public void lockFriendlyUrl() {

        if (!friendlyUrlLocked) {
            friendlyUrlLocked = true;
            save();
        }
    }

    public void log(String message) {
        new LogEntry(context, this, message);
    }

    public void log(User user, String message) {
        new LogEntry(context, this, user, message);
    }

    /**
     *
     */
    public void login(String sessionId) {

        new AccountSession(context, sessionId, this);
        context.setAttribute("account", this);
    }

    /**
     * Logs out anyone logged in as this account
     */
    public void logout() {

        // remove all sessions
        SimpleQuery.delete(context, AccountSession.class, "item", this);

        // remove from context
        context.removeAttribute("account");
        Basket basket = (Basket) context.getAttribute("basket");
//        if (basket != null) {
//            basket.logout();
//        }
        // check for logout clear baksets
        if (ShoppingSettings.getInstance(context).isClearBasketOnLogout()) {
            if (basket != null) {
                basket.empty();
                basket.logout();
            }
        }
    }

    private void makeFriendlyUrl() {

        friendlyUrl = Seo.getFriendlyUrl(getName()) + "-i" + getIdString() + ".html";

        //		friendlyUrl = Seo.getFriendlyUrl(getName()) + "/i" + getIdString() + ".html";

        //		StringBuilder sb = new StringBuilder();
        //		sb.append(Seo.getFriendlyUrl(getName()));
        //		sb.append("/i");
        //		sb.append(getIdString());
        //		sb.append(".html");
        //
        //		friendlyUrl = sb.toString();
    }

    public synchronized void merge(Item mergee) {

        synchronized (mergee) {

            // payments
            Query q = new Query(context, "update # set account=? where account=?");
            q.setTable(Payment.class);
            q.setParameter(this);
            q.setParameter(mergee);
            q.run();

            // orders
            q = new Query(context, "update # set account=? where account=?");
            q.setTable(Order.class);
            q.setParameter(this);
            q.setParameter(mergee);
            q.run();

            // cards and addresses
            new Query(context, "update # set account=? where account=?").setTable(Address.class).setParameter(this).setParameter(mergee).run();
            new Query(context, "update # set account=? where account=?").setTable(Card.class).setParameter(this).setParameter(mergee).run();

            // buddies
            new Query(context, "update # set account=? where account=?").setTable(Buddy.class).setParameter(this).setParameter(mergee).run();
            new Query(context, "update # set buddy=? where buddy=?").setTable(Buddy.class).setParameter(this).setParameter(mergee).run();

            // pms
            new Query(context, "update # set recipient=? where recipient=?").setTable(PrivateMessage.class).setParameter(this).setParameter(mergee).run();
            new Query(context, "update # set sender=? where sender=?").setTable(PrivateMessage.class).setParameter(this).setParameter(mergee).run();

            // nudges
            new Query(context, "update # set recipient=? where recipient=?").setTable(Wink.class).setParameter(this).setParameter(mergee).run();
            new Query(context, "update # set sender=? where sender=?").setTable(Wink.class).setParameter(this).setParameter(mergee).run();

            mergee.delete();
        }
    }

    /**
     * Do any actions required when a price changes.
     */
    public void priceChange() {

        // price watchers
        if (Module.PriceWatch.enabled(context)) {
            getPricing().doPriceWatch();
        }

        // recalc baskets
        flagBaskets();
    }

    public void refresh() throws IOException, JDOMException {

        if (!Module.Feeds.enabled(context)) {
            return;
        }

        new MidwichRefresher().refresh(this);
    }

    public void rejectAccount() {

        logger.fine("[Item] Rejecting account: " + this);

        RegistrationModule module = getItemType().getRegistrationModule();
        module.reject(this);

        delete();
    }

    public void removeAccessGroup(AccessGroup ag) {
        SimpleQuery.delete(context, AccessGroupItem.class, "item", this, "accessGroup", ag);
    }

    public void removeAccessories() {
        SimpleQuery.delete(context, AccessGroupItem.class, "item", this);

        this.accessoryCount = 0;
        save("accessoryCount");
    }

    public void removeAccessory(Item accessory) {

        SimpleQuery.delete(context, Accessory.class, "item", this, "accessory", accessory);
        setAccessoryCount();
    }

    public void removeAccountPrices() {
        SimpleQuery.delete(context, Price.class, "account", this);
        // flag basket for this account
        Basket.flagAccount(context, this);
    }

    public void removeAccountPricing(Item item) {
        SimpleQuery.delete(context, Price.class, "item", item, "account", this);
        Basket.flagAccount(context, this);
    }

    public void removeAddress(Address address) {
        address.delete();
    }

    public void removeAddresses() {
        Query q = new Query(context, "update # set active=0 where account=?");
        q.setTable(Address.class);
        q.setParameter(this);
        q.run();
    }

    /**
     * Deletes this product from all baskets and flags the baskets for a recalc
     */
    private void removeBasketLines() {

        flagBaskets();
        SimpleQuery.delete(context, BasketLine.class, "item", this);
    }

    public void removeBuyer(Item buyer) {
        buyer.setStatus("Deleted");
    }

    public void removeCard(Card card) {
        card.delete();
    }

    private void removeCards() {
        SimpleQuery.delete(context, Card.class, "account", this);
    }

    public final void removeCategories() {
        removeCategories(getCategories());
    }

    public final void removeCategories(Collection<Category> categories) {

        for (Category cat : categories) {
            SimpleQuery.delete(context, CategoryItem.class, "item", this, "category", cat);
            cat.setLiveCount();
        }

        clearCategoryCaches();
        setCategoryInfo();
        save();

    }

    public final void removeCategory(Category category) {
        removeCategories(Collections.singletonList(category));
    }

    public void removeCostPrice(Supplier supplier, boolean calculate) {

        SimpleQuery.delete(context, SupplierCostPrice.class, "item", this, "supplier", supplier);

        // clear default sale price
        if (calculate) {
            calculateCostPrice();
            clearPriceCaches();
        }
    }

    private void removeCostPrices() {

        SimpleQuery.delete(context, SupplierCostPrice.class, "item", this);

        calculateCostPrice();
        clearPriceCaches();
    }

    public void removeFavourite(Favourite favourite) {
        favourite.delete();
    }

    private void removeFavourites() {
        SimpleQuery.delete(context, Favourite.class, "item", this);
    }

    private void removeFromPromotions() {
        SimpleQuery.delete(context, PromotionItem.class, "item", this);
    }

    public final void removeMessages() {
        MessageUtil.removeMessages(context, this);
    }

    private void removeOptions() {
        getOptionSet().delete();
    }

    public void removePost(Post post) {
        post.delete();
        setPostCount();
        save();
    }

    /**
     * Remove price definitions set on this item
     */
    public void removePrices() {

        SimpleQuery.delete(context, Price.class, "item", this);
        clearPriceCaches();
    }

    private void removeRatings() {
        SimpleQuery.delete(context, Rating.class, "item", this);
    }

    public void removeSearch(Search search) {
        search.delete();
    }

    private void removeSessions() {
        SimpleQuery.delete(context, AccountSession.class, "item", this);
    }

    /**
     * Removes the SKU for this supplier
     */
    private void removeSku(Supplier supplier) {
        SimpleQuery.delete(context, Sku.class, "item", this, "supplier", supplier);
    }

    /**
     * Removes all SKUs
     */
    private void removeSkus() {
        SimpleQuery.delete(context, Sku.class, "item", this);
    }

    public void removeStock(Supplier supplier, boolean calculate) {
        SimpleQuery.delete(context, SupplierStock.class, "item", this, "supplier", supplier);
        if (calculate) {
            calculateAvailableStock();
        }
    }

    public void removeStocks() {
        calculateAvailableStock();
    }

    public void removeVideo(Video video) {
        video.delete();
        setVideoCount();
    }

    /**
     * This method will reset the account.
     * <p/>
     * All cards and addresses will be deleted, and a new random password will be generated which will be returned as a String. The customer is
     * advised to change his password upon next login.
     * <p/>
     * All cards and addresses will have been wiped from customer's viewing
     */
    public String reset() {

        this.resetCode = null;
        final String newPassword = setPassword();
        save();

        removeAddresses();

        return newPassword;
    }

    /**
     * Will reset the account as in calling reset() but will first check the supplied code matches the reset code
     */
    public String reset(String code) {

        if (resetCode == null)
            return null;

        if (code == null)
            return null;

        if (!resetCode.equals(code))
            return null;

        return reset();
    }

    /**
     * Request resetting of account.
     * <p/>
     * Will send email to customer with reset code for later validating.
     */
    public void resetRequest() throws EmailAddressException, SmtpServerException {

        resetCode = RandomHelper.getRandomString(8);
        save();

        sendResetEmail();
    }

    private Money roundPrice(Money sellPrice) {
        return getItemType().getPriceRounding().round(sellPrice, getItemType().getRoundTo());
    }

    @Override
    public synchronized void save() {
        this.dateUpdated = new DateTime();
        super.save();

        HighlightedItemsBlock.clearHtmlCache();
        HighlightedItemsBox.clearHtmlCache();
    }

    public void sendConfirmationEmail() throws EmailAddressException, SmtpServerException {
        new ConfirmationEmail(this, context).send(Config.getInstance(context).getSmtpHostname());
        log("Sent confirmation email");
    }

    private void sendCreationEmail(String email) throws EmailAddressException, SmtpServerException {

        logger.fine("[Item] Sending creation email: " + email);

        Config config = Config.getInstance(context);

        Email e = new Email();
        e.setBody("A new " + getItemType().getNameLower() + " has been created. You can view it here:\n" + getAdminUrl());
        e.setSubject(getItemType().getName() + " created");
        e.setTo(email);
        e.setFrom(config.getServerEmail());
        new EmailDecorator(context, e).send(config.getSmtpHostname());

    }

    /**
     * Sends an email to this account, and will replace place holder tags
     */
    public void sendEmail(String subject, String body) throws EmailAddressException, SmtpServerException {

        if (email == null)
            return;

        body = emailTagsParse(body);

        Email e = new Email();
        e.setSubject(subject);
        e.setBody(body);
        e.setFrom(Config.getInstance(context).getServerEmail());
        e.setTo(email);
        new EmailDecorator(context, e).send(Config.getInstance(context).getSmtpHostname());

    }

    /**
     * Sends an email to this account telling him the listing will expire in x days
     */
    @Deprecated
    public void sendExpiryReminder() throws EmailAddressException, SmtpServerException {
        new ListingExpiryReminderEmail(this, context).send(Config.getInstance(context).getSmtpHostname());
    }

    private void sendResetEmail() throws EmailAddressException, SmtpServerException {

        if (!hasEmail()) {
            return;
        }

        Config config = Config.getInstance(context);
        Company company = Company.getInstance(context);

        Email msg = new Email();
        msg.setSubject("Password reset");
        msg.setFrom(company.getName(), config.getServerEmail());
        msg.setTo(email);
        msg.setBody("Hello,\n\nAs you requested, here is a link to reset your password.\n" + config.getUrl() + "/" +
                new Link(PasswordHandler.class, "reset", "code", resetCode, "email", email) +
                "\n\nIf you did not make this request, then you can safely ignore this email.\n\nRegards,\n" + company.getName());

        new EmailDecorator(context, msg).send(config.getSmtpHostname());
    }

    private void setAccessoryCount() {
        accessoryCount = getAccessories().size();
        save("accessoryCount");
    }

    public void setAccount(Item account) {
        this.account = account;
    }

    public void setAccountPrice(Item account, int i, Amount amount) {
        new Price(context, this, account, i, amount);
    }

    public void setAgreement(Agreement agreement) {
        this.agreement = agreement;
    }

    public final void setAwaitingModeration(boolean awaitingModeration) {
        this.awaitingModeration = awaitingModeration;
    }

    public final void setAwaitingValidation(boolean awaitingValidation) {
        this.awaitingValidation = awaitingValidation;
    }

    public final void setBackorders(boolean backorders) {
        this.backorders = backorders;
    }

    public void setBalance() {

        /*
           * Calculate account balance by adding sales totals and substracting payment total
           */
        Money salesTotal = new Query(context, "select sum(saletotal) from # where account=?").setTable(Order.class).setParameter(this).getMoney();
        Money paymentsTotal = new Query(context, "select sum(amount) from # where account=?").setTable(Payment.class).setParameter(this).getMoney();
        Money adjustmentsTotal = new Query(context, "select sum(amount) from # where account=?").setTable(Adjustment.class).setParameter(this).getMoney();

        balance = paymentsTotal.minus(salesTotal).add(adjustmentsTotal);
        save();

        //		/*
        //		 * Now reset amount paid on all orders by cycling through each order and assigning a total until we have no amounts lefter over
        //		 */
        //		Money remaining = paymentsTotal;
        //		List<Order> orders = getOrders();
        //		Iterator<Order> iter = orders.iterator();
        //		while (iter.hasNext() && remaining != null) {
        //
        //			Order order = iter.next();
        //
        //			// if the order total is greater than the remaining amount to assign the rest of the payments total
        //			if (order.getTotalInc().isGreaterThan(remaining)) {
        //
        //				order.setAmountPaid(remaining);
        //				order.save();
        //
        //				remaining = null;
        //
        //			} else {
        //
        //				order.setAmountPaid(order.getTotalInc());
        //				order.save();
        //
        //				remaining = remaining.minus(order.getTotalInc());
        //			}
        //		}

        //		Query q = new Query(context,
        //				"select sum(amount) from (select saleTotal as amount from # where account=? union select -amount from # where account=?) Z");
        //		q.setTable(Order.class);
        //		q.setTable(Payment.class);
        //		q.setParameter(this);
        //		q.setParameter(this);
        //		balance = q.getMoney();

        //		setPaymentPeriod();

    }

    public boolean setBooleanStock(boolean b) {

        if (ourStock > 0 == b) {
            return false;
        }

        if (b) {
            this.ourStock = 1;
        } else {
            this.ourStock = 0;
        }

        calculateAvailableStock();
        return true;
    }

    public final void setBranch(Branch branch) {
        this.branch = branch;
    }

    public final void setBrochure(boolean b) {

        if (b == brochure)
            return;

        this.brochure = b;
        if (b) {

            for (Basket basket : Basket.getContaining(context, this)) {
                basket.removeLines(this);
            }
        }
    }

    public final void setBuyerFor(Item buyerFor) {
        this.buyerFor = buyerFor;
        save("buyerFor");
    }

    public final void setCategories(List<Category> categories) {

        if (getCategories().equals(categories))
            return;

        removeCategories();
        addCategories(categories);
    }

    public final void setCategoryInfo() {

        categoryCached = null;

        List<Category> categories = getCategories();
        this.categoryCount = categories.size();
        if (categories.isEmpty()) {

            this.categoryNames = null;
            this.categoryIds = null;
            this.categoryFullName = null;
            this.categoryName = null;

        } else {

            this.categoryFullName = categories.get(0).getFullName();
            this.categoryName = categories.get(0).getName();
            this.categoryNames = StringHelper.implode(CollectionsUtil.transform(categories, new Transformer<Category, String>() {

                public String transform(Category obj) {
                    return obj.getName();
                }

            }), ", ", true);
            this.categoryIds = StringHelper.implode(CollectionsUtil.transform(categories, new Transformer<Category, String>() {

                public String transform(Category obj) {
                    return obj.getIdString();
                }

            }), ",", true) + ",";
        }
    }

    public void setContent(String content) {
        this.content = content;
        this.contentStripped = StringHelper.stripNewLines(HtmlHelper.stripHtml(content, " "));
    }

    public void setCostConstraintBands(Collection<Money> c) {
    }

    public final void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public void setDateCreated(DateTime date) {
        this.dateCreated = date;
    }

    public void setDateUpdated(DateTime dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public void setDeliveryRates(Map<Integer, Money> deliveryRates) {
        for (DeliveryOption option : DeliveryOption.get(context)) {
            getDelivery().setDeliveryRate(option, deliveryRates.get(option.getId()));
        }
    }

    public void setDescriptionTag(String descriptionTag) {
        this.descriptionTag = descriptionTag;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public final void setEmail(String email) {
        this.email = email == null ? null : email.trim().toLowerCase();
    }

    public final void setEndOfLine(boolean endOfLine) {
        this.endOfLine = endOfLine;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public void setFeedCount(int i) {
        this.feedCount = i;
    }

    public void setFeedSrc(String feedSrc) {
        this.feedSrc = feedSrc;
    }

    public void setFriendlyUrl(String friendlyUrl) {
        this.friendlyUrl = friendlyUrl;
    }

    public void setFriendlyUrlLocked(boolean friendlyUrlLocked) {
        this.friendlyUrlLocked = friendlyUrlLocked;
    }

    public final void setInactiveMessage(String inactiveMessage) {
        this.inactiveMessage = inactiveMessage;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public void setItemType(ItemType clazz) {

        if (clazz == null) {
            return;
        }

        if (itemType.equals(clazz)) {
            return;
        }

        this.itemType = clazz;

        /*
           * if pricing is not enabled on this new item class remove prices
           */
        if (!ItemModule.Pricing.enabled(context, this)) {
            removePrices();
        }

        if (!itemType.isCostPricing())
            removeCostPrices();

        if (!ItemModule.Images.enabled(context, this)) {
            removeImages();
        }

        if (!ItemModule.Options.enabled(context, this)) {
            getOptionSet().removeOptions();
        }

        // remove all attribute values
        removeAttributeValues();

        save();
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public void setLastActive() {
        this.lastActive = System.currentTimeMillis();
        save();
    }

    public void setLastActive(long lastActive) {
        this.lastActive = lastActive;
    }

    public final void setLastMessageAuthor() {
        this.lastMessageAuthor = MessageUtil.getLastMessageAuthor(context, this);
    }

    public final void setLastMessageDate() {
        this.lastMessageDate = MessageUtil.getLastMessageDate(context, this);
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setLocation(String location, int x, int y) {

        this.location = location;
        this.x = x;
        this.y = y;

        save("location", "x", "y");
    }

    public void setMessageCount() {
        this.messageCount = SimpleQuery.count(context, Msg.class, "item", this);
        save();
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    /**
     * Returns true if name was changed
     */
    public boolean setName(String s) {

        if (s.equals(name)) {
            return false;
        }

        /*
           * If updating the name then update the author column of all posts.
           */
        this.name = s.trim();

        // update author for posts
        Query q = new Query(context, "update # set author=? where account=?");
        q.setTable(Post.class);
        q.setParameter(s);
        q.setParameter(this);
        q.run();

        // as name has changed we need to put the descriptions in the baskets, so flag for a recacl
        flagBaskets();

        // clear content links
        ContentBlock.clear(context);

        // if friendly urls are not locked then set to be recalculated
        if (!friendlyUrlLocked) {
            friendlyUrl = null;
            save();
        }

        /*
           * Might need to re-gen display name
           */
        generateDisplayName();

        return true;
    }

    public void setOptionCount() {
        this.optionCount = getOptionSet().getOptions().size();
    }

    public final void setOptionGroup(OptionGroup optionGroup) {
        this.optionGroup = optionGroup;
    }

    public void setOrderQtyMax(int maxOrderQty) {
        this.orderQtyMax = maxOrderQty;
    }

    public void setOrderQtyMin(int minOrderQty) {
        this.orderQtyMin = minOrderQty;
    }

    public void setOrderQtyMultiple(int orderQtyMultiple) {
        this.orderQtyMultiple = orderQtyMultiple;
    }

    /**
     * Sets our generic cost price for this item.
     */
    public void setOurCostPrice(Money money) {

        if (ObjectUtil.equal(money, ourCostPrice)) {
            return;
        }

        this.ourCostPrice = money;
        save("ourCostPrice");

        calculateCostPrice();
        clearPriceCaches();
    }

    /**
     * Sets the generic stock level
     *
     * @return
     */
    public boolean setOurStock(int s) {

        if (ourStock == s)
            return false;

        this.ourStock = s;
        calculateAvailableStock();
        return true;
    }

    public void setOutStockMsg(String s) {
        this.outStockMsg = (s == null ? null : s.trim());
    }

    public final void setOwner(User owner) {
        this.owner = owner;
    }

    /**
     * Set the password to something randomly generated.
     * Returns the password generated
     */
    public String setPassword() {

        String random = RandomHelper.getRandomString(8);
        logger.fine("generating random password: " + random.replaceAll(".", "*"));

        setPassword(random);

        return random;
    }

    /**
     * Sets the password to this plain text one and stores the hashed value
     */
    public void setPassword(String plainTextPassword) {

        this.passwordHash = PasswordHelper.generatePasswordHash(plainTextPassword);
        logger.fine("setting new password plain=" + plainTextPassword.replaceAll(".", "*") + ", hash=" + passwordHash);

        save();
    }

    public final void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public void setPostCount() {
        postCount = SimpleQuery.count(context, Post.class, "account", this);
        save();
    }

    public final void setPriceBand(PriceBand priceBand) {
        this.priceBand = priceBand;
    }

    /**
     * Returns true if the price breaks have changed
     */
    public boolean setPriceBreaks(Collection<Integer> i) {

        this.priceBreaks = new TreeSet();
        this.priceBreaks.addAll(i);

        save("priceBreaks");
        PriceUtil.syncPriceBreaks(context, this, i);
        return true;
    }

    public final void setPrioritised(boolean prioritised) {
        this.prioritised = prioritised;
    }

    public void setRatingsAverages(Map<Integer, Integer> ratingsAverages) {
        this.ratingAverages = ratingsAverages;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    public void setRrp(Money rrp) {
        this.rrp = rrp;
    }

    public void setSellPrice(Amount amount) {
        setStandardPrice(amount);
    }

    public void setSellPrice(Money price) {
        setStandardPrice(new Amount(price));
    }

    public void setSellPrice(PriceBand priceBand, int qty, Amount amount) {
        new Price(context, this, priceBand, qty, amount);
        clearPriceCaches();
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public final void setSmsCredits(int smsCredits) {
        this.smsCredits = smsCredits;
    }

    /**
     * Sets the standard price for this item. Standard is 1 qty, no price band
     */
    public void setStandardPrice(Amount amount) {

        SimpleQuery.delete(context, Price.class, "item", this, "account", 0, "qty", 1, "priceBand", 0);
        if (amount != null) {
            new Price(context, this, (PriceBand) null, 1, amount);
        }
        clearPriceCaches();
    }

    public void setStatus(String s) {

        if (status == s) {
            return;
        }

        this.status = s;
        save("status");

        /*
           * if this item is now disabled remove from baskets and remove listing details
           */
        if (isDisabled()) {

            removeBasketLines();

        }

        if (isDeleted()) {

            removeBasketLines();

            /*
                * Remove forum posts
                */
            Topic topic = getTopic(false);
            if (topic != null) {
                topic.delete();
            }

        }

        if (isLive()) {

            /*
                * if changing to live we should reset the cached sale prices
                */
            clearPriceCaches();
        }

        for (Category category : getCategories()) {
            category.setLiveCount();
        }
    }

    public final void setStockNotifyLevel(int stockNotifyLevel) {
        this.stockNotifyLevel = stockNotifyLevel;
    }

    public final void setSuggestions(List<String> suggestions) {
        this.suggestions = suggestions;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setSupplierCostPrice(Supplier supplier, Money money) {

        removeCostPrice(supplier, false);

        if (money != null && money.isPositive()) {
            new SupplierCostPrice(context, this, supplier, money);
        }

        calculateCostPrice();
        clearPriceCaches();

    }

    public void setSupplierSku(Supplier supplier, String sku) {

        if (supplier == null)
            return;

        removeSku(supplier);
        if (sku != null)
            new Sku(context, supplier, this, sku);
    }

    /**
     * Sets the stock level available at this supplier
     */
    public void setSupplierStock(Supplier supplier, int stock) {

        if (supplier == null) {

            setOurStock(stock);

        } else {

            removeStock(supplier, false);
            if (stock > 0) {
                new SupplierStock(context, this, supplier, stock);
            }

            calculateAvailableStock();
        }
    }

    public void setTitleCase() {
        this.name = StringHelper.toTitleCase(name);
    }

    public void setTitleTag(String titleTag) {
        this.titleTag = titleTag;
    }

    public final void setUnit(int unit) {

        if (this.unit == unit) {
            return;
        }

        this.unit = unit;
        clearPriceCaches();
    }

    public boolean setVatRate(double d, boolean flagBaskets) {

        if (d == vatRate) {
            return false;
        }

        this.vatRate = d;
        save("vatRate");

        // if the vat rate has changed then flag the baskets for a recalc
        flagBaskets();

        return true;
    }

    public void setVideoCount() {
        this.videoCount = Video.getCount(context, this);
        save("videoCount");
    }

    public final void setX(int x) {
        this.x = x;
    }

    public final void setY(int y) {
        this.y = y;
    }

    public boolean isExcludeFromGoogleFeed() {
        return excludeFromGoogleFeed;
    }

    public void setExcludeFromGoogleFeed(boolean excludeFromGoogleFeed) {
        this.excludeFromGoogleFeed = excludeFromGoogleFeed;
    }

    public long getOpenRangeProductId() {
        return openRangeProductId;
    }

    public void setOpenRangeProductId(long openRangeProductId) {
        this.openRangeProductId = openRangeProductId;
    }

    public boolean useImageDescriptions() {
        return true;
    }

    /**
     * Adds VAT if applicable to the amount passed in
     */
    private Money vat(Money money) {

        if (Company.getInstance(context).isVatRegistered()) {
            if (getItemType().isVat()) {
                return money.multiply(vatRate / 100d);
            }
        }
        return money;
    }

    public boolean isEvent() {
        return getItemType().isFullEvent();
    }

    public boolean isBookable() {
        return getItemType().isBookable();
    }

    public int getCommission() {
        if (isBookable()) {
            String attributeValue = getAttributeValue("Commission");
            if (attributeValue != null) {
                return Integer.parseInt(attributeValue);
            }
        }
        return 0;
    }

    public boolean isComments() {
        return comments;
    }

    public void setComments(boolean comments) {
        this.comments = comments;
    }

    public boolean isDiscountApplied() {
        return discountApplied;
    }

    public void setDiscountApplied(boolean discountApplied) {
        this.discountApplied = discountApplied;
    }

    public void addComment(RequestContext context, String title, String commentString) {
        Comment comment = new Comment(context, null);
        comment.setAwaitingModeration(false);
        comment.setItem(this);
        comment.setTitle(title);
        comment.setComment(commentString);
        comment.save();
    }

    public boolean isInFamily() {
//        for(Attribute attr: getAttributes()){
//            if (attr.getType().equals(AttributeType.Family)){
//                 return true;
//            }
//        }
//        return false;
        return getAttribute(FAMILY_ATTRIBUTE) != null && getAttributeValue(FAMILY_ATTRIBUTE) != null;
    }

    public MultiValueMap<Attribute, String> getFamily() {
        MultiValueMap<Attribute, String> map = new MultiValueMap<Attribute, String>();
        map.put(getAttribute(FAMILY_ATTRIBUTE), getAttributeValue(FAMILY_ATTRIBUTE));
        return map;
    }

    public void applyDiscount(Amount discount) {
        List<Price> prices = getPrices();

        for (Price price : prices) {
            Money priceAmount = price.getAmount().getFixed();
            Money discounted = discount.calculate(priceAmount);
            if (discounted.isLessThan(priceAmount)) {
                priceAmount = priceAmount.minus(discount.calculate(priceAmount));
            } else {
                priceAmount = discounted;
            }
            setSellPrice(price.getPriceBand(), price.getQty(), new Amount(priceAmount));
            price.delete();
        }
        setDiscountApplied(true);
        
        save("discountApplied");
    }

    public boolean isListingPaymentDone() {
        return listingPaymentDone;
    }

    public void setListingPaymentDone(boolean listingPaymentDone) {
        this.listingPaymentDone = listingPaymentDone;
        save("listingPaymentDone");
    }

    public String getListingPaymentDone() {
        String paymentDone = String.valueOf(isListingPaymentDone());
        if (isListingPaymentDone()) {
            int expirydate = getListing().getExpiryDays();
//            boolean isfree = false;
            if (getListing().getListingPackage() != null) {
                List<ListingRate> rates = getListing().getListingPackage().getListingRates();
                if (!rates.isEmpty()) {
                    for (ListingRate rate : rates) {
                        if (rate.isFree() && expirydate <= rate.getPeriod()) {
                            paymentDone = "Free";
                            break;
                        }
                    }
                }
            }

        }
        return paymentDone;
    }

    public boolean isShowOptionsList() {
        return showOptionsList;
    }

    public void setShowOptionsList(boolean showOptionsList) {
        this.showOptionsList = showOptionsList;
    }

    public boolean isMobilePnoneEnabled() {
        return !getItemType().isMobilePhoneDisable();
    }


}
