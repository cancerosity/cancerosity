package org.sevensoft.ecreator.model.items.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * User: Tanya
 * Date: 07.03.2011
 */
public class ItemTypeMarker extends MarkerHelper implements IItemMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

        return super.string(context, params, item.getItemTypeName());
    }

    public Object getRegex() {
        return "item_type_name";
    }

}
