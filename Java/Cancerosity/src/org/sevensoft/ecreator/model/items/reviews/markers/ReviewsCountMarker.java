package org.sevensoft.ecreator.model.items.reviews.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 11 Aug 2006 15:54:06
 * 
 * 
 *
 */
public class ReviewsCountMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		if (!ItemModule.Reviews.enabled(context, item)) {
			logger.fine("[ReviewsCountMarker] reviews disabled");
			return null;
		}

		int i = item.getApprovedReviewsCount();
		return super.string(context, params, String.valueOf(i));
	}

	public String getDescription() {
		return "Shows the number of reviews this item has";
	}

	public Object getRegex() {
		return "reviews_count";
	}

}
