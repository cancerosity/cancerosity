package org.sevensoft.ecreator.model.items.listings.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.items.listings.MyListingsHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 26 Jun 2006 09:53:32
 *
 */
public class ListingsMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.Listings.enabled(context)) {
			return null;
		}

		Item account = (Item) context.getAttribute("account");
		if (account == null) {
			return null;
		}

		return super.link(context, params, new Link(MyListingsHandler.class), "account_link");
	}

	public Object getRegex() {
		return new String[] { "listings", "account_listings" };
	}

}
