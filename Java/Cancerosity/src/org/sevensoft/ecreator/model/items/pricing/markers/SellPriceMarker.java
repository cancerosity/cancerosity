package org.sevensoft.ecreator.model.items.pricing.markers;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketLineMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;
import java.util.regex.Matcher;

/**
 * @author sks 20 Apr 2006 17:32:32
 *         Shows our current selling price taking into account discounts
 */
public class SellPriceMarker extends MarkerHelper implements IItemMarker, IBasketLineMarker {

    public Object generate(RequestContext context, Map<String, String> params, BasketLine line) {

        Currency currency = (Currency) context.getAttribute("currency");
        boolean inc = params.containsKey("inc");

        if (inc) {
            return currency.getSymbol() + currency.convert(line.getSalePriceInc());
        } else {
            return currency.getSymbol() + currency.convert(line.getSalePriceEx());
        }

    }

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

        Item account = (Item) context.getAttribute("account");
        Currency currency = (Currency) context.getAttribute("currency");

        if (!PermissionType.ItemPriceSummary.check(context, account, item.getItemType())) {
            return null;
        }

        if (params.containsKey("currency")) {
            currency = EntityObject.getInstance(context, Currency.class, params.get("currency"));
        }

        boolean inc = params.containsKey("inc");
        String suffix = params.get("suffix");
        String prefix = params.get("prefix");
        //		String prefix_promotion = params.get("prefix_promotion");
        int dp;
        if (params.containsKey("dp")) {
            dp = Integer.parseInt(params.get("dp").trim());
        } else {
            dp = 2;
        }

        if (params.containsKey("price_max")) {
            account = null;
        }

        Money salePrice;
        if (inc) {
            salePrice = item.getSellPriceInc(account, 1, currency);
        } else {
            salePrice = item.getSellPrice(account, 1, currency, false);
        }

        if (salePrice.isZero()) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        sb.append("<span class='");

        if (inc) {
            sb.append("price_inc");
        } else {
            sb.append("price");
        }

        sb.append("'>");

        //		if (prefix_promotion != null && item.hasPromotionDiscount()) {
        //			sb.append(prefix_promotion);
        //		} else

        if (prefix != null) {
            sb.append(prefix);
        }

        sb.append(currency.getSymbol());
//        sb.append(salePrice.toString(dp).replace("$", "\\$"));
        sb.append(Matcher.quoteReplacement(salePrice.toString(dp)));

        if (suffix != null) {
            sb.append(suffix);
        }

        sb.append("</span>");
        return sb;
    }

    public Object getRegex() {
        return "pricing_sell";
    }

}
