package org.sevensoft.ecreator.model.items.pricing.markers;

import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.skint.Money;

import java.util.Map;

/**
 * User: Tanya
 * Date: 05.07.2012
 */
public class OrigPriceForCategoryDiscountModuleMarker implements IItemMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
        if (!Module.CategoryAndItemDiscount.enabled(context)) {
            return null;
        }
        if (item.getCategoryWithDiscount().isHideDiscountInfo()) {
            return null;
        }
        Currency currency = (Currency) context.getAttribute("currency");
        String currencySymbol = (String) context.getAttribute("currencySymbol");

        // get our original price
        Money originalSellPrice;
        if (params.containsKey("inc"))
            originalSellPrice = item.getStandardPrice(currency);
        else
            originalSellPrice = item.getStandardPriceInc(currency);

        String prefix = params.get("prefix");
        String suffix = params.get("suffix");

        StringBuilder sb = new StringBuilder();
        sb.append("<span class='price_original'>");

        if (prefix != null)
            sb.append(prefix);

        sb.append("<span class='theprice'>");
        sb.append(currencySymbol);
        sb.append(originalSellPrice);
        sb.append("</span>");

        if (suffix != null)
            sb.append(suffix);

        sb.append("</span>");
        return sb.toString();
    }

    public Object getRegex() {
        return "category_discount_original_price";
    }
}
