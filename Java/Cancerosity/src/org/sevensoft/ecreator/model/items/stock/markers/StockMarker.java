package org.sevensoft.ecreator.model.items.stock.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketLineMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.stock.StockModule;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;

/**
 * @author sks 24 Jun 2006 08:21:28
 *
 */
public class StockMarker extends MarkerHelper implements IItemMarker, IBasketLineMarker {

	public Object generate(RequestContext context, Map<String, String> params, BasketLine line) {

		Item item = line.getItem();
		if (item == null) {
			return null;
		}

		return generate(context, params, item);
	}

	private Object generate(RequestContext context, Map<String, String> params, Item item) {

		// check for stock item module first
		if (!ItemModule.Stock.enabled(context, item)) {
			return null;
		}

		StockModule module = item.getItemType().getStockModule();
		if (!module.isStockControl()) {
			return null;
		}

		String availability = item.getAvailability();
		if (availability == null) {
			return null;
		}

		String prefix = params.get("prefix");
		String suffix = params.get("suffix");

		boolean icon = params.containsKey("icon");
		boolean text = params.containsKey("text") || !icon;

		StringBuilder sb = new StringBuilder();
		sb.append("<span class='stock'>");

		if (prefix != null)
			sb.append(prefix);

		if (icon) {

			ImageTag imageTag = null;

			if (item.isAvailable() && module.hasInStockIcon()) {
				imageTag = new ImageTag("template-data/" + module.getInStockIcon());
			}

			if (!item.isAvailable() && module.hasOutStockIcon()) {
				imageTag = new ImageTag("template-data/" + module.getOutStockIcon());
			}

			if (imageTag != null) {
				imageTag.setAlign("absmiddle");
				sb.append(imageTag);
			}

		}

		if (text) {
			sb.append(availability);
		}

		if (suffix != null) {
			sb.append(suffix);
		}

		sb.append("</span>");
		return sb.toString();
	}

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
		return generate(context, params, item);
	}

	public Object getRegex() {
		return new String[] { "stock", "availability" };
	}
}
