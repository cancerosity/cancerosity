package org.sevensoft.ecreator.model.items.favourites.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.favourites.Favourite;
import org.sevensoft.ecreator.iface.frontend.items.favourites.FavouritesnewHandler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

import java.util.Map;

/**
 * User: Tanya
 * Date: 23.08.2010
 */
public class FavouriteRemoveMarker extends MarkerHelper implements IItemMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
        Item account = (Item) context.getAttribute("account");
        if(account == null){
            return null;
        }
        if (!Favourite.contains(context, account, item)) {
            return null;
        }

        Link link = new Link(FavouritesnewHandler.class, "remove", "item", item);
        return super.link(context, params, link, null);
    }

    public Object getRegex() {
        return "favourites_remove";
    }

}
