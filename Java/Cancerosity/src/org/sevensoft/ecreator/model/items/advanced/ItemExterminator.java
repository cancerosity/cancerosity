package org.sevensoft.ecreator.model.items.advanced;

import org.sevensoft.ecreator.model.accounts.permissions.AccessGroupItem;
import org.sevensoft.ecreator.model.attributes.AttributeValue;
import org.sevensoft.ecreator.model.categories.CategoryItem;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLineOption;
import org.sevensoft.ecreator.model.ecom.suppliers.Sku;
import org.sevensoft.ecreator.model.extras.ratings.Rating;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.ecreator.model.items.pricing.Price;
import org.sevensoft.ecreator.model.items.pricing.costs.SupplierCostPrice;
import org.sevensoft.ecreator.model.items.pricing.watchers.PriceWatch;
import org.sevensoft.ecreator.model.items.reviews.Review;
import org.sevensoft.ecreator.model.items.stock.SupplierStock;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.products.accessories.Accessory;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 2 Oct 2006 16:37:17
 *
 */
public class ItemExterminator {

	private final RequestContext	context;
	private ItemType			itemType;

	public ItemExterminator(RequestContext context) {
		this.context = context;
	}

	public ItemExterminator(RequestContext context, ItemType itemType) {
		this.context = context;
		this.itemType = itemType;
	}

	public void delete() {

		if (itemType == null) {
			SimpleQuery.delete(context, Item.class);
		} else {

            new Query(context, "delete img.* from # img left outer join # i on img.item=i.id where i.itemType=?").setTable(Img.class)
                    .setTable(Item.class).setParameter(itemType).run();

            SimpleQuery.delete(context, Item.class, "itemType", itemType);
		}

		// delete dangling item options
		new Query(context, "delete io.* from # io left outer join # i on io.item=i.id where i.id is null").setTable(ItemOption.class).setTable(Item.class)
				.run();

		// delete dangling item options
		new Query(context, "delete ios.* from # ios left outer join # io on ios.itemOption=io.id where io.id is null").setTable(ItemOptionSelection.class)
				.setTable(ItemOption.class).run();

		SimpleQuery.delete(context, Price.class);
		SimpleQuery.delete(context, PriceWatch.class);
		SimpleQuery.delete(context, Accessory.class);

		SimpleQuery.delete(context, SupplierCostPrice.class);
		SimpleQuery.delete(context, SupplierStock.class);
		SimpleQuery.delete(context, Sku.class);
		SimpleQuery.delete(context, AccessGroupItem.class);

		SimpleQuery.delete(context, Basket.class);
		SimpleQuery.delete(context, BasketLine.class);
		SimpleQuery.delete(context, BasketLineOption.class);

		SimpleQuery.delete(context, Rating.class);
		SimpleQuery.delete(context, Review.class);

		new Query(context, "delete ci.* from # ci left outer join # i on ci.item=i.id where i.id is null").setTable(CategoryItem.class)
				.setTable(Item.class).run();

		new Query(context, "delete av.* from # av left outer join # i on av.item=i.id where i.id is null").setTable(AttributeValue.class).setTable(
				Item.class).run();

	}
}
