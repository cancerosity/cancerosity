package org.sevensoft.ecreator.model.items.favourites;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.items.renderers.ListMarkupDefault;

import java.io.IOException;

/**
 * User: Tanya
 * Date: 23.08.2010
 */
@Table("items_favourites_settings")
@Singleton
public class FavouritesSettings extends EntityObject {

    private Markup markup;

    protected FavouritesSettings(RequestContext context) {
        super(context);
    }

    public static FavouritesSettings getInstance(RequestContext context) {
        return EntityObject.getSingleton(context, FavouritesSettings.class);
    }

    public Markup getMarkup() {
        if (markup == null) {
            ListMarkupDefault markupDefault = new ListMarkupDefault();
            markup = new Markup(context, "Favourites list markup", markupDefault);
            try {
                markup.setCss(markupDefault.getCss());
            } catch (IOException e) {
                e.printStackTrace();
            }
            markup.setTableClass(markupDefault.getCssClass());
            markup.setCycleRows(markupDefault.getCycleRows());
            markup.save();
            save();
        }
        return markup.pop();
    }

    public void setMarkup(Markup markup) {
        this.markup = markup;
    }
}
