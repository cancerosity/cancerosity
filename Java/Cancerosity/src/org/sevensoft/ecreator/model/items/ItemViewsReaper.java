package org.sevensoft.ecreator.model.items;

import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 21 May 2007 14:53:08
 *
 */
public class ItemViewsReaper implements Runnable {

	private RequestContext	context;

	public ItemViewsReaper(RequestContext context) {
		this.context = context;
	}

	public void run() {

		SimpleQuery.delete(context, ItemViews.class);
		
	}

}
