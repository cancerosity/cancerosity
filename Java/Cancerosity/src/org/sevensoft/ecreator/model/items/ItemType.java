package org.sevensoft.ecreator.model.items;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.superstrings.english.Pluraliser;
import org.sevensoft.ecreator.model.accounts.AccountModule;
import org.sevensoft.ecreator.model.accounts.permissions.*;
import org.sevensoft.ecreator.model.accounts.profile.ProfileModule;
import org.sevensoft.ecreator.model.accounts.registration.RegistrationModule;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionModule;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.attributes.support.AttributeOwnerSupport;
import org.sevensoft.ecreator.model.bookings.BookingModule;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.containers.blocks.BlockUtil;
import org.sevensoft.ecreator.model.crm.forms.blocks.FormBlock;
import org.sevensoft.ecreator.model.crm.forum.Forum;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.template.Template;
import org.sevensoft.ecreator.model.design.template.TemplateConfig;
import org.sevensoft.ecreator.model.ecom.boxoffice.BoxOffice;
import org.sevensoft.ecreator.model.ecom.credits.CreditPackage;
import org.sevensoft.ecreator.model.ecom.credits.CreditPackageAccess;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.shopping.ShoppingModule;
import org.sevensoft.ecreator.model.extras.links.ExternalLink;
import org.sevensoft.ecreator.model.extras.links.LinkType;
import org.sevensoft.ecreator.model.extras.mapping.google.GoogleMapIcon;
import org.sevensoft.ecreator.model.extras.ratings.RatingModule;
import org.sevensoft.ecreator.model.feeds.bespoke.events.CamdramFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.events.VueFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.news.PdoNewsFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.prices.base.GoogleBase;
import org.sevensoft.ecreator.model.feeds.bespoke.prices.pricegrabber.PriceGrabber;
import org.sevensoft.ecreator.model.feeds.bespoke.products.midwich.MidwichFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.products.vip.VipFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.property.propertyfinder.PropertyFinderFeed;
import org.sevensoft.ecreator.model.feeds.csv.CsvImportFeed;
import org.sevensoft.ecreator.model.interaction.InteractionModule;
import org.sevensoft.ecreator.model.items.adminsearch.AdminField;
import org.sevensoft.ecreator.model.items.adminsearch.AdminFieldType;
import org.sevensoft.ecreator.model.items.alternatives.AlternativesBlock;
import org.sevensoft.ecreator.model.items.alternatives.AlternativesModule;
import org.sevensoft.ecreator.model.items.listings.ListingModule;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;
import org.sevensoft.ecreator.model.items.listings.ListingPackageAccess;
import org.sevensoft.ecreator.model.items.options.OptionsModule;
import org.sevensoft.ecreator.model.items.options.OptionSet;
import org.sevensoft.ecreator.model.items.pricing.PriceSettings.PriceRounding;
import org.sevensoft.ecreator.model.items.pricing.costs.CostPriceSystem;
import org.sevensoft.ecreator.model.items.relations.RelationsModule;
import org.sevensoft.ecreator.model.items.renderers.*;
import org.sevensoft.ecreator.model.items.reviews.ReviewsBlock;
import org.sevensoft.ecreator.model.items.reviews.ReviewsModule;
import org.sevensoft.ecreator.model.items.sorts.ItemSort;
import org.sevensoft.ecreator.model.items.stock.StockModule;
import org.sevensoft.ecreator.model.media.MediaModule;
import org.sevensoft.ecreator.model.media.videos.blocks.VideosBlock;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.ecreator.model.products.accessories.AccessoriesMarkupDefault;
import org.sevensoft.ecreator.model.products.ordering.OrderingModule;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

import java.util.*;
import java.io.IOException;

/**
 * @author sks 14-Mar-2006 18:58:00
 */
@Table("items_types")
public class ItemType extends AttributeOwnerSupport implements Logging, Domain, Positionable, Selectable, Comparable<ItemType>, Permissions, BlockOwner {

    public static List<ItemType> get(RequestContext context) {
        Query q = new Query(context, "select * from # where deleted=0 order by name");
        q.setTable(ItemType.class);
        return q.execute(ItemType.class);
    }

    public static Map<String, String> getSelectionMap(RequestContext context) {
        Query q = new Query(context, "select id, namePlural from # where deleted=0 order by name");
        q.setTable(ItemType.class);
        return q.getMap(String.class, String.class, new LinkedHashMap());
    }

    public boolean isEvent() {
        return name != null && name.equalsIgnoreCase("Event");
    }

    public boolean isFullEvent() {
        return isEvent() && getAttribute("Recurrence") != null;
    }

    /**
     * @param context
     * @return
     */
    public static ItemType getAccount(RequestContext context) {

        List<ItemType> accountTypes = getAccounts(context);
        if (accountTypes.isEmpty()) {

            ItemType accountType = new ItemType(context, "Account");
            accountType.setHidden(true);
            accountType.addModule(ItemModule.Account);
            accountType.save();

            return accountType;

        } else {

            return accountTypes.get(0);
        }
    }

    /**
     * Returns all item types that are account types
     */
    public static List<ItemType> getAccounts(RequestContext context) {

        return getTypes(context, ItemModule.Account);
    }

    /**
     * Returns all item types that are listing types
     */
    public static List<ItemType> getListings(RequestContext context) {

        return getTypes(context, ItemModule.Listings);
    }

    public static Set<Integer> getSocialNetworkFeedItemTypeId(RequestContext context) {
        Set<Integer> ids = new HashSet<Integer>();
        List<ItemType> itemTypeList = getSocialNetworkFeedItemType(context);
        for (ItemType type : itemTypeList) {
            ids.add(type.getId());
        }
        return ids;
    }

    public static List<ItemType> getSocialNetworkFeedItemType(RequestContext context) {
        return getTypes(context, ItemModule.Listings, ItemModule.SocialNetworkFeed);
    }

    /**
     * Returns all item types that are account types
     */
    public static List<ItemType> getOrderable(RequestContext context) {

        return getTypes(context, ItemModule.Ordering);
    }

    private static List<ItemType> getTypes(RequestContext context, final ItemModule... itemModules) {
        List<ItemType> itemTypes = get(context);
        CollectionsUtil.filter(itemTypes, new Predicate<ItemType>() {

            public boolean accept(ItemType e) {
                boolean accepted = false;
                for (ItemModule module : itemModules) {
                    if (e.getModules().contains(module)) {
                        accepted = true;
                    } else {
                        return false;
                    }
                }
                return accepted;
            }
        });
        return itemTypes;
    }

    /**
     * Returns first itemType or null
     *
     * @param context RequestContext
     * @return itemType
     */
    public static ItemType getPricing(RequestContext context) {

        List<ItemType> types = getPricings(context);
        if (!types.isEmpty()) {
            return types.get(0);
        }

        return null;
    }

    /**
     * Returns all item types that are Pricing types
     *
     * @param context RequestContext
     * @return list of itemTypes
     */
    private static List<ItemType> getPricings(RequestContext context) {

        List<ItemType> itemTypes = get(context);
        CollectionsUtil.filter(itemTypes, new Predicate<ItemType>() {

            public boolean accept(ItemType e) {
                return e.getModules().contains(ItemModule.Pricing);
            }
        });
        return itemTypes;
    }

    /**
     * Returns a list of item types that have attachments
     */
    public static List<ItemType> getAttachmentItemTypes(final RequestContext context) {

        List<ItemType> itemTypes = get(context);
        CollectionsUtil.filter(itemTypes, new Predicate<ItemType>() {

            public boolean accept(ItemType e) {
                return e.getModules().contains(ItemModule.Attachments);
            }
        });

        return itemTypes;
    }

    /**
     * Returns any item type that begins with one of the string params
     */
    public static ItemType getByName(RequestContext context, String... names) {

        if (names == null)
            return null;

        for (ItemType itemType : ItemType.get(context)) {
            for (String name : names) {
                if (itemType.getName().toLowerCase().startsWith(name.toLowerCase())) {
                    return itemType;
                }
            }
        }
        return null;
    }

    public static ItemType getDefault(RequestContext context) {
        Query q = new Query(context, "select * from # order by name");
        q.setTable(ItemType.class);
        return q.get(ItemType.class);
    }

    /**
     * Returns all item types that can be listed
     */
    public static List<ItemType> getListable(RequestContext context) {

        List<ItemType> itemTypes = get(context);
        CollectionsUtil.filter(itemTypes, new Predicate<ItemType>() {

            public boolean accept(ItemType e) {
                return e.getModules().contains(ItemModule.Listings);
            }
        });
        return itemTypes;
    }

    /**
     * Returns all item types that registration is enabled on
     */
    public static List<ItemType> getRegistration(RequestContext context) {

        List<ItemType> itemTypes = get(context);
        CollectionsUtil.filter(itemTypes, new Predicate<ItemType>() {

            public boolean accept(ItemType e) {
                return e.getModules().contains(ItemModule.Registrations);
            }
        });
        return itemTypes;
    }

    public static List<ItemType> getSearchable(RequestContext context) {

        List<ItemType> itemTypes = get(context);
        CollectionsUtil.filter(itemTypes, new Predicate<ItemType>() {

            public boolean accept(ItemType e) {
                return e.isSearchable();
            }
        });
        return itemTypes;
    }

    /**
     *
     */
    public static List<ItemType> getSubscriptions(RequestContext context) {

        List<ItemType> itemTypes = get(context);
        CollectionsUtil.filter(itemTypes, new Predicate<ItemType>() {

            public boolean accept(ItemType e) {
                return e.getModules().contains(ItemModule.Subscriptions);
            }
        });
        return itemTypes;
    }

    public static boolean hasAccounts(RequestContext context) {
        return !getAccounts(context).isEmpty();
    }

    public static boolean hasItemTypes(RequestContext context) {
        return SimpleQuery.count(context, ItemType.class) > 0;
    }

    /**
     * Returns a set of all item types for the Collection of items passed in.
     */
    public static Set<ItemType> set(Collection<Item> items) {

        Set<ItemType> set = new HashSet<ItemType>();
        for (Item item : items) {
            set.add(item.getItemType());
        }

        return set;
    }

    private String emailCaption;

    /**
     * Images added to this item or account by a user should be moderated
     */
    private boolean moderateImages;

    private String referencesCaption;

    private boolean searchable;

    private String name;

    /**
     * Adds a post in the users name with this content when an activity is first created.
     */
    private String initialTopicContent;

    private String imageHolder;

    /**
     * Rename file attachments
     */
    private String attachmentsCaption;

    /**
     * Make this item type a back end only item type
     */
    private boolean hidden;

    @Deprecated
    private Template template;

    /**
     * Rename 'featured' products
     */
    private String featuredCaption;

    /**
     *
     */
    private double defaultVatRate;

    /**
     * Enable vat rates for these items
     */
    private boolean vat;

    /*
      * Use featured items ?
      */
    @Default("1")
    private boolean featured;

    /**
     *
     */
    private boolean prioritised;

    /**
     * Enable messages to be left directly on this item as a pseudo topic.
     */
    private boolean messages;

    /**
     * Enable owners of this item type, ie assigned users
     */
    private boolean owners;

    /**
     * Rename the price text
     */
    private String priceCaption;

    /**
     * Show the date this listing was created on the category listings page.
     */
    private boolean displayDateCreated;

    /**
     * notification emails when an item is made
     */
    private List<String> emails;

    /**
     * Enable RRPs
     */
    private boolean rrp;

    /**
     * Rename the RRP caption
     */
    private String rrpCaption;

    /**
     * Enables cost based pricing for items of this item type
     */
    private boolean costPricing;

    /**
     * Enable access control for this item type
     */
    private boolean permissions;

    /**
     * Flag true when this item class is deleted.
     * We cannot delete in case we have items of this class referenced elsewhere.
     */
    private boolean deleted;

    /**
     * Content for the add ratings page
     */
    private String addRatingsContent;

    /**
     * Markup for accessories
     */
    private Markup accessoriesMarkup;

    private List<LinkType> externalLinks;

    /*
      * Put this item type in a category by default
      * If categories is disabled then it will be added to here and you will not be able
      * to remove it, so useful for items you wnat to auto pop a category
      */
    private Category defaultCategory;

    private String defaultContent;

    /**
     *
     */
    private Markup listMarkup;

    /**
     *
     */
    private Markup viewMarkup;

    /**
     *
     */
    private Markup familyMarkup;

    /**
     *
     */
    private Markup familyHeaderMarkup;


    /**
     * A caption displayed after the price. Eg, 'includes delivery'
     */
    private String priceMessage;

    /**
     * Allow manual entry of summaries
     */
    private boolean summaries;

    private String nameCaption;

    /**
     * The forum that messages from this item class should appear in.
     */
    private Forum forum;

    private String namePlural;

    /**
     * Flag this to yes and emails cannot be included in the content of an item of this type when adding a listing
     */
    private boolean blockEmailsInContent;

    /**
     * The set of modules active on this item type
     */
    private SortedSet<ItemModule> modules;

    /**
     * Enable short names on this item type
     */
    private boolean shortNames;

    private PriceRounding priceRounding;

    /**
     * Allow us to change status of this item other than the delete button at bottom
     */
    @Default("1")
    private boolean statusControl;

    /*
      * Allows us to override SEO tags. Defaults to yes.
      */
    @Default("1")
    private boolean overrideTags;

    private int roundTo;

    /**
     * Max number of categories items of this type can appear in
     */
    private int maxCategories;

    /*
      * Allow this item to be put in a category.
      */
    @Default("1")
    @Deprecated
    private boolean categories;

    /**
     * Enable content for this item type
     */
    @Default("1")
    @Deprecated
    private boolean content;

    private transient List<Block> blocks;

    private CostPriceSystem costPriceSystem;

    /**
     * Enable references for this item type
     */
    private boolean references;

    private String accessoriesBlurb;

    private boolean disableImagePopup;

    /**
     * Always use simple editor rather than HTML for items of this type
     */
    private boolean simpleEditor;

    private Markup printerMarkup;

    private transient StockModule stockModule;

    private transient OrderingModule orderingModule;

    /**
     *
     */
    private String header, footer;

    private String restrictionForwardUrl;

    private String prioritisedCaption;

    private transient List<ItemSort> sorts;

    private int dailyProfileViews;

    private int dailyPmLimit;

    private int position;

    private boolean bookable;

    private boolean noForum;

    private boolean assignCodeToOption;

    private Attribute productCodeAttribute;

    private String acceptListingEmailContent, rejectListingEmailContent, addListingEmailContent;

    private Markup searchListMarkup;

    /**
     * is used to disable Account Phones on sites with no sms and orders
     */
    private boolean mobilePhoneDisable;

    private boolean hiddenCs;

    public ItemType(RequestContext context) {
        super(context);
    }

    public ItemType(RequestContext context, String name) {

        super(context);

        this.name = name;
        this.searchable = true;
        this.overrideTags = true;

        this.featured = true;
        this.vat = true;

        this.defaultVatRate = 17.5;
        this.defaultContent = null;

        this.statusControl = true;

        addModule(ItemModule.Images);
        addModule(ItemModule.Content);
        addModule(ItemModule.Categories);
        save();
    }

    public void addAdminField(AdminFieldType fieldType) {
        new AdminField(context, this, fieldType);
    }

    public Attribute addAttribute(String string) {
        return addAttribute(string, AttributeType.Text);
    }

    public void addBlock(String addBlock) {
        BlockUtil.addBlock(context, this, addBlock);
    }

    public void addExternalLink(LinkType linkType) {
        new ExternalLink(context, this, linkType);
    }

    public void addModule(ItemModule module) {
        getModules().add(module);
        save();
    }

    public ItemSort addSort() {
        sorts = null;
        return new ItemSort(context, this);
    }

    public ItemType clone() throws CloneNotSupportedException {

        ItemType type = (ItemType) super.clone();

        type.setName(type.getName() + " (copy)");
        type.save();

        for (Attribute attribute : getAttributes()) {
            attribute.copyTo(type);
        }

        return type;
    }

    public int compareTo(ItemType o) {
        return name.compareTo(o.getName());
    }

    @Override
    public synchronized boolean delete() {

        // set all items in this class to deleted
        Query q = new Query(context, "update # set status=? where itemType=?");
        q.setTable(Item.class);
        q.setParameter("Deleted");
        q.setParameter(this);
        q.run();

        // remove from listing packages
        SimpleQuery.delete(context, ListingPackage.class, "itemType", this);
        SimpleQuery.delete(context, ListingPackageAccess.class, "itemType", this);

        SimpleQuery.delete(context, ProfileModule.class, "itemType", this);
        SimpleQuery.delete(context, AccountModule.class, "itemType", this);
        SimpleQuery.delete(context, RegistrationModule.class, "itemType", this);
        SimpleQuery.delete(context, StockModule.class, "itemType", this);
        SimpleQuery.delete(context, ListingModule.class, "itemType", this);
        SimpleQuery.delete(context, GoogleMapIcon.class, "itemType", this);

        // remove from search wrappers
        new Query(context, "update # set itemType=0 where itemType=?").setParameter(this).setTable(Search.class).run();

        // remove from feeds
        new Query(context, "update # set itemType=0 where itemType=?").setParameter(this).setTable(VipFeed.class).run();
        new Query(context, "update # set itemType=0 where itemType=?").setParameter(this).setTable(MidwichFeed.class).run();
        new Query(context, "update # set itemType=0 where itemType=?").setParameter(this).setTable(PropertyFinderFeed.class).run();

        new Query(context, "update # set itemType=0 where itemType=?").setParameter(this).setTable(VueFeed.class).run();
        new Query(context, "update # set itemType=0 where itemType=?").setParameter(this).setTable(CamdramFeed.class).run();

        new Query(context, "update # set itemType=0 where itemType=?").setParameter(this).setTable(GoogleBase.class).run();
        new Query(context, "update # set itemType=0 where itemType=?").setParameter(this).setTable(CsvImportFeed.class).run();
        new Query(context, "update # set itemType=0 where itemType=?").setParameter(this).setTable(PdoNewsFeed.class).run();

        new Query(context, "update # set itemType=0 where itemType=?").setParameter(this).setTable(PriceGrabber.class).run();

        removeAttributes();

        new Query(context, "update # set ownerItemType=0 where ownerItemType=?").setTable(FormBlock.class).setParameter(this).run();

        this.deleted = true;
        save();
        return true;
    }

    /**
     * Returns a list of listing packages that have access set for this account type
     */
    public List<ListingPackage> getAccessableListingPackages() {

        Query q = new Query(context, "select lp.* from # lp join # lpo on lp.id=lpo.listingPackage where lpo.itemType=? and lp.deleted=0 order by id");
        q.setTable(ListingPackage.class);
        q.setTable(ListingPackageAccess.class);
        q.setParameter(this);
        return q.execute(ListingPackage.class);
    }

    public String getAccessoriesBlurb() {
        return accessoriesBlurb;
    }

    public Markup getAccessoriesMarkup() {

        if (accessoriesMarkup == null) {

            accessoriesMarkup = new Markup(context, new AccessoriesMarkupDefault());
            accessoriesMarkup.setName("Accessories markup for " + getName());
            accessoriesMarkup.save();

            save();
        }

        return accessoriesMarkup.pop();
    }

    public AccountModule getAccountModule() {
        AccountModule module = SimpleQuery.get(context, AccountModule.class, "itemType", this);
        return module == null ? new AccountModule(context, this) : module;
    }

    public String getAddRatingsContent() {
        return addRatingsContent;
    }

    public List<AdminField> getAdminFields() {

        List<AdminField> fields = SimpleQuery.execute(context, AdminField.class, "itemType", this);
        if (fields.isEmpty()) {

            new AdminField(context, this, AdminFieldType.Id);
            new AdminField(context, this, AdminFieldType.DateCreated);
            new AdminField(context, this, AdminFieldType.Name);

            if (getModules().contains(ItemModule.Pricing)) {
                new AdminField(context, this, AdminFieldType.SellPrice);
            }

            if (getModules().contains(ItemModule.Images)) {
                new AdminField(context, this, AdminFieldType.Images);
            }

            if (isStatusControl()) {
                new AdminField(context, this, AdminFieldType.Status);
            }

            return getAdminFields();
        }
        return fields;
    }

    public AlternativesModule getAlternativesModule() {
        AlternativesModule mod = SimpleQuery.get(context, AlternativesModule.class, "itemType", this);
        if (mod == null)
            mod = new AlternativesModule(context, this);
        return mod;
    }

    public String getAttachmentsCaption() {
        return attachmentsCaption == null ? "File attachments" : attachmentsCaption;
    }

    /**
     * Returns all the attributes that are of the types contained in the var args param
     */
    public List<Attribute> getAttributes(AttributeType... attributeTypes) {

        final List<AttributeType> types = Arrays.asList(attributeTypes);

        List<Attribute> attributes = getAttributes();
        CollectionsUtil.filter(attributes, new Predicate<Attribute>() {

            public boolean accept(Attribute e) {
                return types.contains(e.getType());
            }
        });
        return attributes;
    }

    public List<Attribute> getAttributesAvailable() {
        return getAttributes();
    }

    public List<Attribute> getAttributes() {
        Query q = new Query(context, "select * from # where category=0 and itemtype=? and itemtypesettings=0 order by position");
        q.setTable(Attribute.class);
        q.setParameter(this);
        return q.execute(Attribute.class);
    }

    public Map<String, String> getAttributesSelectionMap(RequestContext context) {
        Query q = new Query(context, "select id, name from # where category=0 and itemtype=? and itemtypesettings=0 order by position");
        q.setTable(Attribute.class);
        q.setParameter(this);
        return q.getMap(String.class, String.class, new LinkedHashMap());
    }

    /**
     * Returns a set of Item modules that this item type can currently have based on what has been enabled in modules
     */
    public SortedSet<ItemModule> getAvailableModules() {

        SortedSet<ItemModule> set = new TreeSet<ItemModule>(new Comparator<ItemModule>() {

            public int compare(ItemModule o1, ItemModule o2) {
                return o1.toString().toLowerCase().compareTo(o2.toString().toLowerCase());
            }
        });

        set.addAll(Arrays.asList(ItemModule.values()));
        CollectionsUtil.filter(set, new Predicate<ItemModule>() {

            public boolean accept(ItemModule e) {
                return e.isAvailable(context);
            }
        });
        return set;
    }

    public List<Block> getBlocks() {

        if (blocks == null) {
            blocks = Block.get2(context, this);
        }

        // ensure we have a videos block if videos areenabled
        if (ItemModule.Videos.enabled(context, this)) {
            if (!Block.contains(VideosBlock.class, blocks)) {
                new VideosBlock(context, this, 0);
            }
        }

        // ensure we have a reviews block if reviews are enabled
        if (ItemModule.Reviews.enabled(context, this)) {
            if (!Block.contains(ReviewsBlock.class, blocks)) {
                new ReviewsBlock(context, this, 0);
            }
        }

        // if this item type has alternatives on then add in that block
        if (ItemModule.Alternatives.enabled(context, this)) {
            if (!Block.contains(AlternativesBlock.class, blocks)) {
                new AlternativesBlock(context, this, 0);
            }
        }

        return blocks;
    }

    public BookingModule getBookingModule() {
        return BookingModule.get(context, this);
    }

    public BoxOffice getBoxOffice() {

        Query q = new Query(context, "select * from # where itemType=?");
        q.setTable(BoxOffice.class);
        q.setParameter(this);

        BoxOffice boxOffice = q.get(BoxOffice.class);
        if (boxOffice == null)
            boxOffice = new BoxOffice(context, this);
        return boxOffice;

    }

    public CostPriceSystem getCostPriceSystem() {
        return costPriceSystem == null ? CostPriceSystem.Cheapest : costPriceSystem;
    }

    public List<CreditPackage> getCreditPackages() {
        Query q = new Query(context, "select cp.* from # cp join # cpa on cp.id=cpa.creditPackage where cpa.itemType=?");
        q.setTable(CreditPackage.class);
        q.setTable(CreditPackageAccess.class);
        q.setParameter(this);
        return q.execute(CreditPackage.class);
    }

    public int getDailyPmLimit() {
        return dailyPmLimit;
    }

    public int getDailyProfileViewsLimit() {
        return dailyProfileViews;
    }

    public final Category getDefaultCategory() {
        return (Category) (defaultCategory == null ? null : defaultCategory.pop());
    }

    public String getDefaultContent() {
        return defaultContent;
    }

    //	/**
    //	 * Returns a mapping of attributes to values selected on live items in this item class.
    //	 *
    //	 */
    //	public MultiValueMap<Attribute, String> getFilters() {
    //
    //		Query q = new Query(context, "select a.id, av.value from # a join # av on a.id=av.attribute join # i on av.item=i.id "
    //				+ "where i.itemType=? and a.filter=1 and i.status=? order by a.name, av.value");
    //		q.setTable(Attribute.class);
    //		q.setTable(AttributeValue.class);
    //		q.setTable(Item.class);
    //
    //		q.setParameter(this);
    //		q.setParameter(Item.Status.Live);
    //
    //		Attribute attribute = null;
    //		MultiValueMap<Attribute, String> map = new LinkedMultiMap<Attribute, String>();
    //		for (Row row : q.execute()) {
    //
    //			if (attribute == null)
    //				attribute = row.getObject(0, Attribute.class);
    //
    //			if (attribute == null)
    //				throw new RuntimeException("Invalid attribute data in database");
    //
    //			if (attribute.getId() != row.getInt(0))
    //				attribute = row.getObject(0, Attribute.class);
    //
    //			map.put(attribute, row.getString(1));
    //		}
    //
    //		return map;
    //	}

    public ItemSort getDefaultSort() {

        List<ItemSort> sorts = getSorts();
        if (sorts.isEmpty()) {
            return null;
        }

        for (ItemSort sort : sorts) {
            if (sort.isDefault()) {
                return sort;
            }
        }

        return sorts.get(0);
    }

    public double getDefaultVatRate() {
        return defaultVatRate;
    }

    public String getDomainName() {
        return getName();
    }

    public final String getEmailCaption() {
        return emailCaption == null ? "Email" : emailCaption;
    }

    public List<String> getEmails() {
        if (emails == null)
            emails = new ArrayList();
        return emails;
    }

    public List<ExternalLink> getExternalLinks() {
        Query q = new Query(context, "select * from # where itemType=? order by position");
        q.setTable(ExternalLink.class);
        q.setParameter(this);
        return q.execute(ExternalLink.class);
    }

    public String getFeaturedCaption() {
        return featuredCaption == null ? "Featured" : featuredCaption;
    }

    public final String getFooter() {
        return footer;
    }

    /**
     * Returns the forum that messages for this item type should go in. If the forum does not exist then create it.
     *
     * @return
     */
    public Forum getForum() {

        if (forum == null) {
            if (!noForum) {
                forum = new Forum(context, getName());
                save();
            }
        }

        return forum;
    }

    /**
     *
     */
    public String getHeader() {
        return header;
    }

    public final String getImageHolder() {
        return imageHolder;
    }

    public String getImageHolderPath() {

        if (hasImageHolder())
            return "template-data/" + getImageHolder();

        return null;
    }

    public final String getInitialTopicContent() {
        return initialTopicContent;
    }

    public InteractionModule getInteractionModule() {
        InteractionModule module = SimpleQuery.get(context, InteractionModule.class, "itemType", this);
        return module == null ? new InteractionModule(context, this) : module;
    }

    public String getLabel() {
        return getNamePlural();
    }

    public ListingModule getListingModule() {
        return ListingModule.get(context, this);
    }

    public Markup getListMarkup() {

        if (listMarkup == null) {
            ListMarkupDefault markupDefault = new ListMarkupDefault();
            listMarkup = new Markup(context, name + " list markup", markupDefault);
            try {
                listMarkup.setCss(markupDefault.getCss());
            } catch (IOException e) {
                e.printStackTrace();
            }
            listMarkup.setTableClass(markupDefault.getCssClass());
            listMarkup.setCycleRows(markupDefault.getCycleRows());
            listMarkup.save();
            save();
        }
        return listMarkup.pop();
    }

    public Markup getSearchListMarkup() {

        if (searchListMarkup == null) {
            searchListMarkup = getListMarkup();
//            ListMarkupDefault markupDefault = new ListMarkupDefault();
//            searchListMarkup = new Markup(context, name + " search results list markup", markupDefault);
//            try {
//                searchListMarkup.setCss(markupDefault.getCss());
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            searchListMarkup.setTableClass(markupDefault.getCssClass());
//            searchListMarkup.setCycleRows(markupDefault.getCycleRows());
//            searchListMarkup.save();
            save();
        }
        return searchListMarkup.pop();
    }


    /**
     *
     */
    public Map<String, String> getLiveCategories() {
        return Category.getItemContainersMap(context, this);
    }

    public List<LogEntry> getLogEntries() {
        return LogEntry.get(context, this);
    }

    public String getLogId() {
        return getFullId();
    }

    public String getLogName() {
        return "Item type #" + getIdString();
    }

    public int getMaxCategories() {
        return maxCategories;
    }

    public MediaModule getMediaModule() {
        return MediaModule.get(context, this);
    }

    public final SortedSet<ItemModule> getModules() {

        if (modules == null) {
            modules = new TreeSet();
        }

        if (content) {
            modules.add(ItemModule.Content);
            content = false;
            save();
        }

        if (categories) {
            modules.add(ItemModule.Categories);
            categories = false;
            save();
        }

        return modules;
    }

    public String getName() {
        return name == null ? "Null" : name;
    }

    public String getNameCaption() {
        return nameCaption == null ? "Name" : nameCaption;
    }

    public String getNameLower() {
        return getName().toLowerCase();
    }

    public String getNamePlural() {
        if (namePlural == null) {
            return Pluraliser.toPlural(name);
        }
        return namePlural;
    }

    public String getNamePluralLower() {
        return getNamePlural().toLowerCase();
    }

    public OptionsModule getOptionsModule() {
        OptionsModule om = SimpleQuery.get(context, OptionsModule.class, "itemType", this);
        if (om == null) {
            om = new OptionsModule(context, this);
        }
        return om;
    }

    public final OrderingModule getOrderingModule() {
        if (orderingModule == null) {
            orderingModule = OrderingModule.get(context, this);
        }
        return orderingModule;
    }

    public Set<PaymentType> getPaymentTypes() {
        return PaymentSettings.getInstance(context).getPaymentTypes();
    }

    public List<Permission> getPermissions() {
        return DomainUtil.getPermissions(context, this, null);
    }

    public List<Permission> getPermissions(Permissions p) {
        return DomainUtil.getPermissions(context, this, p);
    }

    public int getPosition() {
        return position;
    }

    public String getPriceCaption() {
        return priceCaption == null ? "Our price" : priceCaption;
    }

    public String getPriceMessage() {
        return priceMessage;
    }

    public final PriceRounding getPriceRounding() {
        return priceRounding == null ? PriceRounding.None : priceRounding;
    }

    public Markup getPrinterMarkup() {

        if (printerMarkup == null) {

            printerMarkup = new Markup(context, name + " printer markup", new PrinterMarkupDefault());
            save();
        }

        return printerMarkup.pop();
    }

    /**
     *
     */
    public String getPrioritisedCaption() {
        return prioritisedCaption == null ? "Prioritised" : prioritisedCaption;
    }

    public Attribute getProductCodeAttribute() {
        return productCodeAttribute;
    }

    public ProfileModule getProfileModule() {

        if (Module.MemberProfiles.enabled(context)) {
            if (getModules().contains(ItemModule.Profile)) {
                return ProfileModule.get(context, this);
            }
        }

        return null;
    }

    public RatingModule getRatingModule() {
        return RatingModule.get(context, this);
    }

    public String getReferencesCaption() {
        return referencesCaption == null ? "Reference" : referencesCaption;
    }

    public RegistrationModule getRegistrationModule() {
        return RegistrationModule.get(context, this);
    }

    /**
     *
     */
    public RelationsModule getRelationsModule() {
        return RelationsModule.get(context, this);
    }

    public String getRestrictionForwardUrl() {
        return restrictionForwardUrl;
    }

    public ReviewsModule getReviewsModule() {
        return ReviewsModule.get(context, this);
    }

    public int getRoundTo() {
        return roundTo;
    }

    public String getRrpCaption() {
        return rrpCaption == null ? "RRP" : rrpCaption;
    }

    public ShoppingModule getShoppingModule() {
        ShoppingModule module = SimpleQuery.get(context, ShoppingModule.class, "itemType", this);
        return module == null ? new ShoppingModule(context, this) : module;
    }

    public List<ItemSort> getSorts() {
        if (sorts == null) {
            sorts = SimpleQuery.execute(context, ItemSort.class, "itemType", this);
        }
        return sorts;
    }

    public List<String> getStatuses() {
        return new Query(context, "select distinct status from # where itemType=? and status is not null").setTable(Item.class).setParameter(this).getStrings();
    }

    public StockModule getStockModule() {
        if (stockModule == null) {
            stockModule = StockModule.get(context, this);
        }
        return stockModule;
    }

    public SubscriptionModule getSubscriptionModule() {
        SubscriptionModule module = SimpleQuery.get(context, SubscriptionModule.class, "itemType", this);
        return module == null ? new SubscriptionModule(context, this) : module;
    }

    public TemplateConfig getTemplateConfig() {

        TemplateConfig tc = TemplateConfig.get(context, this);

        if (template != null) {

            tc.setTemplate((Template) template.pop());
            tc.save();

            template = null;
            save("template");
        }

        return tc;
    }

    /**
     *
     */
    public Set<ItemModule> getUnusedModules() {

        Set<ItemModule> modules = getAvailableModules();
        modules.removeAll(getModules());
        return modules;
    }

    public OptionSet getOptionSet() {

        OptionSet b = SimpleQuery.get(context, OptionSet.class, "itemType", this);
        if (b == null) {
            b = new OptionSet(context, this);
        }

        return b;
    }

    public String getValue() {
        return getIdString();
    }

    public Markup getViewMarkup() {

        if (viewMarkup == null) {
            ViewMarkupDefault viewMarkupDefault = new ViewMarkupDefault();
            viewMarkup = new Markup(context, name + " view markup", viewMarkupDefault);
            try {
                viewMarkup.setCss(viewMarkupDefault.getCss());
            } catch (IOException e) {
                e.printStackTrace();
            }
            viewMarkup.save();
            save();
        }

        return viewMarkup.pop();
    }

    public Markup getFamilyHeaderMarkup() {

        if (familyHeaderMarkup == null) {

            familyHeaderMarkup = new Markup(context, name + " family header markup", new FamilyHeaderMarkupDefault());
            save();
        }

        return familyHeaderMarkup.pop();
    }

    public Markup getFamilyMarkup() {

        if (familyMarkup == null) {

            familyMarkup = new Markup(context, name + " family markup", new FamilyItemsMarkupDefault());
            save();
        }

        return familyMarkup.pop();
    }

    public boolean hasAddRatingsContent() {
        return addRatingsContent != null;
    }

    public boolean hasAgreement() {
        return false;
    }

    /**
     * Returns true if there are any items of this item type awaiting moderation
     */
    public boolean hasAwaitingModeration() {
        return new Query(context, "select count(*) from # where awaitingModeration=1 and itemType=?").setTable(Item.class).setParameter(this).getInt() > 0;
    }

    public boolean hasDefaultCategory() {
        return defaultCategory != null;
    }

    public boolean hasDefaultContent() {
        return defaultContent != null;
    }

    public boolean hasFooter() {
        return footer != null;
    }

    public boolean hasHeader() {
        return header != null;
    }

    public boolean hasImageHolder() {
        return imageHolder != null;
    }

    public boolean hasInitialTopicContent() {
        return initialTopicContent != null;
    }

    public boolean hasPermission(PermissionType type, Permissions p) {
        return DomainUtil.hasPermission(context, this, type, p);
    }

    public boolean hasPriceMessage() {
        return priceMessage != null;
    }

    public boolean hasPriceRounding() {
        return getPriceRounding() != PriceRounding.None;
    }

    public boolean isAssignCodeToOption() {
        return assignCodeToOption;
    }

    public final boolean isBlockEmailsInContent() {
        return blockEmailsInContent;
    }

    public boolean isCategories() {
        return getModules().contains(ItemModule.Categories);
    }

    public boolean isContent() {
        return getModules().contains(ItemModule.Content);
    }

    public boolean isCostPricing() {
        return costPricing && ItemModule.Pricing.enabled(context, this);
    }

    public boolean isDeleted() {
        return deleted;
    }

    public boolean isDisableImagePopup() {
        return disableImagePopup;
    }

    public boolean isDisplayDateCreated() {
        return displayDateCreated;
    }

    public final boolean isFeatured() {
        return featured;
    }

    public boolean isHidden() {
        return hidden;
    }

    public boolean isHiddenCs() {
        return hiddenCs;
    }

    public boolean isMessages() {
        return messages && Module.Forum.enabled(context);
    }

    public final boolean isModerateImages() {
        return moderateImages;
    }

    public boolean isNoForum() {
        return noForum;
    }

    public boolean isNoGuestLogin() {
        return false;
    }

    public final boolean isOverrideTags() {
        return overrideTags && Seo.getInstance(context).isOverrideTags();
    }

    public final boolean isOwners() {
        return owners;
    }

    public final boolean isPermissions() {
        return permissions;
    }

    public final boolean isPrioritised() {
        return prioritised;
    }

    public boolean isReferences() {
        return references;
    }

    public boolean isRrp() {
        return rrp;
    }

    public boolean isSearchable() {
        return searchable;
    }

    public boolean isShortNames() {
        return shortNames;
    }

    public final boolean isSimpleEditor() {
        return simpleEditor;
    }

    public boolean isStatusControl() {
        return statusControl;
    }

    public boolean isSummaries() {
        return summaries;
    }

    public boolean isVat() {
        return vat && Company.getInstance(context).isVatRegistered();
    }

    public boolean isVisible() {
        return !isHidden();
    }

    public String getAcceptListingEmailContent() {
        return acceptListingEmailContent == null ?
                new StringBuilder().append("Hello,\n\nGood news! Your posted " + getName().toLowerCase()).
                        append(" has been approved for inclusion on our site.\n\nYou can view it by clicking this link:\n").
                        append("[url]").
                        append("\n\n\n[site]").toString() : acceptListingEmailContent;
    }

    public String getRejectListingEmailContent() {
        return rejectListingEmailContent == null ?
                new StringBuilder().append("Hello,\n\nUnfortunately your recent posting '[name]' has been rejected by our admin team.\nPlease try adding another.").
                        append("\n\n\n[site]").toString() : rejectListingEmailContent;
    }

     public String getAddListingEmailContent() {
        return addListingEmailContent == null ?
                new StringBuilder().append("Hello,\n\nGood news! Your posted " + getName().toLowerCase()).
                        append(" has been added to the [site] website in the category you have selected.\n\nYou can view it by clicking this link:\n").
                        append("[url]").
                        append("\n\n\n[site]").toString() : addListingEmailContent;
    }

    public boolean isMobilePhoneDisable() {
        return mobilePhoneDisable;
    }

    public void log(String message) {
        new LogEntry(context, this, message);
    }

    public void log(User user, String message) {
        new LogEntry(context, this, user, message);
    }

    public void removeAdminField(AdminField field) {
        field.delete();
    }

    public void removeExternalLink(ExternalLink link) {
        link.delete();
    }

    public void removeModule(ItemModule module) {
        getModules().remove(module);
    }

    public void removePermissions(Collection<PermissionType> permissionTypes, Permissions p) {
        DomainUtil.removePermissions(context, this, permissionTypes, p);
    }

    public void removePermissions(Permissions p) {
        DomainUtil.removePermissions(context, this, p);
    }

    public void removeSort(ItemSort sort) {
        sort.delete();
    }

    public void setAccessoriesBlurb(String accessoriesCaption) {
        this.accessoriesBlurb = accessoriesCaption;
    }

    public void setAccessoriesMarkup(Markup accessoriesMarkup) {
        this.accessoriesMarkup = accessoriesMarkup;
    }

    public void setAddRatingsContent(String addRatingsContent) {
        this.addRatingsContent = addRatingsContent;
    }

    public void setAssignCodeToOption(boolean assignCodeToOption) {
        this.assignCodeToOption = assignCodeToOption;
    }

    public void setAttachmentsCaption(String attachmentsCaption) {
        this.attachmentsCaption = attachmentsCaption;
    }

    public final void setBlockEmailsInContent(boolean blockEmailsInContent) {
        this.blockEmailsInContent = blockEmailsInContent;
    }

    public void setCostPriceSystem(CostPriceSystem costPriceSystem) {
        this.costPriceSystem = costPriceSystem;
    }

    public void setCostPricing(boolean b) {
        this.costPricing = b;
    }

    public final void setDailyPmLimit(int dailyPmLimit) {
        this.dailyPmLimit = dailyPmLimit;
    }

    public final void setDailyProfileViewsLimit(int dailyProfileViews) {
        this.dailyProfileViews = dailyProfileViews;
    }

    public final void setDefaultCategory(Category defaultCategory) {
        this.defaultCategory = defaultCategory;
    }

    public void setDefaultContent(String s) {
        this.defaultContent = s;
    }

    public void setDefaultVatRate(double doubleVatRate) {
        this.defaultVatRate = doubleVatRate;
    }

    public void setDisableImagePopup(boolean disableImagePopup) {
        this.disableImagePopup = disableImagePopup;
    }

    public void setDisplayDateCreated(boolean displayDateCreated) {
        this.displayDateCreated = displayDateCreated;
    }

    public final void setEmailCaption(String emailCaption) {
        this.emailCaption = emailCaption;
    }

    public void setEmails(List<String> e) {
        this.emails = new ArrayList();
        if (e != null) {
            this.emails.addAll(e);
        }
    }

    public void setExternalLinks(List<LinkType> externalLinks) {
        this.externalLinks = externalLinks;
    }

    public final void setFeatured(boolean f) {

        if (this.featured == f) {
            return;
        }

        this.featured = f;
        Query q = new Query(context, "update # set featured=0 where itemType=?");
        q.setTable(Item.class);
        q.setParameter(this);
        q.run();
    }

    public void setFeaturedCaption(String featuredCaption) {
        this.featuredCaption = featuredCaption;
    }

    public final void setFooter(String footer) {
        this.footer = footer;
    }

    public void setForum(Forum forum) {
        this.forum = forum;
    }

    public final void setHeader(String header) {
        this.header = header;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public void setHiddenCs(boolean hiddenCs) {
        this.hiddenCs = hiddenCs;
    }

    public final void setImageHolder(String imageHolder) {
        this.imageHolder = imageHolder;
    }

    public final void setInitialTopicContent(String initialTopicContent) {
        this.initialTopicContent = initialTopicContent;
    }

    public void setListMarkup(Markup listMarkup) {
        this.listMarkup = listMarkup;
    }

    public void setSearchListMarkup(Markup searchListMarkup) {
        this.searchListMarkup = searchListMarkup;
    }

    public void setMaxCategories(int maxCategories) {
        this.maxCategories = maxCategories;
    }

    public void setMessages(boolean messages) {
        this.messages = messages;
    }

    public final void setModerateImages(boolean moderatingImages) {
        this.moderateImages = moderatingImages;
    }

    public void setName(String n) {

        if (n.equals(name)) {
            return;
        }

        this.name = n;
        this.namePlural = null;
    }

    public void setNameCaption(String nameCaption) {
        this.nameCaption = nameCaption;
    }

    public final void setNamePlural(String namePlural) {
        this.namePlural = namePlural;
    }

    public void setNoForum(boolean noForum) {
        this.noForum = noForum;
    }

    public final void setOverrideTags(boolean overrideTags) {
        this.overrideTags = overrideTags;
    }

    public final void setOwners(boolean owners) {
        this.owners = owners;
    }

    public void setPermission(PermissionType type, Permissions p) {
        new Permission(context, this, type, p);
    }

    public final void setPermissions(boolean permissions) {
        this.permissions = permissions;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setPriceCaption(String priceCaption) {
        this.priceCaption = priceCaption;
    }

    public void setPriceMessage(String priceCaption) {
        this.priceMessage = priceCaption;
    }

    public final void setPriceRounding(PriceRounding priceRounding) {
        this.priceRounding = priceRounding;
    }

    public final void setPrinterMarkup(Markup printerMarkup) {
        this.printerMarkup = printerMarkup;
    }

    public final void setPrioritised(boolean prioritise) {
        this.prioritised = prioritise;
    }

    public final void setPrioritisedCaption(String prioritisedCaption) {
        this.prioritisedCaption = prioritisedCaption;
    }

    public void setProductCodeAttribute(Attribute productCodeAttribute) {
        this.productCodeAttribute = productCodeAttribute;
    }

    public void setReferences(boolean references) {
        this.references = references;
    }

    public void setReferencesCaption(String referencesCaption) {
        this.referencesCaption = referencesCaption;
    }

    public void setRestrictionForwardUrl(String restrictionForwardUrl) {
        this.restrictionForwardUrl = restrictionForwardUrl;
    }

    public final void setRoundTo(int roundTo) {
        this.roundTo = roundTo;
    }

    public void setRrp(boolean rrp) {
        this.rrp = rrp;
    }

    public void setRrpCaption(String rrpCaption) {
        this.rrpCaption = rrpCaption;
    }

    public void setSearchable(boolean b) {
        this.searchable = b;
    }

    public void setShortNames(boolean shortNames) {
        this.shortNames = shortNames;
    }

    public final void setSimpleEditor(boolean simpleEditor) {
        this.simpleEditor = simpleEditor;
    }

    /**
     * Lets users change status
     */
    public void setStatusControl(boolean statusControl) {
        this.statusControl = statusControl;
    }

    public void setSummaries(boolean summaries) {
        this.summaries = summaries;
    }

    public void setVat(boolean vat) {
        this.vat = vat;
    }

    public void setViewMarkup(Markup viewMarkup) {
        this.viewMarkup = viewMarkup;
    }

    public void setAcceptListingEmailContent(String acceptListingEmailContent) {
        this.acceptListingEmailContent = acceptListingEmailContent;
    }

    public void setRejectListingEmailContent(String rejectListingEmailContent) {
        this.rejectListingEmailContent = rejectListingEmailContent;
    }

    public void setAddListingEmailContent(String addListingEmailContent) {
        this.addListingEmailContent = addListingEmailContent;
    }

    public boolean isBookable() {
        return bookable;
    }

    public void setBookable(boolean bookable) {
        this.bookable = bookable;
    }

    public boolean isSocialNetworkFeedItemType() {
        return getModules().contains(ItemModule.Listings) && getModules().contains(ItemModule.SocialNetworkFeed);
    }

    public boolean isAccount(RequestContext context) {
        return getAccounts(context).contains(this);
    }

    public void setMobilePhoneDisable(boolean mobilePhoneDisable) {
        this.mobilePhoneDisable = mobilePhoneDisable;
    }
}