package org.sevensoft.ecreator.model.items.reviews.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.items.ReviewsHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 11 Aug 2006 15:54:06
 * 
 * A link to a separate reviews page for this item
 *
 */
public class ReviewsMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		if (!ItemModule.Reviews.enabled(context, item)) {
			logger.fine("[ReviewsMarker] reviews disabled");
			return null;
		}

		return super.link(context, params, new Link(ReviewsHandler.class, null, "item", item), "reviews_link");
	}

	public String getDescription() {
		return "Links to a separate page with all reviews on";
	}

	public Object getRegex() {
		return "reviews";
	}

}
