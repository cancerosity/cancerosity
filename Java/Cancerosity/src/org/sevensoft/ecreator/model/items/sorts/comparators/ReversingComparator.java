package org.sevensoft.ecreator.model.items.sorts.comparators;

import java.util.Comparator;

/**
 * @author sks 18 Sep 2006 08:27:20
 *
 */
public class ReversingComparator implements Comparator {

	private final Comparator	comparator;

	public ReversingComparator(Comparator comparator) {
		this.comparator = comparator;
	}

	public int compare(Object o1, Object o2) {
		return comparator.compare(o1, o2) * -1;
	}

}
