package org.sevensoft.ecreator.model.items.pricing.markers;

import java.util.Map;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class RrpMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		if (!item.getItemType().isRrp()) {
			return null;
		}

		if (!item.hasRrp()) {
			return null;
		}

		Item account = (Item) context.getAttribute("account");
		Currency currency = (Currency) context.getAttribute("currency");
		String currencySymbol = (String) context.getAttribute("currencySymbol");

        if (!PermissionType.ItemPriceSummary.check(context, account, item.getItemType())) {
            return null;
        }

		if (!item.hasRrpDiscount(account)) {
			return null;
		}

		if (!params.containsKey("prefix")) {
			params.put("prefix", "RRP: ");
		}

		if (!params.containsKey("class")) {
			params.put("class", "rrp");
		}

		Money rrp;
		if (params.containsKey("ex")) {
			rrp = item.getRrp(currency);
		} else {
			rrp = item.getRrpInc(currency);
		}

		return super.string(context, params, currencySymbol + rrp);

	}

	public Object getRegex() {
		return "pricing_rrp";
	}

}
