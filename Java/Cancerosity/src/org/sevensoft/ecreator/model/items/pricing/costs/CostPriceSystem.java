package org.sevensoft.ecreator.model.items.pricing.costs;

/**
 * @author sks 6 Aug 2006 19:25:26
 *
 */
public enum CostPriceSystem {

	/**
	 * Takes the cheapest cost price
	 */
	Cheapest,

	/**
	 * Takes an average cost price from all suppliers
	 */
	Average,

	/**
	 * Takes the cost price from the last used on the PO system
	 */
	LastBuy, 
}