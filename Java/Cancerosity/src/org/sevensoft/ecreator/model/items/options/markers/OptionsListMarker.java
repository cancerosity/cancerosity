package org.sevensoft.ecreator.model.items.options.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.skint.Money;

import java.util.Map;
import java.util.List;

/**
 * User: Tanya
 * Date: 16.11.2011
 */
public class OptionsListMarker extends MarkerHelper implements IItemMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

        if (!ItemModule.Options.enabled(context, item)) {
            logger.fine("[OptionsListMarker] options disabled");
            return null;
        }

        if (!item.isShowOptionsList()) {
            logger.fine("[OptionsListMarker] options list disabled disabled for the item: " + item.getIdString());
            return null;
        }


        /*
                    * If no options to display then exit
        */
        List<ItemOption> options = item.getOptionSet().getOptions();
        if (options.isEmpty()) {
            logger.fine("[OptionsListMarker] no options on item");
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int n = 0;
        boolean rrp = params.containsKey("rrp");
        sb.append("<div class='options_list'>");
        for (ItemOption option : options) {

            sb.append("<div class='option_" + n + "'>");
            sb.append("<span class='option_name'>" + option.getName() + ": </span>");

            List<ItemOptionSelection> selections = option.getSelections();
            for (ItemOptionSelection selection : selections) {
                sb.append("<span class='option_" + n + "_value'>" + selection.getText());
                if (selection.getPrice().isGreaterThan(0)) {
                    sb.append(" - Price \243");
                    sb.append(selection.getPrice());
                }

                if (rrp && selection.getOptionRrp().isGreaterThan(0)) {
                    sb.append(" <span class='option_0_rrp'> - Was \243");
                    sb.append(selection.getRrp());
                    sb.append("</span>");
                }

//                if (selections.indexOf(selection) < selections.size() - 1)
//                    sb.append(", ");
                sb.append("</span>");
            }
            sb.append("</div>");
            n++;
        }
        sb.append("</div>");

        return super.string(context, params, sb.toString());


    }

    public Object getRegex() {
        return "options_list";
    }

}
