package org.sevensoft.ecreator.model.items;

import org.sevensoft.ecreator.iface.admin.misc.AjaxItemSearchHandler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 29 Jan 2007 09:17:40
 *
 */
public class AjaxItemSearch {

	private final String		paramName;
	private final RequestContext	context;
	private String			textTagId;
	private String			selectTagId;

	public AjaxItemSearch(RequestContext context, String paramName) {
		this.context = context;
		this.paramName = paramName;
		this.textTagId = paramName + "_query";
		this.selectTagId = paramName + "_select";
	}

	@Override
	public String toString() {

		TextTag textTag = new TextTag(context, "none", 20);
		textTag.setId(textTagId);

		SelectTag selectTag = new SelectTag(context, paramName);
		selectTag.setId(selectTagId);
		selectTag.setStyle("display: none; ");

		StringBuilder sb = new StringBuilder();
		sb.append("<script>");
		sb.append("var http2 = createRequestObject();");

		Link url = new Link(AjaxItemSearchHandler.class);

		sb.append("function itemSearch() {");
		sb.append("	http2.open('GET', '" + url + "?q=' + document.getElementById('" + textTagId + "').value, true);");
		sb.append("	http2.onreadystatechange = itemSearchResults; ");
		sb.append("	http2.send(null);");
		sb.append("} ");

		sb.append("function itemSearchResults() {");
		sb.append("	if (http2.readyState == 4) {");
		sb.append("		var x = http2.responseText.split('\\n'); \n");
		sb.append("		if (x.length > 1) { ");
		sb.append("			var select = document.getElementById('" + selectTagId + "'); ");
		sb.append("			select.options.length=0; \n ");
		sb.append("			select.options[0] = new Option('-Select from ' + (x.length / 2) + ' matches-', '', false, true); \n");
		sb.append("			for (n = 0; n < x.length; n = n + 2) { \n");
		sb.append("				select.options[(n / 2) + 1]=new Option(x[n+1], x[n], false, false); ");
		sb.append("			}\n");
		sb.append("			select.style.display = 'inline'; ");
		sb.append("		} else { window.alert('No search results found'); } \n");
		sb.append("	}\n");
		sb.append("}\n");
		sb.append("</script>");

		ButtonTag buttonTag = new ButtonTag("Search");
		buttonTag.setOnClick("itemSearch();");

		return sb + " " + textTag + " " + buttonTag + " " + selectTag;
	}
}
