package org.sevensoft.ecreator.model.bespoke.ipoints;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author Stephen K Samuel samspade79@gmail.com 25 Mar 2009 19:29:43
 */
@Table("ipoints_settings")
@Singleton
public class IPointsSettings extends EntityObject {

    public static IPointsSettings getInstance(RequestContext context) {
        return EntityObject.getSingleton(context, IPointsSettings.class);
    }

    private String username;
    private String password;
    private int pointsPerPound;
    private String termsUrl;

    public IPointsSettings(RequestContext context) {
        super(context);
    }

    public String getPassword() {
        return password;
    }

    public int getPointsPerPound() {
        return pointsPerPound;
    }

    public String getTermsUrl() {
        return termsUrl;
    }

    public String getUsername() {
        return username;
    }

    public boolean isEnabled() {
        return username != null;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPointsPerPound(int pointsPerPound) {
        this.pointsPerPound = pointsPerPound;
    }

    public void setTermsUrl(String termsUrl) {
        this.termsUrl = termsUrl;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
