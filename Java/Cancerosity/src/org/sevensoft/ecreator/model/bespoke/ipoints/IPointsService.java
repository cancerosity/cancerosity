package org.sevensoft.ecreator.model.bespoke.ipoints;

import java.io.IOException;

import org.jdom.JDOMException;
import org.samspade79.feeds.ipoints.IPointsAddTransactionRequest;
import org.samspade79.feeds.ipoints.IPointsAddTransactionResponse;
import org.samspade79.feeds.ipoints.IPointsGetUserIdRequest;
import org.samspade79.feeds.ipoints.IPointsGetUserIdResponse;
import org.samspade79.feeds.ipoints.IPointsRegisterUserRequest;
import org.samspade79.feeds.ipoints.IPointsRegisterUserResponse;
import org.samspade79.feeds.ipoints.exceptions.IPointsConnectionException;
import org.samspade79.feeds.ipoints.exceptions.IPointsRequestException;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.skint.Money;

/**
 * @author Stephen K Samuel samspade79@gmail.com 25 Mar 2009 20:19:36
 */
public class IPointsService {

    private RequestContext context;
    IPointsSettings settings;

    public IPointsService(RequestContext context) {
        this.context = context;
        settings = IPointsSettings.getInstance(context);
    }

    public void addPoints(Order order) throws IOException, JDOMException, IPointsConnectionException, IPointsRequestException {

        synchronized (IPointsService.class) {
            try {

                IPointsGetUserIdRequest req = new IPointsGetUserIdRequest();
                req.setPartnerUsername(settings.getUsername());
                req.setPartnerPassword(settings.getPassword());
                req.setUserEmail(order.getIpointsUsername());
                req.setUserPassword(order.getIpointsPassword());

                IPointsGetUserIdResponse resp = req.postRequest();
                int memberId = resp.getMemberId();

                int points = getPoints(order.getTotalInc());

                IPointsAddTransactionRequest req2 = new IPointsAddTransactionRequest();
                req2.setPartnerUsername(settings.getUsername());
                req2.setPartnerPassword(settings.getPassword());
                req2.setDescription("Order " + order.getIdString());
                req2.setTransactionRef(order.getIdString());
                req2.setPointsToAdd(points);
                req2.setMemberId(String.valueOf(memberId));

                IPointsAddTransactionResponse resp2 = req2.postRequest();
                String ipointsReference = resp2.getIpTransactionRef();

                order.addMessage("System", "none", points + "Points added");
                order.setIpointsReference(ipointsReference);
                order.save();

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();

            } catch (JDOMException e) {
                e.printStackTrace();

            } catch (IPointsConnectionException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }

    }

    public void addUser(Item account) throws IOException, JDOMException, IPointsConnectionException, IPointsRequestException {

        IPointsRegisterUserRequest req = new IPointsRegisterUserRequest();
        req.setEmail(account.getEmail());
        req.setFirstName(account.getFirstName());
        req.setLastName(account.getLastName());
        req.setDob(System.currentTimeMillis() - 10000000000000l);
        req.setPostcode("S80 3GB");
        req.setPartnerPassword(settings.getPassword());
        req.setPartnerUsername(settings.getUsername());
        req.setTitle("Mr");
        req.setCity("Sheffield");

        IPointsRegisterUserResponse resp = req.postRequest();
        int memberId = resp.getMemberId();

        account.addMessage("System", "none", "Registered on ipoints memberId=" + memberId);

    }

    /**
     * Lookup a member and check they exist
     * <p/>
     * Will throw an exception if the user does not exist
     *
     * @throws IPointsConnectionException
     * @throws JDOMException
     * @throws IOException
     * @throws IPointsRequestException
     */
    public void checkMember(String ipointsUsername, String ipointsPassword) throws IOException, JDOMException, IPointsConnectionException,
            IPointsRequestException {

        IPointsGetUserIdRequest req = new IPointsGetUserIdRequest();
        req.setPartnerUsername(settings.getUsername());
        req.setPartnerPassword(settings.getPassword());
        req.setUserEmail(ipointsUsername);
        req.setUserPassword(ipointsPassword);

        req.postRequest();
    }

    /**
     *
     */
    public int getPoints(Money total) {

        // calculate points from points per pound
        int dollarAmount = total.getAmount() / 100;
        int points = settings.getPointsPerPound() * dollarAmount;
		return points;
	}
}
