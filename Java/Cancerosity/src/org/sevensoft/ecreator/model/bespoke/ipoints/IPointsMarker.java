package org.sevensoft.ecreator.model.bespoke.ipoints;

import java.util.Map;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author Stephen K Samuel samspade79@gmail.com 25 Apr 2009 12:38:01
 *         <p/>
 *         Display the number of ipoints a product will generate
 */
public class IPointsMarker extends MarkerHelper implements IItemMarker, IBasketMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

        IPointsService s = new IPointsService(context);
        int points = s.getPoints(item.getSellPriceInc());

        return super.string(context, params, String.valueOf(points));
    }

    public Object getRegex() {
        return "ipoints_total";
    }

    public Object generate(RequestContext context, Map<String, String> params, Basket basket) {

        Money total = basket.getTotalInc();
        IPointsService s = new IPointsService(context);
        int points = s.getPoints(total);

        return super.string(context, params, String.valueOf(points));
    }

}
