package org.sevensoft.ecreator.model.attachments;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 19 Sep 2006 18:08:45
 *
 */
@Table("attachments_groups")
public class AttachmentGroup extends EntityObject {

	public AttachmentGroup(RequestContext context) {
		super(context);
	}

}
