package org.sevensoft.ecreator.model.attachments.panels;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 09-Feb-2006 15:12:42
 *
 */
public class AttachmentMarkupDefault implements MarkupDefault {

	public String getBetween() {
		return null;
	}

	public String getCss() {
		return null;
	}

	public String getBody() {

		StringBuilder sb = new StringBuilder();

		sb.append("<style>\n");
		sb.append("table.attachments { width: 100%; background: #fbf3f0; margin-bottom: 8px; font-weight: bold; font-size: 11px; } \n");
		sb.append("table.attachments td.icon { width: 60px; background: #fbf3f0; margin-bottom: 8px;	} \n");
		sb.append("</style>\n\n\n");

		sb.append(new TableTag("attachments"));
		sb.append("\n<tr>\n");
		sb.append("<td width='50' class='icon' align='center'>[attachments_filetype]</td>\n");
		sb.append("<td class='name'>[attachments_name] - [attachments_size]<br/>[attachments_link]</td>\n");
		sb.append("</tr>\n");
		sb.append("</table>");

		return sb.toString();
	}

	public String getEnd() {
		return null;
	}

	public String getName() {
		return null;
	}

	public String getStart() {
		return null;
	}

	public int getTds() {
		return 0;
	}

}
