package org.sevensoft.ecreator.model.attachments;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.commons.simpleio.SimpleUrl;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.frontend.attachments.AttachmentHandler;
import org.sevensoft.ecreator.model.attachments.downloads.Download;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.attachments.previews.PreviewException;
import org.sevensoft.ecreator.model.attachments.previews.audio.Mp3Previewer;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.crm.forms.submission.Submission;
import org.sevensoft.ecreator.model.crm.messages.Msg;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLineOption;
import org.sevensoft.ecreator.model.interaction.pm.PrivateMessage;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.listings.sessions.ListingSession;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.containers.boxes.misc.CustomBox;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.QueryBuilder;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Selectable;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sks 29-Aug-2005 10:37:21
 */
@Table("files")
public class Attachment extends EntityObject implements Selectable, Positionable {

    public enum Method {

        /**
         * Download as file
         */
        Download,

        /**
         * Stream to browser as inline file
         */
        Stream,

    }

    public static final Map<String, Filetype> Filetypes;

    static {

        Filetypes = new HashMap<String, Filetype>();

        // images
        Filetypes.put("bmp", Filetype.Bitmap);
        Filetypes.put("png", Filetype.Png);
        Filetypes.put("gif", Filetype.Gif);
        Filetypes.put("tif", Filetype.Tiff);
        Filetypes.put("tiff", Filetype.Tiff);
        Filetypes.put("jpg", Filetype.JPeg);
        Filetypes.put("jpeg", Filetype.JPeg);

        // ms
        Filetypes.put("ppt", Filetype.Powerpoint);
        Filetypes.put("pptx", Filetype.Powerpoint);
        Filetypes.put("mdf", Filetype.Access);
        Filetypes.put("xls", Filetype.Excel);
        Filetypes.put("xlsx", Filetype.Excel);
        Filetypes.put("mpp", Filetype.Generic);

        // adobe
        Filetypes.put("psd", Filetype.Photoshop);

        // compression
        Filetypes.put("zip", Filetype.Zip);
        Filetypes.put("rar", Filetype.Rar);

        // web
        Filetypes.put("swf", Filetype.Flash);

        // video
        Filetypes.put("mov", Filetype.Quicktime);
        Filetypes.put("mpeg", Filetype.Mpeg);
        Filetypes.put("mpg", Filetype.Mpeg);
        Filetypes.put("avi", Filetype.Mpeg);
        Filetypes.put("wmv", Filetype.Mpeg);

        // audio
        Filetypes.put("mp3", Filetype.Mp3);
        Filetypes.put("wav", Filetype.Wave);
        Filetypes.put("wma", Filetype.Wma);

        // document
        Filetypes.put("pdf", Filetype.Pdf);
        Filetypes.put("rtf", Filetype.RichText);

        // text
        Filetypes.put("html", Filetype.Html);
        Filetypes.put("htm", Filetype.Html);
        Filetypes.put("log", Filetype.Text);
        Filetypes.put("txt", Filetype.Text);
        Filetypes.put("csv", Filetype.Excel);
        Filetypes.put("tsv", Filetype.Excel);
        Filetypes.put("doc", Filetype.Word);
        Filetypes.put("docx", Filetype.Word);
        Filetypes.put("css", Filetype.Html);

        // ms visio
        Filetypes.put("vsw", Filetype.Generic);
        Filetypes.put("vst", Filetype.Generic);
        Filetypes.put("vss", Filetype.Generic);
        Filetypes.put("vsd", Filetype.Generic);

    }

    public static final String PDF_EXTENSION = ".pdf";
    public static final String WMV_EXTENSION = ".wmv";
    public static final String AVI_EXTENSION = ".avi";
    public static final String MPG_EXTENSION = ".mpg";
    public static final String MPEG_EXTENSION = ".mpeg";
    public static final String JPG_EXTENXION = ".jpg";
    public static final String JPEG_EXTENSION = ".jpeg";
    public static final String GIF_EXTENSION = ".gif";
    public static final String TIFF_EXTENSION = "tiff";
    public static final String BMP_ECTENSION = ".bmp";
    public static final String TIF_EXTENSION = ".tif";
    public static final String PNG_EXTENSION = ".png";

    private static List<Attachment> get(RequestContext context) {
        return SimpleQuery.execute(context, Attachment.class);
    }

    public static List<Attachment> get(RequestContext context, String extension) {

        QueryBuilder b = new QueryBuilder(context);
        b.select("*");
        b.from("#", Attachment.class);
        if (extension != null) {
            b.clause("filename like ?", "%" + extension);
        }
        return b.toQuery().execute(Attachment.class);
    }

    public static boolean hasAttachments(RequestContext context) {
        return SimpleQuery.count(context, Attachment.class) > 0;
    }

    /**
     * The download method - streaming or attachment
     */
    private Method method;

    private BasketLineOption basketLineOption;

    private BasketLine basketLine;

    private OrderLine orderLine;

    private Category category;

    private int position;

    /**
     * Custom markup for rendering this attachment
     */
    private Markup markup;

    /*
      *Date / time uploaded
      */
    private DateTime dateUploaded;

    /**
     * Number of credits needed to download this file
     */
    private int credits;

    /**
     * The filename is just the final part of hte path,
     * eg web-inf/files/category-102/doc.txt
     * the filename is just doc.txt
     */
    private String filename;

    /**
     * The path contains the filename plus directory
     */
    private String path;

    /**
     * The size of the file in bytes
     */
    private long size;

    /**
     * The preview filename
     */
    private String previewFilename;

    /*
      * Renames the actual link text
      */
    private String downloadLinkText;

    /**
     * Rename the actual name of this attachment instead of showing the filename.
     */
    private String name;

    /**
     * Number of days a download will stay active for before it expires when credits are required.
     */
    private int validityDays;

    /**
     * Owner item
     */
    private Item item;

    /**
     * The cost to download this attachment
     */
    private Money fee;

    /*
      *The owner msg
      */
    private Msg msg;

    /*
      *The directory this attachment is located in
      */
    private String directory;

    /**
     * The download owner
     */
    private Download download;

    /**
     * Listing session owner
     */
    private ListingSession listingSession;

    /**
     * Submission owner
     */
    private Submission submission;

    /**
     * PM owner
     */
    private PrivateMessage privateMessage;

    /**
     * PM owner
     */
    private CustomBox customBox;

    private Attachment(RequestContext context) {
        super(context);
    }

    /**
     * Copies this file onto another file owner.
     */
    public Attachment(RequestContext context, AttachmentOwner owner, Attachment attachment) throws AttachmentExistsException, AttachmentTypeException,
            IOException, AttachmentLimitException {
        this(context, owner, attachment.getFile(), attachment.getFilename(), false);
    }

    public Attachment(RequestContext context, AttachmentOwner owner, File file, boolean preview) throws FileNotFoundException, AttachmentTypeException,
            AttachmentExistsException, IOException, AttachmentLimitException {
        super(context);
        init(owner, new FileInputStream(file), file.getName(), preview);
    }

    public Attachment(RequestContext context, AttachmentOwner owner, File sourceFile, String filename, boolean preview) throws AttachmentExistsException,
            AttachmentTypeException, IOException, AttachmentLimitException {
        super(context);
        init(owner, new FileInputStream(sourceFile), filename, preview);
    }

    public Attachment(RequestContext context, AttachmentOwner owner, InputStream in, String filename, boolean preview) throws AttachmentExistsException,
            IOException, AttachmentTypeException, AttachmentLimitException {
        super(context);
        init(owner, in, filename, preview);
    }

    public Attachment(RequestContext context, AttachmentOwner owner, Upload upload, boolean preview) throws AttachmentExistsException, IOException,
            AttachmentTypeException, AttachmentLimitException {
        this(context, owner, upload.getFile(), upload.getFilenameSafe(), preview);
    }

    public Attachment(RequestContext context, AttachmentOwner owner, URL url, boolean preview) throws IOException, AttachmentTypeException,
            AttachmentExistsException, AttachmentLimitException {
        super(context);

        byte[] b = SimpleUrl.downloadToByteArray(url);
        File file = new File(url.getPath());

        init(owner, new ByteArrayInputStream(b), file.getName(), preview);
    }

    @Override
    public synchronized boolean delete() {

        File file = getFile();
        boolean lastFile = count(path) == 1;
        if (file.exists() && lastFile) {
            file.delete();
        }

        /*
           * Delete sample
           */
        if (previewFilename != null) {

            File sampleFile = ResourcesUtils.getRealAttachment(directory + File.separator + previewFilename);
            if (sampleFile.exists() && lastFile) {
                sampleFile.delete();
            }
        }

        File samplePdfFile = ResourcesUtils.getRealPdf(filename);
        if (samplePdfFile.exists() && lastFile) {
            samplePdfFile.delete();
        }

        // delete from downloads
        SimpleQuery.delete(context, Download.class, "attachment", this);

        super.delete();

        /*
           * If directory is empty delete.
           */
        if (directory != null) {

            File dir = ResourcesUtils.getRealAttachment(directory);
            if (dir.exists() && dir.isDirectory() && dir.list().length == 0) {
                dir.delete();
            }
        }

        getOwner().setAttachmentCount();
        ((EntityObject) getOwner()).save();

        return true;
    }

    private BasketLine getBasketLine() {
        return (BasketLine) (basketLine == null ? null : basketLine.pop());
    }

    public BasketLineOption getBasketLineOption() {
        return (BasketLineOption) (basketLineOption == null ? null : basketLineOption.pop());
    }

    public Category getCategory() {
        return (Category) (category == null ? null : category.pop());
    }

    public String getContentType() {
        return getFiletype().getContentType();
    }

    public int getCredits() {

        if (!Module.Credits.enabled(context)) {
            return 0;
        }

        return credits;
    }

    public DateTime getDateUploaded() {
        return dateUploaded;
    }

    public String getDirectory() {
        return directory;
    }

    public LinkTag getDownloadLinkTag() {
        return new LinkTag(getRelativeUrl(), getDownloadLinkText());
    }

    public final String getDownloadLinkText() {
        return downloadLinkText == null ? AttachmentSettings.getInstance(context).getDefaultDownloadLinkText() : downloadLinkText;
    }

    /**
     * Returns the extension excluding the dot.
     */
    public String getExtension() {
        return SimpleFile.getExtension(filename);
    }

    public Money getFee() {
        return fee;
    }

    public File getImageFile() {
        return ResourcesUtils.getRealImage(getPath());
    }

    /**
     * Returns a File object to the real file on the system.
     */
    public File getFile() {
        if (isVideoFile(getPath())) {
            return getVideoFile();
        } else if (isImageFile(getPath())) {
            return getImageFile();
        } else if (isPdfFile(getPath())) {
            return getPdfFile();
        } else {
            return ResourcesUtils.getRealAttachment(getPath());
        }
    }

    /**
     * Returns a File object to the real file on the system.
     * @return file
     */
    public File getPdfFile() {
        return ResourcesUtils.getRealPdf(getPath());
    }

     /**
     * Returns a File object to the real file on the system.
      * @return file
      */
    public File getVideoFile() {
        return ResourcesUtils.getRealVideo(getPath());
    }


    /**
     * Returns the filename for this attachment.
     * This is just the filename excluding the path
     */
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Filetype getFiletype() {
        Filetype filetype = Filetypes.get(SimpleFile.getExtension(filename));
        return filetype == null ? Filetype.Generic : filetype;
    }

    public ImageTag getIconTag() {
        return new ImageTag("files/graphics/filetypes/" + getFiletype().name().toLowerCase() + ".gif");
    }

    /**
     * Returns this attachment as an AWT image
     */
    public BufferedImage getImage() throws IOException {
        return ImageIO.read(getFile());
    }

    public Item getItem() {
        return (Item) (item == null ? null : item.pop());
    }

    public String getLabel() {
        return getFilename();
    }

    public ListingSession getListingSession() {
        return (ListingSession) (listingSession == null ? null : listingSession.pop());
    }

    public Markup getMarkup() {
        return (Markup) (markup == null ? null : markup.pop());
    }

    public final Msg getMessage() {
        return getMsg();
    }

    public Method getMethod() {
        return method == null ? Method.Download : method;
    }

    public Msg getMsg() {
        return (Msg) (msg == null ? null : msg.pop());
    }

    public String getName() {
        return name == null ? getFilename() : name;
    }

    public OrderLine getOrderLine() {
        return (OrderLine) (orderLine == null ? null : orderLine.pop());
    }

    public AttachmentOwner getOwner() {

        if (hasItem()) {
            return getItem();

        } else if (category != null) {
            return getCategory();
        } else if (basketLine != null) {
            return getBasketLine();
        } else if (msg != null) {
            return getMessage();
        } else if (basketLineOption != null) {
            return getBasketLineOption();
        } else if (hasListingSession()) {
            return getListingSession();
        } else if (hasPrivateMessage()) {
            return getPrivateMessage();
        } else if (hasCustomBox()) {
            return getCustomBox();
        } else
            throw new RuntimeException("File owner class not recog");
    }

    /**
     * Returns complete path
     */
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    /**
     *
     */
    public String getPlaylistUrl() {
        return Config.getInstance(context).getUrl() + "/" + getIdString() + ".mp3";
    }

    public int getPosition() {
        return position;
    }

    public String getPreviewFilename() {
        return previewFilename;
    }

    public String getPreviewPath() {
        return directory + "/" + previewFilename;
    }

    public final PrivateMessage getPrivateMessage() {
        return (PrivateMessage) (privateMessage == null ? null : privateMessage.pop());
    }

    public final CustomBox getCustomBox() {
        return (CustomBox) (customBox == null ? null : customBox.pop());
    }

    public Link getRelativeUrl() {
        return new Link(AttachmentHandler.class, "download", "attachment", this);
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getSizeKb() {
        return size / 1000 + "kb";
    }

    public String getUrl() {
        return Config.getInstance(context).getUrl() + "/" + new Link(AttachmentHandler.class, "download", "attachment", this);
    }

    public int getValidityDays() {
        return validityDays;
    }

    public String getValue() {
        return getIdString();
    }

    public boolean hasCategory() {
        return category != null;
    }

    public boolean hasCredits() {
        return credits > 0;
    }

    public boolean hasFee() {
        return fee.isPositive();
    }

    public boolean hasItem() {
        return item != null;
    }

    private boolean hasListingSession() {
        return listingSession != null;
    }

    public boolean hasPreview() {
        return previewFilename != null;
    }

    /**
     *
     */
    public boolean hasPrivateMessage() {
        return privateMessage != null;
    }

    public boolean hasCustomBox() {
        return customBox != null;
    }

    private void init(AttachmentOwner owner, InputStream in, String filename, boolean preview) throws AttachmentTypeException, AttachmentExistsException,
            IOException, AttachmentLimitException {

        AttachmentSettings settings = AttachmentSettings.getInstance(context);

        try {

            if (owner.isAtAttachmentLimit()) {
                throw new AttachmentLimitException();
            }

            if (isPdfFile(filename)) {
                this.directory = "";
            } else if (isVideoFile(filename)) {
                if (owner instanceof ListingSession) {
                    this.directory = ((ListingSession)owner).getItem().getClass().getSimpleName().toLowerCase() + "-" + ((ListingSession)owner).getItem().getId();
                } else {
                    this.directory = owner.getClass().getSimpleName().toLowerCase() + "-" + ((EntityObject) owner).getId();
                }
            } else {
                this.directory = owner.getClass().getSimpleName().toLowerCase() + "-" + ((EntityObject) owner).getId();
            }
            this.path = directory + "/" + filename;
            this.filename = filename;
            this.fee = settings.getDefaultFee();
            this.credits = settings.getDefaultCredits();

            // check we accept this filetype
            if (!new AttachmentFilter().accept(filename)) {
                logger.fine("[Attachment] Rejecting attachment type: " + filename);
                throw new AttachmentTypeException();
            }

            // check file is unique in db
            boolean fileExist = isNotUnique(path);
            boolean pdfFile = isPdfFile(path);

            if (fileExist && !pdfFile) {
                logger.fine("[Attachment] existing filename: " + filename);
                throw new AttachmentExistsException("A file already exists with that name.");
            }

            // check directory exists
            if (isVideoFile(filename)) {
                ResourcesUtils.getRealVideo(directory).mkdir();
            } else if (isImageFile(filename)) {
                ResourcesUtils.getRealImage(directory).mkdir();
            } else if (pdfFile) {
                ResourcesUtils.getRealPdfsDir().mkdir();
            } else {
                ResourcesUtils.getRealAttachment(directory).mkdir();
            }

            // 	 destination file
            File destinationFile;
            if (!pdfFile) {
                if (isVideoFile(filename)) {
                    destinationFile = ResourcesUtils.getRealVideo(path);
                } else if (isImageFile(filename)) {
                    destinationFile = ResourcesUtils.getRealImage(path);
                } else {
                    destinationFile = ResourcesUtils.getRealAttachment(path);
                }
            } else {
                destinationFile = ResourcesUtils.getRealPdf(path);
            }

            /*
                * Write out file
                */
            if (!(fileExist && destinationFile.exists()) && destinationFile.length() == 0) {
                SimpleFile.writeStream(in, destinationFile);
            }

            // set size
            this.size = destinationFile.length();
            this.dateUploaded = new DateTime();

            if (owner instanceof Category) {
                this.category = (Category) owner;

            } else if (owner instanceof Download) {
                this.download = (Download) owner;

            } else if (owner instanceof Item) {
                this.item = ((Item) owner);

            } else if (owner instanceof BasketLine) {
                this.basketLine = (BasketLine) owner;

            } else if (owner instanceof BasketLineOption) {
                this.basketLineOption = (BasketLineOption) owner;

            } else if (owner instanceof OrderLine) {
                this.orderLine = (OrderLine) owner;

            } else if (owner instanceof Msg) {
                this.msg = (Msg) owner;

            } else if (owner instanceof ListingSession) {
                this.listingSession = (ListingSession) owner;

            } else if (owner instanceof Submission) {
                this.submission = (Submission) owner;

            } else if (owner instanceof PrivateMessage) {
                this.privateMessage = (PrivateMessage) owner;

            } else if (owner instanceof CustomBox) {
                this.customBox = (CustomBox) owner;

            } else {
                throw new RuntimeException("File owner class not recognised: " + owner);
            }

            save();

            owner.setAttachmentCount();
            ((EntityObject) owner).save();

            if (preview) {
                try {
                    makePreview(null, false);
                } catch (PreviewException e) {
                    logger.fine("[Attachment] " + e);
                    e.printStackTrace();
                }
            }

        } catch (AttachmentTypeException e) {
            throw e;

        } catch (AttachmentExistsException e) {
            throw e;

        } catch (IOException e) {
            throw e;

        } finally {

            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    private boolean isNotUnique(String path) {
        int q = count(path);

        boolean fileExist = q > 0;
        return fileExist;
    }

    private Integer count(String path) {
        Query q = new Query(context, "select count(*) from # where path=?");
        q.setTable(getClass());
        q.setParameter(path);
        return q.getInt();
    }

    public boolean isDownload() {
        return true;
    }

    /**
     * @return
     */
    public boolean isFree() {
        return credits == 0 && fee.isZero();
    }

    public boolean isStream() {
        return getMethod() == Method.Stream;
    }

    public void makePreview(String extension, boolean overwrite) throws PreviewException {

        AttachmentSettings attachmentSettings = AttachmentSettings.getInstance(context);

        logger.fine("[Attachment] preview filetype=" + getFiletype());

        logger.fine("[Attachment] checking attachment has appropriate extension=" + extension);
        if (extension != null) {
            if (!SimpleFile.getExtension(getFilename()).equalsIgnoreCase(extension)) {
                logger.fine("[Attachment] attachment filename not required extension");
                return;
            }
        }

        // check this filetype is covered by our preview types
        if (!attachmentSettings.isPreviewing(getExtension())) {
            logger.fine("[Attachment] previews not used for this extension, exiting");
            return;
        }

        // if we already have a preview filename check it exists
        if (previewFilename != null) {

            File file = context.getRealFile(getPreviewPath());
            logger.fine("[Attachment] existing preview=" + file);

            if (file.exists()) {

                // if overwrite clear existing preview
                if (overwrite) {

                    logger.fine("[Attachment] preview confirmed, but we are overwriting so deleting file");
                    file.delete();
                    previewFilename = null;

                } else {

                    logger.fine("[Attachment] preview confirmed, exiting");
                    return;

                }

            } else {

                logger.fine("[Attachment] preview does not exist, clearing");
                previewFilename = null;
            }
        }

        // generate preview filename
        previewFilename = SimpleFile.appendFilename(getFilename(), "_preview");
        logger.fine("[Attachment] made filename=" + previewFilename);

        // if preview already exists do not remake or delete
        File file = context.getRealFile(getPreviewPath());
        if (file.exists()) {

            if (overwrite) {

                logger.fine("[Attachment] preview already exists, but we are overwriting so deleting file");
                file.delete();

            } else {

                logger.fine("[Attachment] filename already exists=" + file);
                save("previewFilename");
                return;

            }
        }

        try {

            switch (getFiletype()) {

                default:
                    break;

                case Mp3:

                    new Mp3Previewer(context, attachmentSettings.getMp3PreviewLength()).sample(this, previewFilename);
            }

        } catch (PreviewException e) {

            previewFilename = null;
            logger.warning(e.toString());
            throw e;

        } finally {

            logger.fine("[Attachment] preview generated, saving");
            save("previewFilename");
        }

    }

    public static boolean isImageFile(String filename) {
        return filename.indexOf(JPG_EXTENXION) != -1 || filename.indexOf(JPEG_EXTENSION) != -1 ||
                filename.indexOf(TIF_EXTENSION) != -1 || filename.indexOf(TIFF_EXTENSION) != -1 ||
                filename.indexOf(BMP_ECTENSION) != -1 || filename.indexOf(PNG_EXTENSION) != -1 ||
                filename.indexOf(GIF_EXTENSION) != -1;
    }

    public static boolean isPdfFile(String filaname) {
        return filaname.indexOf(PDF_EXTENSION) != -1;
    }

    public static boolean isVideoFile(String filename) {
        return filename.indexOf(WMV_EXTENSION) != -1 || filename.indexOf(AVI_EXTENSION) != -1
            || filename.indexOf(MPG_EXTENSION) != -1 || filename.indexOf(MPEG_EXTENSION) != -1;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public final void setDownloadLinkText(String description) {
        this.downloadLinkText = description;
    }

    public void setFee(Money fee) {
        this.fee = fee;
    }

    public void setMarkup(Markup markup) {
        this.markup = markup;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setValidityDays(int validityDays) {
        this.validityDays = validityDays;
    }

    @Override
    public Attachment clone() throws CloneNotSupportedException {
        return (Attachment) super.clone();
    }
}
