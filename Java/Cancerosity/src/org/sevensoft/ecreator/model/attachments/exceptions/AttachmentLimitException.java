package org.sevensoft.ecreator.model.attachments.exceptions;


/**
 * @author sks 13-Oct-2005 14:26:46
 *
 */
public class AttachmentLimitException extends Exception {

	public AttachmentLimitException() {
		super();
	}

	public AttachmentLimitException(String message) {
		super(message);
	}

	public AttachmentLimitException(String message, Throwable cause) {
		super(message, cause);
	}

	public AttachmentLimitException(Throwable cause) {
		super(cause);
	}

}
