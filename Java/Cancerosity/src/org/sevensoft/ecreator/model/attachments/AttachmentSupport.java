package org.sevensoft.ecreator.model.attachments;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collection;
import java.util.List;

import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;

/**
 * @author sks 4 Sep 2006 19:30:47
 *
 */
public abstract class AttachmentSupport extends EntityObject implements AttachmentOwner {

	private int	attachmentCount;

	public AttachmentSupport(RequestContext context) {
		super(context);
	}

	public boolean acceptedFiletype(String filename) {
		return new AttachmentFilter().accept(filename);
	}

	public final Attachment addAttachment(Attachment attachment) throws AttachmentExistsException, AttachmentTypeException, IOException,
			AttachmentLimitException {
		return new Attachment(context, this, attachment);
	}

	public final Attachment addAttachment(File file, String fileName) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException {
		return new Attachment(context, this, file, false);
	}

	public Attachment addAttachment(InputStream in, String filename) throws AttachmentExistsException, IOException, AttachmentTypeException,
			AttachmentLimitException {
		return new Attachment(context, this, in, filename, false);
	}

	public final Attachment addAttachment(Upload upload, boolean sample) throws IOException, AttachmentLimitException, AttachmentExistsException,
			AttachmentLimitException, AttachmentTypeException {
		return new Attachment(context, this, upload, false);
	}

	public final Attachment addAttachment(URL url) throws IOException, AttachmentTypeException, AttachmentExistsException, AttachmentLimitException {

		Attachment attachment = new Attachment(context, this, url, false);

		setAttachmentCount();
		save();

		return attachment;
	}

	public final void addAttachments(Collection<Upload> uploads) throws IOException, AttachmentLimitException, AttachmentExistsException,
			AttachmentTypeException {
		for (Upload upload : uploads)
			addAttachment(upload, false);
	}

	public final void copyAttachmentsTo(AttachmentOwner target) throws AttachmentExistsException, AttachmentTypeException, IOException,
			AttachmentLimitException {
		AttachmentUtil.copyAttachmentsTo(this, target, context);
	}

	@Override
	public synchronized boolean delete() {
		removeAttachments();
		return super.delete();
	}

	public Attachment getAttachment() {
		List<Attachment> attachments = getAttachments();
		return attachments.isEmpty() ? null : attachments.get(0);
	}

	public final int getAttachmentCount() {
		return attachmentCount;
	}

	public final List<Attachment> getAttachments() {
		return AttachmentUtil.getAttachments(context, this);
	}

	public final boolean hasAttachments() {
		return attachmentCount > 0;
	}

	public boolean isAtAttachmentLimit() {
		return getAttachmentLimit() > 0 && getAttachmentLimit() >= getAttachmentCount();
	}

	public final void moveAttachmentsTo(AttachmentOwner target) throws AttachmentExistsException, AttachmentTypeException, IOException,
			AttachmentLimitException {
		AttachmentUtil.moveAttachmentsTo(context, this, target);
	}

	public final void removeAttachment(Attachment a) {
		a.delete();
	}

	public final void removeAttachments() {
		for (Attachment a : getAttachments())
			removeAttachment(a);
	}

	public final void setAttachmentCount() {
		this.attachmentCount = getAttachments().size();
		save();
	}

}
