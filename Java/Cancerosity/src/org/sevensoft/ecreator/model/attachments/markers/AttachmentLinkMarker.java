package org.sevensoft.ecreator.model.attachments.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.attachments.AttachmentHandler;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.Filetype;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IAttachmentMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 12 Jul 2006 09:19:17
 *
 */
public class AttachmentLinkMarker extends MarkerHelper implements IAttachmentMarker {

	public Object generate(RequestContext context, Map<String, String> params, Attachment attachment) {

		String text = params.get("text");
		if (text == null) {
			text = attachment.getDownloadLinkText();
		}

		boolean preview = params.containsKey("preview");
		if (preview && !attachment.hasPreview()) {
			logger.fine("[AttachmentLinkMarker] No preview, exiting");
			return null;
		}

		if (attachment.getMethod() == Attachment.Method.Stream && attachment.getFiletype() == Filetype.Mp3) {

			// return a .m3u playlist

			Link link = new Link(AttachmentHandler.class, "m3u", "attachment", attachment);
			if (preview) {
				link.setParameter("preview", true);
			}
			return new LinkTag(link, text);

			//			Link link = new Link(AttachmentHandler.class, "popupMp3", "attachment", attachment, "preview", preview);

			//			JavascriptLinkTag js = new JavascriptLinkTag(context, link, text, 320, 320);
			//			js.setTarget("_blank");
			//			return js;

		} else {

			Link link = new Link(AttachmentHandler.class, null, "attachment", attachment);
			if (preview) {
				link.setParameter("preview", true);
			}
			if (attachment.hasCategory()) {
				link.setParameter("category", attachment.getCategory());
			}
			if (attachment.hasItem()) {
				link.setParameter("item", attachment.getItem());
			}
			return new LinkTag(link, text);
		}
	}

	public Object getRegex() {
		return "attachments_link";
	}

	@Override
	public String toString() {
		return "Makes a link to download the attachment's file. Use param preview=1 to download a preview if applicable.";
	}

}
