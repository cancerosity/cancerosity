package org.sevensoft.ecreator.model.attachments.previews;

/**
 * @author sks 5 Feb 2007 08:28:01
 *
 */
public class PreviewException extends Exception {

	public PreviewException() {
	}

	public PreviewException(String message) {
		super(message);
	}

	public PreviewException(String message, Throwable cause) {
		super(message, cause);
	}

	public PreviewException(Throwable cause) {
		super(cause);
	}

}
