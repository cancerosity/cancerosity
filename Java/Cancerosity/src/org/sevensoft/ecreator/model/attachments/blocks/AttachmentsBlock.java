package org.sevensoft.ecreator.model.attachments.blocks;

import org.sevensoft.ecreator.model.attachments.panels.FrontendAttachmentsPanel;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 28 Jul 2006 12:54:59
 * 
 * Holder class for attachments on block owners
 */
@Table("blocks_attachments")
@Label("Attachments")
public class AttachmentsBlock extends Block {

	public AttachmentsBlock(RequestContext context) {
		super(context);
	}

	public AttachmentsBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, 0);
	}

	@Override
	public Object render(RequestContext context) {

		Item item = (Item) context.getAttribute("item");
		if (item != null) {
			return new FrontendAttachmentsPanel(context, item);
		}

		Category category = (Category) context.getAttribute("category");
		if (category != null) {
			return new FrontendAttachmentsPanel(context, category);
		}

		return null;
	}

}
