package org.sevensoft.ecreator.model.attachments.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.attachments.AttachmentHandler;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.design.marker.interfaces.IAttachmentMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 12 Jul 2006 09:20:07
 *
 */
public class AttachmentFileTypeMarker implements IAttachmentMarker {

	public Object generate(RequestContext context, Map<String, String> params, Attachment attachment) {

		return new LinkTag(AttachmentHandler.class, null, new ImageTag("files/graphics/filetypes/" + attachment.getFiletype().name().toLowerCase() + ".gif"),
				"attachment", attachment);
	}

	public Object getRegex() {
		return "attachments_filetype";
	}

}
