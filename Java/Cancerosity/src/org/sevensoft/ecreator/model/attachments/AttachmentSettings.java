package org.sevensoft.ecreator.model.attachments;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.attachments.panels.AttachmentMarkupDefault;
import org.sevensoft.ecreator.model.attachments.previews.PreviewException;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 8 Jun 2006 14:57:56
 *
 */
@Table("settings_attachments")
@Singleton
public class AttachmentSettings extends EntityObject {

	public static AttachmentSettings getInstance(RequestContext context) {
		return getSingleton(context, AttachmentSettings.class);
	}

	/**
	 *  Default markup
	 */
	private Markup		markup;

	private String		defaultDownloadLinkText;
	private int			defaultCredits;
	private Money		defaultFee;
	private int			defaultValidityDays;
	private String		mp3PreviewLength;
	private List<String>	previewExtensions;

	public AttachmentSettings(RequestContext context) {
		super(context);
	}

	public int getDefaultCredits() {
		return defaultCredits;
	}

	public String getDefaultDownloadLinkText() {
		return defaultDownloadLinkText == null ? "Download this file" : defaultDownloadLinkText;
	}

	public Money getDefaultFee() {
		return defaultFee;
	}

	public int getDefaultValidityDays() {
		return defaultValidityDays;
	}

	public Markup getMarkup() {

		if (markup == null) {

			markup = new Markup(context, "Default attachments markup", new AttachmentMarkupDefault());
			save();

		}

		return markup.pop();
	}

	public final String getMp3PreviewLength() {
		return mp3PreviewLength == null ? "0:20" : mp3PreviewLength;
	}

	public List<String> getPreviewExtensions() {
		if (previewExtensions == null) {
			previewExtensions = new ArrayList();
		}
		return previewExtensions;
	}

	public boolean hasDefaultCredits() {
		return defaultCredits > 0;
	}

	public boolean hasDefaultFee() {
		return defaultFee != null && defaultFee.isPositive();
	}

	public boolean isPreviewing(String string) {
		if (string == null) {
			return false;
		}

		if (string.startsWith(".")) {
			string = string.substring(1);
		}

		string = string.toLowerCase();
		return getPreviewExtensions().contains(string);
	}

	/**
	 * Cycle through all attachments making previews 
	 */
	public void makePreviews(String extension, boolean overwrite) {

		List<Attachment> as = Attachment.get(context, extension);
		logger.fine("[AttachmentSettings] calling make previews on " + as.size() + " attachments");

		for (Attachment a : as) {

			try {
				logger.fine("[AttachmentSettings] generating preview for attachment=" + a);
				a.makePreview(extension, overwrite);
			} catch (PreviewException e) {
				e.printStackTrace();
			}
		}
	}

	public void resetCredits(String extension, int credits) {

		for (Attachment a : Attachment.get(context, extension)) {
			a.setCredits(credits);
			a.save();
		}
	}

	public void resetFee(String extension, Money fee) {

		for (Attachment a : Attachment.get(context, extension)) {
			a.setFee(fee);
			a.save();
		}
	}

	public void setDefaultCredits(int defaultCredits) {
		this.defaultCredits = defaultCredits;
	}

	public final void setDefaultDownloadLinkText(String s) {
		this.defaultDownloadLinkText = s;
	}

	public void setDefaultFee(Money defaultFee) {
		this.defaultFee = defaultFee;
	}

	public void setDefaultValidityDays(int defaultValidityDays) {
		this.defaultValidityDays = defaultValidityDays;
	}

	public void setMarkup(Markup markup) {
		this.markup = markup;
	}

	public final void setMp3PreviewLength(String mp3PreviewLength) {
		this.mp3PreviewLength = mp3PreviewLength;
	}

	public final void setPreviewExtensions(List<String> l) {

		this.previewExtensions = new ArrayList();

		if (l != null) {

			for (String string : l) {

				if (string.startsWith(".")) {
					string = string.substring(1);
				}

				string = string.toLowerCase();

				previewExtensions.add(string);
			}
		}
	}

	@Override
	protected void singletonInit(RequestContext context) {
		super.singletonInit(context);
		this.defaultValidityDays = 3;
	}

}
