package org.sevensoft.ecreator.model.attachments;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;

/**
 * @author sks 09-Jan-2006 17:24:36
 *
 */
public class AttachmentUtil {

	public static void copyAttachmentsTo(AttachmentOwner source, AttachmentOwner target, RequestContext context) throws AttachmentExistsException,
			AttachmentTypeException, IOException, AttachmentLimitException {
		for (Attachment attach : source.getAttachments())
			new Attachment(context, target, attach);
	}

	public static Attachment getAttachment(RequestContext context, AttachmentOwner owner) {
		List<Attachment> attachments = getAttachments(context, owner);
		return attachments.isEmpty() ? null : attachments.get(0);
	}

	public static void repairCounts(RequestContext context) {

		ItemSearcher searcher = new ItemSearcher(context);
		for (Item item : searcher) {

			item.setAttachmentCount();
			item.save();
		}
	}

	public static List<Attachment> getAttachments(RequestContext context, AttachmentOwner owner) {

		String column = owner.getClass().getSimpleName();

		Query q = new Query(context, "select * from # where " + column + "=? order by position");
		q.setTable(Attachment.class);
		q.setParameter(owner);
		return q.execute(Attachment.class);
	}

	/**
	 * Returns true if this owner has an attachment with the supplied filename.
	 */
	public static boolean hasAttachment(AttachmentOwner source, String filename) {

		/*
		 * Strip path info from filename
		 */
		File file = new File(filename);
		filename = file.getName();

		for (Attachment attach : source.getAttachments()) {
			if (attach.getFilename().equalsIgnoreCase(filename)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * 
	 */
	public static boolean isAtAttachmentLimit(AttachmentOwner session) {
		return false;
	}

	public static void moveAttachmentsTo(RequestContext context, AttachmentOwner source, AttachmentOwner target) throws AttachmentExistsException,
			AttachmentTypeException, IOException, AttachmentLimitException {
		for (Attachment attach : source.getAttachments())
			new Attachment(context, target, attach);

		source.removeAttachments();
	}

	public static void removeAttachment(AttachmentOwner owner, Attachment a) {
		a.delete();
		owner.setAttachmentCount();
	}

	public static void removeAttachments(AttachmentOwner owner) {
		for (Attachment a : owner.getAttachments())
			owner.removeAttachment(a);
	}

	public static List<Attachment> save(AttachmentOwner owner, Map<Integer, String> descriptions, List<Upload> uploads, boolean makeSamples) {

		List<Attachment> attachments = new ArrayList();

		for (Attachment a : owner.getAttachments()) {

			String desc = descriptions.get(a);
			if (desc != null) {
				a.setDownloadLinkText(desc);
				a.save();
			}
		}

		/*
		 * Process uploaded files
		 */
		if (uploads != null) {

			for (Upload upload : uploads)

				try {

					Attachment a = owner.addAttachment(upload, makeSamples);
					attachments.add(a);

				} catch (AttachmentExistsException e) {
					e.printStackTrace();

				} catch (IOException e) {
					e.printStackTrace();

				} catch (AttachmentTypeException e) {
					e.printStackTrace();

				} catch (AttachmentLimitException e) {
					e.printStackTrace();
				}
		}

		return attachments;
	}
}
