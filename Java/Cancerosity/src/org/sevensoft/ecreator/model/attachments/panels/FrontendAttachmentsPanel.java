package org.sevensoft.ecreator.model.attachments.panels;

import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.logging.Logger;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.ecreator.iface.EcreatorRenderer;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.accounts.permissions.Permissions;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.AttachmentOwner;
import org.sevensoft.ecreator.model.attachments.AttachmentSettings;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 09-Feb-2006 15:12:42
 *
 */
public class FrontendAttachmentsPanel extends EcreatorRenderer {

	private static final Logger		logger	= Logger.getLogger("ecreator");

	private final List<Attachment>	attachments;
	private final AttachmentSettings	attachmentSettings;

	public FrontendAttachmentsPanel(RequestContext c, final AttachmentOwner owner) {
		super(c);

		this.attachments = owner.getAttachments();
		this.attachmentSettings = AttachmentSettings.getInstance(context);

		// strip out any attachments we don't have permissions for
		if (owner instanceof Permissions) {
			if (Module.Permissions.enabled(context)) {
				CollectionsUtil.filter(attachments, new Predicate<Attachment>() {

					public boolean accept(Attachment e) {
						return PermissionType.AttachmentsDownload.check(context, account, (Permissions) owner);
					}
				});
			}
		}
	}

	@Override
	public String toString() {

		logger.fine("[FrontendAttachmentsPanel] rendering attachments=" + attachments);
		if (attachments.isEmpty()) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		for (Attachment a : attachments) {

			// get markup from this attachment.
			Markup markup = a.getMarkup();

			//   if null then get markup from attachment default
			if (markup == null) {
				markup = attachmentSettings.getMarkup();
			}

			// render markup
			MarkupRenderer r = new MarkupRenderer(context, markup);
			r.setBody(a);

			sb.append(r);

		}

		Pattern p = Pattern.compile("\n");
        Matcher m = p.matcher(sb.toString());
        String str = m.replaceAll("");

		return str;
	}
}
