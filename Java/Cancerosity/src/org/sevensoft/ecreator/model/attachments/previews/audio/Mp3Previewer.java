package org.sevensoft.ecreator.model.attachments.previews.audio;

import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.previews.PreviewException;
import org.sevensoft.ecreator.model.attachments.previews.Previewer;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * @author sks 5 Feb 2007 08:22:08
 */
public class Mp3Previewer implements Previewer {

	private static final String	SoxPath		= "sox";
	private static final String	LamePath		= "lame";
	private static final String	Mp3ShellCommand	= "lame --quiet --decode \"${input}\" - | sox -t wav - -t wav - fade 0 ${length} 3 | lame --quiet -b128 - \"${output}\"";

	private static final Logger	logger		= Logger.getLogger("ecreator");

	private RequestContext		context;
	private final String		mp3PreviewLength;

	public Mp3Previewer(RequestContext context, String mp3PreviewLength) {
		this.context = context;
		this.mp3PreviewLength = mp3PreviewLength;
	}

	public String sample(Attachment attachment, String previewFilename) throws PreviewException {

        Config config = Config.getInstance(context);

		String input = ResourcesUtils.getRealAttachmentsPath() + "/" + attachment.getPath();
		input = input.replace("//", "/");

		logger.fine("[Mp3Previewer] input=" + input);

		String output = ResourcesUtils.getRealAttachmentsPath() + "/" + attachment.getDirectory() + "/" + previewFilename;
		output = output.replace("//", "/");

		logger.fine("[Mp3Previewer] output=" + output);

		String command = Mp3ShellCommand;
		command = command.replace("${input}", input);
		command = command.replace("${output}", output);
		command = command.replace("${length}", mp3PreviewLength);

		logger.fine("[Mp3Previewer] Mp3Command=" + command);

		try {

			EntityObject.exec(new String[] { "/bin/sh", "-c", (command) });
			return previewFilename;

		} catch (IOException e) {

			logger.warning("[Mp3Previewer] " + e);
			throw new PreviewException(e);
		}
	}
}
