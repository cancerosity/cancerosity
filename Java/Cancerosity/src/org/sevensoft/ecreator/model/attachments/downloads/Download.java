package org.sevensoft.ecreator.model.attachments.downloads;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.iface.frontend.attachments.AttachmentHandler;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import java.io.IOException;
import java.util.List;

/**
 * @author sks 11 Jul 2006 23:31:04
 *         <p/>
 *         This class represents attachments purchased via the credit system
 */
@Table("downloads")
public class Download extends EntityObject {

    /**
     *
     */
    private Item account;

    /**
     * Date this download was created
     */
    private DateTime dateCreated;

    /**
     * The date this download will cease to be valid
     */
    private DateTime expiryDate;

    private boolean expired;

    private Attachment attachment;

    /**
     * Number of credits used
     */
    private int credits;

    public Download(RequestContext context) {
        super(context);
    }

    public Download(RequestContext context, Item acc, Attachment attachment) throws AttachmentExistsException, AttachmentTypeException, IOException,
            AttachmentLimitException {

        super(context);

        this.account = acc;
        this.attachment = attachment;
        this.dateCreated = new DateTime();

        final int vdays = attachment.getValidityDays();
        if (vdays > 0) {
            this.expiryDate = dateCreated.addDays(vdays);
        }

        this.credits = attachment.getCredits();

        save();
    }

    public void expire() {
        this.expired = true;
    }

    public Item getAccount() {
        return account.pop();
    }

    public final Attachment getAttachment() {
        return attachment.pop();
    }

    public int getAttachmentLimit() {
        return 1;
    }

    public DateTime getDateCreated() {
        return dateCreated;
    }

    public LinkTag getDownloadLinkTag() {
        return new LinkTag(AttachmentHandler.class, "download", getAttachment().getName(), "download", this);
    }

    public DateTime getExpiryDate() {
        return expiryDate;
    }

    public int getCredits() {
        return credits;
    }

    public String getExpiryString() {

        if (expired) {
            return "Expired";

        } else if (expiryDate != null) {
            return "Does not expire";

        } else {
            return expiryDate.toString("dd-MMM-yyyy");
        }
    }

    public boolean hasExpired() {
        return expiryDate != null && expiryDate.isPast();
    }

    public boolean isExpired() {
        return expired;
    }

    /**
     *
     */
    public static List<Download> getByMonth(RequestContext context, Date month) {
        Query q = new Query(context, "select * from # where dateCreated>=? and dateCreated<? group by dateCreated,account");
        q.setTable(Download.class);
        q.setParameter(month.beginMonth());
        q.setParam(month.addMonths(1).beginMonth());
        return q.execute(Download.class);
    }
}