package org.sevensoft.ecreator.model.attachments.downloads;

import java.io.IOException;
import java.util.logging.Logger;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.frontend.attachments.DownloadHandler;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.credits.CreditPackage;
import org.sevensoft.ecreator.model.ecom.credits.CreditStore;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.support.PayableSupport;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 10 Jan 2007 06:39:00
 *
 */
@Table("downloads_sessions")
public class DownloadSession extends PayableSupport {

	@SuppressWarnings("hiding")
	private static Logger	logger	= Logger.getLogger(DownloadSession.class.getName());

	public static DownloadSession get(RequestContext context, String sessionId) {
		DownloadSession session = SimpleQuery.get(context, DownloadSession.class, "sessionId", sessionId);
		return session == null ? new DownloadSession(context, sessionId) : session;
	}

	/**
	 * The source attachment
	 */
	private Attachment	attachment;
	private String		sessionId;
	private Item		account;

	/**
	 * Credit package we are buying to pay for downloads
	 */
	private CreditPackage	creditPackage;

	/**
	 * The created download 
	 */
	private Download		download;

	protected DownloadSession(RequestContext context) {
		super(context);
	}

	public DownloadSession(RequestContext context, String sessionId) {
		super(context);
		this.sessionId = sessionId;
		save();
	}

	public void download(Payment payment, String ipAddress) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException {

		logger.fine("download session callback");

		// if we have a credit package, credit that first
		if (creditPackage != null) {

			if (payment != null) {

				creditPackage.pop();

				if (payment.getAmount().equals(creditPackage.getFee())) {

					logger.fine("crediting credit package=" + creditPackage);
					CreditStore store = getAccount().getCreditStore();
					store.credit(creditPackage, ipAddress);

				}
			}
		}

		if (attachment.hasCredits()) {

			CreditStore store = getAccount().getCreditStore();
			store.spendCredits(attachment.getCredits());
		}

		download = getAccount().addDownload(attachment);
		logger.fine("created download=" + download);

		// clear off package and stuff
		attachment = null;
		creditPackage = null;
		
		super.reset();

		save();
	}

	public Item getAccount() {
		return (Item) (account == null ? null : account.pop());
	}

	public Attachment getAttachment() {
		return attachment.pop();
	}

	/**
	 * 
	 */
	public CreditPackage getCreditPackage() {
		return (CreditPackage) (creditPackage == null ? null : creditPackage.pop());
	}

	public Download getDownload() {
		return (Download) (download == null ? null : download.pop());
	}

	public Item getPayableAccount() {
		return getAccount();
	}

	public Address getPaymentAddress() {
		return null;
	}

	public Money getPaymentAmount() {

		if (getAttachment().hasFee()) {
			return getAttachment().getFee();
		}

		if (creditPackage != null) {
			creditPackage.pop();
			return creditPackage.getFee();
		}

		return null;
	}

	public String getPaymentDescription() {
		return getAttachment().getName();
	}

	public String getPaymentFailureUrl(PaymentType paymentType) {
		return Config.getInstance(context).getUrl() + "/" + new Link(DownloadHandler.class, "declined", "paymentType", paymentType).toString();
	}

	public String getPaymentSuccessUrl(PaymentType paymentType) {
		return Config.getInstance(context).getUrl() + "/" + new Link(DownloadHandler.class, "completed", "paymentType", paymentType).toString();
	}

    public String getBasketUrl() {
        return null;
    }

    public String getSessionId() {
		return sessionId;
	}

	public boolean hasAttachment() {
		return attachment != null;
	}

	public boolean hasCreditPackage() {
		return creditPackage != null;
	}

	public boolean hasDownload() {
		return download != null;
	}

	public boolean hasFee() {
		return getAttachment().hasFee();
	}

	public void reset() {
		super.reset();

		this.attachment = null;
		this.creditPackage = null;
		this.account = null;
		this.download = null;

		save();
	}

	public void setAccount(Item acc) {

		if (ObjectUtil.equal(account, acc)) {
			return;
		}

		this.account = acc;
		save();
	}

	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
		save("attachment");
	}

	public void setCreditPackage(CreditPackage creditPackage) {
		this.creditPackage = creditPackage;
		save("creditPackage");
	}

}
