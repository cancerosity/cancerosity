package org.sevensoft.ecreator.model.attachments;

import java.io.IOException;
import java.io.File;
import java.net.URL;
import java.util.List;

import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.jeezy.http.Upload;

/**
 * @author sks 01-Nov-2005 10:30:29
 * 
 */
public interface AttachmentOwner {

	public boolean acceptedFiletype(String filename);

	public Attachment addAttachment(Attachment attachment) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException;

    public Attachment addAttachment(File file, String fileName) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException;

    public Attachment addAttachment(Upload upload, boolean sample) throws IOException, AttachmentLimitException, AttachmentExistsException,
			AttachmentLimitException, AttachmentTypeException;

	public Attachment addAttachment(URL url) throws IOException, AttachmentTypeException, AttachmentExistsException, AttachmentLimitException;

	public void copyAttachmentsTo(AttachmentOwner target) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException;

	public int getAttachmentCount();

	public int getAttachmentLimit();

	public List<Attachment> getAttachments();

	public boolean hasAttachments();

	public boolean isAtAttachmentLimit();

	public void moveAttachmentsTo(AttachmentOwner target) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException;

	public void removeAttachment(Attachment a);

	public void removeAttachments();

	public void setAttachmentCount();

}