package org.sevensoft.ecreator.model.attachments.previews;

import org.sevensoft.ecreator.model.attachments.Attachment;

/**
 * @author sks 5 Feb 2007 08:28:59
 *
 */
public interface Previewer {

	public String sample(Attachment attachment, String previewFilename) throws PreviewException;

}
