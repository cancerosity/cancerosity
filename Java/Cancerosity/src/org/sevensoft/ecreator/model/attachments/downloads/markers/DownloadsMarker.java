package org.sevensoft.ecreator.model.attachments.downloads.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.attachments.MyDownloadsHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 03-Apr-2006 11:41:59
 *
 */
public class DownloadsMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.AttachmentPayment.enabled(context)) {
			logger.fine("[DownloadsMarker] attachment payments disabled");
			return null;
		}

		if (!params.containsKey("text")) {
			params.put("text", "My downloads");
		}

		return super.link(context, params, new Link(MyDownloadsHandler.class), "account_link");

	}

	public Object getRegex() {
		return new String[] { "downloads_page", "account_downloads" };
	}
}