package org.sevensoft.ecreator.model.attachments;

import java.util.HashSet;
import java.util.Set;

/**
 * @author sks 22 May 2006 15:49:57
 *
 */
public class AttachmentImageFilter {

	private static Set<String>	AcceptedFileTypes;

	static {

		AcceptedFileTypes = new HashSet<String>();

		/*
		 * Images
		 */
		AcceptedFileTypes.add("gif");
		AcceptedFileTypes.add("bmp");
		AcceptedFileTypes.add("csv");
		AcceptedFileTypes.add("tiff");
		AcceptedFileTypes.add("tif");
		AcceptedFileTypes.add("jpeg");
		AcceptedFileTypes.add("jpg");
		AcceptedFileTypes.add("png");
		AcceptedFileTypes.add("psd");

	}

	public boolean accept(String name) {

		if (!name.contains("."))
			return false;

		String ext = name.substring(name.lastIndexOf('.') + 1);
		return AcceptedFileTypes.contains(ext.toLowerCase());
	}

}
