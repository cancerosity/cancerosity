package org.sevensoft.ecreator.model.attachments;

public enum Filetype {

	Access() {

		@Override
		public String getContentType() {
			return "application/octet-stream";
		}

	},
	Excel() {

		@Override
		public String getContentType() {
			return "application/vnd.ms-excel";
		}

	},
	Generic() {

		@Override
		public String getContentType() {
			return "application/octet-stream";
		}
	},
	Gif() {

		@Override
		public String getCategory() {
			return "Image";
		}

		@Override
		public String getContentType() {
			return "image/gif";
		}
	},
	Htm() {

		@Override
		public String getContentType() {
			return "text/html";
		}

		@Override
		public String getCategory() {
			return "HTML";
		}

	},
	Html() {

		@Override
		public String getContentType() {
			return "text/html";
		}

		@Override
		public String getCategory() {
			return "HTML";
		}
	},
	JPeg() {

		@Override
		public String getCategory() {
			return "Image";
		}

		@Override
		public String getContentType() {
			return "image/jpeg";
		}
	},
	Mp3() {

		@Override
		public String getContentType() {
			return "audio/mpeg";
		}

		@Override
		public String getCategory() {
			return "Audio";
		}

	},
	Pdf() {

		@Override
		public String getContentType() {
			return "application/pdf";
		}
	},
	Photoshop() {

		@Override
		public String getContentType() {
			return "application/octet-stream";
		}
	},
	Powerpoint() {

		@Override
		public String getContentType() {
			return "application/vnd.ms-powerpoint";
		}
	},
	Text() {

		@Override
		public String getCategory() {
			return "Text";
		}

		@Override
		public String getContentType() {
			return "text/plain";
		}
	},

	Tiff() {

		@Override
		public String getCategory() {
			return "Image";
		}

		@Override
		public String getContentType() {
			return "image/tiff";
		}
	},
	Word() {

		@Override
		public String getCategory() {
			return "Word document";
		}

		@Override
		public String getContentType() {
			return "application/msword";
		}
	},
	Zip() {

		@Override
		public String getCategory() {
			return "Zip file";
		}

		@Override
		public String getContentType() {
			return "application/zip";
		}
	},
	Flash {

		@Override
		public String getCategory() {
			return "Flash";
		}

		@Override
		public String getContentType() {
			return "application/flash";
		}

	},
	Mpeg {

		@Override
		public String getCategory() {
			return "Video";
		}

		@Override
		public String getContentType() {
			return "application/mpeg";
		}
	},
	RichText, Wma() {

		@Override
		public String getCategory() {
			return "Audio";
		}

	},
	Wave() {

		@Override
		public String getCategory() {
			return "Audio";
		}
	},
	Png {

		@Override
		public String getCategory() {
			return "Image";
		}
	},
	Bitmap() {

		@Override
		public String getCategory() {
			return "Image";
		}
	},
	Rar, Quicktime() {

		@Override
		public String getCategory() {
			return "Video";
		}

	};

	public String getCategory() {
		return toString();
	}

	public String getContentType() {
		return null;
	}

}