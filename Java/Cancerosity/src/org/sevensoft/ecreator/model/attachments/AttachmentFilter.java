package org.sevensoft.ecreator.model.attachments;

/**
 * Filter used to determine if the file we are uploading is an acceptable format.
 * 
 * @author sks 13-Oct-2005 14:23:14
 * 
 */
public class AttachmentFilter {

	public boolean accept(String name) {

		name = name.toLowerCase();

		for (String string : Attachment.Filetypes.keySet()) {

			if (name.endsWith(string)) {
				return true;
			}
		}

		return false;
	}
}