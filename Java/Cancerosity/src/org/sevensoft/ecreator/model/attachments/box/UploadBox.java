package org.sevensoft.ecreator.model.attachments.box;

import org.sevensoft.ecreator.iface.frontend.attachments.AttachmentHandler;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategoryModule;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 17 May 2006 08:25:51
 *
 * A box used to allow users of the site to add attachments
 */
@Table("boxes_upload")
@Label("Attachment upload")
@HandlerClass(UploadBoxHandler.class)
public class UploadBox extends Box {

	private String	submitButtonText;

	private UploadBox(RequestContext context) {
		super(context);
	}

	public UploadBox(RequestContext context, String location) {
		super(context, location);
	}

	@Override
	protected String getCssIdDefault() {
		return "upload";
	}

	private String getSubmitButtonText() {
		return submitButtonText == null ? "Upload" : submitButtonText;
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	@Override
	public String render(RequestContext context) {

		Category category = (Category) context.getAttribute("category");
		Item item = (Item) context.getAttribute("item");
		Item active = (Item) context.getAttribute("account");

		/*
		 * Only display on items or categories
		 */
		if (category == null && item == null)
			return null;

		if (!PermissionType.AttachmentsUpload.check(context, active)) {
			return null;
		}

		/*
		 * Only display on this category if category attachments is enabled
		 */
		if (category != null) {
			if (!CategoryModule.Attachments.enabled(context)) {
				return null;
			}
		}

		/*
		 * Only display on this category if category attachments is enabled
		 */
		if (item != null) {
			if (!ItemModule.Attachments.enabled(context, item)) {
				return null;
			}
		}

		StringBuilder sb = new StringBuilder();
		sb.append(getTableTag());
		sb.append(new FormTag(AttachmentHandler.class, "upload", "multi"));

		if (category != null)
			sb.append(new HiddenTag("category", category));
		else if (item != null)
			sb.append(new HiddenTag("item", item));

		sb.append("<tr><td>");

		if (hasContent())
			sb.append(getContent());

		sb.append("<br/>");

		sb.append(new FileTag("upload", 12));
		sb.append(new SubmitTag(getSubmitButtonText()));

		sb.append("</td></tr>");

		sb.append("<tr><td class='bottom'></td></tr>");
		sb.append("</form>");
		sb.append("</table>");

		return sb.toString();
	}

}
