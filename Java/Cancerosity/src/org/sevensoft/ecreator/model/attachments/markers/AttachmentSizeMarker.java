package org.sevensoft.ecreator.model.attachments.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.design.marker.interfaces.IAttachmentMarker;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 12 Jul 2006 09:20:07
 *
 */
public class AttachmentSizeMarker implements IAttachmentMarker {

	public Object generate(RequestContext context, Map<String, String> params, Attachment attachment) {
		return attachment.getSizeKb();
	}

	public Object getRegex() {
		return "attachments_size";
	}

}
