package org.sevensoft.ecreator.model.attachments.box;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 2 Aug 2006 11:24:54
 *
 */
@Path("admin-attachments-box.do")
public class UploadBoxHandler extends BoxHandler {

	private UploadBox	box;

	public UploadBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {
	}

}
