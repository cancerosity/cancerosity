package org.sevensoft.ecreator.model.attachments.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.attachments.panels.FrontendAttachmentsPanel;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 21 Apr 2006 11:31:20
 *
 */
public class AttachmentsMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		if (!item.hasAttachments()) {
			logger.fine("[AttachmentsMarker] item has no attachments");
			return null;
		}

		return new FrontendAttachmentsPanel(context, item);
	}

	public Object getRegex() {
		return "attachments";
	}

	@Override
	public String toString() {
		return "Includes all the attachments for this item, category, item type, etc";
	}

}
