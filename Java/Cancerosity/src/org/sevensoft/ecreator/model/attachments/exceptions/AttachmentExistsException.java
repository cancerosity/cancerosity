package org.sevensoft.ecreator.model.attachments.exceptions;


/**
 * @author sks 13-Oct-2005 14:31:31
 *
 */
public class AttachmentExistsException extends Exception {

	public AttachmentExistsException() {
		super();
	}

	public AttachmentExistsException(String message) {
		super(message);
	}

	public AttachmentExistsException(String message, Throwable cause) {
		super(message, cause);
	}

	public AttachmentExistsException(Throwable cause) {
		super(cause);
	}

}
