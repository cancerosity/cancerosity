package org.sevensoft.ecreator.model.attachments.downloads;

import java.util.logging.Logger;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 12 Jul 2006 07:13:37
 * This class will scan all downloads and remove any that have expired to free up disc space
 */
public class DownloadReaper implements Runnable {

	private static final Logger	logger	= Logger.getLogger("cron");
	private RequestContext		context;

	public DownloadReaper(RequestContext context) {
		this.context = context;
	}

	public void run() {

		logger.fine("[DownloadReaper] running");

		int count = 0;

		// Get all downloads 
		Query q = new Query(context, "select * from # where expired=0 and expiryDate<?");
		q.setTable(Download.class);
		q.setParameter(new Date());
		for (Download download : q.execute(Download.class)) {

			if (download.hasExpired()) {

				download.expire();
				count++;
			}
		}

		logger.fine("[DownloadReaper] " + count + " downloads reaped");

	}

}
