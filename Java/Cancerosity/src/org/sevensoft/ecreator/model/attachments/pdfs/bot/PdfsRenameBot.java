package org.sevensoft.ecreator.model.attachments.pdfs.bot;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.commons.simpleio.SimpleFile;

import java.util.logging.Logger;
import java.util.List;
import java.io.*;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
public class PdfsRenameBot implements Runnable {

    private static Logger logger = Logger.getLogger("cron");

    private RequestContext context;

    public PdfsRenameBot(RequestContext context) {
        this.context = context;
    }

    public void run() {
        List<Attachment> attachments = Attachment.get(context, Attachment.PDF_EXTENSION);
        if (attachments != null && !attachments.isEmpty()) {
            for (Attachment attach : attachments) {
                if (attach.getItem() != null) {
                    String itemName = attach.getItem().getName().replaceAll("/", "-");
                    if (itemName.length() > 50) {
                        itemName = itemName.substring(0, 50);
                    }
                    if (!itemName.equalsIgnoreCase(attach.getName().substring(0, attach.getName().lastIndexOf(".")))) {
                        Attachment newAttachment = null;
                        try {
                            newAttachment = attach.clone();
                            String extension = attach.getExtension();
                            String filename = itemName + "." + extension;
                            String newFilePath = ResourcesUtils.getRealPdfsPath() + "/" + filename;
                            File file = attach.getPdfFile();
                            if (!file.exists()) {
                                file = attach.getFile();
                            }
                            if (!file.exists()) {
                                newAttachment.delete();
                                throw new FileNotFoundException("File not found: " + filename);
                            }

                            File destinationFile = new File(newFilePath);

                            SimpleFile.writeStream(new FileInputStream(file), destinationFile);

                            newAttachment.setSize(destinationFile.length());
                            newAttachment.setName(filename);
                            newAttachment.setFilename(filename);
                            newAttachment.setPath("/" + filename);
                            newAttachment.save();
                            attach.delete();

                        } catch (CloneNotSupportedException e) {
                            logger.fine("[PdfsRenameBot] clone not suppoted " + e);
                        } catch (FileNotFoundException e) {
                            logger.fine("[PdfsRenameBot] file not found " + e);
                        } catch (IOException e) {
                            logger.fine("[PdfsRenameBot] " + e);
                        }
                    }
                }
            }
        }
    }
}
