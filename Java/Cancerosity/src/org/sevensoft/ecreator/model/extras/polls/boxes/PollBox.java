package org.sevensoft.ecreator.model.extras.polls.boxes;

import org.sevensoft.ecreator.iface.admin.extras.polls.PollBoxHandler;
import org.sevensoft.ecreator.iface.frontend.extras.poll.PollHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.extras.polls.Option;
import org.sevensoft.ecreator.model.extras.polls.Poll;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.RadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 31-Oct-2005 10:40:37
 * 
 */
@Table("boxes_polls")
@Label("Poll")
@HandlerClass(PollBoxHandler.class)
public class PollBox extends Box {

	private Poll	poll;

	/*
	 * Show the poll title (question) in the box
	 */
	@Default("1")
	private boolean	showTitle;

	private PollBox(RequestContext context) {
		super(context);
	}

	public PollBox(RequestContext context, String location) {

		super(context, location);
		this.showTitle = true;

		save();
	}

	@Override
	protected String getCssIdDefault() {
		return "poll";
	}

	public Poll getPoll() {
		return (Poll) (poll == null ? null : poll.pop());
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	public final boolean isShowTitle() {
		return showTitle;
	}

	public String render(RequestContext context) {

		if (!Module.Polls.enabled(context))
			return null;

		if (poll == null)
			return null;

		poll.pop();

		StringBuilder sb = new StringBuilder();

		// start form
		sb.append(new FormTag(PollHandler.class, "vote", "post"));
		sb.append(new HiddenTag("poll", poll));
		sb.append(getTableTag());

		if (showTitle) {
			sb.append("<tr><td class='title' colspan='2'>");
			sb.append(poll.getTitle());
			sb.append("</td></tr>");
		}

		sb.append("<tr><td>");

		for (Option option : poll.getOptions()) {

			sb.append(new RadioTag(context, "option", option));
			sb.append(option.getText());
			sb.append("<br/>");
		}

		sb.append("</td></tr>");
		sb.append("<tr><td class='showresults'>" + new LinkTag(PollHandler.class, null, "Show results", "poll", poll) + "</td></tr>");
		sb.append("<tr><td class='bottom' align='center'>");
		sb.append(new SubmitTag("Vote"));
		sb.append("</td></tr>");

		sb.append("</table>");
		sb.append("</form>");

		return sb.toString();
	}

	@Override
	public void setObjId(int objId) {
		this.poll = EntityObject.getInstance(context, Poll.class, objId);
		this.name = "Poll: " + poll.getTitle();

		save();
	}

	public final void setShowTitle(boolean showTitle) {
		this.showTitle = showTitle;
	}
}
