package org.sevensoft.ecreator.model.extras.meetups;

import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 25 Apr 2007 13:11:28
 *
 */
@Table("meetups_module")
public class MeetupModule extends EntityObject {

	public static MeetupModule get(RequestContext context, ItemType itemType) {
		MeetupModule m = SimpleQuery.get(context, MeetupModule.class, "itemType", itemType);
		return m == null ? new MeetupModule(context, itemType) : m;
	}

	private ItemType	itemType;

	/**
	 * Send a private message to item owner when someone RSVPs
	 */
	private boolean	pmOnRsvp;

	/**
	 * Send an email to item owner when someone rsvps.
	 */
	private boolean	emailOnRsvp;

	protected MeetupModule(RequestContext context) {
		super(context);
	}

	public MeetupModule(RequestContext context, ItemType itemType) {
		super(context);
		this.itemType = itemType;
		save();
	}

	public final ItemType getItemType() {
		return itemType;
	}

	public final boolean isEmailOnRsvp() {
		return emailOnRsvp;
	}

	public final boolean isPmOnRsvp() {
		return pmOnRsvp;
	}

	public final void setEmailOnRsvp(boolean emailOnRsvp) {
		this.emailOnRsvp = emailOnRsvp;
	}

	public final void setPmOnRsvp(boolean pmOnRsvp) {
		this.pmOnRsvp = pmOnRsvp;
	}

}
