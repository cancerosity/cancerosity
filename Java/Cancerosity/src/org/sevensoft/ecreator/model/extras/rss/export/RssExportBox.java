package org.sevensoft.ecreator.model.extras.rss.export;

import org.sevensoft.ecreator.iface.frontend.extras.rss.RssExportHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 9 Mar 2007 13:01:36
 *
 */
@Table("boxes_rss_export")
@Label("Rss export")
@HandlerClass(RssBoxHandler.class)
public class RssExportBox extends Box {

	public RssExportBox(RequestContext context) {
		super(context);
	}

	public RssExportBox(RequestContext context, String panel) {
		super(context, panel);
	}

	@Override
	protected String getCssIdDefault() {
		return "rss_export";
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	@Override
	public String render(RequestContext context) {

		Category category = (Category) context.getAttribute("category");
		if (category == null) {
			return null;
		}

		if (!category.hasLiveItems()) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
        sb.append(getTableTag());
        sb.append("<tr><td>Rss feed ");
		sb.append(new LinkTag(RssExportHandler.class, null, new RssGif(), "category", category));
        sb.append("</td></tr></table>");

		return sb.toString();
	}

}
