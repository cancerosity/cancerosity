package org.sevensoft.ecreator.model.extras.meetups.blocks;

import org.sevensoft.ecreator.iface.admin.extras.meetups.ItemRsvpsBlockHandler;
import org.sevensoft.ecreator.iface.frontend.interaction.pm.PmHandler;
import org.sevensoft.ecreator.iface.frontend.interaction.winks.WinksHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.extras.meetups.Meetup;
import org.sevensoft.ecreator.model.extras.meetups.Rsvp;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Label;

import java.util.List;

/**
 * @author sks 25 Apr 2007 13:02:21
 */
@Table("meetups_blocks_itemrsvps")
@Label("Item Rsvps")
@HandlerClass(ItemRsvpsBlockHandler.class)
public class ItemRsvpsBlock extends Block {

    public ItemRsvpsBlock(RequestContext context) {
        super(context);
    }

    public ItemRsvpsBlock(RequestContext context, BlockOwner owner, int objId) {
        super(context, owner, objId);
    }

    @Override
    public Object render(RequestContext context) {

        Item account = (Item) context.getAttribute("account");
        Item item = (Item) context.getAttribute("item");
        if (item == null) {
            return null;
        }

        Meetup meetup = item.getMeetup();

        // if private guests check that we are going
        if (meetup.isPrivateGuests()) {

            if (!meetup.isRsvp(account)) {
                return null;
            }
        }

        List<Rsvp> yes = meetup.getRsvps(true);
//        List<Rsvp> no = meetup.getRsvps(false);

        StringBuilder sb = new StringBuilder();
        sb.append(new TableTag("ec_rsvps"));

        sb.append("<tr><th colspan='3'>Members who are going to this event</th></tr>");

        for (Rsvp rsvp : yes) {

            row(sb, rsvp);
        }

//        sb.append("<tr><th colspan='2'>Members who can't go</th></tr>");

//        for (Rsvp rsvp : no) {
//
//            row(sb, rsvp);
//        }

        sb.append("</table>");

        return sb.toString();
    }

    /**
     *
     */
    private void row(StringBuilder sb, Rsvp rsvp) {

        sb.append("<tr>");

        sb.append("<td class='img'>");

        Img img = rsvp.getAccount().getApprovedImage();
        if (img != null) {
            sb.append(new LinkTag(rsvp.getAccount().getUrl(), img.getThumbnailTag()));
        }

        sb.append("</td>");

        Item account = rsvp.getAccount();
        sb.append("<td class='details'>" + new LinkTag(rsvp.getAccount().getUrl(), account.getDisplayName()));
        sb.append("<br/>");
        if (rsvp.hasMessage()) {
            sb.append("<b>" + rsvp.getMessage() + "</b>");
        }
        sb.append("<br/>");
        sb.append(account.getMemberDescription(300, "..."));

        sb.append("</td>");
        sb.append("<td>");
        sb.append(new TableTag());
        sb.append("<tr>");
        sb.append("<td nowrap>");
        sb.append(new LinkTag(PmHandler.class, "compose", "send a message!", "recipient", account) + "<br/>");
        sb.append("</td>");
        sb.append("</tr>");
        sb.append("<tr>");
        sb.append("<td nowrap>");
        sb.append(new LinkTag(WinksHandler.class, "wink", "add to Invites!", "recipient", account) + "<br/>");
        sb.append("</td>");
        sb.append("</tr>");
        sb.append("<tr>");
        sb.append("<td nowrap>");
        sb.append(new LinkTag(account.getUrl(), "view full profile!") + "<br/>");
        sb.append("</td>");
        sb.append("</tr>");
        sb.append("</table>");
        sb.append("</td>");

        sb.append("</tr>");
    }
}