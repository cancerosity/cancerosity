package org.sevensoft.ecreator.model.extras.rss.export;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * User: Tanya
 * Date: 24.01.2011
 */
public class RssDescriptionMarkup implements MarkupDefault {

    public String getBetween() {
        return null;
    }

    public String getBody() {
        return "<table><tr><td>[rss_image?limit=1&width=160]<div class='summary'>[summary?max=100]</div></td></tr></table>";
    }

    public String getEnd() {
        return null;
    }

    public String getStart() {
        return null;
    }

    public int getTds() {
        return 0;
    }

    public String getCss() {
        return null;
    }

    public String getName() {
        return null;
    }
}
