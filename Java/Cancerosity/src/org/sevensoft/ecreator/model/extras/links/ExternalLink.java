package org.sevensoft.ecreator.model.extras.links;

import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 8 Nov 2006 20:30:24
 *
 */
@Table("items_modules_exlinks")
public class ExternalLink extends EntityObject implements Positionable {

	private LinkType	linkType;
	private String	target;
	private int		position;
	private ItemType	itemType;
	private String	label;
	private String	linkText;

	private ExternalLink(RequestContext context) {
		super(context);
	}

	public ExternalLink(RequestContext context, ItemType itemType, LinkType linkType) {
		super(context);

		this.itemType = itemType;
		this.linkType = linkType;
		this.label = linkType.getDefaultLabel();
		this.linkText = "Click here";
		this.target = "_blank";

		save();
	}

	public ItemType getItemType() {
		return itemType;
	}

	public String getLabel() {
		return label;
	}

	public String getLinkText() {
		return linkText;
	}

	public LinkType getLinkType() {
		return linkType;
	}

	public int getPosition() {
		return position;
	}

	public String getTarget() {
		return target == null ? "_blank" : target;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setLinkText(String linkText) {
		this.linkText = linkText;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setTarget(String target) {
		this.target = target;
	}

}
