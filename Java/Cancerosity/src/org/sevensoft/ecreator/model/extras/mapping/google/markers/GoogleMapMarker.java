package org.sevensoft.ecreator.model.extras.mapping.google.markers;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.extras.mapping.google.GoogleMap;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 23 Apr 2007 12:53:48
 *
 */
public class GoogleMapMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		if (!Module.GoogleMap.enabled(context)) {
			return null;
		}
		
		if (!item.hasLocation()) {
			return null;
		}

		GoogleMap map = new GoogleMap(context);
		map.addItems(Collections.singletonList(item));

		if (params.containsKey("elementid")) {
			map.setElementId(params.get("elementid"));
		}

		int limit;
		if (params.containsKey("limit")) {
			limit = Integer.parseInt(params.get("limit"));
		} else {
			limit = 20;
		}

		if (params.containsKey("zoom")) {
			map.setZoom(Integer.parseInt(params.get("zoom")));
		}

		if (params.containsKey("info")) {
			map.setInfoWindow(true);
		}

		// do overlaps
		if (params.containsKey("overlays")) {

			for (int itemTypeId : StringHelper.explodeIntegers(params.get("overlays"), ",")) {

				ItemSearcher searcher = new ItemSearcher(context);
				searcher.setItemType(itemTypeId);
				searcher.setLimit(limit);
				searcher.setLocation(item.getLocation(), item.getX(), item.getY());
				searcher.setSortType(SortType.Distance);
				searcher.setDistance(100);

				List<Item> items = searcher.getItems();

				logger.fine("[GoogleMapMarker] itemTypeId=" + itemTypeId + ", items=" + items);
				map.addItems(itemTypeId, items);
			}
		}

		return map;
	}

	public Object getRegex() {
		return "google_map";
	}

}
