package org.sevensoft.ecreator.model.extras.languages.box;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.model.extras.languages.Language;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 19 Oct 2006 11:29:48
 *
 */
@Path("language.do")
public class LanguageHandler extends FrontendHandler {

	private Language	changeLanguage;

	public LanguageHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (changeLanguage != null) {

			language = changeLanguage;
			context.setAttribute("language", language);

			setCookie("language", language.name(), Integer.MAX_VALUE);
		}

		return new CategoryHandler(context).main();
	}
}
