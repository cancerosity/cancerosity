package org.sevensoft.ecreator.model.extras.rss;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.extras.rss.boxes.NewsfeedBox;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Selectable;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;

/**
 * @author sks 07-Jul-2005 10:32:35
 * 
 */
@Table("newsfeeds")
public class Newsfeed extends EntityObject implements Selectable {

    public static List<Newsfeed> get(RequestContext context) {

        Query q = new Query(context, "select * from # order by title");
        q.setTable(Newsfeed.class);
        return q.execute(Newsfeed.class);
    }

    private String copyright;

    private String link;

    private String description;

    private String title;

    private String url;

    private DateTime dateUpdated;

    private String author;

    public Newsfeed(RequestContext context) {
        super(context);
    }

    public Newsfeed(RequestContext context, String url) throws IOException, IllegalArgumentException, FeedException {

        super(context);

        Query q = new Query(context, "select * from # where url=?");
        q.setTable(Newsfeed.class);
        q.setParameter(url);
        if (q.getInt() == 0) {

            this.url = url;
            save();

            refresh();
        }
    }

    @Override
    public synchronized boolean delete() {

        removeItems();
        removeBlocks();
        removeBoxes();

        return super.delete();
    }

    private void doDetails(SyndFeed feed) {

        this.title = feed.getTitle();
        this.author = feed.getAuthor();
        this.link = feed.getLink();
        this.description = feed.getDescription();
        this.copyright = feed.getCopyright();
    }

    private void doItems(SyndFeed feed) {

        removeItems();

        List<SyndEntry> entries = feed.getEntries();
        for (SyndEntry entry : entries) {
            new NewsfeedArticle(context, this, entry);
        }
    }

    public List<NewsfeedArticle> getArticles() {
        return getArticles(0);
    }

    public List<NewsfeedArticle> getArticles(int limit) {

        Query q = new Query(context, "select * from # where feed=? order by timestamp desc");
        q.setTable(NewsfeedArticle.class);
        q.setParameter(this);
        List<NewsfeedArticle> articles = q.execute(NewsfeedArticle.class, limit);

        Collections.sort(articles, new NewsfeedArticleTimestampComparator());
        // reverse order
        Collections.reverse(articles);

        return articles;
    }

    public String getAuthor() {
        return author;
    }

    public String getCopyright() {
        return copyright;
    }

    public DateTime getDateUpdated() {
        return dateUpdated;
    }

    public String getDescription() {
        return description;
    }

    public Link getEditLink() {
        return null;
    }

    public String getLabel() {
        return getTitle();
    }

    public String getLink() {
        return link;
    }

    public String getTitle() {
        return title == null ? "No title set" : title;
    }

    public String getUrl() {
        return url;
    }

    public String getValue() {
        return getIdString();
    }

    public boolean hasCopyright() {
        return copyright != null;
    }

    public boolean hasDescription() {
        return description != null;
    }

    public boolean hasLink() {
        return link != null;
    }

    public boolean hasTitle() {
        return title != null;
    }

    public void refresh() throws IllegalArgumentException, MalformedURLException, IOException, FeedException {

        SyndFeedInput input = new SyndFeedInput();
        SyndFeed feed = input.build(new XmlReader(new URL(url)));

        doDetails(feed);
        doItems(feed);

        dateUpdated = new DateTime();
        save();
    }

    private void removeBlocks() {
        new Query(context, "delete from # where newsfeed=?").setTable(NewsfeedBlock.class).setParameter(this).run();
    }

    private void removeBoxes() {
        new Query(context, "delete from # where newsfeed=?").setTable(NewsfeedBox.class).setParameter(this).run();
    }

    private void removeItems() {
        SimpleQuery.delete(context, NewsfeedArticle.class, "feed", this);
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
