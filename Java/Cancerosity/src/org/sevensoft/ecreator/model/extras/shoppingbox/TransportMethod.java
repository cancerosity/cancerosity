package org.sevensoft.ecreator.model.extras.shoppingbox;


/**
 * @author sks 8 Jun 2007 11:33:11
 *
 */
public enum TransportMethod {

	Air,
	
	Sea
}
