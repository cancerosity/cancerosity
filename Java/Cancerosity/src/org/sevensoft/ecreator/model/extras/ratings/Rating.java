package org.sevensoft.ecreator.model.extras.ratings;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 12 Jul 2006 16:44:48
 * 
 * A rating given by a member to this item
 *
 */
@Table("ratings")
public class Rating extends EntityObject {

	@Index()
	private Item		rater;

	private int			rating;

	@Index()
	private Item		item;

	@Index()
	private RatingType	ratingType;

	private Rating(RequestContext context) {
		super(context);
	}

	public Rating(RequestContext context, Item item, Item rater, RatingType ratingType, int rating) {
		super(context);

		if (!ratingType.hasRated(item, rater)) {

			this.item = item;
			this.rater = rater;
			this.ratingType = ratingType;
			this.rating = rating;

			save();

		}
	}

	public Item getItem() {
		return item.pop();
	}

	public Item getRater() {
		return item.pop();
	}

	public int getRating() {
		return rating;
	}

	public RatingType getRatingType() {
		return ratingType.pop();
	}

}
