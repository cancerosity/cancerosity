package org.sevensoft.ecreator.model.extras.polls;

import java.util.List;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.iface.frontend.extras.poll.PollHandler;
import org.sevensoft.ecreator.model.extras.polls.blocks.PollBlock;
import org.sevensoft.ecreator.model.extras.polls.boxes.PollBox;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 08-Jul-2005 01:15:29
 * 
 */
@Table("polls")
public class Poll extends EntityObject implements Selectable {

	public static List<Poll> get(RequestContext context) {
		Query q = new Query(context, "select * from # order by title");
		q.setTable(Poll.class);
		return q.execute(Poll.class);
	}

	public static Poll getLiveRandom(RequestContext context) {
		Query q = new Query(context, "select * from # where live=1 order by rand()");
		q.setTable(Poll.class);
		return q.get(Poll.class);
	}

	private DateTime	dateCreated;
	private String	title;
	private int		votes, optionsCount;
	private String	content;

	private Poll(RequestContext context) {
		super(context);
	}

	public Poll(RequestContext context, String string) {
		super(context);

		this.title = string;
		this.dateCreated = new DateTime();

		save();
	}

	public void addOption(String text) {
		new Option(context, this, text);
		setOptionsCount();
	}

	@Override
	public synchronized boolean delete() {

		Query q = new Query(context, "delete from # where poll=?");
		q.setParameter(this);
		q.setTable(Option.class);
		q.run();

		removeBox();
		removeBlocks();

		return super.delete();
	}

	public List<PollComment> getComments() {
		Query q = new Query(context, "select * from # where poll=? order by date");
		q.setTable(PollComment.class);
		q.setParameter(this);
		return q.execute(PollComment.class);
	}

	public String getContent() {
		return content;
	}

	public DateTime getDateCreated() {
		return dateCreated;
	}

	public String getLabel() {
		return getTitle();
	}

	public List<Option> getOptions() {
		return SimpleQuery.execute(context, Option.class, "poll", this);
	}

	public int getOptionsCount() {
		return optionsCount;
	}

	@Deprecated
	public String getRelativeUrl() {
		return new Link(PollHandler.class, null, "poll", this).toString();
	}

	public String getTitle() {
		return title;
	}

	public String getUrl() {
		return Config.getInstance(context).getUrl() + "/" + getRelativeUrl();
	}

	public String getValue() {
		return getIdString();
	}

	public int getVotes() {
		return votes;
	}

	public boolean hasContent() {
		return content != null;
	}

	public boolean hasOptions() {
		return optionsCount > 0;
	}

	private void removeBox() {
		new Query(context, "update # set poll=0 where poll=?").setTable(PollBox.class).setParameter(this).run();
	}

	private void removeBlocks() {
		SimpleQuery.delete(context, PollBlock.class, "poll", this);
	}

	public void removeOption(Option option) {
		option.delete();
		setOptionsCount();
		save();
	}

	public void reset() {

		votes = 0;
		save();

		Query q = new Query(context, "update # set votes=0 where poll=?");
		q.setTable(Option.class);
		q.setParameter(this);
		q.run();
	}

	public void reset(Option option) {
		votes = votes - option.reset();
		save();
	}

	public void setContent(String content) {
		this.content = content;
	}

	private void setOptionsCount() {
		save();
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @param option
	 */
	public void vote(Option option) {
		option.vote();
		votes++;
		save();
	}
}
