package org.sevensoft.ecreator.model.extras.facebook;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.media.images.Img;

/**
 * User: Tanya
 * Date: 05.03.2013
 */
public class FacebookOpenGraphPanel {


    private final RequestContext context;
    private final transient FacebookApplicationSettings settings;
    private final transient FacebookOpenGraphTags tags;

    public FacebookOpenGraphPanel(RequestContext context) {
        this.context = context;
        this.settings = FacebookApplicationSettings.getInstance(context);
        this.tags = FacebookOpenGraphTags.getInstance(context);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Item it = (Item) context.getAttribute("item");
        Category cat = (Category) context.getAttribute("category");
        Img defaultImg = tags.getApprovedImage();

        sb.append("<head prefix=\"og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# \n" +
                "                  website: http://ogp.me/ns/website#\">\n");
        sb.append("<meta property=\"fb:app_id\" content=\"").append(settings.getAppId()).append("\">\n");
        sb.append("<meta property=\"og:type\" content=\"website\">\n");
        if (it != null) {
            sb.append("<meta property=\"og:url\" content=\"").append(it.getUrl()).append("\">\n");
            sb.append("<meta property=\"og:image\" content=\"").append(it.getApprovedImage() != null ? it.getApprovedImage().getFullImagePath() : defaultImg.getFullImagePath()).append("\">\n");
            sb.append("<meta property=\"og:title\" content=\"").append(it.getName()).append("\">\n");
            sb.append("<meta property=\"og:description\" content=\"").append(it.hasSummary() ? it.getSummary() : it.getName()).append("\">\n");
        } else if (cat != null) {
            sb.append("<meta property=\"og:url\" content=\"").append(cat.getUrl()).append("\">\n");
            sb.append("<meta property=\"og:image\" content=\"").append(cat.getApprovedImage() != null ? cat.getApprovedImage().getFullImagePath() : defaultImg.getFullImagePath()).append("\">\n");
            sb.append("<meta property=\"og:title\" content=\"").append(cat.getName()).append("\">\n");
            sb.append("<meta property=\"og:description\" content=\"").append(cat.hasSummary() ? cat.getSummary() : cat.getName()).append("\">\n");
        }


        return sb.toString();
    }
}
