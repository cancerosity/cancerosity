package org.sevensoft.ecreator.model.extras.languages;

import java.util.Set;
import java.util.TreeSet;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sam2 Nov 12, 2003 2:00:43 AM
 */
@Table("settings_language")
@Singleton
public class LanguageSettings extends EntityObject {

	public static LanguageSettings getInstance(RequestContext context) {
		return getSingleton(context, LanguageSettings.class);
	}

	private TreeSet<Language>	languages;

	public LanguageSettings(RequestContext context) {
		super(context);
	}

	public void addLanguage(Language language) {
		getLanguages().add(language);
		save();
	}

	public Language getDefaultLanguage() {
		return Language.English;
	}

	public Set<Language> getLanguages() {

		if (languages == null)
			languages = new TreeSet();

		return languages;
	}

	public boolean isValid(Language language) {
		return getLanguages().contains(language);
	}

	public void removeLanguage(Language language) {
		getLanguages().remove(language);
		save();
	}

}