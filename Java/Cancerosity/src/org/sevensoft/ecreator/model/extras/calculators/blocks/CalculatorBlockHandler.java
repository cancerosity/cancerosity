package org.sevensoft.ecreator.model.extras.calculators.blocks;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 15 Mar 2007 09:26:30
 *
 */
@Path("admin-blocks-calculator.do")
public class CalculatorBlockHandler extends BlockEditHandler {

	public CalculatorBlockHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Block getBlock() {
		return null;
	}

	@Override
	protected void saveSpecific() {
	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

}
