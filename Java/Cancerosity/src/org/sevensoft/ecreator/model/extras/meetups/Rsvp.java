package org.sevensoft.ecreator.model.extras.meetups;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 25 Apr 2007 12:46:17
 *
 */
@Table("meetups_rsvp")
public class Rsvp extends EntityObject {

	private boolean	yes;

	private Item	account;

	private String	message;

	/**
	 * The item we are going to, event ,etc
	 */
	private Item	item;

	/**
	 * Is this RSVP been approved by the event owner
	 */
	private boolean	approved;

	protected Rsvp(RequestContext context) {
		super(context);
	}

	public Rsvp(RequestContext context, Item item, Item account, boolean yes, String message) {
		super(context);
		this.item = item;
		this.account = account;
		this.yes = yes;
		this.message = message;

		if (item.getMeetup().isModerated()) {
			this.approved = false;

		} else {
			this.approved = true;
		}

		save();
	}

	public void approve() {

		this.approved = true;

		Config config = Config.getInstance(context);

		// send email
		Email e = new Email();

		e.setSubject("Rsvp approved!");
		e.setBody("You have been accepted to " + getItem().getName());
		e.setTo(getAccount().getEmail());
		e.setFrom(config.getServerEmail());

		try {
			new EmailDecorator(context, e).send(config.getSmtpHostname());
		} catch (EmailAddressException e1) {
			e1.printStackTrace();
		} catch (SmtpServerException e1) {
			e1.printStackTrace();
		}
	}

	public final Item getAccount() {
		return account.pop();
	}

	public final Item getItem() {
		return item.pop();
	}

	public final String getMessage() {
		return message;
	}

	public boolean hasMessage() {
		return message != null;
	}

	public final boolean isYes() {
		return yes;
	}

	/**
	 * 
	 */
	public void reject() {

		// send email
		Email e = new Email();

		Config config = Config.getInstance(context);

		e.setSubject("Rsvp rejected!");
		e.setBody("I'm sorry but you have not been accepted to " + getItem().getName());
		e.setTo(getAccount().getEmail());
		e.setFrom(config.getServerEmail());

		try {
			new EmailDecorator(context, e).send(config.getSmtpHostname());
		} catch (EmailAddressException e1) {
			e1.printStackTrace();
		} catch (SmtpServerException e1) {
			e1.printStackTrace();
		}
		delete();
	}

}
