package org.sevensoft.ecreator.model.extras.shoppingbox;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 8 Jun 2007 11:26:53
 *
 */
@Singleton
@Table("myshoppingbox_settings")
public class ShoppingBoxSettings extends EntityObject {

	public static ShoppingBoxSettings getInstance(RequestContext context) {
		return EntityObject.getSingleton(context, ShoppingBoxSettings.class);
	}

	private Money	seaCostPerUnit;

	private Money	airCostPerUnit;

	protected ShoppingBoxSettings(RequestContext context) {
		super(context);
	}

	public Money getAirCostPerUnit() {
		return airCostPerUnit;
	}

	public Money getSeaCostPerUnit() {
		return seaCostPerUnit;
	}

	public void setAirCostPerUnit(Money airCost) {
		this.airCostPerUnit = airCost;
	}

	public void setSeaCostPerUnit(Money seaCost) {
		this.seaCostPerUnit = seaCost;
	}
}
