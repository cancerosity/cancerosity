package org.sevensoft.ecreator.model.extras.facebook;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * User: Tanya
 * Date: 10.02.2013
 */
@Table("settings_facebook")
@Singleton
public class FacebookApplicationSettings extends EntityObject {

    protected FacebookApplicationSettings(RequestContext context) {
        super(context);
    }

    public static FacebookApplicationSettings getInstance(RequestContext context) {
        return getSingleton(context, FacebookApplicationSettings.class);
    }

    private String appId;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }
}
