package org.sevensoft.ecreator.model.extras.languages.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.extras.languages.LanguageSettings;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 17 May 2006 14:32:27
 *
 */
public final class LanguagesSelectMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		SelectTag tag = new SelectTag(context, "changeLanguage");
		tag.setOnChange("window.location='index.do?changeLanguage=' + this.value;");
		tag.addOptions(LanguageSettings.getInstance(context).getLanguages());
		return tag;
	}

	@Override
	public String getDescription() {
		return "Returns a select menu for changing languages";
	}

	public Object getRegex() {
		return "languages_select";
	}
}