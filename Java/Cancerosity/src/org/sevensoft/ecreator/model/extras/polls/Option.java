package org.sevensoft.ecreator.model.extras.polls;

import java.text.NumberFormat;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 08-Jul-2005 01:15:33
 * 
 */
@Table("polls_options")
public class Option extends EntityObject implements Positionable {

	private Poll	poll;
	private String	text;
	private int		votes, position;

	private Option(RequestContext context) {
		super(context);
	}

	public Option(RequestContext context, Poll poll, String text) {
		super(context);

		this.poll = poll;
		this.text = text;
		this.votes = 0;
		save();
	}

	public int getPercentage() {

		if (votes == 0)
			return 0;

		return (int) (100d * votes / getPoll().getVotes());
	}

	public String getPercentageString() {

		if (votes == 0)
			return null;

		NumberFormat format = NumberFormat.getInstance();
		format.setMaximumFractionDigits(0);
		return format.format(votes / (double) getPoll().getVotes() * 100);
	}

	public Poll getPoll() {
		return poll.pop();
	}

	public int getPosition() {
		return position;
	}

	public String getText() {
		return text;
	}

	public int getVotes() {
		return votes;
	}

	/**
	 * @return
	 */
	public int reset() {
		final int i = votes;
		votes = 0;
		save();
		return i;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	/**
	 * @param string
	 */
	public void setText(String s) {
		text = s;
	}

	void vote() {
		votes++;
		save();

	}

}
