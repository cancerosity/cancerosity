package org.sevensoft.ecreator.model.extras.reorder;

/**
 * User: Tanya
 * Date: 11.01.2012
 */
public interface ReorderOwner {

    public int getPosition();

    public void setPosition(int position);

    public String getReorderName();

    public void save();

}
