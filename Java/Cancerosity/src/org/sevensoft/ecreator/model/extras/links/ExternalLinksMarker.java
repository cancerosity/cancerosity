package org.sevensoft.ecreator.model.extras.links;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 7 Nov 2006 17:43:55
 *
 */
public class ExternalLinksMarker implements IItemMarker {

	private Logger	logger	= Logger.getLogger("tags");

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		logger.fine("[ExternalLinksMarker] item=" + item);

		List<ExternalLink> links = item.getItemType().getExternalLinks();
		if (links.isEmpty())
			return null;

		logger.fine("[ExternalLinksMarker] links=" + links);

		StringBuilder sb = new StringBuilder();
		sb.append(new TableTag("ec_external_links"));

		for (ExternalLink link : links) {
			
			String url = link.getLinkType().getUrl(item);
			if (url != null)
				sb.append("<tr><td>" + link.getLabel() + "</td><td>" + new LinkTag(url, link.getLinkText()).setTarget(link.getTarget()) + "</td></tr>");
		}

		sb.append("</table>");
		return sb;
	}

	public Object getRegex() {
		return "external_links";
	}

}
