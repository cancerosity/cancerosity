package org.sevensoft.ecreator.model.extras.calculators;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.collections.maps.LinkedMultiMap;
import org.sevensoft.ecreator.model.design.util.GridCellOld;
import org.sevensoft.ecreator.model.extras.calculators.renderers.CalculatorFieldRenderer;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;

import com.bestcode.mathparser.IMathParser;
import com.bestcode.mathparser.MathParserFactory;

/**
 * @author sks 27-Mar-2006 11:01:34
 *
 */
@Table("calculators_fields")
public class CalculatorField extends EntityObject implements Positionable, Comparable<CalculatorField>, GridCellOld {

	public enum Type {
		Selection, Numeric, Expression, Radio;
	}

	private static List<CalculatorField> getBySection(RequestContext context, String section) {
		return SimpleQuery.execute(context, CalculatorField.class, "section", section);
	}

	/**
	 *  Strips out all attributes except for the lead attribute for each section
	 */
	public static List<CalculatorField> getLead(List<CalculatorField> fields) {

		// sort attributes by position
		Collections.sort(fields);

		final Set<String> sections = new HashSet<String>();
		CollectionsUtil.filter(fields, new Predicate<CalculatorField>() {

			public boolean accept(CalculatorField e) {

				if (sections.contains(e.getSection()))
					return false;

				sections.add(e.getSection());
				return true;
			}
		});

		return fields;
	}

	public static LinkedMultiMap<String, CalculatorField> getSectionsMap(List<CalculatorField> fields) {

		LinkedMultiMap<String, CalculatorField> map = new LinkedMultiMap<String, CalculatorField>();
		for (CalculatorField field : fields) {
			map.put(field.getSection(), field);
		}

		return map;
	}

	private Calculator					calculator;

	/**
	 * Sectionise fields like attributes
	 */
	private String						section;

	/**
	 * a brief description to the customer
	 */
	private String						description;

	/**
	 * The name of this field
	 */
	private String						name;

	private String						defaultValue;

	/**
	 * Useful extra name for admin peeps backend to get extra info
	 */
	private String						extendedName;

	/**
	 * Variable name if this field is to be used as a variable.
	 */
	private String						variable;

	/**
	 * The expressions to calculate if this an expression field
	 */
	private String						expression;

	/**
	 * 
	 */
	private int							position;

	/**
	 * The order to evaluate fields - useful to avoid look ahead errors on complicated calculations
	 */
	private int							evaluationOrder;

	/**
	 * The type of input field
	 */
	private Type						type;

	private int							width;

	private int							cellsPerRow;

	private int							cellSpan;

	private boolean						hideLabel;

	private transient List<CalculatorFieldOption>	options;

	public CalculatorField(RequestContext context) {
		super(context);
	}

	public CalculatorField(RequestContext context, Calculator calculator, String name) {
		super(context);

		this.calculator = calculator;
		this.name = name;
		this.width = 12;
		save();
	}

	private void addOption(String text) {
		new CalculatorFieldOption(context, this, text);
		options = null;
	}

	public void addOptions(List<String> texts) {
		for (String text : texts)
			addOption(text);
	}

	public int compareTo(CalculatorField o) {
		return position - o.position;
	}

	/**
	 * If this field is an expression then it calculates the value using the variables stored in data
	 * and puts the evaluated value back into the map so it can be used by another expression further down the page.
	 * @throws Exception 
	 * 
	 */
	public void evaluate(Map<CalculatorField, String> data, Map<String, String> asdadoppp) throws Exception {

		System.out.println("Evaluating field: " + getName());

		if (variable == null)
			return;

		String value;

		switch (getType()) {

		case Numeric:
			// numerical fields have their values set directly as parameters from the request
			break;

		case Expression:

			// if no variable set then we have nothing to reference this value against
			if (expression == null)
				break;

			// evaluate the expression
			value = parse(data, expression);
			if (value != null)
				data.put(this, value);

			break;

		case Radio:
		case Selection:

			// get option from input value, 
			CalculatorFieldOption option = getOption(data.get(this));
			if (option == null)
				return;

			// if we have an expression then evaluate
			if (option.hasExpression()) {

				String exp = option.getExpression();
				if (exp == null)
					return;

				value = parse(data, exp);
				if (value != null)
					data.put(this, value);

				// ... otherwise if has a value then put value directly in
			} else if (option.hasValue()) {

				data.put(this, option.getValue());
			}

			break;
		}
	}

	public Calculator getCalculator() {
		return calculator.pop();
	}

	public int getCellSpan() {
		return cellSpan < 1 ? 1 : cellSpan;
	}

	public int getCellsPerRow() {
		return cellsPerRow < 2 ? 2 : cellsPerRow;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public String getDescription() {
		return description;
	}

	public int getEvaluationOrder() {
		return evaluationOrder;
	}

	public String getExpression() {
		return expression;
	}

	public String getExtendedName() {
		return extendedName;
	}

	public Object getGridContents() {
		return new CalculatorFieldRenderer(context, this);
	}

	public String getGridDescription() {
		return description;
	}

	public String getGridLabel() {
		return name;
	}

	public String getGridRowId() {
		return getIdString();
	}

	public String getName() {
		return name;
	}

	/**
	 * Returns the option that matches this value
	 */
	private CalculatorFieldOption getOption(String value) {
		for (CalculatorFieldOption option : getOptions())
			if (option.getIdString().equals(value))
				return option;
		return null;
	}

	public List<CalculatorFieldOption> getOptions() {

		if (options == null) {

			Query q = new Query(context, "select * from # where field=? order by position");
			q.setTable(CalculatorFieldOption.class);
			q.setParameter(this);
			options = q.execute(CalculatorFieldOption.class);
		}

		return options;
	}

	public List<String> getOptionTexts() {
		Query q = new Query(context, "select text from # where field=? order by position");
		q.setParameter(this);
		q.setTable(CalculatorFieldOption.class);
		return q.getStrings();
	}

	public String getParam() {
		return "calculationData_" + getId();
	}

	public int getPosition() {
		return position;
	}

	public String getSection() {
		return section;
	}

	public final Type getType() {
		return type == null ? CalculatorField.Type.Numeric : type;
	}

	/**
	 * Returns the double value associated with the option matching the text param
	 */
	private String getValueFromOption(String string) {
		CalculatorFieldOption option = getOption(string);
		return option == null ? null : option.getValue();
	}

	public String getVariable() {
		return variable;
	}

	public int getWidth() {
		return width < 4 ? 12 : width;
	}

	public boolean hasDescription() {
		return description != null;
	}

	public boolean hasExpression() {
		return expression != null;
	}

	public boolean hasExtendedName() {
		return extendedName != null;
	}

	public boolean hasSection() {
		return section != null;
	}

	public boolean hasVariable() {
		return variable != null;
	}

	public boolean isExpression() {
		return expression != null;
	}

	public boolean isHideLabel() {
		return hideLabel;
	}

	public boolean isInput() {
		return !isExpression();
	}

	public boolean isNumerical() {
		return getType() == Type.Numeric;
	}

	public boolean isSelection() {
		return getType() == Type.Selection;
	}

	public boolean isShowLabel() {
		return !hideLabel;
	}

	private String parse(Map<CalculatorField, String> data, String exp) throws Exception {

		System.out.println("Parsing: " + exp);

		IMathParser parser = MathParserFactory.create();
		parser.setExpression(exp);

		for (Map.Entry<CalculatorField, String> entry : data.entrySet()) {

			String var = entry.getKey().getVariable();
			String value = entry.getValue();

			double d = 0;
			if (value != null)
				try {
					d = Double.parseDouble(value);
				} catch (NumberFormatException e) {
				}

			if (var != null) {
				System.out.println("Setting var: " + var + " " + d);
				parser.setVariable(var, d);
			}

		}

		double value = parser.evaluate();
		return String.valueOf(value);
	}

	public void removeOption(CalculatorFieldOption option) {
		option.delete();
		options = null;
	}

	public void setCellSpan(int cellSpan) {
		this.cellSpan = cellSpan;
	}

	public void setCellsPerRow(int c) {
		this.cellsPerRow = c;

		for (CalculatorField field : CalculatorField.getBySection(context, section)) {
			field.cellsPerRow = c;
			field.save();
		}
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setEvaluationOrder(int evaluationOrder) {
		this.evaluationOrder = evaluationOrder;
	}

	public void setExpression(String e) {
		this.expression = (e == null ? null : e.trim().toUpperCase());
	}

	public void setExtendedName(String extendedName) {
		this.extendedName = extendedName;
	}

	public void setHideLabel(boolean hideLabel) {
		this.hideLabel = hideLabel;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public final void setType(Type type) {
		this.type = type;
	}

	public void setValue(Map<CalculatorField, String> data, Map<String, String> variables) {

		// only resolve thiss field if this field has a variable name
		if (variable == null)
			return;

		String value;

		switch (getType()) {

		// expression and numericals do not have options to resolve
		case Expression:
		case Numeric:

			value = data.get(this);
			if (value != null)
				variables.put(variable, value);

			return;

		case Radio:
		case Selection:

			value = getValueFromOption(data.get(this));
			if (value != null)
				variables.put(variable, value);
		}
	}

	public void setVariable(String variable) {
		this.variable = (variable == null ? null : variable.trim().toUpperCase());
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public boolean useOptions() {

		switch (getType()) {

		case Expression:
		case Numeric:
			return false;

		case Radio:
		case Selection:
			return true;

		}

		return false;
	}

}
