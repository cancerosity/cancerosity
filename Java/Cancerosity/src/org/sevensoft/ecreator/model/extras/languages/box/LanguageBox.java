package org.sevensoft.ecreator.model.extras.languages.box;

import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.extras.languages.Language;
import org.sevensoft.ecreator.model.extras.languages.LanguageSettings;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 11 Oct 2006 21:47:59
 *
 */
@Table("boxes_language")
@Label("Language box")
@HandlerClass(LanguageBoxHandler.class)
public class LanguageBox extends Box {

	public LanguageBox(RequestContext context) {
		super(context);
	}

	public LanguageBox(RequestContext context, String location) {
		super(context, location);
	}

	@Override
	protected String getCssIdDefault() {
		return "languages";
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	@Override
	public String render(RequestContext context) {

		if (!Module.Languages.enabled(context))
			return null;

		Language language = (Language) context.getAttribute("language");
		LanguageSettings settings = LanguageSettings.getInstance(context);

		StringBuilder sb = new StringBuilder(400);
		sb.append(getTableTag());

		sb.append(new FormTag(LanguageHandler.class, null, "get"));

		SelectTag tag = new SelectTag(context, "changeLanguage", language, settings.getLanguages(), "-Please select-");
		tag.setAutoSubmit();

		sb.append("<tr><td>");
		sb.append(tag);
		sb.append("</td></tr>");

		sb.append("</form>");

		sb.append("<tr><td class='bottom'>&nbsp;</td></tr>");

		sb.append("</table>");
		return sb.toString();

	}

}
