package org.sevensoft.ecreator.model.extras.licenses;


/**
 * @author sks 2 May 2007 16:51:45
 *
 */
public class LicenseException extends Exception {

	public LicenseException() {
	}

	public LicenseException(String message) {
		super(message);
	}

	public LicenseException(Throwable cause) {
		super(cause);
	}

	public LicenseException(String message, Throwable cause) {
		super(message, cause);
	}

}
