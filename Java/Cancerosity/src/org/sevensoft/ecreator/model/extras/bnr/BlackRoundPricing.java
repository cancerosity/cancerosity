package org.sevensoft.ecreator.model.extras.bnr;

import java.util.HashMap;
import java.util.Map;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.items.Item;

/**
 * @author sks 1 Jul 2007 07:12:22
 *
 */
public class BlackRoundPricing {

	private static Map<String, Money>	charges	= new HashMap();
	private static Money			defaultCharge;

	static {

		charges.put("15", new Money(1250));
		charges.put("17", new Money(1350));
		charges.put("18", new Money(1500));
		charges.put("19", new Money(1600));
		charges.put("20", new Money(1700));
		charges.put("21", new Money(1800));

		defaultCharge = new Money(1800);

	}

	private Basket				basket;

	public BlackRoundPricing(Basket basket) {
		this.basket = basket;
	}

	public Money getFittingCharge() {

		Money fittingCharge = new Money(0);
		for (BasketLine line : basket.getLines()) {

			Item item = line.getItem();
			if (item != null) {

				String tyreSize = item.getAttributeValue("tyre size");
				if (tyreSize != null) {
					Money charge = charges.get(tyreSize);
					if (charge == null) {
						charge = defaultCharge;
					}
					fittingCharge = fittingCharge.add(charge.multiply(line.getQty()));
				}
			}
		}

		return fittingCharge;
	}
}
