package org.sevensoft.ecreator.model.extras.rss;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndContent;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Date;

/**
 * @author sks 07-Jul-2005 10:23:59
 *
 */
@Table("newsfeeds_items")
public class NewsfeedArticle extends EntityObject {

    private String date;

    private Newsfeed feed;

    private String title;

    private String description;

    private String link;

    private String author;

    private long timestamp;

    public NewsfeedArticle(RequestContext context) {
        super(context);
    }

    public NewsfeedArticle(RequestContext context, Newsfeed feed, SyndEntry entry) {

        super(context);

        if (entry.getDescription() != null) {
            this.description = entry.getDescription().getValue();
        } else if (entry.getContents() != null && !entry.getContents().isEmpty()) {
            this.description = ((SyndContent)entry.getContents().get(0)).getValue();
        }

        this.feed = feed;
        this.title = entry.getTitle();
        this.author = entry.getAuthor();

        this.link = entry.getLink();

        Date publishedDate = entry.getPublishedDate();
        if (publishedDate != null) {
            this.date = publishedDate.toString();
            this.timestamp = publishedDate.getTime();
        }

        save();
    }

    public String getAuthor() {
        return author;
    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public String getDescription(int i) {
        return StringHelper.toSnippet(description, i);
    }

    public Newsfeed getFeed() {
        return feed.pop();
    }

    public String getLink() {
        return link;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getTitle() {
        return title;
    }

    public boolean hasAuthor() {
        return author != null;
    }

    public boolean hasDate() {
        return date != null;
    }

    public boolean hasDescription() {
        return description != null;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setFeed(Newsfeed feed) {
        this.feed = feed;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
