package org.sevensoft.ecreator.model.extras.calculators;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 2 Sep 2006 07:32:54
 *
 */
@Table("calculators_fields_options")
public class CalculatorFieldOption extends EntityObject implements Positionable {

	private CalculatorField	field;
	private String		text;
	private int			position;
	private String		value;
	private String		expression;

	protected CalculatorFieldOption(RequestContext context) {
		super(context);
	}

	CalculatorFieldOption(RequestContext context, CalculatorField field, String text) {
		super(context);

		this.field = field;
		this.text = text;

		save();
	}

	public String getExpression() {
		return expression;
	}

	public CalculatorField getField() {
		return field.pop();
	}

	public int getPosition() {
		return position;
	}

	public String getText() {
		return text;
	}

	public String getValue() {
		return value;
	}

	public boolean hasExpression() {
		return expression != null;
	}

	public boolean hasValue() {
		return value != null;
	}

	public void setExpression(String e) {
		this.expression = e;
		if (expression != null)
			expression = expression.trim().toUpperCase();
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
