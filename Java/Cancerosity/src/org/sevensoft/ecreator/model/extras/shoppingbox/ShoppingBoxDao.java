package org.sevensoft.ecreator.model.extras.shoppingbox;

import java.util.List;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Jun 2007 10:39:49
 *
 */
public class ShoppingBoxDao {

	public static List<ShoppingBox> findUnpaid(RequestContext context, Item account) {
		Query q = new Query(context, "select sb.* from # sb join # i on sb.item=i.id where i.account=? and sb.paid=0");
		q.setTable(ShoppingBox.class);
		q.setTable(Item.class);
		q.setParameter(account);
		return q.execute(ShoppingBox.class);
	}

	public static ShoppingBox get(RequestContext context, Item item) {
		ShoppingBox box = SimpleQuery.get(context, ShoppingBox.class, "item", item);
		return box;
	}
}
