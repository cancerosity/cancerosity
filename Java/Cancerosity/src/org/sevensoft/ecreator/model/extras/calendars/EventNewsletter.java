package org.sevensoft.ecreator.model.extras.calendars;

import org.sevensoft.commons.smail.Email;

/**
 * @author sks 21-Jul-2005 15:55:10
 * 
 */
public class EventNewsletter extends Email {

	public EventNewsletter(int days) {

		setSubject("Events Newsletter");

		StringBuilder sb = new StringBuilder("Our events newsletter");

		setBody(sb);
	}
}
