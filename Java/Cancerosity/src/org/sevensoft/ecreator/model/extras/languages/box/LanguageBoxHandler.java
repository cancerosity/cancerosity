package org.sevensoft.ecreator.model.extras.languages.box;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 11 Oct 2006 21:48:48
 *
 */
@Path("admin-boxes-language.do")
public class LanguageBoxHandler extends BoxHandler {

	private LanguageBox	box;

	public LanguageBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {
	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

}
