package org.sevensoft.ecreator.model.extras.calculators.blocks;

import java.util.List;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.frontend.extras.calculators.CalculatorHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.design.util.GridRendererOld;
import org.sevensoft.ecreator.model.extras.calculators.Calculator;
import org.sevensoft.ecreator.model.extras.calculators.CalculatorField;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 14 Jul 2006 10:34:03
 *
 */
@Table("blocks_calculators")
@Label("Calculator")
@HandlerClass(CalculatorBlockHandler.class)
public class CalculatorBlock extends Block {

	private Calculator	calculator;

	protected CalculatorBlock(RequestContext context) {
		super(context);
	}

	public CalculatorBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, 0);

		this.calculator = EntityObject.getInstance(context, Calculator.class, objId);
		save();
	}

	public Calculator getCalculator() {
		return calculator.pop();
	}

	@Override
	public Object render(RequestContext context) {

		final List<CalculatorField> fields = getCalculator().getFields();

		StringBuilder sb = new StringBuilder();

		sb.append(new MessagesTag(context));

		sb.append(new FormTag(CalculatorHandler.class, null, "POST"));
		sb.append(new HiddenTag("calculator", calculator));
		sb.append(new HiddenTag("category", getOwnerCategory()));

		MultiValueMap<String, CalculatorField> sectionsMap = CalculatorField.getSectionsMap(fields);
		for (String section : sectionsMap.keySet()) {

			GridRendererOld r = new GridRendererOld(sectionsMap.list(section));
			r.setTitle(section);
			sb.append(r);
		}

		sb.append("<div align='center'>" + new SubmitTag(getCalculator().getSubmitButtonText()) + "</div>");

		sb.append("</form>");

		return sb.toString();
	}

}
