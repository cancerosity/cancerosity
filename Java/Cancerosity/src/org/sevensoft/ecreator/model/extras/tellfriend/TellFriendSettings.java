package org.sevensoft.ecreator.model.extras.tellfriend;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.i18n.Messages;

/**
 * @author sks 15 Jan 2007 16:35:39
 *
 */
@Table("tellfriend_settings")
@Singleton
public class TellFriendSettings extends EntityObject {

	public static TellFriendSettings getInstance(RequestContext context) {
		return EntityObject.getSingleton(context, TellFriendSettings.class);
	}

	private List<String>	ccEmails;
    private String          emailGreeting;
    private String          emailBody;
    private String          emailGoodbye;

    public TellFriendSettings(RequestContext context) {
		super(context);
	}

	public final List<String> getCcEmails() {
		if (ccEmails == null) {
			ccEmails = new ArrayList();
		}
		return ccEmails;
	}

    public final String getEmailBody() {
        return emailBody;
    }

    public final String getEmailGoodbye() {
        return emailGoodbye;
    }

    public final String getEmailGreeting() {
        return emailGreeting;
    }

    public void sendEmail(String name, String friendsEmail, String friendsName, String url) throws EmailAddressException, SmtpServerException {

		Company company = Company.getInstance(context);
		Config config = Config.getInstance(context);

		StringBuilder sb = new StringBuilder();
        if (emailGreeting == null || emailGreeting.length() == 0) {
            sb.append("Hello " + friendsName + ",\n\n");
        } else {
            if (emailGreeting.contains("[friendname]")) {
                emailGreeting = emailGreeting.replace("[friendname]", friendsName);
            }
            sb.append(emailGreeting);
        }
        if (emailBody == null || emailBody.length() == 0) {
            sb.append("Your friend " + name + " thought you might be interested in this page:\n");
        } else {
            if (emailBody.contains("[yourname]")) {
                emailBody = emailBody.replace("[yourname]", name);
            }
            sb.append(emailBody);
        }
        sb.append(url);
        if (emailGoodbye == null || emailGoodbye.length() == 0) {
            sb.append("\n\n\nRegards,");
            sb.append(company.getName());
        } else {
            if (emailGoodbye.contains("[comp_name]")) {
                emailGoodbye = emailGoodbye.replace("[comp_name]", company.getName());
            }
            sb.append("\n\n\n" + emailGoodbye);
        }

		Email msg = new Email();
		msg.setBody(sb);
		msg.setTo(friendsEmail);

		for (String cc : getCcEmails()) {
			msg.addBcc(cc);
		}

        msg.setFrom(config.getServerEmail());
        msg.setSubject(Messages.get("frontend.extras.tellfriend.subject").replace("[sender]", name));
        new EmailDecorator(context, msg).send(config.getSmtpHostname());
	}

	public final void setCcEmails(List<String> ccEmails) {
		this.ccEmails = ccEmails;
	}

    public final void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    public final void setEmailGoodbye(String emailGoodbye) {
        this.emailGoodbye = emailGoodbye;
    }

    public final void setEmailGreeting(String emailGreeting) {
        this.emailGreeting = emailGreeting;
    }
}
