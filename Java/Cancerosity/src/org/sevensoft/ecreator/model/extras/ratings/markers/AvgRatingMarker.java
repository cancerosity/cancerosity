package org.sevensoft.ecreator.model.extras.ratings.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;

import java.util.Map;

/**
 * @author sks 11 Oct 2006 12:24:43
 */
public class AvgRatingMarker extends MarkerHelper implements IItemMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

        if (!Module.Ratings.enabled(context)) {
            return null;
        }

        int avg = item.getRatingAverage();
        if (avg == 0) {
            return null;
        }

        String src = item.getItemType().getRatingModule().getRatingGraphic(avg);

        if (src == null) {
            return super.string(context, params, avg);
        } else {
            return super.string(context, params, new ImageTag(Config.TemplateDataPath + "/" + src));
        }
    }

    public Object getRegex() {
        return "ratings_avg";
    }

}
