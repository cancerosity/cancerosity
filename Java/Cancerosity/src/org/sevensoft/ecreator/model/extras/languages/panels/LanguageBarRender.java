package org.sevensoft.ecreator.model.extras.languages.panels;

import org.sevensoft.ecreator.iface.EcreatorRenderer;
import org.sevensoft.ecreator.model.extras.languages.Language;
import org.sevensoft.ecreator.model.extras.languages.LanguageSettings;
import org.sevensoft.ecreator.model.extras.languages.box.LanguageHandler;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 19-Jan-2006 16:36:50
 *
 */
public final class LanguageBarRender extends EcreatorRenderer {

	public LanguageBarRender(RequestContext context) {
		super(context);
	}

	@Override
	public String toString() {

		if (!Module.Languages.enabled(context))
			return null;

		LanguageSettings ls = LanguageSettings.getInstance(context);

		sb.append("<style> span.language_bar img { padding: 0 2px; } </style> ");
		sb.append("<span class='language_bar'>");

		for (Language language : ls.getLanguages()) {

			ImageTag imageTag = (ImageTag) new ImageTag("files/graphics/languages/" + language.name().toLowerCase() + ".png").setAlign("absmiddle");
			LinkTag linkTag = new LinkTag(LanguageHandler.class, null, imageTag, "changeLanguage", language);

			sb.append(linkTag);
		}

		sb.append("</span>");

		return sb.toString();
	}

}
