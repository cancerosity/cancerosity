package org.sevensoft.ecreator.model.extras.mapping.google;

import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

/**
 * @author sks 24 Apr 2007 12:58:37
 */
@Table("google_maps_icon")
public class GoogleMapIcon extends EntityObject {

    public static List<GoogleMapIcon> get(RequestContext context) {
        return SimpleQuery.execute(context, GoogleMapIcon.class);
    }

    private String filename;

    private ItemType itemType;

    private int anchorX, anchorY;

    private int windowAnchorX, windowAnchorY;

    private int width, height;

    private String name;

    protected GoogleMapIcon(RequestContext context) {
        super(context);
    }

    public GoogleMapIcon(RequestContext context, ItemType itemType, String string) {
        super(context);
        this.itemType = itemType;
        name = string;

        save();
    }

    public final int getAnchorX() {
        return anchorX;
    }

    public final int getAnchorY() {
        return anchorY;
    }

    public final String getFilename() {
        return filename;
    }

    public final int getHeight() {
        return height;
    }

    public final ItemType getItemType() {
        return itemType.pop();
    }

    public final String getName() {
        return name;
    }

    public final int getWidth() {
        return width;
    }

    public final int getWindowAnchorX() {
        return windowAnchorX;
    }

    public final int getWindowAnchorY() {
        return windowAnchorY;
    }

    public final void setAnchorX(int anchorX) {
        this.anchorX = anchorX;
    }

    public final void setAnchorY(int anchorY) {
        this.anchorY = anchorY;
    }

    public final void setFilename(String filename) throws IOException {
        this.filename = filename;

        // load image
        BufferedImage image = ImageIO.read(ResourcesUtils.getRealTemplateData(filename));
        this.width = image.getWidth();
        this.height = image.getHeight();
    }

    public final void setName(String name) {
        this.name = name;
    }

    public final void setWindowAnchorX(int windowAnchorX) {
        this.windowAnchorX = windowAnchorX;
    }

    public final void setWindowAnchorY(int windowAnchorY) {
        this.windowAnchorY = windowAnchorY;
    }

}
