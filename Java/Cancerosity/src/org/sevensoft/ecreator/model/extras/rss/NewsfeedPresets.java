package org.sevensoft.ecreator.model.extras.rss;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author sks 18-Nov-2005 13:27:38
 * 
 */
public class NewsfeedPresets {

	static Map<String, String>	newsfeeds;

	static {

		newsfeeds = new TreeMap<String, String>();
		newsfeeds.put("http://www.telegraph.co.uk/newsfeed/rss/property.xml", "Telegraph Property"); // telegraph property
		newsfeeds.put("http://www.dailyhoroscopes.com/index.php?option=com_eventscalrss&feed=RSS2.0&no_html=1", "Dailyhoroscopes.com RSS Horoscopes"); // horoscopes
		newsfeeds.put("http://www.telegraph.co.uk/newsfeed/rss/motoring.xml", "Telegraph Motoring"); // telepgraph motoring
		newsfeeds.put("http://rss.allrecipes.com/2/3.xml", "AllRecipes.com Dinner Recipes"); // dinner ricipes
		newsfeeds.put("http://www.mailonsunday.co.uk/pages/xml/index.html?in_page_id=1798", "the Mail online | Diet & fitness");
		newsfeeds.put("feed:http://www.animalhelp.com/petcarenews.xml", "AnimalHelp Pets");

	}

	public static Map<String, String> getNewsfeeds() {
		return newsfeeds;
	}

}
