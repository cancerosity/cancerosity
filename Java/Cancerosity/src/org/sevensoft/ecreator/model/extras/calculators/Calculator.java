package org.sevensoft.ecreator.model.extras.calculators;

import java.util.List;
import java.util.Map;

import org.sevensoft.ecreator.model.extras.calculators.blocks.CalculatorBlock;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 27-Mar-2006 11:01:29
 *
 */
@Table("calculators")
public class Calculator extends EntityObject implements Selectable {

	public static List<Calculator> get(RequestContext context) {
		Query q = new Query(context, "select * from # order by name");
		q.setTable(Calculator.class);
		return q.execute(Calculator.class);
	}

	/**
	 * The name of this calculator, eg, mortgage calculator
	 */
	private String	name;

	private String	submitButtonText;

	public Calculator(RequestContext context) {
		super(context);
	}

	public Calculator(RequestContext context, String string) {
		super(context);
		name = string;
		save();
	}

	public CalculatorField addField(String name) {
		return new CalculatorField(context, this, name);
	}

	public CalculatorField addField(String name, String var) {
		CalculatorField field = addField(name);
		field.setVariable(var);
		field.save();
		return field;
	}

	@Override
	public synchronized boolean delete() {

		removeFields();
		removeBlocks();

		return super.delete();
	}

	public void evaluate(Map<CalculatorField, String> data) {

		final List<CalculatorField> fields = getFields();

		for (CalculatorField field : fields) {
			try {
				field.evaluate(data, null);
			} catch (Exception e) {
				System.out.println("Error processing field " + field + "=" + e);
			}
		}
	}

	public List<CalculatorField> getFields() {

		Query q = new Query(context, "select * from # where calculator=? order by position");
		q.setTable(CalculatorField.class);
		q.setParameter(this);
		return q.execute(CalculatorField.class);
	}

	public String getLabel() {
		return getName();
	}

	public String getName() {
		return name;
	}

	public final String getSubmitButtonText() {
		return submitButtonText == null ? "Calculate" : submitButtonText;
	}

	public String getValue() {
		return getIdString();
	}

	private void removeBlocks() {
		new Query(context, "delete from # where calculator=?").setTable(CalculatorBlock.class).setParameter(this).run();
	}

	public void removeField(CalculatorField field) {
		field.delete();
	}

	private void removeFields() {
	}

	public void setName(String name) {
		this.name = name;
	}

	public final void setSubmitButtonText(String submitButtonText) {
		this.submitButtonText = submitButtonText;
	}

}
