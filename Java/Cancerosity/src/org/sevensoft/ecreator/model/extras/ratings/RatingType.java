package org.sevensoft.ecreator.model.extras.ratings;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 12 Jul 2006 16:46:05
 */
@Table("ratings_types")
public class RatingType extends EntityObject implements Positionable {

    /**
     * The item type this rating category is for
     */
    private RatingModule ratingModule;

    /**
     * The name of this rating category
     */
    private String name;

    private int position;

    private RatingType(RequestContext context) {
        super(context);
    }

    public RatingType(RequestContext context, RatingModule ratingModule, String name) {
        super(context);

        this.ratingModule = ratingModule;
        this.name = name;

        save();
    }

    @Override
    public synchronized boolean delete() {
        removeRatings();
        return super.delete();
    }

    public String getName() {
        return name;
    }

    public int getPosition() {
        return position;
    }

    public Rating getRating(Item item, Item rater) {
        Query q = new Query(context, "select * from # where item=? and rater=?");
        q.setTable(Rating.class);
        q.setParameter(item);
        q.setParameter(rater);
        return q.get(Rating.class);
    }

    public RatingModule getRatingModule() {
        return ratingModule.pop();
    }

    public boolean hasRated(Item item, Item rater) {

        Query q = new Query(context, "select count(*) from # where item=? and rater=? and ratingType=?");
        q.setTable(Rating.class);
        q.setParameter(item);
        q.setParameter(rater);
        q.setParameter(this);
        return q.getInt() > 0;
    }

    private void removeRatings() {
        SimpleQuery.delete(context, Rating.class, "ratingType", this);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPosition(int position) {
        this.position = position;
    }

}
