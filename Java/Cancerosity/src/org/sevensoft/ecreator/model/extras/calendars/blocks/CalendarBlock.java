package org.sevensoft.ecreator.model.extras.calendars.blocks;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.iface.admin.extras.calendars.CalendarBlockHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.extras.calendars.Calendar;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 14 Jul 2006 10:52:13
 */
@Table("calendars_blocks")
@HandlerClass(CalendarBlockHandler.class)
@Label("Calendar")
public class CalendarBlock extends Block {

    private Attribute startAttribute, endAttribute;
    private ItemType itemType;

    protected CalendarBlock(RequestContext context) {
        super(context);
    }

    public CalendarBlock(RequestContext context, BlockOwner owner, int objId) {
        super(context, owner, 0);
    }

    public Attribute getEndAttribute() {
        return (Attribute) (endAttribute == null ? null : endAttribute.pop());
    }

    public ItemType getItemType() {
        return (ItemType) (itemType == null ? null : itemType.pop());
    }

    public Attribute getStartAttribute() {
        return (Attribute) (startAttribute == null ? null : startAttribute.pop());
    }

    public boolean hasItemType() {
        return itemType != null;
    }

    @Override
    public Object render(RequestContext context) {

        if (itemType == null) {
            return null;
        }

        if (startAttribute == null) {
            return null;
        }

        Date thisMonth = null;

        try {
            if (context.hasParameter("calendarDate")) {
                thisMonth = new Date(Long.parseLong(context.getParameter("calendarDate").trim()));
            }
        } catch (RuntimeException e) {

        }

        if (thisMonth == null) {
            thisMonth = new Date();
        }

        thisMonth = thisMonth.beginMonth();

        // get events for this date
        Calendar cal = new Calendar(context, thisMonth, getItemType(), getStartAttribute(), getEndAttribute());
        CalendarRenderer r = new CalendarRenderer(context, thisMonth, cal.getItemsMap());

        return r;
    }

    public void setEndAttribute(Attribute endAttribute) {
        this.endAttribute = endAttribute;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public void setStartAttribute(Attribute startAttribute) {
        this.startAttribute = startAttribute;
    }

}
