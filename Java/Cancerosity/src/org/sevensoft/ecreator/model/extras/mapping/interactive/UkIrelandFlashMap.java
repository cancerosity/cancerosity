package org.sevensoft.ecreator.model.extras.mapping.interactive;

import java.util.Map;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 4 Apr 2007 11:23:01
 *
 */
@Table("maps_clickable_flash_ukireland")
public class UkIrelandFlashMap extends EntityObject {

	public enum State {

		WesternIsles(01), Highland(02), Moray(03), Aberdeenshire(04), ArgyllBute(05), Aberdeen(06), Dublin(131),

		Meath(132);

		private int	id;

		private State(int id) {
			this.id = id;
		}
	}

	private Map<State, String>	links;
	private Map<State, String>	names;
	private String			color, hoverColor;

	protected UkIrelandFlashMap(RequestContext context) {
		super(context);
	}

	public final String getColor() {
		return color;
	}

	private String getDoc() {

		StringBuilder sb = new StringBuilder();

		sb.append("<config>");
		sb.append("<color_map>0x6699CC</color_map>");
		sb.append("<color_map_over>0xf0f0f0</color_map_over>");
		sb.append("<quantity>109</quantity>");
		sb.append("<background_color>0xffffff</background_color>");
		sb.append("<show_links>0</show_links>");
		sb.append("</config>");

		sb.append("<map_data>");

		for (State state : State.values()) {

			String name = getNames().get(state);
			if (name == null) {
				name = state.toString();
			}

			String link = getLinks().get(state);
			if (link == null) {
				link = "";
			}

			sb.append("<state>");
			sb.append("  <id>" + state.id + "</id>");
			sb.append("  <name>" + name + "</name>");
			sb.append("  <link>" + link + "</link>");
			sb.append("  <color_map>" + getColor() + "</color_map>");
			sb.append("  <color_map_over>" + getHoverColor() + "</color_map_over>");
			sb.append("</state>");
		}

		sb.append("</map_data>");

		return sb.toString();
	}

	public final String getHoverColor() {
		return hoverColor;
	}

	public final Map<State, String> getLinks() {
		return links;
	}

	public final Map<State, String> getNames() {
		return names;
	}

	public final void setColor(String color) {
		this.color = color;
	}

	public final void setHoverColor(String hoverColor) {
		this.hoverColor = hoverColor;
	}

	public final void setLinks(Map<State, String> links) {
		this.links = links;
	}

	public final void setNames(Map<State, String> names) {
		this.names = names;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb
				.append("<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0\" width=\"480\" height=\"590\" id=\"uk_ir_locator\" align=\"middle\">");
		sb.append("<param name=\"allowScriptAccess\" value=\"sameDomain\" />");
		sb.append("<param name=\"movie\" value=\"uk_ir_locator.swf\" /><param name=\"menu\" value=\"false\" />\n");
		sb.append("<param name=\"quality\" value=\"high\" />\n");
		sb.append("<param name=\"bgcolor\" value=\"#ffffff\" />\n");
		sb
				.append("<embed src=\"uk_ir_locator.swf\" menu=\"false\" quality=\"high\" bgcolor=\"#ffffff\" width=\"480\" height=\"590\" name=\"uk_ir_locator\" align=\"middle\" allowScriptAccess=\"sameDomain\" type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" />");
		sb.append("</object>\n");

		return sb.toString();
	}
}
