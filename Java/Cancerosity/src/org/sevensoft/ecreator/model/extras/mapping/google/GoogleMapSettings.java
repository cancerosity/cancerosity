package org.sevensoft.ecreator.model.extras.mapping.google;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 2 May 2007 09:44:12
 *
 */
@Singleton
@Table("google_maps")
public class GoogleMapSettings extends EntityObject {

	public static GoogleMapSettings getInstance(RequestContext context) {
		return EntityObject.getSingleton(context, GoogleMapSettings.class);
	}

	private String	googleMapsApiKey;

	protected GoogleMapSettings(RequestContext context) {
		super(context);
	}

	public final String getGoogleMapsApiKey() {
		return googleMapsApiKey;
	}

	public final void setGoogleMapsApiKey(String googleMapsApi) {
		this.googleMapsApiKey = googleMapsApi;
	}

}
