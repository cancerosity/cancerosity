package org.sevensoft.ecreator.model.extras.languages.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.extras.languages.panels.LanguageBarRender;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 17 May 2006 14:32:27
 *
 */
public final class LanguagesBarMarker extends MarkerHelper implements IGenericMarker {

	@Override
	public String getDescription() {
		return "Returns a span containing the flags of the languages";
	}

	public Object generate(RequestContext context, Map<String, String> params) {
		return new LanguageBarRender(context).toString();
	}

	public Object getRegex() {
		return new String[] { "languages_bar", "languages" };
	}
}