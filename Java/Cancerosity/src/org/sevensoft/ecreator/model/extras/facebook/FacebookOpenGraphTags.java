package org.sevensoft.ecreator.model.extras.facebook;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.media.images.ImageSupport;

/**
 * User: Tanya
 * Date: 05.03.2013
 */
@Table("facebook_open_graph")
@Singleton
public class FacebookOpenGraphTags extends ImageSupport {

    /**
     * an image to be used in case of category/item empty image list
     */
    private static final Integer IMAGE_LIMIT = 1;
    private String type = "website";

    public static FacebookOpenGraphTags getInstance(RequestContext context) {
        return getSingleton(context, FacebookOpenGraphTags.class);
    }

    protected FacebookOpenGraphTags(RequestContext context) {
        super(context);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getImageLimit() {
        return IMAGE_LIMIT;
    }
}
