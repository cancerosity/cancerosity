package org.sevensoft.ecreator.model.extras.ratings.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.extras.ratings.RatingModule;
import org.sevensoft.ecreator.model.extras.ratings.RatingType;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 11 Aug 2006 15:54:06
 *
 */
public class RatingsTableMarker implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		if (!ItemModule.Ratings.enabled(context, item))
			return null;

		Config config = Config.getInstance(context);
		RatingModule module = item.getItemType().getRatingModule();

		String none = params.get("none");
		if (none == null)
			none = "No ratings yet - be the first!";

		if (item.getRatingAverage() == 0)
			return "<span class='ratings-none'>" + none + "</span>";

		String caption = params.get("caption");

		Map<Integer, Integer> averages = item.getRatingAverages();

		StringBuilder sb = new StringBuilder();
		final TableTag tableTag = new TableTag("ratings_table");
		if (caption != null)
			tableTag.setCaption(caption);

		sb.append(tableTag);
		for (RatingType ratingType : module.getRatingTypes()) {

			final Integer average = averages.get(ratingType.getId());
			String src = module.getRatingGraphic(average);

			sb.append("<tr>");
			sb.append("<td class='rating_type'>" + ratingType.getName() + "</td>");

			if (src == null)
				sb.append("<td class='rating_value'>" + average + "</td>");
			else
				sb.append("<td class='rating_value'>" + new ImageTag(config.getTemplateDataPath() + "/" + src) + "</td>");

			sb.append("</tr>");
		}

		String src = module.getRatingGraphic(item.getRatingAverage());

		sb.append("<tr>");
		sb.append("<td>Overall</td>");

		if (src == null)
			sb.append("<td>" + item.getRatingAverage() + "</td>");
		else
			sb.append("<td>" + new ImageTag(config.getTemplateDataPath() + "/" + src) + "</td>");

		sb.append("</tr>");

		sb.append("</table>");

		return sb.toString();
	}

	public Object getRegex() {
		return "ratings_table";
	}

}
