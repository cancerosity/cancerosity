package org.sevensoft.ecreator.model.extras.meetups.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.extras.meetups.Meetup;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 25 Apr 2007 12:50:53
 *
 */
public class RsvpRemainingMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		if (!Module.Meetups.enabled(context)) {
			return null;
		}

		Meetup meetup = item.getMeetup();
		if (meetup.getMaxGuests() == 0) {
			return null;
		}

		return super.string(context, params, meetup.getMaxGuests() - meetup.getRsvps(true).size());
	}

	public Object getRegex() {
		return "meetups_rsvp_remaining";
	}

}
