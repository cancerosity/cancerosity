package org.sevensoft.ecreator.model.extras.meetups.blocks;

import java.util.List;

import org.sevensoft.ecreator.iface.admin.extras.meetups.MyRsvpsBlockHandler;
import org.sevensoft.ecreator.iface.frontend.meetups.RsvpHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.extras.meetups.Meetup;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 25 Apr 2007 13:02:21
 *
 */
@Table("meetups_blocks_myrsvps")
@Label("My rsvps")
@HandlerClass(MyRsvpsBlockHandler.class)
public class MyRsvpsBlock extends Block {

	public MyRsvpsBlock(RequestContext context) {
		super(context);
	}

	public MyRsvpsBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, objId);
	}

	@Override
	public Object render(RequestContext context) {

		Item item = (Item) context.getAttribute("item");

		// we must have an account
		if (item == null) {
			return null;
		}

		Meetup meetup = item.getMeetup();
		List<Item> items = meetup.getMyRsvps();

		StringBuilder sb = new StringBuilder();
		sb.append(new TableTag("ec_myrsvps"));

		sb.append("<tr><th colspan='3'>Events I am going to</th></tr>");

		for (Item event : items) {

			sb.append("<tr>");

			sb.append("<td class='img'>");

			Img img = event.getApprovedImage();
			if (img != null) {
				sb.append(new LinkTag(event.getUrl(), img.getThumbnailTag()));
			}

			sb.append("</td>");

			sb.append("<td class='details'>" + new LinkTag(event.getUrl(), event.getDisplayName()) + "</td>");
			sb.append("<td class='remove'>" + new LinkTag(RsvpHandler.class, "remove", "Leave this event", "item", item) + "</td>");

			sb.append("</tr>");
		}

		sb.append("</table>");

		return sb.toString();
	}

}
