package org.sevensoft.ecreator.model.extras.rss.export;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.text.ParseException;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.samdate.Date;

import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndContentImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndFeedImpl;

/**
 * @author sks 25 Jan 2007 17:42:00
 */
@Singleton
@Table("rss_export")
public class RssExport extends EntityObject {

    private static String[] feedTypes = new String[]{"rss_0.91", "rss_0.92", "rss_2.0"};

    public static RssExport getInstance(RequestContext context) {
        return EntityObject.getSingleton(context, RssExport.class);
    }

    private String feedType;
    private int limit;
    private String title;
    private String copyright;
    private String author;
    private String description;
    private transient MultiValueMap<Attribute, String> attributeValues;
    private Markup descriptionMarkup;
    private Attribute dateLimitAttribute;

    protected RssExport(RequestContext context) {
        super(context);
    }

    public final String getAuthor() {
        return author == null ? "Author" : author;
    }

    public final String getCopyright() {
        return copyright;
    }

    public SyndFeed getFeed(Category category) {

        List<Item> items = category.getItems(getLimit());

        SyndFeed feed = createFeed(items);

        feed.setDescription(category.getName());

        return feed;
    }

    public SyndFeed getFeed(Item account) {
        return getFeed(account, false, 0, null);
    }

    public SyndFeed getFeed(Item account, boolean all, int limit, ItemType iType) {

        List<Item> items = new ArrayList<Item>();
        Set<Integer> listingsItemTypesIds = ItemType.getSocialNetworkFeedItemTypeId(context);
//        List<ItemType> listingsItemTypes = ItemType.getSocialNetworkFeedItemType(context);
        ItemSearcher searcher = new ItemSearcher(context);
        searcher.setStatus("Live");
        searcher.setSearchAccount(account);
        searcher.setLimit(limit);
        if (all || iType == null) {
            searcher.setItemTypes(listingsItemTypesIds);
        } else {
            if (listingsItemTypesIds.contains(iType.getId())) {
                searcher.setItemType(iType);
            }
        }
        searcher.setSortType(SortType.Newest);
        searcher.setAttributeValues(attributeValues);

        items.addAll(searcher.getItems());
/*
        if (all) {
            for (ItemType itemType : listingsItemTypes) {
                if (iType == null || itemType.equals(iType)) {
                    if (items.size() < limit) {
                        items.addAll(Item.get(context, itemType, "Live", "dateCreated DESC"));
                    }
                }
            }
        } else {
            for (ItemType itemType : listingsItemTypes) {
                if (iType == null || itemType.equals(iType)) {
                    if (items.size() < limit) {
                        items.addAll(Item.get(context, itemType, "Live", account));
                    }
                }
            }
        }
*/

     /*   if (items.size() > limit) {
            items = items.subList(0, limit);
        }*/

        if (getDateLimitAttribute() != null)
            CollectionsUtil.filter(items, new Predicate<Item>() {
                public boolean accept(Item i) {
                    try {
                        String value = i.getAttributeValue(getDateLimitAttribute());
                        if (value != null) {
                            return new Date(value).isAfterOrEqual(new Date());
                        }
                    } catch (ParseException e) {
                        return true;
                    }
                    return true;
                }
            });

        SyndFeed feed = createFeed(items);

        return feed;
    }

    private SyndFeed createFeed(List<Item> items) {
        Config config = Config.getInstance(context);

        SyndFeed feed = new SyndFeedImpl();
        feed.setFeedType(getFeedType());
        feed.setTitle(StringHelper.verifyXml(getTitle()));
        feed.setAuthor(getAuthor());
        feed.setDescription(getDescription());
        feed.setLink(config.getUrl());

        if (copyright != null) {
            feed.setCopyright(getCopyright());
        }

        List entries = new ArrayList();

        for (Item item : items) {

            SyndEntry entry = new SyndEntryImpl();
            entry.setLink(item.getUrl());
            entry.setPublishedDate(item.getDateCreated().getJdkDate());
            entry.setTitle(StringHelper.verifyXml(item.getName()));

            SyndContent description = new SyndContentImpl();

            MarkupRenderer r = new MarkupRenderer(context, getDescriptionMarkup());
            r.setBody(item);
            description.setValue(r.toString());
//            description.setValue(StringHelper.verifyXml(item.getContentStripped(200)));
            entry.setDescription(description);

            entries.add(entry);
        }

        feed.setEntries(entries);
        return feed;
    }


    public final String getFeedType() {
        return feedType == null ? "rss_2.0" : feedType;
    }

    public final int getLimit() {
        return limit;
    }

    public final String getTitle() {
        return title == null ? "My RSS Export" : title;
    }

    public String getDescription() {
        return description == null ? "Latest news about me!" : description;
    }

    public final void setAuthor(String author) {
        this.author = author;
    }

    public final void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public final void setFeedType(String feedType) {
        this.feedType = feedType;
    }

    public final void setLimit(int limit) {
        this.limit = limit;
    }

    public final void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAttributeValues(MultiValueMap<Attribute, String> attributeValues) {
        this.attributeValues = attributeValues;
    }

    public Markup getDescriptionMarkup() {
        if (descriptionMarkup == null) {

            descriptionMarkup = new Markup(context, new RssDescriptionMarkup());
            descriptionMarkup.setName("RSS Export Description Markup");
            descriptionMarkup.save();

            save();
        }

        return descriptionMarkup.pop();
    }

    public void setDescriptionMarkup(Markup descriptionMarkup) {
        this.descriptionMarkup = descriptionMarkup;
    }

    public Attribute getDateLimitAttribute() {
        return dateLimitAttribute == null ? null : (Attribute) dateLimitAttribute.pop();
    }

    public void setDateLimitAttribute(Attribute dateLimitAttribute) {
        this.dateLimitAttribute = dateLimitAttribute;
    }

    @Override
    protected void singletonInit(RequestContext context) {
        super.singletonInit(context);
    }

}
