package org.sevensoft.ecreator.model.extras.links;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.location.LocationUtil;

/**
 * @author sks 7 Nov 2006 16:54:39
 * 
 * A link off to another web page from an item
 *
 */
public enum LinkType {

	ViaMichelinStreet("ViaMichelin.com street map") {

		@Override
		public String getUrl(Item item) {

			String postcode = item.getAttributeValue("postcode");
			return getUrl(postcode);

		}

		private String getUrl(String postcode) {
			return postcode == null ? null : "http://www.viamichelin.com/viamichelin/gbr/dyn/controller/mapPerformPage?strCP=" + postcode;
		}
	},

	LocalSchools("Localschools.co.uk nearby schools") {

		@Override
		public String getUrl(Item item) {

			String postcode = item.getAttributeValue("postcode");
			return getUrl(postcode);

		}

		private String getUrl(String postcode) {
			return postcode == null ? null : "http://www.itsalwayslocal.co.uk/schools/results.asp?ac=&btnFind=Search+by+Area+Code&nm=&z=" + postcode;
		}

	},

	MultimapSatellite("Multimap.co.uk satellite map") {

		@Override
		public String getUrl(Item item) {

			String postcode = item.getAttributeValue("postcode");
			return getUrl(postcode);

		}

		private String getUrl(String postcode) {
			return postcode == null ? null : "http://www.multimap.com/map/photo.cgi?client=public&pc=" + postcode;
		}
	},

	MultimapStreet("Multimap.co.uk street map") {

		@Override
		public String getUrl(Item item) {

			String postcode = item.getAttributeValue("postcode");
			return getUrl(postcode);
		}

		/**
		 * 
		 */
		private String getUrl(String postcode) {
			return postcode == null ? null : "http://www.multimap.com/map/browse.cgi?client=public&db=pc&cidr_client=none&pc=" + postcode;
		}
	},

	GoogleStreet("Google street map") {

		@Override
		public String getDefaultLabel() {
			return "Google map";
		}

		@Override
		public String getUrl(Item item) {

			String postcode = item.getAttributeValue("postcode");
			return getUrl(postcode);
		}

		/**
		 * 
		 */
		private String getUrl(String postcode) {
			return postcode == null ? null : "http://maps.google.co.uk/maps?q=" + LocationUtil.getPostcodeFormatted(postcode);
		}
	},

	UKFreeTv("UkFree.tv freeview coverage") {

		@Override
		public String getDefaultLabel() {
			return "Freeview coverage";
		}

		@Override
		public String getUrl(Item item) {

			String postcode = item.getAttributeValue("postcode");
			return getUrl(postcode);
		}

		/**
		 * 
		 */
		private String getUrl(String postcode) {
			return postcode == null ? null : "http://www.ukfree.tv/askforDTT.php?postcode=" + postcode + "&goto=/transmitters.php";
		}

	},

	OurProperty("OurProperty.co.uk property prices") {

		@Override
		public String getDefaultLabel() {
			return "Area house prices";
		}

		@Override
		public String getUrl(Item item) {

			String postcode = item.getAttributeValue("postcode");
			return getUrl(postcode);
		}

		/**
		 * 
		 */
		private String getUrl(String postcode) {
			return postcode == null ? null : "http://www.ourproperty.co.uk/search.html?postcode=" + postcode;
		}
	},

	TheyWorkForYou("TheyWorkForYou.com Constituency info") {

		@Override
		public String getDefaultLabel() {
			return "Constituency info";
		}

		@Override
		public String getUrl(Item item) {

			String postcode = item.getAttributeValue("postcode");
			return getUrl(postcode);
		}

		/**
		 * 
		 */
		private String getUrl(String postcode) {
			return postcode == null ? null : "http://www.theyworkforyou.com/mp/?pc=" + postcode;
		}
	},

	SamKnows("Samknows.com broadband checker") {

		@Override
		public String getDefaultLabel() {
			return "Broadband availability";
		}

		@Override
		public String getUrl(Item item) {

			String postcode = item.getAttributeValue("postcode");
			return getUrl(postcode);
		}

		/**
		 * 
		 */
		private String getUrl(String postcode) {
			return postcode == null ? null : "http://www.samknows.com/broadband/checker2.php@~@p=summary?postcode=" + postcode;
		}
	},

	MousePrice("MousePrice.com property prices") {

		@Override
		public String getDefaultLabel() {
			return "Area house prices";
		}

		@Override
		public String getUrl(Item item) {

			String postcode = item.getAttributeValue("postcode");
			return getUrl(postcode);
		}

		/**
		 * 
		 */
		private String getUrl(String postcode) {
			return postcode == null ? null : "http://www.mouseprice.com/SearchResults.aspx?Search=S&PostCode=" + postcode;
		}
	},

	UpMyStreet("UpMyStreet.com local info") {

		@Override
		public String getUrl(Item item) {

			String postcode = item.getAttributeValue("postcode");
			return getUrl(postcode);
		}

		/**
		 * 
		 */
		private String getUrl(String postcode) {
			return postcode == null ? null : "http://www.upmystreet.com/overview/?l1=" + postcode;
		}

	},

	UkLocalArea("UkLocalArea.com local info") {

		@Override
		public String getDefaultLabel() {
			return "Local area info";
		}

		@Override
		public String getUrl(Item item) {

			String postcode = item.getAttributeValue("postcode");
			return getUrl(postcode);
		}

		/**
		 * 
		 */
		private String getUrl(String postcode) {
			return postcode == null ? null : "http://www.uklocalarea.com/index.php?q==" + postcode;
		}

	},

	WeatherDotCom("WeatherDotCom weather") {

		@Override
		public String getDefaultLabel() {
			return "Weather forcast";
		}

		@Override
		public String getUrl(Item item) {

			String postcode = item.getAttributeValue("postcode");
			return getUrl(postcode);
		}

		/**
		 * 
		 */
		private String getUrl(String postcode) {
			return postcode == null ? null : "http://uk.weather.com/weather/local/" + postcode.substring(3);
		}
	},

	BbcWeather("Bbc.co.uk weather") {

		@Override
		public String getDefaultLabel() {
			return "Weather forecast";
		}

		@Override
		public String getUrl(Item item) {

			String postcode = item.getAttributeValue("postcode");
			return getUrl(postcode);
		}

		/**
		 * 
		 */
		private String getUrl(String postcode) {
//			return postcode == null ? null : "http://www.bbc.co.uk/cgi-perl/weather/search/new_search.pl?search_query=" + postcode;
            return postcode == null ? null : "http://www.bbc.co.uk/weather/" + postcode.split("\\s")[0].toLowerCase();
		}
	},

	GeographPhotos("Geograph.co.uk area photos") {

		@Override
		public String getDefaultLabel() {
			return "Local photos";
		}

		@Override
		public String getUrl(Item item) {

			String postcode = item.getAttributeValue("postcode");
			return getUrl(postcode);
		}

		private String getUrl(String postcode) {
			return postcode == null ? null : "http://www.geograph.org.uk/search.php?q=" + postcode;
		}

	};

	private final String	toString;

	LinkType(String toString) {
		this.toString = toString;
	}

	/**
	 * 
	 */
	public String getDefaultLabel() {
		return toString();
	}

	public abstract String getUrl(Item item);

	@Override
	public String toString() {
		return toString;
	}

}
