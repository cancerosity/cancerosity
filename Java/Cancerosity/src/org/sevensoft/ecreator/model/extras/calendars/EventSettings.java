package org.sevensoft.ecreator.model.extras.calendars;

import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;

/**
 * @author sks 13-Jul-2005 20:59:38
 * 
 */
@Table("settings_events")
@Singleton
public class EventSettings {

	private boolean	eventsNewsletter;

	public boolean isCalendarBox() {
		return false;
	}

	public boolean isEventsNewsletter() {
		return eventsNewsletter;
	}

	public void setEventsNewsletter(boolean eventsNewsletter) {
		this.eventsNewsletter = eventsNewsletter;
	}
}
