package org.sevensoft.ecreator.model.extras.languages;

/**
 * @author sks 12-Aug-2005 20:22:38
 * 
 */
public enum Language {

	English, French, German, Italian, Portugese, Spanish, Dutch, Russian, Swedish, Danish, Norwegian, Czech, Polish, Slovak, Hungarian, Belarusian, Ukrainian, Bulgarian, Romanian, Serbian, Croatian;

}
