package org.sevensoft.ecreator.model.extras.shoppingbox;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 8 Jun 2007 11:29:57
 *
 */
@Table("myshoppingbox")
public class ShoppingBox extends EntityObject {

	private Item		item;
	private int			weight;
	private boolean		paid;
	private TransportMethod	transportMethod;
	private Money		cost;

	protected ShoppingBox(RequestContext context) {
		super(context);
	}

	public ShoppingBox(RequestContext context, Item item) {
		super(context);
		this.item = item;
		this.cost = new Money(0);
		save();
	}

	public Money getCost() {
		return cost;
	}

	public Item getItem() {
		return item.pop();
	}

	public TransportMethod getTransportMethod() {
		return transportMethod;
	}

	public int getWeight() {
		return weight;
	}

	public boolean isAir() {
		return transportMethod == TransportMethod.Air;
	}

	public boolean isPaid() {
		return paid;
	}

	public boolean isSea() {
		return transportMethod == TransportMethod.Sea;
	}

	public void setCost(Money cost) {
		this.cost = cost;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

	public void setTransportMethod(TransportMethod transportMethod) {
		this.transportMethod = transportMethod;
		this.cost = getCost(transportMethod);

		save();
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	/**
	 * Returns the cost of shipping this item by the transport method parameter.
	 */
	public Money getCost(TransportMethod method) {

		if (method == null) {
			return new Money(0);
		}

		ShoppingBoxSettings shoppingBoxSettings = ShoppingBoxSettings.getInstance(context);

		switch (method) {

		default:
		case Air:

			return shoppingBoxSettings.getAirCostPerUnit().multiply(weight);

		case Sea:
			return shoppingBoxSettings.getSeaCostPerUnit().multiply(weight);
		}
	}

	/**
	 * 
	 */
	public boolean hasTransportMethod() {
		return transportMethod != null;
	}
}
