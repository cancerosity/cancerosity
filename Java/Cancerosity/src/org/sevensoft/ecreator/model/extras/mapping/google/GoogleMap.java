package org.sevensoft.ecreator.model.extras.mapping.google;

import java.util.HashMap;
import java.util.List;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.OSRef;

/**
 * @author sks 22 Apr 2007 00:16:34
 *
 */
public class GoogleMap {

	/**
	 * For use on 7soft.co.uk domain
	 */
	public static final String			SevensoftCoUkKey	= "ABQIAAAAfmsumKlMpySGiQ9tYtJwHxS_hLuAUfclB4aGygN4f1zDAmlcyRTh1xxFE3MOfuB-Tcd6rO5p-ebptw";

	private RequestContext				context;

	private int						zoom;

	private boolean					infoWindow;

	private String					elementId;

	private MultiValueMap<Integer, Item>	items;

	public GoogleMap(RequestContext context) {
		this.context = context;
		this.infoWindow = true;
		this.zoom = 8;
		this.elementId = "ec_gmap";
		this.items = new MultiValueMap(new HashMap());
	}

	public void addItems(int itemTypeId, List<Item> items) {
		this.items.putAll(itemTypeId, items);
	}

	public void addItems(List<Item> items) {
		addItems(0, items);
	}

	private StringBuilder getWindowHtml(Item item) {

		StringBuilder itemHtml = new StringBuilder("<table class='ec_gmap_balloon'><tr><td valign='top' class='thumb'>");
		if (item.hasApprovedImages()) {
			itemHtml.append(item.getApprovedImage().getThumbnailTag(75, 60).toString().replace("\"", "'"));
		}
		itemHtml.append("</td><td valign='top' class='details'><span class='name'>");

		itemHtml.append(item.getName());
		itemHtml.append("</span><br/><span class='snippet'>");

		String summary = item.getSummary(80, "...");
		if (summary != null) {
			summary = summary.replace("\"", "'").replace("\n", "").replace("\r", "");
			itemHtml.append(summary);
		}

		itemHtml.append("</span></td></tr></table>");
		return itemHtml;
	}

	private int getZoom() {
		return zoom < 1 ? 3 : zoom;
	}

	public final void setElementId(String elementId) {
		this.elementId = elementId;
	}

	public void setInfoWindow(boolean b) {
		this.infoWindow = b;
	}

	public void setZoom(int i) {
		zoom = i;
	}

	@Override
	public String toString() {

		context.setAttribute("gmap", true);

		StringBuilder sb = new StringBuilder();

		sb.append("<script type=\"text/javascript\">\n\n");

		sb.append("var markerGroups = { group" + StringHelper.implode(items.keySet(), ":[], group") + ":[] }; \n");

		sb.append("function addMarker(map, group, p, html, link, icon) {	\n\n");

		sb.append("	var marker = new GMarker(p, icon); \n\n");
		sb.append("	map.addOverlay(marker);\n");
		sb.append("	GEvent.addListener(marker, \"mouseover\", function() { marker.openInfoWindowHtml(html); }); \n");
		sb.append("	GEvent.addListener(marker, \"mousedown\", function() { window.location=link; }); \n");

		sb.append("	markerGroups[group].push(marker);			\n");
		sb.append("	if (group != 'group0') { marker.hide(); }	\n");

		sb.append("}\n\n");

		sb.append("function gload() { \n");
		sb.append("	if (GBrowserIsCompatible()) {\n");

		sb.append("		map = new GMap2(document.getElementById('" + elementId + "'));	\n");

		// center on first non null
		Item centreItem = items.list(0).get(0);
		LatLng l = new OSRef(centreItem.getX(), centreItem.getY()).toLatLng();
		l.toWGS84();

		sb.append("		map.setCenter(new GLatLng(" + l.getLat() + "," + l.getLng() + "), " + getZoom() + "); \n");

		sb.append("		map.addControl(new GSmallMapControl()); \n");
		sb.append("		map.addControl(new GMapTypeControl()); \n\n");

		// build all markers and put them into the array
		for (int group : items.keySet()) {

			for (Item item : items.getAll(group)) {

				l = new OSRef(item.getX(), item.getY()).toLatLng();
				l.toWGS84();

				// check for icon for this item
				GoogleMapIcon icon = SimpleQuery.get(context, GoogleMapIcon.class, "itemType", item.getItemType());

				sb.append("	icon = null;	\n\n	");

				if (icon != null) {
					sb.append("	icon = new GIcon();	\n");
					sb.append("	icon.image = \"" + context.getContextPath() + "/template-data/" + icon.getFilename() + "\";	\n	");
					sb.append("	icon.iconSize = new GSize(" + icon.getWidth() + ", " + icon.getHeight() + ");	\n");
					sb.append("	icon.iconAnchor = new GPoint(" + icon.getAnchorX() + ", " + icon.getAnchorY() + ");	\n");
					sb.append("	icon.infoWindowAnchor = new GPoint(" + icon.getWindowAnchorX() + ", " + icon.getWindowAnchorY() + ");	\n\n");
				}

				sb.append("	addMarker(map, 'group" + group + "', new GLatLng(" + l.getLat() + "," + l.getLng() + "), \"" + getWindowHtml(item)
						+ "\", \"" + item.getUrl() + "\", icon);	\n\n\n");
			}
		}

		sb.append("	}\n\n");
		sb.append("}\n\n");

		for (int group : items.keySet()) {

			if (group > 0) {

				// build functions to show markers
				sb.append("function togglemarkers" + group + "() { \n\n");

				sb.append("	for (var i = 0; i < markerGroups['group" + group + "'].length; i++) {\n");
				sb.append("		var marker = markerGroups['group" + group + "'][i];\n");
				sb.append("		if (marker.isHidden()) {\n");
				sb.append("			marker.show();\n");
				sb.append("		} else {\n");
				sb.append("			marker.hide();\n");
				sb.append("		}\n");
				sb.append("	}\n");

				sb.append("}\n\n");

			}
		}

		sb.append("</script>\n\n");

		sb.append("<div id=\"" + elementId + "\" class=\"ec_gmap\" style=\"width: 100%; height: 500px\"></div>");

		return sb.toString();
	}
}
