package org.sevensoft.ecreator.model.extras.meetups;

import java.util.List;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.interaction.Interaction;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 25 Apr 2007 12:47:35
 *
 */
@Table("meetups")
public class Meetup extends EntityObject {

	public static Meetup get(RequestContext context, Item item) {
		Meetup meetup = SimpleQuery.get(context, Meetup.class, "item", item);
		return meetup == null ? new Meetup(context, item) : meetup;
	}

	/**
	 * Guest limit, etc
	 */
	private int		maxGuests;

	/**
	 * Item this meetup is for
	 */
	private Item	item;

	/**
	 * User can moderate who is accepted
	 */
	private boolean	moderated;

	/**
	 * Hide who is going.
	 */
	private boolean	privateGuests;

	protected Meetup(RequestContext context) {
		super(context);
	}

	public Meetup(RequestContext context, Item item) {
		super(context);

		this.item = item;
		this.moderated = true;
		this.privateGuests = false;
		this.maxGuests = 10;

		save();
	}

	public void addRsvp(Item rsvper, boolean yes, String message) {

		// remove existing rsvp for this user
		SimpleQuery.delete(context, Rsvp.class, "item", item, "account", rsvper);

		new Rsvp(context, item, rsvper, yes, message);

		if (item.hasAccount()) {

			Config config = Config.getInstance(context);

			// send pm to owner profile
			if (Module.PrivateMessages.enabled(context)) {

				Interaction i = item.getAccount().getInteraction();
				i.sendPm(item.getAccount(), "Someone has rsvped to your event", null);

			}

			if (item.getAccount().hasEmail()) {

				//send email to owner profile
				Email e = new Email();
				e.setSubject("Someone has joined your event");
				e.setBody("Someone has joined your event");
				e.setFrom(config.getServerEmail());
				e.setTo(item.getAccount().getEmail());

				try {
					new EmailDecorator(context, e).send(config.getSmtpHostname());
				} catch (EmailAddressException e1) {
					e1.printStackTrace();
				} catch (SmtpServerException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	public final Item getItem() {
		return item.pop();
	}

	public final int getMaxGuests() {
		return maxGuests;
	}

	public List<Item> getMyRsvps() {
		Query q = new Query(context, "select i.* from # i join # r on i.id=r.item where r.account=?");
		q.setTable(Item.class);
		q.setTable(Rsvp.class);
		q.setParameter(item);
		return q.execute(Item.class);
	}

	public List<Rsvp> getRsvps() {
		Query q = new Query(context, "select * from # where item=?");
		q.setTable(Rsvp.class);
		q.setParameter(item);
		return q.execute(Rsvp.class);
	}

	public List<Rsvp> getRsvps(boolean b) {
		Query q = new Query(context, "select * from # where item=? and yes=?");
		q.setTable(Rsvp.class);
		q.setParameter(item);
		q.setParameter(b);
		return q.execute(Rsvp.class);
	}

	public final boolean isModerated() {
		return moderated;
	}

	public final boolean isPrivateGuests() {
		return privateGuests;
	}

	public boolean isRsvp(Item rsvper) {
		return SimpleQuery.count(context, Rsvp.class, "item", item, "account", rsvper) > 0;
	}

	public void removeRsvp(Item account) {
		SimpleQuery.delete(context, Rsvp.class, "item", item, "account", account);
	}

	public final void setMaxGuests(int maxGuests) {
		this.maxGuests = maxGuests;
	}

	public final void setModerated(boolean moderated) {
		this.moderated = moderated;
	}

	public final void setPrivateGuests(boolean privateGuests) {
		this.privateGuests = privateGuests;
	}
}
