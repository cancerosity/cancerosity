package org.sevensoft.ecreator.model.extras.ratings.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.items.RatingsHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 11 Aug 2006 15:54:06
 *
 */
public class RatingAddMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		if (!ItemModule.Ratings.enabled(context, item)) {
			return null;
		}

		return super.link(context, params, new Link(RatingsHandler.class, null, "item", item), "add_rating");
	}

	public Object getRegex() {
		return "ratings_add";
	}

}
