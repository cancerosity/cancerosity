package org.sevensoft.ecreator.model.extras.polls.blocks;

import org.sevensoft.ecreator.iface.frontend.extras.poll.PollHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.extras.polls.Option;
import org.sevensoft.ecreator.model.extras.polls.Poll;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.RadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 5 Nov 2006 17:23:04
 *
 */
@Table("blocks_polls")
@Label("Poll")
public class PollBlock extends Block {

	private Poll	poll;

	protected PollBlock(RequestContext context) {
		super(context);
	}

	public PollBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, 0);

		this.poll = EntityObject.getInstance(context, Poll.class, objId);
		if (poll != null) {
			setName("Poll: " + getPoll().getTitle());
			save();
		}
	}

	public Poll getPoll() {
		return poll.pop();
	}

	@Override
	public Object render(RequestContext context) {

		StringBuilder sb = new StringBuilder();
		sb.append(new TableTag("poll", "center"));

		// start form
		sb.append(new FormTag(PollHandler.class, "vote", "post"));
		sb.append(new HiddenTag("poll", getPoll()));

		sb.append("<tr><td class='title'>");
		sb.append(poll.getTitle());
		sb.append("</td></tr>");

		sb.append("<tr><td>");

		for (Option option : poll.getOptions()) {

			sb.append(new RadioTag(context, "option", option));
			sb.append(option.getText());
			sb.append("<br/>");
		}

		sb.append("</td></tr>");
		sb.append("<tr><td class='showresults'>" + new LinkTag(PollHandler.class, null, "Show results", "poll", poll) + "</td></tr>");
		sb.append("<tr><td class='bottom' align='center'>");
		sb.append(new SubmitTag("Vote"));
		sb.append("</td></tr>");

		sb.append("</form>");

		sb.append("</table>");
		return sb;
	}

}
