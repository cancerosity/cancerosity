package org.sevensoft.ecreator.model.extras.cookie;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * User: Tanya
 * Date: 01.06.2012
 */
@Table("settings_cookie")
@Singleton
public class CookieSettings extends EntityObject {
    private String generateCodeUrl;
    private String cookieControlCode;

    public CookieSettings(RequestContext context) {
        super(context);
    }

    public static CookieSettings getInstance(RequestContext context) {
        return getSingleton(context, CookieSettings.class);
    }

    public String getGenerateCodeUrl() {
        return generateCodeUrl == null ? "http://civicuk.com/cookie-law/configuration" : generateCodeUrl;
    }

    public void setGenerateCodeUrl(String generateCodeUrl) {
        this.generateCodeUrl = generateCodeUrl;
    }

    public String getCookieControlCode() {
        return cookieControlCode;
    }

    public void setCookieControlCode(String cookieControlCode) {
        this.cookieControlCode = cookieControlCode;
    }
}
