package org.sevensoft.ecreator.model.extras.languages.tables;

import java.util.List;

import org.sevensoft.ecreator.model.extras.languages.Language;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 5 Dec 2006 11:27:02
 *
 */
@Table("languages_translations")
public class Translation extends EntityObject {

	public static List<Translation> get(RequestContext context, Language language) {
		return SimpleQuery.execute(context, Translation.class, "language", language);
	}

	public static String getTranslation(RequestContext context, Language language, String original) {

		Query q = new Query(context, "select translated from # where language=? and original=?");
		q.setTable(Translation.class);
		q.setParameter(language);
		q.setParameter(original);

		String string = q.getString();
		return string == null ? original : string;
	}

	public static String translate(RequestContext context, Language language, String text) {

		logger.fine("[Translation] translating langugage=" + language + ", text=" + text);

		if (!Module.Languages.enabled(context)) {
			return text;
		}

		if (language == Language.English) {
			return text;
		}

		String translated = getTranslation(context, language, text);
		return translated == null ? text : translated;
	}

	private String	original, translated;
	private Language	language;

	private Translation(RequestContext context) {
		super(context);
	}

	public Translation(RequestContext context, Language language, String original, String translated) {
		super(context);
		this.language = language;
		this.original = original;
		this.translated = translated;

		save();
	}

	public final Language getLanguage() {
		return language;
	}

	public final String getOriginal() {
		return original;
	}

	public final String getTranslated() {
		return translated;
	}

	public final void setLanguage(Language language) {
		this.language = language;
	}

	public final void setOriginal(String original) {
		this.original = original;
	}

	public final void setTranslated(String translated) {
		this.translated = translated;
	}

}
