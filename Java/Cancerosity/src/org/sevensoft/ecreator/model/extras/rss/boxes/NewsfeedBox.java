package org.sevensoft.ecreator.model.extras.rss.boxes;

import org.sevensoft.ecreator.iface.admin.extras.newsfeed.NewsfeedBoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.extras.rss.Newsfeed;
import org.sevensoft.ecreator.model.extras.rss.NewsfeedArticle;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 18 Jun 2006 20:04:56
 *
 */
@Table("boxes_newsfeeds")
@Label("Newsfeed")
@HandlerClass(NewsfeedBoxHandler.class)
public class NewsfeedBox extends Box {

	/*
	 *The newsfeeed this box is showing articles from
	 */
	private Newsfeed	newsfeed;

	/*
	 *Number of images to show.
	 */
	private int		limit;

	public NewsfeedBox(RequestContext context) {
		super(context);
	}

	public NewsfeedBox(RequestContext context, String location) {
		super(context, location);
	}

	@Override
	protected String getCssIdDefault() {
		return "newsfeed";
	}

	public final int getLimit() {
		return limit;
	}

	public Newsfeed getNewsfeed() {
		return newsfeed.pop();
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	@Override
	public String render(RequestContext context) {

		if (newsfeed == null) {
			logger.fine("[NewsfeedBox] no newsfeed set");
			return null;
		}

		StringBuilder sb = new StringBuilder();
		sb.append(getTableTag());

		for (NewsfeedArticle article : getNewsfeed().getArticles()) {
			sb.append("<tr><td>");
            sb.append("<div class='title'>" + new LinkTag(article.getLink(), article.getTitle()).setTarget("_blank") + "</div>");
            String description = article.getDescription(100);
            if (description.indexOf("<") != -1 || description.indexOf("/>") != -1) {
                description = article.getDescription();
            }
            sb.append("<div class='desc'>" + description + "</div>");
			sb.append("</td></tr>");
		}

		sb.append("</table>");
		return sb.toString();
	}

	public final void setLimit(int limit) {
		this.limit = limit;
	}

	public void setNewsfeed(Newsfeed newsfeed) {
		this.newsfeed = newsfeed;
	}

	@Override
	public void setObjId(int objId) {
		this.newsfeed = EntityObject.getInstance(context, Newsfeed.class, objId);
		save();
	}

}
