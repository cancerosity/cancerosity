package org.sevensoft.ecreator.model.extras.rss.markers;

import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.markers.ThumbnailPathMarker;
import org.sevensoft.ecreator.model.system.config.markers.SiteNameMarker;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * User: Tanya
 * Date: 15.02.2011
 */
public class RssImageMarker implements IItemMarker {

    public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
        if (item.hasApprovedImages()) {
            StringBuffer sb = new StringBuffer();
            sb.append("<img width='");
            sb.append(params.containsKey("width") ? params.get("width") : 160);
            sb.append("' ");
            sb.append("src='");
            sb.append(new SiteNameMarker().generate(context, params));
            sb.append(new ThumbnailPathMarker().generate(context, params, item, "", 0, 0));
            sb.append("'/>");

            return sb.toString();
//            return "<img width='160' src='[site_name][thumbnail_url?limit=1]'/>";
        } else {
            return null;
        }
    }

    public Object getRegex() {
        return "rss_image";
    }
}
