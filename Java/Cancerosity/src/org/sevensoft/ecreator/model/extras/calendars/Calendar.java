package org.sevensoft.ecreator.model.extras.calendars;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Transformer;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.iface.admin.items.EventDatesUtils;
import org.sevensoft.ecreator.iface.admin.items.EventsListHandler;
import org.sevensoft.ecreator.iface.frontend.search.ItemSearchHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeValue;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.Row;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

import java.util.List;
import java.util.TreeMap;

/**
 * @author sks 31 Aug 2006 17:36:40
 */
@Table("calendars")
public class Calendar {

    public enum Style {
        Grid, Flat
    }

    private final Attribute startAttribute;
    private final Attribute endAttribute;
    private final Date start;
    private final RequestContext context;
    private final Date end;
    private MultiValueMap<Date, Item> itemsMap;
    private final ItemType itemType;

    public Calendar(RequestContext context, Date start, ItemType itemType, Attribute startAttribute, Attribute endAttribute) {

        this.context = context;
        this.start = start.beginMonth();
        this.end = start.endMonth();
        this.itemType = itemType;
        this.startAttribute = startAttribute;
        this.endAttribute = endAttribute;
        if (!itemType.isFullEvent()) {
            initItemsMap();
        } else {
            initEventsItemsMap();
        }
    }

    public Link getDateSearchLink(Date date) {

        Link link = new Link(ItemSearchHandler.class);
        link.setParameter("itemType", itemType);
        link.addParameter(startAttribute.getParamName(), date.toString("dd-MM-yyyy"));
        link.addParameter(endAttribute.getParamName(), date.toString("dd-MM-yyyy"));
        link.addParameter("calendar", true);

        return link;
    }

    public Link getEventDateSearchLink(Date date) {

        Link link = new Link(EventsListHandler.class);
        link.addParameter("date", date);

        return link;
    }

    public Date getEnd() {
        return end;
    }

    public MultiValueMap<Date, Item> getItemsMap() {
        return itemsMap;
    }

    public List<Item> getItemsOn(Date date) {
        return itemsMap.getAll(date);
    }

    public Date getStart() {
        return start;
    }

    public boolean hasItemOn(Date date) {
        return itemsMap.containsKey(date);
    }

    /**
     * populates the map of items mapped to dates.
     * <p/>
     * Each date can be mapped to multiple items and vice versa
     */
    private void initItemsMap() {

        itemsMap = new MultiValueMap<Date, Item>(new TreeMap());

        Query q;
        if (endAttribute == null) {

            q = new Query(context,
                    "select i.id, av1.value, null from # i join # av1 on i.id=av1.item where av1.attribute=? and av1.value>=? and av1.value<?");
            q.setTable(Item.class);
            q.setTable(AttributeValue.class);
            q.setParameter(startAttribute);
            q.setParameter(start);
            q.setParameter(end.nextDay());

        } else {
            q = new Query(context, "select i.id, av1.value, av1.value from # i join # av1 on i.id=av1.item "
                    + "where (av1.attribute=? and av1.value>=?) or (av1.attribute=? and av1.value=?)");
            q.setTable(Item.class);
            q.setTable(AttributeValue.class);
            q.setParameter(startAttribute);
            q.setParameter(endAttribute);
            q.setParameter(start);
            q.setParameter(end.nextDay());
        }
        putToItemsMap(q);
    }

    private void initEventsItemsMap() {
        itemsMap = new MultiValueMap<Date, Item>(new TreeMap());
        processNonRecurrentEvents();
        processRecurrentEvents();
    }

    private void putToItemsMap(Query q) {
        List<Row> results = q.execute();
        for (Row row : results) {
            Item item = row.getObject(0, Item.class);
            Date s = row.getDate(1);
            Date e = row.getDate(2);
            if (s == null) {
                continue;
            } else if (e == null || s.equals(e)) {
                itemsMap.put(s, item);
            } else {
                for (Date date : s.listTo(e)) {
                    itemsMap.put(date, item);
                }
            }
        }
    }

    private boolean belongsToMonth(Date date) {
        return (date.isAfter(start) || date.equals(start)) && (date.equals(end.nextDay()) || date.isBefore(end.nextDay()));
    }

    private List<Item> getRecurrentEvents() {
        Query recurrenceEventsQuery = new Query(context, "SELECT i.id FROM # i JOIN # av1 ON i.id=av1.item WHERE av1.attribute=? AND av1.value='true' ");
        recurrenceEventsQuery.setTable(Item.class);
        recurrenceEventsQuery.setTable(AttributeValue.class);
        recurrenceEventsQuery.setParameter(itemType.getAttribute("Recurrence"));
        List<Row> items = recurrenceEventsQuery.execute();
        return CollectionsUtil.transform(items, new Transformer<Row, Item>() {
            public Item transform(Row row) {
                return row.getObject(0, Item.class);
            }

        });
    }


    private void processNonRecurrentEvents() {
        Query q = new Query(context, "SELECT i.id, av1.value, av2.value FROM # i JOIN # av1 ON i.id=av1.item JOIN # av2 ON i.id=av2.item"
                + " WHERE i.id NOT IN (SELECT i.id FROM # i JOIN # av3 ON i.id=av3.item WHERE av3.attribute=? AND av3.value='true') AND" +
                "   av1.attribute=? AND av2.attribute=? AND ((av1.value>=? AND av1.value<?) OR (av2.value>=? AND av2.value<?) OR (av1.value<? AND av2.value>?))");
        q.setTable(Item.class);
        q.setTable(AttributeValue.class);
        q.setTable(AttributeValue.class);
        q.setTable(Item.class);
        q.setTable(AttributeValue.class);
        q.setParameter(itemType.getAttribute("Recurrence"));
        q.setParameter(itemType.getAttribute("Date start"));
        q.setParameter(itemType.getAttribute("Date end"));
        q.setParameter(start);
        q.setParameter(end.nextDay());
        q.setParameter(start);
        q.setParameter(end.nextDay());
        q.setParameter(start);
        q.setParameter(end.nextDay());
        putToItemsMap(q);
    }

    private void processRecurrentEvents() {
        List<Item> recurrentEvents = getRecurrentEvents();
        for (Item event : recurrentEvents) {
            DateTime time = new DateTime(start).addSeconds(1);
            if (event.getAttributeValue("Date start") != null) {
                List<Date> eventDates = EventDatesUtils.evaluateEventDates(time, event);
                Date s = eventDates.get(0);
                Date e = eventDates.get(1);
                int iterations = 0;
                while (!s.isAfter(end)) {
                    if (s.equals(e)) {
                        if (belongsToMonth(s)) {
                            itemsMap.put(s, event);
                        }
                    } else {
                        for (Date date : s.listTo(e)) {
                            if (belongsToMonth(date)) {
                                itemsMap.put(date, event);
                            }
                        }
                    }
                    time = new DateTime(e.addDays(1)).addSeconds(1);
                    eventDates = EventDatesUtils.evaluateEventDates(time, event);
                    s = eventDates.get(0);
                    e = eventDates.get(1);
                    iterations++;
                    if (iterations > 1000000) {
                        break;
                    }
                }
            }
        }
    }

}
