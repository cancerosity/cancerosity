package org.sevensoft.ecreator.model.extras.polls;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20-Jul-2005 18:45:17
 * 
 */
@Table("polls_comments")
public class PollComment extends EntityObject {

	private String	comment, name, location;
	private DateTime	date;
	private Poll	poll;

	public PollComment(RequestContext context) {
		super(context);
	}

	public PollComment(RequestContext context, Poll poll, String name, String location, String comment) {
		super(context);
		this.poll = poll;
		this.name = name;
		this.location = location;
		this.comment = comment;
		this.date = new DateTime();

		save();
	}

	public String getComment() {
		return comment;
	}

	public DateTime getDate() {
		return date;
	}

	public String getLocation() {
		return location;
	}

	public String getName() {
		return name;
	}

	public Poll getPoll() {
		return poll.pop();
	}

}
