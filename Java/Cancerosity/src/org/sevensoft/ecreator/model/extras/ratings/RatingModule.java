package org.sevensoft.ecreator.model.extras.ratings;

import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sks 16 Aug 2006 14:01:02
 */
@Table("ratings_module")
public class RatingModule extends EntityObject {

    public static RatingModule get(RequestContext context, ItemType itemType) {
        RatingModule module = SimpleQuery.get(context, RatingModule.class, "itemType", itemType);
        if (module == null)
            module = new RatingModule(context, itemType);
        return module;
    }

    /**
     * Enable textual reviews on this item type
     */
    private boolean reviews;

    /**
     * The item type owner
     */
    private ItemType itemType;

    /**
     * A simple map containing a graphic for each integer value in the ratings range
     */
    private Map<Integer, String> ratingGraphics;

    /**
     * Scale of ratings runs from 1 to this, in whole integer values
     */
    private int ratingScale;

    /**
     * Markup to display reviews
     */
    private Markup reviewsMarkup;

    private int attributeCount;

    private RatingModule(RequestContext context) {
        super(context);
    }

    public RatingModule(RequestContext context, ItemType itemType) {
        super(context);
        this.itemType = itemType;
        save();
    }

    public void addRatingType(String name) {
        new RatingType(context, this, name);
    }

    public int getAttributeCount() {
        return attributeCount;
    }

    public ItemType getItemType() {
        return itemType.pop();
    }

    public String getRatingGraphic(int n) {
        return getRatingGraphics().get(n);
    }

    public Map<Integer, String> getRatingGraphics() {
        if (ratingGraphics == null)
            ratingGraphics = new HashMap<Integer, String>();
        return ratingGraphics;
    }

    public List<Rating> getRatings(Item rater, Item item) {
        Query q = new Query(context, "select * from # where rater=? and item=?");
        q.setTable(Rating.class);
        q.setParameter(rater);
        q.setParameter(item);
        return q.execute(Rating.class);

    }

    public int getRatingScale() {
        return ratingScale;
    }

    public List<RatingType> getRatingTypes() {
        Query q = new Query(context, "select * from # where ratingModule=?");
        q.setTable(RatingType.class);
        q.setParameter(this);
        return q.execute(RatingType.class);
    }

    public Markup getReviewsMarkup() {
        return (Markup) (reviewsMarkup == null ? null : reviewsMarkup.pop());
    }

    public boolean hasAttributes() {
        return attributeCount > 0;
    }

    public void removeRatingType(RatingType c) {
        c.delete();
    }

    public void setRatingGraphic(int n, String string) {
        getRatingGraphics().put(n, string);
    }

    public void setRatingScale(int i) {
        this.ratingScale = i;
    }

}
