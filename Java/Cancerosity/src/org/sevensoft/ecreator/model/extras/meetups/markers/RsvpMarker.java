package org.sevensoft.ecreator.model.extras.meetups.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.meetups.RsvpHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 25 Apr 2007 12:50:53
 *
 */
public class RsvpMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		if (!Module.Meetups.enabled(context)) {
			return null;
		}

		Link link = new Link(RsvpHandler.class, null, "item", item);
		return super.link(context, params, link);
	}

	public Object getRegex() {
		return "meetups_rsvp";
	}

}
