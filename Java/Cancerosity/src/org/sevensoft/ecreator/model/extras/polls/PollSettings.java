package org.sevensoft.ecreator.model.extras.polls;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 18-May-2005 12:11:06
 * 
 */
@Table("settings_poll")
@Singleton
public class PollSettings extends EntityObject {

	public static PollSettings getInstance(RequestContext context) {
		return getSingleton(context, PollSettings.class);
	}

	/**
	 * Content for the results page
	 */
	private String	resultsContent;

	public PollSettings(RequestContext context) {
		super(context);
	}

	public String getResultsContent() {
		return resultsContent;
	}

	public boolean hasResultsContent() {
		return resultsContent != null;
	}

	public void setResultsContent(String resultsContent) {
		this.resultsContent = resultsContent;
	}

	@Override
	protected void singletonInit(RequestContext context) {

		Poll poll = new Poll(context, "Would you like to be able to publish Polls on your *own* website?");

		poll.addOption("Nothing would excite me more!");
		poll.addOption("Poll-ease!");
		poll.addOption("What's a poll?");
		poll.addOption("I'll decide later...much later...");

		poll.setContent("We wanted to know if people liked our poll system. Here you can find the results of who has voted so far."
				+ "<br/><br/>Using our simple poll editor, anyone can add professional looking polls to their website in only a few clicks.");
		poll.save();
	}
}