package org.sevensoft.ecreator.model.extras.select;

/**
 * User: Tanya
 * Date: 17.04.2012
 */
public interface SelectableOwner {

    public String getSelectableName();
}
