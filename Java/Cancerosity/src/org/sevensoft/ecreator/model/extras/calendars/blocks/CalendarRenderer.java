package org.sevensoft.ecreator.model.extras.calendars.blocks;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.Day;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Keypad;

/**
 * @author sks 25 Jan 2007 12:51:36
 */
public class CalendarRenderer {

    private final Keypad keypad;
    private final Date month;
    private final MultiValueMap<Date, Item> items;

    public CalendarRenderer(RequestContext context, Date month, MultiValueMap<Date, Item> items) {

        this.month = month;
        this.items = items;

        keypad = new Keypad(7);
        keypad.setPadEmpty(true);
        keypad.setCellVAlign("top");
        keypad.setTableClass("ec_calendar");

        keypad.addObject("Monday");
        keypad.addObject("Tuesday");
        keypad.addObject("Wednesday");
        keypad.addObject("Thursday");
        keypad.addObject("Friday");
        keypad.addObject("Saturday");
        keypad.addObject("Sunday");

        // pad out with extra non days at start of month
        Day day = month.getDay();
        if (day != Day.Monday) {
            keypad.setStartCol(day.getDaysBetween(Day.Monday));
        }

        Date date = month;
        for (int n = 0; n < month.getDaysInMonth(); n++) {

            StringBuilder sb = new StringBuilder();
            sb.append("<div class='day'>" + date.getDayOfMonth() + "</div>");

            if (items != null) {
                int m = 0;
                for (Item item : items.list(date)) {

                    sb.append("<div class='item'>" + new LinkTag(item.getUrl(), item.getDisplayName()) + "</div>");
                    m++;

                    if (m == 5) {
                        break;
                    }
                }
            }

            keypad.addObject(sb);
        }

    }

    @Override
    public String toString() {
        return keypad.toString();
    }

}
