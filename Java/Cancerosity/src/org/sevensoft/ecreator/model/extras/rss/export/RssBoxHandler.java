package org.sevensoft.ecreator.model.extras.rss.export;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 9 Mar 2007 13:33:09
 *
 */
@Path("admin-boxes-rss.do")
public class RssBoxHandler extends BoxHandler {

	private RssExportBox	box;

	public RssBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {

	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

}
