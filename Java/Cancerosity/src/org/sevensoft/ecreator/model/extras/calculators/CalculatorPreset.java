package org.sevensoft.ecreator.model.extras.calculators;

import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 2 May 2006 15:13:49
 *
 */
public enum CalculatorPreset {

	MortgageCalculator() {

		public Calculator create(RequestContext context) {

			Calculator calculator = new Calculator(context, "Mortgage calculator");
			calculator.addField("Sale price", "p");
			calculator.addField("Deposit", "d");
			calculator.addField("Interest rate", "r");
			calculator.addField("Term", "t");

			return calculator;
		}

	};

	public abstract Calculator create(RequestContext context);

}
