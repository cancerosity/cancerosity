package org.sevensoft.ecreator.model.extras.calculators.renderers;

import java.util.Map;

import org.sevensoft.ecreator.model.extras.calculators.CalculatorField;
import org.sevensoft.ecreator.model.extras.calculators.CalculatorFieldOption;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.RadioTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 15 Sep 2006 10:19:48
 *
 */
public class CalculatorFieldRenderer {

	private CalculatorField				field;
	private Map<CalculatorField, String>	calculationData;
	private RequestContext				context;

	public CalculatorFieldRenderer(RequestContext context, CalculatorField field) {
		this.context = context;
		this.field = field;
		this.calculationData = (Map<CalculatorField, String>) context.getAttribute("calculationData");
	}

	@Override
	public String toString() {

		switch (field.getType()) {

		case Expression:

			String value = "--";
			if (calculationData != null && calculationData.containsKey(field))
				value = calculationData.get(field);

			return value;

		case Numeric:

			return new TextTag(context, field.getParam(), field.getDefaultValue(), field.getWidth()).toString();

		case Radio:

			StringBuilder sb = new StringBuilder();

			for (CalculatorFieldOption option : field.getOptions()) {
				sb.append(new RadioTag(context, field.getParam(), option));
				sb.append(" ");
				sb.append(option.getText());
			}

			return sb.toString();

		case Selection:

			SelectTag tag = new SelectTag(context, field.getParam());
			for (CalculatorFieldOption option : field.getOptions())
				tag.addOption(option, option.getText());

			return tag.toString();
		}

		return null;
	}

}
