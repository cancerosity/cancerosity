package org.sevensoft.ecreator.model.extras.mapping.interactive;

import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 4 Apr 2007 11:36:01
 *
 */
@Table("blocks_maps_clickable")
public class ClickableMapBlock extends Block {

	private UkIrelandFlashMap	block;

	public ClickableMapBlock(RequestContext context) {
		super(context);
	}

	public ClickableMapBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, objId);
	}

	@Override
	public Object render(RequestContext context) {
		return null;
	}

}
