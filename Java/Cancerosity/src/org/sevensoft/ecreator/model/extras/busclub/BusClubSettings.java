package org.sevensoft.ecreator.model.extras.busclub;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 18 Jun 2007 11:03:33
 *
 */
@Table("busclub_settings")
@Singleton
public class BusClubSettings extends EntityObject {

	public static BusClubSettings getInstance(RequestContext context) {
		return EntityObject.getSingleton(context, BusClubSettings.class);
	}

	private Money	affiliationDiscount;

	protected BusClubSettings(RequestContext context) {
		super(context);
	}

	public Money getAffiliationDiscount() {
		return affiliationDiscount;
	}

	public void setAffiliationDiscount(Money affiliationDiscount) {
		this.affiliationDiscount = affiliationDiscount;
	}

}
