package org.sevensoft.ecreator.model.extras.tellfriend;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.extras.tellfriend.TellFriendHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.ICategoryMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 17 Jul 2006 15:21:38
 *
 */
public class TellFriendMarker extends MarkerHelper implements IItemMarker, ICategoryMarker {

	public Object generate(RequestContext context, Map<String, String> params, Category category) {
		return generate(context, params, category.getUrl());
	}

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
		return generate(context, params, item.getUrl());
	}

	private Object generate(RequestContext context, Map<String, String> params, String url) {

		params.put("popup", "1");
		params.put("width", "300");
		params.put("height", "280");
		params.put("status", "no");
		params.put("resize", "no");
		params.put("windowname", "tellafriend");

		return super.link(context, params, new Link(TellFriendHandler.class, null, "url", url), null);

	}

	public Object getRegex() {
		return new String[] { "tellfriend", "referafriend" };
	}
}
