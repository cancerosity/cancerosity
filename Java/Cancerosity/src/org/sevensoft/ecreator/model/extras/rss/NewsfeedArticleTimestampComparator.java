package org.sevensoft.ecreator.model.extras.rss;

import java.util.Comparator;

/**
 * @author sks 16 Jan 2008 07:37:03
 *
 */
class NewsfeedArticleTimestampComparator implements Comparator<NewsfeedArticle> {

	public int compare(NewsfeedArticle o1, NewsfeedArticle o2) {

		try {

			Long l1 = o1.getTimestamp();
			Long l2 = o2.getTimestamp();

			return l1.compareTo(l2);

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}