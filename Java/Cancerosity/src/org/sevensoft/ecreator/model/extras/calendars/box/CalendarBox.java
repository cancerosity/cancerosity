package org.sevensoft.ecreator.model.extras.calendars.box;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.iface.admin.extras.calendars.CalendarBoxHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.extras.calendars.Calendar;
import org.sevensoft.ecreator.model.extras.calendars.Calendar.Style;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.http.util.Link;

import java.text.ParseException;
import java.util.Map;

/**
 * @author sks 13 Jul 2006 16:11:57
 */
@Table("boxes_calendar")
@Label("Calendar")
@HandlerClass(CalendarBoxHandler.class)
public class CalendarBox extends Box {

    /**
     * The item type to show items from
     */
    private ItemType itemType;

    /**
     * The rendering style of this box
     */
    private Style style;

    /**
     * Attributes used to get dates
     */
    private Attribute startAttribute, endAttribute;

    /**
     *
     */
    private String dateCaption;

    /**
     *
     */
    private String itemCaption;

    /**
     * Limit on flat lists
     */
    private int limit;

    private transient String param;

    private String todayColor;

    private String eventColor;

    private MultiValueMap<Attribute, String> attributeValues;

    public CalendarBox(RequestContext context) {
        super(context);
    }

    public CalendarBox(RequestContext context, String location) {
        super(context, location);
    }

    @Override
    protected String getCssIdDefault() {
        return "calendar";
    }

    public String getDateCaption() {
        return dateCaption == null ? "Date" : dateCaption;
    }

    public Attribute getEndAttribute() {
        return (Attribute) (endAttribute == null ? null : endAttribute.pop());
    }

    public String getItemCaption() {
        return itemCaption == null ? "Event (Click for info)" : itemCaption;
    }

    public ItemType getItemType() {
        return (ItemType) (itemType == null ? null : itemType.pop());
    }

    public int getLimit() {
        return limit;
    }

    public Attribute getStartAttribute() {
        return (Attribute) (startAttribute == null ? null : startAttribute.pop());
    }

    public Style getStyle() {
        return style == null ? Style.Grid : style;
    }

    @Override
    public boolean hasEditableContent() {
        return false;
    }

    public boolean hasItemType() {
        return itemType != null;
    }

    @Override
    public String render(RequestContext context) {

        if (itemType == null) {
            logger.fine("[CalenderBox] no item type set");
            return null;
        }

        if (startAttribute == null) {
            logger.fine("[CalenderBox] no start attribute set");
            return null;
        }

        // param name for the string value of this cals month
        param = "calendarBoxDate_" + getId();

        // lets see if we changed the date via the arrows
        String stringDate = context.getParameter(param);
        if (stringDate != null) {
            // lets set the new value as a cookie
            context.setCookie(param, stringDate, false, -1);

            // if no context var then lets try a cookie
        } else {
            stringDate = context.getCookieValue(param);
        }

        Date start = null;
        if (stringDate != null) {
            try {
                start = new Date(stringDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (start == null) {
            start = new Date();
        }

        Calendar cal = new Calendar(context, start, getItemType(), getStartAttribute(), getEndAttribute());

        switch (getStyle()) {

            case Flat:

                return renderFlat(cal);

            default:
            case Grid:

                return renderGrid(cal);

        }

    }

    private String renderFlat(Calendar cal) {

        /*
           * In a flat list we want to render X number of items from this month
           */
        MultiValueMap<Date, Item> map = cal.getItemsMap();
        if (map.isEmpty()) {
            logger.fine("[CalenderBox] no items found");
            return null;
        }

        StringBuilder sb = new StringBuilder();

        sb.append(getTableTag());

        sb.append("<tr><th>Date</th><th>Event (Click for info)</th></tr>");

        int n = 0;
        for (Map.Entry<Date, Item> entry : map.entrySet()) {

            Date date = entry.getKey();
            Item item = entry.getValue();

            sb.append("<tr><td class='date'>" + date.toString("dd/MM") + "</td><td class='item'>" + item + "</td></tr>");

            n++;
            if (n > limit) {
                break;
            }
        }

        sb.append("<tr><td class='bottom' colspan='2'>&nbsp;</td></tr>");
        sb.append("</table>");

        return sb.toString();
    }

    private String renderGrid(Calendar cal) {

        // get a link back to this page again
        Link pagelink = (Link) context.getAttribute("pagelink");

        // if the link is null because our handler did not set a return link, then we will return to the home page
        if (pagelink == null) {
            pagelink = new Link(CategoryHandler.class);
        }

        Date start = cal.getStart();
        Date end = cal.getEnd();

        StringBuilder sb = new StringBuilder();

        sb.append(getTableTag());

        sb.append("<tr><th class='prev' align='center'>");
        pagelink.setParameter(param, start.previousMonth().toString("dd-MM-yyyy"));
        sb.append(new LinkTag(pagelink, "<"));
        sb.append("</th>");

        sb.append("<th colspan='5' class='month' align='center'>" + start.toString("MMMM yyyy") + "</th>");

        sb.append("<th class='next' align='center'>");
        pagelink.setParameter(param, start.nextMonth().toString("dd-MM-yyyy"));
        sb.append(new LinkTag(pagelink, ">"));
        sb.append("</th></tr>");

        sb.append("<tr>");
        sb.append("<td align='center'>M</td>");
        sb.append("<td align='center'>T</td>");
        sb.append("<td align='center'>W</td>");
        sb.append("<td align='center'>T</td>");
        sb.append("<td align='center'>F</td>");
        sb.append("<td align='center'>S</td>");
        sb.append("<td align='center'>S</td>");
        sb.append("</tr>");

        /*
           * Firstly we need to pad empty cells until we get to the day of the week that the 1st starts on
           */
        sb.append("<tr>");

        int n = 0;
        int day = Date.Monday;
        while (start.getDayOfWeek() != day) {
            sb.append("<td class='pad'>&nbsp;</td>");
            day = Date.nextDay(day);
            n++;
        }

        if (n % 7 == 0)
            sb.append("</tr>");

        Date today = new Date();
        Date date = start;

        while (!date.isAfter(end)) {

            if (n % 7 == 0) {
                sb.append("<tr>");
            }


            if (cal.hasItemOn(date)) {
                sb.append("<td align='center' style='background-color: " + (date.equals(today) ? getTodayColor() : getEventColor()) + "'>");
                Link link = itemType.isFullEvent() ? cal.getEventDateSearchLink(date) : cal.getDateSearchLink(date);
                sb.append(new LinkTag(link, date.getDayOfMonth()));
            } else {
                sb.append("<td align='center' " + (date.equals(today) ? "style='background-color: " + getTodayColor() + "'" : "") + ">");
                sb.append(date.getDayOfMonth());
            }

            sb.append("</td>");

            n++;

            if (n % 7 == 0) {
                sb.append("</tr>");
            }

            date = date.nextDay();
        }

        sb.append("<tr><td class='bottom' colspan='7'>&nbsp;</td></tr>");
        sb.append("</table>");

        return sb.toString();
    }

    public void setDateCaption(String dateCaption) {
        this.dateCaption = dateCaption;
    }

    public void setEndAttribute(Attribute endAttribute) {
        this.endAttribute = endAttribute;
    }

    public void setItemCaption(String itemCaption) {
        this.itemCaption = itemCaption;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setStartAttribute(Attribute startAttribute) {
        this.startAttribute = startAttribute;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public String getTodayColor() {
        return todayColor == null ? "red" : todayColor;
    }

    public void setTodayColor(String todayColor) {
        this.todayColor = todayColor;
    }

    public String getEventColor() {
        return eventColor == null ? "#6699FF" : eventColor;
    }

    public void setEventColor(String eventColor) {
        this.eventColor = eventColor;
    }

}
