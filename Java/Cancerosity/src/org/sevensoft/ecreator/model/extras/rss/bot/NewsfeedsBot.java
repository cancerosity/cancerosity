package org.sevensoft.ecreator.model.extras.rss.bot;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.logging.Logger;

import org.sevensoft.ecreator.model.extras.rss.Newsfeed;
import org.sevensoft.jeezy.http.RequestContext;

import com.sun.syndication.io.FeedException;

/**
 * @author sks 05-Jan-2006 23:40:51
 * 
 */
public class NewsfeedsBot implements Runnable {

	private static final Logger	logger	= Logger.getLogger("ecreator");
	private RequestContext		context;

	public NewsfeedsBot(RequestContext context) {
		this.context = context;
	}

	public void run() {

		logger.config("[NewsfeedsBot] running");

		for (Newsfeed newsfeed : Newsfeed.get(context))

			try {

				newsfeed.refresh();

			} catch (IllegalArgumentException e) {
				e.printStackTrace();

			} catch (MalformedURLException e) {
				e.printStackTrace();

			} catch (IOException e) {
				e.printStackTrace();

			} catch (FeedException e) {
				e.printStackTrace();
			}
	}

}
