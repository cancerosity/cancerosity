package org.sevensoft.ecreator.model.extras.licenses;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 2 May 2007 16:49:03
 *
 */
@Table("licenses")
public class License extends EntityObject {

	private static String decrypt(String encryptedText, String key) throws InvalidKeyException, InvalidKeySpecException, NoSuchAlgorithmException,
			NoSuchPaddingException, IOException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {

		DESedeKeySpec spec = new DESedeKeySpec(key.getBytes());

		SecretKeyFactory factory = SecretKeyFactory.getInstance("DESede");
		SecretKey secretKey = factory.generateSecret(spec);

		Cipher cipher = Cipher.getInstance("DESede/ECB/NoPadding");
		cipher.init(Cipher.DECRYPT_MODE, secretKey);

		byte[] utf8 = new sun.misc.BASE64Decoder().decodeBuffer(encryptedText);

		byte[] dec = cipher.doFinal(utf8);

		return new String(dec, "UTF-8");

	}

	private static String encrypt(String plainText, String key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidKeySpecException {

		DESedeKeySpec spec = new DESedeKeySpec(key.getBytes());

		SecretKeyFactory factory = SecretKeyFactory.getInstance("DESede");
		SecretKey secretKey = factory.generateSecret(spec);

		Cipher cipher = Cipher.getInstance("DESede");
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);

		// Encode the string into bytes using utf-8
		byte[] utf8 = plainText.getBytes("UTF8");

		// Encrypt
		byte[] enc = cipher.doFinal(utf8);

		// Encode bytes to base64 to get a string
		return new sun.misc.BASE64Encoder().encode(enc);
	}
	public static void main(String[] args) throws IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IOException, InvalidAlgorithmParameterException {

		String appCode = "P0WlXOml0xlVZ/vY3h1QggIVlxr88dO2i6rmyeBoBUeBkjGdqCtmKzicr66xyoZSXZ5zqRE5ujRSmQ1+FcqsFirPRnt6UxXj";

		String orderId = "1210-7028-0701-24190000000000000000000000000";

		String ident = decrypt(appCode, orderId);

		System.out.println(ident);

	}

	private String	key, encrypted;

	private String	orderId;

	private String	identifer;

	public License(RequestContext context) {
		super(context);
	}

	public License(RequestContext context, String orderId, String key) throws LicenseException, InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, IOException {

		super(context);

		// check this order is unique or the key is the same as last time
		if (SimpleQuery.count(context, License.class, "orderId", orderId) > 0) {
			if (SimpleQuery.count(context, License.class, "orderId", orderId, "key", key) == 0) {
				throw new LicenseException("This order has already been used for a registration key");
			}
		}

		this.orderId = orderId;
		this.key = key;

//		identifer = decryptAppCode(null);

//		encrypt();

		save();
	}

	public final String getEncrypted() {
		return encrypted;
	}

	public final String getKey() {
		return key;
	}

}
