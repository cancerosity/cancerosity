package org.sevensoft.ecreator.model.extras.mapping.google.blocks;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.misc.location.Pin;
import org.sevensoft.ecreator.iface.admin.items.modules.maps.GoogleMapBlockV3Handler;

import java.util.Map;
import java.util.List;
import java.util.HashMap;

import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.OSRef;

/**
 * User: Tanya
 * Date: 22.12.2011
 */
@Table("blocks_google_map_v3")
@Label("Google map V3")
@HandlerClass(GoogleMapBlockV3Handler.class)
public class GoogleMapBlockV3 extends Block {

    public GoogleMapBlockV3(RequestContext context) {
        super(context);
    }

    public GoogleMapBlockV3(RequestContext context, BlockOwner owner, int objId) {
        super(context, owner, objId);

    }

    public enum MapType {
        Hybrid, Satellite, Normal, Terrain
    }

    /**
     * For use on 7soft.co.uk domain
     */
    private static final String SevensoftCoUkKey = "ABQIAAAAfmsumKlMpySGiQ9tYtJwHxS_hLuAUfclB4aGygN4f1zDAmlcyRTh1xxFE3MOfuB-Tcd6rO5p-ebptw";

    /**
     * Api key for use by google
     */
    private String apiKey;

    /**
     *
     */
    private String header;

    /**
     *
     */
    private String footer;

    /**
     * Appears above the map in a div.
     */
    private String caption;

    private int zoom;

    private int iconWidth, iconHeight;

    private String iconFilename;
    /**
     *
     */
    private int iconAnchorX, iconAnchorY;

    /**
     *
     */
    private int iconWindowAnchorX, iconWindowAnchorY;

    private String location;

    private MapType mapType;

    private int width, height;

    private boolean showMapTypeControl;

    private boolean useLatLangAttr;

    private Attribute locationAttribute;

    private Attribute latAttribute;

    private Attribute langAttribute;


    public String getApiKey() {
        return apiKey;
    }

    public final String getCaption() {
        return caption;
    }

    public final String getFooter() {
        return footer;
    }

    public final String getHeader() {
        return header;
    }

    public final int getIconAnchorX() {
        return iconAnchorX;
    }

    public final int getIconAnchorY() {
        return iconAnchorY;
    }

    public final String getIconFilename() {
        return iconFilename;
    }

    public final int getIconHeight() {
        return iconHeight;
    }

    public final int getIconWidth() {
        return iconWidth;
    }

    public final int getIconWindowAnchorX() {
        return iconWindowAnchorX;
    }

    public final int getIconWindowAnchorY() {
        return iconWindowAnchorY;
    }

    public String getLocation() {
        return location;
    }

    public MapType getMapType() {
        return mapType == null ? MapType.Normal : mapType;
    }

    public Attribute getAttribute() {
        return (Attribute) (locationAttribute == null ? null : locationAttribute.pop());
    }

    public Attribute getLatAttribute() {
        return (Attribute) (latAttribute == null ? null : latAttribute.pop());
    }

    public Attribute getLangAttribute() {
        return (Attribute) (langAttribute == null ? null : langAttribute.pop());
    }

    private StringBuilder getWindowHtml(Item item) {

        StringBuilder itemHtml = new StringBuilder();
        itemHtml.append("<table class=\"ec_gmap_balloon\"><tr><td valign=\"top\" class=\"thumb\">");
        if (item.hasApprovedImages()) {
            itemHtml.append(item.getApprovedImage().getThumbnailTag(75, 60).toString().replace("'", "\""));
        }
        itemHtml.append("</td><td valign=\"top\" class=\"details\">");
        itemHtml.append("<div class=\"name\">");

        itemHtml.append("<a href=\"" + item.getUrl() + "\">" + item.getName() + "</a>");

        itemHtml.append("</div><div class=\"snippet\">");
        String summary = item.getSummary(130, "...");
        if (summary != null) {
            itemHtml.append(summary.replace("\"", "\\\"").replace("'", "\\\'"));
        }
        itemHtml.append("</div>");
        itemHtml.append("</td></tr></table>");
        return itemHtml;
    }

    public final int getZoom() {
        return zoom < 1 ? 16 : zoom;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isShowMapTypeControl() {
        return showMapTypeControl;
    }

    public boolean isUseLatLangAttr() {
        return useLatLangAttr;
    }

    public boolean hasFooter() {
        return footer != null;
    }

    public boolean hasHeader() {
        return header != null;
    }

    @Override
    public Object render(RequestContext context) {
        if (!Module.GoogleMapV3.enabled(context)) {
            return null;
        }

        Item item = (Item) context.getAttribute("item");
        Category category = (Category) context.getAttribute("category");

        CompileLocation compileLocation = new CompileLocation(context, item, category).invoke();
        Map<Item, String> locations = compileLocation.getLocations();
        if (locations.isEmpty()) {
            return null;
        }

        StringBuilder sb = new StringBuilder();

        sb.append("\n<!-- start google map -->\n");

//		sb.append("<script src=\"http://maps.google.com/maps?file=api&amp;v=2&amp;key=" + GoogleMapSettings.getInstance(context).getGoogleMapsApiKey() + "\" type=\"text/javascript\"></script>\n\n");

        sb.append("<script type=\"text/javascript\">\n\n");

        initMap(sb, locations);

        if (!isUseLatLangAttr()) {
            // show marker window if item

            for (Map.Entry<Item, String> entry : locations.entrySet()) {
                String contentString = entry.getKey().getName();
                String id = entry.getKey().getIdString();
                if (item != null) {
                    contentString = getWindowHtml(item).toString();
                } else if (category != null) {
                    contentString = getWindowHtml(entry.getKey()).toString();
                }

//                sb.append("\n var infowindow" + id + " = new google.maps.InfoWindow({\n" +
//                        "    content: \"" + contentString + "\"\n" +
//                        "});\n");


                if (getIconFilename() != null && getIconFilename().trim().length() != 0) {
                    sb.append("var image = new google.maps.MarkerImage('template-data/" + getIconFilename() + "',\n" +
//                        "      // This marker is 20 pixels wide by 32 pixels tall.\n" +
                            "      new google.maps.Size(" + getIconWidth() + ", " + getIconHeight() + "),\n" +
//                        "      // The origin for this image is 0,0.\n" +
                            "      new google.maps.Point(0,0),\n" +
//                        "      // The anchor for this image is the base of the flagpole at 0,32.\n" +
                            "      new google.maps.Point(" + getIconAnchorX() + ", " + getIconAnchorY() + "));\n");
                }

                sb.append("var marker" + id + ";\n");
                sb.append("var geocoder1 = new google.maps.Geocoder();");
                sb.append("geocoder1.geocode( { 'address': \"" + entry.getValue() + "\"}, function(results, status) {\n" +
                        "      if (status == google.maps.GeocoderStatus.OK) {\n" +
                        "           map.setCenter(results[0].geometry.location);\n" +
                        "           marker" + id + " = new google.maps.Marker({\n" +
                        "               position: results[0].geometry.location,\n" +
                        "               map: map,\n" +
                        "               title: \"" + entry.getKey().getName() + "\"");
                if (getIconFilename() != null && getIconFilename().trim().length() != 0) {
                    sb.append(",\n      icon: image\n");
                }
                sb.append("        });\n");

                sb.append("\n var infowindow" + id + " = new google.maps.InfoWindow();\n");
                sb.append("infowindow" + id + ".setContent(\'"+contentString+"\')\n");
                sb.append("google.maps.event.addListener(marker" + id + ", 'click', function() {\n" +
                       "       infowindow" + id + ".open(map,marker" + id + ");\n"+
                       "});");
                sb.append("     }\n" +
                        " });");


            }
            sb.append("} \n");

        } else {

            for (Map.Entry<Item, String> entry : locations.entrySet()) {

                String contentString = entry.getKey().getName();
                String id = entry.getKey().getIdString();

                if (item != null) {
                    contentString = getWindowHtml(item).toString();
                } else if (category != null) {
                    contentString = getWindowHtml(entry.getKey()).toString();
                }

                sb.append("var infowindow" + id + " = new google.maps.InfoWindow({\n" +
                        "    content: \"" + contentString + "\"\n" +
                        "});");

                if (getIconFilename() != null && getIconFilename().trim().length() != 0) {
                    sb.append("var image = new google.maps.MarkerImage('template-data/" + getIconFilename() + "',\n" +
//                        "      // This marker is 20 pixels wide by 32 pixels tall.\n" +
                            "      new google.maps.Size(" + getIconWidth() + ", " + getIconHeight() + "),\n" +
//                        "      // The origin for this image is 0,0.\n" +
                            "      new google.maps.Point(0,0),\n" +
//                        "      // The anchor for this image is the base of the flagpole at 0,32.\n" +
                            "      new google.maps.Point(" + getIconAnchorX() + ", " + getIconAnchorY() + "));\n");
                }

                sb.append("\n" +
                        "       var marker" + id + " = new google.maps.Marker({\n" +
                        "          position: new google.maps.LatLng(" + entry.getValue() + "),\n" +
                        "          map: map,\n" +
                        "          title: \"" + entry.getKey().getName() + "\"");
                if (getIconFilename() != null && getIconFilename().trim().length() != 0) {
                    sb.append(",\n           icon: image\n");
                }
                sb.append("       });\n" +
                        "      ");

                sb.append("google.maps.event.addListener(marker" + id + ", 'click', function() {\n" +
                        "  infowindow" + id + ".open(map,marker" + id + ");\n" +
                        "});");
            }

            sb.append("} \n");


            //todo update to a new v3
//               addMarker(item,  category, entry, sb);
        }

        sb.append("</script>");

        if (getCaption() != null) {
            sb.append("<div class='ec_gmap_caption'>" + getCaption() + "</div>");
        }
        sb.append("<div id='ec_gmap' class='ec_gmap' style=\"width: 100%; height: 500px\"></div> ");

        context.setAttribute("gmapV3", true);
        context.setCookie("activeLocation", "true");

        sb.append("\n<!-- end google map -->\n");

        return sb.toString();
    }

    private void initMap(StringBuilder sb, Map<Item, String> locations) {
        String latLng = "51.507222, -0.1275"; //london
        if (isUseLatLangAttr()) {
            for (Map.Entry<Item, String> entry : locations.entrySet()) {
                latLng = entry.getValue();
                break;
            }
        }
        sb.append("var map; var geocoder;\n");
        sb.append("function gload() {");
        sb.append("geocoder = new google.maps.Geocoder();\n" +
                "    var latlng = new google.maps.LatLng(" + latLng + ");\n" +
                "    var myOptions = {\n" +
                "      zoom: " + getZoom() + ",\n" +
                "      center: latlng,\n");


        addControls(sb);
        sb.append(",\n");
        setMapType(sb);


        sb.append("\n    };\n" +
                "    map = new google.maps.Map(document.getElementById(\"ec_gmap\"), myOptions);\n");


    }

    private void addControls(StringBuilder sb) {
        if (!isShowMapTypeControl()) {
            sb.append("    mapTypeControl: false");
        } else {
            sb.append("    mapTypeControl: true");
        }
    }

    private void setMapType(StringBuilder sb) {
        switch (getMapType()) {

            case Hybrid:
                sb.append("      mapTypeId: google.maps.MapTypeId.HYBRID");
                break;

            default:
            case Normal:
                sb.append("      mapTypeId: google.maps.MapTypeId.ROADMAP");
                break;

            case Satellite:
                sb.append("      mapTypeId: google.maps.MapTypeId.SATELLITE");
                break;

            case Terrain:
                sb.append("      mapTypeId: google.maps.MapTypeId.TERRAIN\n");
                break;
        }
    }

    @Override
    public String getName() {
        return "Google map V3";
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public final void setFooter(String footer) {
        this.footer = footer;
    }

    public final void setHeader(String header) {
        this.header = header;
    }

    public final void setIconAnchorX(int iconAnchorX) {
        this.iconAnchorX = iconAnchorX;
    }

    public final void setIconAnchorY(int iconAnchorY) {
        this.iconAnchorY = iconAnchorY;
    }

    public final void setIconFilename(String iconFilename) {
        this.iconFilename = iconFilename;
    }

    public final void setIconHeight(int iconHeight) {
        this.iconHeight = iconHeight;
    }

    public final void setIconWidth(int iconWidth) {
        this.iconWidth = iconWidth;
    }

    public final void setIconWindowAnchorX(int iconWindowAnchorX) {
        this.iconWindowAnchorX = iconWindowAnchorX;
    }

    public final void setIconWindowAnchorY(int iconWindowAnchorY) {
        this.iconWindowAnchorY = iconWindowAnchorY;
    }

    public void setLocation(String postcode) {
        this.location = postcode;
    }

    public void setMapType(MapType mapMode) {
        this.mapType = mapMode;
    }

    public final void setZoom(int zoom) {
        this.zoom = zoom;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setShowMapTypeControl(boolean showMapTypeControl) {
        this.showMapTypeControl = showMapTypeControl;
    }

    public void setUseLatLangAttr(boolean useLatLangAttr) {
        this.useLatLangAttr = useLatLangAttr;
    }

    public void setLocationAttribute(Attribute locationAttribute) {
        this.locationAttribute = locationAttribute;
    }

    private boolean hasLocationAttribute() {
        return getAttribute() != null;
    }

    public void setLatAttribute(Attribute latAttribute) {
        this.latAttribute = latAttribute;
    }

    public void setLangAttribute(Attribute langAttribute) {
        this.langAttribute = langAttribute;
    }

    private boolean hasLatLangAttribute() {
        return getLangAttribute() != null && getLatAttribute() != null;
    }

    protected class CompileLocation {
        private RequestContext context;
        private Map<Item, String> locations;
        private Item item;
        private Category category;

        public CompileLocation(RequestContext context) {
            this.context = context;
            this.item = (Item) context.getAttribute("item");
            this.category = (Category) context.getAttribute("category");
        }

        public CompileLocation(RequestContext context, Item item, Category category) {
            this.context = context;
            this.item = item;
            this.category = category;
        }

        public Map<Item, String> getLocations() {
            return locations;
        }

        public CompileLocation invoke() {
            LatLng l = null;
            List<Item> items = null;
            locations = new HashMap<Item, String>();

            if (!isUseLatLangAttr()) {
                if (location == null) {

                    /*	if (item == null) {
                                    logger.fine("[GoogleMapBlock] attempting to render on non item and no location set");
                                    return null;
                                }

                                if (!item.hasLocation()) {
                                    logger.fine("[GoogleMapBlock] item has no location, exiting");
                                    return null;
                                }

                                l = new OSRef(item.getX(), item.getY()).toLatLng();
                                l.toWGS84();
                    */
                    if (item == null && category == null) {
                        logger.fine("[GoogleMapBlock] attempting to render on non item and no location set");
                        return this;
                    }
                    if (item != null && hasLocationAttribute()) {
                        String value = item.getAttributeValue(getAttribute());
                        if (value != null) {
                            locations.put(item, value);
                        } else {
                            return this;
                        }
                    } else if (category != null) {
                        items = category.getItems(null, "LIVE", 0);
                        for (Item i : items) {
                            if (hasLocationAttribute()) {
                                String value = i.getAttributeValue(getAttribute());
                                if (value != null) {
                                    locations.put(i, i.getAttributeValue(getAttribute()));
                                }
                            }
                        }
                        if (locations.isEmpty()) {
                            return this;
                        }
                    }
                } else {

                    Pin pin = Pin.getFirst(context, location);
                    if (pin != null) {
                        l = new OSRef(pin.getX(), pin.getY()).toLatLng();
                        l.toWGS84();
                    }
                }
            } else {
                if (location == null) {

                    if (item == null && category == null) {
                        logger.fine("[GoogleMapBlock] attempting to render on non item and no location set");
                        return this;
                    }
                    if (item != null && hasLatLangAttribute()) {
                        String value = item.getAttributeValue(getLatAttribute()) + "," + item.getAttributeValue(getLangAttribute());
                        if (value != null) {
                            locations.put(item, value);
                        } else {
                            return this;
                        }
                    } else if (category != null) {
                        items = category.getItems(null, "LIVE", 0);
                        for (Item i : items) {
                            if (hasLatLangAttribute()) {
                                String value = i.getAttributeValue(getLatAttribute()) + "," + i.getAttributeValue(getLangAttribute());
                                if (value != null) {
                                    locations.put(i, value);
                                }
                            }
                        }
                        if (locations.isEmpty()) {
                            return this;
                        }
                    }
                }
            }
            return this;
        }
    }
}
    
