package org.sevensoft.ecreator.model.extras.rss;

import java.util.List;

import org.sevensoft.ecreator.iface.admin.extras.newsfeed.NewsfeedBlockHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Keypad;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.http.util.Renderer;

/**
 * @author sks 12 Jul 2006 17:15:34
 *
 */
@Table("blocks_newsfeeds")
@HandlerClass(NewsfeedBlockHandler.class)
@Label("Newsfeed")
public class NewsfeedBlock extends Block {

	private Newsfeed	newsfeed;

	/**
	 * Number of columns to show when dispalying this newsfeed
	 */
	private int		cols;

	private NewsfeedBlock(RequestContext context) {
		super(context);
	}

	public NewsfeedBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, 0);

		this.newsfeed = EntityObject.getInstance(context, Newsfeed.class, objId);
		if (newsfeed != null) {
            setName("Newsfeed: # " + getNewsfeed().getIdString() + " " + getNewsfeed().getTitle());
			save();
		}
	}

	public int getCols() {
		return cols < 1 ? 3 : cols;
	}

	public Newsfeed getNewsfeed() {
		return (newsfeed.pop());
	}

	@Override
	public Object render(RequestContext context) {

		logger.fine("[Newsfeed Block] Rendering newsfeed block newsfeed=" + newsfeed);

		newsfeed.pop();

		List<NewsfeedArticle> items = newsfeed.getArticles();
		logger.fine("[Newsfeed Block] items=" + items);

		StringBuilder sb = new StringBuilder();

		Keypad keypad = new Keypad(getCols());
		keypad.setTableClass("newsfeed");
		keypad.setTableAlign("center");
		keypad.setCellVAlign("top");
		keypad.setObjects(items);
		keypad.setRenderer(new Renderer<NewsfeedArticle>() {

			public Object render(NewsfeedArticle item) {

				StringBuilder sb = new StringBuilder();
				sb.append("<div class='headline'>" + new LinkTag(item.getLink(), item.getTitle()).setTarget("_blank") + "</div>");
				if (item.hasDate()) {
					sb.append("<div class='date'>" + item.getDate() + "</div>");
				}

                String description = item.getDescription(100);
                if (description.indexOf("<") != -1 || description.indexOf("/>") != -1) {
                    description = item.getDescription();
                }

                sb.append("<div class='description'>" + description + "</div>");

                return sb;
			}
		});

		sb.append(keypad);

		sb.append(new TableTag("newsfeed_copyright"));

		if (newsfeed.hasTitle()) {

			sb.append("<tr><td colspan='3' class='copyright'>News provider: ");

			if (newsfeed.hasLink()) {
				sb.append(new LinkTag(newsfeed.getLink(), newsfeed.getTitle()).setTarget("_blank"));
			} else {
				sb.append(newsfeed.getTitle());
			}

			if (newsfeed.hasDescription()) {
				sb.append("<br/>");
				sb.append(newsfeed.getDescription());
			}

			if (newsfeed.hasCopyright()) {
				sb.append("<br/>");
				sb.append(newsfeed.getCopyright());
			}

			sb.append("</td></tr>");
		}
        sb.append("</table>");

		return sb.toString();
	}

	public void setCols(int cols) {
		this.cols = cols;
	}

}
