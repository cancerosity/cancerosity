package org.sevensoft.ecreator.model.containers.boxes;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.ecreator.model.accounts.AccountBox;
import org.sevensoft.ecreator.model.accounts.login.boxes.LoginBox;
import org.sevensoft.ecreator.model.advertising.banners.boxes.BannerBox;
import org.sevensoft.ecreator.model.advertising.banners.boxes.BannerRotationBox;
import org.sevensoft.ecreator.model.attachments.box.UploadBox;
import org.sevensoft.ecreator.model.attributes.box.AttributeBox;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.box.CategoryBox;
import org.sevensoft.ecreator.model.containers.boxes.misc.CustomBox;
import org.sevensoft.ecreator.model.containers.boxes.misc.GoogleWebSearchBox;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.box.FormBox;
import org.sevensoft.ecreator.model.crm.forum.box.ForumSearchBox;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.template.TemplateSettings;
import org.sevensoft.ecreator.model.ecom.shopping.box.BasketBox;
import org.sevensoft.ecreator.model.extras.calendars.box.CalendarBox;
import org.sevensoft.ecreator.model.extras.languages.box.LanguageBox;
import org.sevensoft.ecreator.model.extras.polls.Poll;
import org.sevensoft.ecreator.model.extras.polls.boxes.PollBox;
import org.sevensoft.ecreator.model.extras.rss.Newsfeed;
import org.sevensoft.ecreator.model.extras.rss.boxes.NewsfeedBox;
import org.sevensoft.ecreator.model.extras.rss.export.RssExportBox;
import org.sevensoft.ecreator.model.feeds.annotations.EditHandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.interaction.buddies.box.BuddiesBox;
import org.sevensoft.ecreator.model.interaction.pm.boxes.PmBox;
import org.sevensoft.ecreator.model.interaction.views.ViewsBox;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.favourites.box.FavouritesBox;
import org.sevensoft.ecreator.model.items.highlighted.boxes.HighlightedItemsBox;
import org.sevensoft.ecreator.model.marketing.newsletter.box.NewsletterBox;
import org.sevensoft.ecreator.model.marketing.sms.box.SmsBox;
import org.sevensoft.ecreator.model.media.images.boxes.ImageBox;
import org.sevensoft.ecreator.model.misc.adwords.GoogleAdwordsBox;
import org.sevensoft.ecreator.model.misc.content.clouds.TagCloudBox;
import org.sevensoft.ecreator.model.misc.location.box.ActiveLocationBox;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.reminders.boxes.HolidayReminderBox;
import org.sevensoft.ecreator.model.search.filters.FilterBox;
import org.sevensoft.ecreator.model.search.forms.SearchForm;
import org.sevensoft.ecreator.model.search.forms.boxes.ContentSearchBox;
import org.sevensoft.ecreator.model.search.forms.boxes.ItemSearchBox;
import org.sevensoft.ecreator.model.search.wizards.Wizard;
import org.sevensoft.ecreator.model.search.wizards.WizardBox;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.model.comments.box.CommentListBox;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.db.positionable.ReorderingPositionComparator;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.http.util.Selectable;

import java.util.*;

/**
 * @author sks 27 Jun 2006 23:14:22
 */
public abstract class Box extends EntityObject implements Positionable, Selectable, Logging {

    public static Class[] classes = new Class[]{AccountBox.class, LoginBox.class, AttributeBox.class, BannerBox.class, BasketBox.class,
            BuddiesBox.class, CalendarBox.class, CategoryBox.class, ContentSearchBox.class, CustomBox.class, FavouritesBox.class, FormBox.class,
            FilterBox.class, ForumSearchBox.class, GoogleWebSearchBox.class, GoogleAdwordsBox.class, HighlightedItemsBox.class, HolidayReminderBox.class, ImageBox.class,
            ItemSearchBox.class, TagCloudBox.class, NewsletterBox.class, NewsfeedBox.class, LanguageBox.class, PollBox.class, PmBox.class,
            RssExportBox.class, SmsBox.class, ActiveLocationBox.class, ViewsBox.class, UploadBox.class, WizardBox.class, CommentListBox.class};

    private static List<Class> boxClasses;

    public static List<Box> filter(List<Box> boxes, final String panel) {

        List<Box> copy = new ArrayList(boxes);
        CollectionsUtil.filter(copy, new Predicate<Box>() {

            public boolean accept(Box e) {
                return e.getPanel().equalsIgnoreCase(panel);
            }
        });
        return copy;
    }

    /**
     * Returns all boxes across the site
     */
    public static List<Box> get(RequestContext context) {

        List<Box> boxes = new ArrayList();

        for (Class clazz : getBoxClasses(context)) {
            boxes.addAll(get(context, clazz));
        }

        Collections.sort(boxes, new ReorderingPositionComparator());
        return boxes;
    }

    static <E extends Box> List<E> get(RequestContext context, Class<E> clazz) {
        Query q = new Query(context, "select * from # order by position, id");
        q.setTable(clazz);
        return q.execute(clazz);
    }

    private static List<Class> getBoxClasses(RequestContext context) {

        if (boxClasses == null) {

            List<Class> list = new ArrayList();
            for (Class clazz : classes) {
                if (SimpleQuery.count(context, clazz) > 0) {
                    list.add(clazz);
                }
            }

            boxClasses = list;
        }

        return boxClasses;
    }

    private static int getNextPosition(RequestContext context) {

        int max = 0;
        for (Class clazz : classes) {
            int i = new Query(context, "select max(position) from #").setTable(clazz).getInt();
            if (i > max) {
                max = i;
            }
        }

        return max + 1;
    }

    public static Map<String, String> getOptions(RequestContext context) {

        Map<String, String> options = new HashMap();

        if (Module.Accounts.enabled(context)) {

            options.put(AccountBox.class.getName(), "Account status");
            options.put(LoginBox.class.getName(), "Login");

            if (Module.Buddies.enabled(context)) {
                options.put(BuddiesBox.class.getName(), "Buddies");
            }

            if (Module.PrivateMessages.enabled(context)) {
                options.put(PmBox.class.getName(), "Private messages");
            }

            if (Module.MemberViewers.enabled(context)) {
                options.put(ViewsBox.class.getName(), "Member views");
            }
        }

        if (Module.Banners.enabled(context)) {

            options.put(BannerBox.class.getName(), "Banner");
            options.put(BannerRotationBox.class.getName(), "Banner rotation");
        }

        if (Module.GoogleAdwords.enabled(context)) {
            options.put(GoogleAdwordsBox.class.getName(), "Google adwords");
        }

        if (Module.Shopping.enabled(context)) {
            options.put(BasketBox.class.getName(), "Basket summary");
        }

        options.put(ActiveLocationBox.class.getName(), "Active location");

        if (Module.Languages.enabled(context)) {
            options.put(LanguageBox.class.getName(), "Language selector");
        }

        options.put(HighlightedItemsBox.class.getName(), "Highlighted items");

        if (Module.Favourites.enabled(context)) {
            options.put(FavouritesBox.class.getName(), "Favourites");
        }

        if (Module.Calendars.enabled(context)) {
            options.put(CalendarBox.class.getName(), "Calendar");
        }

        if (Module.TagClouds.enabled(context)) {
            options.put(TagCloudBox.class.getName(), "Search phrase tag cloud");
        }

        if (Module.RssExport.enabled(context)) {
            options.put(RssExportBox.class.getName(), "Rss export");
        }

        options.put(CustomBox.class.getName(), "Custom");
        options.put(CategoryBox.class.getName(), "Categories");
        options.put(GoogleWebSearchBox.class.getName(), "Google web search");
        options.put(ImageBox.class.getName(), "Image");

        if (Module.Polls.enabled(context)) {
            for (Poll poll : Poll.get(context)) {
                options.put(PollBox.class.getName() + ";" + poll.getId(), "Poll: " + poll.getTitle());
            }
        }

        if (Module.Newsfeed.enabled(context)) {
            for (Newsfeed newsfeed : Newsfeed.get(context)) {
                options.put(NewsfeedBox.class.getName() + ";" + newsfeed.getId(), "Newsfeed: #" + newsfeed.getIdString() + " " + newsfeed.getTitle());
            }
        }

        if (Module.Newsletter.enabled(context)) {
            options.put(NewsletterBox.class.getName(), "Newsletter signup");
        }

        if (Module.Sms.enabled(context)) {
            options.put(SmsBox.class.getName(), "Sms Registration");
        }

        if (Module.Wizards.enabled(context)) {
            for (Wizard wizard : Wizard.get(context)) {
                options.put(WizardBox.class.getName() + ";" + wizard.getId(), "Wizard: " + wizard.getName());
            }
        }

        if (Module.Forms.enabled(context) || Module.Sms.enabled(context)) {
            for (Form form : Form.get(context)) {
                options.put(FormBox.class.getName() + ";" + form.getId(), "Form: " + form.getName());
            }
        }

        if (Module.HolidayReminders.enabled(context)) {
            options.put(HolidayReminderBox.class.getName(), "Holiday Reminder");
        }

        options.put(ContentSearchBox.class.getName(), "Content search");
        options.put(ItemSearchBox.class.getName(), "Item Search");
        options.put(UploadBox.class.getName(), "Upload");
        options.put(CommentListBox.class.getName(), "Comments list");

        return options;
    }

    /**
     * Css ID of this box
     */
    protected String cssId;

    /**
     * Css class name, by default by class
     */
    private String cssClass;

    /**
     * Name of this boxed, used for caption
     */
    protected String name;

    /**
     * The name of the panel that stores this box.
     * Called location for BC**
     */
    protected String location;

    /**
     * The markup used to render this box
     */
    protected Markup markup;

    /**
     *
     */
    private int position;

    /**
     * Enable advanced layout control
     */
    private boolean where;

    private boolean border;

    /**
     * Show this box on all non root categories.
     */
    private boolean displayOnAllCategories;

    private boolean displayOnAllItems;

    private boolean hideOnAllItems;

    /**
     * Show this box on basket
     */
    private boolean displayOnBasket;

    protected int displayCategoryCount;

    /**
     * Show this box on checkout pages
     */
    private boolean displayOnCheckout;

    /**
     * Show to people not logged in
     */
    private boolean displayToGuests;

    /**
     * Show this box on the root / home cat
     */
    private boolean displayOnHome;

    /**
     * Manually show / hide boxes
     */
    private boolean disabled;

    /**
     * Show this box on items inside the specified categories
     */
    private boolean displayOnItemsInCategories;

    /**
     * display when a user is logged in regardless of type
     */
    private boolean displayToAllAccounts;

    /**
     * Show this sidebox on all other pages not covered
     */
    private boolean displayOnOthers;

    private boolean who;

    protected String content;

    protected String css;

    private boolean simpleEditor;

    /**
     * Text to be shown in the caption
     */
    protected String captionText;

    /**
     *
     */
    @Default("1")
    private boolean visible;

    private boolean displayOnSearchResults;

    private int displayToItemTypeCount;

    private int displayOnItemTypeCount;

    private boolean subcategories;

    /**
     * When set to true the selected categories will be EXCLUDED and all else will be visable, rather than normally the list is inclusive.
     */
    private boolean excludeCategories;

    protected Box(RequestContext context) {
        super(context);
    }

    public Box(RequestContext context, String panel) {
        super(context);

        this.name = getClass().getAnnotation(Label.class).value();
        this.captionText = name;

        // check panel is valid
        Collection<String> panels = TemplateSettings.getInstance(context).getPanels();
        for (String p : panels) {
            if (p.equalsIgnoreCase(panel)) {
                this.location = panel;
            }
        }
        if (location == null) {
            this.location = "Right";
        }

        this.border = true;
        this.cssId = getCssIdDefault();
        this.visible = true;

        this.position = getNextPosition(context) + 1;
        save();
    }

    public final void addDisplayCategory(Category category) {
        new BoxWhere(context, this, category);
        setDisplayOnCategoryCount();
        save();
    }

    public void addDisplayOnItemType(ItemType displayOnItemType) {
        addDisplayOnItemType(displayOnItemType, false);
    }

    public final void addDisplayOnItemType(ItemType itemType, boolean searchPageItemType) {
        new BoxWhere(context, this, itemType, searchPageItemType);
    }

    public final void addDisplayToItemType(ItemType itemType) {
        new BoxWho(context, this, itemType);
    }

    /**
     * Returns true if we should display this box on a search results page
     */
    public boolean checkSearchResults(SearchForm form) {

        /*
           * if simple layout then we are showing on all pages so return true
           */
        if (!isWhere()) {
            return true;
        }

        // display if we have set to display on search results
        if (isDisplayOnSearchResults() &&
                (getDisplayOnSearchItemTypes() == null || getDisplayOnSearchItemTypes().size() == 0
                        || getDisplayOnSearchItemTypes().contains(form.getItemType()))) {
            return true;
        }

        // display this box if this box was the box that submitted the search
        if (form.hasSearchBox() && this.equals(form.getSearchBox())) {
            return true;
        }

        return false;
    }

    /**
     * Checks if we should display this box on a particular category
     */
    public final boolean checkWhere(Category category) {

        /*
           * if where is disabled then we are not using advanced layout control so return true
           */
        if (!isWhere()) {
            return true;
        }

        // if root then check for home access
        if (category.isRoot()) {
            return isDisplayOnHome();
        }

        // if all categories then obviously yes
        if (isDisplayOnAllCategories()) {
            return true;
        }

        // Check the list of manual categories
        boolean included = false;
        for (Category displayCategory : getDisplayOnCategories()) {

            if (displayCategory.equals(category)) {
                included = true;
                break;
            }

            if (isSubcategories()) {

                if (displayCategory.getChildrenDeep().contains(category)) {
                    included = true;
                    break;
                }
            }
        }

        // if we are included in manual then we want to check if we are excluding
        // or including those
        if (included) {

            if (isExcludeCategories()) {
                return false;
            } else {
                return true;
            }

        } else {

            if (isExcludeCategories()) {
                return true;
            } else {
                return false;
            }

        }
    }

    /**
     * Returns true if we should display this box on an item page
     */
    public final boolean checkWhere(Item item) {

        /*
           * if where is disabled then we are not using advanced layout control so return true
           */
        if (!isWhere()) {
            return true;
        }

        // if we are displaying on all items then yes
        if (isDisplayOnAllItems()) {
            return true;
        }

        // if we are hiding on all items then false
        if (isHideOnAllItems()) {
            return false;
        }

        // if we are displaying on items in certain categories then check if this item is in any one of our categories
        if (!isDisplayOnAllCategories()) {

            // if no categories then obviously no
            if (getDisplayCategoryCount() > 0) {

                List<Category> displayCategories = getDisplayOnCategories();
                if (isSubcategories()) {
                    List<Category> displaySubcategories = new ArrayList<Category>();
                    for (Category category : displayCategories) {
                        displaySubcategories.addAll(category.getChildrenDeep());
                    }
                    displayCategories.addAll(displaySubcategories);
                }
                List<Category> itemCategories = item.getCategories();
                return displayCategories.removeAll(itemCategories);
            }
        }

        // if we are displaying on items of certain item types only then check our item type is included
        return getDisplayOnItemTypes().contains(item.getItemType());
    }

    /**
     * @return
     */
    public final boolean checkWhereBasket() {

        /*
           * if where is disabled then we are not using advanced layout control so return true
           */
        if (!isWhere()) {
            return true;
        }

        // ... otherwise check if we are displaying on checkout pages
        return isDisplayOnBasket();
    }

    /**
     * @return
     */
    public final boolean checkWhereCheckout() {

        /*
           * if where is disabled then we are not using advanced layout control so return true
           */
        if (!isWhere()) {
            return true;
        }

        // ... otherwise check if we are displaying on checkout pages
        return isDisplayOnCheckout();
    }

    /**
     * Checks if we should display this box on a general page
     */
    public boolean checkWhereOthers() {

        /*
           * if where is disabled then we are not using advanced layout control so return true
           */
        if (!isWhere()) {
            return true;
        }

        return isDisplayOnOthers();
    }

    /**
     * Returns true if we should show this box for the current user
     */
    public final boolean checkWho(Item user) {

        /*
           * if not controlling display by who then just return true
           */
        if (!who) {
            return true;
        }

        if (user == null) {

            if (isDisplayToGuests()) {
                return true;

            } else {
                return false;
            }

        } else {

            if (isDisplayToAllAccounts()) {

                return true;

            }

            // it not all accounts then make sure we are in the list of account types
            return getDisplayToItemTypes().contains(user);
        }
    }

    @Override
    public synchronized boolean delete() {

        SimpleQuery.delete(context, BoxWhere.class, "box", getFullId());
        SimpleQuery.delete(context, BoxWho.class, "box", getFullId());

        boolean b = super.delete();
        boxClasses = null;

        return b;
    }

    public String getCaptionText() {
        return captionText;
    }

    public String getContent() {

        if (content == null) {
            resetContent();
        }

        return content;
    }

    public final String getCss() {
        return css;
    }

    public String getCssClass() {
        return cssClass == null ? "sidebar" : cssClass;
    }

    public final String getCssId() {
        return cssId == null ? getCssIdDefault() : cssId;
    }

    protected abstract String getCssIdDefault();

    public final int getDisplayCategoryCount() {
        return displayCategoryCount;
    }

    public final List<Category> getDisplayOnCategories() {

        Query q = new Query(context, "select c.* from # c join # bw on c.id=bw.category where bw.box=? order by c.fullname");
        q.setTable(Category.class);
        q.setTable(BoxWhere.class);
        q.setParameter(getFullId());
        return q.execute(Category.class);
    }

    public final int getDisplayOnItemTypeCount() {
        return displayOnItemTypeCount;
    }

    public final List<ItemType> getDisplayOnItemTypes() {

        Query q = new Query(context, "select i.* from # i join # bw on i.id=bw.itemType where bw.box=? and bw.searchPageItemType=false order by i.name");
        q.setTable(ItemType.class);
        q.setTable(BoxWhere.class);
        q.setParameter(getFullId());
        return q.execute(ItemType.class);
    }

    public final List<ItemType> getDisplayOnSearchItemTypes() {

        Query q = new Query(context, "select i.* from # i join # bw on i.id=bw.itemType where bw.box=? and bw.searchPageItemType=true order by i.name");
        q.setTable(ItemType.class);
        q.setTable(BoxWhere.class);
        q.setParameter(getFullId());
        return q.execute(ItemType.class);
    }

    public final int getDisplayToItemTypeCount() {
        return displayToItemTypeCount;
    }

    public final List<ItemType> getDisplayToItemTypes() {

        Query q = new Query(context, "select i.* from # i join # bw on i.id=bw.itemType where bw.box=? order by i.name");
        q.setTable(ItemType.class);
        q.setTable(BoxWho.class);
        q.setParameter(getFullId());
        return q.execute(ItemType.class);

    }

    /**
     *
     */
    public final Class<? extends Handler> getHandlerClass() {
        return getClass().getAnnotation(HandlerClass.class).value();
    }


    public final Class<? extends Handler> getEditHandler() {
        final EditHandlerClass annotation = getClass().getAnnotation(EditHandlerClass.class);
        return (annotation == null ? getHandlerClass() : annotation.value());
    }

    public final String getLabel() {
        return getName();
    }

    public final String getLocation() {
        return location == null ? "Left" : location;
    }

    public List<LogEntry> getLogEntries() {
        return LogEntry.get(context, this);
    }

    public String getLogId() {
        return getFullId();
    }

    public String getLogName() {
        return "Box #" + getIdString() + " " + getName();
    }

    public Markup getMarkup() {

        if (markup == null) {
            markup = initMarkup();
            if (markup != null) {
                save();
            }
        }
        return (Markup) (markup == null ? null : markup.pop());
    }

    public final String getName() {
        return name;
    }

    public final String getPanel() {
        return getLocation();
    }

    public final int getPosition() {
        return position;
    }

    public Map<Class, String> getStyles() {
        return null;
    }

    public final TableTag getTableTag() {

        if (isBorder()) {

            final TableTag tableTag = new TableTag(getCssClass()).setId(getCssId());
            if (captionText != null) {
                tableTag.setCaption(captionText);
            }
            return tableTag;

        } else {

            return new TableTag().setId(getCssId());
        }
    }

    public final String getValue() {
        return getIdString();
    }

    public final boolean hasContent() {
        return content != null;
    }

    public final boolean hasCss() {
        return css != null;
    }

    public final boolean hasDisplayCategories() {
        return displayCategoryCount > 0;
    }

    public abstract boolean hasEditableContent();

    protected boolean hasMarkup() {
        return markup != null;
    }

    protected Markup initMarkup() {
        return null;
    }

    public boolean isAdvancedEditor() {
        return !isSimpleEditor();
    }

    public final boolean isBorder() {
        return border;
    }

    public boolean isCustom() {
        return false;
    }

    public final boolean isDisplayOnAllCategories() {
        return displayOnAllCategories;
    }

    public final boolean isDisplayOnAllItems() {
        return displayOnAllItems;
    }

    public final boolean isHideOnAllItems() {
        return hideOnAllItems;
    }

    public final boolean isDisplayOnBasket() {
        return displayOnBasket;
    }

    public final boolean isDisplayOnCheckout() {
        return displayOnCheckout;
    }

    public final boolean isDisplayOnHome() {
        return displayOnHome;
    }

    public final boolean isDisplayOnItemsInCategories() {
        return displayOnItemsInCategories;
    }

    public final boolean isDisplayOnOthers() {
        return displayOnOthers;
    }

    public final boolean isDisplayOnSearchResults() {
        return displayOnSearchResults;
    }

    public final boolean isDisplayToAllAccounts() {
        return displayToAllAccounts;
    }

    public final boolean isDisplayToGuests() {
        return displayToGuests;
    }

    public final boolean isExcludeCategories() {
        return excludeCategories;
    }

    public final boolean isSimpleEditor() {
        return simpleEditor;
    }

    public final boolean isSubcategories() {
        return subcategories;
    }

    public boolean isVisible() {
        return visible;
    }

    public boolean isWhere() {
        return where;
    }

    public final boolean isWho() {
        return who;
    }

    public void log(String message) {
        new LogEntry(context, this, message);
    }

    public void log(User user, String message) {
        new LogEntry(context, this, user, message);
    }

    public final void removeDisplayOnCategories() {

        SimpleQuery.delete(context, BoxWhere.class, "box", getFullId());

        setDisplayOnCategoryCount();
    }

    public final void removeDisplayOnCategory(Category category) {

        SimpleQuery.delete(context, BoxWhere.class, "box", getFullId(), "category", category);
        setDisplayOnCategoryCount();
    }

    public final void removeDisplayOnItemType(ItemType itemType, boolean searchItemType) {
        SimpleQuery.delete(context, BoxWhere.class, "itemType", itemType, "box", getFullId(), "searchPageItemType", searchItemType);
    }

    public final void removeDisplayOnItemTypes() {
        SimpleQuery.delete(context, BoxWhere.class, "box", getFullId());
    }

    public final void removeDisplayToItemType(ItemType itemType) {
        SimpleQuery.delete(context, BoxWho.class, "itemType", itemType, "box", getFullId());
    }

    public final void removeDisplayToItemTypes() {
        SimpleQuery.delete(context, BoxWho.class, "box", getFullId());
    }

    public abstract String render(RequestContext context);

    public void replaceStyle(String from, String to) {
        if (css != null)
            css = css.replace(from, to);
    }

    protected void resetContent() {
    }

    @Override
    public synchronized void save() {
        super.save();
        boxClasses = null;
    }

    public final void setBorder(boolean border) {
        this.border = border;
    }

    public void setCaptionText(String captionText) {
        this.captionText = captionText;
    }

    public final void setContent(String content) {
        this.content = content;
    }

    public void removeCss() {
        this.css = null;
        save("css");
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public final void setCssId(String cssId) {
        this.cssId = cssId;
    }

    public final void setDisplayOnAllCategories(boolean displayOnAllCategories) {
        this.displayOnAllCategories = displayOnAllCategories;
    }

    public final void setDisplayOnAllItems(boolean displayOnAllItems) {
        this.displayOnAllItems = displayOnAllItems;
    }

    public final void setHideOnAllItems(boolean hideOnAllItems) {
        this.hideOnAllItems = hideOnAllItems;
    }

    public final void setDisplayOnBasket(boolean displayOnBasket) {
        this.displayOnBasket = displayOnBasket;
    }

    protected final void setDisplayOnCategoryCount() {
        this.displayCategoryCount = getDisplayOnCategories().size();
        save();
    }

    public final void setDisplayOnCheckout(boolean displayOnCheckout) {
        this.displayOnCheckout = displayOnCheckout;
    }

    public final void setDisplayOnHome(boolean displayOnHome) {
        this.displayOnHome = displayOnHome;
    }

    public final void setDisplayOnItemsInCategories(boolean displayOnItemsInCategories) {
        this.displayOnItemsInCategories = displayOnItemsInCategories;
    }

    protected final void setDisplayOnItemTypeCount() {
        this.displayOnItemTypeCount = getDisplayOnCategories().size();
        save();
    }

    public final void setDisplayOnOthers(boolean displayOnOthers) {
        this.displayOnOthers = displayOnOthers;
    }

    public final void setDisplayOnSearchResults(boolean displayOnSearchResults) {
        this.displayOnSearchResults = displayOnSearchResults;
    }

    public final void setDisplayToAllAccounts(boolean displayToAllAccounts) {
        this.displayToAllAccounts = displayToAllAccounts;
    }

    public final void setDisplayToGuests(boolean displayToGuests) {
        this.displayToGuests = displayToGuests;
    }

    protected final void setDisplayToItemTypeCount() {
        this.displayToItemTypeCount = getDisplayOnCategories().size();
        save();
    }

    public final void setExcludeCategories(boolean excludeCategories) {
        this.excludeCategories = excludeCategories;
    }

    public void setMarkup(Markup markup) {
        this.markup = markup;
    }

    public final void setName(String name) {
        this.name = name;
    }

    public void setObjId(int objId) {
    }

    public final void setPanel(String panel) {
        this.location = panel;
    }

    public final void setPosition(int i) {
        this.position = i;
    }

    public final void setSimpleEditor(boolean simpleEditor) {
        this.simpleEditor = simpleEditor;
    }

    public final void setSubcategories(boolean subcategories) {
        this.subcategories = subcategories;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public void setWhere(boolean b) {
        this.where = b;
    }

    public final void setWho(boolean b) {
        this.who = b;
    }

    public final void switchPanel() {

        // move box to the next panel
        List<String> panels = TemplateSettings.getInstance(context).getPanels();

        int indexOf = panels.indexOf(location);

        // if not in list then set to first one
        if (indexOf == -1) {
            this.location = panels.get(0);
        } else if (indexOf + 1 < panels.size()) {
            this.location = panels.get(indexOf + 1);
        } else {
            this.location = panels.get(0);
        }

        save();
    }

}
