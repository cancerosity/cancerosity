package org.sevensoft.ecreator.model.containers.boxes.shortcuts;

import java.util.logging.Logger;

import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.highlighted.HighlightMethod;
import org.sevensoft.ecreator.model.items.highlighted.boxes.HighlightedItemsBox;
import org.sevensoft.ecreator.model.items.highlighted.boxes.HighlightedItemsBox.Style;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Nov 2006 14:47:08
 *
 */
public abstract class AbstractHighlightedBoxShortcut extends BoxShortcut {

	private static final Logger	logger	= Logger.getLogger("ecreator");

	protected Box create(String itemTypeName, HighlightMethod method, String caption, RequestContext context, String location, String cssId) {

		logger.fine("[AbstractHighlightedBoxShortcut] itemTypeName=" + itemTypeName + ", method=" + method + ", caption=" + caption + ", location="
				+ location);

		ItemType itemType = ItemType.getByName(context, itemTypeName);
		if (itemType == null)
			return null;

		HighlightedItemsBox box = new HighlightedItemsBox(context, location);

		Search search = box.getSearch();
		search.setMethod(method);
		search.setLimit(5);
		search.setItemType(itemType);
		search.save();

		box.setStyle(Style.Static);
		box.setCaptionText(caption);
		box.setName(caption);
		box.setCssId(cssId);
		box.save();

		return box;
	}
}
