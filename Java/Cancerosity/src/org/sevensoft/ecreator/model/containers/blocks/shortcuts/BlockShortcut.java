package org.sevensoft.ecreator.model.containers.blocks.shortcuts;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 23 Oct 2006 07:53:51
 *
 */
public abstract class BlockShortcut implements Selectable {

	/**
	 *  Returns block shortcuts applicable for a category
	 */
	public static List<BlockShortcut> getCategory() {

		List<BlockShortcut> shortcuts = new ArrayList<BlockShortcut>();

		shortcuts.add(new LatestNewsTickerBlockShortcut());

		shortcuts.add(new FeaturedListingsBlockShortcut());
		shortcuts.add(new FeaturedProductsBlockShortcut());

		shortcuts.add(new LatestListingsBlockShortcut());
		shortcuts.add(new LatestProductsBlockShortcut());
		shortcuts.add(new RandomProductsBlockShortcut());
		shortcuts.add(new RandomListingsBlockShortcut());

		return shortcuts;
	}

	public String getValue() {
		return getClass().getName();
	}

	public abstract Block install(RequestContext context, BlockOwner owner);
}
