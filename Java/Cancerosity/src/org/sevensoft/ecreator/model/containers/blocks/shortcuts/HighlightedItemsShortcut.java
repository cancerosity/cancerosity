package org.sevensoft.ecreator.model.containers.blocks.shortcuts;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.highlighted.blocks.HighlightedItemsBlock;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 6 Nov 2006 11:47:20
 *
 */
public abstract class HighlightedItemsShortcut extends BlockShortcut {

	protected abstract String getItemTypeName();

	protected abstract SortType getSortType();

	public Block install(RequestContext context, BlockOwner owner) {

		// get item type
		ItemType itemType = ItemType.getByName(context, getItemTypeName());
		if (itemType == null)
			return null;

		// add block on this category
		HighlightedItemsBlock block = new HighlightedItemsBlock(context, owner, 0);
		block.setName(getLabel());
		block.save();

		Search search = block.getSearch();
		search.setLimit(9);
		search.setSortType(SortType.Random);
		search.setItemType(itemType);
		search.setSearchCategory((Category) owner);
		search.setSubcats(true);
		search.save();

		return block;
	}
}
