package org.sevensoft.ecreator.model.containers.boxes.shortcuts;

import org.sevensoft.ecreator.model.items.highlighted.HighlightMethod;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 23 Oct 2006 06:26:40
 *
 */
@Label("Featured listings")
public class FeaturedListingsBoxShortcut extends AbstractHighlightedBoxShortcut {

	public void install(RequestContext context, String location) {
		super.create("listing", HighlightMethod.Featured, "Featured listings", context, location, "highlighted_items");
	}
}
