package org.sevensoft.ecreator.model.containers.blocks.shortcuts;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.highlighted.Ticker;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 23 Oct 2006 06:19:14
 * 
 * Create a random items block for 4 random items for an  item type called Products
 *
 */
public class LatestNewsTickerBlockShortcut extends BlockShortcut {

	public String getLabel() {
		return "Latest news ticker";
	}

	@Override
	public Block install(RequestContext context, BlockOwner owner) {

		// get item type
		ItemType itemType = ItemType.getByName(context, "news");
		if (itemType == null)
			return null;

		// add block on this category
		Ticker ticker = new Ticker(context, owner, 0);
		ticker.setName(getLabel());
		ticker.save();

		Search search = ticker.getSearch();
		search.setLimit(5);
		search.setSortType(SortType.Newest);
		search.setItemType(itemType);
		search.setSearchCategory((Category) owner);
		search.setSubcats(true);
		search.save();

		return ticker;
	}

}
