package org.sevensoft.ecreator.model.containers.boxes.misc;

import org.sevensoft.ecreator.iface.admin.containers.boxes.GoogleWebSearchBoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 05-Apr-2006 10:24:58
 *
 */
@Table("boxes_googlewebsearch")
@Label("Google websearch")
@HandlerClass(GoogleWebSearchBoxHandler.class)
public class GoogleWebSearchBox extends Box {

	public GoogleWebSearchBox(RequestContext context) {
		super(context);
	}

	public GoogleWebSearchBox(RequestContext context, String location) {
		super(context, location);
	}

	@Override
	protected String getCssIdDefault() {
		return "google-search";
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	@Override
	public String render(RequestContext context) {

		return "<form method='GET' action='http://www.google.com/search'>"
				+ "<input type='hidden' name='ie' value='UTF-8'><input type='hidden' name='oe' value='UTF-8'><input type='hidden' name='safe' value='strict'>"
				+ "<table bgcolor='#FFFFFF'><tr><td><a href='http://www.google.com/search?safe=vss'>"
				+ "<img src='http://www.google.com/logos/Google_Safe.gif' border='0' ALT='Google' align='absmiddle'></a><br/>"
				+ "<input TYPE='text' name='q' size='25' maxlength='255'><br/><INPUT type='submit' name='sa' value='Google Search'>"
				+ "</td></tr></table></form>";
	}

}
