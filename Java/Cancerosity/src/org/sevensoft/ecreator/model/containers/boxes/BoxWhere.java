package org.sevensoft.ecreator.model.containers.boxes;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 09-Mar-2006 22:57:10
 *
 */
@Table("boxes_where")
public class BoxWhere extends EntityObject {

	/**
	 * Show on these categories only
	 */
	private Category	category;

	/**
	 * Show on items of this item type only 
	 */
	private ItemType	itemType;

	private String	box;

    /**
     * Check which is this itemType: item or search result page
     */
    private boolean searchPageItemType;

    public BoxWhere(RequestContext context) {
		super(context);
	}

	public BoxWhere(RequestContext context, EntityObject box, Object obj, boolean searchPageItemType) {
		super(context);

		Query q = new Query(context, "select count(*) from # where box=? and `" + obj.getClass().getSimpleName() + "`=? and searchPageItemType=?");
		q.setTable(BoxWhere.class);
		q.setParameter(box.getFullId());
		q.setParameter(obj);
        q.setParameter(searchPageItemType);

        if (q.getInt() == 0) {

			this.box = box.getFullId();

            this.searchPageItemType = searchPageItemType;

			if (obj instanceof Category)
				this.category = (Category) obj;

			else if (obj instanceof ItemType)
				this.itemType = (ItemType) obj;

			save();

		}
	}

    public BoxWhere(RequestContext context, Box box, Object obj) {
        this(context, box, obj, false);
    }
}
