package org.sevensoft.ecreator.model.containers.blocks;

import java.lang.reflect.Constructor;
import java.util.logging.Logger;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 5 Nov 2006 17:14:43
 *
 */
public class BlockUtil {

	private static Logger	logger	= Logger.getLogger("ecreator");

	public static Block addBlock(RequestContext context, BlockOwner owner, String blockString) {

		if (blockString == null)
			return null;

		logger.fine("[BlockableUtil] add block, rawdata=" + blockString);

		// split new block param into class name and id on a semi colon
		String[] string = blockString.split(";");

		// get clazz object for the type of block to add

		try {

			Class<EntityObject> clazz = (Class<EntityObject>) Class.forName(string[0]);
			int id = (string.length == 2 ? Integer.parseInt(string[1]) : 0);
			logger.fine("[BlockableUtil] block class=" + clazz + ", objId=" + id);

			Constructor c = clazz.getConstructor(RequestContext.class, BlockOwner.class, Integer.TYPE);
			Block block = (Block) c.newInstance(context, owner, id);

			return block;

		} catch (Exception e) {

			e.printStackTrace();
			logger.fine("[BlockableUtil] Exception: " + e);
			return null;

		}

	}

}
