package org.sevensoft.ecreator.model.containers.boxes.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.containers.boxes.BoxContainer;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 26 Oct 2006 15:36:34
 *
 */
public class BoxesMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		String name = params.get("name");
		if (name == null) {
			return null;
		}

		BoxContainer container = (BoxContainer) context.getAttribute("boxes_" + name.toLowerCase());
		if (container == null) {
			return null;
		}

		if (params.containsKey("hide")) {
			container.clear();
		}

		String sep = params.get("sep");
		boolean horiz = params.containsKey("horiz");

		String clazz = params.get("class");

		container.setSeparator(sep);
		container.setHorizontal(horiz);
		container.setClass(clazz);

		return container;
	}

	public Object getRegex() {
		return "boxes";
	}

}
