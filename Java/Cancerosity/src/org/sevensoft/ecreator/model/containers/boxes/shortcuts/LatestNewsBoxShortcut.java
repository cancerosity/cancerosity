package org.sevensoft.ecreator.model.containers.boxes.shortcuts;

import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.items.highlighted.HighlightMethod;
import org.sevensoft.ecreator.model.items.highlighted.boxes.HighlightedItemsBox;
import org.sevensoft.ecreator.model.items.highlighted.boxes.LatestNewsMarkup1;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 23 Oct 2006 06:26:40
 *
 */
@Label("Latest news")
public class LatestNewsBoxShortcut extends AbstractHighlightedBoxShortcut {

	public void install(RequestContext context, String location) {
		
		HighlightedItemsBox box = (HighlightedItemsBox) super.create("news", HighlightMethod.Latest, "Latest news", context, location, "news");

		Markup markup = new Markup(context, new LatestNewsMarkup1());

		box.setMarkup(markup);
		box.save();
	}

}
