package org.sevensoft.ecreator.model.containers.blocks;

import java.util.List;

/**
 * @author sks 27 Jul 2006 18:07:05
 *
 */
public interface BlockOwner {

	public List<Block> getBlocks();

	public int getId();

}
