package org.sevensoft.ecreator.model.containers.blocks;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.model.advertising.banners.blocks.BannerBlock;
import org.sevensoft.ecreator.model.advertising.banners.blocks.BannerRotationBlock;
import org.sevensoft.ecreator.model.attachments.blocks.AttachmentsBlock;
import org.sevensoft.ecreator.model.bookings.block.AvailabilityChartBlock;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.blocks.Copyable;
import org.sevensoft.ecreator.model.categories.blocks.ItemsBlock;
import org.sevensoft.ecreator.model.categories.blocks.SiblingsBlock;
import org.sevensoft.ecreator.model.categories.blocks.SubcategoriesBlock;
import org.sevensoft.ecreator.model.crm.forms.blocks.FormBlock;
import org.sevensoft.ecreator.model.crm.jobsheets.blocks.JobsheetSubmitBlock;
import org.sevensoft.ecreator.model.extras.calculators.blocks.CalculatorBlock;
import org.sevensoft.ecreator.model.extras.mapping.google.blocks.GoogleMapBlock;
import org.sevensoft.ecreator.model.extras.mapping.google.blocks.GoogleMapBlockV3;
import org.sevensoft.ecreator.model.extras.meetups.blocks.ItemRsvpsBlock;
import org.sevensoft.ecreator.model.extras.meetups.blocks.MyRsvpsBlock;
import org.sevensoft.ecreator.model.extras.polls.blocks.PollBlock;
import org.sevensoft.ecreator.model.extras.rss.NewsfeedBlock;
import org.sevensoft.ecreator.model.feeds.annotations.EditHandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.ibex.blocks.IbexBlock;
import org.sevensoft.ecreator.model.items.alternatives.AlternativesBlock;
import org.sevensoft.ecreator.model.items.highlighted.Ticker;
import org.sevensoft.ecreator.model.items.highlighted.blocks.HighlightedItemsBlock;
import org.sevensoft.ecreator.model.items.relations.blocks.RelationsBlock;
import org.sevensoft.ecreator.model.items.reviews.ReviewsBlock;
import org.sevensoft.ecreator.model.marketing.newsletter.blocks.NewsletterBlock;
import org.sevensoft.ecreator.model.marketing.sms.blocks.SmsBlock;
import org.sevensoft.ecreator.model.media.images.galleries.blocks.GalleryBlock;
import org.sevensoft.ecreator.model.media.videos.blocks.VideosBlock;
import org.sevensoft.ecreator.model.misc.adwords.GoogleAdwordsBlock;
import org.sevensoft.ecreator.model.misc.content.ContentBlock;
import org.sevensoft.ecreator.model.misc.content.clouds.TagCloudBlock;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.reminders.blocks.HolidayReminderBlock;
import org.sevensoft.ecreator.model.search.forms.blocks.SearchBlock;
import org.sevensoft.ecreator.model.search.wizards.WizardBlock;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.model.comments.blocks.CommentPostBlock;
import org.sevensoft.ecreator.model.sitemap.blocks.SitemapBlock;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

import java.util.*;

/**
 * @author sks 26 Jun 2006 23:08:50
 */
public abstract class Block extends EntityObject implements Positionable, Comparable<Block>, Logging {

    private static Set<Class> blockClasses;

    static Class[] classes;

    static {

        classes = new Class[]{AlternativesBlock.class, SiblingsBlock.class, HighlightedItemsBlock.class, HolidayReminderBlock.class, Ticker.class, CalculatorBlock.class,
                SearchBlock.class, FormBlock.class, GalleryBlock.class, NewsfeedBlock.class, SubcategoriesBlock.class, ContentBlock.class, ItemsBlock.class,
                BannerBlock.class, AvailabilityChartBlock.class, BannerRotationBlock.class, WizardBlock.class, PollBlock.class, JobsheetSubmitBlock.class,
                TagCloudBlock.class, GoogleAdwordsBlock.class, ReviewsBlock.class, AttachmentsBlock.class, GoogleMapBlock.class, RelationsBlock.class,
                VideosBlock.class, SmsBlock.class, NewsletterBlock.class, ItemRsvpsBlock.class, MyRsvpsBlock.class, CommentPostBlock.class, SitemapBlock.class,
                IbexBlock.class, GoogleMapBlockV3.class };

    }

    public String getLogName() {
        return null;
    }

    public static void clearClasses() {
        blockClasses = null;
    }

    public static boolean contains(Class clazz, List<Block> blocks) {

        for (Block block : blocks) {
            if (block.getClass().equals(clazz)) {
                return true;
            }
        }

        return false;
    }

    public static Block get(Class clazz, List<Block> blocks) {

        for (Block block : blocks) {
            if (block.getClass().equals(clazz)) {
                return block;
            }
        }

        return null;
    }

    private static <E extends Block> List<E> get(RequestContext context, Class<E> clazz, BlockOwner owner) {
        return SimpleQuery.execute(context, clazz, "owner" + owner.getClass().getSimpleName(), owner);
    }

    public static List<Block> get2(RequestContext context, BlockOwner owner) {

        List<Block> blocks = new ArrayList();
        for (Class clazz : getBlockClasses(context)) {
            blocks.addAll(Block.get(context, clazz, owner));
        }

        Collections.sort(blocks);
        logger.fine("[Block] get for owner=" + owner + ", blocks=" + blocks + ", blockClasses=" + blockClasses);
        return blocks;
    }

    private static Set<Class> getBlockClasses(RequestContext context) {

        if (blockClasses == null) {

            Set<Class> set = new HashSet<Class>();
            for (Class clazz : classes) {
                if (SimpleQuery.count(context, clazz) > 0) {
                    set.add(clazz);
                }
            }

            blockClasses = set;
        }

        return blockClasses;
    }

    public static Class[] getClasses() {
        return classes;
    }

    private static int getNextPosition(RequestContext context, BlockOwner owner) {

        int max = 0;
        for (Class clazz : classes) {
            int i = new Query(context, "select max(position) from #").setTable(clazz).getInt();
            if (i > max) {
                max = i;
            }
        }

        return max + 1;
    }

    /**
     *
     */
    protected int position;

    /**
     *
     */
    protected String cssId;

    /**
     *
     */
    protected String cssClass;

    /**
     * Only show this on the first page of a category results page
     */
    private boolean firstPageOnly;

    /**
     *
     */
    @Default("1")
    protected boolean visible;

    /**
     * Advanced control over who can see this block
     */
    private boolean who;

    /**
     * This is the category this block is owned by
     */
    private Category ownerCategory;

    /**
     * This is the item type that owns this block
     */
    private ItemType ownerItemType;

    /**
     * The item this block is on
     */
    private Item ownerItem;

    protected String name;

    /**
     * Display to all members
     */
    private boolean displayToMembers;

    /**
     * Display to guests
     */
    private boolean displayToGuests;

    public Block(RequestContext context) {
        super(context);
    }

    public Block(RequestContext context, BlockOwner owner, int objId) {

        super(context);

        if (owner instanceof Category) {
            this.ownerCategory = (Category) owner;
        } else if (owner instanceof ItemType) {
            this.ownerItemType = (ItemType) owner;
        } else {
            throw new RuntimeException();
        }

        this.position = getNextPosition(context, owner);
        this.visible = true;

        logger.fine("[Block] creating block for owner=" + owner + ", position=" + position);

        save();
    }

    @Override
    protected EntityObject clone() throws CloneNotSupportedException {

        Block clone = (Block) super.clone();

        return clone;
    }

    public Block clone(Category category) throws CloneNotSupportedException {

        Block block = (Block) super.clone();
        block.ownerCategory = category;
        block.save();

        return block;
    }

    public int compareTo(Block o) {

        if (position < o.position) {
            return -1;
        }

        if (position > o.position) {
            return 1;
        }

        return 0;

    }

    @Override
    public synchronized boolean delete() {
        boolean b = super.delete();
        blockClasses = null;
        return b;
    }

    public final String getCssClass() {
        return cssClass == null ? getDefaultName().toLowerCase().replaceAll("\\s", "_") : cssClass;
    }

    public final String getCssId() {
        return cssId;
    }

    public final String getDefaultName() {
        return getClass().getAnnotation(Label.class).value();
    }

    public final Class<BlockEditHandler> getHandler() {

        final HandlerClass annotation = getClass().getAnnotation(HandlerClass.class);
        return (Class<BlockEditHandler>) (annotation == null ? null : annotation.value());
    }

    public final Class<BlockEditHandler> getEditHandler() {

        final EditHandlerClass annotation = getClass().getAnnotation(EditHandlerClass.class);
        return (Class<BlockEditHandler>) (annotation == null ? getHandler() : annotation.value());
    }

    public Handler getHandlerInstance(RequestContext context) {

        try {

            return getHandler().getConstructor(RequestContext.class).newInstance(context);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public final String getLabel() {
        return getClass().getAnnotation(Label.class).value();
    }

    public String getName() {
        if (name == null) {
            return getDefaultName();
        } else {
            return name;
        }
    }

    public BlockOwner getOwner() {

        if (ownerCategory != null) {
            return getOwnerCategory();
        } else if (ownerItemType != null) {
            return getOwnerItemType();
        }

        return null;
    }

    public final Category getOwnerCategory() {
        return (Category) (ownerCategory == null ? null : ownerCategory.pop());
    }

    public Item getOwnerItem() {
        return (Item) (ownerItem == null ? null : ownerItem.pop());
    }

    public ItemType getOwnerItemType() {
        return (ItemType) (ownerItemType == null ? null : ownerItemType.pop());
    }

    public final int getPosition() {
        return position;
    }

    public Map<Class, String> getStyles() {
        return null;
    }

    protected final boolean hasCssId() {
        return cssId != null;
    }

    public boolean hasHandler() {
        return getHandler() != null;
    }

    public boolean hasOwnerCategory() {
        return ownerCategory != null;
    }

    public boolean hasOwnerItem() {
        return false;
    }

    public boolean hasOwnerItemType() {
        return ownerItemType != null;
    }

    public final boolean isCopyable() {
        return getClass().getAnnotation(Copyable.class) != null;
    }

    public final boolean isDisplayToGuests() {
        return displayToGuests;
    }

    public final boolean isDisplayToMembers() {
        return displayToMembers;
    }

    public boolean isEditable() {
        return getHandler() != null;
    }

    public boolean isFirstPageOnly() {
        return firstPageOnly;
    }

    public boolean isRenameable() {
        return false;
    }

    /**
     * Returns true if this block should be shown in simple mode
     */
    public boolean isSimple() {
        return true;
    }

    public boolean isVisible() {
        return visible;
    }

    public final boolean isWho() {
        return who;
    }

    public void log(String message) {
        new LogEntry(context, this, message);
    }

    public void log(User user, String message) {
        new LogEntry(context, this, message);
    }

    public abstract Object render(RequestContext context);

    @Override
    public synchronized void save() {
        super.save();
        clearClasses();
    }

    public final void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public final void setCssId(String cssId) {
        this.cssId = cssId;
    }

    public final void setDisplayToGuests(boolean displayToGuests) {
        this.displayToGuests = displayToGuests;
    }

    public final void setDisplayToMembers(boolean displayToMembers) {
        this.displayToMembers = displayToMembers;
    }

    public void setFirstPageOnly(boolean firstPageOnly) {
        this.firstPageOnly = firstPageOnly;
    }

    public void setName(String name) {
        this.name = name;
    }

    public final void setPosition(int position) {
        this.position = position;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public final void setWho(boolean who) {
        this.who = who;
    }
}
