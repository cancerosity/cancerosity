package org.sevensoft.ecreator.model.containers.boxes.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 05-Apr-2006 10:09:31
 *
 */
public class BoxesRightMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {
		return context.getAttribute("boxes_right");
	}

	public Object getRegex() {
		return "boxes_right";
	}

}
