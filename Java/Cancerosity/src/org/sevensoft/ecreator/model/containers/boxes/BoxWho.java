package org.sevensoft.ecreator.model.containers.boxes;

import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 5 Dec 2006 13:56:01
 *
 */
@Table("boxes_who")
public class BoxWho extends EntityObject {

	/**
	 * Show this box to accounts of this item type only
	 * BC
	 */
	private ItemType	itemType;

	/**
	 * Full owner id of the box
	 */
	private String	box;

	protected BoxWho(RequestContext context) {
		super(context);
	}

	public BoxWho(RequestContext context, Box box, ItemType it) {
		super(context);

		if (SimpleQuery.count(context, BoxWho.class, "box", box.getFullId(), "itemType", it) == 0) {

			this.box = box.getFullId();
			this.itemType = it;
			save();

		}

	}

	public final String getBox() {
		return box;
	}

	public final ItemType getItemType() {
		return itemType.pop();
	}

}
