package org.sevensoft.ecreator.model.containers.boxes.misc;

import java.util.List;
import java.io.IOException;
import java.io.File;
import java.net.URL;

import org.sevensoft.ecreator.iface.admin.containers.boxes.CustomBoxHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.design.marker.MarkerRenderer;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.attachments.AttachmentOwner;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.AttachmentUtil;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 16 Jul 2006 12:40:59
 *
 */
@Table("boxes_custom")
@Label("Custom box")
@HandlerClass(CustomBoxHandler.class)
public class CustomBox extends Box implements AttachmentOwner {

	public static List<CustomBox> getCustom(RequestContext context) {
		return SimpleQuery.execute(context, CustomBox.class);
	}

	private boolean	renderTags;
    private int attachmentCount;

	public CustomBox(RequestContext context) {
		super(context);
	}

	public CustomBox(RequestContext context, String location) {
		super(context, location);
		this.cssId = "custom" + getId();
		save();
	}

	@Override
	protected String getCssIdDefault() {
		return "custom";
	}

	@Override
	public boolean hasEditableContent() {
		return true;
	}

	@Override
	public boolean isCustom() {
		return true;
	}

	public boolean isRenderTags() {
		return renderTags;
	}

	@Override
	public String render(RequestContext context) {

		if (content == null) {
			return null;
		}

		Item item = (Item) context.getAttribute("item");
		Category category = (Category) context.getAttribute("category");

		String rendered;
		if (item != null) {
			rendered = MarkerRenderer.render(context, content, item);
		}

		else if (category != null) {
			rendered = MarkerRenderer.render(context, content, category);
		}

		else {
			rendered = MarkerRenderer.render(context, content);
		}

		StringBuilder sb = new StringBuilder();

		sb.append(getTableTag());

		sb.append("<tr><td>");
		sb.append(rendered);
		sb.append("</td></tr>");

		sb.append("<tr><td class='bottom'>&nbsp;</td></tr>");
		sb.append("</table>");

		return sb.toString();
	}

	public void setRenderTags(boolean renderTags) {
		this.renderTags = renderTags;
	}

	public void wordReplace(String from, String to) {

		if (content != null) {
			content = content.replace(from, to);
		}

		if (name != null) {
			name = name.replace(from, to);
		}

		if (captionText != null) {
			captionText = captionText.replace(from, to);
		}
	}

    public boolean acceptedFiletype(String filename) {
        return true;
    }

    public Attachment addAttachment(Attachment attachment) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException {
        return new Attachment(context, this, attachment);
    }

    public Attachment addAttachment(File file, String fileName) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException {
        return null;
    }

    public Attachment addAttachment(Upload upload, boolean sample) throws IOException, AttachmentLimitException, AttachmentExistsException, AttachmentLimitException, AttachmentTypeException {
        return new Attachment(context, this, upload, false);
    }

    public Attachment addAttachment(URL url) throws IOException, AttachmentTypeException, AttachmentExistsException, AttachmentLimitException {
        return null;
    }

    public void copyAttachmentsTo(AttachmentOwner target) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException {
        AttachmentUtil.copyAttachmentsTo(this, target, context);
    }

    public int getAttachmentCount() {
        return attachmentCount;
    }

    public int getAttachmentLimit() {
        return 0;
    }

    public List<Attachment> getAttachments() {
        return AttachmentUtil.getAttachments(context, this);
    }

    public boolean hasAttachments() {
        return attachmentCount > 0;
    }

    public boolean isAtAttachmentLimit() {
        return false;
    }

    public void moveAttachmentsTo(AttachmentOwner target) throws AttachmentExistsException, AttachmentTypeException, IOException, AttachmentLimitException {
        AttachmentUtil.moveAttachmentsTo(context, this, target);
    }

    public void removeAttachment(Attachment a) {
        AttachmentUtil.removeAttachment(this, a);
    }

    public void removeAttachments() {
        AttachmentUtil.removeAttachments(this);
    }

    public void setAttachmentCount() {
        this.attachmentCount = getAttachments().size();
		save("attachmentCount");
    }
}
