package org.sevensoft.ecreator.model.containers.blocks.shortcuts;

import org.sevensoft.ecreator.model.items.sorts.SortType;

/**
 * @author sks 23 Oct 2006 06:19:14
 * 
 *
 *
 */
public class FeaturedProductsBlockShortcut extends HighlightedItemsShortcut {

	@Override
	protected String getItemTypeName() {
		return "product";
	}

	public String getLabel() {
		return "Featured products";
	}

	@Override
	protected SortType getSortType() {
		return SortType.Newest;
	}

}
