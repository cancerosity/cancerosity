package org.sevensoft.ecreator.model.containers.boxes.shortcuts;

import org.sevensoft.ecreator.model.items.highlighted.HighlightMethod;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 23 Oct 2006 06:26:40
 *
 */
@Label("Latest listings")
public class LatestListingsBoxShortcut extends AbstractHighlightedBoxShortcut {

    public void install(RequestContext context, String location) {
        String moduleName = "listing";
        if (ItemType.getListings(context) != null && !ItemType.getListings(context).isEmpty()) {
            moduleName = ItemType.getListings(context).get(0).getName();
        }

        super.create(moduleName, HighlightMethod.Latest, "Latest listings", context, location, "highlighted_items");
    }
}
