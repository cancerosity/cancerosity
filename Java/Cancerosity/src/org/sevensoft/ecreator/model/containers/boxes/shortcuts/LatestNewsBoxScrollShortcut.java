package org.sevensoft.ecreator.model.containers.boxes.shortcuts;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.ecreator.model.items.highlighted.boxes.HighlightedItemsBox;
import org.sevensoft.ecreator.model.items.highlighted.boxes.LatestNewsMarkup2;
import org.sevensoft.ecreator.model.items.highlighted.HighlightMethod;
import org.sevensoft.ecreator.model.design.markup.Markup;

/**
 * User: Tanya
 * Date: 02.11.2010
 */
@Label("Latest scroller news ")
public class LatestNewsBoxScrollShortcut extends AbstractHighlightedBoxShortcut {

    public void install(RequestContext context, String location) {

        HighlightedItemsBox box = (HighlightedItemsBox) super.create("news", HighlightMethod.Latest, "Latest scroller news", context, location, "scroller_news");

        box.setStyle(HighlightedItemsBox.Style.Scroller);

        Markup markup = new Markup(context, new LatestNewsMarkup2());

//        markup = markup.updateFrorScrollerBox(box);

        box.setMarkup(markup);

        box.save();
    }

}
