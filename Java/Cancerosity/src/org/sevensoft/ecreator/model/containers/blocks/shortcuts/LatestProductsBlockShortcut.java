package org.sevensoft.ecreator.model.containers.blocks.shortcuts;

import org.sevensoft.ecreator.model.items.sorts.SortType;

/**
 * @author sks 23 Oct 2006 06:19:14
 * 
 * Create a random items block for 4 random items for an  item type called Products
 *
 */
public class LatestProductsBlockShortcut extends HighlightedItemsShortcut {

	@Override
	protected String getItemTypeName() {
		return "product";
	}

	public String getLabel() {
		return "Latest products";
	}

	@Override
	protected SortType getSortType() {
		return SortType.Newest;
	}

}
