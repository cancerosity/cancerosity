package org.sevensoft.ecreator.model.containers.boxes;

import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.search.forms.SearchForm;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 05-Jan-2006 08:22:55
 * 
 */
public class BoxContainer {

	private final static Logger	logger	= Logger.getLogger("ecreator");

	private List<Box>			boxes;
	private final RequestContext	context;
	private boolean			checkout;

	private final String		panel;
	private String			separator;
	private boolean			horiz;
	private String			clazz;

	private boolean			basket;

	public BoxContainer(RequestContext context, List<Box> boxes, String panel) {

		this.context = context;
		this.panel = panel;
		this.boxes = Box.filter(boxes, panel);

		/*
		 * Filter out disabled boxes
		 */
		CollectionsUtil.filter(this.boxes, new Predicate<Box>() {

			public boolean accept(Box e) {
				return e.isVisible();
			}
		});
	}

	/**
	 * Clear all boxes from this container
	 */
	public void clear() {
		boxes.clear();
	}

	public final void setBasket(boolean basket) {
		this.basket = basket;
	}

	public final void setCheckout(boolean checkout) {
		this.checkout = checkout;
	}

	public void setClass(String clazz) {
		this.clazz = clazz;
	}

	public void setHorizontal(boolean horiz) {
		this.horiz = horiz;
	}

	public void setSeparator(String sep) {
		this.separator = sep;
	}

	public String toString() {

		long timestamp = System.currentTimeMillis();

		logger.fine("[BoxContainer] start, boxes=" + boxes);
		Item account = (Item) context.getAttribute("account");
		logger.fine("[BoxContainer] account=" + account);

		if (boxes.isEmpty()) {
			return "";
		}

		Item item = (Item) context.getAttribute("item");
		Category category = (Category) context.getAttribute("category");
		SearchForm searchForm = (SearchForm) context.getAttribute("searchForm");

		StringBuilder sb = new StringBuilder();

		if (horiz) {
			if (clazz == null) {
				clazz = "ec_boxes";
			}
			sb.append(new TableTag(clazz));
			sb.append("<tr>");
		}

		Iterator<Box> iter = boxes.iterator();
		while (iter.hasNext()) {

			Box box = iter.next();

			/*
			 * Check for member display first. 
			 */
			boolean who = box.checkWho(account);
			boolean where = false;

			if (category != null) {
				where = box.checkWhere(category);

			} else if (item != null) {
				where = box.checkWhere(item);

			} else if (checkout) {
				where = box.checkWhereCheckout();

			} else if (basket) {
				where = box.checkWhereBasket();

			} else if (searchForm != null) {
				where = box.checkSearchResults(searchForm);

			} else {
				where = box.checkWhereOthers();
			}

			if (where && who) {

				String content = box.render(context);
				String css = box.getCss();

				if (content != null) {

					if (css != null) {
						sb.append("<style>");
						sb.append(css);
						sb.append("</style>");
					}

					if (horiz) {
						sb.append("<td>");
					}

					sb.append(content);

					if (horiz) {
						sb.append("</td>");
					}
				}
			}

			if (iter.hasNext() && separator != null)
				sb.append(separator);
		}

		if (horiz) {
			sb.append("</tr></table>");
		}

		logger.fine("[BoxContainer] end, time=" + (System.currentTimeMillis() - timestamp));

		return sb.toString();
	}
}
