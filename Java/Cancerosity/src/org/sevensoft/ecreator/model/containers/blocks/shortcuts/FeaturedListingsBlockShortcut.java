package org.sevensoft.ecreator.model.containers.blocks.shortcuts;

import org.sevensoft.ecreator.model.items.sorts.SortType;

/**
 * @author sks 23 Oct 2006 06:19:14
 * 
 * 
 *
 */
public class FeaturedListingsBlockShortcut extends HighlightedItemsShortcut {

	@Override
	protected String getItemTypeName() {
		return "listing";
	}

	public String getLabel() {
		return "Featured listings";
	}

	@Override
	protected SortType getSortType() {
		return SortType.Newest;
	}

}
