package org.sevensoft.ecreator.model.containers.boxes.shortcuts;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Nov 2006 15:00:11
 *
 */
public abstract class BoxShortcut {

	public static List<Class> getShortcutClasses() {

		List<Class> classes = new ArrayList();

		classes.add(FeaturedListingsBoxShortcut.class);
		classes.add(FeaturedProductsBoxShortcut.class);
		classes.add(LatestListingsBoxShortcut.class);
		classes.add(LatestProductsBoxShortcut.class);
		classes.add(LatestNewsBoxShortcut.class);
        classes.add(LatestNewsBoxScrollShortcut.class);

		return classes;
	}

	public abstract void install(RequestContext context, String location);
}
