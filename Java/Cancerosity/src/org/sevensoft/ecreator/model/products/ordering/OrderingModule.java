package org.sevensoft.ecreator.model.products.ordering;

import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Jan 2007 19:16:15
 *
 */
@Table("ordering_module")
public class OrderingModule extends EntityObject {

	public enum OrderingMethod {
		Basket, InstantBuy
	}

	public static OrderingModule get(RequestContext context, ItemType itemType) {
		OrderingModule module = SimpleQuery.get(context, OrderingModule.class, "itemType", itemType);
		return module == null ? new OrderingModule(context, itemType) : module;
	}

	private OrderingMethod	orderingMethod;
	private ItemType		itemType;

	private OrderingModule(RequestContext context) {
		super(context);
	}

	public OrderingModule(RequestContext context, ItemType itemType) {
		super(context);
		this.itemType = itemType;
		save();
	}

	public final OrderingMethod getOrderingMethod() {
		return orderingMethod == null ? OrderingMethod.Basket : orderingMethod;
	}

	public final void setOrderingMethod(OrderingMethod method) {
		this.orderingMethod = method;
	}

	/**
	 * 
	 */
	public boolean isInstantBuy() {
		return getOrderingMethod() == OrderingMethod.InstantBuy;
	}

}
