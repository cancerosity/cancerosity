package org.sevensoft.ecreator.model.products.accessories.marker;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;

/**
 * @author sks 20 Aug 2006 18:04:46
 *
 */
public class AccessoriesCheckMarker implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		// get item we are in the context for
		Item contextItem = (Item) context.getAttribute("item");
		if (contextItem == null)
			return null;

		if (!ItemModule.Ordering.enabled(context, contextItem))
			return null;

		return new CheckTag(null, "accessories", item, false);
	}

	public Object getRegex() {
		return "accessories_check";
	}

}
