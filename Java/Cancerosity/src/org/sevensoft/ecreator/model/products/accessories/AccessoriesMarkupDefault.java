package org.sevensoft.ecreator.model.products.accessories;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * @author sks 16-Mar-2006 21:05:10
 *
 */
public class AccessoriesMarkupDefault implements MarkupDefault {

	public String getBetween() {
		return null;
	}

	public String getCss() {
		return null;
	}

	public String getBody() {

		StringBuilder sb = new StringBuilder();
//		sb.append("<div class='accessory'>[accessories_check] [item?link=1] [pricing_sell?inc=1]</div>");
        sb.append("<tr><td class='image'>[thumbnail?w=50&h=50&limit=1]</td><td>[accessories_check]</td><td class='detail'>[item?link=1]</td><td class='detail'><strong>[pricing_sell?inc=1]</strong></td></tr>\n");
        return sb.toString();
	}

	public String getEnd() {
		return "</table>\n";
	}

	public String getName() {
		return "Accessories markup";
	}

	public String getStart() {
        StringBuilder sb = new StringBuilder();
        sb.append("<div class='item_details'><span class='desc'>(Tick each accessory before clicking Add to Basket to also purchase these products)</span>Accessories:</div>\n");
        sb.append("<table class='item_accessories' cellspacing='0' cellpadding='0'>\n");
        return sb.toString();
//		return "[accessories_caption]";

	}

	public int getTds() {
		return 0;
	}

}
