package org.sevensoft.ecreator.model.products.accessories.marker;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 21 Apr 2006 11:31:20
 * 
 * This tag will include the accessories
 *
 */
public class AccessoriesMarker implements IItemMarker {

	private static Logger	logger	= Logger.getLogger("tags");

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		List<Item> accessories = item.getAccessories();
		logger.fine("[accessories] accessories=" + accessories);

		if (accessories.isEmpty())
			return null;

		final Item active = (Item) context.getAttribute("account");

		/*
		 * Filter out non orderable accessories
		 */
		CollectionsUtil.filter(accessories, new Predicate<Item>() {

			public boolean accept(Item e) {
				return e.isOrderable(active);
			}
		});
		logger.fine("[accessories] filtered non orderable, " + accessories.size() + " accessories remaining");
		if (accessories.isEmpty())
			return null;

		if (params.containsKey("limit")) {
			int limit = Integer.parseInt(params.get("limit").trim());
			if (limit > accessories.size())
				accessories = accessories.subList(0, limit);
		}

		MarkupRenderer r = new MarkupRenderer(context, item.getItemType().getAccessoriesMarkup());
		r.setBodyObjects(accessories);
		return r;
	}

	public Object getRegex() {
		return "accessories";
	}
}
