package org.sevensoft.ecreator.model.products.accessories;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 12-Sep-2005 00:04:28
 * 
 */
@Table("items_accessories")
public class Accessory extends EntityObject {

	private Item	item;
	private Item	accessory;

	public Accessory(RequestContext context) {
		super(context);
	}

	public Accessory(RequestContext context, Item item, Item accessory) {
		super(context);

		if (!item.getAccessories().contains(accessory)) {

			this.item = item;
			this.accessory = accessory;

			save();
		}

	}
}
