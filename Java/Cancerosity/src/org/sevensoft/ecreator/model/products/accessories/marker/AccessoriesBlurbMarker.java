package org.sevensoft.ecreator.model.products.accessories.marker;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Oct 2006 12:01:17
 *
 */
public class AccessoriesBlurbMarker implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
		return item.getItemType().getAccessoriesBlurb();
	}

	public Object getRegex() {
		return "accessories_blurb";
	}

}
