package org.sevensoft.ecreator.model.ecom.payments.cards;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardHolderException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardTypeException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CscException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiredCardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiryDateException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.IssueNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.StartDateException;
import org.sevensoft.ecreator.model.ecom.payments.processors.Processor;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorException;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 26-Oct-2005 10:54:59
 * 
 */
@Table("payments_cards")
public class Card extends EntityObject implements Selectable {

	public static List<String>	expiryYears;
	public static List<String>	months	= Arrays.asList("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
	public static List<String>	startYears;

	static {

		startYears = new ArrayList<String>();
		int start = new Date().getYear();
		for (int n = start - 6; n <= start; n++)
			startYears.add(String.valueOf(n));

		expiryYears = new ArrayList<String>();
		for (int n = start; n < (start + 18); n++)
			expiryYears.add(String.valueOf(n));

	}

	public static void addTests(RequestContext context, String expiryMonth, String expiryYear) {

		context.test(new LengthValidator(5), "cardHolder");
		context.test(new LengthValidator(16), "cardNumber");
		context.test(new RequiredValidator(), "cardType");
		context.test(new LengthValidator(3, 3), "csc");

		try {

			Date date = new Date("01." + expiryMonth + "." + expiryYear, "dd.MM.yyyy");
			if (date.isPast()) {
				context.setError("expiryDate", "Expiry date is past");
			}

		} catch (ParseException e1) {
			context.setError("expiryDate", "Invalid date");
		}
	}
	private boolean	active;
	private CardType	cardType;
	private DateTime	date;

	private String	ipAddress, csc, cardHolder, issueNumber, cardNumber, cardIssuer;

	/**
	 * In the format MMYYYY
	 */
	private String	startDate;

	/*
	 * In the format MMYYYY
	 */
	private String	expiryDate;

	private Item	account;

	public Card(RequestContext context) {
		super(context);
	}

	public Card(RequestContext context, Item account, String ipAddress, CardType cardType, String cardHolder, String cardNumber, String expiryDate,
			String startDate, String issueNumber, String cscNumber, String cardIssuer) throws IssueNumberException, ExpiryDateException,
			ExpiredCardException, CardNumberException, IOException, CardTypeException, StartDateException, CardHolderException, CscException,
			CardException, ProcessorException {

		super(context);

		this.active = true;
		this.date = new DateTime();

		this.account = account;
		this.ipAddress = ipAddress;

		// ----------- card number ---------- //
		if (cardNumber == null)
			throw new CardNumberException();

		// strip any non digits out
		cardNumber = cardNumber.replaceAll("\\D", "");

		if (cardNumber.length() < 16)
			throw new CardNumberException("Card number is too short");

		this.cardNumber = cardNumber;

		// ---------- card type --------- //
		if (cardType == null)
			throw new CardTypeException("No card type selected");

		this.cardType = cardType;

		// ----------- card issuer --------- //
		if (cardIssuer != null) {
			this.cardIssuer = cardIssuer.trim();
		}

		// ------------ card holder ----------- //
		if (cardHolder == null) {
			throw new CardHolderException("Enter the name of the card holder");
		}

		this.cardHolder = cardHolder.trim();

		// ----------- expiry date --------- //

		// expiry date should be in the format mmyy
		if (expiryDate == null)
			throw new ExpiryDateException("Enter expiry date");

		expiryDate = expiryDate.replaceAll("\\D", "");
		if (expiryDate.length() != 6)
			throw new ExpiryDateException("Invalid expiry date");

		this.expiryDate = expiryDate;

		// ---------- start date ----------- //

		// start date should be either blank or in the format mmyy
		if (startDate != null) {

			startDate = startDate.replaceAll("\\D", "");
			if (startDate.length() != 6)
				throw new StartDateException("Invalid start date");

			this.startDate = startDate;
		}

		// -------- csc number --------------- //

		// csc should be 3 digits
		if (cscNumber == null)
			throw new CscException("Enter card security code");

		cscNumber = cscNumber.replaceAll("\\D", "");
		if (cscNumber.length() != 3)
			throw new CscException("Invalid card security code");

		this.csc = cscNumber;

		// -------- issue number --------- //

		// issue number should be empty or one or two digits
		if (issueNumber != null) {

			issueNumber = issueNumber.replaceAll("\\D", "");
			if (issueNumber.length() < 1 || issueNumber.length() > 2)
				throw new IssueNumberException("Invalid issue number");

			this.issueNumber = issueNumber;
		}

		PaymentSettings paymentSettings = PaymentSettings.getInstance(context);

		// if immediate transact is on then try to verify card details
		if (paymentSettings.isVerifyCard()) {

			Processor processor = Processor.get(context);
			if (processor != null) {
				processor.verify(this);
			}
		}

		//		 if confirming card entry then verify card
		//		 if (PaymentSetup.getInstance().()) {
		//		
		//		 Server server = (Server) PaymentSetup.getInstance().getProcessor();
		//		 if (server == null)
		//		 throw new RuntimeException("No processor configured.");
		//		
		//		 try {
		//		 server.verifyCard(this);
		//		 } catch (CscIncorrectException e) {
		//		 if (!overrideCsc)
		//		 throw e;
		//		 }
		//		 }

		// if (PaymentSetup.getInstance().hasMaxMindLicense())
		// try {
		// doMaxmindLookup();
		// } catch (Exception e) {
		// e.printStackTrace();
		// }

		// try {
		// ipCountry = new MaxMindIpLookup().getCountry(ipAddress);
		// } catch (IOException e1) {
		// e1.printStackTrace();
		// } catch (MaxMindException e1) {
		// e1.printStackTrace();
		// }
		save();
	}

	// /**
	// * @throws MaxMindException
	// * @throws IOException
	// */
	// public void doMaxmindLookup() throws IOException, MaxMindException {
	//
	// MaxMindCardLookup max = new MaxMindCardLookup(this, ipAddress);
	// cardIssuerLookup = max.getIssuingBankName();
	// country = max.getIssuingBankCountry();
	//
	// if (country != null && !country.equals(Country.UK) && !country.equals(Country.IRELAND)) {
	//
	// try {
	// Email e = new Email();
	// e.setSubject("Fraud warning - Credit Card from " + country.getName());
	// e.setBody("http://www.tekheads.co.uk/a/sales.customers.ledger?customer=" + member.getId());
	//
	// e.setFrom("server@tekheads.co.uk", "server@tekheads.co.uk");
	// e.addRecipientTo("steven@tekheads.co.uk");
	// e.addRecipientTo("sks@tekheads.co.uk");
	// e.send(Site.getInstance().getSmtpHostname());
	// } catch (InvalidAddressException e) {
	// e.printStackTrace();
	// } catch (SmtpServerException e) {
	// e.printStackTrace();
	// } catch (SmtpServerAuthenticationException e) {
	// e.printStackTrace();
	// }
	// }
	//
	// save();
	// }

	/*
	 * @see org.sevensoft.commons.jpf.EntityObject#delete()
	 */
	@Override
	public synchronized boolean delete() {
		// super.delete();
		this.active = false;
		save();

		return true;
	}

	public Item getAccount() {
		return (Item) (account == null ? null : account.pop());
	}

	/**
	 * @return Returns the cardHolder.
	 */
	public String getCardHolder() {
		return cardHolder;
	}

	public String getCardIssuer() {
		return cardIssuer;
	}

	/**
	 * @return Returns the cardNumber.
	 */
	public String getCardNumber() {
		return cardNumber;

		// try {
		//			
		// byte[] dec = new
		// sun.misc.BASE64Decoder().decodeBuffer(cardNumber);
		// byte[] utf8 = dcipher.doFinal(dec);
		// return new String(utf8);
		//			
		// } catch (IllegalStateException e) {
		// throw new RuntimeException(e);
		//			
		// } catch (IllegalBlockSizeException e) {
		// throw new RuntimeException(e);
		//			
		// } catch (BadPaddingException e) {
		// throw new RuntimeException(e);
		//			
		// } catch (IOException e) {
		// throw new RuntimeException(e);
		// }
	}

	public String getCardNumberSecure() {

		StringBuilder sb = new StringBuilder(cardNumber);
		sb.replace(4, cardNumber.length() - 4, "********");
		return sb.toString();
	}

	public CardType getCardType() {
		return cardType;
	}

	/**
	 * @return Returns the cscNumber.
	 */
	public String getCscNumber() {
		return csc;
	}

	public DateTime getDate() {
		return date;
	}

	/**
	 * @param startDate2
	 * @return
	 */
	private String getDateShort(String date) {

		if (date == null) {
			return null;
		}
		if (date.length() == 4) {
			return date;
		}

		if (date.length() == 6) {
			return date.substring(0, 2) + date.substring(4, 6);
		}

		return null;
	}

	/**
	 * @return Returns the expiryDate. in a 6 digit format MMYYYY
	 */
	public String getExpiryDate() {
		return expiryDate;
	}

	public String getExpiryDateFormatted() {
		return expiryDate.substring(0, 2) + "/" + this.expiryDate.substring(2);
	}

	/**
	 * @return
	 */
	public String getExpiryDateShort() {
		return getDateShort(expiryDate);
	}

	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @return Returns the issueNumber.
	 */
	public String getIssueNumber() {
		return issueNumber;
	}

	public String getLabel() {
		return getSecureDescription();
	}

	private String getLabel(boolean secure, String sep) {

		StringBuilder sb = new StringBuilder();

		sb.append(getCardHolder());
		sb.append(sep);

		sb.append(getCardType());
		sb.append(" ");
		if (secure) {
			sb.append(getCardNumberSecure());
		} else {
			sb.append(getCardNumber());
		}
		sb.append(sep);

		sb.append("Expires: ");
		sb.append(getExpiryDateFormatted());

		if (hasStartDate()) {
            sb.append(sep);
			sb.append("Starts: ");
			sb.append(getStartDateFormatted());
		}

		if (hasIssue()) {
            sb.append(sep);
			sb.append("Issue: ");
			sb.append(getIssueNumber());
		}

        if (hasCscNumber()) {
			sb.append(sep);
            sb.append("Card Security Code: ");
            sb.append(getCscNumber());
        }

		return sb.toString();
	}

	/**
	 * Returns the card details as a label showing full details including card number.
	 * 
	 * @return
	 */
	public String getLabel(String sep) {
		return getLabel(false, sep);
	}

	/**
	 * Returns a string label of the card details where the card number has been partially blocked for security.
	 * 
	 */
	public String getLabelSecure(String sep) {
		return getLabel(true, sep);
	}

	/**
	 * Simple one line description with secure card number
	 * 
	 * @return
	 */
	public String getSecureDescription() {
		return getCardType() + " " + getCardNumberSecure() + " Expires " + getExpiryDateFormatted();
	}

	/**
	 * @return Returns the startDate in a 6 digit format MM/YYYY
	 */
	public String getStartDate() {
		return startDate;
	}

	public String getStartDateFormatted() {
		if (startDate == null) {
			return null;
		}
		return startDate.substring(0, 2) + "/" + startDate.substring(2);
	}

	public String getStartDateShort() {
		return getDateShort(startDate);
	}

	public String getValue() {
		return getIdString();
	}

	public boolean hasCardIssuer() {
		return cardIssuer != null;
	}

	public boolean hasIssue() {
		return issueNumber != null;
	}

	public boolean hasStartDate() {
		return startDate != null;
	}

    public boolean hasCscNumber() {
		return csc != null;
	}

	public boolean isActive() {
		return active;
	}

	/**
	 * 
	 */
	public void setAccount(Item account) {
		this.account = account;
	}

    public final void reset() {
        csc = null;
        issueNumber = null;
        startDate = null;
        save();
    }
}
