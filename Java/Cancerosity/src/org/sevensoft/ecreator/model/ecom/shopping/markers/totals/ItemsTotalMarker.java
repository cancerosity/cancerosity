package org.sevensoft.ecreator.model.ecom.shopping.markers.totals;

import java.util.Map;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketLineMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderMarker;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Jun 2006 11:46:17
 * 
 * Returns subtotal of just the items bought
 *
 */
public class ItemsTotalMarker implements IBasketMarker, IOrderMarker, IBasketLineMarker {

	public Object generate(RequestContext context, Map<String, String> params, Basket basket) {

		Money total;
		if (params.containsKey("ex")) {
			total = basket.getLinesEx();
		} else {
			total = basket.getLinesInc();
		}

		Currency currency = (Currency) context.getAttribute("currency");
		return currency.getSymbol() + currency.convert(total).toString(2);
	}

	public Object generate(RequestContext context, Map<String, String> params, BasketLine line) {

		Money total;
		if (params.containsKey("ex")) {
			total = line.getLineSalePriceEx();
		} else {
			total = line.getLineSalePriceInc();
		}

		Currency currency = (Currency) context.getAttribute("currency");
		return currency.getSymbol() + currency.convert(total).toString(2);
	}

	public Object generate(RequestContext context, Map<String, String> params, Order order) {

		Money total;
		if (params.containsKey("ex")) {
			total = order.getItemsTotal();
		} else {
			total = order.getItemsTotal();
		}

		return (String) context.getAttribute("currencySymbol") + total;
	}

	public Object getRegex() {
		return "items_total";
	}

}
