package org.sevensoft.ecreator.model.ecom.payments.results;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.location.LocationUtil;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 21 May 2006 08:11:43
 *
 */
@Table("payments_avs")
public class AvsCheck extends EntityObject {

	public enum AvsResult {
		Fail, Match, NotSupported, NotChecked;
	}

	/**
	 * Returns true if this member has had a Match for this postcode
	 */
	public static boolean hasMatch(RequestContext context, Item member, String postcode) {

		postcode = LocationUtil.flattenPostcode(postcode);

		Query q = new Query(context, "select count(*) From # where member=? and postcode=? and AvsResult=?");
		q.setTable(AvsCheck.class);
		q.setParameter(member);
		q.setParameter(postcode);
		q.setParameter(AvsResult.Match);
		return q.getInt() > 0;
	}

	/**
	 * String version of the address this was checked against
	 */
	private String	address;

	/**
	 * The transaction Id from the processor that did this transaction check
	 * If this is null then this AVS Check was performed manually.
	 * 
	 */
	private String	transactionId;

	/**
	 * Just the digits from the address and postcode for AVS.
	 */
	private String	avsString;

	/**
	 * Flattened postcode this avs was checked against
	 */
	private String	postcode;

	/**
	 * Card details for this avs check.
	 */
	private String	card;

	/**
	 * The AVS Result
	 */
	private AvsResult	avsResult;

	/**
	 * 
	 */
	private Item	member;

	/**
	 * Date of AVS check
	 */
	private Date	date;

	public AvsCheck(RequestContext context) {
		super(context);
	}

	public AvsCheck(RequestContext context, Order order, Payment payment, AvsResult result) {
		this(context, payment, order.getDeliveryAddress().getAddress(" "), order.getDeliveryAddress().getPostcode(), result);
	}

	public AvsCheck(RequestContext context, Payment payment, String address, String postcode, AvsResult avs) {
		super(context);

		this.avsResult = avs;
		this.address = address;
		this.postcode = postcode;
		this.transactionId = payment.getProcessorTransactionId();
		this.member = payment.getAccount();
		this.card = payment.getDetails();

		this.date = new Date();
		this.avsString = (address + postcode).replaceAll("\\D", "");
		save();
	}

	public AvsCheck(RequestContext context, String transactionId, Item member, Address address, Card card, AvsResult avsResult) {
		super(context);

		this.transactionId = transactionId;

		this.member = member;
		this.address = address.getAddress(", ");
		this.postcode = address.getPostcode();
		this.card = card.getLabelSecure(" ");
		this.avsResult = avsResult;

		this.date = new Date();
		this.avsString = (address + postcode).replaceAll("\\D", "");
		save();
	}

	public final String getAddress() {
		return address;
	}

	public final AvsResult getAvsResult() {
		return avsResult;
	}

	public final String getAvsString() {
		return avsString;
	}

	public final String getCard() {
		return card;
	}

	public final Date getDate() {
		return date;
	}

	public final Item getMember() {
		return member.pop();
	}

	public final String getPostcode() {
		return postcode;
	}

	public final AvsResult getResult() {
		return avsResult;
	}

	public final String getTransactionId() {
		return transactionId;
	}
}
