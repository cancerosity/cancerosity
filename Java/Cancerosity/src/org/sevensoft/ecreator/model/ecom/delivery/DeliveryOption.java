package org.sevensoft.ecreator.model.ecom.delivery;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.categorisable.Categories;
import org.sevensoft.ecreator.model.categories.categorisable.CategoryJoin;
import org.sevensoft.ecreator.model.categories.categorisable.CategoryUtil;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.model.extras.reorder.ReorderOwner;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

import java.util.*;

/**
 * @author sks 13-Jul-2005 10:54:12
 *         <p/>
 *         A delivery band applies to the entire basket
 */
@Table("delivery")
public class DeliveryOption extends EntityObject implements Logging, Selectable, Positionable, Categories, ReorderOwner {

    public enum Type {
        FlatRate, Price, Qty, Distance, Weight, Item;

        public static List<Type> get() {
            return Arrays.asList(FlatRate, Qty, Price, Weight, Item);
        }
    }

    public static List<DeliveryOption> get(RequestContext context) {
        return SimpleQuery.execute(context, DeliveryOption.class);
    }

    public static List<DeliveryOption> get(RequestContext context, Deliverable d) {

        List<DeliveryOption> rates = new ArrayList<DeliveryOption>();
        for (DeliveryOption rate : get(context)) {

            if (rate.isValid(d)) {
                rates.add(rate);
            }

        }

        return rates;
    }

    /**
     *
     */
    public static List<DeliveryOption> get(RequestContext context, Type type) {
        return SimpleQuery.execute(context, DeliveryOption.class, "type", type);
    }

    public static List<DeliveryOption> getItemBased(RequestContext context) {

        List<DeliveryOption> options = get(context);
        CollectionsUtil.filter(options, new Predicate<DeliveryOption>() {

            public boolean accept(DeliveryOption e) {
                return e.getType() == Type.Item;
            }
        });
        return options;
    }

    public static Map<String, String> getPricing(RequestContext context, Deliverable d, boolean exVat) {

        Currency currency = (Currency) context.getAttribute("currency");
        DeliverySettings deliverySettings = DeliverySettings.getInstance(context);
        Map<String, String> map = new LinkedHashMap();

        for (DeliveryOption option : get(context)) {

            if (option.isValid(d)) {

                StringBuilder text = new StringBuilder();
                text.append(option.getDescription());

                if (deliverySettings.isEstimateDueDate() && option.getPeriod() > 0) {
                    text.append(" delivered by ");
                    text.append(option.getDueDate().toString("dd MMM yyyy"));
                }

                Money charge;
                if (exVat) {
                    charge = option.getChargeEx(d);
                } else {
                    charge = option.getChargeInc(d);
                }

                text.append(" - " + currency.getSymbol() + currency.convert(charge));

                map.put(option.getIdString(), text.toString());
            }
        }

        return map;
    }

    private boolean supermanLock;

    /**
     * A set of the countries this delivery rate is valid for
     */
    private SortedSet<Country> countries;

    private int cutOff, qtyIncrement, startQty, endQty, period;

    /**
     * Should be just interval or valueInterval or something. BC
     */
    private double weightIncrement;

    /**
     * Start weight / end weight
     * Should be start / end attribute value but keeping for BC
     */
    private double startWeight, endWeight;

    private String name, location;

    private boolean saturdayDelivery;

    /**
     * Should be called interval charge now, keeping for BC
     */
    private Money weightIncrementCharge;

    private Money startPrice, endPrice, flatCharge, qtyIncrementCharge, priceIncrementCharge, priceIncrement;

    private Type type;

    @Default("17.5")
    private double vatRate;

    private int x;

    private int y;

    private int position;

    private Money costPriceEx;

    private String code;

    private boolean saturdayDispatch;

    private boolean saturdayService;

    private String postcodes;

    public DeliveryOption(RequestContext context) {
        super(context);
    }

    public DeliveryOption(RequestContext context, String s) {
        super(context);

        this.name = s;
        this.type = Type.FlatRate;

        this.vatRate = 17.5;
        this.flatCharge = new Money(0);
        this.qtyIncrementCharge = new Money(0);
        this.priceIncrement = new Money(0);
        this.startPrice = new Money(0);
        this.endPrice = new Money(0);
        this.priceIncrement = new Money(0);
        this.priceIncrementCharge = new Money(0);

        this.weightIncrementCharge = new Money(0);

        save();
    }

    public void addAllCountries() {
        getCountries().addAll(Country.getAll());
    }

    public void addCategories(List<Category> categories) {
        for (Category category : categories) {
            new CategoryJoin(context, this, category);
        }
    }

    public void addCategory(Category category) {
        new CategoryJoin(context, this, category);
    }

    public void addCountry(Country country) {
        getCountries().add(country);
    }

    @Override
    public DeliveryOption clone() throws CloneNotSupportedException {

        DeliveryOption copy = (DeliveryOption) super.clone();

        copy.name = this.name + " (copy)";
        copy.save();

        return copy;
    }

    @Override
    public synchronized boolean delete() {

        // remove delivery from and recalc all baskets
        Query q = new Query(context, "update # set deliveryOption=0, recalc=1 where deliveryOption=?");
        q.setTable(Basket.class);
        q.setParameter(this);
        q.run();

        removeRates();

        q = new Query(context, "update # set deliveryOption=0 where deliveryOption=?");
        q.setTable(Order.class);
        q.setParameter(this);
        q.run();

        return super.delete();
    }

    public List<Category> getCategories() {
        return CategoryUtil.getCategories(context, this);
    }

    public Money getChargeEx(Deliverable deliverable) {

        logger.fine("[DeliveryOption] calculating delivery charge for deliverable: " + deliverable);

        int incs;

        Money ex = flatCharge;
        if (ex == null) {
            ex = new Money(0);
        }

        logger.fine("[DeliveryOption] starting flat rate=" + flatCharge);

        List<DeliverableItem> deliverableItems = deliverable.getDeliverableItems();

        /*
           * add all the surcharges to the flat rate
           */
        for (DeliverableItem item : deliverableItems) {

            final Money surcharge = item.getDeliverySurcharge();
            int qty = item.getQtyForDelivery();
            if (surcharge != null && surcharge.isPositive()) {
                logger.fine("[DeliveryOption] adding surcharge=" + surcharge + ", item=" + item);
            }
            if (surcharge != null) {
                ex = ex.add(surcharge.multiply(qty));
            }
        }

        logger.fine("[DeliveryOption] calculation type=" + getType());

        switch (getType()) {

            default:
            case FlatRate:
            case Distance:
                return ex;

            case Item:

                /*
                 * Get the delivery costs for this option from each item
                 */
                for (DeliverableItem item : deliverableItems) {

                    Money rate = item.getDeliveryRate(this);

                    if (rate != null) {

                        logger.fine("[DeliveryOption] rate=" + rate + ", item=" + item);
                        ex = ex.add(rate.multiply(item.getQtyForDelivery()));
                    }
                }

                return ex;

            case Weight:

                double value = getWeight(deliverable);
                logger.fine("[DeliveryOption] deliverable weight=" + value);

                if (value < startWeight) {
                    return new Money(0);
                }

                if (value > endWeight) {
                    return new Money(0);
                }

                /*
                 * If qty increment is zero then it does not matter what qty we have, we charge the same so return flat rate.
                 */
                if (weightIncrement == 0) {
                    logger.fine("[DeliveryOption] no weightIncrement, using flat rate");
                    return ex;
                }

                final double c = value - startWeight;
                incs = (int) (c / weightIncrement);
                if (c % weightIncrement > 0) {
                    incs++;
                }

                logger.fine("[DeliveryOption] weightIncrementCharge=" + weightIncrementCharge + ", weightIncs=" + incs);
                return ex.add(weightIncrementCharge.multiply(incs));

            case Price:

                Money total = getPriceTotal(deliverable);
                logger.fine("[DeliveryOption] getPriceTotal=" + total);

                if (total.isLessThan(getStartPriceEx())) {
                    return new Money(0);
                }

                if (total.isGreaterThan(getEndPriceEx())) {
                    return new Money(0);
                }

                /*
                 * If price increment is zero then it does not matter what price we have, we charge the same so return flat rate.
                 */
                if (priceIncrement.isZero()) {
                    logger.fine("[DeliveryOption] no priceIncrement, using flat rate");
//                    return flatCharge;
                    return ex;  // ex includes delivery_surcharge
                }

                final int a = total.getAmount() - startPrice.getAmount();
                incs = a / priceIncrement.getAmount();
                if (a % priceIncrement.getAmount() > 0) {
                    incs++;
                }

                logger.fine("[DeliveryOption] priceIncrement=" + priceIncrement + ", priceIncs=" + incs);
                return ex.add(priceIncrementCharge.multiply(incs));

            case Qty:

                int qty = getQty(deliverable);
                logger.fine("[DeliveryOption] deliverable qty=" + qty);

                if (qty < startQty) {
                    return new Money(0);
                }

                if (qty > endQty) {
                    return new Money(0);
                }

                /*
                 * If qty increment is zero then it does not matter what qty we have, we charge the same so return flat rate.
                 */
                if (qtyIncrement == 0) {
                    logger.fine("[DeliveryOption] no qtyIncrement, using flat rate");
                    return ex;
                }

                final int b = qty - startQty;
                incs = b / qtyIncrement;
                if (b % qtyIncrement > 0) {
                    incs++;
                }

                logger.fine("[DeliveryOption] qtyIncrement=" + qtyIncrement + ", qtyIncs=" + incs);
                return ex.add(qtyIncrementCharge.multiply(incs));
        }
    }

    private Money getChargeInc(Deliverable d) {

        Money charge = getChargeEx(d);

        if (Company.getInstance(context).isVatRegistered() && d.isVatableForDelivery()) {
            return charge.multiply((vatRate / 100) + 1);
        } else {
            return charge;
        }
    }

    public Money getChargeVat(Deliverable d) {

        if (Company.getInstance(context).isVatRegistered()) {
            return getChargeEx(d).multiply(vatRate / 100);
        } else {
            return new Money(0);
        }
    }

    public String getCode() {
        return code;
    }

    public Money getCostPriceEx() {
        return costPriceEx;
    }

    public SortedSet<Country> getCountries() {
        if (countries == null) {
            countries = new TreeSet<Country>();
        }
        return countries;
    }

    public int getCutOff() {
        return cutOff;
    }

    /**
     * Returns the total as used by the delivery attribute
     */
    private int getDeliveryAttributeTotal(Deliverable deliverable) {

        int total = 0;
        for (DeliverableItem item : deliverable.getDeliverableItems()) {
            total = total + item.getDeliveryAttributeValue();
        }
        return total;
    }

    public String getDescription() {
        return name;
    }

    /**
     * Returns the date of when this delivery is estimated to arrive
     */
    public Date getDueDate() {

        Date due = new Date();

        //		Set<Date> nsd = DeliverySettings.getInstance(context).getNonShippingDates();
        Set<Date> nsd = new HashSet();

        /*
           * if past cut off for today then move forward to next day as it cannot be dispatched today anymore
           */
        if (new DateTime().getHour() >= cutOff) {
            due = due.nextDay();
        }

        /*
           * while we are on a non dispatch day forward to the next day
           */
        while (due.isSunday() || nsd.contains(due) || (due.isSaturday() && !isSaturdayDispatch())) {
            due = due.nextDay();
        }

        /*
           * if this is a saturday service then just find the next saturday that isn't a non working day;
           */
        if (isSaturdayService()) {

            // if this is already a saturday (because perhaps we dispatch on a sat), we need to move on at least one day
            if (due.isSaturday())
                due = due.nextDay();

            while (!due.isSaturday() || nsd.contains(due)) {
                due = due.nextDay();
            }

            return due;

        }

        /*
           * Otherwise keep cycling through days adding in our delivery period
           */
        for (int n = 0; n < period; n++) {
            due = due.nextDay();
            while (due.isSunday() || nsd.contains(due) || (due.isSaturday() && !isSaturdayDelivery())) {
                due = due.nextDay();
            }
        }

        return due;
    }

    public Money getEndPrice() {
        return endPrice;
    }

    public Money getEndPriceInc() {
        Money charge = getEndPrice();
        if (charge == null) {
            charge = new Money(0);
        }

        if (Company.getInstance(context).isVatRegistered() && isVatable() && DeliverySettings.getInstance(context).isTaxInc()) {
            return charge.multiply((vatRate / 100) + 1);
        } else {
            return charge;
        }
    }

    public Money getEndPriceEx() {
        return getEndPrice();
    }

    public int getEndQty() {
        return endQty;
    }

    public double getEndValue() {
        return endWeight;
    }

    public Money getFlatCharge() {
        return flatCharge;
    }

    /**
     * Returns a message informing the customer what they must do to get free del or if no requirement then it just returns null.
     */
    public String getFreeDelRequirementMessage(Address address, int weight, int qty, Money total, Money delivery) {

        if (!isFreeDelivery())
            return null;

        /*
           * If we have an address then check our list of countries covers this address. If this delivery has no countries defined then assume it
           * is valid for all countries.
           */
        if (address != null)
            if (getCountries().size() > 0 && !getCountries().contains(address.getCountry()))
                return null;

        switch (getType()) {

            default:
            case Distance:
            case FlatRate:

            case Price:

                if (total.minus(delivery).isLessThan(getStartPriceInc()))
                    return "Spend an extra \243" + getStartPriceInc().minus(total.minus(delivery)) + " to qualify for free delivery!";

                return null;

            case Qty:

                if (qty < endQty)
                    return "Add another " + (endQty - qty) + " items to your basket to qualify for free delivery!";

                return null;

        }

    }

    public Money getIncrementalCharge() {
        return weightIncrementCharge;
    }

    public double getInterval() {
        return weightIncrement;
    }

    public Money getIntervalCharge() {
        return weightIncrementCharge;
    }

    public String getLabel() {
        return getName();
    }

    public String getLocation() {
        return location;
    }

    public List<LogEntry> getLogEntries() {
        return LogEntry.get(context, this);
    }

    public String getLogId() {
        return getFullId();
    }

    public String getLogName() {
        return "Delivery option #" + getIdString();
    }

    public String getName() {
        return name;
    }

    public int getPeriod() {
        return period;
    }

    public String getPeriodDescription() {

        if (saturdayDelivery) {
            return "Upcoming saturday";
        } else if (period < 2) {
            return "Next working day";
        } else {
            return period + " working days";
        }
    }

    public int getPosition() {

        /*
           * set position for old databases where position was not used
           */
        if (position == 0) {
            position = getId();
            save();
        }
        return position;
    }

    public Money getPriceIncrement() {
        return priceIncrement;
    }

    public Money getPriceIncrementCharge() {
        return priceIncrementCharge;
    }

    private Money getPriceTotal(Deliverable deliverable) {
        Money total = new Money(0);
        for (DeliverableItem item : deliverable.getDeliverableItems()) {
            total = total.add(item.getPriceForDelivery());
        }
        return total;
    }

    /**
     * Returns the total qty as used by this deliverable
     */
    private int getQty(Deliverable deliverable) {

        int qty = 0;
        for (DeliverableItem item : deliverable.getDeliverableItems()) {
            qty = qty + item.getQtyForDelivery();
        }

        return qty;
    }

    public int getQtyIncrement() {
        return qtyIncrement;
    }

    public Money getQtyIncrementCharge() {
        return qtyIncrementCharge;
    }

    public Money getStartPrice() {
        return startPrice;
    }

    public Money getStartPriceInc() {
        Money charge = getStartPrice();
        if (charge == null) {
            charge = new Money(0);
        }

        if (Company.getInstance(context).isVatRegistered() && isVatable() && DeliverySettings.getInstance(context).isTaxInc()) {
            return charge.multiply((vatRate / 100) + 1);
        } else {
            return charge;
        }
    }

    public Money getStartPriceEx() {
        return getStartPrice();
    }

    public int getStartQty() {
        return startQty;
    }

    public double getStartValue() {
        return startWeight;
    }

    public Type getType() {
        return type == null ? Type.FlatRate : type;
    }

    public String getValue() {
        return getIdString();
    }

    public double getVatRate() {
        return vatRate;
    }

    private double getWeight(Deliverable deliverable) {

        double weight = 0;

        for (DeliverableItem item : deliverable.getDeliverableItems()) {

            final double w = item.getWeightForDelivery();
            logger.fine("[DeliveryOption] weight for item=" + item + ", weight=" + w);
            weight = weight + w;
        }

        logger.fine("[DeliveryOption] weight=" + weight);
        return weight;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean hasCategory() {
        return CategoryUtil.hasCategory(context, this);
    }

    private boolean hasCountries() {
        return !getCountries().isEmpty();
    }

    public boolean hasGridRef() {
        return x > 0 && y > 0;
    }

    public boolean hasLocation() {
        return location != null;
    }

    public boolean isFreeDelivery() {
        return flatCharge.isZero() && qtyIncrementCharge.isZero() && weightIncrementCharge.isZero() && priceIncrementCharge.isZero();
    }

    public boolean isSaturdayDelivery() {
        return saturdayDelivery;
    }

    public boolean isSaturdayDispatch() {
        return saturdayDispatch;
    }

    public boolean isSaturdayService() {
        return saturdayService;
    }

    public final boolean isSupermanLock() {
        return supermanLock;
    }

    public boolean isValid(Deliverable deliverable) {

        if (hasCountries()) {

            Country country = deliverable.getDeliveryCountry();
            if (country != null && !getCountries().contains(country)) {
                logger.fine("delivery rate is not valid for country: " + country);
                return false;
            }
        }

        switch (getType()) {

            // always applicable for flat rate and item
            default:
            case Item:
            case FlatRate:
                return true;

            case Distance:
                logger.fine("distance rate not implemented yet");
                return false;

            case Price:

                Money price = getPriceTotal(deliverable);
                return getStartPriceEx().isLessThanOrEquals(price) && price.isLessThanOrEquals(getEndPriceEx());

            case Qty:

                int qty = getQty(deliverable);

                // check within our bounds
                return startQty <= qty && qty <= endQty;

            case Weight:

                double value = getWeight(deliverable);

                // check within our bounds
                return startWeight <= value && value <= endWeight;

        }
    }

    public boolean isVatable() {
        return vatRate > 0;
    }

    public void log(String message) {
        new LogEntry(context, this, message);
    }

    public void log(User user, String message) {
        new LogEntry(context, this, user, message);
    }

    public void removeCategories() {
        CategoryUtil.removeCategories(context, this);
    }

    public void removeCategory(Category category) {
        CategoryUtil.removeCategory(context, this, category);
    }

    public void removeCountry(Country country) {
        getCountries().remove(country);
    }

    public void removeLocation() {
        this.location = null;
        this.x = 0;
        this.y = 0;
    }

    /**
     *
     */
    private void removeRates() {
        SimpleQuery.delete(context, DeliveryRate.class, "deliveryOption", this);
    }

    @Override
    public synchronized void save() {
        super.save();

        // flag all baskets for a recalc
        Basket.flagDeliveryRate(context, this);
    }

    public void setCategories(List<Category> categories) {
        CategoryUtil.setCategories(this, categories);
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setCostPriceEx(Money costPriceEx) {
        this.costPriceEx = costPriceEx;
    }

    public void setCutOff(int cutOff) {
        this.cutOff = cutOff;
    }

    public void setEndPrice(Money m) {
        this.endPrice = (m == null ? new Money(0) : m);
    }

    public void setEndQty(int i) {

        if (i < startQty) {
            endQty = startQty;

        } else {
            endQty = i;
        }
    }

    public void setEndWeight(double w) {

        if (w < startWeight) {
            endWeight = startWeight;
        } else {
            endWeight = w;
        }
    }

    public void setFlatCharge(Money m) {
        this.flatCharge = (m == null ? new Money(0) : m);
    }

    public void setIncrementalCharge(Money incrementalCharge) {
        this.weightIncrementCharge = incrementalCharge;
    }

    public void setInterval(double weightIncrement) {
        this.weightIncrement = weightIncrement;
    }

    public void setIntervalCharge(Money m) {
        this.weightIncrementCharge = (m == null ? new Money(0) : m);
    }

    public void setLocation(String location, int x, int y) {

        this.location = location;
        this.x = x;
        this.y = y;

        save("location", "x", "y");
    }

    public void setName(String s) {
        this.name = (s == null ? null : s.trim());
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getReorderName() {
        return getName();
    }

    public void setPriceIncrement(Money m) {
        this.priceIncrement = (m == null ? new Money(0) : m);
    }

    public void setPriceIncrementCharge(Money m) {
        this.priceIncrementCharge = (m == null ? new Money(0) : m);
    }

    public void setQtyIncrement(int qtyIncrement) {
        this.qtyIncrement = qtyIncrement;
    }

    public void setQtyIncrementCharge(Money m) {
        this.qtyIncrementCharge = (m == null ? new Money(0) : m);
    }

    public void setSaturdayDelivery(boolean saturday) {
        this.saturdayDelivery = saturday;
    }

    public void setSaturdayDispatch(boolean saturdayDispatch) {
        this.saturdayDispatch = saturdayDispatch;
    }

    public void setSaturdayService(boolean saturdayService) {
        this.saturdayService = saturdayService;
    }

    public void setStartPrice(Money m) {
        this.startPrice = (m == null ? new Money(0) : m);
    }

    public void setStartQty(int startQty) {
        this.startQty = startQty;
    }

    public void setStartWeight(double startWeight) {
        this.startWeight = startWeight;
    }

    public final void setSupermanLock(boolean supermanLock) {
        this.supermanLock = supermanLock;
    }

    public void setType(Type type) {

        if (type == this.type) {
            return;
        }

        this.type = type;

        priceIncrement = new Money(0);
        priceIncrementCharge = new Money(0);
        endPrice = new Money(0);
        startPrice = new Money(0);

        startQty = 0;
        endQty = 0;
        qtyIncrement = 0;
        qtyIncrementCharge = new Money(0);

        startWeight = 0;
        endWeight = 0;
        weightIncrement = 0;
        weightIncrementCharge = new Money(0);

        save();
    }

    public void setVatRate(double d) {

        if (this.vatRate == d) {
            return;
        }

        this.vatRate = d;
    }

    public String getPostcodes() {
        return postcodes;
    }

    public void setPostcodes(String postcodes) {
        this.postcodes = postcodes;
    }
}
