package org.sevensoft.ecreator.model.ecom.payments.forms.optimal;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 07-Mar-2006 11:37:35
 *
 */
public class OptimalPaymentsDynamic extends Form {

	public OptimalPaymentsDynamic(RequestContext context) {
		super(context);
	}

	@Override
	public String getMethod() {
		return "POST";
	}

	@Override
	public Map<String, String> getParams(Payable payable) throws IOException, FormException {

		PaymentSettings ps = PaymentSettings.getInstance(context);

		Map<String, String> params = new HashMap<String, String>();

		params.put("ACCOUNTID", ps.getOptimalPaymentsAccountId());
		params.put("AMOUNT", payable.getPaymentAmount().toEditString());
		params.put("CURRENCY", "GBP");
		params.put("CUSTMAIL", payable.getPayableAccount().getEmail());
		params.put("CUSTNAME", payable.getPayableAccount().getName());

		// we will use invoice number as the payment id, since there is no callback we don't need to use a session
		params.put("INVNUMBER", payable.getIdString());
		params.put("MERCHANTID", ps.getOptimalPaymentsMerchantId());
		params.put("SUCCESSURL", payable.getPaymentSuccessUrl(PaymentType.OptimalPayments));
		params.put("ERRORURL", payable.getPaymentFailureUrl(PaymentType.OptimalPayments));

		return params;
	}

	@Override
	public String getPaymentUrl() {
		return "https://payment.firepay.com/surefire/ccs/ppll/onlinepaymentPPL.jsp";
	}

	@Override
	public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
	}

	@Override
	public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
		return null;
	}

}
