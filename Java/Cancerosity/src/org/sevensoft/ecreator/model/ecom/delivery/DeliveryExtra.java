package org.sevensoft.ecreator.model.ecom.delivery;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 1 Mar 2007 11:17:42
 * 
 * Optional surchage 
 *
 */
@Table("delivery_extras")
public class DeliveryExtra extends EntityObject {

	private String	name;
	private Money	chargeEx, cost;

	protected DeliveryExtra(RequestContext context) {
		super(context);
	}

	public DeliveryExtra(RequestContext context, String name) {
		super(context);
		this.name = name;
	}

	public final Money getChargeEx() {
		return chargeEx;
	}

	public Money getChargeInc() {
		return null;
	}

	public Money getChargeVat() {
		return null;
	}

	public final Money getCost() {
		return cost;
	}

	public final String getName() {
		return name;
	}

	public final void setChargeEx(Money chargeEx) {
		this.chargeEx = chargeEx;
	}

	public final void setCost(Money cost) {
		this.cost = cost;
	}

	public final void setName(String name) {
		this.name = name;
	}

}
