package org.sevensoft.ecreator.model.ecom.credits.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.ecom.credits.CreditsHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 16 Aug 2006 17:23:37
 *
 */
public class CreditPageMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.Credits.enabled(context)) {
			return null;
		}

		return super.link(context, params, new Link(CreditsHandler.class), "account_credits");
	}

	@Override
	public String getDescription() {
		return "A link to the account's credit status page";
	}

	public Object getRegex() {
		return new String[] { "credits_page", "account_credits" };
	}
}
