package org.sevensoft.ecreator.model.ecom.delivery;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.orders.VatHelper;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 16 Jan 2007 13:26:13
 * 
 * A delivery rate is a per item / type delivery cost
 *
 */
@Table("delivery_rates")
public class DeliveryRate extends EntityObject {

	private Item		item;
	private DeliveryOption	deliveryOption;
	private Money		chargeEx;

	private DeliveryRate(RequestContext context) {
		super(context);
	}

	public DeliveryRate(RequestContext context, DeliveryOption deliveryOption, Item item, Money chargeEx) {
		super(context);

		this.item = item;
		this.deliveryOption = deliveryOption;
		this.chargeEx = chargeEx;

		save();
	}

	public final Money getChargeEx() {
		return chargeEx;
	}

	public Money getChargeInc() {
		return VatHelper.inc(context, getChargeEx(), getItem().getVatRate());
	}

	public final DeliveryOption getDeliveryOption() {
		return deliveryOption.pop();
	}

	public final Item getItem() {
		return item.pop();
	}

	public final void setChargeEx(Money chargeEx) {
		this.chargeEx = chargeEx;
	}
}
