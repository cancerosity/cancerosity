package org.sevensoft.ecreator.model.ecom.delivery;

import java.util.List;

import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.skint.Money;

/**
 * @author sks 1 Aug 2006 07:27:22
 * 
 * This interface is to be used by anything that can be delivered and needs to calculate delivery rates
 * 
 */
public interface Deliverable {

	/**
	 * Returns a list of items that are being delivered.
	 */
	public List<DeliverableItem> getDeliverableItems();

	public Money getDeliveryChargeEx();

	/**
	 * Returns the delivery country.
	 */
	public Country getDeliveryCountry();

	/**
	 * Returns the postcode for use in calculating delivery rates
	 */
	public String getDeliveryPostcode();

	/**
	 * 
	 */
	public boolean isVatableForDelivery();

}
