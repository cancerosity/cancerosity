package org.sevensoft.ecreator.model.ecom.orders.markers;

import java.util.Map;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderLineMarker;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 22 Feb 2007 09:15:13
 *
 */
public class LineSellLineMarker extends MarkerHelper implements IOrderLineMarker {

	public Object generate(RequestContext context, Map<String, String> params, OrderLine line) {

		Money money;
		if (params.containsKey("inc")) {
			money = line.getLineSellInc();
		}

		else {
			money = line.getLineSellEx();
		}

		return super.string(context, params, money);
	}

	@Override
	public String getDescription() {
		return "Selling price per line";
	}

	public Object getRegex() {
		return "order_line_sellline";
	}

}
