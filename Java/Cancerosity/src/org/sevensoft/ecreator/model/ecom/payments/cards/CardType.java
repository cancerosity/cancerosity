package org.sevensoft.ecreator.model.ecom.payments.cards;

import java.util.Arrays;
import java.util.List;

/**
 * @author sks 26-Oct-2005 10:55:33
 * 
 */
public enum CardType {

	Maestro() {

		@Override
		public String toString() {
			return "Maestro";
		}
	},
	Mastercard, Solo, Visa,
    VisaDebit() {

		@Override
		public String toString() {
			return "Visa Debit";
		}
	},
    VisaDelta() {

		@Override
		public String toString() {
			return "Visa Delta";
		}
	},
	VisaElectron() {

		@Override
		public String toString() {
			return "Visa Electron";
		}
	};

	public static List<CardType> get() {
		return Arrays.asList(Mastercard, Visa, VisaDebit, VisaDelta, VisaElectron, Maestro, Solo);
	}
}
