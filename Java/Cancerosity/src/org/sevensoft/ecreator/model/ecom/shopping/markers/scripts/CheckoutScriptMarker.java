package org.sevensoft.ecreator.model.ecom.shopping.markers.scripts;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketMarker;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.ShoppingSettings;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * User: Dmitry Lebedev
 * Date: 30.07.2009
 * Time: 17:38:23
 */
public class CheckoutScriptMarker extends MarkerHelper implements IBasketMarker {
    public Object generate(RequestContext context, Map<String, String> params, Basket basket) {

        ShoppingSettings shoppingSettings = ShoppingSettings.getInstance(context);
        return shoppingSettings.getCheckoutScripts();
    }

    public Object getRegex() {
        return new String[] { "checkout_scripts" };
    }
}
