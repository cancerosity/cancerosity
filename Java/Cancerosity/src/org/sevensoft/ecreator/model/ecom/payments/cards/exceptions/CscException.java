package org.sevensoft.ecreator.model.ecom.payments.cards.exceptions;

/**
 * @author sks 26-Oct-2005 10:58:19
 * 
 */
public class CscException extends Exception {

	public CscException() {
	}

	public CscException(String message, Throwable cause) {
		super(message, cause);
	}

	public CscException(String message) {
		super(message);
	}

	public CscException(Throwable cause) {
		super(cause);
	}



}
