package org.sevensoft.ecreator.model.ecom.payments.forms.paypal;

import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.iface.callbacks.PaypalCallbackHandler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

import java.util.Map;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.net.URLEncoder;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.URLDecoder;
import java.io.IOException;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * @author LebedevDN
 * @version: $id$
 */
public class PayPalExpress extends Form {
	private static final String	SANDBOX_URL	= "https://www.sandbox.paypal.com/cgi-bin/webscr";

	private static final String	URL		= "https://www.paypal.com/cgi-bin/webscr";

	private static final String	gv_Version	= "2.3";

	public PayPalExpress(RequestContext context) {
		super(context);
	}

	@Override
	public String getMethod() {
		return "POST";
	}

	@Override
	public String getPaymentUrl() {
		if (PaymentSettings.getInstance(context).isPayPalExpressLive())
			return URL;
		else
			return SANDBOX_URL;
	}

	@Override
	public Map<String, String> getParams(Payable payable) {
		FormSession session = new FormSession(context, payable);
		Config config = Config.getInstance(context);

		// Map values from which are used in request to PayPal web site
		Map<String, String> map = new LinkedHashMap<String, String>();
		// Map with values from response from PayPal web site
		Map<String, String> m;

		String currencyCode = "";

		if (Currency.getDefault(context).getCode() != null && !Currency.getDefault(context).getCode().equals("")) {
			currencyCode = Currency.getDefault(context).getCode();
		} else {
			currencyCode = "GBP";
		}

        String nvpStr = "&Amt=" + payable.getPaymentAmount().toEditString() + "&PAYMENTACTION=Sale&ReturnUrl=" +
                URLEncoder.encode(payable.getPaymentSuccessUrl(PaymentType.PayPalExpress)) +
                "&CANCELURL=" + URLEncoder.encode(payable.getPaymentFailureUrl(PaymentType.PayPalExpress)) +
                "&CURRENCYCODE=" + currencyCode;

		m = httpCall("SetExpressCheckout", nvpStr);

		map.put("token", m.get("TOKEN"));

		map.put("cancel_return", payable.getPaymentFailureUrl(PaymentType.PayPalExpress));
		map.put("return", payable.getPaymentSuccessUrl(PaymentType.PayPalExpress));

		map.put("item_name", payable.getPaymentDescription());
		map.put("quantity", "1");

		// xclick is our type of basket.
		map.put("cmd", "_express-checkout");

		map.put("notify_url", config.getUrl() + "/" + new Link(PaypalCallbackHandler.class));

		// paypal account email
		map.put("business", PaymentSettings.getInstance(context).getPayPalAccountEmail());

		/*
		 * invoice must be completely unique so we will put in our generated transaction id
		 */
		map.put("invoice", session.getTransactionId());

		/*
		 * We use custom to store our session id
		 */
		map.put("custom", session.getIdString());

		// We don't need the customer to enter a note (instructions)
		map.put("no_note", "1");

		// total for the order including handling (shipping). It seems paypal will remove the shipping we pass in from this to display subtotal
		map.put("amount", payable.getPaymentAmount().toEditString());

		// disallow customer instructions
		map.put("no_note", "1");

		// currency code
		map.put("currency_code", "GBP");

		map.put("item_name", payable.getPaymentDescription());
		map.put("quantity", "1");

		return map;
	}

	@Override
	public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
	}

	public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
		return "xxx";
	}

	public HashMap httpCall(String methodName, String nvpStr) {

        String encodedData = "METHOD=" + methodName + "&VERSION=" + gv_Version +
                "&PWD=" + PaymentSettings.getInstance(context).getPayPalExpressAccountPassword() +
                "&USER=" + PaymentSettings.getInstance(context).getPayPalExpressAcountUsername() +
                "&SIGNATURE=" + PaymentSettings.getInstance(context).getPayPalExpressAccountSignature() + nvpStr;
		String respText = "";
		HashMap nvp = new HashMap();

		try {
			URL postURL;
			if (PaymentSettings.getInstance(context).isPayPalExpressLive()) {
				postURL = new URL("https://api-3t.paypal.com/nvp");
            }
            else {
				postURL = new URL("https://api-3t.sandbox.paypal.com/nvp");
			}
			HttpURLConnection conn = (HttpURLConnection) postURL.openConnection();

			conn.setDoInput(true);
			conn.setDoOutput(true);

			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

			conn.setRequestProperty("Content-Length", String.valueOf(encodedData.length()));
			conn.setRequestMethod("POST");

			DataOutputStream output = new DataOutputStream(conn.getOutputStream());
			output.writeBytes(encodedData);
			output.flush();
			output.close();

            DataInputStream in = new DataInputStream(conn.getInputStream());
			int rc = conn.getResponseCode();
			if (rc != -1) {
				BufferedReader is = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String _line = null;
				while (((_line = is.readLine()) != null)) {
					respText = respText + _line;
				}
				nvp = deformatNVP(respText);
			}
			return nvp;

		} catch (IOException e) {
			return null;
		}
	}

	public HashMap deformatNVP(String pPayload) {
		HashMap nvp = new HashMap();
		StringTokenizer stTok = new StringTokenizer(pPayload, "&");
		while (stTok.hasMoreTokens()) {
			StringTokenizer stInternalTokenizer = new StringTokenizer(stTok.nextToken(), "=");
			if (stInternalTokenizer.countTokens() == 2) {
				String key = URLDecoder.decode(stInternalTokenizer.nextToken());
				String value = URLDecoder.decode(stInternalTokenizer.nextToken());
				nvp.put(key.toUpperCase(), value);
			}
		}
		return nvp;
	}
}
