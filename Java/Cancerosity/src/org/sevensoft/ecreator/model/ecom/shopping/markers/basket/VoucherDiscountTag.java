package org.sevensoft.ecreator.model.ecom.shopping.markers.basket;

import java.util.Map;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketMarker;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Jun 2006 11:48:11
 *
 */
public class VoucherDiscountTag extends MarkerHelper implements IBasketMarker {

	public Object generate(RequestContext context, Map<String, String> params, Basket basket) {

		if (!Module.Vouchers.enabled(context)) {
			return null;
		}

		if (!basket.hasVoucher()) {
			return null;
		}

		Currency currency = (Currency) context.getAttribute("currency");

		Money money;
		if (params.containsKey("ex")) {
			money = basket.getVoucherDiscountEx();
		} else {
			money = basket.getVoucherDiscountEx();
		}

		return super.string(context, params, Currency.convert(money, currency));
	}

	public Object getRegex() {
		return "voucher_discount";
	}

}
