package org.sevensoft.ecreator.model.ecom.orders.hardcopy;

import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Nov 2006 19:02:25
 *
 */
public class InvoiceHtml {

	private Order		order;
	private Company		company;
	private RequestContext	context;
	private Address		deliveryAddress;
	private Address		billingAddress;
	private OrderSettings	orderSettings;

	public InvoiceHtml(RequestContext context, Order order) {
		this.context = context;
		this.order = order;
		this.company = Company.getInstance(context);
		this.billingAddress = order.getBillingAddress();
		this.deliveryAddress = order.getDeliveryAddress();
		orderSettings = OrderSettings.getInstance(context);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("<html>");
		sb.append("<head>");
		sb.append("<style>");
		sb.append("body { margin: 0; padding: 0; }");
		sb.append("* { font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 1.5; }");
		sb.append("h1, table#invtotals td { font-size: 13px; font-weight: bold; }");
		sb.append("th { border-bottom: 1px solid black; }");
		sb.append("#linelist td { padding: 5px; line-height: 1.2; }");
		sb.append("</style>");
		sb.append("</head>");
		sb.append("<body>");
		sb.append("<table cellspacing='0' cellpadding='0' width='600' height='950'>");
		sb.append("<tr>");
		sb.append("<td style='padding: 0; padding-bottom: 10px;' valign='top'>");

		sb.append("<span style='text-transform: uppercase;'>Invoice</span>");

		sb.append("<table cellspacing='0' cellpadding='0' width='600'>");
		sb.append("<tr>");
		sb.append("<td align='left'>");
		sb.append("<div style='margin-left: 20px'><h1>" + company.getName() + "</h1>");
		sb.append(company.getAddressLabel("<br/>"));

		if (company.hasTelephone()) {
			sb.append("Tel: " + company.getTelephone() + "<br />");
		}

		if (company.hasFax()) {
			sb.append("Fax: " + company.getFax());
		}

		sb.append("</div>");
		sb.append("</td>");
		sb.append("<td align='right'>");
		sb.append("<table cellspacing='0' cellpadding='5'>");
		sb.append("<tr>");
		sb.append("<td align='left'>Invoice No.</td>");
		sb.append("<td align='left'>");

		if (order.hasReference()) {

			sb.append(order.getReference());

		} else {

			sb.append(order.getOrderId());

		}

		sb.append("</td>");
		sb.append("</tr>");
		sb.append("<tr>");
		sb.append("<td align='left'>Invoice Date</td>");
		sb.append("<td align='left'>" + order.getDatePlaced().toString("dd-MMM-yyyy") + "</td>");
		sb.append("</tr>");
		sb.append("<tr>");
		//		sb.append("					<td align='left'>Page Number</td><td align='left'>1</td>");
		sb.append("</tr>");
		sb.append("</table>");

		sb.append("</td>");
		sb.append("</tr>");
		sb.append("</table>");

		sb.append("<table cellspacing='0' cellpadding='0' width='600' style='margin-top: 20px; margin-left: 20px;'>");
		sb.append("<tr>");
		sb.append("<td align='left' valign='top' width='12%' style='padding-right: 15px;'>Sold To:</td>");
		sb.append("<td align='left' valign='top' width='35%'>");

		if (order.hasBillingAddress()) {
			sb.append(order.getBillingAddress().getLabel("<br/>", true));
		} else {
			sb.append("None set");
		}

		sb.append("</td>");

		sb.append("<td align='left' valign='top' width='15%' style='padding-left: 15px; padding-right: 15px;'>Ship To:</td>");
		sb.append("<td align='left' valign='top'>");

		if (order.hasDeliveryAddress()) {
			sb.append(order.getDeliveryAddress().getLabel("<br/>", true));
		} else {
			sb.append("Collection");
		}

		sb.append("</td>");
		sb.append("</tr>");
		sb.append("</table>");

		sb.append("</td>");
		sb.append("</tr>");
		sb.append("<tr>");

		sb.append("<td style='padding: 0;'>");

		sb.append("<table cellspacing='3' cellpadding='0' width='600' style='border-top: 1px solid black; border-bottom: 1px solid black;'>");

		sb.append("<tr>");

		sb.append("<td align='left'>Customer Account</td>");
		sb.append("<td align='left'>" + order.getAccount().getId() + "</td>");
		sb.append("<td align='left'>Customer Reference</td>");

		String customerReference = order.getCustomerReference();
		if (customerReference == null)
			customerReference = "";

		sb.append("<td align='left'>" + customerReference + "</td>");
		sb.append("</tr>");

		sb.append("<tr>");

		sb.append("<td align='left'>Origin</td>");
		sb.append("<td align='left'>" + order.getOrigin() + "</td>");

		if (orderSettings.isSalesPersons()) {

			sb.append("<td align='left'>Sales person</td>");
			sb.append("<td align='left'>" + order.getSalesPerson() + "</td>");

		} else {
			sb.append("<td colspan='2'></td>");
		}

		sb.append("</tr>");

		sb.append("</table>");

		sb.append("</td>");
		sb.append("</tr>");
		sb.append("<tr>");
		sb.append("<td style='padding: 0; height: 540px;' valign='top'>");

		sb.append("<table id='linelist' cellspacing='5' cellpadding='0' width='600' style='border-bottom: 1px solid black;'>");
		sb.append("<tr>");
		sb.append("<th align='left'>Description</th>");
		sb.append("<th align='center'>Quantity</th>");
		sb.append("<th align='center'>Unit Price</th>");

		if (company.isVatRegistered()) {
			sb.append("<th align='center'>Vat Rate</th>");
		}

		sb.append("<th align='center'>Total Amount</th>");
		sb.append("</tr>");

		for (OrderLine line : order.getLines()) {

			sb.append("<tr>");
			sb.append("<td align='left'>" + line.getDescription() + "<br/>Item code: " + line.getCode() + "</td>");
			sb.append("<td align='right'>" + line.getQty() + "</td>");
			sb.append("<td align='right'>" + line.getUnitSellEx() + "</td>");
			if (company.isVatRegistered()) {
				sb.append("<td align='right'>" + line.getVatRate() + "%</td>");
			}
			sb.append("<td align='right'>" + line.getLineSellEx() + "</td>");
			sb.append("</tr>");

		}

		if (order.hasDeliveryDetails()) {

			sb.append("<tr>");
			sb.append("<td align='left'>" + order.getDeliveryDetails() + "</td>");
			sb.append("<td align='right'></td>");
			sb.append("<td align='right'>" + order.getDeliveryChargeEx() + "</td>");
			if (company.isVatRegistered()) {
				sb.append("<td align='right'>" + order.getDeliveryVatRate() + "%</td>");
			}
			sb.append("<td align='right'>" + order.getDeliveryChargeEx() + "</td>");
			sb.append("</tr>");
		}

		sb.append("</table>");

		sb.append("<table cellspacing='0' cellpadding='0' width='600' style='margin-top: 10px;'>");
		sb.append("<tr>");
		sb.append("<td align='right'>Total Amount Excl. VAT in</td>");
		sb.append("<td align='left' width='10%' style='padding-left: 10px;'>GBP</td>");
		sb.append("<td align='right' width='12%'>" + order.getTotalEx() + "</td>");
		sb.append("</tr>");

		sb.append("<tr>");
		sb.append("<td align='right'>Total VAT amount in</td>");
		sb.append("<td align='left' width='10%' style='padding-left: 10px;'>GBP</td>");
		sb.append("<td align='right' width='12%'>" + order.getTotalVat() + "</td>");
		sb.append("</tr>");

		// TOTALS
		sb.append("<table id='invtotals' cellspacing='0' cellpadding='0' width='600' style='margin-top: 15px;'>");
		sb.append("<tr>");
		sb.append("<td align='right'>Total Amount Incl. VAT in</td>");
		sb.append("<td align='left' width='10%' style='padding-left: 10px;'>GBP</td>");
		sb.append("<td align='right' width='12%'>" + order.getTotalInc() + "</td>");
		sb.append("</tr>");
		sb.append("</table>");

		sb.append("</td>");
		sb.append("</tr>");
		sb.append("<tr>");
		sb.append("<td style='padding: 0;' valign='bottom'>");

		sb.append("<table cellspacing='0' cellpadding='0' width='600' style='margin-top: 20px;'>");
		sb.append("<tr>");
		sb.append("<td align='center'>");

        if (orderSettings.hasInvoiceFooter()) {
            sb.append(orderSettings.getInvoiceFooter().replace("[comp_vatreg]", "" + company.getVatNumber())
                    .replace("[comp_number]", "" + company.getCompanyNumber()));
        }

		sb.append("</td>");
		sb.append("</tr>");

		if (company.isVatRegistered()) {
			sb.append("<tr>");
			sb.append("<td align='center' style='padding-top: 20px;'>VAT Number: " + company.getVatNumber() + "</td>");
			sb.append("</tr>");
		}

		sb.append("	</table>");

		sb.append("</td>");
		sb.append("</tr>");

		sb.append("</table>");
		sb.append("</body></html>");

		return sb.toString();
	}
}
