package org.sevensoft.ecreator.model.ecom.shopping.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.ecom.shopping.QuickOrderHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 31-Mar-2006 11:20:36
 *
 */
public class QuickOrderMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.QuickOrdering.enabled(context)) {
			return null;
		}

		if (!params.containsKey("text")) {
			params.put("text", "Quick Order");
		}

		return super.link(context, params, new Link(QuickOrderHandler.class), "link_quickorder");
	}

	public Object getRegex() {
		return "quickorder";
	}
}