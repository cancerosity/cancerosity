package org.sevensoft.ecreator.model.ecom.orders.export.csv.ecreator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.jeezy.http.RequestContext;

import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.CsvSaver;

/**
 * @author sks 22 Aug 2006 11:12:45
 * 
 * This class contains logic to export orders to a CSV file. 
 * This CSV file will only contain the headers of the orders.
 * 
 * 
 *
 */
public class OrderHeadersCsv {

	private final Collection<Order>	orders;
	private final RequestContext		context;

	public OrderHeadersCsv(RequestContext context, Collection<Order> orders) {
		this.context = context;
		this.orders = orders;
	}

	public OrderHeadersCsv(RequestContext context, Order order) {
		this(context, Collections.singleton(order));
	}

	private String[] getHeader() {

		List<String> row = new ArrayList();

		row.add("Order ID");
		row.add("Status");
		row.add("Payment type");
		row.add("Date placed");
		row.add("Date placed timestamp");
		row.add("External reference");
		row.add("Customer reference");
		row.add("Origin");
		row.add("Salesperson");

		// member
		row.add("Customer ID");
		row.add("Customer Reference");
		row.add("Name");
		row.add("Email");
		row.add("IP Address");

		// lines row
		row.add("Lines");
		row.add("Items");

		row.add("Delivery type");

		// delivery address
		row.add("Delivery name");
		row.add("Delivery address");
		row.add("Delivery postcode");
		row.add("Delivery country");
		row.add("Delivery telephone");

		// billing address
		row.add("Billing name");
		row.add("Billing address");
		row.add("Billing postcode");
		row.add("Billing country");
		row.add("Billing telephone");

		//		if (orderExportSettings.isExportDeliveryAsLine()) {
		//
		//			row.add("Delivery details");
		//			row.add("Delivery cost");
		//			row.add("Delivery charge ex vat");
		//			row.add("Delivery charge vat");
		//			row.add("Delivery charge inc vat");
		//		}

		row.add("Total ex vat");
		row.add("Total vat");
		row.add("Total inc vat");

		row.add("Last payment details");
		row.add("Last payment amount");
		row.add("Last payment date");
		row.add("Last payment AVS result");
        row.add("Voucher");

        return row.toArray(new String[] {});
	}

	private String[] getRow(Order order) {

		List<String> row = new ArrayList();

		row.add(order.getIdString());
		row.add(order.getStatus());
		row.add(order.getPaymentType().name());
		row.add(order.getDatePlaced().toString("dd-MM-yy"));
		row.add(order.getDatePlaced().getTimestampString());
		row.add(order.getReference());
		row.add(order.getCustomerReference());
		row.add(order.getOrigin().toString());
		row.add(order.getSalesPersonString());

		// member details
		row.add(order.getAccount().getIdString());
		row.add(order.getAccount().getReference());
		row.add(order.getAccount().getName());
		row.add(order.getAccount().getEmail());
		row.add(order.getIpAddress());

		row.add(String.valueOf(order.getLineCount()));
		row.add(String.valueOf(order.getItemCount()));

		row.add(order.getDeliveryType().name());

		// delivery address
		if (order.hasDeliveryAddress()) {

			row.add(order.getDeliveryAddress().getName());
			row.add(order.getDeliveryAddress().getAddress(", "));
			row.add(order.getDeliveryAddress().getPostcode());
			row.add(order.getDeliveryAddress().getCountry().toString());
			row.add(order.getDeliveryAddress().getTelephone1());

		} else {

			for (int n = 0; n < 5; n++)
				row.add(null);
		}

		// billing address
		if (order.hasBillingAddress()) {

			row.add(order.getBillingAddress().getName());
			row.add(order.getBillingAddress().getAddress(", "));
			row.add(order.getBillingAddress().getPostcode());
			row.add(order.getBillingAddress().getCountry().toString());
			row.add(order.getBillingAddress().getTelephone1());

		} else {

			for (int n = 0; n < 5; n++)
				row.add(null);
		}

		//		// delivery costs
		//		if (orderExportSettings.isExportDeliveryAsLine()) {
		//			row.add(order.getDeliveryDetails());
		//			row.add(order.getDeliveryCostEx().toEditString());
		//			row.add(order.getDeliveryChargeEx().toEditString());
		//			row.add(order.getDeliveryChargeVat().toEditString());
		//			row.add(order.getDeliveryChargeInc().toEditString());
		//		}

		row.add(order.getTotalEx().toEditString());
		row.add(order.getTotalVat().toEditString());
		row.add(order.getTotalInc().toEditString());

		Payment payment = order.getLastPayment();

		if (payment == null) {

			row.add(null);
			row.add(null);
			row.add(null);
			row.add(null);

		} else {

			row.add(payment.getDetails());
			row.add(payment.getAmount().toEditString());
			row.add(payment.getDate().toString("dd-MM-yyyy HH:mm"));
			row.add("01");
		}

        row.add(order.getVoucherDescription());

        CollectionsUtil.replaceNulls(row, "");
		return row.toArray(new String[] {});
	}

	public void output(Writer writer) {

		CsvManager csv = new CsvManager();
		csv.setSeparator(",");
		CsvSaver saver = csv.makeSaver(writer);

		saver.begin();
		saver.next(getHeader());

		for (Order order : orders) {

			saver.next(getRow(order));
		}

		saver.end();
	}

	public void output(File file) throws IOException {

		FileWriter writer = null;

		try {

			writer = new FileWriter(file);
			output(writer);

		} catch (IOException e) {
			e.printStackTrace();
			throw e;

		} finally {

			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
