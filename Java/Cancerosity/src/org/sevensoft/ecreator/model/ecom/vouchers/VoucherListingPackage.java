package org.sevensoft.ecreator.model.ecom.vouchers;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;

/**
 * User: Tanya
 * Date: 27.08.2012
 */
@Table("vouchers_listing_packages")
@SuppressWarnings("unused")
public class VoucherListingPackage extends EntityObject {

    private Voucher voucher;
    private ListingPackage listingPackage;

    public VoucherListingPackage(RequestContext context) {
        super(context);
    }

    public VoucherListingPackage(RequestContext context, Voucher voucher, ListingPackage listingPackage) {
        super(context);

        if (!voucher.getListingPackages().contains(listingPackage)) {
            this.voucher = voucher;
            this.listingPackage = listingPackage;

            save();
        }
    }

    public Voucher getVoucher() {
        return voucher.pop();
    }
}