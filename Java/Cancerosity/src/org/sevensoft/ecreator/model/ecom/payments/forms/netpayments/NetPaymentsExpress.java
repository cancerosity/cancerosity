package org.sevensoft.ecreator.model.ecom.payments.forms.netpayments;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.ecom.payments.results.Callback;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 07-Mar-2006 11:37:35
 *
 */
public class NetPaymentsExpress extends Form {

	public NetPaymentsExpress(RequestContext context) {
		super(context);
	}

	@Override
	public String getMethod() {
		return "POST";
	}

	@Override
	public Map<String, String> getParams(Payable payable) {

		PaymentSettings settings = PaymentSettings.getInstance(context);
		FormSession session = new FormSession(context, payable);

		Map<String, String> params = new HashMap<String, String>();
		params.put("NSACCESS", settings.getNetPaymentsExpressAccount());
		params.put("Amount", payable.getPaymentAmount().toEditString());

		// use order id for our session id
		params.put("OrderID", session.getIdString());

		return params;
	}

	private Payment getPayment(Item member, Map<String, String> params, String ipAddress) {

		String txnId = params.get("NSTransID");
		String authCode = params.get("nsrAuthCode");
		boolean successful = "1".equals(params.get("nsrAuth"));
		Money amount = new Money(params.get("Amount"));
		String customerName = params.get("CustomerName");

		if (!successful)
			return null;

		if (amount.isZero())
			return null;

		if (Payment.exists(context, txnId, PaymentType.NetPaymentsExpress))
			return null;

		Payment payment = new Payment(context, member, amount, PaymentType.NetPaymentsExpress);
		payment.setProcessorTransactionId(txnId);
		payment.setAuthCode(authCode);
		payment.setDetails(customerName);
		payment.setIpAddress(ipAddress);
		payment.save();

		return payment;
	}

	@Override
	public String getPaymentUrl() {
		return "https://www.netshopperuk.com/sec/nsauth.cfm";
	}

	@Override
	public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
	}

	@Override
	public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
        log("[NetPaymentsExpress] Server callback params=" + params);

		new Callback(context, PaymentType.NetPaymentsExpress, params);

		FormSession session = EntityObject.getInstance(context, FormSession.class, params.get("OrderID"));
		if (session == null) {
			return null;
		}

		Payment payment = getPayment(session.getAccount(), params, ipAddress);
		if (payment == null) {
			return null;
		}

		session.callback(params, ipAddress, payment);
		return null;
	}
}
