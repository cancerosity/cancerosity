package org.sevensoft.ecreator.model.ecom.shopping.markers.ordering;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketLineMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.ecreator.model.items.stock.StockModule;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.AbstractTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class QtyMarker implements IItemMarker, IBasketLineMarker {

	//	public static final String	OrderingQtyId	= "orderingqty_";

	public Object generate(RequestContext context, Map<String, String> params, BasketLine line) {

		if (line.isPromotion()) {
			return null;
		}

		TextTag tag = new TextTag(context, "qtys_" + line.getId(), line.getQty(), 4);
        tag.setIgnoreParamValue(true);
        return tag;
	}

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		Item account = (Item) context.getAttribute("account");

		//		 set the id to be used by our buy button later
		//		String id = QtyMarker.OrderingQtyId + item.getId();

		if (!item.isOrderable(account)) {
			return null;
		}

		boolean text = params.containsKey("text");

		int start;
		if (params.containsKey("start")) {
			start = Integer.parseInt(params.get("start").trim());
		} else {
			start = 1;
		}

		// ensure start is not lower than our max ordering qty
		if (start < item.getOrderQtyMin()) {
			start = item.getOrderQtyMin();
		}

		int end;
		if (params.containsKey("end")) {
			end = Integer.parseInt(params.get("end").trim());
		} else {
			end = 10;
		}

		// ensure end is not higher than our max ordering qty
		if (item.getOrderQtyMax() > 0) {
//			if (end > item.getOrderQtyMax()) {
				end = item.getOrderQtyMax();
//			}
		}

		int inc;
		if (params.containsKey("inc")) {
			inc = Integer.parseInt(params.get("inc").trim());
		} else {
			inc = 1;
		}

		StockModule module = item.getItemType().getStockModule();
		if (module.isBuyQtyRestriction()) {

            boolean isStockOverridden = false;
            int overriddenStock = 0;
            for (ItemOption option : item.getOptionSet().getOptions()) {
                if (option.isStockOverride()) {
                    isStockOverridden = true;
                    for (ItemOptionSelection selection : option.getSelections()) {
                        overriddenStock += selection.getStock();
                    }
                }
            }
            if (!isStockOverridden) {
                int stock = item.getOurStock();
                if (stock < end) {
                    end = stock;
                }
            }
            else {
                int stock = overriddenStock;
                if (stock < end) {
                    end = stock;
                }
            }

		}

		AbstractTag tag;
		if (text) {

			tag = new TextTag(null, "qtys_" + item.getId(), start, 4);

		} else {

			tag = new SelectTag(null, "qtys_" + item.getId());
			//			tag.setId("addbasketqty_" + item.getId());

			for (int n = start; n <= end; n = n + inc) {
				((SelectTag) tag).addOption(n);
			}
		}

		if (params.containsKey("class")) {
			tag.setClass(params.get("class"));
		}

		return tag;
	}

	public Object getRegex() {
		return "ordering_qty";
	}

}
