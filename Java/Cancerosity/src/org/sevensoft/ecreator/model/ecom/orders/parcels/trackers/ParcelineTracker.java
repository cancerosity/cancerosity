package org.sevensoft.ecreator.model.ecom.orders.parcels.trackers;


/**
 * @author sks 13 Nov 2006 17:54:54
 *
 */
public class ParcelineTracker extends Tracker {

	@Override
	public String getTrackingUrl(String consignment) {
		return "http://www.parceline.com/pltracking/pl_quickSearchForConsignment.do?myData.consignmentNumber=" + consignment;
	}

}
