package org.sevensoft.ecreator.model.ecom.payments.processors.protx;

/**
 * @author sks 7 Oct 2006 16:36:21
 *
 */
enum TxType {

    Authenticate("AUTHENTICATE"), Payment("PAYMENT"), Refund("REFUND");

	private String	protxName;

	private TxType(String protxName) {
		this.protxName = protxName;
	}

	public String getProtxName() {
		return protxName;
	}

	public void setProtxName(String protxName) {
		this.protxName = protxName;
	}

	@Override
	public String toString() {
		return protxName;
	}
}