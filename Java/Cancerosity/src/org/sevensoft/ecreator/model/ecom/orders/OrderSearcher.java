package org.sevensoft.ecreator.model.ecom.orders;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.QueryBuilder;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 22 Aug 2006 12:11:44
 *
 */
public class OrderSearcher {

	private String			status;
	private Order.Sort		sort;
	private String			orderId;
	private String			name;
	private String			email;
	private String			address;
	private String			postcode;
	private Money			totalTo;
	private Money			totalFrom;
	private User			salesperson;
	private boolean			webOrders;
	private int				start, limit;
	private final RequestContext	context;
	private boolean			neverExported;
	private Date			billDate;
	private PaymentType		paymentType;

	/**
	 * Include cancelled orders
	 */
	private boolean			cancelled;
	private Date			reinvoiceDate;
	private boolean			recurring;

	public OrderSearcher(RequestContext context) {
		this.context = context;
		this.cancelled = false;
	}

    public List<Order> execute() {
        return (List<Order>)execute(false);
    }

	public Object execute(boolean selectCount) {

		QueryBuilder b = new QueryBuilder(context);
        if (selectCount) {
            b.select("count(*)");
        } else {
            b.select("o.*");
        }
		b.from("# o", Order.class);
		b.from("join # a on o.account=a.id", Item.class);

		if (orderId != null) {
			b.clause("o.id like ?", "%" + orderId + "%");
		}

		if (!cancelled) {
			b.clause("o.cancelled=0");
		}

		if (name != null) {
			for (String s : name.split("\\s")) {
				b.clause("a.name like ?", "%" + s.trim() + "%");
			}
		}

		if (email != null) {
			b.clause("a.email like ?", "%" + email.trim() + "%");
		}

		if (address != null) {
			b.from("join # da on o.deliveryAddress=da.id", Address.class);
			b.clause("da.address like ?", "%" + address + "%");
		}

		if (postcode != null) {
			b.from("join # da on o.deliveryAddress=da.id", Address.class);
			b.clause("da.postcode like ?", "%" + postcode + "%");
		}

		if (totalFrom != null) {
			b.clause("o.saleTotal>=?", totalFrom);
		}

		if (totalTo != null) {
			b.clause("o.saleTotal>=?", totalTo);
		}

		if (reinvoiceDate != null) {
			b.clause("o.reinvoiceDate=?", reinvoiceDate);

		} else if (recurring) {
			b.clause("o.reinvoiceDate != null");
		}

		if (status != null) {
			b.clause("o.status=?", status);
		}
        /*else {
			b.clause("o.status!=?", "Cancelled");
		}*/

		if (neverExported) {
			b.clause("o.lastExportedOn is not null");
		}

		if (salesperson != null) {
			b.clause("o.salesperson=?", salesperson);
		}

		if (paymentType != null) {
			b.clause("o.paymentType=?", paymentType);
		}

		if (webOrders) {
			b.clause("o.salesperson=0");
		}

		if (sort != null)

			switch (sort) {

			default:
			case Oldest:
			case Reference:
				break;

			case CustomerName:
				b.order("a.name");
				break;

			case Newest:
				b.order("o.id desc");
				break;

			case ItemsAsc:
				b.order("o.lineCount asc");
				break;

			case ItemsDesc:
				b.order("o.lineCount desc");
				break;

			case Status:
				b.order("o.status");
				break;

			case TotalDesc:
				b.order("o.saleTotal desc");
				break;

			case TotalAsc:
				b.order("o.saleTotal asc");
				break;
			}

		b.order("o.id");

        if (selectCount) {
            return b.toQuery().getInt();
        }

		return b.execute(Order.class, start, limit);
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setBillDate() {
		this.billDate = new Date();
	}

	public void setCancelled(boolean b) {
		this.cancelled = b;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNeverExported(boolean neverExported) {
		this.neverExported = neverExported;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public void setReinvoiceDate(Date date) {
		this.reinvoiceDate = date;
	}

	public void setSalesperson(User salesperson) {
		this.salesperson = salesperson;
	}

	public void setSort(Order.Sort sort) {
		this.sort = sort;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTotalFrom(Money totalFrom) {
		this.totalFrom = totalFrom;
	}

	public void setTotalTo(Money totalTo) {
		this.totalTo = totalTo;
	}

	public void setWebOrders(boolean webOrders) {
		this.webOrders = webOrders;
	}

	public void setRecurring(boolean recurring) {
		this.recurring = recurring;
	}
}
