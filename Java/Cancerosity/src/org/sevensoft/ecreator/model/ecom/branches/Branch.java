package org.sevensoft.ecreator.model.ecom.branches;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.ecreator.model.misc.location.Locatable;
import org.sevensoft.ecreator.model.misc.location.LocationUtil;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 25 Mar 2007 17:06:14
 *
 */
@Table("branches")
public class Branch extends EntityObject implements Locatable, Selectable {

	public static List<Branch> get(RequestContext context) {
		Query q = new Query(context, "select * from # order by name");
		q.setTable(Branch.class);
		return q.execute(Branch.class);
	}

	/**
	 * co-ords
	 */
	private int			x, y;

	/**
	 * The name of this branch
	 */
	private String		name;

	/**
	 * The name of the location for the coords
	 */
	private String		location;

	private String		address;

	private String		postcode;

	private String		telephone;

	private List<String>	orderEmails;

	protected Branch(RequestContext context) {
		super(context);
	}

	public Branch(RequestContext context, String name) {
		super(context);
		this.name = name;
		save();
	}

	public String getAddress() {
		return address;
	}

	public int[] getCoords() {
		return new int[] { x, y };
	}

	public String getLabel() {
		return name;
	}

	public String getLocation() {
		return location;
	}

	public final String getName() {
		return name;
	}

	public final List<String> getOrderEmails() {
		if (orderEmails == null) {
			orderEmails = new ArrayList();
		}
		return orderEmails;
	}

	public String getPostcode() {
		return postcode;
	}

	public String getTelephone() {
		return telephone;
	}

	public String getValue() {
		return getIdString();
	}

	public final int getX() {
		return x;
	}

	public final int getY() {
		return y;
	}

	public boolean hasLocation() {
		return location != null;
	}

	public boolean hasOrderEmails() {
		return getOrderEmails().size() > 0;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setLocation(String location, int x, int y) {

		this.location = location;
		this.x = x;
		this.y = y;

		save();
	}

	public final void setName(String name) {
		this.name = name;
	}

	public final void setOrderEmails(List<String> orderEmails) {
		this.orderEmails = orderEmails;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
		LocationUtil.setLocation(context, this, postcode);
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

}
