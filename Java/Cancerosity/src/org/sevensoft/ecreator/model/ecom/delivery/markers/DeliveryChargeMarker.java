package org.sevensoft.ecreator.model.ecom.delivery.markers;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderMarker;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * @author sks 27 Jun 2006 11:48:11
 *         Returns just the charge of delivery
 */
public class DeliveryChargeMarker extends MarkerHelper implements IBasketMarker, IOrderMarker {

    public Object generate(RequestContext context, Map<String, String> params, Basket basket) {

        if (!Module.Delivery.enabled(context)) {
            return null;
        }

        Currency currency = (Currency) context.getAttribute("currency");

        Money money;
        if (params.containsKey("inc")) {

            money = Currency.convert(basket.getDeliveryChargeInc(), currency);

        } else if (params.containsKey("vat")) {

            money = Currency.convert(basket.getDeliveryChargeVat(), currency);

        } else if (params.containsKey("ex")) {

            money = Currency.convert(basket.getDeliveryChargeEx(), currency);

        } else {

            money = Currency.convert(basket.getDeliveryChargeEx(), currency);
        }

        return super.string(context, params, currency.getSymbol() + money);

    }

    public Object generate(RequestContext context, Map<String, String> params, Order order) {

        if (!Module.Delivery.enabled(context)) {
            return null;
        }

        if (!order.hasDeliveryDetails()) {
            return null;
        }

        Money money;
        if (params.containsKey("inc")) {

            money = order.getDeliveryChargeInc();

        } else if (params.containsKey("vat")) {

            money = order.getDeliveryChargeVat();

        } else if (params.containsKey("ex")) {

            money = order.getDeliveryChargeEx();

        } else {

            money = order.getDeliveryChargeEx();
        }

        Currency currency = Currency.getDefault(context);
        return super.string(context, params, currency.getSymbol() + money);
    }

    public Object getRegex() {
        return new String[]{"delivery_charge", "order_delivery_charge"};
    }
}
