package org.sevensoft.ecreator.model.ecom.shopping;

import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

import org.jdom.JDOMException;
import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.accounts.registration.NewsletterOptIn;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.support.AttributeSupport;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.SubmissionOwner;
import org.sevensoft.ecreator.model.crm.forms.submission.Submission;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.branches.Branch;
import org.sevensoft.ecreator.model.ecom.delivery.Deliverable;
import org.sevensoft.ecreator.model.ecom.delivery.DeliverableItem;
import org.sevensoft.ecreator.model.ecom.delivery.DeliveryOption;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.ecreator.model.ecom.orders.OrderStatusException;
import org.sevensoft.ecreator.model.ecom.orders.VatHelper;
import org.sevensoft.ecreator.model.ecom.orders.Order.Origin;
import org.sevensoft.ecreator.model.ecom.orders.emails.OrderEmail;
import org.sevensoft.ecreator.model.ecom.orders.export.OrderExport;
import org.sevensoft.ecreator.model.ecom.payments.PaymentException;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.forms.able2buy.CreditGroup;
import org.sevensoft.ecreator.model.ecom.promotions.Promotion;
import org.sevensoft.ecreator.model.ecom.shopping.ShoppingSettings.DispatchType;
import org.sevensoft.ecreator.model.ecom.vouchers.InvalidVoucherException;
import org.sevensoft.ecreator.model.ecom.vouchers.Vouchable;
import org.sevensoft.ecreator.model.ecom.vouchers.Voucher;
import org.sevensoft.ecreator.model.extras.bnr.BlackRoundPricing;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.ecreator.model.marketing.affiliates.ClickThrough;
import org.sevensoft.ecreator.model.marketing.affiliates.Conversion;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterOptable;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;

/**
 * @author Sam 09-Sep-2003 17:10:39
 */
@Table("basket")
public class Basket extends AttributeSupport implements Deliverable, Vouchable, NewsletterOptable, SubmissionOwner {

	@SuppressWarnings("hiding")
	private static Logger	logger	= Logger.getLogger(Basket.class.getName());

	public static void deleteAll(RequestContext context) {
		new Query(context, "delete from #").setTable(Basket.class).run();
		new Query(context, "delete from #").setTable(BasketLine.class).run();
		new Query(context, "delete from #").setTable(BasketLineOption.class).run();
	}

	public static void flag(RequestContext context) {
		new Query(context, "update # set recalc=1").setTable(Basket.class).run();
	}

	public static void flagAccount(RequestContext context, Item acc) {
		new Query(context, "update # set recalc=1 where account=?").setTable(Basket.class).setParameter(acc).run();
	}

	public static void flagDeliveryRate(RequestContext context, DeliveryOption rate) {
		new Query(context, "update # set recalc=1 where deliveryOption=?").setTable(Basket.class).setParameter(rate).run();
	}

	public static void flagItem(RequestContext context, Item item) {
		Query q = new Query(context, "update # b join # bl on b.id=bl.basket set b.recalc=1 where bl.item=?");
		q.setTable(Basket.class);
		q.setTable(BasketLine.class);
		q.setParameter(item);
		q.run();
	}

	public static Basket getBySession(RequestContext context, String sessionId) {
		Basket basket = SimpleQuery.get(context, Basket.class, "sessionId", sessionId);
		if (basket == null) {
			basket = new Basket(context, sessionId);
		}
		return basket;
	}

	public static List<Basket> getContaining(RequestContext context, Item item) {
		Query q = new Query(context, "select b.* from # b join # bi on b.id=bi.basket where bi.item=?");
		q.setTable(Basket.class);
		q.setTable(BasketLine.class);
		q.setParameter(item);
		return q.execute(Basket.class);
	}

    public static Basket getByOrder(RequestContext context, String order) {
		Basket basket = SimpleQuery.get(context, Basket.class, "order", order);
		return basket;
	}

	public static List<Basket> getInUse(RequestContext context) {

		Query q = new Query(context, "select * from # where itemCount>0");
		q.setTable(Basket.class);
		return q.execute(Basket.class);
	}

	/**
	 * The delivery address currently set on this basket for checkout or delivery purposes.
	 * 
	 */
	private Address		deliveryAddress;

	/**
	 * Invoice address
	 */
	private Address		billingAddress;

	private Card		card;

	/**
	 * 
	 */
	private Country		country;

	/**
	 * The total ex vat of the delivery charge currently set.
	 */
	private Money		deliveryChargeEx;

	/**
	 * The vat on the delivery charge
	 */
	private Money		deliveryChargeVat;

	/**
	 * The name of the delivery method currently set
	 */
	private String		deliveryName;

	/**
	 * The current delivery option if set. Should be delivery rate but leaving as this for
	 * backwards compat.
	 */
	private DeliveryOption	deliveryOption;

	/**
	 * Specials instructions from the customer for this order.
	 */
	private String		customerInstructions;

	private int			itemCount, lineCount;

	/**
	 * Last time this basket was displayed in millis
	 */
	private long		lastAccessTime;

	/**
	 * Flags if the instructions and attachements pages have been shown in the checkout yet.
	 */
	private boolean		offeredAttributes, offeredAttachments;

	private PaymentType	paymentType;

	/**
	 * Flags if the basket needs a recalc.
	 */
	private boolean		recalc;

	/**
	 * 
	 */
	/**
	 * 
	 */
	@Index()
	private String		sessionId;

	/**
	 * The total is the final price
	 */
	private Money		totalEx, totalInc, totalVat;

	/**
	 * The subtotal is after all lines, promotions and discounts but before shipping.
	 */
	private Money		subtotalEx, subtotalInc, subtotalVat;

	private String		customerReference;

	@Default("1")
	private boolean		vatable;

	/**
	 *  the current voucher
	 */
	private Voucher		voucher;

	private Money		deliveryChargeInc;

	/**
	 * Lines total is all items being bought, excluding voucher 
	 */
	private Money		linesEx, linesInc, linesVat;

	/**
	 * Direct item for when we are using instant buy
	 */
	private Item		item;

	/**
	 * Qty for when we are using instant buy
	 */
	private int			qty;

	/**
	 * voucher VAT discount
	 */
	private Money		voucherDiscountVat;

	private Money		voucherDiscountEx;

	private Money		voucherDiscountInc;

	private Item		account;

	private CreditGroup	able2BuyCreditGroup;

	/**
	 * Details used for non account shopping
	 */
	private String		name, email, telephone1;

	private Order		order;

	private boolean		guest;

	private double		deliveryVatRate;

	private String		vatExemptText;

	private String		referrer;

	private DispatchType	dispatchType;

	private Branch		branch;

	public Basket(RequestContext context) {
		super(context);
	}

	public Basket(RequestContext context, String sessionId) {

		super(context);

		this.sessionId = sessionId;
		this.country = Country.UK;
		this.lastAccessTime = System.currentTimeMillis();

		resetTotals();

		this.vatable = Company.getInstance(context).isVatRegistered();

		save();

		logger.fine("created new basket: " + this);
	}

	public BasketLine addItem(String ident, Item item, int qty, Map<ItemOption, String> options, Map<ItemOption, Upload> uploads) throws IOException,
			AttachmentExistsException, AttachmentTypeException {

		logger.fine("adding basket line item=" + item + ", qty=" + qty + "options=" + options + ", uploads=" + uploads);

		if (qty < 1) {
			logger.fine("resetting qty to 1 from " + qty);
			qty = 1;
		}

		BasketLine line = new BasketLine(context, this, ident, item, qty, options, uploads, sessionId);

		/*
		 * Delivery option might not be valid anymore so remove
		 */
		removeDeliveryRate();

		recalc();

		// adding an item could cause things in the checkout to be wrong so reset checkout

		resetCheckout();

		save();

		return line;
	}

	public BasketLine addPromotionLine(String desc, Item item) {

		BasketLine line = new BasketLine(context, this, item, new Money(0));
		line.setDescription(desc + ": " + item.getName());
		line.setPromotion(true);
		line.save();

		return line;

	}

	/**
	 * Adds a new promotion line. The amount should be positive, and this method will convert it to negative.
	 * 
	 */
	public BasketLine addPromotionLine(String description, Money amount) {

		BasketLine line = new BasketLine(context, this, description, amount.negative());
		line.setPromotion(true);
		line.save();

		return line;
	}

	public boolean containsIdent(String ident) {

		if (ident == null) {
			return false;
		}

		for (BasketLine line : getLines()) {
			if (ident.equals(line.getIdent())) {
				return true;
			}
		}

		return false;
	}

    public BasketLine containsItem(Item item, Map<ItemOption, String> options) {
        if (item == null) {
            return null;
        }

        List<String> newLineOptionsValues = getOptionsValues(item, options);

        for (BasketLine line : getLines()) {
            if (!line.isPromotion() && item.getId() == line.getItem().getId()) {
                List<String> optionsValues = line.getOptionsValues();
                boolean equals = true;
                for (String value : newLineOptionsValues) {
                    if (!optionsValues.contains(value)) {
                        equals = false;
                    }
                }
                if (equals) {
                    return line;
                }
            }
        }

        return null;
    }

    private List<String> getOptionsValues(Item item, Map<ItemOption, String> options) {
        List<String> newLineOptionsValues = new ArrayList<String>();
        Iterator<Map.Entry<ItemOption, String>> iter = options.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<ItemOption, String> entry = iter.next();
            ItemOption option = entry.getKey();
            if (option.getItem().equals(item)) {
                if (option.isSelection()) {
                    ItemOptionSelection selection = option.getSelectionFromId(Integer.parseInt(entry.getValue().trim()));
                    if (selection != null) {
                        newLineOptionsValues.add(selection.getText());
                    }
                }
            }
        }
        return newLineOptionsValues;
    }

    public boolean containsZeroPricedItem() {

		for (BasketLine line : getLines()) {
			if (line.getSalePriceInc().isZero()) {
				return true;
			}
		}

		return false;
	}

	@Override
	public synchronized boolean delete() {
		removeLines();
		return super.delete();
	}

	public void empty() {
		removeLines();
	}

	public CreditGroup getAble2BuyCreditGroup() {
		return (CreditGroup) (able2BuyCreditGroup == null ? null : able2BuyCreditGroup.pop());
	}

	public Item getAccount() {
		return (Item) (account == null ? null : account.pop());
	}

	public List<Attribute> getAttributes() {
		return OrderSettings.getInstance(context).getCheckoutAttributes();
	}

	public String getBasketString() {

		StringBuilder sb = new StringBuilder();
		for (BasketLine line : getLines()) {
			sb.append(line.getQty() + " x " + line.getName() + " @ \243" + line.getSalePriceInc());
		}
		return sb.toString();
	}

	public Address getBillingAddress() {
		return (Address) (billingAddress == null ? null : billingAddress.pop());
	}

	public final Branch getBranch() {
		return (Branch) (branch == null ? null : branch.pop());
	}

	public Card getCard() {
		return (Card) (card == null ? null : card.pop());
	}

	public Country getCountry() {
		return country;
	}

	public Item getCustomer() {
		return getAccount();
	}

	public String getCustomerInstructions() {
		return customerInstructions;
	}

	public String getCustomerReference() {
		return customerReference;
	}

	public List<DeliverableItem> getDeliverableItems() {

		List<DeliverableItem> items = new ArrayList();

		for (BasketLine line : getLines()) {

			if (line.hasItem()) {

				if (!line.isPromotion()) {

					items.add(line);

				}
			}
		}

		logger.fine("getting deliverable items=" + items);

		return items;
	}

	public Address getDeliveryAddress() {
		return (Address) (deliveryAddress == null ? null : deliveryAddress.pop());
	}

	public Money getDeliveryChargeEx() {
		return deliveryChargeEx;
	}

	public Money getDeliveryChargeInc() {
		return deliveryChargeVat.add(deliveryChargeEx);
	}

	public Money getDeliveryChargeVat() {
		return deliveryChargeVat;
	}

	public Country getDeliveryCountry() {

		if (deliveryAddress == null) {
			return null;
		}

		return deliveryAddress.getCountryOrig();
	}

	public String getDeliveryName() {
		return deliveryName;
	}

	/**
	 * Returns the currently selected delivery rate
	 */
	public DeliveryOption getDeliveryOption() {
		return (DeliveryOption) (deliveryOption == null ? null : deliveryOption.pop());
	}

	/**
	 * Returns a list of delivery rates appropriate to the contents of this basket.
	 * 
	 */
	public List<DeliveryOption> getDeliveryOptions() {
		return DeliveryOption.get(context, this);
	}

	/**
	 *Returns hte postcode used for delivery rate checking
	 */
	public String getDeliveryPostcode() {

		if (deliveryAddress == null) {
			return null;
		}

		return deliveryAddress.getPostcode();
	}

	/**
	 * Returns a map of options for delivery for use by the customer
	 * The key is the delivery rate ID and the value is a textual description for the customer
	 */
	public Map<String, String> getDeliveryPrices() {
		return getDeliveryPrices(false);
	}

    public Map<String, String> getDeliveryPrices(boolean exVat) {
		return DeliveryOption.getPricing(context, this, exVat);
	}

	public final DispatchType getDispatchType() {
		return dispatchType;
	}

	public final String getEmail() {

		if (account != null) {
			return getAccount().getEmail();
		}

		return email;
	}

    public final String getTelephone1() {

		if (account != null) {
			return getAccount().getMobilePhone();
		}

		return telephone1;
	}

	//	public Money getHighestDeliveryCharge() {
	//
	//		Money deliveryCharge = null;
	//		for (BasketLine line : getLines())
	//			if (line.hasItem() && line.getItem().hasDeliveryCharge())
	//				if (deliveryCharge == null || line.getItem().getDeliveryCharge().isGreaterThan(deliveryCharge))
	//					deliveryCharge = line.getItem().getDeliveryCharge();
	//		return deliveryCharge;
	//	}

	/**
	 * Returns a string with a message of how much the user must add to their basket before a free shipping option will kick in.
	 */
	public String getFreeDeliveryReqMessage() {

		if (hasDeliveryOption() && getDeliveryOption().isFreeDelivery()) {
			return null;
		}

		for (DeliveryOption option : DeliveryOption.get(context)) {

			String message = option.getFreeDelRequirementMessage(getDeliveryAddress(), 0, getItemCount(), getTotalInc(), getDeliveryChargeInc());
			if (message != null) {
				return message;
			}
		}

		return null;
	}

	public final Item getItem() {
		return (Item) (item == null ? null : item.pop());
	}

	public int getItemCount() {
		return itemCount;
	}

	/**
	 * Returns a list of items on this order. If a line contained an item with qty 3, then this would be added in 3 times
	 */
	public List<Item> getItems() {

		List<Item> items = new ArrayList<Item>();
		for (BasketLine line : getLines()) {

			if (line.hasItem() && !line.isPromotion()) {

				for (int n = 0; n < line.getQty(); n++) {
					items.add(line.getItem());
				}

			}
		}

		return items;
	}

	public long getLastAccessTime() {
		return lastAccessTime;
	}

	public BasketLine getLine(Item item) {

		for (BasketLine line : getLines()) {
			if (line.getItem().equals(item)) {
				return line;
			}
		}

		return null;
	}

	public int getLineCount() {
		return lineCount;
	}

	public List<BasketLine> getLines() {
		return SimpleQuery.execute(context, BasketLine.class, "basket", this);
	}

	public Money getLinesEx() {
		return linesEx;
	}

	public Money getLinesInc() {
		return linesInc;
	}

	public Money getLinesVat() {
		return linesVat;
	}

	public List<BasketMessage> getMessages() {
		return SimpleQuery.execute(context, BasketMessage.class, "basket", this);
	}

	public final String getName() {
		if (account != null) {
			return getAccount().getName();
		}
		return name;
	}

	public List<Newsletter> getNewsletterOptIns() {
		return NewsletterOptIn.getNewsletters(context, this);
	}

	public Order getOrder() {
		return (Order) (order == null ? null : order.pop());
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public final int getQty() {
		return qty;
	}

	public String getReferrer() {
		return referrer;
	}

	public String getSessionId() {
		return sessionId;
	}

	public int getSize() {
		return lineCount;
	}

	public Submission getSubmission(Form form) {
		return SimpleQuery.get(context, Submission.class, "basket", this, "form", form);
	}

	private List<Submission> getSubmissions() {
		return SimpleQuery.execute(context, Submission.class, "basket", this);
	}

	public Money getSubtotalEx() {
		if (recalc) {
			recalc();
		}
		return subtotalEx;
	}

	public Money getSubtotalInc() {
		if (recalc) {
			recalc();
		}
		return subtotalInc;
	}

	public Money getSubtotalVat() {
		if (recalc) {
			recalc();
		}
		return subtotalVat;
	}

	public Money getTotalEx() {
		if (recalc) {
			recalc();
		}
		return totalEx;
	}

	public Money getTotalInc() {
		if (recalc) {
			recalc();
		}
		return totalInc;
	}

	public Money getTotalVat() {
		if (recalc) {
			recalc();
		}
		return totalVat;
	}

	public Voucher getVoucher() {
		return (Voucher) (voucher == null ? null : voucher.pop());
	}

	public Money getVoucherDiscountEx() {
		if (recalc) {
			recalc();
		}
		return voucherDiscountEx;
	}

	public Money getVoucherDiscountInc() {
		if (recalc) {
			recalc();
		}
		return voucherDiscountInc;
	}

	public Money getVoucherDiscountVat() {
		if (recalc) {
			recalc();
		}
		return voucherDiscountVat;
	}

	public int getVoucherQty() {
		return itemCount;
	}

	public Money getVoucherTotal() {
		return linesInc;
	}

	public boolean hasAble2BuyCreditGroup() {
		return able2BuyCreditGroup != null;
	}

	public boolean hasAccount() {
		return account != null;
	}

	public boolean hasBillingAddress() {
		return billingAddress != null;
	}

	public boolean hasBranch() {
		return branch != null;
	}

	public boolean hasCard() {
		return card != null;
	}

	public boolean hasCustomerReference() {
		return customerReference != null;
	}

	public boolean hasDeliveryAddress() {
		return deliveryAddress != null;
	}

	public boolean hasDeliveryCountry() {
		return false;
	}

	public boolean hasDeliveryOption() {
		return deliveryOption != null;
	}

	public boolean hasDispatchType() {
		return dispatchType != null;
	}

	public boolean hasEmail() {
		return email != null;
	}

	public boolean hasInstructions() {
		return customerInstructions != null;
	}

	public boolean hasLines() {
		return lineCount > 0;
	}

	public boolean hasName() {
		return name != null;
	}

	public boolean hasOrder() {
		return order != null;
	}

	public boolean hasPaymentType() {
		return paymentType != null;
	}

	/**
	 * Returns true if we have not yet set a value on checkout attributes that require on
	 */
	public boolean hasRequiredCheckoutAttributes() {

		List<Attribute> checkoutAttributes = OrderSettings.getInstance(context).getCheckoutAttributes();
		if (checkoutAttributes.isEmpty()) {
			return false;
		}

		Map<Attribute, String> attributeValues = getAttributeValues();

		for (Attribute attribute : checkoutAttributes) {
			if (attribute.isMandatory() && !attributeValues.containsKey(attribute)) {
				return true;
			}
		}

		return false;
	}

	public boolean hasVoucher() {
		return voucher != null;
	}

	public boolean isEmpty() {

		if (recalc) {
			recalc();
		}

		return lineCount == 0;
	}

	public boolean isGuest() {
		return guest;
	}

	public boolean isNewsletterOptIn(Newsletter newsletter) {
		return NewsletterOptIn.isOptIn(context, this, newsletter);
	}

	public boolean isOfferedAttachments() {
		return offeredAttachments;
	}

	public boolean isOfferedAttributes() {
		return offeredAttributes;
	}

	public boolean isRecalc() {
		return recalc;
	}

	public boolean isUk() {
		return Country.getInstance("GB").equals(country);
	}

	public final boolean isVatable() {
		return vatable;
	}

	public boolean isVatableForDelivery() {
		return deliveryChargeVat.isPositive();
	}

	public boolean isZero() {
		return getTotalInc().isZero();
	}

	public void login(Item m) {
		setAccount(m);
	}

	public void logout() {
		setAccount(null);
	}

	public void recalc() {

		logger.fine("recalcing basket");

		// remove all promotion items as they will be readded if still valid
		removePromotionLines();

		resetTotals();

		// flag to true if this voucher is for free del
		boolean voucherFreeDel = false;

		final boolean vatReg = Company.getInstance(context).isVatRegistered();
		logger.fine("is company vat regged: " + vatReg);

        /*
        * Before vouchers we should process promotions
        */
        if (Module.Promotions.enabled(context)) {

            logger.fine("promotions enabled");

            List<Promotion> promotionsUsed = new ArrayList();
            List<Promotion> promotions = Promotion.getValid(context);
            logger.fine("current valid promotions=" + promotions);

            List<Item> items = getItems();
            logger.fine("generating quantified list of items=" + items);

            for (Promotion promotion : promotions) {

                /*do {
                    logger.fine("processing promotion=" + promotion);
                } while (promotion.process(this, promotionsUsed, items));*/
                promotion.process(this, promotionsUsed, items);
            }
        }

        // calculate all line prices first
        //calculate with promotion lines
		for (BasketLine line : getLines()) {

			line.recalc();
            if (!line.isPromotion()) {
                linesEx = linesEx.add(line.getLineSalePriceEx());
            }

            if (vatReg) {
				linesVat = linesVat.add(line.getLineSalePriceVat());
			}

			itemCount += line.getQty();
			lineCount++;

			logger.fine("linesEx=" + linesEx + ", linesVat=" + linesVat);
			logger.fine("itemCount=" + itemCount + ", lineCount=" + lineCount);
		}

        Money promotionDisc = null;
        for (BasketLine line : getLines()) {
            if (line.isPromotion()) {
                promotionDisc = line.getLineSalePriceEx();
            }
        }
        linesEx = linesEx.add(promotionDisc);

        this.linesInc = linesEx.add(linesVat);
		logger.fine("linesInc=" + linesInc);

		this.subtotalEx = linesEx;
		this.subtotalVat = linesVat;
		this.subtotalInc = linesInc;


		/*
		 * Vouchers are calculated on the lines total. They do not include shipping when calculating
		 * if the customer has spent enough.
		 * 
		 * However the vouchers do apply to the whole total and might need to reduce the shipping VAT as part of their discount.
		 * Therefore we will apply the vouchers after the shipping
		 */
		if (hasVoucher()) {

			logger.fine("voucher detected=" + voucher);

			String error = getVoucher().isValid(this);
			if (error == null) {

				logger.fine("voucher is valid!");
				if (getVoucher().isFreeDelivery()) {

					/*
					 * Defer voucher calculation until later
					 */
					voucherFreeDel = true;
					logger.fine("free del voucher");

				} else {

					/* 
					 * The actual voucher discount is calculated on the lines charge.
					 */
//					this.voucherDiscountEx = getVoucher().getDiscount(linesEx);

					/*
					 * If the discount is greater than our actual selling price, then reduce
					 */
//					if (voucherDiscountEx.isGreaterThan(linesEx)) {
//						voucherDiscountEx = linesEx;
//					}

					/* 
					 * Calculate the VAT discount using the same ratio as a the discount from the total order
					 */
//					if (vatReg) {
//						double d = (double) voucherDiscountEx.getAmount() / (double) linesEx.getAmount();
//						this.voucherDiscountVat = linesVat.multiply(d);
//					}

//					this.voucherDiscountInc = voucherDiscountEx.add(voucherDiscountVat);

					// set subtotals to take into account voucher
//					this.subtotalEx = linesEx.minus(voucherDiscountEx);
//					if (vatReg) {
//						this.subtotalVat = linesVat.minus(voucherDiscountVat);
//					}
//					this.subtotalInc = linesInc.minus(voucherDiscountInc);
				}

			} else {

				logger.fine("voucher was not valid because of error: " + error);
				this.voucher = null;
			}
		}

		/*
		 * If we have no lines on this order we should remove delivery details
		 */
		if (lineCount == 0) {
			logger.fine("no lines so removing delivery option");
			deliveryOption = null;
		}

		/*
		 * If we have a delivery option we should check it is valid with our new totals or remove it if not
		 */
		if (hasDeliveryOption()) {
			if (!getDeliveryOption().isValid(this)) {
				deliveryOption = null;
				logger.fine("delivery option is not valid, removing");
			}
		}

		/* 
		 * if we have no delivery option and no delivery band then we should set one if we have one available
		 * UPDATE: Only set a delivery option if this basket has at least one line on it !!! Otherwise we end up setting a delivery rate on an empty basket
		 * and can never get it back to zero! 
		 */
        /**
         * in case of Collection we don't need delivery options
         */
        if (deliveryOption == null && !DispatchType.Collection.equals(dispatchType) && hasLines()) {

			List<DeliveryOption> options = DeliveryOption.get(context, this);
			if (options.size() > 0) {

				deliveryOption = options.get(0);
				logger.fine("auto setting delivery option=" + deliveryOption);

			}
		}

		// if we have a delivery rate set then update delivery details
		if (hasDeliveryOption()) {

			logger.fine("delivery option set, updating delivery charges");

			this.deliveryName = deliveryOption.getName();
			this.deliveryVatRate = deliveryOption.getVatRate();

			this.deliveryChargeEx = deliveryOption.getChargeEx(this);

//			if (subtotalVat.isZero()) {
//				logger.fine("basket has no vat, overriding delivery vatrate to zero");
//				deliveryVatRate = 0;
//			}

			this.deliveryChargeVat = VatHelper.vat(context, deliveryChargeEx, deliveryVatRate);
			this.deliveryChargeInc = deliveryChargeEx.add(deliveryChargeVat);

			logger.fine("deliveryChargeEx=" + deliveryChargeEx + ", deliveryChargeVat=" + deliveryChargeVat + ", deliveryChargeInc=" + deliveryChargeInc);
		}

		/*
		 * If our voucher was for free delivery then discount delivery rate now
		 */
		if (voucherFreeDel) {

			logger.fine("calculating voucher free del");

			voucherDiscountEx = getVoucher().getDiscount(deliveryChargeEx);
			logger.fine("voucherDiscountEx=" + voucherDiscountEx);

			if (vatReg) {
				double d = (double) voucherDiscountEx.getAmount() / (double) deliveryChargeEx.getAmount();
				voucherDiscountVat = deliveryChargeVat.multiply(d);
			}

			this.voucherDiscountInc = voucherDiscountEx.add(voucherDiscountVat);

			// set subtotals to take into account voucher
			this.subtotalEx = linesEx.minus(voucherDiscountEx);
			if (vatReg) {
				this.subtotalVat = linesVat.minus(voucherDiscountVat);
			}
			this.subtotalInc = linesInc.minus(voucherDiscountInc);
		}

		/*
		 * Finally calculate the totals of the basket
		 */
		this.totalEx = subtotalEx.add(deliveryChargeEx);
		if (vatReg) {
			this.totalVat = subtotalVat.add(deliveryChargeVat);
		}

		this.totalInc = totalEx.add(totalVat);

        if (hasVoucher()) {

            if (!getVoucher().isFreeDelivery() && !getVoucher().isGetOneFree()) {
                if (!getVoucher().isSpecificItemsVoucher()) {
                    this.voucherDiscountEx = getVoucher().getDiscount(linesEx);
                } else {
                    //voucher for specific items only
                    List<Item> voucherItems = getVoucher().getItems();
                    if (!voucherItems.isEmpty()) {
                        for (BasketLine line : getLines()) {
                            if (voucherItems.contains(line.getItem())) {
                                this.voucherDiscountEx = this.voucherDiscountEx.add(getVoucher().getDiscount(line.getLineSalePriceEx()));
                            }
                        }
                    }
                }

                if (vatReg) {
                    double d = (double) voucherDiscountEx.getAmount() / (double) totalEx.getAmount();
                    this.voucherDiscountVat = totalVat.multiply(d);
                }

                this.voucherDiscountInc = voucherDiscountEx.add(voucherDiscountVat);

                this.totalEx = totalEx.minus(voucherDiscountEx);
                if (vatReg) {
                    this.totalVat = totalVat.minus(voucherDiscountVat);
                }
                this.totalInc = totalInc.minus(voucherDiscountInc);
            }
        }

        logger.fine("totalEx=" + totalEx + ", totalVat=" + totalVat + ", totalInc=" + totalInc);

		recalc = false;
		save();
	}

	/**
	 * This method runs through all the items on the order and refreshes them. 
	 * This causes them to update stock and pricing if applicable.
	 * 
	 */
	public void refresh() {

		for (BasketLine line : getLines())
			try {
				line.refresh();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JDOMException e) {
				e.printStackTrace();
			}

		if (recalc) {
			recalc();
		}
	}

	public void removeAccount() {

		logger.fine("removing account");

		if (account == null) {
			logger.fine("basket already null, exiting");
			return;
		}

		account = null;
		save("account");
//		resetCheckout(); //todo we got name and email from account and no need to reset it
	}

	public void removeAddress() {

		deliveryAddress = null;

		/*
		 * Removing the address means the delivery option might not be valid anymore as it might have been
		 * valid for that specific country, postcode only etc.
		 */
		removeDeliveryRate();

		save();
	}

	public void removeCard() {
		card = null;
	}

	private void removeDeliveryRate() {

		deliveryOption = null;

		/*
		 * Removing delivery option means total could have changed
		 */
		recalc = true;
	}

	public void removeLine(BasketLine line) {

		line.delete();

		/*
		 * Delivery option might not be valid anymore so remove
		 */
		removeDeliveryRate();

		save();
	}

	private void removeLines() {

		for (BasketLine line : getLines()) {
			removeLine(line);
		}
	}

	/**
	 * Removes all lines that contain this item.
	 * 
	 * @param item
	 */
	public void removeLines(Item item) {

		for (BasketLine line : getLines()) {
			if (line.hasItem() && line.getItem().equals(item)) {
				removeLine(line);
			}
		}

	}

	public void removeNewsletterOptIns() {
		NewsletterOptIn.deleteAll(context, this);
	}

	public void removePayment() {
		card = null;
		paymentType = null;
	}

	/**
	 * 
	 */
	public void removePaymentType() {
		this.paymentType = null;
		this.able2BuyCreditGroup = null;
		this.card = null;
		save();
	}

	/**
	 * Strips out all promotions lines ready for them to be recalculated
	 */
	public void removePromotionLines() {

		for (BasketLine line : getLines()) {
			if (line.isPromotion()) {
				line.delete();
			}
		}

		logger.fine("Removed promotion lines");
	}

	private void removeSubmissions() {
        Submission submission = SimpleQuery.get(context, Submission.class, "basket", this);
        if (submission != null) {
            submission.removeBasket();
            submission.save();
        }
	}

	/**
	 * 
	 */
	public void removeVoucher() {

		voucher = null;
		recalc = true;

		save();
	}

	/**
	 * @throws IOException 
	 * @throws OrderStatusException 
	 * @throws PaymentException 
	 * 
	 */
	public Order reserve() throws OrderStatusException, IOException, PaymentException {

		deliveryAddress = null;
		paymentType = PaymentType.Cash;

		save();

		return toOrder();
	}

	/**
	 * Reset data for when we want to begin checkout again or have changed the logged in member, and so want to refresh the checkout process
	 */
	public void resetCheckout() {

		logger.fine("resetting checkout data");

		this.offeredAttachments = false;
		this.offeredAttributes = false;

		this.billingAddress = null;
		this.deliveryAddress = null;
		this.card = null;
		this.country = null;
		this.guest = false;

		this.customerInstructions = null;
		this.customerReference = null;
		this.recalc = true;
		this.paymentType = null;

		this.order = null;
		this.email = null;
		this.name = null;
        this.telephone1 = null;

		this.dispatchType = null;

		removeAttributeValues();
		removeSubmissions();

		save();
	}

	private void resetTotals() {

		logger.fine("reseting totals");

		this.totalEx = new Money(0);
		this.totalInc = new Money(0);
		this.totalVat = new Money(0);

		this.subtotalInc = new Money(0);
		this.subtotalEx = new Money(0);
		this.subtotalVat = new Money(0);

		this.linesEx = new Money(0);
		this.linesInc = new Money(0);
		this.linesVat = new Money(0);

		this.voucherDiscountEx = new Money(0);
		this.voucherDiscountVat = new Money(0);
		this.voucherDiscountInc = new Money(0);

		this.deliveryChargeEx = new Money(0);
		this.deliveryChargeVat = new Money(0);
		this.deliveryChargeInc = new Money(0);

		this.itemCount = 0;
		this.lineCount = 0;
	}

	public final void setAble2BuyCreditGroup(CreditGroup able2BuyCreditGroup) {
		this.able2BuyCreditGroup = able2BuyCreditGroup;
	}

	public void setAccount(Item account) {

		if (ObjectUtil.equal(this.account, account)) {
			return;
		}

		this.account = account;
		logger.fine("account set to " + account);

		// as the account has changed, prices could be wrong, so set for recalc
		recalc = true;
		save();

		// as this account has changed, some fields might not be valid anymore, so reset checkout procedure
		resetCheckout();
	}

	public void setBillingAddress(Address a) {
		this.billingAddress = a;
		save("billingAddress");
		logger.fine("billing address set=" + a);
	}

	/**
	 * 
	 */
	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public void setCard(Card card) {
		this.card = card;
		save("card");
	}

	public void setCustomerInstructions(String instructions) {
		this.customerInstructions = instructions;
	}

	/**
	 * @param customerReference
	 */
	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}

	public void setDeliveryAddress(Address a) {

		this.deliveryAddress = a;

		if (a == null) {
			vatable = true;
		} else {
			vatable = a.isVatable();
		}

		// flag for recalc - delivery address could render delivery rate invalid, or change vat rate requiring re-pricing.
		recalc = true;
		save();

		logger.fine("delivery address set=" + deliveryAddress);
	}

	public void setDeliveryOption(DeliveryOption option) {

		// just set delivery rate - we will remove it if invalid on next recalc

		if (ObjectUtil.equal(this.deliveryOption, option)) {
			return;
		}

		logger.fine("setting del opt=" + option);

		this.deliveryOption = option;
		recalc = true;

		save();
	}

	public void setDispatchType(DispatchType dispatchType) {
		this.dispatchType = dispatchType;

		switch (dispatchType) {

		default:
		case Delivery:
			break;

		case Collection:
			//break;
            deliveryOption = null;
            recalc();
		}
	}

	private BasketLine addLine(String string, Money charge, double vatRate) {

		BasketLine line = new BasketLine(context, this, "Collection charge", charge);

		return line;
	}

	public final void setEmail(String email) {
		this.email = email;
	}

    public final void setTelephone(String telephone) {
		this.telephone1 = telephone;
	}

	public final void setGuest(boolean guest) {
		this.guest = guest;
		save("guest");
	}

	public void setInstructions(String s) {
		customerInstructions = s;
	}

	public final void setItem(Item item) {
		this.item = item;
	}

	public void setLastAccessTime() {
		this.lastAccessTime = System.currentTimeMillis();
		save("lastAccessTime");
	}

	public void setLineQty(BasketLine line, int qty) {

		if (qty == 0) {
			removeLine(line);
		}

		else if (line.setQty(qty)) {
			recalc();
		}
	}

	public final void setName(String name) {
		this.name = name;
	}

	public void setNewsletterOptIns(List<Newsletter> newsletters) {
		removeNewsletterOptIns();
		for (Newsletter newsletter : newsletters) {
			new NewsletterOptIn(context, this, newsletter);
		}
	}

	public void setOfferedAttachments(boolean b) {
		this.offeredAttachments = b;
	}

	public void setOfferedAttributes(boolean instructionsOffered) {
		this.offeredAttributes = instructionsOffered;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
		if (paymentType != PaymentType.CardTerminal) {
			card = null;
		}
	}

	public final void setQty(int qty) {
		this.qty = qty;
	}

	public final void setReferrer(String referrer) {
		this.referrer = referrer;
	}

	public final void setVatable(boolean vatable) {
		this.vatable = vatable;
	}

	public void setVatExemptText(String vatExemptText) {
		this.vatExemptText = vatExemptText;
	}

	public void setVoucher(Voucher voucher) throws InvalidVoucherException {

		// sets voucher and flags for recalc - we won't update voucher totals or check it is valid until we recalc
		this.voucher = voucher;
		this.recalc = true;
	}

	public void setVoucherCode(String voucherCode) throws InvalidVoucherException {

		if (voucherCode == null) {
			return;
		}

		Voucher voucher = Voucher.getByCode(context, voucherCode);
		if (voucher == null) {
			throw new InvalidVoucherException("Sorry, that voucher code cannot be found");
		}

		setVoucher(voucher);
	}

	public int size() {
		return lineCount;
	}

	public Order toOrder() throws OrderStatusException, IOException, PaymentException {

		logger.fine("placing order");

		Item accountToUse = null;

		/*
		 * If we do not have an account then we must create a temp one with the email / name we have been supplied
		 */
		if (getAccount() == null) {

            ItemType accountType = ItemType.getAccount(context);

            if (getEmail() != null) {
                accountToUse = Item.getByEmail(context, getEmail(), accountType);
            }

            if (accountToUse == null && name == null) {
                accountToUse = new Item(context, accountType, getPaymentType() + " tmpaccount", "LIVE", null);
            }
			if (accountToUse == null) {


				accountToUse = new Item(context, accountType, name, "LIVE", null);
				accountToUse.setEmail(email);
                accountToUse.setMobilePhone(telephone1);
				accountToUse.save();

				accountToUse.log("Created by checkout process for new order");

				logger.fine("created temp account=" + accountToUse);
            } else {
                if (accountToUse.getMobilePhone() == null) {
                    accountToUse.setMobilePhone(telephone1);
                    accountToUse.save();
                }
            }

		} else {

			accountToUse = getAccount();
			logger.fine("using existing account");

		}

		Map<Branch, Order> orders = new HashMap();
		for (BasketLine line : getLines()) {

			Branch branch;
			if (line.hasItem()) {
				branch = line.getItem().getBranch();
			} else {
				branch = getBranch();
			}

			Order order = orders.get(branch);
			if (order == null) {
				order = createOrder(accountToUse, branch);
				orders.put(branch, order);
			}

			order.addBasketLine(line);
		}

		Config config = Config.getInstance(context);

		if (config.isBlackRound()) {

			if (deliveryAddress == null) {

				BlackRoundPricing brp = new BlackRoundPricing(this);
				Money fittingCharge = brp.getFittingCharge();

				if (fittingCharge.isPositive()) {

					order.addDescriptionLine("Fitting charge", 1, fittingCharge);
				}
			}
		}

		// set default order to any
		order = orders.values().iterator().next();


		/*
		 * Recalc basket as a temp fix for when adding shipping
		 * ??? not sure what this is
		 */
	//	if (hasDeliveryAddress()) {
			if (hasDeliveryOption()) {

				if (config.isToys()) {

					order.setDeliveryDetails(getDeliveryOption().getDescription());
					order.setDeliveryVatRate(getDeliveryOption().getVatRate());

					order.setDeliveryCharge(getDeliveryChargeEx());
					order.setDeliveryChargeInc(getDeliveryChargeInc());
					order.setDeliveryChargeVat(getDeliveryChargeVat());

					order.save();

				} else {

					order.setDeliveryOption(getDeliveryOption());
				}
			}
	//	}

        if (hasVoucher()) {
			order.setVoucher(getVoucher());
		}

		// bill orders
		for (Order order : orders.values()) {

			if (order.getPaymentType() == PaymentType.CardTerminal && PaymentSettings.getInstance(context).isImmediateTransact()) {

				logger.fine("auto billing via processor");

				try {

					order.transactCard();

				} catch (Exception e) {

					order.cancel();
					order.log("Cancelling declined order - customer will changed details and resubmit");

					throw new PaymentException(e);
				}

			}
		}

		// add in form submissions
		for (Submission submission : getSubmissions()) {
			submission.setOrder(order);
		}

		// save basket to remember order
		save("order");

		// seperate try blocks because we don't want an error with the customers confirmation email to affect the staff's confirmation emails.

		for (Order order : orders.values()) {
            if (DispatchType.Collection.equals(dispatchType)) {

                try {

                    order.sendNotificationEmails();

                } catch (EmailAddressException e) {
                    e.printStackTrace();
                    logger.warning(e.toString());

                } catch (SmtpServerException e) {
                    e.printStackTrace();
                    logger.warning(e.toString());
                }

                try {

                    // do third party details
                    for (OrderEmail email : OrderEmail.get(context)) {
                        if (email.hasBody()) {
                            email.send(order);
                        }
                    }

                } catch (EmailAddressException e) {
                    e.printStackTrace();
                    logger.warning(e.toString());

                } catch (SmtpServerException e) {
                    e.printStackTrace();
                    logger.warning(e.toString());
                }

                ShoppingSettings shoppingSettings = ShoppingSettings.getInstance(context);

                try {

                    if (shoppingSettings.isOrderAcknowledgement()) {
                        order.sendAcknowledgementEmail();
                    }

                } catch (EmailAddressException e1) {
                    e1.printStackTrace();
                    logger.warning(e1.toString());

                } catch (SmtpServerException e1) {
                    e1.printStackTrace();
                    logger.warning(e1.toString());

                }

            }
			// do exports here if needed
			List<OrderExport> exports = OrderExport.get(context);
			logger.fine("[CheckoutHandler] exports=" + exports);

			for (OrderExport export : exports) {

				try {

					export.run(order);

				} catch (EmailAddressException e) {
					e.printStackTrace();

				} catch (SmtpServerException e) {
					e.printStackTrace();

				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			// check for affiliation
			if (Module.Affiliates.enabled(context)) {

				// check for click through for this session id
				ClickThrough click = ClickThrough.get(context, context.getSessionId(), false);
				if (click != null) {
					new Conversion(context, click.getAffiliateId(), order);
				}
			}
		}

		// if the order used a temp account and shopping settings is set to register guest accounts, then send password
		if (account == null && ShoppingSettings.getInstance(context).isCreateAccountForGuest()) {

			String password = accountToUse.setPassword();

			Email e = new Email();
			e.setTo(getEmail());
			e.setFrom(config.getServerEmail());
			e.setSubject("New account");
			e.setBody("Thank you for shopping with us. " + "We have created a new account for you to use, " +
					"so next time you shop you can simply login to save entering your details again.\n\nYour login is " + email +
					" and your password is " + password + ".\n\nWelcome to the site!");
			try {
				new EmailDecorator(context, e).send(config.getSmtpHostname());
				logger.fine("send post checkout email");
			} catch (EmailAddressException e1) {
				e1.printStackTrace();
			} catch (SmtpServerException e1) {
				e1.printStackTrace();
			}
		}

		return order;
	}

	private Order createOrder(Item accountToUse, Branch branch) {

		ShoppingSettings shoppingSettings = ShoppingSettings.getInstance(context);

		order = new Order(context, accountToUse, paymentType, Origin.Import, context.getRemoteIp());
		order.log(null, "Order created from basket");

		order.setBranch(branch);

		// ensure address is assigned to this account
		if (deliveryAddress != null) {

			getDeliveryAddress().setAccount(accountToUse);
			getDeliveryAddress().save();

			order.setDeliveryAddress(getDeliveryAddress());
			order.log(null, "Delivery address set from basket - " + deliveryAddress.getLabel());

			if (shoppingSettings.isSeparateAddresses()) {

				// ensure billing address is assigned to this account
				getBillingAddress().setAccount(accountToUse);
				getBillingAddress().save();

				order.setBillingAddress(getBillingAddress());

			} else {

				order.setBillingAddress(getDeliveryAddress());
			}

		}

		order.setPaymentType(getPaymentType());
		order.log(null, "Payment type set from basket - " + getPaymentType());

		if (card != null) {
			// ensure card is assigned to this account
			getCard().setAccount(accountToUse);
			getCard().save();

			order.setCard(getCard());
			order.log(null, "Card details set from basket - " + getCard().getLabelSecure(", "));
		}

		order.setAble2BuyCreditGroup(getAble2BuyCreditGroup());
		order.setCustomerReference(customerReference);
		order.setVatExemptText(vatExemptText);
        order.setReferrer(referrer);

        // add in attributes
		copyAttributeValuesTo(order);

        order.save();
		return order;
	}

	public void removeOrder() {
		order = null;
		save();
	}

    public static Basket getByAccount(RequestContext context, Item account) {
        Basket basket = SimpleQuery.get(context, Basket.class, "account", account);
        return basket;
    }
}