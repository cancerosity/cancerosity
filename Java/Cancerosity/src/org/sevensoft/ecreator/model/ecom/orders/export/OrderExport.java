package org.sevensoft.ecreator.model.ecom.orders.export;

import org.jdom.Document;
import org.jdom.output.XMLOutputter;
import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.ecom.branches.Branch;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.export.csv.ecreator.OrderHeadersCsv;
import org.sevensoft.ecreator.model.ecom.orders.export.csv.ecreator.OrderLinesCsv;
import org.sevensoft.ecreator.model.ecom.orders.export.csv.toysgg.ToysCsv;
import org.sevensoft.ecreator.model.ecom.orders.export.xml.OrderEcreatorXml;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sks 23 Mar 2007 19:00:09
 *         <p/>
 *         Exports an order as soon as it is created
 */
@Table("orders_exports")
public class OrderExport extends EntityObject {

    public enum Format {
        EcreatorXml, EcreatorCsv, ToysGamesGifts;
    }

    public enum Method {

        /**
         * Email the files away !
         */
        Email,

        /**
         * POST the files as strings to an URL
         */
        HttpPost,

        /**
         * FTP the files to an ftp space
         */
        Ftp,

        /**
         * Write the files out to a sub dir in feed-data
         */
        File;
    }

    public static List<OrderExport> get(RequestContext context) {
        return SimpleQuery.execute(context, OrderExport.class);
    }

    private String name;

    private Method method;
    private Format format;

    /**
     * Emails to send to with email method
     */
    private List<String> emails;

    /**
     * Path used with file method
     */
    private String filePath;

    /**
     *
     */
    private Branch branch;

    private OrderExport(RequestContext context) {
        super(context);
    }

    public OrderExport(RequestContext context, Method method) {
        super(context);

        this.method = method;
        this.format = Format.EcreatorXml;
        this.name = format.toString();

        save();
    }

    private void email(Map<String, File> files) throws EmailAddressException, SmtpServerException {

        Config config = Config.getInstance(context);

        Email e = new Email();

        e.setSubject("Order export");
        e.setFrom(config.getServerEmail());

        for (Map.Entry<String, File> entry : files.entrySet()) {
            e.addAttachment(entry.getValue(), entry.getKey());
        }

        e.setRecipients(getEmails());
        e.setBody("Order export, files attached");

        if (branch != null) {
            e.addRecipients(getBranch().getOrderEmails());
        }

        e.addBcc("sks@7soft.co.uk");

        logger.fine("[OrderExport] sending email=" + e);

        new EmailDecorator(context, e).send(config.getSmtpHostname());

        // delete files
        SimpleFile.deleteFiles(files.values());
    }

    private void file(Map<String, File> files) throws FileNotFoundException, IOException {

        // copy the temp files to the filename inside feed-data
        for (Map.Entry<String, File> entry : files.entrySet()) {

            File dest = ResourcesUtils.getRealFeedData(entry.getKey());
            File src = entry.getValue();

            SimpleFile.copy(src, dest);

            src.delete();
        }
    }

    public Branch getBranch() {
        return (Branch) (branch == null ? null : branch.pop());
    }

    public final List<String> getEmails() {
        if (emails == null) {
            emails = new ArrayList();
        }
        return emails;
    }

    public final Format getFormat() {
        return format == null ? Format.EcreatorXml : format;
    }

    public final Method getMethod() {
        return method == null ? Method.Email : method;
    }

    public final String getName() {
        return name;
    }

    public boolean hasBranch() {
        return branch != null;
    }

    public void run(Order order) throws IOException, EmailAddressException, SmtpServerException {

        logger.fine("[OrderExport] running export for order=" + order);

        // chekc branch is correct
        if (order.hasBranch() && branch != null && !order.getBranch().equals(getBranch())) {
            logger.fine("[OrderExport] branches do not match, exiting");
            return;
        }

        Map<String, File> files = new HashMap();

        logger.fine("[OrderExport] format=" + getFormat());

        /*
           * Generate the file in the format set in the order export
           */
        switch (getFormat()) {

            case ToysGamesGifts:

                // write data to temp file
                File toysFile = File.createTempFile("toysFile", null);
                toysFile.deleteOnExit();

                new ToysCsv(context, order).output(toysFile);
                files.put("order.csv", toysFile);

                break;

            case EcreatorCsv:

                File headerFile = File.createTempFile("OrderExport-EcreatorCsv-Header", null);
                headerFile.deleteOnExit();

                File linesFile = File.createTempFile("OrderExport-EcreatorCsv-Lines", null);
                linesFile.deleteOnExit();

                new OrderHeadersCsv(context, order).output(headerFile);
                new OrderLinesCsv(context, order).output(linesFile);

                files.put("headers.csv", headerFile);
                files.put("lines.csv", linesFile);

                break;

            case EcreatorXml:

                // write data to temp file
                File ecreatorXmlFile = File.createTempFile("OrderExport-EcreatorXml", null);
                ecreatorXmlFile.deleteOnExit();

                Document doc = new OrderEcreatorXml(order).getDoc();

                XMLOutputter outputter = new XMLOutputter();
                outputter.setFormat(org.jdom.output.Format.getPrettyFormat());
                outputter.output(doc, new FileWriter(ecreatorXmlFile));

                files.put(order.getIdString() + ".xml", ecreatorXmlFile);
                break;
        }

        logger.fine("[OrderExport] method=" + getMethod());

        /*
           * Now send the file using the selected method
           */
        switch (getMethod()) {

            default:
                break;

            case File:
                file(files);
                break;

            case Email:
                email(files);
        }

    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public final void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public final void setFormat(Format format) {
        this.format = format;
    }

    public final void setMethod(Method method) {
        this.method = method;
    }

    public final void setName(String name) {
        this.name = name;
    }
}
