package org.sevensoft.ecreator.model.ecom.payments.forms.paypal;

import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.io.*;
import java.net.URLEncoder;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.URLDecoder;

/**
 * @author Dmitry Lebedev
 *         Date: 11.11.2009
 */
public class PayPalDirect extends Form {

	private static final String	SANDBOX_URL	= "https://www.sandbox.paypal.com/cgi-bin/webscr";

	private static final String	URL		= "https://www.paypal.com/cgi-bin/webscr";

	private static final String	gv_Version	= "51.0";

	public PayPalDirect(RequestContext context) {
		super(context);
	}

	@Override
	public String getMethod() {
		return "POST";
	}

	@Override
	public String getPaymentUrl() {
		if (PaymentSettings.getInstance(context).isPayPalDirectLive()) {
			return URL;
		} else {
			return SANDBOX_URL;
		}
	}

	public Map<String, String> getParams(Payable payable) throws IOException, FormException {
        FormSession session = new FormSession(context, payable);
        Config config = Config.getInstance(context);

        //Map values from which are used in request to PayPal web site
        Map<String, String> map = new LinkedHashMap<String, String>();
		// Map with values from response from PayPal web site
		Map<String, String> m;

		String currencyCode = "";

		if (Currency.getDefault(context).getCode() != null && !Currency.getDefault(context).getCode().equals("")) {
			currencyCode = Currency.getDefault(context).getCode();
		} else {
			currencyCode = "GBP";
		}

        String nvpStr = "&Amt=" + payable.getPaymentAmount().toEditString() + "&PAYMENTACTION=Sale&ReturnUrl=" +
                URLEncoder.encode(payable.getPaymentSuccessUrl(PaymentType.PayPalDirect)) + "&CANCELURL=" +
                URLEncoder.encode(payable.getPaymentFailureUrl(PaymentType.PayPalExpress)) +
                "&CURRENCYCODE=" + currencyCode + "&FIRSTNAME=" + payable.getPayableAccount().getFirstName() + "&LASTNAME=" +
                payable.getPayableAccount().getLastName() + "&EMAIL=" + payable.getPayableAccount().getEmail() + "&STREET=" + payable.getPaymentAddress().getAddressLine1() + payable.getPaymentAddress().getAddressLine2() +
                payable.getPaymentAddress().getAddressLine3() + "&CITY=" + payable.getPaymentAddress().getTown() + "&STATE=" + payable.getPaymentAddress().getCounty() +
                "&COUNTRYCODE=GB" + "&ZIP=" + payable.getPaymentAddress().getPostcode()/* + "&SHIPTOSTREET=" + payable.getPaymentAddress().getAddressLine1() +
                "&SHIPTOCITY=" + payable.getPaymentAddress().getTown() + "&SHIPTOSTATE=" + payable.getPaymentAddress().getCounty() + "&SHIPTOCOUNTRYCODE=GB" +
                "&SHIPTOZIP=" + payable.getPaymentAddress().getPostcode()*/;

		m = httpCall("DoDirectPayment", nvpStr);

		return m;
	}

	public HashMap httpCall(String methodName, String nvpStr) {

        String encodedData = "TRXTYPE=S&TENDER=C&PARTNER=PayPalUK&METHOD=" + methodName + "&VERSION=" + gv_Version +
                "&PWD=" + PaymentSettings.getInstance(context).getPayPalDirectAccountPassword() +
                "&USER=" + PaymentSettings.getInstance(context).getPayPalDirectAccountUsername() +
                "&SIGNATURE=" + PaymentSettings.getInstance(context).getPayPalDirectAccountSignature() +
                "&ACCT=" + context.getParameter("cardNumber") + "&EXPDATE=" + context.getParameter("expiryMonth") +
                context.getParameter("expiryYear") + "&CREDITCARDTYPE=" + context.getParameter("cardType") + "&CVV2=" +
                context.getParameter("csc")+ nvpStr;

		String respText = "";
		HashMap nvp = new HashMap();

		try {
			URL postURL;
			if (PaymentSettings.getInstance(context).isPayPalDirectLive()) {
				postURL = new URL("https://api-3t.paypal.com/nvp");
            }
            else {
				postURL = new URL("https://api-3t.sandbox.paypal.com/nvp");
			}
			HttpURLConnection conn = (HttpURLConnection) postURL.openConnection();

			conn.setDoInput(true);
			conn.setDoOutput(true);

			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

			conn.setRequestProperty("Content-Length", String.valueOf(encodedData.length()));
			conn.setRequestMethod("POST");

			DataOutputStream output = new DataOutputStream(conn.getOutputStream());
			output.writeBytes(encodedData);
			output.flush();
			output.close();

			DataInputStream in = new DataInputStream(conn.getInputStream());
			int rc = conn.getResponseCode();
			if (rc != -1) {
				BufferedReader is = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String _line = null;
				while (((_line = is.readLine()) != null)) {
					respText = respText + _line;
				}
				nvp = deformatNVP(respText);
			}
			return nvp;

		} catch (IOException e) {
			return null;
		}
	}

	public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
	}

	public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
		return null;
	}

	public HashMap deformatNVP(String pPayload) {
		HashMap nvp = new HashMap();
		StringTokenizer stTok = new StringTokenizer(pPayload, "&");
		while (stTok.hasMoreTokens()) {
			StringTokenizer stInternalTokenizer = new StringTokenizer(stTok.nextToken(), "=");
			if (stInternalTokenizer.countTokens() == 2) {
				String key = URLDecoder.decode(stInternalTokenizer.nextToken());
				String value = URLDecoder.decode(stInternalTokenizer.nextToken());
				nvp.put(key.toUpperCase(), value);
			}
		}
		return nvp;
	}
}
