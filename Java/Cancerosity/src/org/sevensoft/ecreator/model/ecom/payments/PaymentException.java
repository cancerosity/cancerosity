package org.sevensoft.ecreator.model.ecom.payments;

/**
 * @author sks 15 Feb 2007 11:21:44
 *
 */
public class PaymentException extends Exception {

	public PaymentException() {
	}

	public PaymentException(String message) {
		super(message);
	}

	public PaymentException(String message, Throwable cause) {
		super(message, cause);
	}

	public PaymentException(Throwable cause) {
		super(cause);
	}

}
