package org.sevensoft.ecreator.model.ecom.suppliers;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 17-Mar-2006 08:28:43
 * 
 *
 * 
 */
@Table("items_suppliers_sku")
public class Sku extends EntityObject {

	@Index()
	private Item	item;

	@Index()
	private Supplier	supplier;

	@Index()
	private String	sku;

	private Sku(RequestContext context) {
		super(context);
	}

	public Sku(RequestContext context, Supplier supplier, Item item, String sku) {
		super(context);

		this.sku = sku;
		this.item = item;
		this.supplier = supplier;

		save();

	}

	public Item getItem() {
		return item.pop();
	}

	public String getSku() {
		return sku;
	}

	public Supplier getSupplier() {
		return supplier.pop();
	}
}
