package org.sevensoft.ecreator.model.ecom.orders.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderMarker;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * User: Tanya
 * Date: 24.02.2011
 */
public class IdOrderMarker extends MarkerHelper implements IOrderMarker {

    public Object generate(RequestContext context, Map<String, String> params, Order order) {

        return super.string(context, params, order.getIdString());
    }

    @Override
    public String getDescription() {
        return "eCreator ID for the order";
    }

    public Object getRegex() {
        return "id_order";
    }

}
