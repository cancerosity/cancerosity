package org.sevensoft.ecreator.model.ecom.delivery;

import org.sevensoft.commons.skint.Money;

/**
 * @author sks 21 Aug 2006 13:34:14
 *
 */
public interface DeliverableItem {

	/**
	 * Returns the value of the delivery attribute for this item 
	 */
	public int getDeliveryAttributeValue();

	public Money getDeliveryRate(DeliveryOption option);

	/**
	 * An extra charge 
	 */
	public Money getDeliverySurcharge();

	/**
	 * Price total for the purposes of calc d rate
	 */
	public Money getPriceForDelivery();

	/**
	 * Qty for the purposes of calculating delivery rate
	 */
	public int getQtyForDelivery();

	/**
	 * 
	 */
	public double getWeightForDelivery();

	/**
	 * Include this item in the delivery calculations
	 */
	public boolean isIncludedForDelivery();

}
