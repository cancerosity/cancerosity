package org.sevensoft.ecreator.model.ecom.delivery;

import java.util.SortedSet;
import java.util.TreeSet;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sam2 Nov 12, 2003 2:00:43 AM
 */
@Table("settings_delivery")
@Singleton
public class DeliverySettings extends EntityObject {

	public static DeliverySettings getInstance(RequestContext context) {
		return getSingleton(context, DeliverySettings.class);
	}

	private boolean		overseas;

	/**
	 * Use a surcharge on an item which will allow us to enter a price in addition to the options
	 */
	private boolean		surcharges;

	/**
	 * Calculate estimated delivery date
	 */
	private boolean		estimateDueDate;

	/**
	 * 
	 */
	private SortedSet<Date>	nonShippingDates;

	/**
	 * Allows us to override the qty used by an item for shipping purposes
	 */
	private boolean		overrideItemShippingQtys;

	/**
	 * Show a message to customer informing how much extra they must spend to qualify for free delivery.
	 */
	private boolean		showFreeDeliveryRequirement;

	private boolean		codes;

    @Default("1")
    private boolean					taxInc=true;

	public DeliverySettings(RequestContext context) {
		super(context);
	}

	public SortedSet<Date> getNonShippingDates() {
		if (nonShippingDates == null) {
			nonShippingDates = new TreeSet();
		}
		return nonShippingDates;
	}

	public boolean isCodes() {
		return codes;
	}

	public boolean isEstimateDueDate() {
		return estimateDueDate;
	}

	public boolean isOverrideItemShippingQtys() {
		return overrideItemShippingQtys;
	}

	public boolean isOverseas() {
		return overseas;
	}

	public boolean isShowFreeDeliveryRequirement() {
		return showFreeDeliveryRequirement;
	}

	public boolean isSurcharges() {
		return surcharges;
	}

    public boolean isTaxInc() {
        return taxInc;
    }

    public final void setCodes(boolean codes) {
		this.codes = codes;
	}

	public void setEstimateDueDate(boolean calculateDueDate) {
		this.estimateDueDate = calculateDueDate;
	}

	public void setOverrideItemShippingQtys(boolean overrideItemShippingQtys) {
		this.overrideItemShippingQtys = overrideItemShippingQtys;
	}

	public void setOverseas(boolean overseas) {
		this.overseas = overseas;
	}

	public void setShowFreeDeliveryRequirement(boolean showFreeDeliveryRequirement) {
		this.showFreeDeliveryRequirement = showFreeDeliveryRequirement;
	}

	public void setSurcharges(boolean surcharges) {
		this.surcharges = surcharges;
	}

    public void setTaxInc(boolean taxInc) {
        this.taxInc = taxInc;
    }

    @Override
	protected void singletonInit(RequestContext context) {
		this.overseas = false;
	}
}