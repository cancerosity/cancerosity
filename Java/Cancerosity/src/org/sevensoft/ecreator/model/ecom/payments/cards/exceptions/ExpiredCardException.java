package org.sevensoft.ecreator.model.ecom.payments.cards.exceptions;


/**
 * @author sks 26-Oct-2005 10:57:51
 *
 */
public class ExpiredCardException extends Exception  {

	public ExpiredCardException() {
	}

	public ExpiredCardException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExpiredCardException(String message) {
		super(message);
	}

	public ExpiredCardException(Throwable cause) {
		super(cause);
	}

	
}
