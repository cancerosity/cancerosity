package org.sevensoft.ecreator.model.ecom.suppliers;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.feeds.Feed;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.pricing.costs.SupplierCostPrice;
import org.sevensoft.ecreator.model.items.stock.SupplierStock;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.QueryBuilder;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 17-Mar-2006 08:29:19
 *
 */
@Table("suppliers")
public class Supplier extends EntityObject implements Selectable {

	public static List<Supplier> get(RequestContext context) {
		Query q = new Query(context, "select * from # where deleted=0 order by name");
		q.setTable(Supplier.class);
		return q.execute(Supplier.class);
	}

	public static Supplier get(RequestContext context, String name, boolean create) {
		Query q = new Query(context, "select * from # where deleted=0 and name=?");
		q.setTable(Supplier.class);
		q.setParameter(name);
		Supplier supplier = q.get(Supplier.class);
		if (supplier == null)
			supplier = new Supplier(context, name);
		return supplier;
	}

	private String	name;
	private Date	dateCreated;
	private boolean	deleted;

	public Supplier(RequestContext context) {
		super(context);
	}

	public Supplier(RequestContext context, String name) {
		super(context);

		this.name = name;
		this.dateCreated = new Date();

		save();
	}

	@Override
	public synchronized boolean delete() {

		SimpleQuery.delete(context, Sku.class, "supplier", this);
		SimpleQuery.delete(context, SupplierCostPrice.class, "supplier", this);
		SimpleQuery.delete(context, SupplierStock.class, "supplier", this);

		for (Feed feed : Feed.getAll(context)) {
			new Query(context, "update # set supplier=0 where supplier=?").setTable(feed.getClass()).setParameter(this).run();
		}

		return super.delete();
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	/**
	 * Returns the item for this supplier sku
	 * 
	 * sku cannot be null
	 * null status will search for all status.
	 */
	public Item getItem(String sku, String status) {

		QueryBuilder b = new QueryBuilder(context);
		b.select("i.*");
		b.from("# i", Item.class);
		b.from("join # ss on i.id=ss.item", Sku.class);
		b.clause("ss.supplier=?", this);
		b.clause("ss.sku=?", sku);

		if (status != null) {
			b.clause("i.status=?", status);
		}

		return b.get(Item.class);
	}

	public List<Item> getItems(RequestContext context, String status) {

		QueryBuilder b = new QueryBuilder(context);
		b.select("i.*");
		b.from("# i", Item.class);
		b.from("join # ss on i.id=ss.item", Sku.class);
		b.clause("ss.supplier=?", this);

		if (status != null) {
			b.clause("i.status=?", status);
		}

		return b.toQuery().execute(Item.class);
	}

	public String getLabel() {
		return getName();
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return getIdString();
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public void setName(String name) {
		this.name = name;
	}

}
