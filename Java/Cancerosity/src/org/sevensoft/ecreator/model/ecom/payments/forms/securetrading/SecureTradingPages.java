package org.sevensoft.ecreator.model.ecom.payments.forms.securetrading;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.ecom.payments.results.Callback;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 15-Dec-2005 09:24:48
 * 
 * A test account is available which can be used to ensure that your integration is set up properly. This account is available for use whether you are
 * registered with SECPay or not! Below are the details pertaining to this account: � 
 * 
 * Username: secpay � 
 * Login Password: secpay � 
 * Remote Password:
 * secpay � 
 * Digest Key: secpay
 * 
 * This is a �bucket� development account and is usually in use by several developers at any one time. 
 * Please feel free to log in to the Merchant
 * Extranet at https://www.secpay.com/lookup to perform a transaction report and see your own and others� test transactions. ? 
 * 
 * Note: You can test your
 * integration using your own account (for example: abcdef01) through the use of the test_status optional parameter. 
 * 
 * The �secpay� test account is
 * useful for developers who wish to perform an integration before having registered with SECPay � often as part of a �proof of concept� demonstration
 * for clients or management.
 * 
 * 10.1. Test Credit Card Numbers
 * 
 * Below are some test credit card numbers. These can be used with any name and address details, any CV2 3-or-4 digit security code, and any expiry
 * date which is in the future.
 * 
 * Visa 4444333322221111 4111111111111111 Master Card 5555555555554444 5105105105105100
 * 
 * You can of course make an attempt to use an expiry date which is set in the past in order to see what happens in that event!
 * 
 * 
 */
public class SecureTradingPages extends Form {

	public SecureTradingPages(RequestContext context) {
		super(context);
	}

	@Override
	public String getMethod() {
		return "POST";
	}

	@Override
	public Map<String, String> getParams(Payable payable) {

		final Company company = Company.getInstance(context);
		PaymentSettings settings = PaymentSettings.getInstance(context);
		final Config config = Config.getInstance(context);

		FormSession session = new FormSession(context, payable);

		Map<String, String> params = new HashMap<String, String>();
		params.put("merchant", settings.getSecureTradingPagesMerchant());

		// our email
		params.put("merchantemail", settings.getSecureTradingPagesEmail());

		params.put("amount", payable.getPaymentAmount().getAmountString());

		// fields we require the customer to fill in on secure tradings site
		params.put("requiredfields", "name,email,postcode");
		params.put("email", payable.getPayableAccount().getEmail());

		// our order ref, I will use session id
		params.put("orderref", session.getIdString());

		// currency
		params.put("currency", "GBP");

		// description line
		params.put("orderinfo", company.getName() + " - " + payable.getPaymentDescription());

		// the sites url
		params.put("url", config.getUrl());

		// callback url is specified by the line number of callback.txt !!
		params.put("callbackurl", settings.getSecureTradingCallbackLine());

        params.put("failureurl", settings.getSecureTradingFailedCallbackLine());

        params.put("name", payable.getPayableAccount().getName());

		Address address = payable.getPaymentAddress();
		if (address != null) {

			params.put("address", address.getAddressLine1());
			params.put("town", address.getTown());
			params.put("postcode", address.getPostcode());
			params.put("country", address.getCountry().toString());
			params.put("telephone", address.getTelephone1());

		}

        if (settings.getSecureTradingFormref() != null) {
            params.put("formref", settings.getSecureTradingFormref());
        }

        logger.fine("[SecureTradingPages=" + context.getSessionId() + "] getParams()=" + params);

		return params;
	}

	@Override
	public String getPaymentUrl() {
		return "https://securetrading.net/authorize/form.cgi";
	}

	@Override
	public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
	}

	@Override
	public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
        log("[SecureTrading] Server callback params=" + params);

		new Callback(context, PaymentType.SecureTradingPages, params);

		FormSession session = EntityObject.getInstance(context, FormSession.class, params.get("orderref"));
		if (session == null) {
			return null;
		}

		Money amount = new Money(Integer.parseInt(params.get("amount")));
		String cc = "************" + params.get(" String trans_id");
		String stauthcode = params.get("stauthcode");
		String txn = params.get("streference");
		String result = params.get("stresult");
		if (!"1".equals(result)) {
			return null;
		}

		Payment payment = new Payment(context, session.getAccount(), amount, PaymentType.SecureTradingPages);
		payment.setDetails(cc);
		payment.setProcessorTransactionId(txn);
		payment.setAuthCode(stauthcode);
		payment.save();

		session.callback(params, ipAddress, payment);
		return null;
	}
}
