package org.sevensoft.ecreator.model.ecom.payments.forms.paypal;

import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.results.Callback;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.iface.callbacks.PaypalProCallbackHandler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.commons.skint.Money;

import java.util.Map;
import java.util.LinkedHashMap;
import java.io.IOException;

/**
 * User: Tanya
 * Date: 07.05.2012
 */
public class PayPalProHosted extends Form {

    private static final String SANDBOX_URL = "https://securepayments.sandbox.paypal.com/acquiringweb";
    private static final String LIVE_URL = "https://securepayments.paypal.com/acquiringweb?cmd=_hosted-payment";

    public PayPalProHosted(RequestContext context) {
        super(context);
    }

    public String getMethod() {
        return "post";
    }

    public Map<String, String> getParams(Payable payable) throws IOException, FormException {
        FormSession session = new FormSession(context, payable);
        PaymentSettings paymentSettings = PaymentSettings.getInstance(context);
        // Map values from which are used in request to PayPal web site
        Map<String, String> map = new LinkedHashMap<String, String>();

        map.put("cmd", "_hosted-payment");

        map.put("paymentaction", "sale");

        map.put("business", paymentSettings.getPayPalProHostedMerchantId());

        map.put("currency_code", Currency.getDefault(context).getName());

        map.put("template", "templateD");      //todo Enum

        map.put("billing_first_name", payable.getPayableAccount().getFirstName());

        map.put("billing_last_name", payable.getPayableAccount().getLastName());
        map.put("billing_address1", payable.getPaymentAddress().getAddressLine1());
        map.put("billing_address2", payable.getPaymentAddress().getAddressLine2());
        map.put("billing_city", payable.getPaymentAddress().getTown());
        map.put("billing_state", payable.getPaymentAddress().getCounty());
        map.put("billing_zip", payable.getPaymentAddress().getPostcode());
        map.put("billing_country", payable.getPaymentAddress().getCountry().getIsoAlpha2());

        Order order = (Order) payable;

        map.put("first_name", order.getAccount().getFirstName());
        map.put("last_name", order.getAccount().getLastName());
        map.put("address1", order.getDeliveryAddress().getAddressLine1());     //todo delivery address?
        map.put("address2", order.getDeliveryAddress().getAddressLine2());
        map.put("city", order.getDeliveryAddress().getTown());
        map.put("state", order.getDeliveryAddress().getCounty());
        map.put("zip", order.getDeliveryPostcode());
        map.put("night_phone_a", order.getDeliveryAddress().getTelephone());
        map.put("country", order.getDeliveryCountry().getIsoAlpha2());

        map.put("subtotal", order.getItemsSubtotal().toEditString());
        map.put("shipping", order.getDeliveryChargeEx().toEditString());
        map.put("tax", order.getTotalVat().toEditString());

        map.put("custom", session.getTransactionId());
        map.put("return", payable.getPaymentSuccessUrl(PaymentType.PayPalProHosted));
        map.put("cancel_return", payable.getPaymentFailureUrl(PaymentType.PayPalProHosted));
        map.put("notify_url", Config.getInstance(context).getUrl() + "/" + new Link(PaypalProCallbackHandler.class));

        log("[PayPalProHosted] params to post = " + map);

        return map;
    }

    public String getPaymentUrl() {
        if (PaymentSettings.getInstance(context).isPayPalProHostedLive())
            return LIVE_URL;
        return SANDBOX_URL;
    }

    public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
    }

    public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
        log("[PayPalProHosted] server callback, params=" + params);

        logger.severe("[PayPalProHosted] server callback, params=" + params);

        new Callback(context, PaymentType.PayPalProHosted, params);

//		verifyIpn(params);

//		logger.severe("[PaypalStandard] IPN verified");

        /*
           * Get session id from custom field
           */
        FormSession session = FormSession.get(context, params.get("custom"));
        if (session == null) {
            log("[PayPalProHosted] no session found for id=" + params.get("custom"));
            logger.severe("[PayPalProHosted] no session found for id=" + params.get("custom"));
            return null;
        }

        Payment payment = getPayment(session, params);
        if (payment == null) {
            log("[PayPalProHosted] no payment found/created for user=" + session.getAccount() + " and params=" + params);
            logger.severe("[PayPalProHosted] no payment found/created for user=" + session.getAccount() + " and params=" + params);
            return null;
        }

        log("[PayPalProHosted] calling callback on session=" + session);
        logger.severe("[PayPalProHosted] calling callback on session=" + session);

        return session.callback(params, ipAddress, payment);
    }

    private Payment getPayment(FormSession session, Map<String, String> params) {

        String transId = params.get("txn_id");
        String amount = params.get("mc_gross");
        if (transId == null) {
            return null;
        }

        // Check for duplicated payments!!!!
        if (Payment.exists(context, transId, PaymentType.PayPalProHosted)) {
            return null;
        }

        Payment payment = new Payment(context, session.getAccount(), new Money(amount), PaymentType.PayPalProHosted);
        payment.setProcessorTransactionId(transId);
        payment.setPaymentType(PaymentType.PayPalProHosted);

        payment.save();

        return payment;
    }
}
