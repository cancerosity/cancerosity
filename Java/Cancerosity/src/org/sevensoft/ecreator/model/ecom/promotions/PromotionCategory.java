package org.sevensoft.ecreator.model.ecom.promotions;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 14 Jul 2006 09:46:06
 *
 */
@Table("promotions_categories")
public class PromotionCategory extends EntityObject {

	private Promotion	promotion;
	private Category	category;

	public PromotionCategory(RequestContext context) {
		super(context);
	}

	public PromotionCategory(RequestContext context, Promotion promotion, Category category) {
		super(context);

		if (!promotion.getCategories().contains(category)) {
			
			this.promotion = promotion;
			this.category = category;

			save();
		}
	}

}
