package org.sevensoft.ecreator.model.ecom.addresses.panels;

import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.gaia.UsState;
import org.sevensoft.ecreator.iface.frontend.misc.FieldInputCell;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.addresses.AddressFields;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 18-Apr-2006 16:10:15
 *
 */
public class AddressEntryPanel {

	public static final String	ID	= "AddressEntryPanel";

	/**
	 * 
	 */
	private boolean			instructions;
	/**
	 * 
	 */
	private boolean			countries;
	/**
	 * 
	 */
	private boolean			addressType;

	private final RequestContext	context;

	/**
	 * 
	 */
	private final String		nameDescription;
	/**
	 * 
	 */
	private final String		telephoneDescription1;
	/**
	 * 
	 */
	private final String		telephoneDescription2;
	/**
	 * 
	 */
	/**
	 * 
	 */
	private final String		telephoneDescription3;
	/**
	 * 
	 */
	private final String		countryDescription;
	/**
	 * 
	 */
	/**
	 * 
	 */
	private final String		townDescription;
	/**
	 * 
	 */
	private final String		postcodeDescription;
	/**
	 * 
	 */
	private final String		addressDescription1;
	/**
	 * 
	 */
	private final String		addressDescription3;
	/**
	 * 
	 */
	private final String		addressDescription2;

	/**
	 * 
	 */
	private final String		telephoneLabel1;

	/**
	 * 
	 */
	private final String		telephoneLabel3;

	/**
	 * 
	 */
	private final String		telephoneLabel2;

	private String			county;
	private String			postcode;
	private Country			country;

	private String			addressLine3;
	private String			addressLine2;

	/**
	 * Value
	 */
	private String			addressLine1;

	/**
	 * Value
	 */
	private String			name;

	/**
	 * Value 
	 */
	private String			town;
	private final String		addressLabel1;
	private final String		addressLabel2;
	private final String		addressLabel3;
	private final String		telephoneField3;
	private final String		telephoneField2;
	private final String		telephoneField1;
	private final String		nameField;
	private final String		addressField2;
	private final String		townField;
	private final String		addressField3;
	private final String		addressField1;
	private final String		postcodeField;
	private final String		countryField;

	/**
	 * Values from address
	 */
	private String			telephone1, telephone2, telephone3;

	private int				cssId;
	private String			legend;
	private String			countyLabel;
    private String			state;
	private final String		countyField;
	private final String		countyDescription;
	private final String		countryLabel;
	private final String		postcodeLabel;
	private final String		townLabel;
	private final String		nameLabel;
    private final String stateLabel;
    private final String stateField;
    private final String stateDescription;

	public AddressEntryPanel(RequestContext context, AddressFields fields) {

		this.context = context;

		this.nameLabel = fields.getNameLabel();
		this.nameField = "name";
		this.nameDescription = fields.getNameDescription();

		this.addressField1 = "address1";
		this.addressField2 = "address2";
		this.addressField3 = "address3";

		this.addressLabel1 = fields.getAddressLabel1();
		this.addressLabel2 = fields.getAddressLabel2();
		this.addressLabel3 = fields.getAddressLabel3();

		this.addressDescription1 = fields.getAddressDescription1();
		this.addressDescription2 = fields.getAddressDescription2();
		this.addressDescription3 = fields.getAddressDescription3();

		this.telephoneField1 = "telephone1";
		this.telephoneField2 = "telephone2";
		this.telephoneField3 = "telephone3";

		this.telephoneLabel1 = fields.getTelephoneLabel1();
		this.telephoneLabel2 = fields.getTelephoneLabel2();
		this.telephoneLabel3 = fields.getTelephoneLabel3();

		this.telephoneDescription1 = fields.getTelephoneDescription1();
		this.telephoneDescription2 = fields.getTelephoneDescription2();
		this.telephoneDescription3 = fields.getTelephoneDescription3();

		this.countyLabel = fields.getCountyLabel();
		this.countyField = "county";
		this.countyDescription = null;

        this.stateLabel = fields.getStateLabel();
        this.stateField = "state";
        this.stateDescription = fields.getStateDescription();
        
		this.townLabel = fields.getTownLabel();
		this.townField = "town";
		this.townDescription = fields.getTownDescription();

		this.postcodeLabel = fields.getPostcodeLabel();
		this.postcodeField = "postcode";
		this.postcodeDescription = fields.getPostcodeDescription();

		this.countryLabel = fields.getCountryLabel();
		this.countryField = "country";
		this.countryDescription = null;
	}

	/**
	 *  Populate from an existing address data
	 */
	public void setAddress(Address address) {

		if (address != null) {

			this.name = address.getName();
			this.addressLine1 = address.getAddressLine1();
			this.addressLine2 = address.getAddressLine2();
			this.addressLine3 = address.getAddressLine3();
			this.town = address.getTown();
			this.postcode = address.getPostcode();
			this.county = address.getCounty();
			this.country = address.getCountry();
            this.state = address.getState();
            
			this.telephone1 = address.getTelephone1();
			this.telephone2 = address.getTelephone2();
			this.telephone3 = address.getTelephone3();

		}
	}

	public void setCountries(boolean b) {
		countries = b;
	}

	public final void setInstructions(boolean instructions) {
		this.instructions = instructions;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(new FieldInputCell(nameLabel, nameDescription, new TextTag(context, nameField, name, 40), new ErrorTag(context, nameField, "<br/>")));

		sb.append(new FieldInputCell(addressLabel1, addressDescription1, new TextTag(context, addressField1, addressLine1, 40), new ErrorTag(context,
				addressField1, "<br/>")));

		sb.append(new FieldInputCell(addressLabel2, addressDescription2, new TextTag(context, addressField2, addressLine2, 40), new ErrorTag(context,
				addressField2, "<br/>")));

		sb.append(new FieldInputCell(addressLabel3, addressDescription3, new TextTag(context, addressField3, addressLine3, 40), new ErrorTag(context,
				addressField3, "<br/>")));

		TextTag townTag = new TextTag(context, townField, town, 30);
		sb.append(new FieldInputCell(townLabel, townDescription, townTag, new ErrorTag(context, townField, "<br/>")));

		TextTag countyTag = new TextTag(context, countyField, county, 30);
		sb.append(new FieldInputCell(countyLabel, countyDescription, countyTag, new ErrorTag(context, countyField, "<br/>")));

		TextTag postcodeTag = new TextTag(context, postcodeField, postcode, 12);
		sb.append(new FieldInputCell(postcodeLabel, postcodeDescription, postcodeTag, new ErrorTag(context, postcodeField, "<br/>")));

		if (countries) {

			SelectTag tag = new SelectTag(context, countryField, country);
			tag.addOptions(Country.getAll());
            tag.setOnChange("if (this.value == '" + Country.US.getIsoAlpha2() + "'){ document.getElementById('" + stateField + "').style.display=''; } else { document.getElementById('" + stateField + "').style.display = 'none'; }");

            sb.append(new FieldInputCell(countryLabel, countryDescription, tag));

            SelectTag tagState = new SelectTag(context, stateField, state);
            tagState.addOptions(UsState.getAll());

            sb.append(new FieldInputCell(stateLabel, stateDescription, tagState, new ErrorTag(
                    context, stateField, "<br/>")).setId(stateField).
                    setHidden(!(Country.US.getIsoAlpha2().equals(context.getParameter(countryField))
                    || Country.US.equals(country))));

		} else {

			sb.append(new HiddenTag(countryField, Country.UK));

		}

		if (telephoneLabel1 != null) {
			sb.append(new FieldInputCell(telephoneLabel1, telephoneDescription1, new TextTag(context, telephoneField1, telephone1, 20), new ErrorTag(
					context, telephoneField1, "<br/>")));
		}

		if (telephoneLabel2 != null) {
			sb.append(new FieldInputCell(telephoneLabel2, telephoneDescription2, new TextTag(context, telephoneField2, telephone2, 20), new ErrorTag(
					context, telephoneField2, "<br/>")));
		}

		if (telephoneLabel3 != null) {
			sb.append(new FieldInputCell(telephoneLabel3, telephoneDescription3, new TextTag(context, telephoneField3, telephone3, 20), new ErrorTag(
					context, telephoneField3, "<br/>")));
		}

		//		if (instructions) {
		//			sb.append("<tr><td class='key' align='right'>Special instructions</td><td>" + new TextAreaTag(context, "instructions", 40, 2) + "</td></tr>");
		//		}
		//
		//		if (addressType) {
		//			sb.append("<tr><td class='key' align='right'>Type of address<br/>For example, home, work, or relative.</td><td>"
		//					+ new TextTag(context, "addressType", 20) + "</td></tr>");
		//		}

		sb.append("<div class='command'>");

		if (context.containsAttribute("superman")) {

			sb.append(new ButtonTag("Sample data").setOnClick("this.form.elements['" + addressField1 + "'].value = '1655 South Street'; "
					+ "this.form.elements['" + nameField + "'].value ='Benjamin Franklin'; this.form.elements['" + postcodeField
					+ "'].value = 'S1 1DJ'; " + "this.form.elements['" + townField + "'].value = 'Philadelphia'; this.form.elements['"
					+ addressField2 + "'].value = 'Center City'; var e = this.form.elements['name']; if (e != null) e.value='test account'; "
                    + "this.form.elements['" + telephoneField1 + "'].value ='12345678901'; "
                    + "var f = this.form.elements['email']; if (f != null) f.value='test@account.com';"));

		}

		sb.append("</div>");

		return sb.toString();
	}
}
