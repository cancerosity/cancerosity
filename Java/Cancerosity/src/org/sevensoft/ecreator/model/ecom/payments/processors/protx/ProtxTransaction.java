package org.sevensoft.ecreator.model.ecom.payments.processors.protx;

import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.httpc.HttpMethod;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorException;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author sks 17 May 2006 10:47:30
 */
class ProtxTransaction {

    public static final Money MIN_AMOUNT = new Money(100);

    private static final String PROTOCOL_VERSION = "2.22";

    private static final String LIVE_PAYMENT_URL = "https://ukvps.protx.com/vpsdirectauth/paymentgateway.asp";

    private static final String LIVE_REFUND_URL = "https://ukvps.protx.com/vps200/dotransaction.dll";

    private static final String TEST_PAYMENT_URL = "https://ukvpstest.protx.com/VPSDirectAuth/PaymentGateway.asp";
    private static final String TEST_REFUND_URL = "https://ukvpstest.protx.com/vps200/dotransaction.dll";

    private static final String SHOW_POST_URL = "https://ukvpstest.protx.com/showpost/showpost.asp";
    private static final String CURRENCY = "GBP";

    private static final Logger logger = Logger.getLogger("payments");

    private final TxType txType;
    private Card card;
    private Item account;
    private Money amount;
    private Address address;
    private final String description;
    private Payment payment;
    private HttpClient hc;
    private String internalTransactionId;
    private RequestContext context;

    private final ProtxDirectProcessor processor;

    public ProtxTransaction(RequestContext context, ProtxDirectProcessor processor, Item account, TxType txType, String desc) {
        this.context = context;
        this.processor = processor;
        this.account = account;
        this.txType = txType;

        this.description = desc;
    }

    private void address() {

        logger.config("[ProtxTransaction] Setting address details: " + address);

        if (address == null) {

            hc.setParameter("BillingAddress", "No address specified");
            hc.setParameter("BillingPostCode", "S1 1DJ");

        } else {

            hc.setParameter("BillingAddress", address.getAddress(" ", 150));
            if (address.hasPostcode()) {
                hc.setParameter("BillingPostCode", address.getPostcode());
            }
        }
    }

    private void card() throws ProcessorException {

        if (card == null) {
            return;
        }

        logger.config("[ProtxTransaction] CardHolder:" + card.getCardHolder());
        logger.config("[ProtxTransaction] CardNumber:" + card.getCardNumber());
        logger.config("[ProtxTransaction] ExpiryDate:" + card.getExpiryDateShort());
        logger.config("[ProtxTransaction] CV2:" + card.getCscNumber());

        if (card.hasStartDate()) {
            logger.config("[ProtxTransaction] StartDate:" + card.getStartDateShort());
        }

        if (card.hasIssue()) {
            logger.config("[ProtxTransaction] IssueNumber:" + card.getIssueNumber());
        }

        // always set expiry date, card number and holder
        hc.setParameter("CardHolder", card.getCardHolder());
        hc.setParameter("CardNumber", card.getCardNumber());
        hc.setParameter("ExpiryDate", card.getExpiryDateShort());
        hc.setParameter("CV2", card.getCscNumber());

        // only set start date if present
        if (card.hasStartDate()) {
            hc.setParameter("StartDate", card.getStartDateShort());
        }

        // only set issue number if exists
        if (card.hasIssue()) {
            hc.setParameter("IssueNumber", card.getIssueNumber());
        }

        switch (card.getCardType()) {

            default:
                logger.fine("[ProtxTransaction]  no card type set");
                throw new ProcessorException("No card type defined for card " + card.getId());

            case Visa:
                hc.setParameter("CardType", "VISA");
                break;

            case VisaElectron:
                hc.setParameter("CardType", "UKE");
                break;

            case VisaDebit:
            case VisaDelta:
                hc.setParameter("CardType", "DELTA");
                break;

            case Maestro:
                hc.setParameter("CardType", "SWITCH");
                break;

            case Mastercard:
                hc.setParameter("CardType", "MC");
                break;

            case Solo:
                hc.setParameter("CardType", "SOLO");
                break;
        }
    }

    private void customer() {

        logger.config("[ProtxTransaction] Setting customer details: " + account);

        hc.setParameter("CustomerName", account.getName());

        if (account.hasEmail()) {
            hc.setParameter("CustomerEMail", account.getEmail());
        }
    }

    private void general() {

        logger.config("[ProtxTransaction] --Setup params--");
        logger.config("[ProtxTransaction] VPSProtocol: " + PROTOCOL_VERSION);
        logger.config("[ProtxTransaction] TxType: " + txType.toString());
        logger.config("[ProtxTransaction] Vendor: " + processor.getVendorName());
        logger.config("[ProtxTransaction] VendorTxCode: " + internalTransactionId);
        logger.config("[ProtxTransaction] Amount: " + amount.getAmount() / 100d);
        logger.config("[ProtxTransaction] Currency: " + CURRENCY);
        logger.config("[ProtxTransaction] Description: " + description);
        logger.config("[ProtxTransaction] --end params--");

        hc.setParameter("VPSProtocol", PROTOCOL_VERSION);
        hc.setParameter("TxType", txType.getProtxName());
        hc.setParameter("Vendor", processor.getVendorName());
        hc.setParameter("VendorTxCode", internalTransactionId);

        hc.setParameter("Amount", amount.toEditString());
        hc.setParameter("Currency", CURRENCY);
        hc.setParameter("Description", description);
    }

    private void payment() {

        if (payment == null) {
            return;
        }

        logger.config("[ProtxTransaction] --payment params--");
        logger.config("[ProtxTransaction] RelatedVPSTxID: " + payment.getProcessorTransactionId());
        logger.config("[ProtxTransaction] RelatedVendorTxCode: " + payment.getInternalTransactionId());
        logger.config("[ProtxTransaction] RelatedSecurityKey: " + payment.getSecurityKeyCode());
        logger.config("[ProtxTransaction] RelatedTxAuthNo: " + payment.getAuthCode());

        hc.setParameter("RelatedVPSTxID", payment.getProcessorTransactionId());
        hc.setParameter("RelatedVendorTxCode", payment.getInternalTransactionId());
        hc.setParameter("RelatedSecurityKey", payment.getSecurityKeyCode());
        hc.setParameter("RelatedTxAuthNo", payment.getAuthCode());
    }

    ProtxResult process() throws IOException, ProcessorException {

        logger.config("[ProtxTransaction] Creating protx transaction type=" + txType);

        if (processor.isShowPost()) {

            hc = new HttpClient(SHOW_POST_URL, HttpMethod.Post);

        } else if (processor.isLive()) {

            switch (txType) {

                case Payment:
                    hc = new HttpClient(LIVE_PAYMENT_URL, HttpMethod.Post);
                    break;

                case Authenticate:
                    hc = new HttpClient(LIVE_PAYMENT_URL, HttpMethod.Post);
                    break;

                case Refund:
                    hc = new HttpClient(LIVE_REFUND_URL, HttpMethod.Post);
                    hc.setParameter("service", "VendorRefundTx");
                    break;
            }

        } else {

            switch (txType) {

                case Payment:
                    hc = new HttpClient(TEST_PAYMENT_URL, HttpMethod.Post);
                    break;

                case Authenticate:
                    hc = new HttpClient(TEST_PAYMENT_URL, HttpMethod.Post);
                    break;

                case Refund:
                    hc = new HttpClient(TEST_REFUND_URL, HttpMethod.Post);
                    hc.setParameter("service", "VendorRefundTx");
                    break;

            }
        }

        internalTransactionId = Payment.generateUniqueTransactionId(context);
        logger.config("[ProtxTransaction] Internal transaction code generated: " + internalTransactionId);

        general();
        customer();
        address();
        card();
        payment();

        logger.config("[ProtxTransaction] Params complete, connecting");

        hc.connect();
        String input = hc.getResponseString();

        logger.config("[ProtxTransaction] output from protx: " + input);

        Map<String, String> params = new HashMap();

        if (input == null) {
            throw new ProcessorException("Protx returned no data!");
        }

        for (String line : StringHelper.explodeStrings(input, "\n")) {

            String[] param = line.split("=");
            if (param.length == 2) {
                params.put(param[0].trim(), param[1].trim());
            }
        }

        /*
           * if status is equal to one on the bad status list then we have an error so throw error with
           * error message returned by protx which is stored in the StatusDetail field
           */
        logger.config("[ProtxTransaction] Transaction status: " + params.get("Status"));

        List<String> badStatus = Arrays.asList("ERROR", "MALFORMED", "INVALID", "UNDEF");
        if (badStatus.contains(params.get("Status"))) {
            throw new ProcessorException("Failed transaction; status: " + params.get("Status") + "; detail: " + params.get("StatusDetail"));
        }

        return new ProtxResult(params, internalTransactionId);

    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public final void setAmount(Money amount) {
        this.amount = amount;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public final void setPayment(Payment payment) {
        this.payment = payment;
    }

}
