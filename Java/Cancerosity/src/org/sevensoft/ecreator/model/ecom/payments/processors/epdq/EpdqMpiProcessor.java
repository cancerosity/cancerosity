package org.sevensoft.ecreator.model.ecom.payments.processors.epdq;

import java.io.IOException;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentException;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardTypeException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CscException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiredCardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiryDateException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.IssueNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.StartDateException;
import org.sevensoft.ecreator.model.ecom.payments.processors.Processor;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorException;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorResult;
import org.sevensoft.ecreator.model.ecom.payments.processors.misc.ClearComRequest;
import org.sevensoft.ecreator.model.ecom.payments.processors.misc.ClearComResponse;
import org.sevensoft.ecreator.model.ecom.payments.processors.misc.ClearComTxType;
import org.sevensoft.ecreator.model.ecom.payments.results.AvsCheck;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * 
 * AVSRespCode AVSDisplay AVS RespCode Description
 * 
 * S1 (blank) AVS not supported
 * 
 * B1 YN Zip not checked; address match
 * 
 * B2 NN Zip not checked; address no match
 * 
 * B3 NN zip not checked; address partial match
 * 
 * B4 NY Zip (postal) code match; address not checked
 * 
 * EX YY Address and zip code match
 * 
 * B5 NY Zip match only; address not checked
 * 
 * B6 NY Zip match only
 * 
 * B7 NN Zip no match; address no match
 * 
 * B8 YN Zip no match; address match
 * 
 * N NN None match
 * 
 * @author Sam 23-Apr-2003 10:15:51
 */
@Table("payments_processors_epdqmpi")
public class EpdqMpiProcessor extends Processor {

	private static final String	EPDQ_URL	= "https://secure2.epdq.co.uk:11500";

	private String			username, password, clientId;

	public EpdqMpiProcessor(RequestContext context) {
		super(context);
	}

	@Override
	public AvsCheck avsCheck(Card card, Address address) throws IOException, ProcessorException, ExpiredCardException, ExpiryDateException,
			CardNumberException, CardException, IssueNumberException, CscException, PaymentException {

		ClearComRequest doc = new ClearComRequest(context, EPDQ_URL, card.getAccount(), ClearComTxType.PreAuth);
		doc.setCard(card);
		doc.setAddress(address);
		ClearComResponse result = doc.process();

		return new AvsCheck(context, result.getProcessorTransactionId(), card.getAccount(), address, card, result.getAvsResult());
	}

	@Override
	public Payment credit(Payment payment, Money money) throws IOException, ExpiryDateException, CardNumberException, CardTypeException, StartDateException,
			ProcessorException, CardException, IssueNumberException {
		return null;
	}

	@Override
	public ProcessorResult debit(Item account, Card card, Address address, Money money) throws CardException, ExpiryDateException, IssueNumberException,
			CardTypeException, CscException, CardNumberException, StartDateException, IOException, ExpiredCardException, ProcessorException,
			PaymentException {

		ClearComRequest request = new ClearComRequest(context, EPDQ_URL, account, ClearComTxType.Auth);

		request.setCard(card);
		request.setAddress(address);
		request.setAmount(money);
		request.setUsername(username);
		request.setPassword(password);
		request.setClientId(clientId);

		return request.process();
	}

	public final String getClientId() {
		return clientId;
	}

	@Override
	public String getName() {
		return "Epdq Mpi";
	}

	public final String getPassword() {
		return password;
	}

	public final String getUsername() {
		return username;
	}

	@Override
	public boolean isLive() {
		return true;
	}

	public final void setClientId(String storeId) {
		this.clientId = storeId;
	}

	public final void setPassword(String password) {
		this.password = password;
	}

	public final void setUsername(String username) {
		this.username = username;
	}

	@Override
	public void verify(Card card) throws CardException, ExpiryDateException, IssueNumberException, CardTypeException, CardNumberException,
			StartDateException, IOException, ExpiredCardException, ProcessorException, CscException {
	}

	@Override
	public boolean voidPayment(Payment payment) throws IOException, ProcessorException {
		return false;
	}

}
