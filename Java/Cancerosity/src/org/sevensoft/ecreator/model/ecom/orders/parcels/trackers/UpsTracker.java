package org.sevensoft.ecreator.model.ecom.orders.parcels.trackers;

/**
 * @author sks 7 Aug 2006 18:07:58
 *
 */
public class UpsTracker extends Tracker {

	@Override
	public String getMethod() {
		return "GET";
	}

	@Override
	public String getTrackingUrl(String consignment) {
		return "http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&error_carried=true&tracknums_displayed=1&TypeOfInquiryNumber=T&loc=en_GB&InquiryNumber1="
				+ consignment + "&AgreeToTermsAndConditions=yes";
	}

}
