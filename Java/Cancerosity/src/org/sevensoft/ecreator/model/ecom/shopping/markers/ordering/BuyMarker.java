package org.sevensoft.ecreator.model.ecom.shopping.markers.ordering;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.submit.ImageSubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;

/**
 * @author sks 20 Apr 2006 17:32:32
 * 
 * This is for buying a single item
 *
 */
public class BuyMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		Item account = (Item) context.getAttribute("account");

		if (!item.isOrderable(account)) {
			return null;
		}

		String src = params.get("src");
		if (src != null) {

			String align = params.get("align");
			String srchover = params.get("srchover");

			ImageSubmitTag tag = new ImageSubmitTag(src);
			tag.setOnClick("this.form.elements[2].value='" + item.getId() + "'");

			if (srchover != null) {
				tag.setOnMouseOver("this.src='" + srchover + "'");
				tag.setOnMouseOut("this.src='" + src + "'");
			}

			if (align != null) {
				tag.setAlign(align);
			}

			return tag;
		}

		String label = params.get("label");
		if (label == null) {
			label = "Add to basket";
		}

		SubmitTag tag = new SubmitTag(label);
		tag.setOnClick("this.form.elements[2].value='" + item.getId() + "'");
		return tag;
	}

	@Override
	public String getDescription() {
		return "Adds the item to the basket";
	}

	public Object getRegex() {
		return "ordering_buy";
	}

}
