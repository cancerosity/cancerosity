package org.sevensoft.ecreator.model.ecom.reports;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.Period;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 19 Nov 2006 17:05:35
 * 
 * Keeps track of sales per agent per week or month for a total period
 *
 */
public class SalesReport {

	private final RequestContext	context;
	private final List<User>	agents;
	private final List<Date>	dates;

	public SalesReport(RequestContext context, Date start, Date end, Period period) {

		this.context = context;

		// make a list of all end dates based on the periods
		this.dates = new ArrayList();
		Date date = end;
		int n = 0;
		while (date.isAfter(start)) {
			this.dates.add(date);
			date = period.remove(date);

			n++;

			if (n > 20)
				break;
		}

		// work out a list of agents that have sold anything at all in this period otherwise we don't need to show them
		Query q = new Query(context, "select u.*, sum(o.saleTotal) sum from # o join # u on o.salesperson=u.id "
				+ "where o.datePlaced>=? and o.datePlaced<? group by u.id having sum > 0");
		q.setTable(Order.class);
		q.setTable(User.class);
		q.setParameter(start);
		q.setParameter(end.nextDay());

		agents = q.execute(User.class);
	}

	public List<User> getAgents() {
		return agents;
	}

	public List<Date> getDates() {
		return dates;
	}

	/**
	 * Returns the total sales for the periods
	 */
	public List<Money> getTotals() {
		return getTotals(null);
	}

	/**
	 * Returns the totals per agent for each date period
	 */
	public List<Money> getTotals(User agent) {

		List<Money> totals = new ArrayList();
		for (Date date : dates) {

			String query = "select sum(saleTotal) sum from # where datePlaced>=? and datePlaced<?";
			if (agent != null)
				query = query + " and salesperson=?";

			Query q = new Query(context, query);
			q.setTable(Order.class);
			q.setParameter(date.removeWeeks(1));
			q.setParameter(date.nextDay());
			if (agent != null) {
				q.setParameter(agent);
			}
			totals.add(q.getMoney());
		}

		return totals;
	}

}
