package org.sevensoft.ecreator.model.ecom.payments.forms.secpay;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.ecom.payments.results.Callback;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 15-Dec-2005 09:24:48
 * 
 * A test account is available which can be used to ensure that your integration is set up properly. This account is available for use whether you are
 * registered with SECPay or not! Below are the details pertaining to this account: � Username: secpay � Login Password: secpay � Remote Password:
 * secpay � Digest Key: secpay
 * 
 * This is a �bucket� development account and is usually in use by several developers at any one time. Please feel free to log in to the Merchant
 * Extranet at https://www.secpay.com/lookup to perform a transaction report and see your own and others� test transactions. ? Note: You can test your
 * integration using your own account (for example: abcdef01) through the use of the test_status optional parameter. The �secpay� test account is
 * useful for developers who wish to perform an integration before having registered with SECPay � often as part of a �proof of concept� demonstration
 * for clients or management.
 * 
 * 10.1. Test Credit Card Numbers
 * 
 * Below are some test credit card numbers. These can be used with any name and address details, any CV2 3-or-4 digit security code, and any expiry
 * date which is in the future.
 * 
 * Visa 4444333322221111 4111111111111111 Master Card 5555555555554444 5105105105105100
 * 
 * You can of course make an attempt to use an expiry date which is set in the past in order to see what happens in that event!
 * 
 * 
 */
public class SecCard extends Form {

	public SecCard(RequestContext context) {
		super(context);
	}

    private String getDigest(String transId, String amount, String secCardPassword) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("MD5");

        md.reset();

        String string = transId + amount + secCardPassword;

        byte[] hash = md.digest(string.getBytes());

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < hash.length; ++i) {
            sb.append(Integer.toHexString((hash[i] & 0xFF) | 0x100)./*toUpperCase().*/substring(1, 3));
        }

        return sb.toString();
    }

	@Override
	public String getMethod() {
		return "POST";
	}

	@Override
	public Map<String, String> getParams(Payable payable) {

		FormSession session = new FormSession(context, payable);

		Map<String, String> params = new HashMap<String, String>();

		/* 3.1. merchant This is your SECPay username (usually six letters followed by two numbers). */
		PaymentSettings ps = PaymentSettings.getInstance(context);
		if (ps.isSecPayLive()) {
			params.put("merchant", ps.getSecPayUsername());
		} else {
			params.put("merchant", "secpay");
		}

		/*
		 * 3.3. amount
		 * 
		 * This is the amount for the transaction. This should contain no currency symbols or formatting (for example do not send an amount with a
		 * comma in). A decimal point may be used as in the example below but is not mandatory: 50.00 is equal to 50
		 * 
		 */
		params.put("amount", payable.getPaymentAmount().toEditString());

		/*
		 * 3.2 trans_id A unique transaction identifier created by yourself. This can be used to refer to a transaction at a later date (to refund
		 * it for example). There are no constraints placed upon the format of this ID though we do recommend that you do not include spaces.
		 * 
		 * We will use this for our session id
		 */
		params.put("trans_id", session.getIdString());

        /*
         * callback location. The output from here is read and shown to the customer.
         */
        params.put("callback", payable.getPaymentSuccessUrl(PaymentType.SecCard).replace("http://", "https://"));

        /*
           * The value of the digest request parameter is created by concatenating the trans_id and amount for a given transaction along with your
           * remote password known only to you and SECPay.
           */
		if (ps.isSecPayLive()) {
			try {
				params.put("digest", getDigest(session.getIdString(), payable.getPaymentAmount().toEditString(), PaymentSettings.getInstance(context).getSecCardPassword()));
			} catch (NoSuchAlgorithmException e) {
				throw new RuntimeException(e);
			}
		}

		// tell secpay to use certain fields when calculating its returning hash
		params.put("md_flds", "trans_id:amount");

		// tell secpay to do the callback as a post
		params.put("cb_post", "true");

		/*
		 * � test_status=true Simulate an authorised callback without contacting the bank � test_status=false Simulate a declined callback without
		 * contacting the bank � test_status=live Send the transaction to the bank for authorization
		 */
		params.put("test_status", ps.isSecPayLive() ? "live" : "true");

		// the order field is for a basket description
		params.put("order", payable.getPaymentDescription());

        /*if (Config.getInstance(context).isSecured()) {
            params.put("ssl_cb", "true");
        }*/

		logger.fine("[SecPaySecCard] params=" + params);
		return params;
	}

	private Payment getPayment(FormSession session, Money amount, String authCode) {

		Payment payment = new Payment(context, session.getAccount(), amount, PaymentType.SecCard);
		payment.setAuthCode(authCode);

		// it seems they use our id as theirs too
		payment.setProcessorTransactionId(session.getIdString());
		payment.save();

		return payment;
	}

	@Override
	public String getPaymentUrl() {
		return "https://www.secpay.com/java-bin/ValCard";
	}

	/*
	 * � trans_id  
	 * � auth_code 
	 * � cv2avs 
	 * � message 
	 * � resp_code 
	 * � amount 
	 * � code 
	 * � test_status 
	 * � hash ? Note: 
	 * 
	 * The following three callback parameters are only supplied if you sent �repeat=true� in the options field�
	 *  
	 * � expiry 
	 * � card_no 
	 * � customer
	 * 
	 */
	@Override
	public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {

		new Callback(context, PaymentType.SecCard, params);

		logger.fine("[SecPaySecCard] Callback secpay seccard");
		logger.fine("[SecPaySecCard] params=" + params);

		FormSession session = EntityObject.getInstance(context, FormSession.class, params.get("trans_id"));
		if (session == null) {
			return;
		}

		Money amount = new Money(params.get("amount"));
		String authCode = params.get("auth_code");
		String code = params.get("code");
		String hash = params.get("hash");

		// check that the transaction was authorised.
		if (!"A".equals(code)) {
			return;
		}

		PaymentSettings ps = PaymentSettings.getInstance(context);

		// if in live mode and a password is set then we should check the resultant hashes match
//        if (ps.isSecPayLive() && ps.hasSecCardPassword()) {

            String string = "trans_id=" + session.getIdString() + "&amount=" + amount + "&" + ps.getSecCardPassword();

            String currHash = "";
            try {
                currHash = getDigest(string, "", "");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                logger.fine("[SecCard]" + e.toString());
            }

            if (!currHash.equals(hash)) {

                System.out.println("Payment hash unequal");
                return;
            }
//        }

		Payment payment = getPayment(session, amount, authCode);
		if (payment == null) {
			return;
		}

		session.callback(params, ipAddress, payment);

	}

	@Override
	public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
		return null;
	}

}
