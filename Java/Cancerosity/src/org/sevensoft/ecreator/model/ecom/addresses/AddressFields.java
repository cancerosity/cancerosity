package org.sevensoft.ecreator.model.ecom.addresses;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13-Jul-2005 20:59:38
 */
@Table("addresses_fields")
public class AddressFields extends EntityObject {

    public enum Type {
        Delivery, Billing, Booking
    }

    public static AddressFields getInstance(RequestContext context, Type type) {
        AddressFields as = SimpleQuery.get(context, AddressFields.class, "type", type);
        return as == null ? new AddressFields(context, type) : as;
    }

    /**
     *
     */
    private String telephoneLabel1, telephoneLabel2, telephoneLabel3;

    /**
     *
     */
    private boolean telephoneOptional1, telephoneOptional2, telephoneOptional3;

    /**
     *
     */
    private String nameLabel;

    /**
     *
     */
    private String townLabel;

    private String townDescription;

    private String postcodeLabel;

    private String addressDescription1, addressDescription2, addressDescription3;

    private String addressLabel1, addressLabel2, addressLabel3;

    private String countyLabel, stateLabel;

    private String stateDescription;

    private String telephoneDescription1, telephoneDescription2, telephoneDescription3;

    private String nameDescription;

    private String countryLabel;

    private Type type;

    private AddressFields(RequestContext context) {
        super(context);
    }

    private AddressFields(RequestContext context, Type type) {
        super(context);
        this.type = type;

        telephoneOptional1 = false;
        telephoneOptional2 = true;
        telephoneOptional3 = true;

        switch (type) {

            case Booking:
                break;

            case Billing:
                nameDescription = "This should be the name on your credit card or whoever is making payment.";
                telephoneDescription1 = "A contact number is required in case we need to contact you regarding payment.";
                break;

            case Delivery:
                nameDescription = "This should be the name of whoever will sign for your delivery.";
                telephoneDescription1 = "A contact number is required should the delivery company have any problems with your delivery.";
        }

        save();
    }

    public final String getAddressDescription1() {
        return addressDescription1;
    }

    public final String getAddressDescription2() {
        return addressDescription2;
    }

    public final String getAddressDescription3() {
        return addressDescription3;
    }

    public final String getAddressLabel1() {
        return addressLabel1 == null ? "Address 1" : addressLabel1;
    }

    public final String getAddressLabel2() {
        return addressLabel2 == null ? "Address 2" : addressLabel2;
    }

    public final String getAddressLabel3() {
        return addressLabel3 == null ? "Address 3" : addressLabel3;
    }

    public final String getCountryLabel() {
        return countryLabel == null ? "Country" : countryLabel;
    }

    public final String getCountyLabel() {
        return countyLabel == null ? "County" : countyLabel;
    }

    public final String getStateLabel() {
        return stateLabel == null ? "State" : stateLabel;
    }

    public String getNameDescription() {
        return nameDescription;
    }

    public String getNameLabel() {
        return nameLabel == null ? "Name" : nameLabel;
    }

    public String getPostcodeLabel() {
        return postcodeLabel == null ? "Postcode" : postcodeLabel;
    }

    public final String getTownDescription() {
        return townDescription;
    }

    private String postcodeDescription;

    public String getTelephoneDescription1() {
        return telephoneDescription1;
    }

    public final String getTelephoneDescription2() {
        return telephoneDescription2;
    }

    public final String getTelephoneDescription3() {
        return telephoneDescription3;
    }

    public String getTelephoneLabel1() {
        return telephoneLabel1 == null ? "Telephone 1" : telephoneLabel1;
    }

    public String getTelephoneLabel2() {
        return telephoneLabel2 == null ? "Telephone 2" : telephoneLabel2;
    }

    public String getTelephoneLabel3() {
        return telephoneLabel3 == null ? "Telephone 3" : telephoneLabel3;
    }

    public String getTownLabel() {
        return townLabel == null ? "Town / City" : townLabel;
    }

    public boolean hasTelephoneField1() {
        return telephoneLabel1 != null;
    }

    public boolean hasTelephoneField2() {
        return telephoneLabel2 != null;
    }

    public boolean hasTelephoneField3() {
        return telephoneLabel3 != null;
    }

    public boolean isTelephoneOptional1() {
        return telephoneOptional1;
    }

    public boolean isTelephoneOptional2() {
        return telephoneOptional2;
    }

    public boolean isTelephoneOptional3() {
        return telephoneOptional3;
    }

    public final void setAddressDescription1(String addressDescription1) {
        this.addressDescription1 = addressDescription1;
    }

    public final void setAddressDescription2(String addressDescription2) {
        this.addressDescription2 = addressDescription2;
    }

    public final void setAddressDescription3(String addressDescription3) {
        this.addressDescription3 = addressDescription3;
    }

    public final void setAddressLabel1(String address1Label) {
        this.addressLabel1 = address1Label;
    }

    public final void setAddressLabel2(String address2Label) {
        this.addressLabel2 = address2Label;
    }

    public final void setAddressLabel3(String addressLabel3) {
        this.addressLabel3 = addressLabel3;
    }

    public final void setCountryLabel(String deliveryCountryLabel) {
        this.countryLabel = deliveryCountryLabel;
    }

    public final void setCountyLabel(String countyLabel) {
        this.countyLabel = countyLabel;
    }

    public final void setStateLabel(String stateLabel) {
        this.stateLabel = stateLabel;
    }

    public final void setNameDescription(String deliveryAddressNameDescription) {
        this.nameDescription = deliveryAddressNameDescription;
    }

    public void setNameLabel(String nameLabel) {
        this.nameLabel = nameLabel;
    }

    public final void setPostcodeLabel(String postcodeLabel) {
        this.postcodeLabel = postcodeLabel;
    }

    public final void setTownDescription(String telephoneDescription) {
        this.townDescription = telephoneDescription;
    }

    public final void setTelephoneDescription1(String telephoneDescription1) {
        this.telephoneDescription1 = telephoneDescription1;
    }

    public final void setTelephoneDescription2(String deliveryTelephoneDescription2) {
        this.telephoneDescription2 = deliveryTelephoneDescription2;
    }

    public final void setTelephoneDescription3(String deliveryTelephoneDescription3) {
        this.telephoneDescription3 = deliveryTelephoneDescription3;
    }

    public void setTelephoneLabel1(String telephone1Label) {
        this.telephoneLabel1 = telephone1Label;
    }

    public void setTelephoneLabel2(String telephone2Label) {
        this.telephoneLabel2 = telephone2Label;
    }

    public void setTelephoneLabel3(String telephone3Label) {
        this.telephoneLabel3 = telephone3Label;
    }

    public void setTelephoneOptional1(boolean telephone1Optional) {
        this.telephoneOptional1 = telephone1Optional;
    }

    public void setTelephoneOptional2(boolean telephone2Optional) {
        this.telephoneOptional2 = telephone2Optional;
    }

    public void setTelephoneOptional3(boolean telephone3Optional) {
        this.telephoneOptional3 = telephone3Optional;
    }

    public void setTownLabel(String townLabel) {
        this.townLabel = townLabel;
    }


    public final String getPostcodeDescription() {
        return postcodeDescription;
    }


    public final void setPostcodeDescription(String postcodeDescription) {
        this.postcodeDescription = postcodeDescription;
    }

    public String getStateDescription() {
       return stateDescription == null ? "State code for US customers only" : stateDescription;
    }

    public void setStateDescription(String stateDescription) {
        this.stateDescription = stateDescription;
    }
}
