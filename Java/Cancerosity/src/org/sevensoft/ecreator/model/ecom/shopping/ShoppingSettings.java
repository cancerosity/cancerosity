package org.sevensoft.ecreator.model.ecom.shopping;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.ecom.orders.emails.OrderEmailParser;
import org.sevensoft.ecreator.model.ecom.shopping.renderer.BasketLineMarkupDefault;
import org.sevensoft.ecreator.model.ecom.shopping.renderer.BasketMarkupDefault;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.misc.agreements.Agreement;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13-Jul-2005 20:59:38
 * 
 */
@Table("settings_shopping")
@Singleton
public class ShoppingSettings extends EntityObject {

	public enum CheckoutAccounts {

		/**
		 * Registrations disabled, aka simple checkout
		 */
		None,

		/**
		 * You can register if you want
		 */
		Optional,

		/**
		 * You must pre-register to checkout
		 */
		Required
	}

	/**
	 * If branch search is enabled then users will be asked to enter location / postcode to find a branch
	 */
	private boolean	branchSearch;

	public enum DispatchType {
		Collection, Delivery, Both;
	}

	public static ShoppingSettings getInstance(RequestContext context) {
		return getSingleton(context, ShoppingSettings.class);
	}

	private CheckoutAccounts	checkoutAccounts;

	/**
	 * Header for ordr ack. BC name
	 */
	private String			orderAcknowledgementBody;

	/**
	 * Show a vat exemption declaration check box
	 */
	private boolean			vatExemptionDeclaration;

	/**
	 * Extra details with the vat dec
	 */
	private boolean			vatExemptionDetails;

	private String			vatExemptionText;

	/**
	 * Send email to a guest when they checkout with a password so they can login next time
	 */
	private boolean			createAccountForGuest;

	/**
	 * Content fields
	 */
	private String			checkoutContent, basketContent;

	/**
	 * Any scripts needing to be run at checkout, eg, shopping feed conversion scripts.
	 */
	private String			checkoutScripts;

	/**
	 * 
	 */
	private String			checkoutCompletedText;

	/**
	 * 
	 */
	private String			checkoutConfirmationFooter;

	/**
	 * Content for the checkout page
	 */
	private String			checkoutConfirmationHeader;

	/**
	 * Sets a minimum order price for basket systems
	 */
	private Money			minOrderValue;

	private String			paymentDeclinedContent;

	private String			cancellationContent;

	/**
	 * A set of email addresses that will receive an email details on an order - either via the basket, or 
	 */
	private Set<String>		orderNotificationEmails;

	/**
	 * Send an email on placing an order to the lister
	 */
	private boolean			emailSellers;

	private String			sellerEmailText;

	/**
	 * Do not show the basket screen after adding a product, just stay on the product page.
	 */
	private boolean			quickBasketAdd;

	/**
	 * Flags if we should allow reservations, ie, collections
	 */
	@Deprecated
	private boolean			reservations;

	/**
	 * Text that appears when a reservation is made
	 */
	private String			reservationText;

	private String			dispatchTypeHeader;

	/**
	 * Instructions on the collection page
	 * BC
	 */
	private String			reservationInstructions;

	private boolean			customerReference;

	/**
	 * Lets users pick addresses from their address books
	 */
	private boolean			addressBook;

	/**
	 * Show checkout forms before other pages
	 */
	private boolean			formsFirst;

	/**
	 * Use separate addresses for billing and delivery
	 */
	private boolean			separateAddresses;

	/**
	 * How much needs to be spent in order to gain a credit. This will apply to all items
	 */
	private Money			spendPerCredit;

	/**
	 * Send out an order acknowledge to customers
	 */
	@Default("1")
	private boolean			orderAcknowledgement;

	/**
	 * Empty the basket when a user logs out.
	 */
	private boolean			clearBasketOnLogout;

	/**
	 * The footer for the order acknowledgement email
	 */
	private String			orderAckFooter;

	/**
	 * Remove countries selector when adding in an address at basket 
	 */
	private boolean			noCountries;

	private Markup			basketLineMarkup;

	private Markup			basketMarkup;

	private ItemType			customerItemType;

	private String			deliveryAddressHeader;

	private String			billingAddressHeader;

	private String			yourDetailsHeader;

	private String			emailInstructions;

	private String			nameInstructions;

	private String			placeOrderText;

	/**
	 * Referrers used at checkout when in non account mode
	 */
	private List<String>		referrers;

	@Default("1")
	private boolean			addresses;

	private Agreement			agreement;

	private DispatchType		dispatchType;

	private String			branchSelectionInstruction;

    /**
     * text/html email format
     */
    private boolean orderAcknowledgementHtml;

	public ShoppingSettings(RequestContext context) {
		super(context);
	}

	/**
	 * 
	 */
	public Agreement getAgreement() {
		return (Agreement) (agreement == null ? null : agreement.pop());
	}

	public String getBasketCaption() {
		return "Shopping basket";
	}

	public String getBasketContent() {
		return basketContent;
	}

	public Markup getBasketLineMarkup() {

		if (basketLineMarkup == null) {
			basketLineMarkup = new Markup(context, "Basket line", new BasketLineMarkupDefault());
			save();
		}

		return basketLineMarkup.pop();
	}

	public Markup getBasketMarkup() {

		if (basketMarkup == null) {

			basketMarkup = new Markup(context, "Basket", new BasketMarkupDefault());
			save();
		}

		return basketMarkup.pop();
	}

	public String getBillingAddressHeader() {
		return billingAddressHeader == null ? "Choose your billing address. "
				+ "If you are paying by credit card you must enter the address where your card is registered." : billingAddressHeader;
	}

	/**
	 * 
	 */
	public String getBranchSelectionInstruction() {
		return branchSelectionInstruction == null ? "Choose the branch where you want to collect your order from." : branchSelectionInstruction;
	}

    public boolean isOrderAcknowledgementHtml() {
        return orderAcknowledgementHtml;
    }

    public String getCancellationContent() {
		return cancellationContent == null ? "You have cancelled this checkout process" : cancellationContent;
	}

	public final CheckoutAccounts getCheckoutAccounts() {

		if (!Module.Accounts.enabled(context)) {
			return CheckoutAccounts.None;
		}

		return checkoutAccounts == null ? CheckoutAccounts.None : checkoutAccounts;
	}

	public String getCheckoutCompletedText() {
		return checkoutCompletedText == null ? "[name],\n\nThank you for your order.\n\nYour reference is [ref]" : checkoutCompletedText;
	}

	public String getCheckoutConfirmationFooter() {
		return checkoutConfirmationFooter;
	}

	public String getCheckoutConfirmationHeader() {
		return checkoutConfirmationHeader;
	}

	public String getCheckoutContent() {
		return checkoutContent;
	}

	/**
	 * 
	 */
	public List<Form> getCheckoutForms() {
		return SimpleQuery.execute(context, Form.class, "checkout", true);
	}

	public String getCheckoutScripts() {
		return checkoutScripts;
	}

	public String getCollectionInstructions() {
		return reservationInstructions == null ? "If you want to reserve the goods for pick up then click 'reserve this order'. "
				+ "We will email you an order confirmation which you will need to print off and show when you arrive to collect. "
				+ "We will take payment for this order in person." : reservationInstructions;
	}

	public String getConfirmationTextRendered(Order order) {

		String string = getCheckoutCompletedText();

		string = string.replace("\n", "<br/>");
		string = string.replace("[ref]", order.getOrderId());
		string = string.replace("[id]", order.getOrderId());
		string = string.replace("[name]", order.getAccount().getName());

		return string;
	}

	/**
	 * 
	 */
	public String getDeliveryAddressHeader() {
		return deliveryAddressHeader == null ? "Enter the address where your order should be delivered to. "
				+ "Please include any extra instructions that might be required to locate your property." : deliveryAddressHeader;
	}

	public final DispatchType getDispatchType() {

		if (dispatchType == null) {
			if (reservations) {
				dispatchType = DispatchType.Both;
			} else {
				dispatchType = DispatchType.Delivery;
			}
			save();
		}
		return dispatchType;
	}

	public final String getDispatchTypeHeader() {
		return dispatchTypeHeader;
	}

	public String getEmailInstructions() {
		return emailInstructions == null ? "Your email address is used to acknowledge the order and to contact you if there is a problem."
				: emailInstructions;
	}

	public Money getMinOrderValue() {
		return minOrderValue == null ? new Money(0) : minOrderValue;
	}

	/**
	 * 
	 */
	public String getNameInstructions() {
		return nameInstructions == null ? "Enter the full name of the person making the order." : nameInstructions;
	}

	public String getOrderAckFooter() {
		return orderAckFooter;
	}

	public final String getOrderAckHeader() {
		return orderAcknowledgementBody == null ? getOrderAcknowledgementDefault() : orderAcknowledgementBody;
	}

	public final String getOrderAcknowledgementBody() {
		return orderAcknowledgementBody;
	}

	/**
	 * @return
	 */
	private String getOrderAcknowledgementDefault() {

		StringBuilder sb = new StringBuilder();

		sb.append("\nThank you for your order at ");
		sb.append(Company.getInstance(context).getName());
		sb.append("!\n");
		sb.append("We'll begin to process this order right away. If there are any problems we will contact you by email. "
				+ "You'll receive another email once the order has been completed.\n\n");

		return sb.toString();
	}

	public Set<String> getOrderNotificationEmails() {
		if (orderNotificationEmails == null) {
			orderNotificationEmails = new HashSet();
		}
		return orderNotificationEmails;
	}

	public String getOrderNotificationEmailsString() {
		return StringHelper.implode(getOrderNotificationEmails(), "\n", true);
	}

	public String getPaymentDeclinedContent() {
		return paymentDeclinedContent;
	}

	public String getPlaceOrderText() {
		return placeOrderText == null ? "If you are satisified with your order details and are ready to place your order click here. "
				+ "Please click only once and your order can take up to 2 minutes to be processed" : placeOrderText;
	}

	public final List<String> getReferrers() {
		if (referrers == null) {
			referrers = new ArrayList();
		}
		return referrers;
	}

	public String getReservationText() {
		return reservationText == null ? "Your order has been reserved.<br/>Please quote reference [ref] when you collect." : reservationText;
	}

	public String getReservationTextRendered(Order order) {

		String string = getReservationText();

		string = string.replace("[ref]", order.getIdString());
		string = string.replace("[total]", order.getTotalInc().toString());
		string = string.replace("[name]", order.getAccount().getName());
		return string;
	}

	public final String getSellerEmailText() {
		return sellerEmailText;
	}

	public String getSellerEmailTextRendered(OrderLine line) {
		return new OrderEmailParser(context).parse(sellerEmailText, line);
	}

	public Money getSpendPerCredit() {
		return spendPerCredit;
	}

	public String getVatExemptionText() {
		return vatExemptionText == null ? "I confirm I am VAT exempt. Please remove VAT from my order." : vatExemptionText;
	}

	public boolean hasAgreement() {
		return agreement != null;
	}

	public boolean hasBasketContent() {
		return basketContent != null;
	}

	public boolean hasCheckoutConfirmationFooter() {
		return checkoutConfirmationFooter != null;
	}

	public boolean hasCheckoutConfirmationHeader() {
		return checkoutConfirmationHeader != null;
	}

	public boolean hasCheckoutContent() {
		return checkoutContent != null;
	}

	public boolean hasCheckoutScripts() {
		return checkoutScripts != null;
	}

	public boolean hasDispatchTypeHeader() {
		return dispatchTypeHeader != null;
	}

	public boolean hasNotificationEmails() {
		return getOrderNotificationEmails().size() > 0;
	}

	public boolean hasOrderAcknowledgementFooter() {
		return orderAckFooter != null;
	}

	public boolean hasOrderAcknowledgementHeader() {
		return orderAcknowledgementBody != null;
	}

	public boolean hasPaymentDeclinedContent() {
		return paymentDeclinedContent != null;
	}

	/**
	 * 
	 */
	public boolean hasReferrers() {
		return getReferrers().size() > 0;
	}

	public boolean hasSellerEmailText() {
		return sellerEmailText != null;
	}

	public final boolean isAddressBook() {
		switch (getCheckoutAccounts()) {
		default:
		case None:
			return false;
		case Optional:
		case Required:
			return addressBook;
		}
	}

	public boolean isAddresses() {
		return addresses;
	}

	public final boolean isClearBasketOnLogout() {
		return clearBasketOnLogout;
	}

	public boolean isCollections() {

		switch (getDispatchType()) {

		default:
		case Collection:
		case Both:
			return true;

		case Delivery:
			return false;
		}
	}

	public final boolean isCreateAccountForGuest() {
		return createAccountForGuest && getCheckoutAccounts() == CheckoutAccounts.Optional;
	}

	public boolean isCustomerReference() {
		return customerReference;
	}

	public final boolean isEmailSellers() {
		return emailSellers;
	}

	public boolean isFormsFirst() {
		return formsFirst;
	}

	public boolean isNoCountries() {
		return noCountries;
	}

	public boolean isOrderAcknowledgement() {
		return orderAcknowledgement;
	}

	public boolean isQuickBasketAdd() {
		return quickBasketAdd;
	}

	public boolean isSeparateAddresses() {
		return separateAddresses;
	}

	public boolean isShowCountries() {
		return !isNoCountries();
	}

	public boolean isSingleAddress() {
		return !isSeparateAddresses();
	}

	public final boolean isVatExemptionDeclaration() {
		return vatExemptionDeclaration;
	}

	public final boolean isVatExemptionDetails() {
		return vatExemptionDetails;
	}

	public void setAddressBook(boolean addressBook) {
		this.addressBook = addressBook;
	}

	public final void setAddresses(boolean addresses) {
		this.addresses = addresses;
	}

	public final void setAgreement(Agreement agreement) {
		this.agreement = agreement;
	}

	public void setBasketContent(String s) {
		this.basketContent = s;
	}

	public void setBasketLineMarkup(Markup basketLineMarkup) {
		this.basketLineMarkup = basketLineMarkup;
	}

	public void setBasketMarkup(Markup basketMarkup) {
		this.basketMarkup = basketMarkup;
	}

	public final void setBillingAddressHeader(String billingAddressInstructions) {
		this.billingAddressHeader = billingAddressInstructions;
	}

	public final void setBranchSelectionInstruction(String branchSelectionInstruction) {
		this.branchSelectionInstruction = branchSelectionInstruction;
	}

    public void setOrderAcknowledgementHtml(boolean orderAcknowledgementHtml) {
        this.orderAcknowledgementHtml = orderAcknowledgementHtml;
    }

    public void setCancellationContent(String checkoutCancelledContent) {
		this.cancellationContent = checkoutCancelledContent;
	}

	public final void setCheckoutAccounts(CheckoutAccounts checkoutAccounts) {
		this.checkoutAccounts = checkoutAccounts;
	}

	public void setCheckoutCompletedText(String confirmationText) {
		this.checkoutCompletedText = confirmationText;
	}

	public void setCheckoutConfirmationFooter(String checkoutConfirmationFooter) {
		this.checkoutConfirmationFooter = checkoutConfirmationFooter;
	}

	public void setCheckoutConfirmationHeader(String checkoutConfirmationHeader) {
		this.checkoutConfirmationHeader = checkoutConfirmationHeader;
	}

	public void setCheckoutContent(String s) {
		this.checkoutContent = s;
	}

	public void setCheckoutScripts(String checkoutScripts) {
		this.checkoutScripts = checkoutScripts;
	}

	public final void setClearBasketOnLogout(boolean clearBasketOnLogout) {
		this.clearBasketOnLogout = clearBasketOnLogout;
	}

	public final void setCreateAccountForGuest(boolean createAccountForGuest) {
		this.createAccountForGuest = createAccountForGuest;
	}

	public void setCustomerReference(boolean customerReference) {
		this.customerReference = customerReference;
	}

	public final void setDeliveryAddressHeader(String deliveryAddressInstructions) {
		this.deliveryAddressHeader = deliveryAddressInstructions;
	}

	public final void setDispatchType(DispatchType dispatchOptions) {
		this.dispatchType = dispatchOptions;
	}

	public final void setDispatchTypeHeader(String dispatchTypeHeader) {
		this.dispatchTypeHeader = dispatchTypeHeader;
	}

	public final void setEmailInstructions(String emailInstructions) {
		this.emailInstructions = emailInstructions;
	}

	public final void setEmailSellers(boolean emailOrderAckToLister) {
		this.emailSellers = emailOrderAckToLister;
	}

	public void setFormsFirst(boolean formsFirst) {
		this.formsFirst = formsFirst;
	}

	public void setMinOrderValue(Money minOrderValue) {
		this.minOrderValue = minOrderValue;
	}

	public final void setNameInstructions(String nameInstructions) {
		this.nameInstructions = nameInstructions;
	}

	public void setNoCountries(boolean noCountries) {
		this.noCountries = noCountries;
	}

	public void setOrderAckFooter(String s) {
		this.orderAckFooter = s;
	}

	public final void setOrderAckHeader(String s) {
		this.orderAcknowledgementBody = s;
	}

	public final void setOrderAcknowledgement(boolean orderAcknowledgements) {
		this.orderAcknowledgement = orderAcknowledgements;
	}

	public final void setOrderAcknowledgementBody(String orderAcknowledgementBody) {
		this.orderAcknowledgementBody = orderAcknowledgementBody;
	}

	public void setOrderNotificationEmails(Collection<String> emails) {
		this.orderNotificationEmails = new TreeSet();
		if (emails != null) {
			this.orderNotificationEmails.addAll(emails);
		}
	}

	public void setPaymentDeclinedContent(String paymentDeclinedContent) {
		this.paymentDeclinedContent = paymentDeclinedContent;
	}

	public final void setPlaceOrderText(String placeOrderText) {
		this.placeOrderText = placeOrderText;
	}

	public void setQuickBasketAdd(boolean quickBasketAdd) {
		this.quickBasketAdd = quickBasketAdd;
	}

	public final void setReferrers(List<String> referrers) {
		this.referrers = referrers;
	}

	public void setReservationInstructions(String reservationInstructions) {
		this.reservationInstructions = reservationInstructions;
	}

	public void setReservationText(String reservationText) {
		this.reservationText = reservationText;
	}

	public final void setSellerEmailText(String s) {
		this.sellerEmailText = s;
	}

	public void setSeparateAddresses(boolean separateAddresses) {
		this.separateAddresses = separateAddresses;
	}

	public void setSpendPerCredit(Money m) {
		this.spendPerCredit = m;
	}

	public final void setVatExemptionDeclaration(boolean vatExemptionDeclaration) {
		this.vatExemptionDeclaration = vatExemptionDeclaration;
	}

	public final void setVatExemptionDetails(boolean vatExemptionDetails) {
		this.vatExemptionDetails = vatExemptionDetails;
	}

	public void setVatExemptionText(String vatExemptionText) {
		this.vatExemptionText = vatExemptionText;
	}

	@Override
	protected void singletonInit(RequestContext context) {
		this.checkoutCompletedText = "Thank you for your order.\n" + "Your order reference is <b>[ref]</b>.\n\n"
				+ "Please keep a note of this in case you need to contact us regarding your order.";
		this.basketContent = "You now have something in your basket - you can make changes in the boxes below - remember to press the \"update\" button if you do !\n\n"
				+ "Happy with your choices? Then simply click the \"Go to Checkout\" button to pay.";
	}

	public final boolean isBranchSearch() {
		return branchSearch && Config.getInstance(context).isLocations();
	}

	public final void setBranchSearch(boolean branchSearch) {
		this.branchSearch = branchSearch;
	}

}
