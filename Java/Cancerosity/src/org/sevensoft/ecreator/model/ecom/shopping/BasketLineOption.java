package org.sevensoft.ecreator.model.ecom.shopping;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.attachments.AttachmentFilter;
import org.sevensoft.ecreator.model.attachments.AttachmentSupport;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;

/**
 * @author sks 11-Oct-2005 12:08:12
 * 
 */
@Table("basket_lines_options")
public class BasketLineOption extends AttachmentSupport {

	/**
	 * The owner line
	 */
	private BasketLine	line;

	/**
	 * 
	 */
	private ItemOption	itemOption;

	/**
	 * The sale price addition / reduction
	 */
	private Money		salePrice;

	/**
	 * The value choosen by the customer
	 */
	private String		text;

	/**
	 * The name of this item option
	 */
	private String		name;

	/**
	 * the cost price addition / reduction
	 */
	private Money		costPrice;

    private String productCode;

	private BasketLineOption(RequestContext context) {
		super(context);
	}

	public BasketLineOption(RequestContext context, BasketLine line, ItemOption option, ItemOptionSelection selection) {
		this(context, line, option, selection.getText(), selection.getSalePrice(), selection.getProductCode());
		save();
	}

	public BasketLineOption(RequestContext context, BasketLine line, ItemOption option, String text) {
		this(context, line, option, text, option.getPrice());
		save();
	}

	private BasketLineOption(RequestContext context, BasketLine line, ItemOption option, String text, Money salePrice) {

		super(context);

		this.line = line;
		this.itemOption = option;
		this.text = text;
		this.name = option.getName();

		this.salePrice = salePrice;
		if (this.salePrice == null)
			this.salePrice = new Money(0);

		this.costPrice = new Money(0);
	}

	public BasketLineOption(RequestContext context, BasketLine line, ItemOption option, Upload upload) {
		this(context, line, option, upload.getFilename(), null);

		this.salePrice = option.getPrice();
		save();
	}

    public BasketLineOption(RequestContext context, BasketLine line, ItemOption option, String text, Money salePrice, String productCode) {
        this(context, line, option, text, salePrice);

        this.productCode = productCode;
        save();
    }

    public boolean acceptedFiletype(String filename) {
		return new AttachmentFilter().accept(filename);
	}

	public int getAttachmentLimit() {
		return 0;
	}

	public Money getCostPrice() {
		return costPrice;
	}

	public BasketLine getLine() {
		return line.pop();
	}

	public String getName() {
		return name;
	}

	public ItemOption getOption() {
		return getItemOption();
	}

	public ItemOption getItemOption() {
		return itemOption.pop();
	}

	public Money getSalePrice() {
		return salePrice;
	}

	public String getText() {
		return text;
	}

	public String getValue() {
		return text;
	}

    public String getProductCode() {
        return productCode;
    }
}
