package org.sevensoft.ecreator.model.ecom.orders;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 21 Jan 2007 14:02:14
 *
 */
public class VatHelper {

	public static Money inc(RequestContext context, Money money, double vatRate) {

		if (money == null) {
			return null;
		}

		if (vatRate == 0) {
			return money;
		}

		Company company = Company.getInstance(context);
		if (!company.isVatRegistered()) {
			return money;
		}

		return money.multiply(vatRate / 100d + 1d);
	}

	public static Money vat(RequestContext context, Money money, double vatRate) {

		if (money == null) {
			return null;
		}

		if (vatRate == 0) {
			return new Money(0);
		}

		Company company = Company.getInstance(context);
		if (!company.isVatRegistered()) {
			return new Money(0);
		}

		return money.multiply(vatRate / 100);
	}

}
