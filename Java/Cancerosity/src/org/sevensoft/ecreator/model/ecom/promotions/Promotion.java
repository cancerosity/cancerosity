package org.sevensoft.ecreator.model.ecom.promotions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 14 Jul 2006 09:44:51
 *
 */
@Table("promotions")
public class Promotion extends EntityObject implements Positionable {

	@SuppressWarnings("hiding")
	private static Logger	logger	= Logger.getLogger(Promotion.class.getName());

	public enum PromotionType {

		ItemsDiscount() {

			@Override
			public String toString() {
				return "Item discount";
			}
		},
		OrderDiscount() {

			@Override
			public String toString() {
				return "Order discount";
			}
		},
		FreeItems() {

			@Override
			public String toString() {
				return "Free item(s)";
			}
		},
		FixedPrice() {

			@Override
			public String toString() {
				return "Items at fixed price";
			}
		}
	}

	public static List<Promotion> get(RequestContext context) {
		Query q = new Query(context, "select * from # order by id");
		q.setTable(Promotion.class);
		return q.execute(Promotion.class);
	}

	/**
	 * Returns all promotions that are currently usable on the site
	 */
	public static List<Promotion> getValid(RequestContext context) {

		List<Promotion> promotions = get(context);
		CollectionsUtil.filter(promotions, new Predicate<Promotion>() {

			public boolean accept(Promotion e) {

				if (e.hasStartDate() && e.getStartDate().isFuture()) {
					return false;
				}

				if (e.hasEndDate() && e.getEndDate().isPast()) {
					return false;
				}

				return true;
			}
		});
		return promotions;
	}

	/**
	 * 
	 */
	private String		name;

	/**
	 * Min spend on order as a total
	 */
	private Money		minSpend;

	/**
	 * The qty of items this promotion applies to. Buy 3 get 1 free would apply to 3 here, or buy 5 for $25 would apply to 5.
	 */
	private int			qty;

	/**
	 * Validity dates
	 */
	private Date		startDate, endDate;

	private PromotionType	type;

	/**
	 * Max uses per 'checkout session'
	 */
	private int			maxUsesPerSession;

	private Amount		totalDiscount;

	private Money		fixedPrice;

	private Amount		itemsDiscount;

	/**
	 * The use of this promotion excludes the use of any other also marked exclusive
	 */
	private boolean		exclusive;

	private int			position;

	public Promotion(RequestContext context) {
		super(context);
	}

	public Promotion(RequestContext context, String name) {
		super(context);
		this.name = name;

		save();
	}

	public void addCategory(Category category) {
		new PromotionCategory(context, this, category);
		Basket.flag(context);
	}

	public void addFreeItem(Item item) {
		new FreeItem(context, this, item);
		Basket.flag(context);
	}

	public void addItem(Item item) {
		new PromotionItem(context, this, item);
	}

	@Override
	public synchronized boolean delete() {
		Basket.flag(context);
		return super.delete();
	}

	public List<Attribute> getAttributes() {
		return null;
	}

	public List<Category> getCategories() {

		Query q = new Query(context, "select c.* from # c join # pc on c.id=pc.category where pc.promotion=?");
		q.setTable(Category.class);
		q.setTable(PromotionCategory.class);
		q.setParameter(this);
		return q.execute(Category.class);
	}

	public Date getEndDate() {
		return endDate;
	}

	public Money getFixedPrice() {
		return fixedPrice;
	}

	public List<Item> getFreeItems() {
		Query q = new Query(context, "select i.* from # i join # fi on i.id=fi.item where fi.promotion=?");
		q.setTable(Item.class);
		q.setTable(FreeItem.class);
		q.setParameter(this);
		return q.execute(Item.class);
	}

	public List<Item> getItems() {
		Query q = new Query(context, "select i.* from # i join # pi on i.id=pi.item where pi.promotion=?");
		q.setTable(Item.class);
		q.setTable(PromotionItem.class);
		q.setParameter(this);
		return q.execute(Item.class);
	}

	public Amount getItemsDiscount() {
		return itemsDiscount;
	}

	public int getMaxUsesPerSession() {
		return maxUsesPerSession;
	}

	public Money getMinSpend() {
		return minSpend;
	}

	public String getName() {
		return name;
	}

	public int getPosition() {
		return position;
	}

	public int getQty() {
		return qty < 1 ? 1 : qty;
	}

	public Date getStartDate() {
		return startDate;
	}

	private Money getTotal(List<Item> items) {
		Money money = new Money(0);
		for (Item item : items)
			money = money.add(item.getSellPrice());
		return money;
	}

	public Amount getTotalDiscount() {
		return totalDiscount;
	}

	public PromotionType getType() {
		return type == null ? PromotionType.OrderDiscount : type;
	}

	public boolean hasEndDate() {
		return endDate != null;
	}

	public boolean hasStartDate() {
		return startDate != null;
	}

	public boolean isExclusive() {
		return exclusive;
	}

	/**
	 * Modifies the list, srcItems, by removing any items it has used in order to make this promotion valid
	 * If this promotion is used then it adds itself to the promotions used list
	 * 
	 * Returns true if this promotion was used.
	 * 
	 */
	public boolean process(Basket basket, List<Promotion> promotionsUsed, List<Item> srcItems) {

		// if we have no items in the basket then do not process!
		if (srcItems.isEmpty()) {
			return false;
		}

		/*
		 * If this basket has already got this promotion applied to it, and we have a max uses per session set up then we should reject this promotion.
		 */
		if (maxUsesPerSession > 0) {

			if (CollectionsUtil.containsCount(promotionsUsed, this) >= maxUsesPerSession) {
				logger.fine("[promotion] rejecting promotion, it is at max uses limit");
				return false;
			}
		}

		List<Item> sessionItems = new ArrayList(srcItems);

		/* first of all check we have enough items in the master list to act on
		 * this is just an optimisation check - no need to check all items in the list
		 * with expensive category lookups etc if we don't have even enough
		 * items to start with !
		 */
		if (sessionItems.size() < getQty()) {
			logger.fine("[promotion] total items is less than required qty (" + getQty() + "), promotion invalid");
			return false;
		}

		final List<Item> qualifyingItems = getItems();
		final List<Category> qualifyingCategories = getCategories();

		/*
		 * Filter out any item that does not occur in the search
		 */
		CollectionsUtil.filter(sessionItems, new Predicate<Item>() {

			public boolean accept(Item item) {

				if (qualifyingItems.contains(item)) {
					logger.fine("[promotion] item is a qualifying item, accepting");
					return true;
				}

                //allow only for selected qualifying cats
                if (qualifyingCategories.isEmpty()) {
                    return false;
                }

                List<Category> qualifyingSubcategories = new ArrayList<Category>();

                for (Category category : qualifyingCategories) {
                    qualifyingSubcategories.addAll(category.getSubcategories());
                }

                qualifyingCategories.addAll(qualifyingSubcategories);

                for (Category itemCategory : item.getCategories()) {
					if (qualifyingCategories.contains(itemCategory)) {
						logger.fine("[promotion] item is in a category that is a qualifying category, accepting");
						return true;
					}

				}

				logger.fine("[Promotion] rejecting item");
				return false;
			}

		});

		/*
		 * After stripping down our list make sure we still have enough to act on
		 */
		if (sessionItems.size() < getQty()) {
			logger.fine("[promotion] valid items is less than required qty (" + getQty() + "), promotion invalid");
			return false;
		}

		/*
		 * Check we have spent enough if a min spend is set
		 */
		if (minSpend.isPositive() && getTotal(sessionItems).isLessThan(minSpend)) {
			logger.fine("[promotion] total is less than min order total, promotion invalid");
			return false;
		}

		/*
		 * Order remaining items by sell price, as we should generally apply promotions to cheapest first
		 */
		Collections.sort(sessionItems, new PriceComparator());

		Money total, discount;

		switch (getType()) {

		default:
		case OrderDiscount:

			if (totalDiscount == null) {
				break;
			}

			// apply order discount to all items
			total = getTotal(srcItems);
			discount = totalDiscount.calculate(total);

			logger.fine("[" + getType() + "] total=" + total + ", discount=" + discount);
            basket.removePromotionLines();
			basket.addPromotionLine(getName(), discount);
			break;

		case FreeItems:

			List<Item> freeItems = getFreeItems();
			logger.fine("[" + getType() + "] free items=" + freeItems);

			for (Item item : freeItems) {
				basket.addPromotionLine(name, item);
			}

			break;

		case ItemsDiscount:

			if (itemsDiscount == null) {
				break;
			}

			total = getTotal(sessionItems);
			discount = itemsDiscount.calculate(total);

			logger.fine("[" + getType() + "] total=" + total + ", discount=" + discount);

			basket.addPromotionLine(getName(), discount);
			break;

		case FixedPrice:

			total = getTotal(sessionItems);
			discount = total.minus(fixedPrice);

			logger.fine("[" + getType() + "] itemstotal=" + total + ", discount=" + discount);

			basket.addPromotionLine(getName(), discount);
			break;
		}

		// if this promotion works on a set qty of items, then remove those items from the list
		for (int n = 0; n < getQty(); n++) {
			srcItems.remove(sessionItems.get(n));
		}

		promotionsUsed.add(this);
		return true;
	}

	/**
	 * 
	 */
	public void removeCategory(Category category) {
		SimpleQuery.delete(context, PromotionCategory.class, "promotion", this, "category", category);
		Basket.flag(context);
	}

	public void removeFreeItem(Item item) {
		SimpleQuery.delete(context, FreeItem.class, "promotion", this, "item", item);
		Basket.flag(context);
	}

	public void removeItem(Item item) {
		SimpleQuery.delete(context, PromotionItem.class, "item", item, "promotion", this);
	}

	@Override
	public synchronized void save() {
		super.save();

		// invalid all baskets using a promotion
		Basket.flag(context);
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setExclusive(boolean exclusive) {
		this.exclusive = exclusive;
	}

	public void setFixedPrice(Money fixedPrice) {
		this.fixedPrice = fixedPrice;
	}

	public void setItemsDiscount(Amount itemsDiscount) {
		this.itemsDiscount = itemsDiscount;
	}

	public void setMaxUsesPerSession(int maxUsesPerSession) {
		this.maxUsesPerSession = maxUsesPerSession;
	}

	public void setMinSpend(Money qualifyingSpend) {
		this.minSpend = qualifyingSpend;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setTotalDiscount(Amount discount) {
		this.totalDiscount = discount;
	}

	public void setType(PromotionType type) {
		this.type = type;
	}

}
