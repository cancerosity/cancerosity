package org.sevensoft.ecreator.model.ecom.shopping.box;

import org.sevensoft.ecreator.iface.admin.ecom.shopping.BasketBoxHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.BasketHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 31-Oct-2005 10:38:54
 * 
 * Basket summary box
 * 
 */
@Table("boxes_basket")
@Label("Basket Summary")
@HandlerClass(BasketBoxHandler.class)
public class BasketBox extends Box {

	public BasketBox(RequestContext context) {
		super(context);
	}

	public BasketBox(RequestContext context, String location) {
		super(context, location);
	}

	@Override
	protected String getCssIdDefault() {
		return "basket";
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	public String render(RequestContext context) {

		if (!Module.Shopping.enabled(context)) {
			return null;
		}

		Basket basket = (Basket) context.getAttribute("basket");
		if (basket == null) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		sb.append(getTableTag());

		if (basket.isEmpty()) {

			sb.append("<tr><td class='empty'>Your shopping basket is empty</td></tr>");

		} else {

			sb.append("<tr><td class='items'>");

			for (BasketLine line : basket.getLines()) {

				sb.append(line.getQty());
				sb.append(" x <b>");

				if (line.hasItem()) {

					Item item = line.getItem();
					sb.append(new LinkTag(item.getUrl(), item.getShortName()));

				} else {

					sb.append(line.getName());

				}

				sb.append("</b><br/>");

			}

			sb.append("</td></tr>");

			sb.append("<tr><td class='total'>Total: <b>");
			sb.append("\243");
			sb.append(basket.getTotalInc());
			sb.append("</b></td></tr>");

			sb.append("<tr><td class='link'>");
			sb.append(new LinkTag(BasketHandler.class, null, "View basket"));
			sb.append("</td></tr>");
		}

		sb.append("<tr><td class='bottom'>&nbsp;</td></tr>");
		sb.append("</table>");

		return sb.toString();
	}

}
