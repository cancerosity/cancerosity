package org.sevensoft.ecreator.model.ecom.vouchers;

import java.util.List;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.RandomHelper;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;
import org.sevensoft.ecreator.model.stats.vouchers.VoucherUser;
import org.sevensoft.ecreator.model.stats.vouchers.VoucherStats;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 02-Mar-2006 12:48:52
 *
 */
@Table("vouchers")
public class Voucher extends EntityObject implements Selectable {

	public static List<Voucher> get(RequestContext context) {
		Query q = new Query(context, "select * from # order by id");
		q.setTable(Voucher.class);
		return q.execute(Voucher.class);
	}

	public static Voucher getByCode(RequestContext context, String code) {

		Query q = new Query(context, "select * from # where code=?");
		q.setTable(Voucher.class);
		q.setParameter(code);
		return q.get(Voucher.class);
	}

	private static List<Voucher> getValid(RequestContext context) {

		return (List<Voucher>) CollectionsUtil.filter(get(context), new Predicate<Voucher>() {

			public boolean accept(Voucher v) {
				return v.isValid();
			}

		});
	}

	public static boolean hasVouchers(RequestContext context) {

		for (Voucher voucher : get(context))
			if (voucher.isValid())
				return true;

		return false;
	}

	public static boolean isSubscription(RequestContext context) {
		return SimpleQuery.count(context, Voucher.class) > 0;
	}

    public static boolean isExistForPackage (RequestContext context, ListingPackage listingPackage){
        return SimpleQuery.count(context, VoucherListingPackage.class, "listingPackage", listingPackage) > 0;
    }

    public static List<VoucherListingPackage> getVouchers(RequestContext context, ListingPackage listingPackage){
        List<VoucherListingPackage> vouchers = new Query(context, "select vlp.* from # vlp where vlp.listingPackage=?")
                .setTable(VoucherListingPackage.class)
                .setParameter(listingPackage)
                .execute(VoucherListingPackage.class);
        return vouchers;
    }

	/*
	 *Amount that must be spent before the voucher can be used.
	 */
	private Money	qualifyingAmount;

	/*
	 *Qty that must be ordred for the voucher to be valid
	 */
	private int		qualifyingQty;

	/*
	 *Code entered by user to enable voucher
	 */
	private String	code;

	/*
	 * Start and end dates for validity of voucher
	 */
	private Date	startDate, endDate;

	/*
	 * 
	 */
	private Amount	discount;

	/**
	 * Rather than offer a discount, give free shipping
	 */
	private boolean	freeDelivery;

	/**
	 * Max number of times a single member can use this voucher
	 */
	private int		maxUsesPerMember;

	/**
	 * Only allow this voucher to be used on a members first order.
	 */
	private boolean	firstOrderOnly;

	/*
	 *A description of this voucher.
	 */
	private String	name;

    /**
     *  If true voucher vill be allowed to be used only for specific items set in voucher
     */
    private boolean specificItemsVoucher;

    /**
     *  If true the voucher will add an item with 0 price to the basket
     */
    private boolean getOneFree;

    /**
     *  If true the voucher will add an extra item from yhe list with 0 price to the basket
     */
    private boolean getExtraItem;

    public Voucher(RequestContext context) {
		super(context);
	}

	public Voucher(RequestContext context, String name) {
		super(context);
		this.name = name;
		setCode();

		save();
	}

    public void addExtraItem(Item extraItem) {
        new VoucherExtraItem(context, this, extraItem);
    }

    public void addItem(Item item) {
        new VoucherItem(context, this, item);
    }

    public void addListingPackage(ListingPackage listingPackage) {
        new VoucherListingPackage(context, this, listingPackage);
    }

    @Override
	public synchronized boolean delete() {

		// remove vouchers from baskets
		Query q = new Query(context, "update # set recalc=1, voucher=0 where voucher=?");
		q.setParameter(this);
		q.setTable(Basket.class);
		q.run();

		return super.delete();
	}

	private void flagBaskets() {
		// update baskets
		Query q = new Query(context, "update # set recalc=1 where voucher=?");
		q.setParameter(this);
		q.setTable(Basket.class);
		q.run();
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return getName() + " " + getCode();
	}

	public Amount getDiscount() {
		return discount;
	}

	/**
	 * Returns the EX vat discount
	 */
	public Money getDiscount(Money toDiscount) {

		if (discount == null) {
			return new Money(0);
		}

		Money d = discount.calculate(toDiscount);

		// if a free delivery discount then make sure we don't discount more than the delivery!
		if (isFreeDelivery()) {
			return d.isLessThan(toDiscount) ? d : toDiscount;
		} else {
			return d;
		}
	}

	public Date getEndDate() {
		return endDate;
	}

    public List<Item> getExtraItems() {
        Query q = new Query(context, "select i.* from # i join # vei on i.id=vei.extraitem where vei.voucher=?");
        q.setTable(Item.class);
        q.setTable(VoucherExtraItem.class);
        q.setParameter(this);
        return q.execute(Item.class);
    }

    public List<Item> getItems() {
        Query q = new Query(context, "select i.* from # i join # vi on i.id=vi.item where vi.voucher=?");
        q.setTable(Item.class);
        q.setTable(VoucherItem.class);
        q.setParameter(this);
        return q.execute(Item.class);
    }

    public List<ListingPackage> getListingPackages() {
        Query q = new Query(context, "select lp.* from # lp join # vlp on lp.id=vlp.listingPackage where vlp.voucher=?");
        q.setTable(ListingPackage.class);
        q.setTable(VoucherListingPackage.class);
        q.setParameter(this);
        return q.execute(ListingPackage.class);
    }

    public String getLabel() {
		return getDescription();
	}

	public int getMaxUsesPerMember() {
		return maxUsesPerMember;
	}

	public String getName() {
		return name;
	}

	public Money getQualifyingAmount() {
		return qualifyingAmount == null ? new Money(0) : qualifyingAmount;
	}

	public int getQualifyingQty() {
		return qualifyingQty;
	}

	public Date getStartDate() {
		return startDate;
	}

	public String getValue() {
		return getIdString();
	}

	public boolean hasEndDate() {
		return endDate != null;
	}

	public boolean hasMaxUses() {
		return maxUsesPerMember > 0;
	}

	public boolean hasStartDate() {
		return startDate != null;
	}

    public boolean isFirstOrderOnly() {
		return firstOrderOnly;
	}

    public boolean isFreeDelivery() {
	return freeDelivery;
}

    public boolean isGetExtraItem() {
        return getExtraItem;
    }

    public boolean isGetOneFree() {
        return getOneFree;
    }

	public boolean isSpecificItemsVoucher() {
	        return specificItemsVoucher;
	    }

	protected boolean isValid() {
		return (startDate == null || !startDate.isFuture()) && (endDate == null || !endDate.isPast());
	}

	public String isValid(Vouchable vouchable) {

        String maxUsers = isValidMaxUsers(vouchable);

        if (maxUsers != null) {
            return maxUsers;
        }
		if (getQualifyingQty() > 0) {

			if (vouchable.getVoucherQty() < getQualifyingQty())
				return "You can only use this voucher when purchasing at least " + getQualifyingQty() + " items.";

		}

		if (getQualifyingAmount().isPositive()) {

			if (vouchable.getVoucherTotal().isLessThan(getQualifyingAmount()))
				return "You can only use this voucher when you spend at least " + getQualifyingAmount();

		}

		if (hasStartDate() && getStartDate().isFuture())
			return "This voucher is only valid on or after " + getStartDate().toString("dd-MMM-yyyy");

		if (hasEndDate() && getEndDate().isPast())
			return "This voucher expired on " + getEndDate().toString("dd-MMM-yyyy");

		return null;
	}

    private String isValidMaxUsers(Vouchable vouchable) {
        Item member = vouchable.getCustomer();
		if (member != null) {

			if (hasMaxUses()) {

				if (member.getVoucherUses(this) >= getMaxUsesPerMember())
					return "You can only use this voucher a maximum of " + getMaxUsesPerMember() + " times.";

			}

			if (isFirstOrderOnly()) {

				if (member.getOrderCount() > 0)
					return "You can only use this voucher on your first order";

			}
		}
        return null;
    }

    public String isValidForListings(Vouchable vouchable){
        String maxUsers = isValidMaxUsers(vouchable);

        if (maxUsers != null) {
            return maxUsers;
        }

        if (hasStartDate() && getStartDate().isFuture())
			return "The start date for this voucher is in the future";

		if (hasEndDate() && getEndDate().isPast())
			return "This voucher code expired";

        return null;
    }

    public void removeExtraItem(Item extraItem) {
        SimpleQuery.delete(context, VoucherExtraItem.class, "extraItem", extraItem, "voucher", this);
    }

    public void removeItem(Item item) {
        SimpleQuery.delete(context, VoucherItem.class, "item", item, "voucher", this);
    }

    public void removeListingPackage(ListingPackage listingPackage) {
        SimpleQuery.delete(context, VoucherListingPackage.class, "listingPackage", listingPackage, "voucher", this);
    }

    @Override
	public synchronized void save() {
		super.save();

		// requirements for this voucher may have changed - flag basket for recalc
		flagBaskets();
	}

    public void setCode() {
		this.code = RandomHelper.getRandomString(8).toUpperCase();
	}

    public void setCode(String code) {
	this.code = code;
}

	/**
	 * @param discount
	 */
	public void setDiscount(Amount discount) {

		this.discount = discount;
		flagBaskets();
	}

	public void setEndDate(Date d) {
		this.endDate = d;
	}

	public void setFirstOrderOnly(boolean firstOrderOnly) {
			this.firstOrderOnly = firstOrderOnly;
		}

    public void setFreeDelivery(boolean freeShipping) {
	this.freeDelivery = freeShipping;
}

    public void setGetExtraItem(boolean getExtraItem) {
        this.getExtraItem = getExtraItem;
    }

	public void setGetOneFree(boolean getOneFree) {
	        this.getOneFree = getOneFree;
	    }

    public void setMaxUsesPerMember(int maxUsesPerMember) {
		this.maxUsesPerMember = maxUsesPerMember;
	}

    public void setName(String name) {
	this.name = name;
}

	public void setQualifyingAmount(Money m) {
		this.qualifyingAmount = (m == null ? new Money(0) : m);
	}

	public void setQualifyingQty(int i) {
		this.qualifyingQty = i;
	}

	public void setSpecificItemsVoucher(boolean specificItemsVoucher) {
	        this.specificItemsVoucher = specificItemsVoucher;
	    }

	public void setStartDate(Date d) {
		this.startDate = d;
	}

    public boolean isFree(){
//        new Money(0).equals(getD);
        return false;
    }

    public void incUsage(RequestContext context,  Item account, ListingPackage lp){
        VoucherUser.getUser(context, this, account, lp);
        VoucherStats.inc(context, this);
    }

}
