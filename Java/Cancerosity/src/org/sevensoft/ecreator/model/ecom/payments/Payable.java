package org.sevensoft.ecreator.model.ecom.payments;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.forms.able2buy.CreditGroup;
import org.sevensoft.ecreator.model.items.Item;

/**
 * @author sks 26 Jul 2006 17:20:21
 * 
 * Any class that implements this interface can accept payments for it 
 */
public interface Payable {

	/**
	 * 
	 */
	public CreditGroup getAble2BuyCreditGroup();

	/**
	 * Returns the card object used for the credit card payment type on this order
	 */
	public Card getCard();

	/**
	 * Returns the database id for this payable
	 */
	public int getId();

	public String getIdString();

	/**
	 * 
	 */
	public Item getPayableAccount();

	/**
	 * 
	 */
	public Address getPaymentAddress();

	/**
	 * How much is owed to make a payment
	 */
	public Money getPaymentAmount();

	/**
	 * Returns a String that can be used as a description of what the customer is buying.
	 */
	public String getPaymentDescription();

	/**
	 * 
	 */
	public String getPaymentFailureUrl(PaymentType paymentType);

	/**
	 * 
	 */
	public String getPaymentSuccessUrl(PaymentType paymentType);

	/**
	 * Returns the payment type used by this object
	 */
	public PaymentType getPaymentType();

    /**
     * @return
     */
    public String getBasketUrl();
}
