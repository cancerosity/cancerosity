package org.sevensoft.ecreator.model.ecom.credits;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 11 Jul 2006 21:49:55
 * History of all bought credit packages
 */
@Table("credits_history")
public class CreditHistory extends EntityObject {

	private int		credits;
	private Item	member;
	private String	paymentDetails;
	private Money	fee;
	private String	ipAddress;

	public CreditHistory(RequestContext context) {
		super(context);
	}

	public CreditHistory(RequestContext context, Item member, CreditPackage creditPackage, Payment payment, String ipAddress) {
		super(context);

		this.member = member;
		this.credits = creditPackage.getCredits();
		this.fee = creditPackage.getFee();
		this.ipAddress = ipAddress;
		this.paymentDetails = payment.getInfoString();

		save();
	}

	public CreditHistory(RequestContext context, Item account, String string) {
		super(context);
	}

	public Item getCreator() {
		return member.pop();
	}

	public int getCredits() {
		return credits;
	}

	public Money getFee() {
		return fee;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public String getPaymentDetails() {
		return paymentDetails;
	}

}
