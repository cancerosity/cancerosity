package org.sevensoft.ecreator.model.ecom.payments.forms;

import java.io.IOException;
import java.util.Map;
import java.util.logging.Logger;

import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.PaymentException;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13-Jul-2005 01:10:06
 * 
 */
public abstract  class Form implements Logging {

	protected static Logger			logger	= Logger.getLogger("payments");

	protected final RequestContext	context;

	public Form(RequestContext context) {
		this.context = context;
	}

	/**
	 * Returns either post or get depending on what we should use for this form
	 * 
	 * @return
	 */
	public abstract String getMethod();

	/**
	 * @param member 
	 * @throws IOException 
	 * @throws FormException 
	 * 
	 */
	public abstract Map<String, String> getParams(Payable payable) throws IOException, FormException;

	/**
	 * Returns the URL to re-direct the user to for payment.
	 * 
	 * @param parameters
	 * @throws PaymentException
	 */
	public abstract String getPaymentUrl();

	/**
	 * A callback for when the payment form sends the response data back (either as encrypted data or via a token for a lookup) directly with the customer
	 * to the success url
	 */
	public abstract void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException;

	/**
	 * A callback for when the payment form makes a post (usually) to the server directly
	 * Return an url for forwarding back to the site
	 */
	public abstract String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException;

    public void getHeader(RequestContext payable) {
    }

    public String getFullId() {
        return null;
    }

    public String getLogName() {
        return getClass().getSimpleName();
    }

    public void log(String message) {
        new LogEntry(context, this, message);
    }

    public void log(User user, String message) {
    }
}
