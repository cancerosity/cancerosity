package org.sevensoft.ecreator.model.ecom.orders.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderLineMarker;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;
import java.util.Set;
import java.util.LinkedHashSet;

/**
 * User: Tanya
 * Date: 25.02.2011
 */
public class InvoiceDeliveryLinesMarker extends MarkerHelper implements IOrderLineMarker {

    public Object generate(RequestContext context, Map<String, String> params, OrderLine line) {
        StringBuilder sb = new StringBuilder();
        Order order = line.getOrder();
        if (order.hasDeliveryDetails()) {

            Set<Attribute> columns = new LinkedHashSet<Attribute>();

            if (line.hasItem()) {

                ItemType type = line.getItem().getItemType();
                for (Attribute attribute : type.getAttributes()) {

                    if (attribute.isInvoiceColumn())
                        columns.add(attribute);
                }
            }
            sb.append("<tr>");
            sb.append("<td>1</td>");
            sb.append("<td></td>");

            for (int n = 0; n < columns.size(); n++) {
                sb.append("<td></td>");
            }

            sb.append("<td>" + order.getDeliveryDetails() + "</td>");
            sb.append("<td align='right'>" + order.getDeliveryChargeEx() + "</td>");
            sb.append("<td align='right'>" + order.getDeliveryChargeEx() + "</td>");
            sb.append("</tr>	");
        }
        return sb.toString();
    }

    public Object getRegex() {
        return "invoice_delivery_lines";
    }


}
