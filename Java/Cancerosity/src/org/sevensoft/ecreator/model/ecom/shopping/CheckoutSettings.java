package org.sevensoft.ecreator.model.ecom.shopping;

import org.sevensoft.ecreator.model.attributes.support.AttributeOwnerSupport;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 6 Jul 2006 18:01:36
 *
 */
@Table("settings_checkout")
@Singleton
public class CheckoutSettings extends AttributeOwnerSupport {

	public static CheckoutSettings getInstance(RequestContext context) {
		return getSingleton(context, CheckoutSettings.class);
	}

	public CheckoutSettings(RequestContext context) {
		super(context);
	}

}
