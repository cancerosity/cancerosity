package org.sevensoft.ecreator.model.ecom.payments.cards.exceptions;

/**
 * @author sks 26-Oct-2005 10:57:59
 * 
 */
public class ExpiryDateException extends Exception {

	public ExpiryDateException() {
	}

	public ExpiryDateException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExpiryDateException(String message) {
		super(message);
	}

	public ExpiryDateException(Throwable cause) {
		super(cause);
	}


}
