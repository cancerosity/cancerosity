package org.sevensoft.ecreator.model.ecom.vouchers;

import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.items.Item;

/**
 * User: Dmitry Lebedev
 * Date: 05.06.2009
 * Time: 15:53:37
 */
@Table("vouchers_items")
@SuppressWarnings("unused")
public class VoucherItem extends EntityObject {

    private Voucher voucher;
    private Item item;

    public VoucherItem(RequestContext context) {
        super(context);
    }

    public VoucherItem(RequestContext context, Voucher voucher, Item item) {
        super(context);

        if(!voucher.getItems().contains(item)) {
            this.voucher = voucher;
            this.item = item;

            save();
        }
    }
}
