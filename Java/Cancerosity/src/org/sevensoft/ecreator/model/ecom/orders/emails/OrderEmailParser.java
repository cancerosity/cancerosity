package org.sevensoft.ecreator.model.ecom.orders.emails;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 16 Feb 2007 13:50:36
 *
 */
public class OrderEmailParser {

	private RequestContext	context;

	public OrderEmailParser(RequestContext context) {
		this.context = context;
	}

	public String parse(Order order, String body, String linesBody) {

		StringBuilder sb = new StringBuilder();

		sb.append(parse(body, order));

		if (linesBody != null) {
			for (OrderLine line : order.getLines()) {
				sb.append(parse(linesBody, line));
			}
		}

		return sb.toString();
	}

	private String parse(String string, Order order) {

		if (string == null) {
			return null;
		}

		string = string.replace("[order]", order.getOrderId());
		string = string.replace("[id]", order.getOrderId());
		string = string.replace("[name]", order.getAccount().getName());
		string = string.replace("[email]", order.getAccount().getEmail());
		string = string.replace("[lines_qty]", String.valueOf(order.getLineCount()));
		string = string.replace("[items_qty]", String.valueOf(order.getItemCount()));

		string = string.replace("[delivery_details]", order.getDeliveryDetails());

		Address deliveryAddress = order.getDeliveryAddress();
		Address billingAddress = order.getBillingAddress();

		string = string.replace("[delivery_address]", deliveryAddress == null ? "" : deliveryAddress.getLabel());
		string = string.replace("[billing_address]", billingAddress == null ? "" : billingAddress.getLabel());

		return string;
	}

	public String parse(String string, OrderLine line) {

		if (string == null) {
			return null;
		}

		string = string.replace("[name]", line.getDescription());
		string = string.replace("[description]", line.getDescription());
		string = string.replace("[qty]", String.valueOf(line.getQty()));
		string = string.replace("[unit_price_ex]", line.getUnitSellEx().toEditString());
		string = string.replace("[unit_price_inc]", line.getUnitSellInc().toEditString());
		string = string.replace("[line_price_ex]", line.getUnitSellEx().toEditString());
		string = string.replace("[line_price_inc]", line.getLineSellInc().toEditString());

		if (line.hasItem()) {

			StringBuffer sb = new StringBuffer();

			Pattern pattern = Pattern.compile("\\[attribute_(\\d+)\\]", Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(string);
			while (matcher.find()) {

				Attribute attribute = EntityObject.getInstance(context, Attribute.class, matcher.group(1));
				String value = line.getItem().getAttributeValue(attribute);

				matcher.appendReplacement(sb, value);
			}

			matcher.appendTail(sb);
			string = sb.toString();
		}

		string = parse(string, line.getOrder());

		return string;
	}
}
