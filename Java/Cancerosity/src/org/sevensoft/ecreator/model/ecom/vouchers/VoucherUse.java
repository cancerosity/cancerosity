package org.sevensoft.ecreator.model.ecom.vouchers;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 7 Oct 2006 15:37:35
 *
 */
@Table("vouchers_uses")
public class VoucherUse extends EntityObject {

	private Item	account;
	private Voucher	voucher;
	private DateTime	date;
	private Order	order;

	private VoucherUse(RequestContext context) {
		super(context);
	}

	public VoucherUse(RequestContext context, Order order, Voucher voucher) {
		super(context);
		this.order = order;
		this.account = order.getAccount();
		this.voucher = voucher;
		this.date = new DateTime();

		save();
	}

}
