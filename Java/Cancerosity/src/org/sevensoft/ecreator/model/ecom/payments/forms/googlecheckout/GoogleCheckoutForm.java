package org.sevensoft.ecreator.model.ecom.payments.forms.googlecheckout;

import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.results.Callback;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.*;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.gaia.Country;

import java.util.Map;
import java.util.LinkedHashMap;
import java.util.List;
import java.io.IOException;

import sun.misc.BASE64Encoder;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
public class GoogleCheckoutForm extends Form {

    private static String TestUrl = "https://sandbox.google.com/checkout/api/checkout/v2/checkoutForm/Merchant/";

    private static String LiveUrl = "https://checkout.google.com/api/checkout/v2/checkoutForm/Merchant/";

    /**
     * Google sends a new order notification when a buyer places an order through Google Checkout.
     */
    private static final String NEW_ORDER = "new-order-notification";

    /**
     * Google Checkout sends a risk information notification after completing its risk analysis on a new order.
     * A risk-information-notification includes financial information such as the customer's billing address,
     * a partial credit card number and other values that help you verify that an order is not fraudulent.
     * Note: Before shipping the items in the order, you should wait until you have also received the new order
     * notification for the order and the order state change notification informing you that the order's financial state
     * has been updated to CHARGEABLE.
     */
    private static final String RISK_INFORMATION = "risk-information-notification";

    /**
     * Google sends an order state change notification to notify you that an order's financial status or its fulfillment status
     * has changed. The notification identifies the new financial and fulfillment statuses for the order as well as
     * the previous statuses for that order.
     * These status changes can be triggered by Order Processing API commands that you send to Google Checkout.
     * For example, if you send Google Checkout a cancel-order request, Google will respond through the Notification API
     * to inform you that it changed the order's status to CANCELLED.
     */
    private static final String ORDER_STATE_CHANGE = "order-state-change-notification";

    //Amount Notifications

    /**
     * Google Checkout sends a <charge-amount-notification> after successfully charging a customer for an order.
     */
    private static final String CHARGE_AMOUNT = "charge-amount-notification";

    /**
     * Google Checkout sends a <refund-amount-notification> after successfully executing a refund-order order processing command.
     * You should not assume that Google has granted a refund until you receive this notification.
     * Typically, this notification is sent within seconds of the corresponding refund-order request.
     */
    private static final String REFUND_AMOUNT = "refund-amount-notification";

    /**
     * Google Checkout sends a <chargeback-amount-notification> when a customer initiates a chargeback against
     * an order and Google approves the chargeback.
     */
    private static final String CHARGEBACK_AMOUNT = "chargeback-amount-notification";

    /**
     * Google Checkout sends an <authorization-amount-notification> in response to a successful request for an explicit
     * credit card reauthorization.
     * You can also modify your Checkout API request so that Google sends an <authorization-amount-notification> after
     * Google authorizes the buyer's credit card for a new order. Include the following HTTP POST request parameter in your
     * Checkout API request so that you receive an authorization amount notification for the initial credit card authorization:
     * "_type=checkout-shopping-cart
     * &order-processing-support.request-initial-auth-details=true"
     */
    private static final String AUTHORIZATION_AMOUNT = "authorization-amount-notification";

    private FormSession session;

    public GoogleCheckoutForm(RequestContext context) {
        super(context);
    }

    public String getMethod() {
        return "POST";
    }

    /**
     * <input type="hidden" name="item_name_1" value="Peanut Butter"/>
     * <input type="hidden" name="item_description_1" value="Chunky peanut butter."/>
     * <input type="hidden" name="item_quantity_1" value="1"/>
     * <input type="hidden" name="item_price_1" value="3.99"/>
     * <input type="hidden" name="item_currency_1" value="USD"/>
     * <p/>
     * <input type="hidden" name="ship_method_name_1" value="UPS Ground"/>
     * <input type="hidden" name="ship_method_price_1" value="10.99"/>
     * <p/>
     * <input type="hidden" name="ship_method_name_2" value="UPS - International"/>
     * <input type="hidden" name="ship_method_price_2" value="19.99"/>
     * <input type="hidden" name="ship_method_currency_2" value="USD"/>
     * <input type="hidden" name="ship_method_world_2" value=""/>
     * <input type="hidden"
     * name="checkout-flow-support.merchant-checkout-flow-support.shipping-methods.flat-rate-shipping-2.shipping-restrictions.excluded-areas.postal-area-1.country-code"
     * value="US"/>
     * <input type="hidden"
     * name="checkout-flow-support.merchant-checkout-flow-support.shipping-methods.flat-rate-shipping-2.shipping-restrictions.excluded-areas.postal-area-2.country-code"
     * value="KP"/>
     * <input type="hidden" name="tax_rate" value="0.0875"/>
     * <input type="hidden" name="tax_us_state" value="NY"/>
     * <p/>
     * <input type="hidden" name="_charset_"/>
     *
     * @param payable - order
     * @return map of hidden params
     * @throws IOException
     * @throws FormException
     */
    public Map<String, String> getParams(Payable payable) throws IOException, FormException {
        session = new FormSession(context, payable);

        Map<String, String> map = new LinkedHashMap();
        //todo add params
        if (payable instanceof Order) {
            Order order = (Order) payable;

            String currency = Currency.getDefault(context).getName();
            //item-level input fields
            int n = 1;
            for (OrderLine orderLine : order.getLines()) {
                if (orderLine.getItem() != null) {
                    map.put("item_name_" + n, orderLine.getItem().getName());
                    map.put("item_description_" + n, orderLine.getDescription());
                    map.put("item_quantity_" + n, String.valueOf(orderLine.getQty()));
                    map.put("item_price_" + n, orderLine.getUnitSellInc().toString());
//                     map.put("item_price_" + n, orderLine.getUnitSellEx().toString());
//                    map.put("item_price_" + n, orderLine.getLineSellEx().toString()); // TODO incorrect!!!
                    map.put("item_currency_" + n, currency);

                    n++;
                }
            }

            if (order.hasVoucher()) {

                map.put("item_name_" + Integer.toString(order.getLines().size() + 1), order.getVoucherDescription());
                map.put("item_description_" + Integer.toString(order.getLines().size() + 1), "Voucher for your order");
                map.put("item_quantity_" + Integer.toString(order.getLines().size() + 1), "1");
                map.put("item_price_" + Integer.toString(order.getLines().size() + 1), order.getVoucherDiscountInc().toString());
                map.put("item_currency_" + Integer.toString(order.getLines().size() + 1), currency);

            }

            PaymentSettings paymentSettings = PaymentSettings.getInstance(context);
            String merchantId = paymentSettings.getGoogleCheckoutId();

            map.put("item_merchant_id_1", merchantId);
            //delivery address can be null, when direct google checkout
            if (order.getDeliveryAddress() == null || order.getDeliveryAddress().getCountry().equals(Country.UK)) {
                //shipping input fields
                map.put("ship_method_name_1", order.getDeliveryDetails());
//            map.put("ship_method_price_1", order.getDeliveryCharge().toString());
                map.put("ship_method_price_1", order.getDeliveryChargeInc().toString());
                map.put("ship_method_currency_1", currency);
//                map.put("ship_method_country_1", "UK");
                map.put("ship_method_country_1", Company.getInstance(context).getCountry().getIsoAlpha2());
            } else {
                //worldwide
                map.put("ship_method_name_2", order.getDeliveryDetails());
                map.put("ship_method_price_2", order.getDeliveryChargeInc().toString());     //todo delivery charge for international shipping
                map.put("ship_method_currency_2", currency);  //todo should we use  GBP
                map.put("ship_method_world_2", "");

                //the country where the shipping method is not available.
                map.put("checkout-flow-support.merchant-checkout-flow-support.shipping-methods.flat-rate-shipping-2.shipping-restrictions.excluded-areas.postal-area-1.country-code", "UK");
                map.put("checkout-flow-support.merchant-checkout-flow-support.shipping-methods.flat-rate-shipping-2.shipping-restrictions.excluded-areas.postal-area-2.country-code", "KP");
            }

            //tax input fields
// that param is a vat rate for "item_price". Because of products can have different vat rates we add vat to item_price_.
            map.put("tax_rate", "0.0"); /*String.valueOf(order.getDeliveryVatRate()/100)*/
//            map.put("tax_rate", order.getLinesVat().toString());
//            map.put("tax_world", "");
//            map.put("tax_country", "UK");
            map.put("tax_country", Company.getInstance(context).getCountry().getIsoAlpha2());

            //shopping cart input fields
            if (payable.getBasketUrl() != null) {
                map.put("edit_url", payable.getBasketUrl());
            }
            map.put("continue_url", payable.getPaymentSuccessUrl(PaymentType.GoogleCheckout).replaceAll("http://", "https://"));
            map.put("shopping-cart.merchant-private-data", session.getIdString());

        }
        return map;
    }

    public String getPaymentUrl() {
        PaymentSettings paymentSettings = PaymentSettings.getInstance(context);
        return getPaymentUrl(paymentSettings);
    }

    public String getPaymentUrl(PaymentSettings paymentSettings) {
        String merchantId = paymentSettings.getGoogleCheckoutId();
        return paymentSettings.isGoogleCheckoutLive() ? LiveUrl + merchantId : TestUrl + merchantId;
    }

    public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
//        FormSession formSession = EntityObject.getInstance(context, FormSession.class, session.getIdString());
//        String googleOrderNumber = params.get("google-order-number");
//        String serialNumber = params.get("serial-number");
//        Money orderTotal = new Money(params.get("order-total"));
//        Order order = Order.getLatestOrders(context, 1).get(0);
//        order.setGoogleOrderNumber(googleOrderNumber + "**" + params.toString());
//        order.save();
//
//        Payment payment = getPayment(session, orderTotal, serialNumber);
//		if (payment == null) {
//			return;
//		}
//        formSession.callback(params, ipAddress, payment);
    }

    private Payment getPayment(FormSession session, Money amount, String authCode) {

        // Check for duplicated payments!!!!
		if (Payment.exists(context, session.getIdString(), PaymentType.GoogleCheckout)) {
			return null;
		}

        Payment payment = new Payment(context, session.getAccount(), amount, PaymentType.GoogleCheckout);
        payment.setAuthCode(authCode);

        // it seems they use our id as theirs too
        payment.setProcessorTransactionId(session.getIdString());
        payment.save();

        return payment;
    }


    public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
        log("[GoogleCheckout] Server callback params=" + params);
        logger.severe("[GoogleCheckout] Server callback params=" + params);

        String auth = context.getHeader("Authorization");
        if (auth != null) {
            auth = auth.replaceAll("Basic ", "");
            PaymentSettings paymentSettings = PaymentSettings.getInstance(context);
            String merchantId = paymentSettings.getGoogleCheckoutId();
            String password = paymentSettings.getGoogleCheckoutPw();
            String authorization = new BASE64Encoder().encode((merchantId + ":" + password).getBytes());
            if (!auth.equals(authorization)) {
                log("[GoogleCheckout] WRONG AUTHORIZATION! :" + auth);
                logger.severe("[GoogleCheckout] WRONG AUTHORIZATION!" + auth);
                return null;
            }
            log("[GoogleCheckout] AUTHORIZATION OK");
            logger.severe("[GoogleCheckout] AUTHORIZATION OK : " + auth);
        }

        new Callback(context, PaymentType.GoogleCheckout, params);

        String notificationType = params.get("_type");
        String googleOrderNumber = params.get("google-order-number");
        String serialNumber = params.get("serial-number");
        Money orderTotal = new Money(0);
        if (params.get("order-total") != null) {
            orderTotal = new Money(params.get("order-total"));
        }
        Order order = null;

        FormSession session = EntityObject.getInstance(context, FormSession.class, params.get("shopping-cart.merchant-private-data"));
        if (session == null) {
            log("[GoogleCheckout] no session found for id=" + params.get("shopping-cart.merchant-private-data"));
            logger.severe("[GoogleCheckout] no session found for id=" + params.get("shopping-cart.merchant-private-data"));
            return null;
        }
        order = session.getOrder();
        if (order == null) {
            new Callback(context, PaymentType.GoogleCheckout, " ---------- Order is NULL: paymentType='GoogleCheckout', totalAmount=" + orderTotal);
//            return null;
        }

        if (order == null) {
            List<Order> orders = Order.getByGoogleOrderNumber(context, googleOrderNumber);
            if (orders != null && !orders.isEmpty()) {
                order = orders.get(0);
            } else {
                List<Order> lastOrders = Order.getLatestOrders(context, 1, PaymentType.GoogleCheckout, orderTotal);
                if (!lastOrders.isEmpty())
                    order = lastOrders.get(0);
            }
        }
        if (order == null) {
            return null;
        }
        
        order.setGoogleOrderNumber(googleOrderNumber);
        order.addMessage(notificationType, serialNumber, params.toString());
        if (notificationType != null) {
            if (notificationType.equals(NEW_ORDER)) {
                setNewOrderInfo(params, order);
            } else if (notificationType.equals(RISK_INFORMATION)) {
                //todo next stage
//   _type=risk-information-notification
//  &serial-number=0b95f758-0332-45d5-aced-5da64c8fc5b9
//  &google-order-number=841171949013218
//  &risk-information.eligible-for-protection=true
//  &risk-information.billing-address.contact-name=John%20Smith
//  &risk-information.billing-address.email=johnsmith%40example.com
//  &risk-information.billing-address.address1=10%20Example%20Road
//  &risk-information.billing-address.city=Sampleville
//  &risk-information.billing-address.region=CA
//  &risk-information.billing-address.postal-code=94141
//  &risk-information.billing-address.country-code=US
//  &risk-information.avs-response=Y
//  &risk-information.cvn-response=M
//  &risk-information.partial-cc-number=3719
//  &risk-information.ip-address=10.11.12.13
//  &risk-information.buyer-account-age=6
//  &timestamp=2007-03-19T15%3A06%3A26.051Z
            } else if (notificationType.equals(ORDER_STATE_CHANGE)) {
                order.setStatus(params.get("new-fulfillment-order-state"));
                order.setFinansialStatus(params.get("new-financial-order-state"));
//   _type=order-state-change-notification
//  &serial-number=c821426e-7caa-4d51-9b2e-48ef7ecd6423
//  &google-order-number=841171949013218
//  &new-financial-order-state=CHARGING
//  &new-fulfillment-order-state=NEW
//  &previous-financial-order-state=CHARGEABLE
//  &previous-fulfillment-order-state=NEW
//  &timestamp=2007-03-19T15:06:29.051Z
            } else if (notificationType.equals(CHARGE_AMOUNT)) {
//   _type=charge-amount-notification
//  &serial-number=bea6bc1b-e1e2-44fe-80ff-2391b25c2510
//  &google-order-number=841171949013218
//  &latest-charge-amount=226.06
//  &latest-charge-amount.currency=USD
//  &total-charge-amount=226.06
//  &total-charge-amount.currency=USD
//  &timestamp=2007-03-19T15:06:26.051Z
            } else if (notificationType.equals(REFUND_AMOUNT)) {
                //todo Refund is not supported. next stage
// _type=refund-amount-notification
//  &serial-number=bea6bc1b-e1e2-44fe-80ff-2391b25c2510
//  &google-order-number=841171949013218
//  &latest-refund-amount=226.06
//  &latest-refund-amount.currency=USD
//  &total-refund-amount=226.06
//  &total-refund-amount.currency=USD
//  &timestamp=2007-03-19T15:06:26.051Z

//            Money refund = new Money(params.get("latest-refund-amount"));
//            order.setAmountPaid(order.getTotalInc().subtract(refund));
            } else if (notificationType.equals(CHARGEBACK_AMOUNT)) {
//  _type=chargeback-amount-notification
//  &serial-number=bea6bc1b-e1e2-44fe-80ff-2391b25c2510
//  &google-order-number=841171949013218
//  &latest-chargeback-amount=226.06
//  &latest-chargeback-amount.currency=USD
//  &total-chargeback-amount=226.06
//  &total-chargeback-amount.currency=USD
//  &timestamp=2007-03-19T15:06:26.051Z
            } else if (notificationType.equals(AUTHORIZATION_AMOUNT)) {
                //todo is not supported. next stage
//  _type=authorization-amount-notification
//  &serial-number=bea6bc1b-e1e2-44fe-80ff-2391b25c2510
//  &google-order-number=841171949013218
//  &authorization-amount=226.06
//  &authorization-amount.currency=USD
//  &authorization-expiration-date=2007-03-26T15:06:26.000Z
//  &avs-response=Y
//  &cvn-response=Y
//  &timestamp=2007-03-19T15:06:26.051Z

            }
        }
        order.save();

        if (session == null) {
            session = FormSession.getByOrder(context, order.getId());
            logger.severe("[" + GoogleCheckoutForm.class + "] session=" + session);
        }
        if (session != null) {
            FormSession formSession = session;//EntityObject.getInstance(context, FormSession.class, session.getIdString());

            Payment payment = getPayment(formSession, orderTotal, serialNumber);
            if (payment == null) {
                return null;
            }
            formSession.callback(params, ipAddress, payment);
        }
        return null;
    }

    /**
     * @param params _type=new-order-notification
     *               &serial-number=85f54628-538a-44fc-8605-ae62364f6c71
     *               &google-order-number=841171949013218
     *               &buyer-shipping-address.contact-name=Will%20Shipp-Toomey
     *               &buyer-shipping-address.email=willstoomey%40example.com
     *               &buyer-shipping-address.address1=10%20Example%20Road
     *               &buyer-shipping-address.city=Sampleville
     *               &buyer-shipping-address.region=CA
     *               &buyer-shipping-address.postal-code=94141
     *               &buyer-shipping-address.country-code=US
     *               &buyer-shipping-address.phone=5555551234
     *               &buyer-shipping-address.structured-name.first-name=Will
     *               &buyer-shipping-address.structured-name.last-name=Shipp-Toomey
     *               &buyer-billing-address.contact-name=Bill%20Hu
     *               &buyer-billing-address.email=billhu%40example.com
     *               &buyer-billing-address.address1=99%20Credit%20Lane
     *               &buyer-billing-address.city=Mountain%20View
     *               &buyer-billing-address.region=CA
     *               &buyer-billing-address.postal-code=94043
     *               &buyer-billing-address.country-code=US
     *               &buyer-billing-address.phone=5555557890
     *               &buyer-billing-address.structured-name.first-name=Bill
     *               &buyer-billing-address.structured-name.last-name=Hu
     *               &buyer-id=294873009217523
     *               &fulfillment-order-state=NEW
     *               &financial-order-state=REVIEWING
     *               &shopping-cart.cart-expiration.good-until-date=2007-12-31T23%3A59%3A59-05%3A00
     *               &shopping-cart.items.item-1.merchant-item-id=GGLAA1453
     *               &shopping-cart.items.item-1.item-name=Dry%20Food%20Pack
     *               &shopping-cart.items.item-1.item-description=One%20pack%20of%20nutritious%20dried%emergency%20food.
     *               &shopping-cart.items.item-1.quantity=1
     *               &shopping-cart.items.item-1.tax-table-selector=food
     *               &shopping-cart.items.item-1.unit-price=4.99
     *               &shopping-cart.items.item-1.unit-price.currency=USD
     *               &shopping-cart.items.item-2.merchant-item-id=GGLAA1453
     *               &shopping-cart.items.item-2.item-name=Megasound%202GB%20MP3%20Player
     *               &shopping-cart.items.item-2.item-description=Portable%20player%20holds%20500%20songs.
     *               &shopping-cart.items.item-2.quantity=1
     *               &shopping-cart.items.item-2.tax-table-selector=food
     *               &shopping-cart.items.item-2.unit-price=179.99
     *               &shopping-cart.items.item-2.unit-price.currency=USD
     *               &shopping-cart.items.item-2.merchant-private-item-data=merchant-product-id%3D1234567890
     *               &order-adjustment.total-tax=11.05
     *               &order-adjustment.total-tax.currency=USD
     *               &order-adjustment.shipping.flat-rate-shipping-adjustment.shipping-name=SuperShip
     *               &order-adjustment.shipping.flat-rate-shipping-adjustment.shipping-cost=9.95
     *               &order-adjustment.shipping.flat-rate-shipping-adjustment.shipping-cost.currency=USD
     *               &order-total=190.98
     *               &order-total.currency=USD
     *               &buyer-marketing-preferences.email-allowed=false
     *               &timestamp=2007-03-19T15%3A06%3A26.051Z
     * @param order  Order
     */
    private void setNewOrderInfo(Map<String, String> params, Order order) {
        String contactName = params.get("buyer-shipping-address.contact-name");
        String email = params.get("buyer-shipping-address.email");
        String addressLine1 = params.get("buyer-shipping-address.address1");
        String town = params.get("buyer-shipping-address.city");
        String county = params.get("buyer-shipping-address.region");
        String postCode = params.get("buyer-shipping-address.postal-code");
        Country country = Country.getInstance(params.get("buyer-shipping-address.country-code"));
        String phone = params.get("buyer-shipping-address.phone");


        Item account = new Item(context, ItemType.getAccount(context), contactName, "LIVE", null);
        account.setEmail(email);
        account.save();
        Address address = null;
        try {
            address = new Address(context, account, account.getName(), addressLine1, null, null, town, county, postCode, country);
            address.setTelephone1(phone);
            address.save();
        } catch (PostCodeException e) {
            logger.warning("[" + GoogleCheckoutForm.class + "] " + e);
        } catch (ContactNameException e) {
            logger.warning("[" + GoogleCheckoutForm.class + "] " + e);
        } catch (TownException e) {
            logger.warning("[" + GoogleCheckoutForm.class + "] " + e);
        } catch (CountryException e) {
            logger.warning("[" + GoogleCheckoutForm.class + "] " + e);
        } catch (AddressLineException e) {
            logger.warning("[" + GoogleCheckoutForm.class + "] " + e);
        }

        order.setBillingAddress(address);
        order.setDeliveryAddress(address);
        order.setAccount(account);
        order.setStatus(params.get("fulfillment-order-state"));
        order.setFinansialStatus(params.get("financial-order-state"));
    }

    public void getHeader(RequestContext context) {
        PaymentSettings paymentSettings = PaymentSettings.getInstance(context);
        String merchantId = paymentSettings.getGoogleCheckoutId();
        String password = paymentSettings.getGoogleCheckoutPw();
        String authorization = new BASE64Encoder().encode((merchantId + ":" + password).getBytes());
        context.addHeader("Authorization", "Basic " + authorization);
        context.addHeader("Content-Type", "application/xml;charset=UTF-8");
        context.addHeader("Accept", "application/xml;charset=UTF-8");
    }
}
