package org.sevensoft.ecreator.model.ecom.orders.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderMarker;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 29 Oct 2006 08:53:09
 *
 */
public class CustomerReferenceMarker extends MarkerHelper implements IOrderMarker {

	public Object generate(RequestContext context, Map<String, String> params, Order order) {

		if (!order.hasCustomerReference()) {
			return null;
		}

		return super.string(context, params, order.getCustomerReference());
	}

	public Object getRegex() {
		return "order_customer_ref";
	}

}
