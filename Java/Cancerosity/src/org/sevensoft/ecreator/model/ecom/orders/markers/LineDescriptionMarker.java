package org.sevensoft.ecreator.model.ecom.orders.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderLineMarker;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 22 Feb 2007 09:15:13
 *
 */
public class LineDescriptionMarker extends MarkerHelper implements IOrderLineMarker {

	public Object generate(RequestContext context, Map<String, String> params, OrderLine line) {
		return super.string(context, params, line.getDescription());
	}

	@Override
	public String getDescription() {
		return "Order line description";
	}

	public Object getRegex() {
		return "order_line_desc";
	}

}
