package org.sevensoft.ecreator.model.ecom.payments.cards.exceptions;


/**
 * @author sks 26-Oct-2005 10:59:24
 *
 */
public class IssueNumberException extends Exception {

	public IssueNumberException() {
	}

	public IssueNumberException(String message, Throwable cause) {
		super(message, cause);
	}

	public IssueNumberException(String message) {
		super(message);
	}

	public IssueNumberException(Throwable cause) {
		super(cause);
	}


}
