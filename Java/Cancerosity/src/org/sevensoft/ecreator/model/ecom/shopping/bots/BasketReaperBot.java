package org.sevensoft.ecreator.model.ecom.shopping.bots;

import java.util.logging.Logger;

import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 04-Feb-2006 08:21:02
 * 
 * Deletes all baskets that have not been used in the past week.
 *
 */
public class BasketReaperBot implements Runnable {

	private RequestContext	context;
	private static Logger		logger	= Logger.getLogger("cron");

	public BasketReaperBot(RequestContext context) {
		this.context = context;
	}

	public void run() {

		logger.config("[BasketReaperBot] running");
		final long deleteStamp = System.currentTimeMillis() - (1000l * 60 * 60 * 24 * 7);

		try {

			// delete any baskets older than 1 week
			Query q = new Query(context, "delete from # where lastAccessTime<?");
			q.setTable(Basket.class);
			q.setParameter(deleteStamp);
			q.run();

		} catch (RuntimeException e) {
			e.printStackTrace();
		}

	}

}
