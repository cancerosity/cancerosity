package org.sevensoft.ecreator.model.ecom.addresses.exceptions;


/**
 * @author sks
 * 02-Jul-2004 21:48:27
 */
public class PostCodeException extends Exception {

	/**
	 * 
	 */
	public PostCodeException() {
		super();
	}

	/**
	 * @param message
	 */
	public PostCodeException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public PostCodeException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public PostCodeException(String message, Throwable cause) {
		super(message, cause);
	}

}
