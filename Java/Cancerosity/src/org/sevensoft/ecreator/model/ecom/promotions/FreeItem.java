package org.sevensoft.ecreator.model.ecom.promotions;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 3 Oct 2006 14:39:05
 *
 */
@Table("promotions_freeitems")
public class FreeItem extends EntityObject {

	private Promotion	promotion;
	private Item	item;

	public FreeItem(RequestContext context) {
		super(context);
	}

	public FreeItem(RequestContext context, Promotion promotion, Item item) {
		super(context);
		this.promotion = promotion;
		this.item = item;
		
		save();
	}

}
