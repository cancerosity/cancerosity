package org.sevensoft.ecreator.model.ecom.payments.processors.secpay;

import java.util.Map;

import org.sevensoft.ecreator.model.ecom.payments.processors.Processor;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorResult;
import org.sevensoft.ecreator.model.ecom.payments.results.AvsCheck.AvsResult;

/**
 * @author sks 26 Feb 2007 18:40:13
 *
 */
public class SecPayResult extends ProcessorResult {

	private String	valid;
	private String	trans_id;
	private String	auth_code;
	private String	amount;
	private String	test_status;
	private String	message;
	private Processor	processor;

	public SecPayResult(Map<String, String> p) {

		valid = p.get("valid");
		trans_id = p.get("trans_id");
		auth_code = p.get("auth_code");
		amount = p.get("amount");
		test_status = p.get("test_status");
		message = p.get("message");

	}

	@Override
	public String getAuthCode() {
		return auth_code;
	}

	@Override
	public AvsResult getAvsResult() {
		return null;
	}

	@Override
	public String getInternalTransactionId() {
		return trans_id;
	}

	@Override
	public String getMessage() {
		return message;
	}

	public final Processor getProcessor() {
		return processor;
	}

	@Override
	public String getProcessorTransactionId() {
		return null;
	}

	@Override
	public String getSecurityKeyCode() {
		return null;
	}

	@Override
	public boolean isAuthorised() {
		return valid.equals("true");
	}

}
