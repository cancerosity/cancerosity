package org.sevensoft.ecreator.model.ecom.payments.processors;

import org.sevensoft.ecreator.model.ecom.payments.results.AvsCheck.AvsResult;

/**
 * @author sks 19-Jan-2006 21:08:18
 *
 */
public abstract class ProcessorResult {

	public abstract String getInternalTransactionId();

	public abstract String getProcessorTransactionId();

	public abstract AvsResult getAvsResult();

	public abstract String getSecurityKeyCode();

	public abstract String getAuthCode();

	public abstract String getMessage();

	/**
	 * Return true if authoristation was granted for this transaction - only really applicable to payment transactions.
	 * 
	 * @return
	 */
	public abstract boolean isAuthorised();

}
