package org.sevensoft.ecreator.model.ecom.orders.parcels.trackers;

/**
 * @author sks 21 Jun 2007 09:58:25
 *
 */
public class LynxTracker extends Tracker {

	public String getTrackingUrl(String consignment) {
		return "http://www.lynx.co.uk/TAT-CR/jsp/PodDetails.jsp?podSearchNumber=" + consignment + "&podselect=c";
	}
}
