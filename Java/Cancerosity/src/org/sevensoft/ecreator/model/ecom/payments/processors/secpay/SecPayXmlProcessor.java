package org.sevensoft.ecreator.model.ecom.payments.processors.secpay;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.ecom.payments.processors.SecPayXmlHandler;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentException;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardTypeException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CscException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiredCardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiryDateException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.IssueNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.StartDateException;
import org.sevensoft.ecreator.model.ecom.payments.processors.Processor;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorException;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorResult;
import org.sevensoft.ecreator.model.ecom.payments.results.AvsCheck;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 26 Feb 2007 11:34:40
 *
 */
@Table("payments_processors_secpayxml")
@HandlerClass(SecPayXmlHandler.class)
public class SecPayXmlProcessor extends Processor {

	public static final String	SECVPN_HOST_ADDRESS	= "https://www.secpay.com/secxmlrpc/make_call";

	private String			username, password;

	public SecPayXmlProcessor(RequestContext context) {
		super(context);
	}

	@Override
	public AvsCheck avsCheck(Card card, Address address) throws IOException, ProcessorException, ExpiredCardException, ExpiryDateException,
			CardNumberException, CardException, IssueNumberException, CscException, PaymentException {
		return null;
	}

	@Override
	public Payment credit(Payment payment, Money money) throws IOException, ExpiryDateException, CardNumberException, CardTypeException, StartDateException,
			ProcessorException, CardException, IssueNumberException {
		return null;
	}

	@Override
	protected ProcessorResult debit(Item account, Card card, Address address, Money money) throws CardException, ExpiryDateException, IssueNumberException,
			CardTypeException, CscException, CardNumberException, StartDateException, IOException, ExpiredCardException, ProcessorException,
			PaymentException {

		return null;
	}

	@Override
	protected ProcessorResult debit(Payable payable) throws CardException, ExpiryDateException, IssueNumberException, CardTypeException, CscException,
			CardNumberException, StartDateException, IOException, ExpiredCardException, ProcessorException, PaymentException {

		Card card = payable.getCard();
		Address address = payable.getPaymentAddress();

		String transId = Payment.generateUniqueTransactionId(context);

		Element root = new Element("methodCall");
		Document doc = new Document(root);

		Element methodNameEl = new Element("methodName").setText("SECVPN.validateCardFull");
		root.addContent(methodNameEl);

		Element paramsEl = new Element("params");
		methodNameEl.addContent(paramsEl);

		List<String> params = new ArrayList();

		// mid  	  	 This is your secpay username (usually six letters and two numbers).
		params.add(username);

		// vpn_pswd  	secpay  	 Your VPN password can be set from within the Merchant Extranet: https://www.secpay.com/lookup. (Click on "Change Remote Passwords" and select VPN from the drop down list).
		params.add(password);

		// trans_id  	TRAN0001  	 A unique transaction identifier created by yourself. This can be used to refer to a transaction at a later date (to refund it for example).
		params.add(transId);

		// ip  	127.0.0.1  	 The IP address that the cardholders machine is presenting to the internet.
		params.add(context.getRemoteIp());

		// name  	Mr Cardholder  	 The cardholders name as it is on their card.
		params.add(card.getCardHolder());

		// card_number  	4444333322221111  	 The credit card number (this should contain no spaces or hyphens). The example card number show to the left is a test Visa card which can be used during development. Any valid expiry date which is in the future can be used with this card number.
		params.add(card.getCardNumber());

		// amount  	50.00  	 The amount for the transaction. This should contain no currency symbols or formatting (for example do not send an amount with a comma in).
		params.add(payable.getPaymentAmount().toEditString());

		// expiry_date  	0105  	 The credit card expiry date. Should be formatted either as mm/yy or mmyy.
		params.add(card.getExpiryDate());

		// issue_number  	3  	 The credit card issue number. This only applies to switch or solo cards. If the card in use does not have an issue number then an empty string should be passed in.
		if (card.hasIssue()) {
			params.add(card.getIssueNumber());
		} else {
			params.add("");
		}

		// start_date  	0102  	 The credit card start date. If the card does not have a start date then an empty string should used.
		params.add(card.getStartDate());

		/* order  	prod=funny_book,item_amount=18.50x1;prod=sad_book,item_amount=16.50x2
		 * Used to submit order details relevant to this transaction. Please see the User Manual for further details: http://www.secpay.com/sc_api.html#order.
		 */
		params.add(payable.getPaymentDescription());

		/*
		 * shipping name=Fred+Bloggs,company=Online+Shop+Ltd,addr_1=Dotcom+House,addr_2=London+Road,city=Townville,state=Countyshire,post_code=AB1+C23,tel=01234+567+890,fax=09876+543+210,email=somebody%40secpay.com,url=http%3A%2F%2Fwww.somedomain.com
		 * Used to submit shipping address details relevant to this transaction. Please see the User Manual for further details: http://www.secpay.com/sc_api.html#shipping.
		 */
		params.add("");

		/*
		 * billing name=Fred+Bloggs,company=Online+Shop+Ltd,addr_1=Dotcom+House,addr_2=London+Road,city=Townville,state=Countyshire,post_code=AB1+C23,tel=01234+567+890,fax=09876+543+210,email=somebody%40secpay.com,url=http%3A%2F%2Fwww.somedomain.com
		 * Used to submit billing address details relevant to this transaction. Please see the User Manual for further details: http://www.secpay.com/sc_api.html#billing.	 
		 */
		params.add(address.getLabel(", ", false));

		/*
		 * options  	  test_status=true,dups=false,card_type=Visa
		 * Used to submit optional parameters which are used to alter the behaviour of this transaction. Please see the User Manual for further details: http://www.secpay.com/sc_api.html#other.
		 */
		params.add("test_status=true,dups=false");

		for (String param : params) {

			Element paramEl = new Element("param");
			paramsEl.addContent(paramEl);

			Element valueEl = new Element("value").setText(param);
			paramEl.addContent(valueEl);

		}

		XMLOutputter output = new XMLOutputter();

		HttpClient hc = new HttpClient(SECVPN_HOST_ADDRESS);
		hc.setBody(output.outputString(doc));

		hc.connect();

		try {

			doc = new SAXBuilder().build(new StringReader(hc.getResponseString()));

			root = doc.getRootElement();
			Element paramEl = root.getChild("param");
			String value = paramEl.getChildText("value");

			// split records
			Map<String, String> p = StringHelper.explodeMap(value, "&", "=");
			logger.fine("[SecPayXmlProcessor] returned params=" + params);

			return new SecPayResult(p);

		} catch (JDOMException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String getName() {
		return "Sec pay XML";
	}

	public final String getPassword() {
		return password;
	}

	public final String getUsername() {
		return username;
	}

	@Override
	public boolean isLive() {
		return true;
	}

	public final void setPassword(String password) {
		this.password = password;
	}

	public final void setUsername(String username) {
		this.username = username;
	}

	@Override
	public void verify(Card card) throws CardException, ExpiryDateException, IssueNumberException, CardTypeException, CardNumberException,
			StartDateException, IOException, ExpiredCardException, ProcessorException, CscException {
	}

	@Override
	public boolean voidPayment(Payment payment) throws IOException, ProcessorException {
		return false;
	}
}
