package org.sevensoft.ecreator.model.ecom.shopping.markers.basket;

import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketLineMarker;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.CrossGif;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.BasketHandler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import java.util.Map;

/**
 * User: Tanya
 * Date: 30.11.2011
 */
public class BasketItemRemove implements IBasketLineMarker {

    public Object generate(RequestContext context, Map<String, String> params, BasketLine line) {
        if (line.hasItem()) {

            return new LinkTag(BasketHandler.class, "remove", new CrossGif(), "line", line)
                    .setConfirmation("Are you sure you want to remove this item?");

        }
        return null;
    }

    public Object getRegex() {
        return "basket_item_remove";
    }
}
