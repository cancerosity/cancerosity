package org.sevensoft.ecreator.model.ecom.shopping.markers.basket;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.ecom.shopping.BasketHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 04-Apr-2006 10:26:01
 *
 */
public final class BasketMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.Shopping.enabled(context)) {
			return null;
		}

		if (!params.containsKey("text")) {
			params.put("text", "Basket");
		}

		return super.link(context, params, new Link(BasketHandler.class), "link_basket");

	}

	public Object getRegex() {
		return "basket";
	}
}