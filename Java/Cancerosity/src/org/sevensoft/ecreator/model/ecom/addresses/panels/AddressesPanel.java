package org.sevensoft.ecreator.model.ecom.addresses.panels;

import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.shopping.ShoppingSettings;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.RadioTag;
import org.sevensoft.jeezy.http.html.form.bool.SimpleRadioTag;

/**
 * @author sks 19 Jan 2007 09:09:51
 * 
 * Show the panel for selecting existing address or creating a new one
 *
 */
public class AddressesPanel {

	private static final Logger		logger		= Logger.getLogger("ecreator");

	public static final String		CreateRadioID	= "addressCreateRadio";

	private final RequestContext		context;
	private Address				selected;
	private final Item			account;
	private final List<Address>		addresses;
	private final ShoppingSettings	shoppingSettings;

	private boolean				showCreate;

	public AddressesPanel(RequestContext context, Item account, Address selected) {

		this.context = context;
		this.account = account;
		this.selected = selected;
		this.shoppingSettings = ShoppingSettings.getInstance(context);

		if (shoppingSettings.isAddressBook() && account != null) {

			addresses = account.getAddresses();

			logger.fine("[AddressBookPanel] addresses=" + addresses);

		} else {

			addresses = Collections.emptyList();
		}

	}

	public final void setShowCreate(boolean showCreate) {
		this.showCreate = showCreate;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		if (shoppingSettings.isAddressBook()) {

			if (addresses.size() > 0) {

				if (selected == null) {
					selected = addresses.get(0);
				}

				for (Address address : addresses) {

					boolean checked = address.equals(selected);
					SimpleRadioTag radioTag = new SimpleRadioTag(context, "addressId", address, checked);
					radioTag.setOnClick("document.getElementById('" + AddressEntryPanel.ID + "').style.display = 'none';");

					sb.append("<div class='text'>" + radioTag + " " + address.getName() + ", " + address.getAddress(", ") + "</div>");

				}
			}
		}

		// only show create if we have added in at least one option prior
		if (addresses.size() > 0 || showCreate) {

			if (account == null || !account.isBuyer() || account.getBuyerConfig().isEditAddresses()) {

				RadioTag radioTag = new RadioTag(context, "addressId", "create", false);
				radioTag.setId(CreateRadioID);
				radioTag.setOnClick("document.getElementById('" + AddressEntryPanel.ID + "').style.display = 'block';");
				sb.append("<div class='text'>" + radioTag + " Create new address</div>");
			}
		}

		return sb.toString();
	}
}
