package org.sevensoft.ecreator.model.ecom.orders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.support.AttributeOwnerSupport;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.iface.admin.ecom.orders.InvoiceHardcopyMarkupDefault;
import org.sevensoft.ecreator.iface.admin.ecom.orders.InvoiceLinesHardcopyMarkupDefault;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sam2 Nov 12, 2003 2:00:43 AM
 */
@Table("settings_order")
@Singleton
public class OrderSettings extends AttributeOwnerSupport {

	public static OrderSettings getInstance(RequestContext context) {
		return getSingleton(context, OrderSettings.class);
	}

    public enum GoogleFinansialOrderState {
        REVIEWING, CHARGEABLE, CHARGING, CHARGED, PAYMENT_DECLINED, CANCELLED, CANCELLED_BY_GOOGLE
    }

    public enum GoogleFulfillmentOrderState {
        NEW, PROCESSING, DELIVERED, WILL_NOT_DELIVER, CANCELLED
    }

    private Markup		invoiceMarkup;
	private Markup		invoiceLinesMarkup;

	private String		invoiceFooter, invoiceHeader;

	private boolean		salesPersons;

	private Attribute		smsOrderAttribute;

	/**
	 * Group orders by categories
	 */
	private List<String>	categories;

	/**
	 * 
	 */
	@Default("1")
	private boolean		csvExport;

	/**
	 * 
	 */
	private String		orderIdPrefix, orderIdSuffix;

	/**
	 * 
	 */
	@Default("1")
	private boolean		xmlExport;

	/*
	 * Order statuses
	 */
	private List<String>	statuses;

	/**
	 * The body of the completionEmail
	 */
	private String		completionEmailBody;

	/**
	 * Send an emailed invoice to the customer when the order is completed.
	 */
	private boolean		emailInvoiceOnCompletion;

	/**
	 * Log the member out after order has been placed
	 */
	private boolean		logoutAfterOrder;

	/**
	 * Enable parcel tracking
	 */
	private boolean		parcels;

	/*
	 *Enable messages
	 **/
	private boolean		messages;

	private String		initialStatus;

	/**
	 * The password 
	 */
	private String		addressWebServicePassword;

	private Money		floorLimit;

	private boolean		recurringOrders;

	/**
	 * Use items on orders - if turned off then we will only use info lines (manually entered information)
	 */
	@Default("1")
	private boolean		items;

	/**
	 * 
	 */
	private boolean		splitOrdersByBranch;

    private String track;

    @Default("1")
    private boolean printCustomerTelepnone;

    public OrderSettings(RequestContext context) {
		super(context);
	}

	public String getAddressWebServicePassword() {
		return addressWebServicePassword;
	}

	public final List<String> getCategories() {
		if (categories == null) {
			categories = new ArrayList();
		}
		return categories;
	}

	public List<Attribute> getCheckoutAttributes() {

		List<Attribute> attributes = getAttributes();
		CollectionsUtil.filter(attributes, new Predicate<Attribute>() {

			public boolean accept(Attribute e) {
				return e.isCheckout();
			}
		});

		return attributes;
	}

	public String getCompletionEmailBody() {
		return completionEmailBody == null ? getCompletionEmailBodyDefault() : completionEmailBody;
	}

	private String getCompletionEmailBodyDefault() {
		return "Your order is now complete.\nThank you for shopping at " + Company.getInstance(context).getName() + "\n\n[parcels]";
	}

	public Money getFloorLimit() {
		return floorLimit;
	}

	public String getInitialStatus() {
		return initialStatus == null ? getStatuses().get(0) : initialStatus;
	}

	public String getInvoiceFooter() {
		return invoiceFooter == null ? getInvoiceFooterDefault() : invoiceFooter;
	}

	private String getInvoiceFooterDefault() {
		return "<div style='font-weight: bold; font-size: 16px'>THANK YOU FOR YOUR ORDER</div>";
	}

	public String getInvoiceHeader() {
		return invoiceHeader == null ? getInvoiceHeaderDefault() : invoiceHeader;
	}

	private String getInvoiceHeaderDefault() {

		Company company = Company.getInstance(context);

		StringBuilder sb = new StringBuilder();
		sb.append("<div style='font-size: 32px'>" + company.getName() + "</div>");

		if (company.hasAddress()) {
			sb.append("<div style='font-size: 16px; font-weight: bold'>" + company.getAddressLabel("<br/>") + "</div>");
		}

		sb.append("<br/>");
        if (company.hasTelephone()) {
            sb.append("<b>Telephone:</b> " + company.getTelephone() + " &nbsp; ");
        }

		if (company.hasFax()) {
			sb.append("<b>Fax:</b> " + company.getFax() + " &nbsp; ");
		}

		sb.append("<br/>");

		if (company.isVatRegistered()) {
			sb.append("<b>VAT No:</b> [comp_vatreg] &nbsp; ");
		}

		if (company.hasCompanyNumber()) {
			sb.append("<b>Company Reg:</b> [comp_number] &nbsp; ");
		}

		return sb.toString();
	}

	public Markup getInvoiceLinesMarkup() {
		return (Markup) (invoiceLinesMarkup == null ? null : invoiceLinesMarkup.pop());
	}

	public Markup getInvoiceMarkup() {
		return (Markup) (invoiceMarkup == null ? null : invoiceMarkup.pop());
	}

	public final String getOrderIdPrefix() {
		return orderIdPrefix;
	}

	public final String getOrderIdSuffix() {
		return orderIdSuffix;
	}

	public boolean hasOrderIdSuffix() {
		return orderIdSuffix != null;
	}

	public boolean hasOrderIdPrefix() {
		return orderIdPrefix != null;
	}

	/**
	 * Returns a list of all sales persons that have been assigned to an order
	 */
	public List<User> getSalesPersons() {
		Query q = new Query(context, "select u.* from # u join # o on u.id=o.salesPerson group by u.id order by u.name");
		q.setTable(User.class);
		q.setTable(Order.class);
		return q.execute(User.class);
	}

	public Attribute getSmsOrderAttribute() {
		return smsOrderAttribute;
	}

	public final List<String> getStatuses() {

		if (statuses == null) {
			statuses = new ArrayList();
		}

		if (statuses.isEmpty()) {

			statuses.add("Awaiting stock");
			statuses.add("Awaiting payment");
			statuses.add("Awaiting dispatch");
			statuses.add("Completed");
			statuses.add("Cancelled");
		}

		return statuses;
	}

    public String getTrack() {
        return track;
    }

    public boolean hasAddressWebServicePassword() {
		return addressWebServicePassword != null;
	}

	/**
	 * 
	 */
	public boolean hasCategories() {
		return getCategories().size() > 0;
	}

	public boolean hasCheckoutAttributes() {
		return getCheckoutAttributes().size() > 0;
	}

	public boolean hasCompletionEmail() {
		return completionEmailBody != null;
	}

	public boolean hasInvoiceFooter() {
		return invoiceFooter != null;
	}

	public boolean hasInvoiceHeader() {
		return invoiceHeader != null;
	}

	public boolean hasInvoiceMarkup() {
		return invoiceMarkup != null;
	}

	public boolean hasSmsOrderCompletedAttribute() {
		return smsOrderAttribute != null;
	}

	public boolean isCsvExport() {
		return csvExport;
	}

	public boolean isEmailInvoiceOnCompletion() {
		return emailInvoiceOnCompletion;
	}

	public final boolean isItems() {
		return items;
	}

	public boolean isLogoutAfterOrder() {
		return logoutAfterOrder;
	}

	public boolean isMessages() {
		return messages;
	}

	public boolean isParcels() {
		return parcels;
	}

	public boolean isRecurringOrders() {
		return recurringOrders;
	}

	public boolean isSalesPersons() {
		return salesPersons;
	}

	public final boolean isSplitOrdersByBranch() {
		return splitOrdersByBranch;
	}

	public boolean isXmlExport() {
		return xmlExport;
	}

	public void setAddressWebServicePassword(String addressWebServicePassword) {
		this.addressWebServicePassword = addressWebServicePassword;
	}

	public final void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public void setCompletionEmailBody(String completionEmailBody) {
		this.completionEmailBody = completionEmailBody;
	}

	public void setCsvExport(boolean csvExport) {
		this.csvExport = csvExport;
	}

	public void setEmailInvoiceOnCompletion(boolean emailInvoiceOnCompletion) {
		this.emailInvoiceOnCompletion = emailInvoiceOnCompletion;
	}

	public void setFloorLimit(Money floorLimit) {
		this.floorLimit = floorLimit;
	}

	public final void setInitialStatus(String s) {
		if (getStatuses().contains(s)) {
			this.initialStatus = s;
		} else {
			this.initialStatus = null;
		}
	}

	public void setInvoiceFooter(String invoiceFooter) {
		this.invoiceFooter = invoiceFooter;
	}

	public void setInvoiceHeader(String invoiceHeader) {
		this.invoiceHeader = invoiceHeader;
	}

	public void setInvoiceLinesMarkup(Markup invoiceLineMarkup) {
		this.invoiceLinesMarkup = invoiceLineMarkup;
	}

    public void createInvoiceLinesMarkup() {
        if (invoiceLinesMarkup == null) {
            InvoiceLinesHardcopyMarkupDefault viewMarkupDefault = new InvoiceLinesHardcopyMarkupDefault();
            invoiceLinesMarkup = new Markup(context, viewMarkupDefault);
            invoiceLinesMarkup.save();
            save();
        }
    }

	public void setInvoiceMarkup(Markup invoiceMarkup) {
		this.invoiceMarkup = invoiceMarkup;
	}

    public void createInvoiceMarkup() {
        if (invoiceMarkup == null) {
            InvoiceHardcopyMarkupDefault viewMarkupDefault = new InvoiceHardcopyMarkupDefault();
            invoiceMarkup = new Markup(context, viewMarkupDefault);
            invoiceMarkup.save();
            save();
        }
    }

	public final void setItems(boolean deliveryAddresses) {
		this.items = deliveryAddresses;
	}

	public void setLogoutAfterOrder(boolean logoutAfterOrder) {
		this.logoutAfterOrder = logoutAfterOrder;
	}

	public void setMessages(boolean messages) {
		this.messages = messages;
	}

	public final void setOrderIdPrefix(String orderIdPrefix) {
		this.orderIdPrefix = orderIdPrefix;
	}

	public final void setOrderIdSuffix(String orderIdSuffix) {
		this.orderIdSuffix = orderIdSuffix;
	}

	public void setParcels(boolean parcels) {
		this.parcels = parcels;
	}

	public final void setRecurringOrders(boolean reorderDates) {
		this.recurringOrders = reorderDates;
	}

	public void setSalesPersons(boolean salesPersons) {
		this.salesPersons = salesPersons;
	}

	public void setSmsOrderAttribute(Attribute smsOrderCompletedAttribute) {
		this.smsOrderAttribute = smsOrderCompletedAttribute;
	}

	public final void setSplitOrdersByBranch(boolean splitOrdersByBranch) {
		this.splitOrdersByBranch = splitOrdersByBranch;
	}

	public void setStatuses(Collection<String> c) {
		this.statuses = new ArrayList();
		this.statuses.addAll(c);
	}

    public void setTrack(String track) {
        this.track = track;
    }

    public void setXmlExport(boolean xmlExport) {
		this.xmlExport = xmlExport;
	}

    public boolean isPrintCustomerTelepnone() {
        return printCustomerTelepnone;
    }

    public void setPrintCustomerTelepnone(boolean printCustomerTelepnone) {
        this.printCustomerTelepnone = printCustomerTelepnone;
    }

    @Override
	protected void singletonInit(RequestContext context) {

		this.items = true;

		//		this.acknowledgementFooter = "Under the new distance selling regulations you are entitled to cancel this contract with us \n"
		//				+ "within 7 working days after receipt of goods and receive a full refund. This law assumes that the \n"
		//				+ "goods have not been used and are in pristine condition.\n\n"
		//				+ "Upon receipt of your order you will be asked to sign for the goods received in good condition. If\n"
		//				+ "you are unable to check the contents of the package at that moment in time please sign for the \n"
		//				+ "parcel as \"UNCHECKED\". Failure to do so may affect any warranty claims that you make thereafter.\n";
	}
}
