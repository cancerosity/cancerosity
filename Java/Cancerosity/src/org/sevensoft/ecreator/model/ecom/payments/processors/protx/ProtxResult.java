package org.sevensoft.ecreator.model.ecom.payments.processors.protx;

import java.util.Map;

import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorResult;
import org.sevensoft.ecreator.model.ecom.payments.results.AvsCheck.AvsResult;

/**
 * Status codes:
 * 
 * MALFORMED: the data sent was wrong had errors
 * 
 * INVALID: problem with customer data, wrong card number or something,
 * 
 * ERROR: Protx is bust
 * 
 * OK: Transaction approved
 * 
 * NOTAUTHED: Transaction declined
 * 
 * UNDEF: Seems to be mentioned once as a synomyn for ERROR
 * 
 * @author sks 04-Nov-2004 20:37:38
 */
class ProtxResult extends ProcessorResult {

	private AvsResult	avsResult;
	private String	processorTransactionId, internalTransactionId;
	private String	authCode;
	private String	securityKeyCode;
	private boolean	authorised;
	private String	message;

	ProtxResult(Map<String, String> params, String internalTransactionId) {

		this.internalTransactionId = internalTransactionId;

		authCode = params.get("TxAuthNo");
		processorTransactionId = params.get("VPSTxID");
		if (processorTransactionId == null) {
			processorTransactionId = params.get("VPSTxId");
		}

		securityKeyCode = params.get("SecurityKey");
		message = params.get("StatusDetail");

		if ("OK".equalsIgnoreCase(params.get("Status")))
			authorised = true;

		if ("SECURITY CODE MATCH ONLY".equalsIgnoreCase(params.get("AVSCV2"))) {
			avsResult = AvsResult.Fail;
			//			cscResult = CscResult.Match;

		} else if ("NO DATA MATCHES".equalsIgnoreCase(params.get("AVSCV2"))) {
			avsResult = AvsResult.Fail;
			//			cscResult = CscResult.Fail;

		} else if ("DATA NOT CHECKED".equalsIgnoreCase(params.get("AVSCV2"))) {
			avsResult = null;
			//			cscResult = CscResult.NotChecked;

		} else if ("ALL MATCH".equalsIgnoreCase(params.get("AVSCV2"))) {
			avsResult = AvsResult.Match;
			//			cscResult = CscResult.Match;

		} else if ("ADDRESS MATCH ONLY".equalsIgnoreCase(params.get("AVSCV2"))) {
			avsResult = AvsResult.Match;
			//			cscResult = CscResult.Fail;
		}

	}

	@Override
	public String getAuthCode() {
		return authCode;
	}

	@Override
	public AvsResult getAvsResult() {
		return avsResult;
	}

	@Override
	public String getInternalTransactionId() {
		return internalTransactionId;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public String getProcessorTransactionId() {
		return processorTransactionId;
	}

	@Override
	public String getSecurityKeyCode() {
		return securityKeyCode;
	}

	@Override
	public boolean isAuthorised() {
		return authorised;
	}

}
