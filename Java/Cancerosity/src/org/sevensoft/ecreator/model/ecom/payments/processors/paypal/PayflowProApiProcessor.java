package org.sevensoft.ecreator.model.ecom.payments.processors.paypal;

import org.sevensoft.ecreator.model.ecom.payments.processors.Processor;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorException;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorResult;
import org.sevensoft.ecreator.model.ecom.payments.results.AvsCheck;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.*;
import org.sevensoft.ecreator.model.ecom.payments.PaymentException;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.iface.admin.ecom.payments.processors.PayflowProApiProcessorHandler;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.db.annotations.Table;

import java.io.IOException;

/**
 * User: Tanya
 * Date: 21.04.2012
 */
@Table("payments_processor_payflowpro_api")
@HandlerClass(PayflowProApiProcessorHandler.class)
public class PayflowProApiProcessor extends Processor {

    private String username, password, signature;

    public PayflowProApiProcessor(RequestContext context) {
        super(context);
    }

    public AvsCheck avsCheck(Card card, Address address) throws IOException, ProcessorException, ExpiredCardException, ExpiryDateException, CardNumberException, CardException, IssueNumberException, CscException, PaymentException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Payment credit(Payment payment, Money amount) throws IOException, ExpiryDateException, CardNumberException, CardTypeException, StartDateException, ProcessorException, CardException, IssueNumberException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    protected ProcessorResult debit(Item account, Card card, Address address, Money amount) throws CardException, ExpiryDateException, IssueNumberException, CardTypeException, CscException, CardNumberException, StartDateException, IOException, ExpiredCardException, ProcessorException, PaymentException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getName() {
        return "PayPal Payments Pro (API credentials)";
    }

    public boolean isLive() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void verify(Card card) throws CardException, ExpiryDateException, IssueNumberException, CardTypeException, CardNumberException, StartDateException, IOException, ExpiredCardException, ProcessorException, CscException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean voidPayment(Payment payment) throws IOException, ProcessorException {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getSignature() {
        return signature;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
