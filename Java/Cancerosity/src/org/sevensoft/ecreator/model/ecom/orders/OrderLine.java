package org.sevensoft.ecreator.model.ecom.orders;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.formatters.NumberFormatter;
import org.sevensoft.ecreator.model.attachments.AttachmentSupport;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.ecom.delivery.DeliverableItem;
import org.sevensoft.ecreator.model.ecom.delivery.DeliveryOption;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLineOption;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author sks 07-Sep-2005 12:07:36
 */
@Table("orders_lines")
public class OrderLine extends AttachmentSupport implements Positionable, DeliverableItem {

    private Money costPrice, salePrice;

    /**
     * Date this line was created
     */
    private DateTime date;

    private String description, comment;

    /**
     * Text descriptions the options selected for this line.
     */
    private String optionsDescription;

    private Order ord;

    /**
     * The product for this line
     */
    private Item item;

    /**
     * Was this line added as part of a promotion?
     */
    private boolean promotion;

    /**
     * The qty allocated
     */
    private int allocated;

    /**
     * The qty ordered
     */
    private int qty;

    /**
     * The vat rate for this line
     */
    private double vatRate;

    private int fileCount;

    private int position;

    private OrderLine(RequestContext context) {
        super(context);
    }

    /**
     * Creates a new order line for this item
     */
    OrderLine(RequestContext context, Order order, Item i, int qty) {
        super(context);
        this.ord = order;

        this.date = new DateTime();

        this.item = i;

        this.qty = qty;
        this.allocated = i.allocate(this, qty);

        this.description = i.getName();
        this.costPrice = i.getCostPrice();
        this.salePrice = i.getSellPrice(order.getAccount(), qty, null, false);
        this.vatRate = i.getVatRate();

        save();
    }

    /**
     * Creates a new general description line
     */
    OrderLine(RequestContext context, Order order, String description, int qty, Money amount) {
        super(context);
        this.ord = order;

        this.date = new DateTime();

        this.qty = qty;
        this.allocated = qty;

        this.description = description;
        this.vatRate = 17.5;

        this.costPrice = new Money(0);
        this.salePrice = amount;

        save();
    }

    @Override
    public synchronized boolean delete() {

        removeAttachments();

        if (hasItem()) {

            return super.delete();

        } else {

            return super.delete();
        }
    }

    public Item getAccount() {
        return getOrder().getAccount();
    }

    public int getAttachmentLimit() {
        return 0;
    }

    public String getCode() {

        if (item == null) {
            return "MISC";
        }

        return item.getIdString();
    }

    public String getComment() {
        return comment;
    }

    public Money getCostPrice() {
        return costPrice;
    }

    public Money getCostPriceEx() {
        return costPrice;
    }

    public DateTime getDate() {
        return date == null ? new DateTime(0) : date;
    }

    public int getDeliveryAttributeValue() {

        int total = 0;
        if (hasItem()) {

            for (Map.Entry<Attribute, String> entry : getItem().getAttributeValues().entrySet()) {

                if (entry.getKey().isDelivery()) {

                    try {

                        total = total + (qty * Integer.parseInt(entry.getValue().trim()));

                    } catch (NumberFormatException e) {
                        //	e.printStackTrace();
                    }

                }
            }
        }

        return total;
    }

    public Money getDeliveryRate(DeliveryOption band) {

        if (item == null) {
            return null;
        }

        return getItem().getDelivery().getDeliveryRate(band);

    }

    public Money getDeliverySurcharge() {

        if (item == null) {
            return null;
        } else {
            return getItem().getDelivery().getDeliverySurcharge();
        }
    }

    public String getDescription() {
        return description;
    }

    public Item getItem() {
        return (Item) (item == null ? null : item.pop());
    }

    public Money getLineCostEx() {
        return costPrice.multiply(qty);
    }

    public Money getLineProfit() {
        return getLineSellEx().subtract(getLineCostEx());
    }

    public Money getLineSellEx() {
        return salePrice.multiply(qty);
    }

    public Money getLineSellInc() {
        return getUnitSellInc().multiply(qty);
    }

    public Money getLineSellVat() {
        return VatHelper.vat(context, getLineSellEx(), vatRate);
    }

    public double getMargin() {
        return (salePrice.getAmount() - costPrice.getAmount()) / salePrice.getAmount() * 100;
    }

    public String getMarginFormatted() {
        return NumberFormatter.format(getMargin()) + "%";
    }

    public String getOptionsDetails() {
        return optionsDescription;
    }

    public Order getOrder() {
        return ord.pop();
    }

    public int getOrderId() {
        return ord.getId();
    }

    public int getPosition() {
        return position;
    }

    public Money getPriceForDelivery() {
        return getLineSellEx();
    }

    public int getProfit() {
        return salePrice.getAmount() - costPrice.getAmount();
    }

    public String getProfitFormatted() {
        return NumberFormatter.format(getProfit());
    }

    public int getQty() {
        return qty;
    }

    public int getQtyForDelivery() {
        return qty;
    }

    public String getReference() {
        return hasItem() ? getItem().getReference() : null;
    }

    public int getRequired() {
        return qty - allocated;
    }

    public Money getUnitSellEx() {
        return salePrice;
    }

    public Money getUnitSellInc() {
        return getUnitSellEx().add(getUnitSellVat());
    }

    public Money getUnitSellVat() {
        return salePrice.multiply(vatRate / 100);
    }

    public double getVatRate() {
        return vatRate;
    }

    public double getWeightForDelivery() {

        if (item == null) {

            return 0;

        } else {

            String value = getItem().getAttributeValue("weight");
            if (value == null) {
                return 0;
            }

            try {

                return Double.parseDouble(value) * qty;

            } catch (NumberFormatException e) {
                return 0;
            }
        }
    }

    public boolean hasItem() {
        return item != null;
    }

    public boolean hasOptionsDetails() {
        return optionsDescription != null;
    }

    public boolean isAllocated() {
        return allocated == qty;
    }

    public boolean isIncludedForDelivery() {
        return false;
    }

    public boolean isItemLine() {
        return item != null;
    }

    public boolean isMiscLine() {
        return item == null;
    }

    public boolean isPromotion() {
        return promotion;
    }

    void setComment(String comment) {
        this.comment = comment;
        save();
    }

    public void setCostPriceEx(Money costPriceEx) {
        costPrice = costPriceEx;
    }

    public boolean setDescription(String d) {

        if (ObjectUtil.equal(d, description)) {
            return false;
        }

        this.description = d;
        save();

        return true;
    }

    public void setOptions(List<BasketLineOption> options) {

        if (options == null || options.isEmpty()) {
            optionsDescription = null;
            return;
        }

        StringBuilder sb = new StringBuilder();
        Iterator<BasketLineOption> iter = options.iterator();
        while (iter.hasNext()) {

            BasketLineOption option = iter.next();

            if (option.hasAttachments()) {
                try {
                    option.moveAttachmentsTo(this);
                } catch (AttachmentExistsException e) {
                    e.printStackTrace();
                } catch (AttachmentTypeException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (AttachmentLimitException e) {
                    e.printStackTrace();
                }
            }

            sb.append(option.getName());
            sb.append(": ");
            sb.append(option.getValue());
            if (option.getProductCode() != null) {
                sb.append(" - ");
                sb.append(option.getProductCode());
            }
            if (iter.hasNext()) {
                sb.append(", ");
            }

//			salePrice = salePrice.add(option.getSalePrice());
//			costPrice = costPrice.add(option.getCostPrice());

        }

        optionsDescription = sb.toString();
    }

    public void setOrder(Order order) {
        ord = order;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    void setPromotion(boolean promotion) {
        this.promotion = promotion;
    }

    /**
     * Updates qty on this order item.
     * <p/>
     * Returns int for the change in qty. Ie, 0 for no change, positive for an increase, negative for a decrease
     *
     * @param newQty
     * @return
     */
    void setQty(int i) {

        if (i < 1) {
            return;
        }

        if (qty == i) {
            return;
        }

        this.qty = i;
        save();

    }

    boolean setSalePrice(Money money) {

        if (this.salePrice.equals(money)) {
            return false;
        }

        this.salePrice = money;
        save();
        return true;
    }

    boolean setVatRate(double vatRate) {

        if (this.vatRate == vatRate) {
            return false;
        }

        this.vatRate = vatRate;
        save();
        return true;
    }

}