package org.sevensoft.ecreator.model.ecom.payments.processors.misc;

import java.io.IOException;
import java.io.StringReader;
import java.text.NumberFormat;
import java.util.logging.Logger;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.httpc.HttpMethod;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentException;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CscException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiredCardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiryDateException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.IssueNumberException;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorException;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author Sam 06-Apr-2003 21:19:36
 */
public class ClearComRequest {

	private static final Money	MIN_AMOUNT	= new Money(100);

	// public static String MERCHANT_NUMBER = "4129128";

	private static final Logger	logger	= Logger.getLogger("payments");

	private final NumberFormat	numberformat;
	private final String		internalTransactionId;
	private final RequestContext	context;
	private final ClearComTxType	type;
	private Address			address;
	private Card			card;
	private Payment			payment;
	private final Item		account;
	private Money			amount;
	private String			url;
	private String			clientId;
	private String			username;
	private String			password;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public ClearComRequest(RequestContext context, String url, Item account, ClearComTxType type) {

		this.context = context;
		this.url = url;
		this.account = account;
		this.type = type;
		this.internalTransactionId = Payment.generateUniqueTransactionId(context);

		logger.fine("[ClearComRequest] transactionId=" + internalTransactionId);
		logger.fine("[ClearComRequest] account=" + account);

		// default amount to min amount
		this.amount = MIN_AMOUNT;

		numberformat = NumberFormat.getInstance();
		numberformat.setMaximumFractionDigits(0);
		numberformat.setMinimumIntegerDigits(4);
		numberformat.setGroupingUsed(false);
	}

	public ClearComResponse process() throws IOException, ProcessorException, ExpiredCardException, ExpiryDateException, CardNumberException, CardException,
			IssueNumberException, CscException, PaymentException {

		Element root = new Element("EngineDocList");
		root.addContent((new Element("DocVersion")).setText("1.0"));

		Document doc = new Document(root);
		Element engineDoc = new Element("EngineDoc");
		engineDoc.addContent((new Element("ContentType")).setText("OrderFormDoc"));
		root.addContent(engineDoc);

		Element user = new Element("User");

		user.addContent((new Element("Name")).setText(username));
		user.addContent((new Element("Password")).setText(password));
		engineDoc.addContent(user);

		logger.fine("ClientId=" + clientId);
		Element client = new Element("ClientId").setText(clientId);
		client.setAttribute("DataType", "S32");
		user.addContent(client);

		Element instructions = new Element("Instructions");
		instructions.addContent((new Element("Pipeline")).setText("PaymentNoFraud"));
		engineDoc.addContent(instructions);

		Element orderFormDoc = new Element("OrderFormDoc");

		/*
		 * Mode allows production / test environments.
		 * 
		 * P for Production (Live)
		 * 
		 * Y for all results to be returned as test mode success
		 * 
		 * N for all results to be returned as test mode failure
		 * 
		 */
		String mode = "Y";
		//if (PaymentSettings.getInstance().isLiveMode())
		mode = "P";
		logger.fine("setting Mode=" + mode);
		orderFormDoc.addContent((new Element("Mode")).setText(mode));

		/* 
		 * UserId is the account number of the user who is making the payment
		 * I will set this to the member id
		 */
		logger.fine("setting UserId=" + account.getId());
		orderFormDoc.addContent(new Element("UserId").setText(account.getIdString()));
		engineDoc.addContent(orderFormDoc);

		// Start transaction containe
		Element transaction = new Element("Transaction");
		orderFormDoc.addContent(transaction);

		// Set transaction type
		Element transactionType = new Element("Type");
		transactionType.setText(type.toString());
		transaction.addContent(transactionType);

		if (payment != null) {

			// set previous transactionId for void / credit
			Element transId = new Element("Id");

			logger.fine("setting previous transaction id= " + payment.getProcessorTransactionId());
			transId.setText(payment.getProcessorTransactionId());
			transaction.addContent(transId);

		}

		// do totals field if total > 0
		if (amount.isPositive()) {

			Element currentTotals = new Element("CurrentTotals");
			transaction.addContent(currentTotals);

			Element totals = new Element("Totals");
			currentTotals.addContent(totals);

			Element total_element = new Element("Total");
			total_element.setAttribute("Currency", "826");
			total_element.setAttribute("DataType", "Money");
			total_element.setText(numberformat.format(amount.getAmount()));
			totals.addContent(total_element);
		}

		// Consumer fields contain all details of the customers details,
		// card, address, etc
		Element consumer = new Element("Consumer");
		orderFormDoc.addContent(consumer);

		if (card != null) {

			// Do payment mech fields which contain the card to charge
			Element paymentMech = new Element("PaymentMech");
			consumer.addContent(paymentMech);

			// Do Card fields
			Element creditCard = new Element("CreditCard");
			paymentMech.addContent(creditCard);

			Element number = new Element("Number");
			number.setText(card.getCardNumber());
			creditCard.addContent(number);

			Element cvv2Val = new Element("Cvv2Val");
			cvv2Val.setText(card.getCscNumber());
			creditCard.addContent(cvv2Val);

			Element cvv2Indicator = new Element("Cvv2Indicator").setText("1");
			creditCard.addContent(cvv2Indicator);

			Element expiry = new Element("Expires");
			expiry.setAttribute("DataType", "ExpirationDate");
			expiry.setText(card.getExpiryDate());
			creditCard.addContent(expiry);

			if (card.hasIssue()) {

				Element issue = new Element("IssueNum");
				creditCard.addContent(issue);
				issue.setText(card.getIssueNumber());

			}

		}

		// Do address fields if address is not null
		if (address != null) {

			logger.fine("[ClearComRequest] setting address=" + address);

			Element billTo = new Element("BillTo");
			consumer.addContent(billTo);

			Element location = new Element("Location");
			billTo.addContent(location);

			Element address_el = new Element("Address");
			location.addContent(address_el);

			String addressLine1 = address.getAddressLine1();
			if (addressLine1.length() > 60)
				addressLine1 = addressLine1.substring(00, 60);

			Element street1 = new Element("Street1").setText(addressLine1);
			address_el.addContent(street1);

			Element country = (new Element("Country")).setText("826");
			address_el.addContent(country);

			Element postcode = new Element("PostalCode").setText(address.getPostcode());
			address_el.addContent(postcode);
		}

		try {

			HttpClient hc = new HttpClient(url, HttpMethod.Post);

			XMLOutputter outputter = new XMLOutputter();
			// outputter.setNewlines(true);
			// outputter.setLineSeparator("\n");

			logger.fine("[ClearComRequest] output=" + outputter.outputString(doc));

			// set up XML document as CLRCMRC_XML parameter
			String CLRCMRC_XML = outputter.outputString(doc);
			hc.setParameter("CLRCMRC_XML", CLRCMRC_XML.replaceAll("[\\\n\\\r]", " "));

			// connect to epdq and get input
			hc.connect();
			String input = hc.getResponseString();

			logger.fine("[ClearComRequest] input=" + input);

			// build XML doc from result
			SAXBuilder builder = new SAXBuilder();
			Document jdoc = builder.build(new StringReader(input.toString()));

			// return result object
			return new ClearComResponse(jdoc, internalTransactionId, null);

		} catch (JDOMException e) {
			throw new ProcessorException(e);
		}

	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setAmount(Money amount) {
		this.amount = amount;
	}

	/**
	 * Set the card details needed for a payment or pre-auth.
	 */
	public void setCard(Card card) {
		this.card = card;
	}

	/**
	 *  Previous payment details needed for refund / void
	 */
	public void setPayment(Payment payment) {
		this.payment = payment;
	}
}
