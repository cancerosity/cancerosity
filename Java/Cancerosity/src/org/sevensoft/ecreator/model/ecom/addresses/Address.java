package org.sevensoft.ecreator.model.ecom.addresses;

import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.gaia.UsState;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.commons.validators.text.PostcodeValidator;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.*;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.delivery.DeliveryOption;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;
import org.sevensoft.jeezy.i18n.Messages;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author sks 30-Mar-2003 10:50:52
 */
@Table("addresses")
public class Address extends EntityObject implements Selectable {

    public static void addTests(RequestContext context, Country country, AddressFields fields) {

        context.test(new RequiredValidator(), "name");
        context.test(new RequiredValidator(), "address1");
        context.test(new RequiredValidator(), "town");
        context.test(new RequiredValidator(), "country");

        if (Country.UK.equals(country)) {
            context.test(new RequiredValidator(), "postcode");
            context.test(new PostcodeValidator(), "postcode");
        }

        if (fields != null) {

            if (!fields.isTelephoneOptional1()) {
                context.test(new RequiredValidator(), "telephone1");
                context.test(new LengthValidator(11), "telephone1");
            }

            if (!fields.isTelephoneOptional2()) {
                context.test(new RequiredValidator(), "telephone2");
                context.test(new LengthValidator(11), "telephone2");
            }

            if (!fields.isTelephoneOptional3()) {
                context.test(new RequiredValidator(), "telephone3");
                context.test(new LengthValidator(11), "telephone3");
            }
        }
    }

    public static void addTests(RequestContext context, String postcode, DeliveryOption deliveryOption, AddressFields fields) {

        if (deliveryOption == null || deliveryOption.getPostcodes() == null || postcode == null) {
            return;
        }
        boolean noError = false;
        if (postcode != null && deliveryOption.getPostcodes() != null) {
            for (String code : StringHelper.explodeStrings(deliveryOption.getPostcodes(), ",", true)) {
                if (postcode.startsWith(code)) {
                    noError = true;
                    break;
                }
            }
        } else {
            noError = true;
        }

        if (!noError) {
            context.setError("postcode", Messages.get("commons.validators.text.postcode.match"));
        }
    }

    public static boolean isCreate(Item account) {

        if (account == null) {
            return true;
        }

        if (account.isBuyer() && !account.getBuyerConfig().isEditAddresses()) {
            return false;
        }

        return true;
    }

    public static String padPostcode(String postcode) {

        postcode = postcode.replaceAll("\\s", "");

        int a = postcode.length() - 3;
        int l = postcode.length();

        return postcode.substring(0, a) + " " + postcode.substring(a, l);
    }

    /*
      *Contact telephone
      */
    private String telephone, telephone2, telephone3;

    private boolean active;

    /**
     *
     */
    @Index()
    private String addressLine1;

    private String addressLine2, addressLine3, companyName, county;

    /**
     * The name of this addressee, can be company name sometimes
     */
    @Index()
    private String contactName;

    private String town;

    /**
     * Postcode without spaces.
     */
    @Index()
    private String postcode;
    private Country country;

    @Index()
    private Item account;

    /**
     * Date this address was created.
     */
    private DateTime date;

    /**
     * Special instructions for this address when used as a delivery address.
     * For example how to find the property.
     * <p/>
     * These instructions relate to the address and not to the order or whatever this address is attached to.
     */
    private String instructions;

    private String state;

    public Address(RequestContext context) {
        super(context);
    }

    public Address(RequestContext context, Item account, String contactName, String addressLine1, String addressLine2, String addressLine3, String postTown,
                   String county, String postcode, Country country) throws PostCodeException, ContactNameException, TownException, CountryException,
            AddressLineException {
        super(context);

        this.account = account;
        this.date = new DateTime();
        this.active = true;

        // ------------ country ----------
        if (country == null) {
            throw new CountryException("No country selected.");
        }

        this.country = country;

        // ---------- contact name ------------ //
        if (contactName == null) {
            throw new ContactNameException("Contact name is too short.");
        }

        contactName = contactName.trim().toUpperCase();

        if (contactName.length() < 5) {
            throw new ContactNameException("Contact name is too short.");
        }

        if (!contactName.matches(".*[A-Z]*.")) {
            throw new ContactNameException();
        }

        this.contactName = contactName;

        // ----------- company Name ------- //
        //		if (companyName != null)
        //			this.companyName = companyName.trim();

        // ------------ Address Line 1 ------- //
        if (addressLine1 == null) {
            throw new AddressLineException("Address is too short");
        }

        addressLine1 = addressLine1.trim();

        if (addressLine1.length() < 5)
            throw new AddressLineException("Address is too short");

        this.addressLine1 = addressLine1;

        // ----------- secondary address lines ------------- //

        if (addressLine2 != null) {
            this.addressLine2 = addressLine2.trim();
        }

        if (addressLine3 != null) {
            this.addressLine3 = addressLine3.trim();
        }

        // -------------- town ------------ //
        if (postTown == null) {
            throw new TownException("Town / City is too short.");
        }

        postTown = postTown.trim();

        if (postTown.length() < 3) {
            throw new TownException("Town / City is too short.");
        }

        this.town = postTown.toUpperCase();

        // ------------- county ---------- //
        if (county != null) {
            this.county = county.trim();
        }

        // --------- postcode ------------- //
        if (country.equals(Country.UK)) {

            if (postcode == null) {
                throw new PostCodeException("Postcode is too short.");
            }

            postcode = postcode.replaceAll("\\s", "").toUpperCase();

            if (postcode.length() < 5) {
                throw new PostCodeException("Postcode is too short.");
            }

            if (postcode.matches("[^A-Z0-9]")) {
                throw new PostCodeException("Postcode contains invalid characters.");
            }
        }

        this.postcode = postcode;

        save();
    }

    public void deactivate() {
        this.active = false;
        save();
    }

    @Override
    public boolean delete() {

        deactivate();

        new Query(context, "update # set billingAddress=0 where billingAddress=?").setTable(Basket.class).setParameter(this).run();
        new Query(context, "update # set deliveryAddress=0 where deliveryAddress=?").setTable(Basket.class).setParameter(this).run();

        return true;
    }

    public Item getAccount() {
        return (Item) (account == null ? null : account.pop());
    }

    /**
     * Returns the address portion of this address, this includes:
     * <p/>
     * address line 1, 2, 3 and town and county
     */
    public String getAddress(String separator) {

        List<String> list = new ArrayList<String>();
        list.add(addressLine1);

        if (hasAddressLine2())
            list.add(addressLine2);

        if (hasAddressLine3())
            list.add(addressLine3);

        list.add(town);

        if (hasCounty())
            list.add(county);

        return StringHelper.implode(list, separator, true);
    }

    public String getAddress(String string, int i) {
        String address = getAddress(string);
        return address == null || address.length() < i ? address : address.substring(0, i);
    }

    /**
     * @return Returns the line1.
     */
    public String getAddressLine1() {
        return addressLine1;
    }

    /**
     * @return Returns the line2.
     */
    public String getAddressLine2() {
        return addressLine2;
    }

    /**
     * @return Returns the line3.
     */
    public String getAddressLine3() {
        return addressLine3;
    }

    public String getCompanyName() {
        return companyName;
    }

    public Country getCountry() {
        return country == null ? Country.UK : country;
    }

    public Country getCountryOrig() {
        return country;
    }

    public String getCounty() {
        return county;
    }

    public DateTime getDate() {
        return date;
    }

    public String getFirstName() {

        if (contactName == null)
            return null;

        /*
           * Remove salutation
           */
        Pattern pattern = Pattern.compile("(mrs|miss|mr|ms|dr)\\s", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(contactName);
        String string = matcher.replaceFirst("");

        /*
           * Split name on spaces and return first entry.
           */
        String[] names = string.split("\\s");
        return names[0];
    }

    public final String getInstructions() {
        return instructions;
    }

    public String getState() {
        return state;
    }

    public String getLabel() {
        return getLabel(", ", false);
    }

    /**
     * Returns the full address
     */
    public String getLabel(String separator, boolean includeTelephoneNumbers) {

        List<String> list = new ArrayList<String>();
        list.add(contactName);

        if (hasCompanyName()) {
            list.add(companyName);
        }

        list.add(addressLine1);

        if (hasAddressLine2()) {
            list.add(addressLine2);
        }

        if (hasAddressLine3()) {
            list.add(addressLine3);
        }

        list.add(town);

        if (hasCounty()) {
            list.add(county);
        }

        if (hasPostcode()) {
            list.add(postcode);
        }

        list.add(getCountry().toString());

        if (getState() != null && Country.US.equals(getCountry())) {
            list.add(UsState.getStateName(getState()));
        }

        if (includeTelephoneNumbers) {

            if (telephone != null) {
                list.add(telephone);
            }

            if (telephone2 != null) {
                list.add(telephone2);
            }

            if (telephone3 != null) {
                list.add(telephone3);
            }
        }

        return StringHelper.implode(list, separator, true);
    }

    public String getLastName() {

        if (contactName == null) {
            return null;
        }

        String[] names = contactName.split("\\s");
        return names[names.length - 1];
    }

    public String getName() {
        return contactName;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getTelephone1() {
        return telephone;
    }

    public String getTelephone2() {
        return telephone2;
    }

    public String getTelephone3() {
        return telephone3;
    }

    public String getTown() {
        return town;
    }

    public String getValue() {
        return getIdString();
    }

    public boolean hasAddressLine2() {
        return addressLine2 != null;
    }

    public boolean hasAddressLine3() {
        return addressLine3 != null;
    }

    public boolean hasCompanyName() {
        return companyName != null;
    }

    public boolean hasCounty() {
        return county != null;
    }

    public boolean hasInstructions() {
        return instructions != null;
    }

    public boolean hasPostcode() {
        return postcode != null;
    }

    public boolean hasTelephone() {
        return telephone != null;
    }

    public boolean isActive() {
        return active;
    }

    public boolean isLondonAddress() {

        if (!country.equals(Country.UK)) {
            return false;
        }

        Pattern pattern = Pattern.compile("^(SW|SE|W|WC|E|EC|N|NW)\\d.*");

        return pattern.matcher(postcode).matches();
    }

    public boolean isUkAddress() {
        return Country.UK.equals(country);
    }

    public boolean isValidFor(Item acc) {

        if (account.equals(acc)) {
            return true;
        }

        if (account.equals(acc.getBuyerFor())) {
            return true;
        }

        return false;
    }

    /**
     * Address is vatable if it is UK based with a non channel islands postcode OR it is EU based.
     *
     * @return
     */
    public boolean isVatable() {

        if (Country.getEuVatZone().contains(getCountry())) {

            if (postcode == null) {
                return true;
            }

            if (postcode.startsWith("JE") || postcode.startsWith("GY")) {
                return false;
            }

            return true;
        }

        return false;
    }

    public void setAccount(Item item) {
        this.account = item;
    }

    public final void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setTelephone1(String telephone) {
        this.telephone = telephone;
    }

    public void setTelephone2(String telephone2) {
        this.telephone2 = telephone2;
    }

    public void setTelephone3(String telephone3) {
        this.telephone3 = telephone3;
    }
}