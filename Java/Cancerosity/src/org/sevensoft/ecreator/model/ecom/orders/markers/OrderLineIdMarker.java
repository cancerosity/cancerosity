package org.sevensoft.ecreator.model.ecom.orders.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderLineMarker;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * @author Dmitry Lebedev
 *         Date: 11.02.2010
 */
public class OrderLineIdMarker extends MarkerHelper implements IOrderLineMarker {

    public Object generate(RequestContext context, Map<String, String> params, OrderLine line) {
        if (line.isItemLine()) {
            return super.string(context, params, line.getItem().getIdString());
        } else {
            return "";
        }
    }

    public Object getRegex() {
        return "order_line_id";
    }
}
