package org.sevensoft.ecreator.model.ecom.payments.forms.able2buy.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.ecom.payments.info.Able2BuyCreditTermsHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 13 Dec 2006 12:09:26
 *
 */
public class Able2BuyCreditTermsMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		/// check able to buy is enabled
		if (!PaymentSettings.getInstance(context).getPaymentTypes().contains(PaymentType.Able2Buy)) {

			logger.fine("[Able2BuyMarker] able2buy not enabled");
			return null;
		}

		// check the item has a selling price
		if (!item.hasSalePrice(null)) {

			logger.fine("[Able2BuyMarker] no sale price");
			return null;
		}

		if (!params.containsKey("text")) {
			params.put("text", "Click here for example credit terms");
		}

		Link link = new Link(Able2BuyCreditTermsHandler.class, null, "item", item);
		logger.fine("[Able2BuyMarker] able popup link=" + link);
		return super.link(context, params, link, "able2buy");
	}

	public Object getRegex() {
		return new String[] { "able2buy_terms", "pricing_able2buy" };
	}

}
