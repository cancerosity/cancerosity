package org.sevensoft.ecreator.model.ecom.payments.forms.twocheckout;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Mar 2007 15:27:41
 *
 */
public class TwoCheckoutForm extends Form {

	public TwoCheckoutForm(RequestContext context) {
		super(context);
	}

	@Override
	public String getMethod() {
		return "GET";
	}

	/**
	 * Parameters
	 * 
	 * sid - your 2checkout vendor account number
	 * 
	 * total - the total amount to be billed, in decimal form,with no currency symbol.
	 * 
	 * cart_order_id - a unique order id from your program.
	 * 
	 * c_prod or c_prod_[:digit] - assigned_product_id of product in the 2checkout system. 
	 * This id may also contain an optional ','( ASCII comma ) followed by an integer which will represent the quantity.
	 * 
	 * id_type - Indicates type of id in c_prod. It must be either be a 1 for a vendor assigned vendor_product_id or a 2 for asystem assigned assigned_product_id.
	 * 
	 * 

	 * 
	 * Additional Parameters:

	 demo: set this equal to Y to enable demo mode only. Do not pass this in at all to disable it.

	 fixed: Set this equal to Y to remove the Continue Shopping button and lock the quantity fields.

	 lang: Set the equal to sp for Spanish purchase routine pages. English is the default if this parameter is absent, but you may pass in en as well.

	 return_url: Used to control where the Continue Shopping button will send the customer when it is clicked.

	 merchant_order_id: Specify your order number with this parameter. It will also be included in the confirmation emails to yourself and the customer.

	 pay_method: Passes a value of CC for Credit Card or CK for check (Online checks must be enabled within your account first!). This will select the payment method during the checkout process.
	 To pre-populate the billing information page you may pass in:

	 card_holder_name: Card holder's name

	 street_address: Card holder's

	 address_city: Card holder's city

	 state: Card holder's state

	 zip: Card holder's zip

	 country: Card holder's country

	 email: Card holder's email

	 phone: Card holder's phone
	 To pre-populate the shipping information page you may pass in:

	 ship_street_address

	 ship_city

	 ship_state

	 ship_zip

	 ship_country
	 * 
	 * 
	 */
	@Override
	public Map<String, String> getParams(Payable payable) throws IOException, FormException {

		PaymentSettings paymentSettings = PaymentSettings.getInstance(context);

		Map<String, String> params = new HashMap();

		FormSession session = new FormSession(context, payable);

		params.put("sid", paymentSettings.getTwoCheckoutSid());
		params.put("fixed", "Y");
		params.put("total", payable.getPaymentAmount().toEditString());
		params.put("merchant_order_id", session.getIdString());
		params.put("return_url", payable.getPaymentSuccessUrl(PaymentType.TwoCheckout));
		params.put("c_prod", payable.getPaymentDescription());
		params.put("id_type", "1");

		params.put("pay_method", "CC");
		params.put("email", payable.getPayableAccount().getEmail());

		return params;
	}

	@Override
	public String getPaymentUrl() {
		return "https://www.2checkout.com/2co/buyer/purchase";
	}

	@Override
	public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {

		System.out.println("Inline callback!=" + params);
	}

	/**
	 *The following parameters will be passed back to your routine :
	 *
	 * order_number - 2Checkout.com order number
	 * card_holder_name - Card holder's name
	 * street_address - Card holder's address
	 * city - Card holder's city
	 * state - Card holder's state
	 * zip - Card holder's zip
	 * country - Card holder's country
	 * email - Card holder's email
	 * phone - Card holder's phone
	 * cart_order_id - Your cart ID number passed in.
	 * cart_id - Your cart ID number passed in.
	 * credit_card_processed - Y if successful, K if waiting for approval
	 * total - Total purchase amount.
	 * ship_name - Shipping information
	 * ship_street_address - Shipping information
	 * ship_city - Shipping information
	 * ship_state - Shipping information
	 * ship_zip - Shipping information
	 * ship_country - Shipping information
	 * 
	 */
	@Override
	public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
		return null;
	}

}
