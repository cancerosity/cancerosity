package org.sevensoft.ecreator.model.ecom.payments.results;

import java.util.List;
import java.util.Map;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 5 Feb 2007 07:32:28
 * 
 * A logging class to track callbacks
 *
 */
@Table("payments_callbacks")
public class Callback extends EntityObject {

	public static List<Callback> get(RequestContext context, int startIndex, int resultsPerPage) {
		Query q = new Query(context, "select * from # order by id desc limit ?, ?");
		q.setTable(Callback.class);
        q.setParameter(startIndex);
        q.setParameter(resultsPerPage);
		return q.execute(Callback.class);
	}

    public static int size(RequestContext context) {
		Query q = new Query(context, "select count(*) from # ");
		q.setTable(Callback.class);
		return q.getInt();
	}

	private PaymentType	paymentType;
	private String		details;
	private DateTime		time;

	private Callback(RequestContext context) {
		super(context);
	}

	public Callback(RequestContext context, PaymentType paymentType, Map<String, String> params) {
		super(context);

		this.paymentType = paymentType;
		this.time = new DateTime();

		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, String> entry : params.entrySet()) {
			sb.append(entry.getKey() + "=" + entry.getValue() + "\n");
		}
		this.details = sb.toString();

		save();
	}

    public Callback(RequestContext context, PaymentType paymentType, String details) {
		super(context);

		this.paymentType = paymentType;
		this.time = new DateTime();
		this.details = details;

		save();
	}

	public final String getDetails() {
		return details;
	}

	public final PaymentType getPaymentType() {
		return paymentType;
	}

	public final DateTime getTime() {
		return time;
	}

}
