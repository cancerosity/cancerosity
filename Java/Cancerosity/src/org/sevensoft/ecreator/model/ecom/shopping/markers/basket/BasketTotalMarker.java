package org.sevensoft.ecreator.model.ecom.shopping.markers.basket;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.skint.Money;

/**
 * @author sks 04-Apr-2006 10:26:42
 *
 */
public class BasketTotalMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		Basket basket = (Basket) context.getAttribute("basket");
		if (basket == null) {
			return null;
		}

		Currency currency = (Currency) context.getAttribute("currency");

        Money total;
        if (params.containsKey("exdel")) {
            total = basket.getSubtotalInc();
        } else {
            total = basket.getTotalInc();
        }
        return super.string(context, params, currency.getSymbol() + Currency.convert(total, currency));
	}

	public Object getRegex() {
		return "basket_total";
	}
}