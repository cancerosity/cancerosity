package org.sevensoft.ecreator.model.ecom.orders.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderMarker;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 29 Oct 2006 08:53:09
 *
 */
public class OrderAccountIdMarker extends MarkerHelper implements IOrderMarker {

	public Object generate(RequestContext context, Map<String, String> params, Order order) {

		return super.string(context, params, order.getAccount().getIdString());
	}

	@Override
	public String getDescription() {
		return "Id for the account used on this order";
	}

	public Object getRegex() {
		return "order_account_id";
	}

}
