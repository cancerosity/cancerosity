package org.sevensoft.ecreator.model.ecom.payments.forms.metacharge;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.ecom.payments.results.Callback;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 31 Oct 2006 11:19:07
 *
 */
public class MetaChargeForm extends Form {

	public MetaChargeForm(RequestContext context) {
		super(context);
	}

	@Override
	public String getMethod() {
		return "POST";
	}

	@Override
	public Map<String, String> getParams(Payable payable) throws IOException, FormException {

		PaymentSettings ps = PaymentSettings.getInstance(context);
		FormSession session = new FormSession(context, payable);

		Map<String, String> params = new HashMap();
		params.put("intInstID", ps.getMetaChargeInstallId());
		params.put("fltAmount", payable.getPaymentAmount().toEditString());
		params.put("strCartID", session.getIdString());
		params.put("strDesc", payable.getPaymentDescription());
		params.put("strCurrency", "GBP");

		return params;
	}

	@Override
	public String getPaymentUrl() {
		return "https://secure.metacharge.com/mcpe/purser";
	}

	@Override
	public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
	}

	@Override
	public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
        log("[MetaCharge] Server callback params=" + params);

		new Callback(context, PaymentType.MetaCharge, params);

		FormSession session = EntityObject.getInstance(context, FormSession.class, params.get("strCartID"));

		// 0 = failure, 1 = success
		String intStatus = params.get("intStatus");
		if (Integer.parseInt(intStatus) == 0) {
			logger.config("[MetaChargeForm] intstatus=0, failure, so exiting");
			return null;
		}

		String intTransID = params.get("intTransID");
		String fltAmount = params.get("fltAmount");
		String strMessage = params.get("strMessage");
		String strPaymentType = params.get("strPaymentType");

		Payment payment = new Payment(context, session.getAccount(), new Money(fltAmount), PaymentType.MetaCharge);
		payment.setInternalTransactionId(intTransID);
		payment.setProcessorMessage(strMessage);
		payment.setDetails(strPaymentType);
		payment.save();

		session.callback(params, ipAddress, payment);
		session.delete();
		
		return null;
	}

}
