package org.sevensoft.ecreator.model.ecom.payments;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.realex.GlobalIrisRealAuth;
import org.sevensoft.ecreator.model.ecom.payments.forms.cardsave.CardsaveForm;
import org.sevensoft.ecreator.model.ecom.payments.forms.googlecheckout.GoogleCheckoutForm;
import org.sevensoft.ecreator.model.ecom.payments.forms.able2buy.Able2BuyForm;
import org.sevensoft.ecreator.model.ecom.payments.forms.epdq.EpdqCpi;
import org.sevensoft.ecreator.model.ecom.payments.forms.hsbc.HsbcCpi;
import org.sevensoft.ecreator.model.ecom.payments.forms.metacharge.MetaChargeForm;
import org.sevensoft.ecreator.model.ecom.payments.forms.netpayments.NetPaymentsExpress;
import org.sevensoft.ecreator.model.ecom.payments.forms.nochex.NoChexPaymentPage;
import org.sevensoft.ecreator.model.ecom.payments.forms.optimal.OptimalPaymentsDynamic;
import org.sevensoft.ecreator.model.ecom.payments.forms.paypal.PayPalStandard;
import org.sevensoft.ecreator.model.ecom.payments.forms.paypal.PayPalExpress;
import org.sevensoft.ecreator.model.ecom.payments.forms.paypal.PayPalProExpress;
import org.sevensoft.ecreator.model.ecom.payments.forms.paypal.PayPalProHosted;
import org.sevensoft.ecreator.model.ecom.payments.forms.paypal.PayPalDirect;
import org.sevensoft.ecreator.model.ecom.payments.forms.protx.ProtxForm;
import org.sevensoft.ecreator.model.ecom.payments.forms.secpay.SecCard;
import org.sevensoft.ecreator.model.ecom.payments.forms.securetrading.SecureTradingPages;
import org.sevensoft.ecreator.model.ecom.payments.forms.test.TestFormPayment;
import org.sevensoft.ecreator.model.ecom.payments.forms.twocheckout.TwoCheckoutForm;
import org.sevensoft.ecreator.model.ecom.payments.forms.worldpay.WorldpaySelectJunior;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import java.io.IOException;
import java.text.MessageFormat;

/**
 * @author sks 16-Nov-2004 20:45:46
 */
public enum PaymentType {

    GlobalIris("Realex Payments. Global Iris. RealAuth Redirect.") {
        public String getDefaultDescription(RequestContext context) {
            return "The redirect method is suitable for merchants that do not have their own secure server. Using this" +
                    "method, the customer will be redirected to Realex' secure server to enter their credit card details.";
        }

        @Override
        public Form getForm(RequestContext context) {
            return new GlobalIrisRealAuth(context);
        }

        @Override
        public String getPublicName() {
            return " Credit Card / Debit Card - Global Iris";
        }

        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isOnline() {
            return true;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return PaymentSettings.getInstance(context).hasGlobalIrisMerchantId();
        }

    },

    TwoCheckout("2Checkout") {

        @Override
        protected String getDefaultDescription(RequestContext context) {
            return "Pay by credit or debit card";
        }

        @Override
        public Form getForm(RequestContext context) {
            return new TwoCheckoutForm(context);
        }

        @Override
        public String getPublicName() {
            return "2Checkout.com";
        }

        @Override
        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return true;
        }

    },

    Account("Account") {

        public String getDefaultDescription(RequestContext context) {
            return "Pay on our account subject to approval by our accounts department.";
        }

        @Override
        public Form getForm(RequestContext context) {
            return null;
        }

        @Override
        public String getPublicName() {
            return getName();
        }

        public boolean isForm() {
            return false;
        }

        @Override
        public boolean isOnline() {
            return false;
        }

        public boolean isProcessor() {
            return false;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return true;
        }
    },

    /**
     * BC, should be CreditCard now
     */
    CardTerminal("Credit card") {

        public String getDefaultDescription(RequestContext context) {
            return "Pay by credit or debit card through our secure servers which encrypt your card details to the highest standard (128 bit SSL). "
                    + "You can confirm that our system is secure by clicking on the padlock icon on your browser when you are on the card details screen.";
        }

        @Override
        public Form getForm(RequestContext context) {
            return null;
        }

        @Override
        public String getPublicName() {
            return "Credit / Debit card";
        }

        public boolean isForm() {
            return false;
        }

        @Override
        public boolean isOnline() {
            return true;
        }

        public boolean isProcessor() {
            return false;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return true;
        }

        @Override
        public String toString() {
            return "Credit card";
        }

    },
    Cash("Cash") {

        public String getDefaultDescription(RequestContext context) {

            final PaymentSettings ps = PaymentSettings.getInstance(context);

            return "Pay by cash sent securely through the post. It is your responsibility to ensure cash reaches us safely. Please post to the following address and allow up to 10 days for clearance. "
                    + "Don't forget to include your order number (which you will receive once you complete the order) and contact details along with payment.\n\n<b>"
                    + ps.getChequeAddress() + "</b>";
        }

        @Override
        public Form getForm(RequestContext context) {
            return null;
        }

        @Override
        public String getPublicName() {
            return getName();
        }

        public boolean isForm() {
            return false;
        }

        @Override
        public boolean isOnline() {
            return false;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return true;
        }
    },
    Cheque("Postal Order or Cheque") {

        public String getDefaultDescription(RequestContext context) {

            final PaymentSettings ps = PaymentSettings.getInstance(context);

            return "Pay by postal order or cheque made payable to <b>"
                    + ps.getChequePayee()
                    + "</b>.\n\nPlease post to the following address and allow up to 10 days for clearance. "
                    + "Don't forget to include your order number (which you will receive once you complete the order) and contact details along with payment.\n\n<b>"
                    + ps.getChequeAddress() + "</b>";
        }

        @Override
        public Form getForm(RequestContext context) {
            return null;
        }

        @Override
        public String getPublicName() {
            return getName();
        }

        public boolean isForm() {
            return false;
        }

        @Override
        public boolean isOnline() {
            return false;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return true;
        }

        @Override
        public String toString() {
            return "Cheque/Postal Order";
        }
    },
    PayPalStandard("Pay Pal") {

        public String getDefaultDescription(RequestContext context) {
            return "Pay by credit or debit card through " + new LinkTag("http://www.paypal.com", "www.paypal.com").setTarget("_blank")
                    + ". You can also use your Pay Pal account if you have one but that is not required. "
                    + "Your card details are encrypted to the highest standard (128 bit SSL) and we do not store card details after the transaction.";
        }

        @Override
        public Form getForm(RequestContext context) {
            return new PayPalStandard(context);
        }

        @Override
        public String getPublicName() {
            return "Credit / Debit card or Paypal account";
        }

        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isOnline() {
            return true;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {

            PaymentSettings ps = PaymentSettings.getInstance(context);
            return ps.hasPayPalAccountEmail();
        }

        @Override
        public String toString() {
            return "Pay Pal";
        }

        @Override
        public String getConfirmedMessage() {
            return "Please pay by Paypal. You do not need to open a Paypal account, it is secure and confidential, and you can use your credit or debit card.";
        }
    },
    PayPalExpress("Pay Pal Express") {

        public String getDefaultDescription(RequestContext context) {
            return "Pay by credit or debit card through " + new LinkTag("http://www.paypal.com", "www.paypal.com").setTarget("_blank")
                    + ". You can also use your Pay Pal account if you have one but that is not required. "
                    + "Your card details are encrypted to the highest standard (128 bit SSL) and we do not store card details after the transaction.";
        }

        @Override
        public Form getForm(RequestContext context) {
            return new PayPalExpress(context);
        }

        @Override
        public String getPublicName() {
            return "Paypal account (Express Checkout)";
        }

        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isOnline() {
            return true;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {

            PaymentSettings ps = PaymentSettings.getInstance(context);
            return ps.hasPayPalAccountEmail();
        }

        @Override
        public String toString() {
            return "Pay Pal Express";
        }

        @Override
        public String getButtonSrc(RequestContext context) {
            return "files/graphics/payment/paypalexpress.gif";
        }
    },
    PayPalProExpress("PayPal Payments Pro. Express Checkout") {

        public String getDefaultDescription(RequestContext context) {
            return "Pay by credit or debit card through " + new LinkTag("http://www.paypal.com", "www.paypal.com").setTarget("_blank")
                    + ". You can also use your Pay Pal account if you have one but that is not required. "
                    + "Your card details are encrypted to the highest standard (128 bit SSL) and we do not store card details after the transaction.";
        }

        @Override
        public Form getForm(RequestContext context) {
            return new PayPalProExpress(context);
        }

        @Override
        public String getPublicName() {
            return "Paypal account (Express Checkout)";
        }

        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isOnline() {
            return true;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return true;
        }

        @Override
        public String toString() {
            return "Pay Pal Pro Express";
        }

    },
    PayPalProHosted("PayPal Pro Hosted") {

        public String getDefaultDescription(RequestContext context) {
            return "Pay by credit or debit card through " + new LinkTag("http://www.paypal.com", "www.paypal.com").setTarget("_blank")
                    + ". You can also use your Pay Pal account if you have one but that is not required. "
                    + "Your card details are encrypted to the highest standard (128 bit SSL) and we do not store card details after the transaction.";
        }

        @Override
        public Form getForm(RequestContext context) {
            return new PayPalProHosted(context);
        }

        @Override
        public String getPublicName() {
            return "Paypal Pro Hosted Solution";
        }

        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isOnline() {
            return true;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return true;
        }

        @Override
        public String toString() {
            return "PayPal Pro Hosted";
        }

        @Override
        public String getButtonSrc(RequestContext context) {
            return "files/graphics/payment/paypalprohosted.gif";
        }
    },
    PayPalDirect("Pay Pal Direct") {

        public String getDefaultDescription(RequestContext context) {
            return "Pay by credit or debit card through " + new LinkTag("http://www.paypal.com", "www.paypal.com").setTarget("_blank")
                    + ". You can also use your Pay Pal account if you have one but that is not required. "
                    + "Your card details are encrypted to the highest standard (128 bit SSL) and we do not store card details after the transaction.";
        }

        @Override
        public Form getForm(RequestContext context) {
            return new PayPalDirect(context);
        }

        @Override
        public String getPublicName() {
            return "Credit/debit card (Direct Payment)";
        }

        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isOnline() {
            return true;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            //TODO: Change for pay pal direct
            PaymentSettings ps = PaymentSettings.getInstance(context);
            return ps.hasPayPalAccountEmail();
        }

        @Override
        public String toString() {
            return "Pay Pal Direct";
        }

    },
// Protx have changed their name to SagePay
    ProtxForm("SagePay") {

        public String getDefaultDescription(RequestContext context) {
            return "Pay by credit or debit card through our secure server provided by "
                    + "Protx.com which is approved by all the card processing banks. "
                    + "Your card details are encrypted to the highest standard (128 bit SSL) and we do not store card details after the transaction.";
        }

        @Override
        public Form getForm(RequestContext context) {
            return new ProtxForm(context);
        }

        @Override
        public String getPublicName() {
            return "Credit / Debit card";
        }

        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isOnline() {
            return true;
        }

        public boolean isProcessor() {
            return false;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {

            PaymentSettings ps = PaymentSettings.getInstance(context);
            return ps.hasProtxVendorEmail() && ps.hasProtxEncryptionPassword();
        }
    },
    Transfer("Bank Transfer") {

        public String getDefaultDescription(RequestContext context) {

            final PaymentSettings ps = PaymentSettings.getInstance(context);

            return "Pay by bank transfer directly into our account. Don't forget to include your order number (which you will receive when you complete the order) and allow up to 5 days for the payment to be received.\n\n"
                    + ps.getTransferDetails();
        }

        @Override
        public Form getForm(RequestContext context) {
            return null;
        }

        @Override
        public String getPublicName() {
            return "Bank transfer";
        }

        public boolean isForm() {
            return false;
        }

        @Override
        public boolean isOnline() {
            return false;
        }

        public boolean isProcessor() {
            return false;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return true;
        }
    },
    WorldPaySelectJunior("Worldpay") {

        public String getDefaultDescription(RequestContext context) {
            return "Pay by credit or debit card through our secure server provided by "
                    + "Worldpay.com which is owned and operated by Royal Bank of Scotland. "
                    + "Your card details are encrypted to the highest standard (128 bit SSL) and we do not store card details after the transaction.";
        }

        @Override
        public Form getForm(RequestContext context) {
            return new WorldpaySelectJunior(context);
        }

        @Override
        public String getPublicName() {
            return "Credit / Debit card";
        }

        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isOnline() {
            return true;
        }

        public boolean isProcessor() {
            return false;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {

            PaymentSettings ps = PaymentSettings.getInstance(context);
            return ps.hasWorldPayInstallationId();
        }
    },
    EpdqCpi("Barclays ePDQ") {

        public String getDefaultDescription(RequestContext context) {
            return "Pay by credit or debit card through our secure server provided by Barclays ePDQ system. "
                    + "Your card details are encrypted to the highest standard (128 bit SSL) and we do not store card details after the transaction.";
        }

        @Override
        public Form getForm(RequestContext context) {
            return new EpdqCpi(context);
        }

        @Override
        public String getPublicName() {
            return "Credit / Debit card";
        }

        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isOnline() {
            return true;
        }

        public boolean isProcessor() {
            return false;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {

            PaymentSettings ps = PaymentSettings.getInstance(context);
            return ps.hasEpdqStoreId() && ps.hasEpdqPassphrase();
        }
    },

    SecCard("SECPay") {

        public String getDefaultDescription(RequestContext context) {
            return "Pay by credit or debit card through our secure server provided by "
                    + new LinkTag("http://www.secpay.com", "www.secpay.com").setTarget("_blank")
                    + " who are approved by all the card processing banks. "
                    + "Your card details are encrypted to the highest standard (128 bit SSL) and we do not store card details after the transaction.";
        }

        @Override
        public Form getForm(RequestContext context) {
            return new SecCard(context);
        }

        @Override
        public String getPublicName() {
            return "Credit / Debit card";
        }

        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isOnline() {
            return true;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return true;
        }
    },
    Offline("Offline payment") {

        public String getDefaultDescription(RequestContext context) {
            return "If you do not wish to enter your card details online, "
                    + "then choose this option and we will call you back to take your card details over the phone.";
        }

        @Override
        public Form getForm(RequestContext context) {
            return null;
        }

        @Override
        public String getPublicName() {
            return "Details to be collected later";
        }

        public boolean isForm() {
            return false;
        }

        @Override
        public boolean isOnline() {
            return false;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return true;
        }
    },
    Able2Buy("Able2Buy") {

        public String getDefaultDescription(RequestContext context) {

            return "Arrange instant finance online through our partner " + new LinkTag("http://www.able2buy.com", "www.able2buy.com").setTarget("_blank")
                    + " (subject to acceptance). Our application is very quick and you will receive a decision in seconds.";
        }

        @Override
        public Form getForm(RequestContext context) {
            return new Able2BuyForm(context);
        }

        @Override
        public String getPublicName() {
            return "Instant online finance";
        }

        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isOnline() {
            return true;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {

            PaymentSettings ps = PaymentSettings.getInstance(context);
            Money min = ps.getAble2BuyMinSpend();
            Money max = ps.getAble2BuyMaxSpend();

            if (min.isPositive() && total.isLessThan(min)) {
                return false;
            }

            if (max.isPositive() && total.isGreaterThan(max)) {
                return false;
            }

            return true;
        }
    },
    SecureTradingPages("Secure trading") {

        public String getDefaultDescription(RequestContext context) {
            return "Pay by credit or debit card through our secure server provided by "
                    + new LinkTag("http://www.securetrading.com", "www.securetrading.com").setTarget("_blank")
                    + " who are approved by all the card processing banks. "
                    + "Your card details are encrypted to the highest standard (128 bit SSL) and we do not store card details after the transaction.";
        }

        @Override
        public Form getForm(RequestContext context) {
            return new SecureTradingPages(context);
        }

        @Override
        public String getPublicName() {
            return "Credit / Debit card";
        }

        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isOnline() {
            return true;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return true;
        }
    },

    NetPaymentsExpress("Net Payments") {

        public String getDefaultDescription(RequestContext context) {
            return "Pay by credit or debit card through our secure server provided by "
                    + new LinkTag("http://www.netpayments.co.uk", "www.netpayments.co.uk").setTarget("_blank")
                    + " who are approved by all the card processing banks. "
                    + "Your card details are encrypted to the highest standard (128 bit SSL) and we do not store card details after the transaction.";
        }

        @Override
        public Form getForm(RequestContext context) {
            return new NetPaymentsExpress(context);
        }

        @Override
        public String getPublicName() {
            return "Credit / Debit card";
        }

        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isOnline() {
            return true;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return true;
        }
    },
    HsbcCpi("HSBC Secure ePayments") {

        public String getDefaultDescription(RequestContext context) {
            return "Pay by credit or debit card through our secure server provided by Hsbc Secure e-payments. "
                    + "Your card details are encrypted to the highest standard (128 bit SSL) and we do not store card details after the transaction.";
        }

        @Override
        public Form getForm(RequestContext context) {
            return new HsbcCpi(context);
        }

        @Override
        public String getPublicName() {
            return "Credit / Debit card";
        }

        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isOnline() {
            return true;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return PaymentSettings.getInstance(context).hasHsbcCpiHashKey();
        }
    },
    OptimalPayments("Optimal payments") {

        @Override
        public String getDefaultDescription(RequestContext context) {
            return "Pay by credit or debit card";
        }

        @Override
        public Form getForm(RequestContext context) {
            return new OptimalPaymentsDynamic(context);
        }

        @Override
        public String getPublicName() {
            return "Credit / Debit card";
        }

        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isOnline() {
            return true;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return true;
        }
    },
    MetaCharge("Meta charge") {

        @Override
        public String getDefaultDescription(RequestContext context) {
            return "Pay by credit or debit card";
        }

        @Override
        public Form getForm(RequestContext context) {
            return new MetaChargeForm(context);
        }

        @Override
        public String getPublicName() {
            return "Credit / Debit card";
        }

        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isOnline() {
            return true;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return true;
        }

    },
    Test("Test form payment") {

        @Override
        public String getDefaultDescription(RequestContext context) {
            return "Use this payment method for testing";
        }

        @Override
        public Form getForm(RequestContext context) {
            return new TestFormPayment(context);
        }

        @Override
        public String getPublicName() {
            return "Test form payment";
        }

        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return true;
        }

    },
    NoChex("NoChex") {

        @Override
        protected String getDefaultDescription(RequestContext context) {
            return null;
        }

        @Override
        public Form getForm(RequestContext context) {
            return new NoChexPaymentPage(context);
        }

        @Override
        public String getPublicName() {
            return "NoChex";
        }

        @Override
        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return true;
        }

        @Override
        public String getPublicNameImg() {
            return "<input type=\"image\" src=\"files/graphics/payment/nochexcardsboth1.gif \" />";
        }

    },
    ManualPayments("Manual Payments") {

    @Override
    public String getDefaultDescription(RequestContext context) {
        return "Manual Payments";
    }

    @Override
    public String getPublicMessage() {
        return "Thank you for your booking. We will contact you shortly to confirm the booking and take payment.";
    }

    @Override
    public Form getForm(RequestContext context) {
        return null;
    }

    @Override
    public String getPublicName() {
        return "Manual Payments";
    }

    public boolean isForm() {
        return false;
    }

    @Override
    public boolean isValid(RequestContext context, Money total) {
        return true;
    }

},

    GoogleCheckout("Google Checkout") {

        @Override
        protected String getDefaultDescription(RequestContext context) {
            return "Fast checkout through Google  (Test version. It doesn't make real payments)";
        }

        @Override
        public Form getForm(RequestContext context) {
            return new GoogleCheckoutForm(context);
        }

        @Override
        public String getPublicName() {
            return "Google Checkout";
        }

        @Override
        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return true;
        }

        @Override
        public String getButtonSrc(RequestContext context) {
            PaymentSettings paymentSettings = PaymentSettings.getInstance(context);
            String merchantId = paymentSettings.getGoogleCheckoutId();
            return MessageFormat.format("https://checkout.google.com/buttons/checkout.gif?merchant_id={0}&w=160&h=43&style=trans&variant=text&loc=en_GB", merchantId);
        }

    },

    CardsaveRedirect("Cardsave. Redirect/Hosted") {

        @Override
        protected String getDefaultDescription(RequestContext context) {
            return "The Cardsave Payment Gateway offers a secure, reliable platform for merchants to take payments online";
        }

        public Form getForm(RequestContext context) {
            return new CardsaveForm(context);
        }

        @Override
        public String getPublicName() {
            return "Cardsave payment";
        }

        @Override
        public boolean isForm() {
            return true;
        }

        @Override
        public boolean isValid(RequestContext context, Money total) {
            return true;
        }
    },

;

    private final String name;

    private PaymentType(String name) {
        this.name = name;
    }

    protected abstract String getDefaultDescription(RequestContext context);

    public abstract Form getForm(RequestContext context);

    public final String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * Returns a name that means something to the customer
     */
    public abstract String getPublicName();

     public String getPublicNameImg(){
         return getPublicName();
     };

    public void inlineCallback(RequestContext context, MultiValueMap<String, String> parameters, String remoteIp) {

        try {

            if (isForm()) {

                Form form = getForm(context);
                if (form != null) {
                    form.inlineCallback(parameters, remoteIp);
                }

            }

        } catch (IOException e) {
            e.printStackTrace();

        } catch (FormException e) {
            e.printStackTrace();
        }

    }

    public abstract boolean isForm();

    public boolean isManual() {
        return false;
    }

    public boolean isOnline() {
        return isForm();
    }

    /**
     * Returns true if this payment type is valid for these params
     */
    public abstract boolean isValid(RequestContext context, Money total);

    public String getPublicMessage() {
        return null;
    }

    public String getButtonSrc(RequestContext context) {
        return null;
    }

    public String getConfirmedMessage() {
        return "Your order will be paid for by " + getPublicName();
    }
}
