package org.sevensoft.ecreator.model.ecom.orders.parcels.trackers;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sks 10-Jan-2006 13:56:53
 *
 */
public class RoyalMailTracker extends Tracker {

	public String getTrackingUrl(String consignment) {

		Map<String, String> params = new HashMap<String, String>();
		params.put("/rmg/track/TrackFormHandler.value.trackingNumber", consignment);
		params.put("_D:/rmg/track/TrackFormHandler.value.invalidInputUrl", "");
		params.put("_D:/rmg/track/TrackFormHandler.value.incomingInternationalParcel", "");
		params.put("/rmg/track/TrackFormHandler.value.invalidInputUrl", "/portal/po/track?catId=20700386&pageId=trt_resultspage");
		params.put("/rmg/track/TrackFormHandler.value.searchCompleteUrl", "/portal/po/track?catId=20700386&pageId=trt_resultspage");
		params.put("/rmg/track/TrackFormHandler.value.searchWaitUrl", "/portal/po/track?catId=20700386&timeout=true&pageId=trt_timeoutpage");
		params.put("_D:/rmg/track/TrackFormHandler.value.searchCompleteUrl", "");
		params.put("_D:/rmg/track/TrackFormHandler.value.searchWaitUrl", "");
		params.put("_D:/rmg/track/TrackFormHandler.value.trackingNumber", "");
		params.put("_dyncharset", "ISO-8859-1");
		params.put("trackConsigniaPage", "track");

		return "POST /portal/rm/trackresults?ut%3Dtrue%26pageId%3Dtrt_timeoutpage&_D%3A%2Frmg%2Ftrack%2FRMTrackFormHandler.value.searchWaitUrl=+&%2Frmg%2Ftrack%2FRMTrackFormHandler.value.trackingNumber=AA12345678gb&_D%3A%2Frmg%2Ftrack%2FRMTrackFormHandler.value.trackingNumber=+&%2Frmg%2Ftrack%2FRMTrackFormHandler.track.x=37&%2Frmg%2Ftrack%2FRMTrackFormHandler.track.y=13&_D%3A%2Frmg%2Ftrack%2FRMTrackFormHandler.track=+&%2Frmg%2Ftrack%2FRMTrackFormHandler.value.day=9&_D%3A%2Frmg%2Ftrack%2FRMTrackFormHandler.value.day=+&%2Frmg%2Ftrack%2FRMTrackFormHandler.value.month=1&_D%3A%2Frmg%2Ftrack%2FRMTrackFormHandler.value.month=+&%2Frmg%2Ftrack%2FRMTrackFormHandler.value.year=2006&_D%3A%2Frmg%2Ftrack%2FRMTrackFormHandler.value.year=+&_DARGS=%2Fportal%2Frmgroup%2Fapps%2Ftemplates%2Fhtml%2FtrtRMHomeTemplate.jsp";
	}

}
