package org.sevensoft.ecreator.model.ecom.delivery.markers;

import java.util.Map;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.ecom.delivery.DeliveryOption;
import org.sevensoft.ecreator.model.ecom.orders.VatHelper;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 8 Feb 2007 10:21:37
 *
 */
public class DeliveryRatesMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		if (!Module.Delivery.enabled(context)) {
			return null;
		}

		String rateprefix = params.get("rateprefix");
		String ratesuffix = params.get("ratesuffix");

		StringBuilder sb = new StringBuilder();
		sb.append(new TableTag("ec_delrates"));

		for (Map.Entry<DeliveryOption, Money> entry : item.getDelivery().getDeliveryRatesMap().entrySet()) {

			Money charge;
			if (params.containsKey("ex")) {
				charge = entry.getValue();
			} else {
				charge = VatHelper.inc(context, entry.getValue(), entry.getKey().getVatRate());
			}

			sb.append("<tr>");
			sb.append("<td class='ec_deloption'>" + entry.getKey().getName() + "</td>");
			sb.append("<td class='ec_delrate'>");

			if (rateprefix != null) {
				sb.append(rateprefix);
			}

			sb.append(charge);

			if (ratesuffix != null) {
				sb.append(ratesuffix);
			}

			sb.append("</td>");
			sb.append("</tr>");
		}

		sb.append("</table>");

		return sb.toString();
	}

	public Object getRegex() {
		return "delivery_rates";
	}

}
