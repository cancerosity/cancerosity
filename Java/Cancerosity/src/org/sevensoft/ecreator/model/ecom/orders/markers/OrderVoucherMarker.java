package org.sevensoft.ecreator.model.ecom.orders.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderMarker;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * @author Dmitry Lebedev 30.03.2010
 */
public class OrderVoucherMarker extends MarkerHelper implements IOrderMarker {

    public Object generate(RequestContext context, Map<String, String> params, Order order) {
        return super.string(context, params, order.getVoucherDescription());
    }

    public Object getRegex() {
        return "order_voucher";
    }
}
