package org.sevensoft.ecreator.model.ecom.payments.forms.epdq;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.httpc.HttpMethod;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.ecom.payments.results.Callback;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 07-Nov-2005 14:38:06
 * 
 */
public class EpdqCpi extends Form {

	public EpdqCpi(RequestContext context) {
		super(context);
	}

	private String encryptData(FormSession session, Payable payable) throws IOException {

		PaymentSettings paymentSettings = PaymentSettings.getInstance(context);

		HttpClient hc = new HttpClient("https://secure2.epdq.co.uk/cgi-bin/CcxBarclaysEpdqEncTool.e", HttpMethod.Post);
		hc.setParameter("total", payable.getPaymentAmount().toEditString());

		/* 
		 * Text string that can contain the unique order reference code to use for this transaction.
		 * We will use the session ID so we can look it up later 
		 * Oid must be alpha/numeric only (no special characters other than dash).
		 */
		hc.setParameter("oid", session.getIdString());

		// Auth or PreAuth
		hc.setParameter("chargetype", "Auth");

		// Text string containing your passphrase. Important � this must be the passphrase and not the store password.

		hc.setParameter("password", paymentSettings.getEpdqPassphrase());

		// ISO currency code (e.g. 826 for gbp)
		hc.setParameter("currencycode", "826");

		// ePDQ administration service Client Id (Also known as your Store ID)
		hc.setParameter("clientid", paymentSettings.getEpdqStoreId());

		hc.connect();

		String input = hc.getResponseString();

		try {

			Matcher matcher = Pattern.compile("value=\"(.*?)\"").matcher(input);
			if (matcher.find()) {
				input = matcher.group(1);
				return input;
			}

			return null;

		} catch (RuntimeException e) {

			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getMethod() {
		return "POST";
	}

	@Override
	public Map<String, String> getParams(Payable payable) throws IOException, FormException {

		final Company company = Company.getInstance(context);
		FormSession session = new FormSession(context, payable);

		Map<String, String> params = new HashMap();

		params.put("returnurl", payable.getPaymentSuccessUrl(PaymentType.EpdqCpi));

		// name of the company for displaying on epdq site, should be 25 characters or less they recommend.
		params.put("merchantdisplayname", company.getName(25));

		params.put("epdqdata", encryptData(session, payable));
		params.put("email", payable.getPayableAccount().getEmail());
        log("[ePDQ cpi params] "+ params);
		return params;
	}

	@Override
	public String getPaymentUrl() {
		return "https://secure2.epdq.co.uk/cgi-bin/CcxBarclaysEpdq.e";
	}

	@Override
	public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
	}

	@Override
	public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
        log("[EpdpCpi] server callback, params=" + params);

		new Callback(context, PaymentType.EpdqCpi, params);

		FormSession session = EntityObject.getInstance(context, FormSession.class, params.get("oid"));
		if (session == null) {
			return null;
		}

		String status = params.get("transactionstatus");
		if (!"success".equalsIgnoreCase(status)) {
			return null;
		}

		Money amount = new Money(params.get("total"));
		Payment payment = new Payment(context, session.getAccount(), amount, PaymentType.EpdqCpi);

		session.callback(params, ipAddress, payment);
		return null;
	}
}
