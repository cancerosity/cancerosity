package org.sevensoft.ecreator.model.ecom.shopping.markers.totals;

import java.util.Map;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketLineMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderMarker;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Jun 2006 11:46:17
 *
 */
public class SubtotalMarker implements IBasketMarker, IOrderMarker, IBasketLineMarker {

	public Object generate(RequestContext context, Map<String, String> params, Basket basket) {

		Money total;
		if (params.containsKey("ex")) {
			total = basket.getSubtotalEx();
		} else {
			total = basket.getSubtotalInc();
		}

		Currency currency = (Currency) context.getAttribute("currency");
		return currency.getSymbol() + currency.convert(total).toString(2);
	}

	public Object generate(RequestContext context, Map<String, String> params, BasketLine line) {

		Money total;
		if (params.containsKey("ex")) {
			total = line.getSalePriceEx();
		} else {
			total = line.getSalePriceInc();
		}

		Currency currency = (Currency) context.getAttribute("currency");
		return currency.getSymbol() + currency.convert(total).toString(2);
	}

	public Object generate(RequestContext context, Map<String, String> params, Order order) {

		Money total;
		if (params.containsKey("ex")) {
			total = order.getTotalEx();
		} else {
			total = order.getTotalInc();
		}

		Currency currency = (Currency) context.getAttribute("currency");
		return currency.getSymbol() + currency.convert(total).toString(2);
	}

	public Object getRegex() {
		return "subtotal";
	}
}
