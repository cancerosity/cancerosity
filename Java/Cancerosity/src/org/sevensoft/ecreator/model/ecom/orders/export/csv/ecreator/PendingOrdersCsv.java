package org.sevensoft.ecreator.model.ecom.orders.export.csv.ecreator;

import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.collections.CollectionsUtil;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.io.Writer;

import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.QuoteType;
import com.ricebridge.csvman.CsvSaver;

/**
 * User: Dmitry Lebedev
 * Date: 09.09.2009
 * Time: 20:26:26
 */
public class PendingOrdersCsv {

    private final Collection<Order> orders;

    public PendingOrdersCsv(RequestContext context, Collection<Order> orders) {
		this.orders = orders;
    }

    public PendingOrdersCsv(RequestContext context, Order order) {
        this(context, Collections.singleton(order));
    }

    private String[] getHeader() {

        List<String> row = new ArrayList<String>();

        // order details
		row.add("Order ID");
		row.add("Order Date Placed");
        row.add("Customer Name");
        row.add("Billing Name");
        row.add("Billing Address 1");
        row.add("Billing Address 2");
        row.add("Billing Address 3");
        row.add("Billing Address 4");
        row.add("Billing Address 5");
        row.add("Billing Postcode");
        row.add("Billing Country");
        row.add("Shipping Name");
        row.add("Shipping Address 1");
        row.add("Shipping Address 2");
        row.add("Shipping Address 3");
        row.add("Shipping Address 4");
        row.add("Shipping Address 5");
        row.add("Shipping Postcode");
        row.add("Shipping Country");
        row.add("Customer Email");
        row.add("Customer Phone");
        row.add("Product Code");
        row.add("Item Description");
        row.add("Quantity");
        row.add("Item Price (ex VAT)");
        row.add("Carrige Amout (ex VAT)");

        return row.toArray(new String[] {});
    }

    protected String[] getRow(OrderLine orderLine) {

        List<String> row;
		row = new ArrayList<String>();

        row.add(orderLine.getOrder().getIdString());
        row.add(orderLine.getOrder().getDatePlaced().toString("dd/MM/yyyy"));
        row.add(orderLine.getOrder().getAccount().getName());
        row.add(orderLine.getOrder().getBillingAddress().getFirstName() + " " + orderLine.getOrder().getBillingAddress().getLastName());
        row.add(orderLine.getOrder().getBillingAddress().getAddressLine1());
        row.add(orderLine.getOrder().getBillingAddress().getAddressLine2());
        row.add(orderLine.getOrder().getBillingAddress().getAddressLine3());
        row.add(orderLine.getOrder().getBillingAddress().getTown());
        row.add(orderLine.getOrder().getBillingAddress().getCounty());
        row.add(orderLine.getOrder().getBillingAddress().getPostcode());
        row.add(orderLine.getOrder().getBillingAddress().getCountry().getName());
        row.add(orderLine.getOrder().getDeliveryAddress().getFirstName() + " " + orderLine.getOrder().getDeliveryAddress().getLastName());
        row.add(orderLine.getOrder().getDeliveryAddress().getAddressLine1());
        row.add(orderLine.getOrder().getDeliveryAddress().getAddressLine2());
        row.add(orderLine.getOrder().getDeliveryAddress().getAddressLine3());
        row.add(orderLine.getOrder().getDeliveryAddress().getTown());
        row.add(orderLine.getOrder().getDeliveryAddress().getCounty());
        row.add(orderLine.getOrder().getDeliveryAddress().getPostcode());
        row.add(orderLine.getOrder().getDeliveryAddress().getCountry().getName());
        row.add(orderLine.getOrder().getAccount().getEmail());
        row.add(orderLine.getOrder().getBillingAddress().getTelephone());
        row.add(orderLine.getItem().getAttributeValue("Product Code"));
        row.add(orderLine.getItem().getName());
        row.add(Integer.toString(orderLine.getQty()));
        row.add(orderLine.getItem().getSellPrice().toString());
        row.add(orderLine.getOrder().getDeliveryChargeEx().toString());

        CollectionsUtil.replaceNulls(row, "");

		return row.toArray(new String[] {});
    }

    public void output(Writer writer) {

        CsvManager csv = new CsvManager();
		csv.setSeparator(",");
		csv.setQuoteType(QuoteType.AsNeeded);
		csv.setQuote('"');
		csv.setEscape('\\');

		CsvSaver saver = csv.makeSaver(writer);

		saver.begin();
		saver.next(getHeader());

        for (Order order : orders) {
            for (OrderLine line : order.getLines()) {
				saver.next(getRow(line));
			}

            order.setStatus("In Process");
            order.save();
        }

        saver.end();
    }
    
}
