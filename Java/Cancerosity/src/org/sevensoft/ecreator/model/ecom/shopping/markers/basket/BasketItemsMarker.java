package org.sevensoft.ecreator.model.ecom.shopping.markers.basket;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 04-Apr-2006 10:26:42
 *
 */
public class BasketItemsMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.Shopping.enabled(context)) {
			return null;
		}

		Basket basket = (Basket) context.getAttribute("basket");
		if (basket == null) {
			return null;
		}

		return super.string(context, params, basket.size());
	}

	public Object getRegex() {
		return "basket_items";
	}
}