package org.sevensoft.ecreator.model.ecom.payments.processors.hsbc;

import java.io.IOException;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.ecom.payments.processors.HsbcApiHandler;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentException;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardTypeException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CscException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiredCardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiryDateException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.IssueNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.StartDateException;
import org.sevensoft.ecreator.model.ecom.payments.processors.Processor;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorException;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorResult;
import org.sevensoft.ecreator.model.ecom.payments.processors.misc.ClearComRequest;
import org.sevensoft.ecreator.model.ecom.payments.processors.misc.ClearComTxType;
import org.sevensoft.ecreator.model.ecom.payments.results.AvsCheck;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Feb 2007 15:40:19
 *
 */
@Table("payments_processor_hsbcapi")
@HandlerClass(HsbcApiHandler.class)
public class HsbcApiProcessor extends Processor {

	private static final String	POST_URL	= "https://www.secure-epayments.apixml.hsbc.com";

	private String			username, password, clientId;

	public HsbcApiProcessor(RequestContext context) {
		super(context);
	}

	@Override
	public AvsCheck avsCheck(Card card, Address address) throws IOException, ProcessorException {
		return null;
	}

	@Override
	public Payment credit(Payment payment, Money money) throws IOException, ExpiryDateException, CardNumberException, CardTypeException, StartDateException,
			ProcessorException, CardException, IssueNumberException {
		return null;
	}

	@Override
	protected ProcessorResult debit(Item account, Card card, Address address, Money money) throws CardException, ExpiryDateException, IssueNumberException,
			CardTypeException, CscException, CardNumberException, StartDateException, IOException, ExpiredCardException, ProcessorException,
			PaymentException {

		logger.fine("[HsbcApiProcessor] debit card=" + card);

		ClearComRequest request = new ClearComRequest(context, POST_URL, account, ClearComTxType.Auth);

		request.setCard(card);
		request.setAddress(address);
		request.setAmount(money);

		request.setUsername(username);
		request.setPassword(password);
		request.setClientId(clientId);

		logger.fine("[HsbcApiProcessor] request=" + request);

		return request.process();
	}

	public final String getClientId() {
		return clientId;
	}

	@Override
	public String getName() {
		return "Hsbc Api";
	}

	public final String getPassword() {
		return password;
	}

	public final String getUsername() {
		return username;
	}

	@Override
	public boolean isLive() {
		return false;
	}

	public final void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public final void setPassword(String password) {
		this.password = password;
	}

	public final void setUsername(String username) {
		this.username = username;
	}

	@Override
	public void verify(Card card) throws CardException, ExpiryDateException, IssueNumberException, CardTypeException, CardNumberException,
			StartDateException, IOException, ExpiredCardException, ProcessorException, CscException {
	}

	@Override
	public boolean voidPayment(Payment payment) throws IOException, ProcessorException {
		return false;
	}

}
