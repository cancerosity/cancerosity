package org.sevensoft.ecreator.model.ecom.payments.cards.exceptions;

/**
 * @author sks 26-Oct-2005 10:58:47
 * 
 */
public class CardNumberException extends Exception {

	public CardNumberException() {
		super();
	}

	public CardNumberException(String message) {
		super(message);
	}

	public CardNumberException(String message, Throwable cause) {
		super(message, cause);
	}

	public CardNumberException(Throwable cause) {
		super(cause);
	}

}
