package org.sevensoft.ecreator.model.ecom.payments.forms;

import java.io.IOException;
import java.util.Map;
import java.util.logging.Logger;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionSession;
import org.sevensoft.ecreator.model.attachments.downloads.DownloadSession;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.listings.ListingRate;
import org.sevensoft.ecreator.model.items.listings.sessions.ListingPaymentSession;
import org.sevensoft.ecreator.model.marketing.sms.credits.SmsCreditsSession;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.StartId;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 22 May 2006 11:11:31
 *
 */
@Table("payments_sessions")
public class FormSession extends EntityObject {

    public enum SessionType {
        Order, Subscription, Listing, Download, SmsCredits
    }

	@SuppressWarnings("hiding")
	private final static Logger	logger	= Logger.getLogger("payments");

	public static FormSession get(RequestContext context, String transactionId) {

		Query q = new Query(context, "select * from # where transactionId=?");
		q.setParameter(transactionId);
		q.setTable(FormSession.class);
		return q.get(FormSession.class);
	}

    public static FormSession getByOrder(RequestContext context, int order) {
        Query q = new Query(context, "select * from # ps where ps.sessiontype like ? and ps.order=?");
        q.setParameter(SessionType.Order);
        q.setParameter(order);
        q.setTable(FormSession.class);
        return q.get(FormSession.class);
    }

	/**
	 * Our transacton id
	 */
	private String			transactionId;

	/**
	 * The type of callback
	 */
	private SessionType		sessionType;

	/**
	 * 
	 */
	private Money			amount;

	private Item			account;

	/**
	 * 
	 */
	private Order			order;

	private SubscriptionSession	subscriptionSession;

	private ListingRate		listingRate;

	private Item			item;

	private Basket			basket;

	private DownloadSession		downloadSession;

	private SmsCreditsSession	smsCreditsSession;

	public FormSession(RequestContext context) {
		super(context);
	}

	public FormSession(RequestContext context, Payable payable) {

		super(context);

		this.account = payable.getPayableAccount();
		this.transactionId = Payment.generateUniqueTransactionId(context);

		if (payable instanceof DownloadSession) {
			this.sessionType = SessionType.Download;
			this.downloadSession = (DownloadSession) payable;
		}

		else if (payable instanceof Order) {
			this.sessionType = SessionType.Order;
			this.order = (Order) payable;
		}

		else if (payable instanceof SubscriptionSession) {
			this.sessionType = SessionType.Subscription;
			this.subscriptionSession = (SubscriptionSession) payable;
		}

		else if (payable instanceof ListingPaymentSession) {

			this.sessionType = SessionType.Listing;
			this.listingRate = ((ListingPaymentSession) payable).getListingRate();
			this.item = ((ListingPaymentSession) payable).getItem();

		} else if (payable instanceof SmsCreditsSession) {

			this.sessionType = SessionType.SmsCredits;
			this.smsCreditsSession = ((SmsCreditsSession) payable);

		} else {

			throw new RuntimeException("Unrecognised payable!");

		}

		save();
	}

	public String callback(Map<String, String> params, String ipAddress, Payment payment) {

		/*
		 * If payment is not null then we will check if we have any credit packages to credit
		 */
		if (payment != null) {

			//			List<CreditPackage> creditPackages = CreditPackage.getOnSpend(context, payment.getAmount());
			//			for (CreditPackage creditPackage : creditPackages) {

			// as these are 'free' packages given as bonuses they do not need to log payment info
			//				getMember().credit(creditPackage, null, null);
			//			}
		}

		logger.severe("[FormSession] callback type=" + sessionType);

		switch (sessionType) {

		default:
			return null;

		case Download:

			if (downloadSession == null) {
				return null;
			}

			try {

				getDownloadSession().download(payment, ipAddress);
				return getDownloadSession().getPaymentSuccessUrl(null);

			} catch (AttachmentExistsException e) {
				e.printStackTrace();

			} catch (AttachmentTypeException e) {
				e.printStackTrace();

			} catch (IOException e) {
				e.printStackTrace();

			} catch (AttachmentLimitException e) {
				e.printStackTrace();
			}
			return null;

		case SmsCredits:

			if (smsCreditsSession == null) {
				return null;
			}

			logger.config("[FormSession] smsCreditsSession=" + smsCreditsSession);
			smsCreditsSession.pop();
			smsCreditsSession.complete(payment, ipAddress);
			return null;

		case Listing:

			if (listingRate == null) {
				return null;
			}

			if (item == null) {
				return null;
			}

			logger.config("[FormSession] listingRate=" + listingRate);
			logger.config("[FormSession] item=" + item);

			getItem().getListing().list(getListingRate(), payment, ipAddress);
			return null;

		case Order:

			if (order == null) {
				return null;
			}

            if (payment == null) {
                return null;
            }

            logger.severe("[FormSession] adding payment \"" + payment + "\" to order:" + getOrder().getIdString());

            getOrder().addPayment(payment);

            //send email
            try {
                getOrder().sendNotificationEmails();
            } catch (EmailAddressException e) {
                logger.warning("[FormSession] Sending notification email failed " + e);
            } catch (SmtpServerException e) {
                logger.warning("[FormSession] Sending notification email failed " + e);
            }

             try {
                getOrder().sendAcknowledgementEmail();
            } catch (EmailAddressException e) {
                logger.warning("[FormSession] Sending acknowledgement email failed " + e);
            } catch (SmtpServerException e) {
                logger.warning("[FormSession] Sending acknowledgement email failed " + e);
            }

            return getOrder().getPaymentSuccessUrl(null);

			//			AvsResult avsResult = getAvsResult(params);
			//			if (avsResult != null)
			//				new AvsCheck(order, payment, avsResult);

		case Subscription:

			if (subscriptionSession == null) {
				return null;
			}

			logger.config("[FormSession] subscriptionSession=" + subscriptionSession);

			subscriptionSession.pop();
			subscriptionSession.subcribe(payment, ipAddress);

			return subscriptionSession.getPaymentSuccessUrl(null);
		}

	}

	public final Item getAccount() {
		return account;
	}

	public final Money getAmount() {
		return amount;
	}

	public DownloadSession getDownloadSession() {
		return downloadSession.pop();
	}

	public Item getItem() {
		return item.pop();
	}

	public ListingRate getListingRate() {
		return listingRate.pop();
	}

	public Order getOrder() {
		return order.pop();
	}

	public final SessionType getSessionType() {
		return sessionType;
	}

	public final String getTransactionId() {
		return transactionId;
	}

	public final void setAccount(Item account) {
		this.account = account;
	}

}
