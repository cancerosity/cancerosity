package org.sevensoft.ecreator.model.ecom.orders.parcels.trackers;

import java.util.Map;

/**
 * @author sks 06-Dec-2005 12:40:38
 * 
 */
public abstract class Tracker {

	public String getMethod() {
		return "GET";
	}

	public Map<String, String> getParams() {
		return null;
	}

	public abstract String getTrackingUrl(String consignment);
}
