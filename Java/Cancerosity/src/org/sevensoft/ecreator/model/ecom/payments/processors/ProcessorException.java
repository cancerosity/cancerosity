package org.sevensoft.ecreator.model.ecom.payments.processors;


/**
 * @author sks 19-Jan-2006 21:05:22
 *
 */
public class ProcessorException extends Exception {

	public ProcessorException() {
		super();
	}

	public ProcessorException(String message) {
		super(message);
	}

	public ProcessorException(String message, Throwable cause) {
		super(message, cause);
	}

	public ProcessorException(Throwable cause) {
		super(cause);
	}

}
