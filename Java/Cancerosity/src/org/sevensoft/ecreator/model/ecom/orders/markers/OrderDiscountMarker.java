package org.sevensoft.ecreator.model.ecom.orders.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderMarker;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.skint.Money;

import java.util.Map;

/**
 * @author Dmitry Lebedev
 *         Date: 26.01.2010
 */
public class OrderDiscountMarker extends MarkerHelper implements IOrderMarker {

    public Object generate(RequestContext context, Map<String, String> params, Order order) {

        Money money;

        if (params.containsKey("ex")) {
            money = order.getVoucherDiscountEx();
        } else if (params.containsKey("vat")) {
            money = order.getVoucherDiscountVat();
        } else if (params.containsKey("inc")) {
            money = order.getVoucherDiscountInc();
        } else if (params.containsKey("abs")){
            money = order.getVoucherDiscountIncAbs();
        } else {
            money = order.getVoucherDiscountInc();
        }

        return super.string(context, params, money);
    }

    public Object getRegex() {
        return "order_discount";
    }
}
