package org.sevensoft.ecreator.model.ecom.addresses.exceptions;

/**
 * @author sks 17-Dec-2004 12:54:33
 */
public class ContactNameException extends Exception {

	/**
	 * 
	 */
	public ContactNameException() {
		super("Contact name invalid.");
	}

	/**
	 * @param string
	 */
	public ContactNameException(String string) {
		super(string);
	}

}
