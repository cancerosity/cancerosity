package org.sevensoft.ecreator.model.ecom.payments.cards.exceptions;

/**
 * @author sks 26-Oct-2005 10:58:29
 * 
 */
public class CardTypeException extends Exception {

	public CardTypeException() {
	}

	public CardTypeException(String message, Throwable cause) {
		super(message, cause);
	}

	public CardTypeException(String message) {
		super(message);
	}

	public CardTypeException(Throwable cause) {
		super(cause);
	}

	

}
