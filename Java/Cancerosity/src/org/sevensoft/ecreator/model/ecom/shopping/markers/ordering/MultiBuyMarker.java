package org.sevensoft.ecreator.model.ecom.shopping.markers.ordering;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;

/**
 * @author sks 29 Nov 2006 07:04:24
 *
 */
public class MultiBuyMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		// no need to change the basket handler as the default is for multi buy 

		String label = params.get("label");
		if (label == null)
			return new SubmitTag("Buy");
		else
			return new SubmitTag(label);
	}

	@Override
	public String getDescription() {
		return "Adds in all items in the category to the basket if their respective qtys > 0";
	}

	public Object getRegex() {
		return "ordering_multibuy";
	}

}
