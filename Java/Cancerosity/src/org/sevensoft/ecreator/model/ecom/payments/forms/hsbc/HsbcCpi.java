package org.sevensoft.ecreator.model.ecom.payments.forms.hsbc;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.*;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.callbacks.HsbcCallbackHandler;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.ecom.payments.results.Callback;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

import com.clearcommerce.CpiTools.security.HashGenerator;

/**
 * @author sks 19 Apr 2006 16:07:54
 *
 */
public class HsbcCpi extends Form {

    private static final String POST_URL_HSBC = "https://www.cpi.hsbc.com/servlet";
    private static final String POST_URL_IRIS = "https://cpi.globaliris.com/servlet";

    public enum TransactionType {
        Auth, Capture
    }
    
    public HsbcCpi(RequestContext context) {
		super(context);
	}

	private boolean checkHash(Map<String, String> params) {

		String orderHash = params.remove("OrderHash");
		if (orderHash == null) {
			return false;
		}

		PaymentSettings paymentSettings = PaymentSettings.getInstance(context);

		Map<String, String> sortedParams = new TreeMap<String, String>(params);

		try {

			String hashValue = HashGenerator.generateHash(new Vector<String>(sortedParams.values()), paymentSettings.getHsbcCpiHashKey());

			if (orderHash.equals(hashValue)) {
				return true;
			}

		} catch (GeneralSecurityException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		}

		return false;
	}

	@Override
	public String getMethod() {
		return "POST";
	}

	@Override
	public Map<String, String> getParams(Payable payable) throws IOException, FormException {

		PaymentSettings paymentSettings = PaymentSettings.getInstance(context);
		if (!paymentSettings.hasHsbcCpiHashKey()) {
			throw new FormException("No HSBC Cpi hash key provided");
		}

		FormSession session = new FormSession(context, payable);
		Map<String, String> params = new LinkedHashMap();

		Config config = Config.getInstance(context);

		// callback url MUST BE SECURE ON HSBC !
		params.put("CpiDirectResultUrl", (config.getUrl() + "/" + new Link(HsbcCallbackHandler.class)).replace("http://", "https://"));

		/* 
		 * This is the page HSBC will direct back to after payment. 
		 * MUST BE SECURE ON HSBC !
		 */
		params.put("CpiReturnUrl", payable.getPaymentSuccessUrl(PaymentType.HsbcCpi).replace("http://", "https://"));

		/*
		 * We will use the merchant data field to store our sessionID
		 */
		params.put("MerchantData", session.getIdString());

		/*
		 * Must be one of the following values: 
		 * P - Production mode. The customer will be billed  for the order.
		 * T - Test mode. No money will be taken.
		 */
		if (paymentSettings.isHsbcCpiLive()) {
			params.put("Mode", "P");
		} else {
			params.put("Mode", "T");
		}
//        if (!paymentSettings.isHsbcCpiLive()) {
//            params.put("Mode", "T");
//        }

        String desc = payable.getPaymentDescription();
		if (desc == null) {
			desc = Company.getInstance(context).getName();
		}

		if (desc.length() > 50) {
			desc = desc.substring(50);
		}

		// max 50 characters on desc
		params.put("OrderDesc", payable.getPaymentDescription());
		params.put("OrderId", payable.getIdString());
		params.put("PurchaseAmount", payable.getPaymentAmount().getAmountString());
		params.put("PurchaseCurrency", "826");      //todo should I change it depending on Currency module
		params.put("StorefrontId", paymentSettings.getHsbcCpiStorefrontId());

		// timestamp in millis
		params.put("TimeStamp", String.valueOf(System.currentTimeMillis()));

        if (paymentSettings.hasHsbcCpiTransactionType()) {
            params.put("TransactionType", paymentSettings.getHsbcCpiTransactionType());
        }

		// UserId will be used to store our member's id
		params.put("UserId", payable.getPayableAccount().getIdString());

		if (payable instanceof Order) {

			Order order = (Order) payable;

			Address address;
			if (order.hasBillingAddress()) {
				address = order.getBillingAddress();
			} else {
				address = order.getDeliveryAddress();
			}

			params.put("BillingAddress1", address.getAddressLine1());
			params.put("BillingCity", address.getTown());
			params.put("BillingCountry", String.valueOf(address.getCountry().getIsoNumber3()));
//            	params.put("BillingCountry", "826");
			params.put("BillingFirstName", address.getFirstName());
			params.put("BillingLastName", address.getLastName());
			params.put("BillingPostal", address.getPostcode());

			params.put("ShopperEmail", payable.getPayableAccount().getEmail());

			address = order.getDeliveryAddress();

			params.put("ShippingAddress1", address.getAddressLine1());
			params.put("ShippingCity", address.getTown());
            params.put("ShippingCountry", String.valueOf(address.getCountry().getIsoNumber3()));
//			params.put("ShippingCountry", "826");
			params.put("ShippingFirstName", address.getFirstName());
			params.put("ShippingLastName", address.getLastName());
			params.put("ShippingPostal", address.getPostcode());

		}

		/* 
		 * create a vector (how old is that) for the param VALUES which must be in the same order as they appear in the form
		 * hence me using a linked hash map here (a linked hash map will definitely return an ordered list for the values)
		 */

		Vector<String> hashElements = new Vector<String>(params.values());

		/*
		 * Generate hash value
		 */
		try {

			String hashValue = null;
			hashValue = HashGenerator.generateHash(hashElements, paymentSettings.getHsbcCpiHashKey());
			params.put("OrderHash", hashValue);

		} catch (GeneralSecurityException e) {
			e.printStackTrace();

		}

		logger.fine("[HsbcCpi] Sending these params to hsbc: " + params);
        log("[HsbcCpi] Sending these params to hsbc: " + params);

		return params;
	}

	private Payment getPayment(Item member, Money amount, Date date) {
		return member.addPayment(PaymentType.HsbcCpi, amount, date);
	}

	@Override
	public String getPaymentUrl() {
        if (PaymentSettings.getInstance(context).isHsbcCpiIris()) {
            return POST_URL_IRIS;
        }
		return POST_URL_HSBC;
	}

	@Override
	public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
	}

	@Override
	public String serverCallback(Map<String, String> params, String ipAddress) throws FormException {
        log("[HsbcCpi] Server callback params=" + params);

        String cpiResultsCode = params.get("CpiResultsCode");

        params.put("CpiResultsCodeValue", cpiResultsCodeValues.get(Integer.parseInt(cpiResultsCode)));

        new Callback(context, PaymentType.HsbcCpi, params);

		/*
		 * Code zero is success
		 */
		if ("0".equals(cpiResultsCode) == false) {
			return null;
		}

		// merchant data is used to store our sessionId
		FormSession session = EntityObject.getInstance(context, FormSession.class, params.get("MerchantData"));
		if (session == null) {
			return null;
		}

		if (checkHash(params) == false) {
			return null;
		}

		String purchaseAmount = params.get("PurchaseAmount");
		Money amount = new Money(Integer.parseInt(purchaseAmount));
		if (amount.isZero()) {
			return null;
		}

		Payment payment = new Payment(context, session.getAccount(), amount, PaymentType.HsbcCpi);
		session.callback(params, ipAddress, payment);
		return null;
	}

    public static final List<String> cpiResultsCodeValues = new ArrayList<String>(Arrays.asList(
            "The transaction was approved.",
            "The user cancelled the transaction.",
            "The processor declined the transaction for an unknown reason.",
            "The transaction was declined because of a problem with the card.",
            "The processor did not return a response.",
            "The amount specified in the transaction was either too high or too low for the processor.",
            "The specified currency is not supported by either the processor or the card.",
            "The order is invalid because the order ID is a duplicate.",
            "The transaction was rejected by FraudShield.",
            "The transaction was placed in Review state by FraudShield (see note 1 below).",
            "The transaction failed because of invalid input data.",
            "The transaction failed because the CPI was configured incorrectly.",
            "The transaction failed because the Storefront was configured incorrectly.",
            "The connection timed out.",
            "The transaction failed because the cardholder�s browser refused a cookie.",
            "The customer�s browser does not support 128-bit encryption.",
            "The CPI cannot communicate with the Payment Engine."
    ));
}
