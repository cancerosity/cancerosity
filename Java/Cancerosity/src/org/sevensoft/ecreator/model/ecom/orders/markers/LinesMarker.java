package org.sevensoft.ecreator.model.ecom.orders.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderMarker;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 22 Feb 2007 11:17:26
 *
 */
public class LinesMarker extends MarkerHelper implements IOrderMarker {

	public Object generate(RequestContext context, Map<String, String> params, Order order) {

		OrderSettings orderSettings = OrderSettings.getInstance(context);

		MarkupRenderer r = new MarkupRenderer(context, orderSettings.getInvoiceLinesMarkup());
		r.setBodyObjects(order.getLines());
		return r;
	}

	public Object getRegex() {
		return "order_lines";
	}

}
