package org.sevensoft.ecreator.model.ecom.payments.forms.paypal;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;

import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.httpc.HttpMethod;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.callbacks.PaypalCallbackHandler;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.ecom.payments.results.Callback;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 15-Sep-2004 21:04:52
 */
public class PayPalStandard extends Form {

	private static final String	SANDBOX_URL	= "https://www.sandbox.paypal.com/cgi-bin/webscr";

	private static final String	URL		= "https://www.paypal.com/cgi-bin/webscr";

	public PayPalStandard(RequestContext context) {
		super(context);
	}

	@Override
	public String getMethod() {
		return "POST";
	}

	@Override
	public Map<String, String> getParams(Payable payable) {

		FormSession session = new FormSession(context, payable);
		Config config = Config.getInstance(context);

        String currencyCode = "";

        if(Currency.getDefault(context).getCode() != null && !Currency.getDefault(context).getCode().equals("")) {
            currencyCode = Currency.getDefault(context).getCode();
        } else {
            currencyCode = "GBP";
        }

		Map<String, String> map = new LinkedHashMap<String, String>();

		map.put("cancel_return", payable.getPaymentFailureUrl(PaymentType.PayPalStandard));
		map.put("return", payable.getPaymentSuccessUrl(PaymentType.PayPalStandard));

		map.put("item_name", payable.getPaymentDescription());
		map.put("quantity", "1");

		// xclick is our type of basket.
		map.put("cmd", "_xclick");

		map.put("notify_url", config.getUrl() + "/" + new Link(PaypalCallbackHandler.class));

		// paypal account email
		map.put("business", PaymentSettings.getInstance(context).getPayPalAccountEmail());

		/* 
		 * invoice must be completely unique so we will put in our generated transaction id
		 */
		map.put("invoice", session.getTransactionId());

		/*
		 * We use custom to store our session id
		 */
		map.put("custom", session.getIdString());

		// customer details
		//		map.put("email", payable.getPayableAccount().getEmail());
		//		map.put("first_name", payable.getPayableAccount().getFirstName());
		//		map.put("last_name", payable.getPayableAccount().getLastName());

		// We don't need the customer to enter a note (instructions)
		map.put("no_note", "1");

		// total for the order including handling (shipping). It seems paypal will remove the shipping we pass in from this to display subtotal
		map.put("amount", payable.getPaymentAmount().toEditString());

		// disallow customer instructions
//		map.put("no_note", "1");         //todo duplicated

		// currency code
		map.put("currency_code", currencyCode);

//		map.put("item_name", payable.getPaymentDescription());       //todo duplicated
//		map.put("quantity", "1");         //todo duplicated

		//		/* 
		//		 * handling is like shipping but a one off charge whereas it seems shipping is item based
		//		 */
		//		map.put("handling", deliveryCharge.toEditString());

		return map;
	}

	private Payment getPayment(Item customer, Map<String, String> params) {

		String auth_id = params.get("auth_id");
		String payer = params.get("payer_email");
		String txn_id = params.get("txn_id");
		String payment_gross = params.get("mc_gross");

		if (params.containsKey("first_name") && params.containsKey("last_name")) {
			payer = params.get("first_name") + " " + params.get("last_name") + " (" + payer + ")";
		}

		logger.fine("[PaypalStandard] getting payment auth_id=" + auth_id + ", payer=" + payer + ", txn_id=" + txn_id + ", payment_gross=" + payment_gross);

		Payment payment = new Payment(context, customer, new Money(payment_gross), PaymentType.PayPalStandard);
		payment.setProcessorTransactionId(txn_id);
		payment.setDetails(payer);
		payment.setAuthCode(auth_id);
		payment.setProcessorTransactionId(txn_id);
		payment.save();

		return payment;
	}

	@Override
	public String getPaymentUrl() {
		if (PaymentSettings.getInstance(context).isPayPalLive())
			return URL;
		else
			return SANDBOX_URL;
	}

	@Override
	public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
	}

	private void pdtPost(Map<String, String> params, boolean debug) throws IOException, FormException {

		if (!PaymentSettings.getInstance(context).hasPayPalPdtToken()) {

			logger.config("[PaypalStandard] no pdt token set");
			throw new FormException("No at token set");
		}

		HttpClient hc = new HttpClient("https://www.paypal.com/cgi-bin/webscr", HttpMethod.Post);
		hc.setParameter("cmd", "_notify-synch");

		String tx = params.get("txn_id");
		logger.config("[PaypalStandard] tx token=" + tx);
		logger.config("[PaypalStandard] at token=" + PaymentSettings.getInstance(context).getPayPalPdtToken());

		if (tx == null) {
			throw new FormException("No transaction token provided for paypal callback");
		}

		// the transaction token
		hc.setParameter("tx", tx);

		/* 
		 * at = identity token
		 * 
		 * The identity token is the password bascially that pp will issue for us to confirm we are the correct user for the transaction we are 
		 * requesting details for.
		 */
		hc.setParameter("at", PaymentSettings.getInstance(context).getPayPalPdtToken());
		hc.connect();

		String input = hc.getResponseString();
		if (input == null) {
			logger.config("[PaypalStandard] no response from paypal pdt post");
			throw new IOException("Invalid response: null");
		}

		logger.config("[PaypalStandard] input received: " + input);

		String[] rows = input.split("\n");
		if (rows.length == 0 || rows[0].equalsIgnoreCase("FAIL")) {
			throw new FormException("Invalid TX: " + tx);
		}

		for (int n = 1; n < rows.length; n++) {

			try {

				String[] param = rows[n].split("=");
				if (param != null && param.length == 2) {

					String key = URLDecoder.decode(param[0], "UTF-8");
					String value = URLDecoder.decode(param[1], "UTF-8");

					params.put(key, value);

				}

			} catch (UnsupportedEncodingException e) {
				throw new IOException("Unsupported encoding: UTF-8 - must be a bug!!!!");
			}
		}

		new Callback(context, PaymentType.PayPalStandard, params);

		logger.config("[PaypalStandard] params after input: " + params);

		/* 
		 * check payment status is competed to ensure payment has gone through successfully
		 */
		if (!"Completed".equals(params.get("payment_status"))) {
			logger.config("[PaypalStandard] payment is not completed");
			throw new FormException("Payment is not completed so do not process via callback");
		}

		/* 
		 * check txn_id is not duplicate otherwise user could continue to click refresh on the return url GET request and our site would
		 * continue to lookup the details and process as if they were a new transaction each time.
		 */
		final String txn_id = params.get("txn_id");
		if (Payment.exists(context, txn_id, PaymentType.PayPalStandard)) {
			logger.config("[PaypalStandard] txn_id already logged " + txn_id);
			throw new FormException("Existing payment so do not process; potential fraud");
		}

		/* 
		 * 
		 * business = the email of the paypal account
		 * 
		 * check business is us otherwise this transaction could be sent elsewhere (ie a valid amount, etc) 
		 * and we wouldn't get the money but the system would still process the as if it was ours.
		 */
		final String business = params.get("business");
		if (!PaymentSettings.getInstance(context).getPayPalAccountEmail().equalsIgnoreCase(business)) {
			logger.config("[PaypalStandard] receiver_email not our account " + business);
			throw new FormException("This transaction was not sent to us");
		}
	}

	@Override
	public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
        log("[PaypalStandard] server callback, params=" + params);

		logger.severe("[PaypalStandard] server callback, params=" + params);

        new Callback(context, PaymentType.PayPalStandard, params);

		verifyIpn(params);

		logger.severe("[PaypalStandard] IPN verified");

		/*
		 * Get session id from custom field
		 */
		FormSession session = EntityObject.getInstance(context, FormSession.class, params.get("custom"));
		if (session == null) {
            log("[PaypalStandard] no session found for id=" + params.get("custom"));
			logger.severe("[PaypalStandard] no session found for id=" + params.get("custom"));
			return null;
		}

		Payment payment = getPayment(session.getAccount(), params);
		if (payment == null) {
            log("[PaypalStandard] no payment found/created for user=" + session.getAccount() + " and params=" + params);
            logger.severe("[PaypalStandard] no payment found/created for user=" + session.getAccount() + " and params=" + params);
            return null;
		}

        log("[PaypalStandard] calling callback on session=" + session);
		logger.severe("[PaypalStandard] calling callback on session=" + session);

		return session.callback(params, ipAddress, payment);
	}

	private void verifyIpn(Map<String, String> params) throws FormException, IOException {

		HttpClient hc = new HttpClient("https://www.paypal.com/cgi-bin/webscr", HttpMethod.Post);
		hc.setParameter("cmd", "_notify-validate");

		for (Map.Entry<String, String> entry : params.entrySet()) {
			hc.setParameter(entry.getKey(), entry.getValue());
		}

        log("[PaypalStandard] ipn verify, hc=" + hc);
		logger.severe("[PaypalStandard] ipn verify, hc=" + hc);

		hc.connect();
		String response = hc.getResponseString();
		if ("Verified".equalsIgnoreCase(response)) {

			new Callback(context, PaymentType.PayPalStandard, params);

			logger.severe("[PaypalStandard] params after input: " + params);

			/* 
			 * check payment status is competed to ensure payment has gone through successfully
			 */
			if (!"Completed".equalsIgnoreCase(params.get("payment_status"))) {
                log("[PaypalStandard] payment is not completed");
				logger.severe("[PaypalStandard] payment is not completed");
				throw new FormException("Payment is not completed so do not process");
			}

			/* 
			 * check txn_id is not duplicate otherwise user could continue to click refresh on the return url GET request and our site would
			 * continue to lookup the details and process as if they were a new transaction each time.
			 */
			final String txn_id = params.get("txn_id");
			if (Payment.exists(context, txn_id, PaymentType.PayPalStandard)) {
                log("[PaypalStandard] txn_id already logged " + txn_id);
				logger.severe("[PaypalStandard] txn_id already logged " + txn_id);
				throw new FormException("Existing payment so do not process; potential fraud");
			}

			/* 
			 * 
			 * business = the email of the paypal account
			 * 
			 * check business is us otherwise this transaction could be sent elsewhere (ie a valid amount, etc) 
			 * and we wouldn't get the money but the system would still process the as if it was ours.
			 */
			final String receiver_email = params.get("receiver_email");
			if (!PaymentSettings.getInstance(context).getPayPalAccountEmail().equalsIgnoreCase(receiver_email)) {
                log("[PaypalStandard] receiver_email not our account " + receiver_email);
				logger.severe("[PaypalStandard] receiver_email not our account " + receiver_email);
				throw new FormException("This transaction was not sent to us");
			}
            log("[PaypalStandard] IPN verified");
			logger.severe("[PaypalStandard] IPN verified");
			
		} else {
			log("[PaypalStandard] IPN not verified=" + response);
			logger.severe("[PaypalStandard] IPN not verified=" + response);
			throw new FormException("IPN not verified=" + response);
		}
	}
}

//		String status = pdt.get("st");
//		Money amount = new Money(pdt.get("amt"));
//		String message = pdt.get("cm");
//		String custom = pdt.get("custom");
//		String item_number = pdt.get("item_number");
//		String invoice = pdt.get("invoice");
//
//		String auth_id = pdt.get("auth_id");
//		String auth_amount = pdt.get("auth_amount");
//
//		String payer_email = pdt.get("payer_email");
//		String txn_id = pdt.get("txn_id");
//		String receiver_email = pdt.get("receiver_email");
//		String payment_status = pdt.get("payment_status");
//		String receipt_id = pdt.get("receipt_id");