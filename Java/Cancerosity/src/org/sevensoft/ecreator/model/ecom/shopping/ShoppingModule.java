package org.sevensoft.ecreator.model.ecom.shopping;

import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 12 Jan 2007 09:30:01
 *
 */
@Table("shopping_module")
public class ShoppingModule extends EntityObject {

	private ItemType	itemType;

	protected ShoppingModule(RequestContext context) {
		super(context);
	}

	public ShoppingModule(RequestContext context, ItemType itemType) {
		super(context);
		this.itemType = itemType;
		save();
	}

}
