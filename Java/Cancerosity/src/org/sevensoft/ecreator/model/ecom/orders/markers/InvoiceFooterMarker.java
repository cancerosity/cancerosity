package org.sevensoft.ecreator.model.ecom.orders.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * User: Tanya
 * Date: 24.02.2011
 */
public class InvoiceFooterMarker extends MarkerHelper implements IGenericMarker {

    public Object generate(RequestContext context, Map<String, String> params) {
        OrderSettings orderSettings = OrderSettings.getInstance(context);
        return super.string(context, params, orderSettings.getInvoiceFooter());
    }

    public Object getRegex() {
        return "invoice_footer";
    }
}
