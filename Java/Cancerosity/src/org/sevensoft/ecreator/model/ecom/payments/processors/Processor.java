package org.sevensoft.ecreator.model.ecom.payments.processors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentException;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardTypeException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CscException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiredCardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiryDateException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.IssueNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.StartDateException;
import org.sevensoft.ecreator.model.ecom.payments.processors.epdq.EpdqMpiProcessor;
import org.sevensoft.ecreator.model.ecom.payments.processors.hsbc.HsbcApiProcessor;
import org.sevensoft.ecreator.model.ecom.payments.processors.protx.ProtxDirectProcessor;
import org.sevensoft.ecreator.model.ecom.payments.processors.secpay.SecPayXmlProcessor;
import org.sevensoft.ecreator.model.ecom.payments.results.AvsCheck;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * 
 * The processor does the actual connection to the server and processing of the payment / card.
 * 
 * @author sks 29-Mar-2006 18:30:23
 *
 */
public abstract class Processor extends EntityObject {

	public static final Class[]	processorClasses	= new Class[] { HsbcApiProcessor.class, ProtxDirectProcessor.class, SecPayXmlProcessor.class,
			EpdqMpiProcessor.class			};

	@SuppressWarnings("hiding")
	protected static Logger		logger		= Logger.getLogger("payments");

	public static Processor get(RequestContext context) {
		List<Processor> processors = getAll(context);
		return processors.isEmpty() ? null : processors.get(0);
	}

	public static List<Processor> getAll(RequestContext context) {

		List<Processor> processors = new ArrayList();

		for (Class processorClass : processorClasses) {
			processors.addAll(SimpleQuery.execute(context, processorClass));
		}

		return processors;
	}

	protected Processor(RequestContext context) {
		super(context);
	}

	/**
	 * Preauths this card for the minimum spend allowed by the processor to get an AVS result for this card / address combo.
	 * Returns the AVS check object that this processor will create.
	 *  
	 */
	public abstract AvsCheck avsCheck(Card card, Address address) throws IOException, ProcessorException, ExpiredCardException, ExpiryDateException,
			CardNumberException, CardException, IssueNumberException, CscException, PaymentException;

	/**
	 * Will refund this payment. Returns a new payment representing the refund transaction.
	 * 
	 */
	public abstract Payment credit(Payment payment, Money amount) throws IOException, ExpiryDateException, CardNumberException, CardTypeException,
			StartDateException, ProcessorException, CardException, IssueNumberException;

	/**
	 * Debits this card for the money parameter. Will set address
	 * fields for not null address which is used for AVS checking.
	 * 
	 * If the payment fails it will through an exception
	 * 
	 */
	protected abstract ProcessorResult debit(Item account, Card card, Address address, Money amount) throws CardException, ExpiryDateException,
			IssueNumberException, CardTypeException, CscException, CardNumberException, StartDateException, IOException, ExpiredCardException,
			ProcessorException, PaymentException;

	protected ProcessorResult debit(Payable payable) throws CardException, ExpiryDateException, IssueNumberException, CardTypeException, CscException,
			CardNumberException, StartDateException, IOException, ExpiredCardException, ProcessorException, PaymentException {

		return debit(payable.getPayableAccount(), payable.getCard(), payable.getPaymentAddress(), payable.getPaymentAmount());
	}

	@Override
	public synchronized boolean delete() {
		new Query(context, "update # set id=0 where id=?").setTable(Payment.class).setParameter(this).run();
		return super.delete();
	}

	/**
	 * 
	 */
	public final Class<? extends Handler> getHandlerClass() {
		return getClass().getAnnotation(HandlerClass.class).value();
	}

	/**
	 * 
	 */
	public abstract String getName();

	public abstract boolean isLive();

	public final Payment transact(Item account, Card card, Address address, Money amount) throws CardException, ExpiryDateException, IssueNumberException,
			CardTypeException, CscException, CardNumberException, StartDateException, IOException, ExpiredCardException, ProcessorException,
			PaymentException {

		ProcessorResult result = debit(account, card, address, amount);

		Payment payment = new Payment(context, account, amount, PaymentType.CardTerminal);

		payment.setProcessorResult(result);
		payment.setDetails(card.getCardNumberSecure());
		payment.setInternalTransactionId(result.getInternalTransactionId());
		payment.setProcessorTransactionId(result.getProcessorTransactionId());
		payment.save();

		return payment;
	}

	/**
	 * Transact this payable using the card details stored on it
	 * 
	 */
	public final Payment transact(Payable payable) throws CardException, ExpiryDateException, IssueNumberException, CardTypeException, CscException,
			CardNumberException, StartDateException, IOException, ExpiredCardException, ProcessorException, PaymentException {

		logger.fine("[Processor] transact, payable=" + payable);

		ProcessorResult result = debit(payable);

		Payment payment = new Payment(context, payable.getPayableAccount(), payable.getPaymentAmount(), PaymentType.CardTerminal);

		payment.setProcessorResult(result);
		payment.setDetails(payable.getCard().getCardNumberSecure());
		payment.setInternalTransactionId(result.getInternalTransactionId());
		payment.setProcessorTransactionId(result.getProcessorTransactionId());
		payment.save();

		return payment;
	}

	/**
	 * Attempts to verify this card. Throws an exception if the card is invalid.
	 *
	 */
	public abstract void verify(Card card) throws CardException, ExpiryDateException, IssueNumberException, CardTypeException, CardNumberException,
			StartDateException, IOException, ExpiredCardException, ProcessorException, CscException;

	/**
	 * Attempts to void this transaction. 
	 * Returns true if the payment has been voided, false if it cannot be voided due to time expiration.
	 */
	public abstract boolean voidPayment(Payment payment) throws IOException, ProcessorException;

}
