package org.sevensoft.ecreator.model.ecom.shopping.markers.basket;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.ecom.shopping.BasketHandler;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketMarker;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.vouchers.Voucher;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 27 Jun 2006 11:48:11
 *
 */
public class VoucherTag implements IBasketMarker {

	public Object generate(RequestContext context, Map<String, String> params, Basket basket) {

		if (!Module.Vouchers.enabled(context)) {
			return null;
		}

		if (!Voucher.hasVouchers(context)) {
			return null;
		}

		String instruction = params.get("instruction");

		StringBuilder sb = new StringBuilder();

		/*
		 * if the basket has a voucher then show remove voucher option
		 */
		if (basket.hasVoucher()) {

			sb.append("Voucher: " + basket.getVoucher().getName() + " " + basket.getVoucher().getCode());
			sb.append("<br/>");
			sb.append(new LinkTag(BasketHandler.class, "removeVoucher", "remove voucher"));

		} else {

			if (instruction == null) {
				instruction = "If you have a voucher code, enter it in the box below";
			}

			sb.append(instruction);
			sb.append("<br/>");
			sb.append(new TextTag(context, "voucherCode", 20));

		}

		return sb;
	}

	public Object getRegex() {
		return "voucher";
	}

}
