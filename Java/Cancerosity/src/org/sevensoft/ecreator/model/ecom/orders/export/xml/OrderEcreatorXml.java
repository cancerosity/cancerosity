package org.sevensoft.ecreator.model.ecom.orders.export.xml;

import org.jdom.Document;
import org.jdom.Element;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;

/**
 * @author sks 22 Aug 2006 15:22:35
 *
 */
public class OrderEcreatorXml {

	private final Order	order;

	public OrderEcreatorXml(Order order) {
		this.order = order;
	}

	private void addAddressEl(Element deliveryAddressEl, Address deliveryAddress) {
		deliveryAddressEl.addContent(new Element("Name").setText(deliveryAddress.getName()));
		deliveryAddressEl.addContent(new Element("Address").setText(deliveryAddress.getAddress("\n")));
		deliveryAddressEl.addContent(new Element("Name").setText(deliveryAddress.getPostcode()));
		deliveryAddressEl.addContent(new Element("Country").setText(deliveryAddress.getCountry().toString()));
		deliveryAddressEl.addContent(new Element("Telephone").setText(deliveryAddress.getTelephone1()));
	}

	public Document getDoc() {

		//		Element root = new Element("Orders");

		//
		//		for (Order order : orders) {

		Element orderEl = new Element("Order");
		Document doc = new Document(orderEl);

		orderEl.addContent(new Element("OrderId").setText(order.getIdString()));

		if (order.hasReference()) {
			orderEl.addContent(new Element("Reference").setText(order.getReference()));
		}

		orderEl.addContent(new Element("SalesPerson").setText(order.getSalesPersonString()));
		orderEl.addContent(new Element("DatePlaced").setText(order.getDatePlaced().toString("dd-MM-yy")));
		orderEl.addContent(new Element("DatePlacedTimestamp").setText(order.getDatePlaced().getTimestampString()));
		orderEl.addContent(new Element("Status").setText(order.getStatus()));

		Element accountEl = new Element("Account").setText(order.getAccount().getIdString());
		orderEl.addContent(accountEl);

		orderEl.addContent(new Element("DeliveryType").setText(order.isCollection() ? "Collection" : "Delivery"));
		if (order.hasDeliveryAddress()) {

			Element addressEl = new Element("DeliveryAddress");
			orderEl.addContent(addressEl);

			Address address = order.getDeliveryAddress();
			addAddressEl(addressEl, address);
		}

		if (order.hasBillingAddress()) {

			Element addressEl = new Element("BillingAddress");
			orderEl.addContent(addressEl);

			Address address = order.getBillingAddress();
			addAddressEl(addressEl, address);
		}

		if (order.hasDeliveryDetails()) {

			Element deliveryEl = new Element("Delivery");
			orderEl.addContent(deliveryEl);

			deliveryEl.addContent(new Element("DeliveryDetails").setText(order.getDeliveryDetails()));
			deliveryEl.addContent(new Element("DeliveryChargeExVat").setText(order.getDeliveryChargeEx().toEditString()));
			deliveryEl.addContent(new Element("DeliveryChargeVat").setText(order.getDeliveryChargeVat().toEditString()));
			deliveryEl.addContent(new Element("DeliveryChargeIncVat").setText(order.getDeliveryChargeInc().toEditString()));

		}

		orderEl.addContent(new Element("TotalExVat").setText(order.getTotalEx().toEditString()));
		orderEl.addContent(new Element("TotalVat").setText(order.getTotalVat().toEditString()));
		orderEl.addContent(new Element("TotalIncVat").setText(order.getTotalInc().toEditString()));

		for (OrderLine line : order.getLines()) {

			Element lineEl = new Element("OrderLine");
			orderEl.addContent(lineEl);

			if (line.hasItem())
				lineEl.addContent(new Element("ItemCode").setText(line.getIdString()));

			lineEl.addContent(new Element("Description").setText(line.getDescription()));

			if (line.hasOptionsDetails())
				lineEl.addContent(new Element("OptionsDetails").setText(line.getOptionsDetails()));

			lineEl.addContent(new Element("Qty").setText(String.valueOf(line.getQty())));
			lineEl.addContent(new Element("SellPriceExVat").setText(line.getUnitSellEx().toEditString()));
			lineEl.addContent(new Element("SellPriceVat").setText(line.getUnitSellVat().toEditString()));
			lineEl.addContent(new Element("SellPriceIncVat").setText(line.getUnitSellInc().toEditString()));

		}

		return doc;
	}
}
