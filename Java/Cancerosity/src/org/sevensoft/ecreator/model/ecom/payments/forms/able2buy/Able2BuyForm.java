package org.sevensoft.ecreator.model.ecom.payments.forms.able2buy;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.ecom.shopping.CheckoutHandler;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.ecom.payments.results.Callback;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 19-Jan-2006 23:33:20
 *
 *The minimum total price of all goods in the basket is \243120 and the maximum is \24325,000
 *
 *B00  	ELECTRICAL
 *B11 	MOBILE PHONE
 *B13 	Servc CONTRACT/EXT WARRANTY
 *B15 	COMPUTER
 *F00 	FITTED FURNITURE
 *F09 	CARPET
 *H05 	UPHOLSTERY / SOFT FURNISHINGS
 *L00 	LEISURE
 *L01 	HOLIDAY
 *L02 	BICYCLE
 *L03 	SPORTS EQUIPMENT
 *L06 	MUSICAL INSTRUMENT/EQUIPMENT
 *L10 	FISHING EQUIPMENT
 *L13 	GOLFING EQUIPMENT
 *M00 	MISCELLANEOUS
 *P00 	PHOTOGRAPHIC
 *S00 	HOME IMPROVEMENTS
 *T00 	AUTOMOTIVE
 *UNK 	Unknown Goodscode#
 *W00 	WHITE GOODS
 *Y28 	JEWELLERY
 
 
 Description  	Field Name  	Maximum lengths

 Retailer's Product Code  			pc(n)					 20


 Retailer's Goods Description			 gd(n)					 80

 able2buy Goods Code (List)			 gc(n)					3

 Goods Price						 gp(n)					7

 Quantity							 q(n)			


 *
 */
public class Able2BuyForm extends Form {

	private static String	TestUrl	= "http://uat.creditenabler.com/able2buy/servlet/applicationmanager.RetailerPostBox";

	private static String	LiveUrl	= "https://www.creditenabler.com/able2buy/servlet/applicationmanager.RetailerPostBox";

	public Able2BuyForm(RequestContext context) {
		super(context);
	}

	@Override
	public String getMethod() {
		return "POST";
	}

	/**
	 * Able to buy only really makes sense on orders so if not an order then through a form exception
	 */
	@Override
	public Map<String, String> getParams(Payable payable) throws IOException, FormException {

		if (!(payable instanceof Order)) {
			throw new FormException("Able2Buy only supports orders at the moment");
		}

		CreditGroup group = payable.getAble2BuyCreditGroup();
		if (group == null) {
			throw new FormException("no Able2Buy credit group selected");
		}

		Order order = (Order) payable;
		Config config = Config.getInstance(context);
		PaymentSettings paymentSettings = PaymentSettings.getInstance(context);
		FormSession session = new FormSession(context, payable);

		Map<String, String> map = new LinkedHashMap();

		// protocol version
		map.put("ver", "2.0");

		// Retailer Id (id) is the username basically
		map.put("id", paymentSettings.getAble2BuyId());

		// Retailer Id2 (id2) is the password basically
		map.put("id2", paymentSettings.getAble2BuyId2());

		// Credit Group (cgid) is the type of credit we are applying for
		map.put("cgid", group.getCreditGroupId());

		// email
		map.put("eea", payable.getPayableAccount().getEmail());

		//		 Generic group code (goodscode) is a generic goods code if you sell all the same stuff.
		//		map.put("goodscode", paymentSettings.getAble2BuyGoodsCode());

		// eurl is the error url (I think this is the 'cancel credit' option ie returns to checkout
		map.put("eurl", config.getUrl() + "/" + new Link(CheckoutHandler.class));

		// Retail Order ID (ro) is retail order, ie the order id (optional) Appened to return url
		// I will use the id from the form session
		map.put("ro", session.getIdString());

		// Items (it) is number of lines in the basket
		map.put("it", String.valueOf(order.getLineCount()));

		// in test mode setting this determins outcome
		// A = ACCEPT
		// D = DECLINE
		// R = REFER
		if (!paymentSettings.isAble2BuyLive()) {
			map.put("testdecision", "A");
		}

		// lines
		int n = 1;
		for (OrderLine line : order.getLines()) {

			if (line.hasItem()) {

				Item item = line.getItem();

				// group code
				map.put("gc" + n, paymentSettings.getAble2BuyGoodsCode());
				map.put("pc" + n, item.getIdString());
				map.put("gd" + n, line.getDescription());

				// goods price
				map.put("gp" + n, line.getUnitSellInc().toEditString());
				map.put("q" + n, String.valueOf(line.getQty()));
				n++;

			}
		}

		map.put("firstname", payable.getPayableAccount().getFirstName());
		map.put("surname", payable.getPayableAccount().getLastName());

		if (order.hasDeliveryAddress()) {

			Address address = order.getDeliveryAddress();
			map.put("street", address.getAddressLine1());
			map.put("area", address.getAddressLine2());
			map.put("town", address.getTown());
			if (address.hasCounty()) {
				map.put("county", address.getCounty());
			}
			map.put("postcode", address.getPostcode());
			map.put("phone", address.getTelephone1());
			map.put("contactPhone", address.getTelephone2());

		}

		// will add in phone and contactPhone at a later date

		return map;
	}

	@Override
	public String getPaymentUrl() {

		PaymentSettings ps = PaymentSettings.getInstance(context);
		return ps.isAble2BuyLive() ? LiveUrl : TestUrl;
	}

	@Override
	public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
	}

	@Override
	public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
        log("[Able2Buy] server callback, params=" + params);

		new Callback(context, PaymentType.Able2Buy, params);

		// ro was used to store the form session id
		String ro = params.get("ro");
		FormSession session = EntityObject.getInstance(context, FormSession.class, ro);

		if (session == null) {
			logger.fine("[Able2BuyForm] no session found for ro=" + ro);
			return null;
		}

		String a2bref = params.get("a2bref");
		String credit = params.get("credit");

		if (credit.equals("accept")) {

			Payment payment = new Payment(context, session.getAccount(), session.getAmount(), PaymentType.Able2Buy);
			payment.setProcessorTransactionId(a2bref);
			payment.save();

			session.callback(params, ipAddress, payment);
		}

		return null;
	}
}
