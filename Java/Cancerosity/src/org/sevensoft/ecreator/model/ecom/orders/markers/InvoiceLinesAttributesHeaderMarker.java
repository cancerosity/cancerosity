package org.sevensoft.ecreator.model.ecom.orders.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderLineMarker;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;
import java.util.Set;
import java.util.LinkedHashSet;

/**
 * User: Tanya
 * Date: 25.02.2011
 */
public class InvoiceLinesAttributesHeaderMarker extends MarkerHelper implements IOrderLineMarker {

    public Object generate(RequestContext context, Map<String, String> params, OrderLine orderLine) {

        StringBuilder sb = new StringBuilder();
        Set<Attribute> columns = new LinkedHashSet<Attribute>();

        if (orderLine.hasItem()) {

            ItemType type = orderLine.getItem().getItemType();
            for (Attribute attribute : type.getAttributes()) {

                if (attribute.isInvoiceColumn())
                    columns.add(attribute);
            }
        }

        /*
           * Items
           */

        for (Attribute column : columns) {
            sb.append("<th>");
            sb.append(string(context, params, column.getInvoiceName()));
            sb.append("</th>");
        }

        return sb.toString();
    }

    public Object getRegex() {
        return "invoice_attributes_names";
    }
}
