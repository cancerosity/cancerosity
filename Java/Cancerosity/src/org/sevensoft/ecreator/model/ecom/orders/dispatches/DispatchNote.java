package org.sevensoft.ecreator.model.ecom.orders.dispatches;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 17 Apr 2007 08:42:59
 *
 */
@Table("dispatches")
public class DispatchNote extends EntityObject {

	private Date dateCreated;
	private Order	order;

	protected DispatchNote(RequestContext context) {
		super(context);
	}

}
