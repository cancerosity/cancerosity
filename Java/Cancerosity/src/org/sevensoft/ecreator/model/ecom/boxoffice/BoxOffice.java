package org.sevensoft.ecreator.model.ecom.boxoffice;

import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 21 Jul 2006 07:03:41
 *
 */
@Table("items_modules_boxoffice")
public class BoxOffice extends EntityObject {

	private ItemType	itemType;

	private int		maxQtyPerMember;

	/**
	 * Instant booking means booking this item type will bypass the basket.
	 * When set to false will use the basket for booking multiple tickets at once.
	 */
	private boolean	instantBooking;

	public BoxOffice(RequestContext context) {
		super(context);
	}

	public BoxOffice(RequestContext context, ItemType type) {
		super(context);

		itemType = type;
		this.instantBooking = true;

		save();
	}

	public ItemType getItemType() {
		return itemType.pop();
	}

	public int getMaxQtyPerMember() {
		return maxQtyPerMember;
	}

	public boolean isInstantBooking() {
		return instantBooking;
	}

	public void setInstantBooking(boolean instantBooking) {
		this.instantBooking = instantBooking;
	}

	public void setMaxQtyPerMember(int maxQtyPerMember) {
		this.maxQtyPerMember = maxQtyPerMember;
	}

}
