package org.sevensoft.ecreator.model.ecom.orders.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderMarker;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * User: Tanya
 * Date: 24.02.2011
 */
public class InvoiceDeliveryMarker extends MarkerHelper implements IOrderMarker {
    
    public Object generate(RequestContext context, Map<String, String> params, Order order) {
        String address = "Collection";

        if (order.isDelivery()) {
            address = order.getDeliveryAddress().getLabel("<br/>", OrderSettings.getInstance(context).isPrintCustomerTelepnone());
        }

        return super.string(context, params, address);
    }

    public Object getRegex() {
        return "invoice_delivery_label";
    }
}
