package org.sevensoft.ecreator.model.ecom.shopping.markers.totals;

import java.util.Map;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketLineMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderMarker;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Jun 2006 11:46:17
 *
 */
public class TotalMarker implements IBasketMarker, IOrderMarker, IBasketLineMarker {

	public Object generate(RequestContext context, Map<String, String> params, Basket basket) {
		return generate(context, basket.getTotalInc(), params);

	}

	private Object generate(RequestContext context, Money money, Map<String, String> params) {

		int dp;
		if (params.containsKey("dp")) {
			dp = Integer.parseInt(params.get("dp").trim());
		} else {
			dp = 2;
		}

		Currency currency = (Currency) context.getAttribute("currency");
		return currency.getSymbol() + currency.convert(money).toString(dp);
	}

	public Object generate(RequestContext context, Map<String, String> params, BasketLine line) {
		return generate(context, line.getLineSalePriceInc(), params);
	}

	public Object generate(RequestContext context, Map<String, String> params, Order order) {
		return generate(context, order.getTotalInc(), params);
	}

	public Object getRegex() {
		return "total";
	}

}
