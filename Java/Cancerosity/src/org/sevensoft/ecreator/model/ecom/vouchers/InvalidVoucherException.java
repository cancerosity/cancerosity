package org.sevensoft.ecreator.model.ecom.vouchers;

/**
 * @author sks 02-Mar-2006 14:54:15
 *
 */
public class InvalidVoucherException extends Exception {

	public InvalidVoucherException() {
		super();
	}

	public InvalidVoucherException(String message) {
		super(message);
	}

	public InvalidVoucherException(String message, Throwable cause) {
	}

	public InvalidVoucherException(Throwable cause) {
	}

}
