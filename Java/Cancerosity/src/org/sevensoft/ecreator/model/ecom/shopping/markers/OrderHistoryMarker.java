package org.sevensoft.ecreator.model.ecom.shopping.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.ecom.orders.OrderHistoryHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 03-Apr-2006 11:14:43
 *
 */
public class OrderHistoryMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.OrderHistory.enabled(context)) {
			return null;
		}

		if (!params.containsKey("text")) {
			params.put("text", "Order History");
		}

		return super.link(context, params, new Link(OrderHistoryHandler.class), "link_order_history");
	}

	public Object getRegex() {
		return "order_history";
	}
}