package org.sevensoft.ecreator.model.ecom.credits;

import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 30 May 2006 15:21:00
 * 
 * Who can get access to this package
 *
 */
@Table("credits_packages_access")
public class CreditPackageAccess extends EntityObject {

	private CreditPackage		creditPackage;
	private ItemType			itemType;
	private SubscriptionLevel	subscriptionLevel;

	public CreditPackageAccess(RequestContext context) {
		super(context);
	}

	public CreditPackageAccess(RequestContext context, CreditPackage creditPackage, ItemType itemType) {
		super(context);

		if (!creditPackage.getItemTypes().contains(itemType)) {

			this.creditPackage = creditPackage;
			this.itemType = itemType;

			save();
		}
	}

	public CreditPackageAccess(RequestContext context, CreditPackage creditPackage, SubscriptionLevel subscriptionLevel) {
		super(context);

		if (!creditPackage.getSubscriptionLevels().contains(subscriptionLevel)) {

			this.creditPackage = creditPackage;
			this.subscriptionLevel = subscriptionLevel;

			save();
		}
	}

	public final CreditPackage getCreditPackage() {
		return creditPackage.pop();
	}

	public final ItemType getItemType() {
		return itemType.pop();
	}

	public final SubscriptionLevel getSubscriptionLevel() {
		return subscriptionLevel.pop();
	}

}
