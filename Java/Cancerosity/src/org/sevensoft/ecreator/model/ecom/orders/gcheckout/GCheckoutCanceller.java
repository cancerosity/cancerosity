package org.sevensoft.ecreator.model.ecom.orders.gcheckout;

import java.io.IOException;

import org.jdom.JDOMException;
import org.samspade.gcheckout.GCheckoutException;
import org.samspade.gcheckout.GCheckoutResponse;
import org.samspade.gcheckout.orderapi.financial.CancelOrderRequest;
import org.samspade.gcheckout.orderapi.financial.RefundOrderRequest;
import org.sevensoft.ecreator.model.ecom.orders.Order;

/*
 * @author Stephen K Samuel samspade79@gmail.com 16 Dec 2008 18:19:19
 */
public class GCheckoutCanceller {

	/**
	 * @param	order			The order object we want to cancel
	 * @param id				merchantId as supplied by google
	 * @param password		merchant password as supplied by google
	 *
	 * @throws GCheckoutException
	 * @throws JDOMException
	 * @throws IOException
	 */
	public static void cancelOrder(Order order, String id, String password) throws IOException, JDOMException, GCheckoutException {

		RefundOrderRequest req1 = new RefundOrderRequest(id, password);
		req1.setGoogleOrderNumber(order.getGoogleOrderNumber());
		req1.setAmount(order.getTotalInc().getAmount());

		// uncomment this line out if you want to go live, otherwise it runs on sandbox
		// req.setUrl(GCheckoutRequest.URL_ORDERAPI_LIVE);

		req1.connect();

		CancelOrderRequest req2 = new CancelOrderRequest(id, password);
		req2.setGoogleOrderNumber(order.getGoogleOrderNumber());

		// uncomment this line if you want to set a cancellation reason
		// req2.setReason("Optional cancellation reason");

		// uncomment this line if you want to set a cancellation comment
		// req2.setComment("Optional cancellation comment");

		GCheckoutResponse resp = req2.connect();

		// uncomment this line if you want to assign the serial number that google returns into a log or something like that
		// String serialNumber = resp.getSerialNumber();
	}
}
