package org.sevensoft.ecreator.model.ecom.orders.markers;

import java.util.Map;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderMarker;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 29 Oct 2006 08:53:09
 *
 */
public class OrderTotalMarker extends MarkerHelper implements IOrderMarker {

	public Object generate(RequestContext context, Map<String, String> params, Order order) {

		Money money;

		if (params.containsKey("inc")) {
			money = order.getTotalInc();
		}

		else if (params.containsKey("vat")) {
			money = order.getTotalVat();
		}

		else {

			money = order.getTotalEx();
		}

		return super.string(context, params, money);
	}

	@Override
	public String getDescription() {
		return "Sale total for the order";
	}

	public Object getRegex() {
		return "order_total";
	}

}
