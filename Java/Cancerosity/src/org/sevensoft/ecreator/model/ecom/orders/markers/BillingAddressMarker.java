package org.sevensoft.ecreator.model.ecom.orders.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderMarker;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 29 Oct 2006 08:53:09
 *
 */
public class BillingAddressMarker implements IOrderMarker {

	public Object generate(RequestContext context, Map<String, String> params, Order order) {

		Address address = order.getBillingAddress();
		if (address == null) {
			return null;
		}

		String sep = params.get("sep");
		if (sep == null) {
			sep = "\n";
		}

		return address.getLabel(sep, false);
	}

	public Object getRegex() {
		return "order_billing_label";
	}

}
