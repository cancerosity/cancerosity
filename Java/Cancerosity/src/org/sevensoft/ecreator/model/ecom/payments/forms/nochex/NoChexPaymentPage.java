package org.sevensoft.ecreator.model.ecom.payments.forms.nochex;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.sevensoft.ecreator.iface.callbacks.NoChexCallbackHandler;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.ecom.payments.results.Callback;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 07-Mar-2006 11:37:35
 *
 */
public class NoChexPaymentPage extends Form {

	public NoChexPaymentPage(RequestContext context) {
		super(context);
	}

	@Override
	public String getMethod() {
		return "POST";
	}

	@Override
	public Map<String, String> getParams(Payable payable) {

		PaymentSettings settings = PaymentSettings.getInstance(context);
		FormSession session = new FormSession(context, payable);
		Config config = Config.getInstance(context);

		Map<String, String> params = new HashMap<String, String>();
		params.put("merchant_id", settings.getNoChexMerchantId());
		params.put("amount", payable.getPaymentAmount().toEditString());
		params.put("description", payable.getPaymentDescription());

		params.put("responder_url", config.getUrl() + "/" + new Link(NoChexCallbackHandler.class));

		// use order id for our session id
		params.put("order_id", session.getIdString());

		return params;
	}

	@Override
	public String getPaymentUrl() {
		return "https://secure.nochex.com";
	}

	@Override
	public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
	}

	@Override
	public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
        log("[NoChex] Server callback params=" + params);

		new Callback(context, PaymentType.NoChex, params);

		//		FormSession session = EntityObject.getInstance(context, FormSession.class, params.get("OrderID"));
		//		if (session == null) {
		//			return null;
		//		}
		//
		//		Payment payment = getPayment(session.getAccount(), params, ipAddress);
		//		if (payment == null) {
		//			return null;
		//		}

		//		session.callback(params, ipAddress, payment);
		return null;
	}
}
