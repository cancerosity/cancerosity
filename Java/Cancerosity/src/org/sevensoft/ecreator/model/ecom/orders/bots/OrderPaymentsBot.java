package org.sevensoft.ecreator.model.ecom.orders.bots;

import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderSearcher;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 11 Apr 2007 15:26:46
 * 
 * Cycles through all orders waiting to be billed and charges them
 *
 */
public class OrderPaymentsBot implements Runnable {

	private static final Logger	logger	= Logger.getLogger("cron");
	private final RequestContext	context;

	public OrderPaymentsBot(RequestContext context) {
		this.context = context;
	}

	public void run() {

		// get all orders that are ready to be billed and are card terminals
		OrderSearcher searcher = new OrderSearcher(context);
		searcher.setReinvoiceDate(new Date());

		List<Order> orders = searcher.execute();
		logger.fine("[OrderPaymentsBot] orders=" + orders);

		for (Order order : orders) {

			if (order.hasCard()) {

				try {

					order.transactCard();

				} catch (Exception e) {

					e.printStackTrace();
				}

			}
		}

	}
}
