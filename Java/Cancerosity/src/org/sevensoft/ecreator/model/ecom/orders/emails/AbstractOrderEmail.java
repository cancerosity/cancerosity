package org.sevensoft.ecreator.model.ecom.orders.emails;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.ecreator.model.ecom.shopping.ShoppingSettings;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 02-Dec-2005 09:58:21
 * 
 */
public class AbstractOrderEmail extends Email {

	protected Order			order;
	protected Config			config;
	protected Item			customer;
	protected Company			company;
	protected ShoppingSettings	shoppingSettings;
	protected RequestContext	context;
	protected OrderSettings		orderSettings;

	protected AbstractOrderEmail(RequestContext context, Order order) {

		this.context = context;
		this.order = order;
		this.customer = order.getAccount();
		this.config = Config.getInstance(context);
		this.company = Company.getInstance(context);
		this.orderSettings = OrderSettings.getInstance(context);
		this.shoppingSettings = ShoppingSettings.getInstance(context);
	}

	protected void items(StringBuilder sb) {

		for (OrderLine item : order.getLines()) {

			sb.append(" " + item.getQty() + " of " + item.getDescription());
			if (item.hasOptionsDetails())
				sb.append("\n  " + item.getOptionsDetails());
			sb.append("\n     @ \243" + item.getUnitSellEx() + " = \243" + item.getLineSellEx() + "\n");
		}

	}

}
