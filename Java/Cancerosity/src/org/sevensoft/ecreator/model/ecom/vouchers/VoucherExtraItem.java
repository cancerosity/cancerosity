package org.sevensoft.ecreator.model.ecom.vouchers;

import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.items.Item;

/**
 * User: Dmitry Lebedev
 * Date: 26.08.2009
 * Time: 20:36:09
 */
@Table("vouchers_extra_items")
@SuppressWarnings("unused")
public class VoucherExtraItem extends EntityObject {

    private Voucher voucher;
    private Item extraItem;

    public VoucherExtraItem(RequestContext context) {
        super(context);
    }

    public VoucherExtraItem(RequestContext context, Voucher voucher, Item extraItem) {
        super(context);

        if (!voucher.getExtraItems().contains(extraItem)) {
            this.voucher = voucher;
            this.extraItem = extraItem;

            save();
        }
    }
}
