package org.sevensoft.ecreator.model.ecom.shopping.renderer;

import org.sevensoft.ecreator.iface.EcreatorRenderer;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.BasketHandler;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.ecom.delivery.DeliverySettings;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.ShoppingSettings;
import org.sevensoft.ecreator.model.system.config.Captions;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.form.FormTag;

/**
 * @author sks 25 Apr 2006 16:24:40
 *
 */
public class BasketRenderer extends EcreatorRenderer {

	private Basket			basket;
	private boolean			showFreeDeliveryRequirement;
	private String			basketContent;
	private ShoppingSettings	shoppingSettings;
	private DeliverySettings	deliverySettings;
	private final Captions		captions;

	public BasketRenderer(RequestContext context, Basket basket) {
		super(context);

		this.basket = basket;
		this.shoppingSettings = ShoppingSettings.getInstance(context);
		this.captions = Captions.getInstance(context);
		this.basketContent = shoppingSettings.getBasketContent();

		this.deliverySettings = DeliverySettings.getInstance(context);
		this.showFreeDeliveryRequirement = deliverySettings.isShowFreeDeliveryRequirement();
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(new MessagesTag(context));

		if (basketContent != null) {
			sb.append(shoppingSettings.getBasketContent());
			sb.append("<br/><br/>");
		}

		if (showFreeDeliveryRequirement) {

			String freeDeliveryMessage = basket.getFreeDeliveryReqMessage();
			if (freeDeliveryMessage != null)
				sb.append("<div class='free_shipping_message'>" + freeDeliveryMessage + "</div>");
		}

		if (basket.isEmpty()) {

			sb.append(captions.getEmptyBasketCaption());

		} else {

			sb.append(new FormTag(BasketHandler.class, "update", "POST"));

			MarkupRenderer r = new MarkupRenderer(context, shoppingSettings.getBasketMarkup());
			r.setBody(basket);
			sb.append(r);

			sb.append("</form>");
		}

		return sb.toString();
	}
}