package org.sevensoft.ecreator.model.ecom.credits;

import java.util.List;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 11 Jul 2006 21:28:31
 *
 */
@Table("credits_packages")
public class CreditPackage extends EntityObject implements Comparable<CreditPackage> {

	public static List<CreditPackage> get(RequestContext context) {

		Query q = new Query(context, "select * from # order by credits");
		q.setTable(CreditPackage.class);
		return q.execute(CreditPackage.class);
	}

	/**
	 * Returns a package that will work for this number of bids
	 */
	public static CreditPackage getOnCashBids(RequestContext context, int qty) {
		Query q = new Query(context, "select * from # where onCashBids>0 and onCashBids<=? order by onCashBids desc");
		q.setTable(CreditPackage.class);
		q.setParameter(qty);
		return q.get(CreditPackage.class);
	}

	public static List<CreditPackage> getOnSpend(RequestContext context, final Money amount) {

		List<CreditPackage> packages = get(context);
		CollectionsUtil.filter(packages, new Predicate<CreditPackage>() {

			public boolean accept(CreditPackage e) {
				return e.isFree() && e.getOnSpend().isLessThanOrEquals(amount);
			}
		});
		return packages;
	}

	/**
	 * Returns a list of packages that apply when a user has registered
	 */
	public static List<CreditPackage> getRegistration(RequestContext context) {

		List<CreditPackage> packages = get(context);
		CollectionsUtil.filter(packages, new Predicate<CreditPackage>() {

			public boolean accept(CreditPackage e) {
				return e.isFree() && e.isOnRegistration();
			}
		});
		return packages;
	}

	/**
	 *  Cost to the customer of buying these credits
	 */
	private Money	fee;

	/**
	 *  Number of credits in this package
	 */
	private int		credits;

	/**
	 * Can only be applied once per customer
	 */
	private boolean	introductory;

	/**
	 * Tag to true if this is for bonus credits which are different tally to normal credits
	 */
	private boolean	bonus;

	/**
	 * Apply these credits on registration, ie one time only
	 */
	private boolean	onRegistration;

	/**
	 * On spending this amount of money the customer gets this package
	 */
	private Money	onSpend;

	/**
	 * Apply this credit package for this multiple of bids
	 */
	private int		onCashBids;

	private String	name;

	public CreditPackage(RequestContext context) {
		super(context);
	}

	public CreditPackage(RequestContext context, boolean b) {
		super(context);

		this.credits = 10;
		this.fee = new Money(10);

		save();
	}

	public void addAccess(ItemType addItemType) {
		new CreditPackageAccess(context, this, addItemType);
	}

	public void addAccess(SubscriptionLevel addSubscriptionLevel) {
		new CreditPackageAccess(context, this, addSubscriptionLevel);
	}

	public int compareTo(CreditPackage other) {
		return fee.compareTo(other.fee);
	}

	public int getCredits() {
		return credits;
	}

	public boolean hasName() {
		return name != null;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return credits + " credits for \243" + fee;
	}

	public Money getFee() {
		return fee;
	}

	public List<ItemType> getItemTypes() {
		Query q = new Query(context, "select it.* from # it join # cpa on it.id=cpa.itemType where cpa.creditPackage=?");
		q.setTable(ItemType.class);
		q.setTable(CreditPackageAccess.class);
		q.setParameter(this);
		return q.execute(ItemType.class);
	}

	public int getOnCashBids() {
		return onCashBids;
	}

	public Money getOnSpend() {
		return onSpend;
	}

	public List<SubscriptionLevel> getSubscriptionLevels() {
		Query q = new Query(context, "select sl.* from # sl join # cpa on sl.id=cpa.subscriptionLevel where cpa.creditPackage=?");
		q.setTable(SubscriptionLevel.class);
		q.setTable(CreditPackageAccess.class);
		q.setParameter(this);
		return q.execute(SubscriptionLevel.class);
	}

	public boolean isBonus() {
		return bonus;
	}

	public boolean isFree() {
		return fee.isZero();
	}

	public boolean isIntroductory() {
		return introductory;
	}

	public boolean isOnRegistration() {
		return onRegistration && isFree();
	}

	public void removeAccess(ItemType itemType) {
		SimpleQuery.delete(context, CreditPackageAccess.class, "creditPackage", this, "itemType", itemType);
	}

	public void removeAccess(SubscriptionLevel subscriptionLevel) {
		SimpleQuery.delete(context, CreditPackageAccess.class, "creditPackage", this, "subscriptionLevel", subscriptionLevel);
	}

	public void setBonus(boolean bonus) {
		this.bonus = bonus;
	}

	public void setCredits(int credits) {
		this.credits = credits;
	}

	public void setFee(Money fee) {
		this.fee = fee;
	}

	public void setIntroductory(boolean introductory) {
		this.introductory = introductory;
	}

	public void setOnCashBids(int i) {
		this.onCashBids = i;
	}

	public void setOnRegistration(boolean onRegistration) {
		this.onRegistration = onRegistration;
	}

	public void setOnSpend(Money onSpend) {
		this.onSpend = onSpend;
	}

	public final void setName(String name) {
		this.name = name;
	}

}
