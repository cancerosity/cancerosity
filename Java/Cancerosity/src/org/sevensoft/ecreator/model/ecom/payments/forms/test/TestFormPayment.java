package org.sevensoft.ecreator.model.ecom.payments.forms.test;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.ecom.payments.TestPaymentHandler;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 18 Jan 2007 10:03:10
 *
 */
public class TestFormPayment extends Form {

	public TestFormPayment(RequestContext context) {
		super(context);
	}

	@Override
	public String getMethod() {
		return "POST";
	}

	@Override
	public Map<String, String> getParams(Payable payable) throws IOException, FormException {

		FormSession session = new FormSession(context, payable);

		Map<String, String> params = new LinkedHashMap();
		params.put("transactionId", session.getTransactionId());
		params.put("url", payable.getPaymentSuccessUrl(PaymentType.Test));

		return params;
	}

	@Override
	public String getPaymentUrl() {
		return Config.getInstance(context).getUrl() + "/" + new Link(TestPaymentHandler.class);
	}

	/**
	 * always assume payment is fine because this is just for testing
	 */
	@Override
	public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {

		logger.config("[TestFormPayment] inline callback, params=" + params);

		String transactionId = params.get("transactionId");

		FormSession session = FormSession.get(context, transactionId);
		if (session == null) {
			logger.config("[TestFormPayment] No payment session found");
			return;
		}

		session.callback(params, ipAddress, null);
		session.delete();
	}

	@Override
	public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
		return null;
	}

}
