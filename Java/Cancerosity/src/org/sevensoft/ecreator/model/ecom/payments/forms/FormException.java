package org.sevensoft.ecreator.model.ecom.payments.forms;

/**
 * @author sks 05-Mar-2006 21:55:24
 *
 */
public class FormException extends Exception {

	public FormException() {
	}

	public FormException(String message) {
	}

	public FormException(String message, Throwable cause) {
	}

	public FormException(Throwable cause) {
	}

}
