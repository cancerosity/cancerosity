package org.sevensoft.ecreator.model.ecom.orders.parcels.trackers;

/**
 * @author sks 20-Dec-2005 18:54:25
 * 
 */
public class InterlinkTracker extends Tracker {

	public String getTrackingUrl(String consignment) {

		return "http://www.interlinkexpress.com/tracking/trackingSearch.do?page=1&search.searchType=1&search.consignmentNumber=" + consignment;
	}

}
