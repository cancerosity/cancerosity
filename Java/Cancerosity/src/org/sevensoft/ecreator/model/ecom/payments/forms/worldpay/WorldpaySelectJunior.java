package org.sevensoft.ecreator.model.ecom.payments.forms.worldpay;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.callbacks.WorldpayCallbackHandler;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.ecom.payments.results.AvsCheck.AvsResult;
import org.sevensoft.ecreator.model.ecom.payments.results.Callback;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author sks 15-Sep-2004 21:04:52
 */
public class WorldpaySelectJunior extends Form {

    public WorldpaySelectJunior(RequestContext context) {
        super(context);
    }

    private AvsResult getAvsResult(Map<String, String> params) {

        String avs = params.get("AVS");
        if (avs == null) {
            return null;
        }

        if (avs.length() == 4 && avs.substring(2, 3).equals("2")) {
            return AvsResult.Match;
        }

        if (avs.length() == 4 && avs.substring(2, 3).equals("1")) {
            return AvsResult.NotSupported;
        }

        if (avs.length() == 4 && avs.substring(2, 3).equals("0")) {
            return AvsResult.NotSupported;
        }

        if (avs.length() == 4 && avs.substring(2, 3).equals("4")) {
            return AvsResult.Fail;
        }

        return null;
    }

    @Override
    public String getMethod() {
        return "POST";
    }

    @Override
    public Map<String, String> getParams(Payable payable) {

        PaymentSettings ps = PaymentSettings.getInstance(context);
        FormSession session = new FormSession(context, payable);

        Map<String, String> params = new HashMap<String, String>();

        params.put("instId", ps.getWorldPayInstallationId());
        if (ps.hasWorldpayAccountId()) {
            params.put("accId1", ps.getWorldpayAccountId());
        }
        params.put("currency", "GBP");
        params.put("country", "GB");

        // test mode, 100 for random test, 101 for no, 102 for yes, 0 for live
        if (ps.isWorldPayLive()) {
            params.put("testMode", "0");
        } else {
            params.put("testMode", "100");
        }

        params.put("name", payable.getPayableAccount().getName());
        if (payable.getPayableAccount().hasEmail()) {
            params.put("email", payable.getPayableAccount().getEmail());
        }

        //
        if (ps.hasWorldpayAuthMode()) {
            params.put("authMode", ps.getWorldpayAuthMode());
        }

        // amount
        params.put("amount", payable.getPaymentAmount().toEditString());

        params.put("desc", payable.getPaymentDescription());

        // cart id which we will use for our session id
        params.put("cartId", session.getIdString());

        // customer details
        params.put("name", payable.getPayableAccount().getName());

        if (payable instanceof Order) {

            Order order = (Order) payable;
            if (order.hasBillingAddress()) {
                Address billingAddress = order.getBillingAddress();
                params.put("address", billingAddress.getAddress("&#10"));
                params.put("postcode", billingAddress.getPostcode());
            }
        }

        // callback
        params.put("MC_callback", Config.getInstance(context).getUrl() + "/" + new Link(WorldpayCallbackHandler.class));

        return params;
    }

    private Payment getPayment(Item member, Map<String, String> params) {

        String transId = params.get("transId");
        String transStatus = params.get("transStatus");
        String authAmount = params.get("authAmount").replace("&#163;", "");
        String rawAuthCode = params.get("rawAuthCode");
        String ipAddress = params.get("ipAddress");
        String cardType = params.get("cardType");
        String name = params.get("name");

        String callbackPW = params.get("callbackPW");

        // check for existing payment Id
        if (Payment.exists(context, transId, PaymentType.WorldPaySelectJunior)) {
            throw new RuntimeException("Duplicated callback: " + transId);
        }

        if (!"Y".equals(transStatus)) {
            return null;
        }

        final PaymentSettings ps = PaymentSettings.getInstance(context);
        if (ps.hasWorldPayCallbackPassword() && !ps.getWorldPayCallbackPassword().equalsIgnoreCase(callbackPW)) {
            logger.fine("[WorldpaySelectJunior] callback passwords do not match, " + callbackPW + "!=" + ps.getWorldPayCallbackPassword());
            throw new RuntimeException("Callback password does not match!");
        }

        Payment payment = new Payment(context, member, new Money(authAmount), PaymentType.WorldPaySelectJunior);
        payment.setProcessorTransactionId(transId);
        payment.setAuthCode(rawAuthCode);
        payment.setDetails(name + " " + cardType);
        payment.setIpAddress(ipAddress);

        payment.save();

        return payment;
    }

    /*
      * @see org.sevensoft.emerchant.model.sales.payments.gateways.Gateway#getGatewayUrl()
      */
    @Override
    public String getPaymentUrl() {
        return "https://select.worldpay.com/wcc/purchase";
    }

    @Override
    public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
    }

    @Override
    public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
        log("[WorldpaySelectJunior] server callback params=" + params);

        new Callback(context, PaymentType.WorldPaySelectJunior, params);

        FormSession session = EntityObject.getInstance(context, FormSession.class, params.get("cartId"));
        if (session == null) {
            return null;
        }

        Payment payment = getPayment(session.getAccount(), params);
        if (payment == null) {
            return null;
        }

        String url = session.callback(params, ipAddress, payment);
        return url;
    }

}