package org.sevensoft.ecreator.model.ecom.orders.parcels;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.parcels.trackers.Tracker;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 29-Sep-2005 18:44:09
 * 
 */
@Table("orders_parcels")
public class Parcel extends EntityObject {

	public enum Status {
		Current, Deleted;
	}

	private String	consignmentNumber;
	private Courier	courier;
	private DateTime	date;
	private Order	ord;
	private Status	status;

	public Parcel(RequestContext context) {
		super(context);
	}

	public Parcel(RequestContext context, Order ord, Courier courier, String cn) {
		super(context);

		this.ord = ord;
		this.courier = courier;
		this.consignmentNumber = cn;
		this.status = Status.Current;
		this.date = new DateTime();

		save();
	}

	@Override
	public synchronized boolean delete() {
		status = Status.Deleted;
		save();

		return true;
	}

	public String getConsignmentNumber() {
		return consignmentNumber;
	}

	public Courier getCourier() {
		return courier == null ? Courier.CityLink : courier;
	}

	public DateTime getDate() {
		return date == null ? new DateTime(0) : date;
	}

	public Order getOrder() {
		return ord.pop();
	}

	public String getTrackingUrl() {

		Tracker tracker = getCourier().getTracker();
		if (tracker == null)
			return null;

		return tracker.getTrackingUrl(consignmentNumber);
	}

	public boolean isCurrent() {
		return status == Status.Current;
	}

	public boolean isDeleted() {
		return status == Status.Deleted;
	}
}
