package org.sevensoft.ecreator.model.ecom.orders.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderLineMarker;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * User: Tanya
 * Date: 02.07.2010
 */
public class LineOptionsDescriptionMarker extends MarkerHelper implements IOrderLineMarker {

	public Object generate(RequestContext context, Map<String, String> params, OrderLine line) {
		return super.string(context, params, line.getOptionsDetails());
	}

	@Override
	public String getDescription() {
		return "Options chosen for the specific item on that line of the order. Use with prefix=&lt/br&gt";
	}

	public Object getRegex() {
		return "order_line_options";
	}

}