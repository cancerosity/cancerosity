package org.sevensoft.ecreator.model.ecom.orders.parcels.trackers;

/**
 * @author sks 20-Dec-2005 18:50:27
 * 
 */
public class TntTracker extends Tracker {

	public String getTrackingUrl(String consignment) {
		return "http://cgi.tnt.co.uk/trackntrace/conenquiry.asp?ACTION=TRACK&TYPE=C&QUERY=" + consignment;
	}

}
