package org.sevensoft.ecreator.model.ecom.addresses.exceptions;


/**
 * @author sks
 * 17-Dec-2004 13:17:05
 */
public class CountryException extends Exception {

	/**
	 * 
	 */
	public CountryException() {
		super();
	}

	/**
	 * @param message
	 */
	public CountryException(String message) {
		super(message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public CountryException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param cause
	 */
	public CountryException(Throwable cause) {
		super(cause);
	}

}
