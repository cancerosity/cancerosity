package org.sevensoft.ecreator.model.ecom.delivery.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderMarker;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Jun 2006 11:48:11
 * Returns just the charge of delivery
 * 
 */
public class DeliveryDetailsMarker extends MarkerHelper implements IBasketMarker, IOrderMarker {

	public Object generate(RequestContext context, Map<String, String> params, Basket basket) {

		if (!Module.Delivery.enabled(context)) {
			return null;
		}

		if (!basket.hasDeliveryOption()) {
			return null;
		}

		return basket.getDeliveryName();
	}

	public Object generate(RequestContext context, Map<String, String> params, Order order) {

		if (!Module.Delivery.enabled(context)) {
			return null;
		}

		if (!order.hasDeliveryDetails()) {
			return null;
		}

		return super.string(context, params, order.getDeliveryDetails());
	}

	public Object getRegex() {
		return "delivery_details";
	}
}
