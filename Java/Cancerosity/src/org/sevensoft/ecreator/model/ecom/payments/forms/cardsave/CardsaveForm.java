package org.sevensoft.ecreator.model.ecom.payments.forms.cardsave;

import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.results.Callback;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.iface.callbacks.CardsaveCallbackHandler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.skint.Money;

import java.util.Map;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Iterator;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * User: Tanya
 * Date: 25.08.2011
 */
public class CardsaveForm extends Form {
    private static String LiveTestUrl = "https://mms.cardsaveonlinepayments.com/Pages/PublicPages/PaymentForm.aspx";

    private enum TransactionType {
        SALE,
        REFUND,
        PREAUTH
    }

    public CardsaveForm(RequestContext context) {
        super(context);
    }

    public String getMethod() {
        return "POST";
    }

    public Map<String, String> getParams(Payable payable) throws IOException, FormException {
        Order order = (Order) payable;
        PaymentSettings paymentSettings = PaymentSettings.getInstance(context);

        FormSession session = new FormSession(context, payable);
        Map<String, String> params = new LinkedHashMap();

        params.put("PreSharedKey", paymentSettings.getCardsavePreSharedKey());
        params.put("MerchantID", paymentSettings.getCardsaveMerchantId());
        params.put("Password", paymentSettings.getCardsaveMerchantPw());
        params.put("Amount", String.valueOf(order.getTotalInc().getAmount()));
        params.put("CurrencyCode", String.valueOf(Company.getInstance(context).getCountry().getIsoNumber3()));
        params.put("OrderID", order.getIdString());
        params.put("TransactionType", String.valueOf(TransactionType.SALE));
        params.put("TransactionDateTime", new DateTime().toFullDateTimeString());
        params.put("CallbackURL", Config.getInstance(context).getUrl() + "/" + new Link(CardsaveCallbackHandler.class));
        params.put("OrderDescription", order.getDescription());
        params.put("CustomerName", order.getAccount().getName());
        params.put("Address1", order.getBillingAddress().getAddressLine1() != null ? order.getBillingAddress().getAddressLine1() : "");
        params.put("Address2", order.getBillingAddress().getAddressLine2() != null ? order.getBillingAddress().getAddressLine2() : "");
        params.put("Address3", order.getBillingAddress().getAddressLine3() != null ? order.getBillingAddress().getAddressLine3() : "");
        params.put("Address4", "");
        params.put("City", order.getBillingAddress().getTown());
        params.put("State", order.getBillingAddress().getState() != null ? order.getBillingAddress().getState() : "");
        params.put("PostCode", order.getBillingAddress().getPostcode());
        params.put("CountryCode", String.valueOf(order.getBillingAddress().getCountry().getIsoNumber3()));
        params.put("CV2Mandatory", String.valueOf(Boolean.TRUE));
        params.put("Address1Mandatory", String.valueOf(Boolean.TRUE));
        params.put("CityMandatory", String.valueOf(Boolean.TRUE));
        params.put("PostCodeMandatory", String.valueOf(Boolean.TRUE));
        params.put("StateMandatory", String.valueOf(Boolean.FALSE));
        params.put("CountryMandatory", String.valueOf(Boolean.TRUE));
        params.put("ResultDeliveryMethod", "POST");
        params.put("ServerResultURL", ""/* payable.getPaymentSuccessUrl(PaymentType.CardsaveRedirect)*/);
        params.put("PaymentFormDisplaysResult", String.valueOf(Boolean.FALSE));
        params.put("ServerResultURLCookieVariables", "");
        params.put("ServerResultURLFormVariables", "");
        params.put("ServerResultURLQueryStringVariables", "");


        return encrypt(params, paymentSettings);
    }

    private Map<String, String> encrypt(Map<String, String> params, PaymentSettings paymentSettings) {

        Map<String, String> map = new HashMap<String, String>();


        try {
            String hashDigest = encryptParams(params);
            params.put("HashDigest", hashDigest);
        } catch (NoSuchAlgorithmException e) {
            logger.severe(CardsaveForm.class.getName() + e);
        }
        params.remove("PreSharedKey");
        params.remove("Password");
        log(params.toString());
        return params;
    }

    private String encryptParams(Map<String, String> params) throws NoSuchAlgorithmException {

        StringBuilder sb = new StringBuilder();
        Iterator<Map.Entry<String, String>> iter = params.entrySet().iterator();
        while (iter.hasNext()) {

            Map.Entry<String, String> entry = iter.next();

            sb.append(entry.getKey());
            sb.append("=");
            sb.append(entry.getValue());

            if (iter.hasNext()) {
                sb.append("&");
            }
        }
        MessageDigest md = MessageDigest.getInstance("MD5");

        md.reset();


        byte[] hash = md.digest(sb.toString().getBytes());

        StringBuffer hashDigest = new StringBuffer();
        for (int i = 0; i < hash.length; ++i) {
            hashDigest.append(Integer.toHexString((hash[i] & 0xFF) | 0x100)./*toUpperCase().*/substring(1, 3));
        }

        return hashDigest.toString();
    }

    public String getPaymentUrl() {
        return LiveTestUrl;
    }

    public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
    }

    public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
        log("[CardsaveForm] Server callback params=" + params);
        logger.severe("[CardsaveForm] Server callback params=" + params);
        if (params.isEmpty()) {
            context.addError("No returns from Cardsave");
            return null;
        }

        new Callback(context, PaymentType.CardsaveRedirect, params);

        if (!"0".equals(params.get("StatusCode"))) {
            log("[CardsaveForm] StatusCode=" + params.get("StatusCode"));
            context.addError("StatusCode=" + params.get("StatusCode"));
            return null;
        }

        FormSession session = FormSession.getByOrder(context, Integer.valueOf(params.get("OrderID")));

        if (session == null) {
            log("[CardsaveForm] FormSessing is null by orderID = " + params.get("OrderID"));
            return null;
        }

        Order order = session.getOrder();
        order.setCardsaveCrossReference(params.get("CrossReference"));
        order.save();

        Money amount = new Money(Integer.parseInt(params.get("Amount")));
        Payment payment = new Payment(context, session.getAccount(), amount, PaymentType.CardsaveRedirect);

        session.callback(params, ipAddress, payment);

        Basket basket = Basket.getByOrder(context, order.getIdString());

        return basket.getIdString();

    }
}
