package org.sevensoft.ecreator.model.ecom.payments;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.QueryBuilder;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 1 May 2007 21:45:30
 *
 */
public class PaymentSearcher {

	private final RequestContext	context;
	private Money			amountFrom, amountTo;
	private Date			dateFrom, dateTo;
	private PaymentType		paymentType;
	private String			accountName, accountEmail;
	private String			paymentId;
	private int				start;
	private int				limit;

	public PaymentSearcher(RequestContext context) {
		this.context = context;
	}

	public List<Payment> execute() {

		QueryBuilder b = new QueryBuilder(context);
		b.select("p.*");
		b.from("# p", Payment.class);
		b.from("join # a on p.account=a.id", Item.class);

		if (paymentId != null) {
			b.clause("p.id like ?", "%" + paymentId + "%");
		}

		if (accountName != null) {
			for (String s : accountName.split("\\s")) {
				b.clause("a.name like ?", "%" + s.trim() + "%");
			}
		}

		if (accountEmail != null) {
			b.clause("a.email like ?", "%" + accountEmail.trim() + "%");
		}

		if (amountFrom != null) {
			b.clause("p.amount>=?", amountFrom);
		}

		if (amountTo != null) {
			b.clause("p.amount<=?", amountTo);
		}

		if (dateFrom != null) {
			b.clause("p.date>=?", dateFrom);
		}

		if (dateTo != null) {
			b.clause("p.date<=?", dateTo);
		}

		if (transactionId != null) {
			b.clause("p.processorTransactionId like ?", "%" + transactionId + "%");
		}

		if (paymentType != null) {
			b.clause("p.paymentType=?", paymentType);
		}

		b.order("p.id");

		if (limit < 1 || limit > 200) {
			limit = 200;
		}

		return b.execute(Payment.class, start, limit);
	}

	public final String getAccountEmail() {
		return accountEmail;
	}

	public final String getAccountName() {
		return accountName;
	}

	public final Money getAmountFrom() {
		return amountFrom;
	}

	public final Money getAmountTo() {
		return amountTo;
	}

	public final RequestContext getContext() {
		return context;
	}

	public final Date getDateFrom() {
		return dateFrom;
	}

	public final Date getDateTo() {
		return dateTo;
	}

	public final int getLimit() {
		return limit;
	}

	public final String getPaymentId() {
		return paymentId;
	}

	public final PaymentType getPaymentType() {
		return paymentType;
	}

	public final int getStart() {
		return start;
	}

	public final void setAccountEmail(String accountEmail) {
		this.accountEmail = accountEmail;
	}
	private String	transactionId;

	public final String getTransactionId() {
		return transactionId;
	}

	public final void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public final void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public final void setAmountFrom(Money amountFrom) {
		this.amountFrom = amountFrom;
	}

	public final void setAmountTo(Money amountTo) {
		this.amountTo = amountTo;
	}

	public final void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public final void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public final void setLimit(int limit) {
		this.limit = limit;
	}

	public final void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public final void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public final void setStart(int start) {
		this.start = start;
	}

}
