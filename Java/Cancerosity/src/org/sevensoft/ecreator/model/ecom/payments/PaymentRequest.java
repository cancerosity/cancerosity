package org.sevensoft.ecreator.model.ecom.payments;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 22-Dec-2005 15:44:46
 * 
 */
@Table("payments_requests")
public class PaymentRequest extends EntityObject {

	private Money		amount;
	private DateTime		date;
	private Order		orderId;
	private PaymentType	paymentType;

	public PaymentRequest(RequestContext context) {
		super(context);
	}

	public PaymentRequest(RequestContext context, Order orderId, PaymentType paymentType) {
		super(context);
		this.date = new DateTime();
		this.orderId = orderId;
		this.paymentType = paymentType;
		this.amount = orderId.getOutstandingAmount();
	}

	public Money getAmount() {
		return amount;
	}

	public DateTime getDate() {
		return date;
	}

	public Order getOrderId() {
		return orderId;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setAmount(Money amount) {
		this.amount = amount;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public void setOrderId(Order orderId) {
		this.orderId = orderId;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

}
