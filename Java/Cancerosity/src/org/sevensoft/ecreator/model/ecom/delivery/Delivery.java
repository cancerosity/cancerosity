package org.sevensoft.ecreator.model.ecom.delivery;

import java.util.HashMap;
import java.util.Map;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 21 Jan 2007 16:39:28
 *
 */
@Table("delivery_details")
public class Delivery extends EntityObject {

	public static Delivery get(RequestContext context, Item item) {
		Delivery d = SimpleQuery.get(context, Delivery.class, "item", item);
		return d == null ? new Delivery(context, item) : d;
	}

	private Item	item;
	private Money	deliverySurcharge;

	private Delivery(RequestContext context) {
		super(context);
	}

	private Delivery(RequestContext context, Item item) {
		super(context);

		this.item = item;
		save();
	}

	public Money getDeliveryRate(DeliveryOption option) {
		Query q = new Query(context, "select chargeEx from # where item=? and deliveryOption=?");
		q.setTable(DeliveryRate.class);
		q.setParameter(item);
		q.setParameter(option);
		return q.getMoney();
	}

	public Map<DeliveryOption, Money> getDeliveryRatesMap() {

		Map<DeliveryOption, Money> map = new HashMap();
		for (DeliveryOption option : DeliveryOption.get(context)) {
			map.put(option, getDeliveryRate(option));
		}
		return map;
	}

	public final Money getDeliverySurcharge() {
		return deliverySurcharge;
	}

	public final Item getItem() {
		return item.pop();
	}

	public boolean hasDeliverySurcharge() {
		return deliverySurcharge != null;
	}

	private void removeDeliveryRate(DeliveryOption option) {
		SimpleQuery.delete(context, DeliveryRate.class, "deliveryOption", option, "item", item);
	}

	public void setDeliveryRate(DeliveryOption option, Money money) {
		removeDeliveryRate(option);
		if (money != null) {
			new DeliveryRate(context, option, item, money);
		}
	}

	public final void setDeliverySurcharge(Money deliverySurcharge) {
		this.deliverySurcharge = deliverySurcharge;
	}
}
