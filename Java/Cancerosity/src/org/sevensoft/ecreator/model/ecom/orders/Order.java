package org.sevensoft.ecreator.model.ecom.orders;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.samdate.Period;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.commons.superstrings.formatters.NumberFormatter;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.CheckoutHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.OrderStatusHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.BasketHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.OrderHandler;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionRate;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.ecreator.model.attributes.support.AttributeMessageSupport;
import org.sevensoft.ecreator.model.crm.forms.submission.Submission;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.branches.Branch;
import org.sevensoft.ecreator.model.ecom.delivery.Deliverable;
import org.sevensoft.ecreator.model.ecom.delivery.DeliverableItem;
import org.sevensoft.ecreator.model.ecom.delivery.DeliveryOption;
import org.sevensoft.ecreator.model.ecom.orders.emails.InvoiceEmail;
import org.sevensoft.ecreator.model.ecom.orders.emails.OrderAcknowledgementEmail;
import org.sevensoft.ecreator.model.ecom.orders.emails.OrderCompletionEmail;
import org.sevensoft.ecreator.model.ecom.orders.parcels.Courier;
import org.sevensoft.ecreator.model.ecom.orders.parcels.Parcel;
import org.sevensoft.ecreator.model.ecom.orders.gcheckout.GCheckoutCanceller;
import org.sevensoft.ecreator.model.ecom.payments.*;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.*;
import org.sevensoft.ecreator.model.ecom.payments.forms.able2buy.CreditGroup;
import org.sevensoft.ecreator.model.ecom.payments.processors.Processor;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorException;
import org.sevensoft.ecreator.model.ecom.payments.results.AvsCheck;
import org.sevensoft.ecreator.model.ecom.payments.results.AvsCheck.AvsResult;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.ecom.shopping.ShoppingSettings;
import org.sevensoft.ecreator.model.ecom.vouchers.Voucher;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.ecreator.model.items.listings.ListingRate;
import org.sevensoft.ecreator.model.marketing.affiliates.Affiliation;
import org.sevensoft.ecreator.model.misc.geoip.GeoIp;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.messages.SystemMessage;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.model.bespoke.ipoints.IPointsSettings;
import org.sevensoft.ecreator.model.bespoke.ipoints.IPointsService;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.QueryBuilder;
import org.sevensoft.jeezy.db.Row;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.StartId;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Selectable;
import org.jdom.JDOMException;
import org.samspade.gcheckout.GCheckoutException;
import org.samspade79.feeds.ipoints.exceptions.IPointsConnectionException;
import org.samspade79.feeds.ipoints.exceptions.IPointsRequestException;

import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

/**
 * @author sks 19-Dec-2005 18:37:51
 */
@Table("orders")
public class Order extends AttributeMessageSupport implements Logging, Selectable, Deliverable, Payable, Affiliation {

    public enum DeliveryType {
        Collection, Delivery;
    }

    public enum Origin {

        /**
         * Frontend checkout order.
         */
        Customer,

        /**
         * Created in the admin panel by a logged in user
         */
        Admin,

        /**
         * Imported from some feed
         */
        Import,

        /**
         * Auto created by the system
         */
        System
    }

    public enum PaymentSurchargeType {
        Auto, Manual, None
    }

    public enum PaymentTerms {
        Account, Cleared, PaymentOnDelivery
    }

    public enum Sort {
        CustomerName, Oldest, DispatchMethod, ItemsAsc, ItemsDesc, Reference, Status, TotalAsc, TotalDesc, Newest
        // others to add Priority
    }

    public static final byte PRI_HIGH = 2;

    public static final byte PRI_LOW = 0;

    public static final byte PRI_NORMAL = 1;
    public static final byte PRI_URGENT = 3;

    public static final Map<Byte, String> priorityMap;

    static {

        priorityMap = new LinkedHashMap<Byte, String>();
        priorityMap.put(new Byte(PRI_URGENT), "Urgent");
        priorityMap.put(new Byte(PRI_HIGH), "High");
        priorityMap.put(new Byte(PRI_NORMAL), "Normal");
        priorityMap.put(new Byte(PRI_LOW), "Low");
    }

    public static List<Order> getByPhrase(RequestContext context, String searchPhrase) {

        if (searchPhrase == null) {
            return Collections.emptyList();
        }

        searchPhrase = searchPhrase.trim();

        List<Order> orders;
        try {

            long id = Long.parseLong(searchPhrase);
            Query q = new Query(context, "select * from # where id like ? limit 100");
            q.setTable(Order.class);
            q.setParameter("%" + id + "%");
            orders = q.execute(Order.class);

        } catch (NumberFormatException e) {

            Query q = new Query(context, "select o.* from # o join ? c on o.account=c.id " + "where c.name like ? limit 100");
            q.setTable(Order.class);
            q.setTable(Item.class);
            q.setParameter("%" + searchPhrase + "%");
            orders = q.execute(Order.class);
        }

        return orders;
    }

    public static Order getByReference(RequestContext context, String ref) {
        return SimpleQuery.get(context, Order.class, "reference", ref);
    }
    
    public static Order getByOrderId(RequestContext context, String orderId) {
        return SimpleQuery.get(context, Order.class, "id", orderId);
    }

    public static List<Order> getDue(RequestContext context, User salesPerson) {

        QueryBuilder b = new QueryBuilder(context);
        b.select("*");
        b.from("#", Order.class);
        b.clause("amountPaid<saleTotal");
        if (salesPerson != null) {
            b.clause("salesPerson=?", salesPerson);
        }
        b.order("datePlaced");
        return b.execute(Order.class);
    }

    public static List<Order> getLatestOrders(RequestContext context, int i, PaymentType type, Money orderTotal) {
        Query q = new Query(context, "select * from # where cancelled=0 and paymentType=? and saleTotal=? order by id desc");
        q.setTable(Order.class);
        q.setParameter(type);
        q.setParameter(orderTotal);
        return q.execute(Order.class, i);
    }

    public static List<Order> getLatestOrders(RequestContext context, int i) {
        Query q = new Query(context, "select * from # where cancelled=0 order by id desc");
        q.setTable(Order.class);
        return q.execute(Order.class, i);
    }

    public static List<Order> getByGoogleOrderNumber(RequestContext context, String number) {
        Query q = new Query(context, "select * from # where googleOrderNumber=?");
        q.setTable(Order.class);
        q.setParameter(number);
        return q.execute(Order.class);
    }

    public static Map getPriorityMap() {
        return priorityMap;
    }

    public static Map<User, Money> getSalespersonTotals(RequestContext context, Date start, Date end) {

        end = end.nextDay();

        Map<User, Money> map = new LinkedHashMap<User, Money>();

        Query q = new Query(context, "select salesperson, sum(saleTotal) total from # where status!='Cancelled' "
                + "and saleTotal>0 and datePlaced>=? and datePlaced<? group by salesperson order by total desc");
        q.setTable(Order.class);
        q.setParameter(start);
        q.setParameter(end);
        for (Row row : q.execute()) {
            map.put(row.getObject(0, User.class), row.getMoney(1));
        }

        return map;
    }

    /**
     * Card details if terminal payment
     */
    private Card card;

    private Money costSubtotal;

    /**
     * BC fields
     * should be saleTotalInc, saleTotalEx, and saleTotalVat
     */
    private Money saleVat, saleSubtotal, saleTotal;

    private Money paymentCost, paymentCharge;

    /**
     * Delivery charge EX
     */
    private Money deliveryCharge, deliveryChargeVat, deliveryChargeInc;

    private Money itemDeliveryChargesEx;

    /**
     * Cost price of delivery
     */
    private Money deliveryCostEx;

    private DateTime datePlaced;

    /**
     * Addresses for the order
     */
    private Address deliveryAddress, invoiceAddress;

    /**
     * Its like the delivery's product code
     */
    private String deliveryCode;

    private String ipAddress;

    /**
     * Customers own external reference
     */
    private String customerReference;

    /**
     * Date this order was last included in an export of orders.
     * This way we can build a list of orders not exported since the last exported
     */
    private DateTime lastExportedOn;

    /**
     * External reference assigned by our offline database.
     */
    private String reference;

    private String deliveryDetails;

    private Country ipCountry;

    private double deliveryVatRate;

    /**
     * Total of qty's on all lines
     */
    private int itemCount;

    /**
     * Flagged to true if this order is cancelled.
     */
    private boolean cancelled;

    /**
     * Number of lines
     */
    private int lineCount;

    /**
     * The customer for this order
     * BC
     */
    @Index()
    private Item account;

    /**
     *
     */
    private PaymentSurchargeType paymentSurchargeType;

    /**
     *
     */
    private PaymentType paymentType;

    /**
     * Tag this order to a particular sales person
     */
    private User salesPerson;

    /**
     * This is the total amount paid from payments
     */
    private Money amountPaid;

    /**
     * True if this order is subject to VAT
     */
    private boolean vatable;

    /**
     * String status assigned by the admin of the site
     */
    @Index()
    private String status;

    /**
     * Flagged to true when this order has been authorised (passed security) checks
     */
    private boolean authorised;

    /**
     * the last avs check run through for the last payment
     */
    private AvsCheck avsCheck;

    private int rewardedCredits;

    private Origin origin;

    private Money voucherDiscountEx, voucherDiscountVat, voucherDiscountInc;

    // lines totals are the totals of the items on the order before shipping and vouchers
    private Money linesInc, linesVat, linesEx;

    private String voucherDescription;

    /**
     * Sales category
     */
    private String category;

    private CreditGroup able2BuyCreditGroup;

    /**
     * The delivery option choosen by the customer for this order
     */
    private DeliveryOption deliveryOption;

    /**
     * Text included by the user when they opted to be vat free, such as their vat number, or a some other reference.
     */
    private String vatExemptText;

    private Branch branch;

    /**
     * The date this order is next due to be invoiced
     */
    private Date reinvoiceDate;

    /**
     * The recurring period for this order, ie, how often to rebill it
     */
    private Period recurPeriod;

    /**
     * Flagged true when this order is allocated for stock
     * Non stock orders, or sites with stock control off with always be allocated
     */
    private boolean allocated;

    /**
     * Flagged true when this order has been invoiced at least once
     */
    private boolean invoiced;

    /**
     * Flagged true when a payment attempt was declined
     */
    private boolean declined;

    /**
     * where user heard about site
     */
    private String referrer;

    /**
     * used for cancel and chanrge order via google checkout
     */
    private String googleOrderNumber;

    private String reason;

    private String comment;

    private String finansialStatus;

    private String payPalTransactionId;

    private String	ipointsUsername;

    private String	ipointsPassword;

    private String	ipointsReference;

    private String cardsaveCrossReference;

    private Order(RequestContext context) {
        super(context);
    }

    public Order(RequestContext context, Item account, PaymentType paymentType, Origin origin, String ipAddress) {
        super(context);

        OrderSettings orderSettings = OrderSettings.getInstance(context);

        this.account = account;
        this.datePlaced = new DateTime();

        this.ipAddress = ipAddress;
        this.ipCountry = GeoIp.lookup(context, ipAddress);

        this.amountPaid = new Money(0);

        // set admin status
        this.status = orderSettings.getInitialStatus();

        // -- set all monetry fields to zero --
        initTotals();
        initDelivery();

        this.vatable = Company.getInstance(context).isVatRegistered();

        // --- payment fields ---
        this.paymentSurchargeType = PaymentSurchargeType.None;
        this.paymentType = paymentType;

        // order should be authorised until something is added that unauthorises it
        this.authorised = true;

        // order should be allocated until a line is added that unallocates it
        this.allocated = true;
        this.invoiced = false;

        save();

        // get attributes from item type and check for default values
        // we don't need to get attributes from categories because I don't want to set default values for category attributes
        AttributeUtil.setDefaults(this, orderSettings.getAttributes());
    }

    public OrderLine addBasketLine(BasketLine basketLine) {

        logger.fine("adding basket line=" + basketLine);

        OrderLine line;
        if (basketLine.hasItem()) {

            logger.fine("basketline has item=" + basketLine.getItem());
            line = addItemLine(basketLine.getItem(), basketLine.getQty());
            line.setSalePrice(basketLine.getSalePriceEx());
            line.setOptions(basketLine.getOptions());
            line.save();

            reduceOptionStock(line, basketLine);

            if (basketLine.getItem().getPricing().hasCollectionCharge()) {

                Money charge = basketLine.getItem().getPricing().getCollectionCharge();
                if (charge != null && charge.isPositive()) {

                    addDescriptionLine("Collection charge", 1, charge);
                }

            } else
                recalc(); 
            
        } else {

            logger.fine("basketline is description");
            line = addDescriptionLine(basketLine.getName(), basketLine.getQty(), basketLine.getSalePriceEx());
        }

        if (basketLine.hasAttachments()) {

            try {

                basketLine.moveAttachmentsTo(line);

            } catch (AttachmentExistsException e) {
                e.printStackTrace();

            } catch (AttachmentTypeException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();

            } catch (AttachmentLimitException e) {
                e.printStackTrace();
            }
        }

        return line;
    }

    public OrderLine addDescriptionLine(String desc, int qty, Money amount) {

        OrderLine line = new OrderLine(context, this, desc, qty, amount);
        setLineCount();
        save();

        recalc();

        checkAllocation();

        return line;
    }

    public OrderLine addInfoLine(String desc, int qty, Money amount) {

        OrderLine line = addDescriptionLine(desc, qty, amount);
        Double vatRate = 0.0;
        ItemType itemType = ItemType.getPricing(context);
        if (itemType != null && itemType.isVat()) {
            vatRate = itemType.getDefaultVatRate();
        }
        line.setVatRate(vatRate);
        line.save();
        return line;

    }

    public OrderLine addItemLine(Item item, int qty) {

        OrderLine line = new OrderLine(context, this, item, qty);

        setLineCount();
        recalc();

        checkAllocation();

        Company company = Company.getInstance(context);
        Config config = Config.getInstance(context);

        /*
           * Check if this item is listed by a seller.
           * If so, and email sellers orders is on then send an email
           */
        if (item.hasAccount() && item.getAccount().hasEmail()) {

            ShoppingSettings shoppingSettings = ShoppingSettings.getInstance(context);
            if (shoppingSettings.isEmailSellers()) {

                if (shoppingSettings.hasSellerEmailText()) {

                    Email e = new Email();
                    e.setBody(shoppingSettings.getSellerEmailTextRendered(line));
                    e.setSubject("Order received from " + company.getName());
                    e.setFrom(company.getName(), config.getServerEmail());
                    e.setTo(item.getAccount().getEmail());

                    try {

                        logger.fine("sending order details to seller");
                        logger.fine("" + e);
                        new EmailDecorator(context, e).send(config.getSmtpHostname());

                    } catch (EmailAddressException e1) {
                        e1.printStackTrace();

                    } catch (SmtpServerException e1) {
                        e1.printStackTrace();
                    }

                }

            }
        }

        return line;
    }

    public OrderLine addListingPackage(ListingRate rate, Item item) {
        return addDescriptionLine("Listing charge for #" + item.getId() + ": " + rate.getPeriodDescription() + " at " + rate.getFeeEx(), 1, rate.getFeeEx());
    }

    public Parcel addParcel(Courier courier, String consignment) {
        return new Parcel(context, this, courier, consignment);
    }

    public void addPayment(Payment payment) {

        payment.setOrder(this);
        payment.save();

        logger.fine("payment added=" + payment);

        // payment has been added, so set status to processing if fully paid
        if (isPaidFull()) {
            logger.fine("order is now paid in full");
        }
    }

    public OrderLine addSubscription(SubscriptionRate sp) throws OrderStatusException {

        OrderLine line = addDescriptionLine("Subscription: " + sp.getDescription(), 1, sp.getFeeInc());

        line.setVatRate(sp.getVatRate());
        recalc();

        return line;
    }

    public boolean cancel() {

        if (isCancelled()) {
            return false;
        }

//        removeLines();
        initDelivery();
        removeParcels();

        if (PaymentType.GoogleCheckout.equals(getPaymentType())) {
            try {
                GCheckoutCanceller.cancelOrder(this, PaymentSettings.getInstance(context).getGoogleCheckoutId(),
                        PaymentSettings.getInstance(context).getGoogleCheckoutPw());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JDOMException e) {
                e.printStackTrace();
            } catch (GCheckoutException e) {
                e.printStackTrace();
            }
        }
//        this.lineCount = 0;
//        this.itemCount = 0;

        status = "Cancelled";
        this.cancelled = true;

        recalc();

        save();

        getAccount().setBalance();
        return true;
    }

    private void checkAllocation() {

        // if order is awaiting stock, check status of lines
        if (isAllocated()) {

            allocated = true;

        } else {

            allocated = false;
        }

        save("allocated");
    }

    /**
     * Automatically attempt authorisation using FraudGuard
     * <p/>
     * Will set authorised field to true if this order can now be authorised
     */
    private boolean checkAuthorisation() {

        logger.fine("checking authorisation");

        /*
           * If already authorised just return
           */
        if (authorised) {
            logger.fine("order previously authorised manually");
            return true;
        }

        if (getTotalInc().isLessThan(OrderSettings.getInstance(context).getFloorLimit())) {
            logger.fine("order is below floor limit, authorising");
            return true;
        }

        /*
           * If collection then authorise!
           */
        if (isCollection()) {
            logger.fine("collection order, authorising");
            return true;
        }

        /*
           * If the order passed AVS checks for this postcode then authorise
           */
        String postcode = getDeliveryAddress().getPostcode();
        if (AvsCheck.hasMatch(context, account, postcode)) {

            logger.fine("account has matched AVS for this postcode, authorising");
            return true;

        }

        /*
           * TO/DO Check for FRAUD GUARD MODULE HERE
           */

        logger.fine("order cannot be authorised automatically, return false");
        return false;
    }

    /**
     * Actions that should be performed when an order is set to the 'completed' status
     */
    private void completed() {

        new SystemMessage(context, "Order #" + getId() + " has been completed");
        logger.fine("order completed");

        /*
           * Process reward credits
           */
        if (rewardedCredits > 0) {
            getAccount().getCreditStore().addCredits(rewardedCredits, "Rewarded from order# " + getId());
        }

        OrderSettings orderSettings = OrderSettings.getInstance(context);
        if (orderSettings.hasCompletionEmail()) {

            try {

                emailCompletion();

            } catch (EmailAddressException e) {
                e.printStackTrace();

            } catch (SmtpServerException e) {
                e.printStackTrace();
            }
        }

        // if we are email invoices on completion then make it so
        if (orderSettings.isEmailInvoiceOnCompletion()) {
            try {
                emailInvoice();
            } catch (EmailAddressException e) {
                e.printStackTrace();
            } catch (SmtpServerException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // run ipoints
//        synchronized (Order.class) {
            IPointsSettings ipsettings = IPointsSettings.getInstance(context);
            if (ipsettings.getUsername() != null) {

                if (ipointsUsername != null) {

                    IPointsService service = new IPointsService(context);
                    try {
                        service.addPoints(this);
                        Thread.sleep(2000);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JDOMException e) {
                        e.printStackTrace();
                    } catch (IPointsConnectionException e) {
                        e.printStackTrace();
                    } catch (IPointsRequestException e) {
                        e.printStackTrace();
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
//        }

        if (getPaymentType() == PaymentType.CardTerminal)
            clearCard();

    }

    private Order copy() {

        Order copy = new Order(context, account, paymentType, Origin.System, context.getRemoteIp());

        copy.setBillingAddress(getBillingAddress());
        copy.setDeliveryAddress(getDeliveryAddress());
        copy.setPaymentType(paymentType);
        copy.setCard(getCard());

        copy.setSalesPerson(getSalesPerson());
        copy.setCategory(category);
        copy.setRecurPeriod(recurPeriod);

        // do lines
        for (OrderLine line : getLines()) {

            if (!line.hasItem()) {
                copy.addDescriptionLine(line.getDescription(), line.getQty(), line.getUnitSellEx());
            }
        }

        copy.save();

        return copy;
    }

    public void declined() {

        log("Order declined");
        this.declined = true;

        Config config = Config.getInstance(context);

        // send out declined emails
        PaymentSettings paymentSettings = PaymentSettings.getInstance(context);
        if (paymentSettings.hasDeclineEmails()) {

            Email e = new Email();
            e.setSubject("Declined transaction");
            e.setBody("Transaction for order #" + getIdString() + " has been declined");
            e.setRecipients(paymentSettings.getDeclineEmails());

            e.setFrom(config.getServerEmail());

            try {
                new EmailDecorator(context, e).send(config.getSmtpHostname());
            } catch (EmailAddressException e1) {
                e1.printStackTrace();
            } catch (SmtpServerException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public boolean delete() {
        throw new UnsupportedOperationException();
    }

    public void emailCompletion() throws EmailAddressException, SmtpServerException {

        logger.fine("sending completion emails");
        new OrderCompletionEmail(context, this).send(Config.getInstance(context).getSmtpHostname(),
                Config.getInstance(context).getUsername(), Config.getInstance(context).getPassword());
    }

    public void emailInvoice() throws EmailAddressException, SmtpServerException, IOException {

        logger.fine("sending invoice emails");
        new InvoiceEmail(context, this).send(Config.getInstance(context).getSmtpHostname(),
                Config.getInstance(context).getUsername(), Config.getInstance(context).getPassword());
    }

    public CreditGroup getAble2BuyCreditGroup() {
        return (CreditGroup) (able2BuyCreditGroup == null ? null : able2BuyCreditGroup.pop());
    }

    public Item getAccount() {
        return account.pop();
    }

    public Money getAffiliationValue() {
        return getTotalInc();
    }

    public Money getAmountOutstanding() {
        return saleTotal.minus(amountPaid);
    }

    public final Money getAmountPaid() {
        return amountPaid;
    }

    public List<Attribute> getAttributes() {
        return OrderSettings.getInstance(context).getAttributes();
    }

    public List<String> getAttributeValues(String attributeName, boolean exact) {
        if (attributeName == null)
            return null;

        attributeName = attributeName.toLowerCase();

        MultiValueMap<Attribute, String> attributeValues = this.getAttributeValues();
        for (Map.Entry<Attribute, List<String>> entry : attributeValues.entryListSet()) {

            if (exact) {

                if (entry.getKey().getName().toLowerCase().equals(attributeName))
                    return entry.getValue();

            } else {

                if (entry.getKey().getName().toLowerCase().contains(attributeName))
                    return entry.getValue();
            }

        }

        return Collections.emptyList();
    }

    /**
     * Return avs checks that were done as part of billing for this order.
     */
    public List<AvsCheck> getAvsChecks() {

        return Collections.emptyList();
    }

    public Address getBillingAddress() {
        return (Address) (invoiceAddress == null ? null : invoiceAddress.pop());
    }

    public final Date getReinvoiceDate() {
        return reinvoiceDate;
    }

    public final Branch getBranch() {
        return (Branch) (branch == null ? null : branch.pop());
    }

    public Card getCard() {
        return (Card) (card == null ? null : card.pop());
    }

    public String getCategory() {
        return category;
    }

    public Money getCostSubtotal() {
        return costSubtotal;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public DateTime getDatePlaced() {
        return (DateTime) (datePlaced == null ? new Date() : datePlaced);
    }

    public List<DeliverableItem> getDeliverableItems() {

        List<DeliverableItem> list = new ArrayList();
        for (OrderLine line : getLines()) {
            if (line.hasItem()) {
                if (!line.isPromotion()) {
                    list.add(line);
                }
            }
        }

        return list;
    }

    public Address getDeliveryAddress() {
        return (Address) (deliveryAddress == null ? null : deliveryAddress.pop());
    }

    public Money getDeliveryChargeEx() {
        return deliveryCharge;
    }

    public Money getDeliveryChargeInc() {
        return deliveryChargeVat.add(deliveryCharge);
    }

    public Money getDeliveryChargeVat() {
        return deliveryChargeVat;
    }

    public String getDeliveryCode() {
        return deliveryCode;
    }

    public Money getDeliveryCostEx() {
        return deliveryCostEx;
    }

    public Country getDeliveryCountry() {
        if (deliveryAddress == null)
            return null;

        return getDeliveryAddress().getCountry();
    }

    public String getDeliveryDetails() {
        return deliveryDetails;
    }

    public final DeliveryOption getDeliveryOption() {
        return (DeliveryOption) (deliveryOption == null ? null : deliveryOption.pop());
    }

    public String getDeliveryPostcode() {
        return isCollection() ? "None" : getDeliveryAddress().getPostcode();
    }

    public DeliveryType getDeliveryType() {
        return isCollection() ? DeliveryType.Collection : DeliveryType.Delivery;
    }

    public double getDeliveryVatRate() {
        return deliveryVatRate;
    }

    public Money getHighestDeliveryCharge() {
        return null;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public Country getIpCountry() {
        return ipCountry;
    }

    public String getIpointsPassword() {
		return ipointsPassword;
	}

    public String getIpointsReference() {
		return ipointsReference;
	}

    public String getIpointsUsername() {
		return ipointsUsername;
	}

    /**
     * returns the number of items on this order
     */
    public int getItemCount() {
        return itemCount;
    }

    public Money getItemsSubtotal() {
        return getTotalEx().minus(getDeliveryChargeEx());
    }

    public Money getItemsTotal() {
        return getTotalInc().minus(getDeliveryChargeInc());
    }

    public String getLabel() {
        return getIdString();
    }

    public DateTime getLastExportedOn() {
        return lastExportedOn;
    }

    public Payment getLastPayment() {
        List<Payment> payments = getPayments();
        return payments.isEmpty() ? null : payments.get(payments.size() - 1);
    }

    public OrderLine getLine(Item product) {

        Iterator iter = getLines().iterator();
        while (iter.hasNext()) {

            OrderLine item = (OrderLine) iter.next();
            if (item.hasItem() && item.getItem().equals(product))
                return item;
        }

        return null;
    }

    public int getLineCount() {
        return this.lineCount;
    }

    public List<OrderLine> getLines() {

        Query q = new Query(context, " select * from # where ord=? order by position, id ");
        q.setTable(OrderLine.class);
        q.setParameter(this);
        return q.execute(OrderLine.class);
    }

    /**
     * Returns a string of all items on this order for use on things like descriptions for payment pages, or exporting a simple description
     */
    public String getLinesDescription(String sep) {

        StringBuilder sb = new StringBuilder();
        Iterator<OrderLine> iter = getLines().iterator();
        while (iter.hasNext()) {

            OrderLine line = iter.next();

            sb.append(" " + line.getQty() + " of " + line.getDescription());
            if (line.hasOptionsDetails()) {
                sb.append("(" + line.getOptionsDetails() + ")");
            }
            sb.append(" @ \243" + line.getUnitSellEx() + " = \243" + line.getLineSellEx());

            if (iter.hasNext()) {
                sb.append(sep);
            }
        }

        return sb.toString();
    }

    public Money getLinesEx() {
        return linesEx;
    }

    public Money getLinesInc() {
        return linesInc;
    }

    public Money getLinesVat() {
        return linesVat;
    }

    public List<LogEntry> getLogEntries() {
        return LogEntry.get(context, this);
    }

    public String getLogId() {
        return getFullId();
    }

    public String getLogName() {
        return "Order #" + getId();
    }

    public double getMargin() {

        int s = saleSubtotal.getAmount();
        int c = costSubtotal.getAmount();

        double d = s - c;
        return d / s * 100;
    }

    public String getMarginFormatted() {
        return NumberFormatter.format(getMargin(), 1) + "%";
    }

    public double getMarkup() {

        int s = saleSubtotal.getAmount();
        int c = costSubtotal.getAmount();

        double d = s - c;
        return d / c * 100;
    }

    public String getMarkupFormatted() {
        return NumberFormatter.format(getMarkup(), 1) + "%";
    }

    public String getMessageSubject() {
        return "Order #" + getId();
    }

    public String getMessageSubjectPrefix() {
        return "o";
    }

    public long getNextOrderId() {
        return getId() + 1;
    }

    /**
     * Returns an url for a customer to quickly check the status of this order on the front end
     */
    public String getOrderStatusUrl() {
        return Config.getInstance(context).getUrl() + "/" + OrderStatusHandler.class.getAnnotation(Path.class).value()[0] + "?action=show&orderId=" +
                getOrderId();
    }

    public Origin getOrigin() {
        return origin == null ? Origin.System : origin;
    }

    /**
     * Returns the amount of money outstanding on this order
     */
    public Money getOutstandingAmount() {
        return getTotalInc().minus(getPaymentAmount());
    }

    public List<Parcel> getParcels() {
        Query q = new Query(context, "select * from # where ord=? and status=?");
        q.setTable(Parcel.class);
        q.setParameter(this);
        q.setParameter(Parcel.Status.Current);
        return q.execute(Parcel.class);
    }

    public List<Parcel> getParcelsAll() {
        Query q = new Query(context, "select * from # where ord=?");
        q.setTable(Parcel.class);
        q.setParameter(this);
        return q.execute(Parcel.class);
    }

    public String getParcelsDescription(String sep) {

        List<Parcel> parcels = getParcels();
        if (parcels.size() == 0) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        for (Parcel parcel : getParcels()) {
            sb.append(parcel.getCourier().toString() + ": " + parcel.getConsignmentNumber());
            sb.append(sep);
        }
        return sb.toString();
    }

    public Item getPayableAccount() {
        return getAccount();
    }

    public Address getPaymentAddress() {
        return getBillingAddress();
    }

    public Money getPaymentAmount() {
        return getTotalInc();
    }

    public Money getPaymentCharge() {
        return paymentCharge;
    }

    public Money getPaymentCost() {
        return paymentCost;
    }

    public String getPaymentDescription() {
        return "Order #" + getId();
    }

    public String getPaymentFailureUrl(PaymentType paymentType) {
        return Config.getInstance(context).getUrl() + "/" + new Link(CheckoutHandler.class, "cancelled").toString();
    }

    public List<Payment> getPayments() {
        return SimpleQuery.execute(context, Payment.class, "orderId", this);
    }

    public String getPaymentSuccessUrl(PaymentType paymentType) {
        return Config.getInstance(context).getUrl() + "/" + new Link(CheckoutHandler.class, "completed").toString();
    }

    public PaymentSurchargeType getPaymentSurchargeType() {
        return paymentSurchargeType;
    }

    public PaymentType getPaymentType() {
        return paymentType == null ? PaymentType.Cash : paymentType;
    }

    public String getPayPalTransactionId() {
        return payPalTransactionId;
    }

    public String getBasketUrl() {
        return Config.getInstance(context).getUrl() + "/" + new Link(BasketHandler.class).toString();
    }

    public int getPreviousOrderId() {
        return getId() - 1;
    }

    public Money getProfit() {
        return saleSubtotal.subtract(costSubtotal);
    }

    public final Period getRecurPeriod() {
        return recurPeriod;
    }

    public String getReference() {
        return reference;
    }

    public int getRewardedCredits() {
        return rewardedCredits;
    }

    public User getSalesPerson() {
        return (User) (salesPerson == null ? null : salesPerson.pop());
    }

    public String getSalesPersonString() {

        if (salesPerson == null) {
            return "Web Order";
        } else {
            return getSalesPerson().getName();
        }
    }

    public final String getStatus() {
        return status == null ? "New" : status;
    }

    public List<Submission> getSubmissions() {
        return SimpleQuery.execute(context, Submission.class, "order", this);
    }

    public Money getTotalEx() {
        return saleSubtotal;
    }

    public Money getTotalInc() {
        return saleTotal;
    }

    public Money getTotalVat() {
        return saleVat;
    }

    public String getValue() {
        return getIdString();
    }

    public String getVatExemptText() {
        return vatExemptText;
    }

    public String getVoucherDescription() {
        return voucherDescription;
    }

    public Money getVoucherDiscountEx() {
        return voucherDiscountEx;
    }

    public Money getVoucherDiscountInc() {
        return voucherDiscountInc;
    }

    public Money getVoucherDiscountIncAbs() {
        return voucherDiscountInc != null ? voucherDiscountInc.abs() : new Money(0.0);
    }

    public Money getVoucherDiscountVat() {
        return voucherDiscountVat;
    }

    public boolean hasBillingAddress() {
        return invoiceAddress != null;
    }

    public boolean hasBillingDate() {
        return reinvoiceDate != null;
    }

    public boolean hasBranch() {
        return branch != null;
    }

    public boolean hasCard() {
        return card != null;
    }

    public boolean hasCategory() {
        return category != null;
    }

    public boolean hasCustomerReference() {
        return customerReference != null;
    }

    public boolean hasDeliveryAddress() {
        return deliveryAddress != null;
    }

    public boolean hasDeliveryCountry() {
        return false;
    }

    public boolean hasDeliveryDetails() {
        return deliveryDetails != null;
    }

    public boolean hasLines() {
        return lineCount > 0;
    }

    public boolean hasParcels() {
        return getParcels().size() > 0;
    }

    public boolean hasPaymentSurcharge() {
        return paymentCharge.isGreaterThan(0);
    }

    public boolean hasReference() {
        return reference != null;
    }

    public boolean hasSalesPerson() {
        return salesPerson != null;
    }

    public boolean hasVatExemptText() {
        return vatExemptText != null;
    }

    public boolean hasVoucher() {
        return voucherDescription != null;
    }

    private void initDelivery() {
        this.deliveryDetails = null;
        this.deliveryCostEx = new Money(0);
        this.deliveryCharge = new Money(0);
        this.deliveryChargeVat = new Money(0);
    }

    private void initTotals() {

        // total fields
        this.saleTotal = new Money(0);
        this.saleVat = new Money(0);
        this.saleSubtotal = new Money(0);
        this.costSubtotal = new Money(0);

        // field for payment surcharge
        this.paymentCharge = new Money(0);
        this.paymentCost = new Money(0);

    }

    /**
     * Returns true if this order is currently is fully allocated (in stock).
     * If stock system is not enabled then always returns true
     */
    private boolean isAllocated() {

        logger.fine("checking order allocation");

        if (!Module.Availabilitity.enabled(context)) {
            logger.fine("Stock module is off, so defaulting to full allocation");
            return true;
        }

        for (OrderLine line : getLines()) {

            if (!line.isAllocated()) {
                logger.fine("order line=" + line + " is not allocated, returning false");
                return false;
            }
        }

        logger.fine("all lines are allocated, returning true");
        return true;
    }

    public boolean isAuthorised() {
        return authorised;
    }

    public boolean isCancelled() {
        return "cancelled".equalsIgnoreCase(status) || cancelled;
    }

    public boolean isCollection() {
        return deliveryAddress == null;
    }

    public boolean isDelivery() {
        return deliveryAddress != null;
    }

    /**
     * Returns true if this order has been included on an export before
     */
    public boolean isExported() {
        return lastExportedOn != null;
    }

    /**
     * An order is open if it is not yet invoiced and not cancelled
     */
    public boolean isOpen() {

        if (invoiced && recurPeriod != null) {
            return false;
        }

        return !cancelled;
    }

    /**
     * Checks that the total value of all payments is equal to the order value and returns true if so.
     */
    private boolean isPaidFull() {
        return amountPaid.equals(saleTotal);
    }

    private boolean isPaymentOutstanding() {
        return saleTotal.isGreaterThan(amountPaid);
    }

    public boolean isRecurringToday() {
        return new Date().equals(reinvoiceDate);
    }

    public boolean isReservation() {
        return deliveryAddress == null;
    }

    public boolean isVatable() {
        return vatable;
    }

    public boolean isVatableForDelivery() {
        return isVatable();
    }

    public boolean isWebOrder() {
        return salesPerson == null;
    }

    public void log(String message) {
        new LogEntry(context, this, message);
    }

    public void log(User user, String message) {
        new LogEntry(context, this, user, message);
    }

    public Payment manualTransact(Money amountCharged, AvsResult avsResult) {

        Payment payment = new Payment(context, getAccount(), amountCharged, PaymentType.CardTerminal);
        payment.setOrder(this);
        payment.save();

        logger.fine("manual payment=" + payment + ", amount=" + amountCharged);

        if (avsResult != null) {

            logger.fine("avs result= " + avsResult);
            new AvsCheck(context, this, payment, avsResult);

        }

        return payment;
    }

    /**
     * Recalculates the totals for this order. Call this after modifying anything that would affect price, eg:
     * <p/>
     * item qty item price shipping cost payment type
     */
    public void recalc() {

        logger.fine("recalc");

        ShoppingSettings shoppingSettings = ShoppingSettings.getInstance(context);

        // -- reset montry values --
        this.saleSubtotal = new Money(0);
        this.saleVat = new Money(0);
        this.saleTotal = new Money(0);
        this.costSubtotal = new Money(0);

        // lines totals are the totals of the items on the order before shipping and vouchers
        this.linesEx = new Money(0);
        this.linesVat = new Money(0);
        this.linesInc = new Money(0);

        this.rewardedCredits = 0;

        this.paymentCharge = new Money(0);
        this.paymentCost = new Money(0);

        this.lineCount = 0;
        this.itemCount = 0;

        Company company = Company.getInstance(context);
        boolean vat = company.isVatRegistered();

        // -- do order lines --
        for (OrderLine line : getLines()) {

            logger.fine("lineSellEx=" + line.getLineSellEx() + ", lineSellVat=" + line.getLineSellVat() + ", lineSellTotal=" + line.getLineSellInc());

            costSubtotal = costSubtotal.add(line.getLineCostEx());
            linesEx = linesEx.add(line.getLineSellEx());

            if (vat) {
                linesVat = linesVat.add(line.getLineSellVat());
            }

            lineCount++;
            itemCount += line.getQty();
        }

        Config config = Config.getInstance(context);

        if (!config.isToys()) {

            this.deliveryCharge = new Money(0);
            this.deliveryChargeVat = new Money(0);
            this.deliveryChargeInc = new Money(0);

            /*
                * Calculate delivery charges
                */
            if (deliveryOption != null) {

                this.deliveryDetails = getDeliveryOption().getDescription();
                this.deliveryCode = getDeliveryOption().getCode();
                this.deliveryVatRate = getDeliveryOption().getVatRate();

//                if (linesVat.isZero()) {
//                    deliveryVatRate = 0;
//                    logger.fine("order has no vat so far, so overriding vat rate to zero");
//                }

                this.deliveryCharge = getDeliveryOption().getChargeEx(this);
                this.deliveryChargeVat = VatHelper.vat(context, deliveryCharge, deliveryVatRate);
                this.deliveryChargeInc = deliveryCharge.add(deliveryChargeVat);

            }

        }

        logger.fine("linesEx=" + linesEx + ", linesVat=" + linesVat);
        logger.fine("deliveryCharge=" + deliveryCharge + ", deliveryChargeVat=" + deliveryChargeVat);

        // -- subtotal is calculated from lines plus delivery but without voucher
        costSubtotal = costSubtotal.add(deliveryCostEx);

        saleSubtotal = linesEx.add(deliveryCharge).add(voucherDiscountEx);
        saleVat = linesVat.add(deliveryChargeVat).add(voucherDiscountVat);

        logger.fine("saleSubtotal=" + saleSubtotal + ", saleVat=" + saleVat);

        // if not vatable then reset vat total
        if (!vat || !vatable) {
            logger.fine("recalc, resetting VAT to zero");
            saleVat = new Money(0);
        }

        // set total from subtotal and vat
        saleTotal = saleSubtotal.add(saleVat);

        if (Module.Credits.enabled(context)) {

            logger.fine("credits module enabled, spendPerPoint=" + shoppingSettings.getSpendPerCredit());

            if (shoppingSettings.getSpendPerCredit().isPositive()) {

                // get spend per point and calculate points awarded total
                this.rewardedCredits = saleSubtotal.getAmount() / shoppingSettings.getSpendPerCredit().getAmount();
                logger.fine("credit points rewarded: " + rewardedCredits);

            }
        }

        save();

        getAccount().setBalance();
    }

    private void reduceOptionStock(OrderLine line, BasketLine basketLine) {

        for (ItemOption option : line.getItem().getOptionSet().getOptions()) {
            if (option.isStockOverride()) {
                for (ItemOptionSelection selection : option.getSelections()) {
                    String optionNameSelection = option.getName() + ": " + selection.getText();
                    if (optionNameSelection.equals(this.getLine(line.getItem()).getOptionsDetails())) {
                        selection.setStock(selection.getStock() - basketLine.getQty());
                        selection.save();
                    }
                }
            }
        }

    }

    public void removeLine(OrderLine line) {

        line.delete();
        setLineCount();

        recalc();

    }

    private void removeLines() {

        for (OrderLine line : getLines()) {
            removeLine(line);
        }
    }

    public void removeParcel(Parcel parcel) {
        parcel.delete();
    }

    private void removeParcels() {
        for (Parcel parcel : getParcels()) {
            removeParcel(parcel);
        }
    }

    public void removeVoucher() {
        voucherDescription = null;
        voucherDiscountEx = new Money(0);
        voucherDiscountInc = new Money(0);
        voucherDiscountVat = new Money(0);

        recalc();
    }

    public void sendAcknowledgementEmail() throws EmailAddressException, SmtpServerException {

        logger.fine("sending ack email");

        if (isCancelled()) {
            logger.fine("order is cancelled, not sending");
            return;
        }

        if (!getAccount().hasEmail()) {
            logger.fine("account has no email, not sending");
            return;
        }

        OrderAcknowledgementEmail e = new OrderAcknowledgementEmail(context, this);
        e.setTo(getAccount().getEmail());
        new EmailDecorator(context, e).send(Config.getInstance(context).getSmtpHostname());
    }

    public void sendNotificationEmails() throws EmailAddressException, SmtpServerException {

        ShoppingSettings shoppingSettings = ShoppingSettings.getInstance(context);
        Config config = Config.getInstance(context);

        if (shoppingSettings.hasNotificationEmails()) {

            OrderAcknowledgementEmail e = new OrderAcknowledgementEmail(context, this, true);
//            e.setAdminLink();  //deprecated
            e.setRecipients(shoppingSettings.getOrderNotificationEmails());

            new EmailDecorator(context, e).send(config.getSmtpHostname());
        }
    }

    public void sendCancelationEmails() throws EmailAddressException, SmtpServerException {

        ShoppingSettings shoppingSettings = ShoppingSettings.getInstance(context);
        Config config = Config.getInstance(context);

        if (shoppingSettings.hasNotificationEmails()) {

            Email email = new Email();
            email.setRecipients(shoppingSettings.getOrderNotificationEmails());
            email.setFrom(config.getServerEmail());
            email.setSubject(Company.getInstance(context).getName() + " - Order cancellation #" + getOrderId());
            email.setBody(getCancellationBody(config));

            new EmailDecorator(context, email).send(config.getSmtpHostname());
        }
    }

    public String getCancellationBody(Config config) {

        StringBuilder sb = new StringBuilder();

        sb.append("\nAn order has been cancelled. To view it click this URL\n");
        sb.append(config.getUrl() + "/" + new Link(OrderHandler.class, null, "order", getId()));
        sb.append("\n\n\n");

        return sb.toString();
    }

    public final void setAble2BuyCreditGroup(CreditGroup able2BuyCreditGroup) {
        this.able2BuyCreditGroup = able2BuyCreditGroup;
    }

    public boolean setAccount(Item acc) {

        if (account.equals(acc)) {
            return false;
        }

        this.account = acc;
        return true;
    }

    public final void setAmountPaid(Money m) {
        this.amountPaid = m;
    }

    public void setAuthorised(boolean authorised) {
        this.authorised = authorised;
    }

    public void setBillingAddress(Address a) {
        logger.fine("setting billingaddy=" + a);
        this.invoiceAddress = a;
    }

    public final void setReinvoiceDate(Date date) {
        this.reinvoiceDate = date;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public boolean setCard(Card c) {

        logger.fine("card=" + c);

        if (ObjectUtil.equal(card, c)) {
            return false;
        }

        this.card = c;
        return true;
    }

    public void setCategory(String c) {
        this.category = c;
    }

    /**
     * Sets this order's delivery address to collection.
     */
    public void setCollection() {

        // set address to null for collection
        this.deliveryAddress = null;

        // no shipping for collection orders
        this.deliveryCostEx = new Money(0);
        this.deliveryCharge = new Money(0);
        this.deliveryDetails = null;

        // if collecting then VAT must be charged if vatable
        if (Company.getInstance(context).isVatRegistered()) {

            this.vatable = Company.getInstance(context).isVatRegistered();
            save();

        }

        // redo totals as collection will remove shipping costs and vat free
        // status.
        recalc();

        // authorise order - collection orders need not go into auth queue
        authorised = true;
    }

    public void setCustomerReference(String s) {
        this.customerReference = s;
        save();
    }

    /**
     * Changes delivery address on this order.
     */
    public void setDeliveryAddress(Address address) {
        logger.fine("setting deladd=" + address);

        if (address == null){
            return;
        }

        this.deliveryAddress = address;

        if (deliveryDetails == null) {
            deliveryDetails = "Shipping";
        }

        if (!Company.getInstance(context).isVatRegistered()) {
            return;
        }

        if (address.isVatable() != vatable) {
            vatable = !vatable;
            recalc();
        }

        // changing delivery address means we might need to re-authorise
        authorised = false;
        save();

    }

    /**
     * Override the delivery charge
     */
    public void setDeliveryCharge(Money deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
        recalc();
    }

    /**
     * Override the delivery desc
     */
    public void setDeliveryDescription(String deliveryDescription) {
        deliveryDetails = deliveryDescription;
    }

    @SuppressWarnings("hiding")
    private static Logger logger = Logger.getLogger(Order.class.getName());

    public void setDeliveryOption(DeliveryOption option) {

        logger.fine("setting delivery option=" + option);

//		// check this rate is valid for this order
//		if (!option.isValid(this)) {
//			return;
//		}

        this.deliveryOption = option;
        recalc();
    }

    public void setLastExportedOn(DateTime lastExportedOn) {
        this.lastExportedOn = lastExportedOn;
    }

    private void setLineCount() {
        this.lineCount = getLines().size();
    }

    public void setPaymentSurchargeType(PaymentSurchargeType paymentSurchargeType) {
        this.paymentSurchargeType = paymentSurchargeType;
    }

    public final boolean isInvoiced() {
        return invoiced;
    }

    public final void setInvoiced(boolean invoiced) {
        this.invoiced = invoiced;
    }

    public void setIpointsPassword(String ipointsPassword) {
		this.ipointsPassword = ipointsPassword;
	}

    public void setIpointsReference(String ipointsReference) {
		this.ipointsReference = ipointsReference;
	}

    public void setIpointsUsername(String ipointsUsername) {
		this.ipointsUsername = ipointsUsername;
	}

    public boolean isRecurring() {
        return recurPeriod != null && reinvoiceDate != null && reinvoiceDate.isFuture();
    }

    public void setPaymentType(PaymentType paymentType) {

        logger.fine("set payment type=" + paymentType);

        this.paymentType = paymentType;
        if (paymentType != PaymentType.CardTerminal) {
            this.card = null;
        }

        recalc();
    }

    public void setPayPalTransactionId(String payPalTransactionId) {
        this.payPalTransactionId = payPalTransactionId;
    }

    /**
     * Changes the line qt
     */
    public void setQty(OrderLine line, int qty, RequestContext context) throws OrderStatusException {

        // no change needed ?
        if (qty == line.getQty()) {
            return;
        }

        //		if (isCompleted() || isCancelled())
        //			throw new OrderStatusException();

        if (qty == 0) {
            removeLine(line);
            return;
        }

        line.setQty(qty);

        // changing qty should require to re-auth
        authorised = false;
        save();

        return;
    }

    public final void setRecurPeriod(Period recurPeriod) {
        this.recurPeriod = recurPeriod;
    }

    public void setReference(String externalReference) {
        this.reference = externalReference;
    }

    public void setSalesPerson(User salesPerson) {
        this.salesPerson = salesPerson;
    }

    public void setSellPrice(OrderLine line, Money salePrice) {

        // maybe no change needed - will sale recalc
        if (line.getUnitSellEx().equals(salePrice)) {
            return;
        }

        line.setSalePrice(salePrice);

    }

    public void setStatus(String s) {

        if (s == null) {
            return;
        }

        if (s.equals(status)) {
            return;
        }

        this.status = s;

        if ("completed".equalsIgnoreCase(status)) {
			completed();
		}
    }

    public void setVatable(boolean b) {

        if (!Company.getInstance(context).isVatRegistered()) {
            return;
        }

        if (vatable == b) {
            return;
        }

        vatable = b;
        save();

        recalc();
    }

    public final void setVatExemptText(String vatExemptText) {
        this.vatExemptText = vatExemptText;
    }

    public void setVatRate(OrderLine line, double vatRate) {
        // maybe no change needed - will sale recalc
        if (line.getVatRate() == vatRate) {
            return;
        }

        line.setVatRate(vatRate);
    }

    public void setVoucher(Voucher voucher) {
        recalc();

        this.voucherDescription = voucher.getDescription();
        this.voucherDiscountEx = new Money(0);

        if (voucher.isFreeDelivery()) {

            logger.fine("calculating voucher free del");

            voucherDiscountEx = voucher.getDiscount(getDeliveryChargeEx()).negative();
            logger.fine("voucherDiscountEx=" + voucherDiscountEx);

            if (Company.getInstance(context).isVatRegistered()) {
                double d = (double) voucherDiscountEx.getAmount() / (double) getDeliveryChargeEx().getAmount();
                voucherDiscountVat = deliveryChargeVat.multiply(d);
            }

            this.voucherDiscountInc = voucherDiscountEx.add(voucherDiscountVat);

            // set subtotals to take into account voucher

        } else if (!voucher.isFreeDelivery() && !voucher.isGetOneFree()) {
            if (!voucher.isSpecificItemsVoucher()) {
                this.voucherDiscountEx = voucher.getDiscount(getLinesEx()).negative();
            } else {
                //voucher for specific items only
                List<Item> voucherItems = voucher.getItems();
                if (!voucherItems.isEmpty()) {
                    for (OrderLine line : getLines()) {
                        if (voucherItems.contains(line.getItem())) {
                            this.voucherDiscountEx = this.voucherDiscountEx.add(voucher.getDiscount(line.getLineSellEx())).negative();
                        }
                    }
                }
            }


        } else {
            this.voucherDiscountEx = voucher.getDiscount(getLinesEx()).negative();
        }

        if (!voucher.isFreeDelivery()) {
            double ratio = voucherDiscountEx.getAmount() / (double) getLinesEx().getAmount();

            this.voucherDiscountVat = getLinesVat().multiply(ratio);
            this.voucherDiscountInc = voucherDiscountEx.add(voucherDiscountVat);
            this.voucherDescription = voucher.getDescription();
        }


        recalc();
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    public void smsCompletion() {
        logger.fine("sending completion sms");
    }

    /**
     * Splits this order into two orders - this one keeps all items in stock, the other gets all items out of stock.
     */
    public Order splitOrder() {

        Order order = new Order(context, getAccount(), paymentType, null, null);

        order.setCard(getCard());
        order.setPaymentType(paymentType);
        order.save();

        for (OrderLine line : getLines()) {

            if (!line.isAllocated()) {
                line.setOrder(order);
            }

        }

        order.recalc();
        recalc();

        return order;
    }

    /**
     * Transact the card on this order for the total amount and create an invoice for it.
     * <p/>
     * Will throw an exception if the order does not bill properly.
     */
    public Payment transactCard() throws PaymentException, CscException, CardException, CardTypeException, ExpiryDateException, IssueNumberException,
            StartDateException, IOException, CardNumberException, ProcessorException, ExpiredCardException {

        // we must have a card to transact
        if (!hasCard()) {
            logger.fine("cannot transact, no card");
            throw new PaymentException("No card details set");
        }

        // cannot transact if we do not know payment type
        if (paymentType == null) {
            logger.fine("cannot auto transact an order with null payment type");
            throw new PaymentException("No payment type set");
        }

        // payment type must be credit card
        if (paymentType != PaymentType.CardTerminal) {
            logger.fine("cannot auto transact a non credit card order");
            throw new PaymentException("Payment type is not suitable");
        }

        // we must have an available processor
        Processor processor = PaymentSettings.getInstance(context).getDefaultProcessor();
        if (processor == null) {
            logger.fine("no available processor");
            throw new PaymentException("No available processor");
        }

        try {

            Payment payment = processor.transact(this);
            logger.fine("auto transaction order=" + this + ", payment=" + payment);

            // return payment object
            return payment;

        } catch (CardException e) {
            e.printStackTrace();
            declined();
            throw e;

        } catch (ExpiryDateException e) {
            e.printStackTrace();
            declined();
            throw e;

        } catch (IssueNumberException e) {
            e.printStackTrace();
            declined();
            throw e;

        } catch (CardTypeException e) {
            e.printStackTrace();
            declined();
            throw e;

        } catch (CscException e) {
            e.printStackTrace();
            declined();
            throw e;

        } catch (CardNumberException e) {
            e.printStackTrace();
            declined();
            throw e;

        } catch (StartDateException e) {
            e.printStackTrace();
            declined();
            throw e;

        } catch (IOException e) {
            e.printStackTrace();
            declined();
            throw e;

        } catch (ExpiredCardException e) {
            e.printStackTrace();
            declined();
            throw e;

        } catch (ProcessorException e) {
            e.printStackTrace();
            declined();
            throw e;

        } catch (PaymentException e) {
            e.printStackTrace();
            declined();
            throw e;
        }
    }

    public void unauthorise(User user) {

        this.authorised = false;
        save();

        log(user, "Unauthorised");

    }

    /**
     *
     */
    public String getOrderId() {

        OrderSettings orderSettings = OrderSettings.getInstance(context);
        StringBuilder sb = new StringBuilder();

        if (orderSettings.getOrderIdPrefix() != null) {
            sb.append(orderSettings.getOrderIdPrefix());
        }

        sb.append(getIdString());

        if (orderSettings.getOrderIdSuffix() != null) {
            sb.append(orderSettings.getOrderIdSuffix());
        }
        return sb.toString();
    }

    /**
     * An order is completed if it has been invoiced and is not recurring
     */
    public final boolean isCompleted() {
        return invoiced && reinvoiceDate == null;
    }


    public Money getDeliveryCharge() {
        return deliveryCharge;
    }


    public void setDeliveryChargeVat(Money deliveryChargeVat) {
        this.deliveryChargeVat = deliveryChargeVat;
    }


    public void setDeliveryChargeInc(Money deliveryChargeInc) {
        this.deliveryChargeInc = deliveryChargeInc;
    }


    public void setDeliveryDetails(String deliveryDetails) {
        this.deliveryDetails = deliveryDetails;
    }


    public void setDeliveryVatRate(double deliveryVatRate) {
        this.deliveryVatRate = deliveryVatRate;
    }

    public String getReferrer() {
        return referrer;
    }

    public String getGoogleOrderNumber() {
        return googleOrderNumber;
    }

    public String getReason() {
        return reason;
    }

    public String getComment() {
        return comment == null ? "Comments to the buyer" : comment;
    }

    public String getFinansialStatus() {
        return finansialStatus == null ? "REVIEWING" : finansialStatus;
    }

    public String getCardsaveCrossReference() {
        return cardsaveCrossReference;
    }

    public void setGoogleOrderNumber(String googleOrderNumber) {
        this.googleOrderNumber = googleOrderNumber;
    }

    public void setCardsaveCrossReference(String cardsaveCrossReference) {
        this.cardsaveCrossReference = cardsaveCrossReference;
    }

    public void setFinansialStatus(String s) {
        if (s == null) {
            return;
        }

        if (s.equals(finansialStatus)) {
            return;
        }

        this.finansialStatus = s;
    }

    public String getDescription() {
        return "Order N" + getOrderId() + " " + getAccount().getName();
    }

    public final void clearCard() {
        getCard().reset();
    }
}