package org.sevensoft.ecreator.model.ecom.payments.forms.paypal;

import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.CheckoutHandler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.commons.skint.Money;

import java.util.*;
import java.io.*;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.URLDecoder;

/**
 * User: Tanya
 * Date: 23.04.2012
 */
public class PayPalProExpress extends Form {

    private static final String VERSION = "88.0";
    private static final String METHOD_SET_EXPRESS_CHECKOUT = "SetExpressCheckout";
    private static final String METHOD_GET_DETAILS = "GetExpressCheckoutDetails";
    private static final String METHOD_DO_PAYMENT = "DoExpressCheckoutPayment";
    private static final String PAYMENT_ACTION_SALE = "Sale";
    private static final String API_SIGNATURE_TEST_URL = "https://api-3t.sandbox.paypal.com/nvp";
    private static final String API_SIGNATURE_LIVE_URL = "https://api-3t.paypal.com/nvp";
    private static final String SANDBOX_URL = "https://www.sandbox.paypal.com/cgi-bin/webscr";
    private static final String URL = "https://www.paypal.com/cgi-bin/webscr";

    // Map with values from response from PayPal web site
    private Map<String, String> returnMap;

    public PayPalProExpress(RequestContext context) {
        super(context);
    }

    public String getMethod() {
        return "post";
    }

    public Map<String, String> getParams(Payable payable) throws IOException, FormException {
        FormSession session = new FormSession(context, payable);
        PaymentSettings paymentSettings = PaymentSettings.getInstance(context);
        // Map values from which are used in request to PayPal web site
        Map<String, String> map = new LinkedHashMap<String, String>();

        map.put("METHOD", METHOD_SET_EXPRESS_CHECKOUT);
        getMerchantCredentials(paymentSettings, map);

        map.put("PAYMENTREQUEST_0_AMT", payable.getPaymentAmount().toEditString());
        map.put("PAYMENTREQUEST_0_CURRENCYCODE", Currency.getDefault(context).getName());

        String returnUrl = getReturnUrl(session);
        map.put("RETURNURL", returnUrl);

        map.put("CANCELURL", payable.getPaymentFailureUrl(PaymentType.PayPalProExpress));
        map.put("PAYMENTREQUEST_0_PAYMENTACTION", PAYMENT_ACTION_SALE);

        returnMap = httpCall(METHOD_SET_EXPRESS_CHECKOUT, map);

        if ("Success".equals(returnMap.get("ACK"))) {
            map.put("TOKEN", returnMap.get("TOKEN"));
            map.put("cmd", "_express-checkout");
        } else {
            log("[PayPalProExpress] SetExpressCheckout failed");     //todo should not be returned to paypal
        }


        return map;
    }

    private String getReturnUrl(FormSession session) {
        StringBuilder sb = new StringBuilder(Config.getInstance(context).getUrl());
        sb.append("/");
        sb.append(new Link(CheckoutHandler.class, "completed"));
        sb.append("$session=");
        sb.append(session.getTransactionId());
        sb.append("$paymentType=");
        sb.append(PaymentType.PayPalProExpress);
        return sb.toString();
    }

    private void getMerchantCredentials(PaymentSettings paymentSettings, Map<String, String> map) {
        map.put("VERSION", VERSION);

        if (PaymentSettings.getInstance(context).isPayPalProExpressLive()) {
            map.put("USER", paymentSettings.getPayPalProExpressApiUsername());
            map.put("PWD", paymentSettings.getPayPalProExpressApiPassword());
            map.put("SIGNATURE", paymentSettings.getPayPalProExpressSignature());
        } else {
            if (PaymentSettings.getInstance(context).hasPayPalProDetails()) {
                map.put("USER", paymentSettings.getPayPalProExpressApiUsername());
                map.put("PWD", paymentSettings.getPayPalProExpressApiPassword());
                map.put("SIGNATURE", paymentSettings.getPayPalProExpressSignature());
            } else {
                map.put("USER", "sdk-three_api1.sdk.com");
                map.put("PWD", "QFZCWN5HZM8VBG7Q");
                map.put("SIGNATURE", "A-IzJhZZjhg29XQ2qnhapuwxIDzyAZQ92FRP5dqBzVesOkzbdUONzmOU");
            }
        }
    }

    public String getPaymentUrl() {
        if (PaymentSettings.getInstance(context).isPayPalProExpressLive())
            return new StringBuilder(URL).append("?cmd=_express-checkout&token=").append(returnMap.get("TOKEN")).toString();
        else
            return new StringBuilder(SANDBOX_URL).append("?cmd=_express-checkout&token=").append(returnMap.get("TOKEN")).toString();
    }

    public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {

        if (params.get("session") == null) {
            log("[PayPalProExpress] no SESSION was returned with callback");
        }

        FormSession session = FormSession.get(context, params.get("session"));
        if (session == null) {
            log("[PayPalProExpress] no session found with transaction ID=" + params.get("session"));
            return;
        }
        PaymentSettings paymentSettings = PaymentSettings.getInstance(context);

        Map<String, String> map = new LinkedHashMap<String, String>();
        getMerchantCredentials(paymentSettings, map);
        map.put("METHOD", METHOD_GET_DETAILS);
        map.put("TOKEN", params.get("token"));

        returnMap = httpCall(METHOD_GET_DETAILS, map);

        if (!"Success".equals(returnMap.get("ACK"))) {
            log("[PayPalProExpress] GetExpressCheckoutDetails failed");
            return;
        }

        map.put("METHOD", METHOD_DO_PAYMENT);
        map.put("PAYERID", returnMap.get("PAYERID"));
        map.put("PAYMENTREQUEST_0_AMT", returnMap.get("AMT"));
        map.put("PAYMENTREQUEST_0_CURRENCYCODE", Currency.getDefault(context).getName());
        map.put("PAYMENTREQUEST_0_PAYMENTACTION", PAYMENT_ACTION_SALE);

        returnMap = httpCall(METHOD_DO_PAYMENT, map);
        if (!"Success".equals(returnMap.get("ACK"))) {
            log("[PayPalProExpress] DoExpressCheckoutPayment failed");
            return;
        }

        Payment payment = getPayment(session, returnMap);
        if (payment == null) {
            return;
        }

        session.callback(params, ipAddress, payment);
    }

    public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
        return null;
    }

    public HashMap httpCall(String methodName, Map<String, String> params) {

        String encodedData = formatUrl(params);
        String respText = "";
        HashMap nvp = new HashMap();

        try {
            URL postURL;
            if (PaymentSettings.getInstance(context).isPayPalProExpressLive()) {
                postURL = new URL(API_SIGNATURE_LIVE_URL);
            } else {
                postURL = new URL(API_SIGNATURE_TEST_URL);
            }
            HttpURLConnection conn = (HttpURLConnection) postURL.openConnection();

            conn.setDoInput(true);
            conn.setDoOutput(true);

            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            conn.setRequestProperty("Content-Length", String.valueOf(encodedData.length()));
            conn.setRequestMethod("POST");

            DataOutputStream output = new DataOutputStream(conn.getOutputStream());
            output.writeBytes(encodedData);
            output.flush();
            output.close();

            DataInputStream in = new DataInputStream(conn.getInputStream());
            int rc = conn.getResponseCode();
            if (rc != -1) {
                BufferedReader is = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String _line = null;
                while (((_line = is.readLine()) != null)) {
                    respText = respText + _line;
                }
                nvp = deformatNVP(respText);
            }
            return nvp;

        } catch (IOException e) {
            return null;
        }
    }

    private String formatUrl(Map<String, String> params) {
        StringBuilder sb = new StringBuilder();
        Iterator<Map.Entry<String, String>> iter = params.entrySet().iterator();
        while (iter.hasNext()) {

            Map.Entry<String, String> entry = iter.next();

            sb.append(entry.getKey());
            sb.append("=");
            sb.append(entry.getValue());

            if (iter.hasNext()) {
                sb.append("&");
            }
        }
        return sb.toString();
    }

    public HashMap deformatNVP(String pPayload) {
        HashMap nvp = new HashMap();
        StringTokenizer stTok = new StringTokenizer(pPayload, "&");
        while (stTok.hasMoreTokens()) {
            StringTokenizer stInternalTokenizer = new StringTokenizer(stTok.nextToken(), "=");
            if (stInternalTokenizer.countTokens() == 2) {
                String key = URLDecoder.decode(stInternalTokenizer.nextToken());
                String value = URLDecoder.decode(stInternalTokenizer.nextToken());
                nvp.put(key.toUpperCase(), value);
            }
        }
        return nvp;
    }

    private Payment getPayment(FormSession session, Map<String, String> params) {

        String transId = params.get("PAYMENTINFO_0_TRANSACTIONID");
        String amount = params.get("PAYMENTINFO_0_AMT");
        if (transId == null) {
            return null;
        }

        // Check for duplicated payments!!!!
        if (Payment.exists(context, transId, PaymentType.PayPalProExpress)) {
            return null;
        }

        Payment payment = new Payment(context, session.getAccount(), new Money(amount), PaymentType.PayPalProExpress);
        payment.setProcessorTransactionId(transId);
        payment.setPaymentType(PaymentType.PayPalProExpress);

        payment.save();

        return payment;
    }
}
