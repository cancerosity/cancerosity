package org.sevensoft.ecreator.model.ecom.payments.forms.able2buy;

import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Dec 2006 10:26:07
 *
 */
public class CreditGroupInit {

	private final RequestContext	context;

	public CreditGroupInit(RequestContext context) {
		this.context = context;
	}

	public void run() {

		new CreditGroup(context, "STDBNP2980480300", "Buy Now Pay 3 months Later at 29.90% APR variable", 29.9, 1, 3, 1);
		new CreditGroup(context, "STDBNP2980480600", "Buy Now Pay 6 months Later at 29.90% APR variable", 29.9, 1, 6, 1);
		new CreditGroup(context, "STDBNP2980480300", "Buy Now Pay 9 months Later at 29.90% APR variable", 29.9, 1, 9, 1);
		new CreditGroup(context, "STDBNP2980481200", "Buy Now Pay 12 months Later at 29.90% APR variable", 29.9, 1, 12, 1);

		new CreditGroup(context, "STDIB2680120000", "Interest Bearing at 26.90% APR variable over 12 months", 26.9, 12, 0, 1);
		new CreditGroup(context, "STDIB2490360000", "Interest Bearing at 24.90% APR variable over 36 months", 24.9, 36, 0, 1);
		new CreditGroup(context, "STDIB2490480000", "Interest Bearing at 24.90% APR variable over 48 months", 24.9, 48, 0, 1);
		new CreditGroup(context, "STDIB2980480000", "Interest Bearing at 29.90% APR variable over 48 months", 29.9, 48, 0, 1);

		new CreditGroup(context, "STDIF0000060000", "Interest Free over 6 months ", 0, 1, 6, 0.1);
		new CreditGroup(context, "STDIF0000090000", "Interest Free over 9 months ", 0, 1, 9, 0.1);

	}

}
