package org.sevensoft.ecreator.model.ecom.orders.emails;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 7 Feb 2007 14:36:42
 * 
 * This emails are sent out on receipt of an order.
 * 
 *
 */
@Table("orders_emails")
public class OrderEmail extends EntityObject {

	public static List<OrderEmail> get(RequestContext context) {
		return SimpleQuery.execute(context, OrderEmail.class);
	}

	private List<String>	recipients;
	private String		body;
	private String		subject;
	private String		linesBody;
	private String		name;

	private OrderEmail(RequestContext context) {
		super(context);
	}

	public OrderEmail(RequestContext context, String name) {
		super(context);
		this.name = name;
		save();
	}

	public final String getBody() {
		return body;
	}

	public final String getName() {
		return name;
	}

	public final List<String> getRecipients() {
		if (recipients == null) {
			recipients = new ArrayList();
		}
		return recipients;
	}

	public final String getSubject() {
		return subject;
	}

	public boolean hasBody() {
		return body != null;
	}

	public void send(Order order) throws EmailAddressException, SmtpServerException {

		Config config = Config.getInstance(context);

		Email e = new Email();
		e.setFrom(config.getServerEmail());
		e.setRecipients(getRecipients());
		e.setSubject(subject);

		String string = new OrderEmailParser(context).parse(order, body, linesBody);

		e.setBody(string);

		new EmailDecorator(context, e).send(config.getSmtpHostname());
	}

	public final void setBody(String body) {
		this.body = body;
	}

	public final void setName(String name) {
		this.name = name;
	}

	public final void setRecipients(List<String> emails) {
		this.recipients = emails;
	}

	public final void setSubject(String subject) {
		this.subject = subject;
	}

	public String getLinesBody() {
		return linesBody;
	}

	public void setLinesBody(String linesBody) {
		this.linesBody = linesBody;
	}

}
