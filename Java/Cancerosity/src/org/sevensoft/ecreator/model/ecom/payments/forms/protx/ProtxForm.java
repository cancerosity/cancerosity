package org.sevensoft.ecreator.model.ecom.payments.forms.protx;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.gaia.Country;
import org.sevensoft.ecreator.model.design.marker.MarkerRenderer;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.ecom.payments.results.Callback;
import org.sevensoft.ecreator.model.ecom.payments.results.AvsCheck.AvsResult;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * TEST CARD NUMBERS
 *
 * Card Type Card Number Issue Number
 *
 * VISA 4929 0000 0000 6 None
 *
 * MasterCard 5404 0000 0000 0001 None
 *
 * Delta 4462 0000 0000 0003 None
 *
 * Solo 6334 9000 0000 0005 1
 *
 * Switch/UK Maestro 5641 8200 0000 0005 01
 *
 * AMEX 3742 0000 0000 004 None
 *
 * Diner�s Club 3600 0000 0000 08 None
 * 
 * @author sks
 */
public class ProtxForm extends Form {

	// ** Live site **
    private static final String	LiveUrl	= "https://live.sagepay.com/gateway/service/vspform-register.vsp";
//	private static final String	LiveUrl	= "https://ukvps.protx.com/vps2Form/submit.asp";
	//	private static final String	LiveAdmin		= "https://ukvps.protx.com/VSPAdmin";

	//	private enum Status {
	//		ABORT, ERROR, INVALID, MALFORMED, NOTAUTHED, OK
	//	}

	// ** Test site **
    private static final String	SandboxUrl	=  "https://test.sagepay.com/Simulator/VSPFormGateway.asp";   //Sage Pay simulator
    private static final String	TestUrl	= "https://test.sagepay.com/gateway/service/vspform-register.vsp";
//	private static final String	TestUrl	= "https://ukvpstest.protx.com/vps2Form/submit.asp";

	public ProtxForm(RequestContext context) {
		super(context);
	}

	//	private static final String	ShowPostUrl		= "https://ukvps.protx.com/showpost/showpost.asp";

	//	// ** Simulator site **
	//	private static final String	SimulatorUrl	= "https://ukvpstest.protx.com/VSPSimulator/VSPFormGateway.asp";

	//	private static final String	TestAdmin		= "https://ukvpstest.protx.com/VSPAdmin";

	private Map<String, String> decryptParams(String string, Map<String, String> params) throws IOException {

		sun.misc.BASE64Decoder decoder = new sun.misc.BASE64Decoder();
		byte[] decoded = decoder.decodeBuffer(string);
		string = new String(StringHelper.simpleXor(decoded, PaymentSettings.getInstance(context).getProtxEncryptionPassword()));

		for (String param : string.split("&")) {

			String[] keyvalue = param.split("=");
			if (keyvalue != null && keyvalue.length == 2) {
				params.put(keyvalue[0], keyvalue[1]);
			}

		}

		logger.fine("[ProtxForm] Decrypted parameters=" + params);
		return params;
	}

	private Map<String, String> encrypt(Map<String, String> params) {

		Map<String, String> map = new HashMap<String, String>();

		map.put("VPSProtocol", "2.23");

        String type = PaymentSettings.getInstance(context).getProtxPaymentType();
        if (type == null) {
            type = PaymentSettings.PROTX_TXTYPE_PAYMENT;
        }
        map.put("TxType", type);

        map.put("Vendor", PaymentSettings.getInstance(context).getProtxVendorName());
		map.put("Crypt", encryptParams(params));

        log("Params sent to SagePay: " + params);
        
        return map;
	}

	private String encryptParams(Map<String, String> params) {

		PaymentSettings ps = PaymentSettings.getInstance(context);
		if (!ps.hasProtxEncryptionPassword()) {
			throw new RuntimeException("No encryption password");
		}

		StringBuilder sb = new StringBuilder();
		Iterator<Map.Entry<String, String>> iter = params.entrySet().iterator();
		while (iter.hasNext()) {

			Map.Entry<String, String> entry = iter.next();

			sb.append(entry.getKey());
			sb.append("=");
			sb.append(entry.getValue());

			if (iter.hasNext()) {
				sb.append("&");
			}              
		}

		sun.misc.BASE64Encoder encoder = new sun.misc.BASE64Encoder();
		byte[] xor = StringHelper.simpleXor(sb.toString().getBytes(), ps.getProtxEncryptionPassword());
		String crypt = encoder.encode(xor);
		crypt = crypt.replaceAll("[\n\r]", "");
		return crypt;
	}

	private AvsResult getAvsResult(Map<String, String> params) {

		String AVSCV2 = params.get("AVSCV2");

		if ("ADDRESS MATCH ONLY".equals(AVSCV2)) {
			return AvsResult.Match;
		}

		else if ("ALL MATCH".equals(AVSCV2)) {
			return AvsResult.Match;
		}

		else if ("NO DATA MATCHES".equals(AVSCV2)) {
			return AvsResult.Fail;
		}

		else if ("SECURITY CODE MATCH ONLY".equals(AVSCV2)) {
			return AvsResult.Fail;
		}

		return null;

	}

	private String getBasketString(Order order) {

		StringBuilder sb = new StringBuilder();
		List<OrderLine> lines = order.getLines();
		Integer numOfLines = lines.size();
        if (order.hasDeliveryDetails()) {
            numOfLines++;
        }
        sb.append(numOfLines);

		for (OrderLine line : lines) {
			sb.append(":");
			sb.append(line.getDescription().replaceAll("[:=&]", ""));
			sb.append(":");
			sb.append(line.getQty());
			sb.append(":");
			sb.append(line.getUnitSellEx());
			sb.append(":");
			sb.append(line.getUnitSellVat());
			sb.append(":");
			sb.append(line.getUnitSellInc());
			sb.append(":");
			sb.append(line.getLineSellInc());
		}
        if (order.hasDeliveryDetails()) {
            sb.append(":");
            sb.append(order.getDeliveryDetails().replaceAll("[:=&]", ""));
            sb.append(":1:");
            sb.append(order.getDeliveryChargeEx());
            sb.append(":");
            sb.append(order.getDeliveryChargeVat());
            sb.append(":");
            sb.append(order.getDeliveryChargeInc());
            sb.append(":");
            sb.append(order.getDeliveryChargeInc());
        }

        return sb.toString();
	}

	@Override
	public String getMethod() {
		return "POST";
	}

	@Override
	public Map<String, String> getParams(Payable payable) {

		PaymentSettings paymentSettings = PaymentSettings.getInstance(context);
		FormSession session = new FormSession(context, payable);

		Map<String, String> params = new LinkedHashMap();

		params.put("VendorTxCode", session.getTransactionId());
		params.put("Currency", "GBP");
		params.put("Amount", payable.getPaymentAmount().toEditString());
		params.put("CustomerName", payable.getPayableAccount().getName());

		if (paymentSettings.hasProtxVendorEmail()) {

			logger.fine("[ProtxForm] adding vendor email=" + paymentSettings.getProtxVendorEmail());
			params.put("VendorEmail", paymentSettings.getProtxVendorEmail());
		}

		if (paymentSettings.hasProtxEmailHeader()) {

			String msg = MarkerRenderer.render(context, paymentSettings.getProtxEmailHeader(), payable);
			//			logger.fine("[ProtxForm] adding email message=" + msg);
			params.put("eMailMessage", msg);
		}

		if (payable.getPayableAccount().hasEmail()) {
			params.put("CustomerEmail", payable.getPayableAccount().getEmail());
		}

		params.put("Description", Config.getInstance(context).getUrl() + ": " + payable.getPaymentDescription());
		String url = payable.getPaymentSuccessUrl(PaymentType.ProtxForm);
		if (url.contains("?")) {
			url = url + "&paymentType=" + PaymentType.ProtxForm.name();
		} else {
			url = url + "?paymentType=" + PaymentType.ProtxForm.name();
		}

		params.put("SuccessURL", url);
		params.put("FailureURL", payable.getPaymentFailureUrl(PaymentType.ProtxForm));

        if (payable instanceof Order) {

            Order order = (Order) payable;

            params.put("Basket", getBasketString(order));

            params.put("BillingSurname", order.getBillingAddress().getLastName());
            params.put("BillingFirstnames", order.getBillingAddress().getFirstName());
            params.put("BillingAddress1", order.getBillingAddress().getAddressLine1());
            params.put("BillingCity", order.getBillingAddress().getTown());
            params.put("BillingPostCode", order.getBillingAddress().getPostcode());
            params.put("BillingCountry", order.getBillingAddress().getCountry().getIsoAlpha2());
            if (order.getBillingAddress().getCountry().equals(Country.US)) {
                params.put("BillingState", order.getBillingAddress().getState());
            }

            params.put("DeliveryFirstnames", order.getDeliveryAddress().getFirstName());
            params.put("DeliverySurname", order.getDeliveryAddress().getLastName());
            params.put("DeliveryAddress1", order.getDeliveryAddress().getAddressLine1());
            params.put("DeliveryCity", order.getDeliveryAddress().getTown());
            params.put("DeliveryPostCode", order.getDeliveryAddress().getPostcode());
            params.put("DeliveryCountry", order.getDeliveryAddress().getCountry().getIsoAlpha2());
            if (order.getDeliveryAddress().getCountry().equals(Country.US)) {
                params.put("DeliveryState", order.getDeliveryAddress().getState());
            }

        }

		logger.fine("[ProtxForm] params=" + params);
		return encrypt(params);
	}

	private Payment getPayment(Item account, Map<String, String> params) {

		String VendorTxCode = params.get("VendorTxCode");
		String VPSTxId = params.get("VPSTxId");
		String TxAuthNo = params.get("TxAuthNo");                        
		Money amount;
		if (params.containsKey("Amount")) {
			amount = new Money(params.get("Amount"));
		} else {
			amount = new Money(0);
			System.out.println("Protx params: " + params);
		}

		if (VPSTxId == null) {
			return null;
		}

		// Check for duplicated payments!!!!
		if (Payment.exists(context, VPSTxId, PaymentType.ProtxForm)) {
			return null;
		}

		Payment payment = new Payment(context, account, amount, PaymentType.ProtxForm);
		payment.setProcessorTransactionId(VPSTxId);
		payment.setAuthCode(TxAuthNo);
		payment.setInternalTransactionId(VendorTxCode);
		payment.setPaymentType(PaymentType.ProtxForm);

		payment.save();

		return payment;
	}

	@Override
	public String getPaymentUrl() {
        PaymentSettings settings = PaymentSettings.getInstance(context);
        if (settings.isProtxSandbox()){
            return SandboxUrl;
        }
		return settings.isProtxLive() ? LiveUrl : TestUrl;
	}

	@Override
	public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {

		logger.severe("[ProtxForm] params=" + params);

		String crypt = params.get("crypt");
		decryptParams(crypt, params);

		new Callback(context, PaymentType.ProtxForm, params);

		String status = params.get("Status");

		logger.fine("[ProtxForm] Protx STATUS: " + status);

		if (!"OK".equals(status)) {
            logger.severe("[ProtxForm] Protx STATUS: " + status);
			return;
		}

		/*
		 * Check for duplicated payments to make sure someone isn't running this transaction through again
		 * (By simply refreshing the completion page!)
		 */
		String VPSTxId = params.get("VPSTxId");
		if (Payment.exists(context, VPSTxId, PaymentType.ProtxForm)) {
			logger.severe("[ProtxForm] Rejecting duplicated callback");
			return;
		}

		String txnId = params.get("VendorTxCode");

		FormSession session = FormSession.get(context, txnId);
		if (session == null) {
			logger.severe("[ProtxForm] No payment session found for tx=" + txnId);
			return;
		}

		session.callback(params, ipAddress, getPayment(session.getAccount(), params));
		session.delete();
	}

	@Override
	public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
		return null;
	}

}