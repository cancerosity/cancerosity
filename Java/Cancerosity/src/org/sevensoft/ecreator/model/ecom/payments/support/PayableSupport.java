package org.sevensoft.ecreator.model.ecom.payments.support;

import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.forms.able2buy.CreditGroup;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 10 Jan 2007 06:39:58
 *
 */
public abstract class PayableSupport extends EntityObject implements Payable {

	private PaymentType	paymentType;
	private CreditGroup	creditGroup;
	private Card		card;

	protected PayableSupport(RequestContext context) {
		super(context);
	}

	public CreditGroup getAble2BuyCreditGroup() {
		return (CreditGroup) (creditGroup == null ? null : creditGroup.pop());
	}

	public Card getCard() {
		return (Card) (card == null ? null : card.pop());
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public boolean hasCard() {
		return card != null;
	}

	public boolean hasPaymentType() {
		return paymentType != null;
	}

	public void reset() {
		this.paymentType = null;
		this.card = null;
		this.creditGroup = null;
	}

	public void setCard(Card card) {
		this.card = card;
		save("card");
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
		save("paymentType");
	}

}
