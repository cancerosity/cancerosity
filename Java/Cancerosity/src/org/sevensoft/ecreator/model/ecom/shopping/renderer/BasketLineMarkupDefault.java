package org.sevensoft.ecreator.model.ecom.shopping.renderer;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * @author sks 29 Jun 2006 08:46:24
 *
 */
public class BasketLineMarkupDefault implements MarkupDefault {

	public String getBetween() {
		return null;
	}

	public String getCss() {
		return null;
	}

	public String getBody() {

		StringBuilder sb = new StringBuilder();

 		sb.append("<tr class='line'>\n\n");

		/*
		 * Image - only if line is an item line
		 */
		sb.append("<td id='image' valign='top' align='center'>[thumbnail?w=60&h=60&limit=1&link=1]</td>\n");

		// ITEM details
		sb.append("<td><strong>[item?link=1]</strong><br />[options?sep=:]<br />[stock]</td>\n");

		// QTY
		sb.append("<td>[ordering_qty]</td>\n");

		// PRICE
		sb.append("<td class='price'>[pricing_sell?inc=1]</td>\n");

		// LINE AMOUNT
		sb.append("<td class='price' align='right'>[total]</td>\n\n");

		sb.append("</tr>");

		return sb.toString();
	}

	public String getEnd() {
		return null;
	}

	public String getName() {
		return null;
	}

	public String getStart() {
		return null;
	}

	public int getTds() {
		return 0;
	}

}
