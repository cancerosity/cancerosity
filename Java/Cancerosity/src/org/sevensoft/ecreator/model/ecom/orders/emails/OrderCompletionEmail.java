package org.sevensoft.ecreator.model.ecom.orders.emails;

import java.util.List;

import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.ecreator.model.ecom.orders.parcels.Parcel;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 18-Jul-2005 00:46:18
 * 
 */
public class OrderCompletionEmail extends AbstractOrderEmail {

	public OrderCompletionEmail(RequestContext context, Order order) {
		super(context, order);

		setFrom(config.getServerEmail());
		setSubject(Company.getInstance(context).getName() + " - Order Information #" + order.getOrderId());
		setTo(order.getAccount().getEmail());
		setBody(getBody());
	}

	public String getBody() {

		OrderSettings os = OrderSettings.getInstance(context);
		String body = os.getCompletionEmailBody();

		body = body.replace("[parcels]", getParcelDetails());

		return body;
	}

	private CharSequence getParcelDetails() {

		List<Parcel> parcels = order.getParcels();
		if (parcels.isEmpty()) {
			return "";
		}

		StringBuilder sb = new StringBuilder();
		sb.append("These are the tracking numbers for your delivery:\n\n");

		for (Parcel parcel : parcels) {
			sb.append(parcel.getCourier() + ": " + parcel.getConsignmentNumber());
			sb.append("\n");
		}

		return sb.toString();
	}

}
