package org.sevensoft.ecreator.model.ecom.orders.export.csv.ecreator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.jeezy.http.RequestContext;

import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.CsvSaver;
import com.ricebridge.csvman.QuoteType;

/**
 * @author sks 22 Aug 2006 11:12:45
 * 
 *         This class will output the lines of one or more orders into a single
 *         file.
 * 
 */
public class OrderLinesCsv {

	private final Collection<Order> orders;

	public OrderLinesCsv(Order order, RequestContext context) {
		this(context, Collections.singletonList(order));
	}

	public OrderLinesCsv(RequestContext context, Collection<Order> orders) {
		this.orders = orders;
	}

	public OrderLinesCsv(RequestContext context, Order order) {
		this(context, Collections.singleton(order));
	}

	private List<String> getDeliveryRow(Order order) {

		List<String> row = new ArrayList<String>();

		// order details
		row.add(order.getIdString());
		row.add(order.getReference());

		row.add("Delivery");

		row.add(null);
		row.add(order.getDeliveryCode());

		row.add(order.getDeliveryDetails());
		row.add(null);
		row.add("1");

		row.add(order.getDeliveryCostEx().toEditString());

		// unit prices
		row.add(order.getDeliveryChargeEx().toEditString());
		row.add(order.getDeliveryChargeVat().toEditString());
		row.add(order.getDeliveryChargeInc().toEditString());

		// line prices
		row.add(order.getDeliveryChargeEx().toEditString());
		row.add(order.getDeliveryChargeVat().toEditString());
		row.add(order.getDeliveryChargeInc().toEditString());

		CollectionsUtil.replaceNulls(row, "");

		return row;
	}

	private String[] getHeader() {

		List<String> row = new ArrayList<String>();

		// order details
		row.add("Order ID");
		row.add("Order Reference");

		row.add("Line ID");

		row.add("Item ID");
		row.add("Item Reference");
		row.add("ISBN");
		row.add("Description");
		row.add("Options details");
		row.add("Qty");

		row.add("Cost price ex");

		// unit prices
		row.add("Unit price ex");
		row.add("Unit price vat");
		row.add("Unit price inc");

		// line prices
		row.add("Line price ex");
		row.add("Line price vat");
		row.add("Line price inc");

		return row.toArray(new String[] {});
	}

	protected String[] getRow(OrderLine line) {

		List<String> row;
		row = new ArrayList<String>();

		// order details
		row.add(line.getOrder().getIdString());
		row.add(line.getOrder().getReference());

		row.add(line.getIdString());

		if (line.hasItem()) {
			row.add(line.getItem().getIdString());
			row.add(line.getItem().getReference());
		} else {
			row.add(null);
			row.add(null);
		}
		// set ISBN
		if (line.hasItem()) {

			String isbn = null;
			try {
				isbn = line.getItem().getAttributeValue("ISBN");

			} catch (Exception ex) {

			}
			if (isbn == null) {
				try {
					isbn = line.getItem().getAttributeValue(70);

				} catch (Exception ex) {

				}
			}
			row.add(isbn);
		} else {
			row.add(null);
		}

		row.add(line.getDescription());
		row.add(line.getOptionsDetails());
		row.add(String.valueOf(line.getQty()));

		row.add(line.getCostPriceEx().toEditString());

		// unit prices
		row.add(line.getUnitSellEx().toEditString());
		row.add(line.getUnitSellVat().toEditString());
		row.add(line.getUnitSellInc().toEditString());

		// line prices
		row.add(line.getLineSellEx().toEditString());
		row.add(line.getLineSellVat().toEditString());
		row.add(line.getLineSellInc().toEditString());

		CollectionsUtil.replaceNulls(row, "");

		return row.toArray(new String[] {});
	}

	public void output(Writer writer) {

		CsvManager csv = new CsvManager();
		csv.setSeparator(",");
		csv.setQuoteType(QuoteType.AsNeeded);
		csv.setQuote('"');
		csv.setEscape('\\');

		CsvSaver saver = csv.makeSaver(writer);

		saver.begin();
		saver.next(getHeader());

		for (Order order : orders) {
			for (OrderLine line : order.getLines()) {
				saver.next(getRow(line));
			}
		}

		saver.end();
	}

	public void output(File file) throws IOException {

		FileWriter writer = null;

		try {

			writer = new FileWriter(file);
			output(writer);

		} catch (IOException e) {
			e.printStackTrace();
			throw e;

		} finally {

			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
