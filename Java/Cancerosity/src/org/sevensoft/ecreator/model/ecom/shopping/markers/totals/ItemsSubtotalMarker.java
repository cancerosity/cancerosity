package org.sevensoft.ecreator.model.ecom.shopping.markers.totals;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketLineMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderMarker;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Jun 2006 11:46:17
 * 
 * Returns total of just the items bought
 *
 */
public class ItemsSubtotalMarker implements IBasketMarker, IOrderMarker, IBasketLineMarker {

	public Object generate(RequestContext context, Map<String, String> params, Basket basket) {

		Currency currency = (Currency) context.getAttribute("currency");
		return currency.getSymbol() + currency.convert(basket.getLinesEx()).toString(2);
	}

	public Object generate(RequestContext context, Map<String, String> params, BasketLine line) {

		Currency currency = (Currency) context.getAttribute("currency");
		return currency.getSymbol() + currency.convert(line.getLineSalePriceEx()).toString(2);
	}

	public Object generate(RequestContext context, Map<String, String> params, Order order) {
		return (String) context.getAttribute("currencySymbol") + order.getItemsSubtotal();
	}

	public Object getRegex() {
		return "items_subtotal";
	}

}
