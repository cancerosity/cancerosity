package org.sevensoft.ecreator.model.ecom.shopping.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.ecom.shopping.CheckoutHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 03-Apr-2006 11:44:59
 *
 */
public final class CheckoutMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.Shopping.enabled(context)) {
			return null;
		}

		if (!params.containsKey("text"))
			params.put("text", "Checkout");

		return super.link(context, params, new Link(CheckoutHandler.class, "reset"), "link_checkout");
	}

	public Object getRegex() {
		return "checkout";
	}
}