package org.sevensoft.ecreator.model.ecom.orders.emails;

import java.io.File;
import java.io.IOException;

import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.hardcopy.InvoiceHtml;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Nov 2006 18:56:50
 * 
 * This email attaches a html file containing the invoice
 *
 */
public class InvoiceEmail extends Email {

	private Order		order;
	private RequestContext	context;
	private File		file;
	private Company		company;
	private Config		config;

	public InvoiceEmail(RequestContext context, Order order) throws IOException {
		this.context = context;
		this.order = order;
		this.company = Company.getInstance(context);
		this.config = Config.getInstance(context);

		// generate the HTML invoice and write to a file
		file = File.createTempFile("invoice_html", null);
		SimpleFile.writeString(new InvoiceHtml(context, order).toString(), file);

		super.setSubject(Company.getInstance(context).getName() + " - Invoice #" + order.getOrderId());
		super.setBody("Your invoice is attached");
		super.setFrom(config.getServerEmail());
        super.setTo(order.getAccount().getEmail());

		super.addAttachment(file, order.getId() + ".html");
	}

	public void close() {
		if (file != null)
			file.delete();
		file = null;
	}

	@Override
	protected void finalize() throws Throwable {
		if (file != null)
			file.delete();
		file = null;
	}

	@Override
	public void send(String hostname) throws EmailAddressException, SmtpServerException {
		new EmailDecorator(context, this).send(hostname);
	}

	@Override
	public void send(String hostname, String username, String password) throws EmailAddressException, SmtpServerException {
		super.send(hostname, username, password);
	}

}
