package org.sevensoft.ecreator.model.ecom.payments.processors.securetrading;

import org.jdom.Document;
import org.jdom.Element;

/**
 * @author sks 19 Apr 2007 10:23:15
 *
 */
public class SecPayRequest {

	public Document ad() {

		Element root = new Element("XMLRequest");
		Document doc = new Document(root);

		root.addContent(new Element("merchant", "secpay"));
		root.addContent(new Element("trans_id", "secpay"));
		root.addContent(new Element("amount", "secpay"));
		root.addContent(new Element("options", "secpay"));
		root.addContent(new Element("card_no", "secpay"));
		root.addContent(new Element("expiry", "secpay"));
		root.addContent(new Element("customer", "secpay"));
		root.addContent(new Element("merchant", "secpay"));
		root.addContent(new Element("merchant", "secpay"));
		root.addContent(new Element("merchant", "secpay"));

		return doc;

	}
}
