package org.sevensoft.ecreator.model.ecom.payments.reports;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.Row;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Apr 2007 08:57:12
 *
 */
public class PaymentReport {

	/**
	 * 
	 */
	public static List<PaymentReport> getDaily(RequestContext context, Date start, Date end) {

		Query q = new Query(context, "select date, count(*), sum(amount) from # where date>=? and date<=? group by date");
		q.setParameter(start);
		q.setParameter(end);

		List<Row> rows = q.execute();
		List<PaymentReport> list = new ArrayList();
		for (Row row : rows) {
			list.add(new PaymentReport(row.getDate(0), row.getInt(1), row.getMoney(2)));
		}

		return list;
	}

	public static List<PaymentReport> getMonthly(RequestContext context, Date start, Date end) {

		Query q = new Query(context, "select date, count(*), sum(amount), date_format(date / 1000, '%M%Y') df from # where date>=? and date<=? group by df");
		q.setParameter(start);
		q.setParameter(end);

		List<Row> rows = q.execute();
		List<PaymentReport> list = new ArrayList();
		for (Row row : rows) {
			list.add(new PaymentReport(row.getDate(0).beginMonth(), row.getInt(1), row.getMoney(2)));
		}

		return list;
	}

	private Date	date;
	private int		count;

	private Money	total;

	private PaymentReport(Date date, int count, Money total) {
		this.date = date;
		this.count = count;
		this.total = total;
	}

	public final int getCount() {
		return count;
	}

	public final Date getDate() {
		return date;
	}

	public final Money getTotal() {
		return total;
	}

}
