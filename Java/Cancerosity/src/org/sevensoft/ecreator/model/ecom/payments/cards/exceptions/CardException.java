package org.sevensoft.ecreator.model.ecom.payments.cards.exceptions;

/**
 * @author sks 26-Oct-2005 10:57:45
 * 
 */
public class CardException extends Exception {

	public CardException() {
	}

	public CardException(String message) {
		super(message);
	}

	public CardException(String message, Throwable cause) {
		super(message, cause);
	}

	public CardException(Throwable cause) {
		super(cause);
	}

}
