package org.sevensoft.ecreator.model.ecom.payments.forms.realex;

import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormSession;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.results.Callback;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.httpc.HttpMethod;
import org.sevensoft.commons.skint.Money;
import org.joda.time.DateTime;

import java.util.Map;
import java.util.HashMap;
import java.util.Formatter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * User: Tanya
 * Date: 04.01.2013
 */
public class GlobalIrisRealAuth extends Form {

    private static final String REDIRECT_URL = "https://epage.payandshop.com/epage.cgi";

    private static final String DELIMITER = ".";
    private static final String TIMESTAMP_PARAM = "TIMESTAMP";
    private static final String MERCHANT_ID_PARAM = "MERCHANT_ID";
    private static final String ORDER_ID_PARAM = "ORDER_ID";
    private static final String AMOUNT_PARAM = "AMOUNT";
    private static final String CURRENCY_PARAM = "CURRENCY";
    private static final String SHA1HASH_PARAM = "SHA1HASH";
    private static final String CUSTOM_TRANS_ID_PARAM = "TRANS_ID";

    /**
     * error codes
     */
    private static final String CODE_SUCCESSFUL = "00";
    private static final String CODE_DECLINED = "101";
    private static final String CODE_REFERRAL_B = "102";
    private static final String CODE_REFERRAL_A = "103";
    private static final String CODE_Comms = "205";


    public GlobalIrisRealAuth(RequestContext context) {
        super(context);
    }

    public String getMethod() {
        return HttpMethod.Post.toString();
    }

    public Map<String, String> getParams(Payable payable) throws IOException, FormException {
        FormSession session = new FormSession(context, payable);

        Map<String, String> params = new HashMap<String, String>();
        PaymentSettings settings = PaymentSettings.getInstance(context);

        params.put(MERCHANT_ID_PARAM, settings.getGlobalirisMerchantId());
        params.put(ORDER_ID_PARAM, payable.getIdString());
        params.put("ACCOUNT", settings.getGlobalirisSubaccountName());
        params.put(AMOUNT_PARAM, payable.getPaymentAmount().getAmountString());
        params.put(CURRENCY_PARAM, Currency.getDefault(context).getName());
        params.put(TIMESTAMP_PARAM, new DateTime().toString("yyyyMMddhhmmss"));
        params.put(SHA1HASH_PARAM, getHash(params, settings.getGlobalirisSecretKey()));
        params.put("AUTO_SETTLE_FLAG", settings.isGlobalirisAutoSettle() ? "1" : "0");
        if (payable instanceof Order) {
            Order order = (Order) payable;
            params.put("COMMENT1", order.getDescription());
            // _CODE should contain numbers from postcode and address
//            params.put("SHIPPING_CODE", order.getDeliveryAddress().getPostcode());
//            params.put("SHIPPING_CO", order.getDeliveryCountry().toString());
//            params.put("BILLING_CODE", order.getBillingAddress().getPostcode());
//            params.put("BILLING_CO", order.getBillingAddress().getCountry().toString());
            params.put("CUST_NUM", order.getAccount().getIdString());

        }
        params.put(CUSTOM_TRANS_ID_PARAM, session.getTransactionId());
        return params;
    }

    public String getPaymentUrl() {
        return REDIRECT_URL;
    }

    public void inlineCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
    }

    public String serverCallback(Map<String, String> params, String ipAddress) throws IOException, FormException {
        log("[GlobalIrisRedirect] Server callback params=" + params);

        PaymentSettings settings = PaymentSettings.getInstance(context);
        String authCode = params.get("AUTHCODE");
        String authResult= params.get("RESULT");

        new Callback(context, PaymentType.GlobalIris, params);

        /*
           * Code zero is success
           */
        if (authCode != null && !CODE_SUCCESSFUL.equals(authResult)) {
            if (CODE_DECLINED.equals(authResult) || CODE_REFERRAL_A.equals(authResult) || CODE_REFERRAL_B.equals(authResult) || CODE_Comms.equals(authResult)) {
                FormSession session = FormSession.get(context, params.get(CUSTOM_TRANS_ID_PARAM));
                if (session != null) {
                    return session.getOrder().getPaymentFailureUrl(PaymentType.GlobalIris);
                }
            }
            return null;
        }

		// custom param is used to store our transactionId
		FormSession session = FormSession.get(context, params.get(CUSTOM_TRANS_ID_PARAM));
		if (session == null) {
			return null;
		}

		if (!checkHash(params, settings.getGlobalirisSecretKey())) {
			return null;
		}

		String purchaseAmount = params.get(AMOUNT_PARAM);
		Money amount = new Money(Integer.parseInt(purchaseAmount));
		if (amount.isZero()) {
			return null;
		}

		Payment payment = new Payment(context, session.getAccount(), amount, PaymentType.GlobalIris);
		return session.callback(params, ipAddress, payment);
    }

    private boolean checkHash(Map<String, String> params, String key) {
        String shaHash = params.get(SHA1HASH_PARAM);

        String paramStr = new StringBuilder().
                append(params.get(TIMESTAMP_PARAM)).append(DELIMITER).
                append(params.get(MERCHANT_ID_PARAM)).append(DELIMITER).
                append(params.get(ORDER_ID_PARAM)).append(DELIMITER).
                append(params.get("RESULT")).append(DELIMITER).
                append(params.get("MESSAGE")).append(DELIMITER).
                append(params.get("PASREF")).append(DELIMITER).
                append(params.get("AUTHCODE")).
                toString();

        String sha1 =  new StringBuilder(getSha(paramStr)).append(DELIMITER).append(key).toString();

        return getSha(sha1).equals(shaHash);
    }

    private String getHash(Map<String, String> params, String key) {
        String paramStr = new StringBuilder().
                append(params.get(TIMESTAMP_PARAM)).append(DELIMITER).
                append(params.get(MERCHANT_ID_PARAM)).append(DELIMITER).
                append(params.get(ORDER_ID_PARAM)).append(DELIMITER).
                append(params.get(AMOUNT_PARAM)).append(DELIMITER).
                append(params.get(CURRENCY_PARAM)).
                toString();

        String sha1 =  new StringBuilder(getSha(paramStr)).append(DELIMITER).append(key).toString();

        return getSha(sha1);

    }

    public static String getSha(String toSha1) {
        String sha1 = "";
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(toSha1.getBytes("UTF-8"));
            sha1 = byteToHex(crypt.digest());
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return sha1;
    }

    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }
}
