package org.sevensoft.ecreator.model.ecom.orders.parcels.trackers;

/**
 * @author sks 06-Dec-2005 12:40:21
 * 
 */
public class CityLinkTracker extends Tracker {

	public String getTrackingUrl(String consignment) {
		return "http://www.city-link.co.uk/pod/podfrm.php?JobNo=" + consignment + "&hidebanners=1";
	}

}
