package org.sevensoft.ecreator.model.ecom.orders.parcels.trackers;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sks 19 Apr 2006 10:25:04
 *
 */
public class EliteTracker extends Tracker {

	@Override
	public String getMethod() {
		return "POST";
	}

	public Map<String, String> getParams() {

		Map<String, String> map = new HashMap<String, String>();

		return map;

	}

	public String getTrackingUrl(String consignment) {
		return "https://emea.netdespatch.com/mba/11333x0/track/index.php3";
	}
}
