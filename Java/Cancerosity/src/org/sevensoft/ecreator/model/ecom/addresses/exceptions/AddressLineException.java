package org.sevensoft.ecreator.model.ecom.addresses.exceptions;


/**
 * @author sks
 * 17-Dec-2004 13:25:36
 */
public class AddressLineException extends Exception {

	/**
	 * 
	 */
	public AddressLineException() {
		super();
	}

	/**
	 * @param message
	 */
	public AddressLineException(String message) {
		super(message);
	}



}
