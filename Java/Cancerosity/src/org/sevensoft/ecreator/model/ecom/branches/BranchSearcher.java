package org.sevensoft.ecreator.model.ecom.branches;

import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.ecreator.model.misc.location.Locatable;
import org.sevensoft.jeezy.db.StringQueryBuilder;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 12-Nov-2005 11:06:27
 * 
 */
public class BranchSearcher implements Locatable {

	private static Logger	logger	= Logger.getLogger("ecreator");

	private int			distance;

	private String		name;

	private int			y;

	private int			x;

	private String		location;

	private RequestContext	context;

	private int			limit;

	private int			start;

	public BranchSearcher(RequestContext context) {
		this.context = context;
	}

	public List<Branch> execute() {

		StringQueryBuilder b = new StringQueryBuilder(context);

		b.append("select b.*");

		if (location == null) {

			b.append(", 0 dist ");

		} else {

			b.append(", pow(pow(b.x - " + x + ", 2) + pow(b.y- " + y + ", 2), 0.5) / 1000 / 1.6093 dist ");

		}

		b.append(" from # b");
		b.addTable(Branch.class);

		if (name != null) {

			for (String string : name.split("\\s")) {

				if (string.startsWith("-")) {

					string = "%" + string.substring(1) + "%";
					b.append(" b.name not like ? ", string);

				} else {

					string = "%" + string + "%";
					b.append(" and b.name like ? ", string);

				}
			}

		}

		if (x > 0 && y > 0 && distance > 0 && location != null) {
			b.append(" having dist <= ? ", distance);
		}

		if (x > 0 && y > 0) {
			b.append(" order by dist asc ");
		}

		List<Branch> branches = b.toQuery().execute(Branch.class, start, limit);
		logger.fine("[BranchSearcher] " + branches.size() + " branches retrieved");

		return branches;
	}

	public final int getLimit() {
		return limit;
	}

	public String getLocation() {
		return location;
	}

	public final String getName() {
		return name;
	}

	public boolean hasLocation() {
		return location != null;
	}

	public final void setLimit(int limit) {
		this.limit = limit;
	}

	public void setLocation(String location, int x, int y) {

		location = null;
		x = 0;
		y = 0;

	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

}
