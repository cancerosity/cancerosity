package org.sevensoft.ecreator.model.ecom.payments.cards.exceptions;


/**
 * @author sks 26-Oct-2005 10:58:41
 *
 */
public class CardHolderException extends Exception {

	public CardHolderException() {
	}

	public CardHolderException(String message, Throwable cause) {
		super(message, cause);
	}

	public CardHolderException(String message) {
		super(message);
	}

	public CardHolderException(Throwable cause) {
		super(cause);
	}



}
