package org.sevensoft.ecreator.model.ecom.promotions;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.items.Item;

/**
 * @author sks 25 Jul 2006 20:00:03
 *
 */
public interface Promotions {

	/**
	 * Adds a promotion line for a free item
	 */
	public void addPromotionLine(String desc, Item item);

	/**
	 * Adds a promotion line for a discount
	 */
	public void addPromotionLine(String desc, Money discount);
	
	/**
	 * Removes all existing promotion lines
	 */
	public void removePromotionLines();
}
