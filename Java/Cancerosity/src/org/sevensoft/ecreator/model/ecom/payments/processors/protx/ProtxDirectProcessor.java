package org.sevensoft.ecreator.model.ecom.payments.processors.protx;

import java.io.IOException;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.ecom.payments.processors.ProtxDirectHandler;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardTypeException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CscException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiredCardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiryDateException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.IssueNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.StartDateException;
import org.sevensoft.ecreator.model.ecom.payments.processors.Processor;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorException;
import org.sevensoft.ecreator.model.ecom.payments.results.AvsCheck;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 19-Jan-2006 20:54:14
 *
 */
@Table("payments_processors_protxdirect")
@HandlerClass(ProtxDirectHandler.class)
public class ProtxDirectProcessor extends Processor {

	private String	vendorName;
	private boolean	live;
	private boolean	showPost;

	public ProtxDirectProcessor(final RequestContext context) {
		super(context);
	}

	@Override
	public AvsCheck avsCheck(Card card, Address address) throws IOException, ProcessorException {

		ProtxTransaction txn = new ProtxTransaction(context, this, card.getAccount(), TxType.Authenticate, "AVS Check");
		txn.setCard(card);
		txn.setAddress(address);

		ProtxResult result = txn.process();
		AvsCheck avsCheck = new AvsCheck(context, result.getProcessorTransactionId(), card.getAccount(), address, card, result.getAvsResult());

		return avsCheck;
	}

	private void checkMessage(String message) throws ExpiryDateException, CardNumberException, CardTypeException, StartDateException, ProcessorException,
			CardException, IssueNumberException {

		if (message == null) {
			return;
		}

		if (message.contains("VRTXInvalidIPAddress")) {
			throw new ProcessorException(message);
		}

		if (message.contains("VRTXVendorNotValidated")) {
			throw new ProcessorException(message);
		}

		if (message.startsWith("The expired and start dates are not compatable")) {
			throw new ExpiryDateException(message);
		}

		if (message.startsWith("The expiry date is missing or invalid.")) {
			throw new ExpiryDateException(message);
		}

		if (message.startsWith("The card number given is invalid.")) {
			throw new CardNumberException(message);
		}

		if (message.startsWith("Authorisation declined by bank.")) {
			throw new CardException("Authorisation declined by bank.");
		}

		if (message.startsWith("The card issue number is required but missing or invalid")) {
			throw new IssueNumberException(message);
		}

		if (message.startsWith("The start date is required but missing or invalid")) {
			throw new StartDateException(message);
		}

		if (message.startsWith("The start date given is in the future")) {
			throw new StartDateException(message);
		}

		if (message.startsWith("The card type does not match the card number")) {
			throw new CardTypeException("Card type does not match card number.");
		}

		if (message.startsWith("The payment type is invalid")) {
			throw new ProcessorException(message);
		}

		if (message.startsWith("The card number is missing")) {
			throw new CardNumberException(message);
		}

		System.out.println("UNCAUGHT CARD ERROR MESSAGE!!!!:" + message);
	}

	@Override
	public Payment credit(Payment payment, Money amount) throws IOException, ExpiryDateException, CardNumberException, CardTypeException, StartDateException,
			ProcessorException, CardException, IssueNumberException {

		logger.fine("[ProtxDirectProcessor] Beginning credit transaction for payment=" + payment + " and amount=" + amount);

		if (amount.isLessThan(100)) {
			logger.fine("[ProtxDirectProcessor] Cannot refund amount of " + amount);
			throw new ProcessorException("Minimum refund amount is \2431");
		}

		if (!payment.hasInternalTransactionId()) {
			logger.fine("[ProtxDirectProcessor] No internal transaction id is stored with payment, protx requires this to process credit");
			throw new ProcessorException("No internal transaction id stored.");
		}

		if (!payment.hasProcessorTransactionId()) {
			logger.fine("[ProtxDirectProcessor] No processor transaction id is stored with payment, protx requires this to process credit");
			throw new ProcessorException("No processor transaction id stored.");
		}

		/*
		 * We must have security key  code for refund
		 */
		if (!payment.hasSecurityKeyCode()) {
			logger.fine("[ProtxDirectProcessor] No security key code is stored with payment, protx requires this to process credit");
			throw new ProcessorException("No security key code stored with payment.");
		}

		/*
		 * We must have auth code for refund
		 */
		if (!payment.hasAuthCode()) {
			logger.fine("[ProtxDirectProcessor] No auth code is stored with payment, protx requires this to process credit");
			throw new ProcessorException("No auth code stored with payment.");
		}

		/*
		 * Create protx transaction and process credit
		 */
		logger.fine("[ProtxDirectProcessor] Creating transaction");
		ProtxTransaction tx = new ProtxTransaction(context, this, payment.getAccount(), TxType.Refund, "Credit from txn: #"
				+ payment.getProcessorTransactionId());
		tx.setAmount(amount);
		tx.setPayment(payment);

		ProtxResult result = tx.process();

		logger.fine("[ProtxDirectProcessor] Returned message:" + result.getMessage());
		logger.fine("[ProtxDirectProcessor Returned vpst " + result.getProcessorTransactionId());

		/*
		 * Check for error message
		 */
		checkMessage(result.getMessage());

		/*
		 * If credit was successful then create new payment object to show this and return it.
		 */
		if (result.isAuthorised()) {

			logger.fine("[ProtxDirectProcessor] Credit authorised id=" + result.getProcessorTransactionId() + ", authcode=" + result.getAuthCode());

			Payment creditPayment = new Payment(context, payment.getAccount(), amount.negative(), PaymentType.CardTerminal);
			creditPayment.setDetails(payment.getDetails());
			creditPayment.setProcessorResult(result);
			creditPayment.setComments("Credit from " + payment.getProcessorTransactionId());
			creditPayment.setPayment(payment);
			creditPayment.save();

			return payment;
		}

		throw new ProcessorException("Credit failed: " + result.getMessage());
	}

	public ProtxResult debit(Item account, Card card, Address address, Money amount) throws CardException, ExpiryDateException, IssueNumberException,
			CardTypeException, CardNumberException, StartDateException, IOException, ExpiredCardException, ProcessorException {

		logger.fine("[ProtxDirectProcessor] debit amount=" + amount + ", card=" + card + ", address=" + address + ", account=" + account);

		ProtxTransaction tx = new ProtxTransaction(context, this, account, TxType.Payment, "Payment for account #" + account.getIdString());
		tx.setCard(card);
		tx.setAddress(address);
		tx.setAmount(amount);

		ProtxResult result = tx.process();

		logger.fine("[ProtxDirectProcessor]Transaction complete: " + result.getMessage());

		if (!result.isAuthorised()) {

			/*
			 * ... otherwise throw exception
			 */
			logger.fine("[ProtxDirectProcessor Transaction declined");

			throw new CardException("Card declined: " + result.getMessage());

		}

		return result;
	}

	@Override
	public String getName() {
		return "Protx Direct";
	}

	public final String getVendorName() {
		return vendorName;
	}

	@Override
	public boolean isLive() {
		return live;
	}

	public boolean isShowPost() {
		return showPost;
	}

	public void setLive(boolean live) {
		this.live = live;
	}

	public final void setShowPost(boolean showPost) {
		this.showPost = showPost;
	}

	public final void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public void verify(Card card) throws ProcessorException, IOException, ExpiryDateException, CardNumberException, CardTypeException, StartDateException,
			CardException, IssueNumberException, CscException {

		logger.fine("[ProtxDirectProcessor] verify card=" + card);

		ProtxTransaction tx = new ProtxTransaction(context, this, card.getAccount(), TxType.Authenticate, "Card verification");
		tx.setCard(card);
		tx.setAmount(ProtxTransaction.MIN_AMOUNT);

		ProtxResult result = tx.process();

		checkMessage(result.getMessage());

		if (!result.isAuthorised()) {
			throw new CardException("Authorisation declined by card issuing bank");
		}

		//		if (result.getCscResult() == CscResult.Fail)
		//			throw new CscException("Csc did not match");
	}

	@Override
	public boolean voidPayment(Payment payment) throws IOException, ProcessorException {
		throw new ProcessorException("Unsupported by protx");
	}
}