package org.sevensoft.ecreator.model.ecom.orders.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderMarker;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.parcels.Parcel;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 17 Jul 2006 17:30:13
 * Use this tag to attach parcels for an order or something
 */
public class ParcelsMarker implements IOrderMarker {

	public Object generate(RequestContext context, Map<String, String> params, Order order) {

		if (!order.hasParcels())
			return null;

		StringBuilder sb = new StringBuilder();
		sb.append("These are the tracking numbers for your delivery:\n\n");

		for (Parcel parcel : order.getParcels()) {

			sb.append(parcel.getCourier() + ": " + parcel.getConsignmentNumber());
			sb.append("\n");

		}

		return sb.toString();
	}

	public Object getRegex() {
		return "parcels";
	}

}
