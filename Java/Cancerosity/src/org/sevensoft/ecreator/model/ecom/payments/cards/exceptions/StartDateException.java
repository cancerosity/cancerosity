package org.sevensoft.ecreator.model.ecom.payments.cards.exceptions;


/**
 * @author sks 26-Oct-2005 10:58:05
 *
 */
public class StartDateException extends Exception {

	public StartDateException() {
	}

	public StartDateException(String message, Throwable cause) {
		super(message, cause);
	}

	public StartDateException(String message) {
		super(message);
	}

	public StartDateException(Throwable cause) {
		super(cause);
	}



}
