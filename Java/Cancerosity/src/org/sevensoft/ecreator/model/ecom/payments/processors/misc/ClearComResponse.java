package org.sevensoft.ecreator.model.ecom.payments.processors.misc;

import java.util.logging.Logger;

import org.jdom.Document;
import org.jdom.Element;
import org.sevensoft.ecreator.model.ecom.payments.PaymentException;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CscException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiredCardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiryDateException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.IssueNumberException;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorResult;
import org.sevensoft.ecreator.model.ecom.payments.results.AvsCheck.AvsResult;

/**
 * @author sks 19-Jan-2006 21:13:07
 *
 */
public class ClearComResponse extends ProcessorResult {

	private static final Logger	logger	= Logger.getLogger("ecreator");

	private String			internalTransactionId;
	private String			processorTransactionId;
	private String			authCode;
	private AvsResult			avsResult;
	private boolean			authorised;
	private String			message;
	private PaymentType		paymentType;

	public ClearComResponse(Document doc, String intTx, PaymentType paymentType) throws ExpiredCardException, ExpiryDateException, CardNumberException,
			CardException, IssueNumberException, CscException, PaymentException {

		this.internalTransactionId = intTx;
		this.paymentType = paymentType;

		logger.fine("[ClearComResponse] creating response, paymentType=" + paymentType + ", intTx=" + intTx);

		Element root = doc.getRootElement();
		Element engineDoc = root.getChild("EngineDoc");

		Element orderFormDoc = engineDoc.getChild("OrderFormDoc");
		if (orderFormDoc != null) {

			Element transaction = orderFormDoc.getChild("Transaction");
			if (transaction != null) {

				Element authCode_el = transaction.getChild("AuthCode");
				if (authCode_el != null) {
					authCode = authCode_el.getText();
				}

				Element id = transaction.getChild("Id");
				if (id != null) {
					processorTransactionId = id.getText();
				}

				Element cardProcResp = transaction.getChild("CardProcResp");
				if (cardProcResp != null) {

					Element avsRespCode = cardProcResp.getChild("AvsRespCode");
					if (avsRespCode != null) {

						String string = avsRespCode.getText();
						if ("EX".equalsIgnoreCase(string)) {
							avsResult = AvsResult.Match;

						} else if ("S1".equalsIgnoreCase(string)) {
							avsResult = AvsResult.NotSupported;

						} else {
							avsResult = AvsResult.Fail;
						}
					}

					//					Element cscRespCode = cardProcResp.getChild("Cvv2Resp");
					//					if (cscRespCode != null) {
					//
					//						String string = cscRespCode.getText();
					//						if ("1".equalsIgnoreCase(string))
					//							cscResult = CscResult.Match;
					//
					//						else if ("2".equalsIgnoreCase(string))
					//							cscResult = CscResult.Fail;
					//
					//						else if ("3".equalsIgnoreCase(string))
					//							cscResult = CscResult.Unsupported;
					//
					//						else
					//							cscResult = CscResult.NotChecked;
					//					}

					Element procReturnCode = cardProcResp.getChild("ProcReturnCode");
					if (procReturnCode != null) {
						authorised = procReturnCode.getText().equals("00") || procReturnCode.getText().equals("1");
					}
				}
			}
		}

		Element messageList = engineDoc.getChild("MessageList");
		if (messageList != null) {

			Element messageEl = messageList.getChild("Message");
			if (messageEl != null) {

				Element text = messageEl.getChild("Text");
				if (text != null) {
					message = text.getText();
					checkMessage(message);
				}
			}
		}

	}

	private void checkMessage(String message) throws ExpiredCardException, ExpiryDateException, CardNumberException, CardException, IssueNumberException,
			CscException, PaymentException {

		if (message == null) {
			return;
		}

		if (message.startsWith("The card has expired")) {
			throw new ExpiredCardException();
		}

		if (message.startsWith("Value for element 'Expires' is not valid")) {
			throw new ExpiryDateException();
		}

		if (message.startsWith("Unable to determine card type")) {
			throw new CardNumberException();
		}

		if (message.startsWith("The card failed compliancy checks")) {
			throw new CardNumberException();
		}

		if (message.startsWith("Declined (General)")) {
			throw new CardException("Authorisation declined by card issuing bank");
		}

		if (message.startsWith("Issue number (Switch or Solo card) is not valid")) {
			throw new IssueNumberException("Issue number is not valid");
		}

		if (message.startsWith("The Cvv2Indicator-Cvv2Value combination is not valid.")) {
			throw new CscException("Csc code is not valid");
		}

		if (message.endsWith("for element ClientId is not valid.")) {
			throw new PaymentException("The Client ID is invalid");
		}

		logger.fine("[ClearComResponse] UNCAUGHT CARD ERROR MESSAGE!!!!:" + message);

	}

	@Override
	public String getAuthCode() {
		return authCode;
	}

	@Override
	public AvsResult getAvsResult() {
		return avsResult;
	}

	@Override
	public String getInternalTransactionId() {
		return internalTransactionId;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public String getProcessorTransactionId() {
		return processorTransactionId;
	}

	@Override
	public String getSecurityKeyCode() {
		return null;
	}

	@Override
	public boolean isAuthorised() {
		return authorised;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

}
