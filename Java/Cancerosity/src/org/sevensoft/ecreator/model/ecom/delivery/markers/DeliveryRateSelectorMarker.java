package org.sevensoft.ecreator.model.ecom.delivery.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketMarker;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 27 Jun 2006 11:48:11
 *
 */
public class DeliveryRateSelectorMarker implements IBasketMarker {

	public Object generate(RequestContext context, Map<String, String> params, Basket basket) {

		if (!Module.Shopping.enabled(context)) {
			return null;
		}

		if (!Module.Delivery.enabled(context)) {
			return null;
		}
        boolean exVat = params.containsKey("ex");
		SelectTag deliveryTag = new SelectTag(context, "delivery", basket.getDeliveryOption());
		deliveryTag.setOnChange("this.form.submit();");

		Map<String, String> deliveryPricing = basket.getDeliveryPrices(exVat);
		if (deliveryPricing.size() > 0) {
			deliveryTag.addOptions(deliveryPricing);
		} else {
			deliveryTag.setAny("-No delivery rates applicable-");
		}

		return deliveryTag.toString();
	}

	public Object getRegex() {
		return "delivery_selector";
	}

}
