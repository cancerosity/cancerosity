package org.sevensoft.ecreator.model.ecom.shopping.markers.basket;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketMarker;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.ShoppingSettings;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 11 Jul 2006 11:36:27
 *
 */
public class BasketLinesMarker implements IBasketMarker {

	public Object generate(RequestContext context, Map<String, String> params, Basket basket) {

		if (!Module.Shopping.enabled(context)) {
			return null;
		}

		if (basket.isEmpty()) {
			return null;
		}

		Markup markup = ShoppingSettings.getInstance(context).getBasketLineMarkup();

		MarkupRenderer r = new MarkupRenderer(context, markup);
		r.setBodyObjects(basket.getLines());

		return r;
	}

	public Object getRegex() {
		return "basket_lines";
	}

}
