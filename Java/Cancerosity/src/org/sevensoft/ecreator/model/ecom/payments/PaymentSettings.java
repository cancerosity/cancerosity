package org.sevensoft.ecreator.model.ecom.payments;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.iface.frontend.ecom.payments.panels.PaymentTableCss;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.CheckoutHandler;
import org.sevensoft.ecreator.model.ecom.payments.processors.Processor;
import org.sevensoft.ecreator.model.ecom.payments.processors.epdq.EpdqMpiProcessor;
import org.sevensoft.ecreator.model.ecom.payments.processors.hsbc.HsbcApiProcessor;
import org.sevensoft.ecreator.model.ecom.payments.processors.protx.ProtxDirectProcessor;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 05-Jul-2004 15:17:27
 */
@Table("settings_payment")
@Singleton
public class PaymentSettings extends EntityObject {

	public static PaymentSettings getInstance(RequestContext context) {
		return getSingleton(context, PaymentSettings.class);
	}

	/**
	 * Set this live
	 */
	private boolean			netPaymentsExpressLive;

	/**
	 * 
	 */
	private String			chequeAddress;

	/**
	 * The able2buy goods code represents what type of goods the client is selling
	 */
	private String			able2BuyGoodsCode;

	private String			hsbcCpiHashKey;

    private String hsbcCpiTransactionType;

    /**
	 * 
	 */
	private String			chequePayee;

	/**
	 * password for callbacks
	 */
	private String			epdqCallbackPassword;

	private String			epdqPassphrase;

	private String			epdqPostUsername, epdqPostPassword;

	private String			epdqStoreId;

	/**
	 * Is protx direct live ?
	 */
	@Deprecated
	private boolean			protxDirectLive;

	/**
	 * Emails to send a notification to when a payment is declined
	 */
	private List<String>		declineEmails;

	/**
	 * License code for max mind
	 */
	private String			maxMindLicense;

	private String			offlineContactMessage;

	private TreeSet<PaymentType>	paymentTypes;

	/**
	 * Pay pal accout email
	 */
	private String			payPalAccountEmail;

    private String          payPalExpressAccountPassword;

    private String          payPalExpressAccountSignature;

    private String          payPalExpressAccountUsername;

    private String          payPalDirectAccountPassword;

    private String          payPalDirectAccountSignature;

    private String          payPalDirectAccountUsername;

    private String			protxEncryptionPassword;

    /**
     * PayPal Pro Express
     */
    private String payPalProExpressApiUsername;

    private String payPalProExpressApiPassword;

    private String payPalProExpressSignature;
    
    private boolean payPalProExpressLive;

    /**
     * PayPal Pro Hosted
     */
    private String payPalProHostedMerchantId;

    private boolean payPalProHostedLive;


    /**
	 * Flags if protx should use live mode or test mode
	 */
	private boolean			protxLive;

	/**
	 * The email to send confirmations to
	 */
	private String			protxVendorEmail;

	private String			metaChargeInstallId;

	/**
	 * this was created as a non null field on some sites by accident so ensure this field is never null.
	 */
	private String			protxVendorName;

	private boolean			secCardLive;

	private String			secCardPassword;

	private String			secCardUsername;

	/**
	 * 
	 */
	private String			transferDetails;

	/**
	 * 
	 */
	private boolean			verifyCard;

	private String			worldPayCallbackPassword;

	/**
	 * 
	 */
	private String			worldPayInstallationId;

	private boolean			worldPayLive;

	private String			able2BuyId2;

	private String			able2BuyId;

	private boolean			able2BuyLive;

	private boolean			payPalLive;

	@Deprecated
	private String			protxDirectVendorName;

	@Deprecated
	private String			epdqMpiPassword;

	@Deprecated
	private String			epdqMpiUsername;

	/**
	 * Token as required by PDT callback
	 */
	private String			payPalPdtToken;

	private String			secureTradingPagesEmail;

    private String secureTradingCallbackLine;

    private String secureTradingFailedCallbackLine;

    private String secureTradingFormref;

    private String			secureTradingPagesMerchant;

	private boolean			secureTradingPagesLive;

	private String			netPaymentsExpressAccount;

	private boolean			hsbcCpiLive;

    private boolean			hsbcCpiIris;

	private String			worldpayAccountId;

	private String			hsbcCpiStorefrontId;

	private String			mEnableRequestCode;

	private String			mEnableAccount;

	private boolean			terminalAvsSupport;

	private String			mEnableMpcId;

	@Deprecated
	private String			hsbcApiClientId;

	@Deprecated
	private String			hsbcApiUsername;

	@Deprecated
	private String			hsbcApiPassword;

	private String			optimalPaymentsAccountId;

	private String			optimalPaymentsMerchantId;

	private String			worldpayAuthMode;

	private String			paymentTableCss;

	/**
	 * This is inserted into the email that comes from protx.
	 */
	private String			protxEmailHeader;

	private Money			able2BuyMaxSpend;

	private Money			able2BuyMinSpend;

	private String			noChexMerchantId;

	private String			twoCheckoutSid;

	private boolean	immediateTransact;

    private String          googleCheckoutId;

    private String googleCheckoutPw;

    private boolean         googleCheckoutLive;

    private String protxPaymentType;

    private boolean         payPalExpressLive;

    private boolean         payPalDirectLive;

    private boolean protxSandbox;

    public static final String PROTX_TXTYPE_PAYMENT = "PAYMENT";
    public static final String PROTX_TXTYPE_DEFERRED = "DEFERRED";
    public static final String PROTX_TXTYPE_AUTHORISE = "AUTHENTICATE";

    private String cardsaveMerchantId;
    private String cardsaveMerchantPw;
    private String cardsavePreSharedKey;

    private String globalirisMerchantId;
    private String globalirisSubaccountName;
    private boolean globalirisAutoSettle;
    private String globalirisSecretKey;

    public PaymentSettings(RequestContext context) {
		super(context);
	}

	public void addPaymentType(PaymentType type) {
		getPaymentTypes().add(type);
	}

	public boolean contains(PaymentType pt) {
		return getPaymentTypes().contains(pt);
	}

	/**
	 * @return
	 */
	public String getAble2BuyGoodsCode() {
		return able2BuyGoodsCode;
	}

	public String getAble2BuyId() {
		return able2BuyId;
	}

	public String getAble2BuyId2() {
		return able2BuyId2;
	}

	public Money getAble2BuyMaxSpend() {
		return able2BuyMaxSpend;
	}

	public Money getAble2BuyMinSpend() {
		return able2BuyMinSpend;
	}

	public String getAble2BuySuccessUrl() {
		return Config.getInstance(context).getUrl() + "/" + new Link(CheckoutHandler.class, "completed", "paymentType", PaymentType.Able2Buy);
	}

	public Set<PaymentType> getAvailablePaymentTypes(final Money total) {

		Set<PaymentType> paymentTypes = getPaymentTypes();
		CollectionsUtil.filter(paymentTypes, new Predicate<PaymentType>() {

			public boolean accept(PaymentType e) {
				return e.isValid(context, total);
			}
		});
		return paymentTypes;
	}

	/**
	 * Returns a list of payment types that are online processable,
	 *  ie are online card payments AND have all required details entered
	 * 
	 */
	public Set<PaymentType> getCardPaymentTypes() {

		Set<PaymentType> paymentTypes = new TreeSet<PaymentType>(getPaymentTypes());
		CollectionsUtil.filter(paymentTypes, new Predicate<PaymentType>() {

			public boolean accept(PaymentType e) {
				return e.isOnline() && e.isValid(context, null);
			}

		});
		return paymentTypes;
	}

	public String getChequeAddress() {
		return chequeAddress;
	}

	public String getChequeAddressBr() {
		return HtmlHelper.nl2br(getChequeAddress());
	}

	public String getChequePayee() {
		return chequePayee;
	}

	public final List<String> getDeclineEmails() {

		if (declineEmails == null) {
			declineEmails = new ArrayList();
		}
		return declineEmails;
	}

	public Processor getDefaultProcessor() {
		List<Processor> processors = Processor.getAll(context);
		return processors.isEmpty() ? null : processors.get(0);
	}

	public String getEpdqCallbackPassword() {
		return epdqCallbackPassword;
	}

	public String getEpdqPassphrase() {
		return epdqPassphrase;
	}

	public String getEpdqPostPassword() {
		return epdqPostPassword;
	}

	public String getEpdqPostUsername() {
		return epdqPostUsername;
	}

	public String getEpdqStoreId() {
		return epdqStoreId;
	}

	public String getHsbcCpiHashKey() {
		return hsbcCpiHashKey;
	}

    public String getHsbcCpiTransactionType() {
        return hsbcCpiTransactionType;
    }

    public String getHsbcCpiStorefrontId() {
		return hsbcCpiStorefrontId;
	}

	public String getMaxMindLicense() {
		return maxMindLicense;
	}

	/**
	 * @return
	 */
	public String getMEnableAccount() {
		return mEnableAccount;
	}

	/**
	 * @return
	 */
	public String getMEnableMpcId() {
		return mEnableMpcId;
	}

	/**
	 * @return
	 */
	public String getMEnableRequestCode() {
		return mEnableRequestCode;
	}

	public String getMetaChargeInstallId() {
		return metaChargeInstallId;
	}

	/**
	 * @return
	 */
	public String getNetPaymentsExpressAccount() {
		return netPaymentsExpressAccount;
	}

	/**
	 * 
	 */
	public String getNoChexMerchantId() {
		return noChexMerchantId;
	}

	/**
	 * @return
	 */
	public String getOfflineContactMessage() {
		return offlineContactMessage == null ? "We will contact you shortly for your payment details." : offlineContactMessage;
	}

	public String getOptimalPaymentsAccountId() {
		return optimalPaymentsAccountId;
	}

	public String getOptimalPaymentsMerchantId() {
		return optimalPaymentsMerchantId;
	}

	public String getPaymentTableCss() {
		initPaymentTableCss();
		return paymentTableCss;
	}

	/**
	 * Returns the master list of available payment types.
	 * 
	 */
	public TreeSet<PaymentType> getPaymentTypes() {
		if (paymentTypes == null) {
			paymentTypes = new TreeSet();
		}
		return paymentTypes;
	}

	public String getPayPalAccountEmail() {
		return payPalAccountEmail;
	}

    public String getPayPalDirectAccountPassword() {
        return payPalDirectAccountPassword;
    }

    public String getPayPalDirectAccountSignature() {
        return payPalDirectAccountSignature;
    }

    public String getPayPalDirectAccountUsername() {
        return payPalDirectAccountUsername;
    }

    public String getPayPalExpressAccountPassword() {
        return payPalExpressAccountPassword;
    }

    public String getPayPalExpressAccountSignature() {
        return payPalExpressAccountSignature;
    }

    public String getPayPalExpressAcountUsername() {
        return payPalExpressAccountUsername;
    }

    public String getPayPalPdtToken() {
		return payPalPdtToken;
	}

    public String getPayPalProExpressApiUsername() {
        return payPalProExpressApiUsername;
    }

    public String getPayPalProExpressApiPassword() {
        return payPalProExpressApiPassword;
    }

    public String getPayPalProExpressSignature() {
        return payPalProExpressSignature;
    }

    public boolean hasPayPalProDetails() {
        return payPalProExpressApiUsername != null && payPalProExpressApiPassword != null && payPalProExpressSignature != null;
    }

    public String getProtxEmailHeader() {
		return protxEmailHeader;
	}

	public String getProtxEncryptionPassword() {
		return protxEncryptionPassword;
	}

    public String getProtxPaymentType() {
        return protxPaymentType;
    }

    public String getProtxVendorEmail() {
		return protxVendorEmail;
	}

	public String getProtxVendorName() {
		return protxVendorName;
	}

	public String getSecCardPassword() {
		return secCardPassword;
	}

	public String getSecPayUsername() {
		return secCardUsername;
	}

	public String getSecureTradingPagesEmail() {
		return secureTradingPagesEmail;
	}

    public String getSecureTradingCallbackLine() {
        return secureTradingCallbackLine == null ? "1" : secureTradingCallbackLine;
    }

    public String getSecureTradingFailedCallbackLine() {
        return secureTradingFailedCallbackLine == null ? "4" : secureTradingFailedCallbackLine;
    }

    public String getSecureTradingFormref() {
        return secureTradingFormref;
    }

    public String getSecureTradingPagesMerchant() {
		return secureTradingPagesMerchant;
	}

	public String getTransferDetails() {
		return transferDetails;
	}

	public String getTransferDetailsBr() {
		return HtmlHelper.nl2br(getTransferDetails());
	}

	public final String getTwoCheckoutSid() {
		return twoCheckoutSid;
	}

	public String getWorldpayAccountId() {
		return worldpayAccountId;
	}

	public String getWorldpayAuthMode() {
		return worldpayAuthMode;
	}

	public String getWorldPayCallbackPassword() {
		return worldPayCallbackPassword;
	}

	public String getWorldPayInstallationId() {
		return worldPayInstallationId;
	}

    public String getGoogleCheckoutId() {
        return googleCheckoutId;
    }

    public String getGoogleCheckoutPw() {
        return googleCheckoutPw;
    }

    public boolean isGoogleCheckoutLive() {
        return googleCheckoutLive;
    }

    public String getCardsaveMerchantId() {
        return cardsaveMerchantId;
    }

    public String getCardsaveMerchantPw() {
        return cardsaveMerchantPw;
    }

    public String getCardsavePreSharedKey() {
        return cardsavePreSharedKey;
    }

    public String getGlobalirisMerchantId() {
        return globalirisMerchantId;
    }

    public String getGlobalirisSubaccountName() {
        return globalirisSubaccountName;
    }

    public String getGlobalirisSecretKey() {
        return globalirisSecretKey;
    }

    public boolean hasChequeAddress() {
		return chequeAddress != null;
	}

	public boolean hasChequeePayee() {
		return chequePayee != null;
	}

	public boolean hasDeclineEmails() {
		return getDeclineEmails().size() > 0;
	}

	public boolean hasEpdqPassphrase() {
		return epdqPassphrase != null;
	}

	public boolean hasEpdqStoreId() {
		return epdqStoreId != null;
	}

	public boolean hasHsbcCpiHashKey() {
		return hsbcCpiHashKey != null;
	}

    public boolean hasHsbcCpiTransactionType() {
        return hsbcCpiTransactionType != null;
    }

    public boolean hasMaxMindLicense() {
		return maxMindLicense != null;
	}

	public boolean hasPayPalAccountEmail() {
		return payPalAccountEmail != null;
	}

	/**
	 * @return
	 */
	public boolean hasPayPalPdtToken() {
		return payPalPdtToken != null;
	}

	/**
	 * Returns true if we have at least one live processor
	 */
	public boolean hasProcessor() {
		return Processor.getAll(context).size() > 0;
	}

	/**
	 * 
	 */
	public boolean hasProtxEmailHeader() {
		return protxEmailHeader != null;
	}

	/**
	 * @return
	 */
	public boolean hasProtxEncryptionPassword() {
		return protxEncryptionPassword != null;
	}

	/**
	 * @return
	 */
	public boolean hasProtxVendorEmail() {
		return protxVendorEmail != null;
	}

	/**
	 * @return
	 */
	public boolean hasSecCardPassword() {
		return secCardPassword != null;
	}

	public boolean hasTransferDetails() {
		return transferDetails != null;
	}

	public boolean hasWorldpayAccountId() {
		return worldpayAccountId != null;
	}

	public boolean hasWorldpayAuthMode() {
		return worldpayAuthMode != null;
	}

	/**
	 * 
	 */
	public boolean hasWorldPayCallbackPassword() {
		return worldPayCallbackPassword != null;
	}

	/**
	 * @return
	 */
	public boolean hasWorldPayInstallationId() {
		return worldPayInstallationId != null;
	}

    public boolean hasGlobalIrisMerchantId() {
        return globalirisMerchantId != null;
    }

	private void initPaymentTableCss() {

		if (paymentTableCss == null) {

			paymentTableCss = new PaymentTableCss().toString();
			save();
		}
	}

	/**
	 * @return
	 */
	public boolean isAble2BuyLive() {
		return able2BuyLive;
	}

	/**
	 * @return
	 */
	public boolean isCardTerminal() {
		return getPaymentTypes().contains(PaymentType.CardTerminal);
	}

	/**
	 * @return
	 */
	public boolean isHsbcCpiLive() {
		return hsbcCpiLive;
	}

    public boolean isHsbcCpiIris() {
        return hsbcCpiIris;
    }

    public boolean isNetPaymentsExpressLive() {
		return netPaymentsExpressLive;
	}

	/**
	 * @return
	 */
	public boolean isPayPalLive() {
		return payPalLive;
	}

    public boolean isPayPalDirectLive() {
        return payPalDirectLive;
    }

    public boolean isPayPalExpressLive() {
        return payPalExpressLive;
    }

    public boolean isPayPalProExpressLive() {
        return payPalProExpressLive;
    }

    public String getPayPalProHostedMerchantId() {
        return payPalProHostedMerchantId;
    }

    public boolean isPayPalProHostedLive() {
        return payPalProHostedLive;
    }

    public boolean isProtxLive() {
		return protxLive;
	}

    public boolean isProtxSandbox() {
        return protxSandbox;
    }

    /**
	 * @return
	 */
	public boolean isSecPayLive() {
		return secCardLive;
	}

	/**
	 * @return
	 */
	public boolean isSecureTradingPagesLive() {
		return secureTradingPagesLive;
	}

	public boolean isTerminalAvsSupport() {
		return terminalAvsSupport;
	}

	public boolean isVerifyCard() {
		return verifyCard;
    }

    public boolean isGlobalirisAutoSettle() {
        return globalirisAutoSettle;
    }

    /**
	 * @return
	 */
	public boolean isWorldPayLive() {
		return this.worldPayLive;
	}

	public void removePaymentType(PaymentType type) {
		getPaymentTypes().remove(type);
	}

	@Override
	protected void schemaInit(RequestContext context) {

		PaymentSettings ps = PaymentSettings.getInstance(context);

		// check deprecated fields for values, and create objects if required
		if (epdqMpiUsername != null) {

			EpdqMpiProcessor epdq = new EpdqMpiProcessor(context);
			epdq.setPassword(epdqMpiPassword);
			epdq.setUsername(epdqMpiUsername);
			epdq.setClientId(epdqStoreId);
			epdq.save();

			epdqMpiPassword = null;
			epdqMpiUsername = null;
			save();

			ps.addPaymentType(PaymentType.CardTerminal);
			ps.save();
		}

		if (protxDirectVendorName != null) {

			ProtxDirectProcessor protx = new ProtxDirectProcessor(context);
			protx.setVendorName(protxDirectVendorName);
			protx.setLive(protxDirectLive);
			protx.save();

			protxDirectVendorName = null;
			protxDirectLive = false;
			save();

			ps.addPaymentType(PaymentType.CardTerminal);
			ps.save();
		}

		if (hsbcApiUsername != null) {

			HsbcApiProcessor hsbc = new HsbcApiProcessor(context);
			hsbc.setUsername(hsbcApiUsername);
			hsbc.setClientId(hsbcApiClientId);
			hsbc.setPassword(hsbcApiPassword);
			hsbc.save();

			hsbcApiUsername = null;
			hsbcApiClientId = null;
			hsbcApiPassword = null;
			save();

			ps.addPaymentType(PaymentType.CardTerminal);
			ps.save();
		}
	}

	public void setAble2BuyGoodsCode(String s) {
		this.able2BuyGoodsCode = (s == null ? null : s.trim());
	}

	public void setAble2BuyId(String s) {
		this.able2BuyId = (s == null ? null : s.trim());
	}

	public void setAble2BuyId2(String s) {
		this.able2BuyId2 = (s == null ? null : s.trim());
	}

	public void setAble2BuyLive(boolean b) {
		this.able2BuyLive = b;
	}

	public void setAble2BuyMaxSpend(Money m) {
		this.able2BuyMaxSpend = m;
	}

	public void setAble2BuyMinSpend(Money m) {
		this.able2BuyMinSpend = m;
	}

	/**
	 * @param s
	 */
	public void setChequeAddress(String s) {
		this.chequeAddress = (s == null ? null : s.trim());
	}

	/**
	 * @param s
	 */
	public void setChequePayee(String s) {
		this.chequePayee = (s == null ? null : s.trim());
	}

	public final void setDeclineEmails(List<String> declineEmails) {
		this.declineEmails = declineEmails;
	}

	public void setEpdqCallbackPassword(String s) {
		this.epdqCallbackPassword = (s == null ? null : s.trim());
	}

	public void setEpdqPassphrase(String s) {
		this.epdqPassphrase = (s == null ? null : s.trim());
	}

	public void setEpdqPostPassword(String s) {
		this.epdqPostPassword = (s == null ? null : s.trim());
	}

	public void setEpdqPostUsername(String s) {
		this.epdqPostUsername = (s == null ? null : s.trim());
	}

	public void setEpdqStoreId(String s) {
		this.epdqStoreId = (s == null ? null : s.trim());
	}

	public void setHsbcCpiHashKey(String s) {
		this.hsbcCpiHashKey = (s == null ? null : s.trim());
	}

    public void setHsbcCpiTransactionType(String s) {
        this.hsbcCpiTransactionType = (s == null ? null : s.trim());
    }

    public void setHsbcCpiLive(boolean b) {
		this.hsbcCpiLive = b;
	}

    public void setHsbcCpiIris(boolean b) {
        this.hsbcCpiIris = b;
    }

    public void setHsbcCpiStorefrontId(String s) {
		this.hsbcCpiStorefrontId = (s == null ? null : s.trim());
	}

	public void setMaxMindLicense(String s) {
		this.maxMindLicense = (s == null ? null : s.trim());
	}

	public final void setMEnableAccount(String s) {
		mEnableAccount = (s == null ? null : s.trim());
	}

	public final void setMEnableMpcId(String s) {
		mEnableMpcId = (s == null ? null : s.trim());
	}

	public final void setMEnableRequestCode(String s) {
		mEnableRequestCode = (s == null ? null : s.trim());
	}

	public void setMetaChargeInstallId(String s) {
		this.metaChargeInstallId = (s == null ? null : s.trim());
	}

	public void setNetPaymentsExpressAccount(String s) {
		this.netPaymentsExpressAccount = (s == null ? null : s.trim());
	}

	public void setNetPaymentsExpressLive(boolean b) {
		this.netPaymentsExpressLive = b;
	}

	public final void setNoChexMerchantId(String s) {
		this.noChexMerchantId = (s == null ? null : s.trim());
	}

	public void setOfflineContactMessage(String s) {
		this.offlineContactMessage = (s == null ? null : s.trim());
	}

	public void setOptimalPaymentsAccountId(String s) {
		this.optimalPaymentsAccountId = (s == null ? null : s.trim());
	}

	public void setOptimalPaymentsMerchantId(String s) {
		this.optimalPaymentsMerchantId = (s == null ? null : s.trim());
	}

	public void setPayPalAccountEmail(String s) {
		this.payPalAccountEmail = (s == null ? null : s.trim());
	}

    public void setPayPalDirectAccountPassword(String payPalDirectAccountPassword) {
        this.payPalDirectAccountPassword = payPalDirectAccountPassword;
    }

    public void setPayPalDirectAccountSignature(String payPalDirectAccountSignature) {
        this.payPalDirectAccountSignature = payPalDirectAccountSignature;
    }

    public void setPayPalDirectAccountUsername(String payPalDirectAccountUsername) {
        this.payPalDirectAccountUsername = payPalDirectAccountUsername;
    }

    public void setPayPalExpressAccountPassword(String s) {
        this.payPalExpressAccountPassword = (s == null ? null : s.trim());
    }

    public void setPayPalExpressAccountSignature(String s) {
        this.payPalExpressAccountSignature = (s == null ? null : s.trim());
    }

    public void setPayPalExpressAccountUsername(String s) {
        this.payPalExpressAccountUsername = (s == null ? null : s.trim());
    }

    public void setPayPalProExpressApiUsername(String payPalProExpressApiUsername) {
        this.payPalProExpressApiUsername = (payPalProExpressApiUsername == null ? null : payPalProExpressApiUsername.trim());
    }

    public void setPayPalProExpressApiPassword(String payPalProExpressApiPassword) {
        this.payPalProExpressApiPassword = (payPalProExpressApiPassword == null ? null : payPalProExpressApiPassword.trim());
    }

    public void setPayPalProExpressSignature(String payPalProExpressSignature) {
        this.payPalProExpressSignature = (payPalProExpressSignature == null ? null : payPalProExpressSignature.trim());
    }

    public void setPayPalLive(boolean b) {
		this.payPalLive = b;
	}

    public void setPayPalDirectLive(boolean payPalDirectLive) {
        this.payPalDirectLive = payPalDirectLive;
    }

    public void setPayPalExpressLive(boolean payPalExpressLive) {
        this.payPalExpressLive = payPalExpressLive;
    }

    public void setPayPalProExpressLive(boolean payPalProExpressLive) {
        this.payPalProExpressLive = payPalProExpressLive;
    }

    public void setPayPalProHostedMerchantId(String payPalProHostedMerchantId) {
        this.payPalProHostedMerchantId = (payPalProHostedMerchantId == null ? null : payPalProHostedMerchantId.trim());
    }

    public void setPayPalProHostedLive(boolean payPalProHostedLive) {
        this.payPalProHostedLive = payPalProHostedLive;
    }

    public void setPayPalPdtToken(String s) {
		this.payPalPdtToken = (s == null ? null : s.trim());
	}

	public void setProtxEmailHeader(String s) {
		this.protxEmailHeader = (s == null ? null : s.trim());
	}

	public void setProtxEncryptionPassword(String s) {
		this.protxEncryptionPassword = (s == null ? null : s.trim());
	}

	public void setProtxLive(boolean b) {
		this.protxLive = b;
	}

    public void setProtxSandbox(boolean protxSandbox) {
        this.protxSandbox = protxSandbox;
    }

    public void setProxtPaymentType(String protxPaymentType) {
        this.protxPaymentType = protxPaymentType;
    }

    public void setProtxVendorEmail(String s) {
		this.protxVendorEmail = (s == null ? null : s.trim());
	}

	public void setProtxVendorName(String s) {
		this.protxVendorName = (s == null ? null : s.trim());
	}

	public void setSecCardPassword(String s) {
		this.secCardPassword = (s == null ? null : s.trim());
	}

	public void setSecCardUsername(String s) {
		this.secCardUsername = (s == null ? null : s.trim());
	}

	public void setSecPayLive(boolean b) {
		this.secCardLive = b;
	}

	public void setSecureTradingPagesEmail(String s) {
		this.secureTradingPagesEmail = (s == null ? null : s.trim());
	}

    public void setSecureTradingCallbackLine(String s) {
        this.secureTradingCallbackLine = (s == null ? null : s.trim());
    }

    public void setSecureTradingFailedCallbackLine(String s) {
        this.secureTradingFailedCallbackLine = (s == null ? null : s.trim());
    }

    public void setSecureTradingFormref(String s) {
        this.secureTradingFormref = (s == null ? null : s.trim());
    }

    public void setSecureTradingPagesLive(boolean b) {
		this.secureTradingPagesLive = b;
	}

	public void setSecureTradingPagesMerchant(String s) {
		this.secureTradingPagesMerchant = (s == null ? null : s.trim());
	}

	public void setTerminalAvsSupport(boolean b) {
		this.terminalAvsSupport = b;
	}

	public void setTransferDetails(String s) {
		this.transferDetails = (s == null ? null : s.trim());
	}

	public final void setTwoCheckoutSid(String s) {
		this.twoCheckoutSid = (s == null ? null : s.trim());
	}

	public void setVerifyCard(boolean b) {
		this.verifyCard = b;
	}

	public void setWorldpayAccountId(String s) {
		this.worldpayAccountId = (s == null ? null : s.trim());
	}

	public void setWorldpayAuthMode(String s) {
		this.worldpayAuthMode = (s == null ? null : s.trim().toUpperCase());
	}

	public void setWorldPayCallbackPassword(String s) {
		this.worldPayCallbackPassword = (s == null ? null : s.trim());
	}

	public void setWorldPayInstallationId(String s) {
		this.worldPayInstallationId = (s == null ? null : s.trim());
	}

	public void setWorldPayLive(boolean b) {
		this.worldPayLive = b;
	}

    public void setGoogleCheckoutId(String s) {
        this.googleCheckoutId = (s == null ? null : s.trim());
    }

    public void setGoogleCheckoutPw(String googleCheckoutPw) {
        this.googleCheckoutPw = googleCheckoutPw;
    }

    public void setGoogleCheckoutLive(boolean b) {
        this.googleCheckoutLive = b;
    }

    public void setCardsaveMerchantId(String cardsaveMerchantId) {
        this.cardsaveMerchantId = cardsaveMerchantId;
    }

    public void setCardsaveMerchantPw(String cardsaveMerchantPw) {
        this.cardsaveMerchantPw = cardsaveMerchantPw;
    }

    public void setCardsavePreSharedKey(String cardsavePreSharedKey) {
        this.cardsavePreSharedKey = cardsavePreSharedKey;
    }

    public void setGlobalirisMerchantId(String globalirisMerchantId) {
        this.globalirisMerchantId = globalirisMerchantId;
    }

    public void setGlobalirisSubaccountName(String globalirisSubaccountName) {
        this.globalirisSubaccountName = globalirisSubaccountName;
    }

    public void setGlobalirisSecretKey(String globalirisSecretKey) {
        this.globalirisSecretKey = globalirisSecretKey;
    }

    public void setGlobalirisAutoSettle(boolean b) {
        this.globalirisAutoSettle = b;
    }

    @Override
	protected void singletonInit(RequestContext context) {
		paymentTypes = new TreeSet<PaymentType>();
		paymentTypes.add(PaymentType.Cheque);
	}

	/**
	 * 
	 */
	public void setImmediateTransact(boolean immediateTransact) {
		this.immediateTransact = immediateTransact;
	}

	
	public final boolean isImmediateTransact() {
		return immediateTransact;
	}
}