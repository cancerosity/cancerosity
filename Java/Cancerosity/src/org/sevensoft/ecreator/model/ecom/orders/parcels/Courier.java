package org.sevensoft.ecreator.model.ecom.orders.parcels;

import org.sevensoft.ecreator.model.ecom.orders.parcels.trackers.AmtrackTracker;
import org.sevensoft.ecreator.model.ecom.orders.parcels.trackers.CityLinkTracker;
import org.sevensoft.ecreator.model.ecom.orders.parcels.trackers.InterlinkTracker;
import org.sevensoft.ecreator.model.ecom.orders.parcels.trackers.LynxTracker;
import org.sevensoft.ecreator.model.ecom.orders.parcels.trackers.ParcelineTracker;
import org.sevensoft.ecreator.model.ecom.orders.parcels.trackers.TntTracker;
import org.sevensoft.ecreator.model.ecom.orders.parcels.trackers.Tracker;
import org.sevensoft.ecreator.model.ecom.orders.parcels.trackers.UpsTracker;

/**
 * @author sks 29-Sep-2005 18:45:04
 * 
 */
public enum Courier {

	Amtrak(new AmtrackTracker()),

	ANC(),

	BusinessPost("Business Post", null),

	CityLink("City Link", new CityLinkTracker()),

	DHL(null),

	Other(null),

	Lynx(new LynxTracker()),

	Interlink(new InterlinkTracker()),

	ParcelForce("Parcel Force", null),

	RoyalMail("Royal Mail", null),

	TNT(new TntTracker()),

	UPS(new UpsTracker()), Thaipost(),

	Parceline("Parceline", new ParcelineTracker());

	private final String	name;
	private final Tracker	tracker;

	private Courier() {
		this.name = name();
		this.tracker = null;
	}

	private Courier(String name, Tracker tracker) {
		this.name = name;
		this.tracker = tracker;
	}

	private Courier(Tracker tracker) {
		this.name = name();
		this.tracker = tracker;
	}

	public Tracker getTracker() {
		return tracker;
	}

	public boolean hasTracker() {
		return tracker != null;
	}

	@Override
	public String toString() {
		return name;
	}

}
