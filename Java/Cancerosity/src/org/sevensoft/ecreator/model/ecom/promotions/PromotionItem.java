package org.sevensoft.ecreator.model.ecom.promotions;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 9 Oct 2006 20:49:55
 * 
 *         A specific item this promotion applies to
 * 
 */
@Table("promotions_items")
@SuppressWarnings("unused")
public class PromotionItem extends EntityObject {

	private Promotion	promotion;
	private Item	item;

	public PromotionItem(RequestContext context) {
		super(context);
	}

	public PromotionItem(RequestContext context, Promotion promotion, Item item) {
		super(context);

		if (!promotion.getItems().contains(item)) {
			this.promotion = promotion;
			this.item = item;

			save();
		}
	}

}
