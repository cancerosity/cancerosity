package org.sevensoft.ecreator.model.ecom.shopping.markers.totals;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketLineMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderMarker;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Jun 2006 11:46:17
 *
 */
public class VatMarker implements IBasketMarker, IOrderMarker, IBasketLineMarker {

	public Object generate(RequestContext context, Map<String, String> params, Basket basket) {
		Currency currency = (Currency) context.getAttribute("currency");
		return currency.getSymbol() + currency.convert(basket.getTotalVat()).toString(2);
	}

	public Object generate(RequestContext context, Map<String, String> params, BasketLine line) {
		Currency currency = (Currency) context.getAttribute("currency");
		return currency.getSymbol() + currency.convert(line.getLineSalePriceVat()).toString(2);
	}

	public Object generate(RequestContext context, Map<String, String> params, Order order) {
		return (String) context.getAttribute("currencySymbol") + order.getTotalVat();
	}

	public Object getRegex() {
		return "vat";
	}

}
