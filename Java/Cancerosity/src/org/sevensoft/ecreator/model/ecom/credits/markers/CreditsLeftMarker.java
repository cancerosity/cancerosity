package org.sevensoft.ecreator.model.ecom.credits.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 16 Aug 2006 17:23:37
 *
 */
public class CreditsLeftMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.Credits.enabled(context)) {
			logger.fine("[CurrentCreditsMarker] credits disabled, exiting");
			return null;
		}

		Item account = (Item) context.getAttribute("account");
		if (account == null) {
			return null;
		}

		return super.string(context, params, account.getCreditStore().getCredits());
	}

	@Override
	public String getDescription() {
		return "Shows the current number of credits for the logged in account";
	}

	public Object getRegex() {
		return "credits_left";
	}

}
