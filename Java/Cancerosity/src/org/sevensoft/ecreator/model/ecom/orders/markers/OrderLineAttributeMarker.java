package org.sevensoft.ecreator.model.ecom.orders.markers;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IOrderLineMarker;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * @author Dmitry Lebedev
 *         Date: 11.02.2010
 */
public class OrderLineAttributeMarker extends MarkerHelper implements IOrderLineMarker {

    public Object generate(RequestContext context, Map<String, String> params, OrderLine line) {

        String id = params.get("id");
        int idInt = 0;

        try {
            idInt = Integer.parseInt(id);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            logger.warning(e.toString());
        }

        String attributeName = "";
        String attributeValue = "";

        if (line.isItemLine()) {
            attributeName = line.getItem().getAttributeFromId(id).getName();
            attributeValue = line.getItem().getAttributeValue(idInt);
        }


        return super.string(context, params, attributeName + " - " + attributeValue);
    }

    public Object getRegex() {
        return "order_line_attribute";
    }
}
