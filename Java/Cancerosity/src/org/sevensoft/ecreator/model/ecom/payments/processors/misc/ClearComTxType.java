package org.sevensoft.ecreator.model.ecom.payments.processors.misc;

/**
 * @author sks 8 Oct 2006 13:02:57
 *
 */
public enum ClearComTxType {

	Void("Void"), PreAuth("PreAuth"), Credit("Credit"), Auth("Auth");

	private String	toString;

	private ClearComTxType(String toString) {
		this.toString = toString;
	}

	public String toString() {
		return toString;
	}
}
