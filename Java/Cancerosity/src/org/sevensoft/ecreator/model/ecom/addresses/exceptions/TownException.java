package org.sevensoft.ecreator.model.ecom.addresses.exceptions;

/**
 * @author sks 04-Dec-2004 10:16:09
 */
public class TownException extends Exception {

	/**
	 * @param string
	 */
	public TownException(String string) {
		super(string);
	}

}
