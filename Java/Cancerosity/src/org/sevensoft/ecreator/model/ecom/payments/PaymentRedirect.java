package org.sevensoft.ecreator.model.ecom.payments;

import java.io.IOException;
import java.util.Map;

import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.iface.admin.misc.seo.panels.GoogleAnalyticsEcommerceTrackingPanel;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.results.StringResult;

/**
 * @author sks 26 May 2006 14:24:14
 *
 * This class builds a page that contains a form for posting to the payment server and will auto submit this form.
 */
public class PaymentRedirect extends StringResult {

	public PaymentRedirect(RequestContext context, Payable payable) {
		this(context, payable, payable.getPaymentType());
	}

	public PaymentRedirect(RequestContext context, Payable payable, PaymentType paymentType) {
		super(null, null);

		Form form = paymentType.getForm(context);
		Map<String, String> params;
		try {
			params = form.getParams(payable);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} catch (FormException e) {
			throw new RuntimeException(e);
		}

        form.getHeader(context);

        StringBuilder sb = new StringBuilder();
        sb.append("<html>");

        sb.append("<head>"+new GoogleAnalyticsEcommerceTrackingPanel(context, payable)+"</head>");

        sb.append("<body onLoad='document.forms[0].submit();'>");


        if (!payable.getPaymentType().equals(PaymentType.PayPalDirect)) {
            sb.append("Redirecting to payment gateway");
            sb.append("<form method='" + form.getMethod() + "' action='" + form.getPaymentUrl() + "' id='paymentform'>");

            for (Map.Entry<String, String> entry : params.entrySet()) {
                sb.append(new HiddenTag(entry.getKey(), entry.getValue()));
            }

            sb.append(new SubmitTag("Continue to payment"));
        } else {
            sb.append("<form method='" + form.getMethod() + "' action='" + context.getRequestURL() + "' id='paymentform'>");

            for (Map.Entry<String, String> entry : params.entrySet()) {
                sb.append(new HiddenTag(entry.getKey(), entry.getValue()));
            }
        }
		sb.append("</form>");
		sb.append("</body></html>");

		this.content = sb.toString();
		this.contentType = "text/html";
	}
}
