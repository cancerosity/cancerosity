package org.sevensoft.ecreator.model.ecom.orders.export.csv.toysgg;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.CsvSaver;

/**
 * @author sks 15 May 2007 08:47:44
 *
 */
public class ToysCsv {

	private final Collection<Order>	orders;
	private final RequestContext		context;
	private Attribute				attribute;

	public ToysCsv(RequestContext context, Collection<Order> orders) {
		this.context = context;
		this.orders = orders;
		this.attribute = EntityObject.getInstance(context, Attribute.class, 8);
	}

	public ToysCsv(RequestContext context, Order order) {
		this(context, Collections.singleton(order));
	}

	private String[] getHeader() {

		List<String> row = new ArrayList();

		row.add("Order_No");
		row.add("Date_Required");
		row.add("Payment type");
		row.add("Delivery_Name");
		row.add("Delivery_Address_1");
		row.add("Delivery_Address_2");
		row.add("Delivery_Town");
		row.add("Delivery_County	");
		row.add("Delivery_Post_Code");
		row.add("Delivery_Country");
		row.add("Ship_Method");
		row.add("Ship_Date");

		// get first orders line count
		for (int n = 1; n <= orders.iterator().next().getLineCount(); n++) {
			row.add("Product_Code_" + n);
			row.add("Product_Name_" + n);
			row.add("Product_Options_" + n);
			row.add("Qty_" + n);
		}

		return row.toArray(new String[] {});
	}

	private String[] getRow(Order order) {

		List<String> row = new ArrayList();

		row.add(order.getOrderId());
		row.add(order.getDatePlaced().toString("dd/MM/yy"));
		row.add(order.getPaymentType().name());

		// delivery address
		if (order.hasDeliveryAddress()) {

			row.add(order.getDeliveryAddress().getName());
			row.add(order.getDeliveryAddress().getAddressLine1());
			row.add(order.getDeliveryAddress().getAddressLine2());
			row.add(order.getDeliveryAddress().getTown());
			row.add(order.getDeliveryAddress().getCounty());
			row.add(order.getDeliveryAddress().getPostcode());
			row.add(order.getDeliveryAddress().getCountry().toString());

		} else {

			for (int n = 0; n < 7; n++) {
				row.add(null);
			}
		}

		row.add(order.getDeliveryDetails());
		row.add(order.getDatePlaced().toString("dd/MM/yy"));

		// get first orders line count
		for (OrderLine line : order.getLines()) {

			if (line.hasItem()) {

				Item item = line.getItem();
				String productCode = item.getAttributeValue(attribute);

				row.add(productCode);
				row.add(item.getName());
				row.add(line.getOptionsDetails());
				row.add(String.valueOf(line.getQty()));
			}
		}

		CollectionsUtil.replaceNulls(row, "");
		return row.toArray(new String[] {});
	}

	public void output(Writer writer) {

		CsvManager csv = new CsvManager();
		csv.setSeparator(",");
		CsvSaver saver = csv.makeSaver(writer);

		saver.begin();
		saver.next(getHeader());

		for (Order order : orders) {

			saver.next(getRow(order));
		}

		saver.end();
	}

	public void output(File file) throws IOException {

		FileWriter writer = null;

		try {

			writer = new FileWriter(file);
			output(writer);

		} catch (IOException e) {
			e.printStackTrace();
			throw e;

		} finally {

			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
