package org.sevensoft.ecreator.model.ecom.payments.processors.googlecheckout;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.OrderHandler;
import org.sevensoft.ecreator.model.ecom.payments.forms.googlecheckout.GoogleCheckoutForm;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.commons.collections.maps.MultiValueMap;

import javax.servlet.ServletException;
import java.io.IOException;
import java.io.DataOutputStream;
import java.util.Map;
import java.util.Iterator;
import java.util.List;
import java.net.URL;
import java.net.HttpURLConnection;

import sun.misc.BASE64Encoder;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
@Path("admin-payments-processors-google-checkout.do")
public class GoogleCheckoutProcessor extends AdminHandler {

    public enum Commands {

        ChargeOrder("charge-order", "chargeOrder"),
        CancelOrder("cancel-order", "cancelOrder"),
        RefundOrder("refund-order", "refundOrder"),
        AuthorizeOrder("authorize-order", "authorizeOrder");

        private String command;
        private String action;


        Commands(String command, String action) {
            this.command = command;
            this.action = action;
        }


        public String getCommand() {
            return command;
        }

        public String getAction() {
            return action;
        }
    }

    public enum Reasons {

        ItemOutOfStock("Item out of stock"),
        CustomerRequestToCancel("Customer request to cancel"),
        ItemDiscontinued("Item discontinued"),
        Other("Other (describe below)");

        private String reason;

        Reasons(String reason) {
            this.reason = reason;
        }

        public String getReason() {
            return reason;
        }

        public String toString() {
            return getReason();
        }
    }

    /**
     * Common fields for all operations
     */
    private static final String COMMAND_TYPE = "_type";

    private Order order;
    private String googleOrderNumber;

    /**
     * fields for <charge-order> and <refund-order> operations
     */
    private String amount;
    private String currency;

    /**
     * fields for <canel-order> and <refund-order> operations
     */
    private String reason;
    private String comment;


    public GoogleCheckoutProcessor(RequestContext context) {
        super(context);
        Currency currency = Currency.getDefault(context);
        if (currency != null) {
            this.currency = currency.getName();
        } else {
            this.currency = "GBP";
        }
    }

    public Object main() throws ServletException {
        new GoogleCheckoutForm(context).getPaymentUrl();
        return null;
    }

    /**
     * Methos charges an order
     *
     * @return orderHandler
     */
    public Object chargeOrder() {
        test(new RequiredValidator(), "order");
        test(new RequiredValidator(), "amount");

        if (order.getGoogleOrderNumber() == null) {
            context.setError("googleOrderNumber", "Must be completed");
        }

        List<OrderLine> lines = order.getLines();
        if (lines.isEmpty() || lines.get(0).getItem() == null) {
            context.setError("item_name_1", "At least one item should be in the order");
        }

        if (hasErrors()) {
            context.addError(context.getErrors().toString());
            return new OrderHandler(context, order).main();
        }

        MultiValueMap<String, String> postData = new MultiValueMap<String, String>();
        postData.put(COMMAND_TYPE, Commands.ChargeOrder.getCommand());
        postData.put("google-order-number", order.getGoogleOrderNumber());
        postData.put("amount", amount);
        postData.put("amount.currency", currency);
        postData.put("item_name_1", lines.get(0).getItem().getName());
        postData.put("item_description_1", lines.get(0).getDescription());
        postData.put("item_quantity_1", String.valueOf(lines.get(0).getQty()));
        postData.put("item_price_1", lines.get(0).getUnitSellInc().toString());
        postData.put("item_currency_1", currency);
//        NameValuePair[] postData = {
//                new NameValuePair(COMMAND_TYPE, Commands.ChargeOrder.getCommand()),
//                new NameValuePair("google-order-number", order.getGoogleOrderNumber()),
//                new NameValuePair("amount", amount),
//                new NameValuePair("amount.currency", currency)
//        };

        postData(postData);

        //todo add parsing response
        /**
         * Requests to charge an order could generate the following errors:
         *
         * Invalid Argument Errors
         *
         * - The requested charge amount is greater than the remaining chargeable amount. (You are trying to charge the customer for more than the total order amount.)
         * - The requested charge amount is zero or negative.
         *
         * Invalid State Errors
         *
         * - The order can not be charged in its current financial order state.
         */

        return new OrderHandler(context, order).main();
    }

    /**
     * Method calcels an order
     *
     * @return OrderHandler
     */
    public Object cancelOrder() {
        test(new RequiredValidator(), "order");
        test(new RequiredValidator(), "reason");
        test(new RequiredValidator(), "comment");
        test(new LengthValidator(1, 140), "comment");

        if (order.getGoogleOrderNumber() == null) {
            context.setError("googleOrderNumber", "Must be completed");
        }

        List<OrderLine> lines = order.getLines();
        if (lines.isEmpty() || lines.get(0).getItem() == null) {
            context.setError("item_name_1", "At least one item should be in the order");
        }

        if (hasErrors()) {
            context.addError(context.getErrors().toString());
            return new OrderHandler(context, order).main();
        }
        MultiValueMap<String, String> postData = new MultiValueMap<String, String>();
        postData.put(COMMAND_TYPE, Commands.CancelOrder.getCommand());
        postData.put("google-order-number", order.getGoogleOrderNumber());
        postData.put("reason", reason);
        postData.put("comment", comment);
        postData.put("item_name_1", lines.get(0).getItem().getName());
        postData.put("item_description_1", lines.get(0).getDescription());
        postData.put("item_quantity_1", String.valueOf(lines.get(0).getQty()));
        postData.put("item_price_1", lines.get(0).getUnitSellInc().toString());
        postData.put("item_currency_1", currency);

//
//         NameValuePair[] postData = {
//                 new NameValuePair(COMMAND_TYPE, Commands.CancelOrder.getCommand()),
//                 new NameValuePair("google-order-number", order.getGoogleOrderNumber()),
//                 new NameValuePair("reason", reason),
//                 new NameValuePair("comment", comment),
////                 new NameValuePair("item_name_1", lines.get(0).getItem().getName()),
////                 new NameValuePair("item_description_1", lines.get(0).getDescription()),
////                 new NameValuePair("item_quantity_1", String.valueOf(lines.get(0).getQty()))//,
////                 new NameValuePair("item_price_1", lines.get(0).getUnitSellInc().toString()),
////                 new NameValuePair("item_currency_1", currency)
//         };
//
        postData(postData);
//
//        //todo add parsing response
//        /**
//         * Requests to cancel an order could generate the following errors:
//         *
//         * Invalid State Errors
//         *
//         * The order can not be canceled in its current financial order state.
//         */
        return new OrderHandler(context, order).main();
    }

    /**
     * todo next stage. refund operation is not suported by e-creator
     *
     * @return orderHandler
     */
    public Object refundOrder() {
        test(new RequiredValidator(), "order");
        test(new RequiredValidator(), "amount");
        test(new RequiredValidator(), "reason");
        test(new RequiredValidator(), "comment");
        test(new LengthValidator(1, 140), "comment");

        if (order.getGoogleOrderNumber() == null) {
            context.setError("googleOrderNumber", "Must be completed");
        }

        if (hasErrors()) {
            context.addError(context.getErrors().toString());
            return new OrderHandler(context, order).main();
        }

        MultiValueMap<String, String> postData = new MultiValueMap<String, String>();
        postData.put(COMMAND_TYPE, Commands.RefundOrder.getCommand());
        postData.put("google-order-number", order.getGoogleOrderNumber());
        postData.put("amount", amount);
        postData.put("amount.currency", currency);
        postData.put("comment", comment);
        postData.put("reason", reason);
//        NameValuePair[] postData = {
//                new NameValuePair(COMMAND_TYPE, Commands.RefundOrder.getCommand()),
//                new NameValuePair("google-order-number", order.getGoogleOrderNumber()),
//                new NameValuePair("amount", amount),
//                new NameValuePair("amount.currency", currency),
//                new NameValuePair("comment", comment),
//                new NameValuePair("reason", reason)
//        };

        postData(postData);

        //todo add parsing response
        /**
         *  Requests to refund an order could generate the following errors:
         *
         * Invalid Argument Errors
         *
         * The requested refund amount is greater than the amount charged. (You are trying to refund more money than the customer has spent.)
         * The requested refund amount is zero or negative.
         *
         * Invalid State Errors
         *
         * The order can not be refunded in its current financial order state.
         */
        return new OrderHandler(context, order).main();
    }

    /**
     * todo next stage
     *
     * @return orderHandler
     */
    public Object authorizeOrder() {
        test(new RequiredValidator(), "order");

        if (order.getGoogleOrderNumber() == null) {
            context.setError("googleOrderNumber", "Must be completed");
        }

        if (hasErrors()) {
            context.addError(context.getErrors().toString());
            return new OrderHandler(context, order).main();
        }

        MultiValueMap<String, String> postData = new MultiValueMap<String, String>();
        postData.put(COMMAND_TYPE, Commands.AuthorizeOrder.getCommand());
        postData.put("google-order-number", order.getGoogleOrderNumber());
//        NameValuePair[] postData = {
//                new NameValuePair(COMMAND_TYPE, Commands.AuthorizeOrder.getCommand()),
//                new NameValuePair("google-order-number", order.getGoogleOrderNumber())
//        };

        postData(postData);

        //todo add parsing response
        /**
         * Requests to reauthorize an order could generate the following errors:
         *
         * Invalid State Errors
         *
         * The order can not be reauthorized in its current financial order state.
         *
         * Invalid double authorization
         *
         * This error indicates that the order has already been authorized and that the existing authorization has not yet expired. The error message will specify the amount for which the customer's credit card is authorized as well as the date and time when the authorization expires. Note that your code should not require the error message to only contain the text Invalid double authorization since the message may also contain other text.
         */
        return new OrderHandler(context, order).main();

    }

    private void postData(MultiValueMap<String, String> postData) {
        try {

            getHttpClient(postData);

        } catch (IOException e) {

            logger.warning("[" + GoogleCheckoutProcessor.class.getName() + "] " + e);

        }
    }

    /**
     * Method returns HttpClient with common list of params
     *
     * @param postData request data
     * @throws java.io.IOException in case of exeption while executing postMethod
     */
    private void getHttpClient(MultiValueMap<String, String> postData) throws IOException {
        PaymentSettings paymentSettings = PaymentSettings.getInstance(context);

        String merchantId = paymentSettings.getGoogleCheckoutId();
        String password = paymentSettings.getGoogleCheckoutPw();

        String authorization = new BASE64Encoder().encode((merchantId + ":" + password).getBytes());

        String urlString = new GoogleCheckoutForm(context).getPaymentUrl(paymentSettings);

        HttpURLConnection connection = null;
        DataOutputStream printout = null;

        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();

            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setInstanceFollowRedirects(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Authorization", "Basic " + authorization);
//            connection.setRequestProperty("Host", connection.getURL().getHost());

            // Changed to allow i18n character sets to be processed properly
            connection.setRequestProperty("content-type", "application/html; charset=UTF-8");
            connection.setRequestProperty("accept", "application/html");

            printout = new DataOutputStream(connection.getOutputStream());
            printout.writeBytes(getPostData(postData));
            printout.flush();
            printout.close();

            getResponse(connection);

        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            if (printout != null) {
                printout.close();
            }
        }
    }

    private String getPostData(MultiValueMap<String, String> postData) {
        StringBuilder sb = new StringBuilder();
        Iterator<Map.Entry<String, String>> iter = postData.entrySet().iterator();
        while (iter.hasNext()) {

            Map.Entry<String, String> entry = iter.next();

            logger.fine(entry.getKey() + "=" + entry.getValue());

            sb.append(entry.getKey());
            sb.append("=");
            sb.append(entry.getValue());

            if (iter.hasNext()) {
                sb.append("&");
            }
        }
        return sb.toString();
    }

    /**
     * Method process the results of transactions
     *
     * @param connection HttpURLConnection
     * @throws java.io.IOException in case of getting responseBody
     */
    private void getResponse(HttpURLConnection connection) throws IOException {
        int response = connection.getResponseCode();
        if (response == HttpURLConnection.HTTP_OK) {
            context.addError(response + ": Complete !" + connection.getInputStream());
        } else {
            context.addError("[" + GoogleCheckoutProcessor.class.getName() + "] " + response + ":  " + connection.getErrorStream());
        }
    }

    @Override
    public boolean runSecure() {
        return Config.getInstance(context).isSecured();
    }
}
