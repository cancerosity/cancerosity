package org.sevensoft.ecreator.model.ecom.orders.emails;

import java.util.List;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.admin.ecom.orders.OrderHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.values.AValueRenderer;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 18-Jul-2005 00:46:18
 * 
 */
public class OrderAcknowledgementEmail extends AbstractOrderEmail {

	private boolean	adminLink;

    public OrderAcknowledgementEmail(RequestContext context, Order order) {
        this(context, order, false);
    }

    public OrderAcknowledgementEmail(RequestContext context, Order order, boolean admLink) {
        super(context, order);

        setFrom(company.getName(), config.getServerEmail());
        setSubject(Company.getInstance(context).getName() + " - Order acknowledgement #" + order.getOrderId());
        if (admLink)
            setAdminLink();

        String body = getBody();
        String conntentType = null;

        if (shoppingSettings.isOrderAcknowledgementHtml()) {
            body = body.replaceAll("\r\n", "<br />").replaceAll("\n", "<br />");
            conntentType = "text/html; charset=UTF-8";
        }

        setBody(body, conntentType);
    }

	public String getBody() {

		StringBuilder sb = new StringBuilder();

		if (adminLink) {

			sb.append("\nA new order has been received. To view it click this URL\n");
			sb.append(config.getUrl() + "/" + new Link(OrderHandler.class, null, "order", order.getId()));
			sb.append("\n\n\n");

		}

		if (shoppingSettings.hasOrderAcknowledgementHeader()) {
			sb.append(order.getAccount().emailTagsParse(shoppingSettings.getOrderAckHeader()));
		}

		sb.append("\n\n");

		sb.append("Want to track this order?\nVisit ");
		sb.append(order.getOrderStatusUrl());

		sb.append("\n\nORDER SUMMARY");
		sb.append("\nReference: " + order.getOrderId());
		sb.append("\nDate/Time: " + order.getDatePlaced().toString("dd MMM yyyy HH:mm"));
		if (order.hasCustomerReference()) {
			sb.append("\nCustomer reference: " + order.getCustomerReference());
		}
		sb.append("\n\n");

		// ----- customer details -------//
		sb.append("CUSTOMER INFORMATION\n");
		sb.append("---------------------------------------------------------\n");
		sb.append("Name: " + order.getAccount().getName());
		sb.append("\nEmail: " + order.getAccount().getEmail());
		sb.append("\n\n");

		if (orderSettings.hasAttributes()) {

			MultiValueMap<Attribute, String> attributeValues = order.getAttributeValues();
			if (attributeValues.size() > 0) {

				sb.append("ORDER INFORMATION\n");
				sb.append("---------------------------------------------------------\n");

				for (Attribute attribute : attributeValues.keySet()) {

					if (attribute.isCheckout()) {

						List<String> values = attributeValues.list(attribute);

						sb.append(attribute.getName() + ": ");
						sb.append(new AValueRenderer(context, order.getAccount(), attribute, values));

					}
				}

				sb.append("\n\n");
			}
		}

		// ----- payment details -------//
		sb.append("PAYMENT INFORMATION\n");
		sb.append("---------------------------------------------------------\n");
		sb.append(order.getPaymentType().toString());
		if (order.hasCard()) {
			sb.append("\n" + order.getCard().getSecureDescription());
		}
		sb.append("\n\n");

		// ----- delivery details -------//
		sb.append("DELIVERY ADDRESS\n");
		sb.append("---------------------------------------------------------\n");
		if (order.isReservation())
			sb.append("Reserved for pickup");
		else
			sb.append(order.getDeliveryAddress().getLabel("\n", false));

		sb.append("\n\n\n");
		sb.append("---------------------------------------------------------");
		sb.append("\n\n");

		items(sb);

		if (!order.isReservation()) {

			sb.append("Delivery Charge - ");
			sb.append(order.getDeliveryDetails());
			sb.append("\n     @ \243" + order.getDeliveryChargeEx() + "\n");
		}

		if (order.hasVoucher()) {

			sb.append("Voucher - " + order.getVoucherDescription());
			sb.append("\n     @ \243" + order.getVoucherDiscountInc() + "\n");
		}

		if (order.hasPaymentSurcharge()) {
			sb.append("Payment Surcharge");
			sb.append("\n     @ \243" + order.getPaymentCharge() + "\n");
		}

		sb.append("\n\nSubtotal: \t" + order.getTotalEx() + "\n");
		sb.append("Vat: \t" + order.getTotalVat() + "\n");
		sb.append("Total: \t" + order.getTotalInc() + "\n");

		sb.append("\n\n");

		if (shoppingSettings.hasOrderAcknowledgementFooter()) {
			sb.append(order.getAccount().emailTagsParse(shoppingSettings.getOrderAckFooter()));
		}

		return sb.toString();
	}

	public void setAdminLink() {
		this.adminLink = true;
	}

}
