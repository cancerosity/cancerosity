package org.sevensoft.ecreator.model.ecom.orders;

/**
 * @author sks 22-Dec-2005 14:48:20
 * 
 */
public class OrderStatusException extends Exception {

	public OrderStatusException() {
	}

	public OrderStatusException(String message, Throwable cause) {
	}

	public OrderStatusException(String message) {
	}

	public OrderStatusException(Throwable cause) {
	}

}
