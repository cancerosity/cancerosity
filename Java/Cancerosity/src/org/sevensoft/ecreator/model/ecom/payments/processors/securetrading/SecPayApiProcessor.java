package org.sevensoft.ecreator.model.ecom.payments.processors.securetrading;

import java.io.IOException;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentException;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardTypeException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CscException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiredCardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiryDateException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.IssueNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.StartDateException;
import org.sevensoft.ecreator.model.ecom.payments.processors.Processor;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorException;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorResult;
import org.sevensoft.ecreator.model.ecom.payments.results.AvsCheck;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 19 Apr 2007 10:22:14
 *
 */
public class SecPayApiProcessor extends Processor {

	protected SecPayApiProcessor(RequestContext context) {
		super(context);
	}

	@Override
	public AvsCheck avsCheck(Card card, Address address) throws IOException, ProcessorException, ExpiredCardException, ExpiryDateException,
			CardNumberException, CardException, IssueNumberException, CscException, PaymentException {
		return null;
	}

	@Override
	public Payment credit(Payment payment, Money money) throws IOException, ExpiryDateException, CardNumberException, CardTypeException, StartDateException,
			ProcessorException, CardException, IssueNumberException {
		return null;
	}

	@Override
	protected ProcessorResult debit(Item account, Card card, Address address, Money money) throws CardException, ExpiryDateException, IssueNumberException,
			CardTypeException, CscException, CardNumberException, StartDateException, IOException, ExpiredCardException, ProcessorException,
			PaymentException {
		return null;
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public boolean isLive() {
		return false;
	}

	@Override
	public void verify(Card card) throws CardException, ExpiryDateException, IssueNumberException, CardTypeException, CardNumberException,
			StartDateException, IOException, ExpiredCardException, ProcessorException, CscException {
	}

	@Override
	public boolean voidPayment(Payment payment) throws IOException, ProcessorException {
		return false;
	}

}
