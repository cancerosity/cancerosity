package org.sevensoft.ecreator.model.ecom.credits;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 19 Jan 2007 10:33:24
 *
 */
@Table("credits_settings")
@Singleton
public class CreditSettings extends EntityObject {

	public static CreditSettings getInstance(RequestContext context) {
		return EntityObject.getSingleton(context, CreditSettings.class);
	}

	private String	completedText;

	protected CreditSettings(RequestContext context) {
		super(context);
	}

	public final String getCompletedText() {
		return completedText == null ? "Your item is on its way!" : completedText;
	}

	public final void setCompletedText(String completedText) {
		this.completedText = completedText;
	}

}
