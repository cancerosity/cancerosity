package org.sevensoft.ecreator.model.ecom.orders.parcels.trackers;

/**
 * @author sks 20-Dec-2005 18:45:59
 * 
 */
public class AmtrackTracker extends Tracker {

	public String getTrackingUrl(String consignment) {
		return "http://www.amtrak.co.uk/cgi-bin/tracking.cgi?ConNr=" + consignment + "&ConLine=1";
	}

}
