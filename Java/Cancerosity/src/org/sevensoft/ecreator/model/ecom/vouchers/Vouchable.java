package org.sevensoft.ecreator.model.ecom.vouchers;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.items.Item;

/**
 * @author sks 7 Oct 2006 15:34:49
 * 
 * This interface represents an object that can accept a voucher.
 * 
 * At the moment that is basket and order but for the future listing, subscription, booking ?
 *
 */
public interface Vouchable {

	/**
	 * Returns the amount that should be valid for voucher discounts
	 */
	public Money getVoucherTotal();

	/**
	 * Returns the member who will use this voucher
	 */
	public Item getCustomer();

	/**
	 * Returns the qty of items for consideration by the voucher
	 */
	public int getVoucherQty();

}
