package org.sevensoft.ecreator.model.ecom.payments.forms.able2buy;

import java.util.Collections;
import java.util.List;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.comparators.NaturalStringComparator;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 12 Dec 2006 09:19:26
 *
 */
@Table("payments_able2buy_creditgroups")
public class CreditGroup extends EntityObject implements Selectable, Comparable<CreditGroup> {

	public static List<CreditGroup> get(RequestContext context) {
		List<CreditGroup> list = SimpleQuery.execute(context, CreditGroup.class);
		Collections.sort(list);
		return list;
	}

	private String	creditGroupId;
	private String	description;
	private double	factor;
	private int		deferredMonths;
	private int		repaymentMonths;
	private double	apr;

	private CreditGroup(RequestContext context) {
		super(context);
	}

	public CreditGroup(RequestContext context, String creditGroupId) {
		super(context);

		this.creditGroupId = creditGroupId;
		save();
	}

	public CreditGroup(RequestContext context, String creditGroupId, String description, double apr, int repaymentMonths, int deferredMonths, double factor) {
		super(context);

		this.creditGroupId = creditGroupId;
		this.description = description;
		this.apr = apr;
		this.repaymentMonths = repaymentMonths;
		this.deferredMonths = deferredMonths;
		this.factor = factor;

		save();
	}

	public int compareTo(CreditGroup o) {
		return NaturalStringComparator.instance.compare(description, o.description);
	}

	public Money getAmountOfCredit(Money money) {
		return money;
	}

	public double getApr() {
		return apr;
	}

	public final String getCreditGroupId() {
		return creditGroupId;
	}

	public final int getDeferredMonths() {
		return deferredMonths;
	}

	public Money getDeposit(Money money) {
		return new Money(0);
	}

	public final String getDescription() {
		return description == null ? "None" : description;
	}

	public final double getFactor() {
		return factor;
	}

	public String getLabel() {
		return getDescription();
	}

	public Money getMonthlyRepayment(Money amount) {
		return amount.multiply(factor);
	}

	public final int getRepaymentMonths() {
		return repaymentMonths;
	}

	public Money getTotalAmountRepayable(Money money) {
		return getMonthlyRepayment(money).multiply(repaymentMonths);
	}

	public String getValue() {
		return getIdString();
	}

	/**
	 * 
	 */
	public boolean hasDeferredMonths() {
		return deferredMonths > 0;
	}

	@Override
	protected void schemaInit(RequestContext context) {
		super.schemaInit(context);

		// if we have no credit groups then initialise defaults
		if (SimpleQuery.count(context, CreditGroup.class) == 0) {
			new CreditGroupInit(context).run();
		}
	}

	public final void setApr(double apr) {
		this.apr = apr;
	}

	public final void setCreditGroupId(String creditGroupId) {
		this.creditGroupId = creditGroupId;
	}

	public final void setDeferredMonths(int deferredMonths) {
		this.deferredMonths = deferredMonths;
	}

	public final void setDescription(String description) {
		this.description = description;
	}

	public final void setFactor(double factor) {
		this.factor = factor;
	}

	public final void setRepaymentMonths(int repaymentMonths) {
		this.repaymentMonths = repaymentMonths;
	}

}
