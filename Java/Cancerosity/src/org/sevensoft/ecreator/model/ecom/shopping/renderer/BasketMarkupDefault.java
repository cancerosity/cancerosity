package org.sevensoft.ecreator.model.ecom.shopping.renderer;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 29 Jun 2006 08:46:24
 *
 */
public class BasketMarkupDefault implements MarkupDefault {

	public String getBetween() {
		return null;
	}

	public String getBody() {

		StringBuilder sb = new StringBuilder();

        sb.append("<table cellspacing=\"0\" cellpadding=\"0\" class=\"basket\">\n");
        sb.append("\n");
        sb.append("\n");
        sb.append("<tr class='header'>\n");
        sb.append("<th colspan='2'>Item and Description</th>\n");
        sb.append("<th>Quantity</th>\n");
        sb.append("<th>Price</th>\n");
        sb.append("<th>Amount</th>\n");
        sb.append("</tr>\n");
        sb.append("\n");
        sb.append("[basket_lines]\n");
        sb.append("\n");
        sb.append("<tr class='subtotal'>\n");
        sb.append("<td align='right' colspan='5' class='price'>Items Total: [items_total]</td>\n");
        sb.append("</tr>\n");
        sb.append("\n");
        sb.append("<tr class='subtotal'>\n");
        sb.append("<td align='right' colspan='5' class='price'>[voucher]</td>\n");
        sb.append("</tr>\n");
        sb.append("\n");
        sb.append("<tr class='delivery-charge'>\n");
        sb.append("<td align='right' colspan='5' class='price'>Delivery Charge: [delivery_charge?inc=1]</td>\n");
        sb.append("</tr>\n");
        sb.append("\n");
        sb.append("<tr class='delivery-select'>\n");
        sb.append("<td colspan='5' align='right'>To change the delivery rate make a selection from the menu below<br/>\n");
        sb.append("[delivery_selector]\n");
        sb.append("</td>\n");
        sb.append("</tr>\n");
        sb.append("\n");
        sb.append("<tr class='total'>\n");
        sb.append("<td class='text' colspan='2'>To remove an item from the basket, change the quantity to 0 and click update</td>\n");
        sb.append("<td class='button'>[submit?class=basket_button&label=Update]</td>\n");
        sb.append("<td align='right' colspan='2' class='price'>Total: [total]</td>\n");
        sb.append("</tr>\n");
        sb.append("\n");
        sb.append("<tr class='checkout'>\n");
        sb.append("\n");
        sb.append("<td colspan='5' align='right'>[checkout?text=Proceed to checkout&class=basket_button]</td>\n");
        sb.append("</tr>\n");
        sb.append("\n");
        sb.append("</table>");

	/*	sb.append(new TableTag("basket"));

		// header row
		sb.append("\n\n<tr class='header'>\n");
		sb.append("<th colspan='2'>item and description</th>\n");
		sb.append("<th>qty</th>\n");
		sb.append("<th>price</th>\n");
		sb.append("<th>amount</th>\n");
		sb.append("</tr>\n\n");

		// LINES APPEAR HERE
		sb.append("[basket_lines]\n\n");

		// ITEMS TOTAL
		sb.append("<tr class='subtotal'>\n");
		sb.append("<td align='right' colspan='5' class='price'>items total: [items_total]</td>\n");
		sb.append("</tr>\n\n");

		sb.append("<tr class='subtotal'>\n");
		sb.append("<td align='right' colspan='5' class='price'>[voucher]</td>\n");
		sb.append("</tr>\n\n");

		// DELIVERY COST
		sb.append("<tr class='delivery-charge'>\n");
		sb.append("<td align='right' colspan='5' class='price'>delivery charge: [delivery_charge?inc=1]</td>\n");
		sb.append("</tr>\n\n");
*/
		// DELIVERY SELECTION
		//		SelectTag deliveryTag = new SelectTag(null, "delivery", basket.getDeliveryRate());
		//		deliveryTag.setOnChange("document.forms['basket_form'].submit();");
		//		Map<String, String> deliveryRates = basket.getDeliveryRates();
		//		if (deliveryRates.size() > 0)
		//			deliveryTag.addOptions(deliveryRates);
		//		else
		//			deliveryTag.setAny("-No delivery rates applicable-");

		sb.append("<tr class='delivery-select'>\n");
		sb.append("<td colspan='5' align='right'>To change the delivery rate make a selection from the menu below<br/>\n");
		sb.append("[delivery_selector]\n");
		sb.append("</td>\n</tr>\n\n");

		// TOTAL TOTAL
		sb.append("<tr class='total'>\n");
		sb.append("<td class='text' colspan='2'>To remove an item from the basket, change the quantity to 0 and click update</td>\n");
		sb.append("<td class='button'>[submit?class=basket_button&label=Update]</td>\n");
		sb.append("<td align='right' colspan='2' class='price'>total: [total]</td>\n");
		sb.append("</tr>\n\n");

		// CHECKOUT OPTION
		sb.append("<tr class='checkout'>\n\n");
		sb.append("<td colspan='5' align='right'>[checkout?label=Proceed to checkout&class=basket_button]</td>\n");
		sb.append("</tr>\n\n");
		sb.append("</table>\n");

		return sb.toString();
	}

	public String getCss() {

		StringBuilder sb = new StringBuilder();

        sb.append("table.basket { width: 100%; font-family: Verdana; color: #222222; font-size: 11px; }\n");
        sb.append("table.basket td { padding: 3px 6px } \n");
        sb.append("table.basket .header, table.bask .total { background: #ebebeb; color: #333333 }\n");
        sb.append("\n");
        sb.append("table.basket th { text-align: left; font-weight: bold; padding: 4px; } \n");
        sb.append("\n");
        sb.append("table.basket input { border: 1px solid #333333; color: #333333; padding: 2px 4px; background: #fff; font-size: 11px; font-weight: bold; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; }\n");
        sb.append("table.basket tr.total input:hover { background: green; color: #fff; cursor: pointer; }\n");
        sb.append("table.basket tr.checkout a { display: block; width: 150px; border: 1px solid #333333; color: #333333; padding: 3px 4px; background: white; font-size: 11px; font-weight: bold; text-align: center; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; }\n");
        sb.append("table.basket tr.checkout a:hover { background: green; color: #fff; }\n");
        sb.append("\n");
        sb.append("table.basket .price { font-weight: bold; } \n");
        sb.append("table.basket span.name { font-weight: bold; } \n");
        sb.append("table.basket .line td { padding-top: 10px; padding-bottom: 10px; border-bottom: 1px dotted #cccccc; } \n");
        sb.append("\n");
        sb.append("table.basket .info td { padding-top: 5px; padding-bottom: 5px; }\n");
        sb.append("table.basket td#image { width: 65px; } \n");
        sb.append("table.basket select { font-size: 11px; margin: 5px 0px; }\n");
        sb.append("\n");
        sb.append("table.basket input { margin: 3px 0; } \n");
        sb.append("\n");
        sb.append("div.message_error { color: red; font-weight: bold; font-size: 12px; padding: 5px; text-align: center; margin: auto;}\n");
        sb.append(" div.message_info { color: #222222; font-weight: bold; font-size: 12px; padding: 5px; text-align: center; margin: auto;}\n");


		/*sb.append("table.basket { width: 100%; font-family: Verdana; color: #222222; font-size: 11px; }\n");
		sb.append("table.basket td { padding: 3px 6px } \n");
		sb.append("table.basket .header, table.bask .total { background: #ebebeb; color: #333333 }\n\n");

		sb.append("table.basket th { text-align: left; font-weight: bold; padding: 4px; } \n\n");

		sb.append("table.basket input { border: 1px solid #333333; color: #333333; padding: 2px 4px; "
				+ "background: white; font-size: 11px; font-weight: bold; } \n\n");
		sb.append("table.basket .price { font-weight: bold; } \n");

		sb.append("table.basket span.name { font-weight: bold; } \n");
		sb.append("table.basket .line td { padding-top: 10px; padding-bottom: 10px; border-bottom: 1px dotted #cccccc; } \n\n");

		sb.append("table.basket .info td { padding-top: 5px; padding-bottom: 5px; }\n");
		sb.append("table.basket td#image { width: 65px; } \n");

		sb.append("table.basket select { font-size: 11px; margin: 5px 0px; }\n\n");

		sb.append("table.basket input { margin: 3px 0; } \n\n");

		sb.append("div.message_error { color: red; font-weight: bold; font-size: 12px; padding: 5px; text-align: center; margin: auto;}\n ");
		sb.append("div.message_info { color: #222222; font-weight: bold; font-size: 12px; padding: 5px; text-align: center; margin: auto;}\n\n ");
*/
		return sb.toString();
	}

	public String getEnd() {
		return null;
	}

	public String getName() {
		return null;
	}

	public String getStart() {
		return null;
	}

	public int getTds() {
		return 0;
	}

}
