package org.sevensoft.ecreator.model.ecom.shopping;

import org.jdom.JDOMException;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.attachments.AttachmentSupport;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.ecom.delivery.DeliverableItem;
import org.sevensoft.ecreator.model.ecom.delivery.DeliveryOption;
import org.sevensoft.ecreator.model.ecom.orders.VatHelper;
import org.sevensoft.ecreator.model.ecom.vouchers.VoucherItem;
import org.sevensoft.ecreator.model.ecom.vouchers.Voucher;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.ecreator.model.items.pricing.overrides.PriceOverride;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;

import java.io.IOException;
import java.io.File;
import java.util.*;

/**
 * @author Sam 08-Sep-2003 11:45:47
 */
@Table("basket_lines")
public class BasketLine extends AttachmentSupport implements DeliverableItem {

    @Index
    private Basket basket;

    private String description;

    /**
     * Date this line was added
     */
    private Date date;

    /**
     * String containing details of the options ordered for quick rendering.
     */
    private String optionsDescription;

    /**
     *
     */
    private int qty;

    /**
     * We should set price of item in the basket, not for sale necessarily but in case this particular
     * basket has overriden the normal price, say by a price override
     */
    private Money salePrice;

    /**
     *
     */
    private int optionsCount;

    /**
     *
     */
    private double vatRate;

    /**
     *
     */
    @Index
    private Item item;

    /**
     * The ident is a special random string generated
     * so that when an item is added to a basket, another one won't be added with the same hash.
     */
    @Index
    private String ident;

    /**
     * Cost price of this line
     */
    private Money costPrice;

    private boolean priceOverridden;
    /**
     * This line was created as a promotion item and cannot be edited by the user.
     * All promotion lines will be removed each time the basket is recalced
     */
    private boolean promotion;

    private BasketLine(RequestContext context) {
        super(context);
    }

    public BasketLine(RequestContext context, Basket basket, Item item, Money sellPrice) {

        super(context);

        this.item = item;
        this.basket = basket;
        this.date = new Date();

        this.description = item.getName();
        this.vatRate = item.getVatRate();

        this.qty = 1;

        this.salePrice = sellPrice;
        this.costPrice = item.getCostPrice();
    }

    BasketLine(RequestContext context, Basket basket, String ident, Item item, int qty, Map<ItemOption, String> options, Map<ItemOption, Upload> uploads,
               String sessionId) {

        super(context);

        // refresh the item
        try {

            logger.fine("refreshing item price");
            item.refresh();

        } catch (IOException e) {
            logger.warning(e.toString());

        } catch (JDOMException e) {
            logger.warning(e.toString());
        }

        this.ident = ident;
        this.item = item;
        this.basket = basket;
        this.date = new Date();

        this.description = item.getName();
        this.vatRate = item.getVatRate();

        setQty(qty);

        Item account = basket.getAccount() != null ? basket.getAccount() : (Item) context.getAttribute("account");
        if (basket.hasVoucher() && basket.getVoucher().isGetOneFree() && !basket.getVoucher().isGetExtraItem()) {

            Query q = new Query(context, "select voucher, item from # vi join # v where v.code " +
                    "= ? and vi.item = ? and vi.voucher = v.id");
            q.setTable(VoucherItem.class);
            q.setTable(Voucher.class);
            q.setParameter(basket.getVoucher().getCode());
            q.setParameter(item);

            if (q.execute().isEmpty()) {
                this.salePrice = item.getSellPrice(account, qty, null, false);
            } else {
                this.salePrice = new Money(0.00);
            }

        } else if (basket.hasVoucher() && basket.getVoucher().isGetOneFree() && basket.getVoucher().isGetExtraItem()) {
            this.salePrice = new Money(0.00);
        } else if (!basket.hasVoucher() || !basket.getVoucher().isGetOneFree()) {
            this.salePrice = item.getSellPrice(account, qty, null, false);
        }
        this.costPrice = item.getCostPrice();

        /*
           * Lets check if we have a price override
           */
        if (basket.hasAccount()) {

            logger.fine("checking for price override for customer=" + basket.getAccount());

            PriceOverride override = basket.getAccount().getPriceOverride(item);
            if (override != null && override.getPrice().isLessThan(salePrice)) {

                this.salePrice = override.getPrice();

                // set price overridden field so that the price isn't adjusted back up each time the product price changes
                this.priceOverridden = true;
            }
        }

        save();

        addOptions(options);
        addOptionAttachments(uploads);

        flashUploads(item);

    }

    BasketLine(RequestContext context, Basket basket, String description, Money sellPrice) {

        super(context);

        this.basket = basket;
        this.date = new Date();

        this.description = description;
        //vat should only apply to the total discounted figure and not for promotion lines
//        this.vatRate = 17.5;
        this.vatRate = 0.0;

        this.qty = 1;

        this.salePrice = sellPrice;
        this.costPrice = new Money(0);

        save();
    }

    public void addOptionAttachments(Map<ItemOption, Upload> uploads) {

        if (uploads != null) {

            logger.fine("iterating uploads:" + uploads);

            for (Map.Entry<ItemOption, Upload> entry : uploads.entrySet()) {

                ItemOption option = entry.getKey();
                Upload upload = entry.getValue();

                logger.fine("option=" + option + ", upload=" + upload);

                BasketLineOption blo = new BasketLineOption(context, this, option, upload);
                this.salePrice = this.salePrice.add(blo.getSalePrice());

                try {
                    blo.addAttachment(upload, false);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (AttachmentLimitException e) {
                    e.printStackTrace();
                } catch (AttachmentExistsException e) {
                    e.printStackTrace();
                } catch (AttachmentTypeException e) {
                    e.printStackTrace();
                }
            }

            setOptionsDescription();
            setOptionsSellPrices();
            setOptionsCount();
            save();
        }
    }

    private void addOptions(Map<ItemOption, String> options) {

        if (options != null) {

            logger.fine("[BasketLine] iterating options: " + options);

            Iterator<Map.Entry<ItemOption, String>> iter = options.entrySet().iterator();
            while (iter.hasNext()) {

                Map.Entry<ItemOption, String> entry = iter.next();
                ItemOption option = entry.getKey();

                // only process option if it is for this item
                if (option.getItem().equals(item)) {

                    logger.fine("[BasketLine] option: " + option);

                    if (option.isSelection()) {

                        try {

                            logger.fine("[BasketLine] selection option, retriving from selection id");

                            ItemOptionSelection selection = option.getSelectionFromId(Integer.parseInt(entry.getValue().trim()));
                            if (selection != null) {
                                logger.fine("[BasketLine] selection found=" + selection);
                                new BasketLineOption(context, this, option, selection);
                            }

                        } catch (RuntimeException e) {
                            logger.warning(e.toString());
                        }

                    } else {

                        String text = entry.getValue();
                        if (text != null) {
                            logger.fine("[BasketLine] Adding option as text: " + text);
                            new BasketLineOption(context, this, option, text);
                        }
                    }

                }
            }

            setOptionsDescription();
            setOptionsSellPrices();
            setOptionsCount();
            save();
        }
    }

    @Override
    public synchronized boolean delete() {
        removeOptions();
        return super.delete();
    }

    private void flashUploads(Item item) {

        // check for flash uploader
        File file = context.getRealFile("/uploads");
        File[] aa = file.listFiles();
        if (aa != null) {
            List<File> files = Arrays.asList(aa);
            Collections.sort(files, new Comparator() {

                public int compare(Object o1, Object o2) {
                    File f1 = (File) o1;
                    File f2 = (File) o2;
                    if (f1.lastModified() < f2.lastModified())
                        return -1;
                    else
                        return 1;
                }
            });

            if (aa.length > 0) {

                for (ItemOption option : item.getOptionSet().getOptions()) {

                    if (option.getType() == ItemOption.Type.FlashUpload) {

                        BasketLineOption blo = new BasketLineOption(context, this,
                                option, files.get(0).getName());
                        try {
                            blo.addAttachment(files.get(0), files.get(0).getName());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                // delete them all
                for (File f : files)
                    f.delete();
            }
        }
    }

    public int getAttachmentLimit() {
        return 0;
    }

    public String getAvailability() {
        return getItem().getAvailability();
    }

    public Basket getBasket() {
        return basket.pop();
    }

    public Date getDate() {
        return date == null ? new Date() : date;
    }

    public int getDeliveryAttributeValue() {

        int total = 0;
        if (hasItem()) {

            for (Map.Entry<Attribute, String> entry : getItem().getAttributeValues().entrySet()) {

                if (entry.getKey().isDelivery()) {

                    try {

                        total = total + (qty * Integer.parseInt(entry.getValue().trim()));

                    } catch (NumberFormatException e) {
                        //	e.printStackTrace();
                    }

                }
            }
        }

        return total;
    }

    public int getDeliveryQty() {
        return qty;
    }

    public Money getDeliveryRate(DeliveryOption option) {

        if (item == null) {
            return null;
        } else {
            return getItem().getDelivery().getDeliveryRate(option);
        }
    }

    public Money getDeliverySurcharge() {

        if (item == null) {
            return null;
        } else {
            return getItem().getDelivery().getDeliverySurcharge();
        }
    }

    /**
     * Returns the description including options
     *
     * @return
     */
    public String getFullDescription() {

        if (hasOptions())
            return getName() + "-" + getOptionsDescription();
        else
            return getName();
    }

    public final String getIdent() {
        return ident;
    }

    public Item getItem() {
        return (Item) (item == null ? null : item.pop());
    }

    public Money getLineSalePriceEx() {
        return salePrice.multiply(qty);
    }

    public Money getLineSalePriceInc() {
        return getLineSalePriceEx().add(getLineSalePriceVat());
    }

    public Money getLineSalePriceVat() {
        return VatHelper.vat(context, getLineSalePriceEx(), vatRate);
    }

    public String getName() {
        return description;
    }

    public List<BasketLineOption> getOptions() {

        Query q = new Query(context, "select * from # where line=?");
        q.setTable(BasketLineOption.class);
        q.setParameter(this);
        q.order("id");
        return q.execute(BasketLineOption.class);
    }

    public int getOptionsCount() {
        return optionsCount;
    }

    public String getOptionsDescription() {
        return optionsDescription;
    }

    public Money getPriceForDelivery() {
        return salePrice.multiply(qty);
    }

    public int getQty() {
        return qty;
    }

    public int getQtyForDelivery() {

        if (hasItem()) {

            return qty;

        } else {

            return 0;

        }
    }

    /**
     * Returns price for this product taking into account any agreed price match if lower.
     *
     * @return
     */
    public Money getSalePriceEx() {
        return salePrice;
    }

    public Money getSalePriceInc() {
        return salePrice.multiply(vatRate / 100 + 1);
    }

    public Money getSalePriceVat() {
        return salePrice.multiply(vatRate / 100d);
    }

    public double getWeightForDelivery() {

        if (item == null) {

            return 0;

        } else {

            String value = getItem().getAttributeValue("weight", true);
            if (value == null) {
                return 0;
            }

            try {

                final double w = Double.parseDouble(value) * qty;
                logger.fine("[BasketLine] weightForDel=" + w);
                return w;

            } catch (NumberFormatException e) {
                return 0;
            }
        }
    }

    public boolean hasItem() {
        return item != null;
    }

    public boolean hasOptions() {
        return optionsCount > 0;
    }

    public boolean hasVat() {
        return vatRate > 0;
    }

    public boolean isAvailable() {
        return getItem().isAvailable();
    }

    public boolean isIncludedForDelivery() {
        return hasItem();
    }

    public boolean isPromotion() {
        return promotion;
    }

    void recalc() {

        if (item == null) {
            return;
        }

        // only update price if this is not an overridden price
        if (!priceOverridden) {
            if (!basket.hasVoucher() || !basket.getVoucher().isGetOneFree()) {
                Item account = basket.getAccount() != null ? basket.getAccount() : (Item) context.getAttribute("account");
                salePrice = getItem().getSellPrice(account, qty, null, false);
            }
        }

        vatRate = getItem().getVatRate();

        setOptionsSellPrices();

        logger.fine("[BasketLine] recalculating basket price, qty=" + qty + ", item=" + item + ", price=" + salePrice + ", vatRate=" + vatRate);

        save();
    }

    public void refresh() throws IOException, JDOMException {

        if (hasItem()) {
            getItem().refresh();
        }
    }

    private void removeOptions() {
        Query q = new Query(context, "delete from # where line=?");
        q.setTable(BasketLineOption.class);
        q.setParameter(this);
        q.run();
    }

    public void setDescription(String desc) {
        description = desc;
    }

    void setItem(Item item) {
        this.item = item;
    }

    private void setOptionsCount() {
        this.optionsCount = getOptions().size();
    }

    private void setOptionsDescription() {

        StringBuilder sb = new StringBuilder();
        for (BasketLineOption option : getOptions()) {

            sb.append(option.getName());
            sb.append(": ");
            sb.append(option.getText());

            if (option.getProductCode() != null) {
                sb.append(" - ");
                sb.append(option.getProductCode());
            }
        }

        this.optionsDescription = sb.toString();
    }

    private void setOptionsSellPrices() {
        Money itemPrice = salePrice;
        for (BasketLineOption option : getOptions()) {

            if (option.getItemOption() != null && option.getItemOption().isPriceOverride() && option.getSalePrice().getAmount() != 0) {
                salePrice = salePrice.subtract(itemPrice);
            }

            salePrice = salePrice.add(option.getSalePrice());

            costPrice = costPrice.add(option.getCostPrice());
        }
    }

    public void setPromotion(boolean promotion) {
        this.promotion = promotion;
    }

    /**
     * Updates the qty on this basket line
     * <p/>
     * Returns true if the basket line qty was changed.
     *
     * @param q
     * @return
     */
    boolean setQty(int q) {

        if (qty == q)
            return false;

        int max = getItem().getOrderQtyMax();
        int min = getItem().getOrderQtyMin();

        if (max > 0 && q > max)
            q = max;

        if (q < min && min > 0)
            q = min;

        qty = q;
        recalc();

        return true;
    }

    List<String> getOptionsValues() {
        List<String> optionsValues = new ArrayList<String>();
        List<BasketLineOption> basketLineOptions = getOptions();
        for (BasketLineOption basketLineOption : basketLineOptions) {
            optionsValues.add(basketLineOption.getText());
        }
        return optionsValues;
    }

}