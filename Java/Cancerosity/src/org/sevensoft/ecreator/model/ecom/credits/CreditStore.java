package org.sevensoft.ecreator.model.ecom.credits;

import java.util.List;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.ecreator.model.accounts.subscriptions.Subscription;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 10 Jan 2007 06:54:55
 *
 */
@Table("credits_store")
public class CreditStore extends EntityObject {

	private Item	account;
	private int		credits;
	private boolean	introductoryUsed;
	private int		bonusCredits;

	protected CreditStore(RequestContext context) {
		super(context);
	}

	public CreditStore(RequestContext context, Item account) {
		super(context);
		this.account = account;
		save();
	}

	/**
	 * Add credits programatically 
	 * @param string 
	 */
	public void addCredits(int c, String string) {
		this.credits += c;
		new CreditHistory(context, account, "asasd");
	}

	/**
	 * Add credits from a credit package.
	 * 
	 */
	public void credit(CreditPackage creditPackage, String ipAddress) {

		logger.info("old credit amount=" + credits);

		// add the credit package to the credits
		this.credits = this.credits + creditPackage.getCredits();
		this.introductoryUsed = true;

		logger.info("credits after package applied=" + credits);

		save();

	}

	public Item getAccount() {
		return account;
	}

	public int getBonusCredits() {
		return bonusCredits;
	}

	public List<CreditPackage> getCreditPackages() {

		// get packages set to this account type
		List<CreditPackage> packages = account.getItemType().getCreditPackages();

		// add in from subscriptions
		Subscription sub = account.getSubscription();
		if (sub.hasSubscriptionLevel()) {
			packages.addAll(sub.getSubscriptionLevel().getCreditPackages());
		}

		// strip out any introductory ones if we can't use them.
		if (introductoryUsed) {

			logger.fine("[CreditStore] stripping intro packages");
			CollectionsUtil.filter(packages, new Predicate<CreditPackage>() {

				public boolean accept(CreditPackage e) {
					return !e.isIntroductory();
				}
			});
		}
		return packages;
	}

	public int getCredits() {
		return credits;
	}

	public boolean hasCredits() {
		return credits > 0;
	}

	public boolean isIntroductoryUsed() {
		return introductoryUsed;
	}

	public void setBonusCredits(int bonusCredits) {
		this.bonusCredits = bonusCredits;
	}

	public void setCredits(int credits) {
		this.credits = credits;
	}

	public void spendCredits(int c) {

		logger.info("old credit amount before spending credits=" + this.credits);

		this.credits = this.credits - c;

		logger.info("new credit amount after spending credits=" + this.credits);
		save("credits");
	}
}
