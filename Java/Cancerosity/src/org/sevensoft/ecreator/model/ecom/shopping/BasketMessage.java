package org.sevensoft.ecreator.model.ecom.shopping;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 10 Jul 2006 19:25:00
 *
 */
@Table("basket_messages")
public class BasketMessage extends EntityObject {

	private String	message;
	private Basket	basket;

	public BasketMessage(RequestContext context) {
		super(context);
	}

	public BasketMessage(RequestContext context, Basket basket, String message) {
		super(context);
		this.basket = basket;
		this.message = message;
	}

	public Basket getBasket() {
		return basket.pop();
	}

	public String getMessage() {
		return message;
	}

}
