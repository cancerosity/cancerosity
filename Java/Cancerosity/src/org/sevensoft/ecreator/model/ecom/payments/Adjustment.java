package org.sevensoft.ecreator.model.ecom.payments;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 17 Apr 2007 12:00:52
 *
 */
@Table("payments_adjustments")
public class Adjustment extends EntityObject {

	private Item	account;
	private Money	amount;
	private Date	date;

	protected Adjustment(RequestContext context) {
		super(context);
	}

	public Adjustment(RequestContext context, Item account, Date date, Money amount) {
		super(context);
		this.account = account;
		this.date = date;
		this.amount = amount;
		save();
	}

	public final Item getAccount() {
		return account.pop();
	}

	public final Money getAmount() {
		return amount;
	}

	public final Date getDate() {
		return date;
	}

}
