package org.sevensoft.ecreator.model.accounts.login.boxes;

import org.sevensoft.ecreator.iface.admin.accounts.boxes.LoginBoxHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.PasswordTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 31-Oct-2005 10:41:09
 * 
 */
@Table("boxes_login")
@Label("Login")
@HandlerClass(LoginBoxHandler.class)
public class LoginBox extends Box {

	public enum DisplayType {
		Visitor, Always
	}

	private DisplayType	displayType;

	public LoginBox(RequestContext context) {
		super(context);
	}

	public LoginBox(RequestContext context, String location) {
		super(context, location);
	}

	@Override
	protected String getCssIdDefault() {
		return "member_login";
	}

	public LoginBox.DisplayType getDisplayType() {
		return displayType == null ? DisplayType.Visitor : displayType;
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	public String render(RequestContext context) {

		switch (getDisplayType()) {

		case Always:
			break;

		default:
		case Visitor:

			if (context.getAttribute("account") != null) {
				return null;
			}
		}

		StringBuilder sb = new StringBuilder();
		sb.append(getTableTag());
		sb.append(new FormTag(LoginHandler.class, "login", "POST"));

		sb.append("<tr><td>");

		sb.append("<div class='info'>To log into your account enter your email and password.</div>");

		sb.append("<div class='email'>Email:</div>");
		sb.append("<div class='email_input'>" + new TextTag(null, "email", 12) + "</div>");

		sb.append("<div class='password'>Password:</div>");
		sb.append("<div class='password_input'>" + new PasswordTag(null, "password", 12) + "</div>");

		sb.append("<div>" + new SubmitTag("Login") + "</div>");

		sb.append("</td></tr>");

		sb.append("</form>");

		sb.append("<tr><td class='bottom'>&nbsp;</td></tr>");
		sb.append("</table>");

		return sb.toString();
	}

	public void setDisplayType(DisplayType displayType) {
		this.displayType = displayType;
	}
}
