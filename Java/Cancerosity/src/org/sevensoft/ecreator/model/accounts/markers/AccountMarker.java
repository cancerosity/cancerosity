package org.sevensoft.ecreator.model.accounts.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.config.Captions;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 03-Apr-2006 11:14:31
 *
 */
public class AccountMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.Accounts.enabled(context)) {
			return null;
		}

		if (!params.containsKey("text")) {
			params.put("text", Captions.getInstance(context).getAccountCaption());
		}

		return super.link(context, params, new Link(AccountHandler.class), "account_link");

	}

	public Object getRegex() {
		return "account";
	}
}