package org.sevensoft.ecreator.model.accounts.bots;

import org.sevensoft.commons.samdate.Period;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 31 Jan 2007 10:30:28
 * 
 * Remind people to log in if they haven't for so long
 *
 */
@Table("access_reminder_bots")
public class AccessReminderBot extends EntityObject {

	private Period	period;

	/**
	 * The account type
	 */
	private ItemType	itemType;

	private String	emailBody;

	private String	emailSubject;

	public AccessReminderBot(RequestContext context) {
		super(context);
	}

	public AccessReminderBot(RequestContext context, ItemType itemType) {
		super(context);
		this.itemType = itemType;
		save();
	}

	public final String getEmailBody() {
		return emailBody;
	}

	public final String getEmailSubject() {
		return emailSubject;
	}

	public final ItemType getItemType() {
		return itemType;
	}

	public final Period getPeriod() {
		return period;
	}

	public final void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}

	public final void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public final void setPeriod(Period period) {
		this.period = period;
	}
}
