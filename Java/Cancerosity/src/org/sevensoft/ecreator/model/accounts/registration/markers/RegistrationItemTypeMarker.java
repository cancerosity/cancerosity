package org.sevensoft.ecreator.model.accounts.registration.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.accounts.registration.RegistrationSession;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 03-Apr-2006 11:02:07
 *
 */
public class RegistrationItemTypeMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		RegistrationSession registrationSession = (RegistrationSession) context.getAttribute("registrationSession");
		if (registrationSession == null) {
			return null;
		}

		ItemType itemType = registrationSession.getItemType();
		if (itemType == null) {
			return null;
		}

		return super.string(context, params, itemType.getName());
	}

	@Override
	public String getDescription() {
		return "Shows the current account type the user is registering for";
	}

	public Object getRegex() {
		return "register_itemtype";
	}
}