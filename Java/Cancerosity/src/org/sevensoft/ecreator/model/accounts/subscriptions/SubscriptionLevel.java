package org.sevensoft.ecreator.model.accounts.subscriptions;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.sevensoft.ecreator.model.accounts.permissions.Domain;
import org.sevensoft.ecreator.model.accounts.permissions.DomainUtil;
import org.sevensoft.ecreator.model.accounts.permissions.Permission;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.accounts.permissions.Permissions;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.ecom.credits.CreditPackage;
import org.sevensoft.ecreator.model.ecom.credits.CreditPackageAccess;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;
import org.sevensoft.ecreator.model.items.listings.ListingPackageAccess;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 28 May 2006 13:53:13
 *
 *  Subscription is the level of subscription a member has inside their member type. 
 */
@Table("subscriptions_levels")
public class SubscriptionLevel extends EntityObject implements Positionable, Domain, Selectable, Comparable<SubscriptionLevel> {

	/**
	 * Returns all subscripton levels
	 */
	public static List<SubscriptionLevel> get(RequestContext context) {
		Query q = new Query(context, "select * from # order by itemType, name");
		q.setTable(SubscriptionLevel.class);
		return q.execute(SubscriptionLevel.class);
	}

	public static List<SubscriptionLevel> getAvailable(RequestContext context) {
		Query q = new Query(context, "select * from # where available=1 order by name");
		q.setTable(SubscriptionLevel.class);
		return q.execute(SubscriptionLevel.class);
	}

	/**
	 * The item type this subscription is valid for
	 * BC
	 */
	private ItemType			itemType;

	/**
	 * Override item type markup for items with this subscription level. But will only use the main part
	 */
	private Markup			listMarkup;

	/*
	 * live means the subscription can be choosen
	 * if not live then it is a legacy subscription that will be deleted once all existing members of it are expired
	 */
	private boolean			live;

	/**
	 * The name of this member level, eg, bronze, silver, gold
	 */
	private String			name;

	/**
	 * Comments about this subscription
	 */
	private String			comments;

	private String			icon;

	private Map<String, String>	features;

	/**
	 * 
	 */
	private int				position;

	/**
	 * Can be choosen only in admin
	 */
	private boolean			hidden;

	/*
	 * The days when you want a reminder to go out, eg 1 3 5 would send out a reminder 1 3 and 5 days from expiration.
	 * 
	 */
	private SortedSet<Integer>	subscriptionReminderDays;

	/**
	 * Override the email from the module by specifying on here
	 * 
	 */
	private String			subscriptionReminderBody;

	/**
	 * 
	 */
	private boolean			autoSubscribe;

	/**
	 * Number of profile views per day
	 */
	private int				dailyProfileViews;

	/**
	 * DAily pm limit
	 */
	private int				dailyPmLimit;

	private boolean			prioritise;

	private SubscriptionLevel(RequestContext context) {
		super(context);
	}

	public SubscriptionLevel(RequestContext context, ItemType itemType, String string) {
		super(context);

		this.itemType = itemType;
		this.name = string;
		this.live = true;

		save();

		addSubscriptionPackage();
	}

	public SubscriptionLevel addSubscription(String name) {
		return new SubscriptionLevel(context, itemType, name);
	}

	public SubscriptionRate addSubscriptionPackage() {
		SubscriptionRate s = new SubscriptionRate(context, this);
		return s;
	}

	public int compareTo(SubscriptionLevel o) {
		return name.toLowerCase().compareTo(o.getName().toLowerCase());
	}

	@Override
	public synchronized boolean delete() {

		// We can only delete a subscription if no one is using it
		if (isUsed()) {
			this.live = false;
			save();

			return false;
		}

		SimpleQuery.delete(context, Permission.class, "domain", getFullId());

		return super.delete();
	}

	public synchronized boolean delete(SubscriptionLevel subscription) {

		/*
		 * When deleting a subscription we must ensure all members of the subscription are set to the subscription parameter (which
		 * might be null)
		 */
		Query q = new Query(context, "update # set subscription=?");
		q.setTable(Item.class);
		q.setParameter(subscription == null ? 0 : subscription.getId());
		q.run();

		// remove from searches
		new Query(context, "update # set subscription=0 where subscription=?").setTable(Search.class).setParameter(this).run();

		return super.delete();
	}

	public final String getComments() {
		return comments;
	}

	public List<CreditPackage> getCreditPackages() {
		Query q = new Query(context, "select cp.* from # cp join # cpa on cp.id=cpa.creditPackage where cpa.subscriptionLevel=?");
		q.setTable(CreditPackage.class);
		q.setTable(CreditPackageAccess.class);
		q.setParameter(this);
		return q.execute(CreditPackage.class);
	}

	public int getDailyPmLimit() {
		return dailyPmLimit;
	}

	public int getDailyProfileViewsLimit() {
		return dailyProfileViews;
	}

	public String getDomainName() {
		return getName();
	}

	/**
	 * 
	 */
	public Map<SubscriptionFeature, String> getFeatureValues() {
		Query q = new Query(context, "select feature, value from # where subscriptionLevel=?");
		q.setParameter(this);
		q.setTable(FeatureValue.class);
		return q.getMap(SubscriptionFeature.class, String.class, new LinkedHashMap());
	}

	public final String getIcon() {
		return icon;
	}

	public ItemType getItemType() {
		return itemType.pop();
	}

	public String getLabel() {
		return getName();
	}

	/**
	 * Return all listing packages directly assigned to this subscription
	 * 
	 */
	public List<ListingPackage> getListingPackages() {

		Query q = new Query(context,
				"select lp.* from # lp join # lpo on lp.id=lpo.listingPackage where lpo.subscriptionLevel=? and lp.deleted=0 order by id");
		q.setTable(ListingPackage.class);
		q.setTable(ListingPackageAccess.class);
		q.setParameter(this);
		return q.execute(ListingPackage.class);
	}

	public final Markup getListMarkup() {
		return (Markup) (listMarkup == null ? null : listMarkup.pop());
	}

	public final String getName() {
		return name;
	}

	public List<Permission> getPermissions() {
		return DomainUtil.getPermissions(context, this, null);
	}

	public List<Permission> getPermissions(Permissions p) {
		return DomainUtil.getPermissions(context, this, p);
	}

	public int getPosition() {
		return position;
	}

	public List<SubscriptionRate> getSubscriptionRates() {
		return SimpleQuery.execute(context, SubscriptionRate.class, "subscriptionLevel", this);
	}

	public final String getSubscriptionReminderBody() {
		return subscriptionReminderBody;
	}

	public final SortedSet<Integer> getSubscriptionReminderDays() {
		if (subscriptionReminderDays == null) {
			return new TreeSet<Integer>();
		}
		return subscriptionReminderDays;
	}

	public String getValue() {
		return getIdString();
	}

	public boolean hasComments() {
		return comments != null;
	}

	public boolean hasIcon() {
		return icon != null;
	}

	public boolean hasListMarkup() {
		return listMarkup != null;
	}

	public boolean hasPermission(PermissionType type, Permissions p) {
		return DomainUtil.hasPermission(context, this, type, p);
	}

	public boolean hasSubscriptionRates() {
		return !getSubscriptionRates().isEmpty();
	}

	public boolean hasSubscriptionReminderBody() {
		return subscriptionReminderBody != null;
	}

	public boolean hasSubscriptionReminderEmail() {
		return subscriptionReminderBody != null;
	}

	/**
	 * @return
	 */
	public boolean isAutoSubscribe() {
		return autoSubscribe;
	}

	/**
	 * Returns true if this subscription is free.
	 * It is free if it has no rates defined or the rates are free
	 */
	public boolean isFree() {

		List<SubscriptionRate> rates = getSubscriptionRates();
		if (rates.isEmpty())
			return true;

		for (SubscriptionRate rate : rates)
			if (rate.isFree())
				return true;

		return false;
	}

	public final boolean isHidden() {
		return hidden;
	}

	public final boolean isLive() {
		return live;
	}

	public boolean isPrioritise() {
		return prioritise;
	}

	private boolean isUsed() {
		return SimpleQuery.count(context, Subscription.class, "subscriptionLevel", this) > 0;
	}

	private void removeFeatureValues() {
		SimpleQuery.delete(context, FeatureValue.class, "subscriptionLevel", this);
	}

	public void removeListingPackage(ListingPackage listingPackage) {
		listingPackage.delete();
	}

	private void removeListingPackages() {

		for (ListingPackage lp : getListingPackages())
			removeListingPackage(lp);
	}

	public void removePermissions(Collection<PermissionType> permissionTypes, Permissions p) {
		DomainUtil.removePermissions(context, this, permissionTypes, p);
	}

	public void removePermissions(Permissions p) {
		DomainUtil.removePermissions(context, this, p);
	}

	public void removeSubscriptionPackage(SubscriptionRate sp) {
		sp.delete();
	}

	public void setAutoSubscribe(boolean autoSubscribe) {
		this.autoSubscribe = autoSubscribe;
	}

	public final void setComments(String comments) {
		this.comments = comments;
	}

	public final void setDailyPmLimit(int dailyPmLimit) {
		this.dailyPmLimit = dailyPmLimit;
	}

	public final void setDailyProfileViewsLimit(int dailyProfileViews) {
		this.dailyProfileViews = dailyProfileViews;
	}

	public void setFeatureValues(Map<SubscriptionFeature, String> featureValues) {

		removeFeatureValues();

		for (SubscriptionFeature feature : featureValues.keySet()) {
			new FeatureValue(context, feature, this, featureValues.get(feature));
		}
	}

	public final void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public final void setIcon(String icon) {
		this.icon = icon;
	}

	public final void setListMarkup(Markup listMarkup) {
		this.listMarkup = listMarkup;
	}

	public final void setLive(boolean active) {
		this.live = active;
	}

	public final void setName(String name) {
		this.name = name;
	}

	public void setPermission(PermissionType permission, Permissions obj) {
		new Permission(context, this, permission, obj);
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setPrioritise(boolean prioritise) {
		this.prioritise = prioritise;
	}

	public final void setSubscriptionReminderBody(String subscriptionReminderBody) {
		this.subscriptionReminderBody = subscriptionReminderBody;
	}

	public final void setSubscriptionReminderDays(Collection<Integer> c) {
		this.subscriptionReminderDays = new TreeSet();
		if (c != null) {
			this.subscriptionReminderDays.addAll(c);
		}
	}
}
