package org.sevensoft.ecreator.model.accounts;

import java.util.List;

import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 22 Dec 2006 07:10:17
 *
 */
@Table("items_accounts_module")
public class AccountModule extends EntityObject {

	/**
	 * Override the markup for account pages
	 */
	private Markup	accountMarkup;

	private ItemType	itemType;

	/**
	 * Do not display account control panel
	 */
	private boolean	hideAccount;

	/**
	 * Show last access time in admin
	 */
	private boolean	showLastAccessTime;

	/**
	 * Inactive message when users are disabled. Can be override in account
	 */
	private String	inactiveMessage;

	/**
	 * Include the account id in display name
	 */
	private boolean	showAccountId;

	private String	profileEditHeader, profileEditFooter;

	private String	profileImagesHeader, profileImagesFooter;

	/**
	 * Reg exp determining valid emails for registration and login
	 */
	private String	emailRegExp;

	private String	emailErrorMessage;

	private boolean	overridePaymentTypes;

	/**
	 * Header and footer for the account page
	 */
	private String	accountHeader, accountFooter;

	private String	restrictionContent;

	private String	loginForwardUrl;

	private String	logoutForwardUrl;

    private boolean notChangeEmailAfterRegistered;

	protected AccountModule(RequestContext context) {
		super(context);
	}

	public AccountModule(RequestContext context, ItemType itemType) {
		super(context);
		this.itemType = itemType;
		save();
	}

	public final String getAccountFooter() {
		return accountFooter;
	}

	public final String getAccountHeader() {
		return accountHeader;
	}

	public Markup getAccountMarkup() {
		return (Markup) (accountMarkup == null ? null : accountMarkup.pop());
	}

	public String getEmailErrorMessage() {
		return emailErrorMessage;
	}

	public String getEmailRegExp() {
		return emailRegExp;
	}

	public String getInactiveMessage() {
		return inactiveMessage;
	}

	public final ItemType getItemType() {
		return itemType.pop();
	}

	public String getLoginForwardUrl() {
		return loginForwardUrl;
	}

	public String getLogoutForwardUrl() {
		return logoutForwardUrl;
	}

	private List<PaymentType> getPaymentTypes() {
		return null;
	}

	public final String getProfileEditFooter() {
		return profileEditFooter;
	}

	public final String getProfileEditHeader() {
		return profileEditHeader;
	}

	public final String getProfileImagesFooter() {
		return profileImagesFooter;
	}

	public final String getProfileImagesHeader() {
		return profileImagesHeader;
	}

	public String getRestrictionContent() {
		return restrictionContent;
	}

	public boolean hasAccountFooter() {
		return accountFooter != null;
	}

	public boolean hasAccountHeader() {
		return accountHeader != null;
	}

	public boolean hasEmailErrorMessage() {
		return emailErrorMessage != null;
	}

	public boolean hasLogoutForwardUrl() {
		return logoutForwardUrl != null;
	}

    public boolean hasPaymentType(PaymentType paymentType) {
        return getPaymentTypes() == null ? false : getPaymentTypes().contains(paymentType);
    }

	public boolean hasProfileEditFooter() {
		return profileEditFooter != null;
	}

	public boolean hasProfileEditHeader() {
		return profileEditHeader != null;
	}

	public boolean hasProfileImagesFooter() {
		return profileImagesFooter != null;
	}

	public boolean hasProfileImagesHeader() {
		return profileImagesHeader != null;
	}

	public boolean isHideAccount() {
		return hideAccount;
	}

    public boolean isNotChangeEmailAfterRegistered() {
        return notChangeEmailAfterRegistered;
    }

	public boolean isOverridePaymentTypes() {
		return overridePaymentTypes;
	}

	public boolean isShowAccount() {
		return !isHideAccount();
	}

	public boolean isShowAccountId() {
		return showAccountId;
	}

	public final boolean isShowLastAccessTime() {
		return showLastAccessTime;
	}

	public boolean isValidEmail(String email) {

		if (email == null) {
			return false;
		}

		if (emailRegExp == null) {
			return true;
		}

		return email.matches(emailRegExp);
	}

	public final void setAccountFooter(String accountFooter) {
		this.accountFooter = accountFooter;
	}

	public final void setAccountHeader(String accountHeader) {
		this.accountHeader = accountHeader;
	}

	public final void setAccountMarkup(Markup accountMarkup) {
		this.accountMarkup = accountMarkup;
	}

	public final void setEmailErrorMessage(String emailErrorMessage) {
		this.emailErrorMessage = emailErrorMessage;
	}

	public final void setEmailRegExp(String emailRegExp) {
		this.emailRegExp = emailRegExp;
	}

	public final void setHideAccount(boolean hideAccount) {
		this.hideAccount = hideAccount;
	}

	public final void setInactiveMessage(String inactiveMessage) {
		this.inactiveMessage = inactiveMessage;
	}

	public void setLoginForwardUrl(String loginForwardUrl) {
		this.loginForwardUrl = loginForwardUrl;
	}

	public void setLogoutForwardUrl(String logoutForwardUrl) {
		this.logoutForwardUrl = logoutForwardUrl;
	}

    public void setNotChangeEmailAfterRegistered(boolean notChangeEmailAfterRegistered) {
        this.notChangeEmailAfterRegistered = notChangeEmailAfterRegistered;
    }

	public final void setOverridePaymentTypes(boolean overridePaymentTypes) {
		this.overridePaymentTypes = overridePaymentTypes;
	}

	public final void setProfileEditFooter(String profileEditFooter) {
		this.profileEditFooter = profileEditFooter;
	}

	public final void setProfileEditHeader(String profileEditHeader) {
		this.profileEditHeader = profileEditHeader;
	}

	public final void setProfileImagesFooter(String profileImagesFooter) {
		this.profileImagesFooter = profileImagesFooter;
	}

	public final void setProfileImagesHeader(String profileImagesHeader) {
		this.profileImagesHeader = profileImagesHeader;
	}

	public final void setRestrictionContent(String restrictionContent) {
		this.restrictionContent = restrictionContent;
	}

	public void setShowAccount(boolean s) {
		this.hideAccount = !s;
	}

	public final void setShowAccountId(boolean showId) {
		this.showAccountId = showId;
	}

	public final void setShowLastAccessTime(boolean showLastAccessTime) {
		this.showLastAccessTime = showLastAccessTime;
	}
}
