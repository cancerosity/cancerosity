package org.sevensoft.ecreator.model.accounts.subscriptions.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.accounts.subscriptions.Subscription;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 11 Dec 2006 21:17:40
 * 
 *
 */
public class SubscriptionDaysMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		Item account = (Item) context.getAttribute("account");
		if (account == null)
			return null;

		Subscription sub = account.getSubscription();

		if (!sub.hasSubscriptionLevel()) {
			return null;
		}

		String days = String.valueOf(sub.getSubscriptionRemainingDays());

		return super.string(context, params, days);
	}

	@Override
	public String getDescription() {
		return "Shows the remaining number of days on this subscription";
	}

	public Object getRegex() {
		return "subscription_days";
	}
}
