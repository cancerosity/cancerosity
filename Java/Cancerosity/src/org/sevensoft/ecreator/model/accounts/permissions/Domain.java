package org.sevensoft.ecreator.model.accounts.permissions;

import java.util.Collection;
import java.util.List;

/**
 * This interface represents an object that can own permissions,
 *  ie, account type or subscription or special guest domain
 * 
 */
public interface Domain {

	/**
	 * Max number of private messages that this domain can send per day
	 */
	public int getDailyPmLimit();

	/**
	 * Max number of profiles that can be viewed by this domain per day
	 */
	public int getDailyProfileViewsLimit();

	/**
	 * 
	 */
	public String getDomainName();

	/**
	 * 
	 */
	public String getFullId();

	/**
	 * Returns a list of all permissions applicable to this domain that do not use permissible objects
	 * 
	 */
	public List<Permission> getPermissions();

	/**
	 * Returns all permissions applicable to this domain in relation to the permissible
	 * 
	 */
	public List<Permission> getPermissions(Permissions p);

	/**
	 * Returns true if this domain has the permission of type for permissible obj
	 */
	public boolean hasPermission(PermissionType type, Permissions p);

	/**
	 * 
	 */
	public void removePermissions(Collection<PermissionType> permissionTypes, Permissions p);

	/**
	 * Remove all permissions for this domain for this permissible obj
	 */
	public void removePermissions(Permissions p);

	/**
	 * 
	 */
	public void save();

	public void setDailyPmLimit(int i);

	public void setDailyProfileViewsLimit(int i);

	/**
	 * Sets the permission on this domain for type type for permissible obj
	 */
	public void setPermission(PermissionType type, Permissions p);
}
