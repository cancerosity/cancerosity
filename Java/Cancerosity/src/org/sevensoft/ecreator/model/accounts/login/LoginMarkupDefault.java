package org.sevensoft.ecreator.model.accounts.login;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;

/**
 * @author sks 1 Jun 2006 12:47:20
 *
 */
public class LoginMarkupDefault implements MarkupDefault {

	public String getBetween() {
		return null;
	}

	public String getCss() {
		return null;
	}

	public String getBody() {

		StringBuilder sb = new StringBuilder();

		sb.append("<style>\n");
		sb.append("table.login { font-family: Arial; width: 100%; border: 1px solid #e1e1e1; border-top: 0; font-size: 12px; color: #333333; }\n");
		sb
				.append("table.login caption { margin-top: 10px; font-size: 16px; background: #646464; color: white; padding: 4px 8px; font-weight: bold; text-align:left; }\n");
		sb.append("table.login th { text-align: left; background: #e0e0e0; color: #343434; padding: 5px; border-bottom: 1px solid #b0b0b0; }\n");
		sb.append("table.login td { padding: 5px; }\n");
		sb.append("table.login td.key, table.reg_form td.command { background: #f9f9f9; }\n");
		sb.append("table.login .error { color: #cc0000; font-weight: bold; }\n");
		sb.append("table.login a { text-decoration: underline; }\n");
		sb.append("table.login a:hover { text-decoration: none; color: #666666; }\n");
		sb.append("</style>\n");

		sb.append("<table class=\"login_markup\" align=\"center\" cellpadding=\"0\" cellspacing=\"5\">\n");
		sb.append("<tr><td valign=\"top\" width=\"50%\">\n");

		sb.append("	<table class=\"login\" id=\"ec_login_existing\" cellpadding=\"0\" cellspacing=\"0\">\n");
		sb.append("	<caption>Already a member?</caption>\n");
		sb.append("		<tr><td>	If you are already a member then \n");
		sb.append("					enter your email and password\n");
		sb.append("					in the fields below and press the \n");
		sb.append("					login button.<br/><br/>\n");

		sb.append("					<b>Your email:</b><br/>\n");
		sb.append("					[login_email?size=20]<br/><br/>\n");

		sb.append("					<b>Password:</b><br/>\n");
		sb.append("					[login_password?size=20]<br/><br/>\n");

		sb.append("					[submit?label=Login]<br/>\n");
		sb.append("					[forgotten_password]\n");
		sb.append("		</td></tr>\n");
		sb.append("	</table>\n");

		sb.append("</td><td valign=\"top\" width=\"50%\">\n");

		sb.append("	<table class=\"login\" id=\"ec_login_new\" cellpadding=\"0\" cellspacing=\"0\">\n");
		sb.append("	<caption>Want to join?</caption>\n");
		sb.append("		<tr><td>	If you are not a member on our site \n");
		sb.append("					then you can create an account instantly!<br/><br/>\n");
		sb.append("					Just click register to begin.<br/><br/>\n");

		sb.append("					[register?label=Register]\n");
		sb.append("		</td></tr>\n");
		sb.append("	</table>\n");

		sb.append("</td></tr>\n");
		sb.append("</table>\n");

		return sb.toString();
	}

	public String getEnd() {
		return null;
	}

	public String getName() {
		return null;
	}

	public String getStart() {
		return null;
	}

	public int getTds() {
		return 0;
	}

}
