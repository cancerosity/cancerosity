package org.sevensoft.ecreator.model.accounts.registration.emails;

import java.util.Collection;
import java.util.Collections;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.ecreator.model.accounts.registration.EmailTagParser;
import org.sevensoft.ecreator.model.accounts.registration.RegistrationSettings;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 01-Feb-2006 11:08:30
 *
 */
public class RegistrationEmail extends Email {

	private RegistrationSettings	registrationSettings;

	public RegistrationEmail(RequestContext context, Item account, String body, Collection<String> emails, String password) {

		this.registrationSettings = RegistrationSettings.getInstance(context);
		Company company = Company.getInstance(context);
		Config config = Config.getInstance(context);

		String serverEmail = registrationSettings.getServerEmailAddress();
		if (serverEmail == null) {
			serverEmail = config.getServerEmail();
		}

		setFrom(company.getName(), serverEmail);
		setSubject("Your registration");
		setRecipients(emails);
		setBody(EmailTagParser.parse(context, account, body, password));
	}

	public RegistrationEmail(RequestContext context, Item account, String body, String to, String password) {
		this(context, account, body, Collections.singletonList(to), password);
	}
}
