package org.sevensoft.ecreator.model.accounts.registration.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.account.registration.RegistrationHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 03-Apr-2006 11:02:07
 *
 */
public class RegisterMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		// ensure registration is on
		if (ItemType.getRegistration(context).isEmpty()) {
			logger.fine("[RegisterMarker] no registration types");
			return null;
		}

		Link link = new Link(RegistrationHandler.class);

		String linkback = (String) context.getAttribute("linkback");
		if (linkback != null) {
			link.setParameter("linkback", linkback);
		}

		return super.link(context, params, link, null);
	}

	public Object getRegex() {
		return "register";
	}
}