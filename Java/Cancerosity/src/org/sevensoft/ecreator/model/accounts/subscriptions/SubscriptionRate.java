package org.sevensoft.ecreator.model.accounts.subscriptions;

import org.sevensoft.commons.samdate.formatters.DateFormatter;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.ecom.orders.VatHelper;
import org.sevensoft.ecreator.model.extras.busclub.BusClubSettings;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 18 Aug 2006 17:50:23
 *
 * A subscription package is the actual cost of a subscription and how long it lasts for
 */
@Table("subscriptions_rates")
public class SubscriptionRate extends EntityObject implements Selectable, Positionable {

	/**
	 * 
	 */
	private int				period;
	/**
	 * 
	 */
	private Money			fee;
	/**
	 * 
	 */
	private SubscriptionLevel	subscriptionLevel;

	/**
	 *  If introductory is flagged then this package can only be choosen the first time a user ever subscribes.
	 */
	private boolean			introductory;
	/**
	 * 
	 */
	private int				position;

	private double			vatRate;

	private SubscriptionRate(RequestContext context) {
		super(context);
	}

	public SubscriptionRate(RequestContext context, SubscriptionLevel subscription) {

		super(context);
		this.subscriptionLevel = subscription;
		this.period = 0;
		this.fee = new Money(0);

		save();
	}

	public String getDescription() {

		if (isFree() && isLifetime()) {
			return "Free subscription";
		}

		if (isFree()) {
			return "Free for " + getPeriodDescription();
		}

		if (period == 30) {
			return getFeeInc() + " per month";
		}

		return getFeeInc() + " " + getPeriodDescription();
	}

	public Money getFee() {

		// check for business club affiliation discount
		if (context.getRequest().getSession().getAttribute(FrontendHandler.AFFILIATE_ID_KEY) != null) {

			BusClubSettings settings = BusClubSettings.getInstance(context);
			Money discount = settings.getAffiliationDiscount();
			return fee.minus(discount);

		} else {

			return fee;
		}

	}

	public String getFeeDescription() {
		return hasFee() ? "Free" : getFee().toString();
	}

	public Money getFeeInc() {
		return VatHelper.inc(context, getFee(), vatRate);
	}

	public String getLabel() {
		return getDescription();
	}

	public int getPeriod() {
		return period;
	}

	private String getPeriodDescription() {

		if (period == 0) {
			return "lifetime subscription";
		}

		return DateFormatter.daysToString(period);
	}

	public int getPosition() {
		return position;
	}

	public SubscriptionLevel getSubscriptionLevel() {
		return subscriptionLevel.pop();
	}

	public String getValue() {
		return getIdString();
	}

	public double getVatRate() {
		return vatRate;
	}

	public boolean hasFee() {
		return fee.isPositive();
	}

	public boolean isFree() {
		return fee.isZero();
	}

	public final boolean isIntroductory() {
		return introductory;
	}

	public boolean isLifetime() {
		return period == 0;
	}

	public void setFee(Money fee) {
		this.fee = fee;
	}

	public final void setIntroductory(boolean introductory) {
		this.introductory = introductory;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setVatRate(double vatRate) {
		this.vatRate = vatRate;
	}

}
