package org.sevensoft.ecreator.model.accounts.subscriptions;

import java.util.List;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 28 Dec 2006 17:01:43
 *
 */
@Table("subscriptions_features")
public class SubscriptionFeature extends EntityObject implements Positionable {

	public static List<SubscriptionFeature> get(RequestContext context) {
		return SimpleQuery.execute(context, SubscriptionFeature.class);
	}

	private String	name;
	private int		position;

	protected SubscriptionFeature(RequestContext context) {
		super(context);
	}

	public SubscriptionFeature(RequestContext context, String string) {
		super(context);
		name = string;

		save();
	}

	public final String getName() {
		return name;
	}

	public int getPosition() {
		return position;
	}

	public final void setName(String name) {
		this.name = name;
	}

	public void setPosition(int position) {
		this.position = position;
	}

}
