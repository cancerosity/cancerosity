package org.sevensoft.ecreator.model.accounts.login.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 03-Apr-2006 11:15:54
 *
 */
public class LogoutMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		// only show logout if we are logged in
		if (!context.containsAttribute("account")) {
			return null;
		}

		return super.link(context, params, new Link(LoginHandler.class, "logout"), "logout_link");
	}

	public Object getRegex() {
		return "logout";
	}

}
