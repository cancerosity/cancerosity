package org.sevensoft.ecreator.model.accounts.permissions.panels;

import java.util.List;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.accounts.permissions.Domain;
import org.sevensoft.ecreator.model.accounts.permissions.DomainUtil;
import org.sevensoft.ecreator.model.accounts.permissions.Permission;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.accounts.permissions.Permissions;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionsModule;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 27 Dec 2006 18:43:35
 * 
 * Render permissions for this permissible
 *
 */
public class PermissionsPanel {

	private List<PermissionType>				types;
	private RequestContext					context;
	private Permissions					p;
	private List<Domain>					domains;
	private MultiValueMap<Domain, PermissionType>	permissions;

	public PermissionsPanel(RequestContext context, Permissions p, List<PermissionType> types) {
		this.context = context;
		this.p = p;
		this.types = types;

		this.domains = DomainUtil.get(context);

		this.permissions = new MultiValueMap();
		for (Domain domain : domains) {
			for (Permission permission : domain.getPermissions(p)) {
				permissions.put(domain, permission.getPermissionType());
			}
		}
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(new AdminTable("Permissions"));

		if (p != null) {

			int span = types.size();
			sb.append(new AdminRow("Enable permissions", "Enable access permissions on this", new BooleanRadioTag(context, "enablePermissions", p
					.isPermissions())).setSpan(span));

			if (p.isPermissions()) {

				sb.append(new AdminRow("Restriction forward url", "If we do not have access then use this forward.", new TextTag(context,
						"restrictionForwardUrl", p.getRestrictionForwardUrl(), 40)).setSpan(span));

				if (p instanceof Category) {

					PermissionsModule module = ((Category) p).getPermissionsModule();

					sb.append(new AdminRow("No guest login", "If the user is not logged in, then do not show the login page, instead use forward.",
							new BooleanRadioTag(context, "noGuestLogin", module.isNoGuestLogin())).setSpan(span));

					sb.append(new AdminRow("Forward on restriction", "If we do not have access then use this forward.", new TextTag(context,
							"restrictionForward", module.getRestrictionForward(), 40)).setSpan(span));

                    sb.append(new AdminRow("Accept referrer", "Allow only traffic that has come from a specific referrer hostname.", new TextTag(context,
							"allowOnlyReferrer", module.getAllowOnlyReferrer(), 40)).setSpan(span));

                    SelectTag accountAllowed = new SelectTag(context, "accountAllowed", module.getAccountAllowed());
                    accountAllowed.addOptions(Item.getAccountsForOptions(context));
                    accountAllowed.setAny("--Choose--");
                    sb.append(new AdminRow("Account allowed", "Allow only traffic for specified account", accountAllowed));
                }
			}
		}

		if (p == null || p.isPermissions()) {

			sb.append("<tr>");
			sb.append("<td></td>");
			for (PermissionType type : types) {
				sb.append("<td><b>" + type.toString(context) + "</b></td>");
			}
			sb.append("</tr>");

			for (Domain domain : domains) {

				sb.append("<tr>");
				sb.append("<td><b>" + domain.getDomainName() + "</b></td>");

				for (PermissionType type : types) {
					sb
							.append("<td>" + new CheckTag(context, "permissions_" + domain.getFullId(), type, permissions.contains(domain, type))
									+ "</td>");
				}
				sb.append("</tr>");
			}
		}

		sb.append("</table>");

		return sb.toString();
	}
}
