package org.sevensoft.ecreator.model.accounts.permissions;

/**
 * This interface represents an object that can has the permission applied to it. 
 * Eg this is like the file on a write permission, whereas a domain is like the user.
 * 
 * @author sks 1 Jun 2006 01:00:08
 *
 */
public interface Permissions {

	public String getFullId();

	/**
	 *  Where to send the user when the permission fails
	 */
	public String getRestrictionForwardUrl();

	/**
	 * Is permissions enabled for this object ?
	 */
	public boolean isPermissions();

	public void save();

	public void setPermissions(boolean p);

	public void setRestrictionForwardUrl(String restrictionForwardUrl);

}
