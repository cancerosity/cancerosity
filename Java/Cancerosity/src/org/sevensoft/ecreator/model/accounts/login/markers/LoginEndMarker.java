package org.sevensoft.ecreator.model.accounts.login.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 17 Sep 2006 07:37:54
 *
 */
public class LoginEndMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {
		return "</form>";
	}

	public Object getRegex() {
		return "login_end";
	}

	@Override
	public String getDescription() {
		return "This tag closes the login form";
	}

}
