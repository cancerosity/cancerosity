package org.sevensoft.ecreator.model.accounts.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 9 Jan 2007 10:20:19
 *
 */
public class AccountEmailMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		Item account = (Item) context.getAttribute("account");
		if (account == null) {
			return null;
		}

		if (!account.hasEmail()) {
			return null;
		}

		return super.string(context, params, account.getEmail());
	}

	public Object getRegex() {
		return new String[] { "account_email" };
	}

}
