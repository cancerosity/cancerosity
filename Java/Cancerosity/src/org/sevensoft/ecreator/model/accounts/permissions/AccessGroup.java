package org.sevensoft.ecreator.model.accounts.permissions;

import java.util.Collection;
import java.util.List;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 22 Dec 2006 12:19:45
 *
 */
@Table("permissions_accessgroups")
public class AccessGroup extends EntityObject implements Domain, Selectable {

	/**
	 * Returns all non guest access groups
	 */
	public static List<AccessGroup> get(RequestContext context) {
		return SimpleQuery.execute(context, AccessGroup.class, "guest", false);
	}

	/**
	 * returns all access groups including guest
	 */
	public static List<AccessGroup> getAll(RequestContext context) {
		return SimpleQuery.execute(context, AccessGroup.class);
	}

	/**
	 * 
	 */
	public static AccessGroup getGuest(RequestContext context) {
		return SimpleQuery.get(context, AccessGroup.class, "guest", true);
	}

	/**
	 * 
	 */
	private String	name;

	/**
	 * If a restriction forward set then send there rather than being asked to login.
	 * This is general across the site
	 */
	private String	restrictionForward;

	/**
	 * The special guest access group
	 */
	private boolean	guest;

	private int		dailyPmLimit;

	private int		dailyProfileViewsLimit;

	protected AccessGroup(RequestContext context) {
		super(context);
	}

	public AccessGroup(RequestContext context, String string) {
		super(context);
		name = string;

		save();
	}

	@Override
	public synchronized boolean delete() {
		SimpleQuery.delete(context, AccessGroupItem.class, "accessGroup", this);
		return super.delete();
	}

	public int getDailyPmLimit() {
		return dailyPmLimit;
	}

	public int getDailyProfileViewsLimit() {
		return dailyProfileViewsLimit;
	}

	public String getDomainName() {
		return getName();
	}

	public String getLabel() {
		return getName();
	}

	public final String getName() {
		return name;
	}

	public List<Permission> getPermissions() {
		return DomainUtil.getPermissions(context, this, null);
	}

	public List<Permission> getPermissions(Permissions p) {
		return DomainUtil.getPermissions(context, this, p);
	}

	public final String getRestrictionForward() {
		return restrictionForward;
	}

	public String getValue() {
		return getIdString();
	}

	public boolean hasPermission(PermissionType type, Permissions p) {
		return DomainUtil.hasPermission(context, this, type, p);
	}

	public final boolean isGuest() {
		return guest;
	}

	public void removePermissions(Collection<PermissionType> permissionTypes, Permissions p) {
		DomainUtil.removePermissions(context, this, permissionTypes, p);
	}

	public void removePermissions(Permissions p) {
		DomainUtil.removePermissions(context, this, p);
	}

	@Override
	protected void schemaInit(RequestContext context) {
		super.schemaInit(context);

		// ensure we have the guest group
		if (SimpleQuery.count(context, AccessGroup.class, "guest", true) == 0) {
			AccessGroup ag = new AccessGroup(context, "Guests");
			ag.guest = true;
			ag.save();
		}
	}

	public final void setDailyPmLimit(int dailyPmLimit) {
		this.dailyPmLimit = dailyPmLimit;
	}

	public final void setDailyProfileViewsLimit(int dailyProfileViewsLimit) {
		this.dailyProfileViewsLimit = dailyProfileViewsLimit;
	}

	public final void setName(String name) {
		this.name = name;
	}

	public void setPermission(PermissionType type, Permissions p) {
		new Permission(context, this, type, p);
	}

	public final void setRestrictionForward(String restrictionForward) {
		this.restrictionForward = restrictionForward;
	}

}
