package org.sevensoft.ecreator.model.accounts.login;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author sks 14 Dec 2006 15:46:36
 *
 */
public class PasswordHelper {

	private static MessageDigest	md;

	public static boolean isPlainTextMatch(String plain, String hash) {
		return generatePasswordHash(plain).equals(hash);
	}

	static {

		try {

			md = MessageDigest.getInstance("MD5");

		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	public synchronized static String generatePasswordHash(String plainTextPassword) {

		try {

			md.reset();
			byte[] hash = md.digest(plainTextPassword.getBytes("CP1252"));

			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < hash.length; ++i) {
				sb.append(Integer.toHexString((hash[i] & 0xFF) | 0x100).toUpperCase().substring(1, 3));
			}
			return sb.toString();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);

		}
	}
}
