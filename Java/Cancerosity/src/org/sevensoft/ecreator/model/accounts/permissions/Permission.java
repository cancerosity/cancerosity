package org.sevensoft.ecreator.model.accounts.permissions;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 30 May 2006 15:17:45
 *
 */
@Table("permissions")
public class Permission extends EntityObject {

	/**
	 * The permission type we are assigning this permission for
	 */
	private PermissionType	permissionType;

	private String		permissible;

	private String		domain;

	protected Permission(RequestContext context) {
		super(context);
	}

	public final PermissionType getPermissionType() {
		return permissionType;
	}

	public Permission(RequestContext context, Domain domain, PermissionType type, Permissions p) {
		super(context);

		if (!domain.hasPermission(type, p)) {

			this.domain = domain.getFullId();
			this.permissionType = type;

			if (p != null) {
				this.permissible = p.getFullId();
			}

			save();
		}
	}

}
