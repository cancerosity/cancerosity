package org.sevensoft.ecreator.model.accounts.subscriptions.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionCancellationHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 11 Dec 2006 21:17:40
 * 
 * A link to the subscription cancellation page
 *
 */
public class SubscriptionCancelMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		// must be logged in
		Item active = (Item) context.getAttribute("account");
		if (active == null) {
			return null;
		}

		// must have subs enabled
		if (!Module.Subscriptions.enabled(context))
			return null;

		return super.link(context, params, new Link(SubscriptionCancellationHandler.class), "account_link");
	}

	@Override
	public String getDescription() {
		return "A link to the subscription cancellation page";
	}

	public Object getRegex() {
		return "subscription_cancel";
	}
}
