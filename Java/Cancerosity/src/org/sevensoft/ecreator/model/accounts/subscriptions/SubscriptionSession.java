package org.sevensoft.ecreator.model.accounts.subscriptions;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionHandler;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.forms.able2buy.CreditGroup;
import org.sevensoft.ecreator.model.ecom.vouchers.Voucher;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 10 Aug 2006 14:08:29
 *
 */
@Table("subscriptions_sessions")
public class SubscriptionSession extends EntityObject implements Payable {

	public static void delete(RequestContext context, String sessionId) {
		SimpleQuery.delete(context, SubscriptionSession.class, "sessionid", sessionId);
	}

	public static SubscriptionSession getBySession(RequestContext context, String sessionId) {

		Query q = new Query(context, "select * from  # where sessionId=?");
		q.setTable(SubscriptionSession.class);
		q.setParameter(sessionId);
		SubscriptionSession s = q.get(SubscriptionSession.class);
		if (s == null)
			s = new SubscriptionSession(context, sessionId);
		return s;
	}

	private String			sessionId;

	/**
	 * Subscription choosen by user
	 */
	private SubscriptionLevel	subscriptionLevel;

	private SubscriptionRate	subscriptionRate;

	private PaymentType		paymentType;

	private long			timestamp;

	private Item			account;

	private Voucher			voucher;

	private boolean			voucherOffered;

	private Card			card;

	private SubscriptionSession(RequestContext context) {
		super(context);
	}

	public SubscriptionSession(RequestContext context, String sessionId) {
		super(context);
		this.sessionId = sessionId;
		save();
	}

	public CreditGroup getAble2BuyCreditGroup() {
		return null;
	}

	public Item getAccount() {
		return (Item) (account == null ? null : account.pop());
	}

	public Card getCard() {
		return (Card) (card == null ? null : card.pop());
	}

	public Item getPayableAccount() {
		return getAccount();
	}

	public Address getPaymentAddress() {
		return null;
	}

	public Money getPaymentAmount() {
		return getSubscriptionRate().getFeeInc();
	}

	public String getPaymentDescription() {
		return "Subscription: " + getSubscriptionLevel().getName() + " " + getSubscriptionRate().getDescription();
	}

	public String getPaymentFailureUrl(PaymentType paymentType) {
		return Config.getInstance(context).getUrl() + "/" + new Link(SubscriptionHandler.class).toString();
	}

	public String getPaymentSuccessUrl(PaymentType paymentType) {
		return Config.getInstance(context).getUrl() + "/" + new Link(SubscriptionHandler.class, "completed", "paymentType", paymentType).toString();
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

    public String getBasketUrl() {
        return null;
    }

    public String getSessionId() {
		return sessionId;
	}

	public SubscriptionLevel getSubscriptionLevel() {
		return (SubscriptionLevel) (subscriptionLevel == null ? null : subscriptionLevel.pop());
	}

	public SubscriptionRate getSubscriptionRate() {
		return (SubscriptionRate) (subscriptionRate == null ? null : subscriptionRate.pop());
	}

	public final Voucher getVoucher() {
		return (Voucher) (voucher == null ? null : voucher.pop());
	}

	public boolean hasPaymentType() {
		return paymentType != null;
	}

	public boolean hasSubscriptionLevel() {
		return subscriptionLevel != null;
	}

	public boolean hasSubscriptionRate() {
		return subscriptionRate != null;
	}

	public final boolean isVoucherOffered() {
		return voucherOffered;
	}

	@Override
	public synchronized void save() {
		timestamp = System.currentTimeMillis();
		super.save();
	}

	public void setAccount(Item acc) {

		if (ObjectUtil.equal(this.account, acc)) {
			return;
		}

		this.account = acc;
		save("account");
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public void setSubscriptionLevel(SubscriptionLevel s) {
		this.subscriptionLevel = s;
	}

	public void setSubscriptionRate(SubscriptionRate subscriptionRate) {
		this.subscriptionRate = subscriptionRate;
	}

	public final void setVoucher(Voucher voucher) {
		this.voucher = voucher;
	}

	public void setVoucherOffered(boolean b) {
		voucherOffered = b;
	}

	public void subcribe(Payment payment, String ipAddress) {

		getAccount().getSubscription().subscribe(getSubscriptionRate(), payment, ipAddress);
	}

}
