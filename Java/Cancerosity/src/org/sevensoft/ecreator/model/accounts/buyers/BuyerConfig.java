package org.sevensoft.ecreator.model.accounts.buyers;

import java.util.List;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 15 Jan 2007 22:08:50
 *
 */
@Table("buyers_config")
public class BuyerConfig extends EntityObject {

	public static BuyerConfig get(RequestContext context, Item buyer) {
		BuyerConfig c = SimpleQuery.get(context, BuyerConfig.class, "buyer", buyer);
		return c == null ? new BuyerConfig(context, buyer) : c;
	}

	/**
	 * The buyer account these details are for 
	 */
	private Item	buyer;

	private Money	maxMonthlySpend;

	/**
	 * Allow this buyer to add / delete addresses
	 */
	private boolean	editAddresses;

	protected BuyerConfig(RequestContext context) {
		super(context);
	}

	public BuyerConfig(RequestContext context, Item buyer) {
		super(context);

		this.buyer = buyer;
		save();
	}

	public void addAddress(Address address) {
		new BuyerAddress(context, buyer, address);
	}

	public final Item getBuyer() {
		return buyer.pop();
	}

	public List<Item> getBuyers() {
		Query q = new Query(context, "select * from # where status!=? and buyerFor=?");
		q.setTable(Item.class);
		q.setParameter("Deleted");
		q.setParameter(buyer);
		return q.execute(Item.class);
	}

	public List<Item> getBuyersAll() {
		return SimpleQuery.execute(context, Item.class, "buyerFor", buyer);
	}

	public final Money getMaxMonthlySpend() {
		return maxMonthlySpend;
	}

	public final boolean isEditAddresses() {
		return editAddresses;
	}

	public void removeAddresses() {
		SimpleQuery.delete(context, BuyerAddress.class, "buyer", buyer);
	}

	public final void setEditAddresses(boolean editAddresses) {
		this.editAddresses = editAddresses;
	}

	public final void setMaxMonthlySpend(Money maxMonthlySpend) {
		this.maxMonthlySpend = maxMonthlySpend;
	}

}
