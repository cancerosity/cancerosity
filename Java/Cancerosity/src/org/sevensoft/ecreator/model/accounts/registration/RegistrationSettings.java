package org.sevensoft.ecreator.model.accounts.registration;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 30 Jan 2007 17:16:21
 *
 */
@Table("registration_settings")
@Singleton
public class RegistrationSettings extends EntityObject {

	public static RegistrationSettings getInstance(RequestContext context) {
		return EntityObject.getSingleton(context, RegistrationSettings.class);
	}

	/**
	 * Header for initial account selection page
	 */
	private String		itemTypeHeader;

	/**
	 * Footer for initial account selection page
	 */
	private String		itemTypeFooter;

	/**
	 * Referrers used by all account types
	 */
	private List<String>	referrers;

	/**
	 * 
	 */
	private String		detailsFooter;

	/**
	 * 
	 */
	private String		detailsHeader;

	/**
	 * The email address used by the server
	 */
	private String		serverEmailAddress;

	protected RegistrationSettings(RequestContext context) {
		super(context);
	}

	public final String getDetailsFooter() {
		return detailsFooter;
	}

	public final String getDetailsHeader() {
		return detailsHeader;
	}

	public final String getItemTypeFooter() {
		return itemTypeFooter;
	}

	public final String getItemTypeHeader() {
		return itemTypeHeader;
	}

	public final List<String> getReferrers() {
		if (referrers == null) {
			referrers = new ArrayList();
		}
		return referrers;
	}

	public final String getServerEmailAddress() {
		return serverEmailAddress;
	}

	public boolean hasDetailsFooter() {
		return detailsFooter != null;
	}

	public boolean hasDetailsHeader() {
		return detailsHeader != null;
	}

	public boolean hasItemTypeFooter() {
		return itemTypeFooter != null;
	}

	public boolean hasItemTypeHeader() {
		return itemTypeHeader != null;
	}

	/**
	 * 
	 */
	public boolean hasReferrers() {
		return getReferrers().size() > 0;
	}

	public boolean hasServerEmailAddress() {
		return serverEmailAddress != null;
	}

	public final void setDetailsFooter(String detailsFooter) {
		this.detailsFooter = detailsFooter;
	}

	public final void setDetailsHeader(String detailsHeader) {
		this.detailsHeader = detailsHeader;
	}

	public final void setItemTypeFooter(String itemTypeFooter) {
		this.itemTypeFooter = itemTypeFooter;
	}

	public final void setItemTypeHeader(String itemTypeHeader) {
		this.itemTypeHeader = itemTypeHeader;
	}

	public final void setReferrers(List<String> referrers) {
		this.referrers = referrers;
	}

	public final void setServerEmailAddress(String serverEmail) {
		this.serverEmailAddress = serverEmail;
	}

}
