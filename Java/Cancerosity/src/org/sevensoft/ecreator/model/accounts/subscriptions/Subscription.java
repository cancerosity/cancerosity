package org.sevensoft.ecreator.model.accounts.subscriptions;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderStatusException;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.messages.SystemMessage;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 22 Dec 2006 11:10:14
 * 
 */
@Table("subscriptions")
public class Subscription extends EntityObject {

	public static Subscription get(RequestContext context, Item account) {
		Subscription s = SimpleQuery.get(context, Subscription.class, "account", account);
		return s == null ? new Subscription(context, account) : s;
	}

	/**
	 * Returns all members with a future expiry date
	 * 
	 */
	public static List<Item> getExpiring(RequestContext context) {

		Query q = new Query(context, "select i.* from # i join # s on i.id=s.account where s.subscriptionExpiryDate>=? and s.cancelSubscription=0");
		q.setTable(Item.class);
		q.setTable(Subscription.class);
		q.setParameter(System.currentTimeMillis());
		return q.execute(Item.class);
	}

	/**
	 * Returns all members with an expired expiry date
	 */
	public static List<Item> getExpired(RequestContext context) {

		Query q = new Query(context, "select i.* from # i join # s on i.id=s.account where s.subscriptionExpiryDate<? and s.cancelSubscription=0");
		q.setTable(Item.class);
		q.setTable(Subscription.class);
		q.setParameter(new Date());
		return q.execute(Item.class);
	}

	private Item			account;

	/**
	 * This is the date when the user's subscription will expire.
	 * If this is null then the user has a lifetime subscription 
	 * (or no subscription at all if the subscription object is null too)
	 */
	private Date			subscriptionExpiryDate;

	/**
	 * Flags this account so that the subscription will be cancelled at the end of the period
	 */
	private boolean			cancelSubscription;

	/**
	 * The current subscription this member is assigned to if any
	 */
	private SubscriptionLevel	subscriptionLevel;

	private boolean			introductorySubscribed;

    private DateTime subscriptionPurchaseDate;

	protected Subscription(RequestContext context) {
		super(context);
	}

	public Subscription(RequestContext context, Item account) {
		super(context);
		this.account = account;

		save();
	}

	/**
	 * Immediately cancels this subscription
	 */
	public void cancelSubscription() {
		this.subscriptionLevel = null;
		this.subscriptionExpiryDate = null;

		save();
	}

	/**
	 * Returns the date this membership will expire.
	 * 
	 */
	public Date getSubscriptionExpiryDate() {
		return subscriptionExpiryDate;
	}

	public final SubscriptionLevel getSubscriptionLevel() {
		return (SubscriptionLevel) (subscriptionLevel == null ? null : subscriptionLevel.pop());
	}

    public DateTime getSubscriptionPurchaseDate() {
        return subscriptionPurchaseDate;
    }

	/**
	 * Returns the number of days before this subscription will expire. 
	 * Useful for methods where we want to tell the member
	 * they have say 3 days left or whatever.
	 * 
	 */
	public int getSubscriptionRemainingDays() {

		if (getSubscriptionExpiryDate() == null)
			return 0;

		return getSubscriptionExpiryDate().getDaysBetween(new Date());
	}

	/**
	 *  Returns true if this member has a subscription that has expired. 
	 */
	public boolean hasSubscriptionExpired() {

		if (getSubscriptionLevel() == null) {
			return false;
		}

		if (getSubscriptionExpiryDate() == null) {
			return false;
		}

		if (getSubscriptionExpiryDate().isPast()) {
			return true;
		}

		return false;
	}

	/**
	 * 
	 */
	public boolean hasSubscriptionExpiryDate() {
		return hasSubscriptionLevel() && getSubscriptionExpiryDate() != null;
	}

	/**
	 * Returns true if this member is assigned to a subscription level
	 */
	public boolean hasSubscriptionLevel() {
		return subscriptionLevel != null;
	}

	public final boolean isCancelSubscription() {
		return cancelSubscription;
	}

	/**
	 * Returns true if this member is a lifetime member of his subscription.
	 * If false then his subscription will expire.
	 * 
	 */
	public boolean isLifetimeSubscription() {
		return getSubscriptionExpiryDate() == null && hasSubscriptionLevel();
	}

	/**
	 * Returns true if this subscription has not expired. 
	 * IE, the subscription has a future expiry date, or it is lifetime
	 * 
	 * Returns false if the subscription has expired or if the member does
	 * not have a subscription at all !
	 * 
	 */
	public boolean isSubscriptionCurrent() {

		if (getSubscriptionLevel() == null) {
			return false;
		}

		if (getSubscriptionExpiryDate() == null) {
			return true;
		}

		if (getSubscriptionExpiryDate().isPast()) {
			return false;
		}

		return true;
	}

	public boolean isSubscriptionExpired() {
		return hasSubscriptionExpired();
	}

	/**
	 * Remove subscription information
	 */
	public void removeSubscription() {

		subscriptionLevel = null;
		subscriptionExpiryDate = null;
		save();
	}

	/**
	 * Send email to member reminding them their membership will expire shortly.
	 * 
	 */
	public void sendSubscriptionReminder() throws EmailAddressException, SmtpServerException {

		if (subscriptionLevel == null) {
			return;
		}

		String body = getSubscriptionLevel().getSubscriptionReminderBody();
		if (body == null) {
			body = getSubscriptionLevel().getSubscriptionReminderBody();
		}

		if (body == null) {
			return;
		}

		//		body = emailTagsParse(body);

		Email msg = new Email();
		msg.setSubject("Your membership will expire soon");
		msg.setFrom(Company.getInstance(context).getName(), Config.getInstance(context).getServerEmail());
		msg.setTo(account.getEmail());
		msg.addBcc("sks@7soft.co.uk");
		msg.setBody(body);

		logger.fine("[Subscription] subscription reminder email: " + msg);

		new EmailDecorator(context, msg).send(Config.getInstance(context).getSmtpHostname());

	}

	/**
	 * Sets this member to be cancelled at end of subscription
	 */
	public final void setCancelSubscription() {

		// if lifetime cancel now
		if (subscriptionExpiryDate == null) {

			this.subscriptionLevel = null;
			this.subscriptionExpiryDate = null;

		} else {

			this.cancelSubscription = true;
		}

		save();
	}

	public final void setLifetimeSubscription() {
		this.subscriptionExpiryDate = null;
	}

	public final void setSubscriptionExpiryDate(Date d) {
		this.subscriptionExpiryDate = d;
	}

	/**
	 * Admin method for setting the subscription.
	 * 
	 * @param s
	 */
	public void setSubscriptionLevel(SubscriptionLevel s) {

		if (ObjectUtil.equal(s, subscriptionLevel)) {
			return;
		}

		if (s == null) {

			removeSubscription();

		} else {

			this.subscriptionLevel = s;
			save();

			// for bus club
			// once a user has subscribed changed prioritised to true 
			if (Config.getInstance(context).isBusClub()) {
				account.pop();
				account.setPrioritised(true);
				account.save();
			}
		}
	}

    public void setSubscriptionPurchaseDate(DateTime subscriptionPurchaseDate) {
       this.subscriptionPurchaseDate = subscriptionPurchaseDate;
    }

	/**
	 * free lifetime subscription to this level
	 */
	public void subscribe(SubscriptionLevel sl) {

		if (sl == null) {
			
			this.subscriptionLevel = null;
			this.subscriptionExpiryDate = null;
			
		} else {

			/*
			 * Check that this subscription level  is applicable for this item type
			 */
			if (!sl.getItemType().equals(account.getItemType())) {
				logger.fine("[Item] subscription item type does not match the item type for this item");
				return;
			}

			this.subscriptionLevel = sl;
			this.subscriptionExpiryDate = null;

			save();

			// for bus club
			// once a user has subscribed changed prioritised to true 
			if (Config.getInstance(context).getUrl().contains("businessclub")) {
				account.pop();
				account.setPrioritised(true);
				account.save();
			}

			new SystemMessage(context, account.getName() + " has subscribed to subscription " + sl.getName() + ".");

		}
	}

	/**
	 * This method subscribes a user to the subscription. 
	 * If they are already subscribed to this subscription then it will extend their subscription time.
	 * 
	 * If the subscription is a paid subscription and a payment is attached then it checks price is right.
	 * 
	 */
	public void subscribe(SubscriptionRate subscriptionRate, Payment payment, String ipAddress) {

		logger.fine("[Item] subscribing member=" + this + ", rate=" + subscriptionRate);

		/*
		 * if payment is not null then ensure it is for the correct amount
		 * If it is null then we are assuming the admin is doing a manual change
		 */
		if (subscriptionRate.hasFee() && payment != null) {
			if (!subscriptionRate.getFee().equals(payment.getAmount())) {

				logger.fine("[Item] subscription fee does not equal payment amount");
				return;
			}
		}

		/*
		 * Check that this subscription rate is applicable for this member group
		 */
		if (!subscriptionRate.getSubscriptionLevel().getItemType().equals(account.getItemType())) {
			logger.fine("[Item] this subscription rate is not applicable for this item");
			return;
		}

		/*
		 * If this member is already in the subscription and has lifetime then just exit.
		 * This shouldn't occur as someone with lifetime subscription should not be prompted to renew.
		 */
		if (subscriptionRate.getSubscriptionLevel().equals(subscriptionLevel) && subscriptionExpiryDate == null) {
			logger.fine("[Item] lifetime member trying to subscribe to same package!!");
			return;
		}

		/* 
		 * if this is an introductory subscription and we have already used an introductory offer before then exit.
		 * A customer cannot be allowed to use an introductory offer twice.
		 * This should not occur but check just in case.
		 */
		if (introductorySubscribed && subscriptionRate.isIntroductory()) {
			logger.fine("[Item] cannot use an introductory rate again");
			return;
		}

		/* 
		 * Set introductory subscription to true so we cannot take advantage of introductory offers in the future
		 */
		introductorySubscribed = true;

		/*
		 * If we are changing to a different subscription package then we should reset the expiry date
		 * This is to stop people changing to a more expensive package but having their expiry date simply extended meaning they
		 * could end up with free time on the better package for nothing.
		 */
		if (!subscriptionRate.getSubscriptionLevel().equals(subscriptionLevel)) {

			this.subscriptionExpiryDate = null;
			this.subscriptionLevel = subscriptionRate.getSubscriptionLevel();
		}

		/*
		 * Update expiry date
		 */
		if (subscriptionRate.isLifetime()) {

			// if lifetime subscription then set expiry to null
			this.subscriptionExpiryDate = null;

		} else {

			// start subscription from tomorrow.
			if (subscriptionExpiryDate == null)
				subscriptionExpiryDate = new Date().addDays(1);

			subscriptionExpiryDate = subscriptionExpiryDate.addDays(subscriptionRate.getPeriod());
		}

		save();

		if (subscriptionLevel.isPrioritise()) {
			account.pop();
			account.setPrioritised(true);
			account.save();
		}

		new SystemMessage(context, account.getName() + " has subscribed to subscription " + getSubscriptionLevel().getName() + ".");

		/*
		 * If payment is not null then try to create an order for invoice purposes.
		 */
		if (payment != null) {

			Order order = account.addOrder(null, ipAddress);
			try {
				order.addSubscription(subscriptionRate);
			} catch (OrderStatusException e) {
				e.printStackTrace();
                logger.warning("Order: " + order.toString() + ", Subscription: " + subscriptionRate.toString());
                logger.warning(e.toString());
			}
			payment.setOrder(order);
			payment.save();
		}
	}
}
