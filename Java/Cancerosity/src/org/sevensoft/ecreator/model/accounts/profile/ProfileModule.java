package org.sevensoft.ecreator.model.accounts.profile;

import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Jan 2007 17:17:58
 *
 */
@Table("profiles_module")
public class ProfileModule extends EntityObject {

	public static ProfileModule get(RequestContext context, ItemType itemType) {
		ProfileModule module = SimpleQuery.get(context, ProfileModule.class, "itemType", itemType);
		return module == null ? new ProfileModule(context, itemType) : module;
	}

	private ItemType	itemType;

	protected ProfileModule(RequestContext context) {
		super(context);
	}

	public ProfileModule(RequestContext context, ItemType itemType) {
		super(context);
		this.itemType = itemType;
		save();
	}

	public final ItemType getItemType() {
		return itemType.pop();
	}

}
