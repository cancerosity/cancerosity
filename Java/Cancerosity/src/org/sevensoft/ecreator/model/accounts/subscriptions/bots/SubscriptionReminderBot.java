package org.sevensoft.ecreator.model.accounts.subscriptions.bots;

import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.ecreator.model.accounts.subscriptions.Subscription;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.messages.SystemMessage;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 29-Jan-2006 22:09:59
 *
 */
public class SubscriptionReminderBot {

	private Logger		logger	= Logger.getLogger("cron");
	private RequestContext	context;

	public SubscriptionReminderBot(RequestContext context) {
		this.context = context;
	}

	public void run() {

		logger.config("Subscription reminder bot");

		int n = 0;

		// get all accounts with an expiry date
		List<Item> accounts = Subscription.getExpiring(context);
		for (Item account : accounts) {

			Subscription sub = account.getSubscription();

			logger.fine("account #" + account.getId() + " expires in " + sub.getSubscriptionRemainingDays() + " days");

			/*
			 * Only send the email if the number of subscription days left is when we want to send them out.
			 */
			if (sub.getSubscriptionLevel().getSubscriptionReminderDays().contains(sub.getSubscriptionRemainingDays()))

				try {

					sub.sendSubscriptionReminder();
					logger.fine("account #" + account.getId() + " subscription reminder sent");
					account.log("Subscription expiry reminder email sent");

					n++;

				} catch (EmailAddressException e) {
					e.printStackTrace();

				} catch (SmtpServerException e) {
					e.printStackTrace();
				}
		}

		if (n > 0) {
			new SystemMessage(context, "Subscription reminder email has been sent to " + n + " members.");
		}

	}
}
