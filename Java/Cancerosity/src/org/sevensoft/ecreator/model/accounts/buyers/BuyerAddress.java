package org.sevensoft.ecreator.model.accounts.buyers;

import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 15 Jan 2007 18:03:38
 * 
 * Approved addresses for use by a buyer
 *
 */
@Table("buyers_addresses")
public class BuyerAddress extends EntityObject {

	private Address	address;
	private Item	buyer;

	protected BuyerAddress(RequestContext context) {
		super(context);
	}

	public BuyerAddress(RequestContext context, Item buyer, Address address) {
		super(context);

		this.address = address;
		this.buyer = buyer;
		save();
	}

}
