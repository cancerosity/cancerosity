package org.sevensoft.ecreator.model.accounts.subscriptions;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 28 Dec 2006 17:02:54
 *
 */
@Table("subscriptions_features_values")
public class FeatureValue extends EntityObject {

	private SubscriptionFeature	feature;
	private String			value;
	private SubscriptionLevel	subscriptionLevel;

	protected FeatureValue(RequestContext context) {
		super(context);
	}

	public FeatureValue(RequestContext context, SubscriptionFeature feature, SubscriptionLevel subscriptionLevel, String value) {
		super(context);

		this.feature = feature;
		this.subscriptionLevel = subscriptionLevel;
		this.value = value;

		save();
	}

	public final SubscriptionFeature getFeature() {
		return feature.pop();
	}

	public final SubscriptionLevel getSubscriptionLevel() {
		return subscriptionLevel.pop();
	}

	public final String getValue() {
		return value;
	}
}
