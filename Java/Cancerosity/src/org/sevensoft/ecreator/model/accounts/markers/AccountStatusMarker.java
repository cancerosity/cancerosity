package org.sevensoft.ecreator.model.accounts.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 9 Jan 2007 10:20:19
 *
 */
public class AccountStatusMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		boolean noguest = context.containsAttribute("noguest");

		Item account = (Item) context.getAttribute("account");
		if (account == null) {

			if (noguest) {
				return null;
			} else {
				return "You are not logged in. " + new LinkTag(LoginHandler.class, null, "Click here") + " to log in.";
			}

		} else {

			return "You are logged in as " + account.getDisplayName() + ". " + new LinkTag(LoginHandler.class, "logout", "Click here")
					+ " to log out.";
		}
	}

	@Override
	public String getDescription() {
		return "Displays if the current user is logged in or out";
	}

	public Object getRegex() {
		return new String[] { "account_status", "member_status" };
	}

}
