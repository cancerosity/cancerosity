package org.sevensoft.ecreator.model.accounts.login.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.account.PasswordHandler;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 1 Jun 2006 13:02:25
 *
 */
public class ForgottenPasswordMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		String label = params.get("label");
		if (label == null)
			label = "Forgotten your password?";

		return new LinkTag(PasswordHandler.class, null, label).toString();
	}

	public Object getRegex() {
		return "forgotten_password";
	}

}
