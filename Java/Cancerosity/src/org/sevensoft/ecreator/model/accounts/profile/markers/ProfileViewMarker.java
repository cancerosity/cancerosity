package org.sevensoft.ecreator.model.accounts.profile.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class ProfileViewMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		Item account = (Item) context.getAttribute("account");
		if (account == null) {
			logger.fine("[ProfileViewMarker] not logged in");
			return null;
		}

		return super.link(context, params, new Link(ItemHandler.class, null, "item", account), "account_link");
	}

	public Object getRegex() {
		return new String[] { "account_profile_view" };
	}
}
