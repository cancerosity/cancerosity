package org.sevensoft.ecreator.model.accounts.subscriptions.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 3 Aug 2006 12:40:31
 *
 */
public class SubscriptionRenewalMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.Subscriptions.enabled(context)) {
			return null;
		}

		Item account = (Item) context.getAttribute("account");
		if (account == null) {
			return null;
		}

		if (!account.hasLiveSubscriptionLevels()) {
			return null;
		}

		return super.link(context, params, new Link(SubscriptionHandler.class), "account_link");
	}

	public Object getRegex() {
		return new String[] { "subscription_renewal", "account_subscription" };
	}

}
