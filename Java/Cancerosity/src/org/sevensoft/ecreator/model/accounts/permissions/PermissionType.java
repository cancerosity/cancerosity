package org.sevensoft.ecreator.model.accounts.permissions;

import java.util.List;

import org.sevensoft.ecreator.model.accounts.subscriptions.Subscription;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.system.config.Captions;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 30 May 2006 15:16:27
 * 
 * This enum contains all the permissions for an account
 *
 */
public enum PermissionType {

	/**
	 * Show the content of a category
	 */
	CategoryContent() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return true;
		}

		@Override
		public String toString() {
			return "Category content";
		}
	},

	CategoryItems() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return true;
		}

		@Override
		public String toString() {
			return "Category items";
		}
	},

	/**
	 * Shows the category links
	 */
	CategoryStub() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return true;
		}

		@Override
		public String toString() {
			return "Category stub";
		}
	},

	/**
	 * On items, item types and categories
	 */
	AttachmentsUpload() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return true;
		}

		@Override
		public String toString() {
			return "Upload attachments";
		}
	},

	/**
	 * On items, item types and categories
	 */
	AttachmentsDownload() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return true;
		}

		@Override
		public String toString() {
			return "Download attachments";
		}
	},

	/**
	 * Make the actual attribute known to the customer, even if the value is hidden
	 */
	AttributeVisible() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return true;
		}

		@Override
		public String toString() {
			return "Attribute visible";
		}
	},

	/**
	 * Show the value of the attribute
	 */
	AttributeValue() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return true;
		}

		@Override
		public String toString() {
			return "Attribute value";
		}
	},

	/**
	 * View an items full details for an item type
	 */
	ItemFull() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return ItemType.hasItemTypes(context);
		}

		@Override
		public String toString() {
			return "Item details";
		}

	},

	/**
	 * View an item in the summary lists
	 */
	ItemSummary() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return ItemType.hasItemTypes(context);
		}

		@Override
		public String toString() {
			return "Item summaries";
		}
	},

	/**
	 * Send a nudge to a member
	 */
	SendNudge() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return Module.Nudges.enabled(context);
		}

		@Override
		public String toString(RequestContext context) {
			return "Send " + Captions.getInstance(context).getWinksCaption();
		}
	},

	/**
	 * View winks received
	 */
	ViewNudges() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return Module.Nudges.enabled(context);
		}

		@Override
		public String toString(RequestContext context) {
			return "View received " + Captions.getInstance(context).getWinksCaptionPlural();
		}

	},

	/**
	 * Send messages
	 */
	SendMessage() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return Module.PrivateMessages.enabled(context);
		}

		@Override
		public String toString() {
			return "Send messages";
		}
	},

	/**
	 * Read messages received
	 */
	ReadMessages() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return Module.PrivateMessages.enabled(context);
		}

		@Override
		public String toString() {
			return "Read messages";
		}

	},

	UserplaneChat() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return Module.UserplaneChat.enabled(context);
		}

		@Override
		public String toString() {
			return "Userplane Chat";
		}

	},

	UserplaneMessenger() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return Module.UserplaneMessenger.enabled(context);
		}

		@Override
		public String toString() {
			return "Userplane Messenger";
		}

	},

	/**
	 * Use buddies
	 */
	Buddies() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return Module.Buddies.enabled(context);
		}

		public String toString(RequestContext context) {
			return Captions.getInstance(context).getBuddiesCaptionPlural();
		}

	},

	/**
	 * See who has looked at your profile
	 */
	MemberViews() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return Module.MemberViewers.enabled(context);
		}

		@Override
		public String toString() {
			return "Views";
		}
	},

	/**
	 * Own profile images
	 */
	ProfileImages() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return Module.MemberProfiles.enabled(context) && Module.MemberImages.enabled(context);
		}

		@Override
		public String toString() {
			return "Profile images";
		}
	},

	/**
	 * create a topic in a forum
	 */
	PostTopic() {

		@Override
		public boolean isEnabled(RequestContext context) {

			return Module.Forum.enabled(context);
		}
	},

	/**
	 * Let the user view the forum and go into it
	 */
	ViewForum() {

		@Override
		public boolean isEnabled(RequestContext context) {

			return Module.Forum.enabled(context);
		}
	},

    ViewForumContent() {

        @Override
        public boolean isEnabled(RequestContext context) {

            return Module.Forum.enabled(context);
        }
    },

    PmAttachments() {

		@Override
		public boolean isEnabled(RequestContext context) {

			return Module.PrivateMessages.enabled(context);
		}
	},

	/**
	 * Post a reply to a topic in a forum
	 */
	PostReply() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return Module.Forum.enabled(context);
		}
	},

	/**
	 * View forms
	 */
	ViewForm() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return true;
		}

		@Override
		public String toString() {
			return "View form";
		}

	},

	/**
	 * 
	 */
	Calculators() {

		@Override
		public boolean isEnabled(RequestContext context) {

			return Module.Calculators.enabled(context);
		}
	},

	/**
	 * Delete attachments
	 */
	AttachmentsDelete() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return true;
		}

		@Override
		public String toString() {
			return "Delete attachments";
		}

	},

	/**
	 * To record videos 
	 */
	UserplaneRecorder() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return Module.UserplaneRecorder.enabled(context);
		}

		@Override
		public String toString() {
			return "Userplane recorder";
		}

	},

	/**
	 * To view videos 
	 */
	UserplaneViewer() {

		@Override
		public boolean isEnabled(RequestContext context) {
			return Module.UserplaneRecorder.enabled(context);
		}

		@Override
		public String toString() {
			return "Userplane viewer";
		}
	},

    /**
     * View an item price in the summary lists
     */
    ItemPriceSummary() {

        @Override
        public boolean isEnabled(RequestContext context) {
            return ItemType.hasItemTypes(context);
        }

        @Override
        public String toString() {
            return "Item price summaries";
        }
    };

	private boolean check(List<Permission> permissions, PermissionType permissionType) {

		for (Permission permission : permissions) {
			if (permission.getPermissionType() != null) {
				if (permission.getPermissionType().equals(permissionType))
					return true;
			}
		}
		return false;
	}

	public final boolean check(RequestContext context, Item account) {
		return check(context, account, null);
	}

	/**
	 * Checks this permission for this account.
	 * 
	 * Returns true if the account has permission
	 * 
	 * If the account is null then it will check permissions against guest permissions only 
	 * 
	 */
	public final boolean check(RequestContext context, Item account, Permissions p) {

		/*
		 * If Permissions is disabled then always allow the permission.
		 */
		if (!Module.Permissions.enabled(context)) {
			return true;
		}

		/*
		 * If this object is not null then make sure permissions are enabled on this object, otherwise always approve
		 */
		if (p != null) {
			if (!p.isPermissions()) {
				return true;
			}
		}

		// check this permission is enabled
		if (!isEnabled(context)) {
			return false;
		}

		// check guest access group first as this always applies to everyone !
		List<Permission> permissions = AccessGroup.getGuest(context).getPermissions(p);
		if (check(permissions, this)) {
			return true;
		}

		// no further checks if account is null
		if (account == null) {
			return false;
		}

        // check permissions granted to this item type
        permissions = account.getItemType().getPermissions(p);
        if (check(permissions, this)) {
            return true;
        }

		/*
		 * If subscriptions enabled then check that
		 */
		if (Module.Subscriptions.enabled(context)) {

			Subscription sub = account.getSubscription();

			// check permissions granted to the subscription level if available
			if (sub.hasSubscriptionLevel()) {

				permissions = sub.getSubscriptionLevel().getPermissions(p);
				if (check(permissions, this)) {
					return true;
				}
			}
		}

		// check permissions for each access group if any set
		for (AccessGroup accessGroup : account.getAccessGroups()) {

			permissions = accessGroup.getPermissions(p);
			if (check(permissions, this)) {
				return true;
			}
		}

		return false;
	}

	public abstract boolean isEnabled(RequestContext context);

	public String toString(RequestContext context) {
		return toString();
	}

}
