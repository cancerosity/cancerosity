package org.sevensoft.ecreator.model.accounts.subscriptions;

import java.util.List;

import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 28 Dec 2006 17:08:38
 *
 */
@Table("subscriptions_modules")
public class SubscriptionModule extends EntityObject {

	private ItemType	itemType;
	private String	subscriptionsContent;

	/**
	 * 
	 */
	private boolean	forceSubscription;

	/**
	 * 
	 */
	private String	completionContent;

	private String	ratesHeader;

	private String	ratesFooter;

	protected SubscriptionModule(RequestContext context) {
		super(context);
	}

	public SubscriptionModule(RequestContext context, ItemType itemType) {
		super(context);
		this.itemType = itemType;
		save();
	}

	public SubscriptionLevel addSubscriptionLevel() {
		return new SubscriptionLevel(context, itemType, "new subscription level");
	}

	public final String getCompletionContent() {
		return completionContent;
	}

	public final ItemType getItemType() {
		return itemType.pop();
	}

	/**
	 * Returns all subscriptions level set on this item type that are live
	 */
	public List<SubscriptionLevel> getLiveSubscriptionLevels() {
		return SimpleQuery.execute(context, SubscriptionLevel.class, "itemType", itemType, "live", true);
	}

	public final String getRatesFooter() {
		return ratesFooter;
	}

	public final String getRatesHeader() {
		return ratesHeader;
	}

	public List<SubscriptionLevel> getSubscriptionLevels() {
		return SimpleQuery.execute(context, SubscriptionLevel.class, "itemType", itemType);
	}

	public final String getSubscriptionsContent() {
		return subscriptionsContent;
	}

	public List<SubscriptionLevel> getVisibleSubscriptionLevels() {
		return SimpleQuery.execute(context, SubscriptionLevel.class, "itemType", itemType, "hidden", false, "live", true);
	}

	/**
	 * 
	 */
	public boolean hasCompletionContent() {
		return completionContent != null;
	}

	public boolean hasRatesFooter() {
		return ratesFooter != null;
	}

	public boolean hasRatesHeader() {
		return ratesHeader != null;
	}

	public boolean hasSubscriptionsContent() {
		return subscriptionsContent != null;
	}

	public final boolean isForceSubscription() {
		return forceSubscription;
	}

	public boolean removeSubscriptionLevel(SubscriptionLevel subscriptionLevel) {
		return subscriptionLevel.delete();
	}

	public final void setCompletionContent(String completionContent) {
		this.completionContent = completionContent;
	}

	public final void setForceSubscription(boolean forceSubscription) {
		this.forceSubscription = forceSubscription;
	}

	public final void setRatesFooter(String rateFooter) {
		this.ratesFooter = rateFooter;
	}

	public final void setRatesHeader(String rateHeader) {
		this.ratesHeader = rateHeader;
	}

	public final void setSubscriptionsContent(String subscriptionsContent) {
		this.subscriptionsContent = subscriptionsContent;
	}
}
