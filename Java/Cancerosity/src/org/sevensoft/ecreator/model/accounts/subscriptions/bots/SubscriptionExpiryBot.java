package org.sevensoft.ecreator.model.accounts.subscriptions.bots;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;

import org.sevensoft.ecreator.model.accounts.subscriptions.Subscription;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 29-Jan-2006 22:09:59
 *
 */
public class SubscriptionExpiryBot {

	private Logger		logger	= Logger.getLogger("cron");
	private RequestContext	context;

	public SubscriptionExpiryBot(RequestContext context) {
		this.context = context;
	}

	public List<Item> run() {

		logger.config("[SubscriptionExpiryBot] running");

		// get all accounts with an expiry date
		List<Item> accounts = Subscription.getExpired(context);
		logger.config("[SubscriptionExpiryBot] " + accounts.size() + " accounts retrieved");

		int cancelled = 0;
        List<Item> expired = new ArrayList<Item>();
		for (Item account : accounts) {

			Subscription sub = account.getSubscription();

			logger.fine("[SubscriptionExpiryBot] account #" + account.getId() + " expires on " + sub.getSubscriptionExpiryDate().toString("dd-MM-yyyy"));

			if (sub.hasSubscriptionExpired()) {
				sub.cancelSubscription();
				account.log("[SubscriptionExpiryBot] Subscription has expired");
				cancelled++;
                expired.add(account);
			}
		}

//		return cancelled;
        return expired;
	}
}
