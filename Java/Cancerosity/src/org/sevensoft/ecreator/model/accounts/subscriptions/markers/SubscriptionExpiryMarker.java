package org.sevensoft.ecreator.model.accounts.subscriptions.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.accounts.subscriptions.Subscription;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 11 Dec 2006 21:17:40
 * 
 *
 */
public class SubscriptionExpiryMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		Item account = (Item) context.getAttribute("account");
		if (account == null)
			return null;

		Subscription sub = account.getSubscription();

		if (!sub.hasSubscriptionLevel()) {
			return null;
		}

		if (sub.isLifetimeSubscription()) {
			return null;
		}

		String expiry = sub.getSubscriptionExpiryDate().toString("dd-MMM-yy");
		return super.string(context, params, expiry);
	}

	@Override
	public String getDescription() {
		return "Shows the subscription expiry date for the currently logged in member";
	}

	public Object getRegex() {
		return "subscription_expiry";
	}
}
