package org.sevensoft.ecreator.model.accounts.permissions;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 22 Dec 2006 13:45:15
 * 
 * Maps an account to an access group
 *
 */
@Table("permissions_accessgroups_items")
public class AccessGroupItem extends EntityObject {

	private Item		item;
	private AccessGroup	accessGroup;

	protected AccessGroupItem(RequestContext context) {
		super(context);
	}

	public AccessGroupItem(RequestContext context, AccessGroup ag, Item item) {
		super(context);

		if (!item.getAccessGroups().contains(ag)) {

			this.accessGroup = ag;
			this.item = item;
			
			save();
		}
	}

}
