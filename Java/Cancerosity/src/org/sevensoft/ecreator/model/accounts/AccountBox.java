package org.sevensoft.ecreator.model.accounts;

import org.sevensoft.ecreator.iface.admin.accounts.boxes.AccountBoxHandler;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 31-Oct-2005 10:41:09
 * 
 * Shows a link to the account and details of the currently logged in user
 */
@Table("boxes_account")
@Label("Account box")
@HandlerClass(AccountBoxHandler.class)
public class AccountBox extends Box {

	public AccountBox(RequestContext context) {
		super(context);
	}

	public AccountBox(RequestContext context, String location) {
		super(context, location);
	}

	@Override
	protected String getCssIdDefault() {
		return "member_account";
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	public String render(RequestContext context) {

		Item account = (Item) context.getAttribute("account");
		if (account == null) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		sb.append(getTableTag());

		sb.append("<tr><td>");
		sb.append("You are logged in as ");
		sb.append(account.getDisplayName());
		sb.append(". To view your account ");
		sb.append(new LinkTag(AccountHandler.class, null, "click here"));
		sb.append(" or to log out ");
		sb.append(new LinkTag(LoginHandler.class, "logout", "click here"));
		sb.append(".</td></tr>");

		sb.append("<tr><td class='bottom'>&nbsp;</td></tr>");
		sb.append("</table>");

		return sb.toString();
	}
}
