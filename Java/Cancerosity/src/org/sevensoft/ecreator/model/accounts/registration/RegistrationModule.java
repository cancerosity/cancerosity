package org.sevensoft.ecreator.model.accounts.registration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.model.accounts.registration.emails.RegistrationEmail;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.marketing.newsletter.AutoSignupNewsletter;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.misc.agreements.Agreement;
import org.sevensoft.ecreator.model.misc.agreements.AgreementOwner;
import org.sevensoft.ecreator.model.misc.agreements.AgreementUtil;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 22 Sep 2006 17:42:43
 * 
 * Registration options for an item type
 *
 */
@Table("registration_module")
public class RegistrationModule extends EntityObject implements AgreementOwner {

	/**
	 * 
	 */
	public static RegistrationModule get(RequestContext context, ItemType itemType) {
		RegistrationModule module = SimpleQuery.get(context, RegistrationModule.class, "itemType", itemType);
		return module == null ? new RegistrationModule(context, itemType) : module;
	}

	/**
	 * Returns true if the email address has not been used by another account
	 */
	public static boolean isUniqueEmail(RequestContext context, String email) {
		return SimpleQuery.count(context, Item.class, "email", email) == 0;
	}

	/**
	 * Send an email to the user when they register an account
	 */
	private boolean		registrationEmail;

	/**
	 * 
	 */
	private int			minImages, maxImages;

	/**
	 * A list of referrers for this member type only, will override any referrers set in member settings
	 */
	private List<String>	referrers;

	private String		moderationApprovalEmail;

	/**
	 * 
	 */
	private String		moderationRejectionEmail;

	/**
	 * Content of the email sent on registration
	 */
	private String		registrationEmailContent;

	/**
	 * A hidden registration can still be signed up but is hidden from the general list
	 */
	private boolean		hidden;

	/**
	 * Emails to third parties when someone has registered
	 */
	private List<String>	registrationEmailCc;

	/**
	 * Description show below the enter email box
	 */
	private String		emailDescription;

	/**
	 * 
	 * Registration item type
	 * 
	 *  **BC**
	 */
	private ItemType		itemType;

	/**
	 * Put the member into the inactive status until the account has been approved.
	 */
	private boolean		moderation;

	/**
	 * Agreement that must be confirmed before membership can be completed.
	 */
	private Agreement		agreement;

	/**
	 * Content that is displayed once a member registers. Is override by subscriptions and forward url
	 */
	private String		registrationCompleteContent;

	/**
	 * Content that is shown before the user is registered.
	 */
	private String		registrationContent;

	/**
	 * Forward users to this url once registration is completed
	 */
	private String		registrationForwardUrl;

	/**
	 * 
	 */
	private String		registrationIcon;

	/**
	 * Show the option to upload profile images on registration.
	 */
	private boolean		registrationImages;

	/**
	 * If true then you must upload at least one image before you can register.
	 */
	private boolean		requireRegistrationImages;

	private boolean		validateEmails;

	private String		loginSectionTitle;

	private String		loginSectionIntro;

	private String		nameDescription;

    private String      registrationWelcome;

    private String      registratioinInformationContent;

    private RegistrationModule(RequestContext context) {
		super(context);
	}

	private RegistrationModule(RequestContext context, ItemType it) {
		super(context);
		this.itemType = it;

		save();
	}

	public Agreement getAgreement() {
		return (Agreement) (agreement == null ? null : agreement.pop());
	}

	public List<Newsletter> getAutoSignupNewsletters() {
		Query q = new Query(context, "select n.* from # n join # asn on n.id=asn.newsletter where asn.itemType=?");
		q.setTable(Newsletter.class);
		q.setTable(AutoSignupNewsletter.class);
		q.setParameter(this);
		return q.execute(Newsletter.class);
	}

	public String getEmailDescription() {
		return emailDescription == null ? "Your email will be used to login.\nPlease enter a valid email address as we will use this address to verify your account.\nPlease note, automated emails can sometimes go into the junk/spam folders."
				: emailDescription;
	}

	public final ItemType getItemType() {
		return itemType.pop();
	}

	public String getLoginSectionIntro() {
		return loginSectionIntro == null ? "These are the details for your account. "
				+ "Once you've created an account you will be able to log in using the email and password you specify below. "
				+ "Fields marked with a * are mandatory." : loginSectionIntro;
	}

	public String getLoginSectionTitle() {
		return loginSectionTitle == null ? "Your login and personal details" : loginSectionTitle;
	}

	public final int getMaxImages() {
		return maxImages;
	}

	public final int getMinImages() {
		return minImages;
	}

	public String getNameDescription() {
		return nameDescription == null ? "Enter your full name" : nameDescription;
	}

	public List<String> getReferrers() {
		if (referrers == null) {
			referrers = new ArrayList();
		}
		return referrers;
	}

	public final String getRegistrationCompleteContent() {
		return registrationCompleteContent;
	}

	public final String getRegistrationContent() {
		return registrationContent;
	}

	public List<String> getRegistrationEmailCcs() {
		if (registrationEmailCc == null) {
			registrationEmailCc = new ArrayList();
		}
		return registrationEmailCc;
	}

	public String getRegistrationEmailContent() {

		if (registrationEmailContent == null) {

			return "Dear [name],\n\n" + "Thank you for registering on [site].\n\n" + "Your login is [login]\n"
					+ "Your password is what you chose when registering.\n\n" + "Click this link to log in:\n" + "[loginurl]\n\n"
					+ "Please do not reply to this mail.\n\n" + "Kind regards,\n\n" + "[site] team.";
		}

		return registrationEmailContent;
	}

	public String getRegistrationForwardUrl() {
		return registrationForwardUrl;
	}

	public final String getRegistrationIcon() {
		return registrationIcon;
	}

    public final String getRegistrationInformationContent() {
        return registratioinInformationContent;
    }

    public final String getRegistrationWelcome() {
        return registrationWelcome == null ? "Welcome to the registration section" : registrationWelcome;
    }

    public boolean hasAgreement() {
		return agreement != null;
	}

	public boolean hasEmailDescription() {
		return emailDescription != null;
	}

	public boolean hasExternalAgreement() {
		return AgreementUtil.hasExternalAgreement(this);
	}

	public boolean hasInlineAgreement() {
		return AgreementUtil.hasInlineAgreement(this);
	}

	public boolean hasReferrers() {
		return !getReferrers().isEmpty();
	}

	public final boolean hasRegistrationCompleteContent() {
		return registrationCompleteContent != null;
	}

	public final boolean hasRegistrationContent() {
		return registrationContent != null;
	}

	public boolean hasRegistrationEmailCcs() {
		return getRegistrationEmailCcs().size() > 0;
	}

	public boolean hasRegistrationEmailContent() {
		return registrationEmailContent != null;
	}

	public boolean hasRegistrationForwardUrl() {
		return registrationForwardUrl != null;
	}

	public boolean hasRegistrationIcon() {
		return registrationIcon != null;
	}

	public final boolean isHidden() {
		return hidden;
	}

	public boolean isModeration() {
		return moderation;
	}

	public final boolean isRegistrationEmail() {
		return registrationEmail;
	}

	public final boolean isRegistrationImages() {
		return registrationImages || requireRegistrationImages;
	}

	public final boolean isRequireRegistrationImages() {
		return requireRegistrationImages;
	}

	public boolean isValidateEmails() {
		return validateEmails;
	}

	public void sendRegistrationEmail(Item account, String password) {

		if (isRegistrationEmail()) {

			try {

				logger.fine("[RegistrationSession] sending registration email to user");
				RegistrationEmail em = new RegistrationEmail(context, account, getRegistrationEmailContent(), account.getEmail(), password);
				new EmailDecorator(context, em).send(Config.getInstance(context).getSmtpHostname());
				account.log("Sent registration email to user");

			} catch (EmailAddressException e) {
				e.printStackTrace();
				logger.warning(e.toString());

			} catch (SmtpServerException e) {
				e.printStackTrace();
				logger.warning(e.toString());
			}
		}
	}

	public void sendRegistrationEmailCC(Item account, String password) {

		if (hasRegistrationEmailContent()) {

			try {

				logger.fine("[RegistrationSession] sending registration email to cc's");
				RegistrationEmail em = new RegistrationEmail(context, account, getRegistrationEmailContent(), getRegistrationEmailCcs(), password);
				new EmailDecorator(context, em).send(Config.getInstance(context).getSmtpHostname());

				account.log("Sent registration cc emails to " + getRegistrationEmailCcs());

			} catch (EmailAddressException e) {
				e.printStackTrace();
				logger.warning(e.toString());

			} catch (SmtpServerException e) {
				e.printStackTrace();
				logger.warning(e.toString());
			}
		}
	}

	public void setAgreement(Agreement agreement) {
		this.agreement = agreement;
	}

	public void setEmailDescription(String emailDescription) {
		this.emailDescription = emailDescription;
	}

	public final void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public final void setLoginSectionIntro(String loginSectionIntro) {
		this.loginSectionIntro = loginSectionIntro;
	}

	public final void setLoginSectionTitle(String loginSectionTitle) {
		this.loginSectionTitle = loginSectionTitle;
	}

	public final void setMaxImages(int maxImages) {
		this.maxImages = maxImages;
	}

	public final void setMinImages(int minImages) {
		this.minImages = minImages;
	}

	public void setModeration(boolean registrationApproval) {
		this.moderation = registrationApproval;
	}

	public final void setNameDescription(String nameDescription) {
		this.nameDescription = nameDescription;
	}

	public final void setReferrers(Collection<String> referrers) {
		getReferrers().clear();
		getReferrers().addAll(referrers);
	}

	public void setRegistrationCompleteContent(String registrationCompleteContent) {
		this.registrationCompleteContent = registrationCompleteContent;
	}

	public void setRegistrationContent(String registrationContent) {
		this.registrationContent = registrationContent;
	}

	public void setRegistrationEmail(boolean registrationEmail) {
		this.registrationEmail = registrationEmail;
	}

	public void setRegistrationEmailCcs(List<String> e) {

		this.registrationEmailCc = new ArrayList();
		if (e != null) {
			this.registrationEmailCc.addAll(e);
		}
	}

	public void setRegistrationEmailContent(String registrationEmailContent) {
		this.registrationEmailContent = registrationEmailContent;
	}

	public final void setRegistrationForwardUrl(String registrationForwardUrl) {
		this.registrationForwardUrl = registrationForwardUrl;
	}

	public final void setRegistrationIcon(String registrationIcon) {
		this.registrationIcon = registrationIcon;
	}

	public final void setRegistrationImages(boolean profileImagesOnRegistration) {
		this.registrationImages = profileImagesOnRegistration;
	}

	public final void setRequireRegistrationImages(boolean profileImagesRequired) {
		this.requireRegistrationImages = profileImagesRequired;
	}

    public final void setRegistrationInformationContent(String registrationInformationContent) {
        this.registratioinInformationContent = registrationInformationContent;
    }

    public final String getModerationApprovalEmail() {
        return moderationApprovalEmail;
    }

    public final void setRegistrationWelcome(String registrationWelcome) {
        this.registrationWelcome = registrationWelcome;
    }

    public boolean hasModerationApprovalEmail() {
		return moderationApprovalEmail != null;
	}

	public boolean hasModerationRejectionEmail() {
		return getModerationRejectionEmail() != null;
	}

	public final void setModerationApprovalEmail(String moderationApprovalEmail) {
		this.moderationApprovalEmail = moderationApprovalEmail;
	}

	public final String getModerationRejectionEmail() {
		return moderationRejectionEmail;
	}

	public final void setModerationRejectionEmail(String moderationRejectionEmail) {
		this.moderationRejectionEmail = moderationRejectionEmail;
	}

	public void reject(Item account) {

		logger.fine("[RegistrationModule] Rejecting account: " + account);
		logger.fine("[RegistrationModule] moderationRejectionEmail: " + moderationRejectionEmail);

		// send out rejection email if present
		if (hasModerationRejectionEmail()) {

			Config config = Config.getInstance(context);

			Email e = new Email();
			e.setTo(account.getEmail());
			e.setSubject("Account rejected");
            e.setBody(EmailTagParser.parse(context, account, moderationRejectionEmail, null));
			e.setFrom(config.getServerEmail());
			try {
				logger.fine("[RegistrationModule] attempting to send email: " + e);
				new EmailDecorator(context, e).send(config.getSmtpHostname());
			} catch (EmailAddressException e1) {
                logger.warning(moderationRejectionEmail);
				logger.warning(e1.toString());
                e1.printStackTrace();
			} catch (SmtpServerException e1) {
				logger.warning(e1.toString());
                e1.printStackTrace();
			}
		}
	}

	public void approve(Item account) {

		// send out approval email if present
		if (hasModerationApprovalEmail()) {

			Config config = Config.getInstance(context);

			Email e = new Email();
			e.setTo(account.getEmail());
			e.setSubject("Account approved");
            e.setBody(EmailTagParser.parse(context, account, moderationApprovalEmail, null));
			e.setFrom(config.getServerEmail());
			try {
				new EmailDecorator(context, e).send(config.getSmtpHostname());
			} catch (EmailAddressException e1) {
                logger.warning(moderationApprovalEmail);
				logger.warning(e1.toString());
                e1.printStackTrace();
			} catch (SmtpServerException e1) {
				logger.warning(e1.toString());
                e1.printStackTrace();
			}
		} else {
            if (this.isRegistrationEmail()) {
                this.sendRegistrationEmail(account, account.getPasswordHash());
            }
        }
	}

}
