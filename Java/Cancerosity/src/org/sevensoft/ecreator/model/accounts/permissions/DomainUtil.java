package org.sevensoft.ecreator.model.accounts.permissions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 1 Jun 2006 00:09:08
 *
 */
public class DomainUtil {

	private static final Logger	logger	= Logger.getLogger("ecreator");

	public static List<Domain> get(RequestContext context) {

		List<Domain> domains = new ArrayList();
		domains.addAll(AccessGroup.getAll(context));
		domains.addAll(ItemType.getAccounts(context));
		domains.addAll(SubscriptionLevel.get(context));

		return domains;
	}

	public static List<Permission> getPermissions(RequestContext context, Domain domain, Permissions p) {

		if (p == null) {

			return SimpleQuery.execute(context, Permission.class, "domain", domain.getFullId());

		} else {

			return SimpleQuery.execute(context, Permission.class, "domain", domain.getFullId(), "permissible", p.getFullId());
		}
	}

	public static boolean hasPermission(RequestContext context, Domain domain, PermissionType type, Permissions obj) {
		return getPermissions(context, domain, obj).contains(type);
	}

	/**
	 * 
	 */
	private static void removePermission(RequestContext context, Domain domain, Permissions p, PermissionType permissionType) {
		if (p == null) {
			SimpleQuery.delete(context, Permission.class, "domain", domain.getFullId(), "permissionType", permissionType);
		} else {
			SimpleQuery.delete(context, Permission.class, "domain", domain.getFullId(), "permissible", p.getFullId(), "permissionType", permissionType);
		}
	}

	/**
	 * 
	 */
	public static void removePermissions(RequestContext context, Domain domain, Collection<PermissionType> permissionTypes, Permissions p) {
		for (PermissionType type : permissionTypes) {
			removePermission(context, domain, p, type);
		}
	}

	public static void removePermissions(RequestContext context, Domain domain, Permissions p) {

	}

	public static void savePermissions(RequestContext context, Permissions p, MultiValueMap<String, PermissionType> permissions,
			Collection<PermissionType> permissionTypes) {

		logger.fine("[DomainUtil] saving permissions=" + permissions);

		List<Domain> domains = DomainUtil.get(context);
		for (Domain domain : domains) {

			logger.fine("[DomainUtil] removing permissions for domain=" + domain);
			domain.removePermissions(permissionTypes, p);

			if (permissions.containsKey(domain.getFullId())) {

				for (PermissionType type : permissions.list(domain.getFullId())) {

					logger.fine("[DomainUtil] setting permisson domain=" + domain + ", type=" + type);
					domain.setPermission(type, p);
				}
			}
		}
	}

	public static int getDailyPmLimit(RequestContext context, Item account) {

		// if null then check on guest access group
		if (account == null) {
			return AccessGroup.getGuest(context).getDailyPmLimit();

		} else {

			Collection<Domain> domains = account.getDomains();
			int limit = 0;
			for (Domain domain : domains) {
				if (domain.getDailyPmLimit() > limit) {
					limit = domain.getDailyPmLimit();
				}
			}

			return limit;

		}
	}

	public static int getDailyProfileViewsLimit(RequestContext context, Item account) {

		// if null then check on guest access group
        if (account == null) {
            AccessGroup guest = AccessGroup.getGuest(context);
            if (guest == null) {
                return 0;
            } else {
                return guest.getDailyProfileViewsLimit();
            }
            
        } else {

			Collection<Domain> domains = account.getDomains();
			int limit = 0;
			for (Domain domain : domains) {

				if (domain.getDailyProfileViewsLimit() > limit) {
					limit = domain.getDailyProfileViewsLimit();
				}
			}

			return limit;

		}
	}
}
