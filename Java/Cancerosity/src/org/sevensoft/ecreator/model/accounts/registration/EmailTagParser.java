package org.sevensoft.ecreator.model.accounts.registration;

import org.sevensoft.ecreator.iface.frontend.account.PasswordHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionHandler;
import org.sevensoft.ecreator.model.accounts.subscriptions.Subscription;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.listings.Listing;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 21 Dec 2006 16:34:43
 *
 */
public class EmailTagParser {

	public static String parse(RequestContext context, Item item, String string, String password) {

		if (string == null) {
			return null;
		}

		// name 
		string = string.replace("[name]", item.getName());

		Subscription subscription = item.getSubscription();
		if (subscription.hasSubscriptionLevel()) {

			// number of days until expiry
			string = string.replace("[expiry_days]", String.valueOf(subscription.getSubscriptionRemainingDays()));

			// actual date of expiry
			if (subscription.hasSubscriptionExpiryDate()) {
				string = string.replace("[expiry_date]", subscription.getSubscriptionExpiryDate().toString("dd-MMM-yyyy"));
			} else {
				string = string.replace("[expiry_date]", "");
			}

			// link to renewal
			string = string.replace("[subscription_url]", Config.getInstance(context).getUrl() + "/" + new Link(SubscriptionHandler.class));

		}


        if (string.contains("[listing_expiry_date]")) {
            //should be sent only for expiring listings, so expiry date ca not be NULL
            string = string.replace("[listing_expiry_date]", item.getListing().getExpiryDate().toString("dd-MMM-yyyy"));
        }

		// members email
		if (item.hasEmail()) {
			string = string.replace("[email]", item.getEmail());
			string = string.replace("[login]", item.getEmail());
		}

		// attributes
		for (Attribute attribute : item.getAttributes()) {

			String value = item.getAttributeValue(attribute);
			if (value != null) {

				string = string.replace("[attribute_" + attribute.getId() + "]", value);

			}
		}

		string = string.replace("[site]", Company.getInstance(context).getName());
		string = string.replace("[id]", item.getIdString());
		string = string.replace("[loginurl]", Config.getInstance(context).getUrl() + "/" + new Link(LoginHandler.class));
        string = string.replace("[itemurl]", item.getUrl());

		if (string.contains("[password]")) {

			if (password == null) {
				password = item.setPassword();
			}

			string = string.replace("[password]", password);
		}

		string = string.replace("[changepassword]", Config.getInstance(context).getUrl() + "/" + new Link(PasswordHandler.class));

		return string;
	}

}
