package org.sevensoft.ecreator.model.accounts.registration.export;

import java.io.IOException;

import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.httpc.HttpMethod;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 5 Jun 2007 15:31:38
 * 
 * Export a new sign up to business club 365
 *
 */
public class BusinessClubExport {

	private final RequestContext	context;

	public BusinessClubExport(RequestContext context) {
		this.context = context;
	}

	public void run(Item account, String password) throws IOException {

		HttpClient hc = new HttpClient("http://secure.sitekit.net/admin/ws/userfunctions.asmx/AddUser?", HttpMethod.Get);
		hc.setParameter("UserAddSiteID", "2122");
		hc.setParameter("UserAddUserGroupID", "2631");
		hc.setParameter("UserAddExpiryDate", "1/1/2100");
		hc.setParameter("UserAddDepartment", account.getItemType().getName());
		hc.setParameter("UserAddEmail", account.getEmail());
		hc.setParameter("UserAddUserName", account.getEmail());
		hc.setParameter("UserAddPassword", account.getPasswordHash());
		hc.setParameter("UserAddFullName", account.getName());

		// optionals
		String telephone = account.getAttributeValue("telephone");
		if (telephone == null) {
			telephone = "";
		}
		hc.setParameter("UserAddTelephone", telephone);

		String fax = account.getAttributeValue("fax");
		if (fax == null) {
			fax = "";
		}
		hc.setParameter("UserAddFax", fax);

		hc.setParameter("UserAddJobTitle", "");
		hc.setParameter("UserAddAdditionalInfo", "");
		hc.setParameter("UserAddCustomField1", "");
		hc.setParameter("UserAddCustomField2", "");
		hc.setParameter("UserAddCustomField3", "");
		hc.setParameter("UserAddCustomField4", "");
		hc.setParameter("UserAddCustomField5", "");
		hc.setParameter("UserAddCustomField6", "");

		hc.connect();

	}

}
