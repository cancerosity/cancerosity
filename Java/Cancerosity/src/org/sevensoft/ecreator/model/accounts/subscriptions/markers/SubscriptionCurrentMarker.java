package org.sevensoft.ecreator.model.accounts.subscriptions.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.accounts.subscriptions.Subscription;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 11 Dec 2006 21:17:40
 * 
 * Shows the subscription for the currently logged in member
 *
 */
public class SubscriptionCurrentMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		Item account = (Item) context.getAttribute("account");
		if (account == null)
			return null;
		
		Subscription sub = account.getSubscription();

		String name;
		if (sub.hasSubscriptionLevel()) {

			name = sub.getSubscriptionLevel().getName();

		} else {

			name = params.get("none");
			if (name == null)
				name = "None";
		}

		return super.string(context, params, name);
	}

	@Override
	public String getDescription() {
		return "Shows the subscription for the currently logged in member";
	}

	public Object getRegex() {
		return "subscription_current";
	}

}
