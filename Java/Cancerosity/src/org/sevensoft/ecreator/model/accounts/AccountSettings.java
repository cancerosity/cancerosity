package org.sevensoft.ecreator.model.accounts;

import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 29 Dec 2006 01:32:28
 */
@Singleton
@Table("accounts_settings")
public class AccountSettings extends EntityObject {

    public static AccountSettings getInstance(RequestContext context) {
        return EntityObject.getSingleton(context, AccountSettings.class);
    }

    private String loginHeader;
    private String loginFooter;
    private Markup loginMarkup;

    protected AccountSettings(RequestContext context) {
        super(context);
    }

    public final String getLoginFooter() {
        return loginFooter;
    }

    public final String getLoginHeader() {
        return loginHeader;
    }

    public final Markup getLoginMarkup() {
        return (Markup) (loginMarkup == null ? null : loginMarkup.pop());
    }

    public boolean hasLoginFooter() {
        return loginFooter != null;
    }

    public boolean hasLoginHeader() {
        return loginHeader != null;
    }

    public boolean hasLoginMarkup() {
        return loginMarkup != null;
    }

    public final void setLoginFooter(String loginFooter) {
        this.loginFooter = loginFooter;
    }

    public final void setLoginHeader(String loginHeader) {
        this.loginHeader = loginHeader;
    }

    public final void setLoginMarkup(Markup loginMarkup) {
        this.loginMarkup = loginMarkup;
    }

}
