package org.sevensoft.ecreator.model.accounts.login.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;

/**
 * @author sks 17 Sep 2006 07:37:51
 *
 */
public class LoginStartMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {
		return new FormTag(LoginHandler.class, "login", "post");
	}

	public Object getRegex() {
		return new String[] { "login_start", "login_begin" };
	}

	@Override
	public String getDescription() {
		return "This tag starts the login form";
	}

	
}
