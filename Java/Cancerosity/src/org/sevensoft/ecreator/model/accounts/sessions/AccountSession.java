package org.sevensoft.ecreator.model.accounts.sessions;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 04-Aug-2005 12:16:41
 * 
 */
@Table("members_sessions")
public class AccountSession extends EntityObject {

	public static Item getItem(RequestContext context, String sessionId) {

		Query q = new Query(context, "select i.* from # i join # s on i.id=s.item where s.sessionId=?");
		q.setTable(Item.class);
		q.setTable(AccountSession.class);
		q.setParameter(sessionId);
		return q.get(Item.class);
	}

	@Index()
	private Item	item;

	@Index()
	private String	sessionId;

	@Index()
	private long	timestamp;

	private AccountSession(RequestContext context) {
		super(context);
	}

	public AccountSession(RequestContext context, String sessionId, Item item) {

		super(context);

		// logout this item from elsewhere
		SimpleQuery.delete(context, AccountSession.class, "sessionId", sessionId);

		this.item = item;
		this.sessionId = sessionId;
		save();
	}

	public String getSessionId() {
		return sessionId;
	}

	public long getTimestamp() {
		return timestamp;
	}

	private void updateTimestamp() {
		this.timestamp = System.currentTimeMillis();
	}
}
