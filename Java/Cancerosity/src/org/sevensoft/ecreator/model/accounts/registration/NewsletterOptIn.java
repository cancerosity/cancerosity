package org.sevensoft.ecreator.model.accounts.registration;

import java.util.List;

import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 29 Sep 2006 11:04:00
 *
 */
@Table("newsletter_optins")
public class NewsletterOptIn extends EntityObject {

	public static void deleteAll(RequestContext context, EntityObject owner) {
		SimpleQuery.delete(context, NewsletterOptIn.class, "owner", owner.getFullId());
	}

	public static List<NewsletterOptIn> get(RequestContext context, EntityObject owner) {
		return SimpleQuery.execute(context, NewsletterOptIn.class, "owner", owner.getFullId());
	}

	public static List<Newsletter> getNewsletters(RequestContext context, EntityObject owner) {
		Query q = new Query(context, "select nl.* from # nl join # nlopt on nl.id=nlopt.newsletter where nlopt.owner=?");
		q.setTable(Newsletter.class);
		q.setTable(NewsletterOptIn.class);
		q.setParameter(owner.getFullId());
		return q.execute(Newsletter.class);
	}

	public static boolean isOptIn(RequestContext context, EntityObject owner, Newsletter newsletter) {

		Query q = new Query(context, "select count(*) from # where newsletter=? and owner=?");
		q.setTable(NewsletterOptIn.class);
		q.setParameter(newsletter);
		q.setParameter(owner.getFullId());
		return q.getInt() > 0;
	}

	private Newsletter	newsletter;

	private String		owner;

	private NewsletterOptIn(RequestContext context) {
		super(context);
	}

	public NewsletterOptIn(RequestContext context, EntityObject obj, Newsletter newsletter) {
		super(context);

		this.owner = obj.getFullId();
		this.newsletter = newsletter;

		save();
	}

}
