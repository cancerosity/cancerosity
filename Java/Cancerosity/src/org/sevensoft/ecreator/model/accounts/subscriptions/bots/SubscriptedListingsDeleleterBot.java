package org.sevensoft.ecreator.model.accounts.subscriptions.bots;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;

import java.util.logging.Logger;
import java.util.List;

/**
 * Delete listings which account subscription is expired
 * <p/>
 * User: Tanya
 * Date: 03.08.2010
 */
public class SubscriptedListingsDeleleterBot {

    private Logger logger = Logger.getLogger("cron");
    private RequestContext context;

    public SubscriptedListingsDeleleterBot(RequestContext context) {
        this.context = context;
    }

    public void run(List<Item> accounts) {
        logger.config("Subscription Expired Listings deleter bot");
        for (Item account : accounts) {
           List<ListingPackage> packages =  account.getItemType().getAccessableListingPackages();
            for (Item listing : account.getListings()) {
                if (listing.getListing() != null && listing.getListing().getListingPackage() != null
//                        && !listing.getListing().getListingPackage().isFree()
                        && !packages.contains(listing.getListing().getListingPackage())) {
                    listing.delete();
                }
            }
        }
    }

}
