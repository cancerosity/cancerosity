package org.sevensoft.ecreator.model.accounts.profile;

import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Mar 2007 14:23:38
 * 
 * Send users an SMS via the site.
 *
 */
@Table("blocks_profile_sms")
public class ProfileSmsBlock extends Block {

	public ProfileSmsBlock(RequestContext context) {
		super(context);
	}

	public ProfileSmsBlock(RequestContext context, BlockOwner owner) {
		super(context, owner, 0);
	}

	@Override
	public Object render(RequestContext context) {
		return null;
	}

}
