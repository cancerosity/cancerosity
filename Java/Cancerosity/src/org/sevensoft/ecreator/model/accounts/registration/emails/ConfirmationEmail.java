package org.sevensoft.ecreator.model.accounts.registration.emails;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.ecreator.iface.frontend.account.registration.RegistrationHandler;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 01-Feb-2006 11:08:30
 *
 */
public class ConfirmationEmail extends Email {
    private RequestContext	context;
	private Config		config;

	public ConfirmationEmail(Item member, RequestContext context) {
        this.context = context;

		Company company = Company.getInstance(context);
		this.config = Config.getInstance(context);

		setFrom(company.getName(), config.getServerEmail());
		setSubject("Confirm your registration");
		setTo(member.getEmail());

		String url = config.getUrl() + "/" + new Link(RegistrationHandler.class, "confirm", "m", member, "code", member.getConfirmationCode());
		setBody("Thank you for registering.\n\nTo complete your registration please click this link:\n" + url);

	}

    @Override
	public void send(String hostname) throws EmailAddressException, SmtpServerException {
		new EmailDecorator(context, this).send(hostname);
	}

}
