package org.sevensoft.ecreator.model.accounts.profile.markers;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.account.profile.ProfileImagesHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 03-Apr-2006 11:42:21
 *
 */
public class ProfileImagesMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.MemberProfiles.enabled(context)) {
			logger.fine("[ProfileImagesMarker] profiles disabled");
			return null;
		}

		Item account = (Item) context.getAttribute("account");
		if (account == null) {
			logger.fine("[ProfileImagesMarker] not logged in");
			return null;
		}

		return super.link(context, params, new Link(ProfileImagesHandler.class), "account_link");
	}

	@Override
	public String getDescription() {
		return "Makes a link to the profile images page";
	}

	public Object getRegex() {
		return new String[] { "profile_images", "account_profile_images" };
	}
}