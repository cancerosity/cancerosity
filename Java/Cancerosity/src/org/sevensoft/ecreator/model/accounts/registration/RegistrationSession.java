package org.sevensoft.ecreator.model.accounts.registration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.ecreator.model.accounts.registration.export.BusinessClubExport;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeValue;
import org.sevensoft.ecreator.model.attributes.support.AttributeImageSupport;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.ItemTypeSettings;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterOptable;
import org.sevensoft.ecreator.model.marketing.newsletter.Subscriber;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.ImageOwner;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.messages.SystemMessage;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 29 May 2006 19:31:05
 *
 */
@Table("registration_session")
public class RegistrationSession extends AttributeImageSupport implements ImageOwner, NewsletterOptable {

	public static void delete(RequestContext context, String sessionId) {
		Query q = new Query(context, "delete from # where sessionId=?");
		q.setTable(RegistrationSession.class);
		q.setParameter(sessionId);
		q.run();
	}

	public static RegistrationSession getBySession(RequestContext context, String sessionId) {

		RegistrationSession s = SimpleQuery.get(context, RegistrationSession.class, "sessionId", sessionId);
		return s == null ? new RegistrationSession(context, sessionId) : s;
	}

	private String	sessionId;

	/**
	 * The item type we are registering as
	 */
	private ItemType	itemType;

	/**
	 * 
	 */
	private boolean	agreed;

	/**
	 * Default fields
	 */
	private String	name, email, password;
	private boolean	offeredImages;

	private String	referrer;
	private String	linkback;

	public RegistrationSession(RequestContext context) {
		super(context);
	}

	public RegistrationSession(RequestContext context, String sessionId) {
		super(context);

		this.sessionId = sessionId;

		save();
	}

	@Override
	public synchronized boolean delete() {
		removeImages();
		removeAttributeValues();
		return super.delete();
	}

	public void getAgreed(boolean b) {
		agreed = b;
	}

	public String getAlt() {
		return null;
	}

	public List<Attribute> getAttributes() {

		List attributes = new ArrayList();

		attributes.addAll(getItemType().getAttributes());
		attributes.addAll(ItemTypeSettings.getInstance(context).getAttributes());

		CollectionsUtil.filter(attributes, new Predicate<Attribute>() {

			public boolean accept(Attribute e) {
				return e.isRegistration();
			}
		});

		return attributes;
	}

	public List<String> getAttributeValues(int i) {
		MultiValueMap<Attribute, String> attributeValues = this.getAttributeValues();
		for (Map.Entry<Attribute, List<String>> entry : attributeValues.entryListSet()) {

			if (entry.getKey().getId() == i)
				return entry.getValue();
		}
		return Collections.emptyList();
	}

	public List<String> getAttributeValues(String attributeName, boolean exact) {
		if (attributeName == null)
			return null;

		attributeName = attributeName.toLowerCase();

		MultiValueMap<Attribute, String> attributeValues = this.getAttributeValues();
		for (Map.Entry<Attribute, List<String>> entry : attributeValues.entryListSet()) {

			if (exact) {

				if (entry.getKey().getName().toLowerCase().equals(attributeName))
					return entry.getValue();

			} else {

				if (entry.getKey().getName().toLowerCase().contains(attributeName))
					return entry.getValue();
			}

		}

		return Collections.emptyList();
	}

	public final String getEmail() {
		return email;
	}

	public final ItemType getItemType() {
		return (ItemType) (itemType == null ? null : itemType.pop());
	}

	public final String getLinkback() {
		return linkback;
	}

	public final String getName() {
		return name;
	}

	public List<Newsletter> getNewsletterOptIns() {
		return NewsletterOptIn.getNewsletters(context, this);
	}

	public final String getPassword() {
		return password;
	}

	public final String getReferrer() {
		return referrer;
	}

	public final String getSessionId() {
		return sessionId;
	}

	public boolean hasErrors() {

		if (name == null) {
			return true;
		}

		if (email == null) {
			return true;
		}

		if (password == null) {
			return true;
		}

		Map<Attribute, String> attributeValues = getAttributeValues();
		for (Attribute attribute : getAttributes()) {

			if (attribute.isOptional()) {
				continue;
			}

			if (!attributeValues.containsKey(attribute)) {
				return true;
			}
		}

		return false;
	}

	public boolean hasItemType() {
		return itemType != null;
	}

	public boolean hasLinkback() {
		return linkback != null;
	}

	public boolean isAgreed() {
		return agreed;
	}

	public boolean isNewsletterOptIn(Newsletter newsletter) {
		return NewsletterOptIn.isOptIn(context, this, newsletter);
	}

	public boolean isOfferedImages() {
		return offeredImages;
	}

	public Item register(String remoteIp) {

		logger.fine("[RegistrationSession] registering account, name=" + name + ", password=" + password + ", referrer=" + referrer);

		Item account = new Item(context, getItemType(), name, "live", null);
		account.setEmail(email);
		account.setPassword(password);
		account.setReferrer(referrer);
		account.save();

		account.log("Registered online");

		final RegistrationModule regModule = itemType.getRegistrationModule();

		if (regModule.hasAgreement()) {
			account.log("Agreed to " + regModule.getAgreement().getTitle());
		}

		account.setAttributeValues(getAttributes(), getAttributeValues());

		try {
			moveImagesTo(account);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ImageLimitException e) {
			e.printStackTrace();
		}

		/*
		 * Lets put us in the newsletters we have opted in for
		 */
		List<Newsletter> newsletters = getNewsletterOptIns();
		if (newsletters.size() > 0) {
			Subscriber sub = Subscriber.get(context, email, true);
			sub.subscribe(newsletters);
		}

//		if (regModule.isRegistrationEmail()) {
//			regModule.sendRegistrationEmail(account, password);
//		}

		if (regModule.hasRegistrationEmailCcs()) {
			regModule.sendRegistrationEmailCC(account, password);
		}

        if (!account.isConfirmed()) {
            try {
                account.sendConfirmationEmail();
            } catch (EmailAddressException e) {
                e.printStackTrace();
            } catch (SmtpServerException e) {
                e.printStackTrace();
            }
        } else if (!regModule.isModeration()) {
            regModule.sendRegistrationEmail(account, password);
        }

        if (regModule.isModeration()) {
			logger.fine("[RegistrationSession] registration approval is on, setting awaiting moderation on the account");
			account.setAwaitingModeration(true);
			account.save();
		}

		/*
		 * Delete this registration session
		 */
		delete();

		new SystemMessage(context, "A new member has registered with the email " + email);

		Config config = Config.getInstance(context);
		if (config.getUrl().contains("businessclub")) {
			try {
				new BusinessClubExport(context).run(account, password);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return account;
	}

	public void removeNewsletterOptIns() {
		NewsletterOptIn.deleteAll(context, this);
	}

	public final void setAgreed(boolean agreed) {
		this.agreed = agreed;
	}

	/**
	 * Sets the attribute values for this page only.
	 * 
	 * @param attributeValues
	 * @param page
	 */
	public void setAttributeValues(MultiValueMap<Attribute, String> attributeValues, int page) {

		for (Attribute attribute1 : getAttributes(page)) {

			String column = this.getClass().getSimpleName();

			Query q = new Query(context, "delete from # where `" + column + "`=? and attribute=?");
			q.setTable(AttributeValue.class);
			q.setParameter(this);
			q.setParameter(attribute1);
			q.run();
		}
		for (Map.Entry<Attribute, String> entry : attributeValues.entrySet()) {

			// get attribute
			Attribute attribute = entry.getKey();
			String value = entry.getValue();
			attribute.setValue(this, value);
		}
	}

	public final void setEmail(String email) {
		this.email = email;
	}

	public final void setItemType(ItemType it) {

		if (ObjectUtil.equal(itemType, it))
			return;

		this.itemType = it;

		/*
		 * As the member type has changed, reset agreements and also remove all attribute values
		 */
		this.agreed = false;
		this.name = null;
		this.email = null;
		this.offeredImages = false;
		this.password = null;
		removeAttributeValues();
	}

	public void setLinkback(String linkback) {
		this.linkback = linkback;
		logger.fine("[RegistrationSession] setting linkback to " + linkback);
	}

	public final void setName(String name) {
		this.name = name;
	}

	public void setNewsletterOptIns(List<Newsletter> newsletters) {
		removeNewsletterOptIns();
		for (Newsletter newsletter : newsletters) {
			new NewsletterOptIn(context, this, newsletter);
		}
	}

	public void setOfferedImages(boolean b) {
		this.offeredImages = b;
	}

	public final void setPassword(String password) {
		this.password = password;
	}

	public final void setReferrer(String referrer) {
		this.referrer = referrer;
	}

	public boolean useImageDescriptions() {
		return false;
	}

	/**
	 * 
	 */
	public boolean hasName() {
		return name != null;
	}

	public boolean hasEmail() {
		return email != null;
	}

	public boolean hasPassword() {
		return password != null;
	}

	/**
	 * 
	 */
	public boolean hasReferrer() {
		return referrer != null;
	}

}
