package org.sevensoft.ecreator.model.accounts.permissions;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.ecreator.model.categories.CategoryModule;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 31 May 2006 18:16:29
 *
 */
public class PermissionCollections {

	public static List<PermissionType> getAttribute() {

		List<PermissionType> item = new ArrayList<PermissionType>();

		item.add(PermissionType.AttributeValue);
		item.add(PermissionType.AttributeVisible);

		return item;
	}

	public static List<PermissionType> getCategory(RequestContext context) {

		List<PermissionType> category = new ArrayList();
		category.add(PermissionType.CategoryStub);
		category.add(PermissionType.CategoryContent);
		category.add(PermissionType.CategoryItems);

		if (CategoryModule.Attachments.enabled(context)) {

			category.add(PermissionType.AttachmentsDownload);
			category.add(PermissionType.AttachmentsUpload);
			category.add(PermissionType.AttachmentsDelete);
		}

		return category;
	}

	public static List<PermissionType> getForm() {

		List<PermissionType> item = new ArrayList();

		item.add(PermissionType.ViewForm);

		return item;
	}

	public static List<PermissionType> getForum() {

		List<PermissionType> item = new ArrayList();

		item.add(PermissionType.PostTopic);
		item.add(PermissionType.PostReply);
		item.add(PermissionType.ViewForum);
        item.add(PermissionType.ViewForumContent);

        return item;
	}

	public static List<PermissionType> getInteraction(RequestContext context) {

		List<PermissionType> interaction = new ArrayList();

		if (Module.Buddies.enabled(context)) {
			interaction.add(PermissionType.Buddies);
		}

		if (Module.PrivateMessages.enabled(context)) {
			interaction.add(PermissionType.SendMessage);
			interaction.add(PermissionType.ReadMessages);
			interaction.add(PermissionType.PmAttachments);
		}

		if (Module.Nudges.enabled(context)) {
			interaction.add(PermissionType.ViewNudges);
			interaction.add(PermissionType.SendNudge);
		}

		if (Module.MemberViewers.enabled(context)) {
			interaction.add(PermissionType.MemberViews);
		}

		if (Module.Buddies.enabled(context)) {
			interaction.add(PermissionType.ProfileImages);
		}

		if (Module.UserplaneChat.enabled(context)) {
			interaction.add(PermissionType.UserplaneChat);
		}

		if (Module.UserplaneMessenger.enabled(context)) {
			interaction.add(PermissionType.UserplaneMessenger);
		}

		if (Module.UserplaneRecorder.enabled(context)) {
			interaction.add(PermissionType.UserplaneRecorder);
			interaction.add(PermissionType.UserplaneViewer);
		}

		return interaction;

	}

	public static List<PermissionType> getItem() {

		List<PermissionType> item = new ArrayList();

		item.add(PermissionType.ItemFull);
		item.add(PermissionType.ItemSummary);
        item.add(PermissionType.ItemPriceSummary);

		return item;
	}
}
