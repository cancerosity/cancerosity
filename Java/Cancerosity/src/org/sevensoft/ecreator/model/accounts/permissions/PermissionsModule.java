package org.sevensoft.ecreator.model.accounts.permissions;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Jan 2007 22:22:19
 *
 */
@Table("permissions_module")
public class PermissionsModule extends EntityObject {

	public static PermissionsModule get(RequestContext context, Category category) {
		PermissionsModule module = SimpleQuery.get(context, PermissionsModule.class, "category", category);
		return module == null ? new PermissionsModule(context, category) : module;
	}

	/**
	 * Owner category
	 */
	private Category	category;

	/**
	 * No login for guest when restricted to this category
	 */
	private boolean	noGuestLogin;
	
	/**
	 * Forward when restricted
	 */
	private String	restrictionForward;

    private String allowOnlyReferrer;

    private Item accountAllowed;

	protected PermissionsModule(RequestContext context) {
		super(context);
	}

	public PermissionsModule(RequestContext context, Category category) {
		super(context);
		this.category = category;
		save();
	}

	public final Category getCategory() {
		return category.pop();
	}

	public final String getRestrictionForward() {
		return restrictionForward;
	}

	public boolean hasRestrictionForward() {
		return restrictionForward != null;
	}

    public String getAllowOnlyReferrer() {
        return allowOnlyReferrer;
    }

    public Item getAccountAllowed() {
        return (Item) (accountAllowed == null ? null : accountAllowed.pop());
    }

    public boolean hasAllowOnlyReferrer() {
        return allowOnlyReferrer != null && allowOnlyReferrer.trim().length() != 0;
	}

    public boolean hasAccountAllowed() {
        return accountAllowed != null;
    }

    public final boolean isNoGuestLogin() {
		return noGuestLogin;
	}

	public final void setNoGuestLogin(boolean noGuestLogin) {
		this.noGuestLogin = noGuestLogin;
	}

	public final void setRestrictionForward(String restrictionForward) {
		this.restrictionForward = restrictionForward;
	}

    public void setAllowOnlyReferrer(String allowOnlyReferrer) {
        this.allowOnlyReferrer = allowOnlyReferrer;
    }

    public void setAccountAllowed(Item accountAllowed) {
        this.accountAllowed = accountAllowed;
    }
}
