package org.sevensoft.ecreator.model.accounts.login.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.text.PasswordTag;

/**
 * @author sks 1 Jun 2006 12:48:24
 *
 */
public class LoginPasswordMarker implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		int size;
		if (params.containsKey("size"))
			size = Integer.parseInt(params.get("size").trim());
		else
			size = 20;

		boolean noerror = params.containsKey("noerror");

		StringBuilder sb = new StringBuilder();
		sb.append(new PasswordTag(null, "password", size));
		if (noerror == false) {
			sb.append(" ");
			sb.append(new ErrorTag(context, "password", "<br/>"));
		}

		return sb;
	}

	public Object getRegex() {
		return "login_password";
	}
}
