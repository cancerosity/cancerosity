package org.sevensoft.ecreator.model.accounts.debug;

import java.util.List;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 31 Aug 2006 23:43:03
 *
 */
@Table("members_debug_logins")
public class Login extends EntityObject {

	public static List<Login> get(RequestContext context, int i) {

		Query q = new Query(context, "select * from # order by id desc");
		q.setTable(Login.class);
		return q.execute(Login.class, i);

	}

	private String	email;
	private String	password;
	private DateTime	date;
	private String	ipAddress;
	private boolean	success;

	private Login(RequestContext context) {
		super(context);
	}

	public Login(RequestContext context, String email, String password, String ipAddress, boolean success) {
		super(context);

		this.email = email;
		if (success)
			this.password = password.replaceAll(".", "*");
		else
			this.password = password;
		this.success = success;
		this.date = new DateTime();
		this.ipAddress = ipAddress;

		save();
	}

	public DateTime getDate() {
		return date;
	}

	public String getEmail() {
		return email;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public String getPassword() {
		return password;
	}

	public boolean isSuccess() {
		return success;
	}

}
