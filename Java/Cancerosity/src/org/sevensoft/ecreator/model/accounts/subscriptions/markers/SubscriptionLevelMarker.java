package org.sevensoft.ecreator.model.accounts.subscriptions.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.accounts.subscriptions.Subscription;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;

/**
 * @author sks 3 Aug 2006 12:40:31
 *
 */
public class SubscriptionLevelMarker implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item profile, String location, int x, int y) {

		if (!Module.Subscriptions.enabled(context)) {
			return null;
		}

		String override = params.get("override");

		Subscription sub = profile.getSubscription();

		if (!sub.hasSubscriptionLevel()) {
			if (override == null) {
				return null;
			}
		}

		SubscriptionLevel level = sub.getSubscriptionLevel();

		String prefix = params.get("prefix");
		String suffix = params.get("suffix");
		boolean text = params.containsKey("text");
		boolean icon = params.containsKey("icon");

		StringBuilder sb = new StringBuilder();
		sb.append("<span class='subscription-status'>");

		if (prefix != null) {
			sb.append(prefix);
		}

		if (level == null) {
			sb.append(override);
		} else {

			if (text || !icon) {
				sb.append(level.getName());
			}

			if (icon && level.hasIcon()) {
				sb.append(new ImageTag("template-data/" + level.getIcon()));
			}
		}

		if (suffix != null) {
			sb.append(suffix);
		}

		sb.append("</span>");

		return sb.toString();
	}

	public Object getRegex() {
		return "subscription_level";
	}
}
