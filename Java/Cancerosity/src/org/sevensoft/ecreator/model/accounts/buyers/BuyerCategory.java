package org.sevensoft.ecreator.model.accounts.buyers;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 19 Jan 2007 15:36:59
 * 
 * A category this buyer is allowed to order products from
 *
 */
public class BuyerCategory extends EntityObject {

	private Item	buyer;
	private Category	category;

	public BuyerCategory(RequestContext context) {
		super(context);
	}

	public BuyerCategory(RequestContext context, Item buyer, Category category) {
		super(context);
		this.buyer = buyer;
		this.category = category;
		
		save();
	}

}
