package org.sevensoft.ecreator.model.accounts.markers;

import java.util.List;
import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.search.ItemSearchHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 24 May 2007 10:42:57
 *
 */
public class AccountItemsMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		List<Item> items = item.getItems();
		if (items.isEmpty()) {
			return null;
		}

		Link link = new Link(ItemSearchHandler.class, null, "searchAccount", item);
		if (params.containsKey("markup")) {
			link.setParameter("markup", params.get("markup"));
		}
		return super.link(context, params, link);
	}

	public Object getRegex() {
		return "account_items";
	}

}
