package org.sevensoft.ecreator.model.misc.seo;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.misc.seo.sku.SkuArrtibutes;

import java.util.Map;

/**
 * @author sks 17-Nov-2005 22:29:54
 * 
 */
@Singleton
@Table("settings_stats")
public class GoogleSettings extends EntityObject {

	public static GoogleSettings getInstance(RequestContext context) {
		return getSingleton(context, GoogleSettings.class);
	}

	private String	googleAnalytics;
	private String	siteMapMetaTag;

    /**
     * common fof GA
     */
    private String account;

    /**
     * GA Ecommerce traking
     */
    private String storeName;
    @Deprecated
    private Attribute skuAttribute;

    /**
     * GA Outbound clicks tracking
     */
    private Attribute externalLinkAttributeTag;

    private Attribute emailAttributeTag;

	public GoogleSettings(RequestContext context) {
		super(context);
	}

	public String getGoogleAnalytics() {
		return googleAnalytics;
	}

	public final String getSiteMapMetaTag() {
		return siteMapMetaTag;
	}

	public boolean hasSiteMapMetaTag() {
		return siteMapMetaTag != null;
	}

	public boolean hasGoogleAnalytics() {
		return googleAnalytics != null;
	}

	public void setGoogleAnalytics(String s) {
		this.googleAnalytics = s;
	}

	public final void setSiteMapMetaTag(String siteMapMetaTag) {
		this.siteMapMetaTag = siteMapMetaTag;
	}

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getStoreName() {
        return storeName != null ? storeName : Company.getInstance(context).getName();
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Attribute getSkuAttribute() {
        return (Attribute) (skuAttribute == null ? null : skuAttribute.pop());
    }

    public void setSkuAttribute(Attribute skuAttribute) {
        this.skuAttribute = skuAttribute;
    }

    public void setSkuAttributes(Map<ItemType, Attribute> skuAttributes) {
        SkuArrtibutes.remove(context);
        SkuArrtibutes.create(context, skuAttributes);
    }


    public Attribute getExternalLinkAttributeTag() {
        return (Attribute) (externalLinkAttributeTag == null ? null : externalLinkAttributeTag.pop());
    }

    public void setExternalLinkAttributeTag(Attribute externalLinkAttributeTag) {
        this.externalLinkAttributeTag = externalLinkAttributeTag;
    }

    public Attribute getEmailAttributeTag() {
        return (Attribute) (emailAttributeTag == null ? null : emailAttributeTag.pop());
    }

    public void setEmailAttributeTag(Attribute emailAttributeTag) {
        this.emailAttributeTag = emailAttributeTag;
    }

    public Attribute getSkuAttribute(RequestContext context, ItemType itemType) {
        return SkuArrtibutes.getSkuArrtibute(context, itemType).getSkuAttribute();
    }
}
