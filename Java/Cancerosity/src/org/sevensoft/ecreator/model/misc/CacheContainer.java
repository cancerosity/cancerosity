package org.sevensoft.ecreator.model.misc;

/**
 * @author sks 2 Apr 2007 22:07:36
 *
 */
public class CacheContainer {

	private String	cachedContent;
	private int		accessCounter;

	public CacheContainer(String cachedContent) {
		this.cachedContent = cachedContent;
		this.accessCounter = 1;
	}

	public final int getAccessCounter() {
		return accessCounter;
	}

	public final String getCachedContent() {
		accessCounter++;
		return cachedContent;
	}

	public boolean isExpired() {
		return accessCounter >= 25;
	}
}