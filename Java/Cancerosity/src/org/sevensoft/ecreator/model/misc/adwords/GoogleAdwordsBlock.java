package org.sevensoft.ecreator.model.misc.adwords;

import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 16 Mar 2007 06:24:47
 *
 */
@Label("Google adwords")
@Table("blocks_adwords")
@HandlerClass(GoogleAdwordsBlockHandler.class)
public class GoogleAdwordsBlock extends Block {

	private String	googleAdwordsConversionId;

	public GoogleAdwordsBlock(RequestContext context) {
		super(context);
	}

	public GoogleAdwordsBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, 0);
	}

	public final String getGoogleAdwordsConversionId() {
		return googleAdwordsConversionId;
	}

	@Override
	public Object render(RequestContext context) {

		if (!Module.GoogleAdwords.enabled(context)) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		sb.append(new TableTag("ec_adwords"));
		sb.append("<tr><td>");
		sb.append(new GoogleAdwords(googleAdwordsConversionId));
		sb.append("</td></tr></table>");
		return sb.toString();
	}

	public final void setGoogleAdwordsConversionId(String googleAdwordsConversionId) {
		this.googleAdwordsConversionId = googleAdwordsConversionId;
	}

}
