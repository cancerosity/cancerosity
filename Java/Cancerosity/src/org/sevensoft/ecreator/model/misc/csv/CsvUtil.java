package org.sevensoft.ecreator.model.misc.csv;

import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.CsvSpec;
import com.ricebridge.csvman.TrimType;

/**
 * @author sks 5 Aug 2006 18:31:24
 *
 */
public class CsvUtil {

	public static CsvManager getCsvManager(CsvImporter importer) {

		CsvManager csvman = new CsvManager();

		if (importer.getStartRow() > 0) {
			csvman.setStartLine(importer.getStartRow() + 1);
		}

		csvman.setIgnoreEmptyLines(true);
		csvman.setIgnoreBadLines(true);

		CsvSpec csvspec = csvman.getCsvSpec();
		csvspec.setIgnoreEmptyLines(true);
		csvspec.setCloseInputStream(true);
		csvspec.setTrimType(TrimType.Full);

		/*
		 * Set characters used for separators
		 */
		String sep = importer.getSeparator();
		if (sep == null) {
			sep = ",";
		}

		csvspec.setSeparator(sep);
		if (importer.getQuote() == null) {

			csvspec.setUseQuote(false);

		} else {

			csvspec.setUseQuote(true);

			if (importer.getEscape() == null || importer.getEscape().equals(importer.getQuote())) {

				csvspec.setEscape(' ');
				csvspec.setDoubleQuote(true);
				csvspec.setUseEscape(false);

			} else {

				csvspec.setDoubleQuote(false);
				csvspec.setUseEscape(true);
				csvspec.setEscape(importer.getEscape().charAt(0));
			}

			/*
			 * Set quote character, used to enclose records which contain a separator.
			 */
			csvspec.setQuote(importer.getQuote().charAt(0));
		}

		csvman.setCsvSpec(csvspec);

		return csvman;
	}

}
