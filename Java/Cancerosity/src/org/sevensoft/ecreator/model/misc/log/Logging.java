package org.sevensoft.ecreator.model.misc.log;

import org.sevensoft.ecreator.model.system.users.User;

/**
 * @author sks 8 Aug 2006 14:09:20
 *
 */
public interface Logging {

	public String getFullId();

	/**
	 * A name for the entity being logged
	 */
	public String getLogName();

	public void log(String message);

	public void log(User user, String message);
}
