package org.sevensoft.ecreator.model.misc;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHit;
import org.sevensoft.ecreator.model.system.messages.SystemMessage;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sam2 Nov 12, 2003 2:00:43 AM
 */
@Table("settings_misc")
@Singleton
public class MiscSettings extends EntityObject {

	public static MiscSettings getInstance(RequestContext context) {
		return getSingleton(context, MiscSettings.class);
	}

	private int			imageInputs;

	private String		offlineMessage;

	private boolean		hideCompanyDetailsTab;

	/**
	 * Turns on the toolbar when in live mode if logged in
	 */
	@Default("1")
	private boolean		toolbar;

	/**
	 * 
	 */
	private boolean		offline, printerFriendly;

	/**
	 * disable content editor for speed
	 */
	private boolean		simpleEditor;

	private boolean		jimMode;

	private boolean		duplicationCheck;

	private List<String>	ipBlacklist;

	/**
	 * 
	 */
	private int			sessionHourLimit;

	/**
	 * change listing names to title case
	 */
	private boolean		titleCase;

	/**
	 * Hide the site trail
	 */
	private boolean		hideTrail;

	private boolean		favicon;

    private String htmlAttr;

	public MiscSettings(RequestContext context) {
		super(context);
	}

	public int getImageInputs() {

		if (imageInputs < 1) {
			return 1;
		}

		if (imageInputs > 8) {
			return 8;
		}

		return imageInputs;
	}

	public final List<String> getIpBlacklist() {
		if (ipBlacklist == null) {
			ipBlacklist = new ArrayList();
		}
		return ipBlacklist;
	}

    public String getHtmlAttr() {
        return htmlAttr;
    }

    public String getHtmlAttrString() {
        return getHtmlAttr() == null ? "" : getHtmlAttr();
    }

    public String getOfflineMessage() {
		return offlineMessage == null ? "Our site is under construction" : offlineMessage;
	}

	public final int getSessionHourLimit() {
		return sessionHourLimit;
	}

	public boolean isAdvancedMode() {
		return !jimMode;
	}

	public boolean isDuplicationCheck() {
		return duplicationCheck;
	}

	public boolean isFavicon() {
		return favicon;
	}

	public boolean isHideCompanyDetailsTab() {
		return hideCompanyDetailsTab;
	}

	public boolean isHideTrail() {
		return hideTrail;
	}

	public boolean isHtmlEditor() {
		return !simpleEditor;
	}

	public boolean isJimMode() {
		return jimMode;
	}

	public boolean isOffline() {
		return offline;
	}

	public boolean isOnline() {
		return !offline;
	}

	public boolean isPrinterFriendly() {
		return printerFriendly;
	}

	public boolean isShowTrail() {
		return !isHideTrail();
	}

	public boolean isSimpleEditor() {
		return simpleEditor;
	}

	public boolean isTitleCase() {
		return titleCase;
	}

	public boolean isToolbar() {
		return toolbar;
	}

	public void setDuplicationCheck(boolean duplicationWarning) {
		this.duplicationCheck = duplicationWarning;
	}

	/**
	 * 
	 */
	public void setFavicon(boolean b) {
		this.favicon = b;
	}

	public void setHideCompanyDetailsTab(boolean hideCompanyDetailsTab) {
		this.hideCompanyDetailsTab = hideCompanyDetailsTab;
	}

	public void setHideTrail(boolean hideTrail) {
		this.hideTrail = hideTrail;
	}

	public void setImageInputs(int imageUploadInputs) {
		this.imageInputs = imageUploadInputs;
	}

	public final void setIpBlacklist(List<String> ipBlacklist) {
		this.ipBlacklist = ipBlacklist;
	}

    public void setHtmlAttr(String htmlAttr) {
        this.htmlAttr = htmlAttr;
    }

    public void setOffline(boolean b) {

		if (offline == b)
			return;

		this.offline = b;
		if (b) {

			new SystemMessage(context, "Site turned offline");

		} else {

			new SystemMessage(context, "Site turned online");

		}
	}

	public void setOfflineMessage(String offlineMessage) {
		this.offlineMessage = offlineMessage;
	}

	public void setOnline(boolean b) {
		setOffline(!b);
	}

	public void setPrinterFriendly(boolean printerFriendly) {
		this.printerFriendly = printerFriendly;
	}

	public final void setSessionHourLimit(int sessionHourLimit) {
		this.sessionHourLimit = sessionHourLimit;
	}

	public void setSimpleEditor(boolean contentEditorDisabled) {
		this.simpleEditor = contentEditorDisabled;
	}

	public void setSimpleMode(boolean jimMode) {
		this.jimMode = jimMode;
	}

	public void setTitleCase(boolean titleCase) {
		this.titleCase = titleCase;
	}

	public void setToolbar(boolean toolbar) {
		this.toolbar = toolbar;
	}

	@Override
	protected void singletonInit(RequestContext context) {
		this.imageInputs = 1;
		this.toolbar = true;
		this.printerFriendly = true;
	}

	public String specialTags(String string) {

		if (string == null)
			return null;

		string = string.replace("{live_classname}", "0");
		string = string.replace("{date}", new Date().toString("dd/MM/yyyy"));
		string = string.replace("{date_time}", new DateTime().toString("dd/MM/yyyy HH:mm"));

		return string;
	}

	/**
	 * 
	 */
	public void resetSessions() {
		SimpleQuery.delete(context, SiteHit.class);
	}

}