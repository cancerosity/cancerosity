package org.sevensoft.ecreator.model.misc.currencies;

import java.util.List;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 26-Mar-2006 23:58:46
 *
 */
@Table("currencies")
public class Currency extends EntityObject implements Selectable {

	public static Money convert(Money money, Currency currency) {

//		logger.fine("[Currency] converting money=" + money + ", currency=" + currency);

		if (currency == null) {
			return money;
		}

		if (currency.isDefault()) {
			return money;
		}

		Money conversion = currency.convert(money);
//		logger.fine("[Currency] conversion=" + conversion);
		return conversion;
	}

	public static List<Currency> get(RequestContext context) {
		Query q = new Query(context, "select * from # order by name");
		q.setTable(Currency.class);
		return q.execute(Currency.class);
	}

	/**
	 * Returns the currency set as default
	 */
	public static Currency getDefault(RequestContext context) {
		return SimpleQuery.get(context, Currency.class, "dfault", 1);
	}

	/**
	 * 
	 */
	private String	symbol;

	/**
	 * The name of the currency
	 */
	private String	name;

	private double	rate;

	private boolean	dfault;

    private String  code;

	public Currency(RequestContext context) {
		super(context);
	}

	public Currency(RequestContext context, String name, String symbol, double i) {
		super(context);

		this.name = name;
		this.symbol = symbol;
		this.rate = i;

		save();
	}

	public Money convert(Money money) {

		if (money == null) {
			return null;
		}

		return money.multiply(rate);
	}

	@Override
	public synchronized boolean delete() {

		if (isDefault()) {
			return false;
		}

		return super.delete();
	}

    public String getCode() {
        return code;
    }

	public String getLabel() {
		return getName();
	}

	public String getName() {
		return name;
	}

	public double getRate() {
		return rate;
	}

	public String getSymbol() {
		return symbol;
	}

	public String getValue() {
		return getIdString();
	}

	public boolean isDefault() {
		return dfault;
	}

	@Override
	protected void schemaInit(RequestContext context) {

		Query q = new Query(context, "select count(*) from # where dfault=1");
		q.setTable(Currency.class);
		if (q.getInt() == 0) {
			Currency currency = new Currency(context, "GBP", "\243", 1);
			currency.setDefault();
			currency.save();
		}
	}

	public void setDefault() {

		/*
		 * Remove all other defaults first
		 */
		new Query(context, "update # set dfault=0").setTable(Currency.class).run();
		this.dfault = true;

		save();
	}

    public void setCode(String code) {
        this.code = code;
    }

	public void setName(String name) {
		this.name = name;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

}
