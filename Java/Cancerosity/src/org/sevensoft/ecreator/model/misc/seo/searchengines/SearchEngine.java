package org.sevensoft.ecreator.model.misc.seo.searchengines;

import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.ecreator.model.misc.seo.searchengines.google.GoogleRequester;
import org.sevensoft.ecreator.model.misc.seo.searchengines.yahoo.YahooRequester;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 11 Nov 2006 16:18:08
 *
 */
public enum SearchEngine {

	YahooUK() {

		@Override
		public SearchRequester getRequester(RequestContext context) throws SearchException {

			String apiId = Seo.getInstance(context).getYahooWebSearchAppId();
			if (apiId == null && Config.getInstance(context).isDemo()) {
				apiId = YahooRequester.SevensoftApiId;
			}

			return new YahooRequester(apiId);
		}

	},

	Google() {

		@Override
		public SearchRequester getRequester(RequestContext context) {
			return new GoogleRequester();
		}

	};

	public abstract SearchRequester getRequester(RequestContext context) throws SearchException;

}
