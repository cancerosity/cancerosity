package org.sevensoft.ecreator.model.misc.location;

import org.sevensoft.commons.gaia.UkTowns;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 3 Dec 2006 15:38:39
 *
 */
public class TownsTag {

	private boolean			autoSubmit;
	private final RequestContext	context;
	private String			any;
	private boolean			optional;

	public TownsTag(RequestContext context) {
		this.context = context;
	}

	public final void setAny(String any) {
		this.any = any;
	}

	public void setAutoSubmit(boolean autoSubmit) {
		this.autoSubmit = autoSubmit;
	}

	public void setOptional(boolean optional) {
		this.optional = optional;
	}

	public String toString() {

		SelectTag tag = new SelectTag(context, "town");

		if (autoSubmit)
			tag.setAutoSubmit();

		if (optional) {
			if (any == null)
				any = "-Any-";
			tag.setAny(any);
		}

		tag.addOptions(UkTowns.getTowns());
		return tag.toString();
	}

}
