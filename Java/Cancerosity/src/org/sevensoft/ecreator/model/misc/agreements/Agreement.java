package org.sevensoft.ecreator.model.misc.agreements;

import java.util.List;

import org.sevensoft.ecreator.iface.frontend.agreements.AgreementHandler;
import org.sevensoft.ecreator.model.accounts.registration.RegistrationModule;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.JavascriptLinkTag;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 19-Oct-2005 23:26:45
 * 
 */
@Table("items_agreements")
public class Agreement extends EntityObject implements Selectable, Logging {

	public static List<Agreement> get(RequestContext context) {

		Query q = new Query(context, "select * from # order by title");
		q.setTable(Agreement.class);
		return q.execute(Agreement.class);
	}

	private String	title, content;

	private String	confirmButtonText, rejectButtonText;

	/*
	 * Show as a tickable check box rather than as a page
	 */
	private boolean	inline;

	/**
	 * Use this text in the line. Replace [link] with a link to the popup
	 */
	private String	inlineText;

	public Agreement(RequestContext context) {
		super(context);
	}

	public Agreement(RequestContext context, String title) {
		super(context);
		this.title = title;
		save();
	}

	@Override
	public synchronized boolean delete() {

		/*
		 * Remove from items
		 */
		new Query(context, "update # set agreement=0 where agreement=?").setTable(Item.class).setParameter(this).run();

		// remove from registration modules
		new Query(context, "update # set agreement=0 where agreement=?").setTable(RegistrationModule.class).setParameter(this).run();

		return super.delete();
	}

	public final String getConfirmButtonText() {
		return confirmButtonText == null ? "Continue" : confirmButtonText;
	}

	public String getContent() {
		return content;
	}

	public String getInlineText() {
		return inlineText == null ? "Please tick the box to agree to our terms and conditions. To view our terms [link]." : inlineText;
	}

	public String getInlineTextRendered() {

		JavascriptLinkTag link = new JavascriptLinkTag(context, new Link(AgreementHandler.class, null, "agreement", this), "Click here", 400, 550);
		link.setScrollbars(true);

		String string = getInlineText();
		string = string.replace("[link]", link.toString());
		return string;
	}

	public String getLabel() {
		return getTitle();
	}

	public final String getRejectButtonText() {
		return rejectButtonText == null ? "Cancel" : rejectButtonText;
	}

	public String getTitle() {
		return title;
	}

	public String getValue() {
		return getIdString();
	}

	public boolean hasContent() {
		return content != null;
	}

	public final boolean isInline() {
		return inline;
	}

	public boolean isPage() {
		return !isInline();
	}

	public final void setConfirmButtonText(String confirmButtonText) {
		this.confirmButtonText = confirmButtonText;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public final void setInline(boolean inline) {
		this.inline = inline;
	}

	public final void setInlineText(String inlineText) {
		this.inlineText = inlineText;
	}

	public final void setRejectButtonText(String rejectButtonText) {
		this.rejectButtonText = rejectButtonText;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<LogEntry> getLogEntries() {
		return LogEntry.get(context, this);
	}

	public String getLogId() {
		return getFullId();
	}

	public String getLogName() {
		return "Agreement #" + getId();
	}

	public void log(String message) {
		new LogEntry(context, this, message);
	}

	public void log(User user, String message) {
		new LogEntry(context, this, user, message);
	}

}
