package org.sevensoft.ecreator.model.misc.sitemap;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.extras.sitemap.SiteMapHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 03-Apr-2006 11:17:30
 *
 */
public class SitemapMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {
		return super.link(context, params, new Link(SiteMapHandler.class), "link_sitemap");
	}

	public Object getRegex() {
		return "sitemap";
	}
}