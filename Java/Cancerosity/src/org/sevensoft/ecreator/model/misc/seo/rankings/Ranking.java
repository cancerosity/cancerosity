package org.sevensoft.ecreator.model.misc.seo.rankings;

/**
 * @author sks 11 Nov 2006 16:40:02
 *
 */
public class Ranking {

	private int		position;
	private String	url;

	public Ranking(int position, String url) {
		this.position = position;
		this.url = url;
	}

	public int getPosition() {
		return position;
	}

	public String getUrl() {
		return url;
	}

}
