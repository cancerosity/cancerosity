package org.sevensoft.ecreator.model.misc.sitemap;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.sevensoft.ecreator.iface.frontend.extras.sitemap.GoogleSiteMapHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.search.CategorySearcher;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

import java.text.NumberFormat;

/**
 * @author sks 10-Jun-2005 00:27:31
 */
public class GoogleSiteMap {

    public static Document getSitemap(RequestContext context, int start) {

        Document doc = new Document();

        Namespace ns = Namespace.getNamespace("http://www.sitemaps.org/schemas/sitemap/0.9");
        Namespace xsi = Namespace.getNamespace("xsi", "http://www.sitemaps.org/schemas/sitemap/0.9");

        Element urlset = new Element("urlset", ns);
        urlset.addNamespaceDeclaration(xsi);
        //		urlset.setAttribute("schemaLocation", "http://www.google.com/schemas/sitemap/0.84/sitemap.xsd", xsi);
        doc.addContent(urlset);

        // homepage only on start == 0
        if (start == 0) {

            Element url = new Element("url", ns);

            url.addContent(new Element("loc", ns).setText(Config.getInstance(context).getUrl()));
            url.addContent(new Element("changefreq", ns).setText("Daily"));
            url.addContent(new Element("priority", ns).setText(String.valueOf("1")));

            urlset.addContent(url);
        }

        // categories
        CategorySearcher cs = new CategorySearcher(context);
        cs.setStart(start);
        cs.setVisible(true);
        cs.setLimit(200);
        for (Category category : cs.getCategories()) {

            Element url = new Element("url", ns);
            urlset.addContent(url);

            url.addContent(new Element("loc", ns).setText(category.getUrl()));
            url.addContent(new Element("changefreq", ns).setText("weekly"));
            url.addContent(new Element("lastmod", ns).setText(category.getDateUpdated().toIso8601()));
            double priority = Math.pow(0.9, category.getParentCount() + 1);
            url.addContent(new Element("priority", ns).setText(NumberFormat.getNumberInstance().format(priority)));
        }

        // items
        ItemSearcher searcher = new ItemSearcher(context);
        searcher.setStatus("live");
        searcher.setStart(start);
        searcher.setLimit(200);

        for (Item item : searcher.getItems()) {

            Element url = new Element("url", ns);
            urlset.addContent(url);

            url.addContent(new Element("loc", ns).setText(item.getUrl()));
            url.addContent(new Element("changefreq", ns).setText("Monthly"));
            url.addContent(new Element("lastmod", ns).setText(item.getDateUpdated().toIso8601()));
            url.addContent(new Element("priority", ns).setText("0.8"));
        }

        return doc;
    }

    public static Document getSitemaps(RequestContext context) {

        Document doc = new Document();

        Namespace ns = Namespace.getNamespace("http://www.sitemaps.org/schemas/sitemap/0.9");
        Namespace xsi = Namespace.getNamespace("xsi", "http://www.sitemaps.org/schemas/sitemap/0.9");

        Element root = new Element("sitemapindex", ns);
        root.addNamespaceDeclaration(xsi);
        //		root.setAttribute("schemaLocation", "http://www.google.com/schemas/sitemap/0.84/sitemap.xsd", xsi);
        doc.addContent(root);

        int categories = SimpleQuery.count(context, Category.class, "hidden", false);

        Query q = new Query(context, "select count(*) from # i join # it on i.itemType=it.id "
                + "where i.status=? and it.hidden=0 and it.modules not like ? and it.searchable=1");
        q.setTable(Item.class);
        q.setTable(ItemType.class);
        q.setParameter("live");
        q.setParameter("%" + ItemModule.Account.name() + "%");
        int items = q.getInt();

        String base = Config.getInstance(context).getUrl();

        int total = (categories > items ? categories : items);
        for (int n = 0; n < total; n = n + 200) {

            Element sitemap = new Element("sitemap", ns);
            root.addContent(sitemap);

            Element loc = new Element("loc", ns);
            sitemap.addContent(loc);
            loc.setText(base + "/" + new Link(GoogleSiteMapHandler.class, "sitemap", "start", n));
        }

        return doc;
    }
}
