package org.sevensoft.ecreator.model.misc.agreements;

/**
 * @author sks 4 Jul 2006 16:25:59
 *
 */
public interface AgreementOwner {

	public Agreement getAgreement();

	public boolean hasAgreement();

	public boolean hasExternalAgreement();

	public boolean hasInlineAgreement();

	public void setAgreement(Agreement agreement);

}
