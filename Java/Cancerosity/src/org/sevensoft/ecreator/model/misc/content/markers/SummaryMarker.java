package org.sevensoft.ecreator.model.misc.content.markers;

import java.util.Map;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IBasketLineMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.ICategoryMarker;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class SummaryMarker extends MarkerHelper implements IItemMarker, ICategoryMarker, IBasketLineMarker {

	public Object generate(RequestContext context, Map<String, String> params, BasketLine line) {

		if (line.hasItem()) {
			return generate(context, params, line.getItem(), null, 0, 0);
		} else {
			return null;
		}
	}

	public Object generate(RequestContext context, Map<String, String> params, Category category) {
		return generate(context, category.hasSummary() ? category.getSummary() : category.getContentBlock().getContentStripped(), category.getUrl(), params);
	}

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
		return generate(context, item.hasSummary() ? item.getSummary() : item.getContentStripped(), item.getUrl(), params);
	}

	private Object generate(RequestContext context, String content, String url, Map<String, String> params) {

		if (content == null) {
			return null;
		}

		int max;
		if (params.containsKey("max")) {
			max = Integer.parseInt(params.get("max").trim());
		} else {
			max = 200;
		}

		if (!params.containsKey("class")) {
			params.put("class", "summary");
		}

		String link = params.get("link");

		content = StringHelper.toSnippet(content, max, "...");

		if (link != null) {
			content = content + " " + new LinkTag(url, link);
		}

		return super.string(context, params, content);
	}

	public Object getRegex() {
		return "summary";
	}

}
