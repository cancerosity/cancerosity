package org.sevensoft.ecreator.model.misc.sitemap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 02-Nov-2005 09:46:25
 * 
 */
public class SiteMapPage implements Iterable<SiteMapPage> {

	private List<SiteMapPage>	children;
	private String		name, url;

	public SiteMapPage() {
		this.children = new ArrayList<SiteMapPage>();
	}

	public SiteMapPage(String name, Class<? extends FrontendHandler> clazz) {
		this(name, new Link(clazz));
	}

	public SiteMapPage(String name, Link link) {
		this(name, link.toString());
	}

	public SiteMapPage(String name, String url) {
		this.children = new ArrayList<SiteMapPage>();
		this.name = name;
		this.url = url;
	}

	public SiteMapPage add(SiteMapPage node) {
		this.children.add(node);
		return node;
	}

	public SiteMapPage add(String name, Class<? extends FrontendHandler> clazz) {
		return add(new SiteMapPage(name, clazz));
	}

	public SiteMapPage add(String name, Link link) {
		return add(new SiteMapPage(name, link));
	}

	public SiteMapPage add(String name, String url) {
		return add(new SiteMapPage(name, url));
	}

	public List<SiteMapPage> getChildren() {
		return children;
	}

	public String getName() {
		return name;
	}

	public String getUrl() {
		return url;
	}

	public boolean hasChildren() {
		return children.size() > 0;
	}

	public Iterator<SiteMapPage> iterator() {
		return children.iterator();
	}

}
