package org.sevensoft.ecreator.model.misc.location.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class LocationMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		if (params.containsKey("item")) {

			if (!item.hasLocation()) {
				logger.fine("[LocationMarker] item has no location");
				return null;
			}
			
			return super.string(context, params, item.getLocation());
		}

		if (location == null) {
			logger.fine("[LocationMarker] no location present");
			return null;
		}

		return super.string(context, params, location);

	}

	@Override
	public String getDescription() {
		return "Shows the location of the current search, active location or logged in account.";
	}

	public Object getRegex() {
		return "location";
	}

}
