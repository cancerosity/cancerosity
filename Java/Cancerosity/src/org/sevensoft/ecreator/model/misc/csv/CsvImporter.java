package org.sevensoft.ecreator.model.misc.csv;

import com.ricebridge.csvman.CsvManager;

/**
 * @author sks 5 Aug 2006 18:30:24
 *
 */
public interface CsvImporter {

	public CsvManager getCsvManager();

	public String getEscape();

	public String getQuote();

	public String getSeparator();

	public int getStartRow();

	public void setEscape(String escape);

	public void setQuote(String quote);

	public void setSeparator(String separator);

	public void setStartRow(int startRow);
}
