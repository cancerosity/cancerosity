package org.sevensoft.ecreator.model.misc.seo.searchengines;

import org.sevensoft.jeezy.http.ObjectUtil;

/**
 * @author sks 17 Nov 2006 20:43:36
 *
 */
public class SearchResult {

	private final String	title;
	private final String	url;
	private final String	snippet;

	public SearchResult(String title, String url, String snippet) {
		this.title = title;
		this.url = url;
		this.snippet = snippet;
	}

	public String getSnippet() {
		return snippet;
	}

	public String getTitle() {
		return title;
	}

	public String getUrl() {
		return url;
	}

	@Override
	public String toString() {
		return ObjectUtil.fieldsToString(this);
	}

}
