package org.sevensoft.ecreator.model.misc.content.clouds;

import org.sevensoft.ecreator.iface.admin.misc.content.TagCloudBlockHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 15 Mar 2007 08:18:45
 *
 */
@Table("boxes_tagcloud")
@Label("Tag cloud")
@HandlerClass(TagCloudBlockHandler.class)
public class TagCloudBox extends Box {

	private int	limit;

	public TagCloudBox(RequestContext context) {
		super(context);
	}

	public TagCloudBox(RequestContext context, String panel) {
		super(context, panel);

		this.limit = 20;
		save();
	}

	@Override
	protected String getCssIdDefault() {
		return "tagcloud";
	}

	public final int getLimit() {
		return limit < 4 ? 20 : limit;
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	@Override
	public String render(RequestContext context) {

		StringBuilder sb = new StringBuilder();
		sb.append(getTableTag());
		sb.append("<tr><td>");

		sb.append(new TagCloud(context, getLimit()));

		sb.append("</td></tr>");
		sb.append("</table>");
		return sb.toString();
	}

	public final void setLimit(int limit) {
		this.limit = limit;
	}

}
