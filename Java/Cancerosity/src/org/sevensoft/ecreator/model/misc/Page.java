package org.sevensoft.ecreator.model.misc;


/**
 * @author sks 2 Feb 2007 19:12:16
 *
 */
public interface Page {

	/**
	 * 
	 */
	public String getName();
	
	public String getUrl();

}
