package org.sevensoft.ecreator.model.misc.seo.searchengines;


/**
 * @author sks 17 Nov 2006 21:35:55
 *
 */
public class SearchException extends Exception {

	public SearchException() {
	}

	public SearchException(String message) {
		super(message);
	}

	public SearchException(Throwable cause) {
		super(cause);
	}

	public SearchException(String message, Throwable cause) {
		super(message, cause);
	}

}
