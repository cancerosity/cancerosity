package org.sevensoft.ecreator.model.misc.adwords;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 16 Mar 2007 06:30:50
 *
 */
@Path("admin-boxes-adwords.do")
public class GoogleAdwordsBoxHandler extends BoxHandler {

	private GoogleAdwordsBox	box;
	private String			googleAdwordsConversionId;

	public GoogleAdwordsBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {
		box.setGoogleAdwordsConversionId(googleAdwordsConversionId);
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Adwords account"));
		sb.append(new AdminRow("Google adwords conversion ID", "This is your account as provided by google", new TextTag(context,
				"googleAdwordsConversionId", box.getGoogleAdwordsConversionId(), 30)));
		sb.append("</table>");
	}

}
