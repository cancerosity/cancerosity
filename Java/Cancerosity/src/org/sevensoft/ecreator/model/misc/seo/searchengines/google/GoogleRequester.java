package org.sevensoft.ecreator.model.misc.seo.searchengines.google;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom.JDOMException;
import org.sevensoft.ecreator.model.misc.seo.searchengines.SearchRequester;
import org.sevensoft.ecreator.model.misc.seo.searchengines.SearchResult;

/**
 * @author sks 17 Nov 2006 20:41:23
 *
 */
public class GoogleRequester extends SearchRequester {

	public static final String	SevensoftKey	= "P1OgVlRQFHIgxuyENh7PN0hABfgnqLD8";

	@Override
	public List<SearchResult> getResults(String query) throws IOException, JDOMException {

		logger.fine("[GoogleRequester] query=" + query);

		List<SearchResult> results = new ArrayList();

		//		for (int n = 0; n <= 90; n += 10) {
		//
		//			GoogleSearch s = new GoogleSearch();
		//			s.setKey(SevensoftKey);
		//			s.setQueryString(query);
		//			s.setStartResult(0);
		//			s.setMaxResults(10);
		//
		//			try {
		//
		//				GoogleSearchResult r = s.doSearch();
		//
		//				for (GoogleSearchResultElement element : r.getResultElements()) {
		//
		//					String url = element.getURL();
		//					String snippet = element.getSnippet();
		//					String title = element.getTitle();
		//
		//					results.add(new SearchResult(title, url, snippet));
		//				}
		//
		//			} catch (GoogleSearchFault e) {
		//				throw new IOException(e.toString());
		//			}
		//
		//		}

		return results;
	}
}
