package org.sevensoft.ecreator.model.misc.sessions;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 21 Aug 2006 16:39:37
 *
 */
public abstract class SessionSupport extends EntityObject {

	private String	sessionId;

	protected SessionSupport(RequestContext context) {
		super(context);
	}

	protected SessionSupport(RequestContext context, String sessionId) {
		super(context);
		this.sessionId = sessionId;
	}

}
