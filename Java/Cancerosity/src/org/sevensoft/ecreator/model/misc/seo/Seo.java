package org.sevensoft.ecreator.model.misc.seo;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Transformer;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 26-Jul-2005 09:41:44
 * 
 */
@Table("settings_seo")
@Singleton
public class Seo extends EntityObject {

	/**
	 * Returns a string with invalid characters removed and spaces replaced with a dash (-)  for use in search engine friendly urls
	 * 
	 */
	public static String getFriendlyUrl(String string) {

		if (string == null) {
			return null;
		}

		string = string.replaceAll("[^a-zA-Z0-9\\s\\-]", "");
		string = string.replaceAll("\\s", "-");
		string = string.replaceAll("-{2,}", "-");
		string = string.replaceAll("-$", "");
		string = string.replaceAll("^-", "");

		return string.toLowerCase();
	}

	public static Seo getInstance(RequestContext context) {
		return getSingleton(context, Seo.class);
	}

	public static void resetFriendlyUrls(RequestContext context) {

		new Query(context, "update # set friendlyUrl=null").setTable(Item.class).run();
		new Query(context, "update # set friendlyUrl=null").setTable(Category.class).run();

	}

	/**
	 * When creating items the site will auto suggest and complete keywords
	 */
	private boolean		autoKeywordCompletion;

	/**
	 * I have called this field keywordsDefault to retain compatibiltiy with older versions where keywords 
	 * was a boolean field so I cannot re use that. How thoughtful of me, I'm quite surprised.
	 */
	private String		keywordsDefault;

	private boolean		autoContentLinking;

	/**
	 * Automatically highlight keywords (defined in highlightedKeywords field) with b tags to help on seo (gimmick!)
	 */
	private boolean		autoKeywordHighlighting;

	/**
	 * 
	 */
	private List<String>	highlightedKeywords;

	private boolean		autoContentLinkingOverride, overrideTags;

	/**
	 * 
	 */
	private int			autoContentLinkingCategoryDepth;

	/**
	 * Use suggestions
	 */
	private boolean		suggestions;

	/**
	 * Allows users to set their own friendly urls
	 */
	private boolean		overrideFriendlyUrls;

	private String		titleTag, descriptionTag;

	private String		yahooWebSearchAppId;

	/**
	 * Enable the seo toolbar
	 */
	private boolean		toolbar;

	private String		friendlyUrlSeparator;

	private boolean		keywordDensity;

	public Seo(RequestContext context) {
		super(context);
	}

	public final int getAutoContentLinkingCategoryDepth() {
		return autoContentLinkingCategoryDepth;
	}

	public String getDescriptionTag() {
		return descriptionTag == null ? "[name] - [category] - [site]" : descriptionTag;
	}

	public String getDescriptionTagRendered(Meta meta) {

		if (!overrideTags || meta.getDescriptionTag() == null) {
			return renderTags(getDescriptionTag(), meta);
		} else {
			return renderTags(meta.getDescriptionTag(), meta);
		}
	}

	/**
	 * 
	 */
	public String getFriendlyUrlSeparator() {

		if (friendlyUrlSeparator == null) {
			return "-";
		}
		if (friendlyUrlSeparator.equals(" ")) {
			return "-";
		}
		return friendlyUrlSeparator;
	}

	public List<String> getHighlightedKeywords() {
		if (highlightedKeywords == null) {
			highlightedKeywords = new ArrayList();
		}
		return highlightedKeywords;
	}

	public String getKeywords() {
		return keywordsDefault == null ? "[site], [name]" : keywordsDefault;
	}

	public String getKeywordsRendered(Meta meta) {

		if (!overrideTags || meta.getKeywords() == null) {
			return renderTags(getKeywords(), meta);
		} else {
			return renderTags(meta.getKeywords(), meta);
		}
	}

	public String getTitleTag() {
		return titleTag == null ? "[name] - [category] - [site]" : titleTag;
	}

	public String getTitleTagRendered(Meta meta) {

		if (!overrideTags || meta.getTitleTag() == null) {
			return renderTags(getTitleTag(), meta);
		} else {
			return renderTags(meta.getTitleTag(), meta);
		}
	}

	public String getYahooWebSearchAppId() {
		return yahooWebSearchAppId;
	}

	public boolean hasYahooWebSearchAppId() {
		return yahooWebSearchAppId != null;
	}

	public boolean isAutoContentLinking() {
		return autoContentLinking;
	}

	public boolean isAutoContentLinkingOverride() {
		return autoContentLinkingOverride && autoContentLinking;
	}

	public boolean isAutoKeywordCompletion() {
		return false;//autoKeywordCompletion && isOverrideTags();
	}

	public boolean isAutoKeywordHighlighting() {
		return autoKeywordHighlighting;
	}

	public boolean isKeywordDensity() {
		return keywordDensity;
	}

	public boolean isOverrideFriendlyUrls() {
		return overrideFriendlyUrls;
	}

	public boolean isOverrideTags() {
		return overrideTags;
	}

	public final boolean isSuggestions() {
		return suggestions;
	}

	public boolean isToolbar() {
		return toolbar;
	}

	/**
	 * Replace tags
	 */
	private String renderTags(String string, Meta meta) {

		string = string.replace("[name]", meta.getName());
		string = string.replace("[site]", Company.getInstance(context).getName());

		if (meta instanceof Item) {

			Item item = (Item) meta;

			if (string.contains("[category]")) {

				if (item.hasCategory()) {

					Category category = item.getCategory();
					if (category != null) {
						String categoryName = category.getName();
						if (categoryName != null) {
							string = string.replace("[category]", categoryName);
						}
					}

				}
			}

			if (string.contains("[allcategories]")) {

				List<Category> categories = item.getCategories();
				List<String> strings = CollectionsUtil.transform(categories, new Transformer<Category, String>() {

					public String transform(Category obj) {
						return obj.getName();
					}
				});
				string = string.replace("[allcategories]", StringHelper.implode(strings, " "));
			}
		}

		string = string.replace("[category]", "");
		string = string.replace("[allcategories]", "");

		return string;
	}

	public void setAutoContentLinking(boolean autoContentLinking) {
		this.autoContentLinking = autoContentLinking;
	}

	public final void setAutoContentLinkingCategoryDepth(int autoContentLinkingCategoryDepth) {
		this.autoContentLinkingCategoryDepth = autoContentLinkingCategoryDepth;
	}

	public void setAutoContentLinkingOverride(boolean b) {
		this.autoContentLinkingOverride = b;
	}

	public void setAutoKeywordHighlighting(boolean autoKeywordHighlighting) {
		this.autoKeywordHighlighting = autoKeywordHighlighting;
	}

	public void setDescriptionTag(String descriptionTag) {
		this.descriptionTag = descriptionTag;
	}

	public void setFriendlyUrlSeparator(String friendlyUrlSeparator) {
		this.friendlyUrlSeparator = friendlyUrlSeparator;
	}

	public void setHighlightedKeywords(List<String> highlightedKeywords) {
		this.highlightedKeywords = highlightedKeywords;
	}

	public void setKeywordDensity(boolean keywordDensity) {
		this.keywordDensity = keywordDensity;
	}

	public void setKeywords(String keywords) {
		this.keywordsDefault = keywords;
	}

	public void setOverrideFriendlyUrls(boolean overrideFriendlyUrls) {
		this.overrideFriendlyUrls = overrideFriendlyUrls;
	}

	public void setOverrideTags(boolean overrideTags) {
		this.overrideTags = overrideTags;
	}

	public final void setSuggestions(boolean useSuggestions) {
		this.suggestions = useSuggestions;
	}

	public void setTitleTag(String titleTag) {
		this.titleTag = titleTag;
	}

	public void setToolbar(boolean toolbar) {
		this.toolbar = toolbar;
	}

	public void setYahooWebSearchAppId(String yahooWebSearchAppId) {
		this.yahooWebSearchAppId = yahooWebSearchAppId;
	}

	@Override
	protected void singletonInit(RequestContext context) {
		this.autoContentLinking = true;

		save();
	}
}
