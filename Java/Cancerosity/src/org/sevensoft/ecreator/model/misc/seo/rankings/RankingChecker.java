package org.sevensoft.ecreator.model.misc.seo.rankings;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Logger;

import org.jdom.JDOMException;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.model.misc.seo.searchengines.SearchEngine;
import org.sevensoft.ecreator.model.misc.seo.searchengines.SearchException;
import org.sevensoft.ecreator.model.misc.seo.searchengines.SearchResult;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 11 Nov 2006 16:15:01
 * 
 * Get the position of your site for particular phrases
 *
 */
public class RankingChecker {

	private static final Logger	logger	= Logger.getLogger("ecreator");

	public MultiValueMap<SearchEngine, Ranking> getRankings(RequestContext context, String query, int max) throws IOException, JDOMException {

		Config config = Config.getInstance(context);
		String domain = config.getDomain().toLowerCase();

		domain = "peakdistrict";

		MultiValueMap<SearchEngine, Ranking> rankings = new MultiValueMap(new LinkedHashMap());

		SearchEngine[] engines = SearchEngine.values();
		for (SearchEngine engine : engines) {

			logger.fine("[RankingChecker] search engine=" + engine);

			try {

				List<SearchResult> results = engine.getRequester(context).getResults(query);
				logger.fine("[RankingChecker] results=" + results);

				int n = 1;
				for (SearchResult result : results) {

					logger.fine("[RankingChecker] checking result url=" + result.getUrl());
					if (result.getUrl().toLowerCase().contains(domain)) {

						logger.fine("[RankingChecker] ranking found n=" + n);
						rankings.put(engine, new Ranking(n, result.getUrl()));

					}

					n++;
				}

			} catch (IOException e) {
				e.printStackTrace();

			} catch (SearchException e) {
				e.printStackTrace();
			}

		}

		return rankings;
	}
}
