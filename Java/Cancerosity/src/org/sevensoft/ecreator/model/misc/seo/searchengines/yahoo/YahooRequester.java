package org.sevensoft.ecreator.model.misc.seo.searchengines.yahoo;

import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.httpc.HttpMethod;
import org.sevensoft.ecreator.model.misc.seo.searchengines.SearchException;
import org.sevensoft.ecreator.model.misc.seo.searchengines.SearchRequester;
import org.sevensoft.ecreator.model.misc.seo.searchengines.SearchResult;

/**
 * @author sks 22 Jul 2006 12:53:01
 *
 */
public class YahooRequester extends SearchRequester {

	private final static String	Url			= "http://api.search.yahoo.com/WebSearchService/V1/webSearch";
	public final static String	SevensoftApiId	= "sevensoft";

	private int				similar_ok;
	private String			apiId;

	public YahooRequester(String apiId) throws SearchException {
		this.apiId = apiId;
		this.similar_ok = 1;
		if (apiId == null)
			throw new SearchException("No Yahoo Application ID set");
	}

	public List<SearchResult> getResults(String query) throws IOException, JDOMException {

		logger.fine("[YahooRequester] query=" + query + ", apiId=" + apiId);

		URL url = new URL(Url);
		HttpClient hc = new HttpClient(url, HttpMethod.Get);
		hc.setParameter("query", query);
		hc.setParameter("region", "uk");
		hc.setParameter("results", 100);
		hc.setParameter("appid", apiId);
		hc.setParameter("format", "html");
		hc.setParameter("similar_ok", similar_ok);
		hc.connect();

		String response = hc.getResponseString();

		Document doc = new SAXBuilder().build(new StringReader(response));

		Element root = doc.getRootElement();
		List<Element> elements = root.getChildren();

		List<SearchResult> results = new ArrayList();
		for (Element element : elements) {

			String displayUrl = element.getChildTextTrim("DisplayUrl", element.getNamespace());
			String summary = element.getChildTextTrim("Summary", element.getNamespace());
			String title = element.getChildTextTrim("Title", element.getNamespace());

			results.add(new SearchResult(title, displayUrl, summary));
		}

		return results;
	}
}
