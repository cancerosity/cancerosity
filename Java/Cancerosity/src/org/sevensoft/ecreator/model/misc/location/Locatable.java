package org.sevensoft.ecreator.model.misc.location;

/**
 * @author sks 27-Oct-2005 10:58:41
 * 
 */
public interface Locatable {

	public int getX();

	public int getY();

	/**
	 * Returns the location name of this locatable if set. Null otherwise.
	 */
	public String getLocation();

	/**
	 * Returns true if is object has a location set.
	 */
	public boolean hasLocation();

	/**
	 *  Sets the location name and the co-ords
	 *  
	 * @param location 
	 * @param x 
	 * @param y 
	 */
	public void setLocation(String location, int x, int y);
}
