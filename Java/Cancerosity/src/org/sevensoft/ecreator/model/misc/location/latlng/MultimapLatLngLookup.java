package org.sevensoft.ecreator.model.misc.location.latlng;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.sevensoft.commons.httpc.HttpClient;

/**
 * @author sks 24 Oct 2006 09:50:01
 *
 */
public class MultimapLatLngLookup implements LatLngLookup {

	public static void main(String[] args) throws IOException {

		new MultimapLatLngLookup().lookup("s80 3gb");
	}

	public double[] lookup(String postcode) throws IOException {

		String url = "http://www.multimap.com/map/browse.cgi?client=public&db=pc&pc=" + postcode.replaceAll("\\s", "");
		HttpClient hc = new HttpClient(url);
		hc.connect();

		String response = hc.getResponseString();

		double[] coord = new double[2];

		Pattern pattern = Pattern.compile("<dd class=\"latitude\">.*?\\((.*?)\\)</dd>");
		Matcher matcher = pattern.matcher(response);
		if (matcher.find()) {
			coord[0] = Double.parseDouble(matcher.group(1));
		}

		pattern = Pattern.compile("<dd class=\"longitude\">.*?\\((.*?)\\)</dd>");
		matcher = pattern.matcher(response);
		if (matcher.find()) {
			coord[1] = Double.parseDouble(matcher.group(1));
		}

		return coord;
	}

}
