package org.sevensoft.ecreator.model.misc.content;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 14 Jan 2007 18:41:32
 *
 */
public class ContentReplacer {

	private RequestContext	context;
	private String		target;
	private String		replacement;
	private boolean		categoryContent;
	private boolean		itemContent;
	private boolean		categoryTitles;
	private boolean		itemTitles;
	private boolean		includeHtmlTags;

	public ContentReplacer(RequestContext context, String target, String replacement) {
		this.context = context;
		this.target = target;
		this.replacement = replacement;
	}

	public final String getReplacement() {
		return replacement;
	}

	public final String getTarget() {
		return target;
	}

	public boolean isCategoryTitles() {
		return categoryTitles;
	}

	public final boolean isIncludeHtmlTags() {
		return includeHtmlTags;
	}

	public boolean isItemTitles() {
		return itemTitles;
	}

	private String replace(Pattern pattern, String content) {

		List<String> tags = new ArrayList();

		if (!includeHtmlTags) {
			content = HtmlHelper.holdTags(content, tags);
		}

		content = pattern.matcher(content).replaceAll(replacement);

		if (!includeHtmlTags) {
			content = HtmlHelper.restoreTags(content, tags);
		}
		return content;
	}

	public int run() {

		int n = 0;
		Pattern pattern = Pattern.compile(target, Pattern.CASE_INSENSITIVE);

		if (categoryContent) {

			Query q = new Query(context, "select * from # where content like ?");
			q.setTable(ContentBlock.class);
			q.setParameter("%" + target + "%");
			List<ContentBlock> cbs = q.execute(ContentBlock.class);

			for (ContentBlock block : cbs) {

				String content = block.getContent();
				content = replace(pattern, content);
				block.setContent(content);

				n++;
			}
		}

		if (categoryTitles) {

			for (Category category : Category.get(context)) {

				String newName = category.getName().replace(target, replacement);
				category.setName(newName);
			}

		}

		if (itemContent) {

			Query q = new Query(context, "select * from # where content like ?");
			q.setTable(Item.class);
			q.setParameter("%" + target + "%");

			List<Item> items = q.execute(Item.class);

			for (Item item : items) {

				String content = item.getContent();
				content = replace(pattern, content);
				item.setContent(content);
                item.save();
				n++;
			}

		}

		if (itemTitles) {

			for (Item item : Item.get(context, null, null, 0, 0)) {

				String newName = item.getName().replace(target, replacement);
				item.setName(newName);
			}

		}

		return n;
	}

	public final void setCategoryContent(boolean categories) {
		this.categoryContent = categories;
	}

	public void setCategoryTitles(boolean categoryTitles) {
		this.categoryTitles = categoryTitles;
	}

	public final void setIncludeHtmlTags(boolean includeHtmlTags) {
		this.includeHtmlTags = includeHtmlTags;
	}

	public final void setItemContent(boolean items) {
		this.itemContent = items;
	}

	public void setItemTitles(boolean itemTitles) {
		this.itemTitles = itemTitles;
	}

	public final void setReplacement(String replacement) {
		this.replacement = replacement;
	}

	public final void setTarget(String target) {
		this.target = target;
	}

}
