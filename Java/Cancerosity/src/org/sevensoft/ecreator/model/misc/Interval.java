package org.sevensoft.ecreator.model.misc;

/**
 * @author sks 23 Jan 2007 12:04:29
 *
 */
public enum Interval {
	Daily, Hourly, Manual;
}