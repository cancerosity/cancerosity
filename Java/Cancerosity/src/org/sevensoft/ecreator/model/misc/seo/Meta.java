package org.sevensoft.ecreator.model.misc.seo;

/**
 * @author sks 27-Oct-2005 14:05:29
 * 
 */
public interface Meta {

	public String getDescriptionTag();

	public String getFriendlyUrl();

	public String getKeywords();

	public String getName();

	public String getTitleTag();

	public boolean hasDescriptionTag();

	public boolean hasKeywords();

	public boolean hasTitleTag();

	public void save();

	public void setDescriptionTag(String descriptionTag);

	public void setKeywords(String keywords);

	public void setTitleTag(String titleTag);
}
