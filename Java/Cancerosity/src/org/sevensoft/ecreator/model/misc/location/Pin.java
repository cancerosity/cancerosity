package org.sevensoft.ecreator.model.misc.location;

import java.util.List;

import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 15 Aug 2006 20:38:54
 * 
 * A pin is a mapping of a place or postcode to a OS Grid Ref (northing / easting)
 *
 */
@Table("locations")
public class Pin extends EntityObject {

	/**
	 *  Returns the first location that matches this location 
	 */
	public static Pin getFirst(RequestContext context, String location) {

		if (location == null) {
			return null;
		}

		/*
		 * Surely no two letter placenames ?
		 * Could be 3, eg Ely
		 */
		if (location.length() < 3) {
			logger.fine("[LocationUtil] name less than 3");
			return null;
		}

		Config config = Config.getInstance(context);
		if (!config.isLocations()) {
			logger.fine("[Location] no location files installed");
			return null;
		}

		location = location.trim().toUpperCase();

		// if this is a postcode we must strip spaces and reduce granuality if in partial postcodes
		if (location.matches(".*\\d.*")) {

			location = location.replace(" ", "").replace("-", " ");

			// strip out trailing letters
			String partial = location.replaceAll("[A-Z]*$", "") + "%";

			// find all names that partially match or fully match this postcode. Order by length so we always get the best match
			Query q = new Query(context, "select *, '1' T from # where name like ? "
					+ "union select *, '2' T from # where name like ? order by T limit 1");

			q.setParameter(location);
			q.setParameter(partial);

			q.setTable(Pin.class);
			q.setTable(Pin.class);
			return q.get(Pin.class);

		} else {

			location = location.replace("'", "").replace("-", " ");
			logger.fine("[Location] filtered placename=" + location);

			Query q = new Query(context, "select * from # where name like ?");
			q.setParameter(location);
			q.setTable(Pin.class);
			return q.get(Pin.class);

		}
	}

	/**
	 * Returns a list of locations that start with or equal the param name
	 */
	public static List<Pin> getMatching(RequestContext context, String name) {

		Config config = Config.getInstance(context);
		if (!config.isLocations()) {
			return null;
		}

		name = name.trim().toUpperCase();

		// if this is a postcode we must strip spaces and reduce granuality if in partial postcodes
		if (name.matches(".*\\d.*")) {

			name = name.replace(" ", "").replace("-", " ");

			// strip out trailing letters
			String partial = name.replaceAll("[A-Z]*$", "") + "%";

			// find all names that partially match or fully match this postcode. Order by length so we always get the best match
			Query q = new Query(context, "select *, '1' T from # where name like ? "
					+ "union select *, '2' T from # where name like ? order by T desc limit 1");

			q.setParameter(name);
			q.setParameter(partial);

			q.setTable(Pin.class);
			q.setTable(Pin.class);

			return q.execute(Pin.class);

		} else {

			name = name.replace("'", "").replace("-", " ");

			Query q = new Query(context, "select * from # where name like ? order by length(name) asc");
			q.setParameter(name + "%");
			q.setTable(Pin.class);
			return q.execute(Pin.class);
		}
	}

	/**
	 * Returns true if this is a valid location 
	 * 
	 * A location is valid if there is at least one location that begins with the string param
	 */
	public static boolean isValid(RequestContext context, String name) {
		return getFirst(context, name) != null;
	}

	/**
	 * This is the name of the location. 
	 * We will always replace hyphens with spaces and single quote with blank to ensure consistent data.
	 */
	@Index()
	private String	name;

	/**
	 * Easting
	 */
	@Index()
	private int		x;

	/**
	 * Northing
	 */
	@Index()
	private int		y;

	protected Pin(RequestContext context) {
		super(context);
	}

	public int[] getCoords() {
		return new int[] { x, y };
	}

	/**
	 * 
	 */
	public String getLocation() {
		return name;
	}

	public String getName() {
		return name;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	@Override
	public String toString() {
		return "pin x=" + x + ", y=" + y + ", location=" + name + ", super=" + super.toString();
	}
}
