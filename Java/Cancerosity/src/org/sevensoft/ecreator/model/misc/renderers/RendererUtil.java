package org.sevensoft.ecreator.model.misc.renderers;

import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 22-Nov-2005 20:42:12
 */
public class RendererUtil {

    public static CharSequence form1() {

        StringBuilder sb = new StringBuilder();
        sb.append("<style>");
        sb.append("table.form1 { font-family: Arial; width: 100%; border: 1px solid #e1e1e1; border-top: 0; " + "font-size: 12px; color: #333333; } ");
        sb
                .append("table.form1 caption { margin-top: 10px; font-size: 16px; background: #646464; color: white; padding: 4px 8px; font-weight: bold; text-align:left;}");

        sb.append("table.form1 th { text-align: left; background: #e0e0e0; color: #343434; " + "padding: 7px 5px; border-bottom: 1px solid #b0b0b0; } ");

        sb.append("table.form1 td { padding: 7px 5px;} ");
        sb.append("table.form1 td.key { width: 120px; font-weight: bold; background: #f9f9f9} ");

        sb.append("table.form1 .error { color: #cc0000; font-weight: bold; } ");

        sb.append("table.form1 a { text-decoration: underline; } ");
        sb.append("table.form1 a:hover { text-decoration: none; color: #666666; } ");
        sb.append("</style>");
        return sb;
    }

    public static void tinyMce(RequestContext context, StringBuilder sb, String... ids) {

        if (Module.TinyMCEFull.enabled(context)) {
            full(sb, ids);
        } else if(Module.TinyMCEStandard.enabled(context)){
            advanced(sb, ids);
        } else{
        	System.out.println("old");
            old(sb, ids);
        }

    }

    private static void full(StringBuilder sb, String... ids) {
    	//comment by bing
    	// sb.append("<script language='javascript' type='text/javascript' src='files/tiny_mce/tiny_mce.js'></script>\n");
    	sb.append("<script language='javascript' type='text/javascript' src='files/tiny_mce/tinymce.min.js'></script>\n");
        sb.append("<script language='javascript' type='text/javascript'>\n");
        sb.append("tinyMCE.init({\n");

        // General options
        sb.append("mode : 'exact',\n");
        sb.append("elements : '");

        for (int n = 0; n < ids.length; n++) {
            if (n > 0)
                sb.append(", ");
            sb.append(ids[n]);
        }

        sb.append("',\n");
        //comemt by bing
        //sb.append("theme : 'advanced',\n");
        sb.append("theme : 'modern',\n");
        //comment by bing,no these plugins now
        // sb.append("plugins : 'imagemanager,filemanager',\n");  //need extra payment, not free
       // sb.append("plugins : 'table,save,advhr,advimage,advlink,iespell,print,fullscreen,contextmenu,spellchecker,pagebreak,style,layer,emotions,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,template,imagemanager',\n");
        sb.append("plugins : 'table,save,print,contextmenu,fullscreen',\n");
        // Theme options
        sb.append("theme_advanced_buttons1 : 'save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect',\n");
        sb.append("theme_advanced_buttons2 : 'cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor',\n");
        sb.append("theme_advanced_buttons3 : 'tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen',\n");
        sb.append("theme_advanced_buttons4 : 'insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage',\n");
        sb.append("theme_advanced_toolbar_location : 'top',\n");
        sb.append("theme_advanced_toolbar_align : 'left',\n");
        sb.append("theme_advanced_statusbar_location : 'bottom',\n");
        sb.append("theme_advanced_resizing : true,\n");

        //fullscreen
//        sb.append("fullscreen_new_window : true,");
//        sb.append("fullscreen_settings : { theme_advanced_path_location : 'top'	}");
        //fullscreen end

        //Example content CSS (should be your site CSS)
        sb.append("content_css : 'template-data/common.css',\n");

        // Drop lists for link/image/media/template dialogs
        sb.append("template_external_list_url : 'tinymce-templates/lists/template_list.js',\n");
        sb.append("external_link_list_url : 'tinymce-templates/lists/link_list.js',\n");
        sb.append("external_image_list_url : 'tinymce-templates/lists/image_list.js',\n");
        sb.append("media_external_list_url : 'tinymce-templates/lists/media_list.js'\n");
//
//        // Replace values for the template plugin
//        sb.append("template_replace_values : {\n");
//        sb.append("username : 'Some User',\n");
//        sb.append("staffid : '991234'\n");
//        sb.append("}");
        sb.append("});");
        sb.append("</script>");
    }

    private static void advanced(StringBuilder sb, String... ids) {
    	//comemt by bing
    	// sb.append("<script language='javascript' type='text/javascript' src='files/tiny_mce/tiny_mce.js'></script>\n");
    	sb.append("<script language='javascript' type='text/javascript' src='files/tiny_mce/tinymce.min.js'></script>\n");
        sb.append("<script language='javascript' type='text/javascript'>\n");
        sb.append("tinyMCE.init({\n");
        sb.append("mode : 'exact',\n");
        sb.append("elements : '");

        for (int n = 0; n < ids.length; n++) {
            if (n > 0)
                sb.append(", ");
            sb.append(ids[n]);
        }

        sb.append("',\n");
        //comment by bing for tinyMce upgrade,no these plugins
      	//sb.append("plugins : 'table,save,advhr,advimage,advlink,iespell,flash,print,contextmenu,fullscreen',\n");
      	sb.append("plugins : 'table,save,print,contextmenu,fullscreen',\n");
        sb.append("force_br_newlines : true,\n");
        sb.append("remove_linebreaks : false,\n");
        sb.append("force_p_newlines : false,\n");

        sb
                .append("extended_valid_elements : 'script[charset|defer|language|src|type],"
                        + "map[class|dir<ltr?rtl|id|lang|name|onclick|ondblclick|onkeydown|onkeypress|onkeyup|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|style|title],"
                        + "area[accesskey|alt|class|coords|dir<ltr?rtl|href|id|lang|nohref<nohref|onblur|onclick|ondblclick|onfocus|onkeydown|onkeypress|onkeyup|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|shape<circle?default?poly?rect|style|tabindex|title|target],"
                        + "img[!src|border:0|alt|title|width|height|style]a[name|href|target|title|onclick]"
                        + "',\n");

        //commented by bing 
      	//sb.append("theme : 'advanced',\n");
      	sb.append("theme : 'modern',\n");
        sb.append("theme_advanced_toolbar_align : 'left',\n");
        sb.append("theme_advanced_toolbar_location : 'top',\n");
        sb.append("theme_advanced_resizing : true,\n");
        sb.append("theme_advanced_source_editor_width : '500px',\n");
        sb.append("theme_advanced_buttons1 :'bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontselect,fontsizeselect,formatselect,removeformat,|,forecolor,backcolor,|,bullist,numlist,|,outdent,indent,|,link,unlink,|,hr,charmap,table,|,cleanup,code,help,iespell,fullscreen',\n");
        sb.append("theme_advanced_buttons2 : 'cut,copy,paste,|,undo,redo,|,image,insertimage,|,sub,sup,|,charmap,visualaid,anchor,newdocument,blockquote',\n");
        sb.append("theme_advanced_buttons3 : '',\n");
        //fullscreen
        sb.append("fullscreen_new_window : true,");
        sb.append("fullscreen_settings : { theme_advanced_path_location : 'top'	}");
        //fullscreen end

        sb.append("});\n");
        sb.append("</script>\n\n\n");
    }

    private static void old(StringBuilder sb, String... ids) {
        // use the mce gzip compressor
		//		sb.append("<script language='javascript' type='text/javascript' src='files/tiny_mce/tiny_mce_gzip.jsp'></script>");

		// mce non compressed
    	//commented by bing for tinyMce upgrade
		//sb.append("<script language='javascript' type='text/javascript' src='files/tiny_mce_old/tiny_mce.js'></script>\n");
    	sb.append("<script language='javascript' type='text/javascript' src='files/tiny_mce_old/tinymce.min.js'></script>\n");
		sb.append("<script language='javascript' type='text/javascript'>\n");
		sb.append("tinyMCE.init({\n");
		sb.append("mode : 'exact',\n");
		sb.append("elements : '");

		for (int n = 0; n < ids.length; n++) {
			if (n > 0)
				sb.append(", ");
			sb.append(ids[n]);
		}

		sb.append("',\n");
		//comment by bing for tinyMce upgrade,no these plugins
		//sb.append("plugins : 'table,save,advhr,advimage,advlink,iespell,flash,print,contextmenu,fullscreen',\n");
		sb.append("plugins : 'table,save,print,contextmenu,fullscreen',\n");
		sb.append("force_br_newlines : true,\n");
		sb.append("remove_linebreaks : false,\n");
		sb.append("force_p_newlines : false,\n");

		sb
				.append("extended_valid_elements : 'script[charset|defer|language|src|type],"
						+ "map[class|dir<ltr?rtl|id|lang|name|onclick|ondblclick|onkeydown|onkeypress|onkeyup|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|style|title],"
						+ "area[accesskey|alt|class|coords|dir<ltr?rtl|href|id|lang|nohref<nohref|onblur|onclick|ondblclick|onfocus|onkeydown|onkeypress|onkeyup|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|shape<circle?default?poly?rect|style|tabindex|title|target]',\n");
		//commented by bing 
		//sb.append("theme : 'advanced',\n");
		sb.append("theme : 'modern',\n");
		sb.append("theme_advanced_toolbar_align : 'left',\n");
		sb.append("theme_advanced_toolbar_location : 'top',\n");
		sb.append("theme_advanced_resizing : true,\n");
		sb.append("theme_advanced_source_editor_width : '500px',\n");
		sb
				.append("theme_advanced_buttons1 :'bold,italic,underline,separator,justifyleft,justifycenter,justifyright,separator,forecolor,backcolor,separator,fontselect,fontsizeselect,separator,image,separator,cut,copy,paste,separator,bullist,numlist,separator,undo,redo,separator,link,unlink,separator,hr,flash,charmap,table,separator,cleanup,removeformat,code,iespell,fullscreen',\n");
		sb.append("theme_advanced_buttons2 : '',\n");
		sb.append("theme_advanced_buttons3 : ''\n");
		sb.append("});\n");
		sb.append("</script>\n\n\n");
    }

}
