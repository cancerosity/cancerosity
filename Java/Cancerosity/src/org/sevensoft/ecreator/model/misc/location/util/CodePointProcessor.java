package org.sevensoft.ecreator.model.misc.location.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;

/**
 * helper class to sort out the postzon file
 * 
 * @author sks 28 Oct 2006 09:41:35
 *
 */
public class CodePointProcessor {

	public static void main(String[] args) throws IOException {

		new CodePointProcessor("d:/dev/postcodes.txt", "d:/dev/postcodes_partial.txt").run();
	}

	private final String	srcFilename;
	private final String	destFilename;

	public CodePointProcessor(String string, String string2) {
		this.srcFilename = string;
		this.destFilename = string2;
	}

	private void run() throws IOException {

		HashSet<String> set = new HashSet();

		BufferedReader reader = new BufferedReader(new FileReader(srcFilename));
		BufferedWriter writer = new BufferedWriter(new FileWriter(destFilename));

		String string;
		boolean header = true;
		while ((string = reader.readLine()) != null) {

			if (header) {
				header = false;
				continue;
			}

			string = string.replaceAll("\\s+", " ");
			String[] fields = string.split("\\s");

			String postcode = fields[0] + fields[1].trim();
			postcode = postcode.replaceAll("[A-Z]*$", "");

			if (set.contains(postcode))
				continue;

			String easting = fields[2].trim();
			String northing = fields[3].trim();

			if (easting == null || easting.length() == 0)
				continue;

			if (northing == null || northing.length() == 0)
				continue;

			set.add(postcode);

			writer.write(postcode + "\t" + easting + "\t" + northing + "\n");

		}

		writer.flush();
		writer.close();
		reader.close();
	}
}
