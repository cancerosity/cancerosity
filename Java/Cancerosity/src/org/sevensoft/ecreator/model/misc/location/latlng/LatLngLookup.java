package org.sevensoft.ecreator.model.misc.location.latlng;

import java.io.IOException;

/**
 * @author sks 24 Oct 2006 09:40:16
 *
 */
public interface LatLngLookup {

	public double[] lookup(String postcode) throws IOException;

}
