package org.sevensoft.ecreator.model.misc.content.markers;

import java.util.Map;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class ContentMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		String content;
		if (params.containsKey("stripped")) {
			content = item.getContentStripped();
		} else {
			content = item.getContent();
		}

		if (content == null) {
			return null;
		}

		int limit = 0;
		if (params.containsKey("limit")) {
			limit = Integer.parseInt(params.get("limit").trim());
		}

		String suffix = params.get("suffix");
		if (limit > 0) {
			content = StringHelper.toSnippet(content, limit, suffix);
		}

		String anchor = params.get("anchor");
		if (anchor != null) {
			content = content + "<a href='#content'>" + anchor + "</a>";
		}

		return super.string(context, params, content);
	}

	public Object getRegex() {
		return "content";
	}

}
