package org.sevensoft.ecreator.model.misc.location.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * helper class to sort out the gazetter file
 * 
 * @author sks 28 Oct 2006 09:41:35
 *
 */
public class GazetterProcessor {

	static List<String>	requiredTypes;
	static {
		requiredTypes = Arrays.asList(new String[] { "Other Town", "1st Order Town", "2nd Order Town", "3rd Order Town", "Administrative Area",
			"London Borough", "Other Named Place", "Other Town", "University Campus", "Station", "Loch/Lake" });

	}

	public static void main(String[] args) throws IOException {

		new GazetterProcessor("c:/gaz.txt", "c:/gaz2.csv").run();
	}

	private final String	srcFilename;

	private final String	destFilename;

	public GazetterProcessor(String string, String string2) {
		this.srcFilename = string;
		this.destFilename = string2;
	}

	private void run() throws IOException {

		BufferedReader reader = new BufferedReader(new FileReader(srcFilename));
		BufferedWriter writer = new BufferedWriter(new FileWriter(destFilename));

		String string;
		while ((string = reader.readLine()) != null) {

			String[] fields = string.split("\\t");
			if (fields.length == 4) {

				fields[3] = fields[3].trim();

				if (requiredTypes.contains(fields[3])) {

					fields[0] = fields[0].trim();
					fields[1] = fields[1].trim();
					fields[2] = fields[2].trim();

					if (fields[0].startsWith("\""))
						fields[0] = fields[0].substring(1);

					if (fields[0].endsWith("\""))
						fields[0] = fields[0].substring(0, fields[0].length() - 1);

					writer.write(fields[0] + "\t" + fields[1] + "\t" + fields[2] + "\n");

				} else {

					System.out.println("rejecting: " + fields[3]);
				}
			}
		}

		writer.flush();
		writer.close();
		reader.close();
	}
}
