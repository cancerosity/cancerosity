package org.sevensoft.ecreator.model.misc.currencies;

import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 31-Mar-2006 11:21:04
 *
 */
public class CurrenciesMarker extends MarkerHelper implements IGenericMarker {

	public Object generate(RequestContext context, Map<String, String> params) {

		if (!Module.Currencies.enabled(context)) {
			return null;
		}

		// get current page location
		Link link = (Link) context.getAttribute("pagelink");
		if (link == null) {
			link = new Link(CategoryHandler.class);
		}
		link.addParameter("pad", 1);

		Currency currency = (Currency) context.getAttribute("currency");

		SelectTag tag = new SelectTag(null, "currency", currency);
		tag.addOptions(Currency.get(context));
		tag.setOnChange("window.location='" + link + "&chgcur=' + this.value");

		return tag.toString();

	}

	public Object getRegex() {
		return "currencies";
	}
}