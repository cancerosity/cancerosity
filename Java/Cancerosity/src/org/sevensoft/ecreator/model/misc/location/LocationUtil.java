package org.sevensoft.ecreator.model.misc.location;

import java.text.NumberFormat;
import java.util.logging.Logger;

import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 12-Sep-2005 08:06:45
 * 
 * 
 * 
 */
public class LocationUtil {

	private static final Logger	logger	= Logger.getLogger("ecreator");

	public static NumberFormat	distanceFormatter;

	static {
		distanceFormatter = NumberFormat.getNumberInstance();
		distanceFormatter.setMinimumFractionDigits(1);
		distanceFormatter.setMaximumFractionDigits(1);
	}

	public static String flattenPostcode(String s) {
		return s == null ? null : s.replaceAll("\\s", "").toUpperCase();
	}

	/**
	 *	Returns the distance between this locus and the parameter locus 
	 *
	 *	If the param is null returns -1
	 */
	public static double getDistance(int x1, int y1, int x2, int y2) {

		if (x1 == 0 || y1 == 0 || x2 == 0 || y2 == 0) {
			return -1;
		}

		// must divide result by 1.6093 as our coords are in metres and we want results in miles.		
		final double deltaX = x1 - x2;
		final double deltaY = y1 - y2;

		double distance = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2)) / 1000 / 1.6093;

		logger.fine("[LocationUtil] distance between " + x1 + "," + y1 + " and " + x2 + "," + y2 + ", distance=" + distance);
		return distance;
	}

	public static double getDistance(Locatable l1, Locatable l2) {
		return getDistance(l1.getX(), l1.getY(), l2.getX(), l2.getY());
	}

	public static String getDistanceString(int x1, int y1, int x2, int y2) {
		return distanceFormatter.format(getDistance(x1, y1, x2, y2));
	}

	public static String getDistanceString(int x1, int y1, Locatable l2) {
		return getDistanceString(x1, y1, l2.getX(), l2.getY());
	}

	public static String getDistanceString(Locatable l1, Locatable l2) {
		return getDistanceString(l1.getX(), l1.getY(), l2);
	}

	public static String getFormattedPostcode(String postcode) {
		return getPostcodeFormatted(postcode);
	}

	/**
	 * Formats a postcode into the format XXX XXXX
	 */
	public static String getPostcodeFormatted(String postcode) {

		if (postcode == null) {
			return null;
		}

		if (postcode.length() < 4) {
			return postcode;
		}

		// strip spaces
		postcode = postcode.replace(" ", "");

		return postcode.substring(0, postcode.length() - 3) + " " + postcode.substring(postcode.length() - 3);
	}

//    @Deprecated
	public static void setLocation(RequestContext context, Locatable locatable, String location) {                      

		// check if locations are equal, this will save a database lookup for no reason which can be useful on large location db's
		if (ObjectUtil.equal(locatable.getLocation(), location)) {
			logger.fine("[LocationUtil] locations are equal " + locatable.getLocation() + "==" + location);
			return;
		}

		logger.fine("[LocationUtil] setting location, locatable=" + locatable + ", location=" + location);

		// get coords for location
		Pin pin = Pin.getFirst(context, location);
		logger.fine("[LocationUtil] pin=" + pin);

		if (pin == null) {
			locatable.setLocation(null, 0, 0);
		} else {
			locatable.setLocation(pin.getLocation(), pin.getX(), pin.getY());
		}
		
	}

	private LocationUtil() {
	}
}
