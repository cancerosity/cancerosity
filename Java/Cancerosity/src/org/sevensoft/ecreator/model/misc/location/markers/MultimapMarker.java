package org.sevensoft.ecreator.model.misc.location.markers;

import java.util.Map;
import java.util.logging.Logger;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 9 Jun 2006 11:17:20
 *
 */
public class MultimapMarker extends MarkerHelper implements IGenericMarker {

	@SuppressWarnings("hiding")
	private static Logger	logger	= Logger.getLogger("ecreator");

	public Object generate(RequestContext context, Map<String, String> params) {

		logger.fine("asking to generate multimap marker");

		Company company = Company.getInstance(context);
		String postcode = company.getPostcode();
		if (postcode == null) {
			logger.fine("company details missing postcode");
			return null;
		}

		String text = params.get("text");
		if (text == null) {
			params.put("text", "Show map");
		}

		if (!params.containsKey("target")) {
			params.put("target", "_blank");
		}

		return super.link(context, params, company.getGoogleMapUrl(), "link_multimap");
	}

//	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {
//
//		String id = params.get("id");
//		String attributeId = params.get("attribute");
//
//		if (id != null) {
//
//			Attribute attribute = item.getAttributeFromId(id);
//			location = item.getAttributeValue(attribute);
//
//		} else if (attributeId != null) {
//
//			Attribute attribute = item.getAttributeFromId(attributeId);
//			location = item.getAttributeValue(attribute);
//
//		} else {
//
//			location = item.getLocation();
//		}
//
//		if (location == null) {
//			return null;
//		}
//
//		return super.link(context, params, "http://www.multimap.com/map/places.cgi?client=public&keepicon=true&quicksearch=" + location, "multimap");
//	}

	public Object getRegex() {
		return "multimap";
	}

}
