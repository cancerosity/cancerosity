package org.sevensoft.ecreator.model.misc.content.clouds;

import org.sevensoft.ecreator.iface.admin.misc.content.TagCloudBlockHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 15 Mar 2007 08:18:45
 *
 */
@Table("blocks_tagcloud")
@Label("Tag cloud")
@HandlerClass(TagCloudBlockHandler.class)
public class TagCloudBlock extends Block {

	private int	limit;

	public TagCloudBlock(RequestContext context) {
		super(context);
	}

	public TagCloudBlock(RequestContext context, BlockOwner owner, int objId) {
		super(context, owner, 0);

		this.limit = 60;
		save();
	}

	public final int getLimit() {
		return limit < 10 ? 60 : limit;
	}

	@Override
	public String render(RequestContext context) {

		StringBuilder sb = new StringBuilder();
		sb.append(new TableTag("ec_tagcloud"));
		sb.append("<tr><td>");

		sb.append(new TagCloud(context, getLimit()));

		sb.append("</td></tr>");
		sb.append("</table>");
		return sb.toString();
	}

	public final void setLimit(int limit) {
		this.limit = limit;
	}

}
