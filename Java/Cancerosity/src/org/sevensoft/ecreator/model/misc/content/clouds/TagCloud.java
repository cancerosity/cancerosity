package org.sevensoft.ecreator.model.misc.content.clouds;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.superstrings.comparators.NaturalStringComparator;
import org.sevensoft.ecreator.iface.frontend.search.ContentSearchHandler;
import org.sevensoft.ecreator.model.stats.searching.SearchPhrase;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 15 Mar 2007 08:57:30
 *
 */
public class TagCloud {

	private StringBuilder	sb;

	public TagCloud(RequestContext context, int limit) {

		sb = new StringBuilder();

		List<String> phrases = SearchPhrase.getPopular(context, new Date(), limit);
		if (phrases.size() > 3) {

			List<String> ordered = new ArrayList(phrases);
			Collections.sort(ordered, NaturalStringComparator.instance);

			int t = phrases.size() / 3;

			for (String phrase : ordered) {

				Link link = new Link(ContentSearchHandler.class, null, "keywords", phrase);
				sb.append("<a href=\"" + link + "\" class='ec_tagcloud_link'>");

				if (phrases.indexOf(phrase) < t) {

					sb.append("<span class='ec_tagcloud_large'>" + phrase + "</span>");

				} else if (phrases.indexOf(phrase) < t * 2) {

					sb.append("<span class='ec_tagcloud_medium'>" + phrase + "</span>");

				} else {

					sb.append("<span class='ec_tagcloud_small'>" + phrase + "</span>");
				}

				sb.append("</a> ");
			}
		}
	}

	@Override
	public String toString() {
		return sb.toString();
	}

}
