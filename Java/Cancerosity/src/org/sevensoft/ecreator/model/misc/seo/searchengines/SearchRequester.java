package org.sevensoft.ecreator.model.misc.seo.searchengines;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import org.jdom.JDOMException;

/**
 * @author sks 17 Nov 2006 21:31:06
 *
 */
public abstract class SearchRequester {

	protected static Logger	logger	= Logger.getLogger("ecreator");

	/**
	 *  Returns a List of SearchResult objects for this search engine for the search query
	 */
	public abstract List<SearchResult> getResults(String query) throws IOException, JDOMException;
}
