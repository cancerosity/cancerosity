package org.sevensoft.ecreator.model.misc.geoip;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.commons.simpleio.SimpleZip;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

import com.ricebridge.csvman.CsvLoader;
import com.ricebridge.csvman.CsvManager;

/**
 * @author sks 7 Apr 2007 09:28:30
 *
 */
@Table("geoip")
public class GeoIp extends EntityObject {

	private static Map<String, Country>	cache;

	static {
		cache = new HashMap<String, Country>();
	}

	public static int count(RequestContext context) {
		return SimpleQuery.count(context, GeoIp.class);
	}

	public static void delete(RequestContext context) {
		// clear existing countries
		SimpleQuery.delete(context, GeoIp.class);
	}

	public static void install(RequestContext context, File file) throws IOException {

		// if file is zip then extract
		if (file.getName().endsWith(".zip")) {

			List<File> files = SimpleZip.extractToTempFiles(file);

			// delete zip
			file.delete();

			// if no files in zip exit
			if (files.size() == 0) {
				return;
			}

			// get first file
			file = files.remove(0);

			// delete others
			SimpleFile.deleteFiles(files);

		}

		// clear existing countries
		delete(context);

		CsvManager csvman = new CsvManager();
		csvman.setSeparator(",");
		csvman.setQuote('"');
		CsvLoader loader = csvman.makeLoader(file);
		loader.begin();

		StringBuilder sb = null;
		int n = 0;
		while (loader.hasNext()) {

			if (sb == null) {
				sb = new StringBuilder("insert into # (`cc`, `from`, `to`) values ");
			} else {
				sb.append(", ");
			}

			String[] row = loader.next();
			long from = Long.parseLong(row[0]);
			long to = Long.parseLong(row[1]);
			String cc = row[4];

			sb.append("('" + cc + "'," + from + "," + to + ")");
			n++;

			if (n == 1000) {
				new Query(context, sb).setTable(GeoIp.class).run();
				sb = null;
				n = 0;
			}
		}

		if (n > 0) {
			new Query(context, sb).setTable(GeoIp.class).run();
		}

		loader.end();

		// delete file
		file.delete();
	}

	public static Country lookup(RequestContext context, String ipAddress) {

		// check cache
		if (cache.containsKey(ipAddress)) {
			return cache.get(ipAddress);
		}

		// convert ip to numerical
		String[] s = ipAddress.split("\\.");
		long l1 = Long.parseLong(s[0]) * 256 * 256 * 256;
		long l2 = Long.parseLong(s[1]) * 256 * 256;
		long l3 = Long.parseLong(s[2]) * 256;
		long l4 = Long.parseLong(s[3]);

		long l = l1 + l2 + l3 + l4;

		Query q = new Query(context, "select cc from # where `from` <= ? and `to` >= ?");
		q.setTable(GeoIp.class);
		q.setParameter(l);
		q.setParameter(l);

		String cc = q.getString();
		Country country = Country.getInstance(cc);

		if (cache.size() == 50) {
			cache.clear();
		}

		cache.put(cc, country);

		return country;
	}

	private String	cc;
	private long	from, to;

	private GeoIp(RequestContext context) {
		super(context);
	}

}
