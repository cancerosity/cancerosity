package org.sevensoft.ecreator.model.misc.log;

import java.util.List;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 8 Aug 2006 14:07:28
 * 
 * An entry in the log
 * 
 */
@Table("logs_entries")
public class LogEntry extends EntityObject {

	public static List<LogEntry> get(RequestContext context, Logging log) {
		Query q = new Query(context, "select * from # where log=? order by id desc");
		q.setTable(LogEntry.class);
		q.setParameter(log.getFullId());
		return q.execute(LogEntry.class);
	}

	/**
	 *  Who executed this command or blank for system 
	 */
	@Index()
	private User	user;

	/**
	 * The owner object
	 */
	private String	log;

	/**
	 * Log message
	 */
	private String	message;

	private DateTime	time;

	private String	name;

	private String	loggerName;

	private String	ipAddress;

	private boolean	superman;

	protected LogEntry(RequestContext context) {
		super(context);
	}

	public LogEntry(RequestContext context, Logging log, String message) {
		this(context, log, null, message);
	}

	public LogEntry(RequestContext context, Logging obj, User user, String message) {
		super(context);

		this.log = obj.getFullId();
		this.name = obj.getLogName();
		this.message = message;
		this.time = new DateTime();
		this.ipAddress = context.getRemoteIp();
		this.superman = context.containsAttribute("superman");
		this.user = user;

		if (user != null) {
			this.loggerName = user.getName();
		}
		save();
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public User getLogger() {
		return (User) (user == null ? null : user.pop());
	}

	public String getLoggerName() {
		return loggerName == null ? "System" : loggerName;
	}

	public String getMessage() {
		return message;
	}

	public final String getName() {
		return name;
	}

	public final DateTime getTime() {
		return time;
	}

	/**
	 * 
	 */
	public String getTimeString() {
		if (time == null) {
			return "-";
		} else {
			return time.toString("HH:mm dd-MMM-yyyy");
		}
	}

	public boolean hasLogger() {
		return logger != null;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
}
