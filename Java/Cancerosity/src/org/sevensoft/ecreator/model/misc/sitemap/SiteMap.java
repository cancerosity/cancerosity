package org.sevensoft.ecreator.model.misc.sitemap;

import org.sevensoft.ecreator.iface.EcreatorRenderer;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.BasketHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.OrderStatusHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.extras.polls.Poll;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.config.Captions;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 02-Nov-2005 09:45:39
 */
public class SiteMap extends EcreatorRenderer {

    private SiteMapPage root;

    public SiteMap(RequestContext context) {

        super(context);

        Category category = Category.getRoot(context);

        this.root = new SiteMapPage();
        this.root.add(category.getName(), ".");

        for (Category l1 : category.getVisibleChildren()) {
            if (l1.isHidden()) {
                continue;
            }
            SiteMapPage node1 = this.root.add(l1.getName(), l1.getUrl());

            for (Category l2 : l1.getChildren()) {
                if (l2.isHidden()) {
                    continue;
                }
                node1.add(l2.getName(), l2.getUrl());
            }
        }

        // shopping basket
        if (Module.Shopping.enabled(context)) {
            SiteMapPage node = root.add("Shopping Basket", BasketHandler.class);
            node.add("Order Status", OrderStatusHandler.class);
            node.add("Checkout", "checkout.do");
//            node.add("Delivery Rates", "delivery-rates.do");
    //        node.add("Quick Order", "quick-order.do");
        }

        // sms settings
        if (Module.Sms.enabled(context)) {
            root.add("Sms Bulletin", "sms.do");
        }

        // newsletter
        if (Module.Newsletter.enabled(context)) {
            root.add("Newsletter", "newsletter.do");
        }

        // member account
        if (Module.Accounts.enabled(context)) {
            root.add(Captions.getInstance(context).getAccountCaption(), AccountHandler.class);
        }

        if (Module.Polls.enabled(context)) {

            if (Poll.get(context).size() > 0) {
                SiteMapPage node = root.add("Polls", "poll.do");

                for (Poll poll : Poll.get(context)) {
                    node.add(poll.getTitle(), poll.getRelativeUrl());
                }
            }
        }
    }

    public SiteMapPage getRoot() {
        return root;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        sb.append("<ul>");

        for (SiteMapPage node : this.root.getChildren()) {

            sb.append("<li><a href='");
            sb.append(node.getUrl());
            sb.append("'>");
            sb.append(node.getName());
            sb.append("</a></li>");

            if (node.hasChildren()) {

                sb.append("<ul>");
                for (SiteMapPage node1 : node.getChildren()) {

                    sb.append("<li><a href='");
                    sb.append(node1.getUrl());
                    sb.append("'>");
                    sb.append(node1.getName());
                    sb.append("</a></li>");
                }
                sb.append("</ul>");
            }
        }

        sb.append("</ul>");

        return sb.toString();
    }

}
