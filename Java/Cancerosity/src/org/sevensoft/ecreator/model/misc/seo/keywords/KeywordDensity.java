package org.sevensoft.ecreator.model.misc.seo.keywords;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.collections.bags.ArrayBag;
import org.sevensoft.commons.collections.bags.Bag;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.commons.superstrings.english.CommonWords;
import org.sevensoft.commons.superstrings.formatters.NumberFormatter;

/**
 * @author sks 4 Nov 2006 21:24:58
 * 
 * Calculate keyword density for content
 *
 */
public class KeywordDensity {

	private static Pattern	brReplace	= Pattern.compile("<br\\s*/\\?>", Pattern.CASE_INSENSITIVE);
	private static Logger	logger	= Logger.getLogger("ecreator");

	private String		content;
	private List<String>	words;
	private Bag<String>	bag;

	public KeywordDensity(String c) {

		content = c.toLowerCase();
		content = content.replace("&nbsp;", " ");

		stripHtml();
		content = content.replaceAll("\\s{2,}", " ");

		this.words = new ArrayList(Arrays.asList(content.split("\\b|\\n")));

		// remove commons
		words.removeAll(CommonWords.English);

		// remove words under 2 characters
		CollectionsUtil.filter(words, new Predicate<String>() {

			public boolean accept(String e) {
				return e.length() > 2;
			}
		});

		this.bag = new ArrayBag();
		this.bag.addAll(words);
	}

	/**
	 * Returns the x most used keywords mapped to their count
	 */
	public Map<String, Integer> getCounts(int x) {

		Map<String, Integer> map = new LinkedHashMap();

		Iterator<String> iter = bag.listDesc().iterator();
		while (map.size() < x && iter.hasNext()) {

			String word = iter.next();
			int count = bag.size(word);

			map.put(word, count);
		}

		return map;
	}

	public double getDensity(int i) {
		return (double) i / words.size() * 100d;
	}

	public String getDensityFormatted(int i) {
		return NumberFormatter.format(getDensity(i), 1);
	}

	/**
	 * Returns the total number of words used in this content 
	 */
	public int getWordCount() {
		return words.size();
	}

	/**
	 * Remove content that is inside non rendered html and remove all tags
	 */
	private void stripHtml() {

		content = content.replaceAll("<style>.*?</style>", " ");
		content = content.replaceAll("<script>.*?</script>", " ");

		content = HtmlHelper.stripHtml(content, " ");

	}
}
