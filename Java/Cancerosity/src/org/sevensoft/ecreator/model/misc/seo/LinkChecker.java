package org.sevensoft.ecreator.model.misc.seo;

import java.io.IOException;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletResponse;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.httpc.HttpMethod;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.misc.content.ContentBlock;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 11 Nov 2006 14:14:34
 *
 */
public class LinkChecker {

	private static final Logger	logger	= Logger.getLogger("ecreator");
	private final RequestContext	context;

	public LinkChecker(RequestContext context) {
		this.context = context;
	}

	public MultiValueMap<Category, String> check() {

		// get content blocks
		Query q = new Query(context, "select * from # ");
		q.setTable(ContentBlock.class);

		Config config = Config.getInstance(context);

		MultiValueMap<Category, String> errors = new MultiValueMap(new TreeMap());
		int n = 0;
		List<ContentBlock> contentBlocks;
		while ((contentBlocks = q.execute(ContentBlock.class, n, 200)).size() > 0) {

			for (ContentBlock contentBlock : contentBlocks) {

				if (contentBlock.hasContent()) {

					logger.fine("[LinkChecker] content " + contentBlock.getContent().length() + " characters");

					List<String> urls = HtmlHelper.getAnchorUrls(contentBlock.getContent());
					logger.fine("[LinkChecker] urls detected: " + urls);

					for (String url : urls) {

						// do not process mailtos
						if (!url.startsWith("mailto:")) {

							// we must resolve the url
							if (!url.startsWith("http://") && !url.startsWith("https://")) {

								if (url.startsWith("/")) {

									url = config.getUrl() + url;

								} else {

									url = config.getUrl() + "/" + url;
								}
							}

							try {

								logger.fine("[LinkChecker] resolved url to check: " + url);
								HttpClient hc = new HttpClient(url, HttpMethod.Get);
								hc.connect();
								switch (hc.getResponseCode()) {

								case HttpServletResponse.SC_BAD_REQUEST:
								case HttpServletResponse.SC_NOT_FOUND:
								case HttpServletResponse.SC_NO_CONTENT:
								case HttpServletResponse.SC_FORBIDDEN:
								case HttpServletResponse.SC_BAD_GATEWAY:
								case HttpServletResponse.SC_NOT_ACCEPTABLE:
								case HttpServletResponse.SC_UNAUTHORIZED:
								case HttpServletResponse.SC_REQUEST_TIMEOUT:
									errors.put(contentBlock.getOwnerCategory(), url);

								}

							} catch (IOException e) {
								e.printStackTrace();
								errors.put(contentBlock.getOwnerCategory(), url);
							}

						}
					}

				}

			}

			n += 200;
		}
		
		return errors;
	}
}
