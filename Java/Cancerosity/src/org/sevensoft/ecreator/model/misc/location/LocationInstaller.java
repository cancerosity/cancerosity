package org.sevensoft.ecreator.model.misc.location;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.commons.simpleio.SimpleZip;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 15 Nov 2006 10:06:03
 *
 */
public class LocationInstaller {

	private final static Logger	logger	= Logger.getLogger("ecreator");

	public static void delete(RequestContext context) {

		SimpleQuery.delete(context, Pin.class);
		logger.config("[LocationInstaller] location files deleted");
	}

	public static Object getSize(RequestContext context) {
		return SimpleQuery.count(context, Pin.class);
	}

	/**
	 * Create the database from the contents of this file
	 * and then deletes the file
	 * 
	 * the file should be command delimited with name,easting,northing
	 * 
	 */
	public static void install(final RequestContext context, final File file) throws IOException {

		List<File> files;

		// if file is zip then extract
		if (file.getName().endsWith(".zip")) {

			files = SimpleZip.extractToTempFiles(file);

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			// delete original zip
			file.delete();

		} else {

			// otherwise just use original file
			files = Collections.singletonList(file);
		}

		logger.config("[LocationInstaller] attempting to install from files=" + files);

		if (files.isEmpty()) {
			return;
		}

		// empty table
		SimpleQuery.empty(context, Pin.class);
		logger.config("[LocationInstaller] deleted all existing location files");

		// now process all files
		for (File f : files) {

			logger.config("[LocationInstaller] installing from file=" + f);
			installFile(context, f);

			// delete file
			f.delete();
		}
	}

	private static void installFile(final RequestContext context, File file) {

		BufferedReader reader = null;

		try {

			logger.fine("[LocationInstaller] installing from file=" + file);

			StringBuilder sb = null;
			int n = 0;

			reader = new BufferedReader(new FileReader(file));
			String string;
			while ((string = reader.readLine()) != null) {

				String[] row = string.split(",");

				if (row.length > 2) {

					// strip apostraphes and hyphens
					String location = row[0].replace("'", "").replace("-", " ").trim();
					int x = Integer.parseInt(row[1].trim());
					int y = Integer.parseInt(row[2].trim());

					if (sb == null) {
						sb = new StringBuilder("insert into # (name, x, y) values ");
					} else {
						sb.append(", ");
					}

					sb.append("('" + location + "'," + x + "," + y + ")");
					n++;

					if (n == 1000) {

						new Query(context, sb).setTable(Pin.class).run();
						sb = null;
						n = 0;
					}

				}
			}

			if (n > 0) {
				new Query(context, sb).setTable(Pin.class).run();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		} finally {

			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
