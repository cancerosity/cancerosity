package org.sevensoft.ecreator.model.misc.adwords;

/**
 * @author sks 5 Jun 2006 10:44:36
 *
 */
public class GoogleAdwords {

	private final String	googleAdwordsConversionId;

	public GoogleAdwords(String googleAdwordsConversionId) {
		this.googleAdwordsConversionId = googleAdwordsConversionId;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("<!-- Google Code for Purchase Conversion Page -->\n");
		sb.append("<script language=\"JavaScript\" type=\"text/javascript\">\n");
		sb.append("<!--\n");
		sb.append("var google_conversion_id = " + googleAdwordsConversionId + ";\n");
		sb.append("var google_conversion_language = \"en_US\";\n");
		sb.append("var google_conversion_format = \"1\";\n");
		sb.append("var google_conversion_color = \"666666\";\n");
		sb.append("if (1) {\n");
		sb.append("var google_conversion_value = 1;\n");
		sb.append("}\n");
		sb.append("var google_conversion_label = \"Purchase\";\n");
		sb.append("//-->\n");
		sb.append("</script>\n");
		sb.append("<script language=\"JavaScript\" type=\"text/javascript\" src=\"http://www.googleadservices.com/pagead/conversion.js\">\n");
		sb.append("</script>\n\n");

		sb.append("<noscript>\n");
		sb.append("<img height=1 width=1 border=0 "
				+ "src=\"http://www.googleadservices.com/pagead/conversion/1234567890/?value=1&label=Purchase&script=0\">\n");
		sb.append("</noscript>\n");

		sb.append("<!-- End Google Code -->\n");

		return sb.toString();
	}

}
