package org.sevensoft.ecreator.model.misc.location.box;

import org.sevensoft.ecreator.iface.admin.misc.location.ActiveLocationBoxHandler;
import org.sevensoft.ecreator.iface.frontend.search.LocationHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 1 Apr 2007 11:58:42
 *
 */
@Table("boxes_active_location")
@HandlerClass(ActiveLocationBoxHandler.class)
@Label("Active location")
public class ActiveLocationBox extends Box {

	private String	header, footer;

	/**
	 * Where to forward to after setting location
	 */
	private String	forward;

	public ActiveLocationBox(RequestContext context) {
		super(context);
	}

	public ActiveLocationBox(RequestContext context, String panel) {
		super(context, panel);

		this.header = "Enter a placename, postcode or station in the UK";
		this.captionText = "Change location";

		save();
	}

	@Override
	protected String getCssIdDefault() {
		return "active_location";
	}

	public final String getFooter() {
		return footer;
	}

	public final String getForward() {
		return forward;
	}

	public final String getHeader() {
		return header;
	}

	public String getSubmitLabel() {
		return "Search";
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	@Override
	public String render(RequestContext context) {

		StringBuilder sb = new StringBuilder();
		sb.append(getTableTag());

		if (header != null) {
			sb.append("<tr><td class='header'>");
			sb.append(header);
			sb.append("</td></tr>");
		}

		sb.append(new FormTag(LocationHandler.class, null, "get"));
		sb.append(new HiddenTag("forward", forward));

		sb.append("<tr><td class='input'>");
		sb.append(new TextTag(context, "location", 12));
		sb.append(new SubmitTag(getSubmitLabel()));
		sb.append("</td></tr>");

		sb.append("</form>");

		if (footer != null) {
			sb.append("<tr><td class='footer'>");
			sb.append(footer);
			sb.append("</td></tr>");
		}

		sb.append("</table>");
		return sb.toString();
	}

	public final void setFooter(String footer) {
		this.footer = footer;
	}

	public final void setForward(String forward) {
		this.forward = forward;
	}

	public final void setHeader(String header) {
		this.header = header;
	}

}
