package org.sevensoft.ecreator.model.misc.adwords;

import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;

/**
 * @author sks 16 Mar 2007 06:24:42
 *
 */
@Table("boxes_adwords")
@HandlerClass(GoogleAdwordsBoxHandler.class)
@Label("Google adwords")
public class GoogleAdwordsBox extends Box {

	private String	googleAdwordsConversionId;

	public GoogleAdwordsBox(RequestContext context) {
		super(context);
	}

	public GoogleAdwordsBox(RequestContext context, String panel) {
		super(context, panel);
	}

	@Override
	protected String getCssIdDefault() {
		return "adwords";
	}

	public final String getGoogleAdwordsConversionId() {
		return googleAdwordsConversionId;
	}

	@Override
	public boolean hasEditableContent() {
		return false;
	}

	@Override
	public String render(RequestContext context) {

		if (!Module.GoogleAdwords.enabled(context)) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		sb.append(getTableTag());
		sb.append("<tr><td>");
		sb.append(new GoogleAdwords(googleAdwordsConversionId));
		sb.append("</td></tr></table>");
		return sb.toString();
	}

	public final void setGoogleAdwordsConversionId(String googleAdwordsConversionId) {
		this.googleAdwordsConversionId = googleAdwordsConversionId;
	}

}
