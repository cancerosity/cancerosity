package org.sevensoft.ecreator.model.misc.content;

import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.misc.content.ContentBlockHandler;
import org.sevensoft.ecreator.iface.frontend.search.ItemSearchHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.search.CategorySearcher;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.design.marker.MarkerRenderer;
import org.sevensoft.ecreator.model.design.styles.Style;
import org.sevensoft.ecreator.model.extras.languages.Language;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.http.util.Link;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author sks 6 Jul 2006 16:51:35
 */
@Table("blocks_content")
@Label("Content")
@HandlerClass(ContentBlockHandler.class)
public class ContentBlock extends Block {

    private static String[] metas = new String[]{"?", "$", ")", "(", "+", "%", "&", "*", ".", "/", "\\", "`"};

    private static Integer MAX_STRING_LENGTH = 65535;
    private static String TOO_LONG_STRING = "too long";

    public static void clear(RequestContext context) {
        new Query(context, "update # set contentLinked=null").setTable(ContentBlock.class).run();
    }

    private Style style;

    private Language language;

    @Index()
    private String content;

    /**
     *
     */
    @Index()
    private String contentStripped;

    private String contentLinked;

//        @Index()
//    private byte[] content;
//
//    /**
//     *
//     */
//    @Index()
//    private byte[] contentStripped;
//
//    private byte[] contentLinked;

    /**
     * Number of pages of content
     */
    private int pages;

    /**
     * Extra code, css, image maps etc that we want to include without messing about in the main content editor
     */
    private String code;

    /**
     *
     */
    private boolean simpleEditor;

    private static boolean lock;
    private static Map<Integer,String> contentLink = new HashMap<Integer, String>();

    public ContentBlock(RequestContext context) {
        super(context);
    }

    public ContentBlock(RequestContext context, BlockOwner category, int objId) {
        super(context, category, 0);

        this.language = Language.English;
        setName("Content");
        save();
    }

    private void addSuggestions(Map<String, String> keywords, List<String> suggestions, String url) {

        for (String suggestion : suggestions) {

            suggestion = suggestion.trim();
            if (suggestion.length() > 3) {

                if (keywords.containsKey(suggestion)) {

                    keywords.put(suggestion, new Link(ItemSearchHandler.class, null, "keywords", suggestion).toString());

                } else {

                    keywords.put(suggestion, url);

                }
            }
        }
    }

    @Override
    public synchronized boolean delete() {

        /*
           * We must always leave in at least one content block
           */
        if (SimpleQuery.count(context, ContentBlock.class, "ownerCategory", getOwnerCategory()) == 1) {
            return false;
        }

        return super.delete();
    }

    public final String getCode() {
        return code;
    }

    public String getContent() {
        return content;
//        return content == null ? null : new String(content);
    }

    public Integer getContentLength() {
        return content == null ? 0 : content.length();
    }

    public Integer getContentLinkedLength() {
        return contentLinked == null ? 0 : contentLinked.length();
    }

    public String getContent(int i) {
        return StringHelper.toSnippet(content, i);
//        return content == null ? null : StringHelper.toSnippet(new String(content), i);
    }

    public String getContentStripped() {
        return contentStripped;
//        return contentStripped == null ? null : new String(contentStripped);
    }

    public String getContentStripped(int i) {
        return StringHelper.toSnippet(contentStripped, i);
//        return contentStripped == null ? null : StringHelper.toSnippet(new String(contentStripped), i);
    }

    /*
      * Get all keywords mapped to urls
      */
    private Map<String, String> getKeywords() {

        logger.fine("[ContentBlock] getting auto content keywords");

        Seo seo = Seo.getInstance(context);
        Map<String, String> keywords = new HashMap();

        CategorySearcher searcher = new CategorySearcher(context);
//        searcher.setLimit(500);
        searcher.setMaxParents(seo.getAutoContentLinkingCategoryDepth());
        searcher.setVisible(true);

        for (Category category : searcher.getCategories(false)) {

            if (category.isContentLinkingKeyword() && category.getName() != null && category.getName().length() != 0) {

                keywords.put(category.getName(), category.getUrl());

                for (String addKey: category.getContentLinkingKeywords()){
                    keywords.put(addKey, category.getUrl());
                }
                
                if (seo.isSuggestions()) {
                    addSuggestions(keywords, category.getSuggestions(), category.getUrl());
                }
            }
        }



        Query q = new Query(context, "select i.name, i.friendlyUrl from # i join items_types it on it.id=i.itemtype where i.status='LIVE' and it.hidden=0 ");
        q.setTable(Item.class);
        Map map = q.getMap(String.class, String.class, new HashMap());
        Map<String, String> itemsMap = new HashMap();
        for (Object key : map.keySet()) {
            if (((String) key).length() != 0) {
                itemsMap.put((String) key, Item.getUrl(context, (String) map.get(key), (String) key));
            }
        }
        keywords.putAll(itemsMap);


        //			if (seo.isSuggestions()) {
        //				addSuggestions(keywords, item.getSuggestions(), item.getRelativeUrl());
        //			}
        //		}

        if (hasOwnerCategory()) {

            keywords.remove(getOwnerCategory().getName());

        } else if (hasOwnerItem()) {

            keywords.remove(getOwnerItem().getName());

        }

        keywords = StringHelper.stripIllegals(keywords);

        logger.fine("[ContentBlock] keywords=" + keywords);

        return keywords;
    }

    public Language getLanguage() {
        return language == null ? Language.English : language;
    }

    @Override
    public String getLogName() {
        return "Content block #" + getId();
    }

    @Override
    public String getName() {

        if (name == null) {
            name = getDefaultName();
        }

        if (Module.Languages.enabled(context)) {
            return name + " " + getLanguage();
        } else {
            return name;
        }
    }

    public int getPages() {
        return pages;
    }

    public final Style getStyle() {
        return style;
    }

    public boolean hasCode() {
        return code != null;
    }

    public boolean hasContent() {
        return content != null;
//        return content != null && new String(content).length() != 0;
    }

    public boolean hasContentLinked() {
        return contentLinked != null;
//        return contentLinked != null && new String(contentLinked).length() != 0;
    }

    public boolean hasStyle() {
        return style != null;
    }

    @Override
    public boolean isRenameable() {
        return true;
    }

    @Override
    public boolean isSimple() {
        return false;
    }

    public final boolean isSimpleEditor() {
        return simpleEditor;
    }

    private String keywordHighlighting(String processedContent) {

        Seo seo = Seo.getInstance(context);

        logger.fine("[ContentBlock] highlighting keywords=" + seo.isAutoKeywordHighlighting());

        if (seo.isAutoKeywordHighlighting()) {

            // hold tags so we don't replace words inside HTML Tags
            List<String> tags = new ArrayList();
            processedContent = HtmlHelper.holdTags(processedContent, tags);

            List<String> keywords = seo.getHighlightedKeywords();
            stripIllegals(keywords);

            for (String keyword : keywords) {
                processedContent = processedContent.replaceAll("\\b" + keyword + "\\b", "<b>" + keyword + "</b>");
            }

            processedContent = HtmlHelper.restoreTags(processedContent, tags);

        }

        return processedContent;
    }

    /**
     * creates the linked content
     */
    public void makeLinkedContent() {

        if (getContent() == null) {
            return;
        }

        context.disableCache();

        Map<String, String> keywords = getKeywords();

        contentLinked = content;// == null ? new byte[0] : content;
        for (String keyword : keywords.keySet()) {

            List<String> tags = new ArrayList();
            contentLinked = HtmlHelper.holdTags(contentLinked, tags);
//            contentLinked = HtmlHelper.holdTags(new String(contentLinked), tags).getBytes();

            String url = keywords.get(keyword);
            if (url != null) {
                if (keyword.contains("|")) {
                    keyword = keyword.replace("|", "\\|");
                }
                try {
                    Pattern pattern = Pattern.compile("\\b(" + keyword.replace("\\s", "(&nbsp;)|\\s") + ")\\b", Pattern.CASE_INSENSITIVE);
                    Matcher matcher = pattern.matcher(contentLinked);
                    contentLinked = matcher.replaceAll("<a href=\"" + url + "\">$1</a>");
//                    Matcher matcher = pattern.matcher(new String(contentLinked));
//                    contentLinked = matcher.replaceAll("<a href=\"" + url + "\">$1</a>").getBytes();
                } catch (Exception e) {
                    logger.warning(e.toString());
                }

            }

            contentLinked = HtmlHelper.restoreTags(contentLinked, tags);
//            contentLinked = HtmlHelper.restoreTags(new String(contentLinked), tags).getBytes();
        }
        contentLink.put(getId(), contentLinked);

//        if (contentLinked.length() <= MAX_STRING_LENGTH) {
            save();
//        } else {
//            contentLinked = TOO_LONG_STRING;
//        }
    }

    @Override
    public Object render(RequestContext context) {

        logger.fine("[ContentBlock] rendering, content=" + (content == null ? null : content.length()));
//        logger.fine("[ContentBlock] rendering, content=" + (content == null ? null : content.length));

        if (getContent() == null) {
            return null;
        }

        // only render if correct language
        if (getLanguage() != context.getAttribute("language")) {
            return null;
        }

        StringBuilder sb = new StringBuilder();

        // render code
        if (code != null) {

            sb.append("\n<!--start content scripts -->\n");
            sb.append(code);
            sb.append("\n<!--end content scripts -->\n");
        }

        String processedContent;

        // if auto content linking is enabled then we should use the linked content
        if (getOwnerCategory().isAutoContentLinking()) {

            logger.fine("[ContentBlock] auto content linking is enabled.");

            // if the linked content is null, then we need to create it.
            try {
                for (int i = 0; i < 200; i++) {
                    if (!lock) {
                        if (!hasContentLinked() && (contentLink == null || !contentLink.containsKey(getId()))) {
                            logger.fine("[ContentBlock] generating linked content");
                            makeLinkedContent();
                        }
                        if (!hasContentLinked()) {
                            contentLinked = contentLink.get(getId());
                            contentLink.clear();
                        }
                        break;
                    } else {

                        Thread.sleep(1000);
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

//            if (contentLinked.equals(TOO_LONG_STRING)) {
//                processedContent = content;
//            } else {
            if (contentLinked == null) {
                processedContent = "";
            } else {
                processedContent = contentLinked;
            }
//            processedContent = new String(contentLinked);
//            }

        } else {

            processedContent = content;
//            processedContent = new String(content);
        }

        // keyword highlighting
        processedContent = keywordHighlighting(processedContent);

        // finally check to render tags
        if (getOwnerCategory().isRenderTags()) {
            logger.fine("[ContentBlock] rendering tags");
            processedContent = MarkerRenderer.render(context, processedContent, getOwnerCategory());
        }

        sb.append("\n<!--start content html-->\n");

        if (style == null) {
            sb.append(processedContent);
        } else {
            sb.append(style.render(processedContent, getOwnerCategory().getApprovedImages()));
        }
        sb.append("\n<!--end content html-->\n");

        return sb;
    }

    /**
     * Replace content
     */
    public void replace(String from, String to) {
        if (hasContent()) {
            setContent(content.replace(from, to));
//            setContent(new String(content).replace(from, to));
        }
    }

    public final void setCode(String code) {
        this.code = code;
    }

    public void setContent(String content) {

        this.content = content;
        this.contentLinked = null;
        contentLink.clear();
        this.contentStripped = HtmlHelper.stripHtml(content, " ");

//        this.content = content == null ? new byte[0] : content.getBytes();
//        this.contentLinked = new byte[0];
//        this.contentStripped = content == null ? new byte[0] : HtmlHelper.stripHtml(content, " ").getBytes();

        if (content == null) {
            pages = 1;

        } else {
            Pattern pattern = Pattern.compile("^@page$", Pattern.MULTILINE);
            pages = pattern.split(content).length;
        }

        save();
          //not necessary to clear all the blocks. performance issues!!!!
//        clear(context);
        lock = true;
        new Thread(new Runnable() {
            public void run() {
                if (getOwnerCategory().isAutoContentLinking()) {

                    logger.fine("[ContentBlock] auto content linking is enabled.");

                    // if the linked content is null, then we need to create it.
                    if (!hasContentLinked()) {
                        logger.fine("[ContentBlock] generating linked content");
                        makeLinkedContent();
                    }
                }
                lock=false;
            }
        }).start();
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public final void setSimpleEditor(boolean simpleEditor) {
        this.simpleEditor = simpleEditor;
    }

    public final void setStyle(Style style) {
        this.style = style;
    }

    private void stripIllegals(Collection<String> keywords) {

        Iterator<String> iter = keywords.iterator();
        while (iter.hasNext()) {

            if (StringHelper.contains(iter.next(), metas)) {
                iter.remove();
            }
        }
    }

/*
    private Map<String, String> stripIllegals(Map<String, String> keywords) {

        // create a new map to put keywords into
        Map<String, String> keywordsCopy = new HashMap(keywords);

        for (String key : keywords.keySet()) {

            if (StringHelper.contains(key, metas)) {
                keywordsCopy.remove(key);
            }
        }

        return keywordsCopy;
    }
*/

}
