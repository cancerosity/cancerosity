package org.sevensoft.ecreator.model.misc.adwords;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 16 Mar 2007 06:30:18
 *
 */
@Path("admin-blocks-adwords.do")
public class GoogleAdwordsBlockHandler extends BlockEditHandler {

	private GoogleAdwordsBlock	block;
	private String			googleAdwordsConversionId;

	public GoogleAdwordsBlockHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Block getBlock() {
		return block;
	}

	@Override
	protected void saveSpecific() {
		block.setGoogleAdwordsConversionId(googleAdwordsConversionId);
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Adwords account"));
		sb.append(new AdminRow("Google adwords conversion ID", "This is your account as provided by google", new TextTag(context,
				"googleAdwordsConversionId", block.getGoogleAdwordsConversionId(), 30)));
		sb.append("</table>");
	}

}
