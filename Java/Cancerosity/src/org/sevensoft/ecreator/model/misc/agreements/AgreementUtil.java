package org.sevensoft.ecreator.model.misc.agreements;

/**
 * @author sks 4 Jul 2006 16:37:41
 *
 */
public class AgreementUtil {

	public static boolean hasExternalAgreement(AgreementOwner owner) {
		return owner.hasAgreement() && owner.getAgreement().isPage();
	}

	public static boolean hasInlineAgreement(AgreementOwner owner) {
		return owner.hasAgreement() && owner.getAgreement().isInline();
	}

}
