package org.sevensoft.ecreator.model.misc.location.markers;

import java.util.Map;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IItemMarker;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.location.LocationUtil;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Apr 2006 17:32:32
 *
 */
public class DistanceMarker extends MarkerHelper implements IItemMarker {

	public Object generate(RequestContext context, Map<String, String> params, Item item, String location, int x, int y) {

		Item account = (Item) context.getAttribute("account");
		boolean isAccount = params.containsKey("account");

		if (isAccount && account == null) {
			logger.fine("[DistanceMarker] showing distance from account but not logged in");
			return null;
		}

		if (isAccount) {

			logger.fine("[DistanceMarker] overriding location from account=" + account);
			location = account.getLocation();
			x = account.getX();
			y = account.getY();

		}

		if (location == null) {
			logger.fine("[DistanceMarker] no location, exiting");
			return null;
		}

		if (!item.hasLocation()) {

			logger.fine("[DistanceMarker] item has no location");
			return null;
		}

		final String distanceString = LocationUtil.getDistanceString(x, y, item);
		return super.string(context, params, distanceString);
	}

	@Override
	public String getDescription() {
		return "Shows the distance in miles between the location of the current user, active location, or location entered on search";
	}

	public Object getRegex() {
		return "distance";
	}

}
