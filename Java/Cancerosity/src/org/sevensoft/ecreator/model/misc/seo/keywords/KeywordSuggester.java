package org.sevensoft.ecreator.model.misc.seo.keywords;

import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jdom.JDOMException;
import org.sevensoft.commons.collections.bags.ArrayBag;
import org.sevensoft.commons.collections.bags.Bag;
import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.httpc.HttpMethod;
import org.sevensoft.commons.superstrings.english.CommonWords;
import org.sevensoft.ecreator.model.misc.seo.searchengines.SearchEngine;
import org.sevensoft.ecreator.model.misc.seo.searchengines.SearchException;
import org.sevensoft.ecreator.model.misc.seo.searchengines.SearchRequester;
import org.sevensoft.ecreator.model.misc.seo.searchengines.SearchResult;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 23 Jul 2006 07:20:43
 *
 */
public class KeywordSuggester {

	private static final Logger	logger	= Logger.getLogger("ecreator");

	private final SearchRequester	searchRequester;
	private final Pattern		keywordSplitPattern;
	private final Pattern		metaTagPattern;
	private final RequestContext	context;

	public KeywordSuggester(RequestContext context) throws SearchException {

		this.context = context;

		this.searchRequester = SearchEngine.YahooUK.getRequester(context);

		this.metaTagPattern = Pattern.compile("<meta name=[\"']?keywords[\"']?\\s+content=[\"']?(.+?)[\"']?\\s*?>", Pattern.DOTALL
				| Pattern.CASE_INSENSITIVE);
		this.keywordSplitPattern = Pattern.compile("\\s|,");
	}

	public List<String> getKeywords(String query) throws IOException, JDOMException {

		List<SearchResult> results = searchRequester.getResults(query);

		Bag bag = new ArrayBag();
		for (SearchResult result : results) {

			try {

				String url = result.getUrl();

				logger.fine("[YahooKeywordGenerator] url=" + url);

				HttpClient hc = new HttpClient(new URL(url), HttpMethod.Get);
				hc.setReadTimeout(10000);
				hc.setConnectTimeout(3000);
				hc.connect();

				String response = hc.getResponseString();
				if (response == null) {
					System.out.println("Response is null, continuing");
					continue;
				}

				Matcher matcher = metaTagPattern.matcher(response);
				if (matcher.find()) {

					String match = matcher.group(1);
					String[] keywords = keywordSplitPattern.split(match);

					for (String keyword : keywords) {

						keyword = keyword.trim().toLowerCase();
						if (keyword.length() > 0)
							bag.add(keyword);
					}

				} else {

					logger.fine("[YahooKeywordGenerator] Could not find meta tag in: " + url);

				}

			} catch (RuntimeException e) {
				e.printStackTrace();
			}
		}

		// strip out common words
		bag.removeAll(CommonWords.English);

		// strip out 2 letter words
		Iterator<String> iter = bag.iterator();
		while (iter.hasNext()) {
			if (iter.next().length() < 3)
				iter.remove();
		}

		// keep only first 10 keywords
		bag.retainMost(10);

		return bag.listDesc();
	}
}
