package org.sevensoft.ecreator.model.misc.location.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * helper class to sort out the postzon file
 * 
 * @author sks 28 Oct 2006 09:41:35
 *
 */
public class PostZonProcessor {

	public static void main(String[] args) throws IOException {

		new PostZonProcessor("c:/postzon.csv", "c:/postzon2.txt").run();
	}

	private final String	srcFilename;
	private final String	destFilename;

	public PostZonProcessor(String string, String string2) {
		this.srcFilename = string;
		this.destFilename = string2;
	}

	private void run() throws IOException {

		BufferedReader reader = new BufferedReader(new FileReader(srcFilename));
		BufferedWriter writer = new BufferedWriter(new FileWriter(destFilename));

		String string;
		boolean header = true;
		while ((string = reader.readLine()) != null) {

			if (header) {
				header = false;
				continue;
			}

			String[] fields = string.split(",");
			String postcode = fields[0].replace(" ", "").replace("\"", "").trim();
			String easting = fields[1].replace("\"", "").trim();
			String northing = fields[2].replace("\"", "").trim();

			if (easting == null || easting.length() == 0)
				continue;

			if (northing == null || northing.length() == 0)
				continue;

			writer.write(postcode + "\t" + easting + "0\t" + northing + "0\n");

		}

		writer.flush();
		writer.close();
		reader.close();
	}
}
