package org.sevensoft.ecreator.model.misc.seo.sku;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.attributes.Attribute;

import java.util.Map;

/**
 * User: Tanya
 * Date: 21.09.2012
 */
@Table("gmap_sku")
public class SkuArrtibutes extends EntityObject {

    public static SkuArrtibutes getSkuArrtibute(RequestContext context, ItemType itemType) {
        SkuArrtibutes attribute = SimpleQuery.get(context, SkuArrtibutes.class, "itemType", itemType);
        if (attribute == null) {
            attribute = new SkuArrtibutes(context, itemType);
        }
        return attribute;
    }

    public static void remove(RequestContext context) {
        SimpleQuery.empty(context, SkuArrtibutes.class);
    }

    public static void create(RequestContext context, Map<ItemType, Attribute> skuAttributes) {
        for (Map.Entry<ItemType, Attribute> entry : skuAttributes.entrySet()){
            new SkuArrtibutes(context, entry.getKey(), entry.getValue());
        }
    }

    /**
     * any itemType with IModule.Ordering
     */
    private ItemType itemType;
    /**
     * attribute of current itemType to store Sku code
     */
    private Attribute skuAttribute;

    protected SkuArrtibutes(RequestContext context) {
        super(context);
    }

    public SkuArrtibutes(RequestContext context, ItemType itemType) {
        super(context);
        this.itemType = itemType;
        save();
    }

    public SkuArrtibutes(RequestContext context, ItemType itemType, Attribute skuAttribute) {
        super(context);
        this.itemType = itemType;
        this.skuAttribute = skuAttribute;
        save();
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public Attribute getSkuAttribute() {
        return skuAttribute == null ? null : (Attribute) skuAttribute.pop();
    }

    public void setSkuAttribute(Attribute skuAttribute) {
        this.skuAttribute = skuAttribute;
    }
    
}
