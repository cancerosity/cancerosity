package org.sevensoft.ecreator.model.misc.currencies;

import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.interfaces.IGenericMarker;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * User: Tanya
 * Date: 26.09.2011
 */
public class CurrentPageMarker extends MarkerHelper implements IGenericMarker {

    public Object generate(RequestContext context, Map<String, String> params) {
        if (!Module.Currencies.enabled(context)) {
            return null;
        }

        StringBuilder sb = new StringBuilder(context.getServletPath());
        sb.append("?");
        if (context.getQueryString() != null) {
            if (context.getParameter("item") != null) {
                sb.append("item=");
                sb.append(context.getParameter("item"));
            } else if (context.getParameter("category") != null) {
                sb.append("category=");
                sb.append(context.getParameter("category"));
            } else
                sb.append(context.getQueryString());
        } else if (context.containsAttribute("category")) {
            sb.append("id=");
            sb.append(((Category) context.getAttribute("category")).getIdString());
        }

        return sb.toString();
    }

    public Object getRegex() {
        return "current_page";
    }

}
