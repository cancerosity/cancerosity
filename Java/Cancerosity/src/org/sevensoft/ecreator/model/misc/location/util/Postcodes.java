package org.sevensoft.ecreator.model.misc.location.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author sks 7 Feb 2007 09:20:37
 * 
 * Makes a postcode file containing only the postcodes prefixes we want
 *
 */
public class Postcodes {

	public static void main(String[] args) throws IOException {

		File input = new File("D:/locations/postcodes.txt");
		BufferedReader reader = new BufferedReader(new FileReader(input));

		File output = new File("D:/locations/postcodes_processed.txt");
		BufferedWriter writer = new BufferedWriter(new FileWriter(output));

		String line;
		int n = 0;
		while ((line = reader.readLine()) != null) {

			n++;

			line = line.replaceAll(",$", "");

			writer.write(line);
			writer.write("\n");

			if (n % 1000 == 0) {
				System.out.println(n + " postcodes done");
			}
		}

		writer.close();
		reader.close();
	}
}
