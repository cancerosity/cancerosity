package org.sevensoft.ecreator.model.misc.location;

import org.sevensoft.commons.gaia.UkMajorTowns;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 17 Sep 2006 00:08:42
 *
 */
public class MajorTownsTag {

	private final RequestContext	context;
	private String			any;
	private boolean			autoSubmit;
	private boolean			optional;

	public MajorTownsTag(RequestContext context) {
		this.context = context;
	}

	public final void setAny(String any) {
		this.any = any;
	}

	public void setAutoSubmit(boolean autoSubmit) {
		this.autoSubmit = autoSubmit;
	}

	public void setOptional(boolean optional) {
		this.optional = optional;
	}

	public String toString() {

		SelectTag tag = new SelectTag(context, "town");

		if (autoSubmit)
			tag.setAutoSubmit();

		if (optional) {
			if (any == null)
				any = "-Any-";
			tag.setAny(any);
		}

		tag.addOptions(UkMajorTowns.getPlaces());
		return tag.toString();
	}
}
