package org.sevensoft.ecreator.model.misc.seo;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 6 May 2006 09:11:32
 *
 */
public class SeoToolbar {

	private String	keywords;
	private String	description;

	public SeoToolbar(RequestContext context) {

		this.keywords = (String) context.getAttribute("keywords");
		this.description = (String) context.getAttribute("description_tag");
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(new TableTag("cpbar", "center").setId("cp_toolbar"));

		sb.append("<tr><td valign='middle'><span>Keywords</span> &nbsp; ");
		if (keywords != null) {
			sb.append(keywords);
		}
		sb.append("</td>");

		sb.append("<td valign='middle'><span>Description</span> &nbsp; ");
		if (description != null) {
			sb.append(description);
		}
		sb.append("</td></tr>");

		sb.append("</table>");

		return sb.toString();
	}
}
