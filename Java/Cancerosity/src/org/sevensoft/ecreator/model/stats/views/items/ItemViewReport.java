package org.sevensoft.ecreator.model.stats.views.items;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.items.Item;

/**
 * @author sks 22 Apr 2007 22:51:02
 * 
 * Total views per item for a particular date
 *
 */
public class ItemViewReport {

	private final Date	date;
	private final Item	item;
	private final int		total;

	public ItemViewReport(Date date, Item item, int total) {
		this.date = date;
		this.item = item;
		this.total = total;
	}

	public final Date getDate() {
		return date;
	}

	public final Item getItem() {
		return item;
	}

	public final int getTotal() {
		return total;
	}
}
