package org.sevensoft.ecreator.model.stats.visits;

import java.util.List;

import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.misc.geoip.GeoIp;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 7 Apr 2007 10:14:21
 *
 */
@Table("visitors")
public class Visitor extends EntityObject {

	public static String[]	bots	= new String[] { "NL-Crawler", "CazoodleBot", "msnbot", "googlebot", "yahoo! slurp", "shopwiki", "uptime-girl", "sbider",
			"w3c_validator", "sitesell.com", "baidu.com", "bigsearch.ca" };

	private static Visitor get(RequestContext context) {
		return SimpleQuery.get(context, Visitor.class, "sessionId", context.getSessionId(), "ipAddress", context.getRemoteIp());
	}

	public static void process(RequestContext context, String pageName) {

		// reject any request that contains an ignored useragent string
        String userAgent = context.getUserAgent();
        if (userAgent != null) {
            userAgent = userAgent.toLowerCase();

            for (String bot : bots) {

                if (userAgent.toLowerCase().contains(bot.toLowerCase())) {
                    return;
                }
            }
        }

		// get the page for this request including GET parameters
		String pageUrl = context.getGetRequest();

		// get the referrer for this page
		String referrer = context.getReferrer();

		// get the visitor matching this ip / session
		Visitor visitor = get(context);

		// if visitor is null then create one
		if (visitor == null) {
			visitor = new Visitor(context, true);
		}

		visitor.view(pageUrl, pageName, referrer);
	}

	/**
	 * Ip address for the visitor
	 */
    @Index()
	private String	ipAddress;

	/**
	 * Total number of visits for this visitor
	 */
	private int		visits;

	/**
	 * Session id 
	 */
	private String	sessionId;

	/**
	 * The timestamp when this visitor first came to the site
	 */
	private long	firstAccess;

	/**
	 * The time this visitor last had a visit to the site.
	 */
	private long	lastAccess;

	/**
	 * Duration of this session only, in millis
	 */
	private long	sessionDuration;

	/**
	 * Total number of page views
	 */
	private int		totalViews;

	/**
	 * Total duration of all visits, in millis
	 */
	private long	totalDuration;

	/**
	 * Number of views for the last or current session
	 */
	private int		sessionViews;

	private Country	country;

	protected Visitor(RequestContext context) {
		super(context);
	}

	public Visitor(RequestContext context, boolean create) {
		super(context);

		this.firstAccess = System.currentTimeMillis();

		this.ipAddress = context.getRemoteIp();
		this.sessionId = context.getSessionId();

		this.country = GeoIp.lookup(context, ipAddress);

		save();
	}

	public final Country getCountry() {
		return country;
	}

	/**
	 *	Returns the visit for this view 
	 */
	public Session getCurrentSession() {

		Query q = new Query(context, "select * from # where visitor=? and lastAccess>=?");
		q.setTable(Session.class);
		q.setParameter(this);
		q.setParameter(System.currentTimeMillis() - 1000l * 60 * 5);

		return q.get(Session.class);
	}

	public final long getFirstAccess() {
		return firstAccess;
	}

	public final String getIpAddress() {
		return ipAddress;
	}

	public final long getLastAccess() {
		return lastAccess;
	}

	public long getSessionDuration() {
		return sessionDuration;
	}

	public String getSessionDurationString() {
		return DateHelper.millisToString(sessionDuration);
	}

	public final String getSessionId() {
		return sessionId;
	}

	/**
	 * 
	 */
	public List<Session> getSessions() {
		Query q = new Query(context, "select * from # where visitor=? order by lastAccess desc");
		q.setParameter(this);
		q.setTable(Session.class);
		return q.execute(Session.class);
	}

	public int getSessionViews() {
		return sessionViews;
	}

	public long getTotalDuration() {
		return totalDuration;
	}

	public String getTotalDurationString() {
		return DateHelper.millisToString(totalDuration);
	}

	public int getTotalViews() {
		return totalViews;
	}

	public List<View> getViews() {
		Query q = new Query(context, "select * from # where visitor=? order by timestamp desc");
		q.setParameter(this);
		q.setTable(View.class);
		return q.execute(View.class);
	}

	public final int getVisits() {
		return visits;
	}

	public boolean hasCountry() {
		return country != null;
	}

	/**
	 *  Returns true if this visitor is still online - defined by their last visit being under 5 minutes ago
	 */
	public boolean isOnline() {
		return System.currentTimeMillis() - lastAccess < 1000l * 60 * 5;
	}

	public void view(String pageUrl, String pageName, String referrer) {

		// get current session
		Session session = getCurrentSession();

		// if session is null then create it
		if (session == null) {

			session = new Session(context, this, pageUrl, pageName, referrer);

			// creating a new session means updating visits
			this.visits++;

			// otherwise update session
		} else {

			session.update(pageUrl, pageName);

		}

		// set session vies
		this.sessionViews = session.getViews();
		this.sessionDuration = session.getDuration();

		// update total views
		this.totalViews++;

		// set last access and calculate total duration
		this.lastAccess = System.currentTimeMillis();

		// update total duration which is a sum of session durations
		Query q = new Query(context, "select sum(duration) from # where visitor=?");
		q.setTable(Session.class);
		q.setParameter(this);
		this.totalDuration = q.getLong();

		save();
	}

    public static List<Visitor> get(RequestContext context, Date start, Date end) {

        Query q = new Query(context, "select * from # where lastAccess>=? and lastAccess<=? order by lastAccess desc");
        q.setTable(Visitor.class);
        q.setParameter(start);
        q.setParameter(end);
        return q.execute(Visitor.class);
    }

    public static List<Visitor> getRecent(RequestContext context, Integer limit) {
        Query q = new Query(context, "select * from # order by id desc");
        q.setTable(Visitor.class);
        return q.execute(Visitor.class, limit);
    }

    public static List<Visitor> getAll(RequestContext context, Integer limit) {
        Query q = new Query(context, "select * from # order by id asc");
        q.setTable(Visitor.class);
        return q.execute(Visitor.class, limit);
    }
}
