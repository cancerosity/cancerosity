package org.sevensoft.ecreator.model.stats.listings;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.items.Item;

import java.util.List;

/**
 * @author Dmitry Lebedev
 *         Date: 05.11.2009
 */
public abstract class ListingHitCounter extends EntityObject {

    public static <E extends ListingHitCounter> E get(RequestContext context, Class<E> clazz, Date date, Item item) {
        Query q = new Query(context, "select * from # where date=? and item=?");
        q.setTable(clazz);
        q.setParameter(date);
        q.setParameter(item);
        return q.get(clazz);
    }

    /**
     * Returns a list of hit counters for SITE WIDE stats
     */
    public static <E extends ListingHitCounter> List<E> get(RequestContext context, Class<E> clazz, Date start, Date end, Item item) {
        Query q = new Query(context, "select * from # where date>=? and date<=? and item=? order by date desc");
        q.setTable(clazz);
        q.setParameter(start);
        q.setParameter(end);
        q.setParameter(item);
        return q.execute(clazz);
    }

    @Index()
    private Date date;

    @Index()
    private Item item;

    /**
     * The total (regardless) and the unique number of hits
     */
    private int total, unique;

    protected ListingHitCounter(RequestContext context) {
        super(context);
    }

    public ListingHitCounter(RequestContext context, Date date, Item item) {
        super(context);
        this.date = date;
        this.item = item;
    }

    public Date getDate() {
        return date;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setUnique(int unique) {
        this.unique = unique;
    }

    public int getUnique() {
        return unique;
    }

    /**
     *
     */
    public void inc(boolean incUnique) {

        total++;

        if (incUnique)
            unique++;

        save();
    }

}
