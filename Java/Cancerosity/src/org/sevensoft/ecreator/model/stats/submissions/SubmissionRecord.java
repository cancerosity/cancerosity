package org.sevensoft.ecreator.model.stats.submissions;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.Page;
import org.sevensoft.ecreator.model.stats.StatablePage;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 14 Aug 2006 17:34:37
 *
 * This class stores submission details for a particular submission
 */
@Table("reports_submissions")
public class SubmissionRecord extends EntityObject {

	/**
	 * Returns the last 'limit' submissions across the site 
	 */
	public static List<SubmissionRecord> get(RequestContext context, int limit) {

		Query q = new Query(context, "select * from # order by id desc");
		q.setTable(SubmissionRecord.class);
		return q.execute(SubmissionRecord.class, limit);

	}

	/**
	 *Returns all the records for any items owned by this member between the date params 
	 */
	public static List<SubmissionRecord> get(RequestContext context, Item member, Date start, Date end) {

		Query q = new Query(context, "select s.* from # s join # i on s.item=i.id where i.member=? and date>=? and date<=? order by id");
		q.setTable(SubmissionRecord.class);
		q.setTable(Item.class);
		q.setParameter(member);
		q.setParameter(start);
		q.setParameter(end);
		return q.execute(SubmissionRecord.class);
	}

	public static List<SubmissionRecord> get(RequestContext context, StatablePage page, Date start, Date end) {

		Query q = new Query(context, "select * from # where " + page.getClass().getSimpleName() + "=? and date>=? and date<=? order by id");
		q.setTable(SubmissionRecord.class);
		q.setParameter(page);
		q.setParameter(start);
		q.setParameter(end);
		return q.execute(SubmissionRecord.class);
	}

	/**
	 * Date of submission
	 */
	@Index()
	private DateTime	date;

	/**
	 * The form the member received
	 */
	private int		form;

	/*
	 * Ip address of submitter
	 */
	private String	ipAddress;

	/**
	 * Name of form in case form is deleted
	 */
	private String	formName;

	/**
	 * 
	 */
	@Index()
	private Item	item;

	/**
	 * 
	 */
	@Index()
	private Category	category;

	private String	pageName;

	private SubmissionRecord(RequestContext context) {
		super(context);
	}

	public SubmissionRecord(RequestContext context, Page page, Form form, String ipAddress) {
		super(context);

		this.form = form.getId();
		this.ipAddress = ipAddress;
		this.formName = form.getName();
		this.date = new DateTime();

		if (page instanceof Item) {
			item = (Item) page;
			this.pageName = page.getName();

		} else if (page instanceof Category) {
			category = (Category) page;
			this.pageName = page.getName();

		}

		save();
	}

	public DateTime getDate() {
		return date;
	}

	public String getFormName() {
		return formName;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * 
	 */
	public String getPageName() {
		return pageName;
	}

}
