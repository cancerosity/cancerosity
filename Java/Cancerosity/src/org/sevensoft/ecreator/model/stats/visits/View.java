package org.sevensoft.ecreator.model.stats.visits;

import org.sevensoft.ecreator.model.stats.browsers.Browser;
import org.sevensoft.ecreator.model.stats.os.OperatingSystem;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 11 Aug 2006 07:20:54
 * 
 * Stores details of a particular 'view' or 'hit' to a reportable page on the site
 *
 */
@Table("visitors_views")
public class View extends EntityObject {

	/**
	 * The user agent string used for this view
	 */
	private String	userAgent;

	/**
	 * timestamp of access
	 */
	private long	timestamp;

	/**
	 * The page the user visited
	 */
	private String	pageUrl;

	private String	pageName;

	private Visitor	visitor;

	private Session	session;

	private View(RequestContext context) {
		super(context);
	}

	public View(RequestContext context, Session session, String pageUrl, String pageName) {
		super(context);

		this.visitor = session.getVisitor();
		this.session = session;

		this.pageUrl = pageUrl;
		this.pageName = pageName;
		this.userAgent = context.getUserAgent();

		this.timestamp = System.currentTimeMillis();

		save();
	}

	public Browser getBrowser() {
		return Browser.getBrowser(userAgent);
	}

	public OperatingSystem getOs() {
		return OperatingSystem.getOs(getUserAgent());
	}

	public final String getPageName() {
		return pageName;
	}

	public String getPageUrl() {
		return pageUrl;
	}

	public final Session getSession() {
		return (Session) (session == null ? null : session.pop());
	}

	public final long getTimestamp() {
		return timestamp;
	}

	public final String getUserAgent() {
		return userAgent;
	}

	public final Visitor getVisitor() {
		return (Visitor) (visitor == null ? null : visitor.pop());
	}

	public boolean hasPageName() {
		return pageName != null;
	}

}
