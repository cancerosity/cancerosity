package org.sevensoft.ecreator.model.stats.seo;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import org.sevensoft.jeezy.http.Urls;

/**
 * @author sks 13 Aug 2006 23:07:44
 *
 */
public enum SearchEngine {

	Google() {

		@Override
		public String parse(String referrer) throws IOException {

			Map<String, String> params = Urls.getParams(new URL(referrer));
			return params.get("q");

		}

		@Override
		public String toString() {
			return "Google.com";
		}

		@Override
		protected String[] urls() {
			return new String[] { "google.com" };
		}
	},
	GoogleUk() {

		@Override
		public String parse(String referrer) throws MalformedURLException {

			Map<String, String> params = Urls.getParams(new URL(referrer));
			return params.get("q");
		}

		@Override
		public String toString() {
			return "Google.co.uk";
		}

		@Override
		protected String[] urls() {
			return new String[] { "google.co.uk" };
		}

	},
	GoogleOther() {

		@Override
		public String parse(String referrer) throws MalformedURLException {
			Map<String, String> params = Urls.getParams(new URL(referrer));
			return params.get("q");
		}

		@Override
		public String toString() {
			return "Google (Other)";
		}

		@Override
		protected String[] urls() {
			return new String[] { "google.com.au", "google.ca" };
		}

	},
	MsnUk() {

		@Override
		public String parse(String referrer) throws IOException {

			Map<String, String> params = Urls.getParams(new URL(referrer));
			return params.get("q");
		}

		@Override
		public String toString() {
			return "MSN Search UK";
		}

		@Override
		protected String[] urls() {
			return new String[] { "search.msn.co.uk" };
		}
	},
	Yahoo() {

		@Override
		public String parse(String referrer) throws IOException {
			Map<String, String> params = Urls.getParams(new URL(referrer));
			return params.get("p");
		}

		@Override
		public String toString() {
			return "Yahoo.co.uk";
		}

		@Override
		protected String[] urls() {
			return new String[] { "uk.search.yahoo.com" };
		}
	},
	Technorati() {

		@Override
		public String parse(String referrer) {
			return null;
		}

		@Override
		public String toString() {
			return "Technorati";
		}

		@Override
		protected String[] urls() {
			return new String[] { "technorati.co.uk" };
		}
	};

	/**
	 * Checks if this referrer string was for a recongised search engine
	 */
	public static SearchEngine get(String referrer) {

		if (referrer == null)
			return null;

		referrer = referrer.toLowerCase();

		for (SearchEngine engine : SearchEngine.values()) {

			for (String url : engine.urls()) {

				if (referrer.startsWith("http://" + url) || referrer.startsWith("http://www." + url))
					return engine;

			}

		}

		return null;
	}

	/**
	 * Parses the referrer string to find out what the search terms where.
	 * @throws IOException 
	 */
	public abstract String parse(String referrer) throws IOException;

	protected abstract String[] urls();
}
