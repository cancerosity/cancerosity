package org.sevensoft.ecreator.model.stats.listings;

import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.items.Item;

import java.util.List;

/**
 * @author Dmitry Lebedev
 *         Date: 05.11.2009
 */
@Table("listings_hits_daily")
public class ListingHitCounterDaily extends ListingHitCounter {

    public static ListingHitCounterDaily get(RequestContext context, Date day, Item item) {
        return ListingHitCounter.get(context, ListingHitCounterDaily.class, day, item);
    }

    public static List<ListingHitCounterDaily> get(RequestContext context, Date start, Date end, Item item) {
        return ListingHitCounter.get(context, ListingHitCounterDaily.class, start, end, item);
    }

    /**
     * Delete off stats older than param days
     */
    public static void reap(RequestContext context, int days) {
        new Query(context, "delete from # where date<?").setTable(ListingHitCounterDaily.class).setParameter(new Date().removeDays(days)).run();
    }

    private ListingHitCounterDaily(RequestContext context) {
        super(context);
    }

    public ListingHitCounterDaily(RequestContext context, Date day, Item item) {
        super(context, day, item);
    }
}
