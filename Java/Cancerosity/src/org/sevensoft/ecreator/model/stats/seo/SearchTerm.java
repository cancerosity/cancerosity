package org.sevensoft.ecreator.model.stats.seo;

import java.util.List;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Aug 2006 22:00:55
 *
 */
@Table("reports_terms")
public class SearchTerm extends EntityObject {

	public static List<SearchTerm> get(RequestContext context, int limit) {
		Query q = new Query(context, "select * from # order by id desc");
		q.setTable(SearchTerm.class);
		return q.execute(SearchTerm.class, limit);
	}

	/**
	 * The keywords entered in the search engine, in full, and in the order it was entered
	 */
	private String		term;

	@Index()
	private String		ipAddress;

	private SearchEngine	engine;

	private DateTime		date;

	private SearchTerm(RequestContext context) {
		super(context);
	}

	public SearchTerm(RequestContext context, SearchEngine engine, String term, String ipAddress) {
		super(context);

		this.engine = engine;
		this.term = term;
		this.ipAddress = ipAddress;
		this.date = new DateTime();

		save();
	}

	public DateTime getDate() {
		return date;
	}

	public SearchEngine getEngine() {
		return engine;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public String getTerm() {
		return term;
	}
}
