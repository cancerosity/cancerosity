package org.sevensoft.ecreator.model.stats.submissions;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.QueryBuilder;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 14 Aug 2006 17:34:37
 *
 * This class counts the number of times any form was submitted on a particular page
 */
@Table("reports_submissions_members_monthly")
public class AccountSubmissionsCounter extends EntityObject {

	private static AccountSubmissionsCounter get(RequestContext context, Date date, Form form, Item member) {

		QueryBuilder b = new QueryBuilder(context);
		b.select("*");
		b.from("#", AccountSubmissionsCounter.class);
		b.clause("member=?", member);
		b.clause("date=?", date);
		b.clause("form=?", form);

		return b.toQuery().get(AccountSubmissionsCounter.class);
	}

	/**
	 * Returns all submissions for members for this month
	 */
	public static List<AccountSubmissionsCounter> get(RequestContext context, Date month, int limit) {

		Query q = new Query(context, "select * from # where date=? order by total desc");
		q.setTable(AccountSubmissionsCounter.class);
		q.setParameter(month);
		return q.execute(AccountSubmissionsCounter.class, limit);
	}

	public static void submit(RequestContext context, Form form, Item member) {

		Date date = new Date().beginMonth();
		AccountSubmissionsCounter c = AccountSubmissionsCounter.get(context, date, form, member);
		if (c == null)
			c = new AccountSubmissionsCounter(context, form, member);
		c.inc();
	}

	/**
	 * Date of submission
	 */
	@Index()
	private Date	date;

	/**
	 * The member who received a submission
	 */
	@Index()
	private Item	member;

	/**
	 * The form the member received
	 */
	@Index()
	private Form	form;

	private int		total;

	protected AccountSubmissionsCounter(RequestContext context) {
		super(context);
	}

	public AccountSubmissionsCounter(RequestContext context, Form form, Item member) {
		super(context);

		this.form = form;
		this.member = member;
		this.date = new Date().beginMonth();

		save();
	}

	public Date getDate() {
		return date;
	}

	public Form getForm() {
		return form.pop();
	}

	public Item getMember() {
		return member.pop();
	}

	public int getTotal() {
		return total;
	}

	public void inc() {
		total++;
		save();
	}
}
