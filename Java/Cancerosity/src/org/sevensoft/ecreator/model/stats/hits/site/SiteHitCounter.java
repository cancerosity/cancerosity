package org.sevensoft.ecreator.model.stats.hits.site;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Aug 2006 22:17:12
 * 
 * Counts the number of hits on the site 
 *
 */
public abstract class SiteHitCounter extends EntityObject {

	public static <E extends SiteHitCounter> E get(RequestContext context, Class<E> clazz, Date date) {
		Query q = new Query(context, "select * from # where date=?");
		q.setTable(clazz);
		q.setParameter(date);
		return q.get(clazz);
	}

	/**
	 * Returns a list of hit counters for SITE WIDE stats
	 */
	public static <E extends SiteHitCounter> List<E> get(RequestContext context, Class<E> clazz, Date start, Date end) {
		Query q = new Query(context, "select * from # where date>=? and date<=? order by date desc");
		q.setTable(clazz);
		q.setParameter(start);
		q.setParameter(end);
		return q.execute(clazz);
	}

	@Index()
	private Date	date;

	/**
	 * The total (regardless) and the unique number of hits
	 */
	private int		total, unique;

	protected SiteHitCounter(RequestContext context) {
		super(context);
	}

	public SiteHitCounter(RequestContext context, Date date) {
		super(context);
		this.date = date;
	}

	public Date getDate() {
		return date;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public void setUnique(int unique) {
		this.unique = unique;
	}

	public int getUnique() {
		return unique;
	}

	/**
	 * 
	 */
	public void inc(boolean incUnique) {

		total++;

		if (incUnique)
			unique++;

		save();
	}

}
