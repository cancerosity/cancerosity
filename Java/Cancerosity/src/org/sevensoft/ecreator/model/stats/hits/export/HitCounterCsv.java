package org.sevensoft.ecreator.model.stats.hits.export;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import org.sevensoft.ecreator.model.stats.hits.site.SiteHitCounter;

import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.LineProviderSupport;

/**
 * @author sks 15 Aug 2006 06:54:32
 *
 */
public class HitCounterCsv {

	private final List<SiteHitCounter>	counters;

	public HitCounterCsv(List<SiteHitCounter> counters) {
		this.counters = counters;
	}

	public void write(OutputStream stream) {

		CsvManager csvman = new CsvManager();

		try {

			csvman.save(stream, new LineProviderSupport() {

				@Override
				public void endSaveImpl() throws Exception {
				}

				@Override
				public boolean hasNextFieldImpl() throws Exception {
					return false;
				}

				@Override
				public boolean hasNextLineImpl() throws Exception {
					return false;
				}

				@Override
				public String nextFieldImpl() throws Exception {
					return null;
				}

				@Override
				public void nextLineImpl() throws Exception {
				}

				@Override
				public void startSaveImpl() throws Exception {
				}
			});

		} finally {

			if (stream != null)
				try {
					stream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
}
