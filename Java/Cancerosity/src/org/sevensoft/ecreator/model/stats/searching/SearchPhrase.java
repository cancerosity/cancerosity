package org.sevensoft.ecreator.model.stats.searching;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 15 Mar 2007 08:11:16
 *
 */
@Table("reports_search_phrases")
public class SearchPhrase extends EntityObject {

	public static List<String> getPopular(RequestContext context, Date month, int limit) {

		Query q = new Query(context, "select phrase from # where month=? order by count desc");
		q.setTable(SearchPhrase.class);
		q.setParameter(month.beginMonth());
		return q.getStrings(limit);
	}

	public static void process(RequestContext context, String phrase) {

		Date month = new Date().beginMonth();

		SearchPhrase st = SimpleQuery.get(context, SearchPhrase.class, "phrase", phrase, "month", month);
		if (st == null) {
			st = new SearchPhrase(context, phrase, month);
		}
		st.inc();

	}

	private String	phrase;
	private Date	month;
	private int		count;

	private SearchPhrase(RequestContext context) {
		super(context);
	}

	private SearchPhrase(RequestContext context, String phrase, Date month) {
		super(context);

		this.phrase = phrase;
		this.month = month;

	}

	private void inc() {
		count++;
		save();
	}

}
