package org.sevensoft.ecreator.model.stats.views.items;

import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.stats.browsers.Browser;
import org.sevensoft.ecreator.model.stats.os.OperatingSystem;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 22 Apr 2007 22:42:26
 *
 */
@Table("stats_hits_items")
public class ItemView extends EntityObject {

	/**
	 * 
	 */
	private String	ipAddress;

	/**
	 * 
	 */
	private long	timestamp;

	/**
	 * 
	 */
	private Item	item;

	/**
	 * The full referral url
	 */
	private String	referrer;

	private String	sessionId;

	/**
	 * The user agent string used for this view
	 */
	private String	userAgent;

	private ItemView(RequestContext context) {
		super(context);
	}

	public ItemView(RequestContext context, Item item) {
		super(context);

		this.ipAddress = context.getRemoteIp();
		this.sessionId = context.getSessionId();
		this.item = item;
		this.referrer = context.getReferrer();
		this.timestamp = System.currentTimeMillis();
		this.userAgent = context.getUserAgent();

		save();
	}

	public Browser getBrowser() {
		return Browser.getBrowser(userAgent);
	}

	public final String getIpAddress() {
		return ipAddress;
	}

	public final Item getItem() {
		return item;
	}

	public OperatingSystem getOs() {
		return OperatingSystem.getOs(getUserAgent());
	}

	public final String getReferrer() {
		return referrer;
	}

	public final String getSessionId() {
		return sessionId;
	}

	public final long getTimestamp() {
		return timestamp;
	}

	public final String getUserAgent() {
		return userAgent;
	}

}
