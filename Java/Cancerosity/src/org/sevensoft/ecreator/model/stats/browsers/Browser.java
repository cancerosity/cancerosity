package org.sevensoft.ecreator.model.stats.browsers;

/**
 * @author sks 3 Jul 2006 11:09:27
 *
 */
public enum Browser {

	IE4, IE5, IE6, Firefox1, Mozilla, Opera9, Opera8, Other, IE7, Konqueror, Safari, Firefox2, Firefox3, iOS;

	/**
	 * Returns the browser from the user agent header
	 */
	public static Browser getBrowser(String userAgent) {

		if (userAgent == null) {
			return Other;
		}

        userAgent = userAgent.toLowerCase();

		if (userAgent.contains("Safari/".toLowerCase())) {
			return Safari;
		}

		if (userAgent.contains("Firefox/1".toLowerCase())) {
			return Firefox1;
		}

		if (userAgent.contains("Firefox/2".toLowerCase())) {
			return Firefox2;
		}

        if (userAgent.contains("Firefox/3".toLowerCase())) {
            return Firefox3;
        }

        if (userAgent.contains("MSIE 7".toLowerCase())) {
			return IE7;
		}

		if (userAgent.contains("MSIE 6".toLowerCase())) {
			return IE6;
		}

		if (userAgent.contains("MSIE 5".toLowerCase())) {
			return IE5;
		}

		if (userAgent.contains("MSIE 4".toLowerCase())) {
			return IE4;
		}

		if (userAgent.contains("Konqueror/".toLowerCase())) {
			return Konqueror;
		}

		if (userAgent.contains("Opera/8".toLowerCase())) {
			return Opera8;
		}

		if (userAgent.contains("Opera/9".toLowerCase())) {
			return Opera9;
		}

        if (userAgent.contains("iPhone".toLowerCase()) || userAgent.contains("iPad".toLowerCase())) {
            return iOS;
        }


		return Other;
	}
}
