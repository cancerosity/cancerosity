package org.sevensoft.ecreator.model.stats.hits.site;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Aug 2006 22:07:29
 *
 */
@Table("reports_hits_monthly")
public class SiteHitCounterMonthly extends SiteHitCounter {

	public static SiteHitCounterMonthly get(RequestContext context, Date date) {
		return SiteHitCounter.get(context, SiteHitCounterMonthly.class, date);
	}

	public static List<SiteHitCounterMonthly> get(RequestContext context, Date start, Date end) {
		return SiteHitCounter.get(context, SiteHitCounterMonthly.class, start, end);
	}

	private SiteHitCounterMonthly(RequestContext context) {
		super(context);
	}

	public SiteHitCounterMonthly(RequestContext context, Date day) {
		super(context, day);
	}

}
