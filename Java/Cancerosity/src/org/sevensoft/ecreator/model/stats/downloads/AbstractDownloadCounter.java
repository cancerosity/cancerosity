package org.sevensoft.ecreator.model.stats.downloads;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 9 Mar 2007 16:50:46
 *
 */
public class AbstractDownloadCounter extends EntityObject {

	private Attachment	attachment;
	private int			full, previews;
	private String		page;
	private Date		date;
	private String		filename;

	protected AbstractDownloadCounter(RequestContext context) {
		super(context);
	}

	protected AbstractDownloadCounter(RequestContext context, Attachment attachment, Date date) {
		super(context);
		this.attachment = attachment;
		this.date = date;
		this.filename = attachment.getFilename();

		if (attachment.hasItem()) {

			this.page = attachment.getItem().getName();

		} else if (attachment.hasCategory()) {

			this.page = attachment.getCategory().getName();
		}
	}

	public final Attachment getAttachment() {
		return attachment.pop();
	}

	public final Date getDate() {
		return date;
	}

	public final String getFilename() {
		return filename;
	}

	public final int getFull() {
		return full;
	}

	public final String getPage() {
		return page;
	}

	public final int getPreviews() {
		return previews;
	}

	private void incFull() {
		full++;
	}

	private void incPreviews() {
		previews++;
	}

	protected void process(boolean preview) {

		if (preview) {
			incPreviews();
		} else {
			incFull();
		}

		save();
	}

}
