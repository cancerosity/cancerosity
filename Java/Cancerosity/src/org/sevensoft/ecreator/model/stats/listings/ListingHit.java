package org.sevensoft.ecreator.model.stats.listings;

import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.stats.browsers.Browser;

import java.util.List;

/**
 * @author Dmitry Lebedev
 *         Date: 05.11.2009
 */
@Table("listings_hits")
public class ListingHit extends EntityObject {

    public static List<ListingHit> getCurrent(RequestContext context, int limit) {
        Query q = new Query(context, "select * from # order by id desc");
        q.setTable(ListingHit.class);
        return q.execute(ListingHit.class, limit);
    }

    /**
     * Returns the number of hits for this per hour
     */
    public static int getHourlyCount(RequestContext context, String remoteIp) {

        Query q = new Query(context, "select count(*) from # where ipAddress=? and date>?");
        q.setTable(ListingHit.class);
        q.setParameter(remoteIp);
        q.setParameter(System.currentTimeMillis() - (1000l * 60 * 60));
        return q.getInt();
    }

    public static List<ListingHit> getRange(RequestContext context, Date start, Date end, Item item) {
        Query q = new Query(context, "select * from # where date>=? and date<? and item=? order by date desc");
        q.setTable(ListingHit.class);
        q.setParameter(start);
        q.setParameter(end);
        q.setParameter(item);
        return q.execute(ListingHit.class);
    }

    public static void reap(RequestContext context, int i) {
        new Query(context, "delete from # where date<?").setTable(ListingHit.class).setParameter(new Date().removeDays(i)).run();
    }

    /**
     * The full referral url
     */
    private String referrer;

    /**
     * The users ip
     */
    private String ipAddress;

    /**
     * The user agent string used for this view
     */
    private String userAgent;

    /**
     * timestamp of access
     */
    private Date date;

    private String sessionId;

    /**
     * The page the user visited
     */
    private String page;

    private Item account;

    private Item item;

    private ListingHit(RequestContext context) {
        super(context);
    }

    public ListingHit(RequestContext context, String referrer, String ipAddress, String page, Browser browser, String os, Item account, Item item) {
        super(context);

        this.referrer = referrer;
        this.ipAddress = ipAddress;
        this.page = page;
        this.userAgent = context.getUserAgent();
        this.account = account;
        this.item = item;
        this.date = new Date(System.currentTimeMillis());

        save();
    }

    public Item getAccount() {
        return account;
    }

    public final Date getDate() {
        return date;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public Item getItem() {
        return item;
    }

    public String getPage() {
        return page;
    }

    public String getReferrer() {
        return referrer;
    }

    public final String getUserAgent() {
        return userAgent;
    }

    public boolean hasReferrer() {
        return referrer != null;
    }

}
