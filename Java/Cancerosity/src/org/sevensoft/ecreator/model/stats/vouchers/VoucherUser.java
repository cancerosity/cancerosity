package org.sevensoft.ecreator.model.stats.vouchers;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.ecom.vouchers.Voucher;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;
import org.sevensoft.commons.superstrings.comparators.NaturalStringComparator;

import java.util.List;

/**
 * User: Tanya
 * Date: 06.09.2012
 */
@Table("vouchers_users")
public class VoucherUser extends EntityObject implements Comparable<VoucherUser> {

    private Voucher voucher;
    private Item account;
    private ListingPackage listingPackage;

    public static VoucherUser getUser(RequestContext context, Voucher voucher, Item account, ListingPackage listingPackage) {
        VoucherUser voucherUser;
        voucherUser = SimpleQuery.get(context, VoucherUser.class, "voucher", voucher, "account", account, "listingpackage", listingPackage);
        if (voucherUser == null) {
            voucherUser = new VoucherUser(context, voucher, account, listingPackage);
        }

        return voucherUser;

    }

    public static List<VoucherUser> getVoucherUsers(RequestContext context, Voucher voucher) {
        return SimpleQuery.execute(context, VoucherUser.class, "voucher", voucher);
    }

    protected VoucherUser(RequestContext context) {
        super(context);
    }

    public VoucherUser(RequestContext context, Voucher voucher, Item account, ListingPackage listingPackage) {
        super(context);
        this.voucher = voucher;
        this.account = account;
        this.listingPackage = listingPackage;

        save();
    }

    public Item getAccount() {
        return account.pop();   //can not be NULL
    }

    public String getListingPackage() {
        return ((ListingPackage) listingPackage.pop()).getName();
    }

    public int compareTo(VoucherUser voucherUser) {
        if (getAccount().getLastName() != null && voucherUser.getAccount().getLastName() != null) {
            return NaturalStringComparator.instance.compare(getAccount().getLastName(), voucherUser.getAccount().getLastName());
        }
        return 0;
    }
}
