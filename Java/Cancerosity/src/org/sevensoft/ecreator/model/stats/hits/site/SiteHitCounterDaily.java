package org.sevensoft.ecreator.model.stats.hits.site;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Aug 2006 22:07:29
 *
 */
@Table("reports_hits_daily")
public class SiteHitCounterDaily extends SiteHitCounter {

	public static SiteHitCounterDaily get(RequestContext context, Date day) {
		return SiteHitCounter.get(context, SiteHitCounterDaily.class, day);
	}

	public static List<SiteHitCounterDaily> get(RequestContext context, Date start, Date end) {
		return SiteHitCounter.get(context, SiteHitCounterDaily.class, start, end);
	}

	/**
	 * Delete off stats older than param days 
	 */
	public static void reap(RequestContext context, int days) {
		new Query(context, "delete from # where date<?").setTable(SiteHitCounterDaily.class).setParameter(new Date().removeDays(days)).run();
	}

	private SiteHitCounterDaily(RequestContext context) {
		super(context);
	}

	public SiteHitCounterDaily(RequestContext context, Date day) {
		super(context, day);
	}

}
