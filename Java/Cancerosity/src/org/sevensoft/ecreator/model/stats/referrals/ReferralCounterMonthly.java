package org.sevensoft.ecreator.model.stats.referrals;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Aug 2006 22:43:29
 * 
 * Counts referrals per month
 *
 */
@Table("reports_referrals_counter")
public class ReferralCounterMonthly extends EntityObject {

	public static List<ReferralCounterMonthly> get(RequestContext context, Date month, int limit) {
		Query q = new Query(context, "select * from # where date=? order by total desc");
		q.setTable(ReferralCounterMonthly.class);
		q.setParameter(month);
		return q.execute(ReferralCounterMonthly.class, limit);
	}

	public static ReferralCounterMonthly get(RequestContext context, String hostname, Date month) {
		Query q = new Query(context, "select * from # where hostname=? and date=?");
		q.setTable(ReferralCounterMonthly.class);
		q.setParameter(hostname);
		q.setParameter(month);
		return q.get(ReferralCounterMonthly.class);
	}

	/**
	 * The hostname of the referrer
	 */
	private String	hostname;

	/**
	 * 
	 */
	private int		total;

	/**
	 * The month in question
	 */
	private Date	date;

	private ReferralCounterMonthly(RequestContext context) {
		super(context);
	}

	public ReferralCounterMonthly(RequestContext context, String hostname) {
		super(context);
		this.hostname = hostname;
		this.date = new Date().beginMonth();
		save();
	}

	public Date getDate() {
		return date;
	}

	public String getHostname() {
		return hostname;
	}

	public int getTotal() {
		return total;
	}

	public void inc() {
		total++;
		save();
	}

}
