package org.sevensoft.ecreator.model.stats.hits.site;

import java.util.List;

import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 7 Apr 2007 08:56:42
 *
 */
public class HitUtil {

	/**
	 * Returns a list of Hits for users who are still currently on the site.
	 * We do this by including anyone browsing within the past 5 minutes
	 * 
	 */
	public static List<SiteHit> getCurrent(RequestContext context, int start) {

		int limit = 20;

		Query q = new Query(context, "select * from # where date>? order by id desc");
		q.setTable(SiteHit.class);
		q.setParameter(System.currentTimeMillis() - (1000l * 60 * 5));
		return q.execute(SiteHit.class, start, limit);

	}

}
