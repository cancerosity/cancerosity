package org.sevensoft.ecreator.model.stats.ips;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.stats.StatablePage;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Aug 2006 22:10:40
 * 
 * Stores lists of ips used per date across any page object
 *
 */
@Table("reports_ips_page")
@SuppressWarnings("unused")
public class IpUsedPage extends EntityObject {

	/**
	 * Returns true if this ip is unique for the date range for this page
	 */
	public static boolean isUnique(RequestContext context, Date month, String ipAddress, StatablePage page) {
		Query q = new Query(context, "select count(*) from # where date=? and " + page.getClass().getSimpleName() + "=? and ipaddress=?");
		q.setTable(IpUsedPage.class);
		q.setParameter(month);
		q.setParameter(page);
		q.setParameter(ipAddress);
		return q.getInt() == 0;
	}

	@Index()
	private Date	date;

	@Index()
	private Category	category;

	@Index()
	private Item	item;

	@Index()
	private String	ipAddress;

	/**
	 *
	 */
	protected IpUsedPage(RequestContext context) {
		super(context);
	}

	public IpUsedPage(RequestContext context, Date date, String ipAddress, StatablePage page) {
		super(context);

		this.date = date;
		this.ipAddress = ipAddress;

		if (page instanceof Category)
			category = (Category) page;

		else if (page instanceof Item)
			item = (Item) page;

		else
			throw new RuntimeException();

		save();
	}

	public Date getDate() {
		return date;
	}

	public String getIpAddress() {
		return ipAddress;
	}

}
