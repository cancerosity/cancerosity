package org.sevensoft.ecreator.model.stats;

import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 14 Aug 2006 06:59:36
 * 
 *  A recorder of stats
 *
 */
public interface StatablePage {

	/**
	 * Returns a link to this recorders admin page
	 */
	public Link getEditRelativeLink();

	/**
	 * 
	 */
	public String getName();

	/**
	 * 
	 */
	public String getUrl();

}
