package org.sevensoft.ecreator.model.stats.hits.pages;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.stats.StatablePage;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.QueryBuilder;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Aug 2006 22:07:29
 * 
 *         Monthly record of hits on a specific element
 * 
 */
@Table("reports_hits_pages_monthly")
public class PageHitCounterMonthly extends EntityObject {

	/**
	 * Returns the <param>limit</param> most popular recorders for the particular date passed in.
	 */
	public static List<PageHitCounterMonthly> get(RequestContext context, Date date, int limit) {

		Query q = new Query(context, "select * from # where date=? order by total desc");
		q.setTable(PageHitCounterMonthly.class);
		q.setParameter(date);
		return q.execute(PageHitCounterMonthly.class, limit);
	}

    public static List<PageHitCounterMonthly> get(RequestContext context, Date start, Date end, Item item) {
        Query q = new Query(context, "select * from # where date>=? and date<=? and item=? order by date desc");
        q.setTable(PageHitCounterMonthly.class);
        q.setParameter(start);
        q.setParameter(end);
        q.setParameter(item);
        return q.execute(PageHitCounterMonthly.class);
    }

	public static List<PageHitCounterMonthly> getForAccount(RequestContext context, Item account) {

		List<PageHitCounterMonthly> counters = new ArrayList();
		for (Item listing : account.getItems()) {
			counters.addAll(getForItem(context, listing, 1));
		}
		return counters;
	}

	public static List<PageHitCounterMonthly> getForItem(RequestContext context, Item item, int limit) {

		Query q = new Query(context, "select * from # where item=? order by date desc");
		q.setTable(PageHitCounterMonthly.class);
		q.setParameter(item);
		return q.execute(PageHitCounterMonthly.class, limit);
	}

	public static PageHitCounterMonthly get(RequestContext context, StatablePage recorder, Date date) {

		QueryBuilder b = new QueryBuilder(context);
		b.select("*");
		b.from("#", PageHitCounterMonthly.class);
		b.clause(recorder.getClass().getSimpleName() + "=?", recorder);
		b.clause("date=?", date);

		return b.toQuery().get(PageHitCounterMonthly.class);
	}

	@Index()
	private Date	date;

	/**
	 * The total number of hits
	 */
	@Index()
	private int		total;

	/**
	 * The owning item for this hit counter
	 */
	@Index()
	private Item	item;

	/**
	 * The owning category for this hit counter
	 */
	@Index()
	private Category	category;

	private int		uniques;

	private PageHitCounterMonthly(RequestContext context) {
		super(context);
	}

	public PageHitCounterMonthly(RequestContext context, StatablePage recorder, Date date) {
		super(context);

		this.date = date;

		if (recorder instanceof Category)
			category = (Category) recorder;

		else if (recorder instanceof Item)
			item = (Item) recorder;

		else
			throw new RuntimeException();
	}

	public Category getCategory() {
		return (Category) (category == null ? null : category.pop());
	}

	public Date getDate() {
		return date;
	}

	public StatablePage getElement() {

		if (item != null) {
			return getItem();
		}

		if (category != null) {
			return getCategory();
		}

		return null;
	}

	public Item getItem() {
		return (Item) (item == null ? null : item.pop());
	}

	public int getTotal() {
		return total;
	}

	public int getUniques() {
		return uniques;
	}

	public void inc(boolean isUniqueMonthly) {

		total++;
		if (isUniqueMonthly)
			uniques++;

		save();
	}

}
