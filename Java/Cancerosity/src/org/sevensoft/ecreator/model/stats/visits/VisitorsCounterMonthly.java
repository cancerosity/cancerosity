package org.sevensoft.ecreator.model.stats.visits;

import org.sevensoft.ecreator.model.stats.hits.site.SiteHitCounter;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.commons.samdate.Date;

import java.util.List;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
@Table("reports_visitors_monthly")
public class VisitorsCounterMonthly extends SiteHitCounter {

    protected VisitorsCounterMonthly(RequestContext context) {
        super(context);
    }

    public VisitorsCounterMonthly(RequestContext context, Date date) {
        super(context, date);
    }

    public static VisitorsCounterMonthly get(RequestContext context, Date day) {
		return SiteHitCounter.get(context, VisitorsCounterMonthly.class, day);
	}

	public static List<VisitorsCounterMonthly> get(RequestContext context, Date start, Date end) {
		return SiteHitCounter.get(context, VisitorsCounterMonthly.class, start, end);
	}

}
