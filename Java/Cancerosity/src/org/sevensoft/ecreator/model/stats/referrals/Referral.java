package org.sevensoft.ecreator.model.stats.referrals;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Aug 2006 22:05:19
 *
 * This class stores details of a particular referral
 */
@Table("reports_referrals")
public class Referral extends EntityObject {

	public static List<Referral> get(RequestContext context, int start, int limit) {
		Query q = new Query(context, "select * from # order by id desc limit ?, ?");
		q.setTable(Referral.class);
        q.setParameter(start);
        q.setParameter(limit);
		return q.execute(Referral.class);
	}

    public static int count(RequestContext context) {
		Query q = new Query(context, "select count(*) from #");
		q.setTable(Referral.class);
		return q.getInt();
	}

	/**
	 * Delete off older than param days
	 */
	public static void reap(RequestContext context, int days) {
		new Query(context, "delete from # where date<?").setTable(Referral.class).setParameter(new Date().removeDays(days)).run();
	}

	/**
	 * 
	 */
	private String	referrer;

	/**
	 * 
	 */
	private DateTime	date;

	/**
	 * 
	 */
	private String	ipAddress;

	private Referral(RequestContext context) {
		super(context);
	}

	public Referral(RequestContext context, String referrer, String ipAddress) {
		super(context);

		this.referrer = referrer;
		this.ipAddress = ipAddress;
		this.date = new DateTime();

		save();
	}

	public DateTime getDate() {
		return date;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public String getReferrer() {
		return referrer;
	}
}
