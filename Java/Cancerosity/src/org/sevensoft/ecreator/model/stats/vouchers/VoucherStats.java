package org.sevensoft.ecreator.model.stats.vouchers;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.ecom.vouchers.Voucher;

import java.util.List;

/**
 * User: Tanya
 * Date: 06.09.2012
 */
@Table("report_vouchers")
public class VoucherStats extends EntityObject {

    public static List<VoucherStats> getAll(RequestContext context) {
        return SimpleQuery.execute(context, VoucherStats.class);
    }

    private Voucher voucher;
    private int usersCount;

    protected VoucherStats(RequestContext context) {
        super(context);
    }

    public VoucherStats(RequestContext context, Voucher voucher) {
        super(context);
        this.voucher = voucher;
        save();
    }

    public Voucher getVoucher() {
        return voucher.pop();
    }

    public void setVoucher(Voucher voucher) {
        this.voucher = voucher;
    }

    public int getUsersCount() {
        return usersCount;
    }

    public void setUsersCount(int usersCount) {
        this.usersCount = usersCount;
        save("usersCount");
    }

    public static void inc(RequestContext context, Voucher voucher) {
        VoucherStats voucherStats;
        voucherStats = SimpleQuery.get(context, VoucherStats.class, "voucher", voucher);
        if (voucherStats == null) {
            voucherStats = new VoucherStats(context, voucher);
        }

        voucherStats.setUsersCount(voucherStats.getUsersCount() + 1);
    }
}
