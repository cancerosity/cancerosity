package org.sevensoft.ecreator.model.stats.visits;

import org.sevensoft.ecreator.model.stats.hits.site.SiteHitCounter;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.commons.samdate.Date;

import java.util.List;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
@Table("reports_visitors_daily")
public class VisitorsCounterDaily extends SiteHitCounter {

    protected VisitorsCounterDaily(RequestContext context) {
        super(context);
    }

    public VisitorsCounterDaily(RequestContext context, Date date) {
        super(context, date);
    }

    public static VisitorsCounterDaily get(RequestContext context, Date day) {
        return SiteHitCounter.get(context, VisitorsCounterDaily.class, day);
    }

    public static List<VisitorsCounterDaily> get(RequestContext context, Date start, Date end) {
        return SiteHitCounter.get(context, VisitorsCounterDaily.class, start, end);
    }

    /**
     * Delete off visirors statistic older than param days
     */
    public static void reap(RequestContext context, int days) {
        new Query(context, "delete from # where date<?").setTable(VisitorsCounterDaily.class).setParameter(new Date().removeDays(days)).run();
    }
}
