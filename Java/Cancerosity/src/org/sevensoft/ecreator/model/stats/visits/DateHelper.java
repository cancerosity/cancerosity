package org.sevensoft.ecreator.model.stats.visits;

/**
 * @author sks 11 Apr 2007 09:59:51
 *
 */
public class DateHelper {

	/**
	 * 
	 */
	public static String millisToString(long d) {

		d = d / 1000;

		StringBuilder sb = new StringBuilder();

		if (d > 3600) {

			sb.append(d / 3600);
			sb.append("h ");
		}

		if (d > 60) {

			d = d % 3600;

			sb.append(d / 60);
			sb.append("m ");
		}

		d = d % 60;

		sb.append(d);
		sb.append("s ");

		return sb.toString();
	}

}
