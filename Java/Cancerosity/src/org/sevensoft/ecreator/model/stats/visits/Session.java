package org.sevensoft.ecreator.model.stats.visits;

import java.util.List;

import org.sevensoft.ecreator.model.stats.browsers.Browser;
import org.sevensoft.ecreator.model.stats.os.OperatingSystem;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 7 Apr 2007 10:15:23
 * 
 * A session is a current visit for a visitor
 *
 */
@Table("visitors_sessions")
public class Session extends EntityObject {

	/**
	 * Returns all current 'visits' on the site
	 */
	public static List<Session> getActive(RequestContext context) {

		Query q = new Query(context, "select * from # where lastAccess>=?");
		q.setTable(Session.class);
		q.setParameter(System.currentTimeMillis() - 1000l * 60 * 5);
		return q.execute(Session.class);
	}

	/**
	 * Return recent sessions
	 */
	public static List<Session> getRecent(RequestContext context, int start, int limit) {

		Query q = new Query(context, "select * from (select * from # order by lastAccess desc) Z group by visitor order by id desc");
		q.setTable(Session.class);
		return q.execute(Session.class, start, limit);
	}

	/**
	 * The visitor object
	 */
    @Index()
	private Visitor	visitor;

	/**
	 * The length in seconds of this visit
	 */
	private long	duration;

	/**
	 * The timestamp of the first access
	 */
	private long	firstAccess;

	/**
	 * The first page this visitor viewed when entering the site
	 */
	private String	entryPageUrl;

	/**
	 * The last page this visitor viewed
	 */
	private String	exitPageUrl;

	/**
	 * The referrer URL that directed the user to their first page of this session
	 */
	private String	referrer;

	/**
	 * Number of page views in this visit
	 */
	private int		views;

	/**
	 * The last time this visit was updated, we use this date to know if the visit has ended or not. The last access time was under 5 minutes ago then we assume the user
	 * is continuing the same visit and can update this, otherwise we start a new one. 
	 */
	private long	lastAccess;

	private String	exitPageName;

	private String	entryPageName;

	/**
	 * User agent string for first access
	 */
	private String	userAgent;

	private Session(RequestContext context) {
		super(context);
	}

	public Session(RequestContext context, Visitor visitor, String pageUrl, String pageName, String referrer) {
		super(context);

		this.visitor = visitor;

		this.entryPageUrl = pageUrl;
		this.entryPageName = pageName;
		this.referrer = referrer;
		this.firstAccess = System.currentTimeMillis();

		this.userAgent = context.getUserAgent();

		update(pageUrl, pageName);
	}

	public Browser getBrowser() {
		return Browser.getBrowser(userAgent);
	}

	/**
	 *  Returns all the page views in order for this session
	 */
	public List<View> getClickPath() {
		Query q = new Query(context, "select * from # where visitor=? order by id desc");
		q.setParameter(visitor);
		q.setTable(View.class);
		return q.execute(View.class);
	}

	public final long getDuration() {
		return duration;
	}

	public String getDurationString() {

		if (duration == 0) {
			return "--";
		}

		return DateHelper.millisToString(duration);
	}

	public final String getEntryPageName() {
		return entryPageName;
	}

	public final String getEntryPageUrl() {
		return entryPageUrl;
	}

	public final String getExitPageName() {
		return exitPageName;
	}

	public final String getExitPageUrl() {
		return exitPageUrl;
	}

	public final long getFirstAccess() {
		return firstAccess;
	}

	public final long getLastAccess() {
		return lastAccess;
	}

	public OperatingSystem getOs() {
		return OperatingSystem.getOs(getUserAgent());
	}

	public final String getReferrer() {
		return referrer;
	}

	public final String getUserAgent() {
		return userAgent;
	}

	public int getViews() {
		return views;
	}

	public Visitor getVisitor() {
		return (Visitor) (visitor == null ? null : visitor.pop());
	}

	public boolean hasExitPageName() {
		return exitPageName != null;
	}

	public boolean hasReferrer() {
		return referrer != null;
	}

	public final void setDuration(int duration) {
		this.duration = duration;
	}

	public final void setEntryPageUrl(String entryPage) {
		this.entryPageUrl = entryPage;
	}

	public final void setExitPageUrl(String exitPage) {
		this.exitPageUrl = exitPage;
	}

	public final void setFirstAccess(long start) {
		this.firstAccess = start;
	}

	public void update(String pageUrl, String pageName) {

		this.lastAccess = System.currentTimeMillis();
		this.duration = lastAccess - firstAccess;
		this.exitPageUrl = pageUrl;
		this.exitPageName = pageName;
		this.views++;

		save();

		// create a view for this session page
		new View(context, this, pageUrl, pageName);
	}

}
