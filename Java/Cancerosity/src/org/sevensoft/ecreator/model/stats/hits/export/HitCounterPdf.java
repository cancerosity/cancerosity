package org.sevensoft.ecreator.model.stats.hits.export;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHitCounter;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * @author sks 5 Jul 2006 09:36:32
 *
 */
public class HitCounterPdf {

	private final Date		start;
	private final Date		end;
	private Item			item;
	private Category			category;
	private String			name;
	private List<SiteHitCounter>	counters;

	public HitCounterPdf(Date start, Date end, Item item, Category category) {
		this.start = start;
		this.end = end;
		this.item = item;
		this.category = category;

		//		if (item != null) {
		//			
		//			this.impressions = item.getImpressions(start, end);
		//			this.name = item.getName();
		//
		//		} else if (category != null) {
		//			
		//			this.impressions = category.getImpressions(start, end);
		//			this.name = category.getName();
		//
		//		} else
		//			throw new RuntimeException();
		//
		//		assert impressions != null : "ERror";
	}

	public byte[] getDocument() throws IOException {

		Document doc = new Document();

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();

		try {

			PdfWriter.getInstance(doc, bos);
			doc.open();

			doc.add(new Paragraph("Total and unique hits for the month " + start.toString("dd-MMM-yy") + "\n\n"));

			PdfPTable table = new PdfPTable(5);

			Font font = new Font(Font.HELVETICA, 8, Font.BOLD);

			PdfPCell cell = new PdfPCell(new Paragraph("Date", font));
			table.addCell(cell);

			cell = new PdfPCell(new Paragraph("Total", font));
			table.addCell(cell);

			cell = new PdfPCell(new Paragraph("Uniques", font));
			table.addCell(cell);

			for (SiteHitCounter counter : counters) {

				font = new Font(Font.HELVETICA, 8, Font.NORMAL);

				cell = new PdfPCell(new Paragraph(counter.getDate().toString("HH:MM dd-MMM-yy"), font));
				table.addCell(cell);

				//				cell = new PdfPCell(new Paragraph(counter.getIpAddress(), font));
				//				table.addCell(cell);

				//				cell = new PdfPCell(new Paragraph(impression.getCountry().toString(), font));
				//				table.addCell(cell);

				cell = new PdfPCell(new Paragraph(String.valueOf(counter.getTotal()), font));
				table.addCell(cell);

				cell = new PdfPCell(new Paragraph(String.valueOf(counter.getUnique()), font));
				table.addCell(cell);

			}

			doc.add(table);
			doc.close();

			return bos.toByteArray();

		} catch (DocumentException e) {
			throw new IOException(e.getMessage());
		}
	}
}
