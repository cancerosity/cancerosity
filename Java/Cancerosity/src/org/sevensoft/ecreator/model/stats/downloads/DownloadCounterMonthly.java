package org.sevensoft.ecreator.model.stats.downloads;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 7 Mar 2007 17:38:00
 *
 */
@Table("reports_downloads_monthly")
public class DownloadCounterMonthly extends AbstractDownloadCounter {

	public static List<DownloadCounterMonthly> get(RequestContext context, Date month, int limit) {
		Query q = new Query(context, "select * from # where date=? order by full desc");
		q.setTable(DownloadCounterMonthly.class);
		q.setParameter(month);
		return q.execute(DownloadCounterMonthly.class);
	}

	public static void process(RequestContext context, Date date, Attachment attachment, boolean preview) {

		DownloadCounterMonthly counter = SimpleQuery.get(context, DownloadCounterMonthly.class, "attachment", attachment, "date", date.beginMonth());

		if (counter == null) {
			counter = new DownloadCounterMonthly(context, attachment, new Date().beginMonth());
		}

		counter.process(preview);
	}

	protected DownloadCounterMonthly(RequestContext context) {
		super(context);
	}

	protected DownloadCounterMonthly(RequestContext context, Attachment attachment, Date date) {
		super(context, attachment, date);
	}

}
