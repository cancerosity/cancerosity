package org.sevensoft.ecreator.model.stats.os;

/**
 * @author sks 11 Apr 2007 09:47:19
 *
 */
public enum OperatingSystem {

	WindowsXp, OSX, Linux;

	public static OperatingSystem getOs(String userAgent) {

		if (userAgent == null) {
			return null;
		}

		userAgent = userAgent.toLowerCase();

		if (userAgent.contains("Windows NT 5.1".toLowerCase())) {
			return WindowsXp;
		}

		if (userAgent.contains("Mac OS X".toLowerCase())) {
			return OSX;
		}

		return null;
	}
}
