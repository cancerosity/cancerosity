package org.sevensoft.ecreator.model.stats.seo;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Aug 2006 22:00:58
 *
 */
@Table("reports_terms_counter")
public class SearchTermCounterMonthly extends EntityObject {

	public static List<SearchTermCounterMonthly> get(RequestContext context, Date month, int limit) {
		Query q = new Query(context, "select * from # where date=? order by total desc");
		q.setTable(SearchTermCounterMonthly.class);
		q.setParameter(month);
		return q.execute(SearchTermCounterMonthly.class, limit);
	}

	public static SearchTermCounterMonthly get(RequestContext context, SearchEngine engine, String term, Date month) {
		Query q = new Query(context, "select * from # where engine=? and term=? and date=?");
		q.setTable(SearchTermCounterMonthly.class);
		q.setParameter(engine);
		q.setParameter(term);
		q.setParameter(month);
		return q.get(SearchTermCounterMonthly.class);
	}

	/**
	 * The keywords entered in the search engine, in full, and in the order it was entered
	 */
	@Index()
	private String		term;

	/**
	 * Total number of referrals using this search phrase this month
	 */
	@Index()
	private int			total;

	/**
	 * The month we are counting
	 */
	private Date		date;

	/**
	 * Break down results by search engine
	 */
	private SearchEngine	engine;

	private SearchTermCounterMonthly(RequestContext context) {
		super(context);
	}

	public SearchTermCounterMonthly(RequestContext context, SearchEngine searchEngine, String term) {
		super(context);
		this.engine = searchEngine;
		this.term = term;
		this.date = new Date().beginMonth();
	}

	public Date getDate() {
		return date;
	}

	public SearchEngine getEngine() {
		return engine;
	}

	public String getTerm() {
		return term;
	}

	public int getTotal() {
		return total;
	}

	public void inc() {
		total++;
		save();
	}
}
