package org.sevensoft.ecreator.model.stats.submissions;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.QueryBuilder;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 17 Aug 2006 17:47:28
 * 
 * This class counts submissions across the site for a particular form by month
 *
 */
@Table("reports_submissions_monthly")
public class SubmissionsCounter extends EntityObject {

	public static List<SubmissionsCounter> get(RequestContext context, Date start, Date end) {

		Query q = new Query(context, "select * from # where date>=? and date<=? order by date desc, form");
		q.setTable(SubmissionsCounter.class);
		q.setParameter(start);
		q.setParameter(end);
		return q.execute(SubmissionsCounter.class);

	}

	private static SubmissionsCounter get(RequestContext context, Date date, Form form) {

		QueryBuilder b = new QueryBuilder(context);
		b.select("*");
		b.from("#", SubmissionsCounter.class);
		b.clause("form=?", form);
		b.clause("date=?", date);

		return b.toQuery().get(SubmissionsCounter.class);
	}

	public static List<SubmissionsCounter> get(RequestContext context, Date month, int limit) {

		Query q = new Query(context, "select * from # where date=? order by total desc");
		q.setTable(SubmissionsCounter.class);
		q.setParameter(month);
		return q.execute(SubmissionsCounter.class, limit);
	}

	public static void update(RequestContext context, Form form) {

		Date date = new Date().beginMonth();
		SubmissionsCounter c = SubmissionsCounter.get(context, date, form);
		if (c == null)
			c = new SubmissionsCounter(context, form);
		c.inc();
	}

	/**
	 * The form we are counting
	 */
	@Index()
	private int		form;

	/**
	 * The month in question
	 */
	@Index()
	private Date	date;

	private int		total;

	private String	formName;

	private SubmissionsCounter(RequestContext context) {
		super(context);
	}

	public SubmissionsCounter(RequestContext context, Form form) {

		super(context);

		this.form = form.getId();
		this.date = new Date().beginMonth();
		this.formName = form.getName();

		save();
	}

	public Date getDate() {
		return date;
	}

	public String getFormName() {
		return formName;
	}

	public int getTotal() {
		return total;
	}

	public void inc() {
		total++;
		save();
	}
}
