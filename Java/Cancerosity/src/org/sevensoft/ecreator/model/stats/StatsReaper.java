package org.sevensoft.ecreator.model.stats;

import java.util.logging.Logger;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHitCounterDaily;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHit;
import org.sevensoft.ecreator.model.stats.ips.IpUsedPage;
import org.sevensoft.ecreator.model.stats.ips.IpUsedSite;
import org.sevensoft.ecreator.model.stats.referrals.Referral;
import org.sevensoft.ecreator.model.stats.seo.SearchTerm;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 17 Aug 2006 17:34:01
 *
 */
public class StatsReaper implements Runnable {

	private Logger		logger	= Logger.getLogger("ecreator");
	private RequestContext	context;

	public StatsReaper(RequestContext context) {
		this.context = context;
	}

	public void run() {

		logger.config("[ReportsReaper] run");

		// delete off hit details older than 7 days
		SiteHit.reap(context, 7);
		Referral.reap(context, 7);

		Query q = new Query(context, "delete from # where date<?");
		q.setTable(SiteHit.class);
		q.setParameter(System.currentTimeMillis() - (1000l * 60 * 60 * 24 * 45));
		q.run();

		q = new Query(context, "delete from # where date<?");
		q.setTable(SearchTerm.class);
		q.setParameter(System.currentTimeMillis() - (1000l * 60 * 60 * 24 * 45));
		q.run();

		q = new Query(context, "delete from # where date<?");
		q.setTable(Referral.class);
		q.setParameter(System.currentTimeMillis() - (1000l * 60 * 60 * 24 * 45));
		q.run();

		// remove site hits daily older than 60 days
		SiteHitCounterDaily.reap(context, 60);

		Date month = new Date().beginMonth();
		logger.config("[Reports Reaper] delete ip uses older than " + month.toString("dd MMM yyyy"));

		// empty IP uses older than this month
		q = new Query(context, "delete from # where date<=?").setTable(IpUsedPage.class);
		q.setParameter(month);
		q.run();
		q = new Query(context, "delete from # where date<=?").setTable(IpUsedSite.class);
		q.setParameter(month);
		q.run();

	}

}
