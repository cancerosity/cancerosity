package org.sevensoft.ecreator.model.stats.hits.site;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.stats.browsers.Browser;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 11 Aug 2006 07:20:54
 * 
 * Stores details of a particular 'view' or 'hit' to a reportable page on the site
 *
 */
@Table("reports_hits")
public class SiteHit extends EntityObject {

	public static List<SiteHit> getCurrent(RequestContext context, int limit) {
		Query q = new Query(context, "select * from # order by id desc");
		q.setTable(SiteHit.class);
		return q.execute(SiteHit.class, limit);
	}

	/**
	 * Returns the number of hits for this per hour
	 */
	public static int getHourlyCount(RequestContext context, String remoteIp) {

		Query q = new Query(context, "select count(*) from # where ipAddress=? and date>?");
		q.setTable(SiteHit.class);
		q.setParameter(remoteIp);
		q.setParameter(System.currentTimeMillis() - (1000l * 60 * 60));
		return q.getInt();
	}

	public static void reap(RequestContext context, int i) {
		new Query(context, "delete from # where date<?").setTable(SiteHit.class).setParameter(new Date().removeDays(i)).run();
	}

	/**
	 * The full referral url
	 */
	private String	referrer;

	/**
	 * The users ip
	 */
	private String	ipAddress;

	/**
	 * The user agent string used for this view
	 */
	private String	userAgent;

	/**
	 * timestamp of access
	 */
	private long	date;

	private String	sessionId;

	/**
	 * The page the user visited
	 */
	private String	page;

	private SiteHit(RequestContext context) {
		super(context);
	}

	public SiteHit(RequestContext context, String referrer, String ipAddress, String page, Browser browser, String os) {
		super(context);

		this.referrer = referrer;
		this.ipAddress = ipAddress;
		this.page = page;
		this.userAgent = context.getUserAgent();

		this.date = System.currentTimeMillis();

		save();
	}

	public final long getDate() {
		return date;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public String getPage() {
		return page;
	}

	public String getReferrer() {
		return referrer;
	}

	public final String getUserAgent() {
		return userAgent;
	}

	public boolean hasReferrer() {
		return referrer != null;
	}

}
