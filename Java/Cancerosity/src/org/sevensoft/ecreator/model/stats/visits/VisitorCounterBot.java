package org.sevensoft.ecreator.model.stats.visits;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHitCounter;

import java.util.logging.Logger;
import java.util.*;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
public class VisitorCounterBot implements Runnable {

    private static final Logger logger = Logger.getLogger("cron");
    private RequestContext context;
    private Map<String, SiteHitCounter> counterCache = new HashMap<String, SiteHitCounter>();

    public VisitorCounterBot(RequestContext context) {
        this.context = context;
    }

    public void run() {
        //visits for the last 30 days
        processDaily();

        //summeries info of old visitors to clear visitors table
        processMonthly();

    }

    private void processMonthly() {

        List<Visitor> visitorList = Visitor.getAll(context, 5000);

        for (Visitor visitor : visitorList) {
            Long lastAccess = visitor.getLastAccess();
            String date = new DateTime(lastAccess).toString("MMM-yyyy");
            VisitorsCounterMonthly counterMonthly = null;
            if (counterCache.containsKey(date)) {
                counterMonthly = (VisitorsCounterMonthly) counterCache.get(date);
            } else if (VisitorsCounterMonthly.get(context, new Date(lastAccess).beginMonth()) == null) {
                counterMonthly = new VisitorsCounterMonthly(context, new Date(lastAccess).beginMonth());
            } else {
                counterMonthly = VisitorsCounterMonthly.get(context, new Date(lastAccess).beginMonth());
            }


            if (counterMonthly != null) {
                counterMonthly.setTotal(counterMonthly.getTotal() + visitor.getTotalViews());
                counterMonthly.setUnique(counterMonthly.getUnique() + visitor.getVisits());
                counterCache.put(date, counterMonthly);
            }
        }

        for (Map.Entry<String, SiteHitCounter> entry : counterCache.entrySet()) {
            entry.getValue().save();
        }

        counterCache.clear();

        if (!visitorList.isEmpty()) {
            deleteOldVisitors(visitorList);
        }
    }

    private void deleteOldVisitors(List<Visitor> visitorList) {

        long time = new Date().removeMonths(1).getTimestamp();
        if (visitorList.get(visitorList.size() - 1).getLastAccess() < time) {
            new Query(context, "delete ses.* from # ses left outer join # vis on ses.visitor=vis.id where vis.id >=? and vis.id <=?")
                    .setTable(Session.class).setTable(Visitor.class).setParameter(visitorList.get(0).getId())
                    .setParameter(visitorList.get(visitorList.size() - 1).getId()).run();
            new Query(context, "delete v.* from # v left outer join # vis on v.visitor=vis.id where vis.id >=? and vis.id <=?")
                    .setTable(View.class).setTable(Visitor.class)
                    .setParameter(visitorList.get(0).getId()).setParameter(visitorList.get(visitorList.size() - 1).getId()).run();
            new Query(context, "delete from # where id >= ? and id <=? ").setTable(Visitor.class)
                    .setParameter(visitorList.get(0).getId()).setParameter(visitorList.get(visitorList.size() - 1).getId()).run();
        } else {
            for (Visitor visitor : visitorList) {
                if (visitor.getLastAccess() < time) {
                    new Query(context, "delete ses.* from # ses left outer join # vis on ses.visitor=vis.id where vis.id=?")
                            .setTable(Session.class).setTable(Visitor.class).setParameter(visitor.getId()).run();
                    new Query(context, "delete v.* from # v left outer join # vis on v.visitor=vis.id where vis.id=?")
                            .setTable(View.class).setTable(Visitor.class).setParameter(visitor.getId()).run();
                    new Query(context, "delete from # where id=? ").setTable(Visitor.class).setParameter(visitor.getId()).run();
                }
            }
        }
    }

    private void processDaily() {

        List<Visitor> visitors = Visitor.get(context, new Date().removeMonths(1), new Date().removeDays(1));

        for (Visitor visitor : visitors) {
            String date = new DateTime(visitor.getLastAccess()).toString("dd-MMM-yyyy");
            VisitorsCounterDaily counterDaily = null;
            if (counterCache.containsKey(date)) {
                counterDaily = (VisitorsCounterDaily) counterCache.get(date);

            } else if (VisitorsCounterDaily.get(context, new Date(visitor.getLastAccess())) == null) {
                counterDaily = new VisitorsCounterDaily(context, new Date(visitor.getLastAccess()));
            }
            if (counterDaily != null) {
                counterDaily.setTotal(counterDaily.getTotal() + visitor.getTotalViews());
                counterDaily.setUnique(counterDaily.getUnique() + visitor.getVisits());
                counterCache.put(date, counterDaily);
            }
        }

        for (Map.Entry<String, SiteHitCounter> entry : counterCache.entrySet()) {
            entry.getValue().save();
        }

        counterCache.clear();

        VisitorsCounterDaily.reap(context, 60);
    }
}
