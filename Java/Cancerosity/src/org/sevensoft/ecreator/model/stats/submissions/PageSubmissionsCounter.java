package org.sevensoft.ecreator.model.stats.submissions;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.stats.StatablePage;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.QueryBuilder;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 14 Aug 2006 17:34:37
 *
 * This class counts the number of times any form was submitted on a particular page
 */
@Table("reports_submissions_pages_monthly")
public class PageSubmissionsCounter extends EntityObject {

	public static List<PageSubmissionsCounter> get(RequestContext context, Date date, int limit) {

		Query q = new Query(context, "select * from # where date=? order by total desc");
		q.setTable(PageSubmissionsCounter.class);
		q.setParameter(date);
		return q.execute(PageSubmissionsCounter.class, limit);
	}

	private static PageSubmissionsCounter get(RequestContext context, Date date, StatablePage page) {

		QueryBuilder b = new QueryBuilder(context);
		b.select("*");
		b.from("#", PageSubmissionsCounter.class);
		b.clause(page.getClass().getSimpleName() + "=?", page);
		b.clause("date=?", date);

		return b.toQuery().get(PageSubmissionsCounter.class);
	}

	public static void submit(RequestContext context, StatablePage page) {

		Date date = new Date().beginMonth();
		PageSubmissionsCounter c = PageSubmissionsCounter.get(context, date, page);
		if (c == null)
			c = new PageSubmissionsCounter(context, page);
		c.inc();
	}

	/**
	 * Date of submission
	 */
	@Index()
	private Date	date;

	/**
	 * If this form was submitted on an item
	 */
	@Index()
	private Item	item;

	/**
	 * If this form was submitted on a category. 
	 */
	@Index()
	private Category	category;

	private int		total;

	private PageSubmissionsCounter(RequestContext context) {
		super(context);
	}

	public PageSubmissionsCounter(RequestContext context, StatablePage page) {
		super(context);

		this.date = new Date().beginMonth();

		if (page instanceof Category)
			category = (Category) page;

		else if (page instanceof Item)
			item = (Item) page;

		save();
	}

	public Category getCategory() {
		return (Category) (category == null ? null : category.pop());
	}

	public Date getDate() {
		return date;
	}

	public Item getItem() {
		return (Item) (item == null ? null : item.pop());
	}

	public StatablePage getPage() {

		if (category != null)
			return getCategory();

		if (item != null)
			return getItem();

		return null;
	}

	public int getTotal() {
		return total;
	}

	public void inc() {
		total++;
		save();
	}
}
