package org.sevensoft.ecreator.model.stats.browsers;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Aug 2006 23:08:55
 *
 * Counts number of times a browser was used per month 
 */
@Table("reports_browsers_counter")
public class BrowserCounterMonthly extends EntityObject {

	public static BrowserCounterMonthly get(RequestContext context, Date month, Browser browser) {

		Query q = new Query(context, "select * from # where browser=? and date=?");
		q.setTable(BrowserCounterMonthly.class);
		q.setParameter(browser);
		q.setParameter(month);
		return q.get(BrowserCounterMonthly.class);
	}

	/**
	 * Delete off older than param days
	 */
	public static void reap(RequestContext context, int days) {
		new Query(context, "delete from # where date<?").setTable(BrowserCounterMonthly.class).setParameter(new Date().removeDays(days)).run();
	}

	@Index()
	private Browser	browser;

	@Index()
	private Date	date;

	private int		total;

	private BrowserCounterMonthly(RequestContext context) {
		super(context);
	}

	public BrowserCounterMonthly(RequestContext context, Browser browser, Date month) {
		super(context);
		this.browser = browser;
		this.date = month;
	}

	public Browser getBrowser() {
		return browser;
	}

	public Date getDate() {
		return date;
	}

	public int getTotal() {
		return total;
	}

	public void inc() {
		total++;
		save();
	}
}
