package org.sevensoft.ecreator.model.stats.listings;

import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.items.Item;

import java.util.List;

/**
 * @author Dmitry Lebedev
 *         Date: 05.11.2009
 */
@Table("listings_hit_monthly")
public class ListingHitCounterMonthly extends ListingHitCounter {

    public static ListingHitCounterMonthly get(RequestContext context, Date date, Item item) {
        return ListingHitCounter.get(context, ListingHitCounterMonthly.class, date, item);
    }

    public static List<ListingHitCounterMonthly> get(RequestContext context, Date start, Date end, Item item) {
        return ListingHitCounter.get(context, ListingHitCounterMonthly.class, start, end, item);
    }

    private ListingHitCounterMonthly(RequestContext context) {
        super(context);
    }

    public ListingHitCounterMonthly(RequestContext context, Date day, Item item) {
        super(context, day, item);
    }
}
