package org.sevensoft.ecreator.model.stats.ips;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Aug 2006 22:10:40
 * 
 * Stores list of ips used for a date across any page on site
 *
 */
@Table("reports_ips")
public class IpUsedSite extends EntityObject {

	/**
	 * Returns true if this ip is unique for the date range for any page on the site
	 */
	public static boolean isUnique(RequestContext context, Date start, Date end, String ipAddress) {

		Query q = new Query(context, "select count(*) from # where date>=? and date<=? and ipaddress=? ");

		q.setTable(IpUsedSite.class);
		q.setParameter(start);
		q.setParameter(end);
		q.setParameter(ipAddress);
		return q.getInt() == 0;
	}

	@Index()
	private Date	date;

	@Index()
	private String	ipAddress;

	/**
	 *
	 */
	protected IpUsedSite(RequestContext context) {
		super(context);
	}

	public IpUsedSite(RequestContext context, Date date, String ipAddress) {
		super(context);

		this.date = date;
		this.ipAddress = ipAddress;

		save();
	}

	public Date getDate() {
		return date;
	}

	public String getIpAddress() {
		return ipAddress;
	}

}
