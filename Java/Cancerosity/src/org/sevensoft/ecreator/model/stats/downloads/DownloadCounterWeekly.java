package org.sevensoft.ecreator.model.stats.downloads;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 7 Mar 2007 17:38:00
 *
 */
@Table("reports_downloads_weekly")
public class DownloadCounterWeekly extends AbstractDownloadCounter {

	public static List<DownloadCounterWeekly> get(RequestContext context, Date month, int limit) {
		Query q = new Query(context, "select * from # where date=? order by full desc");
		q.setTable(DownloadCounterWeekly.class);
		q.setParameter(month);
		return q.execute(DownloadCounterWeekly.class);
	}

	public static void process2(RequestContext context, Date date, Attachment attachment, boolean preview) {

		DownloadCounterWeekly counter = SimpleQuery.get(context, DownloadCounterWeekly.class, "attachment", attachment, "date", date.beginWeek());

		if (counter == null) {
			counter = new DownloadCounterWeekly(context, attachment, new Date().beginWeek());
		}

		counter.process(preview);

	}

	protected DownloadCounterWeekly(RequestContext context) {
		super(context);
	}

	protected DownloadCounterWeekly(RequestContext context, Attachment attachment, Date date) {
		super(context, attachment, date);
	}

}
