package org.sevensoft.ecreator.model.stats;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.stats.browsers.Browser;
import org.sevensoft.ecreator.model.stats.browsers.BrowserCounterMonthly;
import org.sevensoft.ecreator.model.stats.hits.pages.PageHitCounterMonthly;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHitCounterDaily;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHitCounterMonthly;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHit;
import org.sevensoft.ecreator.model.stats.ips.IpUsedPage;
import org.sevensoft.ecreator.model.stats.ips.IpUsedSite;
import org.sevensoft.ecreator.model.stats.ips.IpUsedListing;
import org.sevensoft.ecreator.model.stats.referrals.Referral;
import org.sevensoft.ecreator.model.stats.referrals.ReferralCounterMonthly;
import org.sevensoft.ecreator.model.stats.seo.SearchEngine;
import org.sevensoft.ecreator.model.stats.seo.SearchTerm;
import org.sevensoft.ecreator.model.stats.seo.SearchTermCounterMonthly;
import org.sevensoft.ecreator.model.stats.listings.ListingHitCounterDaily;
import org.sevensoft.ecreator.model.stats.listings.ListingHitCounterMonthly;
import org.sevensoft.ecreator.model.stats.listings.ListingHit;
import org.sevensoft.ecreator.model.stats.visits.Visitor;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 13 Aug 2006 22:14:48
 * 
 * Does all reporting needed per request
 */
public class StatsCenter {

	private static String[]		Robots	= Visitor.bots; //new String[] { "Mirago", "GoogleBot", "msnbot", "Yahoo! Slurp" };
	private static Logger		logger	= Logger.getLogger("ecreator");

	private String			referrer;
	private String			userAgent;
	private Browser			browser;
	private String			ipAddress;
	private String			page;
	private Date			thisMonth;
	private Date			today;
	private StatablePage		reportablePage;
	private Date			endMonth;
	private final RequestContext	context;
    private Item            viewer;
    private Item            item;

    public StatsCenter(RequestContext context) {

		this.context = context;
		this.today = new Date();
		this.thisMonth = new Date().beginMonth();
		this.endMonth = thisMonth.endMonth();

		this.referrer = context.getReferrer();
		this.userAgent = context.getUserAgent();
		this.ipAddress = context.getRemoteIp();
		this.browser = Browser.getBrowser(userAgent);
		this.page = context.getGetRequest();
	}

    public StatsCenter(RequestContext context, Item viewer, Item item) {

        this.context = context;
        this.today = new Date();
        this.thisMonth = new Date().beginMonth();
        this.endMonth = thisMonth.endMonth();

        this.referrer = context.getReferrer();
        this.userAgent = context.getUserAgent();
        this.ipAddress = context.getRemoteIp();
        this.browser = Browser.getBrowser(userAgent);
        this.page = context.getGetRequest();
        this.viewer = viewer;
        this.item = item;
    }

    private void doBrowser() {

		BrowserCounterMonthly bc = BrowserCounterMonthly.get(context, thisMonth, browser);
		if (bc == null)
			bc = new BrowserCounterMonthly(context, browser, thisMonth);
		bc.inc();

	}

	private void doHits() {

		boolean isUniqueDaily = IpUsedSite.isUnique(context, today, today, ipAddress);
		boolean isUniqueMonthly = IpUsedSite.isUnique(context, thisMonth, endMonth, ipAddress);

		/*
		 * if unique today then add in ip address
		 * it cannot be unique monthly without also being unique daily
		 */
		if (isUniqueDaily) {
			new IpUsedSite(context, today, ipAddress);
		}

		// update SITE WIDE hits
		{
			SiteHitCounterDaily hcd = SiteHitCounterDaily.get(context, today);
			if (hcd == null) {
				hcd = new SiteHitCounterDaily(context, today);
			}
			hcd.inc(isUniqueDaily);

			// update monthly hit records site wide
			SiteHitCounterMonthly hcm = SiteHitCounterMonthly.get(context, thisMonth);
			if (hcm == null)
				hcm = new SiteHitCounterMonthly(context, thisMonth);
			hcm.inc(isUniqueMonthly);
		}

		// create a new hit for this access
		new SiteHit(context, referrer, ipAddress, page, browser, null);
	}

    @Deprecated
    private void doListingHits() {

        boolean isUniqueDaily = IpUsedListing.isUnique(context, today, today, item, ipAddress);
        boolean isUniqueMonthly = IpUsedListing.isUnique(context, thisMonth, endMonth, item, ipAddress);

        /*
           * if unique today then add in ip address
           * it cannot be unique monthly without also being unique daily
           */
        if (isUniqueDaily) {
            new IpUsedListing(context, today, item, ipAddress);
        }

        // update SITE WIDE hits
        {
            ListingHitCounterDaily hcd = ListingHitCounterDaily.get(context, today, item);
            if (hcd == null) {
                hcd = new ListingHitCounterDaily(context, today, item);
            }
            hcd.inc(isUniqueDaily);

            // update monthly hit records site wide
            ListingHitCounterMonthly hcm = ListingHitCounterMonthly.get(context, thisMonth, item);
            if (hcm == null)
                hcm = new ListingHitCounterMonthly(context, thisMonth, item);
            hcm.inc(isUniqueMonthly);
        }

        // create a new hit for this access
        new ListingHit(context, referrer, ipAddress, page, browser, null, viewer, item);

    }

    private void doPageHits() {

		boolean isUniqueMonthly = IpUsedPage.isUnique(context, thisMonth, ipAddress, reportablePage);

		/*
		 * if unique today then add in ip address
		 * it cannot be unique monthly without also being unique daily
		 */
		if (isUniqueMonthly) {
			new IpUsedPage(context, thisMonth, ipAddress, reportablePage);
		}

		// update monthly hit records for this recorder
		PageHitCounterMonthly m = PageHitCounterMonthly.get(context, reportablePage, thisMonth);
		if (m == null)
			m = new PageHitCounterMonthly(context, reportablePage, thisMonth);
		m.inc(isUniqueMonthly);

	}

	private void doReferral() {
        
        if (Config.getInstance(context).getDomain() == null){
            Config.getInstance(context).init(context);
        }

		// if the referrer is part of our domain then exit
		if (referrer.contains(Config.getInstance(context).getDomain())) {
			return;
		}

		new Referral(context, referrer, ipAddress);

		String hostname;
		try {

			hostname = new URL(referrer).getHost();

			// update monthly referrals
			ReferralCounterMonthly rc = ReferralCounterMonthly.get(context, hostname, thisMonth);
			if (rc == null)
				rc = new ReferralCounterMonthly(context, hostname);
			rc.inc();

		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}

		// check for search engine referral
		SearchEngine engine = SearchEngine.get(referrer);

		// if search engine found then try to get search engine terms
		if (engine != null) {

			try {

				String term = engine.parse(referrer);
				if (term != null) {

					new SearchTerm(context, engine, term, ipAddress);

					SearchTermCounterMonthly tc = SearchTermCounterMonthly.get(context, engine, term, thisMonth);
					if (tc == null)
						tc = new SearchTermCounterMonthly(context, engine, term);
					tc.inc();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 */
	private boolean isRobot() {

		if (userAgent == null)
			return false;

		for (String robot : Robots) {

			if (userAgent.toLowerCase().contains(robot.toLowerCase()))
				return true;
		}

		return false;
	}

	public void run() {

		logger.fine("[ReportCenter] running reports center");

		/*
		 * If this is a hit from a robot then do not count it
		 */
		if (isRobot()) {
			return;
		}

		doHits();

		// do referral if our referrer string is not null
		if (referrer != null) {
			doReferral();
		}

		// update browser counter
		doBrowser();

		if (reportablePage != null) {
			doPageHits();
            /*
            if (reportablePage instanceof Item && ((Item) reportablePage).getItemType() != null
                    && ((Item) reportablePage).getItemType().getModules().contains(ItemModule.Listings))
                doListingHits();
                */
		}


        
	}

    @Deprecated
    public void runListingStats() {

        /*
		 * If this is a hit from a robot then do not count it
		 */
        if (isRobot()) {
            return;
        }

        doListingHits();

        // do referral if our referrer string is not null
        if (referrer != null) {
            doReferral();
        }

//        // update browser counter
//		doBrowser();
//
//		if (reportablePage != null) {
//			doPageHits();
//		}

    }

    public void run(StatablePage p) {

		this.reportablePage = p;
		run();
	}

    @Deprecated
    public void runListingStats(StatablePage p) {

        this.reportablePage = p;
		runListingStats();
    }
}
