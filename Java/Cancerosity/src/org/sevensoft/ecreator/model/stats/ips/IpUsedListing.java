package org.sevensoft.ecreator.model.stats.ips;

import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.items.Item;

/**
 * @author Dmitry Lebedev
 *         Date: 05.11.2009
 */
@Table("reports_ips_listing")
public class IpUsedListing extends EntityObject {

    /**
	 * Returns true if this ip is unique for the date range for this listing
	 */
	public static boolean isUnique(RequestContext context, Date dateStart, Date dateEnd, Item item, String ipAddress) {
		Query q = new Query(context, "select count(*) from # where date>=? and date<=? and item=? and ipaddress=?");
		q.setTable(IpUsedListing.class);
		q.setParameter(dateStart);
        q.setParameter(dateEnd);
        q.setParameter(item);
        q.setParameter(ipAddress);
		return q.getInt() == 0;
	}

    @Index()
	private Date	date;

    @Index()
	private Item item;

	@Index()
	private String	ipAddress;

    protected IpUsedListing(RequestContext context) {
        super(context);
    }

    public IpUsedListing(RequestContext context, Date date, Item item, String ipAddress) {
        super(context);

        this.date = date;
        this.item = item;
        this.ipAddress = ipAddress;

        save();
    }

    public Date getDate() {
        return date;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public Item getItem() {
        return item;
    }


}
