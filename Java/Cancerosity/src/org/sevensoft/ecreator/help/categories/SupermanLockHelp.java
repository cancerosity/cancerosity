package org.sevensoft.ecreator.help.categories;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.ToolTip;

/**
 * @author sks 5 Apr 2007 10:52:02
 *
 */
public class SupermanLockHelp extends ToolTip {

	@Override
	protected String getHelp() {
		return "Sites with complicated content placed in a category can sometimes become broken through editing, often when the content editor does not understand some complex code placed within the category content.\n\n"
				+ "For this reason adminstrators may have a category they wish to 'lock' so that no users can edit and thus can no longer inadvertantly break the code of the page.";
	}
}
