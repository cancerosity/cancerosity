package org.sevensoft.ecreator.help.categories;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.ToolTip;

/**
 * @author sks 5 Apr 2007 11:10:48
 *
 */
public class MergeIntoHelp extends ToolTip {

	@Override
	protected String getHelp() {
		return "This is a very powerful tool that will move all of the content and subcategories of the current category into the cateogry specified from the dropdown selector. This will not overwrite existing content in the category selected, any content moved across will be placed into additional content blocks, editable from the 'Add ons' section of the category. This merge also includes moving highlighted item boxes and other category Add ons over to the selected category. All the subcategories from the category being merged are moved into the new category and will appear in that categories subdirectory tree. This powerful tool is not often required and should be used with great care, as once a category is merged with another it cannot be un-merged and have the component parts redistributed again.";
	}

}
