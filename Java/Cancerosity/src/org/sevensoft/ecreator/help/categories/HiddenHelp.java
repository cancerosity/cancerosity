package org.sevensoft.ecreator.help.categories;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.ToolTip;

/**
 * @author sks 5 Apr 2007 10:52:56
 *
 */
public class HiddenHelp extends ToolTip {

	@Override
	protected String getHelp() {
		return "Setting a category to hidden will prevent the category being listed anywhere on the site, including menu bars and category sideboxes. "
				+ "The category is not removed or disabled and still exists, but it can only be linked to manually if you know the URL.";
	}

}
