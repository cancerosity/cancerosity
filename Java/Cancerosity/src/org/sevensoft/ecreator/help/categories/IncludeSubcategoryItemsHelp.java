package org.sevensoft.ecreator.help.categories;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.ToolTip;

/**
 * @author sks 5 Apr 2007 10:54:46
 *
 */
public class IncludeSubcategoryItemsHelp extends ToolTip {

	@Override
	protected String getHelp() {
		return "If set to 'Yes', items from all the subcategories of this category will also be displayed on the front end of the site, not just the items that are in this category. If this is set to 'Yes' on the Home Page for instance, all the items from across the entire site will be listed on the Home Page.";
	}

}
