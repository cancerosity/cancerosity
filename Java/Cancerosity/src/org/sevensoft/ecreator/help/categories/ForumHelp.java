package org.sevensoft.ecreator.help.categories;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.ToolTip;

/**
 * @author sks 5 Apr 2007 11:11:04
 *
 */
public class ForumHelp extends ToolTip {

	@Override
	protected String getHelp() {
		return "When the forum is enabled and configured, setting this option to 'Yes' will make this category forward directly to the forum, ignoring all other settings and category content.";
	}

}

