package org.sevensoft.ecreator.help.categories;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.ToolTip;

/**
 * @author sks 5 Apr 2007 10:55:27
 *
 */
public class MoveSubcategoriesHelp extends ToolTip {

	@Override
	protected String getHelp() {
		return "Once a new 'parent' category is selected from the drop down list, clicking update will move all the subcategories of the current category into the new parent category. This will not affect the current category, which will keep it's own parent category and position in your category structure, but there will no longer be any subcategories for this category.";
	}

}
