package org.sevensoft.ecreator.help.categories;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.ToolTip;


/**
 * @author sks 5 Apr 2007 10:53:54
 *
 */
public class SimpleEditorHelp extends ToolTip {

	@Override
	protected String getHelp() {
		return "Turning on the simple editor will disable the advanced content editor. While the content editor is very user-friendly and powerful, it has some limitations. Complicated HTML used to construct webpages can confuse the content editor, and it also does not support certain other code, such as scripts/javascript, certain style information and some general complex HTML arrangements. In most scenarios you will not need to turn the simple editor on, however a skilled web designer or the requirement to use a javascript applet may mean this is more beneficial or entirely necessary.\n\nPlease note: Turning the simple editor off without first saving the HTML from the editor window in a text document can be disasterous, as the content editor will strip out the code elements it doesn't understand and you may lose this content. Please turn the simple editor on and off with extreme caution.";

	}

}
