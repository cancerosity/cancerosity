package org.sevensoft.ecreator.help.seo;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.ToolTip;

/**
 * @author sks 11 Apr 2007 15:12:20
 *
 */
public class DescriptionTagHelp extends ToolTip {

	@Override
	protected String getHelp() {
		return "The meta description tag describes your site's content, this is used by the search engines spiders as an accurate summary of your site.\n\n"
				+ "This description should contain multiple keywords and read in a logical sentence, it is recommended that the Description Tag is no more than 250 characters and that your keywords are not repeated more than 6 times as search engines may consider this to be spam.";
	}

}
