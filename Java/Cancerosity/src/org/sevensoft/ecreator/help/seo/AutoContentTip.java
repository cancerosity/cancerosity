package org.sevensoft.ecreator.help.seo;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.ToolTip;

/**
 * @author sks 11 Apr 2007 15:16:45
 *
 */
public class AutoContentTip extends ToolTip {

	@Override
	protected String getHelp() {
		return "Enabling this feature will search your category content and when there is a word or phrase that matches a category name exactly it will create a link to that category. example of this is if you have a category called 'bed and breakfast' and you add the words 'bed and breakfast' into another category as part of the content then the words 'bed and breakfast' will become a link which when clicked on will take you to the 'bed and breakfast' category. This when used correctly will not only make navigation of your site easier but the search engines approve of internal links within the site. improving your sites Search Engine Optimisation.";
	}

}
