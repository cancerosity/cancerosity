package org.sevensoft.ecreator.help.seo;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.ToolTip;

/**
 * @author sks 4 Apr 2007 12:12:30
 *
 */
public class TitleTagHelp extends ToolTip {

	@Override
	protected String getHelp() {
		return "Title Tags are one of the most important factors when considering optimisation of your websites pages for search engines. "
				+ "The search engines use your keywords that are found in your title tag and usually use these for the title of your listing in the search engines results.\n\n"
				+ "We recommend no more than 80 characters - including spaces. We advise that you have a relevant description of your webpage with keywords included.\n\n"
				+ "The following will add relevant information to your title tags:\n\n"
				+ "For the name of the website in your title tag then use -   [site]\n"
				+ "For the name of the relevant category in your title tag then use - [category]\n"
				+ "For the name of the relevant page in your title tag then use -   [name]";
	}
}
