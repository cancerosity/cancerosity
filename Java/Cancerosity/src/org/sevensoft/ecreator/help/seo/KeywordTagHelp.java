package org.sevensoft.ecreator.help.seo;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.ToolTip;

/**
 * @author sks 11 Apr 2007 15:12:48
 *
 */
public class KeywordTagHelp extends ToolTip {

	@Override
	protected String getHelp() {
		return "Keyword Tags are a list of keywords that certain search engines use to help determine what search terms your site is found. So it is recomended that you include these. These do not need to be in a sentence just in a list. There is not an official maximum of characters that can be used, but the guidelines that are commonly mentioned are up to 500. Try to repeat your most important keywords but no more than 4 times and do not have them one after the other.";
	}

}
