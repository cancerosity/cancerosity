package org.sevensoft.ecreator.help.seo;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.ToolTip;

/**
 * @author sks 11 Apr 2007 15:14:47
 *
 */
public class AutoContentOverrideTip extends ToolTip {

	@Override
	protected String getHelp() {
		return "If you are using 'Auto Content Linking' then this function will then allow you to edit a category and turn off the Auto Content Linking faciltiy in that category.";
	}

}
