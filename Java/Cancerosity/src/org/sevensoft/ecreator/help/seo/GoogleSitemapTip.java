package org.sevensoft.ecreator.help.seo;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.ToolTip;

/**
 * @author sks 11 Apr 2007 15:15:43
 *
 */
public class GoogleSitemapTip extends ToolTip {

	@Override
	protected String getHelp() {
		return "Click on 'Google Sitemap XML'  this will give you the url of your website's Google Sitemap which you will need to submit to google. To do this visit http://www.google.com/webmasters/sitemaps/login";
	}

}
