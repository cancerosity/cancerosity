package org.sevensoft.ecreator.help.seo;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.ToolTip;

/**
 * @author sks 11 Apr 2007 15:13:18
 *
 */
public class KeywordDensityTip extends ToolTip {

	@Override
	protected String getHelp() {
		return "With this feature enabled you can edit a category and it will show you a table with the 15 most used words in the categories text and show you how many times and percentage these word occur. This is useful as a good Search Engine Optimised page will have relevant kewords for that page occur between 3% and 5%.";
	}

}
