package org.sevensoft.ecreator.iface;

import org.sevensoft.ecreator.model.ecom.shopping.ShoppingSettings;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.system.config.Captions;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Modules;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.model.design.template.Template;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.i18n.Messages;

import javax.servlet.ServletException;
import java.util.logging.Logger;

/**
 * @author sks 31-Jul-2005 20:03:46
 */
public abstract class EcreatorHandler extends Handler {

    protected static Logger logger = Logger.getLogger("ecreator");

    protected transient Captions captions;

    /**
     * Current currency string
     */
    protected transient String currencySymbol;

    protected transient Config config;

    protected transient Modules modules;

    protected transient ShoppingSettings shoppingSettings;

    protected transient MiscSettings miscSettings;

    /**
     * The currently logged in admin user
     */
    protected transient User user;

    protected transient Template template;

    public EcreatorHandler(RequestContext context) {

        super(context);

        user = (User) getAttribute("user");

        context.addHeader("Cache-Control", "no-cache");

        this.config = Config.getInstance(context);
        this.modules = Modules.getInstance(context);
        this.shoppingSettings = ShoppingSettings.getInstance(context);
        this.miscSettings = MiscSettings.getInstance(context);
        this.captions = Captions.getInstance(context);
    }

    /*protected String getRealThumbnailsPath() {
        return context.getRealPath(config.getThumbnailsPath());
    }*/

    @Override
    protected Object init() throws ServletException {

        user = User.getUserFromSession(context, context.getSessionId());
        if (user != null) {
            setAttribute("user", user);
        }

        template = Template.getFromSession(context);
        if (template != null) {
            setAttribute("template", template);
        }

        /*
           * If our IP is in the superman list then up us to superman level
           */
        if (User.isSuperIp(context)) {

            /*
                * Check superman off cookie value
                */
            if (!"off".equals(context.getCookieValue("superman"))) {
                context.setAttribute("superman", "true");
            }
        }

        // if we are in demo mode updating last access timestamp
        if (config.isDemo()) {
            config.setLastAccessTimestamp();
        }

        return null;
    }

    public boolean isSamIp() {
        return "81.168.52.32".equals(context.getRemoteIp());
    }

    public final boolean isSuperman() {
        return User.isSuperman(context);
    }

    
}
