package org.sevensoft.ecreator.iface.cron;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.model.accounts.sessions.AccountSessionRunner;
import org.sevensoft.ecreator.model.accounts.subscriptions.bots.SubscriptionExpiryBot;
import org.sevensoft.ecreator.model.accounts.subscriptions.bots.SubscriptionReminderBot;
import org.sevensoft.ecreator.model.accounts.subscriptions.bots.SubscriptedListingsDeleleterBot;
import org.sevensoft.ecreator.model.attachments.downloads.DownloadReaper;
import org.sevensoft.ecreator.model.attachments.pdfs.bot.PdfsRenameBot;
import org.sevensoft.ecreator.model.ecom.orders.bots.OrderPaymentsBot;
import org.sevensoft.ecreator.model.ecom.shopping.bots.BasketReaperBot;
import org.sevensoft.ecreator.model.extras.rss.bot.NewsfeedsBot;
import org.sevensoft.ecreator.model.feeds.FeedRunner;
import org.sevensoft.ecreator.model.interaction.winks.WinkBot;
import org.sevensoft.ecreator.model.items.ItemViews;
import org.sevensoft.ecreator.model.items.ItemViewsReaper;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.bots.ExpiryBotRunner;
import org.sevensoft.ecreator.model.items.listings.bots.ListingExpiryBot;
import org.sevensoft.ecreator.model.items.listings.bots.ListingExpiryDeleterBot;
import org.sevensoft.ecreator.model.misc.Interval;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.ecreator.model.reminders.services.HolidayReminderServiceRunner;
import org.sevensoft.ecreator.model.stats.StatsReaper;
import org.sevensoft.ecreator.model.stats.visits.VisitorCounterBot;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHitCounterDaily;
import org.sevensoft.ecreator.model.system.messages.SystemMessage;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.users.UserSessionBot;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.model.media.images.bot.ImagesRenameBot;
import org.sevensoft.ecreator.model.crm.forms.submission.report.SubmissionReportRunner;
import org.sevensoft.ecreator.model.marketing.newsletter.bots.NewsletterNewItems;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.logging.Logger;
import java.util.List;

/**
 * @author sks 15 Apr 2007 10:40:25
 */
public class CronDailyTasks implements Logging {

    private static Logger logger = Logger.getLogger("cron");
    private final RequestContext context;

    public CronDailyTasks(RequestContext context) {
        this.context = context;
    }

    public void run() {
        if (Module.CronStop.enabled(context)) {
            return;
        }

        log("Cron daily task has been started");

        if (Module.FilesRenameBots.enabled(context)) {
            logger.config("[CronDaily] files rename bots");
            new ImagesRenameBot(context).run();
            new PdfsRenameBot(context).run();
        }

        new NewsfeedsBot(context).run();

        if (Module.Listings.enabled(context)) {
            logger.config("[CronDaily] access reminder bots");
            try {
                new ListingExpiryBot(context).run();
                new ListingExpiryDeleterBot(context).run();
            } catch (Exception e) {
                logger.warning(e.toString());
            }
        }

        if (Module.HolidayReminders.enabled(context)) {
            new HolidayReminderServiceRunner(context).run();
        }

        if (Module.Shopping.enabled(context)) {
            new BasketReaperBot(context).run();
        }

        if (Module.NewsletterNewItems.enabled(context)){
            new NewsletterNewItems(context).run();
        }
        logger.config("[CronDaily] resetting item views");
        SimpleQuery.delete(context, ItemViews.class);

        new AccountSessionRunner(context).run();

        new UserSessionBot(context).run();

        new StatsReaper(context).run();

        new ItemViewsReaper(context).run();

        logger.fine("[CronDaily] wink runner");
        new WinkBot(context).run();

        if (Module.Subscriptions.enabled(context)) {

            new SubscriptionReminderBot(context).run();
            List<Item> expired= new SubscriptionExpiryBot(context).run();

            if (Module.SubscriptionDeleter.enabled(context)){
                new SubscriptedListingsDeleleterBot(context).run(expired);
            }
            
        }

        // now bill all outstanding server payment orders
        new OrderPaymentsBot(context).run();

        new DownloadReaper(context).run();

        new FeedRunner(context, Interval.Daily).run();

        new ExpiryBotRunner(context, Interval.Daily).run();

        new VisitorCounterBot(context).run();

        /*
           * Get daily stats and create system message
           */
        List<SiteHitCounterDaily> counters = SiteHitCounterDaily.get(context, new Date().removeDays(1), new Date());
        if (!counters.isEmpty()) {
            SiteHitCounterDaily counter = counters.get(0);
            if (counter != null) {
                new SystemMessage(context, "Yesterday your site received " + counter.getTotal() + " page views and " + counter.getUnique() + " unique visits");
            }
        }

        if (Module.FormsReport.enabled(context)){
             new SubmissionReportRunner(context).run();
        }
    }

    public String getFullId() {
        return null;
    }

    public String getLogName() {
        return CronDailyTasks.class.getName();
    }

    public void log(String message) {
        new LogEntry(context, this, message);
    }

    public void log(User user, String message) {
    }
}
