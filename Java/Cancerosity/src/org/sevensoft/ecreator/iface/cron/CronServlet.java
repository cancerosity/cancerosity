package org.sevensoft.ecreator.iface.cron;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;
import org.sevensoft.jeezy.http.DatabaseServlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

public class CronServlet extends DatabaseServlet {
    private static String cronHourlyLocation, cronDailyLocation;

    public static String getCronDailyLocation() {
        return cronDailyLocation;
    }

    public static String getCronHourlyLocation() {
        return cronHourlyLocation;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
//        cronHourlyLocation = getInitParameter("cron-location-hourly");
        cronDailyLocation = getInitParameter("cron-location-daily");
        try {
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            //hourly
//            JobDetail jdHourly = new JobDetail("job-hourly", "group-hourly", CronHourlyJob.class);
//            CronTrigger ctHourly = new CronTrigger("trigger-hourly", "group-hourly");
//            String cronExprHourly = getInitParameter("cron-hourly");
//			//System.out.println(cronExprHourly);
//            ctHourly.setCronExpression(cronExprHourly);
//            scheduler.scheduleJob(jdHourly, ctHourly);
            //daily
            JobDetail jdDaily = new JobDetail("job-daily", "group-daily", CronDailyJob.class);
            CronTrigger ctDaily = new CronTrigger("trigger-daily", "group-daily");
            String cronExprDaily = getInitParameter("cron-daily");
//			System.out.println(cronExprDaily);
            ctDaily.setCronExpression(cronExprDaily);
            scheduler.scheduleJob(jdDaily, ctDaily);
            //schedule
            scheduler.start();
//			System.out.println("Job scheduled now ..");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}