package org.sevensoft.ecreator.iface.cron;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.httpc.HttpMethod;

import java.util.logging.Logger;

public class CronHourlyJob implements Job {
	private Logger logger = Logger.getLogger(CronHourlyJob.class.getName());
	
	public CronHourlyJob() {		
	}

    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        try {
            new HttpClient(CronServlet.getCronHourlyLocation(), HttpMethod.Get).connect();
        } catch (Exception e) {
            logger.severe("[" + CronHourlyJob.class.getName() + "] cannot run hourly task " + e);
        }
    }
}
