package org.sevensoft.ecreator.iface.cron;

import java.util.logging.Logger;

import org.sevensoft.ecreator.model.feeds.FeedRunner;
import org.sevensoft.ecreator.model.items.bots.ExpiryBotRunner;
import org.sevensoft.ecreator.model.misc.Interval;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.system.messages.SystemMessage;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 15 Apr 2007 10:41:28
 *
 */
public class CronHourlyTasks implements Logging {

	private static Logger		logger	= Logger.getLogger("cron");
	private final RequestContext	context;

	public CronHourlyTasks(RequestContext context) {
		this.context = context;
	}

    public void run() {
        if (Module.CronStop.enabled(context)) {
            return;
        }

        log("Cron hourly task has been started");

        new FeedRunner(context, Interval.Hourly).run();
        new ExpiryBotRunner(context, Interval.Hourly).run();
    }

    public String getFullId() {
        return null;
    }

    public String getLogName() {
        return CronHourlyTasks.class.getName();
    }

    public void log(String message) {
        new LogEntry(context, this, message);
    }

    public void log(User user, String message) {
    }
}
