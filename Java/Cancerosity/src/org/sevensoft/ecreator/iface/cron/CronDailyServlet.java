package org.sevensoft.ecreator.iface.cron;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.jeezy.http.DatabaseServlet;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 22-Aug-2005 19:08:04
 * 
 */
public class CronDailyServlet extends DatabaseServlet {

	@Override
	protected void doGet(HttpServletRequest arg0, HttpServletResponse arg1) throws ServletException, IOException {

		RequestContext context = new RequestContext(arg0, arg1, servletContext, ds, schemaValidator);
		new CronDailyTasks(context).run();

	}

}
