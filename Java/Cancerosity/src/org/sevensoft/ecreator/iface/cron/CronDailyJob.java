package org.sevensoft.ecreator.iface.cron;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.sevensoft.commons.httpc.HttpClient;
import org.sevensoft.commons.httpc.HttpMethod;

import java.util.logging.Logger;

public class CronDailyJob implements Job {
    private Logger logger = Logger.getLogger(CronDailyJob.class.getName());

    public CronDailyJob() {
    }

    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        try {
            new HttpClient(CronServlet.getCronDailyLocation(), HttpMethod.Get).connect();
        } catch (Exception e) {
            logger.severe("[" + CronDailyJob.class.getName() + "] cannot run daily task " + e);
        }
    }
}
