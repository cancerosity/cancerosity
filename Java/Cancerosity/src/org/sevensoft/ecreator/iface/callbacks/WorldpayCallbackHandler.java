package org.sevensoft.ecreator.iface.callbacks;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.iface.EcreatorHandler;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;

/**
 * @author sks 28 Apr 2006 16:36:40
 *
 */
@Path("callback-worldpay.do")
public class WorldpayCallbackHandler extends FrontendHandler {

	public WorldpayCallbackHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		try {

			final String url = PaymentType.WorldPaySelectJunior.getForm(context).serverCallback(getParameters(), getRemoteIp());

			if (url == null) {
				return HttpServletResponse.SC_OK;
			} else {
                FrontendDoc doc = new FrontendDoc(context, "Returning to the site. Please wait...");
                doc.addBody(new Body() {
                    @Override
                    public String toString() {
                        return "<head><meta http-equiv='refresh' content='0;" + url + "' /></head>";
                    }
                });
                return doc;
			}

		} catch (IOException e) {
			e.printStackTrace();
			return HttpServletResponse.SC_BAD_REQUEST;

		} catch (FormException e) {
			e.printStackTrace();
			return HttpServletResponse.SC_BAD_REQUEST;
		}

	}

	@Override
	protected boolean runSecure() {
		return false;
	}

}
