package org.sevensoft.ecreator.iface.callbacks;

import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.system.config.Config;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * User: Tanya
 * Date: 11.01.2013
 */
@Path("callback-globaliris.do")
public class GlobalIrisRealAuthCallbackHandler extends Handler {

    private static Logger logger = Logger.getLogger(GoogleCheckoutCallbackHandler.class.getName());

    public GlobalIrisRealAuthCallbackHandler(RequestContext context) {
        super(context);
    }

    protected Object init() throws ServletException {
        return null;
    }

    public Object main() throws ServletException {
         try {

             String url = PaymentType.GlobalIris.getForm(context).serverCallback(getParameters(), getRemoteIp());
             if (url == null) {
                 return HttpServletResponse.SC_OK;
             } else {
                 return "<html><body onload=\"window.location='" + url + "';\">Returning to the site.<br/>Please wait....</body></html>";
             }

        } catch (IOException e) {
            logger.warning("[" + GlobalIrisRealAuthCallbackHandler.class + "] " + e);
            return HttpServletResponse.SC_BAD_REQUEST;

        } catch (FormException e) {
            logger.warning("[" + GlobalIrisRealAuthCallbackHandler.class + "] " + e);
            return HttpServletResponse.SC_BAD_REQUEST;
        }
    }

    protected boolean runSecure() {
        return Config.getInstance(context).isSecured();
    }
}
