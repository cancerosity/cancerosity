package org.sevensoft.ecreator.iface.callbacks;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 28 May 2006 22:35:29
 *
 */
@Path("callback-able2buy.do")
public class Able2BuyCallback extends Handler {

	public Able2BuyCallback(RequestContext context) {
		super(context);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		try {

			PaymentType.Able2Buy.getForm(context).serverCallback(getParameters(), getRemoteIp());
			return HttpServletResponse.SC_OK;

		} catch (IOException e) {
			e.printStackTrace();
			return HttpServletResponse.SC_BAD_REQUEST;

		} catch (FormException e) {
			e.printStackTrace();
			return HttpServletResponse.SC_BAD_REQUEST;
		}
	}

	@Override
	protected boolean runSecure() {
		return false;
	}

}
