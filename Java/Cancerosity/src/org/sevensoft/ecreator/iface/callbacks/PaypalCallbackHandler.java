package org.sevensoft.ecreator.iface.callbacks;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.iface.EcreatorHandler;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.HttpCode;

/**
 * @author sks 28 Apr 2006 16:36:40
 *
 */
@Path("callback-paypal.do")
public class PaypalCallbackHandler extends EcreatorHandler {

	@SuppressWarnings("hiding")
	private static Logger	logger	= Logger.getLogger("payments");

	public PaypalCallbackHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		try {

			logger.config("[PaypalCallbackHandler] params=" + getParameters());
			String url = PaymentType.PayPalStandard.getForm(context).serverCallback(getParameters(), getRemoteIp());
			logger.config("[PaypalCallbackHandler] url=" + url);

			if (url == null) {
				return HttpServletResponse.SC_OK;
			} else {
				return "<html><body onload=\"window.location='" + url + "';\">Returning to the site.<br/>Please wait....</body></html>";
			}

		} catch (IOException e) {
			e.printStackTrace();
			return HttpServletResponse.SC_BAD_REQUEST;

		} catch (FormException e) {
			e.printStackTrace();
			return new HttpCode(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
		}
	}

	@Override
	protected boolean runSecure() {
		return false;
	}

}
