package org.sevensoft.ecreator.iface.callbacks;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 28 May 2006 22:35:37
 *
 */
@Path("callback-epdqcpi.do")
public class EpdqCpiCallbackHandler extends Handler {

	public EpdqCpiCallbackHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		try {

			PaymentType.EpdqCpi.getForm(context).serverCallback(getParameters(), null);
			return HttpServletResponse.SC_OK;

		} catch (IOException e) {
			e.printStackTrace();
			
		} catch (FormException e) {
			e.printStackTrace();
		}

		return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
	}

	@Override
	protected boolean runSecure() {
		return false;
	}

}
