package org.sevensoft.ecreator.iface.callbacks;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.iface.EcreatorHandler;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 28 Apr 2006 16:36:40
 *
 */
@Path("callback-securetrading.do")
public class SecureTradingCallbackHandler extends EcreatorHandler {

	public SecureTradingCallbackHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		try {

			PaymentType.SecureTradingPages.getForm(context).serverCallback(getParameters(), getRemoteIp());
			return HttpServletResponse.SC_OK;

		} catch (IOException e) {
			e.printStackTrace();
			return HttpServletResponse.SC_BAD_REQUEST;

		} catch (FormException e) {
			e.printStackTrace();
			return HttpServletResponse.SC_BAD_REQUEST;
		}

	}

	@Override
	protected boolean runSecure() {
		return false;
	}

}
