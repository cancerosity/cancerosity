package org.sevensoft.ecreator.iface.callbacks;

import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.CheckoutHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Logger;
import java.io.IOException;

/**
 * User: Tanya
 * Date: 25.08.2011
 */
@Path("callback-cardsave.do")
public class CardsaveCallbackHandler extends Handler {

    private static Logger logger = Logger.getLogger(CardsaveCallbackHandler.class.getName());

    public CardsaveCallbackHandler(RequestContext context) {
        super(context);
    }

    protected Object init() throws ServletException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object main() throws ServletException {
        try {

            String ret = PaymentType.CardsaveRedirect.getForm(context).serverCallback(getParameters(), getRemoteIp());
            if (ret != null) {
                return new ActionDoc(context, "", new Link(CheckoutHandler.class, "completed", "basket", ret));
            } else {
                return new ActionDoc(context, "", new Link(CategoryHandler.class));
            }

        } catch (IOException e) {
            logger.warning("[" + CardsaveCallbackHandler.class + "] " + e);
            return HttpServletResponse.SC_BAD_REQUEST;

        } catch (FormException e) {
            logger.warning("[" + CardsaveCallbackHandler.class + "] " + e);
            return HttpServletResponse.SC_BAD_REQUEST;
        }
    }

    protected boolean runSecure() {
        return false;
    }
}
