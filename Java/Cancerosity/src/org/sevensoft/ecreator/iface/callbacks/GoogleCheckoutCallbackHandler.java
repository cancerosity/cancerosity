package org.sevensoft.ecreator.iface.callbacks;

import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
@Path("callback-google.do")
public class GoogleCheckoutCallbackHandler extends Handler {

    private static Logger logger = Logger.getLogger(GoogleCheckoutCallbackHandler.class.getName());

    public GoogleCheckoutCallbackHandler(RequestContext context) {
        super(context);
    }

    protected Object init() throws ServletException {
        return null;
    }

    public Object main() throws ServletException {
        try {

            PaymentType.GoogleCheckout.getForm(context).serverCallback(getParameters(), getRemoteIp());
            return HttpServletResponse.SC_OK;

        } catch (IOException e) {
            logger.warning("[" + GoogleCheckoutCallbackHandler.class + "] " + e);
            return HttpServletResponse.SC_BAD_REQUEST;

        } catch (FormException e) {
            logger.warning("[" + GoogleCheckoutCallbackHandler.class + "] " + e);
            return HttpServletResponse.SC_BAD_REQUEST;
        }
    }

    protected boolean runSecure() {
        return true;
    }
}
