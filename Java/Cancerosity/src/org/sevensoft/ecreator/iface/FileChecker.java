package org.sevensoft.ecreator.iface;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.commons.simpleio.SimpleUrl;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 9 Mar 2007 07:29:15
 *
 */
public class FileChecker {

	private static Logger	logger	= Logger.getLogger("ecreator");

	public static void run(RequestContext context) {

		if (context.isLocalhost()) {
			return;
		}

		List<String> filenames = new ArrayList();

		filenames.add("/files/graphics/admin/icon_help.gif");

		filenames.add("/files/graphics/admin/toolbar_viewsite_hover.gif");
		filenames.add("/files/graphics/admin/toolbar_viewsite.gif");
		filenames.add("/files/graphics/admin/toolbar_validate.gif");
		filenames.add("/files/graphics/admin/toolbar_validate_hover.gif");
		filenames.add("/files/graphics/admin/toolbar_frontpage_hover.gif");
		filenames.add("/files/graphics/admin/toolbar_frontpage.gif");
		filenames.add("/files/graphics/admin/toolbar_controlpanel_hover.gif");
		filenames.add("/files/graphics/admin/toolbar_controlpanel.gif");
		filenames.add("/files/graphics/admin/toolbar_edit.gif");
		filenames.add("/files/graphics/admin/toolbar_edit_hover.gif");
		filenames.add("/files/js/misc/misc.js");

		// help balloons
		filenames.add("/files/graphics/helpballoon/balloon_l.gif");
		filenames.add("/files/graphics/helpballoon/balloon_r.gif");
		filenames.add("/files/graphics/helpballoon/balloon_t.gif");
		filenames.add("/files/graphics/helpballoon/balloon_b.gif");
		filenames.add("/files/graphics/helpballoon/balloon_tl.gif");
		filenames.add("/files/graphics/helpballoon/balloon_bl.gif");
		filenames.add("/files/graphics/helpballoon/balloon_tr.gif");
		filenames.add("/files/graphics/helpballoon/balloon_br.gif");
		filenames.add("/files/graphics/helpballoon/balloon_back.gif");

		filenames.add("/files/graphics/admin/drag.gif");
		filenames.add("/files/graphics/admin/spanner.gif");
		filenames.add("/files/graphics/admin/cog.gif");
		filenames.add("/files/graphics/admin/cross.gif");
		filenames.add("/files/graphics/admin/tick.gif");
		filenames.add("/files/graphics/admin/delete.gif");
		filenames.add("/files/graphics/admin/traffic_amber.gif");
		filenames.add("/files/graphics/admin/traffic_green.gif");
		filenames.add("/files/graphics/admin/traffic_red.gif");
		filenames.add("/files/graphics/admin/arrowup.gif");
		filenames.add("/files/graphics/admin/arrowdown.gif");
		filenames.add("/files/graphics/admin/visitor_offline.gif");
		filenames.add("/files/graphics/admin/visitor_online.gif");

		filenames.add("/files/graphics/rss/rss.gif");

		filenames.add("/files/js/admin/scripts.js");

		filenames.add("/files/js/jquery/jquery.js");
		filenames.add("/files/js/jquery/jquery.form.js");

		filenames.add("/files/js/jquery/date/calendar.png");
		filenames.add("/files/js/jquery/date/styles.css");
		filenames.add("/files/js/jquery/date/datePicker.js");

		filenames.add("/files/graphics/misc/loadingAnimation.gif");

		filenames.add("/files/graphics/flags/ad.gif");
		filenames.add("/files/graphics/flags/ae.gif");
		filenames.add("/files/graphics/flags/af.gif");
		filenames.add("/files/graphics/flags/ag.gif");
		filenames.add("/files/graphics/flags/ai.gif");
		filenames.add("/files/graphics/flags/al.gif");
		filenames.add("/files/graphics/flags/am.gif");
		filenames.add("/files/graphics/flags/an.gif");
		filenames.add("/files/graphics/flags/ao.gif");
		filenames.add("/files/graphics/flags/ap.gif");
		filenames.add("/files/graphics/flags/ar.gif");
		filenames.add("/files/graphics/flags/as.gif");
		filenames.add("/files/graphics/flags/at.gif");
		filenames.add("/files/graphics/flags/au.gif");
		filenames.add("/files/graphics/flags/aw.gif");
		filenames.add("/files/graphics/flags/az.gif");
		filenames.add("/files/graphics/flags/ba.gif");
		filenames.add("/files/graphics/flags/bb.gif");
		filenames.add("/files/graphics/flags/bd.gif");
		filenames.add("/files/graphics/flags/be.gif");
		filenames.add("/files/graphics/flags/bf.gif");
		filenames.add("/files/graphics/flags/bg.gif");
		filenames.add("/files/graphics/flags/bh.gif");
		filenames.add("/files/graphics/flags/bi.gif");
		filenames.add("/files/graphics/flags/bj.gif");
		filenames.add("/files/graphics/flags/bm.gif");
		filenames.add("/files/graphics/flags/bn.gif");
		filenames.add("/files/graphics/flags/bo.gif");
		filenames.add("/files/graphics/flags/br.gif");
		filenames.add("/files/graphics/flags/bs.gif");
		filenames.add("/files/graphics/flags/bt.gif");
		filenames.add("/files/graphics/flags/bw.gif");
		filenames.add("/files/graphics/flags/by.gif");
		filenames.add("/files/graphics/flags/bz.gif");
		filenames.add("/files/graphics/flags/ca.gif");
		filenames.add("/files/graphics/flags/cd.gif");
		filenames.add("/files/graphics/flags/cf.gif");
		filenames.add("/files/graphics/flags/cg.gif");
		filenames.add("/files/graphics/flags/ch.gif");
		filenames.add("/files/graphics/flags/ci.gif");
		filenames.add("/files/graphics/flags/ck.gif");
		filenames.add("/files/graphics/flags/cl.gif");
		filenames.add("/files/graphics/flags/cm.gif");
		filenames.add("/files/graphics/flags/cn.gif");
		filenames.add("/files/graphics/flags/co.gif");
		filenames.add("/files/graphics/flags/cr.gif");
		filenames.add("/files/graphics/flags/cs.gif");
		filenames.add("/files/graphics/flags/cu.gif");
		filenames.add("/files/graphics/flags/cv.gif");
		filenames.add("/files/graphics/flags/cy.gif");
		filenames.add("/files/graphics/flags/cz.gif");
		filenames.add("/files/graphics/flags/de.gif");
		filenames.add("/files/graphics/flags/dj.gif");
		filenames.add("/files/graphics/flags/dk.gif");
		filenames.add("/files/graphics/flags/dm.gif");
		filenames.add("/files/graphics/flags/do.gif");
		filenames.add("/files/graphics/flags/dz.gif");
		filenames.add("/files/graphics/flags/ec.gif");
		filenames.add("/files/graphics/flags/ee.gif");
		filenames.add("/files/graphics/flags/eg.gif");
		filenames.add("/files/graphics/flags/er.gif");
		filenames.add("/files/graphics/flags/es.gif");
		filenames.add("/files/graphics/flags/et.gif");
		filenames.add("/files/graphics/flags/eu.gif");
		filenames.add("/files/graphics/flags/fi.gif");
		filenames.add("/files/graphics/flags/fj.gif");
		filenames.add("/files/graphics/flags/fk.gif");
		filenames.add("/files/graphics/flags/fm.gif");
		filenames.add("/files/graphics/flags/fo.gif");
		filenames.add("/files/graphics/flags/fr.gif");
		filenames.add("/files/graphics/flags/ga.gif");
		filenames.add("/files/graphics/flags/gb.gif");
		filenames.add("/files/graphics/flags/gd.gif");
		filenames.add("/files/graphics/flags/ge.gif");
		filenames.add("/files/graphics/flags/gh.gif");
		filenames.add("/files/graphics/flags/gi.gif");
		filenames.add("/files/graphics/flags/gl.gif");
		filenames.add("/files/graphics/flags/gm.gif");
		filenames.add("/files/graphics/flags/gn.gif");
		filenames.add("/files/graphics/flags/gp.gif");
		filenames.add("/files/graphics/flags/gq.gif");
		filenames.add("/files/graphics/flags/gr.gif");
		filenames.add("/files/graphics/flags/gt.gif");
		filenames.add("/files/graphics/flags/gu.gif");
		filenames.add("/files/graphics/flags/gw.gif");
		filenames.add("/files/graphics/flags/gy.gif");
		filenames.add("/files/graphics/flags/hk.gif");
		filenames.add("/files/graphics/flags/hn.gif");
		filenames.add("/files/graphics/flags/hr.gif");
		filenames.add("/files/graphics/flags/ht.gif");
		filenames.add("/files/graphics/flags/hu.gif");
		filenames.add("/files/graphics/flags/id.gif");
		filenames.add("/files/graphics/flags/ie.gif");
		filenames.add("/files/graphics/flags/il.gif");
		filenames.add("/files/graphics/flags/in.gif");
		filenames.add("/files/graphics/flags/io.gif");
		filenames.add("/files/graphics/flags/iq.gif");
		filenames.add("/files/graphics/flags/ir.gif");
		filenames.add("/files/graphics/flags/is.gif");
		filenames.add("/files/graphics/flags/it.gif");
		filenames.add("/files/graphics/flags/jm.gif");
		filenames.add("/files/graphics/flags/jo.gif");
		filenames.add("/files/graphics/flags/jp.gif");
		filenames.add("/files/graphics/flags/ke.gif");
		filenames.add("/files/graphics/flags/kg.gif");
		filenames.add("/files/graphics/flags/kh.gif");
		filenames.add("/files/graphics/flags/ki.gif");
		filenames.add("/files/graphics/flags/km.gif");
		filenames.add("/files/graphics/flags/kn.gif");
		filenames.add("/files/graphics/flags/kp.gif");
		filenames.add("/files/graphics/flags/kr.gif");
		filenames.add("/files/graphics/flags/kw.gif");
		filenames.add("/files/graphics/flags/ky.gif");
		filenames.add("/files/graphics/flags/kz.gif");
		filenames.add("/files/graphics/flags/la.gif");
		filenames.add("/files/graphics/flags/lb.gif");
		filenames.add("/files/graphics/flags/lc.gif");
		filenames.add("/files/graphics/flags/li.gif");
		filenames.add("/files/graphics/flags/lk.gif");
		filenames.add("/files/graphics/flags/lr.gif");
		filenames.add("/files/graphics/flags/ls.gif");
		filenames.add("/files/graphics/flags/lt.gif");
		filenames.add("/files/graphics/flags/lu.gif");
		filenames.add("/files/graphics/flags/lv.gif");
		filenames.add("/files/graphics/flags/ly.gif");
		filenames.add("/files/graphics/flags/ma.gif");
		filenames.add("/files/graphics/flags/mc.gif");
		filenames.add("/files/graphics/flags/md.gif");
		filenames.add("/files/graphics/flags/mg.gif");
		filenames.add("/files/graphics/flags/mh.gif");
		filenames.add("/files/graphics/flags/mk.gif");
		filenames.add("/files/graphics/flags/ml.gif");
		filenames.add("/files/graphics/flags/mm.gif");
		filenames.add("/files/graphics/flags/mn.gif");
		filenames.add("/files/graphics/flags/mo.gif");
		filenames.add("/files/graphics/flags/mp.gif");
		filenames.add("/files/graphics/flags/mq.gif");
		filenames.add("/files/graphics/flags/mr.gif");
		filenames.add("/files/graphics/flags/ms.gif");
		filenames.add("/files/graphics/flags/mt.gif");
		filenames.add("/files/graphics/flags/mu.gif");
		filenames.add("/files/graphics/flags/mv.gif");
		filenames.add("/files/graphics/flags/mw.gif");
		filenames.add("/files/graphics/flags/mx.gif");
		filenames.add("/files/graphics/flags/my.gif");
		filenames.add("/files/graphics/flags/mz.gif");
		filenames.add("/files/graphics/flags/na.gif");
		filenames.add("/files/graphics/flags/nc.gif");
		filenames.add("/files/graphics/flags/ne.gif");
		filenames.add("/files/graphics/flags/nf.gif");
		filenames.add("/files/graphics/flags/ng.gif");
		filenames.add("/files/graphics/flags/ni.gif");
		filenames.add("/files/graphics/flags/nl.gif");
		filenames.add("/files/graphics/flags/no.gif");
		filenames.add("/files/graphics/flags/np.gif");
		filenames.add("/files/graphics/flags/nr.gif");
		filenames.add("/files/graphics/flags/nz.gif");
		filenames.add("/files/graphics/flags/om.gif");
		filenames.add("/files/graphics/flags/pa.gif");
		filenames.add("/files/graphics/flags/pe.gif");
		filenames.add("/files/graphics/flags/pf.gif");
		filenames.add("/files/graphics/flags/pg.gif");
		filenames.add("/files/graphics/flags/ph.gif");
		filenames.add("/files/graphics/flags/pk.gif");
		filenames.add("/files/graphics/flags/pl.gif");
		filenames.add("/files/graphics/flags/pm.gif");
		filenames.add("/files/graphics/flags/pr.gif");
		filenames.add("/files/graphics/flags/ps.gif");
		filenames.add("/files/graphics/flags/pt.gif");
		filenames.add("/files/graphics/flags/pw.gif");
		filenames.add("/files/graphics/flags/py.gif");
		filenames.add("/files/graphics/flags/qa.gif");
		filenames.add("/files/graphics/flags/re.gif");
		filenames.add("/files/graphics/flags/ro.gif");
		filenames.add("/files/graphics/flags/ru.gif");
		filenames.add("/files/graphics/flags/rw.gif");
		filenames.add("/files/graphics/flags/sa.gif");
		filenames.add("/files/graphics/flags/sb.gif");
		filenames.add("/files/graphics/flags/sc.gif");
		filenames.add("/files/graphics/flags/sd.gif");
		filenames.add("/files/graphics/flags/se.gif");
		filenames.add("/files/graphics/flags/sg.gif");
		filenames.add("/files/graphics/flags/si.gif");
		filenames.add("/files/graphics/flags/sk.gif");
		filenames.add("/files/graphics/flags/sl.gif");
		filenames.add("/files/graphics/flags/sm.gif");
		filenames.add("/files/graphics/flags/sn.gif");
		filenames.add("/files/graphics/flags/so.gif");
		filenames.add("/files/graphics/flags/sr.gif");
		filenames.add("/files/graphics/flags/st.gif");
		filenames.add("/files/graphics/flags/sv.gif");
		filenames.add("/files/graphics/flags/sy.gif");
		filenames.add("/files/graphics/flags/sz.gif");
		filenames.add("/files/graphics/flags/tc.gif");
		filenames.add("/files/graphics/flags/td.gif");
		filenames.add("/files/graphics/flags/tf.gif");
		filenames.add("/files/graphics/flags/tg.gif");
		filenames.add("/files/graphics/flags/th.gif");
		filenames.add("/files/graphics/flags/tj.gif");
		filenames.add("/files/graphics/flags/tm.gif");
		filenames.add("/files/graphics/flags/tn.gif");
		filenames.add("/files/graphics/flags/to.gif");
		filenames.add("/files/graphics/flags/tp.gif");
		filenames.add("/files/graphics/flags/tr.gif");
		filenames.add("/files/graphics/flags/tt.gif");
		filenames.add("/files/graphics/flags/tv.gif");
		filenames.add("/files/graphics/flags/tw.gif");
		filenames.add("/files/graphics/flags/tz.gif");
		filenames.add("/files/graphics/flags/ua.gif");
		filenames.add("/files/graphics/flags/ug.gif");
		filenames.add("/files/graphics/flags/uk.gif");
		filenames.add("/files/graphics/flags/um.gif");
		filenames.add("/files/graphics/flags/us.gif");
		filenames.add("/files/graphics/flags/uy.gif");
		filenames.add("/files/graphics/flags/uz.gif");
		filenames.add("/files/graphics/flags/va.gif");
		filenames.add("/files/graphics/flags/vc.gif");
		filenames.add("/files/graphics/flags/ve.gif");
		filenames.add("/files/graphics/flags/vg.gif");
		filenames.add("/files/graphics/flags/vi.gif");
		filenames.add("/files/graphics/flags/vn.gif");
		filenames.add("/files/graphics/flags/vu.gif");
		filenames.add("/files/graphics/flags/ws.gif");
		filenames.add("/files/graphics/flags/ye.gif");
		filenames.add("/files/graphics/flags/yu.gif");
		filenames.add("/files/graphics/flags/za.gif");
		filenames.add("/files/graphics/flags/zm.gif");
		filenames.add("/files/graphics/flags/zr.gif");
		filenames.add("/files/graphics/flags/zw.gif");

		for (String icon : filenames) {

			File file = context.getRealFile(icon);

			if (!file.exists()) {

				try {

					URL url = new URL("http://www.7soft.co.uk" + icon);
					logger.fine("[IconChecker] downloading from=" + url);

					SimpleUrl.downloadToFile(url, file);

				} catch (MalformedURLException e) {
					e.printStackTrace();

				} catch (FileNotFoundException e) {
					e.printStackTrace();

				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}

	}
}
