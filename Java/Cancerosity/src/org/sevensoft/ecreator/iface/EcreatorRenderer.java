package org.sevensoft.ecreator.iface;

import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.extras.languages.Language;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 09-Feb-2006 14:57:39
 *
 */
public class EcreatorRenderer extends Body {

	protected final RequestContext	context;
	protected final Item			account;
	protected final Basket			basket;
	protected final Currency		currency;
	protected final Language		language;
	protected final String			currencySymbol;
	protected final User			user;

	public EcreatorRenderer(RequestContext context) {

		this.context = context;

		this.user = (User) context.getAttribute("user");
		this.account = (Item) context.getAttribute("account");
		this.basket = (Basket) context.getAttribute("basket");
		this.currency = (Currency) context.getAttribute("currency");
		this.currencySymbol = (String) context.getAttribute("currencySymbol");
		this.language = (Language) context.getAttribute("language");

	}
}
