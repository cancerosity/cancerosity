package org.sevensoft.ecreator.iface;

import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Modules;
import org.sevensoft.ecreator.model.system.scripts.Script;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.docs.Doc;

/**
 * @author sks 05-Dec-2005 16:16:05
 * 
 */
public abstract class EcreatorDoc extends Doc {

	protected transient Company		company		= Company.getInstance(context);
	protected transient String		currencySymbol;

	protected transient Config		config		= Config.getInstance(context);

	protected transient MiscSettings	miscSettings	= MiscSettings.getInstance(context);
	protected transient Modules		modules		= Modules.getInstance(context);

	protected transient Seo			seo			= Seo.getInstance(context);

    protected transient Script      scripts     = Script.getInstance(context);

	protected EcreatorDoc(RequestContext context) {
		super(context);
	}

}
