package org.sevensoft.ecreator.iface.frontend.interaction.views;

import java.util.List;

import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.model.interaction.views.View;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Captions;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 6 Dec 2006 15:08:13
 *
 */
public class ViewsRenderer {

	private Item			account;
	private Captions			captions;
	private final RequestContext	context;

	public ViewsRenderer(RequestContext context, Item account) {
		this.context = context;
		this.account = account;
		this.captions = Captions.getInstance(context);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(new TableTag("form").setCaption(captions.getViewsCaptionPlural()));

		sb.append("<tr>");
		sb.append("<th>When</th>");
		sb.append("<th>Who</th>");
		sb.append("<th>Last Active</th>");
		sb.append("</tr>");

		List<View> views = account.getInteraction().getViews(50);
		if (views.size() > 0) {

			for (View view : views) {

				sb.append("<tr>");
				sb.append("<td>" + view.getDate().toString("hh:mma dd/MM/yyyy").toLowerCase() + "</td>");
				sb.append("<td>" + new LinkTag(view.getViewer().getUrl(), view.getViewer().getDisplayName()) + "</td>");
				sb.append("<td>" + view.getViewer().getLastActive() + "</td>");
				sb.append("</tr>");
			}

		} else {

			sb.append("<tr><td colspan='3'>No one has looked at your profile yet!</td></tr>");

		}

		if (account.getItemType().getAccountModule().isShowAccount()) {
			sb.append("<tr><th colspan='3'>To return to your account " + new LinkTag(AccountHandler.class, null, "click here") + "</td></tr>");
		}

		sb.append("</table>");

		return sb.toString();
	}
}
