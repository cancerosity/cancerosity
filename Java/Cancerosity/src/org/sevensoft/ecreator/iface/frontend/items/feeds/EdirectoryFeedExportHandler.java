package org.sevensoft.ecreator.iface.frontend.items.feeds;

import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.StreamResult;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.bespoke.prices.edir.EdirectoryFeed;
import org.jdom.JDOMException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

import com.enterprisedt.net.ftp.FTPException;

/**
 * @author Dmitry Lebedev
 *         Date: 27.10.2009
 */
@Path("admin-feeds-edirectory-export.do")
public class EdirectoryFeedExportHandler extends Handler {

    private EdirectoryFeed feed;
    private int previewCount;

    public EdirectoryFeedExportHandler(RequestContext context) {
        super(context);
    }

    @Override
    protected Object init() throws ServletException {
        return null;
    }

    @Override
    public Object main() throws ServletException {
        try {
            StreamResult result;
            File file = File.createTempFile("edir", ".csv");
            file.deleteOnExit();
            feed.setPreviewCount(previewCount);
            feed.run(file);
            result = new StreamResult(file, "text/plain", "edirectory.csv", StreamResult.Type.Attachment);
            result.deleteOnExit();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
        } catch (FTPException e) {
            e.printStackTrace();
            return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
        } catch (FeedException e) {
            e.printStackTrace();
            return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
        } catch (JDOMException e) {
            e.printStackTrace();
            return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
        }

    }

    @Override
    protected boolean runSecure() {
        return false;
    }
}
