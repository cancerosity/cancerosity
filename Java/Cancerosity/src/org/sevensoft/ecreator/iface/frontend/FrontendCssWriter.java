package org.sevensoft.ecreator.iface.frontend;

import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.jeezy.http.RequestContext;

import java.io.File;
import java.io.IOException;

/**
 * @author sks 27 Nov 2006 16:38:47
 */
public class FrontendCssWriter {

    /**
     *
     */
    private void able2buy(StringBuilder sb) {

        // able2buy credit box
        sb.append("table.ec_able2buy_border { width: 96%; border: 2px solid #008451; } \n");
        sb.append("table.ec_able2buy_border td { padding: 15px; } \n ");
        sb.append("table.ec_able2buy td { padding: 2px; } ");
        sb.append("table.ec_able2buy td.logo { text-align: center; padding: 5px; } \n");
        sb.append("table.ec_able2buy td.rate { font-weight: bold; font-family: Verdana; font-size: 12px; padding: 5px;} \n");
        sb.append("table.ec_able2buy { font-family: Verdana; font-size: 12px; } \n");
        sb.append("table.ec_able2buy td { padding-right: 14px; padding-top: 5px; }\n");
        sb.append("table.ec_able2buy td.right { width: 100px; }\n");
    }

    private void accountPage(StringBuilder sb) {

        sb.append("div.ec_account_welcome { padding-bottom: 15px; font-family: Arial; font-size: 12px; } \n");
        sb.append("table.ec_account { font-family: Arial; font-size: 12px; } \n");
        sb.append("table.ec_account td { padding-top: 3px; padding-right: 18px; } \n");
        sb.append("table.ec_account a { font-weight: bold; } \n");
    }

    /**
     *
     */
    private void addListing(StringBuilder sb) {

        // choose listing pacakge
        sb.append("table.ec_listingpackage { width: 100%; margin: 8px 0; } \n");
        sb.append("table.ec_listingpackage th { background: #e1e1e1; padding: 3px; text-align: left; font-size: 14px; } \n");
        sb.append("table.ec_listingpackage th a { text-decoration: underline; } \n");
        sb.append("table.ec_listingpackage td { padding: 3px; } \n\n");

        // lisings
        sb.append("table.ec_addlisting_categories td { font-family: Arial; font-size: 15px; font-weight: bold; padding: 2px 0; } ");
        sb.append("table.ec_addlisting_categories a { text-decoration: none; } ");
        sb.append("table.ec_addlisting_categories a:hover { text-decoration: underline; } ");
    }

    /**
     *
     */
    private void contentSearch(StringBuilder sb) {
        // content search results
        sb.append("div.ec_csresult_name a { font-family: Arial; font-size: 15px; color: #0000cc; text-decoration: underline; } ");
        sb.append("div.ec_csresult_summary { font-family: Arial; font-size: 12px; color: #000000; } ");
        sb.append("div.ec_csresult_url { padding-bottom: 6px; } ");
        sb.append("div.ec_csresult_url a { font-family: Arial; font-size: 12px; color: #008000; } ");
    }

    /**
     *
     */
    private void forms(StringBuilder sb) {

        sb.append("fieldset.ec_form { font-family: Verdana; font-size: 12px; margin-top: 15px; border: 1px solid #aaaaaa; padding: 0px 12px; } ");
        sb.append("fieldset.ec_form legend { font-family: Verdana; font-size: 12px; font-weight: bold; color: #339933; } ");
        sb.append("fieldset.ec_form legend span { font-size: 12px; color: #888888; } ");

        sb.append("fieldset.ec_form div.text, fieldset.ec_form div.command { margin-bottom: 13px; } ");

        // terms
        sb.append("fieldset.ec_terms { margin-top: 15px; padding: 0px 12px; "
                + "border: 3px solid #339933; background: #e8fcea; font-family: Verdana; font-size: 12px; } ");
        sb.append("fieldset.ec_terms legend { font-family: Verdana; font-size: 12px; font-weight: bold; color: #339933; } ");
        sb.append("fieldset.ec_terms a { font-weight: bold; } ");
        sb.append("fieldset.ec_terms div { margin: 10px 0; } ");

        // commands
        sb.append("div.ec_form_commands input { font-size: 13px; font-family: Arial; padding: 3px 6px; margin: 10px 4px; } ");

        // field value cells
        sb.append("fieldset.ec_form div.field_value { margin-bottom: 13px; padding-right: 10px; } ");
        sb.append("fieldset.ec_form div.field_value div.label { margin-bottom: 2px; font-weight: bold; font-family: Verdana; "
                + "font-size: 12px; color: #333333; } ");

        // field input cells
        sb.append("fieldset.ec_form div.field_input { margin-bottom: 13px; padding-right: 10px; } ");
        sb.append("fieldset.ec_form div.field_input div.label { margin-bottom: 2px; font-weight: bold; font-family: Verdana; "
                + "font-size: 12px; color: #333333; } ");

        sb.append("fieldset.ec_form div.field_input div.desc { margin-top: 3px; font-family: Verdana; color: #555555; font-size: 10px; } ");

        sb.append("fieldset.ec_form input.text, fieldset.ec_form input.password, fieldset.ec_form input.file, "
                + "fieldset.ec_form textarea, fieldset.ec_form select { "
                + "border: 1px solid #7f9db9;  font-size: 13px; font-family: Verdana; color: #333333; padding: 4px;  } ");

        //		sb.append("fieldset.ec_form input { font-size: 16px; font-family: Arial; color: #333333; padding: 1px 5px; } ");
    }

    /**
     *
     */
    private void listingCp(StringBuilder sb) {
        sb.append("table.ec_listing_controlpanel {  margin: 10px;  background: #f6f6f6; width: 95%; } ");

        sb.append("table.ec_listing_controlpanel td { padding: 6px 6px; }");
        sb.append("table.ec_listing_controlpanel td.status, table.ec_listing_controlpanel td.link "
                + "{ font-size: 15px; font-family: Arial; color: #333333; font-weight: bold; letter-spacing; -0.5px; } ");

        sb.append("table.ec_listing_controlpanel td.commands input { font-size: 18px; font-family: Arial; padding: 4px 8px; margin-right: 5px; } ");
    }

    private void pageControl(StringBuilder sb) {

        sb.append("table.ec_page_control { width: 100%; border: 1px solid #aaaaaa; font-family: Verdana; font-size: 12px; } ");
        sb.append("table.ec_page_control td { padding: 4px 8px; } ");
        sb.append("table.ec_page_control td.results span { font-weight: bold; } ");
        sb.append("table.ec_page_control td.pages { text-align: right; } ");
        sb.append("table.ec_page_control td span.disabled { color: #999999; font-weight: bold; } ");
        sb.append("table.ec_page_control a { font-weight: bold; text-decoration: underline; } ");
        sb.append("table.ec_page_control a:hover { text-decoration: none; } ");
    }

    private void polls(StringBuilder sb) {

        sb.append("table.ec_poll_content { margin-bottom: 20px; } ");

        sb.append("table.ec_poll_results { margin-bottom: 20px; } ");
        sb.append("table.ec_poll_results td.intro { padding: 0 3px; } ");
        sb.append("table.ec_poll_results td.title { font-weight: bold; padding: 3px; border-bottom: 1px solid #333333; } ");
        sb.append("table.ec_poll_results tr.option td { border-bottom: 1px solid #999999; padding: 3px 10px; } ");
        sb.append("table.ec_poll_results td.total { background: #eeeeee; font-weight: bold; padding: 3px 10px; } ");

        sb.append("table.ec_poll_links td.intro { padding: 0 3px; } ");
        sb.append("table.ec_poll_links td.title { font-weight: bold; padding: 3px; border-bottom: 1px solid #333333; } ");

    }

    private void bookingCalendar(StringBuilder sb) {
        sb.append("table.ec_bkmonth { font-family: Arial; border: 2px solid #cc3300; } ");
        sb.append("table.ec_bkmonth td.empty { background: #eaeaea; } ");

        sb.append("table.ec_bkmonth td {  font-family: Arial;  background: #83c0b9; padding: 3px; font-size: 15px; font-weight: bold; } ");

        sb.append("table.ec_bkmonth td a { text-decoration: none; } ");
        sb.append("table.ec_bkmonth td a { color: white; } ");
        sb.append("table.ec_bkmonth td a:hover { color: white; } ");

        sb.append("table.ec_bkmonth td.disabled { background: url(files/graphics/booking/notavailable.gif); } ");
        sb.append("table.ec_bkmonth td.reserved { background: #ff69b4; } ");
        sb.append("table.ec_bkmonth td.unavailable { background: #4f4f4f; } ");
        sb.append("table.ec_bkmonth td.booked { background: #ff0000; } ");

        sb.append("table.ec_bkmonth td.disabled a { color: #999999; } ");
        sb.append("table.ec_bkmonth td.disabled a:hover { color: #999999; } ");

        sb.append("table.ec_bkmonth caption { text-align: left; padding: 3px 0 3px 8px; "
                + "background: #cc3300; font-weight: bold; color: white; font-size: 14px;  } ");

        sb.append("table.ec_bkmonth td.heading { font-weight: bold; background: #faeae5; font-size: 9px; color: #cc6600; } ");

        sb.append("table.ec_bkyears td { padding: 2px 10px; font-family: Arial; font-size: 12px; font-weight: bold; } ");

        sb.append("table.ec_bookingcharts td { padding: 8px; font-family: Verdana; font-size: 11px;  }");
        sb.append("table.ec_bookingcharts table.month td { padding: 1px 2px; }");
    }

    /**
     *
     */
    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        forms(sb);

        sb.append("table.newsfeed { font-family: Arial; width: 100%; } ");
        sb.append("table.newsfeed td { padding: 0 5px 10px 5px; } ");
        sb.append("table.newsfeed div.headline { font-family: tahoma; font-size: 15px; font-weight: bold; } ");
        sb.append("table.newsfeed div.date { font-size: 11px; color: #666666; } ");
        sb.append("table.newsfeed div.description { font-size: 13px; } ");
        sb.append("table.newsfeed_copyright { margin-bottom: 10px; font-size: 10px; color: #666666; font-weight: bold;} ");

        // listing control panel
        listingCp(sb);

        sb.append("table.ec_pricebreaks { border-collapse: collapse; font-family: Verdana; color: #333333; } ");
        sb.append("table.ec_pricebreaks th { background: #f5f5f5; border: 1px outset;  padding: 2px 4px; } ");
        sb.append("table.ec_pricebreaks td { background: #fff7ea; border: 1px outset; padding: 2px 4px; } ");

        sb.append("table.ec_search_header { width: 100%; border: 1px solid #999999; margin-bottom: 7px; "
                + "font-family: Trebuchet MS; font-size: 11px; font-color: #666666; } ");
        sb.append("table.ec_search_header td { padding: 3px 10px; } ");
        sb.append("table.ec_search_header a { text-decoration: underline; } ");
        sb.append("table.ec_search_header a:hover { text-decoration: none; } ");

        sb.append("table.form { width: 100%; font-family: Verdana; margin-bottom: 20px; } \n");

        sb.append("table.form caption { font-family: Verdana; font-weight: bold; border-bottom: 2px groove #c3c3c3; padding-bottom: 3px; "
                + "margin: 0; color: black; font-size: 14px; text-align: left; } \n");
        // form_lines
        sb.append("table.form_lines td { padding: 2px 4px; } ");
        sb.append("table.form th { padding: 5px 2px; font-size: 11px; text-align: left; font-weight: normal; background: #696969; } ");
        sb.append("table.form td { padding: 5px 2px; font-size: 11px; text-align: left; background: #D3D3D3 } \n");
        sb.append("table.form th { padding: 5px 2px; font-size: 11px; text-align: left; font-weight: normal; } ");
        sb.append("table.form td { padding: 5px 2px; font-size: 11px; text-align: left; } \n");
        sb.append("table.form td.action { padding: 8px; } \n");
        // sb.append("table.form td.key { font-weight: bold; padding-right: 12px; color: black; text-align: right; width: 180px;} \n");
        sb.append("table.form td.key { font-weight: bold; padding-right: 12px; color: black; text-align: right; } \n");
        sb.append("table.form td.key div { font-weight: normal; padding: 2px 0; font-size: 10px; color: #666666; } \n");
        sb.append("table.form span.error { font-weight: bold; font-size: 11px; color: #cc0000; } \n");
        sb.append("table.form span.small { font-size: 10px; } \n");
        sb.append("table.form th { background: #666666; color: white; font-weight: bold; } \n");
        sb.append("table.form td { background: #ffffff; } \n");

        sb.append("div.message_error { color: #cc0000; font-weight: bold; font-family: Verdana; font-size: 12px; padding: 2px 0; } \n");

        // errors
        sb.append("span.ec_error { font-weight: bold; font-size: 12px; color: #cc0000; } \n");

        // tag clouds
        sb.append("a.ec_tagcloud_link, a.ec_tagcloud_link:visited { color: blue; font-family: Arial; text-decoration: none; font-weight: bold; } ");
        sb.append("a.ec_tagcloud_link:hover { color: orange; text-decoration: underline; } ");
        sb.append("span.ec_tagcloud_large {  font-size: 18px;  } ");
        sb.append("span.ec_tagcloud_medium { font-size: 14px; } ");
        sb.append("span.ec_tagcloud_small { font-size: 11px; } ");

        // item g balloons
        sb.append("div.ec_gmap { margin-top: 10px; } ");
        sb.append("div.ec_gmap_caption { margin-top: 15px; font-family: Arial; font-size: 14px; font-weight: bold; color: #333333; }");
        sb.append("table.ec_gmap_balloon { width: 300px; } ");
        sb.append("table.ec_gmap_balloon div.name { font-family: Arial; font-size: 14px; font-weight: bold; color: #333333; }");
        sb.append("table.ec_gmap_balloon div.content { font-family: Arial; font-size: 11px; bold; color: #333333; }");
        sb.append("table.ec_gmap_balloon img { padding: 3px; border: 1px solid #333333; } ");

        sb.append("table.registration td { padding-right: 20px; } \n");
        sb.append("table.registration td.begin { font-weight: bold; } \n");
        sb.append("div.text.welcome { margin-bottom: 30px; } \n");

        sb.append("table.ec_account h2 { padding-bottom: 10px; border-bottom: 1px solid #333333; } \n");
        sb.append("table.ec_account h2 img { float: left; padding-right: 5px; margin-bottom: -5px; } \n");
        sb.append("table.ec_account td.header { padding-top: 20px; } \n");

        sb.append("table.statsperiod td { padding-right: 30px; font-weight: bold; padding-bottom: 10px; font-size: 14px; } \n");


        able2buy(sb);

        accountPage(sb);

        contentSearch(sb);

        wizard(sb);

        addListing(sb);

        pageControl(sb);

        styles(sb);
        bookingCalendar(sb);
        sb.append(new ToolbarCss());

        return sb.toString();
    }

    private void styles(StringBuilder sb) {
        sb.append("div.ecStylesZigZag img { margin: 6px; 15px; padding: 1px; border: 1px solid #999999; } ");
    }

    private void wizard(StringBuilder sb) {
        // wizard defaults
        sb.append("table.ec_wizard td { padding: 2px 4px; } ");
    }

    public void write(RequestContext context) throws IOException {

        File file = context.getRealFile("files/css");
        file.mkdirs();

        file = context.getRealFile("files/css/ecreator-front.css");
        SimpleFile.writeString(toString(), file);
    }
}
