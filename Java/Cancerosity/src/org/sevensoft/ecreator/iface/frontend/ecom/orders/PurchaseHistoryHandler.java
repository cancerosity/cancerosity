package org.sevensoft.ecreator.iface.frontend.ecom.orders;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.BasketHandler;
import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 8 Oct 2006 20:11:49
 *
 */
@Path("purchase-history.do")
public class PurchaseHistoryHandler extends FrontendHandler {

	public PurchaseHistoryHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		FrontendDoc doc = new FrontendDoc(context, "Previous purchases");
		doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		doc.addTrail(PurchaseHistoryHandler.class, null, "Previous purchases");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("form1", "center").setCaption("Previous purchases"));

				sb.append("<tr>");
				sb.append("<th>Date</th>");
				sb.append("<th>Item</th>");

				sb.append("<th>Current price</th>");
				sb.append("<th>Last price paid</th>");
				sb.append("<th>Most recent order</th>");
				sb.append("<th>Add to basket</th>");
				sb.append("</tr>");

				final List<OrderLine> lines = account.getPurchaseHistory(50);
				if (lines.isEmpty()) {

					sb.append("<tr><td colspan='4'>You have not purchased any items yet.</td></tr>");

				} else {

					for (OrderLine line : lines) {

						sb.append("<tr>");
						sb.append("<td>" + line.getOrder().getDatePlaced().toString("dd-MMM-yyyy") + " </td>");
						sb.append("<td>" + new LinkTag(ItemHandler.class, null, line.getDescription(), "item", line.getItem()) + " </td>");
						sb.append("<td>" + line.getItem().getSellPrice(account) + "</td>");
						sb.append("<td>" + line.getUnitSellEx() + "</td>");
						sb.append("<td>" + line.getOrder().getId() + "</td>");
						sb.append("<td>" + new LinkTag(BasketHandler.class, "add", "Add to basket", "item", line.getItem()) + "</td>");
						sb.append("</tr>");

					}
				}

				sb.append("<tr><th colspan='6'>" + "To return to your account " + new LinkTag(AccountHandler.class, null, "click here") + "</th></tr>");

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}
}
