package org.sevensoft.ecreator.iface.frontend.ecom.shopping;

import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FormRow;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.iface.frontend.misc.FieldInputCell;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.panels.DeliveryConfirmationPanel;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderStatusException;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.PaymentException;
import org.sevensoft.ecreator.model.ecom.payments.PaymentRedirect;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.addresses.panels.AddressesPanel;
import org.sevensoft.ecreator.model.ecom.addresses.panels.AddressEntryPanel;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.addresses.AddressFields;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.PostCodeException;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.ContactNameException;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.TownException;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.CountryException;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.AddressLineException;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterControl;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.SimpleRadioTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.commons.validators.net.EmailValidator;
import org.sevensoft.commons.gaia.Country;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Dmitry Lebedev
 * @version: $id$
 * Date: 04.03.2009
 * Time 15:46:02
 */
@Path("chkdirect.do")
public class DirectGoogleCheckoutHandler extends FrontendHandler {

    final transient OrderSettings orderSettings;
    private final transient PaymentSettings paymentSettings;
    transient final NewsletterControl newsletterControl;
    private Country country;
    private String address1, address2, address3;
    private String name;
    private String postcode;
    private String town;
    private String telephone1;
    private String telephone2;
    private String telephone3;
    private String county;
    private String instructions;
    private String accountEmail;
    private String accountName;
    private String referrer;
    private int addressId;

    public DirectGoogleCheckoutHandler(RequestContext context) {
        super(context);

        this.paymentSettings = PaymentSettings.getInstance(context);
        this.orderSettings = OrderSettings.getInstance(context);
        this.newsletterControl = NewsletterControl.getInstance(context);
    }

    /**
     * Ask details includes
     * <p/>
     * name / email for non account shopping
     * delivery address
     * billing address
     * attribute options
     */
    private Object askDeliveryAddress() {

        final String title = "Checkout - Delivery address";
        FrontendDoc doc = new FrontendDoc(context, title);
        doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
        doc.addTrail(CheckoutHandler.class, null, title, "editDeliveryAddress", true);
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new MessagesTag(context));
                sb.append(new FormTag(DirectGoogleCheckoutHandler.class, "setDeliveryAddress", "POST"));

                // ACCOUNT DETAILS
                if (!basket.hasAccount()) {

                    sb.append(new TableTag("form").setCaption("Your details"));

                    sb.append(new FormRow("Your name", shoppingSettings.getNameInstructions(), new TextTag(context, "accountName", basket.getName(),
                            20) +
                            "" + new ErrorTag(context, "accountName", "<br/>")));

                    sb.append(new FormRow("Your email address", shoppingSettings.getEmailInstructions(), new TextTag(context, "accountEmail", basket
                            .getEmail(), 40) +
                            "" + new ErrorTag(context, "accountEmail", "<br/>")));

                    sb.append("</table>");

                    if (shoppingSettings.hasReferrers()) {

                        sb.append(new FormFieldSet("Referred from"));

                        SelectTag tag = new SelectTag(context, "referrer", basket.getReferrer(), shoppingSettings.getReferrers(), "-Please choose-");

                        sb.append(new FieldInputCell("How did you hear about our site?",
                                "We would be very grateful if you would let us know how you found out about our site.", tag));

                        sb.append("</fieldset>");
                    }
                }

                // only show the addresses panel if address book is active, and we have addresses
                if (shoppingSettings.isAddressBook() && basket.hasAccount() && account.hasAddresses()) {

                    sb.append(new FormFieldSet("Delivery address"));
                    sb.append("<div class='text'>" + shoppingSettings.getDeliveryAddressHeader() + "</div>");

                    AddressesPanel addressesPanel = new AddressesPanel(context, account, basket.getDeliveryAddress());
                    sb.append(addressesPanel);

                    sb.append("</fieldset>");

                }

                if (Address.isCreate(account)) {

                    AddressFields fields = AddressFields.getInstance(context, AddressFields.Type.Delivery);

                    AddressEntryPanel panel = new AddressEntryPanel(context, fields);
                    panel.setAddress(basket.getDeliveryAddress());
                    panel.setCountries(shoppingSettings.isShowCountries());

                    FormFieldSet set = new FormFieldSet("Enter delivery address");
                    set.setId(AddressEntryPanel.ID);
                    if (shoppingSettings.isAddressBook() && basket.hasAccount() && account.hasAddresses()) {
                        set.setStyle("display: none;");
                    }
                    sb.append(set);

                    sb.append(panel);

                    sb.append("</fieldset>");
                }

                sb.append("<div style='text-align: center; padding-top: 10px;'>" + new SubmitTag("Continue") + "</div>");

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    private Object deliveryConfirmation() {

        String title = "Checkout confirmation";

        FrontendDoc doc = new FrontendDoc(context, title);
        doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
        doc.addTrail(CheckoutHandler.class, null, title);
        doc.addBody(new MessagesTag(context));
        doc.addBody(new DeliveryConfirmationPanel(context, basket));

        return doc;
    }

    @Override
    public Object main() throws ServletException {
        return processGoogleDirectly();
    }

    public Object processGoogleDirectly() throws ServletException {

        /*
		 * Check shopping module is enabled
		 */
        if (!Module.Shopping.enabled(context)) {
            logger.fine("[CheckoutHandler=" + context.getSessionId() + "] shopping disabled");
            return index();
        }

        // check for null basket, should never be null if shopping is enabled
        if (basket == null) {
            logger.fine("[CheckoutHandler=" + context.getSessionId() + "] basket is null, exiting");
            return new CategoryHandler(context).main();
        }

        // check for empty basket
        if (basket.isEmpty() || basket.isZero()) {

            logger.fine("[CheckoutHandler=" + context.getSessionId() + "] basket is empty, returning to basket screen");
            addError("Cannot make an order with an empty basket");
            return new BasketHandler(context).main();
        }

        // check for minimum shopping total
        if (basket.getTotalInc().isLessThan(shoppingSettings.getMinOrderValue())) {

            logger.fine("[CheckoutHandler=" + context.getSessionId() + "] order total is below min spend, minspend=");
            addError("Our site has a minimum order of " + shoppingSettings.getMinOrderValue());
            return new BasketHandler(context).main();
        }
        //doesn't need for direct checkout, because all this information there is in the google checout accounts
/*
        if (!basket.hasAccount()) {

            if (!basket.hasName()) {
                logger.fine("[CheckoutHandler=" + context.getSessionId() + "] no name");
                return askDeliveryAddress();
            }

            if (!basket.hasEmail()) {
                logger.fine("[CheckoutHandler=" + context.getSessionId() + "] no email address");
                return askDeliveryAddress();
            }
        }

        if (shoppingSettings.isAddresses()) {

            if (!basket.hasDeliveryAddress()) {
                logger.fine("[CheckoutHandler=" + context.getSessionId() + "] no delivery address");
                return askDeliveryAddress();
            }


            if (shoppingSettings.isSeparateAddresses()) {

                logger.fine("[CheckoutHandler=" + context.getSessionId() + "] using separate addresses");

                if (!basket.hasBillingAddress()) {
                    logger.fine("[CheckoutHandler=" + context.getSessionId() + "] no billing address");
                    return askBillingAddress();
                }
            }
        }
  */
        Order order = null;
        try {

            basket.setPaymentType(PaymentType.GoogleCheckout);
            order = basket.toOrder();
//            order.setPaymentType(PaymentType.GoogleCheckout);

        } catch (OrderStatusException e) {
            e.printStackTrace();
            return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

        } catch (IOException e) {
            e.printStackTrace();
            return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

        } catch (PaymentException e) {
            e.printStackTrace();
            addError(e);

//            return deliveryConfirmation();
        }
        return new PaymentRedirect(context, order);
    }

    @Override
    protected boolean runSecure() {
        return Config.getInstance(context).isSecured();
    }

    private Object askBillingAddress() {

        final String title = "Checkout - Billing address";
        FrontendDoc doc = new FrontendDoc(context, title);
        doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
        doc.addTrail(CheckoutHandler.class, null, title, "editBillingAddress", true);
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new MessagesTag(context));

                sb.append(new FormTag(DirectGoogleCheckoutHandler.class, "setBillingAddress", "POST"));

                sb.append(new FormFieldSet("Billing address"));
                sb.append("<div class='text'>" + shoppingSettings.getBillingAddressHeader() + "</div>");

//				RadioTag sameAddressRadio = new RadioTag(context, "addressId", "-1", !basket.hasBillingAddress());
                SimpleRadioTag sameAddressRadio = new SimpleRadioTag(context, "addressId", "-1", true);
                sameAddressRadio.setId("sameAddressRadio");
                sameAddressRadio.setOnClick("document.getElementById('" + AddressEntryPanel.ID + "').style.display = 'none';");

                sb.append("<div class='text'>" + sameAddressRadio + " My billing address and delivery address are the same</div>");

//				AddressesPanel addressBookPanel = new AddressesPanel(context, account, basket.getBillingAddress());
                AddressesPanel addressBookPanel = new AddressesPanel(context, account, new Address(context));
                addressBookPanel.setShowCreate(true);
                sb.append(addressBookPanel);

                sb.append("</fieldset>");

                if (Address.isCreate(account)) {

                    AddressFields fields = AddressFields.getInstance(context, AddressFields.Type.Billing);

                    AddressEntryPanel panel = new AddressEntryPanel(context, fields);
                    panel.setCountries(shoppingSettings.isShowCountries());
                    panel.setAddress(basket.getBillingAddress());

                    FormFieldSet set = new FormFieldSet("Create new address").setId(AddressEntryPanel.ID);
                    set.setStyle("display: none;");

                    sb.append(set);
                    sb.append(panel);
                    sb.append("</fieldset>");

                }

                sb.append("<div style='text-align: center; padding-top: 10px;'>" + new SubmitTag("Continue") + "</div>");

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object setBillingAddress() throws ServletException {

        if (!shoppingSettings.isSeparateAddresses()) {
            logger.fine("[CheckoutHandler=" + context.getSessionId() + "] not using separate addresses, returning to process");
            return processGoogleDirectly();
        }

        AddressFields fields = AddressFields.getInstance(context, AddressFields.Type.Billing);

        // if not same adress for billing then we must check for fields
        if (addressId == 0) {
            Address.addTests(context, country, fields);
        }

        if (hasErrors()) {
            logger.fine("[CheckoutHandler=" + context.getSessionId() + "] immediate errors detected: " + context.getErrors());
            return askBillingAddress();
        }

        if (addressId == -1) {

            Address address = basket.getDeliveryAddress();
            logger.fine("[CheckoutHandler=" + context.getSessionId() + "] setting billing add to same as del=" + address);
            basket.setBillingAddress(address);

            return processGoogleDirectly();

            // create addresss
        }
        else if (addressId == 0) {

            try {

                Address address = new Address(context, basket.getAccount(), name, address1, address2, address3, town, county, postcode, country);
                address.setTelephone1(telephone1);
                address.setTelephone2(telephone2);
                address.setTelephone3(telephone3);
                address.save();

                logger.fine("[CheckoutHandler=" + context.getSessionId() + "] billing address created=" + address);

                basket.setBillingAddress(address);

            } catch (PostCodeException e) {
                setError("billingPostcode", e);
                logger.fine("[CheckoutHandler=" + context.getSessionId() + "] billing address error: " + e);
                return askBillingAddress();

            } catch (ContactNameException e) {
                setError("billingName", e);
                logger.fine("[CheckoutHandler=" + context.getSessionId() + "] billing address error: " + e);
                return askBillingAddress();

            } catch (TownException e) {
                setError("billingTown", e);
                logger.fine("[CheckoutHandler=" + context.getSessionId() + "] billing address error: " + e);
                return askBillingAddress();

            } catch (CountryException e) {
                setError("billingCountry", e);
                logger.fine("[CheckoutHandler=" + context.getSessionId() + "] billing address error: " + e);
                return askBillingAddress();

            } catch (AddressLineException e) {
                setError("billingAddress1", e);
                logger.fine("[CheckoutHandler=" + context.getSessionId() + "] billing address error: " + e);
                return askBillingAddress();
            }

        }
        else {

            if (shoppingSettings.isAddressBook()) {

                Address address = EntityObject.getInstance(context, Address.class, addressId);
                logger.fine("[CheckoutHandler=" + context.getSessionId() + "] asking to use billingAddress=" + address);

                if (address != null && address.isValidFor(account)) {

                    logger.fine("[CheckoutHandler=" + context.getSessionId() + "] confirmed we are the owner, setting");
                    basket.setBillingAddress(address);

                }
            }
        }

        return processGoogleDirectly();
    }

    public Object setDeliveryAddress() throws ServletException {

        logger.fine("[CheckoutHandler=" + context.getSessionId() + "] set delivery address");

        // we need name and email if account is null
        if (!basket.hasAccount()) {

            logger.fine("[CheckoutHandler=" + context.getSessionId() +
                    "] basket does not have account, so testing for accountName and accountEmail params");

            test(new LengthValidator(6), "accountName");
            test(new LengthValidator(6), "accountEmail");
            test(new EmailValidator(), "accountEmail");
        }

        if (!basket.hasAccount()) {

            logger.fine("[CheckoutHandler=" + context.getSessionId() + "] basket does not have account, setting accountName=" + accountName +
                    ", accountEmail=" + accountEmail);

            // set name and email on the basket
            basket.setName(accountName);
            basket.setEmail(accountEmail);
            basket.setReferrer(referrer);
            basket.save();

        }

        AddressFields fields = AddressFields.getInstance(context, AddressFields.Type.Delivery);

        // add tests for delivery if required
        if (addressId == 0) {

            logger.fine("[CheckoutHandler=" + context.getSessionId() + "] no address param, so testing for creation fields");
            Address.addTests(context, country, fields);
        }

        if (hasErrors()) {
            logger.fine("[CheckoutHandler=" + context.getSessionId() + "] immediate errors detected: " + context.getErrors());
            return askDeliveryAddress();
        }

        if (addressId == 0) {
            // create delivery address

            logger.fine("[CheckoutHandler=" + context.getSessionId() + "] attempting to create address");

            try {

                Address newadd = new Address(context, basket.getAccount(), name, address1, address2, address3, town, county, postcode, country);
                newadd.setTelephone1(telephone1);
                newadd.setTelephone2(telephone2);
                newadd.setTelephone3(telephone3);
                newadd.setInstructions(instructions);
                newadd.save();

                logger.fine("[CheckoutHandler=" + context.getSessionId() + "] delivery address created=" + newadd);

                // set delivery address on basket
                basket.setDeliveryAddress(newadd);

                if (shoppingSettings.isSingleAddress()) {

                    logger.fine("[CheckoutHandler=" + context.getSessionId() + "] using delivery address for billing");
                    basket.setBillingAddress(newadd);
                }

            } catch (PostCodeException e) {
                setError("postcode", e);
                logger.fine("[CheckoutHandler=" + context.getSessionId() + "] delivery address error: " + e);
                return askDeliveryAddress();

            } catch (ContactNameException e) {
                setError("name", e);
                logger.fine("[CheckoutHandler=" + context.getSessionId() + "] delivery address error: " + e);
                return askDeliveryAddress();

            } catch (TownException e) {
                setError("town", e);
                logger.fine("[CheckoutHandler=" + context.getSessionId() + "] delivery address error: " + e);
                return askDeliveryAddress();

            } catch (CountryException e) {
                setError("country", e);
                logger.fine("[CheckoutHandler=" + context.getSessionId() + "] delivery address error: " + e);
                return askDeliveryAddress();

            } catch (AddressLineException e) {
                setError("address1", e);
                logger.fine("[CheckoutHandler=" + context.getSessionId() + "] delivery address error: " + e);
                return askDeliveryAddress();
            }

        }
        else {
            // if we have selected an address then use that

            if (shoppingSettings.isAddressBook()) {

                logger.fine("[CheckoutHandler=" + context.getSessionId() + "] address book confirmed enabled");

                Address address = EntityObject.getInstance(context, Address.class, addressId);
                logger.fine("[CheckoutHandler=" + context.getSessionId() + "] asking to use address=" + addressId);

                if (address.isValidFor(account)) {

                    logger.fine("[CheckoutHandler=" + context.getSessionId() + "] confirmed address is valid");
                    basket.setDeliveryAddress(address);

                }
            }
        }

        return processGoogleDirectly();
    }
}
