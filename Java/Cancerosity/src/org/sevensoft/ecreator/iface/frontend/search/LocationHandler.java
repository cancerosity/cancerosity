package org.sevensoft.ecreator.iface.frontend.search;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.misc.location.Pin;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.ExternalRedirect;

/**
 * @author sks 1 Apr 2007 12:02:37
 *
 */
@Path("location.do")
public class LocationHandler extends FrontendHandler {

	private String	location;
	private Pin		pin;
	private String	forward;
	private boolean	force;

	public LocationHandler(RequestContext context) {
		super(context);
	}

	private Object empty() {

		FrontendDoc doc = new FrontendDoc(context, "Location not found");
		doc.addBody(new Body() {

			@Override
			public String toString() {
				return "No matches I'm afraid.";
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		if (location == null) {
			return empty();
		}

		final List<Pin> pins = Pin.getMatching(context, location);

		// if no matches return empty
		if (pins.isEmpty()) {
			return empty();
		}

		// if only a single match just set that and forward back to category
		// if we have multiple matches but force is set then choose first one
		if (pins.size() == 1 || force) {
			pin = pins.get(0);
			return set();
		}

		FrontendDoc doc = new FrontendDoc(context, "Choose location");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				StringBuilder sb = new StringBuilder();

				for (Pin pin : pins) {
					sb.append("<div>" + new LinkTag(LocationHandler.class, "set", pin.getName(), "pin", pin, "forward", forward) + "</div>");
				}

				return sb.toString();
			}

		});
		return doc;
	}

	public Object set() throws ServletException {

		if (pin == null) {
			return index();
		}

		activeLocation = pin.getLocation();
		context.setAttribute("activeLocation", activeLocation);
		context.setCookie("activeLocation", activeLocation);

		if (forward != null) {
			return new ExternalRedirect(forward);
		}

		return index();
	}
}
