package org.sevensoft.ecreator.iface.frontend.account.profile.panels;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.text.PasswordTag;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.input.AFormSectionsInputPanel;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.iface.frontend.misc.FieldInputCell;

import java.util.List;

/**
 * Class for panel, updating profile data, includes   'Section' data displayed
 */
public class ProfileEditPanel {

    private final RequestContext context;
    private final Item account;
    private boolean notChangeEmail;

    public ProfileEditPanel(RequestContext context, Item account, boolean notChangeEmail) {
        this.context = context;
        this.account = account;
        this.notChangeEmail = notChangeEmail;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        sb.append(new MessagesTag(context));

        sb.append(new FormFieldSet("Your profile"));

        sb.append(new FieldInputCell(account.getItemType().getNameCaption(), "", new TextTag(context, "name", account.getName(), 30).setDisabled(true)));

        if (!account.isBuyer()) {

            if (!notChangeEmail) {
                sb.append(new FieldInputCell("Email", "", new TextTag(context, "email", account.getEmail(), 30), new ErrorTag(context,
                        "email", " ")));
            }

        }

        sb.append(new FieldInputCell("Enter new password", "Enter a password for this account.<br/>It should be between 5 and 15 characters in length.<br/>"
                + "Passwords are case sensitive.", new PasswordTag(context, "password1", 16), new ErrorTag(context, "password1", "")));
        sb.append(new FieldInputCell("Repeat new password", "Please repeat your password to ensure it was entered correctly.",
                new PasswordTag(context, "password2", 16), new ErrorTag(context, "password2", "")));

        if (account.isMobilePnoneEnabled()) {
            sb.append(new FieldInputCell("Mobile Phone", "", new TextTag(context, "mobilePhone", account.getMobilePhone(), 20)));
        }
        sb.append("</fieldset>");

        List<Attribute> editableAttr = account.getEditableAttributes();
        if (editableAttr.size() > 0) {
            AFormSectionsInputPanel panel = new AFormSectionsInputPanel(context, account, editableAttr);
            sb.append(panel);
        }

        SubmitTag submitTag = new SubmitTag("Update your profile");

        sb.append("<div class='ec_form_commands'>");
        sb.append(submitTag);


        sb.append("</div>");

        sb.append(new FieldInputCell("To return to your account without making changes to your profile", "", new LinkTag(AccountHandler.class, null, "click here")));

        return sb.toString();
    }
}
