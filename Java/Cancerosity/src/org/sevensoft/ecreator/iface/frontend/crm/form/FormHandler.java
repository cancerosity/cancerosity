package org.sevensoft.ecreator.iface.frontend.crm.form;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.FormField;
import org.sevensoft.ecreator.model.crm.forms.FormLine;
import org.sevensoft.ecreator.model.crm.forms.submission.Submission;
import org.sevensoft.ecreator.model.crm.forms.submission.restriction.SubmissionRestrictionModule;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.misc.Page;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.i18n.Messages;

/**
 * @author sks 10-Jan-2006 14:59:36
 *
 */
@Path("form.do")
public class FormHandler extends FrontendHandler {

	private Form					form;
	private LinkedHashMap<FormField, String>	data;
    private LinkedHashMap<FormField, String>	data_1;
	private Category					category;
	private List<Upload>				uploads;
	private Item					item;
	private FormLine					line;
	private Map<Integer, Integer>			qtys;
	private Map<ItemOption, String>		options;
	private Map<ItemOption, Upload>		optionUploads;
	private Item					referenceItem;
	private String					captchaResponse;

	public FormHandler(RequestContext context) {
		super(context);
	}

	public FormHandler(RequestContext context, Form form, Item item, Map<ItemOption, String> options, Map<ItemOption, Upload> optionUploads) {
		super(context);

		this.form = form;
		this.item = item;
		this.options = options;
		this.optionUploads = optionUploads;
	}

    @Deprecated
	public Object addLine() throws ServletException {

		if (item == null) {
			return main();
		}

		if (form == null) {
			return index();
		}

		form.addItem(item, options, optionUploads);
		return main();
	}

	/**
	 * The main method will show the form along with any form lines
	 */
	@Override
	public Object main() throws ServletException {

		if (form == null) {
			return index();
		}

		logger.fine("[FormHandler] showing form=" + form);

		FrontendDoc doc = new FrontendDoc(context, form.getName());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(FormHandler.class, "submit", "multi"));
				sb.append(new HiddenTag("form", form));
				sb.append(new HiddenTag("referenceItem", referenceItem));
				sb.append(new HiddenTag("item", item));
				sb.append(new HiddenTag("category", category));

				sb.append(form.render());

				sb.append("</form>");

				return sb.toString();
			}

		});

		return doc;
	}

	public Object removeLine() throws ServletException {

		if (line == null) {
			return index();
		}

		form = line.getForm();
		form.removeLine(line);

		return main();
	}

	public Object submit() throws ServletException {

		logger.fine("[FormHandler] processing form: " + form);

		if (form == null) {
			return new CategoryHandler(context).main();
		}

		form.checkErrors(data, captchaResponse, data_1);

		if (hasErrors()) {

			if (category != null) {

				logger.fine("[FormHandler] submission has errors, returning to category=" + category);

				addMessage(Messages.get("frontend.crm.form.errors.category"));
				return new CategoryHandler(context, category).main();

			} else if (item != null) {

				logger.fine("[FormHandler] submission has errors, returning to item=" + item);

				addMessage(Messages.get("frontend.crm.form.errors.item"));
				return new ItemHandler(context, item).main();

			} else if (form != null) {

				logger.fine("[FormHandler] submission has errors, return to main form display page");
				return main();

			} else {

				logger.fine("[FormHandler] submission has errors, probably from sidebox, showing simple error page");

				FrontendDoc doc = new FrontendDoc(context, form.getName());
				doc.addTrail(FormHandler.class, null, form.getName());
				doc.addBody(new Body() {

					@Override
					public String toString() {
						sb.append(Messages.get("frontend.crm.form.errors.doc.body"));
						return sb.toString();
					}

				});
				return doc;

			}
		}

		logger.fine("[FormHandler] submission is accepted");

		Page page = item;
		if (page == null) {
			page = referenceItem;
		}
		if (page == null) {
			page = category;
		}

		logger.fine("[FormHandler] submitting on page");
		Submission sub = form.submit(null, data, uploads, getRemoteIp(), page);

        //reach limit of allowed form submissions per day
        if (sub == null) {
            context.setAttribute("item", item);
            context.setAttribute("category", category);

            final SubmissionRestrictionModule restrictionModule = SubmissionRestrictionModule.getInstance(context);
            FrontendDoc doc = new FrontendDoc(context, "Form Not Completed");
            doc.addTrail(FormHandler.class, null, "Form Not Completed");
            doc.addBody(new Body() {

                @Override
                public String toString() {
                    sb.append(restrictionModule.getCustomMessage());
                    return sb.toString();
                }

            });

            return doc;
        }

        final String script = new FormSubmissionScriptParser().parse(form, sub, data);

        if (form.hasSubmissionForward()) {
			return new ExternalRedirect(form.getSubmissionForward());
		}

        logger.fine("[FormHandler] returning to page =" + page);
        context.setAttribute("item", item);
        context.setAttribute("category", category);

        FrontendDoc doc = new FrontendDoc(context, Messages.get("frontend.crm.form.completed"));
        doc.addTrail(FormHandler.class, null, Messages.get("frontend.crm.form.completed.trail"));
        doc.addBody(new Body() {

            @Override
            public String toString() {
                if (script != null) {
                    sb.append(script);
                }
                sb.append(form.getSubmissionMessageBr());
                return sb.toString();
            }

        });

        return doc;
    }

	public Object updateLines() throws ServletException {

		if (form == null)
			return index();

		for (FormLine line : form.getLines(context.getSessionId())) {
			line.setQty(qtys.get(line));
			line.save();
		}

		clearParameters();
		return main();
	}
}
