package org.sevensoft.ecreator.iface.frontend.interaction.pm.panels;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.interaction.pm.PmHandler;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.crm.forum.Topic;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 5 Apr 2007 17:21:36
 *
 */
public class PmComposePanel extends Panel {

	private final Item	sender;
	private final Item	recipient;
	private final Config	config;
	private final Topic	topic;

	public PmComposePanel(RequestContext context, Item sender, Item recipient, Topic topic) {
		super(context);
		this.sender = sender;
		this.recipient = recipient;
		this.topic = topic;
		this.config = Config.getInstance(context);
	}

	@Override
	public void makeString() {

		sb.append(new TableTag("form").setId("ec_pm_compose").setCaption("Send message"));
		sb.append(new FormTag(PmHandler.class, "send", "multi"));
		sb.append(new HiddenTag("recipient", recipient));
		sb.append(new HiddenTag("topic", topic));

		if (topic != null) {
			sb.append("<tr><th>Send a message to all posters in the topic</th></tr>");
		}

		if (recipient != null) {
			sb.append("<tr><th>Send a message to " + recipient.getName() + "</th></tr>");
		}

		// CONTENT
		sb.append("<tr><td>" + new TextAreaTag(context, "body", 40, 4) + new ErrorTag(context, "body", "<br/>") + "</td></tr>");

		if (PermissionType.PmAttachments.check(context, sender)) {
			sb.append("<tr><td>Attach a file: " + new FileTag("upload") + "</td></tr>");
		}

		sb.append("<tr><td>" + new SubmitTag("Send") + "</td></tr>");

		sb.append("</form>");
		sb.append("</table>");

	}

}
