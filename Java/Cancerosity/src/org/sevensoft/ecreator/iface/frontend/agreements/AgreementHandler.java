package org.sevensoft.ecreator.iface.frontend.agreements;

import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.misc.agreements.Agreement;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.StringResult;

/**
 * @author sks 4 Jul 2006 17:04:16
 *
 */
@Path("agreement.do")
public class AgreementHandler extends FrontendHandler {

	@SuppressWarnings("hiding")
	private static Logger	logger	= Logger.getLogger(AgreementHandler.class.getName());

	private Agreement		agreement;

	public AgreementHandler(RequestContext context) {
		super(context);
	}

	/**
	 * This method will just display the agreement
	 */
	@Override
	public Object main() throws ServletException {

		if (agreement == null) {
			return HttpServletResponse.SC_BAD_REQUEST;
		}

		if (!agreement.hasContent()) {
			return HttpServletResponse.SC_NO_CONTENT;
		}

		return new StringResult(agreement.getContent(), "text/html");

	}
}
