package org.sevensoft.ecreator.iface.frontend.ecom.shopping.panels;

import java.util.List;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Transformer;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.BasketHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.CheckoutHandler;
import org.sevensoft.ecreator.iface.frontend.misc.FieldValueCell;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.extras.bnr.BlackRoundPricing;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 25 Mar 2007 19:12:39
 *
 */
public class CollectionConfirmationPanel extends Panel {

	private final Basket	basket;
	private final Config	config;
	private Company		company;

	public CollectionConfirmationPanel(RequestContext context, Basket basket) {
		super(context);
		this.basket = basket;
		this.config = Config.getInstance(context);
		this.company = Company.getInstance(context);
	}

	private void lines() {

		sb.append(new FormFieldSet("Order details"));

		sb.append(new TableTag());

		for (BasketLine line : basket.getLines()) {

			sb.append("<tr>");
			sb.append("<td>" + line.getFullDescription());
			if (line.hasAttachments()) {

				List<String> filenames = CollectionsUtil.transform(line.getAttachments(), new Transformer<Attachment, String>() {

					public String transform(Attachment obj) {
						return obj.getName();
					}
				});

				sb.append("<div class='attachments'>Attachments: " + StringHelper.implode(filenames, ", ", true) + "</div>");
			}

			sb.append("</td>");
			sb.append("<td valign='top'>" + line.getQty() + " @ \243" + line.getSalePriceInc() + "</td>");
			sb.append("</tr>");
		}

		sb.append("<tr>");
		sb.append("<td></td>");
		sb.append("<td>" + new LinkTag(BasketHandler.class, null, "Return to basket") + "</td>");
		sb.append("</tr>");

		if (config.isBlackRound()) {

			BlackRoundPricing brp = new BlackRoundPricing(basket);
			Money fittingCharge = brp.getFittingCharge();

			if (fittingCharge.isPositive()) {

				sb.append("<tr>");
				sb.append("<td>Fitting charge</td>");
				sb.append("<td>\243" + fittingCharge + "</td>");
				sb.append("</tr>");
			}
		}

		// TOTALS
		sb.append("<tr><th colspan='2'><b>Total to pay</b></th><tr>");

		if (company.isVatRegistered()) {

			sb.append("<tr>");
			sb.append("<td>Subtotal</td>");
			sb.append("<td>\243" + basket.getTotalEx() + "</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td>VAT</td>");
			sb.append("<td>\243" + basket.getTotalVat() + "</td>");
			sb.append("</tr>");
		}

		sb.append("<tr>");
		sb.append("<td>Total</td>");
		sb.append("<td>\243" + basket.getTotalInc() + "</td>");
		sb.append("</tr>");

		sb.append("</table>");
		sb.append("</fieldset>");
	}

	@Override
	public void makeString() {

		String email, name, telephone;
		if (basket.hasAccount()) {
			name = basket.getAccount().getName();
			email = basket.getAccount().getEmail();
            telephone = basket.getAccount().getMobilePhone();
		} else {
			name = basket.getName();
			email = basket.getEmail();
            telephone = basket.getTelephone1();
		}

		sb.append(new FormFieldSet("Confirm details"));
		sb.append(new FieldValueCell("Your name", name));
		sb.append(new FieldValueCell("Your email", email));
        sb.append(new FieldValueCell("Your telephone", telephone));

		if (basket.hasBranch()) {
			sb.append(new FieldValueCell("Collection location", basket.getBranch().getName()));
		}

		sb.append("<div class='text'>To change these details " + new LinkTag(CheckoutHandler.class, null, "click here", "editCollectionDetails", true) +
				"</div>");
		sb.append("</fieldset>");

		lines();

		sb.append(new FormTag(CheckoutHandler.class, null, "post"));
		sb.append(new HiddenTag("reserve", true));
		sb.append("<div class='ec_form_commands'>" + new SubmitTag("Reserve for collection") + "</div>");
		sb.append("</form>");

	}
}
