package org.sevensoft.ecreator.iface.frontend.account.registration;

import java.io.IOException;
import java.lang.reflect.Member;
import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.commons.superstrings.RandomHelper;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.net.EmailValidator;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.ecreator.iface.frontend.ErrorDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.registration.panels.ProfileImagesPanel;
import org.sevensoft.ecreator.iface.frontend.account.registration.panels.RegistrationConfirmationPanel;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.frontend.misc.FieldInputCell;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.model.accounts.registration.RegistrationModule;
import org.sevensoft.ecreator.model.accounts.registration.RegistrationSession;
import org.sevensoft.ecreator.model.accounts.registration.RegistrationSettings;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.ecreator.model.attributes.renderers.input.AFormInputPanel;
import org.sevensoft.ecreator.model.attributes.renderers.input.AFormSectionsInputPanel;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterControl;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterOptInPanel;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.misc.agreements.Agreement;
import org.sevensoft.ecreator.model.bespoke.ipoints.IPointsSettings;
import org.sevensoft.ecreator.model.bespoke.ipoints.IPointsService;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.PasswordTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.jdom.JDOMException;
import org.samspade79.feeds.ipoints.exceptions.IPointsConnectionException;
import org.samspade79.feeds.ipoints.exceptions.IPointsRequestException;

/**
 * @author sam
 */
@Path( { "r.do", "member-registration.do" })
public class RegistrationHandler extends FrontendHandler {

	private static final String				NameParam		= "name";

	private static final String				Email1Param		= "email1";
    private static final String				Email2Param		= "email2";
	private static final String				Password2Param	= "password2";
	private static final String				Password1Param	= "password1";

	private final transient RegistrationSettings	registrationSettings;

	private MultiValueMap<Attribute, String>		attributeValues;
	private String						email1, email2, password1, password2, name;
	private String						linkback;
	private String						code;
	private String						referrer;
	private boolean						agree;
	private transient RegistrationSession		registrationSession;
	private SubscriptionLevel				subscriptionLevel;
	private Img							image;
	private Upload						imageUpload;
	private boolean						editImages;
	private boolean						editDetails;
	private boolean						register;
	private boolean						agreed;
	private Member						m;
	private boolean						editItemType;
	private transient NewsletterControl			newsletterControl;
	private List<Newsletter>				newsletterOptIns;
	private ItemType						itemType;

	private int							page;

    private boolean                     ipointsRegister;
    private String imageCaption;

    public RegistrationHandler(RequestContext context) {

		super(context);
		this.newsletterControl = NewsletterControl.getInstance(context);

		registrationSession = RegistrationSession.getBySession(context, context.getSessionId());
		this.registrationSettings = RegistrationSettings.getInstance(context);

		assert registrationSession != null;

		context.setAttribute("registrationSession", registrationSession);
	}

	public Object agreed() throws ServletException {

		registrationSession.getAgreed(true);
		registrationSession.save();

		return process();
	}

	private Object askAgreement() throws ServletException {

		final Agreement agreement = registrationSession.getItemType().getRegistrationModule().getAgreement();
		assert agreement != null;

		String title = captions.getRegistrationPageCaption() + " - " + agreement.getTitle();
		FrontendDoc page = new FrontendDoc(context, title);
		page.addTrail(RegistrationHandler.class, "process", title);
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("<style> div.agreement-buttons { padding: 8px; } </style> ");

				sb.append(agreement.getContent());

				sb.append("<div class='agreement-buttons' align='center'>");
				sb.append(new ButtonTag(RegistrationHandler.class, "agreed", agreement.getConfirmButtonText()));
				sb.append(" &nbsp; ");
				sb.append(new ButtonTag(CategoryHandler.class, null, agreement.getRejectButtonText()));
				sb.append("</div>");

				return sb.toString();
			}
		});
		return page;
	}

	private Object askDetails() {

		final RegistrationModule module = registrationSession.getItemType().getRegistrationModule();

		final List<String> referrers;
		if (registrationSettings.hasReferrers()) {
			referrers = registrationSettings.getReferrers();
		} else {
			referrers = module.getReferrers();
		}

		// include non sectioned attributes
		final List<Attribute> nonSectionAttributes = AttributeUtil.getNonSectionedAttributes(registrationSession);
		logger.fine("[RegistrationHandler] nonSectionAttributes=" + nonSectionAttributes);

		// Get attributes and strip out any that have no section, as they will be rendered as part of the default panel
		final List<Attribute> sectionAttributes = AttributeUtil.retainSectionAttributes(registrationSession.getAttributes(page));
		logger.fine("[RegistrationHandler] sectionAttributes=" + sectionAttributes);

		String title = captions.getRegistrationPageCaption() + " - Your details";
		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(RegistrationHandler.class, "process", title);
		doc.addHead("<meta name='robots' content='noindex' />");

        doc.addBody(new MessagesTag(context));
        
		if (registrationSettings.hasDetailsHeader()) {
			doc.addBody(registrationSettings.getDetailsHeader());
		}
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(RegistrationHandler.class, "setDetails", "POST"));
				sb.append(new HiddenTag("page", page));

				/*
				 * The default fields should always be shown on page 1
				 */
				if (page == 1) {

					sb.append(new FormFieldSet(module.getLoginSectionTitle()));

					sb.append("<div class='text'>" + module.getLoginSectionIntro() + "</div>");

					// name field
					sb.append(new FieldInputCell(" *" + registrationSession.getItemType().getNameCaption(), module.getNameDescription(), new TextTag(
							context, NameParam, registrationSession.getName(), 30), new ErrorTag(context, NameParam, " ")));

					// email field
					sb.append(new FieldInputCell(" *" + registrationSession.getItemType().getEmailCaption(), HtmlHelper.nl2br(registrationSession
							.getItemType().getRegistrationModule().getEmailDescription()), new TextTag(context, Email1Param, registrationSession
							.getEmail(), 30), new ErrorTag(context, Email1Param, " ")));

                    sb.append(new FieldInputCell(" *Repeat your " + registrationSession.getItemType().getEmailCaption(),"Please repeat your password to ensure it was entered correctly." ,
                            new TextTag(context, Email2Param, registrationSession.getEmail(), 30), new ErrorTag(context, Email2Param, " ")));

					// password fields
					sb.append(new FieldInputCell("*Enter a password",
							"Enter a password for this account.<br/>It should be between 5 and 15 characters in length.<br/>"
									+ "Passwords are case sensitive.", new PasswordTag(context, Password1Param, registrationSession
									.getPassword(), 16), new ErrorTag(context, Password1Param, " ")));

					sb.append(new FieldInputCell("*Repeat your password", "Please repeat your password to ensure it was entered correctly.",
							new PasswordTag(context, Password2Param, registrationSession.getPassword(), 16), new ErrorTag(context,
									Password2Param, " ")));

					if (nonSectionAttributes.size() > 0) {
						AFormInputPanel panel = new AFormInputPanel(context, registrationSession, nonSectionAttributes);
						sb.append(panel);
					}

					sb.append("</fieldset>");

					if (referrers.size() > 0) {

						sb.append(new FormFieldSet("Referred from"));

						SelectTag tag = new SelectTag(context, "referrer", registrationSession.getReferrer(), referrers, "-Please choose-");

						sb.append(new FieldInputCell("How did you hear about our site?",
								"We would be very grateful if you would let us know how you found out about our site.", tag));

						sb.append("</fieldset>");
					}
				}

				if (sectionAttributes.size() > 0) {
					AFormSectionsInputPanel panel = new AFormSectionsInputPanel(context, registrationSession, sectionAttributes);
					sb.append(panel);
				}

				/*
				 * Show newsletter opt in on the first page
				 */
				if (page == 1) {

					if (newsletterControl.isRegistrationOptIn()) {

						List<Newsletter> newsletters = Newsletter.get(context);
						// remove newsletters we are auto signed up to
						newsletters.removeAll(module.getAutoSignupNewsletters());

						if (newsletters.size() > 0) {
							sb.append(new NewsletterOptInPanel(context, registrationSession, newsletters));
						}
					}
				}

				/*
				 * If we have more pages to come then show continue button otherwise show complete button.
				 */
				SubmitTag submitTag = new SubmitTag("Continue with registration");

				sb.append("<div class='ec_form_commands'>");
				sb.append(submitTag);

				if (config.isDemo() || isSuperman()) {

					sb.append(new ButtonTag("Sample data").setOnClick("this.form.elements['name'].value = 'Mr Test Test'; " +
							"this.form.elements['email1'].value ='" + RandomHelper.getRandomString(6).toLowerCase() +
							"@7soft.co.uk'; this.form.elements['password1'].value = 'qwerty'; " +
							"this.form.elements['password2'].value = 'qwerty';"));
				}

				sb.append("</div>");
				sb.append("</form>");

				return sb.toString();
			}
		});
		if (registrationSettings.hasDetailsFooter()) {
			doc.addBody(registrationSettings.getDetailsFooter());
		}
		return doc;
	}

	private Object askImages() throws ServletException {

		registrationSession.setOfferedImages(true);
		registrationSession.save();

		String title = captions.getRegistrationPageCaption() + " - Images";

		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(RegistrationHandler.class, "process", title, "editImages", true);
		doc.addBody(new MessagesTag(context));
		doc.addBody(new ProfileImagesPanel(context, registrationSession));
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("<div class='ec_form_commands'>");
				sb.append(new ButtonTag(RegistrationHandler.class, "process", "Continue"));
				sb.append(new ButtonTag(RegistrationHandler.class, "process", "Go back to details", "editDetails", true));
				sb.append("</div>");

				return sb.toString();
			}

		});
		return doc;
	}

	private Object askItemType() throws ServletException {

		/*
		 * Get all signup item types
		 */
		final List<ItemType> itemTypes = ItemType.getRegistration(context);

		// strip out any that are hidden
		CollectionsUtil.filter(itemTypes, new Predicate<ItemType>() {

			public boolean accept(ItemType e) {
				return !e.getRegistrationModule().isHidden();
			}

		});

		/*
		 * If no public groups defined then we cannot continue !
		 */
		if (itemTypes.isEmpty()) {
			return new ErrorDoc(context, "No registration item types defined");
		}

		/* 
		 * if only one group not much point showing option screen!
		 */
		if (itemTypes.size() == 1) {
			itemType = itemTypes.get(0);
			return setItemType();
		}

		FrontendDoc doc = new FrontendDoc(context, "Registration");
		doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		doc.addTrail(RegistrationHandler.class, "process", "Registration");

		if (registrationSettings.hasItemTypeHeader()) {
			doc.addBody(registrationSettings.getItemTypeHeader());
		}

		doc.addBody(new Body() {

			@Override
			public String toString() {

				for (ItemType itmeType : itemTypes) {
					sb.append("<div>" + new LinkTag(RegistrationHandler.class, "setItemType", itmeType.getName(), "itemType", itmeType) + "</div>");
				}

				return sb.toString();
			}
		});

		if (registrationSettings.hasItemTypeFooter()) {
			doc.addBody(registrationSettings.getItemTypeFooter());
		}

		return doc;
	}

	/**
	 * Checks input for the current page
	 */
	private void attributeValidators(int page) {

		List<Attribute> attributes = registrationSession.getAttributes(page);
		logger.fine("[RegistrationHandler] adding validators for page=" + page + ", attributes=" + attributes);

		AttributeUtil.setErrors(context, attributes, attributeValues);
	}

	public Object confirm() {

		if (code == null || m == null) {// || !m.confirm(code)) {

			FrontendDoc page = new FrontendDoc(context, "Account confirmation");
			page.addTrail(RegistrationHandler.class, "confirm", "Account confirmation");
			page.addBody(new Body() {

				@Override
				public String toString() {

					sb.append("There was an error confirming your account. Please make sure you followed the link correctly.");
					return sb.toString();
				}

			});
			return page;

		} else {

			FrontendDoc doc = new FrontendDoc(context, "Account confirmation");
			doc.addTrail(RegistrationHandler.class, "confirm", "Account confirmation");
			doc.addBody(new Body() {

				@Override
				public String toString() {

					sb.append("Thank you, your account has been confirmed.<br/>Please " + new LinkTag(AccountHandler.class, null, "click here") +
							" to proceed to your account page.");
					return sb.toString();
				}

			});
			return doc;
		}
	}

	private Object confirmation() {

		String title = captions.getRegistrationPageCaption() + " - Confirm";
		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(RegistrationHandler.class, "process", title);
		doc.addBody(new MessagesTag(context));
		doc.addBody(new RegistrationConfirmationPanel(context, registrationSession));
		return doc;
	}

	private Object confirmationEmailSent() {

		FrontendDoc doc = new FrontendDoc(context, captions.getAccountCaption());
		doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		doc.addBody(new Body() {

			/**
			 *
			 */
			@Override
			public String toString() {

				sb.append("Thank you for registering.<br/><br/>");
				sb.append("We have sent you an email which contains a link that you need to click on to confirm your address is valid.");

				return sb.toString();
			}

		});
		return doc;
	}

	private void defaultFieldValidators() {

		test(new LengthValidator(5, 15), Password1Param);
		test(new LengthValidator(5, 15), Password2Param);
		test(new RequiredValidator(), Email1Param);
        test(new RequiredValidator(), Email2Param);
		test(new RequiredValidator(), NameParam);
		test(new EmailValidator(), Email1Param);
        test(new EmailValidator(), Email2Param);

		if (email1 != null && !RegistrationModule.isUniqueEmail(context, email1)) {
			setError("email1", "That email is already in use. Please use another.");
		}

		if (!registrationSession.getItemType().getAccountModule().isValidEmail(email1)) {

			if (registrationSession.getItemType().getAccountModule().hasEmailErrorMessage()) {
				setError("email1", registrationSession.getItemType().getAccountModule().getEmailErrorMessage());
			} else {
				setError("email1", "Invalid email");
			}
		}

        if (email1 != null && !email1.equalsIgnoreCase(email2)) {

            setError("email1", "Emails do not match.");
            setError("email2 ", "Emails do not match.");
        }

		if (password1 != null && !password1.equalsIgnoreCase(password2)) {

			setError("password1", "Passwords do not match.");
			setError("password2 ", "Passwords do not match.");
		}
	}

	private Object existingUser() {

		String title = captions.getRegistrationPageCaption() + " - Confirm";
		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(RegistrationHandler.class, "process", title);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("You cannot register because you are already logged in.<br/>If you really want to register again, please " +
						new LinkTag(LoginHandler.class, "logout", "log out") + " first.");

				return sb.toString();
			}
		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {
		RegistrationSession.delete(context, context.getSessionId());
        registrationSession = new RegistrationSession(context, context.getSessionId());
        if (linkback != null) {
            registrationSession.setLinkback(linkback);
            registrationSession.save();
        }

        // if we have an item type passed in, set it immediately
        if (itemType != null) {
            registrationSession.setItemType(itemType);
            registrationSession.save();
        }

        if (!registrationSession.hasItemType()) {
            final List<ItemType> itemTypes = ItemType.getRegistration(context);

            // strip out any that are hidden
            CollectionsUtil.filter(itemTypes, new Predicate<ItemType>() {

                public boolean accept(ItemType e) {
                    return !e.getRegistrationModule().isHidden();
                }

            });

            /*
            * If no public groups defined then we cannot continue !
            */
            if (itemTypes.isEmpty()) {
                return new ErrorDoc(context, "No registration item types defined");
            }

            /*
            * if only one group not much point showing option screen!
            */
            if (itemTypes.size() == 1) {
                itemType = itemTypes.get(0);
                registrationSession.setItemType(itemType);
                registrationSession.save();
            } else {
                return askItemType();
            }
        }

        final RegistrationModule module = registrationSession.getItemType().getRegistrationModule();
        List<SubscriptionLevel> subscriptionLevels = registrationSession.getItemType().getSubscriptionModule().getSubscriptionLevels();
        for (SubscriptionLevel subscription : subscriptionLevels) {
            if (registrationSession.getItemType().equals(subscription.getItemType())) {
                subscriptionLevel = subscription;
            }
        }

        String title = captions.getRegistrationPageCaption();
        FrontendDoc doc = new FrontendDoc(context, title);
        doc.addTrail(RegistrationHandler.class, "process", title);
        doc.addHead("<meta name='robots' content='noindex' />");
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append("<div class='text welcome'>" + module.getRegistrationWelcome() + "</div>");
                if (module.getRegistrationInformationContent() != null) {
                    sb.append("<br>");
                    sb.append("<div class='text'>" + module.getRegistrationInformationContent() + "</div>");
                    sb.append("<br>");
                }
                sb.append("<table class='registration account_types' cellspacing='0' cellpadding='0'>");
//                sb.append("<tr><td>");
//                sb.append("<img src='\\images\\NoImage.jpg '></td>");
//                if (subscriptionLevel != null) {
//                    sb.append("<td>" + subscriptionLevel.getComments() + "</td></tr>");
//                }
                sb.append("<tr><td align='right' class='begin'>Click to begin registration</td><td align='left'>" + new LinkTag(RegistrationHandler.class, "start", new ImageTag("files/graphics/pics/start.png"), "linkback", linkback) +
                        "</td></tr>");
                sb.append("</table>");
                return sb.toString();

            }

        });
        return doc;
	}

	public Object process() throws ServletException {

		/* 
		 * if we already logged in show account screen
		 */
		if (account != null) {

			logger.fine("[RegistrationHandler] already logged in");
			if (account.getItemType().getAccountModule().isShowAccount()) {
				return new AccountHandler(context).main();
			} else {
				return existingUser();
			}
		}

		if (page < 1 || page > AttributeUtil.getPagesCount(registrationSession)) {
			page = 1;
		}

		/*
		 * If no item type show item type screen
		 */
		if (!registrationSession.hasItemType() || editItemType) {
			logger.fine("[RegistrationHandler] no item type, asking for item type");
			return askItemType();
		}

		/*
		 * If member group has agreement and not yet agreed then show agreement page
		 */
		if (registrationSession.getItemType().getRegistrationModule().hasExternalAgreement() && !registrationSession.isAgreed()) {
			logger.fine("[RegistrationHandler] showing external agreement");
			return askAgreement();
		}

		/*
		 * If we have asked to edit details again, or are messing basic details, plonk us back to page 1 !
		 */
		if (editDetails) {
			logger.fine("[RegistrationHandler] asking to edit details, returning to page 1");
			page = 1;
			return askDetails();
		}

		boolean requiredDetails = true;
		if (!registrationSession.hasName()) {
			requiredDetails = false;
			logger.fine("[RegistrationHandler] missing name, returning to page 1");
		}

		if (!registrationSession.hasEmail()) {
			requiredDetails = false;
			logger.fine("[RegistrationHandler] missing email, returning to page 1");
		}

		if (!registrationSession.hasPassword()) {
			requiredDetails = false;
			logger.fine("[RegistrationHandler] missing password, returning to page 1");
		}

		if (!requiredDetails) {
			page = 1;
			return askDetails();
		}

		RegistrationModule module = registrationSession.getItemType().getRegistrationModule();

		if (module.isRegistrationImages()) {

			/*
			 * If we haven't been offered images then show images page
			 */
			if (!registrationSession.isOfferedImages()) {
				logger.fine("[RegistrationHandler] images not shown yet");
				return askImages();
			}

			/*
			 * If we have asked to edit images then show screen
			 */
			if (editImages) {
				logger.fine("[RegistrationHandler] asking to edit images");
				return askImages();
			}

			/*
			 * Check we have minimum number of uploaded images
			 */

			if (module.getMinImages() > 0 && registrationSession.getAllImagesCount() < module.getMinImages()) {

				addError("You must upload at least " + module.getMinImages() + " images.");

				logger
						.fine("[RegistrationHandler] min images=" + module.getMinImages() + ", current images=" +
								registrationSession.getAllImagesCount());

				return askImages();
			}

		}

		if (register) {
			return register();
		} else {
			return confirmation();
		}
	}

	private Object register() throws ServletException {

		if (registrationSession.hasErrors()) {

			page = 1;
			return askDetails();
		}

		/*
		 * Check that we have agreed to any inline agreement
		 */
		if (registrationSession.getItemType().getRegistrationModule().hasInlineAgreement() && !agreed) {
			addError("You must agree to " + registrationSession.getItemType().getRegistrationModule().getAgreement().getTitle() +
					" before you can register");
			return confirmation();
		}

		/*
		 * Otherwise do registration !
		 */
		account = registrationSession.register(getRemoteIp());

        IPointsSettings ipointsSettings = IPointsSettings.getInstance(context);
        if (ipointsSettings.isEnabled()) {

            if (ipointsRegister) {

                try {

                    IPointsService service = new IPointsService(context);
                    service.addUser(account);

                } catch (IOException e) {
                    logger.warning(e.toString());

                } catch (JDOMException e) {
                    logger.warning(e.toString());

                } catch (IPointsConnectionException e) {
                    logger.warning(e.toString());
                    throw new RuntimeException(e);

                } catch (IPointsRequestException e) {
                    logger.warning(e.toString());
                }
            }
        }

        /*
		 * If we are confirmed then log us in, otherwise show a message saying we have sent them an email
		 */
		if (!account.isConfirmed()) {
			return confirmationEmailSent();
		}

		if (account.isAwaitingModeration()) {
			return awaitingModeration();
		}

		// only do login stuff if we are approved
		if (account.isLive()) {
			logger.fine("[RegistrationHandler] account is live, logging in");
			account.login(context.getSessionId());
		}

		/*
		 * If subscriptions are available and we haven't been auto subscribed
		 * then send us to the subscriptions handler if force subscriptions is on
		 */
		if (!account.getSubscription().hasSubscriptionLevel()) {
			if (account.hasLiveSubscriptionLevels()) {
				logger.fine("[RegistrationHandler] subscription levels available");
				if (account.getItemType().getSubscriptionModule().isForceSubscription()) {
					logger.fine("[RegistrationHandler] force subscription enabled, showing subscription handler");
					return new SubscriptionHandler(context).main();
				}
			}
		}

		/*
		 * If we have a linkback then forward
		 */
		if (registrationSession.hasLinkback()) {
			logger.fine("[RegistrationHandler] forwarding to linkback=" + registrationSession.getLinkback());
			return new ExternalRedirect(registrationSession.getLinkback());
		}

		/*
		 * If account has registration forward url then forward
		 */
		if (account.getItemType().getRegistrationModule().hasRegistrationForwardUrl()) {

			return new ExternalRedirect(account.getItemType().getRegistrationModule().getRegistrationForwardUrl());

		} else {

			/* 
			 * else send us to the account screen.
			 */
			return new AccountHandler(context).main();

		}
	}

	private Object awaitingModeration() {

		FrontendDoc doc = new FrontendDoc(context, "Account pending moderation");
		doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		doc.addBody(new Body() {

			/**
			 *
			 */
			@Override
			public String toString() {

				sb.append("Thank you for registering.<br/><br/>");
				sb.append("Your account is pending moderation - you will be emailed when it has been approved.");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object removeImage() throws ServletException {

		if (image != null) {
			registrationSession.removeImage(image);
		}

		return askImages();
	}

	/**
	 * Maintain this link for BC
	 * @throws ServletException 
	 */
	public Object reset() throws ServletException {
		return main();
	}

	public Object setDetails() throws ServletException {

		if (!registrationSession.hasItemType()) {
			return process();
		}

		/*
		 * Check field values
		 */
		if (page == 1) {
			defaultFieldValidators();
		}

		/*
		 * Validators for the attributes for our current page
		 */
		attributeValidators(page);

		/*
		 * If we have errors or our places map contains at least one entry then return
		 */
		if (hasErrors()) {
			return askDetails();
		}

		/*
		 * If page 1 set default fields
		 */
		if (page == 1) {

			registrationSession.setName(name);
			registrationSession.setEmail(email1);
			registrationSession.setPassword(password1);
			registrationSession.setReferrer(referrer);
			registrationSession.save();

			registrationSession.setNewsletterOptIns(newsletterOptIns);

		}

		/*
		 * Set attribute values for this page
		 */
		registrationSession.setAttributeValues(attributeValues, page);

		if (page < AttributeUtil.getPagesCount(registrationSession)) {
			page++;
			return askDetails();
		}

		return process();
	}

	public Object setItemType() throws ServletException {

		registrationSession.setItemType(itemType);
		registrationSession.save();

		return process();
	}

	public Object start() throws ServletException {

		RegistrationSession.delete(context, context.getSessionId());
		registrationSession = new RegistrationSession(context, context.getSessionId());

		if (linkback != null) {
			registrationSession.setLinkback(linkback);
			registrationSession.save();
		}

		// if we have an item type passed in, set it immediately
		if (itemType != null) {
			registrationSession.setItemType(itemType);
			registrationSession.save();
		}

		return process();
	}

	public Object uploadImage() throws ServletException {

		logger.fine("[RegistrationHandler] asking to upload: " + imageUpload);

		if (imageUpload != null) {

			RegistrationModule module = registrationSession.getItemType().getRegistrationModule();
			if (module.getMaxImages() > 0 && module.getMaxImages() <= registrationSession.getAllImagesCount()) {
				logger.fine("[RegistrationHandler] max images reached");
				return process();
			}

			try {

				registrationSession.addImage(imageUpload, true, true, false, imageCaption);
				logger.fine("[RegistrationHandler] image uploaded");
                clearParameters();

			} catch (IOException e) {
				logger.warning(e.toString());

			} catch (ImageLimitException e) {
				e.printStackTrace();
			}
		}

		return askImages();
	}
}
