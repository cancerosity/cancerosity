package org.sevensoft.ecreator.iface.frontend.ecom.shopping;

import java.util.List;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.orders.OrderHistoryHandler;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.ecreator.model.ecom.orders.parcels.Parcel;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sam
 */
@Path("order-status.do")
public class OrderStatusHandler extends FrontendHandler {

	private Order				order;
	private String				email;
	private String				orderId;
	private transient OrderSettings	orderSettings;

	public OrderStatusHandler(RequestContext context) {
		super(context);
		this.orderSettings = OrderSettings.getInstance(context);
	}

	@Override
	public Object main() {

		FrontendDoc doc = new FrontendDoc(context, captions.getOrderStatusCaption());
		doc.addTrail(OrderStatusHandler.class, captions.getOrderStatusCaption());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new MessagesTag(context));

                sb.append("<div class='title'>Tracking your order</div>");
                sb.append("Tracking your order could not be easier. Simply enter your order number in the fields below and click the 'Track' button." +
                        " If you do not know your order number, enter your email address and we will show the most recent order for that email." +
                        " Please note that this only tracks the progress of your order in our system, it does not show delivery tracking information.");

				sb.append(new TableTag("order_status"));
				sb.append(new FormTag(OrderStatusHandler.class, "show", "POST"));
				sb.append("<tr><td>Order number:</td><td>" + new TextTag(context, "order", 20) + "</td></tr>");
				sb.append("<tr><td>Email:</td><td>" + new TextTag(context, "email", 30) + "</td></tr>");
				sb.append("<tr><td></td><td>" + new SubmitTag("Track") + "</td></tr>");
				sb.append("</form>");
				sb.append("</table>");

				if (Module.OrderHistory.enabled(context)) {
					sb.append("<div class='title'>Order history</div>");
					sb.append("If you have ordered from us previously then you can log into your account to view your complete "
							+ new LinkTag(OrderHistoryHandler.class, null, "order history") + " at a glance.");
				}

				return sb.toString();
			}

		});
		return doc;
	}

	public Object show() {

		if (order == null) {

			if (orderId != null) {

				// remove prefix and suffix
				if (orderSettings.hasOrderIdPrefix()) {
					orderId = orderId.replace(orderSettings.getOrderIdPrefix(), "");
				}

				if (orderSettings.hasOrderIdSuffix()) {
					orderId = orderId.replace(orderSettings.getOrderIdSuffix(), "");
				}

				order = EntityObject.getInstance(context, Order.class, orderId);
			}
		}

		if (order == null) {

			if (email != null) {

				Item member = Item.getByEmail(context, email);
				if (member != null) {

					List<Order> orders = member.getOrders();
					if (orders.size() > 0)
						order = orders.get(orders.size() - 1);

				}
			}
		}

		if (order == null) {
			addMessage("That order cannot be found");
			return main();
		}

		FrontendDoc doc = new FrontendDoc(context, captions.getOrderStatusCaption());
		doc.addTrail(OrderStatusHandler.class, captions.getOrderStatusCaption());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(OrderStatusHandler.class, "check", "POST"));
				sb.append("<div class='title'>Your order status</div>");
                sb.append("The current status of order number  " + order.getIdString() + " is: " + order.getStatus() + ".");

				if (order.hasParcels()) {

					sb.append(new TableTag("parcels"));
					sb.append("<tr>");
					sb.append("<th>Courier</th>");
					sb.append("<th>Consignment</th");
					sb.append("<th>Track</th>");
					sb.append("</tr>");

					for (Parcel parcel : order.getParcels()) {

						sb.append("<tr>");
						sb.append("<td>" + parcel.getCourier().toString() + "</td>");
						sb.append("<td>" + parcel.getConsignmentNumber() + "</td>");

						String url = parcel.getTrackingUrl();
						if (url == null) {
							sb.append("<td>" + new LinkTag(orderSettings.getTrack(), "Ttrack") +  "</td>");
						} else {
							sb.append("<td>" + new LinkTag(url, "Track parcel").setTarget("_blank") + "</td>");
						}
						sb.append("</tr>");

					}

					sb.append("</table>");
				}

				return sb.toString();
			}

		});
		return doc;
	}

}