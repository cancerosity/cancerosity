package org.sevensoft.ecreator.iface.frontend.items;

import javax.servlet.ServletException;

import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.misc.FieldInputCell;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.reviews.ReviewsDelegate;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 16 Aug 2006 14:05:18
 *
 * Read reviews for an item
 */
@Path("reviews.do")
public class ReviewsHandler extends FrontendHandler {

	private Item	item;
	private String	content;
	private String	author;

	public ReviewsHandler(RequestContext context) {
		super(context);
	}

	public Object add() throws ServletException {

		if (item == null || !ItemModule.Reviews.enabled(context, item)) {
			return index();
		}

		test(new RequiredValidator(), "author");
		test(new RequiredValidator(), "content");

		if (context.hasErrors()) {
			return main();
		}

		ReviewsDelegate del = item.getReviewsDelegate();
		del.addReview(author, content);

		return new ExternalRedirect(item.getUrl());
	}

	@Override
	public Object main() throws ServletException {

		if (item == null || !ItemModule.Reviews.enabled(context, item)) {
			return index();
		}

		context.setAttribute("pagelink", new Link(ReviewsHandler.class, null, "item", item));

		FrontendDoc doc = new FrontendDoc(context, "Add review");
		doc.addTrail(item.getUrl(), item.getName());
		doc.addTrail(ReviewsHandler.class, null, "Add review", "item", item);
		doc.addBody(new MessagesTag(context));
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(ReviewsHandler.class, "add", "post"));
				sb.append(new HiddenTag("item", item));

				sb.append(new FormFieldSet("Your review"));

				sb.append(new FieldInputCell("Your name", "Enter your name as you want it to appear on the review", new TextTag(context, "author", 30),
						new ErrorTag(context, "author", "<br/>")));

				sb.append(new FieldInputCell("Your review", "Enter your name as you want it to appear on the review", new TextAreaTag(context,
						"content", 60, 4), new ErrorTag(context, "content", "<br/>")));

				sb.append("</fieldset>");

				sb.append("<div class='ec_form_commands'>" + new SubmitTag("Add review") + "</div>");

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}
}
