package org.sevensoft.ecreator.iface.frontend.marketing.panels;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.marketing.SmsHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.AGrid;
import org.sevensoft.ecreator.model.attributes.renderers.ARenderType;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.marketing.sms.SmsSettings;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 20 Apr 2007 10:46:34
 *
 */
public class SmsSubscribePanel extends Panel {

	private SmsSettings				smsSettings;
	private MultiValueMap<Attribute, String>	attributeValues;

	public SmsSubscribePanel(RequestContext context, MultiValueMap<Attribute, String> attributeValues) {
		super(context);
		this.attributeValues = attributeValues;
		this.smsSettings = SmsSettings.getInstance(context);
	}

	@Override
	public void makeString() {

		Category category = (Category) context.getAttribute("category");

		sb.append(new FormTag(SmsHandler.class, "add", "GET"));
		sb.append(new HiddenTag("category", category));

		sb.append(new TableTag("form"));
		sb.append("<tr><td colspan='2'>");
		sb.append(smsSettings.getSignupContent());
		sb.append("<td/></tr>");
		sb.append("<tr><td colspan='2'>" + new TextTag(context, "number", 12) + " " + new ErrorTag(context, "number", "<br/>") + "</td></tr>");

		/* Render attribute options
		 * 
		 */
		if (smsSettings.hasAttributes()) {
			AGrid grid = new AGrid(context, null, smsSettings.getAttributes(), attributeValues, ARenderType.Input);
			grid.setCaption("Registration details");
			sb.append(grid);
		}

		sb.append("<tr><td colspan='2'>" + new SubmitTag("Register") + "</td></tr>");
		sb.append("</table>");
		sb.append("</form>");
	}

}
