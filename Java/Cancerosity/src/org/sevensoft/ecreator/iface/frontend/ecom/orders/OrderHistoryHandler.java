package org.sevensoft.ecreator.iface.frontend.ecom.orders;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionStatusHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.InvoiceHardcopyDoc;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.ecom.orders.hardcopy.InvoiceHtml;
import org.sevensoft.ecreator.model.ecom.orders.parcels.Parcel;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.JavascriptLinkTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.results.StringResult;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sam
 */
@Path( { "my-orders.do", "member-orders.do" })
public class OrderHistoryHandler extends SubscriptionStatusHandler {

	private Order	order;

	public OrderHistoryHandler(RequestContext context) {
		super(context);
	}

	public Object invoice() {

		if (order == null) {
			return HttpServletResponse.SC_BAD_REQUEST;
		}

        return new InvoiceHardcopyDoc(context, order);
	}

	public Object list() throws ServletException {

		if (account == null) {
			return new LoginHandler(context, new Link(OrderHistoryHandler.class)).main();
		}

		context.setAttribute("pagelink", new Link(OrderHistoryHandler.class, "list"));

		final List<Order> orders = account.getOrders();

		// add in orders from our 'buyers'
		final List<Item> buyers = account.getBuyerConfig().getBuyers();
		for (Item buyer : buyers) {
			orders.addAll(buyer.getOrders());
		}

		FrontendDoc doc = new FrontendDoc(context, captions.getOrderHistoryCaption());
		doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		doc.addTrail(OrderHistoryHandler.class, null, captions.getOrderHistoryCaption());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("form").setCaption("Your recent orders"));

				sb.append("<tr>");
				sb.append("<th>Order ref</th>");
				if (buyers.size() > 0) {
					sb.append("<th>Account</th>");
				}
				sb.append("<th>Date placed</th>");
				sb.append("<th>Status</th>");
				sb.append("<th>Total</th>");
				sb.append("<th>Print invoice</th>");
				sb.append("</tr>");

				if (orders.isEmpty()) {

					sb.append("<tr><td colspan='5'>You have placed no orders yet.</td></tr>");

				} else {

					for (Order order : orders) {

						Link link = new Link(OrderHistoryHandler.class, "invoice", "order", order);
						JavascriptLinkTag popupLink = new JavascriptLinkTag(context, link, "Print invoice", 600, 800);
						popupLink.setToolbar(true);

						sb.append("<tr>");
						sb.append("<td>" + new LinkTag(OrderHistoryHandler.class, "view", order.getIdString(), "order", order) + "</td>");
						if (buyers.size() > 0) {
							sb.append("<td>#" + order.getAccount().getId() + " " + order.getAccount().getName() + "</td>");
						}
						sb.append("<td>" + order.getDatePlaced().toString("dd/MM/yyyy HH:mm") + "</td>");
						sb.append("<td>" + order.getStatus() + "</td>");
						sb.append("<td>" + order.getTotalInc() + "</td>");

						sb.append("<td>" + popupLink + "</td>");
						sb.append("</tr>");

					}
				}

				sb.append("<tr><th colspan='5'>" + "To return to your account " + new LinkTag(AccountHandler.class, null, "click here") + "</th></tr>");

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {
		return list();
	}

    protected boolean runSecure() {
        return Config.getInstance(context).isSecured();
    }

    public Object view() throws ServletException {

		if (order == null) {
			return list();
		}

		// check we are logged in
		if (account == null) {
			return new LoginHandler(context, new Link(OrderHistoryHandler.class, "view", order, order)).main();
		}

		/*
		 * Check the account matches for this order
		 */
		boolean valid;
		if (order.getAccount().equals(account)) {
			valid = true;
		} else if (order.getAccount().getBuyerFor().equals(account)) {
			valid = true;
		} else {
			valid = false;
		}

		if (!valid) {
			logger.fine("[OrderHistoryHandler] invalid order, returning to list");
			return list();
		}

		context.setAttribute("pagelink", new Link(OrderHistoryHandler.class, "view", "order", order));

		FrontendDoc doc = new FrontendDoc(context, captions.getOrderHistoryCaption());
		doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		doc.addTrail(OrderHistoryHandler.class, null, captions.getOrderHistoryCaption());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("form").setCaption("Order number " + order.getId()));

				sb.append("<tr><th>Order reference</th><td>" + order.getId() + "</td></tr>");
				sb.append("<tr><th>Status</th><td>" + order.getStatus() + "</td></tr>");
				sb.append("<tr><th>Subtotal</th><td>" + order.getTotalEx() + "</td></tr>");
				sb.append("<tr><th>Vat</th><td>" + order.getTotalVat() + "</td></tr>");
				sb.append("<tr><th>Total</th><td>" + order.getTotalInc() + "</td></tr>");
				sb.append("<tr><th>Date placed</th><td>" + order.getDatePlaced().toString("dd MMM yyyy HH:mm") + "</td></tr>");

				sb.append("<tr><th colspan='2'>" + "To return to your list of orders " + new LinkTag(OrderHistoryHandler.class, null, "click here")
						+ "</th></tr>");

				sb.append("</table>");

				sb.append(new TableTag("form").setCaption("Order items"));

				for (OrderLine line : order.getLines()) {

					sb.append("<tr>");
					sb.append("<td>" + line.getQty() + "</td>");
					sb.append("<td>" + line.getDescription() + "</td>");
					sb.append("<td>" + line.getUnitSellInc() + "</td>");
					sb.append("<td>" + line.getLineSellInc() + "</td>");
					sb.append("</tr>");

				}
				sb.append("</table>");

				if (order.hasParcels()) {

					sb.append(new TableTag("form").setCaption("Parcel tracking"));

					sb.append("<tr>");
					sb.append("<th>Courier</th>");
					sb.append("<th>Consignment</th");
					sb.append("<th>Track</th>");
					sb.append("</tr>");

					for (Parcel parcel : order.getParcels()) {

						sb.append("<tr>");
						sb.append("<td>" + parcel.getCourier().toString() + "</td>");
						sb.append("<td>" + parcel.getConsignmentNumber() + "</td>");

						String url = parcel.getTrackingUrl();
						if (url == null)
							sb.append("<td>Track</td>");
						else
							sb.append("<td>" + new LinkTag(url, "Track parcel").setTarget("_blank") + "</td>");

						sb.append("</tr>");

					}

					sb.append("</table>");

				}

				return sb.toString();
			}

		});
		return doc;
	}
}