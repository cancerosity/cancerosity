package org.sevensoft.ecreator.iface.frontend.interaction.winks;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.RestrictionHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionStatusHandler;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.interaction.Interaction;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

/**
 * @author sam
 */
@Path( { "winks.do", "member-winks.do" })
public class WinksHandler extends SubscriptionStatusHandler {

	private Item	recipient;
	private int		page;

	public WinksHandler(RequestContext context) {
		super(context);
	}

	public Object list() throws ServletException {

		if (account == null) {
			return new LoginHandler(context).main();
		}

		if (!PermissionType.ViewNudges.check(context, account)) {
			if (account.getItemType().getInteractionModule().hasRestrictionForward()) {
				return new ExternalRedirect(account.getItemType().getInteractionModule().getRestrictionForward());
			} else {
				return new RestrictionHandler(context).main();
			}
		}

		Interaction interaction = account.getInteraction();

		// now that we are viewing winks, update our account to say we have no new winks
		interaction.setNewWinks(false);
		interaction.save();

		FrontendDoc doc = new FrontendDoc(context, captions.getWinksCaptionPlural());
		if (account.getItemType().getAccountModule().isShowAccount()) {
			doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		}
		doc.addTrail(WinksHandler.class, null, captions.getWinksCaptionPlural());

		Markup markup = account.getItemType().getInteractionModule().getWinksMarkup();
		if (markup == null) {

			logger.fine("[WinksHandler] rendering standard winks");
			doc.addBody(new WinksRenderer(account, context));

		} else {

			List<Item> winkers = account.getInteraction().getWinkers(50);

			Results results = new Results(winkers, page, markup.getPerPage());
			if (results.hasMultiplePages()) {

				doc.addBody(new ResultsControl(context, results, new Link(WinksHandler.class)));
				winkers = results.subList(winkers);
			}

			logger.fine("[WinksHandler] using custom markup=" + markup + ", winkers=" + winkers);
			MarkupRenderer r = new MarkupRenderer(context, markup);
			r.setBodyObjects(winkers);
			doc.addBody(r);

			if (results.hasMultiplePages()) {
				winkers = results.subList(winkers);
			}
		}

		return doc;
	}

	@Override
	public Object main() throws ServletException {
		return list();
	}

	public Object nudge() throws ServletException {
		return wink();
	}

	public Object wink() throws ServletException {

		if (account == null) {
			return new LoginHandler(context).main();
		}

		if (recipient == null) {
			return index();
		}

		if (!PermissionType.SendNudge.check(context, account)) {
			return new RestrictionHandler(context).main();
		}

		account.getInteraction().wink(recipient);

		FrontendDoc page = new FrontendDoc(context, captions.getWinksCaption() + " Sent");
		page.addTrail(recipient.getUrl(), recipient.getDisplayName());
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("You've sent a " + captions.getWinksCaption() + " to " + recipient.getDisplayName() + "!");
				sb.append("<br/><br/>");
				sb.append(new LinkTag(recipient.getUrl(), "Return to member profile"));

				return sb.toString();
			}
		});
		return page;
	}
}
