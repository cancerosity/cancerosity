package org.sevensoft.ecreator.iface.frontend.misc;

/**
 * @author sks 12 Mar 2007 17:11:29
 *
 */
public class TermsFieldSet {

	private String	legend;

	public TermsFieldSet(String legend) {
		this.legend = legend;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("<fieldset class='ec_terms'>");

		if (legend != null) {
			sb.append("<legend>&nbsp;&nbsp;");
			sb.append(legend);
			sb.append("&nbsp;&nbsp;</legend>");
		}

		return sb.toString();
	}
}
