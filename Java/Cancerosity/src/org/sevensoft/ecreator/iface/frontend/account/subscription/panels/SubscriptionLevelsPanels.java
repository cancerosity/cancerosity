package org.sevensoft.ecreator.iface.frontend.account.subscription.panels;

import java.util.List;

import org.sevensoft.ecreator.iface.EcreatorRenderer;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionHandler;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionFeature;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionRate;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 15 May 2006 22:10:19
 *
 */
public class SubscriptionLevelsPanels extends EcreatorRenderer {

	private final List<SubscriptionLevel>	subscriptionLevels;
	private final Item				member;

	public SubscriptionLevelsPanels(RequestContext context, Item member, List<SubscriptionLevel> ls) {
		super(context);

		this.member = member;
		this.subscriptionLevels = ls;
	}

	private void comments() {

		boolean comments = false;
		for (SubscriptionLevel subscription : subscriptionLevels) {
			if (subscription.hasComments()) {
				comments = true;
				break;
			}
		}

		if (!comments)
			return;

		sb.append("<tr>");
		sb.append("<td class='y'>Membership details</td>");

		for (SubscriptionLevel subscription : subscriptionLevels) {

			sb.append("<td align='center'>");
			if (subscription.hasComments())
				sb.append(subscription.getComments());
			sb.append("</td>");
		}

		sb.append("</tr>");
	}

	private void features() {

		for (SubscriptionFeature feature : SubscriptionFeature.get(context)) {
			
			sb.append("<tr>");
			sb.append("<td class='y'>" + feature.getName() + "</td>");
			for (SubscriptionLevel subscriptionLevel : subscriptionLevels) {
				
				String value = subscriptionLevel.getFeatureValues().get(feature);
				if (value == null) {
					value = "";
				}
				
				sb.append("<td>" + value + "</td>");
			}
			sb.append("</tr>");
		}
	}

	private void fee() {

		sb.append("<tr>");
		sb.append("<td class='y'>Subscription fees</td>");

		for (SubscriptionLevel subscriptionLevel : subscriptionLevels) {

			sb.append(new FormTag(SubscriptionHandler.class, "setSubscriptionLevel", "get"));
			sb.append(new HiddenTag("subscriptionLevel", subscriptionLevel));

			sb.append("<td align='center'>");

			List<SubscriptionRate> rates = subscriptionLevel.getSubscriptionRates();
			if (rates.isEmpty()) {

				sb.append("Free subscription");

			} else {

				for (SubscriptionRate sp : rates) {

					sb.append(sp.getDescription());
					sb.append("<br/>");

				}
			}

			sb.append(new SubmitTag("Subscribe"));

			sb.append("</td>");

			sb.append("</form>");
		}

		sb.append("</tr>");
	}

	private void header() {

		sb.append("<tr>");
		sb.append("<td class='y'></td>");

		for (SubscriptionLevel subscription : subscriptionLevels) {
			sb.append("<td class='name' align='center'>" + subscription.getName() + "</td>");
		}

		sb.append("</tr>");
	}

	@Override
	public String toString() {

		int width = 75 / (subscriptionLevels.size() + 1);

		sb.append("<style>");
		sb.append(" table.subscriptions { margin-top: 10px; background: #f1f1f1; font-size: 10pt; font-family: Arial; } ");
		sb.append(" table.subscriptions td.group { font-weight: bold; } ");
		sb.append(" table.subscriptions td { border: 1px solid white; padding: 8px; width: " + width + "%; } ");
		sb.append(" table.subscriptions td.y { width: 25%; } ");
		sb.append("</style> ");

		sb.append(new TableTag("subscriptions"));

		header();
		fee();
		comments();
		features();

		sb.append("</table>");

		return sb.toString();
	}

}
