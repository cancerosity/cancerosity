package org.sevensoft.ecreator.iface.frontend.extras.banners;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.model.advertising.banners.Banner;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.RequestForward;

/**
 * @author sks 23 Oct 2006 07:47:59
 *
 */
@Path("banner-forward.do")
public class BannerForwarderHandler extends Handler {

	private Banner	banner;

	protected BannerForwarderHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		if (banner == null || !banner.hasUrl())
			return new CategoryHandler(context).main();
		
		banner.click();

		return new RequestForward(banner.getUrl());
	}

	@Override
	protected boolean runSecure() {
		return false;
	}
}
