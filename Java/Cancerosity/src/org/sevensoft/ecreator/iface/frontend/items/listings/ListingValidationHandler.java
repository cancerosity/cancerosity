package org.sevensoft.ecreator.iface.frontend.items.listings;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 8 Mar 2007 09:17:55
 *
 */
@Path("lval.do")
public class ListingValidationHandler extends FrontendHandler {

	private Item	item;
	private String	code;

	public ListingValidationHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (item == null) {
			logger.fine("[ListingValidationHandler] no item supplied");
			return HttpServletResponse.SC_BAD_REQUEST;
		}

		if (code == null) {
			logger.fine("[ListingValidationHandler] no val code supplied");
			return HttpServletResponse.SC_BAD_REQUEST;
		}

		if (item.getListing().validate(code)) {

			FrontendDoc doc = new FrontendDoc(context, "Email address validated");
			doc.addBody("Thank you, your email address has been confirmed.<br/><br/>" + new LinkTag(item.getUrl(), "Click here")
					+ " to view your listing.");
			return doc;

		} else {

			FrontendDoc doc = new FrontendDoc(context, "Email address validation error");
			doc
					.addBody("There was a problem validating your email address.<br/>Please make sure the link in the email was followed correctly.<br/><br/>To receive the email again, "
							+ new LinkTag(ListingValidationHandler.class, "resend", "click here", "item", item));
			return doc;

		}
	}

	public Object resend() {

		if (item == null) {
			logger.fine("[ListingValidationHandler] no item supplied");
			return HttpServletResponse.SC_BAD_REQUEST;
		}

		try {
			item.getListing().emailValidation();
		} catch (EmailAddressException e) {
			e.printStackTrace();
		} catch (SmtpServerException e) {
			e.printStackTrace();
		}

		FrontendDoc doc = new FrontendDoc(context, "Email validation sent");
		doc.addBody("The email to validate this listing has been sent again.<br/><br/>" + new LinkTag(item.getUrl(), "Click here")
				+ " to view your listing.");
		return doc;
	}
}
