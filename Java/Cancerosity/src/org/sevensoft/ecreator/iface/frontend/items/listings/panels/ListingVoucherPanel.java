package org.sevensoft.ecreator.iface.frontend.items.listings.panels;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.items.listings.AddListingHandler;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.iface.frontend.misc.FieldInputCell;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * User: Tanya
 * Date: 03.09.2012
 */
public class ListingVoucherPanel extends Panel {

    private final Item item;
    private Class<? extends Handler> handler;

    public ListingVoucherPanel(RequestContext context, Item item) {
        super(context);
        this.item = item;
        this.handler = AddListingHandler.class;
    }

    public void makeString() {
        sb.append("If you have an appropriate voucher code you can apply it on this screen.");
        sb.append(new FormTag(handler, "applyVoucher", "multi"));
        sb.append(new HiddenTag("item", item));

        sb.append(new FormFieldSet("Add Voucher"));
        sb.append(new FieldInputCell("Voucher code", null, new TextTag(context, "voucherCode", item.getListing().getVoucher())));
        sb.append(new FieldInputCell("", null, new SubmitTag("Apply")));

        sb.append("</fieldset>");

        sb.append("</form>");

    }
}
