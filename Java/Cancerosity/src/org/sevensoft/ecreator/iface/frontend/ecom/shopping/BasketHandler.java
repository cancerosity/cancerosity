package org.sevensoft.ecreator.iface.frontend.ecom.shopping;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.LinkedHashMap;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.containers.boxes.BoxContainer;
import org.sevensoft.ecreator.model.ecom.delivery.DeliveryOption;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLineOption;
import org.sevensoft.ecreator.model.ecom.shopping.renderer.BasketRenderer;
import org.sevensoft.ecreator.model.ecom.vouchers.InvalidVoucherException;
import org.sevensoft.ecreator.model.ecom.vouchers.Voucher;
import org.sevensoft.ecreator.model.ecom.vouchers.VoucherItem;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.stock.StockModule;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.commons.samdate.Date;

/**
 * @author sam
 */
@Path("basket.do")
public class BasketHandler extends FrontendHandler {

	private DeliveryOption			delivery;
	private BasketLine			line;
	private LinkedHashMap<ItemOption, String> options;
	private List<Item>			accessories;
	private String				voucherCode;
	private String				ident;
	private Map<ItemOption, Upload>	optionUploads;
	private Map<Integer, Integer>		qtys;
	private Item				item;
    private String linkback;

	public BasketHandler(RequestContext context) {
		this(context, null, null, null);
	}

	public BasketHandler(RequestContext context, Item item, LinkedHashMap<ItemOption, String> options, Map<ItemOption, Upload> optionUploads) {
		super(context);
		this.item = item;
		this.options = options;
		this.optionUploads = optionUploads;
	}

	/**
	 * Add a single item to the basket
	 */
	public Object add() throws ServletException {

		if (!Module.Shopping.enabled(context)) {
			return main();
		}

		logger.fine("[BasketHandler=" + context.getSessionId() + "] main item=" + item);

		// check for presence of ident in this basket
//        if (basket.containsIdent(ident)) {
        BasketLine basketLine = basket.containsItem(item, options);
        if (basketLine != null) {
            logger.fine("[BasketHandler=" + context.getSessionId() + "] Basket contains item - skipping");
            if (item.hasStockModule() && (item.getItemType().getStockModule().getStockControl() == StockModule.StockControl.RealTime
                    || item.getItemType().getStockModule().getStockControl() == StockModule.StockControl.Manual) &&
                    item.getAvailability() != null && basketLine.getQty() >= item.getAvailableStock() && !item.isBackorders()) {
                addError("Qty of " + item.getName() + " greater then in stock!");
            }
            else
            if (item.hasStockModule() && (item.getItemType().getStockModule().getStockControl() == StockModule.StockControl.RealTime
                    || item.getItemType().getStockModule().getStockControl() == StockModule.StockControl.Manual) && item.getAvailability() != null
                    && basketLine.getQty() >= item.getAvailableStock() && item.isBackorders()) {
                addMessage("The item " + item.getName() + " is not in stock, so we put it on back order for you. ");
                basket.setLineQty(basketLine, basketLine.getQty() + 1);
            }
            else {
                basket.setLineQty(basketLine, basketLine.getQty() + 1);
                addMessage(captions.getAddBasketItemCaptionRendered(context, item));
            }
            return returnPage();
        }

		if (item == null) {
			return main();
		}

		// if instant buy empty basket first as we only want a single item in it
		if (item.getItemType().getOrderingModule().isInstantBuy()) {
			basket.empty();
		}

		// get QTY
		int qty = qtys.get(item.getId());

		logger.fine("[BasketHandler=" + context.getSessionId() + "] adding item: " + item + ", qty=" + qty + ", options=" + options);

		if (!item.isOrderable(account)) {

			logger.fine("item is not orderable");
			addError("'" + item.getName() + "' is not orderable");
			return new ItemHandler(context, item).main();
		}

		// check qty is between min / max shopping levels
		if (item.hasOrderQtyMax() && qty > item.getOrderQtyMax()) {
			logger.fine("qty is above max limit of " + item.getOrderQtyMax());
			addError("You can only order a maximum of " + item.getOrderQtyMax() + " of '" + item.getName() + "'");
			return new ItemHandler(context, item).main();
		}

		if (item.hasOrderQtyMin() && qty < item.getOrderQtyMin()) {
			logger.fine("qty is below min limit of " + item.getOrderQtyMin());
			addError("You must order at least " + item.getOrderQtyMin() + " of '" + item.getName() + "'");
			return new ItemHandler(context, item).main();
		}

		// we now check we have values for all non-optional options!
		final List<ItemOption> itemOptions = item.getOptionSet().getOptions();
		for (ItemOption option : itemOptions) {

			if (!option.isOptional()) {
				if (!options.containsKey(option)) {
					logger.fine("option required but missing optionId" + option.getId() + ", value=" + options.get(option));
					setError("options_" + option.getId(), "You must choose an option");
				} else {
                    if (option.isStockOverride()) {
                        if (qty > option.getSelectionFromId(new Integer(options.get(option))).getStock()) {
                            setError("options_" + option.getId(), "We currently have " + option.getSelectionFromId(new Integer(options.get(option))).getStock() +
                                    " items of this option");
                        }
                    }
                }
			}
		}

		// check we have ordered our min number of options
		int min = item.getOptionSet().getMinimumOptionSelection();
		if (min > 0 && min <= itemOptions.size() && min > options.size()) {
			addError("You must choose at least " + min + " options from the item screen.");
			return new ItemHandler(context, item).main();
		}

		if (hasErrors()) {
			logger.fine("[BasketHandler=" + context.getSessionId() + "] errors, returning to item");
			return new ItemHandler(context, item).main();
		}

        Map<ItemOption, String> tempOptions = new LinkedHashMap<ItemOption, String>();
        for (ItemOption option : itemOptions) {
            tempOptions.put(option, options.get(option));
        }
        options.clear();
        options.putAll(tempOptions);

		try {

			basket.addItem(ident, item, qty, options, optionUploads);

		} catch (IOException e) {
			e.printStackTrace();
			return returnPage();

		} catch (AttachmentExistsException e) {
			e.printStackTrace();
			return returnPage();

		} catch (AttachmentTypeException e) {
			e.printStackTrace();
			return returnPage();
		}

		// add in any present accessories
		for (Item accessory : accessories) {
			try {
				basket.addItem(null, accessory, 1, null, null);
			} catch (IOException e) {
				addError(e);
			} catch (AttachmentExistsException e) {
				addError(e);
			} catch (AttachmentTypeException e) {
				addError(e);
			}
		}

		// if instant buy forward straight to check. Do not add messages
		if (item.getItemType().getOrderingModule().isInstantBuy()) {
			return new CheckoutHandler(context).reset();
		}

		addMessage(captions.getAddBasketItemCaptionRendered(context, item));

        return returnPage();
    }

    private Object returnPage() throws ServletException {
        if (shoppingSettings.isQuickBasketAdd() && linkback != null) {
            return new ExternalRedirect(linkback);
            
        } else if (shoppingSettings.isQuickBasketAdd() && item != null) {

            return new ItemHandler(context, item).main();

        } else {

            return main();
        }
    }

    public Object empty() throws ServletException {

		if (!Module.Shopping.enabled(context)) {
			return new CategoryHandler(context).main();
		}

		basket.empty();
		addMessage(captions.getBasketClearedCaption());

		return main();
	}

	@Override
	public Object main() throws ServletException {

		if (!Module.Shopping.enabled(context)) {
			return new CategoryHandler(context).main();
		}

		// pagelink is a link back to this page
		context.setAttribute("pagelink", new Link(BasketHandler.class));

		FrontendDoc doc = new FrontendDoc(context, captions.getShoppingBasketCaption());
		doc.addTrail(BasketHandler.class, captions.getShoppingBasketCaption());
		doc.addHead("<meta name='robots' content='noindex' />");
		doc.addBody(new BasketRenderer(context, basket));
        final PaymentSettings paymentSettings = PaymentSettings.getInstance(context);
        if (Module.GoogleCheckout.enabled(context) && !basket.isEmpty() && paymentSettings.getPaymentTypes().contains(PaymentType.GoogleCheckout)) {
            doc.addBody(new FormTag(DirectGoogleCheckoutHandler.class, "processGoogleDirectly", "multi"));
            doc.addBody("<input type='hidden' name='togoogle' value='direct'/>");
            doc.addBody("<input src=" + PaymentType.GoogleCheckout.getButtonSrc(context) + " type='image'>");
            doc.addBody("</form>");
        }
        return doc;
	}

	public Object remove() throws ServletException {

		if (!Module.Shopping.enabled(context)) {
			return new CategoryHandler(context).main();
		}

		if (line != null) {
			basket.removeLine(line);
		}

		return main();
	}

	public Object removeVoucher() throws ServletException {

		if (!Module.Shopping.enabled(context))
			return new CategoryHandler(context).main();

		basket.removeVoucher();

		addMessage("Voucher has been removed");
		return main();
	}

	@Override
	protected void setBoxCons(List<BoxContainer> boxcons) {
		for (BoxContainer boxcon : boxcons) {
			boxcon.setBasket(true);
		}
	}

	public Object update() throws ServletException {

		if (!Module.Shopping.enabled(context)) {
			return new CategoryHandler(context).main();
		}

		for (BasketLine line : basket.getLines()) {

			if (line.isPromotion()) {
				continue;
			}

            Integer qty = qtys.get(line.getId());
            Item item = line.getItem();
            boolean stockOverrided = false;
            if (line.hasOptions()) {
                for (BasketLineOption basketLineOption : line.getOptions()) {
                    if (basketLineOption.getOption().isStockOverride()) {
                        for (ItemOptionSelection itemOptionSelection : basketLineOption.getOption().getSelections()) {
                            if (basketLineOption.getValue().equals(itemOptionSelection.getText())) {
                                if (qty > itemOptionSelection.getStock()) {
                                    qty = itemOptionSelection.getStock();
                                    addError("We currently have only " + qty + " items of this option");
                                }
                            }
                        }
                        stockOverrided = true;
                        break;
                    }
                }
            }
            if (!stockOverrided) {
                if (item.hasStockModule() && (item.getItemType().getStockModule().getStockControl() == StockModule.StockControl.RealTime
                        || item.getItemType().getStockModule().getStockControl() == StockModule.StockControl.Manual) && item.getAvailability() != null
                        && qty != null && qty > item.getAvailableStock() && !item.isBackorders()) {
                    basket.setLineQty(line, item.getAvailableStock());
                    addError("Qty of " + item.getName() + " greater then in stock!");
                    return main();
                } else if (qty != null) {
                    basket.setLineQty(line, qty);
                    if (item.hasStockModule() && (item.getItemType().getStockModule().getStockControl() == StockModule.StockControl.RealTime
                            || item.getItemType().getStockModule().getStockControl() == StockModule.StockControl.Manual) && item.getAvailability() != null
                            && qty > item.getAvailableStock() && item.isBackorders()) {
                        addMessage("The item " + item.getName() + " is not in stock, so we put it on back order for you. ");
                    }
                }
            } else {
                basket.setLineQty(line, qty);
            }
        }

		basket.setDeliveryOption(delivery);
		try {

			logger.fine("[BasketHandler=" + context.getSessionId() + "] setting voucher code=" + voucherCode);

            Voucher voucher = Voucher.getByCode(context, voucherCode);

            if (voucher != null && voucher.isSpecificItemsVoucher()) {
                boolean addVoucher = false;

                for (Item item : basket.getItems()) {
                    Query q = new Query(context, "select voucher, item from # vi join # v where v.code " +
                            "= ? and vi.item = ? and vi.voucher = v.id");
                    q.setTable(VoucherItem.class);
                    q.setTable(Voucher.class);
                    q.setParameter(voucherCode);
                    q.setParameter(item);

                    if (!q.execute().isEmpty()) {
                        addVoucher = true;
                        break;
                    }

                }

                if (addVoucher) {
                    basket.setVoucherCode(voucherCode);
                    for (BasketLine basketLine : basket.getLines()) {
                        for (Item item : voucher.getItems()) {
                            if (basketLine.getItem().equals(item) && voucher.isGetOneFree() && !voucher.isGetExtraItem()
                                    && (voucher.getStartDate().getTimestamp() > new Date().getTimestamp() && voucher.getEndDate().getTimestamp() < new Date().getTimestamp())) {
                                try {
                                    basket.addItem(ident, item, 1, null, null);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (AttachmentExistsException e) {
                                    e.printStackTrace();
                                } catch (AttachmentTypeException e) {
                                    e.printStackTrace();
                                }
                            } else if (basketLine.getItem().equals(item) && voucher.isGetOneFree() && voucher.isGetExtraItem()
                                    && (voucher.getStartDate().getTimestamp() > new Date().getTimestamp() && voucher.getEndDate().getTimestamp() < new Date().getTimestamp())) {
                                for (Item extraItem : voucher.getExtraItems()) {
                                    try {
                                        basket.addItem(ident, extraItem, 1, null, null);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    } catch (AttachmentExistsException e) {
                                        e.printStackTrace();
                                    } catch (AttachmentTypeException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                basket.setVoucherCode(voucherCode);
            }

        } catch (InvalidVoucherException e) {

			logger.fine(e.toString());
			addError(e);
		}

		addMessage(captions.getBasketUpdatedCaption());
		clearParameters();
		return main();
	}
}