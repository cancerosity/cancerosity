package org.sevensoft.ecreator.iface.frontend.interaction.userplane.recorder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.account.RestrictionHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionStatusHandler;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.interaction.userplane.recorder.UserplaneRecorderSettings;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 26 Nov 2006 14:25:38
 * 
 * This handler will display a page to record the video
 *
 */
@Path("userplane-recorder.do")
public class UserplaneRecorderHandler extends SubscriptionStatusHandler {

	private final transient UserplaneRecorderSettings	settings;

	public UserplaneRecorderHandler(RequestContext context) {
		super(context);
		this.settings = UserplaneRecorderSettings.getInstance(context);
	}

	public Object main() throws ServletException {

		if (!Module.UserplaneRecorder.enabled(context)) {
			return HttpServletResponse.SC_FORBIDDEN;
		}

		if (account == null) {
			return new LoginHandler(context, new Link(UserplaneRecorderHandler.class)).main();
		}

		if (!PermissionType.UserplaneRecorder.check(context, account)) {
			if (account != null && account.getItemType().getInteractionModule().hasRestrictionForward()) {
				return new ExternalRedirect(account.getItemType().getInteractionModule().getRestrictionForward());
			} else {
				return new RestrictionHandler(context).main();
			}
		}

		FrontendDoc doc = new FrontendDoc(context, "My video");
		doc.addTrail(UserplaneRecorderHandler.class, "My video");
		if (settings.hasRecorderPageHeader()) {
			doc.addBody(settings.getRecorderPageHeader());
		}

		doc.addBody(new UserplaneRecorderRenderer(context, 0, false));

		if (settings.hasRecorderPageFooter()) {
			doc.addBody(settings.getRecorderPageFooter());
		}

		return doc;
	}
}
