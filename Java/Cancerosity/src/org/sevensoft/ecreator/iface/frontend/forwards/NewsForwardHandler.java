package org.sevensoft.ecreator.iface.frontend.forwards;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.ExternalRedirect;

/**
 * @author sks 29 Aug 2006 10:23:11
 * 
 * Handler for mapping old news.do links to new news page
 */
@Path("news.do")
public class NewsForwardHandler extends Handler {

	public NewsForwardHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		// find category called news
		Category category = Category.getByName(context, "News");
		if (category == null) {
			return new CategoryHandler(context).main();
		} else {
			return new ExternalRedirect(category.getUrl());
		}
	}

	@Override
	protected boolean runSecure() {
		return false;
	}
}
