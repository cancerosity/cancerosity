package org.sevensoft.ecreator.iface.frontend.attachments;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.AttachmentOwner;
import org.sevensoft.ecreator.model.attachments.downloads.Download;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.stats.downloads.DownloadCounterMonthly;
import org.sevensoft.ecreator.model.stats.downloads.DownloadCounterWeekly;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.StreamResult;
import org.sevensoft.jeezy.http.results.StringResult;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * @author sks 10-Jan-2006 14:59:36
 */
@Path("attachments.do")
public class AttachmentHandler extends FrontendHandler {

    @SuppressWarnings("hiding")
    private static Logger logger = Logger.getLogger(AttachmentHandler.class.getName());

    private Attachment attachment;
    private Upload upload;
    private Category category;
    private Item item;
    private boolean preview;
    private Download download;

    public AttachmentHandler(RequestContext context) {
        super(context);
    }

    public Object download() throws ServletException {

        if (download != null) {
            attachment = download.getAttachment();
        }

        if (attachment == null) {
            return HttpServletResponse.SC_BAD_REQUEST;
        }

        File physicalFile;
        if (preview) {
            physicalFile = ResourcesUtils.getRealAttachment(attachment.getPreviewPath());
        } else {
            physicalFile = ResourcesUtils.getRealAttachment(attachment.getPath());
            if (!physicalFile.exists()) {
                physicalFile = ResourcesUtils.getRealPdf(attachment.getPath());
            }
            if (!physicalFile.exists()) {
                physicalFile = ResourcesUtils.getRealVideo(attachment.getPath());
            }
            if (!physicalFile.exists()) {
                physicalFile = ResourcesUtils.getRealImage(attachment.getPath());
            }
        }

        /*
           * check file actually exists - it might have been deleted on the underlying filesystem.
           */
        if (!physicalFile.exists()) {
            logger.fine("[AttachmentHandler] physical file not found=" + physicalFile);
            return HttpServletResponse.SC_EXPECTATION_FAILED;
        }

        /*
           * if we require credits or payment forward to the download session handler
           * but only if it is not a preview we want
           */
        if (download == null) {

            if (!preview) {

                if (attachment.hasCredits() || attachment.hasFee()) {

                    logger.fine("attachment requires purchase or credits, exiting to download handler");
                    return new DownloadHandler(context, attachment).main();
                }

            }

            // if download is null then always log download, otherwise we will have logged it when they bought it
            DownloadCounterMonthly.process(context, new Date(), attachment, preview);
            DownloadCounterWeekly.process2(context, new Date(), attachment, preview);

        } else {

            // check this download is ours
            if (download.isExpired()) {
                logger.fine("[AttachmentHandler] download expired");
                return HttpServletResponse.SC_FORBIDDEN;
            }

            if (account == null) {
                logger.fine("[AttachmentHandler] not logged in");
                return new LoginHandler(context, new Link(AttachmentHandler.class, "download", "download", download)).main();
            }

            if (!download.getAccount().equals(account)) {
                logger.fine("[AttachmentHandler] current user does not own this download");
                return HttpServletResponse.SC_FORBIDDEN;
            }
        }

        logger.fine("[AttachmentHandler] attachment method=" + attachment.getMethod());

        try {

            // get method for attachment and return result object
            switch (attachment.getMethod()) {

                default:
                case Download:
                    return new StreamResult(physicalFile, attachment.getContentType(), attachment.getFilename(), StreamResult.Type.Attachment);

                case Stream:
                    return new StreamResult(physicalFile, attachment.getContentType(), attachment.getFilename(), StreamResult.Type.Stream);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return HttpServletResponse.SC_BAD_GATEWAY;
        }
    }

    public Object edit() throws ServletException {

        if (attachment == null) {
            return new AccountHandler(context).main();
        }

        AdminDoc doc = new AdminDoc(context, user, "Edit attachment", null);
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update attachment"));
                sb.append(new ButtonTag(AttachmentHandler.class, "delete", "Delete this attachment", "attachment", attachment)
                        .setConfirmation("Are you sure you want to delete this attachment?"));

                sb.append("</div>");
            }

            private void general() {

                sb.append(new AdminTable("General"));

                sb.append("<tr><td width='300'><b>Name</b><br/>Rename this attachment. By default this is the name of the file.</td>");
                sb.append("<td>" + new TextTag(context, "name", attachment.getName(), 40) + "</td>");

                SelectTag tag = new SelectTag(context, "type", attachment.getMethod());
                tag.addOptions(Attachment.Method.values());

                if (Module.AttachmentPayment.enabled(context)) {

                    sb.append("<tr><td width='300'><b>Credits</b><br/>"
                            + "Enter the number of credits needed to download this attachment or leave at zero for free download.</td>");
                    sb.append("<td>" + new TextTag(context, "credits", attachment.getCredits(), 4) + "</td></tr>");
                }

                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(AttachmentHandler.class, "save", "POST"));
                sb.append(new HiddenTag("attachment", attachment));

                general();
                commands();

                sb.append("</form>");
                return sb.toString();
            }

        });

        return doc;
    }

    public Object m3u() {

        if (attachment == null) {
            return HttpServletResponse.SC_BAD_REQUEST;
        }

        //		string = attachment.getPlaylistUrl();
        String string = attachment.getUrl();
        return new StreamResult(string, "application/octect-stream", attachment.getIdString() + ".m3u", StreamResult.Type.Stream);
    }

    @Override
    public Object main() throws ServletException {
        return download();
    }

    public Object popupMp3() {

        String link = Config.getInstance(context).getUrl() + "/" + new Link(PlaylistHandler.class, null, "attachment", attachment);

        StringBuilder sb = new StringBuilder();

        sb.append("<html>");
        sb.append("<head>");
        sb.append("<title>Preview</title>");
        sb.append("<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />");
        sb.append("</head>");
        sb.append("<body topmargin='0' leftmargin='0' marginwidth='0' marginheight='0'>");
        sb.append("<OBJECT ID='mediaPlayer' CLASSID='CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95' width='320' height='306'>");
        sb.append("<PARAM name=\"URL\" value=\"" + link + "\">");
        sb.append("<PARAM name=\"autoStart\" value=\"True\">");
        sb.append("<PARAM name=\"PreviewMode\" VALUE=\"false\">");
        sb.append("<PARAM NAME=\"FileName\" VALUE=\"" + link + "\">");
        sb.append("<PARAM NAME=\"AnimationatStart\" VALUE=\"true\">");
        sb.append("<EMBED type=\"application/x-mplayer2\"; pluginspage=\"http://www.microsoft.com/Windows/MediaPlayer/\"; SRC=\"" + link +
                "\"; width=320; height=320; autostart=1; previewmode=0; showcontrols=1></EMBED>");
        sb.append("</OBJECT>");
        sb.append("</body>");
        sb.append("</html>");

        return new StringResult(sb, "text/html");
    }

    public Object removeAttachment() throws ServletException {

        if (attachment == null)
            return main();

        // check we are logged in
        if (account == null) {
            return new LoginHandler(context).main();
        }

        // check we are owner
        if (!attachment.getItem().getAccount().equals(account)) {
            return HttpServletResponse.SC_BAD_REQUEST;
        }

        AttachmentOwner owner = attachment.getOwner();
        owner.removeAttachment(attachment);

        return main();
    }

    public Object upload() throws ServletException {

        String error = null;
        if (upload != null) {

            try {

                if (category != null) {

                    category.addAttachment(upload, false);
                    return new CategoryHandler(context, category).main();
                }

                if (item != null) {
                    item.addAttachment(upload, false);
                    return new ItemHandler(context, item).main();
                }

            } catch (AttachmentExistsException e) {
                error = e.getMessage();

            } catch (IOException e) {
                e.printStackTrace();
                error = "There was a server error.";

            } catch (AttachmentTypeException e) {
                error = e.getMessage();

            } catch (AttachmentLimitException e) {
                error = "You cannot upload anymore files.";
            }
        }

        final String message;
        if (error != null)
            message = "The error was: " + error;
        else
            message = null;

        FrontendDoc page = new FrontendDoc(context, "Attachment upload error");
        page.addBody(new Body() {

            @Override
            public String toString() {

                sb.append("There was an error uploading the file. Please press back on your browser and try again.<br/><br/>");
                if (message != null)
                    sb.append(message);

                return sb.toString();
            }
        });
        return page;
    }
}
