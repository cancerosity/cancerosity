package org.sevensoft.ecreator.iface.frontend.ecom.shopping;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.net.EmailValidator;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.iface.frontend.ErrorDoc;
import org.sevensoft.ecreator.iface.frontend.FormRow;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.registration.RegistrationHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.payments.panels.PaymentTable;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.panels.CollectionConfirmationPanel;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.panels.DeliveryConfirmationPanel;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.panels.PayPalProHostedPanel;
import org.sevensoft.ecreator.iface.frontend.misc.FieldInputCell;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.iface.admin.misc.seo.panels.GoogleAnalyticsEcommerceTrackingPanel;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.ecreator.model.attributes.renderers.input.AFormSectionsInputPanel;
import org.sevensoft.ecreator.model.containers.boxes.BoxContainer;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.FormField;
import org.sevensoft.ecreator.model.design.marker.MarkerRenderer;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.addresses.AddressFields;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.AddressLineException;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.ContactNameException;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.CountryException;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.PostCodeException;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.TownException;
import org.sevensoft.ecreator.model.ecom.addresses.panels.AddressEntryPanel;
import org.sevensoft.ecreator.model.ecom.addresses.panels.AddressesPanel;
import org.sevensoft.ecreator.model.ecom.branches.Branch;
import org.sevensoft.ecreator.model.ecom.branches.BranchSearcher;
import org.sevensoft.ecreator.model.ecom.delivery.DeliveryOption;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.ecreator.model.ecom.orders.OrderStatusException;
import org.sevensoft.ecreator.model.ecom.orders.emails.OrderEmail;
import org.sevensoft.ecreator.model.ecom.payments.PaymentException;
import org.sevensoft.ecreator.model.ecom.payments.PaymentRedirect;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.cards.CardType;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardHolderException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardTypeException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CscException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiredCardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiryDateException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.IssueNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.StartDateException;
import org.sevensoft.ecreator.model.ecom.payments.forms.able2buy.CreditGroup;
import org.sevensoft.ecreator.model.ecom.payments.forms.hsbc.HsbcCpi;
import org.sevensoft.ecreator.model.ecom.payments.forms.paypal.PayPalExpress;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorException;
import org.sevensoft.ecreator.model.ecom.shopping.ShoppingSettings.DispatchType;
import org.sevensoft.ecreator.model.ecom.shopping.ShoppingSettings;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.ecom.vouchers.InvalidVoucherException;
import org.sevensoft.ecreator.model.ecom.vouchers.Voucher;
import org.sevensoft.ecreator.model.ecom.vouchers.VoucherItem;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterControl;
import org.sevensoft.ecreator.model.marketing.newsletter.Subscriber;
import org.sevensoft.ecreator.model.misc.location.LocationUtil;
import org.sevensoft.ecreator.model.stats.visits.Visitor;
import org.sevensoft.ecreator.model.stats.StatsCenter;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.bespoke.ipoints.IPointsSettings;
import org.sevensoft.ecreator.model.bespoke.ipoints.IPointsService;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.accounts.login.markers.ForgottenPasswordMarker;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.RadioTag;
import org.sevensoft.jeezy.http.html.form.bool.SimpleRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.PasswordTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;
import org.jdom.JDOMException;
import org.samspade79.feeds.ipoints.exceptions.IPointsConnectionException;
import org.samspade79.feeds.ipoints.exceptions.IPointsRequestException;

/**
 * @author sks 28 Dec 2006 15:24:42
 *
 */
@Path( { "chk.do", "checkout.do" })
public class CheckoutHandler extends FrontendHandler {

	private DeliveryOption				deliveryOption;
	private boolean					editPaymentType;
	private List<Newsletter>			newsletterOptIns;

	final transient OrderSettings			orderSettings;
	private final transient PaymentSettings	paymentSettings;
	transient final NewsletterControl		newsletterControl;
	private boolean					editCreditGroup;

	private boolean					editCard;
	private PaymentType				paymentType;
	private String					expiryYear;
	private String					expiryMonth;
	private String					startYear;
	private String					startMonth;
	private String					cardIssuer;
	private String					issueNumber;
	private String					csc;
	private String					cardNumber;
	private String					cardHolder;
	private CardType					cardType;
	private CreditGroup				creditGroup;
	private Country					country;
    private String state;
	private String					address1, address2, address3;
	private String					name;
	private String					postcode;
	private String					town;
	private String					telephone1;
	private String					telephone2;
	private String					telephone3;
	private String					county;
	private String					instructions;
	private String					accountEmail;
	private String					accountName;
    private String                  referrer;
    private boolean					editBillingAddress;
	private boolean					editDeliveryAddress;
	private boolean					vatExempt;
	private boolean					place;
	private boolean					editDeliveryOption;
	private Form					form;
	private LinkedHashMap<FormField, String>	data;
	private List<Upload>				uploads;
	private int						page;
	private String					voucherCode;
	private Map<Attribute, String>		attributeValues;
	private String					vatExemptText;
	private boolean					editAttributes;
	private boolean					askReserve;
	private boolean					reserve;
	private DispatchType				dispatchType;
	private boolean					editDispatchType;
	private Branch					branch;
	private boolean					editCollectionDetails;
	protected String					location;
	private boolean					agreed;
	private int						addressId;
    private String resultCode;
    private String                  customerReference;
    private String					ipointsUsername;
	private String					ipointsPassword;
    private String                  longMessage;

    public CheckoutHandler(RequestContext context) {
		super(context);

		this.paymentSettings = PaymentSettings.getInstance(context);
		this.orderSettings = OrderSettings.getInstance(context);
		this.newsletterControl = NewsletterControl.getInstance(context);
	}

	private Object askAccount() {

		FrontendDoc doc = new FrontendDoc(context, "Your account");
		doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
		doc.addTrail(CheckoutHandler.class, null, "Your account");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("form").setCaption("I already have an account"));
				sb.append(new FormTag(LoginHandler.class, "login", "post"));
				sb.append(new HiddenTag("linkback", new Link(CheckoutHandler.class)));

				sb.append("<tr><td colspan='2'>If you already have an account then you can checkout more quickly using details from your previous orders. "
						+ "Please enter your email and password to login.</td></tr>");
				sb.append(new FormRow("Email", null, new TextTag(context, "email", 30)));
				sb.append(new FormRow("Password", null, new PasswordTag(context, "password", 16)));
                sb.append(new FormRow(null, null, new SubmitTag("Login")));
                sb.append(new FormRow(null, null, new ForgottenPasswordMarker().generate(context, new HashMap<String, String>())));

				sb.append("</form>");
				sb.append("</table>");

				sb.append(new TableTag("form").setCaption("I want to register for an account"));
				sb.append("<tr><td>If you do not have an account then it only takes a minute to register.<br/>"
						+ "Please click the 'Register now' button to begin.</td></tr>");
				sb.append("<tr><td>" + new ButtonTag(RegistrationHandler.class, null, "Register now", "linkback", new Link(CheckoutHandler.class)) +
						"</td></tr>");
				sb.append("</table>");

				sb.append(new TableTag("form").setCaption("I do not want to register"));
				sb.append("<tr><td>If you want to shop without registering for an account then you can do this.<br/>"
						+ "Just click 'Continue checkout' button below.</td></tr>");
				sb.append("<tr><td>" + new ButtonTag(CheckoutHandler.class, "setGuest", "Continue checkout") + "</td></tr>");
				sb.append("</table>");
				return sb.toString();
			}

		});
		return doc;
	}

	private Object askAttributes() {

		basket.setOfferedAttributes(true);
		basket.save();

		FrontendDoc doc = new FrontendDoc(context, "Checkout - details");
		doc.addTrail(CheckoutHandler.class, null, "Checkout - details");
		doc.addHead("<meta name='robots' content='noindex' />");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(CheckoutHandler.class, "setAttributes", "POST"));
				sb.append(new HiddenTag("page", page));

				/*
				 * Get attributes for this page and render
				 */
				final List<Attribute> attributes = orderSettings.getCheckoutAttributes();
				AFormSectionsInputPanel panel = new AFormSectionsInputPanel(context, basket, attributes);
				panel.setDefaultSection("Extra information");

				sb.append(panel);

				sb.append("<div class='ec_form_commands'>" + new SubmitTag("Continue") + "</div>");

				sb.append("</form>");

				return sb.toString();
			}
		});
		return doc;
	}

	private Object askBillingAddress() {

		final String title = "Checkout - Billing address";
		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
		doc.addTrail(CheckoutHandler.class, null, title, "editBillingAddress", true);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new MessagesTag(context));

				sb.append(new FormTag(CheckoutHandler.class, "setBillingAddress", "POST"));

				sb.append(new FormFieldSet("Billing address"));
				sb.append("<div class='text'>" + shoppingSettings.getBillingAddressHeader() + "</div>");

//				RadioTag sameAddressRadio = new RadioTag(context, "addressId", "-1", !basket.hasBillingAddress());
                SimpleRadioTag sameAddressRadio = new SimpleRadioTag(context, "addressId", "-1", true);
                sameAddressRadio.setId("sameAddressRadio");
				sameAddressRadio.setOnClick("document.getElementById('" + AddressEntryPanel.ID + "').style.display = 'none';");

				sb.append("<div class='text'>" + sameAddressRadio + " My billing address and delivery address are the same</div>");

//				AddressesPanel addressBookPanel = new AddressesPanel(context, account, basket.getBillingAddress());
                AddressesPanel addressBookPanel = new AddressesPanel(context, account, new Address(context));
                addressBookPanel.setShowCreate(true);
				sb.append(addressBookPanel);

				sb.append("</fieldset>");

				if (Address.isCreate(account)) {

					AddressFields fields = AddressFields.getInstance(context, AddressFields.Type.Billing);

					AddressEntryPanel panel = new AddressEntryPanel(context, fields);
					panel.setCountries(shoppingSettings.isShowCountries());
					panel.setAddress(basket.getBillingAddress());

					FormFieldSet set = new FormFieldSet("Create new address").setId(AddressEntryPanel.ID);
                    set.setStyle("display: none;");

					sb.append(set);
					sb.append(panel);
					sb.append("</fieldset>");

				}

				sb.append("<div style='text-align: center; padding-top: 10px;'>" + new SubmitTag("Continue") + "</div>");

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	private Object askCardDetails() throws ServletException {

		String title = "Checkout - Card details";
		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
		doc.addTrail(CheckoutHandler.class, null, title, "editCard", true);
		doc.addBody(new MessagesTag(context));
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(CheckoutHandler.class, "createCard", "POST"));
				sb.append(new TableTag("form", "center").setCaption("Enter card details"));

				sb.append("<tr><td class='info' colspan='2'>Enter the name exactly as it appears on the card, eg. Mrs Jane B Smith</td></tr>");

				sb.append("<tr><td class='key' align='right'>Card Holder</td><td class='value'>" + new TextTag(context, "cardHolder", 20) +
						new ErrorTag(context, "cardHolder", "<br/>") + "</td></tr>");

				SelectTag tag = new SelectTag(context, "cardType");
				tag.addOptions(CardType.get());

				sb.append("<tr><td class='key' align='right'>Card Type</td><td class='value'>" + tag + new ErrorTag(context, "cardType", "<br/>") +
						"</td></tr>");

				sb.append("<tr><td class='info' colspan='2'>Enter the long card number without any spaces.</td></tr>");

				sb.append("<tr><td class='key' align='right'>Card Number</td><td class='value'>" + new TextTag(context, "cardNumber", 25) +
						new ErrorTag(context, "cardNumber", "<br/>") + "</td></tr>");

				SelectTag month = new SelectTag(context, "startMonth");
				month.setAny("--");
				month.addOptions(Card.months);

				SelectTag year = new SelectTag(context, "startYear");
				year.setAny("--");
				year.addOptions(Card.startYears);

				sb.append("<tr><td class='key' align='right'>Start date</td><td class='value'>" + month + " " + year + " " +
						new ErrorTag(context, "startDate", "<br/>") + "</td></tr>");

				month = new SelectTag(context, "expiryMonth");
				month.addOptions(Card.months);

				year = new SelectTag(context, "expiryYear");
				year.addOptions(Card.expiryYears);

				sb.append("<tr><td class='key' align='right'>Expiry date</td><td class='value'>" + month + " " + year + " " +
						new ErrorTag(context, "expiryDate", "<br/>") + "</td></tr>");

				sb.append("<tr><th colspan='2'>" + "Enter the issue number exactly as it appears on the card. "
						+ "If your card does not have an issue number leave this field blank.</th></tr>");

				sb.append("<tr><td class='key' align='right'>Issue number</td><td class='value'>" + new TextTag(context, "issueNumber", 3) +
						new ErrorTag(context, "issueNumber", "<br/>") + "</td></tr>");

				sb.append("<tr><th colspan='2'>"
						+ "Enter the card security code which is the last 3 digits printed on the reverse of the card.</th></tr>");

				sb.append("<tr><td class='key' align='right'>Card security code</td><td class='value'>" + new TextTag(context, "csc", 5) +
						new ErrorTag(context, "csc", "<br/>") + "</td></tr>");

				sb.append("<tr><td class='command' align='center' colspan='2'>" + new SubmitTag("Continue with order").setClass("checkout_button") +
						"</td></tr>");

				sb.append("</table>");
				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	/**
	 * 
	 */
	private Object askCollectionDetails() {

		final List<Branch> branches = Branch.get(context);
		final String title = "Collection details";

		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
		doc.addTrail(CheckoutHandler.class, null, title);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(CheckoutHandler.class, "setCollectionDetails", "post"));

				if (!basket.hasAccount()) {

					sb.append(new FormFieldSet("Your details"));

					sb.append("<div class='text'>Enter the contact details of the person collecting the order in case we need to contact you.</div>");

					sb.append(new FieldInputCell("Name", "Enter the full name of the person collecting this order.", new TextTag(context,
							"accountName", basket.getName(), 30), new ErrorTag(context, "accountName", " ")));

					sb
							.append(new FieldInputCell(
									"Email",
									"Enter a valid email address so we can send you the reservation confirmation for you to print and use as a reference when collecting your order.",
									new TextTag(context, "accountEmail", basket.getEmail(), 30), new ErrorTag(context, "accountEmail", " ")));

                    sb.append(new FieldInputCell("Telephone", "Enter a valid telephone number so we can contact you regarding the reservation",
                            new TextTag(context, "telephone1", basket.getTelephone1(), 20), new ErrorTag(
					context, "telephone1", " ")));

					sb.append("</fieldset>");
				}

				if (branches.size() > 0) {

					if (config.getUrl().contains("blacknround")) {
						sb.append(new FormFieldSet("Choose branch"));
					} else {
						sb.append(new FormFieldSet("Choose " + captions.getBranchCaption().toLowerCase()));
					}

					sb.append("<div class='text'>" + shoppingSettings.getBranchSelectionInstruction() + "</div>");

					if (shoppingSettings.isBranchSearch()) {

						sb.append(new FieldInputCell("Enter postcode or town/city", "Enter your location to find the nearest branches.",
								new TextTag(context, "location", 20), new ErrorTag(context, "location", " ")));

						if (location != null) {

							BranchSearcher searcher = new BranchSearcher(context);
							searcher.setLimit(10);

							LocationUtil.setLocation(context, searcher, location);
							List<Branch> branches = searcher.execute();

							SelectTag branchTag = new SelectTag(context, "branch", basket.getBranch());
							branchTag.setAny("-Choose from 10 nearest branches-");
							for (Branch branch : branches) {

								branchTag.addOption(branch, branch.getName() + " " + LocationUtil.getDistanceString(searcher, branch) +
										" miles");
							}

							sb.append(new FieldInputCell("Choose " + captions.getBranchCaption().toLowerCase(), null, branchTag));
						}

					} else {

						sb.append(new FieldInputCell("Choose " + captions.getBranchCaption().toLowerCase(), null, new SelectTag(context, "branch",
								basket.getBranch(), branches)));

					}

					sb.append("</fieldset>");
				}

				sb.append("<div class='ec_form_commands'>" + new SubmitTag("Click here to continue") + "</div>");

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	/**
	 * Ask for the credit group for this product
	 */
	private Object askCreditGroup() {

		String title = "Checkout - Choose credit rate";
		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
		doc.addTrail(CheckoutHandler.class, null, title, "editCreditGroup", true);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(CheckoutHandler.class, "setCreditGroup", "POST"));
				sb.append(new TableTag());

				for (CreditGroup creditGroup : CreditGroup.get(context)) {

					sb.append("<tr>");
					sb.append("<td>" + new RadioTag(context, "creditGroup", creditGroup) + "</td>");
					sb.append("<td>" + creditGroup.getDescription() + "</td>");
					sb.append("</tr>");
				}
				sb.append("</table>");

				sb.append("<div style='text-align: center; padding-top: 10px;'>" + new SubmitTag("Continue") + "</div>");

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	/**
	 * Ask details includes
	 * 
	 * name / email for non account shopping
	 * delivery address
	 * billing address
	 * attribute options
	 * 
	 */
	private Object askDeliveryAddress() {

		final String title = "Checkout - Delivery address";
		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
		doc.addTrail(CheckoutHandler.class, null, title, "editDeliveryAddress", true);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new MessagesTag(context));
				sb.append(new FormTag(CheckoutHandler.class, "setDeliveryAddress", "POST"));

				// ACCOUNT DETAILS
				if (!basket.hasAccount()) {

					sb.append(new TableTag("form").setCaption("Your details"));

					sb.append(new FormRow("Your name", shoppingSettings.getNameInstructions(), new TextTag(context, "accountName", basket.getName(),
							20) +
							"" + new ErrorTag(context, "accountName", "<br/>")));

					sb.append(new FormRow("Your email address", shoppingSettings.getEmailInstructions(), new TextTag(context, "accountEmail", basket
							.getEmail(), 40) +
							"" + new ErrorTag(context, "accountEmail", "<br/>")));

					sb.append("</table>");

					if (shoppingSettings.hasReferrers()) {

						sb.append(new FormFieldSet("Referred from"));

						SelectTag tag = new SelectTag(context, "referrer", basket.getReferrer(), shoppingSettings.getReferrers(), "-Please choose-");

						sb.append(new FieldInputCell("How did you hear about our site?",
								"We would be very grateful if you would let us know how you found out about our site.", tag));

						sb.append("</fieldset>");
					}
				}

				// only show the addresses panel if address book is active, and we have addresses
				if (shoppingSettings.isAddressBook() && basket.hasAccount() && account.hasAddresses()) {

					sb.append(new FormFieldSet("Delivery address"));
                    sb.append("<div class='text'>" + shoppingSettings.getDeliveryAddressHeader() + "</div>");

					AddressesPanel addressesPanel = new AddressesPanel(context, account, basket.getDeliveryAddress());
					sb.append(addressesPanel);

					sb.append("</fieldset>");

				}

				if (Address.isCreate(account)) {

					AddressFields fields = AddressFields.getInstance(context, AddressFields.Type.Delivery);

					AddressEntryPanel panel = new AddressEntryPanel(context, fields);
					panel.setAddress(basket.getDeliveryAddress());
					panel.setCountries(shoppingSettings.isShowCountries());

					FormFieldSet set = new FormFieldSet("Enter delivery address");
					set.setId(AddressEntryPanel.ID);
                    if (shoppingSettings.isAddressBook() && basket.hasAccount() && account.hasAddresses()) {
						set.setStyle("display: none;");
					}
					sb.append(set);

					sb.append(panel);

					sb.append("</fieldset>");
				}

				sb.append("<div style='text-align: center; padding-top: 10px;'>" + new SubmitTag("Continue") + "</div>");

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	private Object askDeliveryOption() {

		final Map<String, String> deliveryPrices = basket.getDeliveryPrices();
		if (deliveryPrices.isEmpty()) {
			return new ErrorDoc(context, "There are no delivery options available");
		}

		String title = captions.getCheckoutDeliveryTitle();

		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
		doc.addTrail(CheckoutHandler.class, null, title, "editDeliveryOption", true);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new MessagesTag(context));
				sb.append(new FormTag(CheckoutHandler.class, "setDeliveryOption", "POST"));

				sb.append(new TableTag("form").setCaption("Delivery method"));
				sb.append("<tr><th colspan='2'>Choose your preferred delivery method and then click continue at the bottom</th></tr>");

				for (Map.Entry<String, String> entry : deliveryPrices.entrySet()) {

					String id = entry.getKey();
					String description = entry.getValue();

					sb.append("<tr><td>" + new RadioTag(context, "deliveryOption", id) + "</td>");
					sb.append("<td>" + description + "</td></tr>");
				}

				sb.append("<tr><th colspan='2'>" + new SubmitTag("Use the selected delivery method") + "</th></tr>");

				sb.append("</form>");
				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	private Object askDispatchType() {

		final String title = "Collection or delivery";

		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
		doc.addTrail(CheckoutHandler.class, null, title, "editDispatchType", true);
		doc.addBody(new Body() {

			private void collection() {

				sb.append(new FormTag(CheckoutHandler.class, "setDispatchType", "post"));
				sb.append(new HiddenTag("dispatchType", DispatchType.Collection));

				if (config.getUrl().contains("blacknround")) {

					sb.append(new FormFieldSet("Fully Fitted"));
					sb.append("<div class='text'>If you want your tyres fitted at a station of your choice choose this option.</div>");
					sb.append("<div class='text'>" + new SubmitTag("Click here for fitted tyres") + "</div>");

				} else {

					sb.append(new FormFieldSet("I want to collect this order"));
					sb
							.append("<div class='text'>If you want to collect your order in person, and pay at the time of collection, then choose this option.</div>");
					sb.append("<div class='text'>" + new SubmitTag("Click here to reserve order for collection") + "</div>");
				}

				sb.append("</fieldset>");
				sb.append("</form>");
			}

			private void delivery() {

				sb.append(new FormTag(CheckoutHandler.class, "setDispatchType", "post"));
				sb.append(new HiddenTag("dispatchType", DispatchType.Delivery));

				if (config.getUrl().contains("blacknround")) {

					sb.append(new FormFieldSet("Mail order"));
					sb.append("<div class='text'>If you want your order to be delivered to you, then choose this option.</div>");
					sb.append("<div class='text'>" + new SubmitTag("Click here to continue with checkout") + "</div>");

				} else {

					sb.append(new FormFieldSet("I want my order to be delivered"));
					sb.append("<div class='text'>If you want your order to be delivered to you, then choose this option.</div>");
					sb.append("<div class='text'>" + new SubmitTag("Click here to continue with checkout") + "</div>");
				}

				sb.append("</fieldset>");
				sb.append("</form>");
			}

			@Override
			public String toString() {

				if (shoppingSettings.hasDispatchTypeHeader()) {
					sb.append(shoppingSettings.getDispatchTypeHeader());
				}

				delivery();
				collection();

				return sb.toString();
			}

		});
		return doc;
	}

	private Object askForm(final Form form) {

		logger.fine("[CheckoutHandler=" + context.getSessionId() + "] showing form=" + form);

		final String title = "Checkout " + form.getName();
		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
		doc.addTrail(CheckoutHandler.class, null, title, "editForm", form);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(CheckoutHandler.class, "setSubmission", "multi"));
				sb.append(new HiddenTag("form", form));

				sb.append(form.render());

				sb.append(new SubmitTag("Continue"));
				sb.append("</form>");

				return sb.toString();
			}

		});

		return doc;
	}

	private Object askPaymentType() throws ServletException {

		logger.fine("[CheckoutHandler=" + context.getSessionId() + "] showing payment screen");

		/*
		 * Get all payment types applicable for all groups this member is in
		 */
		final Set<PaymentType> paymentTypes = PaymentSettings.getInstance(context).getAvailablePaymentTypes(basket.getTotalInc());
		if (paymentTypes.size() == 0) {
			return new ErrorDoc(context, "There are no payment methods configured on this site");
		}

		/*
		 * if we only have one payment method then check if we can forward on automatically or if it is a payment form that requires a post we
		 * must still show the payment option.
		 */
		if (paymentTypes.size() == 1) {
			Iterator<PaymentType> iter = paymentTypes.iterator();
			paymentType = iter.next();
			return setPaymentType();
		}

		String title = "Checkout - Payment method";

		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
		doc.addTrail(CheckoutHandler.class, null, title, "editPaymentType", true);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new PaymentTable(context, paymentTypes, CheckoutHandler.class, "setPaymentType"));
				return sb.toString();
			}

		});
		return doc;
	}

	public Object cancelled() throws ServletException {

        Order order = basket.getOrder();
        try {
            if (order != null) {
                if (order.cancel()) {
                    order.sendCancelationEmails();
                }
            }
        } catch (EmailAddressException e) {
            logger.warning("[" + CheckoutHandler.class.getName() + "]" + e.toString());
            addError(e);

        } catch (SmtpServerException e) {
            logger.warning("[" + CheckoutHandler.class.getName() + "]" + e.toString());
            addError(e);
        }

		FrontendDoc doc = new FrontendDoc(context, "Payment cancelled");
		doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
		doc.addTrail(CheckoutHandler.class, "cancelled", "Payment cancelled");
		doc.addBody(shoppingSettings.getCancellationContent());
		return doc;
	}

    public Object failed() throws ServletException {

        FrontendDoc doc = new FrontendDoc(context, "Payment failed");
        doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
        doc.addTrail(CheckoutHandler.class, "failed", "Payment failed", "resultCode", resultCode);
        doc.addBody(new Body() {

            @Override
            public String toString() {
                sb.append("<div>The transaction failed bacause of code: " + "<br/><br/>");
                sb.append("<span style='color: red;'>" + resultCode + ":  " + HsbcCpi.cpiResultsCodeValues.get(Integer.parseInt(resultCode)) + "</span>");
                sb.append("<br/></div>");

                return sb.toString();

            }
        });
        return doc;
    }

    public Object payPalDirectPaymentFailed() throws ServletException {

        FrontendDoc doc = new FrontendDoc(context, "Payment failed");
        doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
        doc.addTrail(CheckoutHandler.class, "failed", "Payment failed", "resultCode", resultCode);
        doc.addBody(new Body() {

            @Override
            public String toString() {
                sb.append("<div>The transaction failed bacause of code: " + "<br/><br/>");
                sb.append("<span style='color: red;'>" + resultCode + ":  " + longMessage + "</span>");
                sb.append("<br/></div>");
                return sb.toString();
            }
        });
        return doc;
    }

    private Form checkForms() {

		List<Form> forms = shoppingSettings.getCheckoutForms();
		if (forms.size() > 0) {

			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] " + forms.size() + " checkout forms detected=" + forms);

			for (Form form : forms) {
				if (basket.getSubmission(form) == null) {
					logger.fine("[CheckoutHandler=" + context.getSessionId() + "] no submission for form=" + form);
					return form;
				}
			}
		}

		return null;
	}

	private Object collectionConfirmation() {

		String title = "Checkout confirmation";

		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
		doc.addTrail(CheckoutHandler.class, null, title);
		doc.addBody(new MessagesTag(context));
		doc.addBody(new CollectionConfirmationPanel(context, basket));
		return doc;
	}

	/**
	 * This screen shows the completed page.
	 * The order for the basket will be stored with the basket so we can retrieve it if we come back again
	 * 
	 */
	public Object completed() throws ServletException {

        final Order order = basket.getOrder();

        if (context.getRequest().getParameterMap().containsKey("token") && order.getPaymentType() == PaymentType.PayPalExpress) {
            String nvpStr = "&TOKEN=" + context.getRequest().getParameter("token");
            PayPalExpress payPalExpress = new PayPalExpress(context);
            HashMap map = payPalExpress.httpCall("GetExpressCheckoutDetails", nvpStr);
            String nvpStrDo = "&CURRENCYCODE=GBP&PAYMENTACTION=Sale&PAYERID=" + map.get("PAYERID") + "&TOKEN=" + map.get("TOKEN") +
                    "&AMT=" + basket.getOrder().getAmountOutstanding();
            HashMap mapDo = payPalExpress.httpCall("DoExpressCheckoutPayment", nvpStrDo);
            order.setPayPalTransactionId(mapDo.get("TRANSACTIONID").toString());
            order.save();
        }

        if (paymentType != null) {
			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] payment callback on payment type=" + paymentType);
			paymentType.inlineCallback(context, getParameters(), getRemoteIp());
		}

        if (order.getPaymentType().equals(PaymentType.PayPalProExpress)){
            PaymentType.PayPalProExpress.inlineCallback(context, getParameters(), getRemoteIp());
        }

		/*
		 * if not localhost then empty basket. I don't want to empty mine because it saves me from putting items 
		 * in it all the time when I'm testing
		 */
		if (!context.isLocalhost()) {
			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] Emptying basket");
			basket.empty();
		}

		if (order == null) {
			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] no order stored with basket, returning to process method");
			return process();
		}

		// empty basket but don't clear the order off
		basket.empty();
        if (order.getPaymentType() == PaymentType.HsbcCpi) {
            String cpiResultsCode = getParameters().get("CpiResultsCode");
            if (cpiResultsCode.equals("1")) {
                return cancelled();

            } else if (!cpiResultsCode.equals("0") && !cpiResultsCode.equals("9")) {
                resultCode = cpiResultsCode;
                return failed();
            }
        }

        if (order.getPaymentType().equals(PaymentType.PayPalDirect)) {
            String payPalDirectResult = getParameters().get("ACK");
            if (payPalDirectResult.equals("Failure")) {
                resultCode = getParameters().get("L_ERRORCODE0");
                longMessage = getParameters().get("L_LONGMESSAGE0");
                return payPalDirectPaymentFailed();
            } else if (payPalDirectResult.equals("Success")) {
                order.setPayPalTransactionId(getParameters().get("TRANSACTIONID"));
                order.save();
            }
        }

        sendEmails(order);

        FrontendDoc doc = new FrontendDoc(context, "Checkout - Order completed");
		doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
		doc.addTrail(CheckoutHandler.class, "completed", "Checkout - Order completed");

        doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("<div class='ec_checkoutconfirmation'>");
				sb.append(shoppingSettings.getConfirmationTextRendered(order));
				sb.append("</div>");

				if (order.getPaymentType() == PaymentType.Cheque) {

					sb.append("<div class='ec_checkoutpaymentinfo'>");
					sb.append("The cheque amount is <b>\243" + order.getTotalInc() + "</b>.<br/>");

					if (paymentSettings.hasChequeePayee()) {
						sb.append("Payable to: <b>" + paymentSettings.getChequePayee() + "</b>.<br/>");
					}

					if (paymentSettings.hasChequeAddress()) {
						sb.append("Post to:<br/>");
						sb.append("<b>" + paymentSettings.getChequeAddressBr() + "</b>");
					}

					sb.append("</div>");

				} else if (order.getPaymentType() == PaymentType.Transfer) {

					sb.append("<div class='ec_checkoutpaymentinfo'>");
					sb.append("The total to transfer is <b>\243" + order.getTotalInc() + "</b>.<br/>");

					if (paymentSettings.hasTransferDetails()) {
						sb.append("Bank account information:<br/>");
						sb.append(paymentSettings.getTransferDetailsBr());
					}

					sb.append("</div>");

				} else if (order.getPaymentType() == PaymentType.Offline) {

					sb.append("<div class='ec_checkoutpaymentinfo'>");
					sb.append(paymentSettings.getOfflineContactMessage());
					sb.append("</div>");

                } else if (order.getPaymentType() == PaymentType.HsbcCpi) {
                    String cpiResultsCode = getParameters().get("CpiResultsCode");
                    if (cpiResultsCode.equals("9")) {
                        sb.append("Payment is under review for approval and you will be contacted directly to confirm.  " +
                                "Sorry for any inconvenience caused");
                    }
                }

				return sb.toString();
			}
		});

		/*
		 * Check for checkout scripts and add to headers if present.
		 * todo: add to Body!
		 */

        doc.addBody(new GoogleAnalyticsEcommerceTrackingPanel(context, order));

		if (shoppingSettings.hasCheckoutScripts()) {
			String scripts = shoppingSettings.getCheckoutScripts();
			scripts = MarkerRenderer.render(context, scripts, order);
			doc.addBody(scripts);
		}

		return doc;
	}

	public Object createCard() throws ServletException {

		test(new LengthValidator(5), "cardHolder");
		test(new LengthValidator(16), "cardNumber");
		test(new RequiredValidator(), "cardType");
		test(new RequiredValidator(), "expiryYear");
		test(new RequiredValidator(), "expiryMonth");
		test(new LengthValidator(3, 3), "csc");

		if (hasErrors()) {
			return askCardDetails();
		}

		String expiryDate = expiryMonth + expiryYear;
		String startDate = null;
		if (startMonth != null && startYear != null) {
			startDate = startMonth + startYear;
		}

		try {

            Card card;
            if (basket.hasAccount()) {
                card = new Card(context, basket.getAccount(), getRemoteIp(), cardType, cardHolder, cardNumber, expiryDate, startDate, issueNumber, csc,
                        cardIssuer);
            }
            else {
                card = new Card(context, Item.getByEmail(context, basket.getEmail()), getRemoteIp(), cardType, cardHolder, cardNumber, expiryDate, startDate, issueNumber, csc,
                        cardIssuer);
            }

            logger.fine("[CheckoutHandler] card created=" + card + ", cardaccount=" + card.getAccount());

			basket.setCard(card);

			// card created, so return to process
			return process();

		} catch (IssueNumberException e) {
			setError("issueNumber", e);

		} catch (ExpiryDateException e) {
			setError("expiryDate", e);

		} catch (ExpiredCardException e) {
			setError("expiryDate", e);

		} catch (CardNumberException e) {
			setError("cardNumber", e);

		} catch (IOException e) {
			addError(e);

		} catch (CardTypeException e) {
			setError("cardType", e);

		} catch (StartDateException e) {
			setError("startDate", e);

		} catch (CardHolderException e) {
			setError("cardHolder", e);

		} catch (CscException e) {
			setError("csc", e);

		} catch (CardException e) {
			e.printStackTrace();
			addError(e);

		} catch (ProcessorException e) {
			e.printStackTrace();
			addError(e);
		}

		return askCardDetails();

	}

	private Object declined() throws ServletException {
		addMessage("Your payment was declined. Please check your payment details and try again");
		return process();
	}

	private Object deliveryConfirmation() {

		String title = "Checkout confirmation";

		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
		doc.addTrail(CheckoutHandler.class, null, title);
		doc.addBody(new MessagesTag(context));
		doc.addBody(new DeliveryConfirmationPanel(context, basket));

		return doc;
	}

	@Override
	public Object main() throws ServletException {
		return process();
	}

	public Object process() throws ServletException {

		setAttribute("pagelink", new Link(CheckoutHandler.class));

		/*
		 * Check shopping module is enabled
		 */
		if (!Module.Shopping.enabled(context)) {
			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] shopping disabled");
			return index();
		}

		// check for null basket, should never be null if shopping is enabled
		if (basket == null) {
			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] basket is null, exiting");
			return new CategoryHandler(context).main();
		}

		/*
		 * If we have an order already then show the order screen
		 */
		if (basket.hasOrder()) {

			if (basket.getOrder().hasDeliveryAddress()) {
				return completed();
			} else {
				return reserved();
			}
		}

		/*
		 * We should check that the basket prices, promotions, etc, is up to date by calling refresh
		 * 
		 * basket.refresh();
		 */

		// check for empty basket
		if (basket.isEmpty() || basket.isZero()) {

			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] basket is empty, returning to basket screen");
			addError("Cannot make an order with an empty basket");
			return new BasketHandler(context).main();
		}

		// check for minimum shopping total
		if (basket.getTotalInc().isLessThan(shoppingSettings.getMinOrderValue())) {

			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] order total is below min spend, minspend=");
			addError("Our site has a minimum order of " + shoppingSettings.getMinOrderValue());
			return new BasketHandler(context).main();
		}

		//form
		if (shoppingSettings.isFormsFirst()) {
			form = checkForms();
			if (form != null) {
				return askForm(form);
			}
		}

		/* 
		 * Check for customer choice on dispatch type, and if so then show dispatch screen
		 */
		if (shoppingSettings.getDispatchType() == DispatchType.Both) {

			/*
			 * If no dispatch type set, then ask for one
			 */
			if (!basket.hasDispatchType() || editDispatchType) {
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] no dispatch type set, showing askDispatchType page");
				return askDispatchType();
			}
		}

		/*
		 * Update account on basket depending on checkout accounts mode
		 */
		logger.fine("[CheckoutHandler=" + context.getSessionId() + "] checkoutAccounts=" + shoppingSettings.getCheckoutAccounts());
		switch (shoppingSettings.getCheckoutAccounts()) {

		case None:
			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] removing account");
			basket.removeAccount();
			break;

		case Optional:

			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] setting account=" + account);
			basket.setAccount(account);

			if (!basket.hasAccount() && !basket.isGuest()) {
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] not logged in, asking if we want account (optional mode)");
				return askAccount();
			}

			break;

		case Required:

			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] setting account=" + account);
			basket.setAccount(account);

			if (account == null) {

				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] not logged in, but account is required, forwarding to login handler");
				return new LoginHandler(context, new Link(CheckoutHandler.class)).main();

			}

			break;
		}

		/*
		 * If dispatch type is set to collection or that is our only option then show collection confirmation if we have branches or need account details
		 */
		if (shoppingSettings.getDispatchType() == DispatchType.Collection || basket.getDispatchType() == DispatchType.Collection) {

			if (editCollectionDetails) {

				logger.fine("[CheckoutHandler] asking to edit collection details");
				return askCollectionDetails();
			}

			if (!basket.hasAccount()) {

				logger.fine("[CheckoutHandler] basket has no account set");

				if (!basket.hasName()) {

					logger.fine("[CheckoutHandler] no name, asking for collection details");
					return askCollectionDetails();
				}

				if (!basket.hasEmail()) {

					logger.fine("[CheckoutHandler] no email, asking for collection details");
					return askCollectionDetails();
				}
			}

			if (reserve) {

				try {

					logger.fine("[CheckoutHandler] reserving order");

					basket.reserve();
					return reserved();

				} catch (OrderStatusException e) {
					e.printStackTrace();
					return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

				} catch (IOException e) {
					e.printStackTrace();
					return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

				} catch (PaymentException e) {
					e.printStackTrace();
					return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

				}

			}

			return collectionConfirmation();
		}

		/*
		 * Ensure we have proper account details if we are not logged in
		 */
		if (!basket.hasAccount()) {

			if (!basket.hasName()) {
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] no name");
				return askDeliveryAddress();
			}

			if (!basket.hasEmail()) {
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] no email address");
				return askDeliveryAddress();
			}
		}

		/*
		 * Show screen asking for account details (if no account set) and 
		 * delivery address, billing address.
		 */
		if (shoppingSettings.isAddresses()) {

			if (!basket.hasDeliveryAddress()) {
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] no delivery address");
				return askDeliveryAddress();
			}

			if (editDeliveryAddress) {
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] asking to edit delivery address");
				return askDeliveryAddress();
			}

			// ask for delivery if not set
			if (Module.Delivery.enabled(context)) {

				if (!basket.hasDeliveryOption()) {
					logger.fine("[CheckoutHandler=" + context.getSessionId() + "] no delivery option but deliveries enabled.");
					return askDeliveryOption();
				}

				if (editDeliveryOption) {
					logger.fine("[CheckoutHandler=" + context.getSessionId() + "] asking to edit delivery option");
					return askDeliveryOption();
				}
			}

			if (shoppingSettings.isSeparateAddresses()) {

				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] using separate addresses");

				if (!basket.hasBillingAddress()) {
					logger.fine("[CheckoutHandler=" + context.getSessionId() + "] no billing address");
					return askBillingAddress();
				}

				if (editBillingAddress) {
					logger.fine("[CheckoutHandler=" + context.getSessionId() + "] asking to edit billing address");
					return askBillingAddress();
				}
			}
		}

		//form
		if (!shoppingSettings.isFormsFirst()) {
			form = checkForms();
			if (form != null) {
				return askForm(form);
			}
		}

		// check for attributes
		List<Attribute> attributes = orderSettings.getCheckoutAttributes();
		if (attributes.size() > 0) {
			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] using checkout attributes=" + attributes);

			if (!basket.isOfferedAttributes()) {
				return askAttributes();
			}

			if (editAttributes) {
				return askAttributes();
			}
		}

		// ensure payment type is current
		if (basket.hasPaymentType()) {

			if (!paymentSettings.getPaymentTypes().contains(basket.getPaymentType())) {
				basket.removePaymentType();
			}
		}

		// if no payment type set, or the payment type currently set has become invalid, then show payment screen
		if (!basket.hasPaymentType()) {
			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] no payment type set");
			return askPaymentType();
		}

		if (editPaymentType) {
			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] requesting to edit payment type");
			return askPaymentType();
		}

		// if the payment type is card terminal and we have no card set then show card entry screen
		if (basket.getPaymentType() == PaymentType.CardTerminal) {

			if (!basket.hasCard()) {
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] payment type requires card, none set");
				return askCardDetails();
			}

			if (editCard) {
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] asking to edit card details");
				return askCardDetails();
			}
		}

		// check we have a credit group for able2buy
		if (basket.getPaymentType() == PaymentType.Able2Buy) {

			if (!basket.hasAble2BuyCreditGroup()) {
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] payment type requires a2b credit group, none set");
				return askCreditGroup();
			}

			if (editCreditGroup) {
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] asking to edit a2b credit group");
				return askCreditGroup();
			}
		}

		if (place) {

            if (IPointsSettings.getInstance(context).isEnabled()) {

                if (ipointsUsername != null) {

                    // if ipoints credentials entered then check they are valid
                    IPointsService service = new IPointsService(context);
                    try {

                        service.checkMember(ipointsUsername, ipointsPassword);

                    } catch (IOException e) {
                        e.printStackTrace();
                        throw new RuntimeException(e);

                    } catch (JDOMException e) {
                        e.printStackTrace();
                        throw new RuntimeException(e);

                    } catch (IPointsConnectionException e) {
                        e.printStackTrace();
                        throw new RuntimeException(e);

                    } catch (IPointsRequestException e) {

                        e.printStackTrace();
                        addError("Invalid account details");
                        return deliveryConfirmation();
                    }
                }
            }

            // check we have agreed to the inline agreement if one exists
			if (shoppingSettings.hasAgreement()) {
				if (shoppingSettings.getAgreement().isInline()) {
					if (!agreed) {
						addError("You must agree to our " + shoppingSettings.getAgreement().getTitle() + " to place the order.");
						return deliveryConfirmation();
					}
				}
			}

			// override vat exemption field if it is turned off
			if (!shoppingSettings.isVatExemptionDeclaration()) {
				vatExempt = false;
				vatExemptText = null;
			}

			/*
			 * update order with vat exempt status
			 */
			basket.setVatable(vatExempt);
			basket.setVatExemptText(vatExemptText);

            if (shoppingSettings.isCustomerReference()) {
                basket.setCustomerReference(customerReference);
            }

            /*
			 * Create the order. We cannot create it using the callback as some are unreliable
			 * and some don't have callbacks !
			 */
			Order order;

			try {

				order = basket.toOrder();
                order.setCustomerReference(customerReference);
                order.setIpointsPassword(ipointsPassword);
                order.setIpointsUsername(ipointsUsername);
                order.save();

            } catch (OrderStatusException e) {
				e.printStackTrace();
				return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

			} catch (IOException e) {
				e.printStackTrace();
				return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

			} catch (PaymentException e) {
				e.printStackTrace();
				addError(e);

				return deliveryConfirmation();
			}

			/*
			 * if we have a form payment type then we must redirect to the form by using javascript to submit a POST. 
			 * After the forms payment screen we will be returned to the completed method.
			 */
			if (order.getPaymentType().isForm()) {

				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] Form payment detected=" + order.getPaymentType());
//                if (order.getPaymentType().getName().equals(PaymentType.GoogleCheckout.getName())) {
                    try {
                        order.sendNotificationEmails();
                    } catch (EmailAddressException e) {
                        e.printStackTrace();
                    } catch (SmtpServerException e) {
                        e.printStackTrace();
                    }
//                }
                if (order.getPaymentType().equals(PaymentType.PayPalProHosted)){
                        return payPalProHosted(context, order);
                }   else
                return new PaymentRedirect(context, order);

				/*
				 * if we have a processor payment type, and it is set to immediate payment, then we should attempt to bill the card now
				 * Failure means we will be returned to the confirmation screen with a declined message
				 */

			}

			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] order placed=" + order);

			// do newsletter options
			if (newsletterOptIns.size() > 0) {

				Subscriber sub = Subscriber.get(context, account == null ? basket.getEmail() : account.getEmail(), true);

				for (Newsletter nl : newsletterOptIns) {
					nl.subscribe(sub);
				}

			}

			return completed();
		}

		return deliveryConfirmation();
	}

    private Object payPalProHosted(RequestContext context, Order order) {
        String title = "Checkout confirmation";

		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
		doc.addTrail(CheckoutHandler.class, null, title);
		doc.addBody(new MessagesTag(context));
		doc.addBody(new PayPalProHostedPanel(context, order));

		return doc;
    }

    public Object removeVoucher() throws ServletException {

		// check basket is not null
		if (basket == null) {
			return main();
		}

		// check we are logged in
//		if (account == null) {
//			return new LoginHandler(context, new Link(CheckoutHandler.class)).main();
//		}

		basket.removeVoucher();
		return process();
	}

	private Object reserved() throws ServletException {

		final Order order = basket.getOrder();
		final String title = "Order reserved for collection";

		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
		doc.addTrail(CheckoutHandler.class, null, title);
		doc.addBody(new Body() {

			@Override
			public String toString() {
				return HtmlHelper.nl2br(shoppingSettings.getReservationTextRendered(order));
			}

		});
		return doc;
	}

	public Object reset() throws ServletException {

		logger.fine("[CheckoutHandler=" + context.getSessionId() + "] reset checkout procedure, referrer=" + context.getReferrer() + ", ip=" +
				context.getRemoteIp() + ", user-agent" + context.getUserAgent());

		// reset basket
		basket.resetCheckout();
		return process();
	}

	@Override
	protected boolean runSecure() {
		return Config.getInstance(context).isSecured();
	}

	public Object setAttributes() throws ServletException {

		/*
		 * Validators for the attributes for our current page
		 */
		AttributeUtil.setErrors(context, orderSettings.getCheckoutAttributes(), attributeValues);

		/*
		 * If we have errors or our places map contains at least one entry then return
		 */
		if (hasErrors()) {
			return askAttributes();
		}

		basket.setAttributeValues(attributeValues);

		return process();
	}

	public Object setBillingAddress() throws ServletException {

		if (!shoppingSettings.isSeparateAddresses()) {
			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] not using separate addresses, returning to process");
			return process();
		}

		AddressFields fields = AddressFields.getInstance(context, AddressFields.Type.Billing);

		// if not same adress for billing then we must check for fields
		if (addressId == 0) {
			Address.addTests(context, country, fields);
		}

		if (hasErrors()) {
			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] immediate errors detected: " + context.getErrors());
			return askBillingAddress();
		}

		if (addressId == -1) {

			Address address = basket.getDeliveryAddress();
			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] setting billing add to same as del=" + address);
			basket.setBillingAddress(address);

			return process();

			// create addresss
		} else if (addressId == 0) {

			try {

				Address address = new Address(context, basket.getAccount(), name, address1, address2, address3, town, county, postcode, country);
				address.setTelephone1(telephone1);
				address.setTelephone2(telephone2);
				address.setTelephone3(telephone3);
                address.setState(state);
				address.save();

				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] billing address created=" + address);

				basket.setBillingAddress(address);

			} catch (PostCodeException e) {
				setError("billingPostcode", e);
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] billing address error: " + e);
				return askBillingAddress();

			} catch (ContactNameException e) {
				setError("billingName", e);
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] billing address error: " + e);
				return askBillingAddress();

			} catch (TownException e) {
				setError("billingTown", e);
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] billing address error: " + e);
				return askBillingAddress();

			} catch (CountryException e) {
				setError("billingCountry", e);
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] billing address error: " + e);
				return askBillingAddress();

			} catch (AddressLineException e) {
				setError("billingAddress1", e);
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] billing address error: " + e);
				return askBillingAddress();
			}

		} else {

			if (shoppingSettings.isAddressBook()) {

				Address address = EntityObject.getInstance(context, Address.class, addressId);
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] asking to use billingAddress=" + address);

				if (address != null && address.isValidFor(account)) {

					logger.fine("[CheckoutHandler=" + context.getSessionId() + "] confirmed we are the owner, setting");
					basket.setBillingAddress(address);

				}
			}
		}

		return process();
	}

	@Override
	protected void setBoxCons(List<BoxContainer> boxcons) {

		for (BoxContainer boxcon : boxcons) {
			boxcon.setCheckout(true);
		}
	}

	public Object setCollectionDetails() throws ServletException {

		if (!basket.hasAccount()) {

			/*
			 * Ensure we have a name and email for the basket
			 */
			test(new RequiredValidator(), "accountName");
			test(new RequiredValidator(), "accountEmail");
			test(new EmailValidator(), "accountEmail");
            test(new RequiredValidator(), "telephone1");

		}

		// if we have set a branch search  then check for location entered
		if (shoppingSettings.isBranchSearch()) {
			test(new RequiredValidator(), "location");
		}

		if (context.hasErrors()) {
			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] collection details errors=" + context.getErrors());
			return askCollectionDetails();
		}

		basket.setName(accountName);
		basket.setEmail(accountEmail);
        basket.setTelephone(telephone1);
		basket.save();

		if (Module.Branches.enabled(context)) {
			if (Branch.get(context).size() > 0) {
				if (branch == null) {
					return askCollectionDetails();
				}
				basket.setBranch(branch);
				basket.save();
			}
		}

		return main();
	}

	public Object setCreditGroup() throws ServletException {

		if (creditGroup == null) {
			return askCreditGroup();
		}

		basket.setAble2BuyCreditGroup(creditGroup);
		basket.save();

		return process();
	}

	//
	//	public Object setVoucher() throws ServletException {
	//
	//		// check basket is not null
	//		if (basket == null)
	//			return main();
	//
	//		test(new RequiredValidator(), "voucherCode");
	//		Voucher voucher = Voucher.getByCode(context, voucherCode);
	//		if (voucher == null)
	//			setError("voucherCode", "Invalid voucher code");
	//
	//		if (hasErrors())
	//			return confirmation();
	//
	//		try {
	//			basket.setVoucher(voucher);
	//			addMessage("Voucher added");
	//		} catch (InvalidVoucherException e) {
	//			addError(e);
	//		}
	//
	//		clearParameters();
	//		return confirmation();
	//	}

	public Object setDeliveryAddress() throws ServletException {

		logger.fine("[CheckoutHandler=" + context.getSessionId() + "] set delivery address");

		// we need name and email if account is null
		if (!basket.hasAccount()) {

			logger.fine("[CheckoutHandler=" + context.getSessionId() +
					"] basket does not have account, so testing for accountName and accountEmail params");

			test(new LengthValidator(6), "accountName");
			test(new LengthValidator(6), "accountEmail");
			test(new EmailValidator(), "accountEmail");
		}

		if (!basket.hasAccount() || shoppingSettings.getCheckoutAccounts() == ShoppingSettings.CheckoutAccounts.None) {

			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] basket does not have account, setting accountName=" + accountName +
					", accountEmail=" + accountEmail);

			// set name and email on the basket
			basket.setName(accountName);
			basket.setEmail(accountEmail);
            basket.setReferrer(referrer);
            basket.save();

		}

		AddressFields fields = AddressFields.getInstance(context, AddressFields.Type.Delivery);

		// add tests for delivery if required
		if (addressId == 0) {

			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] no address param, so testing for creation fields");
			Address.addTests(context, country, fields);
            Address.addTests(context, postcode, basket.getDeliveryOption(), fields);

		}

		if (hasErrors()) {
			logger.fine("[CheckoutHandler=" + context.getSessionId() + "]P immediate errors detected: " + context.getErrors());
			return askDeliveryAddress();
		}

		if (addressId == 0) {
			// create delivery address

			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] attempting to create address");

			try {

				Address newadd = new Address(context, basket.getAccount(), name, address1, address2, address3, town, county, postcode, country);
				newadd.setTelephone1(telephone1);
				newadd.setTelephone2(telephone2);
				newadd.setTelephone3(telephone3);
				newadd.setInstructions(instructions);
                newadd.setState(state);
				newadd.save();

				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] delivery address created=" + newadd);

				// set delivery address on basket
				basket.setDeliveryAddress(newadd);

				if (shoppingSettings.isSingleAddress()) {

					logger.fine("[CheckoutHandler=" + context.getSessionId() + "] using delivery address for billing");
					basket.setBillingAddress(newadd);
				}

			} catch (PostCodeException e) {
				setError("postcode", e);
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] delivery address error: " + e);
				return askDeliveryAddress();

			} catch (ContactNameException e) {
				setError("name", e);
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] delivery address error: " + e);
				return askDeliveryAddress();

			} catch (TownException e) {
				setError("town", e);
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] delivery address error: " + e);
				return askDeliveryAddress();

			} catch (CountryException e) {
				setError("country", e);
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] delivery address error: " + e);
				return askDeliveryAddress();

			} catch (AddressLineException e) {
				setError("address1", e);
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] delivery address error: " + e);
				return askDeliveryAddress();
			}

		} else {
			// if we have selected an address then use that

			if (shoppingSettings.isAddressBook()) {

				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] address book confirmed enabled");

				Address address = EntityObject.getInstance(context, Address.class, addressId);
				logger.fine("[CheckoutHandler=" + context.getSessionId() + "] asking to use address=" + addressId);

				if (address.isValidFor(account)) {

					logger.fine("[CheckoutHandler=" + context.getSessionId() + "] confirmed address is valid");
					basket.setDeliveryAddress(address);

				}
			}
		}

		return process();
	}

	public Object setDeliveryOption() throws ServletException {

		// if no delivery option choosen then dump back to delivery selection
		if (deliveryOption == null) {
			addError("Please choose a delivery method");
			return askDeliveryOption();
		}

		basket.setDeliveryOption(deliveryOption);
		return main();
	}

	public Object setDispatchType() throws ServletException {

		basket.setDispatchType(dispatchType);
		basket.save();

		return main();
	}

	public Object setGuest() throws ServletException {

		basket.setGuest(true);
		return main();
	}

	public Object setPaymentType() throws ServletException {

		basket.setPaymentType(paymentType);
		basket.save();

		return main();
	}

	/**
	 * set submission for form
	 */
	public Object setSubmission() throws ServletException {

		logger.fine("[CheckoutHandler=" + context.getSessionId() + "] set submission");

		if (form == null) {
			logger.fine("[CheckoutHandler=" + context.getSessionId() + "] form is null");
			return main();
		}

		form.checkErrors(data, null);
		if (hasErrors()) {
			return askForm(form);
		}

		form.submit(basket, data, uploads, getRemoteIp(), account);
		return main();
	}

	public Object setVoucher() throws ServletException {

		// check basket is not null
		if (basket == null) {
			return main();
		}

		test(new RequiredValidator(), "voucherCode");
		Voucher voucher = Voucher.getByCode(context, voucherCode);
		if (voucher == null) {
			setError("voucherCode", "Invalid voucher code");
		}

		if (hasErrors()) {
			return deliveryConfirmation();
		}

		try {
			if (voucher != null && voucher.isSpecificItemsVoucher()) {
                boolean addVoucher = false;

                for (Item item : basket.getItems()) {
                    Query q = new Query(context, "select voucher, item from # vi join # v where v.code " +
                            "= ? and vi.item = ? and vi.voucher = v.id");
                    q.setTable(VoucherItem.class);
                    q.setTable(Voucher.class);
                    q.setParameter(voucherCode);
                    q.setParameter(item);

                    if (!q.execute().isEmpty()) {
                        addVoucher = true;
                        break;
                    }

                }

                if (addVoucher) {
                    basket.setVoucherCode(voucherCode);
                    for (BasketLine basketLine : basket.getLines()) {
                        for(Item item : voucher.getItems()) {
                            if (basketLine.getItem().equals(item) && voucher.isGetOneFree() && !voucher.isGetExtraItem()
                                    && (voucher.getStartDate().getTimestamp() > new Date().getTimestamp() && voucher.getEndDate().getTimestamp() < new Date().getTimestamp())) {
                                try {
                                    basket.addItem("", item, 1, null, null);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (AttachmentExistsException e) {
                                    e.printStackTrace();
                                } catch (AttachmentTypeException e) {
                                    e.printStackTrace();
                                }
                            } else if (basketLine.getItem().equals(item) && voucher.isGetOneFree() && voucher.isGetExtraItem()
                                    && (voucher.getStartDate().getTimestamp() > new Date().getTimestamp() && voucher.getEndDate().getTimestamp() < new Date().getTimestamp())) {
                                for (Item extraItem : voucher.getExtraItems()) {
                                    try {
                                        basket.addItem("", extraItem, 1, null, null);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    } catch (AttachmentExistsException e) {
                                        e.printStackTrace();
                                    } catch (AttachmentTypeException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                basket.setVoucherCode(voucherCode);
            }
		} catch (InvalidVoucherException e) {
			addError(e);
		}

		clearParameters();
		return deliveryConfirmation();
	}

	@Override
	protected void stats() {
        new StatsCenter(context).run();
		Visitor.process(context, "Checkout");
	}

    private void sendEmails(Order order) {
        try {

            order.sendNotificationEmails();

        } catch (EmailAddressException e) {
            e.printStackTrace();
            logger.warning(e.toString());

        } catch (SmtpServerException e) {
            e.printStackTrace();
            logger.warning(e.toString());
        }

        try {

            // do third party details
            for (OrderEmail email : OrderEmail.get(context)) {
                if (email.hasBody()) {
                    email.send(order);
                }
            }

        } catch (EmailAddressException e) {
            e.printStackTrace();
            logger.warning(e.toString());

        } catch (SmtpServerException e) {
            e.printStackTrace();
            logger.warning(e.toString());
        }

        ShoppingSettings shoppingSettings = ShoppingSettings.getInstance(context);

        try {

            if (shoppingSettings.isOrderAcknowledgement()) {
                order.sendAcknowledgementEmail();
            }

        } catch (EmailAddressException e1) {
            e1.printStackTrace();
            logger.warning(e1.toString());

        } catch (SmtpServerException e1) {
            e1.printStackTrace();
            logger.warning(e1.toString());

        }
    }

}
