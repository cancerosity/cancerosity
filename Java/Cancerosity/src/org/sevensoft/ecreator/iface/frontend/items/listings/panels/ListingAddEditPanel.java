package org.sevensoft.ecreator.iface.frontend.items.listings.panels;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.frontend.misc.FieldInputCell;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.model.attachments.AttachmentOwner;
import org.sevensoft.ecreator.model.attributes.Attributable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.ecreator.model.attributes.renderers.input.AFormInputPanel;
import org.sevensoft.ecreator.model.attributes.renderers.input.AFormSectionsInputPanel;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.ecom.delivery.DeliveryOption;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;
import org.sevensoft.ecreator.model.items.listings.sessions.ListingSession;
import org.sevensoft.ecreator.model.items.stock.StockModule;
import org.sevensoft.ecreator.model.items.stock.StockModule.StockControl;
import org.sevensoft.ecreator.model.media.images.ImageOwner;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 4 Feb 2007 17:52:17
 *
 */
public class ListingAddEditPanel {

	private static final Logger			logger	= Logger.getLogger("ecreator");

	private StockControl				stockControl;
	private int						stock;
	private RequestContext				context;
	private String					outStockMsg;
	private AttachmentOwner				attachmentOwner;
	private ListingPackage				listingPackage;
	private ItemType					itemType;
	private String					content;
	private ImageOwner				imageOwner;
	private String					name;
	private MultiValueMap<Attribute, String>	attributeValues;
	private List<Attribute>				attributes;
	private Attributable				attributable;
	private List<Category>				categories;
	private boolean					showCategories;
	private Money					price;
	private Item					account;
	private String					email;
	private boolean					editName;
	private Class<? extends Handler>		handler;
	private int						page;
	private int						pages;
	private boolean					editEmail;
	private StockModule				stockModule;
	private Map<DeliveryOption, Money>		deliveryRates;

	public ListingAddEditPanel(RequestContext context, Item item, Class<? extends Handler> handler, int page, int pages) {

		this(context, item.getItemType(), item, item, item, page, pages);
		this.deliveryRates = item.getDelivery().getDeliveryRatesMap();

		this.handler = handler;

		this.outStockMsg = item.getOutStockMsg();
		this.listingPackage = item.getListing().getListingPackage();
		this.content = item.getContent();
		this.stock = item.getOurStock();
		this.name = item.getName();
		this.email = item.getAccount().getEmail();
		this.editEmail = false;
		this.showCategories = false;
		this.price = item.getSellPrice();
		this.editName = listingPackage.isNameChange();

		// filter out non editable attributes
		CollectionsUtil.filter(attributes, new Predicate<Attribute>() {

			public boolean accept(Attribute e) {
				return e.isEditable();
			}
		});

	}

	private ListingAddEditPanel(RequestContext context, ItemType itemType, Attributable a, ImageOwner imageOwner, AttachmentOwner attachmentOwner, int p,
			int ps) {

		this.context = context;
		this.itemType = itemType;
		this.imageOwner = imageOwner;
		this.attachmentOwner = attachmentOwner;
		this.editEmail = true;

		if (ps < 2) {

			this.page = 1;
			this.pages = 1;
			this.attributes = a.getAttributes();

		} else {

			this.pages = ps;

			if (p < 1) {
				this.page = 1;
			} else if (p > pages) {
				this.page = pages;
			} else {
				this.page = p;
			}

			this.attributes = a.getAttributes(page);
		}

		this.attributeValues = a.getAttributeValues();
		this.attributable = a;

		this.account = (Item) context.getAttribute("account");
		this.stockModule = itemType.getStockModule();
		this.stockControl = itemType.getStockModule().getStockControl();

		logger.fine("[ListingDetailsPanel] page=" + page + ", pages=" + pages + ", attributes=" + attributes);
	}

	public ListingAddEditPanel(RequestContext context, ListingSession listingSession, Class<? extends Handler> handler, int page, int pages) {

		this(context, listingSession.getItemType(), listingSession, listingSession, listingSession, page, pages);

		this.handler = handler;

		this.outStockMsg = listingSession.getOutStockMsg();
		this.listingPackage = listingSession.getListingPackage();
		this.content = listingSession.getContent();
		this.stock = listingSession.getStock();
		this.name = listingSession.getName();
		this.categories = listingSession.getCategories();
		this.showCategories = listingPackage.isCategories();
		this.price = listingSession.getPrice();
		this.email = listingSession.getEmail();
		this.editName = true;

		this.attributes = listingSession.getAttributes(page);
	}

	private void account(StringBuilder sb) {

		sb.append(new FormFieldSet("Your contact details"));

		sb.append("<div class='text'>Enter your contact details for this listing. "
				+ "These will not be displayed on the site - they will be used to send you details of how to edit or "
				+ "delete your listing after it has been created.</div>");

		TextTag nameTextTag = new TextTag(context, "accountName", name, 30);
		sb.append(new FieldInputCell("*Your name", "Enter your name.", nameTextTag, new ErrorTag(context, "accountName", null, " ")));

		TextTag emailTextTag = new TextTag(context, "email", email, 30);
		if (!editEmail) {
			emailTextTag.setReadOnly(true);
		}

		sb.append(new FieldInputCell("*Your email address", "Please use a real email address as we need to email you to confirm this listing.<br/>"
				+ "You will also be sent a link via email to let you modify / delete this listing.", emailTextTag, new ErrorTag(context, "email", null,
				" ")));

		sb.append("</fieldset>");
	}

	private void categories(StringBuilder sb) {

		logger.fine("[ListingDetailsPanel] showing categories");

		// get categories applicable to this session
		List<Category> availableCategories = listingPackage.getAvailableCategories();

		// only show cat selector if more than 1 category
		if (availableCategories.size() > 1) {

			SelectTag categoryTag = new SelectTag(context, "categories", categories);
			categoryTag.setSize(8);
			if (listingPackage.getMaxCategories() > 1) {
				categoryTag.setMultiple(true);
			}

			for (Category category : availableCategories) {
				categoryTag.addOption(category, category.getName());
			}

			sb.append(new FieldInputCell("*Category", "", categoryTag , new ErrorTag(context, "category", "<br/>")));
		}

	}

	private void content(StringBuilder sb) {

		sb.append(new FormFieldSet(listingPackage.getContentSectionLabel()));
        sb.append("</span>");

		/*StringBuilder desc = new StringBuilder();
		desc.append("Enter a general description of your " + itemType.getNameLower() + ".<br/>");
		if (listingPackage.hasMaxCharacters()) {
			desc.append(" You are limited to " + listingPackage.getMaxCharacters() +
					" characters. We will tell you if you have gone over this limit.<br/>");
		}
		desc
				.append("HTML code is not allowed in your description except for &lt;b&gt; and &lt;i&gt; tags. Anything else will be stripped out by our editor.");*/

        if (content != null) {
            content = content.replaceAll("<br />", "\r\n");
        }
        TextAreaTag contentTag = new TextAreaTag(context, "content", content, 60, 6);
		contentTag.setId("content");

		ErrorTag errorTag = new ErrorTag(context, "content", "<br/>");

		sb.append(new FieldInputCell(listingPackage.getContentFieldLabel(), listingPackage.getContentFieldDescription(), contentTag, errorTag));

		sb.append("</fieldset>");

	}

	private void deliveryRates(StringBuilder sb) {

		final List<DeliveryOption> options = DeliveryOption.get(context, DeliveryOption.Type.Item);
		if (options.isEmpty()) {
			return;
		}

		if (listingPackage.hasDeliveryRatesHeader()) {
			sb.append(listingPackage.getDeliveryRatesHeader());
		}

		sb.append(new FormFieldSet("Delivery rates"));

		for (DeliveryOption deliveryOption : options) {

			Money value;
			if (deliveryRates != null) {
				value = deliveryRates.get(deliveryOption);
			} else {
				value = null;
			}

			String param = "deliveryRates_" + deliveryOption.getId();
			sb.append(new FieldInputCell(deliveryOption.getName(), null, new TextTag(context, param, value, 12), new ErrorTag(context, param, "<br/>")));
		}

		sb.append("</fieldset>");
	}

	private void meetups(StringBuilder sb) {

		sb.append(new FieldInputCell("Max guests", "Enter the max number of guests to this event, or leave blank for no limit.", new TextTag(context,
				"maxGuests", 6)));

		sb.append(new FieldInputCell("Hide guests",
				"Tick this box if you want the list of who is going to your event to only be viewable by people who are going.", new CheckTag(context,
						"privateGuests", "true", false)));

	}

	private void name(StringBuilder sb) {

		TextTag nameTextTag = new TextTag(context, "name", name, 30);
		if (!editName) {
			nameTextTag.setReadOnly(true);
		}

		sb.append(new FieldInputCell("*" + itemType.getNameCaption(), listingPackage.getNameDescription(), nameTextTag,
				new ErrorTag(context, "name", " ")));
	}

	private void prices(StringBuilder sb) {
		sb.append(new FieldInputCell("*Price", null, new TextTag(context, "price", price, 12)));
	}

	private void stock(StringBuilder sb) {

		switch (stockControl) {

		default:
		case None:
			break;

		case Boolean:

			sb.append(new FieldInputCell("In stock", "Tick this box if the item is in stock", new CheckTag(context, "stock", "1", stock > 0)));

			break;

		case Manual:
		case RealTime:

			sb.append(new FieldInputCell("Stock/Qty", "How many items do you have in stock", new TextTag(context, "stock", stock, 6)));

			if (!stockModule.hasOutStockMsgOverride()) {

				sb.append(new FieldInputCell("Out of stock message", null, new TextTag(context, "outStockMsg", outStockMsg, 20)));
			}

			break;

		case String:

			sb.append(new FieldInputCell("Availability", "Enter the availability of this item", new TextTag(context, "outStockMsg", outStockMsg, 20)));

			break;

		}

	}

	public String toString() {

		StringBuilder sb = new StringBuilder();

		if (page == 1) {

			/*
			 * If we are not logged in (and therefore this is an anoymous listing
			 * we should enter email address and name
			 */
			if (account == null) {
				account(sb);
			}

			sb.append(new FormFieldSet(listingPackage.getItemType().getName() + " details"));

			name(sb);

			if (showCategories) {

				// only show categories if they are not set to be selected first
				if (!listingPackage.isCategoriesFirst()) {
					categories(sb);
				}
			}

			if (listingPackage.isPrices()) {
				prices(sb);
			}

			if (listingPackage.isStock()) {
				stock(sb);
			}

			if (ItemModule.Meetups.enabled(context, listingPackage.getItemType())) {
				meetups(sb);
			}

			/*
			 * Also include any attributes on here that have no section assigned
			 */
			List<Attribute> noSectionAttributes = AttributeUtil.retainSectionAttributes(attributes, null);

			if (noSectionAttributes.size() > 0) {

				AFormInputPanel panel = new AFormInputPanel(context, attributable, noSectionAttributes);
				sb.append(panel);

			}

			sb.append("</fieldset>");
		}

		// strip out any attributes that have no section, as they will have been already rendered
		List<Attribute> sectionAttributes = AttributeUtil.retainSectionAttributes(attributes);

		// render using section renderer
		AFormSectionsInputPanel r = new AFormSectionsInputPanel(context, attributable, sectionAttributes);
		sb.append(r);

		if (page == pages) {

			if (itemType.isContent()) {
                if (listingPackage.isAllowDescription()) {
                    content(sb);
                }
            }

			if (listingPackage.isDeliveryRates()) {
				deliveryRates(sb);
			}

		}

		return sb.toString();
	}
}
