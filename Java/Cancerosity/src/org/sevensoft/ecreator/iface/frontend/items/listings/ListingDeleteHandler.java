package org.sevensoft.ecreator.iface.frontend.items.listings;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 28 Dec 2006 08:14:02
 *
 */
@Path("ldel.do")
public class ListingDeleteHandler extends AdminHandler {

	private Item	item;
	private String	p;

	public ListingDeleteHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (item == null) {
			logger.fine("[ListingValidationHandler] no item supplied");
			return HttpServletResponse.SC_BAD_REQUEST;
		}

		if (p == null) {
			logger.fine("[ListingValidationHandler] no password supplied");
			return HttpServletResponse.SC_BAD_REQUEST;
		}

		Item account = item.getAccount();
		if (account == null) {
			return HttpServletResponse.SC_BAD_REQUEST;
		}

		if (item.getAccount().isPasswordMatch(p)) {

			item.delete();
			FrontendDoc doc = new FrontendDoc(context, "Email address validation error");
			doc.addBody("Listing deleted");
			return doc;

		} else {

			return HttpServletResponse.SC_FORBIDDEN;
		}
	}

}
