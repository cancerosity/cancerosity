package org.sevensoft.ecreator.iface.frontend.items;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.BasketHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.values.AValueRenderer;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 22 Nov 2006 13:58:44
 *
 */
@Path("comparison.do")
public class ComparisonHandler extends FrontendHandler {

	private List<Item>	items;

	public ComparisonHandler(RequestContext context) {
		this(context, null);
	}

	public ComparisonHandler(RequestContext context, List<Item> items) {
		super(context);
		this.items = items;
	}

	@Override
	public Object main() throws ServletException {

		// limit to a max of 4 comparisions

		if (items.isEmpty()) {
			return "No items selected!";
		}

		if (items.size() > 4) {
			items = items.subList(0, 4);
		}

		final ItemType itemType = items.get(0).getItemType();

		FrontendDoc doc = new FrontendDoc(context, "Comparing");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag());

				sb.append("<tr>");
				sb.append("<th></th>");
				for (Item item : items) {

					sb.append("<th>");
					sb.append(new LinkTag(item.getUrl(), item.getName()));
					sb.append("</th>");
				}
				sb.append("</tr>");

				if (Module.Pricing.enabled(context) && ItemModule.Pricing.enabled(context, itemType)) {

					sb.append("<tr>");
					sb.append("<th></th>");

					for (Item item : items) {

						Money price = item.getSellPrice();
						if (price.isPositive()) {
							sb.append("<div class='price'>" + price + "</div>");
						}
					}
					sb.append("</tr>");
				}

				// category

				sb.append("<tr>");
				sb.append("<th></th>");

				for (Item item : items) {

					sb.append("<th>");
					sb.append(item.getCategoryNames());
					sb.append("</th>");
				}
				sb.append("</tr>");

				// attribute values
				for (Attribute attribute : items.get(0).getItemType().getAttributes()) {

					sb.append("<tr>");
					sb.append("<th>" + attribute.getName() + "</th>");
					for (Item item : items) {
						sb.append("<td>" + new AValueRenderer(context, item, attribute, item.getAttributeValues(attribute)) + "</td>");
					}
					sb.append("</tr>");
				}

				// add to basket row

				sb.append("<tr>");
				sb.append("<th></th>");

				for (Item item : items) {
					sb.append("<td>" + new ButtonTag(BasketHandler.class, "add", "Add to basket", "item", item) + "</td>");
				}
				sb.append("</tr>");

				return sb.toString();
			}

		});
		return doc;
	}
}
