package org.sevensoft.ecreator.iface.frontend.items.favourites;

import java.util.List;

import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.values.AValueRenderer;
import org.sevensoft.ecreator.model.items.favourites.Favourite;
import org.sevensoft.ecreator.model.items.favourites.FavouritesGroup;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 22 Jan 2007 07:58:36
 *
 */
public class FavouritesRenderer {

	private final RequestContext	context;
	private final FavouritesGroup	favouritesGroup;
	private final List<Favourite>	favourites;

	public FavouritesRenderer(RequestContext context, FavouritesGroup favouritesGroup, List<Favourite> favourites) {
		this.context = context;
		this.favouritesGroup = favouritesGroup;
		this.favourites = favourites;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(new TableTag("form").setCaption(favouritesGroup.getName()));
		sb.append("<tr><td colspan='3' class='text'>"
				+ new LinkTag(FavouritesHandler.class, null, "Click here for a printable version", "printer", 1, "favouritesGroup", favouritesGroup)
				+ "</td></tr>");

		if (favourites.isEmpty()) {

			sb.append("<tr><th colspan='4' class='text'>Your " + favouritesGroup.getName() + " is empty</th></tr>");

		} else {

			sb.append("<tr>");
			sb.append("<th>Name</th>");
			for (Attribute attribute : favouritesGroup.getAttributes()) {
				sb.append("<th>" + attribute.getName() + "</th>");
			}
			if (favouritesGroup.isRename()) {
				sb.append("<th>Rename</th>");
			}
			sb.append("<th>Remove</th>");
			sb.append("</tr>");

			for (Favourite favourite : favourites) {

				sb.append("<tr>");
				sb.append("<td>" + new LinkTag(favourite.getItem().getUrl(), favourite.getName()) + "</td>");

				for (Attribute attribute : favouritesGroup.getAttributes()) {
					sb.append("<td>"
							+ new AValueRenderer(context, favourite.getItem(), attribute, favourite.getItem().getAttributeValues(attribute))
							+ "</td>");
				}

				LinkTag rename = new LinkTag(FavouritesHandler.class, "rename", "Rename", "favourite", favourite);
				rename.setPrompt("Enter the new name for " + favourite.getName(), "name");

				if (favouritesGroup.isRename()) {
					sb.append("<td>" + rename + "</td>");
				}

				sb.append("<td>" + new LinkTag(FavouritesHandler.class, "remove", "Remove", "favourite", favourite) + "</td>");
				sb.append("</tr>");
			}

		}

		sb.append("</table>");

		return sb.toString();
	}

}
