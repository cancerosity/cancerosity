package org.sevensoft.ecreator.iface.frontend.search;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionStatusHandler;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 25-May-2004 15:30:26
 */
@Path("my-searches.do")
public class SavedSearchesHandler extends SubscriptionStatusHandler {

	private Search	search;

	public SavedSearchesHandler(RequestContext context) {
		super(context);
	}

	public Object delete() throws ServletException {

		if (search == null) {
			return main();
		}
		if (account == null) {
			return main();
		}

		account.removeSearch(search);

		clearParameters();
		addMessage("Search deleted.");
		return main();
	}

	@Override
	public Object main() throws ServletException {

		if (!Module.SavedSearches.enabled(context)) {
			logger.fine("[SavedSearches disabled, exiting");
			return index();
		}

		if (account == null) {
			return new LoginHandler(context, new Link(SavedSearchesHandler.class)).main();
		}

		final String title = captions.getMySavedSearchesCaption();
		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(SavedSearchesHandler.class, null, title);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new MessagesTag(context));

				sb.append(new TableTag("form").setCaption("Your saved searches"));

				sb.append("<tr>");
				sb.append("<th>Description</th>");
				sb.append("<th>Search now</th>");
				sb.append("<th>Delete</th>");
				sb.append("</tr>");

				List<Search> searches = account.getSearches();
				if (searches.isEmpty()) {

					sb.append("<tr><td colspan='3'>You have no saved searches.</td></tr>");

				} else {

					for (Search search : searches) {

						sb.append("<tr>");
						sb.append("<td>" + search.getString() + "</td>");
						sb.append("<td>" + new LinkTag(search.getLink(), "Search") + "</td>");
						sb.append("<td>"
								+ new LinkTag(SavedSearchesHandler.class, "delete", "Delete", "search", search)
										.setConfirmation("Are you sure you want to delete this search?") + "</td>");
						sb.append("</tr>");
					}
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;

	}
}
