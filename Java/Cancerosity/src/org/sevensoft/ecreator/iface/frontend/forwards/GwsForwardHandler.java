package org.sevensoft.ecreator.iface.frontend.forwards;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 17 Oct 2006 09:42:54
 *
 */
@Path( { "content.php", "products.php" })
public class GwsForwardHandler extends Handler {

	private int	categoryId;
	private int	productId;

	public GwsForwardHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		if (categoryId > 0) {

			Category category = EntityObject.getInstance(context, Category.class, categoryId);
			if (category == null)
				return HttpServletResponse.SC_NOT_FOUND;

			context.getResponse().setHeader("Location", category.getUrl());
			return HttpServletResponse.SC_MOVED_PERMANENTLY;

		}

		if (productId > 0) {

			Item item = EntityObject.getInstance(context, Item.class, productId);
			if (item == null)
				return HttpServletResponse.SC_NOT_FOUND;

			context.getResponse().setHeader("Location", item.getUrl());
			return HttpServletResponse.SC_MOVED_PERMANENTLY;
		}

		Category category = Category.getRoot(context);
		context.getResponse().setHeader("Location", category.getUrl());
		return HttpServletResponse.SC_MOVED_PERMANENTLY;
	}

	@Override
	protected boolean runSecure() {
		return false;
	}

}
