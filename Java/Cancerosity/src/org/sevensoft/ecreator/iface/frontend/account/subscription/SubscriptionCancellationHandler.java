package org.sevensoft.ecreator.iface.frontend.account.subscription;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.model.accounts.subscriptions.Subscription;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 11 May 2006 14:25:36
 *
 */
@Path("subscriptions-cancel.do")
public class SubscriptionCancellationHandler extends FrontendHandler {

	private boolean	confirm;

	public SubscriptionCancellationHandler(RequestContext context) {
		super(context);
	}

	public Object cancelled() throws ServletException {

		if (!Module.Subscriptions.enabled(context)) {
			logger.fine("[SubscriptionCancellationHandler] Subscriptions not enabled, exiting");
			return index();
		}

		final Subscription sub = account.getSubscription();

		FrontendDoc doc = new FrontendDoc(context, "Subscription cancelled");
		if (account != null) {
			if (account.getItemType().getAccountModule().isShowAccount()) {
				doc.addTrail(AccountHandler.class, captions.getAccountCaption());
			}
		}
		doc.addTrail(SubscriptionCancellationHandler.class, "cancelled", "Subscription cancelled");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				if (sub.hasSubscriptionExpiryDate()) {
					sb.append("Your subscription will be cancelled once it expires on " + sub.getSubscriptionExpiryDate().toString("dd MMM yy")
							+ ".<br/><br/>");

				} else {

					sb.append("Your subscription has been cancelled with immediate effect.<br/><br/>");
					sb.append("If you have changed your mind and want to subscribe again, please "
							+ new LinkTag(SubscriptionHandler.class, null, "click here") + " to view our subscription rates.");

				}

				return sb.toString();
			}

		});
		return doc;
	}

	private Object confirmation() {

		final Subscription sub = account.getSubscription();

		FrontendDoc doc = new FrontendDoc(context, "Subscription cancellation");
		if (account.getItemType().getAccountModule().isShowAccount()) {
			doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		}
		doc.addTrail(SubscriptionCancellationHandler.class, null, "Subscription cancellation");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				if (sub.isLifetimeSubscription()) {

					sb.append("You are subscribed as a lifetime member. "
							+ "This means your subscription will not expire and that you will not have to make another payment."
							+ "<br/><br/>If you still want to cancel your subscription, then "
							+ new LinkTag(SubscriptionCancellationHandler.class, null, "click here", "confirm", true) + ".");

				} else {

					sb.append("We're sorry to see you want to cancel your subscription.<br/><br/>If you are sure you won't stay, please "
							+ new LinkTag(SubscriptionCancellationHandler.class, null, "click here", "confirm", true) + " to cancel.");

				}

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		if (!Module.Subscriptions.enabled(context)) {
			logger.fine("[SubscriptionCancellationHandler] Subscriptions not enabled, exiting");
			return index();
		}

		if (account == null) {
			logger.fine("[SubscriptionCancellationHandler] not logged in, exiting");
			return new LoginHandler(context, new Link(SubscriptionCancellationHandler.class)).main();
		}

		Subscription sub = account.getSubscription();

		if (!sub.hasSubscriptionLevel()) {
			return noSubscription();
		}

		if (confirm) {

			sub.setCancelSubscription();
			return cancelled();

		} else {

			logger.fine("[SubscriptionCancellationHandler] showing confirmation");
			return confirmation();
		}
	}

	private Object noSubscription() {

		FrontendDoc doc = new FrontendDoc(context, "Subscription cancellation");
		if (account.getItemType().getAccountModule().isShowAccount()) {
			doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		}
		doc.addTrail(SubscriptionCancellationHandler.class, null, "Subscription cancellation");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("You are not currently subscribed to our site.<br/><br/>");
				sb.append("To subscribe, " + new LinkTag(SubscriptionHandler.class, null, "click here") + " to view our subscription rates.");

				return sb.toString();
			}

		});
		return doc;
	}
}
