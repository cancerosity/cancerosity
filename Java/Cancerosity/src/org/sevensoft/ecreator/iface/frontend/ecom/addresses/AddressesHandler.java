package org.sevensoft.ecreator.iface.frontend.ecom.addresses;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.text.PostcodeValidator;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.AddressLineException;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.ContactNameException;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.CountryException;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.PostCodeException;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.TownException;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 18 Jan 2007 16:42:27
 *
 */
@Path( { "addresses.do", "member-addresses.do" })
public class AddressesHandler extends FrontendHandler {

	private Address	address;
	private String	instructions;
	private String	telephone1, telephone2, telephone3;
	private Country	country;
	private String	address3;
	private String	address2;
	private String	address1;
	private String	name;
	private String	town;
	private String	county;
	private String	postcode;

	public AddressesHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		if (null == account) {
			return main();
		}

		if (account.isBuyer()) {
			return new AccountHandler(context).main();
		}

		FrontendDoc doc = new FrontendDoc(context, "Your address book");
		doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		doc.addTrail(AddressesHandler.class, "Your address book");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(AddressesHandler.class, "doCreate", "post"));

				sb.append(new TableTag("form").setCaption("New address details"));
				sb.append("</table>");

				sb.append("<div>" + new SubmitTag("Create address") + "</div>");

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object delete() throws ServletException {

		if (null == account) {
			return main();
		}

		if (address == null) {
			return main();
		}

		if (!address.getAccount().equals(account)) {
			return main();
		}

		account.removeAddress(address);
		addMessage("The address has been deleted");
		return main();
	}

	public Object doCreate() throws ServletException {

		// we can only view addresses if we are logged in as a member but not a buyer
		if (null == account) {
			return index();
		}

		if (account.isBuyer()) {
			return new AccountHandler(context).main();
		}

		Address.addTests(context, country, null);

		test(new RequiredValidator(), "name");
		test(new RequiredValidator(), "address1");
		test(new RequiredValidator(), "town");
		test(new RequiredValidator(), "country");

		if (Country.UK.equals(country)) {
			test(new PostcodeValidator(), "postcode");
		}

		if (hasErrors()) {
			return create();
		}

		try {

			account.addAddress(name, address1, address2, address3, town, county, postcode, country, telephone1, telephone2, telephone3, instructions);

			addMessage("Address has been created");
			return main();

		} catch (PostCodeException e) {
			setError("postcode", e);

		} catch (ContactNameException e) {
			setError("name", e);

		} catch (TownException e) {
			//e.printStackTrace();
			setError("town", e);

		} catch (CountryException e) {
			//e.printStackTrace();
			setError("country", e);

		} catch (AddressLineException e) {
			//e.printStackTrace();
			setError("address1", e);
		}

		return create();
	}

	@Override
	public Object main() throws ServletException {

		// we can only view addresses if we are logged in as a member but not a buyer
		if (account == null) {
			return new LoginHandler(context, new Link(AddressesHandler.class)).main();
		}

		if (account.isBuyer()) {
			return new AccountHandler(context).main();
		}

		final List<Address> addresses = account.getAddresses();

		FrontendDoc doc = new FrontendDoc(context, "Your address book");
		doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		doc.addTrail(AddressesHandler.class, "Your address book");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("form").setCaption("Addresses in your address book"));

				if (addresses.isEmpty()) {

					sb.append("<tr><td>You have no addresses in your address book.<br/>");
					sb.append(new LinkTag(AddressesHandler.class, "create", "Click here") + " to add a new address.</td></tr>");

				} else {

					sb.append("<tr>");
					sb.append("<th>Address</th>");
					sb.append("<th>Remove</th>");
					sb.append("</tr>");

					for (Address address : addresses) {

						sb.append("<tr>");

						sb.append("<td>" + address.getLabel(", ", false) + "</td>");
						sb.append("<td>" +
								new LinkTag(AddressesHandler.class, "delete", "Remove", "address", address)
										.setConfirmation("Are you sure you want to delete this address?") + "</td>");

						sb.append("</tr>");
					}

					sb.append("<tr><td colspan='2'>" + new LinkTag(AddressesHandler.class, "create", "Click here") +
							" to add a new address.</td></tr>");
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	protected boolean runSecure() {
		return Config.getInstance(context).isSecured();
	}

}
