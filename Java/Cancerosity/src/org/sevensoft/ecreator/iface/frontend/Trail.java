package org.sevensoft.ecreator.iface.frontend;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 02-Dec-2005 17:26:49
 * 
 */
public class Trail {

	public class View {

		private String	separator;

		private Trail	trail;

		public View(Trail trail, String separator) {
			this.trail = trail;
			this.separator = separator;

		}

		@Override
		public String toString() {
			return trail.toString(separator);
		}
	}

	private Map<String, String>	map;

	public Trail() {
		this.map = new LinkedHashMap<String, String>();
	}

	public void add(String url, String label) {
		map.put(url, label);
	}

	public Map getMap() {
		return map;
	}

	public View getView(String separator) {
		return new View(this, separator);
	}

	public String render(String separator, boolean current, boolean skipindex) {

		StringBuilder sb = new StringBuilder();
		sb.append("<div class='trail'>");
		Iterator<Map.Entry<String, String>> iter = map.entrySet().iterator();
		while (iter.hasNext()) {

			// if this is the last one in the iterator and we don't want to display our current page then break
			if (!iter.hasNext() && !current)
				break;

			Map.Entry<String, String> entry = iter.next();

			String url = entry.getKey();
			if (skipindex && url.equals("."))
				url = new Link(CategoryHandler.class).toString();

			// if last one then add on extra class to a tag

			sb.append("<a href=\"");
			sb.append(url);
			sb.append("\"");

			if (!iter.hasNext())
				sb.append(" class='current'");

			sb.append(">");
			sb.append(entry.getValue());
			sb.append("</a>");

			if (iter.hasNext()) {
				sb.append(" ");
				sb.append(separator);
				sb.append(" ");
			}
		}
		sb.append("</div>");
		return sb.toString();

	}

	public String toString(String separator) {

		StringBuilder sb = new StringBuilder();
		sb.append("<div class='trail'>");
		Iterator<Map.Entry<String, String>> iter = map.entrySet().iterator();
		while (iter.hasNext()) {

			Map.Entry<String, String> entry = iter.next();
			sb.append("<a href=\"");
			sb.append(entry.getKey());
			sb.append("\">");
			sb.append(entry.getValue());
			sb.append("</a>");

			if (iter.hasNext()) {
				sb.append(" ");
				sb.append(separator);
				sb.append(" ");
			}

		}
		sb.append("</div>");
		return sb.toString();
	}
}
