package org.sevensoft.ecreator.iface.frontend.ecom.payments.panels;

import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.able2buy.CreditGroup;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ImageSubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

import java.util.Collection;

/**
 * @author sks 26 Jul 2006 17:18:35
 */
public class PaymentTable {

    private final Collection<PaymentType> paymentTypes;
    private final RequestContext context;
    private final Class<? extends FrontendHandler> handler;
    private final String action;

    public PaymentTable(RequestContext context, Collection<PaymentType> paymentTypes, Class<? extends FrontendHandler> handler, String action) {
        this.context = context;
        this.paymentTypes = paymentTypes;
        this.handler = handler;
        this.action = action;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        //sb.append(ps.getPaymentTableCss());
        sb.append(new PaymentTableCss());

        for (PaymentType paymentType : paymentTypes) {
            if (paymentType == PaymentType.ManualPayments) {
                continue;
            }
            sb.append(new TableTag("payment_choice", null, 3, 0));
            sb.append(new FormTag(handler, action, "get"));
            sb.append(new HiddenTag("paymentType", paymentType));

            sb.append("<tr><th colspan='3'>" + paymentType.getPublicName() + "</th></tr>");
            sb.append("<tr><td class='image' align='center'>");

            if (paymentType.isOnline()) {
                String src = paymentType.getButtonSrc(context);
                if (src == null) {
                    src = "files/graphics/payment/" + paymentType.name().toLowerCase() + ".gif";
                }
                sb.append(new ImageSubmitTag(src));
            }

            sb.append("</td><td class='description'>");
            sb.append(paymentType.getPublicNameImg());
            sb.append("</td><td class='use' align='center'>");

            if (paymentType == PaymentType.Able2Buy) {

                SelectTag creditGroups = new SelectTag(context, "able2BuyCreditGroup");
                creditGroups.addOptions(CreditGroup.get(context));
                sb.append(creditGroups);

            }

            sb.append(new SubmitTag("Select this payment"));
            sb.append("</td>");

            sb.append("</form>");
            sb.append("</table>");
        }

        return sb.toString();
    }

}
