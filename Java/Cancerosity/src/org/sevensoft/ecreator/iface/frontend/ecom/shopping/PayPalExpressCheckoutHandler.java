package org.sevensoft.ecreator.iface.frontend.ecom.shopping;

import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderStatusException;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.PaymentException;
import org.sevensoft.ecreator.model.ecom.payments.PaymentRedirect;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User: Tanya
 * Date: 21.12.2012
 */
@Path("chkpaypalexpress.do")
public class PayPalExpressCheckoutHandler extends FrontendHandler {

    public PayPalExpressCheckoutHandler(RequestContext context) {
        super(context);
    }

    public Object main() throws ServletException {
        return processExpressCheckout();
    }

    public Object processExpressCheckout() throws ServletException {

        /*
		 * Check shopping module is enabled
		 */
        if (!Module.Shopping.enabled(context)) {
            logger.fine("[CheckoutHandler=" + context.getSessionId() + "] shopping disabled");
            return index();
        }

        // check for null basket, should never be null if shopping is enabled
        if (basket == null) {
            logger.fine("[CheckoutHandler=" + context.getSessionId() + "] basket is null, exiting");
            return new CategoryHandler(context).main();
        }

        // check for empty basket
        if (basket.isEmpty() || basket.isZero()) {

            logger.fine("[CheckoutHandler=" + context.getSessionId() + "] basket is empty, returning to basket screen");
            addError("Cannot make an order with an empty basket");
            return new BasketHandler(context).main();
        }

        // check for minimum shopping total
        if (basket.getTotalInc().isLessThan(shoppingSettings.getMinOrderValue())) {

            logger.fine("[CheckoutHandler=" + context.getSessionId() + "] order total is below min spend, minspend=");
            addError("Our site has a minimum order of " + shoppingSettings.getMinOrderValue());
            return new BasketHandler(context).main();
        }

        Order order = null;
        try {

            basket.setPaymentType(PaymentType.PayPalExpress);
            order = basket.toOrder();
//            order.setPaymentType(PaymentType.GoogleCheckout);

        } catch (OrderStatusException e) {
            e.printStackTrace();
            return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

        } catch (IOException e) {
            e.printStackTrace();
            return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

        } catch (PaymentException e) {
            e.printStackTrace();
            addError(e);

//            return deliveryConfirmation();
        }
        return new PaymentRedirect(context, order);
    }

    @Override
    protected boolean runSecure() {
        return Config.getInstance(context).isSecured(); //todo check
    }
}
