package org.sevensoft.ecreator.iface.frontend.forum;

import javax.servlet.ServletException;

import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.RestrictionHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.ecreator.model.crm.forum.Forum;
import org.sevensoft.ecreator.model.crm.forum.Topic;
import org.sevensoft.ecreator.model.crm.forum.renderers.PostsRenderer;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 27-Mar-2006 23:49:37
 *
 */
@Path("topic.do")
public class TopicHandler extends FrontendHandler {

	private Item	item;
	private Topic	topic;
	private String	Link;
	private String	content;
	private String	body;
	private Forum	forum;
	private String	title;

	public TopicHandler(RequestContext context) {
		super(context);
	}

	public Object addPost() throws ServletException {

		if (!Module.Forum.enabled(context)) {
			return index();
		}

		if (account == null) {
			return new LoginHandler(context, new Link(TopicHandler.class, "addPost", "topic", topic, "item", item, "content", content)).main();
		}

		if (item != null) {
			topic = item.getTopic(true);
		}

		if (topic == null) {
			return new CategoryHandler(context).main();
		}

		test(new RequiredValidator(), "content");
		test(new LengthValidator(2), "content");

		if (hasErrors()) {

			if (topic.hasItem()) {
				return new ItemHandler(context, topic.getItem()).main();
			}

			else {
				return main();
			}
		}

		topic.addPost(account, content);

		clearParameters();

		if (topic.hasItem()) {
			return new ItemHandler(context, topic.getItem()).main();
		}

		else {
			return main();
		}
	}

	public Object close() throws ServletException {

		if (!Module.Forum.enabled(context)) {
			return index();
		}

		if (topic == null) {
			return main();
		}

		topic.close();
		if (topic.hasItem()) {
			return new ItemHandler(context, topic.getItem()).main();
		}
		return main();
	}

	public Object composePost() throws ServletException {

		if (!Module.Forum.enabled(context)) {
			return index();
		}

		if (topic == null) {
			return main();
		}

		if (account == null) {
			return new LoginHandler(context, new Link(TopicHandler.class, "composeMessage", "topic", topic)).main();
		}

		FrontendDoc doc = new FrontendDoc(context, "Compose message");
		doc.addTrail(TopicHandler.class, null, topic.getTitle(), "topic", topic);
		doc.addTrail(TopicHandler.class, "composeMessage", "Compose message", "topic", topic);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(TopicHandler.class, "sendMessage", "POST"));
				sb.append(new HiddenTag("topic", topic));

				sb.append(new TableTag("form1").setCaption("Send a message to all contributors in this topic"));
				sb.append("<tr><th colspan='2'>Enter your message details and click 'send message'.</td></tr>");

				sb.append("<tr><td class='key' align='right' valign='top'>Your message</td><td>" + new TextAreaTag(context, "body", 40, 6) +
						new ErrorTag(context, "body", "<br/>") + "</td></tr>");
				sb.append("<tr><td class='key'>&nbsp;</td><td>" + new SubmitTag("Send message") + "</td></tr>");

				sb.append("<tr><th colspan='2'>To return to the topic " + new LinkTag(TopicHandler.class, null, "click here") + "</td></tr>");

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object create() throws ServletException {

		if (!Module.Forum.enabled(context))
			return index();

		if (account == null) {
			return new LoginHandler(context, new Link(TopicHandler.class, "create", "forum", forum)).main();
		}

		if (forum == null) {
			return main();
		}

        if (!PermissionType.PostTopic.check(context, account, forum)) {
            if (account.getItemType().getForum().getRestrictionForwardUrl() != null) {
                return new ExternalRedirect(account.getItemType().getForum().getRestrictionForwardUrl());
            } else {
                return new RestrictionHandler(context).main();
            }
        }

        FrontendDoc page = new FrontendDoc(context, "Post new topic");
		page.addTrail(ForumHandler.class, null, forum.getName(), "forum", forum);
		page.addTrail(TopicHandler.class, "create", "Post new topic", "forum", forum);
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(TopicHandler.class, "doCreate", "POST"));
				sb.append(new HiddenTag("forum", forum));

				sb.append(new TableTag("form1").setCaption("Post a new topic"));
				sb.append("<tr><th colspan='2'>Enter your topic title and initial message and click 'post topic'</td></tr>");

				sb.append("<tr><td class='key' align='right' valign='top'>Title</td><td>" + new TextTag(context, "title", 40) +
						new ErrorTag(context, "title", "<br/>") + "</td></tr>");

				sb.append("<tr><td class='key' align='right' valign='top'>Message</td><td>" + new TextAreaTag(context, "body", 40, 6) +
						new ErrorTag(context, "body", "<br/>") + "</td></tr>");

				sb.append("<tr><td class='key'>&nbsp;</td><td>" + new SubmitTag("Post topic") + "</td></tr>");

				sb.append("<tr><th colspan='2'>To return to the forum " + new LinkTag(ForumHandler.class, null, "click here", "forum", forum) +
						"</td></tr>");

				sb.append("</table>");

				return sb.toString();
			}

		});
		return page;
	}

	public Object doCreate() throws ServletException {

		if (!Module.Forum.enabled(context)) {
			return index();
		}

		if (account == null) {
			return new LoginHandler(context, new Link(TopicHandler.class, "create", "forum", forum)).main();
		}

		if (forum == null) {
			return main();
		}

		test(new RequiredValidator(), "title");
		test(new RequiredValidator(), "body");
		if (hasErrors()) {
			return create();
		}

		topic = forum.addTopic(title, account, body);

		return main();
	}

	@Override
	public Object main() throws ServletException {

        if (!PermissionType.ViewForumContent.check(context, account, forum)) {
            if (account != null && account.getItemType().getForum().getRestrictionForwardUrl() != null) {
                return new ExternalRedirect(account.getItemType().getForum().getRestrictionForwardUrl());
            } else {
                return new RestrictionHandler(context).main();
            }
        }

        if (!Module.Forum.enabled(context)) {
			return index();
		}

		if (topic == null) {
			return new BoardHandler(context).main();
		}

		topic.viewed();

		if (topic.hasItem()) {
			return new ItemHandler(context, topic.getItem()).main();
		}

		FrontendDoc doc = new FrontendDoc(context, "Topic");
		doc.addTrail(ForumHandler.class, null, topic.getForum().getName(), "forum", topic.getForum());
		doc.addTrail(TopicHandler.class, null, topic.getTitle(), "topic", topic);
		doc.addBody(new PostsRenderer(context, topic, 1));
		return doc;
	}

	public Object open() throws ServletException {

		if (topic == null) {
			return main();
		}

		topic.open();
		if (topic.hasItem()) {
			return new ItemHandler(context, topic.getItem()).main();
		}

		return main();
	}

}
