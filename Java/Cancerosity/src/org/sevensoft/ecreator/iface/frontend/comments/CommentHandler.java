package org.sevensoft.ecreator.iface.frontend.comments;

import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.accounts.debug.Login;
import org.sevensoft.ecreator.model.comments.Comment;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.commons.validators.net.EmailValidator;

import javax.servlet.ServletException;

/**
 * User: Tanya
 * Date: 29.07.2010
 */
@Path("comment-add.do")
public class CommentHandler extends FrontendHandler {

    private Item account;
    private String title;
    private String post;
    private Category category;
    private Item item;
    private String linkback;
    private String email;
    private String password;

    public CommentHandler(RequestContext context) {
        super(context);
    }

    public Object main() throws ServletException {
        return add();
    }

    public Object add() throws ServletException {
        if (account == null) {

            addError("Login to post comments.");
            return new LoginHandler(context).main();
        }


        test(new RequiredValidator(), "title");
        test(new RequiredValidator(), "post");

        if (hasErrors()) {
            logger.fine("[CommentHandler] errors=" + context.getErrors());
            setError("title", "Title and Comment Required");
            return new ExternalRedirect(linkback);
        }
        Comment comment = new Comment(context, account);
        comment.setTitle(title);
        comment.setComment(post);
        if (category != null) {
            comment.setCategory(category);
        } else {
            comment.setItem(item);
        }
        comment.save();

        comment.notification();

        if (comment.isAwaitingModeration()) {
            return moderation();
        }
        return new ExternalRedirect(linkback);
    }

    public Object moderation() throws ServletException {
        FrontendDoc doc = new FrontendDoc(context, "Comment pending moderation");
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append("Thank you for post.<br/><br/>");
                sb.append("Your comment is pending moderation.<br/><br/>");
//                sb.append("Your comment is pending moderation - you will be emailed when it has been approved.");
                sb.append("To go back " + new LinkTag(linkback, "click here."));

                return sb.toString();
            }

        });
        return doc;
    }
}
