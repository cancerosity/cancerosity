package org.sevensoft.ecreator.iface.frontend;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.EcreatorHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.model.accounts.sessions.AccountSession;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.renderers.CategoryRenderer;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.containers.boxes.BoxContainer;
import org.sevensoft.ecreator.model.design.template.TemplateSettings;
import org.sevensoft.ecreator.model.design.template.Template;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.ShoppingSettings;
import org.sevensoft.ecreator.model.extras.languages.Language;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.marketing.affiliates.ClickThrough;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.stats.StatsCenter;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHit;
import org.sevensoft.ecreator.model.stats.visits.Visitor;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.TemplateSession;
import org.sevensoft.ecreator.model.system.config.Config.SecureMode;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.docs.Doc;
import org.sevensoft.jeezy.i18n.Messages;

/**
 * @author sks 24-May-2004 16:02:44
 */
public abstract class FrontendHandler extends EcreatorHandler {

	public static final String			AFFILIATE_ID_KEY	= "org.sevensoft.ecreator.aff.id";

	protected transient Company			company;
	protected transient Basket			basket;
	protected transient Category			root;
	protected transient CategoryRenderer	categoryRenderer;

	/**
	 * If we are changing currency
	 */
	private Currency					chgcur;

	/**
	 * Current currency
	 */
	protected transient Currency			currency;

	/**
	 * The active item is the one we are 'logged in' as
	 */
	protected transient Item			account;

	/**
	 * Current logged in member
	 */
	private String					referrer;

	protected transient Language			language;

	private MultiValueMap<Attribute, String>	wizardAttributeValues;

	/**
	 * The current 'active' location which is the location set via a search or a link
	 * etc, and is stored via cookies for this system.
	 */
	protected transient String			activeLocation;

	/**
	 * Affiliate ID
	 */
	@Deprecated
	private String					aff;

	/**
	 * Affiliate id
	 */
	private String					aId;

	private Language					changeLanguage;

	/**
	 * wl is true if we are white labelling the doc
	 */
	protected boolean					wl;

    protected Template  template;

    protected String canonical;

	public FrontendHandler(RequestContext context) {

		super(context);

		this.basket = (Basket) getAttribute("basket");
		this.language = (Language) getAttribute("language");
		this.currency = (Currency) getAttribute("currency");
		this.currencySymbol = (String) getAttribute("currencySymbol");
		this.company = Company.getInstance(context);
		this.account = (Item) context.getAttribute("account");
		this.activeLocation = (String) context.getAttribute("activeLocation");
	}

	private void doAccount() {

		account = AccountSession.getItem(context, context.getSessionId());
		if (account != null) {

			// update last active time
			account.setLastActive();

			// add this active item into the context
			context.setAttribute("account", account);
		}
	}

	private void doActiveLocation() {

		// try to get active location from cookie
		activeLocation = context.getCookieValue("activeLocation");
		context.setAttribute("activeLocation", activeLocation);
	}

	private void doBasket() {

		if (!Module.Shopping.enabled(context)) {
			return;
		}

        if (account != null) {
            basket = Basket.getByAccount(context, account);
            if (basket != null && basket.isEmpty()) {
                basket.resetCheckout();
                basket.delete();
                basket = null;
            }
        }
        if (basket == null) {
            basket = Basket.getBySession(context, context.getSessionId());
        }
        if (account != null && shoppingSettings.getCheckoutAccounts() != ShoppingSettings.CheckoutAccounts.None) {
            basket.setAccount(account);
        }

		if (!wl) {

			basket.setLastAccessTime();
			setAttribute("basket", basket);

		}

		try {

			if (basket.isRecalc()) {
				basket.recalc();
			}

		} catch (RuntimeException e) {
            e.printStackTrace();
			logger.warning(e.toString());
			basket.delete();
			basket = new Basket(context, context.getSessionId());
			setAttribute("basket", basket);
		}

		assert basket != null;

	}

	private void doCurrency() {

		/*
		 * If currencies are enabled then check for currency cookie and retrieve currency.
		 */
		if (Module.Currencies.enabled(context)) {

			if (chgcur == null) {

				String currencyId = context.getCookieValue("currency");
				if (currencyId != null) {
					try {
						currency = EntityObject.getInstance(context, Currency.class, currencyId);
					} catch (RuntimeException e) {

					}
				}

			} else {

				currency = chgcur;

			}
		}

		if (currency == null) {
			currency = Currency.getDefault(context);
		}

		assert currency != null;
		currencySymbol = currency.getSymbol();

		setCookie("currency", currency.getIdString());
		setAttribute("currency", currency);
		setAttribute("currencySymbol", currencySymbol);
	}

	private void doPanels() {

		// no need for panels if wl
		if (wl) {
			return;
		}

		List<Box> boxes = Box.get(context);
		List<BoxContainer> boxContainers = new ArrayList();
		for (String panel : TemplateSettings.getInstance(context).getPanels()) {

			BoxContainer container = new BoxContainer(context, boxes, panel);
			boxContainers.add(container);

			setAttribute("panels_" + panel.toLowerCase(), container);
			setAttribute("boxes_" + panel.toLowerCase(), container);
		}

		setBoxCons(boxContainers);
	}

	protected Object index() throws ServletException {
		return new CategoryHandler(context).main();
	}

	@Override
	protected Object init() throws ServletException {

		Object obj = super.init();
		if (obj != null) {
			return obj;
		}

		// put wl in contxt
		if (wl) {
			context.setAttribute("wl", true);
		}

        // put template in contxt
        if (template != null) {
            context.setAttribute("template", template);
            TemplateSession.get(context, context.getSessionId()).setTemplate(template);
        } else if (context.getReferrer() == null) {
            if (context.containsAttribute("template")) {
                context.removeAttribute("template");
                TemplateSession session = TemplateSession.getExistent(context, context.getSessionId());
                if (session != null) {
                    session.setTemplate(null);
                }
            }
        }

		/*
		 * Check for ip lock out
		 */
		if (isBlacklisted(getRemoteIp())) {
			return HttpServletResponse.SC_FORBIDDEN;
		}

		// check for max session views
		if (miscSettings.getSessionHourLimit() > 0) {

			int count = SiteHit.getHourlyCount(context, context.getRemoteIp());
			if (count > miscSettings.getSessionHourLimit()) {
				return HttpServletResponse.SC_FORBIDDEN;
			}
		}

		// check for affiliate referal for bus club
		if (aId != null) {

			HttpSession session = context.getRequest().getSession();
			session.setAttribute(AFFILIATE_ID_KEY, aId);

		}

		// check for affiliate referral
		if (!wl) {
			if (aff != null) {
				new ClickThrough(context, aff, context.getRequestURL().toString());
			}
		}

		// check if site is set to offline
		if (miscSettings.isOffline() && user == null) {
			return offline();
		}

		context.setAttribute("root", Category.getRoot(context));

		doAccount();
		doActiveLocation();
		doCurrency();
		doBasket();

		doPanels();
		language();
        messagesBundle();

        doCanonical();

		// tags not included in wl
		if (!wl) {

			setAttribute("title_tag", company.getName());
			setAttribute("description_tag", company.getName());
		}

        if (!Module.StatsBlock.enabled(context)) {
            /*
            * If we are logged in as an admin user (ie user == null) then do not show stats - we do not want to distort stats by counting
            * our own views.
            */
            if (user == null || context.isLocalhost()) {
                stats();
            }
        }

		// put wizard attributes into the context
		context.setAttribute("wizardAttributeValues", wizardAttributeValues);

		return null;
	}

	private boolean isBlacklisted(String remoteIp) {

		if (miscSettings.getIpBlacklist().contains(remoteIp)) {
			return true;
		}

		return false;
	}

	private void language() {

		// if not changing language then look for a cookie
		if (changeLanguage == null) {

			String cookieValue = context.getCookieValue("language");
			if (cookieValue != null) {
				logger.fine("language cookieValue=" + language);
				try {
					language = Language.valueOf(cookieValue);
				} catch (RuntimeException e) {
                    e.printStackTrace();
					logger.warning(e.toString());
				}
			}

		} else {

			language = changeLanguage;
		}

		if (language == null) {
			language = Language.English;
		}

		logger.fine("[FrontendHandler] setting language to=" + language);

		setAttribute("language", language);
		setCookie("language", language.name());

		assert language != null;
	}

	private Object offline() {

		return new Doc(context) {

			@Override
			public StringBuilder output() throws ServletException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {

				StringBuilder sb = new StringBuilder();

				sb.append("<html><head>");
				sb.append("<title>" + company.getName() + "</title>");
				sb.append("</head><body>");
				sb.append(miscSettings.getOfflineMessage());
				sb.append("</body></html>");

				return sb;
			}
		};
	}

	@Override
	protected boolean runSecure() {
		return Config.getInstance(context).getSecureMode() == SecureMode.All;
	}

	protected void setBoxCons(List<BoxContainer> boxcons) {
	}

	protected void stats() {

		// do not run stats if we are in wl mode
		if (!wl) {


/*
//todo this code there is in   Visitor.process(context, null);
			String userAgent = context.getUserAgent();

			// ignore user agents that contain bots
			for (String string : Visitor.bots) {
				if (userAgent.contains(string)) {
					logger.fine("[FrontendHandler] user-agent contains ignored agent=" + string);
					return;
				}
			}
*/

			logger.fine("[FrontendHandler] stats");
			new StatsCenter(context).run();
			Visitor.process(context, null);

		}
	}

    private void messagesBundle() {
        Messages.init(company.getCountry().getLanguage(), company.getCountry().getIsoAlpha2());
    }

    private void doCanonical() {
        context.setAttribute("canonical", canonical);
    }
}