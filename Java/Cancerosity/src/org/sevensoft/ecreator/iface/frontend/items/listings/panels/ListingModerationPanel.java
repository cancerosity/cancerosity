package org.sevensoft.ecreator.iface.frontend.items.listings.panels;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 7 Mar 2007 14:02:17
 * 
 * Shows that a listing is waiting to be approved by a moderator
 *
 */
public class ListingModerationPanel extends Panel {

	private final Item	item;

	public ListingModerationPanel(RequestContext context, Item item) {
		super(context);
		this.item = item;
	}

	@Override
	public void makeString() {

		sb.append(new TableTag("ec_listing_moderationpanel"));
		sb.append("<tr><td>This " + item.getItemType().getNameLower()
				+ " is awaiting moderation from our admin team before it will appear on the site.<td></tr>");
		sb.append("</table>");
	}

}
