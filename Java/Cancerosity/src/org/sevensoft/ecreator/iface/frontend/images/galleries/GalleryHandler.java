package org.sevensoft.ecreator.iface.frontend.images.galleries;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SlideshowGif;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.media.images.galleries.*;
import org.sevensoft.ecreator.model.media.images.galleries.search.GallerySearchPanel;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.ImageOpenerTag;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

/**
 * @author sks 6 Feb 2007 12:59:59
 */
@Path("gallery.do")
public class GalleryHandler extends FrontendHandler {

    private Gallery gallery;
    private int page;
    private ImageSortType imageSortType;
    private String path;

    public GalleryHandler(RequestContext context) {
        super(context);
    }

    @Override
    public Object main() throws ServletException {

        if (gallery != null && path != null && !path.equals(gallery.getFriendlyUrl())) {
            return HttpServletResponse.SC_NOT_FOUND;
        }
        
        if (gallery == null) {
            return index();
        }

        Seo seo = Seo.getInstance(context);

        setAttribute("title_tag", seo.getTitleTagRendered(gallery));
        setAttribute("description_tag", seo.getDescriptionTagRendered(gallery));
        setAttribute("keywords", seo.getKeywordsRendered(gallery));

        int count = gallery.getApprovedImageCount();

        imageSortType = gallery.isEnableSortOption() ? imageSortType : ImageSortType.DATE_DESC;
        ImageSortType sortType = imageSortType == null ? gallery.getSortType() : imageSortType;
        Results results = new Results(count, page, gallery.getResultsPerPage());

        FrontendDoc doc = new FrontendDoc(context, gallery.getName());
        if (gallery.isEnableSearchOption())
            doc.append(new GallerySearchPanel(context));
        if (gallery.isEnableSortOption())
            doc.addBody(new GalleriesSortPanel(context, GalleryHandler.class, gallery, sortType));
        doc.addBody(new ResultsControl(context, results, new Link(GalleryHandler.class, null, "gallery", gallery, "imageSortType", sortType)));
        doc.addBody(new GalleryGridRenderer(context, gallery, results, sortType));
        if (gallery.isEnableSlideshow()) {
            ImageOpenerTag slideshow = new ImageOpenerTag(context, new Link(GallerySlideShowHandler.class, null, "gallery", gallery), SlideshowGif.SRC, gallery.getMaxImgWidth() + 50, gallery.getMaxImgHeight() + 50);
            slideshow.setScrollbars(true);
            slideshow.setTarget("_blank");
            doc.append(slideshow);
        }
        return doc;
    }

}
