package org.sevensoft.ecreator.iface.frontend.ecom.shopping.panels;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.payments.forms.Form;
import org.sevensoft.ecreator.model.ecom.payments.forms.FormException;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;

import java.util.Map;
import java.io.IOException;

/**
 * User: Tanya
 * Date: 09.05.2012
 */
public class PayPalProHostedPanel extends Panel {

    private Form form;
    private Map<String, String> params;
    private Payable payable;

    public PayPalProHostedPanel(RequestContext context) {
        super(context);
    }

    public PayPalProHostedPanel(RequestContext context, Order order) {
        super(context);
        payable = order;
        form = order.getPaymentType().getForm(context);
        try {
            params = form.getParams(order);
        } catch (IOException e) {
        } catch (FormException e) {
        }
    }

    public void makeString() {
        sb.append("<iframe frameborder=\"0\" scrolling=\"no\" name=\"hss_iframe\" width=\"570px\" height=\"540px\"></iframe>");

        sb.append("<form style=\"display:none\" target=\"hss_iframe\" name=\"form_iframe\" method=\"" + form.getMethod() + "\"\n" +
                "action=\"" + form.getPaymentUrl() + "\"/>\n");

        for (Map.Entry<String, String> entry : params.entrySet()) {
            sb.append(new HiddenTag(entry.getKey(), entry.getValue()));
        }

        sb.append("</form>\n");
        sb.append("<script type=\"text/javascript\">\n");
        sb.append("document.form_iframe.submit();\n");
        sb.append("</script>");
    }
}
