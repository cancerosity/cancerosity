package org.sevensoft.ecreator.iface.frontend.interaction.winks;

import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.model.interaction.Interaction;
import org.sevensoft.ecreator.model.interaction.InteractionModule;
import org.sevensoft.ecreator.model.interaction.winks.Wink;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Captions;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 5 Dec 2006 16:36:50
 *
 */
final class WinksRenderer extends Body {

	private Item		account;
	private Captions		captions;
	private RequestContext	context;

	public WinksRenderer(Item account, RequestContext context) {
		this.account = account;
		this.context = context;
		this.captions = Captions.getInstance(context);
	}

	@Override
	public String toString() {

		InteractionModule module = account.getItemType().getInteractionModule();

		if (module.hasWinksHeader()) {
			sb.append(module.getWinksHeader());
		}

		sb.append(new TableTag("form").setId("winks").setCaption(captions.getWinksCaptionPlural()));

		Interaction interaction = account.getInteraction();

		if (interaction.hasWinks()) {

			sb.append("<tr>");
			sb.append("<th>When</th>");
			sb.append("<th>Sender</th>");
			sb.append("<th>Sender Last Active</th>");
			sb.append("</tr>");

			for (Wink wink : account.getInteraction().getWinks()) {

				sb.append("<tr>");
				sb.append("<td>" + wink.getDate().toDateTimeString() + "</td>");
				sb.append("<td>" + new LinkTag(wink.getSender().getUrl(), wink.getSender().getDisplayName()) + "</td>");
				sb.append("<td>" + wink.getSender().getLastActive() + "</td>");
				sb.append("</tr>");
			}

		} else {

			sb.append("<tr><td colspan='3'>You have received no " + captions.getWinksCaptionPlural() + "</td></tr>");

		}

		if (account.getItemType().getAccountModule().isShowAccount()) {
			sb.append("<tr><th colspan='3'>To return to your account " + new LinkTag(AccountHandler.class, null, "click here") + "</td></tr>");
		}

		sb.append("</table>");

		if (module.hasWinksFooter()) {
			sb.append(module.getWinksFooter());
		}

		return sb.toString();
	}
}