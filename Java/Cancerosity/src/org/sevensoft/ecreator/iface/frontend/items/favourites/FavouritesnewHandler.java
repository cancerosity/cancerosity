package org.sevensoft.ecreator.iface.frontend.items.favourites;

import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionStatusHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.favourites.Favourite;
import org.sevensoft.ecreator.model.items.favourites.FavouritesSettings;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;
import java.util.List;
import java.util.ArrayList;

/**
 * User: Tanya
 * Date: 23.08.2010
 */
@Path({"favs.do", "favourites.do", "members-favourites.do"})
public class FavouritesnewHandler extends SubscriptionStatusHandler {

    private Item item;
    private Favourite favourite;
    private String name;
    private FavouritesSettings favouritesSettings;

    public FavouritesnewHandler(RequestContext context) {
        super(context);
        this.favouritesSettings = FavouritesSettings.getInstance(context);
    }

    public Object add() throws ServletException {

        if (item == null) {
            logger.fine("[FavouritesnewHandler] no item");
            return main();
        }

        if (account == null) {
            logger.fine("[FavouritesnewHandler] not logged in");
            return new LoginHandler(context, new Link(FavouritesnewHandler.class, "add", "item", item)).main();
        }

        if (Favourite.contains(context, account, item)) {

            addError("This " + item.getItemTypeNameLower() + " is already in your " + captions.getFavouritesCaption());
            return new ItemHandler(context, item).main();
        }

        Favourite favourite = account.addFavourite(item);
        if (name != null) {
            favourite.setName(name);
            favourite.save();
        }

        addMessage("This " + item.getItemTypeNameLower() + " has been added to your " + new LinkTag(FavouritesnewHandler.class, null, captions.getFavouritesCaption()));
        return new ItemHandler(context, item).main();
    }

    @Override
    public Object main() throws ServletException {

        if (account == null) {
            return new LoginHandler(context, new Link(FavouritesnewHandler.class)).main();
        }

        final List<Favourite> favourites = Favourite.get(context, account);

        FrontendDoc doc = new FrontendDoc(context, captions.getFavouritesCaption());
        if (account.getItemType().getAccountModule().isShowAccount()) {
            doc.addTrail(AccountHandler.class, captions.getAccountCaption());
        }
        doc.addTrail(FavouritesnewHandler.class, null, captions.getFavouritesCaption());
        doc.addBody(new Body() {

            @Override
            public String toString() {

                Markup markup = favouritesSettings.getMarkup();

                // the markup requires the item context, not favourite
                List<Item> items = new ArrayList();
                for (Favourite favourite : favourites) {
                    items.add(favourite.getItem());
                }
                MarkupRenderer r = new MarkupRenderer(context, markup);
                r.setBodyObjects(items);
                sb.append(r);

                return sb.toString();
            }

        });
        return doc;
    }

    public Object remove() throws ServletException {

        if (account == null)
            return new LoginHandler(context, new Link(FavouritesnewHandler.class)).main();

        if (favourite != null) {
            account.removeFavourite(favourite);
        } else if (item != null ) {
            Favourite.removeFavourite(context, account, item);
        }

        if (item != null) {
            addMessage("This " + item.getItemTypeNameLower() + " has been removed from your " + new LinkTag(FavouritesnewHandler.class, null, captions.getFavouritesCaption()));
            return new ItemHandler(context, item).main();
        }

        return main();
    }

    public Object rename() throws ServletException {

        if (account == null) {
            return new LoginHandler(context, new Link(FavouritesnewHandler.class)).main();
        }

        if (favourite == null || name == null)
            return main();

        favourite.setName(name);
        favourite.save();

        return main();
    }
}

