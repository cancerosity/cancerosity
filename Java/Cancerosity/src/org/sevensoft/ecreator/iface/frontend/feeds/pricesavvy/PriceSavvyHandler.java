package org.sevensoft.ecreator.iface.frontend.feeds.pricesavvy;

import javax.servlet.ServletException;

import org.jdom.Document;
import org.samspade79.feeds.pricesavvy.PriceSavvyFeed;
import org.samspade79.feeds.pricesavvy.PriceSavvyProduct;
import org.samspade79.feeds.pricesavvy.PriceSavvyXmlBuilder;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.XmlResult;

/**
 * @author Stephen K Samuel samspade79@gmail.com 26 Mar 2009 19:49:14
 */
@Path("pricesavvy.do")
public class PriceSavvyHandler extends Handler {

    private int itemTypeId;

    public PriceSavvyHandler(RequestContext context) {
        super(context);
    }

    @Override
    protected Object init() throws ServletException {
        return null;
    }

    @Override
    public Object main() throws ServletException {

        ItemSearcher s = new ItemSearcher(context);
        s.setItemType(itemTypeId);
        s.setStatus("LIVE");
        s.setImageOnly(true);

        PriceSavvyFeed feed = new PriceSavvyFeed();
        for (Item item : s) {

            PriceSavvyProduct p = new PriceSavvyProduct();
            p.setName(item.getName());
            p.setUrl(item.getUrl());
            p.setCategory(item.getCategoryName());
            p.setReference(item.getIdString());
            p.setPrice(item.getSellPriceInc().getAmount() / 100d);

            p.setDescription(item.getContentStripped());

            Img img = item.getApprovedImage();
            if (img != null) {
                p.setImageUrl(img.getUrl());
            }

            feed.addProduct(p);

        }

        PriceSavvyXmlBuilder b = new PriceSavvyXmlBuilder();
        Document doc = b.build(feed);
        return new XmlResult(doc);
    }

    @Override
    protected boolean runSecure() {
        return false;
    }

}
