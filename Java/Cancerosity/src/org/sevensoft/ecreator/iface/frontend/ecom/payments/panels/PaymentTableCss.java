package org.sevensoft.ecreator.iface.frontend.ecom.payments.panels;

/**
 * @author sks 10 Aug 2006 07:59:40
 *
 */
public class PaymentTableCss {

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("<style>");
		
		sb.append("table.payment_choice { font-family: Tahoma; width: 100%; margin-bottom: 10px; } ");
		sb.append("table.payment_choice td.image { width: 150px; } ");
		sb.append("table.payment_choice td.use { width: 120px; } ");
		sb.append("table.payment_choice caption { } ");
		sb.append("table.payment_choice th { padding: 3px 6px; background: #888888; color: white; text-align: left; font-weight: bold; } ");
		sb.append("table.payment_choice td { border: 1px solid #888888; padding: 8px; }");

		sb.append("</style>");

		return sb.toString();
	}
}
