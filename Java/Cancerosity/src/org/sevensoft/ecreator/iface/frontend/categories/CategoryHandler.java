package org.sevensoft.ecreator.iface.frontend.categories;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.RestrictionHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.forum.BoardHandler;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionsModule;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategorySettings;
import org.sevensoft.ecreator.model.categories.referrer.CategoryReferrerAllowed;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.design.marker.MarkerRenderer;
import org.sevensoft.ecreator.model.items.sorts.ItemSort;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.ecreator.model.stats.StatsCenter;
import org.sevensoft.ecreator.model.stats.visits.Visitor;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.comments.blocks.CommentPostBlock;
import org.sevensoft.ecreator.model.media.images.galleries.blocks.GalleryBlock;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.results.docs.Doc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 09-Feb-2006 13:48:13
 *
 */
@Path( { "c.do", "category.do", "browse.do" })
public class CategoryHandler extends FrontendHandler {

	private final transient CategorySettings	categorySettings;
	private final transient Seo			seo;
	private transient int				limit;

	private Category					category;
	private int						page;

	private ItemSort					sort;
	private String					id;

	/**
	 * Used to look up a category
	 */
	private String					name;

	private MultiValueMap<Attribute, String>	attributeValues;
	private transient Doc				doc;
    private String path;

	public CategoryHandler(RequestContext context) {
		this(context, null, null);
	}

	public CategoryHandler(RequestContext context, Category category) {
		this(context, category, null);
	}

	public CategoryHandler(RequestContext context, Category category, MultiValueMap<Attribute, String> attributeValues) {
		super(context);

		this.category = category;
		this.categorySettings = CategorySettings.getInstance(context);
		this.seo = Seo.getInstance(context);
	}

	public CategoryHandler(RequestContext context, MultiValueMap<Attribute, String> attributeValues) {
		this(context, null, attributeValues);
	}

	private void footer() {

		// show footer if we have one
		if (category.hasFooter()) {

			logger.fine("[CategoryHandler] rendering footer from category");
			doc.addBody(MarkerRenderer.render(context, category.getFooter()));

		} else if (categorySettings.hasFooter()) {

			logger.fine("[CategoryHandler] rendering footer from cat settings");
			doc.addBody(MarkerRenderer.render(context, categorySettings.getFooter()));

		}
	}

	private Link getLink() {

		Link link = new Link(CategoryHandler.class, null, "category", category);
		if (page > 0) {
			link.setParameter("page", page);
		}
		if (sort != null) {
			link.setParameter("sort", sort);
		}
		return link;
	}

	private void header() {

		if (category.hasHeader()) {

			logger.fine("[CategoryHandler] rendering header from category");
			doc.addBody(MarkerRenderer.render(context, category.getHeader()));

		} else if (categorySettings.hasHeader()) {

			logger.fine("[CategoryHandler] rendering header from category settings");
			doc.addBody(MarkerRenderer.render(context, categorySettings.getHeader()));
		}
	}

	@Override
	public Object main() throws ServletException {
        if (category != null && !categorySettings.isShowHomePage()) {
            if (path != null && !path.equals(category.getFriendlyUrl())) {
                return HttpServletResponse.SC_NOT_FOUND;
            }
        }
		if (category == null && name != null) {
			category = Category.getByName(context, name);
            //category with such a name doesn't exist
            if (category == null && !categorySettings.isShowHomePage()) {
                return HttpServletResponse.SC_NOT_FOUND;
            }
		}

		if (category == null) {
            if (!categorySettings.isShowHomePage() && !context.getParameters().containsKey("category")) {
                category = Category.getRoot(context);
            }
            else if (!categorySettings.isShowHomePage() && context.getParameters().containsKey("category")) {
                return HttpServletResponse.SC_NOT_FOUND;
            }
            else if (categorySettings.isShowHomePage()) {
                category = Category.getRoot(context);
            }
        }


		if (Module.Forum.enabled(context)) {
			if (category.isForum()) {

				logger.fine("[CategoryHandler] forum forward enabled");
				return new BoardHandler(context).main();
			}
		}

		/* 
		 * if we have forward then we should forward onto the url
		 */
		if (category.hasForwardUrl()) {

			switch (category.getForwardWho()) {

			case All:
				return new ExternalRedirect(category.getForwardUrl());

			case Guests:
				if (account == null) {
					return new ExternalRedirect(category.getForwardUrl());
				}
				break;

			case Members:
				if (account != null) {
					return new ExternalRedirect(category.getForwardUrl());
				}
				break;
			}
		}

        if (category.isPermissions() && category.getPermissionsModule().hasAllowOnlyReferrer() ||
                category.isPermissions() && category.getPermissionsModule().hasAccountAllowed()) {

            String refferer = context.getReferrer();
            if (CategoryReferrerAllowed.isAllowed(context, category)) {
                //the page will be shown
            } else if (account != null && account.equals(category.getPermissionsModule().getAccountAllowed())) {
                new CategoryReferrerAllowed(context, category);
                //the page will be shown
            } else if (refferer != null && refferer.contains(category.getPermissionsModule().getAllowOnlyReferrer())) {
                if (!category.getPermissionsModule().hasAccountAllowed()) {
                    new CategoryReferrerAllowed(context, category);
                } else {
                    if (account == null) {
                        category.getPermissionsModule().getAccountAllowed().login(context.getSessionId());
                        new CategoryReferrerAllowed(context, category);
                    } else {
                        return redirect();
                    }
                }
            
                //the page will be shown
            } else {
                return redirect();
            }
        } else {
            /*
            * Check we have permission to view this category
            */
            if (category.isPermissions()) {

                if (!PermissionType.CategoryContent.check(context, account, category)) {
                    if (category.getRestrictionForwardUrl() != null) {
                        return new ExternalRedirect(category.getRestrictionForwardUrl());
                    } else {
                        return new RestrictionHandler(context).main();
                    }
                }
            }

            /*
            * Check we have permission to view this category and all its parents
            */
            for (Category trailCategory : category.getTrail()) {

                if (!PermissionType.CategoryContent.check(context, account, trailCategory)) {

                    logger.fine("[CategoryHandler] we are restricted from viewing cat=" + trailCategory);

                    PermissionsModule module = trailCategory.getPermissionsModule();
                    if (account != null || module.isNoGuestLogin()) {

                        logger.fine("[CategoryHandler] we are logged in, or no guest login, showing restriction");
                        if (module.hasRestrictionForward()) {

                            return new ExternalRedirect(module.getRestrictionForward());

                        } else {

                            return new RestrictionHandler(context).main();
                        }

                    } else {

                        logger.fine("[CategoryHandler] redirecting to login");
                        return new LoginHandler(context, new Link(CategoryHandler.class, null, "category", category), category).main();
                    }
                }
            }
        }
		// we should update the context with a reference to this category
		setAttribute("category", category);
		setAttribute("primary", category.getPrimaryParent());

		// update our category sidebar render with the current category so we can have the template update tabs and things based
		// on the current location
		if (categoryRenderer != null) {
			categoryRenderer.setCurrent(category);
		}

		// we must now lock down friendly url if not already done
		category.lockFriendlyUrl();

		// check we have a content block - all categories should have a content block
		category.getContentBlock();

		// set a link back to the page we are on, link is deprecated
		context.setAttribute("link", getLink());

		// pagelink is a link back to this page
		context.setAttribute("pagelink", getLink());

		// get blocks
		List<Block> blocks = category.getBlocks();

        //if user os logged CommentPostBlock should have account        //todo setAccount
        if (Block.contains(CommentPostBlock.class, blocks)) {
            CommentPostBlock block = (CommentPostBlock) Block.get(CommentPostBlock.class, blocks);
            block.setAccount(account);
        }

        if (Block.contains(GalleryBlock.class, blocks)) {
            GalleryBlock block = (GalleryBlock) Block.get(GalleryBlock.class, blocks);
            block.setPage(page);
        }

		doc = new FrontendDoc(context, category.getName());

		// do trail
		if (category.isChild()) {
			for (Category cat : category.getTrail()) {
				if (doc instanceof FrontendDoc) {
					((FrontendDoc) doc).addTrail(cat.getUrl(), cat.getName());
				}
			}
		}

		setAttribute("title_tag", seo.getTitleTagRendered(category));
		setAttribute("description_tag", seo.getDescriptionTagRendered(category));
		setAttribute("keywords", seo.getKeywordsRendered(category));

		/*
		 * If this category has CSS then we want to include it
		 */
		if (category.hasCss()) {

			doc.addBody("<style>");
			doc.addBody(category.getCss());
			doc.addBody("</style>");
		}

		header();

		/*
		 * Render blocks
		 * If items block does not exist then create
		 */

		logger.fine("[CategoryHandler] processing blocks: " + blocks);
		context.setAttribute("attributeValues", attributeValues);
		context.setAttribute("link", getLink());
		context.setAttribute("page", page);
		context.setAttribute("sort", sort);

		for (Block block : blocks) {

			logger.fine("[CategoryHandler] rendering block=" + block + " is visible=" + block.isVisible());
			if (block.isVisible()) {

				if (block.isFirstPageOnly() && page > 1) {
					logger.fine("[CategoryHandler] block only to be shown on first page and this is page " + page + ", skipping");
					continue;
				}

				doc.addBody(block.render(context));
			}
		}



		footer();

		return doc;
	}

    private Object redirect() throws ServletException {
        if (category.getRestrictionForwardUrl() != null) {
            return new ExternalRedirect(category.getRestrictionForwardUrl());
        } else {
            return wrongReferrer();
        }
    }

    public Object wrongReferrer() throws ServletException {
         doc = new FrontendDoc(context, category.getName());

		// do trail
		if (category.isChild()) {
			for (Category cat : category.getTrail()) {
				if (doc instanceof FrontendDoc) {
					((FrontendDoc) doc).addTrail(cat.getUrl(), cat.getName());
				}
			}
		}

		setAttribute("title_tag", seo.getTitleTagRendered(category));
		setAttribute("description_tag", seo.getDescriptionTagRendered(category));
		setAttribute("keywords", seo.getKeywordsRendered(category));

		/*
		 * If this category has CSS then we want to include it
		 */
		if (category.hasCss()) {

			doc.addBody("<style>");
			doc.addBody(category.getCss());
			doc.addBody("</style>");
		}

		header();
        doc.addBody("<div class='referrer'>You are not authorised to view this page.</div>");

		footer();

		return doc;
    }

	@Override
	protected boolean runSecure() {

		Config config = Config.getInstance(context);
		switch (config.getSecureMode()) {

		case All:
			return true;

		default:
		case None:
			return false;

		case AsRequired:
			return category != null && category.hasForm();

		}

	}

	@Override
	protected void stats() {

		logger.fine("[CategoryHandler] calling stats on page=" + category);
		new StatsCenter(context).run(category);

		// if (category is null then we are in root
		if (category == null) {
			Visitor.process(context, ((Category) context.getAttribute("root")).getName());
		} else {
			Visitor.process(context, category.getName());
		}
	}
}
