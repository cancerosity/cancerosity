package org.sevensoft.ecreator.iface.frontend.forum;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.crm.forum.Forum;
import org.sevensoft.ecreator.model.crm.forum.Post;
import org.sevensoft.ecreator.model.crm.forum.PostSearcher;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 15-Feb-2006 22:26:53
 *
 */
@Path("post-search.do")
public class PostSearchHandler extends FrontendHandler {

	private String	keywords;
	private Item	author;
	private Forum	forum;
	private Item	account;

	public PostSearchHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		FrontendDoc doc = new FrontendDoc(context, "Message search");
		doc.addTrail(PostSearchHandler.class, null, "Message search");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new MessagesTag(context));

				sb.append(new FormTag(PostSearchHandler.class, "search", "post"));
				sb.append(new TextTag(context, "keywords", keywords));
				sb.append(new SubmitTag("Search for messages"));

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object member() throws ServletException {

		if (author == null) {
			return main();
		}

		PostSearcher searcher = new PostSearcher(context);
		searcher.setPoster(author);
		searcher.setLimit(25);

		List<Post> posts = searcher.execute();

		return results("Latest messages posted by " + author.getDisplayName(), posts);
	}

	private Object results(String title, final List<Post> messages) throws ServletException {

		if (messages.size() == 0) {
			addMessage("We could not find any posts matching your search");
			return main();
		}

		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(PostSearchHandler.class, null, "Message search");
		doc.addTrail(PostSearchHandler.class, null, title);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("<style>");
				sb.append("table.posts_search { width: 100%; font-size: 12px; } ");
				sb.append("table.posts_search td { padding-bottom: 10px; } ");
				sb.append("table.posts_search div.name { font-weight: bold; } ");
				sb.append("table.posts_search div.name a { text-decoration: underline; color: #222222; } ");
				sb.append("table.posts_search div.name:hover { text-decoration: none; color: #666666; } ");
				sb.append("table.posts_search div.member {color: #666666; } ");
				sb.append("table.posts_search div.content { font-size: 11px; padding: 3px 0; } ");
				sb.append("</style>");

				sb.append(new TableTag("posts_search", "center"));

				for (Post message : messages) {

					sb.append("<tr><td>");

					sb.append("<div class='name'>" +
							new LinkTag(TopicHandler.class, null, message.getTopic().getTitle(), "topic", message.getTopic()) + "</div>");

					sb.append("<div class='member'>Posted by ");
					sb.append(new LinkTag(message.getAccount().getUrl(), message.getAccount().getDisplayName()));
					sb.append(message.getAccount().getName());

					sb.append(" on " + message.getDate().toString("dd/MM/yyyy"));
					sb.append("</div>");

					sb.append("<div class='content'>" + message.getContent(200) + "...</div>");

					sb.append("</td></tr>");
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object search() throws ServletException {

		PostSearcher searcher = new PostSearcher(context);

		searcher.setKeywords(keywords);
		searcher.setSortType(SortType.Newest);
		searcher.setForum(forum);
		searcher.setAccount(account);
		searcher.setLimit(25);

		List<Post> posts = searcher.execute();

		return results("Message search results", posts);
	}

}
