package org.sevensoft.ecreator.iface.frontend.bespoke.ipoints;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletException;

import org.jdom.JDOMException;
import org.samspade79.feeds.ipoints.IPointsRegisterUserRequest;
import org.samspade79.feeds.ipoints.IPointsRegisterUserResponse;
import org.samspade79.feeds.ipoints.exceptions.IPointsConnectionException;
import org.samspade79.feeds.ipoints.exceptions.IPointsRequestException;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.bespoke.ipoints.IPointsSettings;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.validators.DateValidator;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.net.EmailValidator;

/**
 * @author Stephen K Samuel samspade79@gmail.com 16 Apr 2009 16:50:18
 */
@Path("ipoints-register.do")
public class IPointsRegistrationHandler extends FrontendHandler {

    private String postcode;
    private String lastName;
    private String firstName;
    private String email;
    private String city;
    private String dob;
    private String title;
    private boolean agreement;

    public IPointsRegistrationHandler(RequestContext context) {
        super(context);
    }

    private Object error(IPointsRequestException e) {
        FrontendDoc doc = new FrontendDoc(context, "Error");
        doc.addBody("There was an error registering you on ipoints, please go back and try again.\nError: " + e.getMessage());
        return doc;
    }

    public Object register() throws ServletException {

        test(new DateValidator(), "dob");
        test(new EmailValidator(), "email");
        test(new RequiredValidator(), "postcode");
        test(new RequiredValidator(), "lastName");
        test(new RequiredValidator(), "firstName");
        test(new RequiredValidator(), "email");
        test(new RequiredValidator(), "city");
        test(new RequiredValidator(), "dob");
        test(new RequiredValidator(), "agreement");

        if (hasErrors()) {
            return main();
        }

        IPointsSettings is = IPointsSettings.getInstance(context);

        IPointsRegisterUserRequest req = new IPointsRegisterUserRequest();

        req.setEmail(email);
        req.setFirstName(firstName);
        req.setLastName(lastName);
        try {
            req.setDob(new Date(dob).getTimestamp());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        req.setPostcode(postcode);
        req.setPartnerPassword(is.getPassword());
        req.setPartnerUsername(is.getUsername());
        req.setTitle(title);
        req.setCity(city);

        IPointsRegisterUserResponse resp;
        try {

            resp = req.postRequest();
            resp.getMemberId();

            return registered();

        } catch (IOException e) {
            e.printStackTrace();
            return e.toString();

        } catch (JDOMException e) {
            e.printStackTrace();
            return e.toString();

        } catch (IPointsConnectionException e) {
            return e.getMessage();

        } catch (IPointsRequestException e) {
            return error(e);
        }

    }

    /**
     *
     */
    private Object registered() {

        FrontendDoc doc = new FrontendDoc(context, "Registered");
        doc.addBody("Thank you, you are now registered on ipoints.");
        return doc;
    }

    @Override
    public Object main() throws ServletException {

        final IPointsSettings ipointsSettings = IPointsSettings.getInstance(context);

        FrontendDoc doc = new FrontendDoc(context, "Error");
        doc.addBody(new Object() {

            private StringBuilder sb = new StringBuilder();

            @Override
            public String toString() {

                sb.append(new FormTag(IPointsRegistrationHandler.class, "register", "POST"));

                sb.append(new TableTag("ipointsRegister"));

                SelectTag tag = new SelectTag(context, "title");
                tag.addOptions(new String[]{"Mr", "Mrs", "Ms", "Miss"});

                sb.append("<tr>");
                sb.append("<td>Title</td>");
                sb.append("<td>" + tag + "</td>");
                sb.append("</tr>");

                sb.append("<tr>");
                sb.append("<td>First name</td>");
                sb.append("<td>" + new TextTag(context, "firstName") + "</td>");
                sb.append("</tr>");

                sb.append("<tr>");
                sb.append("<td>Last name</td>");
                sb.append("<td>" + new TextTag(context, "lastName") + "</td>");
                sb.append("</tr>");

                sb.append("<tr>");
                sb.append("<td>Date of birth dd/mm/yyyy</td>");
                sb.append("<td>" + new TextTag(context, "dob") + "</td>");
                sb.append("</tr>");

                sb.append("<tr>");
                sb.append("<td>City</td>");
                sb.append("<td>" + new TextTag(context, "city") + "</td>");
                sb.append("</tr>");

                sb.append("<tr>");
                sb.append("<td>Postcode</td>");
                sb.append("<td>" + new TextTag(context, "postcode", 12) + "</td>");
                sb.append("</tr>");

                sb.append("<tr>");
                sb.append("<td>Email address</td>");
                sb.append("<td>" + new TextTag(context, "email") + "</td>");
                sb.append("</tr>");
                sb.append("<tr>");

                sb.append("<tr class='agreement'>");
                sb.append("<td></td>");
                sb.append("<td>I agree to the ipoints " + new LinkTag(ipointsSettings.getTermsUrl(), "terms and conditions") + " "
                        + new CheckTag(context, "agreement", true) + "</td>");
                sb.append("</tr>");
                sb.append("<tr>");

                sb.append(new SubmitTag("Register"));
                sb.append("</form>");

                return sb.toString();
            }
        });
        return doc;
    }

}

