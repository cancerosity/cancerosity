package org.sevensoft.ecreator.iface.frontend.interaction.userplane.recorder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.RestrictionHandler;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.interaction.userplane.recorder.UserplaneRecorderSettings;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.ExternalRedirect;

/**
 * @author sks 26 Nov 2006 14:25:38
 * 
 * This handler will display a page to display the profiles video
 *
 */
@Path("userplane-viewer.do")
public class UserplaneViewerHandler extends FrontendHandler {

	private final transient UserplaneRecorderSettings	settings;
	private Item							profile;

	public UserplaneViewerHandler(RequestContext context) {
		super(context);
		this.settings = UserplaneRecorderSettings.getInstance(context);
	}

	public Object main() throws ServletException {

		if (!Module.UserplaneRecorder.enabled(context)) {
			return HttpServletResponse.SC_FORBIDDEN;
		}

		if (profile == null) {
			return index();
		}

		if (!PermissionType.UserplaneViewer.check(context, account)) {
			if (account != null && account.getItemType().getInteractionModule().hasRestrictionForward()) {
				return new ExternalRedirect(account.getItemType().getInteractionModule().getRestrictionForward());
			} else {
				return new RestrictionHandler(context).main();
			}
		}

		FrontendDoc doc = new FrontendDoc(context, "View profile video");
		doc.addTrail(profile.getUrl(), profile.getDisplayName());
		doc.addTrail(UserplaneViewerHandler.class, "View profile video");
		if (settings.hasViewerPageHeader()) {
			doc.addBody(settings.getViewerPageHeader());
		}

		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("Showing video for profile: " + profile.getDisplayName() + "<br/>");
				sb.append(new LinkTag(profile.getUrl(), "Return to profile"));

				return sb.toString();
			}
		});
		doc.addBody(new UserplaneRecorderRenderer(context, profile.getId(), false));
		if (settings.hasViewerPageFooter()) {
			doc.addBody(settings.getViewerPageFooter());
		}

		return doc;
	}
}
