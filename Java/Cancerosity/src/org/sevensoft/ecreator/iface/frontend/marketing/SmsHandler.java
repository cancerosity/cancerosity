package org.sevensoft.ecreator.iface.frontend.marketing;

import java.util.Set;
import java.io.IOException;

import javax.servlet.ServletException;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.validators.NumberValidator;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.frontend.marketing.panels.SmsSubscribePanel;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.marketing.sms.SmsBulletin;
import org.sevensoft.ecreator.model.marketing.sms.SmsSettings;
import org.sevensoft.ecreator.model.marketing.sms.SmsNumberException;
import org.sevensoft.ecreator.model.marketing.sms.SmsSenderException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsMessageException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayAccountException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayMessagingException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsCreditsException;
import org.sevensoft.ecreator.model.marketing.sms.exceptions.ExistingRegistrationException;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 25-May-2004 15:30:26
 */
@Path("sms.do")
public class SmsHandler extends FrontendHandler {

	private String					number;
	private Set<String>				keywords;
	private transient SmsSettings			smsSettings;
	private MultiValueMap<Attribute, String>	attributeValues;
	private Category					category;

	public SmsHandler(RequestContext context) {
		super(context);
		smsSettings = SmsSettings.getInstance(context);
	}

	public Object add() throws ServletException {

		test(new NumberValidator(), "number");
		test(new LengthValidator(11), "number");

		if (hasErrors()) {
			if (category == null) {
				return main();
			} else {
				return new CategoryHandler(context, category).main();
			}
		}

		try {

			smsSettings.addRegistration(number, SmsBulletin.get(context));

		} catch (ExistingRegistrationException e) {
			setError("number", "That number is already registered");
		}

		class RegisteredRender extends Body {

			@Override
			public String toString() {
				return "Thank you for registering for our SMS bulletin.";
			}

		}

		FrontendDoc doc = new FrontendDoc(context, "Sms bulletin");
		doc.addTrail(SmsHandler.class, "Sms bulletin");
		doc.addBody(new RegisteredRender());
		return doc;
	}

	@Override
	public Object main() {

		FrontendDoc page = new FrontendDoc(context, "Sms bulletin");
		page.addTrail(SmsHandler.class, "Sms bulletin");
		page.addBody(new SmsSubscribePanel(context, attributeValues));
		return page;
	}

    public Object send() throws ServletException {
        String message = context.getParameter("name") + "," + context.getParameter("phone") + "," + context.getParameter("mobile") +
                "," + context.getParameter("email") + "," + context.getParameter("comments") + ".Inquiry from " + context.getParameter("url");
        if (context.getParameter("toPhone") != null) {
            try {

                smsSettings.sendMessage(message, context.getParameter("toPhone"));

            } catch (IOException e) {
                e.printStackTrace();
                addError(e);

            } catch (SmsMessageException e) {
                e.printStackTrace();
                addError(e);

            } catch (SmsGatewayAccountException e) {
                e.printStackTrace();
                addError(e);

            } catch (SmsGatewayMessagingException e) {
                e.printStackTrace();
                addError(e);

            } catch (SmsGatewayException e) {
                e.printStackTrace();
                addError(e);

            } catch (SmsCreditsException e) {
                e.printStackTrace();

            } catch (SmsNumberException e) {
                e.printStackTrace();

            } catch (SmsSenderException e) {
                e.printStackTrace();
            }
            return "Message '" + message + "' was sent to " + context.getParameter("toPhone");
        }
        else {
            return "The listing does not have phone number";
        }
    }
}