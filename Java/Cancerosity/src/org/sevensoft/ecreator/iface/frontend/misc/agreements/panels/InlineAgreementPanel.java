package org.sevensoft.ecreator.iface.frontend.misc.agreements.panels;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.misc.TermsFieldSet;
import org.sevensoft.ecreator.model.misc.agreements.Agreement;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;

/**
 * @author sks 1 Feb 2007 14:19:59
 *
 */
public class InlineAgreementPanel extends Panel {

	private final Agreement	agreement;

	public InlineAgreementPanel(RequestContext context, Agreement agreement) {
		super(context);
		this.agreement = agreement;
	}

	@Override
	public void makeString() {

		sb.append(new TermsFieldSet(agreement.getTitle()));
		sb.append("<div>" + new CheckTag(context, "agreed", true, false) + " " + agreement.getInlineTextRendered() + "</div>");
		sb.append("</fieldset>");
	}
}
