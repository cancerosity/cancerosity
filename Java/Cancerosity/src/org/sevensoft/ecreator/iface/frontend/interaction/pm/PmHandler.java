package org.sevensoft.ecreator.iface.frontend.interaction.pm;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.RestrictionHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionStatusHandler;
import org.sevensoft.ecreator.iface.frontend.forum.ForumHandler;
import org.sevensoft.ecreator.iface.frontend.forum.TopicHandler;
import org.sevensoft.ecreator.iface.frontend.interaction.pm.panels.PmComposePanel;
import org.sevensoft.ecreator.iface.frontend.interaction.pm.panels.PmDailyLimitReachedPanel;
import org.sevensoft.ecreator.iface.frontend.interaction.pm.panels.PmProfilePanel;
import org.sevensoft.ecreator.model.accounts.permissions.DomainUtil;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.crm.forum.Topic;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.interaction.Interaction;
import org.sevensoft.ecreator.model.interaction.pm.PrivateMessage;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.results.RequestForward;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sam
 */
@Path( { "pm.do", "member-messages.do" })
public class PmHandler extends SubscriptionStatusHandler {

	private String			body;
	private PrivateMessage		message;
	private String			link;
	private Upload			upload;
	private Item			recipient;
	private List<PrivateMessage>	messages;
	private Topic			topic;

	public PmHandler(RequestContext context) {
		super(context);
	}

	public Object compose() throws ServletException {

		if (recipient == null && topic == null) {
			return inbox();
		}

		if (account == null) {
			return new LoginHandler(context, new Link(PmHandler.class, "compose", "recipient", recipient)).main();
		}

		if (!PermissionType.SendMessage.check(context, account)) {
			if (account.getItemType().getInteractionModule().hasRestrictionForward()) {
				return new ExternalRedirect(account.getItemType().getInteractionModule().getRestrictionForward());
			} else {
				return new RestrictionHandler(context).main();
			}
		}

		final Interaction interaction = account.getInteraction();

		FrontendDoc doc = new FrontendDoc(context, "Send message");

		if (recipient != null) {
			if (account.getItemType().getAccountModule().isShowAccount()) {
				doc.addTrail(AccountHandler.class, captions.getAccountCaption());
			}
			doc.addTrail(PmHandler.class, "inbox", "Your inbox");
		}

		if (topic != null) {
			doc.addTrail(ForumHandler.class, null, topic.getForum().getName(), "forum", topic.getForum());
			doc.addTrail(TopicHandler.class, null, topic.getTitle(), "topic", topic);
			doc.addTrail(PmHandler.class, "compose", "Message", "topic", topic);
		}

		int dailyPmLimit = DomainUtil.getDailyPmLimit(context, account);

		if (dailyPmLimit > 0 && interaction.getPmSentToday() >= dailyPmLimit) {

			doc.addBody(new PmDailyLimitReachedPanel(context));

		} else {

			doc.addBody(new PmComposePanel(context, account, recipient, topic));

		}

		return doc;
	}

	public Object delete() throws ServletException {

		/*
		 * if not logged in then reeturn to login screen
		 */
		if (account == null) {
			return new LoginHandler(context, new Link(PmHandler.class)).main();
		}

		/*
		 * the message object will check if we are eligble to delete.
		 */
		if (messages != null) {
			for (PrivateMessage message : messages) {
				message.delete(account);
			}
		}

		if (link == null) {
			return inbox();
		} else {
			return new RequestForward(link);
		}
	}

	public Object inbox() throws ServletException {

		if (account == null) {
			return new LoginHandler(context, new Link(PmHandler.class, "inbox")).main();
		}

		if (config.getUrl().contains("boysdateforfree.co.uk")) {
			return new PmBdffHandler(context).inbox();
		}

		if (!PermissionType.ReadMessages.check(context, account)) {
			if (account.getItemType().getInteractionModule().hasRestrictionForward()) {
				return new ExternalRedirect(account.getItemType().getInteractionModule().getRestrictionForward());
			} else {
				return new RestrictionHandler(context).main();
			}
		}

		String title = "Welcome to your " + captions.getPmCaption() + " mailbox";

		FrontendDoc doc = new FrontendDoc(context, title);
		if (account.getItemType().getAccountModule().isShowAccount()) {
			doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		}
		doc.addTrail(PmHandler.class, "inbox", "Your inbox");
		doc.addBody(new Body() {

			public String toString() {

				sb.append(RendererUtil.form1());

				sb.append("<div align='right'>" + new LinkTag(PmHandler.class, "inbox", "Inbox") + " | " +
						new LinkTag(PmHandler.class, "sent", "Sent items") + "</div>");

				sb.append(new TableTag("form1").setCaption(captions.getPmCaptionPlural() + ": Inbox"));

				sb.append("<tr>");
				sb.append("<th>Status</th>");
				sb.append("<th>Date Received</th>");
				sb.append("<th>From</th>");
				sb.append("<th>Message</th>");
				sb.append("<th>Delete</th>");
				sb.append("</tr>");

				sb.append(new FormTag(PmHandler.class, "delete", "POST"));

				List<PrivateMessage> messages = account.getInteraction().getPmInbox();

				if (messages.size() > 0) {

					for (PrivateMessage message : messages) {

						sb.append("<tr>");
						sb.append("<td class='status'>" +
								new LinkTag(PmHandler.class, "view", new ImageTag(message.isRead() ? "template-data/message_read.gif"
										: "template-data/message_new.gif"), "message", message) + "</td>");
						sb.append("<td class='date'>" + message.getDate().toString("HH:mma dd/MM/yyyy").toLowerCase().replace(" ", "&nbsp;") +
								"</td>");

						logger.fine("[PmHandler] sender=" + message.getSender());

						sb.append("<td  class='who'>" + new LinkTag(message.getSender().getUrl(), message.getSender().getDisplayName()) + "</td>");
						sb.append("<td>" + new LinkTag(PmHandler.class, "view", message.getSnippet(30), "message", message) + "</td>");
						sb.append("<td>" + new CheckTag(context, "messages", message, false) + "</td>");
						sb.append("</tr>");

					}

				} else {

					sb.append("<tr><td colspan='5'>You have received no messages yet</td></tr>");

				}

				sb.append("<tr>");
				sb.append("<td colspan='3'>&nbsp;</td>");
				sb.append("<td colspan='2'>" + new SubmitTag("Delete selected messages") + "</td>");
				sb.append("</tr>");

				sb.append("</form>");

				if (account.getItemType().getAccountModule().isShowAccount()) {
					sb
							.append("<tr><th colspan='5'>To return to your account " + new LinkTag(AccountHandler.class, null, "click here") +
									"</td></tr>");
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {
		return inbox();
	}

	public Object message() throws ServletException {
		return view();
	}

	public Object send() throws ServletException {

		if (account == null) {
			return new LoginHandler(context, new Link(getClass(), "message")).main();
		}

		if (!PermissionType.SendMessage.check(context, account)) {
			if (account.getItemType().getInteractionModule().hasRestrictionForward()) {
				return new ExternalRedirect(account.getItemType().getInteractionModule().getRestrictionForward());
			} else {
				return new RestrictionHandler(context).main();
			}
		}

		if (recipient == null) {
			return index();
		}

		test(new RequiredValidator(), "body");
		if (hasErrors()) {
			return message();
		}

		if (topic == null) {

			logger.fine("[PmHandler] recipient=" + recipient + ", body=" + body + ", upload=" + upload);
			account.getInteraction().sendPm(recipient, body, upload);

			FrontendDoc doc = new FrontendDoc(context, captions.getPmCaption() + " sent");
			if (account.getItemType().getAccountModule().isShowAccount()) {
				doc.addTrail(AccountHandler.class, captions.getAccountCaption());
			}
			doc.addTrail(PmHandler.class, "inbox", captions.getPmCaption() + " sent");
			doc.addBody(new Body() {

				@Override
				public String toString() {

					sb.append(captions.getPmCaption() + " sent to " + recipient.getDisplayName() + "!");
					sb.append("<br/><br/>");
					sb.append("<div class='action_link'>" + new LinkTag(recipient.getUrl(), "View recipients profile") + "</div>");
					sb.append("<div class='action_link'>" + new LinkTag(PmHandler.class, null, "Display your inbox") + "</div>");

					return sb.toString();
				}

			});
			return doc;

		} else {

			final int size = topic.messageAll(body);

			FrontendDoc doc = new FrontendDoc(context, "Message sent");
			doc.addTrail(TopicHandler.class, null, topic.getTitle(), "topic", topic);
			doc.addTrail(TopicHandler.class, null, "Message sent", "topic", topic);
			doc.addBody(new Body() {

				@Override
				public String toString() {

					sb.append("Your mesage has been sent to all contributors on this topic (" + size + " in total).<br/><br/>");
					sb.append("To return to the topic ");
					sb.append(new LinkTag(TopicHandler.class, null, "click here", "topic", topic));

					return sb.toString();
				}

			});
			return doc;

		}
	}

	/**
	 * Shows sent messages page
	 */
	public Object sent() throws ServletException {

		if (account == null) {
			return new LoginHandler(context).main();
		}

//		if (!PermissionType.SendMessage.check(context, account)) {
//			if (account.getItemType().getInteractionModule().hasRestrictionForward()) {
//				return new ExternalRedirect(account.getItemType().getInteractionModule().getRestrictionForward());
//			} else {
//				return new RestrictionHandler(context).main();
//			}
//		}

		String title = "Welcome to your " + captions.getPmCaption() + " mailbox - sent items folder";

		FrontendDoc doc = new FrontendDoc(context, title);
		if (account.getItemType().getAccountModule().isShowAccount()) {
			doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		}
		doc.addTrail(PmHandler.class, "sent", "Your sent messages");
		doc.addBody(new Body() {

			public String toString() {

				sb.append(RendererUtil.form1());

				sb.append("<div align='right'>" + new LinkTag(PmHandler.class, "inbox", "Inbox") + " | " +
						new LinkTag(PmHandler.class, "sent", "Sent items") + "</div>");

				sb.append(new TableTag("form1").setCaption(captions.getPmCaptionPlural() + ": Sent items"));

				sb.append("<tr>");
				sb.append("<th>Date sent</th>");
				sb.append("<th>To</th>");
				sb.append("<th>Message</th>");
				sb.append("<th>Delete</th>");
				sb.append("</tr>");

				sb.append(new FormTag(PmHandler.class, "delete", "POST"));
				sb.append(new HiddenTag(link, new Link(PmHandler.class, "sent")));

				List<PrivateMessage> messages = account.getInteraction().getPmSent();

				if (messages.size() > 0) {

					for (PrivateMessage message : messages) {

						sb.append("<tr>");
						sb.append("<td class='date'>" + message.getDate().toString("HH:mma dd/MM/yyyy").toLowerCase().replace(" ", "&nbsp;") +
								"</td>");
						sb.append("<td  class='who'>" + new LinkTag(message.getRecipient().getUrl(), message.getRecipient().getDisplayName()) +
								"</td>");
						sb.append("<td class='subject'>" + new LinkTag(PmHandler.class, "view", message.getSnippet(30), "message", message) +
								"</td>");
						sb.append("<td>" + new CheckTag(null, "messages", message, false) + "</td>");
						sb.append("</tr>");

					}

				} else {

					sb.append("<tr><td colspan='4'>You have received no messages yet</td></tr>");

				}

				sb.append("<tr>");
				sb.append("<td colspan='2'>&nbsp;</td>");
				sb.append("<td colspan='2'>" + new SubmitTag("Delete selected items") + "</td>");
				sb.append("</tr>");

				sb.append("</form>");

				if (account.getItemType().getAccountModule().isShowAccount()) {
					sb
							.append("<tr><th colspan='4'>To return to your account " + new LinkTag(AccountHandler.class, null, "click here") +
									"</td></tr>");
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object view() throws ServletException {

		if (message == null) {
			return inbox();
		}

		if (account == null) {
			return new LoginHandler(context, new Link(PmHandler.class, "message", "message", message)).main();
		}

		if (!PermissionType.ReadMessages.check(context, account)) {
			return new RestrictionHandler(context).main();
		}

		// ensure this is our message to view !
		if (!message.getSender().equals(account) && !message.getRecipient().equals(account)) {
			logger.fine("[PrivateMessageHandler] we are neither sender nor recip, exiting");
			return inbox();
		}

		final Item correspondant;

		// if we are the recpient then check on permission to read messages
		if (message.getRecipient().equals(account)) {

			if (!PermissionType.ReadMessages.check(context, account)) {
				logger.fine("[PrivateMessageHandler] read messages is restricted, exiting");
				return new RestrictionHandler(context).main();
			}

			correspondant = message.getSender();

		} else {

			correspondant = message.getRecipient();
		}

		// mark this message as read
		message.read();

		final Interaction interaction = account.getInteraction();

		String title;
		if (message.getSender().equals(account)) {
			title = "Showing message sent by you to " + message.getRecipient().getName();
		} else {
			title = "Showing message sent by " + message.getSender().getName() + " to you";
		}

		FrontendDoc doc = new FrontendDoc(context, title);

		if (account.getItemType().getAccountModule().isShowAccount()) {
			doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		}

		if (message != null) {

			if (message.getSender().equals(account)) {

				doc.addTrail(PmHandler.class, "sent", captions.getPmCaptionPlural() + " Sent");

			} else {

				doc.addTrail(PmHandler.class, "inbox", captions.getPmCaptionPlural() + " Inbox");
			}

		} else {

			doc.addTrail(PmHandler.class, "view", "Showing message", "message", message);

		}

		doc.addBody(new Body() {

			private void profile() {

				Markup markup = interaction.getModule().getPmProfileMarkup();
				if (markup == null) {

					sb.append(new PmProfilePanel(context, correspondant));

				} else {

					MarkupRenderer r = new MarkupRenderer(context, markup);
					r.setBody(correspondant);
					sb.append(r);
				}
			}

			@Override
			public String toString() {

				profile();
				display();
				reply();

				return sb.toString();
			}

			private void display() {

				sb.append(RendererUtil.form1());

				sb.append(new TableTag("form1"));

				sb.append("<tr><td>" + message.getContent() + "</td></tr>");

				sb.append("</table>");

			}

			private void reply() {

				sb.append("<div class='ec_pm_reply'>To send a message to this member, " +
						new LinkTag(PmHandler.class, "compose", "click here.", "recipient", correspondant) + "</div>");
			}
		});
		return doc;

	}
}
