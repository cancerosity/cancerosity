package org.sevensoft.ecreator.iface.frontend.interaction.sms.panels;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.interaction.sms.SmsHandler;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 5 Apr 2007 17:21:36
 *
 */
public class SmsComposePanel extends Panel {

	private final Item	account;
	private final Item	profile;

	public SmsComposePanel(RequestContext context, Item account, Item profile) {
		super(context);
		this.account = account;
		this.profile = profile;
	}

	@Override
	public void makeString() {

		sb.append(new TableTag("form").setId("ec_sms_compose").setCaption("Send sms message"));
		sb.append(new FormTag(SmsHandler.class, "send", "post"));
		sb.append(new HiddenTag("profile", profile));

		sb.append("<tr><th>Send an sms message to this member</th></tr>");

		// CONTENT
		sb.append("<tr><td>" + new TextAreaTag(context, "message", 40, 4) + new ErrorTag(context, "message", "<br/>") + "</td></tr>");

		sb.append("<tr><td>" + new SubmitTag("Send") + "</td></tr>");

		sb.append("</form>");
		sb.append("</table>");

	}

}
