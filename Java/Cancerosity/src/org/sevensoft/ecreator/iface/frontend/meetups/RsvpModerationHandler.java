package org.sevensoft.ecreator.iface.frontend.meetups;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.model.extras.meetups.Rsvp;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;


/**
 * @author sks 25 Apr 2007 12:53:19
 *
 */
@Path("rsvp-moderation.do")
public class RsvpModerationHandler extends FrontendHandler {

	private Rsvp	rsvp;

	public RsvpModerationHandler(RequestContext context) {
		super(context);
	}

	public Object approve() throws ServletException {

		if (!Module.Meetups.enabled(context)) {
			return index();
		}

		if (account == null) {
			return new LoginHandler(context, new Link(RsvpModerationHandler.class)).main();
		}

		if (rsvp == null) {
			return main();
		}

		// make sure we are the owner of the even
		if (!rsvp.getItem().getAccount().equals(account)) {
			return main();
		}

		rsvp.approve();
		return main();
	}

	@Override
	public Object main() throws ServletException {

		if (!Module.Meetups.enabled(context)) {
			return index();
		}

		Query q = new Query(context, "select r.* from # r join # i on r.item=i.id where i.account=? and r.approved=0");
		q.setTable(Rsvp.class);
		q.setTable(Item.class);
		q.setParameter(account);
		final List<Rsvp> rsvps = q.execute(Rsvp.class);

		FrontendDoc doc = new FrontendDoc(context, "Rsvp moderation");
		doc.addTrail(RsvpModerationHandler.class, null, "Rsvp moderation");
		doc.addBody(new MessagesTag(context));
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("Rsvps waiting your approval"));

				for (Rsvp rsvp : rsvps) {

					sb.append("<tr>");

					sb.append("<td>" + new LinkTag(rsvp.getItem().getUrl(), rsvp.getItem().getName()) + "</td>");
					sb.append("<td>" + new LinkTag(rsvp.getAccount().getUrl(), rsvp.getAccount().getDisplayName()) + "</td>");
					sb.append("<td>" + new LinkTag(RsvpModerationHandler.class, "approve", "Approve", "rsvp", rsvp) + "</td>");
					sb.append("<td>" + new LinkTag(RsvpModerationHandler.class, "reject", "Reject", "rsvp", rsvp) + "</td>");

					sb.append("</tr>");
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object reject() throws ServletException {

		if (!Module.Meetups.enabled(context)) {
			return index();
		}

		if (account == null) {
			return new LoginHandler(context, new Link(RsvpModerationHandler.class)).main();
		}

		if (rsvp == null) {
			return main();
		}

		// make sure we are the owner of the even
		if (!rsvp.getItem().getAccount().equals(account)) {
			return main();
		}

		rsvp.reject();
		return main();
	}
}
