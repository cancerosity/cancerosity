package org.sevensoft.ecreator.iface.frontend.misc;

/**
 * @author sks 12 Mar 2007 18:54:34
 *
 */
public class FieldValueCell {

	private final String	label;
	private final String	value;

	public FieldValueCell(String label, Object value) {
		this.label = label;
		this.value = value.toString();
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("<div class='field_value'>");

		sb.append("<div class='label'>");
		sb.append(label);
		sb.append("</div>");

		sb.append("<div class='value'>");
		sb.append(value);
		sb.append("</div>");

		sb.append("</div>");

		return sb.toString();
	}

}
