package org.sevensoft.ecreator.iface.frontend.interaction.sms;

import javax.servlet.ServletException;

import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.ecreator.iface.frontend.ErrorDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionStatusHandler;
import org.sevensoft.ecreator.iface.frontend.interaction.sms.panels.SmsComposePanel;
import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sam
 */
@Path("p-sms.do")
public class SmsHandler extends SubscriptionStatusHandler {

	private String	message;
	private Item	profile;

	public SmsHandler(RequestContext context) {
		super(context);
	}

	/**
	 * Show all messages between the current account and the profile
	 */
	public Object compose() throws ServletException {

		if (profile == null) {
			return index();
		}

		if (account == null) {
			return new LoginHandler(context, new Link(SmsHandler.class, "compose", "profile", profile)).main();
		}

		FrontendDoc doc = new FrontendDoc(context, "Sms message");
		doc.addTrail(ItemHandler.class, null, profile.getDisplayName());
		doc.addTrail(SmsHandler.class, "compose", "Sms message");
		doc.addBody(new SmsComposePanel(context, account, profile));
		return doc;
	}

	@Override
	public Object main() throws ServletException {
		return compose();
	}

	public Object send() throws ServletException {

		// check we have a profile to send to
		if (profile == null) {
			return index();
		}

		// check we are logged in
		if (account == null) {
			return new LoginHandler(context, new Link(SmsHandler.class, "compose", "profile", profile)).main();
		}

		test(new RequiredValidator(), "message");
		test(new LengthValidator(1, 160), "message");
		if (hasErrors()) {
			return compose();
		}

		logger.fine("[SmsHandler] profile=" + profile + ", message=" + message);
		try {
			account.getInteraction().sendSms(profile, message);
		} catch (Exception e) {
			return new ErrorDoc(context, e.getMessage());
		}

		FrontendDoc doc = new FrontendDoc(context, captions.getPmCaption() + " sent");
		doc.addTrail(ItemHandler.class, null, profile.getDisplayName());
		doc.addTrail(SmsHandler.class, "compose", "Sms message");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("Sms message has been sent");
				sb.append("<br/><br/>");
				sb.append("<div class='action_link'>" + new LinkTag(profile.getUrl(), "Return to profile") + "</div>");

				return sb.toString();
			}

		});
		return doc;
	}

}
