package org.sevensoft.ecreator.iface.frontend.account.registration.panels;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.account.profile.ProfileEditHandler;
import org.sevensoft.ecreator.iface.frontend.account.registration.RegistrationHandler;
import org.sevensoft.ecreator.iface.frontend.misc.FieldInputCell;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.model.accounts.registration.RegistrationSession;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.media.images.ImageOwner;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.media.images.ImageSettings;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Grid;

/**
 * @author sks 8 Mar 2007 12:42:47
 *
 */
public class ProfileImagesPanel extends Panel {

	private final ImageOwner		owner;
	private Class<? extends Handler>	handler;

	public ProfileImagesPanel(RequestContext context, Item account) {
		super(context);
		this.owner = account;
		this.handler = ProfileEditHandler.class;
	}

	public ProfileImagesPanel(RequestContext context, RegistrationSession session) {
		super(context);
		this.owner = session;
		this.handler = RegistrationHandler.class;
	}

	@Override
	public void makeString() {

		sb.append("You can upload images to your profile to make it stand out from the crowd!");

		if (owner.hasAnyImage()) {

			sb.append(new FormFieldSet("Images uploaded"));

			Grid grid = new Grid(4);
            for (Img image : owner.getAllImages()) {
                grid.addCell(image.getThumbnailTag(80, 60) + "<br/>" + image.getCaption() + "<br/>" + image.getWidth() + "px by " + image.getHeight() + "px<br/>"
                        + new LinkTag(handler, "removeImage", "Remove", "image", image), 1);
            }

			sb.append(grid);
			sb.append("<br/>&nbsp;");
			sb.append("</fieldset>");
		}

		if (!owner.isAtImageLimit()) {

			sb.append(new FormTag(handler, "uploadImage", "multi"));

			sb.append(new FormFieldSet("Upload an image"));

            sb.append("<div class='text'>" + ImageSettings.getInstance(context).getFormatsNote() + "</div>");

			sb.append(new FieldInputCell("Browse for file", null, new FileTag("imageUpload")));
            sb.append(new FieldInputCell("Image caption", null, new TextTag(context, "imageCaption", null)));
            sb.append(new FieldInputCell("", null, new SubmitTag("Upload image")));

			sb.append("</fieldset>");

			sb.append("</form>");
		}

	}
}
