package org.sevensoft.ecreator.iface.frontend.interaction.buddies;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.RestrictionHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionStatusHandler;
import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.interaction.buddies.BuddyLimitException;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

/**
 * @author sks 19-Aug-2005 07:10:43
 * 
 */
@Path( { "buddies.do", "member-buddies.do" })
public class BuddiesHandler extends SubscriptionStatusHandler {

	private Item	buddy;
	private int		page;

	public BuddiesHandler(RequestContext context) {
		super(context);
	}

	public Object add() throws ServletException {

		if (account == null) {
			return new LoginHandler(context).main();
		}

		if (!PermissionType.Buddies.check(context, account)) {
			return main();
		}

		if (buddy == null) {
			return main();
		}

		try {

			account.getInteraction().addBuddy(buddy);
			addMessage(captions.getBuddiesCaption() + " added.");

			FrontendDoc doc = new FrontendDoc(context, captions.getBuddiesCaption() + " added");
			if (account.getItemType().getAccountModule().isShowAccount()) {
				doc.addTrail(AccountHandler.class, captions.getAccountCaption());
			}
			doc.addTrail(buddy.getUrl(), buddy.getDisplayName());
			doc.addBody(new Body() {

				public String toString() {

					sb.append(buddy.getDisplayName());
					sb.append(" has been added to ");
					sb.append(captions.getBuddiesCaptionPlural());
					sb.append("<br/><br/>");
					sb.append(new LinkTag(buddy.getUrl(), "Return to member profile"));

					return sb.toString();
				}

			});
			return doc;

		} catch (BuddyLimitException e) {

			addMessage("You have reached your " + captions.getBuddiesCaption() + " limit.");
			return new ItemHandler(context, buddy).main();
		}

	}

	@Override
	public Object main() throws ServletException {

		if (account == null) {
			return new LoginHandler(context).main();
		}

		if (!PermissionType.Buddies.check(context, account)) {
			if (account != null && account.getItemType().getInteractionModule().hasRestrictionForward()) {
				return new ExternalRedirect(account.getItemType().getInteractionModule().getRestrictionForward());
			} else {
				return new RestrictionHandler(context).main();
			}
		}

		// create doc
		FrontendDoc doc = new FrontendDoc(context, captions.getBuddiesCaptionPlural());

		// is showing member account then add in trail
		if (account.getItemType().getAccountModule().isShowAccount()) {
			doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		}

		// add trail to the buddies page
		doc.addTrail(BuddiesHandler.class, captions.getBuddiesCaptionPlural());

		// check for header
		if (account.getItemType().getInteractionModule().hasBuddiesHeader()) {
			doc.addBody(account.getItemType().getInteractionModule().getBuddiesHeader());
		}

		// get markup for rendering
		Markup markup = account.getItemType().getInteractionModule().getBuddiesMarkup();

		// get all buddies
		List<Item> buddies = account.getInteraction().getBuddies();

		if (markup == null) {

			logger.fine("[BuddiesHandler] rendering standard buddies");
			doc.addBody(new BuddiesRenderer(context, account, buddies));

		} else {

			// if the markup is set and it is using per page then override the number of buddies

			Results results = new Results(buddies, page, markup.getPerPage());

			if (results.hasMultiplePages()) {
				doc.addBody(new ResultsControl(context, results, new Link(BuddiesHandler.class)));
				buddies = results.subList(buddies);
			}

			logger.fine("[BuddiesHandler] using custom markup=" + markup + ", buddies=" + buddies);
			MarkupRenderer r = new MarkupRenderer(context, markup);
			r.setBodyObjects(buddies);
			doc.addBody(r);

			if (results.hasMultiplePages()) {
				doc.addBody(new ResultsControl(context, results, new Link(BuddiesHandler.class)));
			}
		}

		// check for footer
		if (account.getItemType().getInteractionModule().hasBuddiesFooter()) {
			doc.addBody(account.getItemType().getInteractionModule().getBuddiesFooter());
		}

		return doc;
	}

	public Object remove() throws ServletException {

		if (account == null) {
			return new LoginHandler(context).main();
		}

		if (buddy == null) {
			return main();
		}

		account.getInteraction().removeBuddy(buddy);
		return main();
	}

	/**
	 * Show members who have added us 
	 */
	public Object who() throws ServletException {
		return addedTo();
	}

	public Object addedTo() throws ServletException {

		if (account == null) {
			return new LoginHandler(context).main();
		}

		if (!PermissionType.Buddies.check(context, account)) {
			return new RestrictionHandler(context).main();
		}

		// create doc
		FrontendDoc doc = new FrontendDoc(context, captions.getAddedToBuddiesCaptionPlural());

		// is showing member account then add in trail
		if (account.getItemType().getAccountModule().isShowAccount()) {
			doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		}

		// add trail to the buddies page
		doc.addTrail(BuddiesHandler.class, "addedTo", captions.getAddedToBuddiesCaptionPlural());

		// check for header
		if (account.getItemType().getInteractionModule().hasAddedToBuddiesHeader()) {
			doc.addBody(account.getItemType().getInteractionModule().getAddedToBuddiesHeader());
		}

		// get markup for rendering
		Markup markup = account.getItemType().getInteractionModule().getBuddiesMarkup();

		// get all members who have added you
		List<Item> buddies = account.getInteraction().getAddedToBuddies();

		if (markup == null) {

			logger.fine("[BuddiesHandler] rendering standard buddies");
			doc.addBody(new BuddiesRenderer(context, account, buddies));

		} else {

			// if the markup is set and it is using per page then override the number of buddies

			Results results = new Results(buddies, page, markup.getPerPage());

			if (results.hasMultiplePages()) {
				doc.addBody(new ResultsControl(context, results, new Link(BuddiesHandler.class, "addedTo")));
				buddies = results.subList(buddies);
			}

			logger.fine("[BuddiesHandler] using custom markup=" + markup + ", buddies=" + buddies);
			MarkupRenderer r = new MarkupRenderer(context, markup);
			r.setBodyObjects(buddies);
			doc.addBody(r);

			if (results.hasMultiplePages()) {
				doc.addBody(new ResultsControl(context, results, new Link(BuddiesHandler.class, "addedTo")));
			}

		}

		// check for footer
		if (account.getItemType().getInteractionModule().hasAddedToBuddiesFooter()) {
			doc.addBody(account.getItemType().getInteractionModule().getAddedToBuddiesFooter());
		}

		return doc;
	}
}
