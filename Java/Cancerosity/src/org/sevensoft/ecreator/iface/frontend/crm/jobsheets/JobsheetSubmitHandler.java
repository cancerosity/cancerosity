package org.sevensoft.ecreator.iface.frontend.crm.jobsheets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.net.EmailValidator;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.crm.jobsheets.Job;
import org.sevensoft.ecreator.model.crm.jobsheets.Jobsheet;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 12 Mar 2007 13:06:49
 *
 */
@Path("jobsheets-submit.do")
public class JobsheetSubmitHandler extends FrontendHandler {

	private Jobsheet	jobsheet;
	private Category	category;
	private String	name;
	private String	body;
	private String	email;

	public JobsheetSubmitHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (jobsheet == null) {
			return HttpServletResponse.SC_BAD_REQUEST;
		}

		test(new RequiredValidator(), "name");
		test(new RequiredValidator(), "body");
		test(new RequiredValidator(), "email");
		test(new EmailValidator(), "email");
		if (context.hasErrors()) {
			return new CategoryHandler(context, category).main();
		}

		Job job = jobsheet.addJob(null, "Customer jobsheet submitted by " + name);
		job.addMessage(name, email, body);

		FrontendDoc doc = new FrontendDoc(context, "Message submitted");
		doc.addBody("Thank you, your message has been logged into our system. We will reply shortly.");
		return doc;
	}

}
