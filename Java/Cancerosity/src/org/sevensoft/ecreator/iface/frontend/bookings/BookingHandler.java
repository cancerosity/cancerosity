package org.sevensoft.ecreator.iface.frontend.bookings;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.net.EmailValidator;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.ecreator.iface.frontend.ErrorDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.bookings.panels.BookingConfirmationPanel;
import org.sevensoft.ecreator.iface.frontend.ecom.payments.panels.PaymentTable;
import org.sevensoft.ecreator.iface.frontend.misc.FieldInputCell;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.ecreator.model.attributes.renderers.input.AFormSectionsInputPanel;
import org.sevensoft.ecreator.model.bookings.*;
import org.sevensoft.ecreator.model.bookings.sessions.BookingSession;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.addresses.AddressFields;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.*;
import org.sevensoft.ecreator.model.ecom.addresses.panels.AddressEntryPanel;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.cards.CardType;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.*;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorException;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.agreements.Agreement;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.date.DateTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Map;

/**
 * @author sks 15 Feb 2007 17:02:46
 */
@Path("bookings.do")
public class BookingHandler extends FrontendHandler {

    private transient final BookingSettings bookingSettings;

    private Item item;
    private BookingSession bookingSession;
    private Date start;
    private Date end;
    private boolean book;
    private String email;
    private String name;
    private PaymentType paymentType;
    private Card card;
    private boolean editPaymentType;
    private MultiValueMap<Attribute, String> attributeValues;
    private List<BookingOption> options;
    private boolean agreed;
    private Country country;
    private int children;
    private int adults;
    private String county;
    private String town;
    private String telephone2;
    private String telephone3;
    private String telephone1;
    private String address1;
    private String address2;
    private String address3;
    private String postcode;

    private String accountEmail;

    private String accountName;

    private boolean editOptions;

    private boolean editDetails;

    private String cardHolder;

    private String cardIssuer;

    private String cardNumber;

    private CardType cardType;

    private String csc;

    private String expiryMonth;

    private String expiryYear;

    private String issueNumber;

    private String startMonth;

    private String startYear;

    public BookingHandler(RequestContext context) {
        super(context);
        this.bookingSession = BookingSession.get(context, context.getSessionId());
        this.bookingSettings = BookingSettings.getInstance(context);
    }

    private Object askAttributes() {

        bookingSession.setOfferedAttributes(true);
        bookingSession.save();

        String title = "Online booking - attributes";
        FrontendDoc doc = new FrontendDoc(context, title);
        doc.addTrail(BookingHandler.class, null, title);
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new FormTag(BookingHandler.class, "setAttributes", "POST"));

                /*
                     * Get attributes for this page and render
                     */
                final List<Attribute> attributes = bookingSettings.getAttributes();
                AFormSectionsInputPanel panel = new AFormSectionsInputPanel(context, bookingSession, attributes);
                panel.setDefaultSection("Extra information");

                sb.append(panel);

                sb.append("<div class='ec_form_commands'>" + new SubmitTag("Continue") + "</div>");

                sb.append("</form>");

                return sb.toString();
            }
        });
        return doc;
    }

    private Object askDetails() {

        final String title = "Online booking - your details";
        FrontendDoc doc = new FrontendDoc(context, title);
        doc.addTrail(BookingHandler.class, null, title, "edit details", true);
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new MessagesTag(context));
                sb.append(new FormTag(BookingHandler.class, "setDetails", "POST"));

                // ACCOUNT DETAILS
                if (!bookingSession.hasAccount()) {

                    sb.append(new FormFieldSet("Your details"));

                    sb.append(new FieldInputCell("Your name", "Please enter your name, or if the booking is for another person, enter their name.",
                            new TextTag(context, "accountName", bookingSession.getAccountName(), 30), new ErrorTag(context, "accountName",
                            "<br/>")));

                    sb.append(new FieldInputCell("Your email address", bookingSettings.getEmailInstructions(), new TextTag(context, "accountEmail",
                            bookingSession.getAccountEmail(), 30), new ErrorTag(context, "accountEmail", "<br/>")));

                    sb.append("</fieldset>");

                    if (bookingSettings.hasReferrers()) {

                        sb.append(new FormFieldSet("Referred from"));

                        SelectTag tag = new SelectTag(context, "referrer", bookingSession.getReferrer(), bookingSettings.getReferrers(),
                                "-Please choose-");

                        sb.append(new FieldInputCell("How did you hear about our site?",
                                "We would be very grateful if you would let us know how you found out about our site.", tag));

                        sb.append("</fieldset>");
                    }

                }

                sb.append(new FormFieldSet("Your address"));

                AddressFields fields = AddressFields.getInstance(context, AddressFields.Type.Booking);

                AddressEntryPanel entryPanel = new AddressEntryPanel(context, fields);
                entryPanel.setCountries(shoppingSettings.isShowCountries());

                sb.append(entryPanel);

                sb.append("<div class='ec_form_commands'>" + new SubmitTag("Continue") + "</div>");

                sb.append("</fieldset>");

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    private Object askOptions() {

        final BookingSetup setup = bookingSession.getItem().getBookingSetup();

        bookingSession.setOfferedOptions(true);

        final String title = "Online booking";
        FrontendDoc doc = new FrontendDoc(context, title);
        doc.addTrail(BookingHandler.class, null, title);
        doc.addBody(new MessagesTag(context));
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new FormTag(BookingHandler.class, "setOptions", "post"));

                sb.append(new FormFieldSet("Booking details"));
                DateTag startTag = new DateTag(context, "start", bookingSession.getStart());
                DateTag endTag = new DateTag(context, "end", bookingSession.getEnd());
                sb.append("<div class='text'>You are making a booking for: <strong>" + bookingSession.getItem().getName() + "</strong></div>");
                sb.append("<div class='text'> from " + startTag + " to " + endTag + "</div>");        /*<strong>" + bookingSession.getStay() + "</strong> nights*/

                sb.append(new FieldInputCell("Adults", "Choose the number of adults for this booking.", new TextTag(context, "adults", bookingSession
                        .getAdults(), 4)));

                sb.append(new FieldInputCell("Children", "Choose the number of children for this booking.", new TextTag(context, "children",
                        bookingSession.getChildren(), 4)));

                sb.append("</fieldset>");

                List<BookingOption> options = setup.getOptions();
                if (options.size() > 0) {

                    sb.append(new FormFieldSet("Extra options"));

                    sb.append("<div class='text'>Choose any extra options you would like with your booking</div>");

                    for (BookingOption option : options) {

                        sb.append("<div class='text'><strong>" + new CheckTag(context, "options", option, false) + " " + option.getName() +
                                " at \243" + option.getChargeInc() + "</strong></div>");
                    }

                    sb.append("</fieldset>");
                }

                sb.append("<div class='ec_form_commands'>" + new SubmitTag("Continue") + "</div>");
                sb.append("</form>");

                return sb.toString();
            }
        });
        return doc;
    }

    private Object registerBooking() throws ServletException {

        try {
            bookingSession.setPaymentType(PaymentType.ManualPayments);
            bookingSession.toBooking();
        } catch (Exception e) {
            e.printStackTrace();
        }

        final String title = "Online booking - payment details";
        FrontendDoc doc = new FrontendDoc(context, title);
        doc.addBody(new Body() {

            @Override
            public String toString() {
                sb.append("<b>" + PaymentType.ManualPayments.getPublicMessage() + "</b>");
                sb.append(new AdminTable(""));
                sb.append("<tr><td><strong>Booking Status: </strong>" + bookingSession.getBooking().getStatus().toString() + "</tr></td>");
                sb.append("<tr><td><strong>Accomodation: </strong>" + bookingSession.getBooking().getItem().getName() + "</tr></td>");
                sb.append("<tr><td><strong>Start date: </strong>" + bookingSession.getBooking().getStart().toString("dd-MMM-yyyy") + "</tr></td>");
                sb.append("<tr><td><strong>End date: </strong>" + bookingSession.getBooking().getEnd().toString("dd-MMM-yyyy") + "</tr></td>");

                sb.append("<tr><td><strong>Accomodation charge: </strong>" + bookingSession.getBooking().getItemChargeEx() + "</tr></td>");

                if (bookingSession.getBooking().hasOptions()) {
                    sb.append("<tr><td><strong>Extra options: </strong>" + bookingSession.getBooking().getOptionsString() + "</tr></td>");
                    sb.append("<tr><td><strong>Options charges: </strong>" + bookingSession.getBooking().getOptionsChargesEx() + "</tr></td>");
                }

                sb.append("<tr><td><strong>Commission: </strong>" + bookingSession.getBooking().getCommissionValue() + "</tr></td>");
                sb.append("<tr><td><strong>Subtotal: </strong>" + bookingSession.getBooking().getTotalEx() + "</tr></td>");
                sb.append("<tr><td><strong>Vat: </strong>" + bookingSession.getBooking().getTotalVat() + "</tr></td>");
                sb.append("<tr><td><strong>Total: </strong>" + bookingSession.getBooking().getTotalInc() + "</tr></td>");

                sb.append("<tr><td><strong>Occupancy: </strong>" + "Adults " + bookingSession.getBooking().getAdults() +
                        " Children " + bookingSession.getBooking().getChildren() + "</tr></td>");

                sb.append("</table>");
                return sb.toString();
            }

        });
        return doc;

    }

    private Object askPaymentType() throws ServletException {

        logger.fine("[BookingHandler=" + context.getSessionId() + "] showing payment screen");

        /*
        * Get all payment types applicable for all groups this member is in
        */
        final Set<PaymentType> paymentTypes = PaymentSettings.getInstance(context).getAvailablePaymentTypes(new Money(0));
        if (paymentTypes.isEmpty()) {
            return new ErrorDoc(context, "There are no payment methods configured");
        }

        /*
           * if we only have one payment method then check if we can forward on automatically or if it is a payment form that requires a post we
           * must still show the payment option.
           */
        if (paymentTypes.size() == 1) {
            Iterator<PaymentType> iter = paymentTypes.iterator();
            paymentType = iter.next();
            return setPaymentType();
        }

        final String title = "Online booking - payment details";
        FrontendDoc doc = new FrontendDoc(context, title);
        doc.addTrail(BookingHandler.class, null, title, "editPaymentType", true);
        doc.addBody(new Body() {

            @Override
            public String toString() {
                sb.append(new PaymentTable(context, paymentTypes, BookingHandler.class, "setPaymentType"));
                return sb.toString();
            }

        });
        return doc;
    }

    private Object book() throws ServletException {

        /*
           * Ensure we have ticked agreement box if present
           */
        if (bookingSession.getBookingModule().hasAgreement()) {

            Agreement agreement = bookingSession.getBookingModule().getAgreement();
            if (agreement.isInline()) {

                if (!agreed) {

                    logger.fine("[BookingHandler] not agreed for inline agreement");
                    addError("You must agree to our " + agreement.getTitle() + " before you can continue");

                    book = false;
                    return main();
                }
            }
        }

        try {
            bookingSession.toBooking();
        } catch (Exception e) {
            addError("Error with your card details, please re-enter them");
            return askCardDetails();
        }

        return completed();
    }

    private Object completed() throws ServletException {

        final Booking booking = bookingSession.getBooking();

        final String title = "Online booking completed";
        FrontendDoc doc = new FrontendDoc(context, title);
        doc.addTrail(BookingHandler.class, null, title);
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(bookingSettings.getCompletedTextRendered(booking));
                return sb.toString();
            }

        });
        return doc;
    }

    private Object confirmation() {

        String title = "Confirm booking ";
        FrontendDoc doc = new FrontendDoc(context, title);
        doc.addTrail(BookingHandler.class, null, title);
        doc.addBody(new MessagesTag(context));
        doc.addBody(new BookingConfirmationPanel(context, bookingSession));
        return doc;
    }

    @Override
    public Object main() throws ServletException {

        /*
           * Check if we have already completed a booking
           */
//		if (bookingSession.hasBooking()) {
//
//			logger.fine("[BookingHandler] booking completed, forwarding to completion");
//			return completed();
//		}
        /*
           * Check for required data
           */
        if (!bookingSession.hasItem()) {
            logger.fine("[BookingHandler] no item set on session");
            return index();
        }
        if (!bookingSession.hasStart()) {
            logger.fine("[BookingHandler] no start date set");
            return index();
        }
        if (!bookingSession.hasEnd()) {
            logger.fine("[BookingHandler] no end date set");
            return index();
        }

        /*
           * If we are logged in then set account, otherwise check for name and email
           */
        bookingSession.setAccount(account);
        logger.fine("[BookingHandler] setting account=" + account);

        /*
           * If we have not yet been asked for options then ask for options
           */
        if (!bookingSession.isOfferedOptions()) {

            logger.fine("[BookingHandler] not shown options screen yet");
            return askOptions();
        }

        if (editOptions) {

            logger.fine("[BookingHandler] asking to edit options");
            return askOptions();
        }

        if (!bookingSession.hasAccount()) {

            logger.fine("[BookingHandler] no account set...");
            if (!bookingSession.hasName() || !bookingSession.hasEmail()) {

                logger.fine("[BookingHandler] missing required details");
                return askDetails();
            }
        }

        if (editDetails) {

            logger.fine("[BookingHandler] asking to edit details");
            return askDetails();
        }

        // check for payment details
        if (editPaymentType) {

//            logger.fine("[BookingHandler] asking to edit payment type");
            return askPaymentType();
        }

        /*if (!bookingSession.hasPaymentType()) {

            logger.fine("[BookingHandler] no payment type set");
            return askPaymentType();
        }*/

        // we must have card details
        /*if (!bookingSession.hasCard()) {

            logger.fine("[BookingHandler] no card details");
            return askCardDetails();
        }
        */
        /*if (editCardDetails) {

            logger.fine("[BookingHandler] no card details");
            return askCardDetails();
        }*/

//        if (book) {

        return registerBooking();
//        }
        /*else {
            return confirmation();
        }*/
    }

    private boolean editCardDetails;

    public Object setAttributes() throws ServletException {

        /*
           * Validators for the attributes for our current page
           */
        AttributeUtil.setErrors(context, bookingSettings.getAttributes(), attributeValues);

        /*
           * If we have errors or our places map contains at least one entry then return
           */
        if (hasErrors()) {
            return askAttributes();
        }

        bookingSession.setAttributeValues(attributeValues);
        logger.fine("[BookingHandler] setting attributeValues=" + attributeValues);

        return main();
    }

    public Object setDetails() throws ServletException {

        AddressFields fields = AddressFields.getInstance(context, AddressFields.Type.Booking);

        logger.fine("[BookingHandler=" + context.getSessionId() + "] set details");

        // we need name and email if account is null
        if (!bookingSession.hasAccount()) {

            logger.fine("[BookingHandler=" + context.getSessionId() + "] booking session does not have account, so testing for name and email params");

            test(new LengthValidator(6), "accountName");
            test(new LengthValidator(6), "accountEmail");
            test(new EmailValidator(), "accountEmail");
        }

        // add tests for delivery if required
        //		if (address == null) {

        logger.fine("[BookingHandler=" + context.getSessionId() + "] no address param, so testing for creation fields");
        Address.addTests(context, country, fields);
        //		}

        if (hasErrors()) {
            logger.fine("[BookingHandler=" + context.getSessionId() + "] immediate errors detected: " + context.getErrors());
            return askDetails();
        }

        if (!bookingSession.hasAccount()) {

            logger.fine("[BookingHandler=" + context.getSessionId() + "] booking session does not have account, setting accountName=" + accountName +
                    ", accountEmail=" + accountEmail);

            bookingSession.setAccountName(accountName);
            bookingSession.setAccountEmail(accountEmail);
            bookingSession.save();

        }

        logger.fine("[BookingHandler=" + context.getSessionId() + "] setting adults=" + adults + ", children=" + children);

        //		if (address == null) {

        logger.fine("[BookingHandler=" + context.getSessionId() + "] attempting to create address");

        try {

            Address newadd = new Address(context, bookingSession.getAccount(), name, address1, address2, address3, town, county, postcode, country);
            newadd.setTelephone1(telephone1);
            newadd.setTelephone2(telephone2);
            newadd.setTelephone3(telephone3);
            newadd.save();

            logger.fine("[BookingHandler=" + context.getSessionId() + "] address created=" + newadd);

            // set delivery address on session
            bookingSession.setAddress(newadd);

        } catch (PostCodeException e) {
            setError("postcode", e);
            logger.fine("[BookingHandler=" + context.getSessionId() + "] address error: " + e);
            return askDetails();

        } catch (ContactNameException e) {
            setError("name", e);
            logger.fine("[BookingHandler=" + context.getSessionId() + "] address error: " + e);
            return askDetails();

        } catch (TownException e) {
            setError("town", e);
            logger.fine("[BookingHandler=" + context.getSessionId() + "] address error: " + e);
            return askDetails();

        } catch (CountryException e) {
            setError("country", e);
            logger.fine("[BookingHandler=" + context.getSessionId() + "] address error: " + e);
            return askDetails();

        } catch (AddressLineException e) {
            setError("address1", e);
            logger.fine("[BookingHandler=" + context.getSessionId() + "] address error: " + e);
            return askDetails();
        }

        return main();
    }

    public Object setOptions() throws ServletException {

        // we should have at least one child or adult
        if (adults + children < 1) {

            logger.fine("[BookingHandler] adults and children==0");

            addError("Please choose number of required occupants");
            return askOptions();
        }

        // check constraints on adults, children and total
        BookingSetup bookingSetup = bookingSession.getItem().getBookingSetup();

        if (new Date().getTimestamp() > start.getTimestamp()) {

            logger.fine("[BookingHandler] incorect start date, start=" + new Date().beginMonth());

            addError("Min start date is today: " + new Date().toDayMonthString());
            return askOptions();
        }

        if (bookingSession.getEnd().getTimestamp() < end.getTimestamp()) {

            logger.fine("[BookingHandler] incorect ent date, end=" + bookingSession.getEnd());

            addError("Max end date is " + bookingSession.getEnd().toDayMonthString());
            return askOptions();
        }

        if (bookingSetup.getMaxAdults() > 0 && adults > bookingSetup.getMaxAdults()) {

            logger.fine("[BookingHandler] too many adults, maxadults=" + bookingSetup.getMaxAdults() + ", adults=" + adults);

            addError("Max number of adults is " + bookingSetup.getMaxAdults());
            return askOptions();
        }

        if (bookingSetup.getMaxChildren() > 0 && children > bookingSetup.getMaxChildren()) {

            logger.fine("[BookingHandler] too many children, maxchildren=" + bookingSetup.getMaxChildren() + ", children=" + children);

            addError("Max number of children is " + bookingSetup.getMaxChildren());
            return askOptions();
        }

        if (bookingSetup.getMaxOccupants() > 0 && (children + adults) > bookingSetup.getMaxOccupants()) {

            logger.fine("[BookingHandler] too many occupants, max=" + bookingSetup.getMaxOccupants() + ", adults=" + adults + ", children=" + children);

            addError("Max number of occupants is " + bookingSetup.getMaxOccupants());
            return askOptions();
        }

        List<BookingSlot> dates = bookingSetup.getBookingDates();
        Map<Date, BookingSlot> datesMap = bookingSetup.getDatesMap(dates);
        List<Date> days = Date.getDaysBetween(start, end);

        for (Date day : days) {
            BookingSlot date = datesMap.get(day);
            if (date == null || date.getBookings() > 0) {
                logger.fine("[BookingHandler] This day has been already reserved , date = " + day);

                addError("The day " + day.toTextDateString() + " is unavailable for reservation ");
                return askOptions();
            }
        }

        for (Date day : days) {
            datesMap.get(day).setStatus(BookingSlot.Status.Reserved);
        }

        bookingSession.setAdults(adults);
        bookingSession.setChildren(children);
        bookingSession.setStart(start);
        bookingSession.setEnd(end);
        bookingSession.save();

        bookingSession.removeOptions();

        for (BookingOption option : options) {

            logger.fine("[BookingHandler] adding option=" + option);
            bookingSession.addOption(option);
        }

        return main();
    }

    public Object setPaymentType() throws ServletException {

        logger.fine("[BookingHandler] setting payment type=" + paymentType);
        bookingSession.setPaymentType(paymentType);

        return main();
    }

    public Object start() throws ServletException {

        bookingSession.delete();
        bookingSession = new BookingSession(context, context.getSessionId());

        logger.fine("[BookingHandler] starting new session item=" + item + ", start=" + start + ", end=" + end);

        bookingSession.setItem(item);
        bookingSession.setStart(start);
        bookingSession.setEnd(end);

        bookingSession.save();

        assert bookingSession != null;
        return main();
    }

    private Object askCardDetails() throws ServletException {

        String title = "Checkout - Card details";
        FrontendDoc doc = new FrontendDoc(context, title);
        doc.addTrail(BookingHandler.class, title);
        doc.addBody(new MessagesTag(context));
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new FormTag(BookingHandler.class, "createCard", "POST"));
                sb.append(new TableTag("form", "center").setCaption("Enter card details"));

                sb.append("<tr><td class='info' colspan='2'>Enter the name exactly as it appears on the card, eg. Mrs Jane B Smith</td></tr>");

                sb.append("<tr><td class='key' align='right'>Card Holder</td><td class='value'>" + new TextTag(context, "cardHolder", 20) +
                        new ErrorTag(context, "cardHolder", "<br/>") + "</td></tr>");

                SelectTag tag = new SelectTag(context, "cardType");
                tag.addOptions(CardType.get());

                sb.append("<tr><td class='key' align='right'>Card Type</td><td class='value'>" + tag + new ErrorTag(context, "cardType", "<br/>") +
                        "</td></tr>");

                sb.append("<tr><td class='info' colspan='2'>Enter the long card number without any spaces.</td></tr>");

                sb.append("<tr><td class='key' align='right'>Card Number</td><td class='value'>" + new TextTag(context, "cardNumber", 25) +
                        new ErrorTag(context, "cardNumber", "<br/>") + "</td></tr>");

                SelectTag month = new SelectTag(context, "startMonth");
                month.setAny("--");
                month.addOptions(Card.months);

                SelectTag year = new SelectTag(context, "startYear");
                year.setAny("--");
                year.addOptions(Card.startYears);

                sb.append("<tr><td class='key' align='right'>Start date</td><td class='value'>" + month + " " + year + " " +
                        new ErrorTag(context, "startDate", "<br/>") + "</td></tr>");

                month = new SelectTag(context, "expiryMonth");
                month.addOptions(Card.months);

                year = new SelectTag(context, "expiryYear");
                year.addOptions(Card.expiryYears);

                sb.append("<tr><td class='key' align='right'>Expiry date</td><td class='value'>" + month + " " + year + " " +
                        new ErrorTag(context, "expiryDate", "<br/>") + "</td></tr>");

                sb.append("<tr><th colspan='2'>" + "Enter the issue number exactly as it appears on the card. "
                        + "If your card does not have an issue number leave this field blank.</th></tr>");

                sb.append("<tr><td class='key' align='right'>Issue number</td><td class='value'>" + new TextTag(context, "issueNumber", 3) +
                        new ErrorTag(context, "issueNumber", "<br/>") + "</td></tr>");

                sb.append("<tr><th colspan='2'>"
                        + "Enter the card security code which is the last 3 digits printed on the reverse of the card.</th></tr>");

                sb.append("<tr><td class='key' align='right'>Card security code</td><td class='value'>" + new TextTag(context, "csc", 5) +
                        new ErrorTag(context, "csc", "<br/>") + "</td></tr>");

                sb.append("<tr><td class='command' align='center' colspan='2'>" + new SubmitTag("Continue with order").setClass("checkout_button") +
                        "</td></tr>");

                sb.append("</table>");
                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object createCard() throws ServletException {

        test(new LengthValidator(5), "cardHolder");
        test(new LengthValidator(16), "cardNumber");
        test(new RequiredValidator(), "cardType");
        test(new RequiredValidator(), "expiryYear");
        test(new RequiredValidator(), "expiryMonth");
        test(new LengthValidator(3, 3), "csc");

        if (hasErrors()) {
            return askCardDetails();
        }

        String expiryDate = expiryMonth + expiryYear;
        String startDate = null;
        if (startMonth != null && startYear != null) {
            startDate = startMonth + startYear;
        }

        try {

            Card card = new Card(context, null, getRemoteIp(), cardType, cardHolder, cardNumber, expiryDate, startDate, issueNumber, csc, cardIssuer);

            logger.fine("[BookingHandler] card created=" + card);

            bookingSession.setCard(card);

            // card created, so return to process
            return main();

        } catch (IssueNumberException e) {
            setError("issueNumber", e);

        } catch (ExpiryDateException e) {
            setError("expiryDate", e);

        } catch (ExpiredCardException e) {
            setError("expiryDate", e);

        } catch (CardNumberException e) {
            setError("cardNumber", e);

        } catch (IOException e) {
            addError(e);

        } catch (CardTypeException e) {
            setError("cardType", e);

        } catch (StartDateException e) {
            setError("startDate", e);

        } catch (CardHolderException e) {
            setError("cardHolder", e);

        } catch (CscException e) {
            setError("csc", e);

        } catch (CardException e) {
            e.printStackTrace();
            addError(e);

        } catch (ProcessorException e) {
            e.printStackTrace();
            addError(e);
        }

        return askCardDetails();

    }
}
