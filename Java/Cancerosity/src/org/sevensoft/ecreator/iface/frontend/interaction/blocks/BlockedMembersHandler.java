package org.sevensoft.ecreator.iface.frontend.interaction.blocks;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionStatusHandler;
import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 3 Dec 2006 18:16:49
 *
 */
@Path("blocked-members.do")
public class BlockedMembersHandler extends SubscriptionStatusHandler {

	/**
	 * The profile to block
	 */
	private Item	profile;

	public BlockedMembersHandler(RequestContext context) {
		super(context);
	}

	public Object block() throws ServletException {

		if (profile == null) {
			return index();
		}

		if (account == null) {
			return new LoginHandler(context, new Link(BlockedMembersHandler.class, "block", "profile", profile)).main();
		}

		account.getInteraction().block(profile);
		return new ItemHandler(context, profile).main();
	}

	/**
	 *Show all blocked members
	 */
	@Override
	public Object main() throws ServletException {

		if (account == null) {
			return new LoginHandler(context, new Link(BlockedMembersHandler.class)).main();
		}

		FrontendDoc doc = new FrontendDoc(context, "Blocked members");
		if (account.getItemType().getAccountModule().isShowAccount()) {
			doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		}
		doc.addTrail(BlockedMembersHandler.class, "Blocked members");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("form").setCaption("Blocked members"));
				sb.append("<tr><td colspan='2'>These are the members you have blocked from contacting you and from viewing your profile.<td></tr>");

				for (Item blocked : account.getInteraction().getBlockedAccounts()) {

					sb.append("<tr>");
					sb.append("<td>" + new LinkTag(blocked.getUrl(), blocked.getDisplayName()) + "</td>");
					sb.append("<td>" + new LinkTag(BlockedMembersHandler.class, "unblock", "Unblock", "profile", blocked) + "</td>");
					sb.append("</tr>");
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object unblock() throws ServletException {

		if (account == null) {
			return new LoginHandler(context).main();
		}

		if (profile == null) {
			return index();
		}

		account.getInteraction().unblock(profile);
		return new ItemHandler(context, profile).main();
	}
}
