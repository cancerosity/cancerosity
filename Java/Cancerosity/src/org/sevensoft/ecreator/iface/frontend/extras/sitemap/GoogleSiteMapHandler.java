package org.sevensoft.ecreator.iface.frontend.extras.sitemap;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.jdom.Document;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.sevensoft.ecreator.model.misc.sitemap.GoogleSiteMap;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.StringResult;

/**
 * @author sks 29-Oct-2005 09:51:03
 * 
 */
@Path("google-sitemap.do")
public class GoogleSiteMapHandler extends Handler {

	private int	start;

	public GoogleSiteMapHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() {

		Document doc = GoogleSiteMap.getSitemaps(context);

		XMLOutputter outputter = new XMLOutputter();
		Format format = outputter.getFormat();
		format.setLineSeparator("\n");
		format.setOmitDeclaration(false);
		format.setOmitEncoding(false);

		outputter.setFormat(format);
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {

			outputter.output(doc, out);
			return new StringResult(new String(out.toByteArray()), "text/xml");

		} catch (IOException e) {
			return HttpServletResponse.SC_BAD_REQUEST;
		} finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

	}

	@Override
	protected boolean runSecure() {
		return false;
	}

	public Object sitemap() {

		Document doc = GoogleSiteMap.getSitemap(context, start);

		XMLOutputter outputter = new XMLOutputter();
		Format format = outputter.getFormat();
		format.setLineSeparator("\n");
		format.setOmitDeclaration(false);
		format.setOmitEncoding(false);

		outputter.setFormat(format);
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {

			outputter.output(doc, out);
			return new StringResult(new String(out.toByteArray()), "text/xml");

		} catch (IOException e) {
			return HttpServletResponse.SC_BAD_REQUEST;
		}
	}

}
