package org.sevensoft.ecreator.iface.frontend.bookings;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.items.listings.MyListingsHandler;
import org.sevensoft.ecreator.model.bookings.Booking;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.EmailTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;
import java.util.List;

/**
 * User: MeleshkoDN
 * Date: 10.10.2007
 * Time: 10:58:16
 */
@Path("item-bookings.do")
public class ViewBookingsHandler extends FrontendHandler {

    private transient List<Booking> bookings;
    private Item item;

    public ViewBookingsHandler(RequestContext context) {
        super(context);
    }

    public Object main() throws ServletException {

        /**
         * We must be logged in to view bookings
         */
        if (account == null) {
            logger.fine("[ViewBookingsHandler] not logged in");
            return new LoginHandler(context, new Link(MyListingsHandler.class)).main();
        }

        if (item == null) {
            return index();
        }
        if (bookings == null) {
            bookings = SimpleQuery.execute(context, Booking.class, "item", item);
        }

        FrontendDoc doc = new FrontendDoc(context, "Bookings for " + item.getItemType().getNameLower() + " " + item.getLabel());
        if (account.getItemType().getAccountModule().isShowAccount()) {
            doc.addTrail(AccountHandler.class, captions.getAccountCaption());
        }
        doc.addTrail(MyListingsHandler.class, captions.getListingsCaption());
        doc.addTrail(ViewBookingsHandler.class, null, "View Bookings", "item", item);

        doc.addBody(new Body() {

            @Override
            public String toString() {

                int span = 6;

                sb.append(new TableTag("form").setCaption("Viewing bookings"));

                sb.append("<tr>");
                sb.append("<td><b>Booking No</b></td>");
                sb.append("<td><b>Date placed</b></td>");
                sb.append("<td><b>Customer</b></td>");
                sb.append("<td><b>Status</b></td>");
                sb.append("<td><b>Item</b></td>");
                sb.append("<td><b>Booking dates</b></td>");
                sb.append("</tr>");

                if (!bookings.isEmpty()) {

                    for (Booking booking : bookings) {

                        sb.append("<tr>");
                        sb.append("<td>" + new LinkTag(EditBookingHandler.class, null, booking.getIdString(), "booking", booking) + "</td>");
                        sb.append("<td>" + booking.getDatePlaced().toString("dd-MMM-yy") + "</td>");
                        sb.append("<td>" + booking.getAccount().getName());

                        if (booking.getAccount().hasEmail()) {
                            sb.append("<br/>");
                            sb.append(new EmailTag(booking.getAccount().getEmail()));
                        }

                        sb.append("</td>");
                        sb.append("<td>" + booking.getStatus() + "</td>");
                        sb.append("<td>" + booking.getItem().getName() + "</td>");
                        sb.append("<td>" + booking.getStart().toString("dd-MMM-yyyy") + " to " + booking.getEnd().toString("dd-MMM-yyyy") + "</td>");
                        sb.append("</tr>");
                    }

                } else {
                    sb.append("<tr><td colspan='" + span + "'>You have no listings yet.</td></tr>");
                }
                sb.append("</table>");
                return sb.toString();
            }

        });
        return doc;
    }
}
