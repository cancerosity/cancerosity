package org.sevensoft.ecreator.iface.frontend.extras.calculators;

import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.extras.calculators.Calculator;
import org.sevensoft.ecreator.model.extras.calculators.CalculatorField;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 8 May 2006 23:29:53
 *
 */
@Path("calculator.do")
public class CalculatorHandler extends FrontendHandler {

	private Calculator				calculator;
	private Map<CalculatorField, String>	calculationData;
	private Category					category;

	public CalculatorHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (calculator == null)
			return HttpServletResponse.SC_BAD_REQUEST;

		try {

			calculator.evaluate(calculationData);
			setAttribute("calculationData", calculationData);

		} catch (NumberFormatException e) {
			e.printStackTrace();
			addError(e);

		} catch (Exception e) {
			e.printStackTrace();
			addError(e);
		}

		return new CategoryHandler(context, category).main();
	}
}
