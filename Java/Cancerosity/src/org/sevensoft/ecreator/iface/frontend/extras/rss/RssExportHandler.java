package org.sevensoft.ecreator.iface.frontend.extras.rss;

import java.io.IOException;
import java.io.StringWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.extras.rss.export.RssExport;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.StringResult;
import org.sevensoft.commons.collections.maps.MultiValueMap;

import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedOutput;

/**
 * @author sks 25 Jan 2007 18:56:57
 */
@Path("rss.do")
public class RssExportHandler extends FrontendHandler {

    private Category category;
    private Item account;
    private boolean all;
    private int limit;
    private ItemType itemType;
    private MultiValueMap<Attribute, String> attributeValues;

    public RssExportHandler(RequestContext context) {
        super(context);
    }

    @Override
    public Object main() throws ServletException {

        if (category == null) {
            return HttpServletResponse.SC_BAD_REQUEST;
        }

        logger.fine("[RssExportHandler] category=" + category);

        RssExport rssExport = RssExport.getInstance(context);
        SyndFeed feed = rssExport.getFeed(category);

        return getOutput(feed);
    }

    public Object accountFeed() throws ServletException {

        logger.fine("[RssExportHandler] account=" + account);

        RssExport rssExport = RssExport.getInstance(context);

        rssExport.setAttributeValues(attributeValues);
        
        if (limit == 0) {
            limit = 1000;
        }
        SyndFeed feed = rssExport.getFeed(account, all, limit, itemType);

        return getOutput(feed);
    }

    private Object getOutput(SyndFeed feed) {
        StringWriter writer = new StringWriter();
        SyndFeedOutput output = new SyndFeedOutput();

        try {

            output.output(feed, writer);
            return new StringResult(writer.toString(), "text/xml");

        } catch (IOException e) {
            e.printStackTrace();
            return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

        } catch (FeedException e) {
            e.printStackTrace();
            return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
        }
    }
}
