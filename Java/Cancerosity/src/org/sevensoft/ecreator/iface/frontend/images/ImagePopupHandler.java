package org.sevensoft.ecreator.iface.frontend.images;

import javax.servlet.ServletException;

import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.results.docs.Doc;
import org.sevensoft.jeezy.http.results.docs.SimpleDoc;

/**
 * @author sks 25 Oct 2006 21:48:11
 * 
 * This hander will display the larger image in a window and will put the captiojn underneath
 */
public class ImagePopupHandler extends Handler {

	private String	name;
	private String	filename;

	public ImagePopupHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		Doc doc = new SimpleDoc(context);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new ImageTag(filename));
				sb.append("<div>" + name + "</div>");
				return sb.toString();
			}
		});
		return doc;
	}

	@Override
	protected boolean runSecure() {
		return false;
	}

}
