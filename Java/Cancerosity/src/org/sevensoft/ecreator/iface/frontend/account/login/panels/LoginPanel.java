package org.sevensoft.ecreator.iface.frontend.account.login.panels;

import org.sevensoft.ecreator.iface.frontend.account.PasswordHandler;
import org.sevensoft.ecreator.iface.frontend.account.registration.RegistrationHandler;
import org.sevensoft.ecreator.iface.frontend.misc.FieldInputCell;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.PasswordTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 29 Dec 2006 11:40:31
 *
 */
public class LoginPanel {

	private final RequestContext	context;
	private final String		linkback;

	public LoginPanel(RequestContext context, String linkback) {
		this.context = context;
		this.linkback = linkback;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		// If you already have an account enter your email and password to login.

		sb.append(new FormFieldSet("Existing users enter your login details").setId("ec_login_existing"));

		sb.append(new FieldInputCell("Enter your email address", "Emails are not case sensitive.", new TextTag(context, "email", 30), new ErrorTag(context,
				"email", " ")));
		sb.append(new FieldInputCell("Enter your password", "Your password is between 5 and 15 characters.<br/>Passwords are case sensitive.",
				new PasswordTag(context, "password", 16), new ErrorTag(context, "password", " ")));
		sb.append("<div class='command'>" + new SubmitTag("Login") + "</div>");

		sb.append("<div class='text'>If you have forgotten your password " + new LinkTag(PasswordHandler.class, null, "Click here") + "</div>");

		sb.append("</fieldset>");

		if (ItemType.getRegistration(context).size() > 0) {

			sb.append(new FormFieldSet("I am a new user").setId("ec_login_new"));

			sb
					.append("<div class='text'>If you do not have an account then it only takes a minute to register.<br/>You will require a valid email address. "
							+ "Please click the 'Register now' button to begin.</div>");
			sb.append("<div class='command'>" + new ButtonTag(RegistrationHandler.class, null, "Register now", "linkback", linkback) + "</div>");

			sb.append("</fieldset>");

		}

		return sb.toString();
	}
}
