package org.sevensoft.ecreator.iface.frontend.items.listings;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.interaction.views.ListingStatsHandler;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionStatusHandler;
import org.sevensoft.ecreator.iface.frontend.bookings.BookingPricesHandler;
import org.sevensoft.ecreator.iface.frontend.bookings.ViewBookingsHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.listings.Listing;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;
import java.util.List;

/**
 * @author sks 19-Aug-2005 07:10:43
 */
@Path({"listings.do", "member-listings.do"})
public class MyListingsHandler extends SubscriptionStatusHandler {

    private Item item;

    public MyListingsHandler(RequestContext context) {
        super(context);
    }

    public Object delete() throws ServletException {

        if (account == null) {
            return new LoginHandler(context).main();
        }

        if (item == null)
            return main();

        // check this is our listing to delete
        if (!item.getAccount().equals(account)) {
            return main();
        }

        item.delete();
        return main();
    }

    public Object disable() throws ServletException {

        if (account == null) {
            return new LoginHandler(context).main();
        }

        if (item == null)
            return main();

        // check this is our listing to delete
        if (!item.getAccount().equals(account)) {
            return main();
        }

        item.setStatus("Disabled");
        item.save();

        return main();

    }

    /**
     * Shows the list of listings.
     */
    public Object main() throws ServletException {

        /*
           * If listings module is disabled then exit
           */
        if (!Module.Listings.enabled(context)) {

            logger.fine("[ListingsHandler] listings disabled, exiting");
            return new CategoryHandler(context).main();
        }

        /*
           * We must be logged in to do listings
           */
        if (account == null) {

            logger.fine("[ListingsHandler] not logged in");
            return new LoginHandler(context, new Link(MyListingsHandler.class)).main();
        }

        /*
           * Get a list of existing listings
           */
        final List<Item> items = account.getItemsOrdered("name");

        FrontendDoc doc = new FrontendDoc(context, captions.getListingsCaption());
        if (account.getItemType().getAccountModule().isShowAccount()) {
            doc.addTrail(AccountHandler.class, captions.getAccountCaption());
        }
        doc.addTrail(MyListingsHandler.class, captions.getListingsCaption());
        doc.addBody(new Body() {

            @Override
            public String toString() {
                sb.append(new MessagesTag(context));
                
                int span = 6;

                sb.append(new TableTag("form").setCaption(captions.getListingsCaption()).setId("ec_account_listings"));

                sb.append("<tr class=listing_header>");
                sb.append("<th>ID</th>");
                sb.append("<th>Name</th>");
                sb.append("<th>Status</th>");
                sb.append("<th>Details</th>");
                sb.append("<th>Expire</th>");
                sb.append("<th>Images</th>");
                if (Module.ListingFrontendStats.enabled(context)) {
                    sb.append("<th>Stats</th>");
                }
                sb.append("</tr>");

                if (items.size() > 0) {

                    int n = 0;
                    for (Item item : items) {

                        Listing listing = item.getListing();
                        if (!listing.hasListingPackage()) {
                            continue;
                        }

                        boolean listingUpdate = listing.getListingPackage().isListingUpdate();

                        n++;

                        sb.append("<tr>");
                        sb.append("<td>" + item.getId() + "</td>");

                        // name and category
                        sb.append("<td valign='top'><strong>" + new LinkTag(item.getUrl(), item.getName()).toString() + "</strong><br/>");
                        sb.append("<span class='small'>" + item.getCategoryNames() + "</span></td>");

                        // Status column
                        if (listing.getItem().getStatus().equals("LIVE")) {
                            sb.append("<td valign='top'>" + new ImageTag("files/graphics/pics/status_ok.png") + "<br/>");
                        }
                        else {
                            sb.append("<td valign='top'>" + new ImageTag("files/graphics/pics/status_x.png") + "<br/>");
                        }

                        /*
                               * If the item is disabled then we want to show options to list it
                               */
                        if (item.isDisabled() && item.isAwaitingModeration()) {

                            sb.append("<span class='small'>" + new LinkTag(ListingPaymentHandler.class, null, "List item", "item", item) +
                                    "</span>");

                            /*
                                    * Otherwise if the item will expire then we want to show option to extend it
                                    * but only if the item has payable listing types. We don't want to be able to extend a free listing until it has
                                    * expired (then it will be covered by the above).
                                    */
                        } else if (listing.isExpiring()) {

                            sb.append("<span class='small'>" + new LinkTag(ListingPaymentHandler.class, null, "Extend listing", "item", item) +
                                    "</span>");
                        }

                        sb.append("</td>");

                        // links to edit screens
                        if (listing.getItem().getStatus().equals("LIVE") && listingUpdate || Module.ListingModeration.enabled(context)) {
                            sb.append("<td valign='top'>" + new LinkTag(EditListingHandler.class, null, new ImageTag("files/graphics/pics/details.png"), "item", item));
                        }
                        else /*if (listing.getItem().getStatus().equals("DISABLED"))*/ {
                            sb.append("<td valign='top'>" + new ImageTag("files/graphics/pics/details_grey.png"));
                        }

                        sb.append(" ");

                        /*if (listing.getListingPackage().isImages()) {
                            sb.append(new LinkTag(EditListingHandler.class, "editImages", "Edit images", "item", item));
                            sb.append(" ");
                        }*/

                        // delete link
                        LinkTag delete = new LinkTag(MyListingsHandler.class, "delete", "Delete", "item", item);
                        delete.setConfirmation("Are you sure you want to delete this listing?");
                        delete.setClass("listingstatusdelete");
                        sb.append(delete);

                        sb.append(" ");

                        LinkTag changeStatus;
                        if (item.getStatus().equalsIgnoreCase("Live")) {
                            changeStatus = new LinkTag(MyListingsHandler.class, "disable", "Disable", "item", item);
                            changeStatus.setClass("listingstatusdisable");
                            changeStatus.setConfirmation("Are you sure you want to disable this listing?");
                        } else {
                            changeStatus = new LinkTag(MyListingsHandler.class, "setLive", "Set Live", "item", item);
                            changeStatus.setClass("listingstatussetlive");
                        }

                        sb.append(changeStatus);

                        if (item.isBookable()) {
                            sb.append(" ");
                            sb.append(new LinkTag(ViewBookingsHandler.class, null, "View bookings", "item", item));
                            sb.append(" ");
                            sb.append(new LinkTag(BookingPricesHandler.class, null, "Set prices", "item", item));
                        }


                        sb.append("</td>");

                        sb.append("<td>");
                        if (item.getListing().getExpiryDate() != null) {
                            sb.append(item.getListing().getExpiryDate().toString("dd-MMM-yyyy"));
                        }
                        sb.append("</td>");

                        //Images column
                        if (listing.getItem().getStatus().equals("LIVE") && listingUpdate || Module.ListingEdit.enabled(context)) {
                            sb.append("<td valign='top'>" + new LinkTag(EditListingHandler.class, "editImages", new ImageTag("files/graphics/pics/images.png"), "item", item));
                        }
                        else /*if (listing.getItem().getStatus().equals("DISABLED"))*/ {
                            sb.append("<td valign='top'>" + new ImageTag("files/graphics/pics/images_grey.png"));
                        }

                        //Stats column
                        if (Module.ListingFrontendStats.enabled(context)) {
                            LinkTag link = new LinkTag(ListingStatsHandler.class, null, new ImageTag("files/graphics/pics/stats.png"), "item", item);
                            link.setTarget("_blank");
                            sb.append("<td vailign='top'>" + link + "</td>");
                        }
                        sb.append("</tr>");
                    }

                } else {

                    sb.append("<tr><td colspan='" + span + "'>You have no listings yet.</td></tr>");

                }

                // CREATE
                if (account.hasListingPackages()) {
                    sb.append("<tr><td colspan='" + span + "'>Create a new listing " +
                            new LinkTag(AddListingHandler.class, null, new ImageTag("files/graphics/pics/start.png")) + "</td></tr>");
                }
                sb.append("</table>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object setLive() throws ServletException {

        if (account == null) {
            return new LoginHandler(context).main();
        }

        if (item == null)
            return main();

        // check this is our listing to delete
        if (!item.getAccount().equals(account)) {
            return main();
        }

        // check if it was payed
        if (item.getListing().getListingPackage()!=null && item.getListing().getListingPackage().hasRates()) {
            if (!item.isListingPaymentDone()){
                addError("The Listing has not been paid for");
                return new ExternalRedirect(new Link(AddListingHandler.class, "payment", "item", item, "addVoucher", true));
//                return new ListingPaymentHandler(context, item).main();
//                return main();

            }
        }

        item.setStatus("Live");
        item.save();

        return main();

    }

}
