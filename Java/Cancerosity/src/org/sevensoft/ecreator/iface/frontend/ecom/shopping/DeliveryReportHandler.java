package org.sevensoft.ecreator.iface.frontend.ecom.shopping;

import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.ecreator.iface.EcreatorHandler;
import org.sevensoft.ecreator.iface.admin.items.modules.stock.ReportDoc;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.system.config.Config;

import javax.servlet.ServletException;

/**
 * @author Dmitry Lebedev
 *         Date: 26.01.2010
 */
@Path("print-order.do")
public class DeliveryReportHandler extends EcreatorHandler {

    private Basket basket;

    public DeliveryReportHandler(RequestContext context) {
        super(context);
    }

    public DeliveryReportHandler(RequestContext context, Basket basket) {
        super(context);

        this.basket = basket;
    }

    public Object main() throws ServletException {

        ReportDoc doc = new ReportDoc(context, "Checkout - details");
        doc.addBody(new Body() {

            @Override
            public String toString() {
                sb.append(new TableTag("form"));
                sb.append("<tr><td>Our addres:");
                sb.append(PaymentSettings.getInstance(context).getChequeAddress() + "</td></tr>");
                sb.append("<tr bgcolor='grey'><td colspan=2><b>Your details:</b></td></tr>");
                sb.append("<tr ><td><b>First name:</b></td><td>" + basket.getName().split(" ")[0] + "</td></tr>");
                sb.append("<tr ><td><b>Surname:</b></td><td>" + basket.getName().split(" ")[1] + "</td></tr>");
                sb.append("<tr ><td><b>Address:</b></td><td>" + basket.getBillingAddress().getAddressLine1() + " " + basket.getBillingAddress().getAddressLine2() + " " +
                        basket.getBillingAddress().getAddressLine3() + "</td></tr>");
                sb.append("<tr ><td><b>City:</b></td><td>" + basket.getBillingAddress().getTown() + "</td></tr>");
                sb.append("<tr ><td><b>Post Code:</b></td><td>" + basket.getBillingAddress().getPostcode() + "</td></tr>");
                sb.append("<tr ><td><b>Country:</b></td><td>" + basket.getBillingAddress().getCountry() + "</td></tr>");
                sb.append("<tr ><td><b>Telephone(home):</b></td><td>" + basket.getBillingAddress().getTelephone() + "</td></tr>");
                sb.append("<tr ><td><b>Mobile:</b></td><td>" + basket.getBillingAddress().getTelephone1() + "</td></tr>");
                sb.append("<tr bgcolor='grey'><td colspan=2><b>Order Details:</b></td></tr>");

                for (BasketLine line : basket.getLines()) {

                    sb.append("<tr>");
                    sb.append("<td>" + line.getFullDescription());
                    sb.append("</td>");
                    sb.append("<td valign='top'>" + line.getQty() + " @ \243" + line.getSalePriceInc() + "</td>");
                    sb.append("</tr>");
                }

                sb.append("<tr><th colspan='2'><b>Total to pay</b></th><tr>");

                sb.append("<tr>");
                sb.append("<td>Subtotal</td>");
                sb.append("<td>\243" + basket.getTotalEx() + "</td>");
                sb.append("</tr>");
                sb.append("<tr>");
                sb.append("<td>Total</td>");
                sb.append("<td>\243" + basket.getTotalInc() + "</td>");
                sb.append("</tr>");

                sb.append("<tr><td colspan=2>");
                sb.append(new LinkTag("JavaScript:window.print();", "Print report"));
                sb.append("</tr></tr>");

                sb.append("</table>");
                return sb.toString();
            }
        });

        return doc;
    }

    protected boolean runSecure() {
        return Config.getInstance(context).isSecured();
    }
}
