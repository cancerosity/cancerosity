package org.sevensoft.ecreator.iface.frontend.items.listings;

import java.util.Collection;
import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.ErrorDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.payments.panels.PaymentTable;
import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.ecreator.model.ecom.payments.PaymentRedirect;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.listings.ListingRate;
import org.sevensoft.ecreator.model.items.listings.emails.ListingAddedEmail;
import org.sevensoft.ecreator.model.items.listings.sessions.ListingPaymentSession;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.config.Captions;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 26 May 2006 16:26:34
 *
 */
@Path("listings-payment.do")
public class ListingPaymentHandler extends FrontendHandler {

	private static String			Title;

	private PaymentType				paymentType;
	private Item					item;
	private ListingRate				listingRate;
	private transient ListingPaymentSession	session;
	private transient PaymentSettings		paymentSettings;
	private boolean					pay;

	private boolean					editRate;

	private boolean					editPaymentType;

	public ListingPaymentHandler(RequestContext context) {
		this(context, null);
	}

    public ListingPaymentHandler(RequestContext context, Item item) {
        super(context);
        this.item = item;
        this.session = ListingPaymentSession.get(context);
        this.paymentSettings = PaymentSettings.getInstance(context);
        Title = captions.getListingsCaption() + " payment";
    }

	private Object askPaymentType() throws ServletException {

		/*
		 * Get online payment types - instant payment only.
		 */
		final Collection<PaymentType> paymentTypes = paymentSettings.getCardPaymentTypes();
		if (paymentTypes.isEmpty()) {
			return new ErrorDoc(context, "No payment types configured for listing package");
		}

		/*
		 * If we only have one payment type we don't need to show the selection screen
		 */
		if (paymentTypes.size() == 1) {

			paymentType = paymentTypes.iterator().next();

			logger.fine("[ListingPaymentHandler] single payment type, setting=" + paymentType);
			return setPaymentType();

		} else {

			FrontendDoc doc = new FrontendDoc(context, Title);
            doc.addTrail(MyListingsHandler.class, null, "My " + captions.getListingsCaption());
			doc.addTrail(ListingPaymentHandler.class, "process", Title, "editPaymentType", true);
			doc.addBody(new Body() {

				@Override
				public String toString() {

					sb.append(new PaymentTable(context, paymentTypes, ListingPaymentHandler.class, "setPaymentType"));
					return sb.toString();
				}

			});
			return doc;
		}
	}

	private Object askRates() throws ServletException {

		/* 
		 * get list of rates applicable for this item
		 */
		final List<ListingRate> rates = session.getItem().getListing().getListingPackage().getListingRates();
		logger.fine("[ListingPaymentHandler] available rates for package: " + rates);

		/*
		 * If no rates are defined for the package on this item then we should exit, as we should
		 * not even be in the listing payment handler
		 */
		if (rates.isEmpty()) {
			return new ErrorDoc(context, "No listing rates defined for the package on this item");
		}

		/*
		 * If only one rate is available then we should use that!
		 */
		if (rates.size() == 1) {

			listingRate = rates.iterator().next();

			logger.fine("[ListingPaymentHandler] single rate detected, defaulting to that=" + listingRate);
			return setListingRate();
		}

		/*
		 * Otherwise show selection table
		 */
		FrontendDoc doc = new FrontendDoc(context, Title);
		doc.addTrail(MyListingsHandler.class, null, "My " + captions.getListingsCaption());
		doc.addTrail(ListingPaymentHandler.class, "process", Title, "editRate", true);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(ListingPaymentHandler.class, "setListingRate", "GET"));

				sb.append("Choose a rate from the list below.<br/>");

				SelectTag tag = new SelectTag(context, "listingRate");
				tag.setAny("-Please choose-");
				tag.addOptions(rates);

				sb.append(tag + " " + new SubmitTag("Continue"));
				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object completed() throws ServletException {
        item = session.getItem();

        item.setListingPaymentDone(true);
        item.setStatus("Live");

        if (item.getListing().getListingPackage().hasForwardUrl()) {
            return new ExternalRedirect(item.getListing().getListingPackage().getForwardUrl());
        }

        if (!item.isAwaitingModeration() && !item.isAwaitingValidation()){
            sendCompletedNotification(context, item, session.getAccount());
        }

        if (paymentType != null) {

			logger.fine("[ListingPaymentHandler] payment callback, paymentType=" + paymentType);
			paymentType.inlineCallback(context, getParameters(), getRemoteIp());
		}


        FrontendDoc doc = new FrontendDoc(context, captions.getListingsCaption() + " payment successful");
        doc.addTrail(MyListingsHandler.class, null, "My " + captions.getListingsCaption());
        doc.addTrail(MyListingsHandler.class, null, captions.getListingsCaption() + " payment successful", "item", item);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(session.getListingRate().getListingPackage().getCompletedMessage());

				sb.append("<br/><br/>");

				if (item.isAwaitingModeration()) {

					sb.append("Your listing will not be visible until it has been approved by our moderation team. "
							+ "You will be sent an email as soon as it goes live.");

				} else {

					sb.append(new LinkTag(ItemHandler.class, null, "Click here", "item", item));
					sb.append(" to view your listing.<br/>");

				}

                LinkTag clickHere = new LinkTag(AddListingHandler.class, null, "click here");
                clickHere.setTitle("Add another listing");

                sb.append("<strong>Please " + clickHere + " if you wish to add another listing.</strong> ");

				return sb.toString();
			}
		});
		return doc;
	}

    private void sendCompletedNotification(RequestContext context, Item item, Item account) {
        if (account != null && account.hasEmail()) {
            Thread t = new Thread(new ListingAddedEmail(context, item, account));
            t.setDaemon(true);
            t.start();
        }
    }

	private Object confirmation() {

		logger.fine("[ListingPaymentHandler] showing confirmation page");

		/*
		 * Otherwise show confirmation page.
		 */
		FrontendDoc doc = new FrontendDoc(context, Title);
		doc.addTrail(MyListingsHandler.class, null, "My " + captions.getListingsCaption());
		doc.addTrail(ListingPaymentHandler.class, "process", Title);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("form", 1, 0).setCaption("Confirm payment").setWidth("100%"));

				sb.append(new FormTag(ListingPaymentHandler.class, "process", "post"));
				sb.append(new HiddenTag("pay", true));

				sb.append("<tr><td colspan='2'>Please confirm the details below. "
						+ "If you are happy to continue click the 'make payment' button.</td></tr>");

				sb.append("<tr><td class='key' valign='top' align='right' width='180'>Cost</td><td class='input'>"
						+ session.getListingRate().getDescription());

                if (session.getItem().getListing().hasVoucher()) {
                    sb.append("<tr><td class='key' valign='top' align='right' width='180'>Discount</td><td class='input'>"
                            + session.getItem().getListing().getVoucher().getDiscount());
                }

				// if more than one listing rate then show link to change
				if (session.getListingRates().size() > 1) {
					sb.append("<br/>");
					sb.append(new LinkTag(ListingPaymentHandler.class, "removeListingRate", "Change rate"));
				}

				sb.append("</td></tr>");

				if (session.getListingRate().hasFee()) {

					sb.append("<tr><td class='key' valign='top' align='right' width='180'>Payment method</td><td class='input'>"
							+ session.getPaymentType().getPublicName());

					if (paymentSettings.getCardPaymentTypes().size() > 1) {

						sb.append("<br/>");
						sb.append(new LinkTag(ListingPaymentHandler.class, "removePaymentType", "Change payment method"));
					}

					sb.append("</td></tr>");
				}

				sb.append("<tr><td class='key' width='180'></td><td class='input'>");
				sb.append(new SubmitTag("Make payment"));
				sb.append(" ");
                sb.append(new ButtonTag(MyListingsHandler.class, null, "Cancel", "item", session.getItem()).
                        setConfirmation("Are you sure you do not wish to proceed with payment?").setClass("cancelpayment"));
				sb.append("</td></tr>");

				sb.append("</form>");
				sb.append("</table>");

				return sb.toString();
			}
		});
		return doc;
	}

	/**
	 * Reset payment process and then start anew
	 */
	@Override
	public Object main() throws ServletException {

		if (item == null) {
			return new CategoryHandler(context).main();
		}

		session.delete();
		session = new ListingPaymentSession(context, true);

		session.setItem(item);
		session.save();

		return process();
	}

	public Object process() throws ServletException {

		/*
		 * If listings module is disabled then exit
		 */
		if (!Module.Listings.enabled(context)) {

			logger.fine("[ListingPaymentHandler] listings module is disabled, escaping to index");
			return index();
		}

		/*
		 * If the item is not set then exit out, this should not occur normally - perhaps the item was deleted by an admin ?
		 */
		if (!session.hasItem()) {
			logger.fine("[ListingPaymentHandler] session does not have an item set, escaping to index");
			return index();
		}

		/*
		 * If this item is a lifetime listing then we should exit as there is no point paying
		 * any further ! 
		 */
		if (session.getItem().getListing().isLifetimeListing()) {

			logger.fine("[ListingPaymentHandler] this item is already set to be a lifetime listing, exiting to listings handler");
			return new MyListingsHandler(context).main();
		}

		/*
		 * Update account on session to account holder of item
		 */
		if (!session.getItem().hasAccount()) {

			logger.fine("[ListingPaymentHandler] this item does not have an account on it, exiting");
			return new MyListingsHandler(context).main();
		}

		logger.fine("[ListingPaymentHandler] setting account holder on session, account=" + session.getItem().getAccount());
		session.setAccount(session.getItem().getAccount());

		/*
		 * If this item has no package set then it is not a listing so we should exit
		 */
		if (!session.getItem().getListing().hasListingPackage()) {

			logger.fine("[ListingPaymentHandler] no listing package is set on this item, therefore we can't do anything!");
			return new MyListingsHandler(context).main();
		}

		/*
		 * If no rate set yet then show rates selection
		 */
		if (!session.hasListingRate()) {

			logger.fine("[ListingPaymentHandler] no rate selected, showing rates screen");
			return askRates();
		}

		if (editRate) {

			logger.fine("[ListingPaymentHandler] user asked to edit rate, showing rates screen");
			return askRates();
		}

		/*
		 * If the rate selected is free then list item with it !
		 */
		if (session.getListingRate().isFree()) {

			logger.fine("[ListingPaymentHandler] rate selected is free, listing without payment");

			session.list(null, getRemoteIp());
			return completed();
		}

        if (session.getPaymentAmount().isZero() || session.getPaymentAmount().isNegative()) {
            logger.fine("[ListingPaymentHandler] rate to pay inc voucher is free, listing without payment");

            session.list(null, getRemoteIp());
            return completed();
        }

		/*
		 * Rate is not free, so If no payment set then we should show payment screen
		 */
		if (!session.hasPaymentType()) {

			logger.fine("[ListingPaymentHandler] rate requires payment, no payment type set, showing ask payment type screen");
			return askPaymentType();
		}

		if (editPaymentType) {

			logger.fine("[ListingPaymentHandler] user asked to edit Payment type, showing payments screen");
			return askRates();
		}

		if (pay) {

			logger.fine("[ListingPaymentHandler] making payment, forwarding to form");

			/*
			 * Forward us to payment provider
			 */
			return new PaymentRedirect(context, session);

		} else {

			return confirmation();

		}
	}

	public Object setListingRate() throws ServletException {

		logger.fine("[ListingPaymentHandler] setting listing rate=" + listingRate);

		session.setListingRate(listingRate);
		session.save();

		return process();
	}

	public Object setPaymentType() throws ServletException {

		session.setPaymentType(paymentType);
		session.save();

		return process();
	}

    public Object removeListingRate() throws ServletException {
        return askRates();
    }

    public Object removePaymentType() throws ServletException {
        return askPaymentType();
    }
}
