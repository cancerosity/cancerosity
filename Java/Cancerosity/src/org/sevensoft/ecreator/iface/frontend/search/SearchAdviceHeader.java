package org.sevensoft.ecreator.iface.frontend.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Results;

/**
 * @author sks 11 Mar 2007 11:04:52
 *
 */
public class SearchAdviceHeader extends Panel {

	private static List<String>	filterCookies, resultCookies;

	static {

		filterCookies = new ArrayList();
		filterCookies.add("The wise oracle, he say, 'Use the filters on the left to narrow down your search!'");
		filterCookies.add("So many choices, but so much control with our search filters on the left!");
		filterCookies.add("Confused? Who wouldn't be with so many results. Narrow down with the filters on the left.");
		filterCookies.add("Our artful filters will strip away the rising damp from the landfill sites.");
	}

	private final Results		results;

	public SearchAdviceHeader(RequestContext context, Results results) {
		super(context);
		this.results = results;
	}

	@Override
	public void makeString() {

		sb.append("<div class='ec_search_advice'>");
		sb.append("<div class='advice1'>Wow! that's a popular search. You've found ");
		sb.append(results.getTotalResults());
		sb.append(" results.</div>");
		sb.append("<div class='advice2'>");

		Collections.shuffle(filterCookies);
		sb.append(filterCookies.get(0));

		sb.append("</div>");
		sb.append("</div>");
	}

}
