package org.sevensoft.ecreator.iface.frontend.marketing;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.net.EmailValidator;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.marketing.panels.NewsletterSubscribePanel;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterControl;
import org.sevensoft.ecreator.model.marketing.newsletter.Subscriber;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterOption;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 25-May-2004 15:30:26
 */
@Path( { "nl.do", "newsletter.do" })
public class NewsletterHandler extends FrontendHandler {

	private String					e;
	private List<Newsletter>			newsletters;
    private List<NewsletterOption>			newsletterOptions;
	private String					q;
	private String					c;
	private MultiValueMap<Attribute, String>	attributeValues;
	private final transient NewsletterControl	newsletterControl;
	private Category					category;

	public NewsletterHandler(RequestContext context) {
		super(context);
		this.newsletterControl = NewsletterControl.getInstance(context);
	}

	public Object edit() throws ServletException {

		if (c == null || e == null) {
			addError("Invalid link. Please try again");
			return main();
		}

		final Subscriber s = Subscriber.get(context, e, false);

		// check subscriber exists
		if (s == null) {
			logger.fine("no subscriber found for email=" + e);
			addError("Invalid link. Please try again");
			return main();
		}

		// check code is correct
		if (!s.getCode().equals(c)) {
			logger.fine("no subscriber found for email=" + e);
			addError("Invalid link. Please try again");
			return main();
		}

		final List<Newsletter> newsletters = Newsletter.get(context);
		if (newsletters.isEmpty()) {
			logger.fine("no newsletters created");
			return main();
		}

		FrontendDoc doc = new FrontendDoc(context, "Newsletter subscription preferences");
		doc.addTrail(NewsletterHandler.class, "Newsletter");
		doc.addTrail(NewsletterHandler.class, "edit", "Subscription preferences", "c", c, "e", e);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(NewsletterHandler.class, "save", "post"));
				sb.append(new HiddenTag("c", c));
				sb.append(new HiddenTag("e", e));

				sb.append("Welcome, ");
				sb.append(e);
				sb.append("<br/><br/>");

				if (newsletters.size() == 1) {

					sb.append("Click the unsubscribe button if you wish to leave our newsletter.<br/>");
					sb.append(new SubmitTag("Unsubscribe"));

				} else {

					sb.append("This screen allows you to control your newsletter subscriptions.<br/>");
					sb.append("Please tick the box next to the newsletter if you wish to be subscribed.<br/><br/>");

					for (Newsletter newsletter : newsletters) {

						sb.append(new CheckTag(context, "newsletters", newsletter, newsletter.hasSubscriber(s)));
						sb.append(" ");
						sb.append(newsletter.getName());
						sb.append("<br/>");

					}

					sb.append("<br/><br/>");
					sb.append(new SubmitTag("Update"));
				}

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	/**
	 * Subscription page where users enter their email and choose newsletters
	 */
	@Override
	public Object main() throws ServletException {

		FrontendDoc doc = new FrontendDoc(context, newsletterControl.getName());
		doc.addTrail(NewsletterHandler.class, newsletterControl.getName());
		doc.addBody(new NewsletterSubscribePanel(context, attributeValues));
		return doc;
	}

	public Object subscribe() throws ServletException {

		if (newsletters.isEmpty()) {
			newsletters = Newsletter.get(context);
		}

		AttributeUtil.setErrors(context, newsletterControl.getAttributes(), attributeValues);

		test(new RequiredValidator(), "e");
		test(new EmailValidator(), "e");
		if (hasErrors()) {
			return main();
		}

		final Subscriber sub = newsletterControl.subscribe(newsletters, e, attributeValues);
        newsletterControl.subscribeOpt(newsletterOptions, e, attributeValues);

		FrontendDoc doc = new FrontendDoc(context, newsletterControl.getName());
		doc.addTrail(NewsletterHandler.class, newsletterControl.getName());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("Thank you, we have added you to our newsletter(s).<br/>");
				if (sub.isValidating()) {
					sb.append("We have sent you an email that contains a link, which you must click to confirm your email address.");
				}

				return sb.toString();
			}
		});
		return doc;
	}

	/**
	 * Validate subscribers account
	 */
	public Object v() {

		final String message;
		if (c == null) {

			message = "Invalid link. Please try again";

		} else if (newsletterControl.validate(e, c)) {

			message = "Your newsletter subscription has been validated, thank you.";

		} else {

			message = "The code you entered was invalid, please try clicking the link again.";

		}

		FrontendDoc doc = new FrontendDoc(context, "Newsletter validation");
		doc.addTrail(NewsletterHandler.class, null, "Newsletter");
		doc.addTrail(NewsletterHandler.class, "v", "Validation", "c", c, "e", e);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(message);

				return sb.toString();
			}
		});
		return doc;
	}

//	public Object sendCode() throws ServletException {
//
//		test(new RequiredValidator(), "e");
//		test(new EmailValidator(), "e");
//
//		if (hasErrors()) {
//			return edit();
//		}
//
//		try {
//
//			newsletterControl.sendAccessCode(e);
//
//			FrontendDoc doc = new FrontendDoc(context, newsletterControl.getName());
//			doc.addTrail(NewsletterHandler.class, newsletterControl.getName());
//			doc.addBody(new Body() {
//
//				@Override
//				public String toString() {
//
//					sb.append("An email has been sent to you. When you receive it please click the link.");
//					return sb.toString();
//				}
//			});
//			return doc;
//
//		} catch (EmailAddressException e) {
//			e.printStackTrace();
//		} catch (SmtpServerException e) {
//			e.printStackTrace();
//		}
//
//		return edit();
//	}

	public Object save() throws ServletException {

		if (c == null || e == null) {
			return main();
		}

		Subscriber s = Subscriber.get(context, e, false);
		if (s == null) {
			return main();
		}

		s.unsubscribe();
		s.subscribe(newsletters);

		// dlete sub if not in any newsletters
		if (s.getNewsletters().isEmpty()) {
			s.delete();
		}

		FrontendDoc doc = new FrontendDoc(context, "Newsletter preferences saved");
		doc.addTrail(NewsletterHandler.class, null, "Newsletter");

		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("Thank you, your newsletter preferences have been updated.<br/>");

				if (newsletters.size() > 1) {
					sb.append(new LinkTag(NewsletterHandler.class, "edit", "Click here", "c", c));
					sb.append(" if you wish to edit them again.");
				}

				return sb.toString();
			}
		});
		return doc;
	}

}