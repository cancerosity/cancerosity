package org.sevensoft.ecreator.iface.frontend.ecom.shopping.panels;

import java.util.List;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Transformer;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.admin.ecom.payments.cards.panels.CardEntryPanel;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.BasketHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.CheckoutHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.DeliveryReportHandler;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.iface.frontend.misc.FieldInputCell;
import org.sevensoft.ecreator.iface.frontend.misc.agreements.panels.InlineAgreementPanel;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.values.AValueRenderer;
import org.sevensoft.ecreator.model.design.marker.MarkerRenderer;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.ecom.shopping.ShoppingSettings;
import org.sevensoft.ecreator.model.ecom.vouchers.Voucher;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterControl;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterOptInPanel;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.bespoke.ipoints.IPointsSettings;
import org.sevensoft.ecreator.model.bespoke.ipoints.IPointsService;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ImageSubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.text.PasswordTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.links.ButtonOpenerTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 13 Mar 2007 15:10:18
 *
 */
public class DeliveryConfirmationPanel extends Panel {

	private final Basket			basket;
	private final ShoppingSettings	shoppingSettings;
	private final Item			account;
	private final OrderSettings		orderSettings;
	private final Company			company;
	private final NewsletterControl	newsletterControl;
	private final Config			config;

	public DeliveryConfirmationPanel(RequestContext context, Basket basket) {
		super(context);

		this.basket = basket;
		this.shoppingSettings = ShoppingSettings.getInstance(context);
		this.account = basket.getAccount();
		this.company = Company.getInstance(context);
		this.orderSettings = OrderSettings.getInstance(context);
		this.config = Config.getInstance(context);
		this.newsletterControl = NewsletterControl.getInstance(context);
	}

	private void addresses() {

		if (shoppingSettings.isSeparateAddresses()) {

			sb.append("<table width='100%'><tr><td width='50%'>");
			deliveryAddress();
			sb.append("</td><td width='50%'>");
			billingAddress();
			sb.append("</td></tr></table>");

		} else {

			deliveryAddress();
		}
	}

	/**
	 * 
	 */
	private void agreement() {
		if (shoppingSettings.hasAgreement()) {
			sb.append(new InlineAgreementPanel(context, shoppingSettings.getAgreement()));
		}
	}

	private void billingAddress() {

		sb.append(new TableTag("form").setCaption("Billing address"));

		sb.append("<tr><td>" + basket.getBillingAddress().getLabel("<br/>", true) + "</td></tr>");
		sb.append("<tr><td>" + new LinkTag(CheckoutHandler.class, "process", "Change billing address", "editBillingAddress", true) + "</td></tr>");

		sb.append("</table>");
	}

	private void deliveryAddress() {

		sb.append(new TableTag("form").setCaption("Delivery address"));

		sb.append("<tr><td>" + basket.getDeliveryAddress().getLabel("<br/>", true) + "</td></tr>");
		sb.append("<tr><td>" + new LinkTag(CheckoutHandler.class, "process", "Change delivery address", "editDeliveryAddress", true) + "</td></tr>");

		sb.append("</table>");
	}

	private void extra() {

		sb.append(new TableTag("form").setCaption("Extra details"));

		/*
		 * Customer reference
		 */
		if (shoppingSettings.isCustomerReference()) {

			sb.append("<tr>");
			sb.append("<td>Your reference</td><td>");
			sb.append(new TextTag(context, "customerReference", basket.getCustomerReference(), 20));
			sb.append("</td></tr>");
		}

		for (Attribute attribute : basket.getAttributes()) {

			sb.append("<tr>");
			sb.append("<td>" + attribute.getName() + "</td><td>");

			if (basket.hasAttributeValue(attribute)) {

				sb.append(new AValueRenderer(context, account, attribute, basket.getAttributeValues(attribute)));

			} else {

				sb.append("- -");

			}

			sb.append("</td></tr>");

		}

		sb.append("<tr><td></td><td>");
		sb.append(new LinkTag(CheckoutHandler.class, "process", "Change extra details", "editAttributes", true));
		sb.append("</td></tr>");

		sb.append("</table>");
	}

    private void ipoints() {

        sb.append(new FormFieldSet("Your iPoints account"));

        IPointsService is = new IPointsService(context);
        int points = is.getPoints(basket.getTotalInc());

        sb
                .append("<div class='text'>If you have an ipoints account enter your username and password here so you can earn points on your purchases. This order will earn "
                        + points + " points!</div>");

        sb.append(new FieldInputCell("Username", "Enter your ipoints username.", new TextTag(context, "ipointsUsername", 30), new ErrorTag(context,
                "ipointsUsername", " ")));

        sb.append(new FieldInputCell("Password", "Enter your ipoints password.", new PasswordTag(context, "ipointsPassword", 30), new ErrorTag(context,
                "ipointsPassword", " ")));

        sb.append("</fieldset>");

   }

    private void lines() {

		sb.append(new TableTag("form").setCaption("Order details"));

		for (BasketLine line : basket.getLines()) {

			sb.append("<tr>");
			sb.append("<td>" + line.getFullDescription());
			if (line.hasAttachments()) {

				List<String> filenames = CollectionsUtil.transform(line.getAttachments(), new Transformer<Attachment, String>() {

					public String transform(Attachment obj) {
						return obj.getName();
					}
				});

				sb.append("<div class='attachments'>Attachments: " + StringHelper.implode(filenames, ", ", true) + "</div>");
			}

			sb.append("</td>");
			sb.append("<td valign='top'>" + line.getQty() + " @ \243" + line.getSalePriceInc() + "</td>");
			sb.append("</tr>");
		}

		sb.append("<tr>");
		sb.append("<td></td>");
		sb.append("<td>" + new LinkTag(BasketHandler.class, null, "Return to basket") + "</td>");
		sb.append("</tr>");

		if (Module.Delivery.enabled(context)) {

			if (basket.hasDeliveryOption()) {

				sb.append("<tr>");
				sb.append("<td>" + basket.getDeliveryOption().getDescription() + "</td>");
				sb.append("<td>\243" + basket.getDeliveryChargeEx() + "</td>");
				sb.append("</tr>");

			}

			// only show change option if we have more than one delivery option.
			if (basket.getDeliveryOptions().size() > 1) {

				sb.append("<tr>");
				sb.append("<td></td>");
				sb.append("<td>" + new LinkTag(CheckoutHandler.class, "process", "Change delivery method", "editDeliveryOption", true) + "</td>");
				sb.append("</tr>");

			}
		}

		// VOUCHER
		if (Voucher.hasVouchers(context)) {

			sb.append("<tr>");
			sb.append(new FormTag(CheckoutHandler.class, "setVoucher", "POST"));

			sb.append("<td>");

			if (basket.hasVoucher()) {

				sb.append(basket.getVoucher().getName() + " (" + basket.getVoucher().getCode() + ")");
				sb.append("<br/>To remove this voucher, or to enter a different code, " +
						new LinkTag(CheckoutHandler.class, "removeVoucher", "click here"));

			} else {

				sb.append("If you have a discount voucher enter the code here and click use voucher<br/>");
				sb.append(new TextTag(context, "voucherCode", 12));
				sb.append(new SubmitTag("Use voucher"));
				sb.append(new ErrorTag(context, "voucherCode", "<br/>"));
			}

			sb.append("</td>");

			sb.append("<td>");
			if (basket.hasVoucher()) {
				sb.append("-\243" + basket.getVoucherDiscountInc());
			} else {
				sb.append(" -- ");
			}
			sb.append("</td>");

			sb.append("</form>");
			sb.append("</tr>");
		}

		// TOTALS
		sb.append("<tr><th colspan='2'><b>Total to pay</b></th><tr>");

		if (company.isVatRegistered()) {

			sb.append("<tr>");
			sb.append("<td>Subtotal</td>");
			sb.append("<td>\243" + basket.getTotalEx() + "</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td>VAT</td>");
			sb.append("<td>\243" + basket.getTotalVat() + "</td>");
			sb.append("</tr>");
		}

		sb.append("<tr>");
		sb.append("<td>Total</td>");
		sb.append("<td>\243" + basket.getTotalInc() + "</td>");
		sb.append("</tr>");

		sb.append("</table>");
	}

	@Override
	public void makeString() {

		if (shoppingSettings.hasCheckoutConfirmationHeader()) {
			sb.append(MarkerRenderer.render(context, shoppingSettings.getCheckoutConfirmationHeader()));
		}

		if (shoppingSettings.isAddresses()) {
			addresses();
		}

		// ORDER LINES
		lines();

		// PAYMENT
		payment();

        sb.append(new FormTag(CheckoutHandler.class, "process", "post"));

        // ATTRIBUTES
		if (orderSettings.hasCheckoutAttributes()) {
			extra();
		}

		sb.append(new HiddenTag("place", true));

		/*
		 * Vat declaration
		 */
		if (company.isVatRegistered()) {
			vatExemption();
		}

		if (newsletterControl.isCheckoutOptIn()) {
			newsletterOptIn();
		}

        if (IPointsSettings.getInstance(context).isEnabled() && Module.Ipoints.enabled(context)) {
            ipoints();
        }

        if (basket.getPaymentType().equals(PaymentType.PayPalDirect)) {
            sb.append(new CardEntryPanel(context));
        }

        agreement();
		place();

		sb.append("</form>");

		if (shoppingSettings.hasCheckoutConfirmationFooter()) {
			sb.append(MarkerRenderer.render(context, shoppingSettings.getCheckoutConfirmationFooter()));
		}
	}

	private void newsletterOptIn() {

		List<Newsletter> newsletters = Newsletter.get(context);

		if (newsletters.size() > 0) {
			sb.append(new NewsletterOptInPanel(context, basket, newsletters));
		}
	}

	private void payment() {

		sb.append(new TableTag("form").setCaption("Payment method"));

		sb.append("<tr><td width='200' colspan='2'>"+ basket.getPaymentType().getConfirmedMessage() + "</td></tr>");

        if (basket.getPaymentType().equals(PaymentType.Cheque)) {
            sb.append("<tr><td>Our addres:");
            sb.append(PaymentSettings.getInstance(context).getChequeAddress() + "</td></tr>");
            sb.append("<tr bgcolor='grey'><td colspan=2><b>Your details</b></td></tr>");
            String[] fullName =  basket.getName().split(" ");
            if (fullName.length == 1)
                sb.append("<tr ><td><b>Name:</b></td><td>" + fullName[0] + "</td></tr>");
            if (fullName.length > 1) {
                sb.append("<tr ><td><b>First name:</b></td><td>" + fullName[0] + "</td></tr>");
                sb.append("<tr ><td><b>Surname:</b></td><td>" + fullName[1] + "</td></tr>");
            }
            sb.append("<tr ><td><b>Address:</b></td><td>" + basket.getBillingAddress().getAddressLine1() + " " + basket.getBillingAddress().getAddressLine2() + " " +
                    basket.getBillingAddress().getAddressLine3() + "</td></tr>");
            sb.append("<tr ><td><b>City:</b></td><td>" + basket.getBillingAddress().getTown() + "</td></tr>");
            sb.append("<tr ><td><b>Post Code:</b></td><td>" + basket.getBillingAddress().getPostcode() + "</td></tr>");
            sb.append("<tr ><td><b>Country:</b></td><td>" + basket.getBillingAddress().getCountry() + "</td></tr>");
            sb.append("<tr ><td><b>Telephone(home):</b></td><td>" + basket.getBillingAddress().getTelephone() + "</td></tr>");
            sb.append("<tr ><td><b>Mobile:</b></td><td>" + basket.getBillingAddress().getTelephone1() + "</td></tr>");
            sb.append("<tr><td colspan=2>");
            Link link = new Link(DeliveryReportHandler.class);
            link.addParameter("basket", basket);
            sb.append(new ButtonOpenerTag(context, link, "Print", 500, 500));
            sb.append("</td></tr>");
        }

		if (PaymentSettings.getInstance(context).getPaymentTypes().size() > 1) {
			sb.append("<tr><td>" + new LinkTag(CheckoutHandler.class, "process", "Change payment method", "editPaymentType", true) + "</td></tr>");
		}

		if (basket.hasCard()) {

			sb.append("<tr><td>Your card details</td><td>" + basket.getCard().getSecureDescription() + "</td></tr>");
			sb.append("<tr><td width='200'></td><td>" + new LinkTag(CheckoutHandler.class, "process", "Change card details", "editCard", true) +
					"</td></tr>");

		}

		if (basket.hasAble2BuyCreditGroup()) {

			sb.append("<tr><td>" + basket.getAble2BuyCreditGroup().getDescription() + "</td><td>" +
					new LinkTag(CheckoutHandler.class, "process", "Change credit rate", "editCreditGroup", true) + "</td></tr>");
		}

		sb.append("</table>");
	}

	private void place() {

		sb.append(new FormFieldSet("Place order"));

		sb.append("<div class='text'>" + HtmlHelper.nl2br(shoppingSettings.getPlaceOrderText()) + "</div>");
        String src = basket.getPaymentType().getButtonSrc(context);
        if (src != null) {
            sb.append("<div class='command'>" + new ImageSubmitTag(src) + "</div>");
        } else {
            sb.append("<div class='command'>" + new SubmitTag("Place order") + "</div>");
        }

		sb.append("</fieldset>");
	}

	private void vatExemption() {

		if (!shoppingSettings.isVatExemptionDeclaration()) {
			return;
		}

		sb.append(new FormFieldSet("Vat exemption declaration"));

		sb.append("<div class='text'>" + new CheckTag(context, "vatExempt", true, false));
		sb.append(HtmlHelper.nl2br(shoppingSettings.getVatExemptionText()));

		if (shoppingSettings.isVatExemptionDeclaration()) {
			sb.append(new TextTag(context, "vatExemptText", 20));
		}

		sb.append("</div>");
		sb.append("</fieldset>");
	}
}