package org.sevensoft.ecreator.iface.frontend.attachments;

import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionStatusHandler;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.downloads.Download;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 19-Aug-2005 07:10:43
 * 
 */
@Path("my-downloads.do")
public class MyDownloadsHandler extends SubscriptionStatusHandler {

	@SuppressWarnings("hiding")
	private static Logger	logger	= Logger.getLogger(MyDownloadsHandler.class.getName());

	public MyDownloadsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (account == null) {
			return new LoginHandler(context, new Link(MyDownloadsHandler.class)).main();
		}

		FrontendDoc doc = new FrontendDoc(context, captions.getDownloadsCaption());
		if (account.getItemType().getAccountModule().isShowAccount()) {
			doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		}
		doc.addTrail(MyDownloadsHandler.class, captions.getDownloadsCaption());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("form").setCaption(captions.getDownloadsCaptionPlural()));

				sb.append("<tr>");
				sb.append("<th>Date</th>");
				sb.append("<th>Filename</th>");
				sb.append("<th>Download</th>");
				sb.append("</tr>");

				List<Download> downloads = account.getDownloads();
				if (downloads.isEmpty()) {

					sb.append("<tr><td colspan='3'>You have no " + captions.getDownloadsCaptionPlural().toLowerCase() + "</td></tr>");

				} else {

					for (Download download : downloads) {

						Attachment attachment = download.getAttachment();

						sb.append("<tr>");
						sb.append("<td>" + download.getDateCreated().toString("dd-MMM-yy HH:mm") + "</td>");

						if (attachment == null) {

							sb.append("<td>Error with file</td>");
							sb.append("<td></td>");

						} else {

							sb.append("<td>" + attachment.getName() + "</td>");
							sb.append("<td>" +
									new LinkTag(AttachmentHandler.class, null, new ImageTag("files/graphics/downloads/icon_download.gif"),
											"download", download) + "</td>");

						}

						sb.append("</tr>");

					}
				}

				if (account.getItemType().getAccountModule().isShowAccount()) {
					sb
							.append("<tr><th colspan='3'>To return to your account " + new LinkTag(AccountHandler.class, null, "click here") +
									"</td></tr>");
				}
				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}
}
