package org.sevensoft.ecreator.iface.frontend.interaction.buddies;

import java.util.List;

import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.interaction.pm.PmHandler;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.ecreator.model.system.config.Captions;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 13 Dec 2006 09:26:12
 *
 */
public class BuddiesRenderer {

	private final Item		member;
	private final Captions		captions;
	private final List<Item>	buddies;
	private final RequestContext	context;

	public BuddiesRenderer(RequestContext context, Item member, List<Item> buddies) {
		this.context = context;
		this.member = member;
		this.buddies = buddies;
		this.captions = Captions.getInstance(context);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(RendererUtil.form1());

		sb.append(new TableTag("form1").setCaption(captions.getBuddiesCaptionPlural()));

		sb.append("<tr>");
		sb.append("<th>Status</th>");
		sb.append("<th>Name</th>");
		sb.append("<th>Last Active</th>");
		sb.append("<th>&nbsp;</th>");
		sb.append("<th>&nbsp;</th>");
		sb.append("</tr>");

		if (buddies.size() > 0) {

			for (Item buddy : buddies) {

				sb.append("<tr>");
				if (buddy.isOnline()) {
					sb.append("<td>" + new ImageTag("template-data/buddy_on.gif") + "</td>");
				} else {
					sb.append("<td>" + new ImageTag("template-data/buddy_off.gif") + "</td>");
				}

				sb.append("<td>" + new LinkTag(buddy.getUrl(), buddy.getDisplayName()) + "</td>");
				sb.append("<td>" + buddy.getLastActive() + "</td>");

				if (Module.PrivateMessages.enabled(context)) {
					sb.append("<td>" + new LinkTag(PmHandler.class, "compose", captions.getPmCaption(), "recipient", buddy) + "</td>");
				}

				sb.append("<td>" + new LinkTag(BuddiesHandler.class, "remove", "Remove", "buddy", buddy) + "</td>");
				sb.append("</tr>");
			}

		} else {

			sb.append("<tr><td colspan='5'>You have no " + captions.getBuddiesCaptionPlural().toLowerCase() + "</td></tr>");

		}

		sb.append("<tr><th colspan='5'>To return to your account " + new LinkTag(AccountHandler.class, null, "click here") + "</td></tr>");
		sb.append("</table>");

		return sb.toString();
	}

}
