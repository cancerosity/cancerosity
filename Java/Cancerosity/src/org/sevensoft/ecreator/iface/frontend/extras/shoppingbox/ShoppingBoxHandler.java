package org.sevensoft.ecreator.iface.frontend.extras.shoppingbox;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.cards.CardType;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardHolderException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardTypeException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CscException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiredCardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiryDateException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.IssueNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.StartDateException;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorException;
import org.sevensoft.ecreator.model.extras.shoppingbox.ShoppingBox;
import org.sevensoft.ecreator.model.extras.shoppingbox.ShoppingBoxDao;
import org.sevensoft.ecreator.model.extras.shoppingbox.TransportMethod;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.RadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 8 Jun 2007 11:33:45
 *
 */
@Path("msb.do")
public class ShoppingBoxHandler extends FrontendHandler {

	private Map<ShoppingBox, TransportMethod>	boxes;
	private String					cardHolder;
	private String					cardIssuer;
	private String					cardNumber;
	private CardType					cardType;
	private String					csc;
	private String					expiryMonth;
	private String					expiryYear;
	private String					startMonth;
	private String					startYear;
	private String					issueNumber;

	public Object createCard() throws ServletException {

		test(new LengthValidator(5), "cardHolder");
		test(new LengthValidator(16), "cardNumber");
		test(new RequiredValidator(), "cardType");
		test(new RequiredValidator(), "expiryYear");
		test(new RequiredValidator(), "expiryMonth");
		test(new LengthValidator(3, 3), "csc");

		if (hasErrors()) {
			return askCardDetails();
		}

		String expiryDate = expiryMonth + expiryYear;
		String startDate = null;
		if (startMonth != null && startYear != null) {
			startDate = startMonth + startYear;
		}

		try {

			Card card = new Card(context, account, getRemoteIp(), cardType, cardHolder, cardNumber, expiryDate, startDate, issueNumber, csc, cardIssuer);

			logger.fine("[CheckoutHandler] card created=" + card + ", cardaccount=" + card.getAccount());

			// update items that require payment
			// get all items not yet paid for
			final List<ShoppingBox> shoppingBoxes = ShoppingBoxDao.findUnpaid(context, account);
			for (ShoppingBox box : shoppingBoxes) {
				if (box.hasTransportMethod() && !box.isPaid()) {
					box.setPaid(true);
					box.save();
				}
			}

			// card created, so now bill and show completed screen
			return completed();

		} catch (IssueNumberException e) {
			setError("issueNumber", e);

		} catch (ExpiryDateException e) {
			setError("expiryDate", e);

		} catch (ExpiredCardException e) {
			setError("expiryDate", e);

		} catch (CardNumberException e) {
			setError("cardNumber", e);

		} catch (IOException e) {
			addError(e);

		} catch (CardTypeException e) {
			setError("cardType", e);

		} catch (StartDateException e) {
			setError("startDate", e);

		} catch (CardHolderException e) {
			setError("cardHolder", e);

		} catch (CscException e) {
			setError("csc", e);

		} catch (CardException e) {
			e.printStackTrace();
			addError(e);

		} catch (ProcessorException e) {
			e.printStackTrace();
			addError(e);
		}

		return askCardDetails();

	}

	private Object completed() {

		FrontendDoc doc = new FrontendDoc(context, "Shopping box - items paid");
		doc.addTrail(ShoppingBoxHandler.class, "completed", "Shopping box - items paid");
		doc.addBody(new Object() {

			@Override
			public String toString() {

				StringBuilder sb = new StringBuilder();

				sb.append("Thank you<br/>Your items have been paid for and will be dispatched shortly");

				return sb.toString();
			}
		});
		return doc;
	}

	public ShoppingBoxHandler(RequestContext context) {
		super(context);
	}

	public Object askCardDetails() throws ServletException {

		// must be logged in
		if (account == null) {
			return new LoginHandler(context, new Link(ShoppingBoxHandler.class)).main();
		}

		String title = "Shopping box - card details";
		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(ShoppingBoxHandler.class, null, title, "askCardDetails", true);
		doc.addBody(new MessagesTag(context));
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(ShoppingBoxHandler.class, "createCard", "POST"));
				sb.append(new TableTag("form", "center").setCaption("Enter card details"));

				sb.append("<tr><td class='info' colspan='2'>Enter the name exactly as it appears on the card, eg. Mrs Jane B Smith</td></tr>");

				sb.append("<tr><td class='key' align='right'>Card Holder</td><td class='value'>" + new TextTag(context, "cardHolder", 20) +
						new ErrorTag(context, "cardHolder", "<br/>") + "</td></tr>");

				SelectTag tag = new SelectTag(context, "cardType");
				tag.addOptions(CardType.get());

				sb.append("<tr><td class='key' align='right'>Card Type</td><td class='value'>" + tag + new ErrorTag(context, "cardType", "<br/>") +
						"</td></tr>");

				sb.append("<tr><td class='info' colspan='2'>Enter the long card number without any spaces.</td></tr>");

				sb.append("<tr><td class='key' align='right'>Card Number</td><td class='value'>" + new TextTag(context, "cardNumber", 25) +
						new ErrorTag(context, "cardNumber", "<br/>") + "</td></tr>");

				SelectTag month = new SelectTag(context, "startMonth");
				month.setAny("--");
				month.addOptions(Card.months);

				SelectTag year = new SelectTag(context, "startYear");
				year.setAny("--");
				year.addOptions(Card.startYears);

				sb.append("<tr><td class='key' align='right'>Start date</td><td class='value'>" + month + " " + year + " " +
						new ErrorTag(context, "startDate", "<br/>") + "</td></tr>");

				month = new SelectTag(context, "expiryMonth");
				month.addOptions(Card.months);

				year = new SelectTag(context, "expiryYear");
				year.addOptions(Card.expiryYears);

				sb.append("<tr><td class='key' align='right'>Expiry date</td><td class='value'>" + month + " " + year + " " +
						new ErrorTag(context, "expiryDate", "<br/>") + "</td></tr>");

				sb.append("<tr><th colspan='2'>" + "Enter the issue number exactly as it appears on the card. "
						+ "If your card does not have an issue number leave this field blank.</th></tr>");

				sb.append("<tr><td class='key' align='right'>Issue number</td><td class='value'>" + new TextTag(context, "issueNumber", 3) +
						new ErrorTag(context, "issueNumber", "<br/>") + "</td></tr>");

				sb.append("<tr><th colspan='2'>"
						+ "Enter the card security code which is the last 3 digits printed on the reverse of the card.</th></tr>");

				sb.append("<tr><td class='key' align='right'>Card security code</td><td class='value'>" + new TextTag(context, "csc", 5) +
						new ErrorTag(context, "csc", "<br/>") + "</td></tr>");

				sb.append("<tr><td class='command' align='center' colspan='2'>" + new SubmitTag("Make payment").setClass("checkout_button") +
						"</td></tr>");

				sb.append("</table>");
				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object confirmation() throws ServletException {

		if (boxes == null || boxes.isEmpty()) {
			return main();
		}

		// get all items not yet paid for
		final List<ShoppingBox> shoppingBoxes = ShoppingBoxDao.findUnpaid(context, account);

		// iterate through, and see if we have a transport method set for each of these. If we do then add it to the total
		Money t = new Money(0);
		for (ShoppingBox box : shoppingBoxes) {

			if (boxes.containsKey(box)) {

				box.setTransportMethod(boxes.get(box));
				t = t.add(box.getCost());

			} else if (box.isPaid()) {

				box.setTransportMethod(null);
			}
		}

		final Money total = t;

		FrontendDoc doc = new FrontendDoc(context, "My shopping box items");
		doc.addBody(new Object() {

			private StringBuilder	sb	= new StringBuilder();

			@Override
			public String toString() {

				sb.append(new TableTag("form").setId("msb_conf"));

				sb.append("<tr>");
				sb.append("<th>Name</th>");
				sb.append("<th>Weight</th>");
				sb.append("<th>Transport</th>");
				sb.append("<th>Cost</th>");
				sb.append("</tr>");

				sb.append(new FormTag(ShoppingBoxHandler.class, "askCardDetails", "post"));

				for (ShoppingBox box : boxes.keySet()) {

					sb.append(new HiddenTag("boxes_" + box.getId(), boxes.get(box)));

					sb.append("<tr>");

					sb.append("<td>" + box.getItem().getName() + "</td>");
					sb.append("<td>" + box.getWeight() + "</td>");
					sb.append("<td>" + box.getTransportMethod() + "</td>");
					sb.append("<td>" + box.getCost() + "</td>");

					sb.append("</tr>");
				}

				sb.append("<tr><td colspan='4'><strong>Total due: " + total + "</strong></td></tr>");
				sb.append("<tr><td colspan='4'>Continue to payment screen: " + new SubmitTag("Continue") + "</td></tr>");
				sb.append("<tr><td colspan='4'>Cancel and return: " + new ButtonTag(ShoppingBoxHandler.class, null, "Cancel") + "</td></tr>");
				sb.append("</form>");

				sb.append("</table>");
				return sb.toString();
			}

		});

		return doc;
	}

	/**
	 *The default controller will show a list of all unpaid items so the user can select some to pay for and then proceed to the confirmation page
	 */
	@Override
	public Object main() throws ServletException {

		// must be logged in
		if (account == null) {
			return new LoginHandler(context, new Link(ShoppingBoxHandler.class)).main();
		}

		// get all items not yet paid for
		final List<ShoppingBox> shoppingBoxes = ShoppingBoxDao.findUnpaid(context, account);
		if (shoppingBoxes.isEmpty()) {

			FrontendDoc doc = new FrontendDoc(context, "My shopping box items");
			doc.addBody(new Object() {

				private StringBuilder	sb	= new StringBuilder();

				@Override
				public String toString() {

					sb.append("You have no items booked in with My Shopping Box at the moment.");

					return sb.toString();
				}

			});
			return doc;
		}

		FrontendDoc doc = new FrontendDoc(context, "My shopping box items");
		doc.addBody(new Object() {

			private StringBuilder	sb	= new StringBuilder();

			@Override
			public String toString() {

				sb.append(new TableTag("form").setId("msb_basket"));

				sb.append("<tr>");
				sb.append("<th>Name</th>");
				sb.append("<th>Weight</th>");
				sb.append("<th>Hold</th>");
				sb.append("<th>Air</th>");
				sb.append("<th>Sea</th>");
				sb.append("</tr>");

				sb.append(new FormTag(ShoppingBoxHandler.class, "confirmation", "post"));
				for (ShoppingBox box : shoppingBoxes) {

					sb.append("<tr>");

					sb.append("<td>" + box.getItem().getName() + "</td>");
					sb.append("<td>" + box.getWeight() + "</td>");

					sb.append("<td>" + new RadioTag(context, "boxes_" + box.getId(), null) + "</td>");

					sb.append("<td>" + new RadioTag(context, "boxes_" + box.getId(), TransportMethod.Air) + " " + box.getCost(TransportMethod.Air) +
							"</td>");

					sb.append("<td>" + new RadioTag(context, "boxes_" + box.getId(), TransportMethod.Sea) + " " + box.getCost(TransportMethod.Sea) +
							"</td>");

					sb.append("</tr>");
				}

				sb.append("<tr><td colspan='5'>Pay for selected items: " + new SubmitTag("Update") + "</td></tr>");
				sb.append("</form>");

				sb.append("</table>");
				return sb.toString();
			}

		});

		return doc;
	}
}
