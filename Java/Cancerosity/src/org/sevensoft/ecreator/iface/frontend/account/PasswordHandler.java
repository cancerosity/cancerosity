package org.sevensoft.ecreator.iface.frontend.account;

import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.registration.RegistrationHandler;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 25-May-2004 15:30:26
 */
@Path( { "p.do", "password.do", "member-password.do" })
public class PasswordHandler extends FrontendHandler {

	private String	email, code;

	public PasswordHandler(RequestContext context) {
		super(context);
	}

	public Object email() {

		if (email == null) {
			setError("email", "You must enter an email address");
			return main();
		}

		Item account = Item.getByEmail(context, email);
		if (account == null) {
			setError("email", "We could not find a member with that email");
			return main();
		}

		try {
			account.resetRequest();
		} catch (EmailAddressException e) {
            logger.warning(email + " " + e.toString());
		} catch (SmtpServerException e) {
			logger.warning(e.toString());
		}

		FrontendDoc page = new FrontendDoc(context, "Password assistance");
		page.addTrail(PasswordHandler.class, "Password assistance");
		page.addBody(new Body() {

			@Override
			public String toString() {

				return "We have sent you an email that contains a link that will allow you to enter a new password.<br/>"
						+ "Please check your email and follow the instructions.";
			}

		});
		return page;
	}

	@Override
	public Object main() {

		FrontendDoc doc = new FrontendDoc(context, "Password assistance");
		doc.addTrail(PasswordHandler.class, "Password assistance");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("<style>");
				sb.append(".error { color: red; font-weight: bold; } ");
				sb.append("table.password_recovery { font-family: Tahoma; font-size: 13px; color: #373737; } ");
				sb.append("table.password_recovery a { color: #373737; text-decoration: underline } ");
				sb.append("table.password_recovery a:hover { color: #ff9900; text-decoration: underline; } ");
				sb.append("</style>");

				sb.append(new TableTag("password_recovery"));
				sb.append(new FormTag(PasswordHandler.class, "email", "POST"));

				sb.append(new MessagesTag(context));

				sb.append("<tr><td>");
				sb.append("If you've forgotten your password and can no longer use the email address "
						+ "you used to create your account then you will need to ");
				sb.append(new LinkTag(RegistrationHandler.class, null, "create a new account"));
				sb.append(".");

				sb.append("<br/><br/>");

				sb.append("Otherwise enter your email address and we will send you a link to reset your password.<br/>");
				sb.append(new TextTag(context, "email", 30));
				sb.append(new SubmitTag("Recover password"));
				sb.append(new ErrorTag(context, "email", "<br/>"));

				sb.append("</td></tr>");

				sb.append("</form>");
				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object reset() {

		Item account = Item.getByEmail(context, email);
		if (account == null || code == null || !code.equals(account.getResetCode())) {

			FrontendDoc doc = new FrontendDoc(context, "Password assistance");
			doc.addTrail(PasswordHandler.class, "Password assistance");
			doc.addBody(new Body() {

				@Override
				public String toString() {

					sb.append("The details used to reset your password are not valid. "
							+ "Please make sure you followed the link correctly in the email. To send the recovery email again ");
					sb.append(new LinkTag(PasswordHandler.class, null, "click here"));
					sb.append(".");

					return sb.toString();
				}

			});
			return doc;
		}

		final String password = account.setPassword();

		FrontendDoc doc = new FrontendDoc(context, "Password assistance");
		doc.addTrail(getClass(), "Password assistance");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("Your password has been reset. Your new password is <b>");
				sb.append(password);
				sb.append("</b><br/>");
				sb.append("You can change your password as soon as you login to your account.<br/><br/>");
				sb.append("To login now ");
				sb.append(new LinkTag(LoginHandler.class, null, "click here"));
				sb.append(".");

				return sb.toString();
			}

		});
		return doc;

	}

	@Override
	protected boolean runSecure() {
		return Config.getInstance(context).isSecured();
	}
}