package org.sevensoft.ecreator.iface.frontend;

/**
 * @author sks 29 Oct 2006 00:17:21
 * 
 * Form row used by front end forms
 *
 */
public class FormRow {

	private final String	label;
	private final String	desc;
	private final Object	obj;

	public FormRow(String label, Object obj) {
		this(label, null, obj);
	}

	public FormRow(String label, String desc, Object obj) {

		this.label = label;
		this.desc = desc;
		this.obj = obj;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("<tr><td width='200' class='key' valign='top'>");
		if (label != null) {

			sb.append(label);

			if (desc != null) {
				sb.append("<div>");
				sb.append(desc);
				sb.append("</div>");
			}
		}

		sb.append("</td><td valign='top'>");
		sb.append(obj);
		sb.append("</td></tr>");

		return sb.toString();

	}

}
