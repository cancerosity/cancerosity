package org.sevensoft.ecreator.iface.frontend.interaction.views;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.RestrictionHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionStatusHandler;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

/**
 * @author sam
 */
@Path( { "views.do", "member-views.do" })
public class ViewsHandler extends SubscriptionStatusHandler {

	private int	page;

	public ViewsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (account == null) {
			return new LoginHandler(context, new Link(ViewsHandler.class)).main();
		}

		if (!PermissionType.MemberViews.check(context, account)) {
			if (account.getItemType().getInteractionModule().hasRestrictionForward()) {
				return new ExternalRedirect(account.getItemType().getInteractionModule().getRestrictionForward());
			} else {
				return new RestrictionHandler(context).main();
			}
		}

		context.setAttribute("pagelink", new Link(ViewsHandler.class));

		FrontendDoc doc = new FrontendDoc(context, captions.getViewsCaptionPlural());

		// show account breadcrumb if account is enabled
		if (account.getItemType().getAccountModule().isShowAccount()) {
			doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		}
		doc.addTrail(ViewsHandler.class, captions.getViewsCaptionPlural());

		// see if the primary member group for this member has views markup
		Markup markup = account.getItemType().getInteractionModule().getViewsMarkup();
		if (markup == null) {

			doc.addBody(new ViewsRenderer(context, account));

		} else {

			List<Item> viewers = account.getInteraction().getViewers(50);

			Results results = new Results(viewers, page, markup.getPerPage());

			if (results.hasMultiplePages()) {
				doc.addBody(new ResultsControl(context, results, new Link(ViewsHandler.class)));
				viewers = results.subList(viewers);
			}

			MarkupRenderer r = new MarkupRenderer(context, markup);
			r.setBodyObjects(viewers);
			doc.addBody(r);

			if (results.hasMultiplePages()) {
				doc.addBody(new ResultsControl(context, results, new Link(ViewsHandler.class)));
			}
		}

		return doc;

	}

}
