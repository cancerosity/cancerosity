package org.sevensoft.ecreator.iface.frontend.interaction.userplane.chat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.iface.frontend.account.RestrictionHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionStatusHandler;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.interaction.userplane.chat.UserplaneChatSettings;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.results.StringResult;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 9 Dec 2006 20:01:10
 *
 */
@Path("userplane-chat.do")
public class UserplaneChatHandler extends SubscriptionStatusHandler {

	private final transient UserplaneChatSettings	settings;

	public UserplaneChatHandler(RequestContext context) {
		super(context);
		this.settings = UserplaneChatSettings.getInstance(context);
	}

	public Object app() throws ServletException {

		if (!Module.UserplaneChat.enabled(context)) {
			logger.fine("[UserplaneChatHandler] userplane chat disabled, exiting");
			return null;
		}

		if (account == null) {
			return new RestrictionHandler(context).main();
		}

		// check we have permission
		Item account = (Item) context.getAttribute("account");
		if (!PermissionType.UserplaneChat.check(context, account)) {

			logger.fine("[UserplaneChatHandler] domain does not have permission for userplane chat");

			if (account != null && account.getItemType().getInteractionModule().hasRestrictionForward()) {
				return new ExternalRedirect(account.getItemType().getInteractionModule().getRestrictionForward());
			} else {
				return new RestrictionHandler(context).main();
			}
		}

		StringBuilder sb = new StringBuilder();

		sb.append("<html>\n");
		sb.append("<head>\n");
		sb.append("	<meta http-equiv=Content-Type content=\"text/html;  charset=ISO-8859-1\" />\n");
		sb.append("	<title>Userplane AV Webchat</title>\n");

		sb.append("	<script language=\"JavaScript\">");
		sb.append("	<!--");
		sb.append("		function csEvent( strEvent, strParameter1, strParameter2 )");
		sb.append("		{");
		sb.append("			if( strEvent == \"InstantCommunicator.StartConversation\" )");
		sb.append("			{");
		sb.append("				var strUserID = strParameter1;");
		sb.append("				var bServer = strParameter2;");
		sb.append("			}");
		sb.append("			else if( strEvent == \"User.ViewProfile\" )");
		sb.append("			{");
		sb.append("				var strUserID = strParameter1;");
		sb.append("			}");
		sb.append("			else if( strEvent == \"User.Block\" )");
		sb.append("			{");
		sb.append("				var strBlockedUserID = strParameter1;");
		sb.append("				var bBlocked = strParameter2;");
		sb.append("			}");
		sb.append("			else if( strEvent == \"User.AddFriend\" )");
		sb.append("			{");
		sb.append("				var strFriendUserID = strParameter1;");
		sb.append("				var bFriend = strParameter2;");
		sb.append("			}");
		sb.append("			else if( strEvent == \"Chat.Help\" )");
		sb.append("			{");
		sb.append("			}");
		sb.append("			else if( strEvent == \"User.NoTextEntry\" )");
		sb.append("			{");
		sb.append("			}");
		sb.append("			else if( strEvent == \"Connection.Success\" )");
		sb.append("			{");
		sb.append("			}");
		sb.append("			else if( strEvent == \"Connection.Failure\" ) ");
		sb.append("			{ ");
		sb.append("				if( strParameter1 == \"Session.Timeout\" ) ");
		sb.append("				{ ");
		sb.append("					//handle timeout here, both inactivity and session timeouts				");
		sb.append("				} ");
		sb.append("				if( strParameter1 == \"User.Banned\" )");
		sb.append("				{");
		sb.append("					//handle ban event here");
		sb.append("				}");
		sb.append("			}			");
		sb.append("		}");

		sb.append("		function replaceAlpha( strIn )");
		sb.append("		{");
		sb.append("			var strOut = '';");
		sb.append("			for( var i = 0 ; i < strIn.length ; i++ )");
		sb.append("			{");
		sb.append("				var cChar = strIn.charAt(i);");
		sb.append("				if( ( cChar >= 'A' && cChar <= 'Z' )");
		sb.append("					|| ( cChar >= 'a' && cChar <= 'z' )");
		sb.append("					|| ( cChar >= '0' && cChar <= '9' ) )");
		sb.append("				{");
		sb.append("					strOut += cChar;");
		sb.append("				}");
		sb.append("				else");
		sb.append("				{");
		sb.append("					strOut += '_';");
		sb.append("				}");
		sb.append("			}");
		sb.append("			");
		sb.append("			return strOut;");
		sb.append("		}");
		sb.append("	//-->");
		sb.append("	</script>");
		sb.append("</head>");
		sb.append("<body bgcolor='#ffffff' bottommargin='0' leftmargin='0' marginheight='0' marginwidth='0' rightmargin='0' topmargin='0'>");

		sb.append("<script type='text/javascript' src='files/userplane-chat/flashobject.js'></script>\n   \n");

		sb.append("<div id='flashcontent'>   \n");
		sb.append("	<strong>You need to upgrade your Flash Player by clicking "
				+ "<a href='http://www.macromedia.com/go/getflash/' target='_blank'>this link</a>.</strong><br><br>"
				+ "<strong>If you see this and have already upgraded we suggest you follow "
				+ "<a href='http://www.adobe.com/cfusion/knowledgebase/index.cfm?id=tn_14157' target='_blank'>this link</a> "
				+ "to uninstall Flash and reinstall again.</strong>   \n");
		sb.append("</div>   \n");

		sb.append("<script type=\"text/javascript\">   \n");
		sb.append("	// <![CDATA[   \n");

		sb
				.append("	var fo = new FlashObject('http://swf.userplane.com/CommunicationSuite/ch.swf', 'ch', '100%', '100%', '6', '#ffffff', false, 'best');   \n");
		sb.append("	fo.addParam('scale', 'noscale');   \n");
		sb.append("	fo.addParam('menu', 'false');   \n");
		sb.append("	fo.addParam('salign', 'LT');   \n");
		sb.append("	fo.addVariable('strServer', '" + settings.getFlashcomServer() + "');   \n");
		sb.append("	fo.addVariable('strSwfServer', 'swf.userplane.com');   \n");
		sb.append("	fo.addVariable('strApplicationName', 'CommunicationSuite');   \n");
		sb.append("	fo.addVariable('strDomainID', '" + settings.getDomainID() + "');   \n");
		sb.append("	fo.addVariable('strSessionGUID', '" + context.getSessionId() + "');   \n");
		sb.append("	fo.addVariable('strKey', '');   \n");
		sb.append("	fo.addVariable('strLocale', 'english');   \n");
		sb.append("	fo.addVariable('strInitialRoom', '');   \n");
		sb.append("	fo.write('flashcontent');   \n");

		sb.append("	// COPYRIGHT Userplane 2006 (http://www.userplane.com)   \n");
		sb.append("	// CS version 1.9.0   \n");

		sb.append("	// ]]>   \n");
		sb.append("</script>   \n");

		sb.append("</body>");
		sb.append("</html>");

		return new StringResult(sb, "text/html");
	}

	/**
	 * This main method will open up the frameset for the flash app and adverts
	 */
	@Override
	public Object main() throws ServletException {

		if (!Module.UserplaneChat.enabled(context)) {
			logger.fine("[UserplaneChatHandler] userplane chat disabled, exiting");
			return HttpServletResponse.SC_FORBIDDEN;
		}

		if (account == null) {
			return new RestrictionHandler(context).main();
		}

		if (!PermissionType.UserplaneChat.check(context, account)) {
			logger.fine("[UserplaneChatHandler] domain does not have permission for userplane chat");
			return new RestrictionHandler(context).main();
		}

		StringBuilder sb = new StringBuilder();

		sb.append("<html>\n");
		sb.append("<head></head>\n\n");

		sb.append("<frameset rows='*,145' framespacing='0' frameborder='no' border='0'>\n");
		sb.append("<frame src='" + new Link(UserplaneChatHandler.class, "app") + "' Frame' name='Webchat_Frame' scrolling='NO' noresize>\n");
		sb.append("<frame src='http://subtracts.userplane.com/mmm/bannerstorage/ch_int_frameset.html?app=wc&zoneID=" + settings.getBannerZoneId()
				+ "&textZoneID=" + settings.getTextZoneId() + "' name='Ad_Frame' scrolling='NO' noresize>\n");
		sb.append("</frameset>\n\n");
		sb.append("</html>");

		return new StringResult(sb, "text/html");
	}
}
