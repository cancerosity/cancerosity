package org.sevensoft.ecreator.iface.frontend.account.profile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 13 Mar 2007 16:30:03
 *
 */
@Path("pv.do")
public class ProfileViewHandler extends FrontendHandler {

	public ProfileViewHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (account == null) {
			return new LoginHandler(context, new Link(ProfileViewHandler.class)).main();
		}

		if (account.getItemType().isHidden()) {
			return HttpServletResponse.SC_FORBIDDEN;
		}

		return new ExternalRedirect(account.getUrl());
	}
}
