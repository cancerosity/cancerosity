package org.sevensoft.ecreator.iface.frontend.search;

import java.util.Map;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.model.search.forms.SearchForm;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 7 Mar 2007 10:01:04
 *
 */
public class SearchAlertHeader extends Panel {

	private final SearchForm				searchForm;
	private final MultiValueMap<String, String>	parameters;

	public SearchAlertHeader(RequestContext context, SearchForm searchForm, MultiValueMap<String, String> multiValueMap) {
		super(context);

		this.searchForm = searchForm;
		this.parameters = multiValueMap;
	}

	@Override
	public void makeString() {

		sb.append(new TableTag("ec_addalert_header"));
		sb.append(new FormTag(ItemSearchHandler.class, "alert", "post"));
		sb.append(new HiddenTag("searchForm", searchForm));

		for (Map.Entry<String, String> entry : parameters.entrySet()) {
			sb.append(new HiddenTag(entry.getKey(), entry.getValue()));
		}

		sb.append("<tr><td>Enter your email address and we will email you when more properties are added that match your search. ");
		sb.append(new TextTag(context, "alertEmail", 20));
		sb.append("</td></tr>");

		sb.append("</form>");
		sb.append("</table>");
	}
}
