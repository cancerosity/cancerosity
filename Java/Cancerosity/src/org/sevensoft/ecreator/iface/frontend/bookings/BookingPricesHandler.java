package org.sevensoft.ecreator.iface.frontend.bookings;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.Day;
import org.sevensoft.commons.samdate.Month;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.ecom.bookings.config.ItemPeriodPricesPanel;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.items.listings.MyListingsHandler;
import org.sevensoft.ecreator.model.bookings.BookingSetup;
import org.sevensoft.ecreator.model.bookings.BookingSlot;
import org.sevensoft.ecreator.model.bookings.MonthlyConfigurator;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.results.StringResult;
import org.sevensoft.jeezy.http.results.docs.SimpleDoc;
import org.sevensoft.jeezy.http.util.Keypad;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * User: MeleshkoDN
 * Date: 10.10.2007
 * Time: 14:37:20
 */
@Path("bookings-prices.do")
public class BookingPricesHandler extends FrontendHandler {

    private Item item;
    private Date year;
    private List<Date> dates;
    private Date date;
    private BookingSlot.Status status;
    private Month month;
    private Day day;
    private Money rate;

    public BookingPricesHandler(RequestContext context) {
        super(context);
    }

    public Object closer() {
        return new StringResult("<html><head><script> window.opener.location = '" + new Link(BookingPricesHandler.class, null, "item", item) +
                "'; window.close(); </script></head></html>", "text/html");
    }

    public Object delete() throws ServletException {

        if (date == null || item == null) {
            return HttpServletResponse.SC_BAD_REQUEST;
        }

        BookingSetup setup = item.getBookingSetup();
        setup.removeBookingDate(date);

        return closer();
    }

    @Override
    public Object main() throws ServletException {

        if (item == null || account == null) {
            return index();
        }

        if (year == null) {
            year = new Date();
        }

        year = year.beginYear();
        final BookingSetup setup = item.getBookingSetup();

        FrontendDoc doc = new FrontendDoc(context, "Booking prices: " + item.getName());
        if (account.getItemType().getAccountModule().isShowAccount()) {
            doc.addTrail(AccountHandler.class, captions.getAccountCaption());
        }
        doc.addTrail(MyListingsHandler.class, captions.getListingsCaption());
        doc.addTrail(BookingPricesHandler.class, null, "Set prices", "item", item);
        doc.addBody(new Body() {

            @Override
            public String toString() {
                sb.append(new TableTag());
                sb.append("<tr>");
                sb.append("<td>");
                sb.append(new TableTag("ec_bkyears"));

                sb.append("<tr><td>");
                sb.append(new LinkTag(BookingPricesHandler.class, null, "<< previous year", "item", item, "year", year.previousYear()));
                sb.append("</td><td>");
                sb.append(new LinkTag(BookingPricesHandler.class, null, "next year >>", "item", item, "year", year.nextYear()));
                sb.append("</td></tr>");

                sb.append("</table>");

                Keypad keypad = new Keypad(1);
                keypad.setTableClass("ec_bookingcharts");
                keypad.setCellVAlign("top");

                // render 12 calendars
                Date month = year.beginMonth();
                for (int n = 0; n < 12; n++) {
                    keypad.addObject(new MonthlyConfigurator(context, month, setup, item, BookingPricesHandler.class));
                    month = month.nextMonth();
                }

                sb.append(keypad);
                sb.append("</td>");
                sb.append("<td valign='top'>");
                sb.append(new ItemPeriodPricesPanel(context, item, ItemPeriodPriceHandler.class));
                sb.append("</td>");
                sb.append("</tr>");
                sb.append("</table>");
                return sb.toString();
            }

        });
        return doc;
    }

    public Object popup() throws ServletException {

        if (date == null || item == null) {
            return HttpServletResponse.SC_BAD_REQUEST;
        }

        final Money rate;

        // get booking date for this date
        BookingSlot bookingDate = item.getBookingSetup().getBookingSlot(date);

        if (bookingDate == null) {

            status = null;
            rate = null;

        } else {

            status = bookingDate.getStatus();
            rate = bookingDate.getRate();

        }

        SimpleDoc doc = new SimpleDoc(context);
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new FormTag(BookingPricesHandler.class, "save", "post"));
                sb.append(new HiddenTag("date", date));
                sb.append(new HiddenTag("item", item));

                sb.append(new TableTag("booking_date"));

                sb.append("<tr><td>Status</td><td>" + new SelectTag(context, "status", status, BookingSlot.Status.values()) +
                        "</td></tr>");

                sb.append("<tr><td>Rate</td><td>" + new TextTag(context, "rate", rate, 12) + "</td></tr>");

                sb.append("</table>");
                sb.append("<br/>");
                sb.append("<div>" + new SubmitTag("Update") + " " +
                        new ButtonTag(BookingPricesHandler.class, "delete", "Delete", "date", date, "item", item) + "</div>");

                sb.append("</form>");

                sb.append(new HiddenTag("date", date));
                sb.append(new HiddenTag("item", item));

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object save() throws ServletException {

        if (date == null || item == null) {
            return index();
        }

        BookingSetup setup = item.getBookingSetup();

        setup.setStatus(date, status);
        setup.setRate(date, rate);

        return closer();
    }


}
