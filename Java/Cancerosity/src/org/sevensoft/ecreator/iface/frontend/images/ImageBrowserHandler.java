package org.sevensoft.ecreator.iface.frontend.images;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.RestrictionHandler;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.results.docs.Doc;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.util.Keypad;
import org.sevensoft.jeezy.i18n.Messages;

/**
 * @author sks 15 Nov 2006 12:35:40
 *
 */
@Path("image-browser.do")
public class ImageBrowserHandler extends FrontendHandler {

	private Item	item;
	private int		i;

	public ImageBrowserHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {
        if (item == null) {
            return HttpServletResponse.SC_BAD_REQUEST;
        }

		final List<Img> images = item.getApprovedImages();
		if (images.isEmpty())
			return HttpServletResponse.SC_BAD_REQUEST;

        Object obj = checkPermission();
        if (obj != null) {
            logger.fine("[ImageBrowserHandler] permission forward=" + obj);
            return obj;
        }

        if (i < 1)
			i = 1;
		if (i > images.size())
			i = images.size();

		final Img img = images.get(i - 1);

		Doc doc = new Doc(context) {

			@Override
			public StringBuilder output() throws ServletException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {

				StringBuilder sb = new StringBuilder();
				sb.append("<style>");
				sb.append("table.browser { width: 100%; border-top: 1px solid #bcbcbc; border-bottom: 1px solid #bcbcbc;}");
				sb.append("table.browser td { padding: 6px; } ");
				sb.append("table.browser td.thumbnails { border-right: 1px solid #bcbcbc; } ");
				sb.append("table.browser td.fullsize { width: 100%; } ");
				sb.append("div.name { font-weight: bold; font-size: 16px; color: black; font-family: Arial; } ");
				sb.append("div.id { color: #666666; font-size: 10px; font-family: Verdana; margin-bottom: 5px;} ");
				sb.append("div.thumbnail { margin: 5px; float: left; width: 70px; height: 70px; "
						+ "text-align: center; padding: 5px; border: 1px solid #bcbcbc; } ");
				sb.append("div.control { text-align: center; font-size: 11px; font-family: Verdana;  margin-bottom: 8px; } ");
				sb.append("div.control a { color: blue; } ");
				sb.append("div.close { margin-top: 5px;  text-align: right; font-size: 11px; font-family: Verdana; } ");
				sb.append("div.close a { color: blue; } ");
				sb.append("</style>");

                sb.append("<div class='name'>" + item.getName() + " " + Messages.get("frontend.images.browser.name") + "</div>");
                sb.append("<div class='id'>" + item.getItemType().getName() + " " + Messages.get("frontend.images.browser.ref") + " " + item.getId() + "</div>");
				sb.append(new TableTag("browser"));
				sb.append("<tr><td class='thumbnails' valign='top'>");

				List<String> list = new ArrayList();
				int n = 1;
				for (Img img : images) {

					ImageTag imgtag = img.getThumbnailTag(70, 50);
					LinkTag linkTag = new LinkTag(ImageBrowserHandler.class, null, imgtag, "i", n, "item", item);
					list.add(linkTag.toString());
					n++;
				}

				Keypad keypad = new Keypad(2);
				keypad.setCellAlign("center");
				keypad.setCellVAlign("middle");
				keypad.setObjects(list);
				sb.append(keypad);

				sb.append("</td><td class='fullsize' valign='top' align='center'>");

				int prev = (i == 1 ? images.size() : i - 1);
				int next = (i == images.size() ? 1 : i + 1);

				sb.append("<div class='control'>" + new LinkTag(ImageBrowserHandler.class, null, Messages.get("frontend.images.browser.prev"), "item", item, "i", prev)
                        + " : " + Messages.get("frontend.images.browser.page1") + " " + i + " " + Messages.get("frontend.images.browser.page2") + " " + images.size()
                        + " : " + new LinkTag(ImageBrowserHandler.class, null, Messages.get("frontend.images.browser.next"), "item", item, "i", next)
						+ "</div>");

				int[] d = Img.getDimensionsToFit(450, 450, img, false);
				sb.append(new ImageTag(img.getPath(), 0, d[0], d[1]));

				sb.append("</td></tr></table>");
                sb.append("<div class='close'><a href='#' onclick='window.close();'>" + Messages.get("frontend.images.browser.close") + "</a></div>");

				return sb;
			}
		};
		return doc;
	}

    private Object checkPermission() throws ServletException {

        /*
           * Check for access on the categories of this item
           */
        if (Module.Permissions.enabled(context)) {

            logger.fine("[ItemHandler] permissions module enabled - checking " + item);

            if (!PermissionType.ItemFull.check(context, account, item.getItemType())) {
                logger.fine("[ItemHandler] ItemFull permission denied");
                if (item != null && item.getItemType().getRestrictionForwardUrl() != null) {
                    return new ExternalRedirect(item.getItemType().getRestrictionForwardUrl());
                } else {
                    return new RestrictionHandler(context).main();
                }
            }

            /*
                * Check category permissions but only if we are in at least one category
                */
            if (item.hasCategories()) {

                List<Category> categories = item.getCategories();
                if (categories.size() > 0) {

                    // as long as we have permission for one of the categories, then continue
                    boolean categoryPermission = false;
                    for (Category c : categories) {
                        if (PermissionType.CategoryItems.check(context, account, c)) {
                            logger.fine("[ItemHandler] CategoryItems permission allowed for cat=" + c);
                            categoryPermission = true;
                            break;
                        }
                    }

                    if (false == categoryPermission) {
                        logger.fine("[ItemHandler] no CategoryItems permission for any cat");
                        return new RestrictionHandler(context).main();
                    }

                }
            }
        }

        logger.fine("[ItemHandler] permissions ok - accepting " + item);

        return null;
    }

    @Override
	protected boolean runSecure() {
		return false;
	}

}
