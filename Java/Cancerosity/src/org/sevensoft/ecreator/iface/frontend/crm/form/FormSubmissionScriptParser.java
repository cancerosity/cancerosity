package org.sevensoft.ecreator.iface.frontend.crm.form;

import java.util.LinkedHashMap;

import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.FormField;
import org.sevensoft.ecreator.model.crm.forms.submission.Submission;

/**
 * @author Stephen K Samuel samspade79@gmail.com 16 Mar 2009 07:29:02
 *
 */
public class FormSubmissionScriptParser {

	/**
	 * @param sub
	 * @param data
	 *
	 */
	public String parse(Form form, Submission sub, LinkedHashMap<FormField, String> data) {

		if (form.getSubmissionScript() == null)
			return null;

		// replace tags
		String script = form.getSubmissionScript().replace("[id]", form.getIdString());
		script = script.replace("[name]", form.getName());

		if (form.hasEmailField()) {
			String email = data.get(form.getEmailField());
			script = script.replace("[form_email]", email);
		}

		return script;
	}
}
