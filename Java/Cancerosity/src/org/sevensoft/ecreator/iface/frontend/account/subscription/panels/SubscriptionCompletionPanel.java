package org.sevensoft.ecreator.iface.frontend.account.subscription.panels;

import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.model.accounts.subscriptions.Subscription;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Captions;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 31 Jan 2007 10:42:15
 *
 */
public class SubscriptionCompletionPanel {

	private final RequestContext	context;
	private final Subscription	sub;
	private final Item		account;
	private final Captions		captions;

	public SubscriptionCompletionPanel(RequestContext context, Item account, Subscription sub) {
		this.context = context;
		this.account = account;
		this.sub = sub;
		this.captions = Captions.getInstance(context);
	}

	public String toString() {

		StringBuilder sb = new StringBuilder();

		if (sub.hasSubscriptionExpiryDate()) {
			sb.append("<div class='subscription_compl_date'><b>Your subscription will expire on " + sub.getSubscriptionExpiryDate().toString("dd MMM yyyy")+"</b></div><br>");
		}

		if (account.getItemType().getAccountModule().isShowAccount()) {
            sb.append("<div class='subscription_acc_return'>");
			sb.append(new LinkTag(AccountHandler.class, null, "Click here"));
			sb.append(" to return to your ");
			sb.append(captions.getAccountCaption());
			sb.append(".</div>");

		}

		return sb.toString();
	}

}
