package org.sevensoft.ecreator.iface.frontend.items.listings.panels;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.items.listings.AddListingHandler;
import org.sevensoft.ecreator.iface.frontend.items.listings.EditListingHandler;
import org.sevensoft.ecreator.iface.frontend.misc.FieldInputCell;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.listings.Listing;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;
import org.sevensoft.ecreator.model.items.listings.sessions.ListingSession;
import org.sevensoft.ecreator.model.media.images.ImageOwner;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.media.images.ImageSettings;
import org.sevensoft.ecreator.model.system.config.Captions;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Grid;

/**
 * @author sks 8 Mar 2007 12:42:47
 */
public class ListingImagesPanel extends Panel {

    private final ImageOwner owner;
    private Class<? extends Handler> handler;
    private String p;
    private Item item;
    private boolean editPreview;

    public ListingImagesPanel(RequestContext context, Item item, String p) {
        super(context);
        this.item = item;
        this.owner = item;
        this.p = p;
        this.handler = EditListingHandler.class;
    }

    public ListingImagesPanel(RequestContext context, ListingSession session) {
        super(context);
        this.owner = session;
        this.handler = AddListingHandler.class;
    }

    public Panel setEditPreview(boolean editPreview) {
        this.editPreview = editPreview;
        return this;
    }

    @Override
    public void makeString() {

        sb.append("You can attach images to your " + Captions.getInstance(context).getListingsCaption().toLowerCase() + " which make your " + Captions.getInstance(context).getListingsCaption().toLowerCase() + " stand out and improve the appearance.<br/>"
                + "You can also add more images or remove unwanted images after your " + Captions.getInstance(context).getListingsCaption().toLowerCase() + " has been created.");

        if (owner.hasAnyImage()) {

            sb.append(new FormFieldSet("Images uploaded"));

            Grid grid = new Grid(4);
            for (Img image : owner.getAllImages()) {
                grid.addCell(image.getThumbnailTag(80, 60) + "<br/>" + image.getCaption() + "<br/>" + image.getWidth() + "px by " + image.getHeight() + "px<br/>"
                        + new LinkTag(handler, "removeImage", "Remove", "item", item, "image", image, "p", p), 1);
            }

            sb.append(grid);
            sb.append("<br/>&nbsp;");
            sb.append("</fieldset>");
        }

        boolean canAddMore = true;

        if (item != null && item.getListing() != null) {
            Listing listing = item.getListing();
            canAddMore = owner.getAllImagesCount() < listing.getListingPackage().getMaxImages();
        } else if (owner instanceof ListingSession) {
            ListingSession session = (ListingSession) owner;
            ListingPackage lp = session.getListingPackage();
            if (lp.isImages()) {
                // ensure we have not uploaded too many images
                if (lp.getMaxImages() > 0) {
                    if (session.getAllImagesCount() >= lp.getMaxImages()) {
                        canAddMore = false;
                    }
                }
            }
        }

        if (canAddMore) {

            sb.append(new FormTag(handler, "uploadImage", "multi"));
            sb.append(new HiddenTag("item", item));
            sb.append(new HiddenTag("p", p));
            sb.append(new HiddenTag("editPreview", editPreview));

            sb.append(new FormFieldSet("Upload an image"));

            sb.append("<div class='text'>" + ImageSettings.getInstance(context).getFormatsNote() + "</div>");

            sb.append(new FieldInputCell("Browse for file", null, new FileTag("imageUpload")));

            sb.append(new FieldInputCell("Image caption", null, new TextTag(null, "imageCaption")));

            sb.append(new FieldInputCell("", null, new SubmitTag("Upload image")));
            
            sb.append("</fieldset>");

            sb.append("</form>");
        }

    }
}
