package org.sevensoft.ecreator.iface.frontend.misc;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 18 May 2007 11:20:14
 *
 */
@Path("avpselector.do")
public class AvpSelectorHandler extends FrontendHandler {

	public AvpSelectorHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {
		return null;
	}

}
