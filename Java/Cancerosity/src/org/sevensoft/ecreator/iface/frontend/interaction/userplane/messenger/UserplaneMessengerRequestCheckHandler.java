package org.sevensoft.ecreator.iface.frontend.interaction.userplane.messenger;

import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.jdom.Document;
import org.jdom.Element;
import org.sevensoft.ecreator.model.accounts.sessions.AccountSession;
import org.sevensoft.ecreator.model.interaction.userplane.messenger.UserplaneMessengerRequest;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.XmlResult;

/**
 * @author sks 9 Dec 2006 12:31:27
 * 
 * Checks for any pending requests.
 * If found returns a single request. If there are further pending requests, further ajax requests will pick these up.
 * 
 * importer - this check gets its own handler because I do not want to extend from frontend doc - this is because I do not want the overhead associated
 * with the frontend doc when this handler could be called many times
 *
 */
@Path("userplane-msgr-request-check.do")
public class UserplaneMessengerRequestCheckHandler extends Handler {

	private static final Logger	logger	= Logger.getLogger("ecreator");

	public UserplaneMessengerRequestCheckHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		logger.fine("[UserplaneMessengerRequestCheckHandler] checking for request for sessionId=" + context.getSessionId());

		Item account = AccountSession.getItem(context, context.getSessionId());
		if (account != null) {

			// update member last active time
			account.setLastActive();

			logger.fine("[UserplaneMessengerRequestCheckHandler] account=" + account);

			UserplaneMessengerRequest request = UserplaneMessengerRequest.get(context, account);
			if (request != null) {

				logger.fine("[UserplaneMessengerRequestCheckHandler] request=" + request);

				Element root = new Element("userplane-request");
				Document doc = new Document(root);

				Element originatorId = new Element("originatorId").setText(request.getOriginator().getIdString());
				Element originatorName = new Element("originatorName").setText(request.getOriginator().getDisplayName());

				root.addContent(originatorId);
				root.addContent(originatorName);

				return new XmlResult(doc);
			}
		}

		return HttpServletResponse.SC_NO_CONTENT;

	}

	@Override
	protected boolean runSecure() {
		return false;
	}

}
