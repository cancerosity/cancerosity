package org.sevensoft.ecreator.iface.frontend.interaction.userplane.messenger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.ecreator.model.interaction.userplane.messenger.UserplaneMessengerRequest;
import org.sevensoft.ecreator.model.interaction.userplane.messenger.UserplaneMessengerSettings;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.StringResult;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 26 Nov 2006 14:25:38
 * 
 * This handler will output the html required to call the flash chat module 
 *
 */
@Path("userplane-msgr.do")
public class UserplaneMessengerHandler extends FrontendHandler {

	private final transient UserplaneMessengerSettings	userplaneSettings;

	/**
	 * The member who we want to start a chat with
	 */
	private Item							target;

	public UserplaneMessengerHandler(RequestContext context) {
		super(context);
		this.userplaneSettings = UserplaneMessengerSettings.getInstance(context);
	}

	public Object app() {

		if (!Module.UserplaneMessenger.enabled(context)) {
			return HttpServletResponse.SC_FORBIDDEN;
		}

		logger.fine("[UserplaneHandler] target=" + target + ", active=" + account);

		if (account == null) {
			return HttpServletResponse.SC_BAD_REQUEST;
		}

		if (target == null) {
			return HttpServletResponse.SC_BAD_REQUEST;
		}

		// clear off pending requests as we have now excepted
		SimpleQuery.delete(context, UserplaneMessengerRequest.class, "target", account, "originator", target);

		String flashcomServer = userplaneSettings.getFlashcomServer();
		String domainID = userplaneSettings.getDomainID();
		String sessionGUID = context.getSessionId();

		StringBuilder sb = new StringBuilder();

		sb.append("<html>\n");
		sb.append("<head>\n\n");
		sb.append("	<meta http-equiv=Content-Type content='text/html;  charset=ISO-8859-1' />\n");
		sb.append("	<title>Userplane AV Webmessenger</title>\n");

		sb.append("	<script language='JavaScript'>\n\n");

		sb.append("		function sendCommand( commandIn, valueIn )\n");
		sb.append("		{\n");
		sb.append("			if( commandIn == 'focus' )\n");
		sb.append("			{\n");
		sb.append("				// DO NOT EDIT\n");
		sb.append("				var wmObject = getWMObject();\n");
		sb.append("				// only do the focus if we are sure it is not going remove focus from typing area\n");
		sb
				.append("				if( wmObject != null && ( wmObject.focus != undefined || ( navigator.userAgent.indexOf( 'MSIE' ) >= 0 && navigator.userAgent.indexOf('Mac' ) >= 0 ) ) )\n");
		sb.append("				{\n");
		sb.append("					window.focus();\n");
		sb.append("					wmObject.focus();\n");
		sb.append("				}\n");
		sb.append("			}\n");
		sb.append("			else ");
		sb.append("			{\n");
		sb.append("				// EDIT HERE: you will need to handle the following commands from the wm client\n");
		sb.append("				if( commandIn == 'viewProfile' )\n");
		sb.append("				{\n");
		sb.append("					if( valueIn == '-1' )\n");
		sb.append("					{\n");
		sb.append("						// view their own profile\n");
		sb.append("						window.parent.opener.location = '" + account.getUrl() + "'; ");
		sb.append("					}\n");
		sb.append("					else\n");
		sb.append("					{\n");
		sb.append("						var userID = valueIn;\n");
		sb.append("						// view userID's profile\n");
		sb.append("						window.parent.opener.location = '" + new Link(ItemHandler.class) + "?item=' + userID; ");
		sb.append("					}\n");
		sb.append("				}\n");
		sb.append("				else if( commandIn == 'help' )\n");
		sb.append("				{\n");
		sb.append("					// view the help\n");
		sb.append("				}\n");
		sb.append("				else if( commandIn == 'buddyList' )\n");
		sb.append("				{\n");
		sb.append("					// view their buddy list\n");
		sb.append("				}\n");
		sb.append("				else if( commandIn == 'preferences' )\n");
		sb.append("				{\n");
		sb.append("					// view the preferences\n");
		sb.append("				}\n");
		sb.append("				else if( commandIn == 'addBuddy' )\n");
		sb.append("				{\n");
		sb.append("					var userID = valueIn;\n");
		sb.append("					// respond to an add buddy click (XML has also been notified)\n");
		sb.append("				}\n");
		sb.append("				else if( commandIn == 'block' )\n");
		sb.append("				{\n");
		sb.append("					// they blocked the user\n");
		sb.append("				}\n");
		sb.append("				else if( commandIn == 'unblock' )\n");
		sb.append("				{\n");
		sb.append("					// they unblocked the user\n");
		sb.append("				}\n");
		sb.append("				else if( commandIn == 'Connection.Success' )\n");
		sb.append("				{\n");
		sb.append("					// client successfully connected to server\n");
		sb.append("				}\n");
		sb.append("				else if( commandIn == 'Connection.Failure' )\n");
		sb.append("				{\n");
		sb.append("					// client was disconnected from server\n");
		sb.append("				}				\n");
		sb.append("			}\n");
		sb.append("		}");

		sb.append("		function focusIt()\n");
		sb.append("		{\n");
		sb.append("			window.focus();\n");
		sb.append("			var wmObject = getWMObject();\n");
		sb.append("			\n");
		sb.append("			if( wmObject != null && wmObject.focus != undefined )\n");
		sb.append("			{\n");
		sb.append("				wmObject.focus();\n");
		sb.append("			}\n");
		sb.append("		}\n\n");

		sb.append("		function getWMObject()\n");
		sb.append("		{\n");
		sb.append("			if(document.all)\n");
		sb.append("			{\n");
		sb.append("				return document.all['wm'];\n");
		sb.append("			}\n");
		sb.append("			else if(document.layers)\n");
		sb.append("			{\n");
		sb.append("				return document.wm;\n");
		sb.append("			}\n");
		sb.append("			else if(document.getElementById)\n");
		sb.append("			{\n");
		sb.append("				return document.getElementById('wm');\n");
		sb.append("			}\n");
		sb.append("			\n");
		sb.append("			return null;\n");
		sb.append("		}");
		sb.append("		function wm_DoFSCommand( command, args ) { } ");
		sb.append("	</script>\n\n");

		sb.append("	<script language='VBScript'>\n");
		sb.append("	<!-- \n");
		sb.append("		//  Map VB script events to the JavaScript method - Netscape will ignore this... \n");
		sb.append("		//  Since FSCommand fires a VB event under ActiveX, we respond here \n");
		sb.append("		Sub wm_FSCommand(ByVal command, ByVal args)\n");
		sb.append("	  		call wm_DoFSCommand(command, args)\n");
		sb.append("		end sub\n");
		sb.append("	-->\n");
		sb.append("	</script>\n\n");

		sb.append("</head>\n\n");

		sb.append("<body onLoad='javascript: focusIt();' bgcolor='#ffffff' "
				+ "bottommargin='0' leftmargin='0' marginheight='0' marginwidth='0' rightmargin='0' topmargin='0'>\n");

		sb.append("<div id='flashcontent'>");
		sb.append("<strong><a href='http://www.macromedia.com/go/getflash/' target='_blank'>"
				+ "You need to upgrade your Flash Player by clicking this link</a></strong>");
		sb.append("</div>");

		sb.append("<script type='text/javascript' src='files/userplane-webmessenger/flashobject.js'></script>\n");
		sb.append("<script type='text/javascript'>\n");
		sb.append("	// <![CDATA[\n");
		sb.append("	var fo = new FlashObject('http://swf.userplane.com/Webmessenger/ic.swf', 'wm', '100%', '100%', '6', '#ffffff', false, 'best');\n");
		sb.append("	fo.addParam('scale', 'noscale');\n");
		sb.append("	fo.addParam('menu', 'false');\n");
		sb.append("	fo.addParam('salign', 'LT');\n");
		sb.append("	fo.addVariable('server', '" + flashcomServer + "');\n");
		sb.append("	fo.addVariable('swfServer', 'swf.userplane.com');\n");
		sb.append("	fo.addVariable('applicationName', 'Webmessenger');\n");
		sb.append("	fo.addVariable('domainID', '" + domainID + "');\n");
		sb.append("	fo.addVariable('sessionGUID', '" + sessionGUID + "');\n");
		sb.append("	fo.addVariable('key', '');\n");
		sb.append("	fo.addVariable('locale', 'english');\n");
		sb.append("	fo.addVariable('destinationMemberID', '" + target.getId() + "');\n");
		sb.append("	fo.addVariable('resizable', 'true');\n");
		sb.append("	fo.write('flashcontent');\n");
		sb.append("	// ]]>\n");
		sb.append("</script>\n");

		sb.append("</body>");
		sb.append("</html>");

		return new StringResult(sb, "text/html");
	}

	/**
	 * This main method will open up the frameset for the flash app and adverts
	 */
	@Override
	public Object main() throws ServletException {

		if (!Module.UserplaneMessenger.enabled(context)) {
			return HttpServletResponse.SC_FORBIDDEN;
		}

		if (account == null) {
			return HttpServletResponse.SC_BAD_REQUEST;
		}

		if (target == null) {
			return HttpServletResponse.SC_BAD_REQUEST;
		}

		StringBuilder sb = new StringBuilder();

		sb.append("<html>\n");
		sb.append("<head></head>\n\n");
		sb.append("<frameset rows='*,108' framespacing='0' frameborder='no' border='0'>\n");
		sb.append("<frame src='" + new Link(UserplaneMessengerHandler.class, "app", "target", target)
				+ "' name='Webmessenger_Frame'  scrolling='NO' noresize>\n");
		sb.append("<frame src='http://subtracts.userplane.com/mmm/bannerstorage/ch_int_frameset.html?app=wm&zoneID=" + userplaneSettings.getBannerZoneId()
				+ "&textZoneID=" + userplaneSettings.getTextZoneId() + "' name='Ad_Frame' scrolling='NO' noresize>\n");
		sb.append("</frameset>\n\n");
		sb.append("</html>");

		return new StringResult(sb, "text/html");
	}
}
