package org.sevensoft.ecreator.iface.frontend.account.subscription;

import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.ErrorDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.panels.SubscriptionCompletionPanel;
import org.sevensoft.ecreator.iface.frontend.account.subscription.panels.SubscriptionLevelsPanels;
import org.sevensoft.ecreator.iface.frontend.ecom.payments.panels.PaymentTable;
import org.sevensoft.ecreator.model.accounts.subscriptions.Subscription;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionModule;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionRate;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionSession;
import org.sevensoft.ecreator.model.ecom.payments.PaymentRedirect;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.vouchers.Voucher;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.RadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.commons.samdate.DateTime;

/**
 * @author sks 11 May 2006 14:25:36
 *
 */
@Path( { "sub.do", "subscriptions.do" })
public class SubscriptionHandler extends FrontendHandler {

	private static final String			CompletedTitle	= "Subscription completed";
	private static final String			PaymentTitle	= "Subscription payment";
	private static final String			PackageTitle	= "Subscription";

	private PaymentType				paymentType;
	private SubscriptionSession			subscriptionSession;
	private String					password;
	private SubscriptionLevel			subscriptionLevel;
	private SubscriptionRate			subscriptionRate;
	private final transient PaymentSettings	paymentSettings;
	private boolean					editSubscriptionRate;
	private boolean					editSubscriptionLevel;
	private Voucher					voucher;
	private boolean					editVoucher;
	private boolean					subscribe;

	public SubscriptionHandler(RequestContext context) {
		super(context);

		this.paymentSettings = PaymentSettings.getInstance(context);

		this.subscriptionSession = SubscriptionSession.getBySession(context, context.getSessionId());
		assert subscriptionSession != null;
	}

	private Object askPaymentType() throws ServletException {

		/*
		 * Only use online payments for subscriptions
		 */
		final Set<PaymentType> paymentTypes = paymentSettings.getPaymentTypes();
		if (paymentTypes.isEmpty()) {
			return new ErrorDoc(context, "No payment types configured");
		}

		if (paymentTypes.size() == 1) {

			paymentType = paymentTypes.iterator().next();
			logger.fine("[SubscriptionHandler] only one payment type, auto setting=" + paymentType);
			return setPaymentType();
		}

		/*
		 * Otherwise render page to choose subscriptions
		 */
		FrontendDoc doc = new FrontendDoc(context, PaymentTitle);
		doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		doc.addTrail(SubscriptionHandler.class, "process", PackageTitle);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new PaymentTable(context, paymentTypes, SubscriptionHandler.class, "setPaymentType"));
				return sb.toString();
			}
		});

		return doc;
	}

	private Object askSubscriptionLevel() throws ServletException {

		logger.fine("[SubscriptionHandler] askSubscriptionLevel");

		//	 get available subscriptions
		final List<SubscriptionLevel> subscriptionLevels = account.getVisibleSubscriptionLevels();

		if (subscriptionLevels.size() == 1) {

			logger.fine("[SubscriptionHandler] only one subscriptionLevel, auto setting");
			subscriptionLevel = subscriptionLevels.get(0);
			return setSubscriptionLevel();
		}

		final Subscription sub = account.getSubscription();

		FrontendDoc doc = new FrontendDoc(context, "Account subscription");
		doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		doc.addTrail(SubscriptionHandler.class, "process", "Account subscription", "editSubscriptionLevel", true);
        final SubscriptionModule module = account.getItemType().getSubscriptionModule();
        if (module.hasRatesHeader()) {
            doc.addBody(module.getRatesHeader());
        }
        doc.addBody(new Body() {

			/**
			 * Will show details of a current subscription
			 */
			private void current() {

				sb.append("<div class='current'>");

				if (sub.hasSubscriptionLevel()) {

					sb.append("Your current subscription level: <b>" + sub.getSubscriptionLevel().getName() + "</b><br/>");

					if (sub.isSubscriptionExpired()) {

						sb.append("Your subscription expired on: <b>" + sub.getSubscriptionExpiryDate().toString("dd-MMM-yy") + "</b><br/>");

					} else if (sub.hasSubscriptionExpiryDate()) {

						sb.append("Your subscription is due to expire on: <b>" + sub.getSubscriptionExpiryDate().toString("dd-MMM-yy") +
								"</b><br/>");
					}

				} else {

					sb.append("You are not currently subscribed<br/>");
				}

				if (account.getItemType().getAccountModule().isShowAccount()) {

					sb.append("<br/>Return to your " + new LinkTag(AccountHandler.class, null, "account") + " or " +
							new LinkTag(LoginHandler.class, "logout", "logout"));
				}

				sb.append("</div><br/><br/>");
			}

			@Override
			public String toString() {

				current();

				if (account.getItemType().getSubscriptionModule().hasSubscriptionsContent()) {
					sb.append(account.getItemType().getSubscriptionModule().getSubscriptionsContent());
				}

				sb.append(new SubscriptionLevelsPanels(context, account, subscriptionLevels));

				return sb.toString();
			}

		});
        if (module.hasRatesFooter()) {
            doc.addBody(module.getRatesFooter());
        }
        return doc;
	}

	private Object askSubscriptionRate() throws ServletException {

		// get available subscription rates
		final SubscriptionModule module = account.getItemType().getSubscriptionModule();
		final List<SubscriptionRate> rates = subscriptionSession.getSubscriptionLevel().getSubscriptionRates();
		logger.fine("[SubscriptionHandler] subscription rates=" + rates);

		/*
		 * If we only have one rate, then simply set it - we do not need another screen for the customer to select
		 * a single option - they will be aware of price from subscription chooser as that will show the single rate.
		 */
		if (rates.size() == 1) {

			logger.fine("[SubscriptionHandler] only one SubscriptionRate, auto setting");

			subscriptionRate = rates.get(0);
			return setSubscriptionRate();
		}

		FrontendDoc doc = new FrontendDoc(context, "Account subscription");
		if (account.getItemType().getAccountModule().isShowAccount()) {
			doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		}
		doc.addTrail(SubscriptionHandler.class, "process", "Account subscription", "editSubscriptionRate", true);

		if (module.hasRatesHeader()) {
			doc.addBody(module.getRatesHeader());
		}

		doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new FormTag(SubscriptionHandler.class, "setSubscriptionRate", "get"));
                for (SubscriptionRate rate : rates) {

                    LinkTag link = new LinkTag(SubscriptionHandler.class, "setSubscriptionRate", rate.getDescription());
                    link.addParameter("subscriptionRate", rate);
                    sb.append(link);
                    sb.append("<br/>");
                }

                sb.append("</form>");
                return sb.toString();
            }

		});

		if (module.hasRatesFooter()) {
			doc.addBody(module.getRatesFooter());
		}

		return doc;
	}

	private Object askVoucher() throws ServletException {

		FrontendDoc doc = new FrontendDoc(context, "Voucher");
		doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		doc.addTrail(SubscriptionHandler.class, "process", "Voucher", "editVoucher", true);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("<div class='ec_formtext'>If you have a voucher please enter it here and then click 'Use voucher'."
						+ "Otherwise click 'No voucher'.</div>");

				sb.append(new FormTag(SubscriptionHandler.class, "setVoucher", "post"));

				sb.append(new TextTag(context, "voucher", 30));
				sb.append("<div class='ec_formaction'>" + new SubmitTag("Continue") + "</div>");
				sb.append("<div class='ec_formaction'>" + new SubmitTag("No voucher") + "</div>");

				sb.append("</form>");
				return sb.toString();
			}

		});
		return doc;
	}

	/**
	 * Just shows the completed screen. 
	 * The callbacks will take care of the subscription.
	 * 
	 */
	public Object completed() throws ServletException {

		if (account == null) {
			return index();
		}

		final Subscription sub = account.getSubscription();

		if (paymentType != null) {
			paymentType.inlineCallback(context, getParameters(), getRemoteIp());
		}

		FrontendDoc doc = new FrontendDoc(context, CompletedTitle);
		if (account.getItemType().getAccountModule().isShowAccount()) {
			doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		}
		doc.addTrail(SubscriptionHandler.class, "completed", CompletedTitle);
		if (account.getItemType().getSubscriptionModule().hasCompletionContent()) {
			doc.addBody(account.getItemType().getSubscriptionModule().getCompletionContent());
		} else {
			doc.addBody(new SubscriptionCompletionPanel(context, account, sub));
		}
        if (subscriptionSession.hasSubscriptionRate()) {
            final List<SubscriptionRate> rates = subscriptionSession.getSubscriptionLevel().getSubscriptionRates();
            doc.addBody("Upgrade your subscription");
            doc.addBody("<br>");
            for (SubscriptionRate rate : rates) {
                if (!subscriptionSession.getSubscriptionRate().equals(rate)) {
                    LinkTag link = new LinkTag(SubscriptionHandler.class, "setSubscriptionRate", rate.getDescription());
                    link.addParameter("subscriptionRate", rate);
                    doc.addBody(link);
                    doc.addBody("<br>");
                }
            }

        }
        sub.setSubscriptionPurchaseDate(new DateTime());
        sub.save();
        return doc;
	}

	private Object confirmation() {

		logger.fine("[SubscriptionHandler] showing confirmation");

		String title = "Subscription confirmation";
		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(SubscriptionHandler.class, "process", title);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(SubscriptionHandler.class, "process", "post"));
				sb.append(new HiddenTag("subscribe", true));

				sb.append(new TableTag("form").setCaption("Confirmation"));

				sb
						.append("<tr><td colspan='2'>Confirm your subscription details and press the 'Subscribe' button to continue to the payment page.</td></tr>");

				sb.append("<tr><td class='key' valign='top'>Subscription:</td><td>" + subscriptionSession.getSubscriptionLevel().getName() +
						"</td></tr>");
				sb
						.append("<tr><td class='key' valign='top'>Cost:</td><td>" + subscriptionSession.getSubscriptionRate().getDescription() +
								"</td></tr>");

				sb.append("</table>");

				sb.append("<div class='ec_formaction'>" + new SubmitTag("Subscribe") + "</div>");

				sb.append("</form>");

				return sb.toString();
			}
		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		if (account == null) {
			return new LoginHandler(context, new Link(SubscriptionHandler.class)).main();
		}

		// delete off old sessions first
		SubscriptionSession.delete(context, context.getSessionId());

		subscriptionSession = new SubscriptionSession(context, context.getSessionId());
		return process();
	}

	/*
	 * The main method will show current subscription level and renewal date, etc, along with subscription table.
	 * Just like gameknot.
	 * http://gameknot.com/premium.pl
	 * 
	 */
	public Object process() throws ServletException {

		// make sure we are logged in
		if (account == null) {
			return new LoginHandler(context, new Link(SubscriptionHandler.class)).main();
		}

		// make sure the currently logged in member owns this session
		subscriptionSession.setAccount(account);

		final Subscription sub = account.getSubscription();

		/*
		 * If no subscriptions available then there is nothing to show so exit.
		 */
		if (!account.hasLiveSubscriptionLevels()) {

			logger.fine("[SubscriptionHandler] No available subscriptions, exiting to account");
			return new AccountHandler(context).main();
		}

		// if no subscription set in session then let the customer choose
		if (!subscriptionSession.hasSubscriptionLevel()) {

			logger.fine("[SubscriptionHandler] No subscription set, showing ask subscription screen");
			return askSubscriptionLevel();
		}

		// if no subscription set in session then let the customer choose
		if (editSubscriptionLevel) {

			logger.fine("[SubscriptionHandler] asking to edit subscription");
			return askSubscriptionLevel();
		}

		/*
		 * If we have vouchers available and we haven't yet been offered one, then ask here
		 */
		if (Module.Vouchers.enabled(context) && Voucher.isSubscription(context)) {

			logger.fine("[SubscriptionHandler] vouchers available");

			if (!subscriptionSession.isVoucherOffered()) {
				logger.fine("[SubscriptionHandler] vouchers not yet offered, showing voucher screen");
				return askVoucher();
			}

			if (editVoucher) {
				logger.fine("[SubscriptionHandler] asking to edit voucher");
				return askVoucher();
			}
		}

		/*
		 * If the subscription has no rates then we will by default subscribe
		 * for a free lifetime subscription
		 */
		if (!subscriptionSession.getSubscriptionLevel().hasSubscriptionRates()) {

			logger.fine("[SubscriptionHandler] no rates available for selected subscription level, so auto subscribing for free");

			sub.subscribe(subscriptionSession.getSubscriptionLevel());
			return completed();
		}

		/*
		 * Otherwise we will ask the user to select a subscription rate
		 */
		if (!subscriptionSession.hasSubscriptionRate()) {

			logger.fine("[SubscriptionHandler] No subscription rate selected, showing ask subscription page");
			return askSubscriptionRate();
		}

		// if no subscription set in session then let the customer choose
		if (editSubscriptionRate) {

			logger.fine("[SubscriptionHandler] asking to edit subscription rate");
			return askSubscriptionRate();
		}

		// if the chosen rate is free then just subscribe without payment.
		if (subscriptionSession.getSubscriptionRate().isFree()) {

			logger.fine("[SubscriptionHandler] subscription rate is free, so we do not need payment, auto subscribing");
			sub.subscribe(subscriptionSession.getSubscriptionRate(), null, getRemoteIp());
			return completed();
		}

		// so subscription rate is set, and requires payment, do we have a payment type ?
		if (!subscriptionSession.hasPaymentType()) {

			logger.fine("[SubscriptionHandler] we need payment and no type is set, showing ask payment screen");
			return askPaymentType();
		}

		// if subscribe then subscribe, otherwise show confirmation
		if (subscribe) {

			if (subscriptionSession.getPaymentType() == PaymentType.Account) {

				logger.fine("[SubscriptionHandler] account payment, auto subscribing");
				subscriptionSession.subcribe(null, context.getRemoteIp());
				return completed();

			} else if (subscriptionSession.getPaymentType().isForm()) {

				// ok, everything we need now, lets forward to payment server
				logger.fine("[SubscriptionHandler] forwarding to payment server.");
				return new PaymentRedirect(context, subscriptionSession);

			} else {

				return completed();
			}
		}

		logger.fine("[SubscriptionHandler] showing confirmation");
		return confirmation();

	}

	public Object setPaymentType() throws ServletException {

		subscriptionSession.setPaymentType(paymentType);
		subscriptionSession.save();

		return process();
	}

	public Object setSubscriptionLevel() throws ServletException {

		subscriptionSession.setSubscriptionLevel(subscriptionLevel);
		subscriptionSession.save();

		return process();
	}

	public Object setSubscriptionRate() throws ServletException {

		subscriptionSession.setSubscriptionRate(subscriptionRate);
		subscriptionSession.save();

		return process();
	}

	public Object setVoucher() throws ServletException {

		subscriptionSession.setVoucherOffered(true);
		subscriptionSession.setVoucher(voucher);
		subscriptionSession.save();

		return process();
	}

	@Deprecated
	public Object start() throws ServletException {
		return main();
	}
}
