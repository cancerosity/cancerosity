package org.sevensoft.ecreator.iface.frontend.items.feeds;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.jdom.JDOMException;
import org.sevensoft.ecreator.model.feeds.Feed;
import org.sevensoft.ecreator.model.feeds.FeedException;
import org.sevensoft.ecreator.model.feeds.ExportFeed;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.StreamResult;

import com.enterprisedt.net.ftp.FTPException;

/**
 * @author sks 29-Mar-2006 15:12:15
 *
 */
@Path("feed-export.do")
public class FeedExportHandler extends Handler {

	private int		id;
	private String	feedClass;
    private int previewCount;

	public FeedExportHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		try {

			Feed feed = EntityObject.getInstance(context, (Class<Feed>) Class.forName(feedClass), id);

			File file = File.createTempFile("feed_export", null);
			file.deleteOnExit();

            ((ExportFeed)feed).setPreviewCount(previewCount);
			feed.run(file);

			StreamResult result = new StreamResult(file, "text/plain");
			result.deleteOnExit();
			return result;

		} catch (ClassNotFoundException e) {
			return HttpServletResponse.SC_BAD_REQUEST;

		} catch (IOException e) {
			e.printStackTrace();
			return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

		} catch (FTPException e) {
			e.printStackTrace();
			return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

		} catch (FeedException e) {
			e.printStackTrace();
			return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

		} catch (JDOMException e) {
			e.printStackTrace();
			return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		}

	}

	@Override
	protected boolean runSecure() {
		return false;
	}

}
