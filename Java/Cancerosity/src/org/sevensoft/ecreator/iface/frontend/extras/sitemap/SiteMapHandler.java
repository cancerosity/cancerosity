package org.sevensoft.ecreator.iface.frontend.extras.sitemap;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.misc.sitemap.SiteMap;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 02-Nov-2005 09:45:04
 * 
 */
@Path("sitemap.do")
public class SiteMapHandler extends FrontendHandler {

	public SiteMapHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() {

		FrontendDoc doc = new FrontendDoc(context, "Sitemap");
		doc.addTrail(SiteMapHandler.class, "Sitemap");
		doc.addBody(new SiteMap(context));
		return doc;

	}

}
