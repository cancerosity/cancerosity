package org.sevensoft.ecreator.iface.frontend.extras.rss;

import java.io.IOException;
import java.net.MalformedURLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.model.extras.rss.Newsfeed;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

import com.sun.syndication.io.FeedException;

/**
 * @author Stephen K Samuel samspade79@gmail.com 24 Mar 2009 19:10:48
 */
@Path("cron-newsfeeds.do")
public class NewsfeedRefreshHandler extends Handler {

    public NewsfeedRefreshHandler(RequestContext context) {
        super(context);
    }

    @Override
    protected Object init() throws ServletException {
        return null;
    }

    @Override
    public Object main() throws ServletException {

        for (Newsfeed newsfeed : Newsfeed.get(context)) {

            try {

                newsfeed.refresh();

            } catch (IllegalArgumentException e) {
                e.printStackTrace();

            } catch (MalformedURLException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();

            } catch (FeedException e) {
                e.printStackTrace();
            }
        }

        return HttpServletResponse.SC_NO_CONTENT;
    }

    @Override
    protected boolean runSecure() {
        return false;
    }

}
