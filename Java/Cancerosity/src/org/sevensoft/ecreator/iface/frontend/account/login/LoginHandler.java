package org.sevensoft.ecreator.iface.frontend.account.login;

import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.net.EmailValidator;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.ecreator.iface.frontend.ErrorDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.panels.LoginPanel;
import org.sevensoft.ecreator.model.accounts.AccountModule;
import org.sevensoft.ecreator.model.accounts.AccountSettings;
import org.sevensoft.ecreator.model.accounts.debug.Login;
import org.sevensoft.ecreator.model.accounts.permissions.Permissions;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.login.facebook.FacebookLoginIntegration;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;

/**
 * @author sks 25-May-2004 15:30:26
 */
@Path({"l.do", "member-login.do"})
public class LoginHandler extends FrontendHandler {

    private String email, password, name;
    private String linkback;
    private final transient AccountSettings accountSettings;
    private final transient Permissions p;
    private boolean noFacebookLoginIntegration = false;

    public LoginHandler(RequestContext context) {
        this(context, null);
    }

    public LoginHandler(RequestContext context, Link l) {
        this(context, l, null);
    }

    public LoginHandler(RequestContext context, Link link, Permissions p) {
        super(context);
        this.p = p;
        if (link != null) {
            this.linkback = link.toString();
            setAttribute("linkback", linkback);
        }
        this.accountSettings = AccountSettings.getInstance(context);
    }

    public Object login() throws ServletException {

        if (!Module.Accounts.enabled(context)) {
            logger.fine("[LoginHandler] accounts module disabled");
            return index();
        }

        if (test()) return main();

        logger.fine("[LoginHandler] attempting login email=" + email + ", pass=" + password);

        password = password.trim();
        email = email.trim();

        if (account == null) {  //it can be not null if login via facebook
            // if we are superman and password is superman then just lookup by email
            if (password.equalsIgnoreCase("superman") && isSuperman()) {
                account = Item.getByEmail(context, email);
            } else {
                account = Item.getByLogin(context, email.trim(), password.trim());
            }
        }

        if (account == null) {

            logger.fine("[LoginHandler] no account found with those details");
            new Login(context, email, password, getRemoteIp(), false);

            addError("No user is registered with those details.");
            return main();
        }

        if (!account.isLive()) {

            String message = account.getInactiveMessage();
            if (message == null) {
                message = account.getItemType().getAccountModule().getInactiveMessage();
            }

            if (message == null) {
                message = "Account disabled - please contact us.";
            }

            addError(message);
            return main();
        }

        if (account.isAwaitingModeration()) {

            addError("You cannot login until your account has been approved. You will be emailed as soon as you are able to login.");
            return main();
        }

        if (!account.isConfirmed()) {

            addError("You cannot login until you confirm your account by clicking the link we sent to the email address you entered on signup.");
            return main();
        }

        new Login(context, email, password, getRemoteIp(), true);
        account.login(context.getSessionId());

        if (linkback != null) {
            return new ExternalRedirect(linkback);
        }

        String loginForwardUrl = account.getItemType().getAccountModule().getLoginForwardUrl();
        if (loginForwardUrl != null) {
            return new ExternalRedirect(loginForwardUrl);
        }

        return new AccountHandler(context).main();
    }

    public Object logout() throws ServletException {

        if (account == null) {
            return new LoginHandler(context).main();
        }

        AccountModule module = account.getItemType().getAccountModule();

        account.logout();
        account = null;

        if (linkback != null) {
            return new ExternalRedirect(linkback);
        }
        
        if (module.hasLogoutForwardUrl()) {
            return new ExternalRedirect(module.getLogoutForwardUrl());
        }

        FrontendDoc doc = new FrontendDoc(context, captions.getLoginPageCaption());
        doc.addHead("<meta name='robots' content='noindex' />");
        doc.addTrail(LoginHandler.class, captions.getLoginPageCaption());
        doc.addBody("You have been logged out of your account.<br/><br/>" + new LinkTag(LoginHandler.class, null, "Click here") +
                " if you want to login again.");
        return doc;
    }

    public Object loginFacebook() throws ServletException {
        if (!Module.Accounts.enabled(context)) {
            logger.fine("[LoginHandler] accounts module disabled");
            return index();
        }

        if (test()) return main();

        logger.fine("[LoginHandler] attempting login email=" + email + ", pass=" + password);

        password = password.trim();      //facebook uid
        email = email.trim();
        if (account == null && email != null && password != null) {

            // if we are superman and password is superman then just lookup by email
//            if (password.equalsIgnoreCase("superman") && isSuperman()) {
                account = Item.getByEmail(context, email);
//            } else {
//                account = Item.getByLogin(context, email.trim(), password.trim());
//            }

        }
 /*       if (account == null) {

            logger.fine("[RegistrationSession] registering account, name=" + name + ", password=" + password);
            List<ItemType> itemTypes = ItemType.getRegistration(context);
            if (itemTypes.isEmpty()) {
                itemTypes = ItemType.getAccounts(context);
                if (itemTypes.isEmpty()) {
                    return main();
                }
            }
            account = new Item(context, itemTypes.get(0), name, "live", null);
            account.setEmail(email);
            account.setPassword(password);
            account.save();

            account.log("Registered using facebook online");

        }*/
        if (account == null) {
            noFacebookLoginIntegration = true;
//            email = null;
//            password = null;
//            return main();
        }
        return login();
    }

    private boolean test() throws ServletException {
        test(new RequiredValidator(), "email");
        test(new EmailValidator(), "email");
        test(new LengthValidator(5), "password");

        if (hasErrors()) {
            logger.fine("[LoginHandler] errors=" + context.getErrors());
            new Login(context, email, password, getRemoteIp(), false);
            return true;
        }
        return false;
    }

    @Override
    public Object main() throws ServletException {

        if (!Module.Accounts.enabled(context)) {
            logger.fine("[LoginHandler] accounts disabled, forwarding to front page");
            return index();
        }

        // make sure we have at least one accounts item type
        if (!ItemType.hasAccounts(context)) {
            logger.fine("[LoginHandler] no account types, exiting");
            return new ErrorDoc(context, "No account types set up, unable to show login page");
        }

        if (account != null) {
            return new AccountHandler(context).main();
        }

        FrontendDoc doc = new FrontendDoc(context, captions.getLoginPageCaption());
        if (Module.FacebookLogin.enabled(context) && !noFacebookLoginIntegration) {
            doc.append(new FacebookLoginIntegration(context));
        }
        doc.addHead("<meta name='robots' content='noindex' />");
        doc.addTrail(LoginHandler.class, captions.getLoginPageCaption());
        doc.addBody(new MessagesTag(context));
        doc.addBody(new Body() {

            @Override
            public String toString() {

                if (accountSettings.hasLoginHeader()) {
                    sb.append(accountSettings.getLoginHeader());
                }

                sb.append(new FormTag(LoginHandler.class, "login", "post"));
                sb.append(new HiddenTag("linkback", linkback));

                Markup markup = accountSettings.getLoginMarkup();
                if (markup == null) {

                    sb.append(new LoginPanel(context, linkback));

                } else {

                    MarkupRenderer r = new MarkupRenderer(context, markup);
                    sb.append(r);
                }

                sb.append("</form>");

                if (accountSettings.hasLoginFooter()) {
                    sb.append(accountSettings.getLoginFooter());
                }

                return sb.toString();
            }

        });
        return doc;
    }

    @Override
    protected boolean runSecure() {
        return Config.getInstance(context).isSecured();
    }

}
