package org.sevensoft.ecreator.iface.frontend.forum;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.crm.forum.Forum;
import org.sevensoft.ecreator.model.crm.forum.ForumSettings;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 25 Sep 2006 18:59:35
 * 
 * Show the overview of the forums
 *
 */
@Path("board.do")
public class BoardHandler extends ForumSystemHandler {

	private ForumSettings	forumSettings;

	public BoardHandler(RequestContext context) {
		super(context);
		this.forumSettings = ForumSettings.getInstance(context);
	}

	@Override
	public Object main() throws ServletException {

		if (!Module.Forum.enabled(context)) {
			logger.fine("[BoardHandler] Forum module disabled");
			return index();
		}

		final List<Forum> forums = Forum.get(context);

		// strip forums we cannot view
		CollectionsUtil.filter(forums, new Predicate<Forum>() {

			public boolean accept(Forum e) {
				return PermissionType.ViewForum.check(context, account, e);
			}
		});

		// if no forums available then return to index
		if (forums.isEmpty()) {
			logger.fine("[BoardHandler] no accessable forums");
			return index();
		}

		context.setAttribute("pagelink", new Link(BoardHandler.class));

		FrontendDoc doc = new FrontendDoc(context, "Forums");
		doc.addTrail(ForumHandler.class, null, "Forums");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("<style>");
				sb.append("table.forum { border-collapse: collapse; border: 1px solid #cccccc; width: 100%; } ");
				sb.append("table.forum th { padding: 3px; background: #438cb4; color: white; font-weight: bold; "
						+ "font-family: Verdana; font-size: 11px; } ");

				sb.append("table.forum td.category { padding: 4px; background: #eeeeee; border: 1px solid #cccccc; "
						+ "font-family: Arial; font-size: 13px; font-weight: bold; }");

				sb.append("table.forum td { padding: 2px 5px; border: 1px solid #cccccc; font-family: Verdana; font-size: 11px; }");
				sb.append("table.forum td a { color: #003399; text-decoration: underline; font-family: Arial; font-weight: bold; font-size: 12px; } ");
				sb.append("table.forum td a:hover { color: #99ccff; text-decoration: none; } ");
				sb.append("</style>");

				if (forumSettings.hasBoardHeader()) {
					sb.append(HtmlHelper.nl2br(forumSettings.getBoardHeader()));
				}

				sb.append(new TableTag("forum"));

				sb.append("<tr>");
				sb.append("<th>Forum</th>");
				sb.append("<th>Views</th>");
				sb.append("<th>Topics</th>");
				sb.append("<th>Posts</th>");
				sb.append("<th>Last post</th>");
				sb.append("</tr>");

				List<String> categoriesDone = new ArrayList();

				for (Forum forum : forums) {

					if (forum.hasCategory()) {
						if (categoriesDone.contains(forum.getCategory())) {
							sb.append("<tr>");
							sb.append("<td class='category' colspan='5'>" + forum.getCategory() + "</td>");
							sb.append("</tr>");
						}
					}

					sb.append("<tr>");

					sb.append("<td valign='top'>" + new LinkTag(ForumHandler.class, null, forum.getName(), "forum", forum));
					if (forum.hasDescription()) {
						sb.append("<br/>" + forum.getDescription());
					}
					sb.append("</td>");

					sb.append("<td>" + forum.getViews() + "</td>");
					sb.append("<td>" + forum.getTopicCount() + "</td>");
					sb.append("<td>" + forum.getPostCount() + "</td>");
					sb.append("<td>");

					if (forum.hasLastPost()) {

						sb.append(forum.getLastPostTopicTitle());
						sb.append("<br/>by: ");
						sb.append(forum.getLastPostAuthor());
						sb.append("<div align='right'>" + forum.getLastPostDate().toString("HH:mm dd/MM/yy") + "</div>");

					} else {

						sb.append("No posts yet");

					}
					sb.append("</td>");
					sb.append("</tr>");

				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

}
