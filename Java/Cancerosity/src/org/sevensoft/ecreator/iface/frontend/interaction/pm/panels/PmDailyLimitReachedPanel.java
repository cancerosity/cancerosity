package org.sevensoft.ecreator.iface.frontend.interaction.pm.panels;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 5 Apr 2007 17:24:03
 *
 */
public class PmDailyLimitReachedPanel extends Panel {

	public PmDailyLimitReachedPanel(RequestContext context) {
		super(context);
	}

	@Override
	public void makeString() {
		sb.append("You have reached your daily private messages limit. Please upgrade your account.");
	}

}
