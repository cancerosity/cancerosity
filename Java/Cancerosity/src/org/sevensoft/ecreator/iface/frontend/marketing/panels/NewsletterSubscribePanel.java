package org.sevensoft.ecreator.iface.frontend.marketing.panels;

import java.util.List;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.marketing.NewsletterHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.AGrid;
import org.sevensoft.ecreator.model.attributes.renderers.ARenderType;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterControl;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterOption;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 20 Apr 2007 10:38:22
 *
 */
public class NewsletterSubscribePanel extends Panel {

	private final MultiValueMap<Attribute, String>	attributeValues;
	private final NewsletterControl			newsletterControl;
	private final List<Newsletter>			newsletters;

	public NewsletterSubscribePanel(RequestContext context, MultiValueMap<Attribute, String> attributeValues) {
		super(context);
		this.attributeValues = attributeValues;
		this.newsletterControl = NewsletterControl.getInstance(context);
		this.newsletters = Newsletter.get(context);
	}

	@Override
	public void makeString() {

		if (newsletters.isEmpty()) {
			return;
		}

		Category category = (Category) context.getAttribute("category");

		sb.append(new FormTag(NewsletterHandler.class, "subscribe", "post"));
		sb.append(new HiddenTag("category", category));

		sb.append(new TableTag("form"));

		sb.append("<tr><td colspan='2'>");
		sb.append(newsletterControl.getSignupContent());

		sb.append("<br/><br/>" + new TextTag(context, "e", 30) + " " + new ErrorTag(context, "e", "<br/>"));
		sb.append("</td></tr>");

		if (newsletters.size() > 1) {

			sb.append("<tr><td colspan='2' class='text'>"
					+ "Choose which newsletters you want to subscribe to.<br/>You can change your subscriptions at any time.</td></tr>");

			for (Newsletter newsletter : newsletters) {

				sb.append("<tr><td width='10'>" + new CheckTag(context, "newsletters", newsletter, true) + "</td><td>" + newsletter.getName() +
						"</td></tr>");

				if (newsletter.hasDescription()) {
					sb.append("<tr><td width='10'></td><td>" + newsletter.getDescription() + "</td></tr>");
				}

                if (Module.NewsletterNewItems.enabled(context)) {
                    for (NewsletterOption newsletterOption : NewsletterOption.get(context, newsletter)) {
                        sb.append("<tr><td width='10'>" + new CheckTag(context, "newsletterOptions", newsletterOption, true) + "</td><td>"
                                + newsletterOption.getName() +
                                "</td></tr>");
                        if (newsletterOption.hasDescription()) {
                            sb.append("<tr><td width='10'></td><td>" + newsletterOption.getDescription() + "</td></tr>");
                        }
                    }
                }
			}
		}

		sb.append("</table>");

		/* Render attribute options
		 * 
		 */
		if (newsletterControl.hasAttributes()) {

			AGrid agrid = new AGrid(context, null, newsletterControl.getAttributes(), attributeValues, ARenderType.Input);
			agrid.setCaption("Subscriber details");
			sb.append(agrid);

		}

		sb.append(new SubmitTag("Subscribe"));
		sb.append("</form>");

	}

}
