package org.sevensoft.ecreator.iface.frontend.bookings.panels;

import java.util.List;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.bookings.BookingHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.frontend.misc.FieldValueCell;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.iface.frontend.misc.agreements.panels.InlineAgreementPanel;
import org.sevensoft.ecreator.model.bookings.BookingOption;
import org.sevensoft.ecreator.model.bookings.sessions.BookingSession;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.misc.agreements.Agreement;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 12 Mar 2007 19:01:38
 *
 */
public class BookingConfirmationPanel extends Panel {

	private final BookingSession	bookingSession;

	public BookingConfirmationPanel(RequestContext context, BookingSession bookingSession) {
		super(context);
		this.bookingSession = bookingSession;
	}

	@Override
	public void makeString() {

		sb.append(new FormTag(BookingHandler.class, null, "post"));
		sb.append(new HiddenTag("book", true));

		// Booking info
		sb.append(new FormFieldSet("Booking details"));

		sb.append(new FieldValueCell(bookingSession.getItem().getItemType().getName(), bookingSession.getItem().getName()));
		sb.append(new FieldValueCell("Date", "From " + bookingSession.getStart().toString("dd-MMM-yyyy") + " to " +
				bookingSession.getEnd().toString("dd-MMM-yyyy") + "<br/>" + bookingSession.getStay() + " nights"));

		sb.append(new FieldValueCell("Occupants", bookingSession.getAdults() + " adults and " + bookingSession.getChildren() + " children"));

		sb.append("<div class='text'>To change these details " + new LinkTag(BookingHandler.class, null, "click here", "editOptions", true) + "</div>");

		sb.append("</fieldset>");

		sb.append(new FormFieldSet("Personal details"));

		sb.append(new FieldValueCell("Your name", bookingSession.getAccountName()));
		sb.append(new FieldValueCell("Your email address", bookingSession.getAccountEmail()));

		sb.append("<div class='text'>To change these details " + new LinkTag(BookingHandler.class, null, "click here", "editDetails", true) + "</div>");

		sb.append("</fieldset>");

		List<BookingOption> options = bookingSession.getOptions();
		if (options.size() > 0) {

			// Booking info
			sb.append(new FormFieldSet("Extra options"));
			sb.append("<div class='text'>");

			for (BookingOption option : options) {
				sb.append(option.getName() + " at <strong>" + context.getAttribute("currencySymbol") + option.getChargeInc() + "</strong><br/>");
			}

			sb.append("</div>");
			sb.append("<div class='text'>To change the selected options " + new LinkTag(BookingHandler.class, null, "click here", "editOptions", true) +
					"</div>");

			sb.append("</fieldset>");

		}

		// Booking info

		if (PaymentSettings.getInstance(context).getPaymentTypes().size() > 1) {
			sb.append(new FormFieldSet("Payment "));

			sb.append("<div class='text'>Your payment method is <strong>" + bookingSession.getPaymentType().getPublicName() + "</strong></div>");
			sb.append("<div class='text'>To change the payment method " + new LinkTag(BookingHandler.class, null, "click here", "editPaymentType", true) +
					"</div>");

			sb.append("</fieldset>");
		}

		if (bookingSession.hasCard()) {
			sb.append(new FormFieldSet("Card details "));

			sb.append("<div class='text'>You are paying on card <strong>" + bookingSession.getCard().getSecureDescription() + "</strong></div>");
			sb.append("<div class='text'>To change the card details " + new LinkTag(BookingHandler.class, null, "click here", "editCardDetails", true) +
					"</div>");

			sb.append("</fieldset>");
		}

		sb.append(new FormFieldSet("Total charges"));

		sb.append("<div class='text'>The total cost of this booking is <strong>" + context.getAttribute("currencySymbol") + bookingSession.getChargeInc() +
				"</strong></div>");
		sb.append("</fieldset>");
		/*
		 * Do any inline agreements
		 */
		if (bookingSession.getBookingModule().hasAgreement()) {

			Agreement agreement = bookingSession.getBookingModule().getAgreement();
			if (agreement.isInline()) {
				sb.append(new InlineAgreementPanel(context, agreement));
			}
		}

		sb.append("<div class='ec_form_commands'>");
		sb.append(new SubmitTag("Complete booking"));
		sb.append(new ButtonTag(CategoryHandler.class, null, "Cancel booking"));
		sb.append("</div>");

		sb.append("</form>");
	}
}
