package org.sevensoft.ecreator.iface.frontend.affiliates;

import javax.servlet.ServletException;

import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 23 Feb 2007 08:00:47
 *
 */
@Path("affil.do")
public class AffiliateLandingHandler extends Handler {

	private String	url;

	public AffiliateLandingHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		return context.getRequest().getRequestDispatcher(url);
	}

	@Override
	protected boolean runSecure() {
		return false;
	}

}
