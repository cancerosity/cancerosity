package org.sevensoft.ecreator.iface.frontend.ecom.payments;

import javax.servlet.ServletException;

import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.ExternalRedirect;

/**
 * @author sks 18 Jan 2007 10:14:48
 *
 */
@Path("payments-test.do")
public class TestPaymentHandler extends Handler {

	private String	transactionId;
	private String	url;

	public TestPaymentHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		// just return to the payment success url
		return new ExternalRedirect(url + "&transactionId=" + transactionId);
	}

	@Override
	protected boolean runSecure() {
		return false;
	}

}
