package org.sevensoft.ecreator.iface.frontend.attachments;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.ServletException;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.frontend.ErrorDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.payments.panels.PaymentTable;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.downloads.Download;
import org.sevensoft.ecreator.model.attachments.downloads.DownloadSession;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.ecom.credits.CreditPackage;
import org.sevensoft.ecreator.model.ecom.payments.PaymentRedirect;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 10 Jan 2007 06:45:17
 *
 */
@Path("dl.do")
public class DownloadHandler extends FrontendHandler {

	@SuppressWarnings("hiding")
	private static Logger				logger	= Logger.getLogger(DownloadHandler.class.getName());

	private Attachment				attachment;
	private final transient DownloadSession	session;
	private CreditPackage				creditPackage;
	private PaymentType				paymentType;
	private boolean					editPaymentType;
	private boolean					editCreditPackage;
	private boolean					download;

	public DownloadHandler(RequestContext context) {
		this(context, null);
	}

	public DownloadHandler(RequestContext context, Attachment attachment) {
		super(context);
		this.attachment = attachment;
		this.session = DownloadSession.get(context, context.getSessionId());
	}

	private Object askCreditPackage() {

		final List<CreditPackage> packages = account.getCreditStore().getCreditPackages();
		if (packages.isEmpty()) {
			return new ErrorDoc(context, "No credit packages available");
		}

		final int credits = account.getCreditStore().getCredits();
		final int required = session.getAttachment().getCredits();

		String title = "Choose credit package";
		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("You currently have " + credits + " credits and this download requires " + required + ".<br/>");
				sb.append("Please choose a package to top up your credits before you download.<br/><br/>");

				for (CreditPackage cp : packages) {

					sb.append(new TableTag("ec_listingpackage"));

					String name;
					if (cp.hasName()) {
						name = cp.getName();
					} else {
						name = cp.getDescription();
					}

					sb.append("<tr><th>" + new LinkTag(DownloadHandler.class, "setCreditPackage", name, "creditPackage", cp) + "</th></tr>");
					sb.append("<tr><td><b>Cost:</b> " + cp.getDescription() + "</td></tr>");

					if (cp.isIntroductory()) {
						sb.append("<tr><td><b>Introductory offer:</b> This package is an introductory offer and can only be selected "
								+ "the first time you purchase credits.</td></tr>");
					}

					sb.append("</table>");

				}

				return sb.toString();
			}

		});
		return doc;
	}

	private Object askPaymentType() throws ServletException {

		logger.fine("[DownloadHandler[ showing payment screen");

		/*
		 * Get all payment types applicable for all groups this member is in
		 */
		final Set<PaymentType> paymentTypes = PaymentSettings.getInstance(context).getPaymentTypes();

		if (paymentTypes.size() == 0) {
			return new ErrorDoc(context, "There are no payment methods configured");
		}

		/*
		 * if we only have one payment method then check if we can forward on automatically or if it is a payment form that requires a post we
		 * must still show the payment option.
		 */
		if (paymentTypes.size() == 1) {
			Iterator<PaymentType> iter = paymentTypes.iterator();
			paymentType = iter.next();
			return setPaymentType();
		}

		String title = "Choose payment method for download";

		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new PaymentTable(context, paymentTypes, DownloadHandler.class, "setPaymentType"));
				return sb.toString();
			}

		});
		return doc;
	}

	public Object completed() {

		if (paymentType != null) {
			logger.fine("[DownloadHandler] payment callback on payment type=" + paymentType);
			paymentType.inlineCallback(context, getParameters(), getRemoteIp());
		}

		final Download download = session.getDownload();

		FrontendDoc doc = new FrontendDoc(context, "Download ready");
		doc.addTrail(DownloadHandler.class, "completed", "Download ready");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				if (download != null) {

					sb.append("Your files are ready to download.<br/>");
					sb.append(download.getDownloadLinkTag());

				} else {

					sb.append("Your file are being prepared for download. You will receive an email shortly with download instructions.");
				}

				return sb.toString();
			}

		});
		return doc;
	}

	private Object confirmation() {

		final int credits = session.getAttachment().getCredits();
		final int current = account.getCreditStore().getCredits();
		final CreditPackage cp = session.getCreditPackage();
		final Money fee = session.getAttachment().getFee();

		final Set<PaymentType> paymentTypes = PaymentSettings.getInstance(context).getPaymentTypes();
		final List<CreditPackage> packages = account.getCreditStore().getCreditPackages();

		String title = "Confirm download";
		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(DownloadHandler.class, "process", "Confirm download");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("You are downloading: " + session.getAttachment().getName() + "<br/><br/>");

				if (credits > 0) {

					sb.append("This download requires " + credits + " credits and you currently have " + current + " credits.<br/>");

					if (cp != null) {
						sb.append("You are purchasing a credit package of an extra " + cp.getCredits() + " credits for \243" + cp.getFee() + " by " +
								session.getPaymentType().getName() + ".<br/><br/>");
					}

					if (packages.size() > 1) {
						sb.append(new LinkTag(DownloadHandler.class, "process", "Change credit package", "editCreditPackage", true));
						sb.append("<br/>");
					}

					if (paymentTypes.size() > 1) {
						sb.append(new LinkTag(DownloadHandler.class, "process", "Change payment type", "editPaymentType", true));
						sb.append("<br/>");
					}
				}

				if (fee.isPositive()) {
				}

				sb.append("<br/>To proceed please press the download button below.<br/>");
				sb.append(new ButtonTag(DownloadHandler.class, "process", "Download", "download", true));

				return sb.toString();
			}

		});
		return doc;
	}

	public Object declined() {

		FrontendDoc doc = new FrontendDoc(context, "Payment declined");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("Payment was declined or cancelled.");
				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		if (attachment == null) {
			return "No attachment";
		}

		session.reset();
		session.setAttachment(attachment);

		return process();
	}

	public Object process() throws ServletException {

		if (!session.hasAttachment()) {
			return new ErrorDoc(context, "No attachment");
		}

		if (account == null) {
			return new LoginHandler(context, new Link(DownloadHandler.class, "process")).main();
		}

		// if we are logged in, change session to the current account
		if (account != null) {
			logger.fine("[DownloadHandler] setting account=" + account);
			session.setAccount(account);
		}

		int creditsRequired = session.getAttachment().getCredits();
		int creditsAvailable = session.getAccount().getCreditStore().getCredits();

		// do we need credits for this attachment ?
		logger.info("credits required for download=" + creditsRequired);
		logger.info("credits available on account=" + creditsAvailable);

		// check we have enough credits
		if (creditsAvailable < creditsRequired) {

			logger.fine("more credits required");

			if (!session.hasCreditPackage()) {
				logger.fine("no credit package set, asking");
				return askCreditPackage();
			}

			if (editCreditPackage) {
				logger.fine("asking to edit credit package");
				return askCreditPackage();
			}

			if (!session.hasPaymentType()) {
				logger.fine("session has credit package but no payment type set");
				return askPaymentType();
			}

			if (editPaymentType) {
				logger.fine("asking to edit payment type");
				return askPaymentType();
			}
		}

		// confirmation bool
		if (download) {

			// if we require more credits then send off to payment
			if (creditsAvailable < creditsRequired) {

				if (session.getPaymentType().isForm()) {

					return new PaymentRedirect(context, session);

				} else {

					return new ErrorDoc(context, "None form payment selected");

				}

			} else {

				try {

					session.download(null, getRemoteIp());
					return completed();

				} catch (AttachmentExistsException e) {
					e.printStackTrace();

				} catch (AttachmentTypeException e) {
					e.printStackTrace();

				} catch (IOException e) {
					e.printStackTrace();

				} catch (AttachmentLimitException e) {
					e.printStackTrace();
				}

			}

		}

		return confirmation();
	}

	public Object setCreditPackage() throws ServletException {

		session.setCreditPackage(creditPackage);
		return process();
	}

	public Object setPaymentType() throws ServletException {

		session.setPaymentType(paymentType);
		return process();
	}
}
