package org.sevensoft.ecreator.iface.frontend.ecom.shopping;

import java.io.IOException;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;

/**
 * @author sam
 */
@Path("quick-order.do")
public class QuickOrderHandler extends FrontendHandler {

	private String	rows;

	public QuickOrderHandler(RequestContext context) {
		super(context);
	}

	public Object add() throws ServletException {

		if (rows == null)
			return main();

		int lines = 0;

		for (String row : rows.split("\n")) {

			String[] data = row.split("\\s");
			if (data.length > 0) {

				Item item = EntityObject.getInstance(context, Item.class, data[0]);
				if (item != null) {

					int qty = 0;
					if (data.length > 1)
						try {
							qty = Integer.parseInt(data[1]);
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}

					try {

						BasketLine line = basket.addItem(null, item, qty, null, null);

						if (line != null)
							lines++;

					} catch (IOException e) {
						addError(e);
					} catch (AttachmentExistsException e) {
						addError(e);
					} catch (AttachmentTypeException e) {
						addError(e);
					}

				}
			}
		}

		if (lines > 0)
			addMessage("You have added " + lines + " lines to your basket.");

		return new BasketHandler(context).main();
	}

	@Override
	public Object main() {

		FrontendDoc page = new FrontendDoc(context, "Order status");
		page.addTrail(QuickOrderHandler.class, "Order status");
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb
						.append("Add multiple items by entering the product code's and quantity (optional; default is one) into the box below. "
								+ "Please enter one code on each line. You can enter the information manually, or by cutting and pasting from another application. For example, you can enter the following:<br/><br/>"
								+ "100001</br>" + "100002 3<br/>" + "100003 1<br/><br/>"
								+ "This would add one of 100001, three of 100002 and one of 100003.<br/><br/>"
								+ "When you are done, please press the 'Add Items to Cart' button.");

				sb.append(new FormTag(QuickOrderHandler.class, "add", "post"));
				sb.append(new TextAreaTag(context, "rows", 60, 15));
				sb.append("<br/>");
				sb.append(new SubmitTag("Add items to shopping basket"));
				sb.append("</form>");

				return sb.toString();
			}

		});
		return page;
	}
}