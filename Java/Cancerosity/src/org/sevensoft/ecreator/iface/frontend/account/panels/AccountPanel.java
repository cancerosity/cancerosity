package org.sevensoft.ecreator.iface.frontend.account.panels;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.iface.frontend.account.buyers.BuyersHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.profile.ProfileEditHandler;
import org.sevensoft.ecreator.iface.frontend.account.profile.ProfileImagesHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionCancellationHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.orders.OrderHistoryHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.BasketHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.CheckoutHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.OrderStatusHandler;
import org.sevensoft.ecreator.iface.frontend.interaction.buddies.BuddiesHandler;
import org.sevensoft.ecreator.iface.frontend.interaction.pm.PmHandler;
import org.sevensoft.ecreator.iface.frontend.interaction.sms.credits.SmsCreditsPurchaseHandler;
import org.sevensoft.ecreator.iface.frontend.interaction.views.ViewsHandler;
import org.sevensoft.ecreator.iface.frontend.interaction.winks.WinksHandler;
import org.sevensoft.ecreator.iface.frontend.items.listings.AddListingHandler;
import org.sevensoft.ecreator.iface.frontend.items.listings.MyListingsHandler;
import org.sevensoft.ecreator.iface.frontend.items.favourites.FavouritesnewHandler;
import org.sevensoft.ecreator.iface.frontend.meetups.RsvpModerationHandler;
import org.sevensoft.ecreator.model.accounts.subscriptions.Subscription;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.system.config.Captions;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.html.ImageTag;

import java.util.List;

/**
 * @author sks 21 Nov 2006 12:15:15
 * 
 * Default built in acount renderer
 *
 */
public class AccountPanel {

	private final RequestContext	context;
	private final Item		account;
	private final Captions		captions;
	private final Config		config;

	public AccountPanel(RequestContext context, Item active) {
		this.context = context;
		this.account = active;
		this.captions = Captions.getInstance(context);
		this.config = Config.getInstance(context);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

        sb.append(new TableTag("ec_account"));
        sb.append("<tr><td class='header'><h2>" + new ImageTag("files/graphics/pics/account.png") + " " + captions.getAccountCaption() + "</h2>");
        sb.append("</tr></td>");

        sb.append("<tr><td>" + new LinkTag(ProfileEditHandler.class, null, "Edit profile") + "</td></tr>");

        if (Module.MemberProfiles.enabled(context)) {

            if (ItemModule.Profile.enabled(context, account)) {

                sb.append("<tr><td>" + new LinkTag(account.getUrl(), "view profile") + "</td></tr>");
            }

            if (ItemModule.ProfileImages.enabled(context, account)) {

                sb.append("<tr><td>" + new LinkTag(ProfileImagesHandler.class, null, "profile images") + "</td></tr>");
            }
        }

        if (Module.Subscriptions.enabled(context)) {

            Subscription sub = account.getSubscription();

            if (account.hasLiveSubscriptionLevels()) {
                sb.append("<tr><td>" + new LinkTag(SubscriptionHandler.class, null, "subscription") + "</td></tr>");
            }

            if (sub.hasSubscriptionLevel()) {
                sb.append("<tr><td>" + new LinkTag(SubscriptionCancellationHandler.class, null, "cancel subscription") +
                        "</td></tr>");
            }
        }

        if (Module.MemberViewers.enabled(context)) {
            sb.append("<tr><td>" + new LinkTag(ViewsHandler.class, null, "viewers") +
                    "</td></tr>");
        }

//        if (config.getUrl().contains("overfiftiesfriends.co.uk")) {

        if (Module.Subscriptions.enabled(context)) {
            Subscription sub = account.getSubscription();
            SubscriptionLevel subscriptionLevel = sub.getSubscriptionLevel();
            if (subscriptionLevel != null) {

                Date expiryDate = sub.getSubscriptionExpiryDate();
                if (expiryDate == null) {
                    sb.append("<tr><td colspan='2'>Your current subscription level is: " + subscriptionLevel.getName() + ". This is a lifetime subscription<br/></td></tr>");
                }
                else {
                    sb.append("<tr><td colspan='2'>Your current subscription level is: " + subscriptionLevel.getName() + ". You currently have " +
                            expiryDate.getDaysBetween(new Date()) + " days remaining of this subscription.<br/></td></tr>");
                }
            }
            else {
                sb.append("<tr><td colspan='2'>You are not a subscriber yet. Please subscribe by clicking the link below.<br/></td></tr>");
            }
        }
//		}

        if (Module.Meetups.enabled(context)) {

            sb.append("<tr><td>" + new LinkTag(RsvpModerationHandler.class, null, "rsvp moderation") +
                    "</td></tr>");

        }

        if (!config.getUrl().contains("overfiftiesfriends.co.uk")) {

            if (Module.SmsCredits.enabled(context)) {

                if (ItemModule.SmsCredits.enabled(context, account)) {

                    sb.append("<tr><td>" + new LinkTag(SmsCreditsPurchaseHandler.class, null, "purchase sms credits") +
                            "</td></tr>");
                }
            }

            if (Module.Shopping.enabled(context)) {

                if (Module.Buyers.enabled(context)) {

                    if (ItemModule.Buyers.enabled(context, account)) {

                        if (!account.isBuyer()) {

                            sb.append("<tr><td>" + new LinkTag(BuyersHandler.class, null, captions.getBuyersCaptionPlural()) +
                                    "</td></tr>");
                        }
                    }
                }

                sb.append("<tr><td>" + new LinkTag(BasketHandler.class, null, "shopping basket") +
                        "</td></tr>");
                sb.append("<tr><td>" + new LinkTag(CheckoutHandler.class, null, "checkout") + "</td></tr>");

                if (OrderSettings.getInstance(context).isParcels()) {
                    sb.append("<tr><td>" + new LinkTag(OrderStatusHandler.class, null, "Order Status") + "</td><td>- Track your order status</td></tr>");
                }

                if (Module.OrderHistory.enabled(context)) {
                    sb.append("<tr><td>" + new LinkTag(OrderHistoryHandler.class, null, "order history") +
                            "</td></tr>");
                }

            }

        }

        if (Module.Buddies.enabled(context)) {
            sb.append("<tr><td>" + new LinkTag(BuddiesHandler.class, null, "buddies") +
                    "</td></tr>");
        }

        if (Module.PrivateMessages.enabled(context)) {
            sb.append("<tr><td>" + new LinkTag(PmHandler.class, null, "private messages") + "</td></tr>");
        }

        if (Module.Nudges.enabled(context)) {
            sb.append("<tr><td>" + new LinkTag(WinksHandler.class, null, "winks") + "</td></tr>");
        }

        if (Module.Listings.enabled(context)) {

            sb.append("<tr><td class='header'><h2>" + new ImageTag("files/graphics/pics/listings.png") + " My " + captions.getListingsCaption() + "</h2>");
            sb.append("</tr></td>");

            if (account.hasListings()) {

                sb.append("<tr><td>" + new LinkTag(MyListingsHandler.class, null, "View/Edit "+ captions.getListingsCaption()) +
                        "</td></tr>");

            }

            sb.append("<tr><td>" + new LinkTag(AddListingHandler.class, null, "Add "+ captions.getListingsCaption()) + "</td></tr>");

        }

        if (Module.Favourites.enabled(context)) {
            sb.append("<tr><td class='header'><h2>" + new ImageTag("files/graphics/pics/details_grey.png") + captions.getFavouritesCaption() +"</h2></tr></td>");

            sb.append("<tr><td>" + new LinkTag(FavouritesnewHandler.class, null, "View/Edit " + captions.getFavouritesCaption() + " Items") + "</td></tr>");
        }

        sb.append("<tr><td class='header'><h2>" + new ImageTag("files/graphics/pics/logout.png") + " " + new LinkTag(LoginHandler.class, "logout", "Logout") + "</h2></td></tr>");

        sb.append("</table>");

        return sb.toString();
	}
}
