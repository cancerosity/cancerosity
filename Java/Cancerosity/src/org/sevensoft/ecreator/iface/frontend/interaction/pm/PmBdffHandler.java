package org.sevensoft.ecreator.iface.frontend.interaction.pm;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.RestrictionHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionStatusHandler;
import org.sevensoft.ecreator.iface.frontend.interaction.pm.panels.PmComposePanel;
import org.sevensoft.ecreator.iface.frontend.interaction.pm.panels.PmProfilePanel;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.interaction.Interaction;
import org.sevensoft.ecreator.model.interaction.pm.PrivateMessage;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sam
 */
@Path( { "pmbdff.do" })
public class PmBdffHandler extends SubscriptionStatusHandler {

	private String			body;
	private PrivateMessage		message;
	private String			link;
	private List<PrivateMessage>	messages;
	private Item			correspondant;
	private Upload			upload;

	public PmBdffHandler(RequestContext context) {
		super(context);
	}

	public Object inbox() throws ServletException {

		if (account == null) {
			return new LoginHandler(context).main();
		}

		if (!PermissionType.ReadMessages.check(context, account)) {
			if (account.getItemType().getInteractionModule().hasRestrictionForward()) {
				return new ExternalRedirect(account.getItemType().getInteractionModule().getRestrictionForward());
			} else {
				return new RestrictionHandler(context).main();
			}
		}

		String title = "Welcome to your " + captions.getPmCaption() + " mailbox";

		FrontendDoc page = new FrontendDoc(context, title);
		if (account.getItemType().getAccountModule().isShowAccount()) {
			page.addTrail(AccountHandler.class, captions.getAccountCaption());
		}
		page.addTrail(PmBdffHandler.class, "inbox", "Your inbox");
		page.addBody(new Body() {

			public String toString() {

				sb.append(RendererUtil.form1());

				sb.append("<div align='right'>" + new LinkTag(PmBdffHandler.class, "inbox", "Inbox") + " | " +
						new LinkTag(PmBdffHandler.class, "sent", "Sent items") + "</div>");

				sb.append(new TableTag("form1").setCaption(captions.getPmCaptionPlural() + ": Inbox"));

				sb.append("<tr>");
				sb.append("<th>Status</th>");
				sb.append("<th>Date Received</th>");
				sb.append("<th>From</th>");
				sb.append("<th>Message</th>");
				sb.append("<th>Delete</th>");
				sb.append("</tr>");

				sb.append(new FormTag(PmBdffHandler.class, "delete", "POST"));

				if (account.getInteraction().hasPmInbox()) {

					for (PrivateMessage message : account.getInteraction().getPmInbox()) {

						sb.append("<tr>");
						sb.append("<td class='status'>" +
								new LinkTag(PmBdffHandler.class, "view", new ImageTag(message.isRead() ? "template-data/message_read.gif"
										: "template-data/message_new.gif"), "message", message) + "</td>");
						sb.append("<td class='date'>" + message.getDate().toString("HH:mma dd/MM/yyyy").toLowerCase().replace(" ", "&nbsp;") +
								"</td>");

						logger.fine("[PmHandler] sender=" + message.getSender());

						sb.append("<td  class='who'>" + new LinkTag(message.getSender().getUrl(), message.getSender().getDisplayName()) + "</td>");
						sb.append("<td>" + new LinkTag(PmBdffHandler.class, "view", message.getSnippet(30), "message", message) + "</td>");
						sb.append("<td>" + new CheckTag(context, "messages", message, false) + "</td>");
						sb.append("</tr>");

					}

				} else {

					sb.append("<tr><td colspan='5'>You have received no messages yet</td></tr>");

				}

				sb.append("<tr>");
				sb.append("<td colspan='3'>&nbsp;</td>");
				sb.append("<td colspan='2'>" + new SubmitTag("Delete selected items") + "</td>");
				sb.append("</tr>");

				sb.append("</form>");

				if (account.getItemType().getAccountModule().isShowAccount()) {
					sb
							.append("<tr><th colspan='5'>To return to your account " + new LinkTag(AccountHandler.class, null, "click here") +
									"</td></tr>");
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return page;
	}

	@Override
	public Object main() throws ServletException {
		return inbox();
	}

	/**
	 * Show all messages between the active member and the correspondant
	 */
	public Object message() throws ServletException {

		if (account == null) {
			return new LoginHandler(context, new Link(PmBdffHandler.class, "message", "message", message, "correspondant", correspondant)).main();
		}

		if (!PermissionType.ReadMessages.check(context, account)) {
			return new RestrictionHandler(context).main();
		}

		if (message != null) {

			// if message is not null then set correspondant from that
			correspondant = message.getCorrespondant(account);

			// ensure this is our message to view !
			if (!message.getSender().equals(account) && !message.getRecipient().equals(account)) {
				logger.fine("[PrivateMessageHandler] we are neither sender nor recip, exiting");
				return main();
			}

			// if we are the recpient then check on permission
			if (message.getRecipient().equals(account)) {

				if (!PermissionType.ReadMessages.check(context, account)) {
					logger.fine("[PrivateMessageHandler] read messages is restricted, exiting");
					return new RestrictionHandler(context).main();
				}
			}

			// mark this message as read
			message.read();
		}

		if (correspondant == null) {

			logger.fine("[PrivateMessageHandler] read messages is restricted, exiting");
			return index();
		}

		final Interaction interaction = account.getInteraction();

		String title = captions.getPmCaptionPlural() + " exchanged between you and " + correspondant.getDisplayName();

		FrontendDoc doc = new FrontendDoc(context, title);

		if (account.getItemType().getAccountModule().isShowAccount()) {
			doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		}

		if (message != null) {

			if (message.getSender().equals(account)) {

				doc.addTrail(AccountHandler.class, captions.getPmCaption() + " Sent");

			} else {

				doc.addTrail(PmBdffHandler.class, captions.getPmCaption() + " Inbox");
			}

		} else {

			doc.addTrail(PmBdffHandler.class, "view", "Compose message", "correspondant", correspondant);

		}

		doc.addBody(new Body() {

			private void attachments() {

				// show attachments from previous 5 messages
				final List<Attachment> attachments = interaction.getMessageAttachmentsReceived(correspondant, 5);
				if (attachments.isEmpty()) {
					return;
				}

				sb.append(new TableTag("form").setId("ec_pm_attachments").setCaption("Attachments"));
				sb.append("<tr><th colspan='3'>These are the attachments sent to you from " + correspondant.getDisplayName() + "</th></tr>");

				for (Attachment attachment : attachments) {
					sb.append("<tr>");
					sb.append("<td>" + attachment.getName() + "</td>");
					sb.append("<td>" + attachment.getDownloadLinkTag() + "</td>");
					//					sb.append("<td>" + new LinkTag("Remove this file") + "</td></tr>");
					sb.append("</tr>");
				}

				sb.append("</table>");
			}

			private void history() {

				// show previous 5 messages
				final List<PrivateMessage> previousMessages = interaction.getMessages(correspondant, 5);
				if (previousMessages.isEmpty()) {
					return;
				}

				sb.append("<style> table.form#ec_pm_history td.account { font-weight: bold; } </style> ");

				sb.append(new TableTag("form").setId("ec_pm_history").setCaption("History"));
				sb.append("<tr><th colspan='3'>This is your recent message history between you and " + correspondant.getDisplayName() + "</th></tr>");

				for (PrivateMessage previous : previousMessages) {

					sb.append("<tr><td class='account'>At " + previous.getDate().toString("hh:mm dd MMM yyyy") + " " +
							previous.getSender().getDisplayName() + " wrote</td></tr>");

					sb.append("<tr><td class='message'>" + HtmlHelper.nl2br(previous.getContent()) + "</td></tr>");
				}

				sb.append("</table>");
			}

			private void profile() {

				Markup markup = interaction.getModule().getPmProfileMarkup();
				if (markup == null) {

					sb.append(new PmProfilePanel(context, correspondant));

				} else {

					MarkupRenderer r = new MarkupRenderer(context, markup);
					r.setBody(correspondant);
					sb.append(r);
				}
			}

			@Override
			public String toString() {

				profile();
				history();
				attachments();

				sb.append(new PmComposePanel(context, account, correspondant, null));

				return sb.toString();
			}
		});
		return doc;
	}

	/**
	 * Shows sent messages page
	 */
	public Object sent() throws ServletException {

		if (account == null) {
			return new LoginHandler(context).main();
		}

		if (!PermissionType.SendMessage.check(context, account)) {
			if (account.getItemType().getInteractionModule().hasRestrictionForward()) {
				return new ExternalRedirect(account.getItemType().getInteractionModule().getRestrictionForward());
			} else {
				return new RestrictionHandler(context).main();
			}
		}

		String title = "Welcome to your " + captions.getPmCaption() + " mailbox - sent items folder";

		FrontendDoc doc = new FrontendDoc(context, title);
		if (account.getItemType().getAccountModule().isShowAccount()) {
			doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		}
		doc.addTrail(PmBdffHandler.class, "sent", "Your sent messages");
		doc.addBody(new Body() {

			public String toString() {

				sb.append(RendererUtil.form1());

				sb.append("<div align='right'>" + new LinkTag(PmBdffHandler.class, "inbox", "Inbox") + " | " +
						new LinkTag(PmBdffHandler.class, "sent", "Sent items") + "</div>");

				sb.append(new TableTag("form1").setCaption(captions.getPmCaptionPlural() + ": Sent items"));

				sb.append("<tr>");
				sb.append("<th>Date sent</th>");
				sb.append("<th>To</th>");
				sb.append("<th>Message</th>");
				sb.append("<th>Delete</th>");
				sb.append("</tr>");

				sb.append(new FormTag(PmBdffHandler.class, "delete", "POST"));
				sb.append(new HiddenTag(link, new Link(PmBdffHandler.class, "sent")));

				List<PrivateMessage> pmSent = account.getInteraction().getPmSent();
				if (pmSent.size() > 0) {
					
					for (PrivateMessage message : pmSent) {

						sb.append("<tr>");
						sb.append("<td class='date'>" + message.getDate().toString("HH:mma dd/MM/yyyy").toLowerCase().replace(" ", "&nbsp;") +
								"</td>");
						sb.append("<td  class='who'>" + new LinkTag(message.getRecipient().getUrl(), message.getRecipient().getDisplayName()) +
								"</td>");
						sb.append("<td class='subject'>" + new LinkTag(PmBdffHandler.class, "view", message.getSnippet(30), "message", message) +
								"</td>");
						sb.append("<td>" + new CheckTag(null, "messages", message, false) + "</td>");
						sb.append("</tr>");

					}

				} else {

					sb.append("<tr><td colspan='4'>You have received no messages yet</td></tr>");

				}

				sb.append("<tr>");
				sb.append("<td colspan='2'>&nbsp;</td>");
				sb.append("<td colspan='2'>" + new SubmitTag("Delete selected items") + "</td>");
				sb.append("</tr>");

				sb.append("</form>");

				if (account.getItemType().getAccountModule().isShowAccount()) {
					sb
							.append("<tr><th colspan='4'>To return to your account " + new LinkTag(AccountHandler.class, null, "click here") +
									"</td></tr>");
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object view() throws ServletException {
		return message();
	}
}
