package org.sevensoft.ecreator.iface.frontend.search;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.search.content.ContentSearchResult;
import org.sevensoft.ecreator.model.search.content.ContentSearcher;
import org.sevensoft.ecreator.model.stats.searching.SearchPhrase;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

/**
 * @author sks 29 Dec 2006 07:02:28
 * 
 * Content search for searching categories and items
 *
 */
@Path("cs.do")
public class ContentSearchHandler extends FrontendHandler {

	private String	keywords;
	private int		resultsPerPage;
	private int		page;

	public ContentSearchHandler(RequestContext context) {
		super(context);
	}

	private Object empty() {

		FrontendDoc doc = new FrontendDoc(context, "Search results");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("No results found.");
				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

        if (keywords == null || keywords.trim().length() == 0) {
			return empty();
		}

		if (page < 1) {
			page = 1;
		}

		if (resultsPerPage < 1 || resultsPerPage > 100) {
			resultsPerPage = 20;
		}

		ContentSearcher searcher = new ContentSearcher(context, keywords);
		int size = searcher.size();
		if (size == 0) {
			return empty();
		}

		Results results = new Results(size, page, resultsPerPage);

		searcher.setStart(results.getStartIndex());
		searcher.setLimit(resultsPerPage);

		final List<ContentSearchResult> searchResults = searcher.getResults();

		// only log search phrase after we know it found something
		SearchPhrase.process(context, keywords);

		// get a link back here for results control
		Link link = (Link) context.getAttribute("pagelink");
		if (link == null) {
			link = new Link(ContentSearchHandler.class);
		}
		link.setParameter("resultsPerPage", resultsPerPage);
		link.setParameter("keywords", keywords);

		FrontendDoc doc = new FrontendDoc(context, "Search results");
		doc.addBody(new ResultsControl(context, results, link));
		doc.addBody(new Body() {

			@Override
			public String toString() {

				for (ContentSearchResult result : searchResults) {
					sb.append("<div class='ec_csresult_name'>" + new LinkTag(result.getUrl(), result.getName()) + "</div>");
					if (result.hasSummary()) {
						sb.append("<div class='ec_csresult_summary'>" + result.getSummary() + "</div>");
					}
					sb.append("<div class='ec_csresult_url'>" + new LinkTag(result.getUrl(), result.getUrl()) + "</div>");
				}

				return sb.toString();
			}

		});
		return doc;
	}

}
