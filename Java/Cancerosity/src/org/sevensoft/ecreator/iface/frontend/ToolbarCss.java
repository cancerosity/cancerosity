package org.sevensoft.ecreator.iface.frontend;

/**
 * @author sks 3 Apr 2007 22:31:45
 *
 */
public class ToolbarCss {

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("table.cpbar { width: 100%; background: #efefef; margin-bottom: 5px; } \n");
		sb.append("table.cpbar td { padding: 3px 8px; color: #333333; font-family: Tahoma; font-size: 11px; font-weight: normal; } \n");
		sb.append("table.cpbar td a { color: #333333; text-decoration: underline; } \n");
		sb.append("table.cpbar td a:hover { color: white; text-decoration: none; } \n");
		sb.append("table.cpbar td img { margin-left: 3px; } \n");
		sb.append("table.cpbar span { color: #333333; font-family: Trebuchet MS; font-size: 14px; font-weight: bold; letter-spacing: -1px; } \n");

		return sb.toString();
	}

}
