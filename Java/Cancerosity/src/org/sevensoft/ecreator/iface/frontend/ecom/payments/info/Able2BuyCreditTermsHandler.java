package org.sevensoft.ecreator.iface.frontend.ecom.payments.info;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.model.ecom.payments.forms.able2buy.CreditGroup;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.results.docs.SimpleDoc;

/**
 * @author sks 13 Dec 2006 08:42:17
 *
 */
@Path("able2buy-credit-terms.do")
public class Able2BuyCreditTermsHandler extends Handler {

	private Item		item;
	private CreditGroup	creditGroup;

	public Able2BuyCreditTermsHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		if (item == null) {
			return HttpServletResponse.SC_BAD_REQUEST;
		}

		List<CreditGroup> creditGroups = CreditGroup.get(context);
		if (creditGroups.isEmpty()) {
			return HttpServletResponse.SC_BAD_REQUEST;
		}

		if (creditGroup == null) {
			creditGroup = creditGroups.get(0);
		}

		Money sellPrice = item.getSellPrice();

		StringBuilder sb = new StringBuilder();

		sb.append(new TableTag("ec_able2buy_border", "center"));
		sb.append("<tr><td>");

		sb.append(new TableTag("ec_able2buy"));

		sb.append("<tr><td class='logo' colspan='2'><img src='files/graphics/able2buy/terms_logo.gif'/></td></tr>");
		sb.append("<tr><td class='rate' colspan='2'>Available credit rates for " + item.getDisplayName() + ":<br/>");

		if (creditGroups.size() == 1) {

			sb.append(creditGroup.getDescription());

		} else {

			sb.append(new FormTag(Able2BuyCreditTermsHandler.class, null, "get"));
			sb.append(new HiddenTag("item", item));
			sb.append(new SelectTag(context, "creditGroup", creditGroup, creditGroups, null).setAutoSubmit(true));
			sb.append("</form>");
		}

		sb.append("</td></tr>");

		sb.append("<tr><td>Selling price</td><td class='right'>\243" + sellPrice + "</td></tr>");
		sb.append("<tr><td>Deposit</td><td class='right'>\243" + creditGroup.getDeposit(sellPrice) + "</td></tr>");
		sb.append("<tr><td>Amount of credit</td><td class='right'>\243" + creditGroup.getAmountOfCredit(sellPrice) + "</td></tr>");

		sb.append("<tr><td>Number of repayments</td><td class='right'>" + creditGroup.getRepaymentMonths() + " months</td></tr>");
		sb.append("<tr><td>Total amount repayable</td><td class='right'>\243" + creditGroup.getTotalAmountRepayable(sellPrice) + "</td></tr>");

		if (creditGroup.hasDeferredMonths()) {
			sb.append("<tr><td>Deferred period</td><td class='right'>" + creditGroup.getDeferredMonths() + " months</td></tr>");
		}
		//		sb.append("<tr><td>APR in deferred period</td><td class='right'>0%</td></tr>");
		sb.append("<tr><td>Typical APR</td><td class='right'>" + creditGroup.getApr() + "%</td></tr>");

		sb
				.append("<tr><td class='monthlyrep'>Monthly repayment</td><td class='right'><b>\243" + creditGroup.getMonthlyRepayment(sellPrice)
						+ "</b></td></tr>");

		sb.append("</table>");
		sb.append("</td></tr>");
		sb.append("</table>");

		SimpleDoc doc = new SimpleDoc(context);
		doc.addHead("<link href='files/css/ecreator-front.css' rel='stylesheet' type='text/css' />\n");
		doc.addBody(sb);

		return doc;
	}

	@Override
	protected boolean runSecure() {
		return false;
	}

}
