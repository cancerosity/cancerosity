package org.sevensoft.ecreator.iface.frontend.ecom.shopping.panels;

import org.sevensoft.commons.superstrings.RandomHelper;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.BasketHandler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;

/**
 * @author sks 2 Apr 2007 08:10:28
 *
 */
public class OrderingForm {

	private final RequestContext	context;

	public OrderingForm(RequestContext context) {
		this.context = context;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(new FormTag(BasketHandler.class, "add", "multi").setId("OrderingForm"));
		sb.append(new HiddenTag("ident", RandomHelper.getRandomString(6).toLowerCase()));
		sb.append(new HiddenTag("item", "0"));

		return sb.toString();
	}

}
