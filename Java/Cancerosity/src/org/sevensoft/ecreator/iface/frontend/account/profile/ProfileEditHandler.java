package org.sevensoft.ecreator.iface.frontend.account.profile;

import javax.servlet.ServletException;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.net.EmailValidator;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.profile.panels.ProfileEditPanel;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionStatusHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.model.accounts.AccountModule;
import org.sevensoft.ecreator.model.accounts.AccountSettings;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.input.AInputRenderer;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.PasswordTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 19-Aug-2005 07:10:43
 *
 */
@Path("pe.do")
public class ProfileEditHandler extends SubscriptionStatusHandler {

	private MultiValueMap<Attribute, String>	attributeValues;
	private String					email, nickname, postcode, name, mobilePhone;
	private String					password1, password2;
	private final transient AccountSettings	accountSettings;

	public ProfileEditHandler(RequestContext context) {
		super(context);
		this.accountSettings = AccountSettings.getInstance(context);
	}

	@Override
	public Object main() throws ServletException {

		if (account == null) {
			return new LoginHandler(context, new Link(getClass())).main();
		}

		final AccountModule module = account.getItemType().getAccountModule();

		FrontendDoc doc = new FrontendDoc(context, "My profile");
		if (account.getItemType().getAccountModule().isShowAccount()) {
			doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		}
		doc.addTrail(ProfileEditHandler.class, null, "Edit profile");
		doc.addBody(new Body() {

			@Override
            public String toString() {

                sb.append(new MessagesTag(context));

                if (module.hasProfileEditHeader()) {
                    sb.append(module.getProfileEditHeader());
                }

                sb.append(new FormTag(ProfileEditHandler.class, "save", "POST"));
                sb.append(new ProfileEditPanel(context, account, module.isNotChangeEmailAfterRegistered()));

                sb.append("</form>");

                if (module.hasProfileEditFooter()) {
                    sb.append(module.getProfileEditFooter());
                }

                return sb.toString();
            }
        });
        return doc;

	}

	public Object save() throws ServletException {

		if (!Module.Accounts.enabled(context)) {
			return new CategoryHandler(context).main();
		}

		if (account == null) {
			return new LoginHandler(context, new Link(ProfileEditHandler.class)).main();
		}

		/*
		 * If we are allowing members to change their name, then we should make sure they enter one
		 */

		test(new RequiredValidator(), "email");
		test(new EmailValidator(), "email");

		//		test(new RequiredValidator(), "email");
		//		if (!member.getEmail().equals(email))
		//			if (!Member.isEmailValid(email))
		//				setError("email", "That email is already in use.");

		/*
		 * We only set new password if it is present. The member can leave blank.
		 * If the new password param is present  check they are equal
		 */
		if (password1 != null) {

			test(new LengthValidator(6), "password1");
			test(new LengthValidator(6), "password2");
			if (!ObjectUtil.equal(password1, password2)) {
				setError("password1", "Passwords do not match");
				setError("password2", "Passwords do not match");
			}
		}

		for (Attribute attribute : account.getEditableAttributes()) {

			if (attribute.isOptional()) {
				continue;
			}

			if (!attributeValues.containsKey(attribute)) {
				setError(attribute.getParamName(), "Must be completed");
			}

		}

		if (hasErrors()) {
			logger.fine("[ProfileEditHandler] errors detected=" + context.getErrors());
			addError("There was a problem with the data you entered. Please correct the errors shown below and try again.");
			return main();
		}

		account.setEmail(email);

		if (password1 != null) {
			account.setPassword(password1);
		}

        if (account.isMobilePnoneEnabled())
            account.setMobilePhone(mobilePhone);

        account.save();

		// save attributes
		account.setAttributeValues(account.getEditableAttributes(), attributeValues);

		account.log("User updated his/her account through front end. Name=" + name + ", email=" + email);

		clearParameters();

		FrontendDoc doc = new FrontendDoc(context, "My profile");
		if (account.getItemType().getAccountModule().isShowAccount()) {
			doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		}
		doc.addTrail(ProfileEditHandler.class, null, "Edit profile");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				StringBuilder sb = new StringBuilder();

				sb.append("Your profile has been updated.");
				sb.append("<br/><br/>");

				if (account.getItemType().getAccountModule().isShowAccount()) {
					sb.append(new LinkTag(AccountHandler.class, null, "Click here"));
					sb.append(" to return to your account");
				}

				return sb.toString();

			}
		});
		return doc;
	}
}
