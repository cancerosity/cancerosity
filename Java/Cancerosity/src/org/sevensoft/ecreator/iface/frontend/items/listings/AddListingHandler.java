package org.sevensoft.ecreator.iface.frontend.items.listings;

import java.io.IOException;
import java.util.*;

import javax.servlet.ServletException;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.net.EmailValidator;
import org.sevensoft.commons.validators.number.DecimalValidator;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.ecreator.iface.frontend.ErrorDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.FormRow;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.registration.RegistrationHandler;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.frontend.items.listings.panels.ListingAddEditPanel;
import org.sevensoft.ecreator.iface.frontend.items.listings.panels.ListingAttachmentsPanel;
import org.sevensoft.ecreator.iface.frontend.items.listings.panels.ListingImagesPanel;
import org.sevensoft.ecreator.iface.frontend.items.listings.panels.ListingVoucherPanel;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.ecom.delivery.DeliveryOption;
import org.sevensoft.ecreator.model.ecom.vouchers.Voucher;
import org.sevensoft.ecreator.model.ecom.vouchers.VoucherListingPackage;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.listings.Listing;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;
import org.sevensoft.ecreator.model.items.listings.ListingRate;
import org.sevensoft.ecreator.model.items.listings.ListingSettings;
import org.sevensoft.ecreator.model.items.listings.emails.ListingAddedEmail;
import org.sevensoft.ecreator.model.items.listings.sessions.ListingSession;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.misc.agreements.Agreement;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.positionable.ReorderingPositionComparator;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.text.PasswordTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.bool.SimpleRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 30 Nov 2006 10:47:17
 * 
 * This handler is used to edit existing listings or add a new listings
 *
 */
@Path( { "al.do", "listing.do", "listings-add.do" })
public class AddListingHandler extends FrontendHandler {

	private transient ListingSession		listingSession;
	private ItemType					itemType;
	private ListingPackage				listingPackage;
	private Attachment				attachment;
	private boolean					editDetails;
	private Img						image;
	private boolean					confirm;
	private int						page;
	private Upload					attachmentUpload;
	private Upload					imageUpload;
	private String					name;
	private String					content;
	private MultiValueMap<Attribute, String>	attributeValues;
	private List<Category>				categories;
	private String					email;
	private Money					price;
	private boolean					agreed;
	private String					outStockMsg;
	private int						stock;
	private transient int				pages;
	private Map<Integer, Money>			deliveryRates;
	private boolean					editImages;

	private boolean					editAttachments;

	private String					accountName;
	private int						maxGuests;
	private boolean					privateGuests;

    private transient ListingSettings listingSettings;

    private String imageCaption;

    private boolean addVoucher;
    private String voucherCode;

    private Item item;

	public AddListingHandler(RequestContext context) {
		super(context);

		this.listingSession = ListingSession.get(context);
        this.listingSettings = ListingSettings.getInstance(context);

		this.pages = AttributeUtil.getPagesCount(listingSession.getAttributes());

		if (page < 1) {
			page = 1;
		}

		if (page > pages) {
			page = pages;
		}

		logger.fine("[AddListingHandler] pages=" + pages + ", page=" + page);
	}

    private Object askAccount() {

        FrontendDoc doc = new FrontendDoc(context, "Your account");
        doc.addTrail(AddListingHandler.class, null, "Your account");
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new TableTag("form").setCaption("I already have an account"));
                sb.append(new FormTag(LoginHandler.class, "login", "post"));
                sb.append(new HiddenTag("linkback", new Link(AddListingHandler.class)));

                sb.append("<tr><td colspan='2'>If you already have an account then you can checkout more quickly using details from your previous orders. "
                        + "Please enter your email and password to login.</td></tr>");
                sb.append(new FormRow("Email", null, new TextTag(context, "email", 30)));
                sb.append(new FormRow("Password", null, new PasswordTag(context, "password", 16)));
                sb.append(new FormRow(null, null, new SubmitTag("Login")));

                sb.append("</form>");
                sb.append("</table>");

                sb.append(new TableTag("form").setCaption("I want to register for an account"));
                sb.append("<tr><td>If you do not have an account then it only takes a minute to register.<br/>"
                        + "Please click the 'Register now' button to begin.</td></tr>");
                sb.append("<tr><td>" + new ButtonTag(RegistrationHandler.class, null, "Register now", "linkback", new Link(AddListingHandler.class)) +
                        "</td></tr>");
                sb.append("</table>");

                return sb.toString();
            }

        });
        return doc;
    }

    private Object askAgreement() throws ServletException {

		if (!listingSession.hasListingPackage()) {
			return process();
		}

		if (!listingSession.getListingPackage().hasAgreement()) {
			return process();
		}

		final Agreement agreement = listingSession.getListingPackage().getAgreement();
		assert agreement != null;

        String title = "Add " + captions.getListingsCaption().toLowerCase() + ": " + agreement.getTitle();
		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(AddListingHandler.class, "process", title);
		doc.addBody(new Body() {

			@Override
			public String toString() {


				sb.append(agreement.getContent());
				sb.append("<div class='ec_form_commands' align='center'>");
				sb.append(new ButtonTag(MyListingsHandler.class, null, agreement.getRejectButtonText()));
				sb.append(" &nbsp; ");
				sb.append(new ButtonTag(AddListingHandler.class, "setAgreed", agreement.getConfirmButtonText()));
				sb.append("</div>");

				return sb.toString();
			}
		});
		return doc;
	}

    private Object confirmation() throws ServletException {
        if (!listingSession.hasListingPackage()) {
            return process();
        }

        if (!listingSession.getListingPackage().hasAgreement()) {
            return process();
        }

        final Agreement agreement = listingSession.getListingPackage().getAgreement();
        assert agreement != null;

        String title = "Add " + captions.getListingsCaption().toLowerCase() + ": " + agreement.getTitle();
        FrontendDoc doc = new FrontendDoc(context, title);
        doc.addTrail(AddListingHandler.class, "process", title);
        doc.addBody(new Body() {

            @Override
            public String toString() {
                sb.append(new FormTag(AddListingHandler.class, "setAgreed", "post"));

				sb.append(agreement.getContent());
                sb.append("<div class='ec_form_commands'>");
                sb.append(new ButtonTag(AddListingHandler.class, "process", agreement.getRejectButtonText(), "editDetails", true, "page", pages));
                sb.append(" &nbsp; ");
                sb.append(new ButtonTag(AddListingHandler.class, "setAgreed", agreement.getConfirmButtonText()));
                sb.append("</div>");

                return sb.toString();
            }
        });
        return doc;
    }


	private Object askAttachments() {

        final FrontendDoc doc = new FrontendDoc(context, "Add " + captions.getListingsCaption().toLowerCase() + " attachments");
        doc.addTrail(AddListingHandler.class, "process", "Add " + captions.getListingsCaption().toLowerCase() + " attachments", "editAttachments", true);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new MessagesTag(context));

				sb.append(new ListingAttachmentsPanel(context, listingSession));

				sb.append("<div class='ec_form_commands'>");
				sb.append(new ButtonTag(AddListingHandler.class, "process", "Continue"));

				if (listingSession.getListingPackage().isImages()) {
					sb.append(new ButtonTag(AddListingHandler.class, "process", "Go back to images", "editImages", true));
				} else {
					sb.append(new ButtonTag(AddListingHandler.class, "process", "Go back to details", "editDetails", true, "page", pages));
				}

				sb.append("</div>");

				return sb.toString();
			}

		});
		return doc;
	}

	/**
	 * Ask category page for when add categories is on a separate page
	 */
	private Object askCategories() {

		FrontendDoc doc = new FrontendDoc(context, "Select category");
		doc.addTrail(AddListingHandler.class, "completed", "Select category");

        //list <h2> ctegories
        Structure structure = new Structure().invoke();
        final List<Category> parent = structure.getSortedParents();
        final HashMap<Category, List<Category>> parents2 = structure.getParents2();
        final HashMap<Category, List<Category>> map = structure.getMap();

		doc.addBody(new Body() {

			@Override
			public String toString() {
                sb.append(new MessagesTag(context));


                if (listingSession.getListingPackage().getMaxCategories() > 1) {

                    sb.append(new FormTag(AddListingHandler.class, "setCategory", "post"));
                    sb.append(new TableTag("ec_addlisting_categories"));
                    sb.append("<th>Where would you like your " + captions.getListingsCaption().toLowerCase() + " to appear. You can choose a maximum of " + listingSession.getListingPackage().getMaxCategories() + " categories.</th>");
                    for (Category par2 : parent) {
                        sb.append("<tr><td><h2>" + par2.getName() + "</h2></td></tr>");
                        if (map.containsKey(par2)) {
                            List<Category> cats = map.get(par2);
                            for (Category category : cats) {
                                sb.append("<tr><td style='font-weight: bold;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + new CheckTag(context, "categories", category, false) + " " + category.getName() + "</td></tr>");
                            }
                            map.remove(par2);
                        }
                        List<Category> pars1 = parents2.get(par2);
                        for (Category par1 : pars1) {
                            if (map.containsKey(par1)) {
                                if (!par2.equals(par1)) {
                                    sb.append("<tr><td style='font-weight: bold;'>&nbsp;&nbsp;&nbsp;&nbsp;" + par1.getName() + "</td></tr>");
                                }
                                List<Category> cats = map.get(par1);
                                for (Category category : cats) {
                                    sb.append("<tr><td style='font-weight: bold;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + new CheckTag(context, "categories", category, false) + " " + category.getName() + "</td></tr>");
                                }
                                map.remove(par1);
                            }

                        }
                    }

                /*    HashMap parentCategories = new HashMap();
                    for (Category category : listingSession.getListingPackage().getAvailableCategories()) {
                        if (category.isChild()) {
                            if (parentCategories.containsKey(category.getParent().getName()) && !category.getParent().getName().equals("Home Page")) {
                                sb.append("<tr><td style='font-weight: bold;'>&nbsp;&nbsp;&nbsp;&nbsp;" + new CheckTag(context, "categories", category, false) + " " + category.getName() + "</td></tr>");
                            } else if (!parentCategories.containsKey(category.getParent().getName()) && !category.getParent().getName().equals("Home Page")) {
                                if (!category.getParent().getName().equals("Home Page")) {
                                    parentCategories.put(category.getParent().getName(), 1);
                                    sb.append("<tr><td>" + category.getParent().getName() + "</td></tr>");
                                    sb.append("<tr><td style='font-weight: bold;'>&nbsp;&nbsp;&nbsp;&nbsp;" + new CheckTag(context, "categories", category, false) + " " + category.getName() + "</td></tr>");
                                }
                            } else if (category.getParent().getName().equals("Home Page") && !category.hasChildren()) {
                                sb.append("<tr><td style='font-weight: bold;'>&nbsp;&nbsp;&nbsp;&nbsp;" + new CheckTag(context, "categories", category, false) + " " + category.getName() + "</td></tr>");
                            }
                        }
                    }
                    parentCategories.clear();*/
//                    sb.append(new TableTag());
//                    sb.append("<tr><td><div class='ec_form_commands'>").append(new SubmitTag("Continue")).append("</div></td></tr>");
                    sb.append("</table>");

                    sb.append("<div class='ec_form_commands'>").append(new SubmitTag("Continue")).append("</div>");

                    sb.append("</form>");

                } else {
                    sb.append(new TableTag("ec_addlisting_categories"));

                    sb.append("<th>Where would you like your " + captions.getListingsCaption().toLowerCase() + " to appear?</th>");

                    for (Category par2 : parent) {
                        sb.append("<tr><td><h2>" + par2.getName() + "<h2></td></tr>");
                        if (map.containsKey(par2)) {

                            List<Category> cats = map.get(par2);
                            for (Category category : cats) {
                                sb.append("<tr><td style='font-weight: bold;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                                        new LinkTag(AddListingHandler.class, "setCategory", "-" + category.getName(), "categories", category) + "</td></tr>");
                            }
                            map.remove(par2);
                        }
                        List<Category> pars1 = parents2.get(par2);
                        for (Category par1 : pars1) {
                            if (map.containsKey(par1)) {
                                if (!par2.equals(par1)) {
                                    sb.append("<tr><td style='font-weight: bold;'>&nbsp;&nbsp;&nbsp;&nbsp;" + par1.getName() + "</td></tr>");
                                }
                                List<Category> cats = map.get(par1);
                                for (Category category : cats) {
                                    sb.append("<tr><td style='font-weight: bold;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                                            new LinkTag(AddListingHandler.class, "setCategory", "-" + category.getName(), "categories", category) + "</td></tr>");
                                }
                                map.remove(par1);
                            }
                        }
                    }

/*                    HashMap parentCategories = new HashMap();
                    for (Category category : listingSession.getListingPackage().getAvailableCategories()) {
                        if (category.isChild()) {
                            if (parentCategories.containsKey(category.getParent().getName()) && !category.getParent().getName().equals("Home Page")) {
                                sb.append("<tr><td>" + new LinkTag(AddListingHandler.class, "setCategory", "-" + category.getName(), "categories", category) +
                                        "</td></tr>");
                            }
                            else
                            if (!parentCategories.containsKey(category.getParent().getName()) && !category.getParent().getName().equals("Home Page")) {
                                if (!category.getParent().getName().equals("Home Page")) {
                                    parentCategories.put(category.getParent().getName(), 1);
                                    sb.append("<tr><td><b>" + category.getParent().getName() + "</b></td></tr>");
                                    sb.append("<tr><td>" + new LinkTag(AddListingHandler.class, "setCategory", "-" + category.getName(), "categories", category) +
                                            "</td></tr>");
                                }
                            }
                            else if (category.getParent().getName().equals("Home Page") && !category.hasChildren()) {
                                sb.append("<tr><td>" + new LinkTag(AddListingHandler.class, "setCategory", category.getName(), "categories", category) +
                                        "</td></tr>");
                            }
                        }
                    }
                    parentCategories.clear();*/
                    sb.append("</table>");

                }

                return sb.toString();
            }
		});
		return doc;
	}

    /*
        private void getCategoryChildList(StringBuilder sb, List<Category> availableCategory, Category category, int i, List<Category> deleted) {
            ++i;
            List<Category> children = category.getChildren();
            if (children.isEmpty())
                return;
            for (Category child : children) {
                if (availableCategory.contains(child)) {
                    if (i <= 1) {
                        sb.append("<tr><td>");
                    } else
                        sb.append("<tr><td style='font-size: 12px; color: gray;'>");

                    for (int y = 0; y < i; y++) {
                        sb.append("&ensp;&ensp;&ensp;&ensp;");
                    }
                    sb.append(new CheckTag(context, "categories", child, false) + " " + child.getName() + "</td></tr>");
                    deleted.add(child);
                    getCategoryChildList(sb, availableCategory, child, i, deleted);
                }
            }

        }
    */
	private Object askDetails() throws ServletException {

        String title = "Add " + captions.getListingsCaption().toLowerCase() + ": " + captions.getListingsCaption() + " details ";
		if (pages > 1) {
			title = title + page;
		}

		// reset images and attachments
		listingSession.setOfferedAttachments(false);
		listingSession.setOfferedImages(false);
		listingSession.save();

		final ListingPackage listingPackage = listingSession.getListingPackage();

		final FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(AddListingHandler.class, "process", title, "page", page, "editDetails", true);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new MessagesTag(context));

				sb.append("Please enter the details of your " + listingSession.getItemType().getNameLower() +
						" carefully. When you have finished click continue at the bottom of the page. " +
						"If there are any problems with the details you have entered then these will be highlighted " +
						"in red after you click continue.<br/><br/>");

				if (listingPackage.hasDetailsPageHeader()) {
					sb.append(listingPackage.getDetailPagesHeader());
				}

				sb.append(new FormTag(AddListingHandler.class, "setDetails", "multi"));
				sb.append(new HiddenTag("page", page));

				sb.append(new ListingAddEditPanel(context, listingSession, AddListingHandler.class, page, pages));

				sb.append("<div class='ec_form_commands'>");
				sb.append(new SubmitTag("Continue"));

				// If previous pages then show go back button otherwise show cancel button 
				if (page > 1) {

					sb.append(new ButtonTag(AddListingHandler.class, "process", "Go back", "editDetails", true, "page", page - 1));

				} else {

					sb.append(new ButtonTag(MyListingsHandler.class, null, "Cancel"));
				}

				sb.append("</div>");

				sb.append("</form>");

				if (listingPackage.hasDetailsPageFooter()) {
					sb.append(listingPackage.getDetailPagesFooter());
				}

				return sb.toString();
			}

		});
		return doc;
	}

	private Object askImages() {

        final FrontendDoc doc = new FrontendDoc(context, "Add " + captions.getListingsCaption().toLowerCase() + " images");
        doc.addTrail(AddListingHandler.class, "process", "Add " + captions.getListingsCaption().toLowerCase() + " images", "editImages", true);
		doc.addBody(new MessagesTag(context));
		doc.addBody(new ListingImagesPanel(context, listingSession));
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("<div class='ec_form_commands'>");
				sb.append(new ButtonTag(AddListingHandler.class, "process", "Continue"));
				sb.append(new ButtonTag(AddListingHandler.class, "process", "Go back to details", "editDetails", true, "page", pages));
				sb.append("</div>");

				return sb.toString();
			}

		});
		return doc;
	}

	private Object askListingPackage() throws ServletException {

		// get a list of packages applicable for the account / guest for the selected item type
		final Collection<ListingPackage> packages = listingSession.getListingPackages();
        final List<ListingPackage> list = Arrays.asList(packages.toArray(new ListingPackage[0]));

        Collections.sort(list, new ReorderingPositionComparator());
		logger.fine("[AddListingHandler] packages=" + list);

		/* 
		 * if no packages exist then the admin has not yet set up any groups 
		 * to be paid for and as such we cannot continue.
		 */
		if (list.isEmpty()) {

			// if we are not logged in then and there are no guest packages then ask to register.
			if (account == null) {

				return new LoginHandler(context, new Link(AddListingHandler.class, "process")).main();

			} else {

				return new ErrorDoc(context, "No listing packages are setup for account type: " + account.getItemType().getNameLower());
			}
		}

		/*
		 * If only one package then simply select that and return, no point showing choice !
		 */
		if (list.size() == 1) {

			listingPackage = list.iterator().next();

			logger.fine("[AddListingHandler] only one listing package, auto setting=" + listingPackage);
			return setListingPackage();
		}

		/*
		 * Otherwise show selection table
		 */
        String title = "Add " + captions.getListingsCaption().toLowerCase() + ": Choose " + captions.getListingsCaption().toLowerCase() + " package";
		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(AddListingHandler.class, "process", title);
		doc.addBody(new Body() {

			@Override
			public String toString() {

                sb.append("<div class='ec_text'>" + listingSettings.getChoosePackageText() + "</div>");

				for (ListingPackage p : list) {

					sb.append(new TableTag("ec_listingpackage"));
					sb
							.append("<tr><th>" + new LinkTag(AddListingHandler.class, "setListingPackage", p.getName(), "listingPackage", p) +
									"</th></tr>");

					if (p.hasDescription()) {

						sb.append("<tr><td class='description'>" + p.getDescription() + "</td></tr>");
					}

					if (p.hasRates()) {

						List<ListingRate> rates = p.getListingRates();

                        sb.append("<tr><td><strong>");
                        if (rates.size() == 1) {
                            sb.append("Cost:");
                        } else {
                            sb.append("Costs:");
                        }
                        sb.append("</strong> ");
                        sb.append("<ul class='ec_listing_costs'>");
                        for (ListingRate rate : rates) {

                            List<String> list = new ArrayList<String>();
                            list.add(rate.getDescription());
                            sb.append("<li>" + StringHelper.implode(list, ", ") + "</li>");
                        }
                        sb.append("</ul>");

						sb.append("</td></tr>");

					} else {

						sb.append("<tr><td><b>Free</b> lifetime listing</td></tr>");
					}

					if (p.isModeration()) {
						sb.append("<tr><td><b>Important:</b> Your listing will not appear until it is approved by a moderator</td></tr>");
					}

					sb.append("</table>");
				}

				return sb.toString();
			}

		});
		return doc;

	}

    private Object askVoucher() {

        final FrontendDoc doc = new FrontendDoc(context, "Add " + captions.getListingsCaption().toLowerCase() + " voucher");
        doc.addTrail(AddListingHandler.class, "process", "Add " + captions.getListingsCaption().toLowerCase() + " voucher", "addVoucher", true);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new MessagesTag(context));

				sb.append(new ListingVoucherPanel(context, item));

				sb.append("<div class='ec_form_commands'>");
				sb.append(new ButtonTag(AddListingHandler.class, "payment", "Continue", "item", item));

                sb.append("</div>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object completed() {
        if (listingSession.hasListingPackage() && listingSession.getListingPackage().hasForwardUrl()) {
            return new ExternalRedirect(listingSession.getListingPackage().getForwardUrl());
        }
        final Item item = this.item != null ? this.item : listingSession.getItem();
		final Listing listing = item.getListing();

        if (!item.isAwaitingModeration() && !item.isAwaitingValidation()){
            sendCompletedNotification(context, item, listingSession.getAccount());
        }

        addStats(listing);
        String title = "Add " + captions.getListingsCaption().toLowerCase() + ": Completed";
		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(AddListingHandler.class, "completed", title);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(listing.getListingPackage().getCompletedMessage());
				sb.append("<br/><br/>");

				if (item.isAwaitingModeration()) {

					sb.append("Your listing will not be visible until it has been checked by our moderation team. "
							+ "You will be sent an email as soon as it has been approved..<br/><br/>");

				}

				if (item.isAwaitingValidation()) {

					sb.append("You have been sent an email with a url to verifiy your email address.<br/>"
							+ "You must click this url before this listing can appear on the site.<br/><br/>");

				}

                if (listingPackage != null && (listingPackage.isGuest() && !listingSession.hasAccount()) && listingPackage.isAllowGuestToEdit() == true) {
                    sb.append("The url to access your listing is:<br/>" + new LinkTag(item.getUrl(), item.getUrl()));
                } else if (listingPackage == null) {
                    sb.append("The url to access your listing is:<br/>" + new LinkTag(item.getUrl(), item.getUrl()));
                } else {
                    sb.append("You have no rights to view/edit listings");
                }
				sb.append("<br/><br/>");

                LinkTag clickHere = new LinkTag(AddListingHandler.class, null, "click here");
                clickHere.setTitle("Add another listing");

                sb.append("<strong>Please " + clickHere + " if you wish to add another listing.</strong> ");

                sb.append("<br/><br>" + new LinkTag(AccountHandler.class, null, "Go back to  " + captions.getAccountCaption()));

                return sb.toString();
			}
		});
		return doc;
	}

    private void sendCompletedNotification(RequestContext context, Item item, Item account) {
        if (account != null && account.hasEmail()) {
            Thread t = new Thread(new ListingAddedEmail(context, item, account));
            t.setDaemon(true);
            t.start();
        }
    }

    public Object main() throws ServletException {
		return start();
	}

	private Object max(final int max) {

		FrontendDoc doc = new FrontendDoc(context, "Max listings reached");
		doc.addTrail(AddListingHandler.class, "process", "Max listings reached");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("I'm sorry but you have reached the maximum number of listings allowed.<br/>");
				sb.append("You can only post " + max + " listings at a time. Please delete existing listings before adding another.");

				return sb.toString();
			}

		});
		return doc;

	}

	public Object process() throws ServletException {

		if (page < 0) {
			page = 1;
		}

		if (page > pages) {
			page = pages;
		}

		context.setAttribute("pagelink", new Link(AddListingHandler.class, "process"));

		/*
		 * If listings module is disabled then exit
		 */
		if (!Module.Listings.enabled(context)) {

			logger.fine("[AddListingHandler] listings module disabled, exiting to front page");
			return new CategoryHandler(context).main();
		}

		/*
		 * If this session has already resulted in a completed item then forward to the completed page
		 */
		if (listingSession.hasItem()) {

			logger.fine("[AddListingHandler] session already has item, showing completed page.");
			return completed();
		}

		/*
		 * Update the listing session with our account status
		 */
		logger.fine("[AddListingHandler] updating session to use account=" + account);
		listingSession.setAccount(account);

		/*
		 * If we are logged in, then check we have not exhausted our limit of listings
		 */
		if (account != null) {

			int max = account.getItemType().getListingModule().getMaxListings();
			int current = account.getListings().size();

			logger.fine("[AddListingHandler] max listings=" + max + ", current=" + current);
			if (max > 0) {
				if (current >= max) {
					return max(max);
				}
			}
		}

		/*
		 * If we do not yet have a listing package then we must choose. 
		 * The listing package will determine which categories we can list things in, 
		 * coming on the next screen
		 */
		if (!listingSession.hasListingPackage()) {

			logger.fine("[AddListingHandler] no listing package set, asking user");
			return askListingPackage();
		}

        /*
		 * Check for category and show category screen if categories are first
		 */
		if (listingSession.getListingPackage().isCategories()) {
			if (listingSession.getListingPackage().isCategoriesFirst()) {
				if (!listingSession.hasCategory()) {
					return askCategories();
				}
			}
		}

		/*
		 * If we are missing details or have asked to edit them again then show details pages again
		 */
		if (!listingSession.hasRequiredDetails()) {

			logger.fine("[AddListingHandler] missing required details, returning to ask details");

			page = 1;
			return askDetails();
		}

		if (editDetails) {

			logger.fine("[AddListingHandler] user has requested to change listing details, returning to ask details");
			return askDetails();
		}

		if (listingSession.getListingPackage().isImages() && !listingSession.isOfferedImages()) {

			logger.fine("[AddListingHandler] not shown images screen yet");

			listingSession.setOfferedImages(true);
			return askImages();
		}

		if (editImages) {
			logger.fine("[AddListingHandler] user has requested to edit images");
			return askImages();
		}

        if (listingSession.getListingPackage().isAttachments() && !listingSession.isOfferedAttachments()) {

			logger.fine("[AddListingHandler] not shown attachments screen yet");

			listingSession.setOfferedAttachments(true);
			return askAttachments();
		}

//        Item item = listingSession.toItem(); //get double item moderation queu because of agreement

		/*
		 * attachments next
		 */
		if (editAttachments) {

			logger.fine("[AddListingHandler] user has requested to edit attachments");
			return askAttachments();
		}

		/*
		 * If listing package has agreement and not yet agreed then show agreement page
		 */
		if (listingSession.getListingPackage().hasAgreement()) {

			if (!listingSession.isAgreed()) {

                if (listingSession.getListingPackage().getAgreement().isPage()) {

                    logger.fine("[AddListingHandler] we have an agreement that has not yet been agreed - showing agreement screen");
                    return askAgreement();
                } else {
                    logger.fine("[AddListingHandler] we have inline agreement that was not set to agreed.");
                    addError("You must agree to our terms and conditions");
                    return confirmation();
                }
			}
		}

		//			// check for line agreement
//					if (listingSession.getListingPackage().hasAgreement()) {
//
//						if (!agreed) {
//
//							if (listingSession.getListingPackage().getAgreement().isInline()) {
//
//								logger.fine("[AddListingHandler] we have inline agreement that was not set to agreed.");
//								addError("You must agree to our terms and conditions");
//								return confirmation();
//							}
//						}
//					}

		Item item = listingSession.toItem();
		logger.fine("[ListingHandler] item created=" + item);

        return new ExternalRedirect(new Link(AddListingHandler.class, "preview", "item", item));


    }

    public Object payment() throws ServletException {

        if (Module.ListingVouchers.enabled(context) && item.getListing().getListingPackage().hasRates()) {
            if (!item.getListing().hasVoucher() && addVoucher) {                                     //todo one more check
                logger.fine("[AddListingHandler] not shown voucher screen yet");

                return askVoucher();
            }
            if (addVoucher) {
                logger.fine("[AddListingHandler] user has requested to edit voucher");
                return askVoucher();
            }
        }

//        addStats(item.getListing());  // TODO only when process is completed
        // if we have payment rates on this then forward to payment handler.
        if (item.getListing().getListingPackage().hasRates()) {

            logger.fine("[ListingHandler] listing package has rates, forwarding to payment handler");
        //set item Disabled till payment
            item.setStatus("Disabled");
            return new ListingPaymentHandler(context, item).main();

        } else {

            // if no rates then do a free lifetime listing

            logger.fine("[ListingHandler] no rates on package, listing immediately");
            item.getListing().list(null, null, context.getRemoteIp());

            return completed();
        }
    }


    public Object preview() {
        final FrontendDoc doc = new FrontendDoc(context, item.getName());
        doc.addTrail(AddListingHandler.class, "process", "Preview " + captions.getListingsCaption().toLowerCase(), "_a", "preview", "item", item);
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new MessagesTag(context));

                MarkupRenderer r = new MarkupRenderer(context, item.getItemType().getViewMarkup());
                r.setBody(item);
                sb.append(r);

                commands();
                return sb.toString();
            }

            private void commands() {
                sb.append("<div class='ec_form_commands'>");
                sb.append(new ButtonTag(EditListingHandler.class, null, "Go back to details", "item", item, "editPreview", true));
                sb.append(new ButtonTag(AddListingHandler.class, "payment", "Continue", "item", item, "addVoucher", true));
                sb.append("</div>");
            }

        });

        return doc;
    }

    public Object removeAttachment() throws ServletException {

		if (attachment != null) {
			listingSession.removeAttachment(attachment);
		}

		// return to final page which is where attachments are shown
		page = pages;
		return askAttachments();
	}

	public Object removeImage() throws ServletException {

		if (image != null) {
			listingSession.removeImage(image);
		}

		// return to final page which is where images are shown
		page = pages;
		return askImages();
	}

	public Object setAgreed() throws ServletException {

		listingSession.setAgreed(true);
		listingSession.save();

		return process();
	}

	public Object setCategory() throws ServletException {

		logger.fine("[AddListingHandler] setting categories=" + categories);

        if (categories.size() > listingSession.getListingPackage().getMaxCategories()) {
            context.addError("You can choose a maximum of " + listingSession.getListingPackage().getMaxCategories() + " categories");
            return process();
        }
//            categories = categories.subList(0, listingSession.getListingPackage().getMaxCategories());

		listingSession.addCategories(categories);
		listingSession.save();

		return process();
	}

	public Object setDetails() throws ServletException {

		logger.fine("[AddListingHandler] setDetails, page=" + page + ", pages=" + pages);

		// name is entered on first page so if page==1 then test for name var
		if (page == 1) {

			test(new RequiredValidator(), "name");

			/*
			 * Ensure we have picked a category if the listing package has categories available
			 */
			if (listingSession.getItemType().isCategories()) {

				logger.fine("[AddListingHandler] item type uses categories");

				if (listingSession.getListingPackage().isCategories()) {

					logger.fine("[AddListingHandler] listing package uses categories");

					if (!listingSession.getListingPackage().isCategoriesFirst()) {

						List<Category> availableCategories = listingSession.getListingPackage().getCategories();

						// if just a single category to choose from then simply set it
						if (availableCategories.size() == 1) {

							categories = availableCategories;
							logger.fine("[AddListingHandler] a single category, using that=" + categories);

						} else {

							logger.fine("[AddListingHandler] selected categories=" + categories);

							if (categories.isEmpty()) {
								setError("category", "You must select a category");
							}
						}
					}
				}
			}

			/*
			 * If we are not logged in then ensure we have entered an email
			 */
			if (account == null) {

				test(new RequiredValidator(), "accountName");
				test(new RequiredValidator(), "email");
				test(new EmailValidator(), "email");
			}

			// prices
			if (listingSession.getListingPackage().isPrices()) {
				test(new DecimalValidator(), "price");
			}

			// check for optional delivery rates
			if (listingSession.getListingPackage().isDeliveryRates()) {
				if (!listingSession.getListingPackage().isDeliveryRatesOptional()) {
					for (DeliveryOption option : DeliveryOption.get(context)) {
						if (!deliveryRates.containsKey(option.getId())) {
							context.setError("deliveryRates_" + option.getId(), "You must enter a delivery rate");
						}
					}
				}
			}
		}

		// content is shown on last page so if we are on final page then test for content
        if (listingSession.getListingPackage().isAllowDescription()) {
            if (page == pages) {

                if (listingSession.getItemType().isContent()) {

                    if (content == null) {
                        setError("content", "You must enter a description");
                    }

                    if (listingSession.getListingPackage().hasMaxCharacters()) {
                        test(new LengthValidator(0, listingSession.getListingPackage().getMaxCharacters()), "content");
                    }

                }
            }
        }

        // test we have valid input for all the attributes on this page
		List<Attribute> attributes = listingSession.getAttributes(page);
		AttributeUtil.setErrors(context, attributes, attributeValues);

		if (hasErrors()) {
			addError("There were errors with your information - please check and try again.");
			logger.fine("[AddListingHandler] errors with input errors=" + context.getErrors());
			return askDetails();
		}

		/*
		 * Only name and category if this is page 1
		 */
		if (page == 1) {

			listingSession.setAccountName(accountName);
			listingSession.setName(name);
			listingSession.setEmail(email);

			if (!listingSession.getListingPackage().isCategoriesFirst()) {
				listingSession.setCategories(categories);
			}

			// stock if applicable
			if (listingSession.getListingPackage().isStock()) {

				listingSession.setOutStockMsg(outStockMsg);
				listingSession.setStock(stock);

			}

			// prices
			if (listingSession.getListingPackage().isPrices()) {
				listingSession.setPrice(price);
			}

			// deliveyr rates
			if (listingSession.getListingPackage().isDeliveryRates()) {
				listingSession.setDeliveryRates(deliveryRates);
			}

			// meetup details
			if (ItemModule.Meetups.enabled(context, listingSession.getItemType())) {
				listingSession.setPrivateGuests(privateGuests);
				listingSession.setMaxGuests(maxGuests);
			}
		}

		/*
		 * Only set content if this is the last page
		 */
        if (listingSession.getListingPackage().isAllowDescription()) {
            if (page == pages) {
                content = content.replaceAll("\r\n", "<br />");
                listingSession.setContent(content);
            }
        }

		listingSession.save();

		listingSession.setAttributeValues(attributes, attributeValues);

		// in
		page++;

		// return details again if there are more pages to come
		if (page <= pages) {

			logger.fine("[AddListingHandler] setting next page to " + page);
			return askDetails();

		} else {

			return process();
		}
	}

	public Object setListingPackage() throws ServletException {

        if (!listingPackage.isGuest() && !listingSession.hasAccount()) {
            logger.fine("[AddListingHandler] user not logged in, asking to log in");
            return askAccount();
        }

        logger.fine("[AddListingHandler] setting listingPackage=" + listingPackage);

		listingSession.setListingPackage(listingPackage);
		listingSession.save();

		return process();
	}

	/**
	 * The main method resets to an add listing process
	 */
	public Object start() throws ServletException {
        if (account == null) {
            return new LoginHandler(context).main();
        }
		// create a new listing session to refresh
		SimpleQuery.delete(context, ListingSession.class, "sessionId", context.getSessionId());

		this.listingSession = ListingSession.get(context);

		assert listingSession != null;

        listingSession.removeCategories();

        logger.fine("Created new listing session=" + listingSession);
		logger.fine("Listing package?=" + listingPackage);

		// if we have an initial listing package, set now
		if (listingPackage != null) {
			return setListingPackage();
		}

		return process();
	}

	public Object uploadAttachment() {

		/* 
		 * check for an uploaded attachment, if the user has included one then add it and return to the ask details page
		 */
		if (Module.ListingAttachments.enabled(context)) {

			if (listingSession.getListingPackage().isAttachments()) {

				if (attachmentUpload != null) {

					try {

						listingSession.addAttachment(attachmentUpload, false);

					} catch (IOException e) {
						e.printStackTrace();
						addError(e);
						logger.warning("[AddListingHandler] ioexception: " + e);

					} catch (AttachmentLimitException e) {
						addError("You cannot upload anymore attachments");

					} catch (AttachmentExistsException e) {
						addError("A file with this name already exists");

					} catch (AttachmentTypeException e) {
						addError("You are not allowed to upload files of this type");
					}

				}
			}
		}

		return askAttachments();
	}

	public Object uploadImage() {

		ListingPackage lp = listingSession.getListingPackage();

		/* 
		 * check for an uploaded image, if the user has included one then add it and return to the ask details page
		 */
		if (lp.isImages()) {

			// ensure we have not uploaded too many images
			if (lp.getMaxImages() > 0) {
				if (listingSession.getAllImagesCount() >= lp.getMaxImages()) {
					addError("You cannot upload anymore images");
					return askImages();
				}
			}

			if (imageUpload != null) {

				try {

					listingSession.addImage(imageUpload, true, true, lp.isModerateImages(), imageCaption);

				} catch (IOException e) {
					e.printStackTrace();
					addError(e);
					logger.warning("[AddListingHandler] ioexception: " + e);

				} catch (ImageLimitException e) {
					e.printStackTrace();
				}

			}
		}

		return askImages();
	}

    public Object applyVoucher() throws ServletException {

        ListingPackage lp = item.getListing().getListingPackage();
        if (lp == null) {
            return start();
        }

        if ((voucherCode == null || voucherCode.trim().length() == 0) && item.getListing().hasVoucher()) {
            removeVoucher();
            addMessage("Voucher removed");
            return askVoucher();
        }
        if (voucherCode == null || !Voucher.isExistForPackage(context, lp)) {
            addError("Wrong voucher code");
            return askVoucher();
        }
        Voucher voucher = Voucher.getByCode(context, voucherCode);
        if (voucher == null) {
            addError("Voucher code not recognised");
            return askVoucher();
        }
        /*
           * check for an uploaded image, if the user has included one then add it and return to the ask details page
           */
        if (Voucher.isExistForPackage(context, lp)) {
            String error = voucher.isValidForListings(item.getListing());
            if (error != null) {
                addError(error);
            } else {
                setVoucher(lp, voucher);
            }


        }

        return askVoucher();
    }

    private void setVoucher(ListingPackage lp, Voucher voucher) {
        List<VoucherListingPackage> vouchers = Voucher.getVouchers(context, lp);
        boolean applied = false;
        for (VoucherListingPackage v : vouchers) {
            if (v.getVoucher().equals(voucher)) {

                item.getListing().setVoucher(voucher);
                applied = true;
                addMessage("Voucher applied");

            }
        }


        if (!applied) {
            addError("There is no voucher to apply");
        }
    }

    private void removeVoucher() {
        item.getListing().setVoucher(null);
    }

    private void addStats(Listing listing) {
        if (listing.hasVoucher()){
             listing.getVoucher().incUsage(context, account, listing.getListingPackage());
        }
    }

    private class Structure {
        private HashMap<Category, List<Category>> parents2;
        private HashMap<Category, List<Category>> map;

        public HashMap<Category, List<Category>> getParents2() {
            return parents2;
        }

        public HashMap<Category, List<Category>> getMap() {
            return map;
        }

        public Structure invoke() {
            parents2 = new HashMap<Category, List<Category>>();
            map = new HashMap<Category, List<Category>>();
            for (Category category : listingSession.getListingPackage().getAvailableCategories()) {
                Category parent1 = category.getParent();
                //category is not Home page
                if (parent1 != null) {
                    Category parent2 = parent1.getParent();
            // parent1 is not Home pahe && parent2 is not Home Page
                    if (parent2 != null && parent2.getParent() != null) {
                        if (!parents2.containsKey(parent2)) {
                            parents2.put(parent2, new ArrayList(Arrays.asList(parent1)));
                        } else {
                            List<Category> list = parents2.get(parent2);
                            if (!list.contains(parent1)) {
                                list.add(parent1);
                                parents2.put(parent2, list);
                            }
                        }

                    } else {
                        if (!parents2.containsKey(parent1)) {
                            parents2.put(parent1, new ArrayList(Arrays.asList(parent1)));
                        } else {
                            List<Category> list = parents2.get(parent1);
                            if (!list.contains(parent1)) {
                                list.add(parent1);
                                parents2.put(parent1, list);
                            }
                        }
                    }
                    List<Category> child;
                    if (map.containsKey(parent1)) {
                        child = (ArrayList<Category>) map.get(parent1);
                        child.add(category);
                    } else {
                        child = new ArrayList(Arrays.asList(category));
                    }
                    map.put(parent1, child);

                }
            }
            return this;
        }

        public List<Category> getSortedParents() {
            List<Category> keyParents = Arrays.asList(getParents2().keySet().toArray(new Category[]{}));
            Collections.sort(keyParents, comparator);

            return keyParents;
        }


        Comparator<Category> comparator = new Comparator<Category>() {

            public int compare(Category o1, Category o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        };

    }
}

//	private Object category() throws ServletException {
//
//		SortedSet<Category> listingCategories = listingSession.getListingCategories(member);
//		if (listingCategories.isEmpty())
//			return "No categories defined for this item type!";
//
//		/*
//		 * If only one category available might as well just select it !
//		 */
//		if (listingCategories.size() == 1) {
//			category = listingCategories.iterator().next();
//			return setCategory();
//		}
//
//		switch (listingSettings.getCategorySelectionStyle()) {
//
//		default:
//		case Flat:
//			return categoryFlat(listingCategories);
//
//		case Navigation:
//
//			return categoryNavigation(listingCategories);
//		}
//	}

//	/**
//	 * @param listingCategories
//	 * @return
//	 */
//	private Object categoryFlat(final SortedSet<Category> listingCategories) throws ServletException {
//
//		String title = captions.getAddListingCaption() + " - Choose category";
//		FrontendDoc p = new FrontendDoc(context, title);
//		p.addTrail(MemberAccountHandler.class, captions.getAccountCaption());
//		p.addTrail(ListingsHandler.class, null, captions.getMyListingsCaption());
//		p.addTrail(AddListingHandler.class, "process", title);
//		p.addBody(new Render() {
//
//			@Override
//			public String toString() {
//
//				sb.append("<div>Where do you want this listing to appear?</div>");
//
//				SelectTag categoryTag = new SelectTag(context, "nothing");
//				categoryTag.setSize(12);
//				categoryTag.setOnChange("window.location='" + new Link(AddListingHandler.class, "setCategory") + "&category=' + this.value");
//
//				for (Category c : listingCategories)
//					categoryTag.addOption(c, c.getName());
//
//				sb.append(categoryTag);
//
//				return sb.toString();
//
//			}
//		});
//		return p;
//	}

//	/**
//	 * Select categories each in a separate select menu
//	 * 
//	 * @param listingCategories 
//	 * @return 
//	 */
//	private FrontendDoc categoryNavigation(final SortedSet<Category> listingCategories) throws ServletException {
//
//		/*
//		 * Always start with primary categories, so create a set containing all the primaries
//		 * for our listing categories.
//		 */
//		final SortedSet<Category> primaries = new TreeSet<Category>();
//		for (Category c : listingCategories)
//			primaries.add(c.getPrimaryParent());
//
//		String title = captions.getAddListingCaption() + " - Choose category";
//		FrontendDoc page = new FrontendDoc(context, title);
//		page.addTrail(MemberAccountHandler.class, captions.getAccountCaption());
//		page.addTrail(ListingsHandler.class, null, captions.getMyListingsCaption());
//		page.addTrail(AddListingHandler.class, "process", title);
//		page.addBody(new Render() {
//
//			@Override
//			public String toString() {
//
//				sb.append("<div>Where do you want this listing to appear?</div>");
//
//				SelectTag categoryTag = new SelectTag(context, "nothing");
//				categoryTag.setSize(12);
//				categoryTag.setOnChange("window.location='" + new Link(AddListingHandler.class, "setCategory") + "&navigation=' + this.value");
//
//				for (Category c : primaries) {
//
//					/*
//					 * Primary categories are either directly listable (so contained in listingCategories) or 
//					 * have children for further selection.
//					 */
//					if (listingCategories.contains(c))
//						categoryTag.addOption(c, c.getName());
//
//					else if (c.hasChildren())
//						categoryTag.addOption(c, c.getName() + " -->");
//				}
//
//				sb.append(categoryTag);
//
//				/*
//				 * If we have a currently selected category then show a menu for each category on its trail
//				 */
//				if (navigation != null) {
//
//					for (Category c : navigation.getTrail()) {
//
//						//						if (c.hasChildren()) {
//
//						// reset options on select tag
//						categoryTag.removeOptions();
//
//						/*
//						 * For each child of the trail category, we should be checking if the category is listable 
//						 * or if it has children which are listable
//						 */
//						for (Category child : c.getChildren()) {
//
//							// check if it is directly listable
//							if (listingCategories.contains(child)) {
//
//								categoryTag.addOption(child, child.getName());
//
//							} else {
//
//								// check that any of our listable categories appear as a child of this category
//								for (Category listable : listingCategories) {
//
//									if (listingSession.getTrail().contains(child)) {
//										categoryTag.addOption(child, child.getName() + " -->");
//										break;
//									}
//								}
//							}
//						}
//
//						sb.append(categoryTag);
//
//						//						}
//					}
//				}
//
//				return sb.toString();
//
//			}
//		});
//		return page;
//	}