package org.sevensoft.ecreator.iface.frontend.items.listings.panels;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.items.listings.ListingValidationHandler;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 7 Mar 2007 14:02:17
 * 
 * Shows that a listing is waiting to be approved by a moderator
 *
 */
public class ListingVerificationPanel extends Panel {

	private final Item	item;

	public ListingVerificationPanel(RequestContext context, Item item) {
		super(context);
		this.item = item;
	}

	@Override
	public void makeString() {

		sb.append(new TableTag("ec_listing_validationpanel"));
		sb.append("<tr><td>This " + item.getItemType().getNameLower()
				+ " is awaiting validation of the email used when adding the listing.<br/>If the validation email was not received, "
				+ new LinkTag(ListingValidationHandler.class, "resend", "click here", "item", item) + " for it to be resent.<td></tr>");
		sb.append("</table>");
	}
}
