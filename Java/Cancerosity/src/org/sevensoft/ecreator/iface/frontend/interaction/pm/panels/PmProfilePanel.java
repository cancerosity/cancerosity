package org.sevensoft.ecreator.iface.frontend.interaction.pm.panels;

import java.util.List;
import java.util.Map;

import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.values.AValueRenderer;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 18 Dec 2006 16:38:07
 * 
 * Renders the profile for display on the messages screen
 *
 */
public class PmProfilePanel {

	private Item		account;
	private RequestContext	context;

	public PmProfilePanel(RequestContext context, Item acc) {
		this.context = context;
		this.account = acc;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(new TableTag("member_form", "center").setId("message_profile"));
		sb.append("<tr>");
		sb.append("<td class='image' valign='top' align='center'>");

		ImageTag imageTag = null;

		if (account.hasApprovedImages()) {

			Img image = account.getApprovedImage();
			if (image != null) {
				imageTag = new ImageTag(account.getApprovedImage().getThumbnailPath());
			}
		}

		if (imageTag == null && account.hasImagePlaceholder()) {
			imageTag = new ImageTag(account.getItemType().getImageHolderPath(), 0, 60, 0);
		}

		if (imageTag != null) {
			sb.append(new LinkTag(account.getUrl(), imageTag));
		}

		sb.append("</td>");

		sb.append("<td class='details' valign='top'>");
		sb.append("<b>Name:</b> " + new LinkTag(account.getUrl(), account.getDisplayName()) + "<br/>");

		for (Map.Entry<Attribute, List<String>> entry : account.getAttributeValues().entryListSet()) {

			Attribute attribute = entry.getKey();
			List<String> values = entry.getValue();

			if (attribute.isSummary()) {
				sb.append("<b>" + attribute.getName() + ":</b> ");
				sb.append(new AValueRenderer(context, account, attribute, values));
				sb.append("<br/>");
			}
		}

		sb.append("<b>Last active:</b> " + account.getLastActive());
		sb.append("</td>");

		sb.append("</table>");

		return sb.toString();
	}
}
