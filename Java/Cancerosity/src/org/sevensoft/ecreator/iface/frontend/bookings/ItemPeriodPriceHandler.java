package org.sevensoft.ecreator.iface.frontend.bookings;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.items.listings.MyListingsHandler;
import org.sevensoft.ecreator.model.bookings.BookingSlot;
import org.sevensoft.ecreator.model.bookings.ItemPeriodPrice;
import org.sevensoft.ecreator.model.bookings.Period;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;

/**
 * User: MeleshkoDN
 * Date: 11.10.2007
 * Time: 13:40:41
 */
@Path("items-period-price.do")
public class ItemPeriodPriceHandler extends FrontendHandler {

    private ItemPeriodPrice itemPeriodPrice;
    private Period period;
    private Item item;
    private Money dailyPrice;
    private Money weeklyPrice;

    public ItemPeriodPriceHandler(RequestContext context) {
        super(context);
    }

    public Object main() throws ServletException {
        if (item == null) {
            return index();
        }
        FrontendDoc doc = new FrontendDoc(context, "Period prices");
        if (account.getItemType().getAccountModule().isShowAccount()) {
            doc.addTrail(AccountHandler.class, captions.getAccountCaption());
        }
        doc.addTrail(MyListingsHandler.class, captions.getListingsCaption());

        doc.addBody(new Body() {

            @Override
            public String toString() {
                sb.append(new FormTag(ItemPeriodPriceHandler.class, "save", "POST"));
                sb.append(new HiddenTag("itemPeriodPrice", itemPeriodPrice));
                sb.append(new HiddenTag("item", item));
                sb.append(new HiddenTag("period", period));

                sb.append(new AdminTable("Period prices"));

                sb.append(new AdminRow("Period", null, period.getStartDate().toString("dd.MM") + "-" + period.getEndDate().toString("dd.MM")));
                sb.append(new AdminRow("Daily price", null, new TextTag(context, "dailyPrice", itemPeriodPrice != null ? itemPeriodPrice.getDailyPrice().toEditString() : new Money(0), 12)));
                if (period.isWeekly()) {
                    sb.append(new AdminRow("Weekly price", null, new TextTag(context, "weeklyPrice", itemPeriodPrice != null ? itemPeriodPrice.getWeeklyPrice().toEditString() : new Money(0), 12)));
                }
                sb.append("</table>");
                sb.append("<div align='center' class='actions'>");
                sb.append(new ButtonTag(BookingPricesHandler.class, null, "Return to calendar", "item", item));
                sb.append(new SubmitTag("Apply prices"));
                sb.append("</div>");
                sb.append("</form>");
                return sb.toString();
            }
        });
        return doc;
    }

    public Object save() {
        if (itemPeriodPrice == null) {
            itemPeriodPrice = new ItemPeriodPrice(context);
            itemPeriodPrice.setPeriod(period);
            itemPeriodPrice.setItem(item);
        }
        itemPeriodPrice.setDailyPrice(dailyPrice);
        if (period.isWeekly()) {
            if (weeklyPrice == null || weeklyPrice.isZero()) {
                weeklyPrice = dailyPrice.multiply(7d);
            }
            itemPeriodPrice.setWeeklyPrice(weeklyPrice);
        }
        itemPeriodPrice.save();
        updateBookingSlots(itemPeriodPrice);
        return new ActionDoc(context, "Prices were successfully applied", new Link(BookingPricesHandler.class, null, "item", item));
    }


    private void updateBookingSlots(ItemPeriodPrice itemPeriodPrice) {
        Date date = period.getStartDate();
        Date endPeriod = period.getEndDate().nextDay();
        while (!date.equals(endPeriod)) {
            BookingSlot slot = SimpleQuery.get(context, BookingSlot.class, "date", date, "item", item);
            if (slot == null) {
                slot = new BookingSlot(context, item, date);
            }
            if (slot.getStatus() == BookingSlot.Status.Open) {
                slot.setRate(itemPeriodPrice.getDailyPrice());
            }
            slot.save();
            date = date.nextDay();
        }
    }
}
