package org.sevensoft.ecreator.iface.frontend.interaction.userplane.recorder;

import org.sevensoft.ecreator.model.interaction.userplane.recorder.Recording;
import org.sevensoft.ecreator.model.interaction.userplane.recorder.UserplaneRecorderSettings;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 14 Dec 2006 15:50:55
 *
 */
public class UserplaneRecorderRenderer {

	private final transient UserplaneRecorderSettings	settings;
	private final RequestContext					context;
	private final String						type;
	private int								targetId;
	private final boolean						admin;
	private int								width, height;

	public UserplaneRecorderRenderer(RequestContext context, int targetId, boolean admin) {

		this.context = context;
		this.admin = admin;
		this.targetId = targetId;
		this.settings = UserplaneRecorderSettings.getInstance(context);

		if (targetId == 0) {
			this.type = "vrRecorder.swf";
		} else {
			this.type = "vrPlayer.swf";
		}

		this.width = 320;
		this.height = 381;
	}

	public UserplaneRecorderRenderer(RequestContext context, Recording recording) {
		this(context, 0, false);
		this.targetId = recording.getAccount().getId();
	}

	public final void setHeight(int height) {
		this.height = height;
	}

	public final void setWidth(int width) {
		this.width = width;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("<script type='text/javascript' src='files/userplane-recorder/flashobject.js'></script>\n");
		sb.append("<div id='flashcontent'>\n");
		sb.append("<strong>You need to upgrade your Flash Player by clicking "
				+ "<a href='http://www.macromedia.com/go/getflash/' target='_blank'>this link</a>.</strong>"
				+ "<br><br><strong>If you see this and have already upgraded we suggest you follow "
				+ "<a href='http://www.adobe.com/cfusion/knowledgebase/index.cfm?id=tn_14157' target='_blank'>this link</a> "
				+ "to uninstall Flash and reinstall again.</strong>\n");
		sb.append("</div>\n");

		sb.append("<script type='text/javascript'>\n");
		sb.append("	// <![CDATA[\n");

		sb.append("	var fo = new FlashObject('http://swf.userplane.com/Webrecorder/" + type + "', 'wr', '" + width + "', '" + height
				+ "', '6', '#ffffff', false, 'best');\n");
		sb.append("	fo.addParam('scale', 'noscale');\n");
		sb.append("	fo.addParam('menu', 'false');\n");
		sb.append("	fo.addParam('salign', 'LT');\n");
		sb.append("	fo.addVariable('server', '" + settings.getFlashcomServer() + "');\n");
		sb.append("	fo.addVariable('swfServer', 'swf.userplane.com');\n");
		sb.append("	fo.addVariable('applicationName', 'Webrecorder');\n");
		sb.append("	fo.addVariable('domainID', '" + settings.getDomainId() + "');\n");
		sb.append("	fo.addVariable('sessionGUID', '" + context.getSessionId() + "');\n");
		sb.append("	fo.addVariable('key', '');\n");
		sb.append("	fo.addVariable('locale', 'english');\n");
		sb.append("	fo.addVariable('admin', '" + admin + "');\n");

		// if we are viewing, then pass in the member id of who we want to view
		if (targetId > 0) {
			sb.append("fo.addVariable('viewingMemberID', '" + targetId + "');\n");
		}

		// actual name of the recording, we can ignore as users only have one recording
		//sb.append("	fo.addVariable('recordingName', '<?php echo( $strRecordingName ); ?>');\n");
		sb.append("	fo.addVariable('enableJavascript', 'false');\n");
		sb.append("	fo.write('flashcontent');\n");

		sb.append("	// ]]>\n");
		sb.append("</script>\n");

		return sb.toString();
	}

}
