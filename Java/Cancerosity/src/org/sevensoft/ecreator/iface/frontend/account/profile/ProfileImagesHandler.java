package org.sevensoft.ecreator.iface.frontend.account.profile;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.model.accounts.AccountModule;
import org.sevensoft.ecreator.model.accounts.AccountSettings;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.ImageSettings;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 29 Dec 2006 14:25:51
 *
 */
@Path("pi.do")
public class ProfileImagesHandler extends FrontendHandler {

	private Img						image;
	private List<Upload>				uploads;
	private transient final ImageSettings	imageSettings;
	private final transient AccountSettings	accountSettings;
    private String imageCaption;

	public ProfileImagesHandler(RequestContext context) {
		super(context);
		this.accountSettings = AccountSettings.getInstance(context);
		this.imageSettings = ImageSettings.getInstance(context);
	}

	@Override
	public Object main() throws ServletException {

		if (account == null) {
			logger.fine("[ProfileImagesHandler] we are not logged in");
			return new LoginHandler(context, new Link(ProfileImagesHandler.class)).main();
		}

		final AccountModule module = account.getItemType().getAccountModule();

		FrontendDoc doc = new FrontendDoc(context, captions.getImagesCaptionPlural());
		if (account.getItemType().getAccountModule().isShowAccount()) {
			doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		}
		doc.addTrail(ProfileImagesHandler.class, captions.getImagesCaptionPlural());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new MessagesTag(context));

				if (module.hasProfileImagesHeader()) {
					sb.append(module.getProfileImagesHeader());
				}

				sb.append(new FormTag(ProfileImagesHandler.class, "upload", "multi"));
				sb.append(new TableTag("form").setCaption(captions.getImagesCaption()));

				sb.append("<tr>");
				sb.append("<th width='140'>Position</th>");
				sb.append("<th>Preview</th>");
				sb.append("<th width='120'>Remove</th>");
				sb.append("</tr>");

				int n = 1;
				for (Img image : account.getAllImages()) {

					sb.append("<tr>");
					sb.append("<td width='140'>" + n + " " + new ButtonTag(ProfileImagesHandler.class, "moveImageUp", "Up", "image", image) + " "
							+ new ButtonTag(ProfileImagesHandler.class, "moveImageDown", "Down", "image", image) + "</td>");
					sb.append("<td>" + new ImageTag(image.getThumbnailPath()) + "<div class='img_caption'>"+image.getCaption()+"</div></td>");
					sb.append("<td width='120'>" + new ButtonTag(ProfileImagesHandler.class, "removeImage", "Remove", "image", image) + "</td>");
					sb.append("</tr>");

					n++;
				}

				sb.append("<tr>");
				sb.append("<th>Upload new image</th> ");
				sb.append("<th>"+new FileTag("uploads")+"</th><th></th>");
				sb.append("</tr>");
                sb.append("<tr><th>Image caption</th><th> " + new TextTag(context, "imageCaption", null) + "</th><th></th><td></tr>");
                sb.append("<tr><th></th><th colspan='2'>" + new SubmitTag("Upload") + "</th></tr>");

				if (account.getItemType().getAccountModule().isShowAccount()) {
					sb.append("<tr><th colspan='3'>" + "To return to your account " + new LinkTag(AccountHandler.class, null, "click here")
							+ "</th></tr>");
				}

				sb.append("</table>");
				sb.append("</form>");

				if (module.hasProfileImagesFooter()) {
					sb.append(module.getProfileImagesFooter());
				}

				return sb.toString();
			}

		});
		return doc;
	}

	public Object moveImageDown() throws ServletException {

		if (account == null || image == null) {
			return main();
		}

		EntityUtil.moveDown(image, account.getAllImages());
		return main();
	}

	public Object moveImageUp() throws ServletException {

		if (account == null || image == null) {
			return main();
		}

		EntityUtil.moveUp(image, account.getAllImages());
		return main();
	}

	public Object removeImage() throws ServletException {

		if (account == null) {
			return main();
		}

		if (image == null) {
			return main();
		}

		account.removeImage(image, false, false);
		account.log("User removed an image from their profile");

		return main();
	}

	public Object upload() throws ServletException {

		if (account == null) {
			return main();
		}

		boolean pending = false;
		for (Upload upload : uploads) {

			try {

				Img image = account.addImage(upload, true, true, false, imageCaption);
				account.log("User uploaded an image to their profile");
                clearParameters();

				if (!image.isApproved()) {
					pending = true;
				}

			} catch (IOException e) {
                logger.warning(upload.getPath() + "/" + upload.getFilename());
                logger.warning(e.toString());

			} catch (ImageLimitException e) {
				e.printStackTrace();
				addMessage("You have reached your image limit");
			}
		}

		if (pending) {

			if (imageSettings.hasPendingContent()) {
				return pending();
			}

		}

		return main();
	}

	private Object pending() {

		FrontendDoc doc = new FrontendDoc(context, captions.getImagesCaptionPlural());
		if (account.getItemType().getAccountModule().isShowAccount()) {
			doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		}
		doc.addTrail(ProfileImagesHandler.class, captions.getImagesCaptionPlural());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(imageSettings.getPendingContent());
				sb.append("<div>" + new LinkTag(ProfileImagesHandler.class, null, "Click here") + " to add or remove another image.</div>");

				return sb.toString();
			}

		});
		return doc;
	}

}
