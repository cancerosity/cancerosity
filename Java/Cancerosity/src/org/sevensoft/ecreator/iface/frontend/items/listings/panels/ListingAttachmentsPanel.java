package org.sevensoft.ecreator.iface.frontend.items.listings.panels;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.items.listings.AddListingHandler;
import org.sevensoft.ecreator.iface.frontend.items.listings.EditListingHandler;
import org.sevensoft.ecreator.iface.frontend.misc.FieldInputCell;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.AttachmentOwner;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.listings.sessions.ListingSession;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 8 Mar 2007 12:42:47
 *
 */
public class ListingAttachmentsPanel extends Panel {

	private final AttachmentOwner			owner;
	private final Class<? extends Handler>	handler;
	private String					p;
	private Item					item;
    private boolean editPreview;

	public ListingAttachmentsPanel(RequestContext context, Item item, String p) {
		super(context);
		this.item = item;
		this.owner = item;
		this.p = p;
		this.handler = EditListingHandler.class;
	}

	public ListingAttachmentsPanel(RequestContext context, ListingSession session) {
		super(context);
		this.owner = session;
		this.handler = AddListingHandler.class;
	}

    public Panel setEditPreview(boolean editPreview) {
        this.editPreview = editPreview;
        return this;
    }

	@Override
	public void makeString() {

		sb.append("If you want to attach a file to your listing you can upload them on this screen.");

		if (owner.hasAttachments()) {

			sb.append(new FormFieldSet("Files uploaded"));

			for (Attachment attachment : owner.getAttachments()) {

				Link link = new Link(handler, "removeAttachment", "attachment", attachment, "item", item, "p", p);
				sb.append(attachment.getFiletype().getCategory() + "<br/>" + attachment.getName() + "<br/>" + attachment.getSizeKb() + "<br/>"
						+ new LinkTag(link, "Remove"));

			}

			sb.append("<br/>&nbsp;");
			sb.append("</fieldset>");

		}

		if (!owner.isAtAttachmentLimit()) {

			sb.append(new FormTag(handler, "uploadAttachment", "multi"));
			sb.append(new HiddenTag("item", item));
			sb.append(new HiddenTag("p", p));
            sb.append(new HiddenTag("editPreview", editPreview));

			sb.append(new FormFieldSet("Upload a file"));

			sb.append("<div class='text'>You can upload files in any common format such as office, images, movies, images and audio</div>");

			sb.append(new FieldInputCell("Browse for file", "This site has a maximum upload size of 10mb", new FileTag("attachmentUpload"),
					new SubmitTag("Upload file")));

			sb.append("</fieldset>");

			sb.append("</form>");
		}

	}
}
