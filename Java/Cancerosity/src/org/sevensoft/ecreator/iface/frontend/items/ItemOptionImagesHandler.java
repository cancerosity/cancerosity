package org.sevensoft.ecreator.iface.frontend.items;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.docs.SimpleDoc;

/**
 * @author sks 21 Aug 2006 18:39:00
 *
 */
@Path("items-options-images.do")
public class ItemOptionImagesHandler extends Handler {

	private ItemOption	option;

	public ItemOptionImagesHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		if (option == null)
			return HttpServletResponse.SC_BAD_REQUEST;

		StringBuilder sb = new StringBuilder();
		sb.append("<style> * { font-family: Verdana; font-size: 12px; </style>");

		sb.append("<table>");

		for (ItemOptionSelection selection : option.getSelections()) {

			Img image = selection.getApprovedImage();
			if (image != null) {
				sb.append("<tr><td align='center'>");
				sb.append(image.getThumbnailTag());
				sb.append("<br/>");
				sb.append(selection.getText());
				sb.append("</td></tr>");
			}
		}

		sb.append("</table>");

		return new SimpleDoc(context, sb);
	}

	@Override
	protected boolean runSecure() {
		return false;
	}

}
