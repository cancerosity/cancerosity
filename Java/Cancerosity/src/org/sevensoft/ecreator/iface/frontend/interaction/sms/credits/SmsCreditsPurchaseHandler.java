package org.sevensoft.ecreator.iface.frontend.interaction.sms.credits;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.frontend.ErrorDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.bookings.BookingHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.payments.panels.PaymentTable;
import org.sevensoft.ecreator.model.ecom.payments.PaymentRedirect;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.marketing.sms.credits.SmsCreditPackage;
import org.sevensoft.ecreator.model.marketing.sms.credits.SmsCreditsSession;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 25 Apr 2007 11:08:28
 *
 */
@Path("sms-credits-purchase.do")
public class SmsCreditsPurchaseHandler extends FrontendHandler {

	private boolean				editPaymentType;
	private SmsCreditPackage		smsCreditPackage;
	private PaymentType			paymentType;
	private transient SmsCreditsSession	session;

	public SmsCreditsPurchaseHandler(RequestContext context) {
		super(context);
		this.session = SmsCreditsSession.get(context);
	}

	private Object askPaymentType() throws ServletException {

		logger.fine("[SmsCreditsPurchaseHandler=" + context.getSessionId() + "] showing payment screen");

		/*
		 * Get all payment types applicable for all groups this member is in
		 */
		final Set<PaymentType> paymentTypes = PaymentSettings.getInstance(context).getAvailablePaymentTypes(new Money(0));
		if (paymentTypes.size() == 0) {
			return new ErrorDoc(context, "There are no payment methods configured");
		}

		/*
		 * if we only have one payment method then check if we can forward on automatically or if it is a payment form that requires a post we
		 * must still show the payment option.
		 */
		if (paymentTypes.size() == 1) {
			Iterator<PaymentType> iter = paymentTypes.iterator();
			paymentType = iter.next();
			return setPaymentType();
		}

		final String title = "Sms credits - payment details";
		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(BookingHandler.class, null, title, "editPaymentType", true);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new PaymentTable(context, paymentTypes, BookingHandler.class, "setPaymentType"));
				return sb.toString();
			}

		});
		return doc;
	}

	/**
	 * @throws ServletException 
	 * 
	 */
	private Object askSmsCreditPackage() throws ServletException {

		/* 
		 * get list of rates applicable for this item
		 */
		final List<SmsCreditPackage> packages = SmsCreditPackage.get(context);
		logger.fine("[SmsCreditsPurchaseHandler] available packages: " + packages);

		/*
		 * If no rates are defined for the package on this item then we should exit, as we should
		 * not even be in the listing payment handler
		 */
		if (packages.isEmpty()) {
			return new ErrorDoc(context, "No packages defined");
		}

		/*
		 * If only one rate is available then we should use that!
		 */
		if (packages.size() == 1) {

			smsCreditPackage = packages.iterator().next();

			logger.fine("[SmsCreditsPurchaseHandler] single package detected, defaulting to that=" + smsCreditPackage);
			return setSmsCreditPackage();
		}

		/*
		 * Otherwise show selection table
		 */
		FrontendDoc doc = new FrontendDoc(context, "Sms credits - choose package");
		doc.addTrail(SmsCreditsPurchaseHandler.class, null, "Sms credits - choose package");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(SmsCreditsPurchaseHandler.class, "setSmsCreditPackage", "GET"));

				sb.append("Choose a package from the list below.<br/>");

				SelectTag tag = new SelectTag(context, "smsCreditPackage");
				tag.setAny("-Please choose-");
				tag.addOptions(packages);

				sb.append(tag + " " + new SubmitTag("Continue"));
				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object cancelled() throws ServletException {

		final String title = "Sms credits - cancelled";
		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(SmsCreditsPurchaseHandler.class, null, title);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("You have cancelled this purchase");
				return sb.toString();
			}

		});
		return doc;
	}

	public Object completed() throws ServletException {

		if (session.hasPaymentType()) {
			logger.fine("[SmsCreditsPurchaseHandler=" + context.getSessionId() + "] payment callback on payment type=" + paymentType);
			session.getPaymentType().inlineCallback(context, getParameters(), getRemoteIp());
		}

		final String title = "Sms credits - completed";
		FrontendDoc doc = new FrontendDoc(context, title);
		doc.addTrail(SmsCreditsPurchaseHandler.class, null, title);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(session.getSmsCreditPackage().getCredits() + " sms credits have been added to your account, thank you.");
				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		// we must be logged in
		session.setAccount(account);

		if (!session.hasAccount()) {
			return new LoginHandler(context, new Link(SmsCreditsPurchaseHandler.class)).main();
		}

		// check for sms package
		if (!session.hasSmsCreditsPackage()) {
			return askSmsCreditPackage();
		}

		// check for payment details
		if (editPaymentType) {

			logger.fine("[SmsCreditsPurchaseHandler] asking to edit payment type");
			return askPaymentType();
		}

		if (!session.hasPaymentType()) {

			logger.fine("[SmsCreditsPurchaseHandler] no payment type set");
			return askPaymentType();
		}

		logger.fine("[SmsCreditsPurchaseHandler] making payment, forwarding to form");

		/*
		 * Forward us to payment provider
		 */
		return new PaymentRedirect(context, session);
	}

	public Object setPaymentType() throws ServletException {

		logger.fine("[SmsCreditsPurchaseHandler] setting payment type=" + paymentType);
		session.setPaymentType(paymentType);

		return main();
	}

	public Object setSmsCreditPackage() throws ServletException {

		logger.fine("[SmsCreditsPurchaseHandler] setting sms credit package=" + smsCreditPackage);

		session.setSmsCreditPackage(smsCreditPackage);
		session.save();

		return main();
	}
}
