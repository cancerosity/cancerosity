package org.sevensoft.ecreator.iface.frontend.interaction.userplane.messenger;

import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.model.accounts.sessions.AccountSession;
import org.sevensoft.ecreator.model.interaction.userplane.messenger.UserplaneMessenger;
import org.sevensoft.ecreator.model.interaction.userplane.messenger.UserplaneMessengerSettings;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 26 Nov 2006 18:46:48
 *
 */
@Path("userplane-msgr-cmd.do")
public class UserplaneMessengerCmdHandler extends Handler {

	private static final Logger				logger	= Logger.getLogger("ecreator");

	private transient UserplaneMessengerSettings	userplaneSettings;
	private Item						originator;

	public UserplaneMessengerCmdHandler(RequestContext context) {
		super(context);
		this.userplaneSettings = UserplaneMessengerSettings.getInstance(context);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	/**
	 *	Cancels chat requests
	 */
	@Override
	public Object main() throws ServletException {

		logger.fine("[UserplaneMessengerCmdHandler] conversation refused, sessionId=" + context.getSessionId() + ", originator=" + originator);

		Item target = AccountSession.getItem(context, context.getSessionId());
		if (target == null) {
			return HttpServletResponse.SC_EXPECTATION_FAILED;
		}

		// cancel off requests
		new UserplaneMessenger(context).declineChatRequest(target, originator);

		String flashcomServer = userplaneSettings.getFlashcomServer();
		String domainID = userplaneSettings.getDomainID();
		String sessionGUID = context.getSessionId();
		String parameter = "";
		String event = "User.ConversationRefused";

		StringBuilder sb = new StringBuilder();

		sb.append("<html>\n");
		sb.append("<head>\n");
		sb.append("	<title>CMD Frame</title>\n");
		sb.append("</head>\n");

		sb.append("<body bottommargin='0' leftmargin='0' marginheight='0' marginwidth='0' rightmargin='0' topmargin='0'>\n");

		sb.append("<script type='text/javascript' src='files/js/userplane-webmessenger/flashobject.js'></script>\n");

		sb.append("<!--- \n");
		sb.append("	The content of this div should hold whatever HTML you would like to show in the case that the \n");
		sb.append("	user does not have Flash installed.  Its contents get replaced with the Flash movie for everyone\n");
		sb.append("	else.\n");
		sb.append("--->\n");
		sb.append("<div id='flashcontent'>\n");
		sb.append("	<strong><a href='http://www.macromedia.com/go/getflash/' target='_blank'>"
				+ "You need to upgrade your Flash Player by clicking this link</a></strong>\n");
		sb.append("</div>\n");

		sb.append("<script type='text/javascript'>\n");
		sb.append("	// <![CDATA[\n");

		sb.append("	var fo = new FlashObject('http://swf.userplane.com/CommunicationSuite/cmd.swf', 'cmd', '1', '1', '6', '#ffffff', false, 'best');\n");
		sb.append("	fo.addParam('scale', 'noscale');\n");
		sb.append("	fo.addParam('menu', 'false');\n");
		sb.append("	fo.addParam('salign', 'LT');\n");
		sb.append("	fo.addVariable('strServer', '" + flashcomServer + "');\n");
		sb.append("	fo.addVariable('strSwfServer', 'swf.userplane.com');\n");
		sb.append("	fo.addVariable('strApplicationName', 'Webmessenger');\n");
		sb.append("	fo.addVariable('strDomainID', '" + domainID + "');\n");
		sb.append("	fo.addVariable('strSessionGUID', '" + sessionGUID + "'); ?>');\n");
		sb.append("	fo.addVariable('strKey', '');\n");
		sb.append("	fo.addVariable('strLocale', 'english');\n");
		sb.append("	fo.addVariable('strEvent', '" + event + "');\n");
		sb.append("	fo.addVariable(strParameter', '" + parameter + "');\n");
		sb.append("	fo.addVariable('strMemberID', '" + target.getId() + "');\n");
		sb.append("	fo.addVariable('strDestinationMemberID', '" + originator.getId() + "');\n");
		sb.append("	fo.write('flashcontent');\n\n");
		sb.append("	// ]]>\n");
		sb.append("</script>\n\n");

		sb.append("</body>");
		sb.append("</html>\n");

		return sb.toString();
	}

	@Override
	protected boolean runSecure() {
		return false;
	}
}
