package org.sevensoft.ecreator.iface.frontend.interaction.userplane.chat;

import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.jdom.Document;
import org.sevensoft.ecreator.model.interaction.userplane.chat.UserplaneChat;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.XmlResult;

/**
 * @author sks 26 Nov 2006 13:42:42
 *
 */
@Path("userplane-chat-action.do")
public class UserplaneChatActionHandler extends Handler {

	private static final Logger	logger	= Logger.getLogger("ecreator");

	private String			function;
	private String			sessionGUID;

	public UserplaneChatActionHandler(RequestContext context) {
		super(context);
	}

	private Object getAnnouncements() {

		UserplaneChat userplane = new UserplaneChat(context);
		return new XmlResult(userplane.getEmptyDoc());
	}

	private Object getDomainPreferences() {

		UserplaneChat userplane = new UserplaneChat(context);
		Document doc = userplane.getDomainPreferencesDoc();

		return new XmlResult(doc);
	}

	private Object getUser() {

		UserplaneChat userplane = new UserplaneChat(context);
		Document doc = userplane.getUser(sessionGUID);

		return new XmlResult(doc);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		logger.fine("[UserplaneChatActionHandler] function=" + function + ", sessionGUID=" + sessionGUID);

		if ("getDomainPreferences".equalsIgnoreCase(function)) {
			return getDomainPreferences();
		}

		if ("getUser".equalsIgnoreCase(function)) {
			return getUser();
		}

		if ("onUserRoomChange".equalsIgnoreCase(function)) {
			return onUserRoomChange();
		}

		if ("onUserConnectionChange".equalsIgnoreCase(function)) {
			return onUserConnectionChange();
		}

		if ("getAnnouncements".equalsIgnoreCase(function)) {
			return getAnnouncements();
		}

		return HttpServletResponse.SC_BAD_REQUEST;
	}

	private Object onUserConnectionChange() {

		UserplaneChat userplane = new UserplaneChat(context);
		return new XmlResult(userplane.getEmptyDoc());
	}

	private Object onUserRoomChange() {

		UserplaneChat userplane = new UserplaneChat(context);
		return new XmlResult(userplane.getEmptyDoc());
	}

	@Override
	protected boolean runSecure() {
		return false;
	}

}
