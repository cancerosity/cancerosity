package org.sevensoft.ecreator.iface.frontend.items.listings.panels;

import org.sevensoft.ecreator.iface.frontend.items.listings.EditListingHandler;
import org.sevensoft.ecreator.iface.frontend.items.listings.ListingDeleteHandler;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.listings.Listing;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 27 Dec 2006 17:46:48
 *
 */
public class ListingControlPanel {

	private final Item		item;
	private final RequestContext	context;
	private final Listing		listing;
	private final String		p;

	public ListingControlPanel(RequestContext context, Item item, String p) {

		this.context = context;
		this.item = item;
		this.p = p;
		this.listing = item.getListing();
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(new TableTag("ec_listing_controlpanel", "center"));

		sb.append("<tr><td class='status'>" + listing.getStatusLong() + "</td></tr>");

		sb.append("<tr><td class='link'>Use this url to link to your " + item.getItemTypeNameLower() + ":<br/>" + new LinkTag(item.getUrl(), item.getUrl())
				+ "</td></tr>");

		sb.append("<tr><td class='commands'>");

		sb.append(new ButtonTag(EditListingHandler.class, null, "Edit details", "item", item, "p", p));

		if (listing.getListingPackage().isImages()) {
			sb.append(new ButtonTag(EditListingHandler.class, "editImages", "Edit images", "item", item, "p", p));
		}

		if (listing.getListingPackage().isAttachments()) {
			sb.append(new ButtonTag(EditListingHandler.class, "editAttachments", "Edit attachments", "item", item, "p", p));
		}

		sb.append(new ButtonTag(ListingDeleteHandler.class, null, "Delete " + item.getItemTypeNameLower(), "item", item, "p", p)
				.setConfirmation("Are you sure you want to delete this " + item.getItemTypeNameLower() + "?"));

		sb.append("</td></tr>");
		sb.append("</table>");

		return sb.toString();
	}
}
