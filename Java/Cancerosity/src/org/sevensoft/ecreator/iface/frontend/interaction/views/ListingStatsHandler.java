package org.sevensoft.ecreator.iface.frontend.interaction.views;

import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.items.listings.MyListingsHandler;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.stats.listings.ListingHitCounterDaily;
import org.sevensoft.ecreator.model.stats.listings.ListingHitCounter;
import org.sevensoft.ecreator.model.stats.listings.ListingHit;
import org.sevensoft.ecreator.model.stats.hits.pages.PageHitCounterMonthly;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.commons.samdate.Date;

import javax.servlet.ServletException;
import java.util.List;

/**
 * @author Dmitry Lebedev
 *         Date: 05.11.2009
 */
@Path({"listing-stats.do"})
public class ListingStatsHandler extends FrontendHandler {

    private Date start;
    private Date end;
    private Item item;
    private String mode;

    public ListingStatsHandler(RequestContext context) {
        super(context);
    }

    @Deprecated
    public Object daily() throws ServletException {
        if (!Module.ListingFrontendStats.enabled(context)) {
            return new MyListingsHandler(context).main();
        }

        if (start == null)
            start = new Date().removeDays(30);

        if (end == null)
            end = new Date();

        final List<ListingHitCounterDaily> hits = ListingHitCounterDaily.get(context, start, end, item);
        FrontendDoc doc = new FrontendDoc(context, "Daily listing hits");

        doc.addBody(new Body() {

            @Override
            public String toString() {

                if (!context.getParamNames().contains("printer")) {
                    sb.append(changeViewLinks());
                }

                sb.append(new TableTag().setCaption("Daily " + item.getName() + " hits"));

                sb.append("<tr>");
                sb.append("<th>Date</th>");
                sb.append("<th>Website Clickthroughs</th>");
                sb.append("<th>Unique Visits</th>");
                sb.append("</tr>");

                for (ListingHitCounter hit : hits) {
                    LinkTag linkDetails = new LinkTag(ListingStatsHandler.class, "details", hit.getTotal());
                    linkDetails.addParameter("item", item);
                    linkDetails.addParameter("start", hit.getDate());
                    linkDetails.addParameter("mode", "daily");
                    sb.append("<tr bordercolor='#F8F8FF'>");
                    sb.append("<td align='center'>" + hit.getDate().toString("dd-MMM-yyyy") + "</td>");
                    sb.append("<td align='center'>" + linkDetails + "</td>");
                    sb.append("<td align='center'>" + hit.getUnique() + "</td>");
                    sb.append("</tr>");
                }

                sb.append("</table>");
                if (!context.getParamNames().contains("printer")) {
                    sb.append(generalLinks());
                }
                return sb.toString();
            }
        });

        return doc;
    }

    @Deprecated
    public Object details() {

        if (mode.equals("daily")) {
            end = start.addDays(1);
        }
        else if (mode.equals("monthly")) {
            end = start.addMonths(1);
        }

        final List<ListingHit> hits = ListingHit.getRange(context, start, end, item);

        FrontendDoc doc;
        if (mode.equals("daily")) {
            doc = new FrontendDoc(context, "Daily listing hits details");
        }
        else {
            doc = new FrontendDoc(context, "Monthly listing hits details");
        }

        doc.addBody(new Body() {

            @Override
            public String toString() {

                if (mode.equals("daily")) {
                    sb.append(new TableTag("form").setCaption("Daily " + item.getName() + " hits details"));
                }
                else {
                    sb.append(new TableTag("form").setCaption("Monthly " + item.getName() + " hits details"));
                }

                sb.append("<tr>");
                sb.append("<th>Date</th>");
                sb.append("<th>User</th>");
                sb.append("<th>IP Address</th>");
                sb.append("<th>Referrer</th>");
                sb.append("<th>Page</th>");
                sb.append("</tr>");

                for (ListingHit hit : hits) {
                    sb.append("<tr>");
                    sb.append("<td align='center'>" + hit.getDate().toString("dd-MMM-yyyy") + "</td>");
                    sb.append("<td align='center'>" + hit.getAccount().getName() + "</td>");
                    sb.append("<td align='center'>" + hit.getIpAddress() + "</td>");
                    sb.append("<td align='center'>" + hit.getReferrer() + "</td>");
                    sb.append("<td align='center'>" + hit.getPage() + "</td>");
                    sb.append("</tr>");
                }

                sb.append("</table>");

                return sb.toString();
            }
        });

        return doc;

    }

    @Override
    public Object main() throws ServletException {
        if (!Module.ListingFrontendStats.enabled(context)){
            return new MyListingsHandler(context).main();
        }

        return monthly();
    }

    public Object monthly() throws ServletException {
        if (!Module.ListingFrontendStats.enabled(context)) {
            return new MyListingsHandler(context).main();
        }
        
        if (start == null)
            start = new Date().removeYear(1).beginMonth();

        if (end == null)
            end = new Date().beginMonth();

//        final List<ListingHitCounterMonthly> hits = ListingHitCounterMonthly.get(context, start, end, item);
        final List<PageHitCounterMonthly> hits = PageHitCounterMonthly.get(context, start, end, item);

        FrontendDoc doc = new FrontendDoc(context, "Monthly listing hits");

        doc.addBody(new Body() {

            @Override
            public String toString() {

                if (!context.getParamNames().contains("printer")) {
                    sb.append(changeViewLinks());
                }

                sb.append(new TableTag("form").setCaption("Monthly " + item.getName() + " hits"));

                sb.append("<tr>");
                sb.append("<th>Date</th>");
                sb.append("<th>Website Clickthroughs</th>");
                sb.append("<th>Unique Visits</th>");
                sb.append("</tr>");

                for (PageHitCounterMonthly hit : hits) {

//                    LinkTag linkDetails = new LinkTag(ListingStatsHandler.class, "details", hit.getTotal());
//                    linkDetails.addParameter("item", item);
//                    linkDetails.addParameter("start", hit.getDate());
//                    linkDetails.addParameter("mode", "monthly");
                    sb.append("<tr>");
                    sb.append("<td align='center'>" + hit.getDate().toString("MMM-yyyy") + "</td>");
                    sb.append("<td align='center'>" + hit.getTotal() + "</td>");
                    sb.append("<td align='center'>" + hit.getUniques() + "</td>");
                    sb.append("</tr>");

                }

                sb.append("</table>");
                if (!context.getParamNames().contains("printer")) {
                    sb.append(generalLinks());
                }
                return sb.toString();
            }
        });

        return doc;
    }

    private String generalLinks() {
        return new Body() {
            @Override
            public String toString() {
                LinkTag linkClose = new LinkTag("#", new ImageTag("files/graphics/pics/close.png"));
                linkClose.setOnClick("window.close()");
                sb.append("<table>");
                sb.append("<tr><td align='left'>");
                sb.append(linkClose);
                sb.append("Close this window");
                LinkTag linkPrint = new LinkTag("", new ImageTag("files/graphics/pics/print.png"));
                linkPrint.addParameter("item", item);
                linkPrint.addParameter("printer", "1");
                sb.append("</td><td align='right'>");
                sb.append(linkPrint);
                sb.append("Print these statistics");
                sb.append("</td></tr>");
                sb.append("</table>");
                return sb.toString();
            }
        }.toString();
    }

    private String changeViewLinks() {
        return new Body() {
            @Override
            public String toString() {
//                LinkTag linkDaily = new LinkTag(ListingStatsHandler.class, "daily", "Listing daily stats");
//                linkDaily.addParameter("item", item);
                LinkTag linkMonthly = new LinkTag(ListingStatsHandler.class, "monthly", "Listing monthly stats");
                linkMonthly.addParameter("item", item);
                sb.append(new TableTag("statsperiod", 0, 0));
                sb.append("<tr>");
//                sb.append("<td align='left'>");
//                sb.append(linkDaily);
//                sb.append("</td>");
                sb.append("<td align='right'>");
                sb.append(linkMonthly);
                sb.append("</td></tr>");
                sb.append("</table>");
                return sb.toString();
            }
        }.toString();
    }
}
