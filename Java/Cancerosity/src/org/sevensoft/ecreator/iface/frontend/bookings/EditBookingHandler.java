package org.sevensoft.ecreator.iface.frontend.bookings;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.iface.admin.ecom.bookings.BookingNotificationEmails;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.items.listings.MyListingsHandler;
import org.sevensoft.ecreator.model.bookings.Booking;
import org.sevensoft.ecreator.model.bookings.BookingSlot;
import org.sevensoft.ecreator.model.bookings.BookingStatus;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.ButtonOpenerTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;

/**
 * User: MeleshkoDN
 * Date: 10.10.2007
 * Time: 18:20:52
 */
@Path("bookings-edit.do")
public class EditBookingHandler extends FrontendHandler {

    private Booking booking;
    private String action;
    private int adults, children;

    public EditBookingHandler(RequestContext context) {
        super(context);
    }

    public Object main() throws ServletException {
        if (booking == null || account == null) {
            return index();
        }

        FrontendDoc doc = new FrontendDoc(context, "Booking #" + booking.getIdString());
        if (account.getItemType().getAccountModule().isShowAccount()) {
            doc.addTrail(AccountHandler.class, captions.getAccountCaption());
        }
        doc.addTrail(MyListingsHandler.class, captions.getListingsCaption());
        doc.addTrail(ViewBookingsHandler.class, null, "View bookings", "item", booking.getItem());

        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                if (booking.getStatus() == BookingStatus.UNCONFIRMED) {
                    sb.append(new SubmitTag("action", "Confirm booking"));
                }

                sb.append(new ButtonTag(EditBookingHandler.class, "cancel", "Cancel booking", "booking", booking));

                sb.append(new SubmitTag("action", "Update booking"));
                sb.append("</div>");
            }

            private void customer() {

                Address address = booking.getAddress();
                Item account = booking.getAccount();

                sb.append(new AdminTable("Customer"));

                sb.append(new AdminRow("Name", "This is the name of the person who made the booking.", account.getName()));

                ButtonOpenerTag clientDetails = new ButtonOpenerTag(context, new Link(ClientDetailsHandler.class, null, "booking", booking), "Client Details", 650, 500);
                clientDetails.setScrollbars(true);
                clientDetails.setTarget("_blank");

                sb.append(new AdminRow("Email", "This is the email of the person who made the booking.", account.getEmail() + "   " + clientDetails));

                if (address != null) {

                    sb.append(new AdminRow("Address", address.getLabel("<br/>", true)));

                }

                sb.append("</table>");
            }


            private void general() {

                sb.append(new AdminTable("General"));
                sb.append(new AdminRow("Booking status", null, booking.getStatus()));
                sb.append(new AdminRow(booking.getItem().getItemTypeName(), null, booking.getItem().getName()));

                sb.append(new AdminRow("Start date", null, booking.getStart().toString("dd-MMM-yyyy")));
                sb.append(new AdminRow("End date", null, booking.getEnd().toString("dd-MMM-yyyy")));

                sb.append(new AdminRow(booking.getItem().getItemTypeName() + " charge", null, booking.getItemChargeEx()));

                if (booking.hasOptions()) {
                    sb.append(new AdminRow("Extra options", null, booking.getOptionsString()));
                    sb.append(new AdminRow("Options charges", null, booking.getOptionsChargesEx()));
                }

                sb.append(new AdminRow("Commission", null, booking.getCommissionValue()));
                sb.append(new AdminRow("Subtotal", null, booking.getTotalEx()));
                sb.append(new AdminRow("Vat", null, booking.getTotalVat()));
                sb.append(new AdminRow("Total", null, booking.getTotalInc()));

                sb.append(new AdminRow("Occupancy", "Adults " + new TextTag(context, "adults", booking.getAdults(), 4) +
                        " Children " + new TextTag(context, "children", booking.getChildren(), 4)));

                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(EditBookingHandler.class, "doAction", "POST"));
                sb.append(new HiddenTag("booking", booking));

                general();
                customer();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object doAction() throws ServletException {
        if (action == null) {
            return index();
        }
        if (action.equals("Update booking")) {
            return save();
        } else if (action.equals("Confirm booking")) {
            return confirm();
        } else {
            return index();
        }
    }

    public Object cancel() {

        booking.clearBookingSlots();

        booking.setStatus(BookingStatus.CANCELLED);
        booking.save();
        return new ActionDoc(context, "Booking was cancelled", new Link(EditBookingHandler.class, null, "booking", booking.getId()));
    }


    public Object confirm() {
        booking.setStatus(BookingStatus.CONFIRMED);
        booking.setConfirmationPerson(user);
        booking.setConfirmationTime(new DateTime());
        booking.save();
        BookingNotificationEmails.informOwnerOfConfirmation(context, booking);
        BookingNotificationEmails.informClientOfConfirmation(context, booking);
        BookingSlot.setStatus(BookingSlot.Status.Booked, booking.getItem(), booking.getStart(), booking.getEnd());
        return new ActionDoc(context, "Booking was confirmed", new Link(EditBookingHandler.class, null, "booking", booking.getId()));
    }

    public Object save() {
        booking.setAdults(adults);
        booking.setChildren(children);
        booking.save();
        return new ActionDoc(context, "Booking was updated", new Link(EditBookingHandler.class, null, "booking", booking.getId()));
    }
}       
