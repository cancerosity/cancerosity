package org.sevensoft.ecreator.iface.frontend.interaction.userplane.messenger;

import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.jdom.Document;
import org.sevensoft.ecreator.model.interaction.userplane.messenger.UserplaneMessenger;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.XmlResult;

/**
 * @author sks 26 Nov 2006 13:42:42
 *
 */
@Path( { "userplane-msgr-action.do", "userplane-xml.do" })
public class UserplaneMessengerActionHandler extends Handler {

	private static final Logger	logger	= Logger.getLogger("ecreator");

	private String			action;
	private String			sessionGUID;
	private Item			memberID, targetMemberID;

	public UserplaneMessengerActionHandler(RequestContext context) {
		super(context);
	}

	/**
	 * 
	 * addFriend( domainID, memberID, targetMemberID, callID )
	 * 
	 * Description: 	Sets targetMemberID as a friend of memberID
	 * 
	 * Parameters:
	 * action:				addFriend
	 * domainID:			The identifier for the domain the user is on
	 * memberID:			The ussers emberID
	 * targetMemberID:	The user to be made a friend
	 * callID:				A unique identifier for this XML call
	 * 
	 * Returns 
	 * Nothing
	 */
	private Object addFriend() {

		UserplaneMessenger userplane = new UserplaneMessenger(context);
		return new XmlResult(userplane.getEmptyDoc());

	}

	private Object getDomainPreferences() {

		UserplaneMessenger userplane = new UserplaneMessenger(context);
		Document doc = userplane.getDomainPreferencesDoc();

		return new XmlResult(doc);
	}

	/**
	 *  Returns the member ID for the member who is logged in and chatting.
	 *  
	 *  Userplane sends us a session id which is the general jsp session. We cannot have userplane just send a member id through as 
	 *  it would be easy for someone to change the id and chat as any member on the site. By passing userplane the session id when creating the
	 *  flash applet, we are making it difficult for a user to guess another session id and log in as someone else.
	 */
	private Object getMemberID() {

		UserplaneMessenger userplane = new UserplaneMessenger(context);
		Document doc = userplane.getMemberIdDoc(sessionGUID);

		return new XmlResult(doc);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		logger.fine("[UserplaneMsgActionHandler] action=" + action + ", memberID=" + memberID + ", targetMemberID=" + targetMemberID + ", sessionGUID="
				+ sessionGUID);

		if ("getDomainPreferences".equals(action)) {
			return getDomainPreferences();
		}

		if ("getMemberID".equals(action)) {
			return getMemberID();
		}

		if ("startIC".equals(action)) {
			return startIC();
		}

		if ("addFriend".equals(action)) {
			return addFriend();
		}

		if ("setBlockedStatus".equals(action)) {
			return setBlockedStatus();
		}

		if ("startConversation".equals(action)) {
			return startConversation();
		}

		if ("notifyConnectionClosed".equals(action)) {
			return notifyConnectionClosed();
		}

		if ("sendArchive".equals(action)) {
			return sendArchive();
		}

		if ("sendPendingMessages".equals(action)) {
			return sendPendingMessages();
		}

		return HttpServletResponse.SC_BAD_REQUEST;
	}

	private Object notifyConnectionClosed() {

		if (memberID != null && targetMemberID != null) {

			UserplaneMessenger userplane = new UserplaneMessenger(context);
			userplane.notifyConnectionClosed(memberID, targetMemberID);

			return new XmlResult(userplane.getEmptyDoc());

		} else {
			return HttpServletResponse.SC_BAD_REQUEST;
		}
	}

	@Override
	protected boolean runSecure() {
		return false;
	}

	/**
	 * 
	 */
	private Object sendArchive() {
		UserplaneMessenger userplane = new UserplaneMessenger(context);
		return new XmlResult(userplane.getEmptyDoc());
	}

	/**
	 * 
	 */
	private Object sendPendingMessages() {
		UserplaneMessenger userplane = new UserplaneMessenger(context);
		return new XmlResult(userplane.getEmptyDoc());
	}

	/**
	 * setBlockedStatus( domainID, memberID, targetMemberID, trueFalse, 	callID )

	 Description: 	Sets whether or not memberID wants targetMemberID blocked
	 Parameters:
	 action:	setBlockedStatus
	 domainID:		The identifier for the domain the user is on
	 memberID:		The user�s memberID
	 targetMemberID:		The user to block (or unblock)
	 trueFalse:		Whether or not to block the user
	 callID:			A unique identifier for this XML call
	 Returns 
	 Nothing

	 */
	private Object setBlockedStatus() {
		UserplaneMessenger userplane = new UserplaneMessenger(context);
		return new XmlResult(userplane.getEmptyDoc());
	}

	/**
	 * This action is sent when a user initiates a chat by launching the web messenger on someone's profile.
	 * The userplane msg app will be opened and then userplane will send this request through to the server so that we know to 
	 * do some action that will trigger the loading of the chat window on the target computer. 
	 */
	private Object startConversation() {

		if (memberID != null && targetMemberID != null) {

			UserplaneMessenger userplane = new UserplaneMessenger(context);
			userplane.startConversation(memberID, targetMemberID);

			return new XmlResult(userplane.getEmptyDoc());

		} else {

			return HttpServletResponse.SC_BAD_REQUEST;
		}
	}

	/**
	 * This action is called to get the data for the app
	 */
	private Object startIC() {

		if (memberID != null && targetMemberID != null) {

			UserplaneMessenger userplane = new UserplaneMessenger(context);
			Document doc = userplane.startIC(memberID, targetMemberID);

			return new XmlResult(doc);

		}

		return HttpServletResponse.SC_BAD_REQUEST;
	}

}
