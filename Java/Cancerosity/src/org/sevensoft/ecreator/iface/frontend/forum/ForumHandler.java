package org.sevensoft.ecreator.iface.frontend.forum;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.RestrictionHandler;
import org.sevensoft.ecreator.model.crm.forum.Forum;
import org.sevensoft.ecreator.model.crm.forum.Topic;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 27-Mar-2006 23:49:37
 *
 * Show 
 */
@Path("forum.do")
public class ForumHandler extends FrontendHandler {

	private Forum	forum;

	public ForumHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (!Module.Forum.enabled(context)) {
			return index();
		}

		if (forum == null) {
			return new BoardHandler(context).main();
		}

        if (!PermissionType.ViewForum.check(context, account, forum)) {
            if (account != null && account.getItemType().getForum().getRestrictionForwardUrl() != null) {
                return new ExternalRedirect(account.getItemType().getForum().getRestrictionForwardUrl());
            } else {
                return new RestrictionHandler(context).main();
            }
        }

        context.setAttribute("pagelink", new Link(ForumHandler.class, null, "forum", forum));

		FrontendDoc doc = new FrontendDoc(context, forum.getName());
		doc.addTrail(BoardHandler.class, null, "Forums");
		doc.addTrail(ForumHandler.class, null, forum.getName(), "forum", forum);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("forum").setCaption(forum.getName()));

				if (!forum.isLocked()) {
					sb.append("<tr><td colspan='4' align='right' class='post_topic'>"
							+ new LinkTag(TopicHandler.class, "create", "Post new topic", "forum", forum) + "</td></tr>");
				}

				sb.append("<tr>");
				sb.append("<th>Topic</th>");
				sb.append("<th>Author</th>");
				sb.append("<th>Replies</th>");
				sb.append("<th>Views</th>");
				sb.append("</tr>");

				for (Topic topic : forum.getTopics(0, 40)) {

					sb.append("<tr>");

					sb.append("<td class='topic'>" + new LinkTag(TopicHandler.class, null, topic.getTitle(), "topic", topic));
					if (topic.hasLastPost()) {

						sb.append("<br/>Last post by ");
						sb.append(topic.getLastPostAuthor());
						sb.append(", ");
						sb.append(topic.getLastPostDate().toString("HH:mm dd-MMM-yyyy"));
					}
					sb.append("</td>");

					sb.append("<td class='author'>" + topic.getAuthor() + "</td>");
					sb.append("<td class='reply_count'>" + topic.getReplyCount() + "</td>");
					sb.append("<td class='views'>" + topic.getViews() + "</td>");
					sb.append("</tr>");

				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}
}
