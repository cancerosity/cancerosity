package org.sevensoft.ecreator.iface.frontend;

import java.io.IOException;

import javax.servlet.ServletException;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.docs.Doc;

/**
 * @author sks 10 Jan 2007 07:17:51
 *
 */
public class ErrorDoc extends Doc {

	private final String	message;

	public ErrorDoc(RequestContext context, String message) {
		super(context);
		this.message = message;
	}

	@Override
	public StringBuilder output() throws ServletException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {

		StringBuilder sb = new StringBuilder();

		sb.append("<html><head>");
		sb.append("<style> div { font-size: 13px; font-family: Tahoma; color: #666666; } </style>");
		sb.append("</head><body>");
		sb.append("<div><strong>Configuration error:</strong> ");
		sb.append(message);
		sb.append("</div>");
		sb.append("</body></html>");

		return sb;
	}

}
