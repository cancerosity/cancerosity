package org.sevensoft.ecreator.iface.frontend.items.ibex;

import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.items.ibex.blocks.IbexBlock;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.results.docs.Doc;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

/**
 * User: Tanya
 * Date: 22.03.2011
 */
@Path("ibex-popup.do")
public class IbexPopupHandler extends FrontendHandler {

    private Item item;

    public IbexPopupHandler(RequestContext context) {
        super(context);
    }

    public Object main() throws ServletException {
        if (!Module.IBex.enabled(context)) {
            return HttpServletResponse.SC_BAD_REQUEST;
        }
        if (item == null) {
            return HttpServletResponse.SC_BAD_REQUEST;
        }
        context.setAttribute("item", item);

        Doc doc = new Doc(context) {

            @Override
            public StringBuilder output() {
                StringBuilder sb = new StringBuilder();
                sb.append(new IbexBlock(context).render(context));
                return sb;
            }
        };
        return doc;
    }
}
