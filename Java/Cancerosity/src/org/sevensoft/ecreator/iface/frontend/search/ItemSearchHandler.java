package org.sevensoft.ecreator.iface.frontend.search;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.RandomHelper;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.BasketHandler;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.ecreator.model.attributes.SearchControl;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.interaction.LastActive;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.sorts.FrontSortRenderer;
import org.sevensoft.ecreator.model.items.sorts.ItemSort;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.ecreator.model.media.images.ImageUtil;
import org.sevensoft.ecreator.model.misc.location.Pin;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.ecreator.model.search.Search.FeaturedMethod;
import org.sevensoft.ecreator.model.search.SearchSaveHeader;
import org.sevensoft.ecreator.model.search.alerts.SearchAlert;
import org.sevensoft.ecreator.model.search.forms.SearchForm;
import org.sevensoft.ecreator.model.search.forms.boxes.ItemSearchBox;
import org.sevensoft.ecreator.model.search.wizards.AutoForward;
import org.sevensoft.ecreator.model.stats.searching.SearchPhrase;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

import javax.servlet.ServletException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author sks 6 Sep 2006 11:36:27
 *         <p/>
 *         Search handler just for searching items
 */
@Path("s.do")
public class ItemSearchHandler extends FrontendHandler {

    /**
     * @author sks 31 Mar 2007 19:00:56
     */
    private final class ItemPermissionsPredicate implements Predicate<Item> {

        public boolean accept(Item e) {

            for (Category c : e.getCategories()) {
                if (!PermissionType.CategoryItems.check(context, account, c)) {
                    return false;
                }
            }

            if (!PermissionType.ItemSummary.check(context, account, e.getItemType())) {
                return false;
            }

            return true;
        }
    }

    /**
     * @author sks 31 Mar 2007 19:00:31
     */
    private final class VisibleItemPredicate implements Predicate<Item> {

        public boolean accept(Item e) {
            return e.isVisible();
        }
    }

    private Map<Attribute, SearchControl> searchControls;
    private boolean exact;
    private String keywords, details, groupedKeywords;
    private int id;
    private int distance;

    /**
     * Main item type
     */
    private ItemType itemType;

    /**
     * Multiple item types
     */
    private Set<Integer> itemTypes;

    private Money sellPriceFrom, sellPriceTo;
    private MultiValueMap<Attribute, String> attributeValues;
    private boolean imageOnly;
    private ItemSort sort;
    private int page;

    private int seed;

    /**
     * Redirect straight to item if only one result
     */
    private AutoForward autoForward;

    /**
     * The location the user has entered - either postcode or placename
     */
    private String location;
    private SortType sortType;
    private LastActive lastActive;
    private String town, county;
    private SearchForm searchForm;
    private boolean header;
    private Category searchCategory;
    private String name;
    private ItemSearchBox searchBox;
    private boolean excludeAccount;
    private boolean excludeAccountItemType;
    private FeaturedMethod featuredMethod;
    private boolean includeSubcategories;

    /**
     * The category we have come from
     */
    private Category category;
    private Date createdAfter;
    private int age;
    private String email;
    private Search search;
    private String alertSms;
    private Attribute sortAttribute;
    private int resultsPerPage;
    private String reference;
    private Date bookingStart;
    private String alertEmail;
    private Date bookingEnd;
    private int duration;
    private ItemSearcher searcher;
    private boolean useActiveLocation;

    /**
     * Attributes from the wizard
     */
    private MultiValueMap<Attribute, String> wizardAttributeValues;
    private Item searchAccount;
    private Markup markup;

    /**
     * Condition for search events on calendar date
     */
    private boolean calendar;

    public ItemSearchHandler(RequestContext context) {
        super(context);
    }

    public ItemSearchHandler(RequestContext context, ItemSearcher searcher) {
        super(context);
        this.searcher = searcher;
    }

    /**
     * Save this search as a search alert
     */
    public Object alert() {

        // create a new search alert
        SearchAlert alert = new SearchAlert(context);
        alert.setEmailAddress(alertEmail);
        alert.setSmsNumber(alertSms);
        alert.save();

        // get the associated search and set parameters
        Search search = alert.getSearch();
        createSearch(search);

        // return page
        FrontendDoc doc = new FrontendDoc(context, "Search alert created");
        doc.addBody("Your search has been saved.<br/><br/>You will be emailed when a new item is added that matches your search criteria.");
        return doc;
    }

    private void createSearch(Search search) {

        search.setAge(age);
        search.setAttributeValues(attributeValues);

        search.setBookingStart(bookingStart);
        search.setBookingEnd(bookingEnd);

        search.setCreatedAfter(createdAfter);

        search.setDetails(details);
        search.setDistance(distance);

        search.setEmail(email);
        search.setExcludeAccount(excludeAccount);
        search.setExcludeAccountItemType(excludeAccountItemType);

        search.setFeaturedMethod(featuredMethod);

        search.setItemType(itemType);
        search.setItemTypes(itemTypes);

        search.setKeywords(keywords);
        search.setLastActive(lastActive);
        search.setLocation(location);
        search.setSearchAccount(searchAccount);

        search.setName(name);

        search.setSearchForm(searchForm);
        search.setSearchCategory(searchCategory);
        search.setSortType(sortType);
        search.setSellPriceFrom(sellPriceFrom);
        search.setSellPriceTo(sellPriceTo);

        search.save();
    }

    public Object email() throws ServletException {

        if (search == null) {
            return main();
        }

        return "The results of this search have been emailed to you";

    }

    /**
     * Returns a link back to this search
     */
    private Link getLink() {

        Link link = new Link(ItemSearchHandler.class);

        if (attributeValues != null) {
            link.addParameters("attributeValues", attributeValues);
        }

        if (searchControls != null) {
            link.addParameters("searchControls", searchControls);
        }

        link.addParameter("category", category);

        link.addParameter("details", details);
        if (distance > 0) {
            link.addParameter("distance", distance);
        }

        if (excludeAccount) {
            link.addParameter("excludeAccount", excludeAccount);
        }

        if (excludeAccountItemType) {
            link.addParameter("excludeAccountItemType", excludeAccountItemType);
        }

        link.addParameter("id", id);
        if (imageOnly) {
            link.addParameter("imageOnly", imageOnly);
        }

        if (itemType != null) {
            link.addParameter("itemType", itemType);
        }
        if (keywords != null) {
            link.addParameter("keywords", keywords);
        }
        if (lastActive != null) {
            link.addParameter("lastActive", lastActive);
        }
        if (location != null) {
            link.addParameter("location", location);
        }
        if (name != null) {
            link.addParameter("name", name);
        }
        link.addParameter("page", page);

        if (seed > 0) {
            link.addParameter("seed", seed);
        }
        if (searchCategory != null)
            link.addParameter("searchCategory", searchCategory);

        link.addParameter("searchForm", searchForm);
        if (sellPriceTo != null) {
            if (sellPriceTo.isPositive()) {
                link.addParameter("sellPriceTo", sellPriceTo);
            }
        }
        if (sellPriceFrom != null) {
            if (sellPriceFrom.isPositive()) {
                link.addParameter("sellPriceFrom", sellPriceFrom);
            }
        }
        if (sort != null) {
            link.addParameter("sort", sort);
        }

        if (sortType != null) {
            link.addParameter("sortType", sortType);
        }
        if (sortAttribute != null) {
            link.addParameter("sortAttribute", sortAttribute);
        }
        if (town != null) {
            link.addParameter("town", town);
        }
        if (searchAccount != null) {
            link.addParameter("searchAccount", searchAccount);
        }
        if (calendar) {
            link.addParameter("calendar", calendar);
        }

        return link;

    }

    /**
     * Get a searcher object from parameters
     */
    private ItemSearcher getSearcher(int resultsPerPage, String location, int x, int y) {

        ItemSearcher searcher = new ItemSearcher(context);

        searcher.setAccount(account);
        searcher.setAttributeValues(attributeValues);

        searcher.setBookingStart(bookingStart);
        searcher.setBookingEnd(bookingEnd);

        searcher.setDetails(details);
        searcher.setDistance(distance);

        searcher.setExcludeAccount(excludeAccount);
        searcher.setExcludeAccountItemType(excludeAccountItemType);

        searcher.setImageOnly(imageOnly);
        searcher.setItemType(itemType);
        searcher.setItemTypes(itemTypes);

        searcher.setKeywords(keywords);
        searcher.setGroupedKeywords(groupedKeywords);

        searcher.setLastActive(lastActive);
        searcher.setLocation(location, x, y);

        searcher.setName(name);

        searcher.setReference(reference);

        searcher.setSearchAccount(searchAccount);
        searcher.setSearchControls(searchControls);
        searcher.setSearchCategory(searchCategory);
        searcher.setSubcats(includeSubcategories);
        searcher.setSeed(seed);
        searcher.setSellPriceFrom(sellPriceFrom);
        searcher.setSellPriceTo(sellPriceTo);

        // if location is not null then always default to distance
        if (location != null) {
            searcher.setSortType(SortType.Distance);

            // otherwise check for presence of a item sort
        } else if (sort != null) {
            searcher.setSort(sort);

            // finally check for an enum sort type
        } else {

            searcher.setSortType(sortType);
            searcher.setSortAttribute(sortAttribute);
        }

        searcher.setStatus("Live");
        searcher.setLimit(resultsPerPage);

        return searcher;
    }

    private String getTitle() {

        // check for the presence of a search form. If we have one then use that for the name if set

        if (searchForm != null) {
            if (searchForm.hasResultsPageTitle()) {
                return searchForm.getResultsPageTitle();
            }
        }

        return "Search results";
    }

    /**
     * All incoming search requests will come here
     */
    @Override
    public Object main() throws ServletException {

        if (searchForm != null) {
            resultsPerPage = searchForm.getResultsPerPage();
            includeSubcategories = searchForm.isIncludeSubcategories();
        }

        // check bounds on results per page
        if (resultsPerPage < 1 || resultsPerPage > 100) {
            resultsPerPage = 20;
        }

        // ensure we are on a sensible page
        if (page < 1) {
            page = 1;
        }

        // check for wizard attributes, they should override standard attributes
        if (wizardAttributeValues != null && wizardAttributeValues.size() > 0) {
            attributeValues = wizardAttributeValues;
        }

        // if seed is not set then generate
        if (seed == 0) {
            seed = RandomHelper.getRandomNumber(0, 99999);
        }

        /*
           * If we have a location value then we need to get coords for it
           */
        int x = 0, y = 0;
        if (location != null) {

            logger.fine("[ItemSearchHandler] location entered: " + location);

            Pin pin = Pin.getFirst(context, location);
            if (pin == null) {

                logger.fine("[ItemSearchHandler] invalid location");
                setError("location", "Invalid location");

            } else {

                x = pin.getX();
                y = pin.getY();
            }
        }

        /*
           * If location is null see if we were passed a town param instead
           */
        if (location == null) {
            location = town;
        }

        // if we have an active location and location is null then override            //todo location gets boolean value
//        if (activeLocation != null && location == null) {
//            location = activeLocation;
//        }

        // link to this search in the context
        setAttribute("link", getLink());
        setAttribute("searchLink", getLink());

        // put this search form in the context is used
        setAttribute("searchForm", searchForm);

        // if from a search box put in context
        context.setAttribute("searchBox", searchBox);

        if (resultsPerPage > 0) {
            context.setAttribute("resultsPerPage", resultsPerPage);
        }

        // if we have errors and we came from a category then return to that category
        if (hasErrors()) {
            if (category != null) {
                return new CategoryHandler(context, category).main();
            }
        }

        // put booking start and stay in context for use by markers
        if (bookingStart != null) {

            if (duration > 0) {
                bookingEnd = bookingStart.addDays(duration);
            }

            context.setAttribute("bookingEnd", bookingEnd);
            context.setAttribute("bookingStart", bookingStart);
        }

        ItemSearcher searcher = getSearcher(resultsPerPage, location, x, y);

        // put the the searcher in the context so it is available to our filter
        setAttribute("searcher", searcher);

        if (calendar) {
            searcher.setIsCalendar(calendar);
        }

        int totalItems = searcher.size();

        /*
           * Create results object encapsulating all the pages etc info
           */
        Results results = new Results(totalItems, page, resultsPerPage);
        setAttribute("results", results);

        searcher.setPrioritised(true);
        searcher.setStart(results.getStartIndex());
        searcher.setLimit(resultsPerPage);

        List<Item> items = searcher.getItems();

        /*
           * strip out any items we do not have permission to view.
           */
        if (Module.Permissions.enabled(context)) {
            CollectionsUtil.filter(items, new ItemPermissionsPredicate());
            logger.fine("[ItemSearchHandler] filtered for permissions, " + items.size() + " remaining");
        }

        /*
           * Strip out any non approved (ie waiting moderation) items
           */
        if (Module.ListingModeration.enabled(context)) {
            CollectionsUtil.filter(items, new VisibleItemPredicate());
            logger.fine("[ItemSearchHandler] filtered for visibility, " + items.size() + " remaining");
        }

        if (searchForm != null) {
            Seo seo = Seo.getInstance(context);
            setAttribute("title_tag", seo.getTitleTagRendered(searchForm));
            setAttribute("description_tag", seo.getDescriptionTagRendered(searchForm));
            setAttribute("keywords", seo.getKeywordsRendered(searchForm));
        }

        // get title
        final String title = getTitle();
        FrontendDoc doc = new FrontendDoc(context, title);

        if (searchForm != null) {

            // featured search banners
            ItemType bannerItemType = ItemType.getByName(context, "banner", "banners");
            if (bannerItemType != null) {

                // wrap banners in a div
                doc.addBody("<div id='banners'>");

                ItemSearcher searcher2 = getSearcher(resultsPerPage, location, x, y);
                searcher2.setFeaturedMethod(null);
                searcher2.setItemTypes(null);
                searcher2.setItemType(bannerItemType);
                searcher2.setLimit(1);
                searcher2.setStart(0);

                Item banner = searcher2.getItem();
                Markup m = bannerItemType.getListMarkup();
                MarkupRenderer r = new MarkupRenderer(context, m);
                r.setBody(banner);

                doc.addBody(r);
                doc.addBody("</div>");
            }
        }

        if (hasError("location")) {
            doc.addBody(searchForm != null ? searchForm.getNoResultsText() : "No results");
            return doc;
        }

        // do the highlighted results block
        if (searchForm != null) {

            int i = searchForm.getNumberOfHighlightedResults();
            Markup m = searchForm.getHighlightedResultsMarkup();
            if (i > 0) {

                // wrap highlightes results in a div
                doc.addBody("<div id='highlightedSearchResults'>");

                searcher.setFeaturedMethod(FeaturedMethod.Yes);
                searcher.setLimit(i);

                List<Item> highlightedItems = searcher.getItems();
                MarkupRenderer r = new MarkupRenderer(context, m);
                r.setBodyObjects(highlightedItems);

                doc.addBody(r);
                doc.addBody("</div>");
            }
        }

        // wrap search results in a div that we can replace content with on ajax searches
        doc.addBody("<div id='searchResultsDiv'>");

        // if we have come a category, set that category as part of the trail
        if (category != null) {
            doc.addTrail(category.getUrl(), category.getName());
        }
        doc.addTrail(getLink(), title);

        logger.fine("[ItemSearchHandler] items retrieved=" + items);

        if (items.isEmpty()) {

            if (searchForm != null) {

                if (searchForm.hasNoResultsForwardUrl()) {

                    logger.fine("[ItemSearchHandler] forwarding to " + searchForm.getNoResultsForwardUrl());
                    return new ExternalRedirect(searchForm.getNoResultsForwardUrl());
                }

                if (searchForm.isAlerts()) {

                    doc.addBody(new SearchAlertHeader(context, searchForm, searcher.getParameters()));
                }
            }

            if (exact) {
                return index();
            }

            doc.addBody(searchForm != null ? searchForm.getNoResultsText() : "No results");
            doc.addBody("<div class='searchterms'>" + StringHelper.implode(searcher.getSearchParameters(), null) + "</div>");
            doc.addBody("</div>");
            return doc;
        }

        if (exact && items.size() != 1) {
            return index();
        }

        /*
           * An autoforward will forward us immediately under certain circumstances
           * Do not auto forward on white label searches
           */
        if (!wl) {

            if (searchForm != null) {
                autoForward = searchForm.getAutoForward();
            }

            if (autoForward != null) {

                switch (autoForward) {

                    default:
                    case Never:
                        break;

                    case SingleResult:
                        if (items.size() == 1) {
                            return new ExternalRedirect(items.get(0).getUrl());
                        }
                        break;

                    case Always:
                        return new ExternalRedirect(items.get(0).getUrl());
                }
            }

            // we have results so log this search
            if (searcher.getKeywords() != null) {

                SearchPhrase.process(context, searcher.getKeywords());

            } else if (searcher.getName() != null) {

                SearchPhrase.process(context, searcher.getName());
            }

        }

        // prepopulate
        ImageUtil.prepopulate(context, items);
        AttributeUtil.prepopulate(context, items);

        if (searchForm != null) {

            markup = searchForm.getListMarkup();

            if (searchForm.isSavable()) {
                doc.addBody(new SearchSaveHeader(context, searchForm, searcher.getParameters()));
            }
        }

        doc.addBody("<div class='searchterms'>" + StringHelper.implode(searcher.getSearchParameters(), null) + "</div>");
        if (searchForm == null || searchForm.isResultsTop()) {
            doc.addBody(new ResultsControl(context, results, getLink()));
        }

        if (searchForm == null || searchForm.isSortsTop()) {
            doc.addBody(new FrontSortRenderer(context, getLink(), items.get(0).getItemType().getSorts(), sort, location));
        }

        //		if (results.getTotalResults() > 200) {
        //			doc.addBody(new SearchAdviceHeader(context, results));
        //		}

        if (markup == null) {
            if (itemType != null) {
                markup = itemType.getSearchListMarkup();
            }
        }

        // if markup still null then use default from first item type
        if (markup == null) {
            markup = items.get(0).getItemType().getListMarkup();
        }

        doc.append(new FormTag(BasketHandler.class, "add", "multi"));
        doc.append(new HiddenTag("ident", RandomHelper.getRandomString(6)));
        doc.append(new HiddenTag("item", "0"));
        doc.append(new HiddenTag("linkback", "s.do?" + context.getQueryString()));

        MarkupRenderer r = new MarkupRenderer(context, markup);
        r.setLocation(location, x, y);
        r.setBodyObjects(items);

        doc.addBody(r);
        doc.addBody("</form>");

        if (searchForm != null) {

            if (searchForm.isSortsBottom()) {
                doc.addBody(new FrontSortRenderer(context, getLink(), items.get(0).getItemType().getSorts(), sort, location));
            }

            if (searchForm.isResultsBottom()) {
                doc.addBody(new ResultsControl(context, results, getLink()));
            }
        }

        doc.addBody("</div>");

        return doc;
    }

    /**
     * Save this search
     */
    public Object save() throws ServletException {

        Link link = new Link(ItemSearchHandler.class, "save");
        link.addParameters(getSearcher(20, null, 0, 0).getLink().getParameters());

        if (account == null) {
            return new LoginHandler(context, link).main();
        }

        // create a search object for this member using the search parameters
        search = new Search(context, account);
        createSearch(search);

        Link runLink = search.getLink();

        FrontendDoc doc = new FrontendDoc(context, "Search saved");
        doc.addBody("Your search has been saved.<br/><br/>" + new LinkTag(runLink, "Click here") + " to return to this search, or " +
                new LinkTag(SavedSearchesHandler.class, null, "click here") + " to view all your saved searches.");
        return doc;
    }

}
