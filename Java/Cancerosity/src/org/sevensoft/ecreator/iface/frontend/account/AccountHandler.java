package org.sevensoft.ecreator.iface.frontend.account;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.panels.AccountPanel;
import org.sevensoft.ecreator.iface.frontend.account.registration.RegistrationHandler;
import org.sevensoft.ecreator.model.accounts.AccountModule;
import org.sevensoft.ecreator.model.accounts.registration.RegistrationModule;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.login.facebook.FacebookLoginSettings;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.commons.samdate.Date;

import javax.servlet.ServletException;
import java.util.StringTokenizer;
import java.io.IOException;
import java.text.ParseException;

import sun.misc.BASE64Decoder;
import net.sf.json.JSONObject;
import net.sf.json.JSONException;
import net.sf.json.JsonConfig;

/**
 * @author sks 19-Aug-2005 07:10:43
 */
@Path({"a.do", "account.do", "member-account.do"})
public class AccountHandler extends FrontendHandler {

    private transient FacebookLoginSettings facebookLoginSettings;

    public AccountHandler(RequestContext context) {
        super(context);
        this.facebookLoginSettings = FacebookLoginSettings.getInstance(context);
    }

    private Link getLink() {
        return new Link(AccountHandler.class);
    }

    @Override
    public Object main() throws ServletException {

        if (!Module.Accounts.enabled(context)) {
            logger.fine("[AccountHandler] accounts are disabled");
            return index();
        }

        if (account == null) {
            logger.fine("[AccountHandler] not logged in, returning to login handler");
            return new LoginHandler(context).main();
        }

        if (account.getItemType().getAccountModule().isHideAccount()) {
            logger.fine("[AccountHandler] main account page is hidden, exiting");
            return index();
        }

        final AccountModule module = account.getItemType().getAccountModule();

        setAttribute("link", getLink());

        FrontendDoc doc = new FrontendDoc(context, captions.getAccountCaption());
        doc.addTrail(AccountHandler.class, captions.getAccountCaption());
        if (module.hasAccountHeader()) {
            doc.addBody(module.getAccountHeader());
        }
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append("<div class='ec_account_welcome'>Welcome <strong>" + account.getName() + "</strong>.</div>");

                Markup markup = account.getItemType().getAccountModule().getAccountMarkup();
                if (markup == null) {

                    sb.append(new AccountPanel(context, account));

                } else {

                    MarkupRenderer r = new MarkupRenderer(context, markup);
                    sb.append(r);

                }

                return sb.toString();
            }

        });
        if (module.hasAccountFooter()) {
            doc.addBody(module.getAccountFooter());
        }
        return doc;
    }

    public Object  registerFacebook() throws ServletException {
        String signedReq = context.getParameter("signed_request");

        if(signedReq == null)
        {
            logger.warning("ERROR: Unable to retrieve signed_request parameter");
            addError("we are unable to register you");
            return main();
        }

        int count = 0;
        String payload = null;

        //The parameter contains encoded signature and payload separated by �.�
        StringTokenizer st = new StringTokenizer(signedReq, ".");

        //Retrieve payload (Note: encoded signature is used for internal verification and it is optional)
        while (st.hasMoreTokens())
        {
            if(count == 1)
            {
                payload = st.nextToken();
                break;
            }
            else
                st.nextToken();

            count++;
        }

        //Initialize Base64 decoder
        BASE64Decoder decoder = new BASE64Decoder();

        //Replace special character in payload as indicated by FB
        payload = payload.replace("-", "+").replace("_", "/").trim();

        //Decode payload
        try
        {
            byte[] decodedPayload = decoder.decodeBuffer(payload);
            payload = new String(decodedPayload, "UTF8");
        }
        catch (IOException e)
        {
            logger.warning("ERROR: Unable to perform Decode");
            addError("we are unable to register you");
            return main();
        }

        payload = payload.substring(0, payload.lastIndexOf("}") + 1);

        //JSON Decode - payload
        try
        {
            JSONObject payloadObj = JSONObject.fromObject(payload, new JsonConfig());
            JSONObject payloadObject = (JSONObject) payloadObj.get("registration");
            String facebookId = "" + payloadObj.get("user_id"); //Retrieve user ID
            String oauthToken = "" + payloadObj.get("oauth_token"); //Retrieve oauth token
            String nameToken = "" + payloadObject.get("name"); //Retrieve pass
            String passToken = "" + payloadObject.get("password"); //Retrieve login
            String emailToken = "" + payloadObject.get("email"); //Retrieve login
            String genderToken = ""+payloadObject.get("gender");
            String birthdayToken = ""+payloadObject.get("birthday");
            JSONObject locationObj = (JSONObject) payloadObject.get("location");
            String locationToken = "" + locationObj.get("name");
            String phoneToken = "" + payloadObject.get("phone");
            String anniversaryToken = ""+payloadObject.get("anniversary");
            String aboutme =  ""+payloadObject.get("aboutme");

            if (emailToken != null && !RegistrationModule.isUniqueEmail(context, emailToken)) {
                addError("That email is already in use. Please use another.");
                return new RegistrationHandler(context).start();
            }

            account = new Item(context, facebookLoginSettings.getAccountType(), nameToken, "live", null);
            account.setEmail(emailToken);
            account.setPassword(passToken);

            if (facebookLoginSettings.getRealNameAttribute() != null)
                account.setAttributeValue(facebookLoginSettings.getRealNameAttribute(), nameToken);
            if (facebookLoginSettings.getGenderAttribute() != null)
                account.setAttributeValue(facebookLoginSettings.getGenderAttribute(), genderToken);
            if (facebookLoginSettings.getBirthdayAttribute() != null) {
                try {
                    if (birthdayToken != null) {
                        Date birthDate = new Date(birthdayToken, "MM/dd/yyyy");
                        account.setAttributeValue(facebookLoginSettings.getBirthdayAttribute(), birthDate.toEditString());
                    }
                } catch (ParseException e) {
                    logger.warning("[AccountHandler] registerFacebook (). Birthday token '" + birthdayToken + "' parse error");
                }
            }
            if (facebookLoginSettings.getLocationAttribute() != null && locationToken != null)
                account.setAttributeValue(facebookLoginSettings.getLocationAttribute(), locationToken);
            if (facebookLoginSettings.getPhoneAttribute() != null && phoneToken != null)
                account.setAttributeValue(facebookLoginSettings.getPhoneAttribute(), phoneToken);
            if (facebookLoginSettings.getAnniversaryAttribute() != null && anniversaryToken != null)
                account.setAttributeValue(facebookLoginSettings.getAnniversaryAttribute(), anniversaryToken);
            if (facebookLoginSettings.getAboutMeAttribute() != null && aboutme != null)
                account.setAttributeValue(facebookLoginSettings.getAboutMeAttribute(), aboutme);

            if (facebookLoginSettings.getImageAttribute() != null && facebookId != null && !"null".equalsIgnoreCase(facebookId)) {
                String imageUrl = "http://graph.facebook.com/" + facebookId + "/picture?type=large";
                account.setAttributeValue(facebookLoginSettings.getImageAttribute(), imageUrl);
            }
            account.save();
            account.login(context.getSessionId());

/*
            if (facebookId != null) {
                String imageUrl = "http://graph.facebook.com/" + facebookId + "/picture?type=large";
                try {

                    HttpURLConnection conn = (HttpURLConnection) new URL(imageUrl).openConnection();
                    InputStream inputStream = (InputStream)conn.getInputStream();
                    String img = new HttpClient(conn.getURL()).getUrl().toString();

                    account.addImage(new URL(img), true, true, true);

                } catch (ImageLimitException e) {
                    addError(e);
                } catch (IOException e) {
                    return new ErrorDoc(context, "The image you are attempting to upload is in CYMK colour mode. " +
                            "This is unsopported currently, please use a photo editing application (e.g. Photoshop) to convert the file to RGB colour. For more information or assistance please contact support.");
                }
            }
          */

        }
        catch (JSONException e)
        {
            logger.warning("ERROR: Unable to perform JSON decode");
            addError("we are unable to register you");
            return main();
        }

        return main();
    }

    @Override
    protected boolean runSecure() {
        return Config.getInstance(context).isSecured();
    }
}
