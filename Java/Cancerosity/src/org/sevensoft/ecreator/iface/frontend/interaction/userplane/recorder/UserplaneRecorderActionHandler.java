package org.sevensoft.ecreator.iface.frontend.interaction.userplane.recorder;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.sevensoft.ecreator.model.interaction.userplane.recorder.UserplaneRecorder;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.XmlResult;

/**
 * @author sks 26 Nov 2006 13:42:42
 *
 */
@Path("userplane-recorder-action.do")
public class UserplaneRecorderActionHandler extends Handler {

	private static final Logger	logger	= Logger.getLogger("ecreator");

	private String			action;
	private String			sessionGUID;
	private Item			memberID;
	private String			status;
	private String			exists;
	private String			xmlData;

	public UserplaneRecorderActionHandler(RequestContext context) {
		super(context);
	}

	public UserplaneRecorderActionHandler(RequestContext context, String sessionGUID, Item memberID) {
		super(context);
		this.sessionGUID = sessionGUID;
		this.memberID = memberID;
	}

	private Object getAdminIPAddresses() {

		UserplaneRecorder recorder = new UserplaneRecorder(context);
		return new XmlResult(recorder.getEmptyDoc());
	}

	private Object getDeletedRecordings() {

		UserplaneRecorder recorder = new UserplaneRecorder(context);
		return new XmlResult(recorder.getEmptyDoc());
	}

	private Object getDomainPreferences() {

		UserplaneRecorder recorder = new UserplaneRecorder(context);
		Document doc = recorder.getDomainPreferencesDoc();

		return new XmlResult(doc);
	}

	private Object getMemberID() {

		UserplaneRecorder userplane = new UserplaneRecorder(context);
		Document doc = userplane.getMemberID(sessionGUID);

		return new XmlResult(doc);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		logger.fine("[UserplaneRecorderActionHandler] action=" + action + ", memberID=" + memberID + ", sessionGUID=" + sessionGUID);

		if ("getDomainPreferences".equalsIgnoreCase(action)) {
			return getDomainPreferences();
		}

		if ("getMemberID".equalsIgnoreCase(action)) {
			return getMemberID();
		}

		if ("getDeletedRecordings".equalsIgnoreCase(action)) {
			return getDeletedRecordings();
		}

		if ("getAdminIPAddresses".equalsIgnoreCase(action)) {
			return getAdminIPAddresses();
		}

		if ("notifyRecordingChange".equalsIgnoreCase(action)) {
			return notifyRecordingChange();
		}

		if ("sendRecordingList".equals(action)) {
			return sendRecordingList();
		}

		return HttpServletResponse.SC_BAD_REQUEST;
	}

	private Object notifyRecordingChange() {

		if (memberID == null) {
			return HttpServletResponse.SC_BAD_REQUEST;
		}

		UserplaneRecorder recorder = new UserplaneRecorder(context);
		Document doc = recorder.notifyRecordingChange(memberID, status, exists);

		return new XmlResult(doc);
	}

	@Override
	protected boolean runSecure() {
		return false;
	}

	private Object sendRecordingList() {

		UserplaneRecorder recorder = new UserplaneRecorder(context);
		try {

			Document doc = recorder.sendRecordingList(xmlData);

			return new XmlResult(doc);

		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
	}

}
