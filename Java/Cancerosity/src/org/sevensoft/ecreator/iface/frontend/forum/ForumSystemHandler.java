package org.sevensoft.ecreator.iface.frontend.forum;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Sep 2006 09:19:30
 *
 */
public abstract class ForumSystemHandler extends FrontendHandler {

	public ForumSystemHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Object init() throws ServletException {
		return super.init();
	}

}
