package org.sevensoft.ecreator.iface.frontend.extras.poll;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.extras.polls.Option;
import org.sevensoft.ecreator.model.extras.polls.Poll;
import org.sevensoft.ecreator.model.stats.visits.Visitor;
import org.sevensoft.ecreator.model.stats.StatsCenter;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 25-May-2004 15:30:26
 */
@Path("poll.do")
public class PollHandler extends FrontendHandler {

	private enum Colors {
		Red("#cc3333"), Blue("#0066cc"), Yellow("#cc9900"), Green("#009900"), LightBlue("#0099cc"), Orange("#ff9900"), Lime("#99cc00"), Turk("#00cc99"), Purple(
				"#6600ff");

		private final String	hex;

		private Colors(String hex) {
			this.hex = hex;
		}

		@Override
		public String toString() {
			return hex;
		}
	}

	private Option	option;
	private Poll	poll;

	public PollHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (poll == null) {
			return index();
		}

		final List<Poll> pollLinks = Poll.get(context);
		pollLinks.remove(poll);

		FrontendDoc doc = new FrontendDoc(context, "Poll results");
		doc.addTrail(poll.getUrl(), poll.getTitle());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				if (poll.hasContent()) {
					sb.append(new TableTag("ec_poll_content"));
					sb.append("<tr><td>");
					sb.append(poll.getContent());
					sb.append("</td></tr>");
					sb.append("</table>");
				}

				sb.append(new TableTag("ec_poll_results"));
				sb.append("<tr><td class='intro' colspan='4'>We asked..</td></tr>");
				sb.append("<tr><td class='title' colspan='4'>" + poll.getTitle() + "</td></tr>");

				int n = 0;
				for (Option option : poll.getOptions()) {

					sb.append("<tr class='option'>");
					sb.append("<td>" + option.getText() + "</td>");
					sb.append("<td>");

					if (option.getPercentage() > 0) {
						sb.append("<table class='bar'><tr><td bgcolor='" + Colors.values()[n % Colors.values().length] + "' width='"
								+ option.getPercentage() + "' height='16'></td></tr></table>");
					} else {
						sb.append("&nbsp;");
					}

					sb.append("</td>");

					sb.append("<td>");
					if (option.getVotes() > 0)
						sb.append(option.getPercentageString() + "%");
					else
						sb.append("&nbsp;");
					sb.append("</td>");
					sb.append("<td>" + option.getVotes() + " Votes</td>");
					sb.append("</tr>");

					n++;
				}

				sb.append("<tr><td class='total' colspan='4' align='right'>Total: " + poll.getVotes() + " Votes</td></tr>");
				sb.append("</table>");

				if (pollLinks.size() > 0) {

					sb.append(new TableTag("ec_poll_links"));
					sb.append("<tr><td class='intro'>Some of our other polls..</td></tr>");
					for (Poll poll : pollLinks) {
						sb.append("<tr><td class='title'>" + poll.getTitle() + " - "
								+ new LinkTag(PollHandler.class, null, "View results", "poll", poll) + "</td></tr>");
					}
					sb.append("</table>");
				}

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	protected void stats() {
        new StatsCenter(context).run();
		Visitor.process(context, "Poll results");
	}

	public Object vote() throws ServletException {

		if (poll == null || option == null)
			return main();

		poll.vote(option);

		clearParameters();
		return main();
	}

}