package org.sevensoft.ecreator.iface.frontend.account.registration.panels;

import java.util.List;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Transformer;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.frontend.account.registration.RegistrationHandler;
import org.sevensoft.ecreator.iface.frontend.misc.FieldValueCell;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.iface.frontend.misc.agreements.panels.InlineAgreementPanel;
import org.sevensoft.ecreator.model.accounts.registration.RegistrationModule;
import org.sevensoft.ecreator.model.accounts.registration.RegistrationSession;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.ecreator.model.attributes.renderers.values.AFormSectionsValuePanel;
import org.sevensoft.ecreator.model.attributes.renderers.values.AFormValuePanel;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.misc.agreements.Agreement;
import org.sevensoft.ecreator.model.bespoke.ipoints.IPointsSettings;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 12 Mar 2007 19:01:38
 *
 */
public class RegistrationConfirmationPanel extends Panel {

	private final RegistrationSession	registrationSession;
	private final ItemType			itemType;
	private final RegistrationModule	module;

	public RegistrationConfirmationPanel(RequestContext context, RegistrationSession registrationSession) {
		super(context);
		this.registrationSession = registrationSession;
		this.itemType = registrationSession.getItemType();
		this.module = itemType.getRegistrationModule();
	}

	@Override
	public void makeString() {

		sb.append(new FormTag(RegistrationHandler.class, "process", "post"));
		sb.append(new HiddenTag("register", true));

		//				/*
		//				 * If more than one signup group then show selection here and let us change it
		//				 */
		//				List<ItemType> itemTypes = ItemType.getRegistration(context);
		//				if (itemTypes.size() > 1) {
		//
		//					sb.append("<tr><td colspan='2'> You are signing up for a " + registrationSession.getItemType().getName() + " account. ");
		//					sb.append(new LinkTag(RegistrationHandler.class, "process", "Change account type", "editMemberGroup", true));
		//					sb.append("</td></tr>");
		//				}

		sb.append(new FormFieldSet("Confirm personal details"));

		sb.append(new FieldValueCell(registrationSession.getItemType().getNameCaption(), registrationSession.getName()));
		sb.append(new FieldValueCell(registrationSession.getItemType().getEmailCaption(), registrationSession.getEmail()));
		sb.append(new FieldValueCell("Password", registrationSession.getPassword().replaceAll(".", "*")));

		if (registrationSession.hasReferrer()) {
			sb.append(new FieldValueCell("Referred from", registrationSession.getReferrer()));
		}

		// attributes that have no section
		List<Attribute> nonSectionAttributes = AttributeUtil.getNonSectionedAttributes(registrationSession);

		//		 attributes in a section
		//		List<Attribute> sectionAttributes = AttributeUtil.getSectionedAttributes(registrationSession);

		AFormValuePanel panel = new AFormValuePanel(context, registrationSession, nonSectionAttributes);
		sb.append(panel);

		sb.append("<div class='text'>To edit these details " + new LinkTag(RegistrationHandler.class, "process", "click here", "editDetails", true)
				+ "</div>");

		sb.append("</fieldset>");

		sb.append(new AFormSectionsValuePanel(context, registrationSession));

		// if this listing package allows images then show images selector

        if (module.isRegistrationImages()) {
            sb.append(new FormFieldSet("Images"));

            if (registrationSession.hasAnyImage()) {

                sb.append("<div class='text'>");

                for (Img img : registrationSession.getAllImages()) {
                    sb.append(img.getThumbnailTag());
                }

                sb.append(new LinkTag(RegistrationHandler.class, "process", "Change images", "editImages", true));
                sb.append("</div>");

            } else {

                sb.append("<div class='text'>You have chosen to not add any images yet. ");
                sb.append(new LinkTag(RegistrationHandler.class, "process", "Add image", "editImages", true));
                sb.append("</div>");

            }

            sb.append("</fieldset>");
        }

		// Newsletter opt ins
		List<Newsletter> newsletters = registrationSession.getNewsletterOptIns();
		if (newsletters.size() > 0) {

			List<String> newsletterNames = CollectionsUtil.transform(newsletters, new Transformer<Newsletter, String>() {

				public String transform(Newsletter obj) {
					return obj.getName();
				}
			});

			sb.append(new FormFieldSet("Newsletter(s) opted in"));

			sb.append("<div class='text'>You chosen to opt into the following newsletters: " + StringHelper.implode(newsletterNames, ", ", true)
					+ ". To change these newsletters " + new LinkTag(RegistrationHandler.class, "process", "click here", "editDetails", true)
					+ ".</div>");

			sb.append("</fieldset>");
		}

		/*
		 * Do any inline agreements
		 */
		if (registrationSession.getItemType().getRegistrationModule().hasInlineAgreement()) {

			Agreement agreement = registrationSession.getItemType().getRegistrationModule().getAgreement();

			sb.append(new InlineAgreementPanel(context, agreement));
		}

        IPointsSettings settings = IPointsSettings.getInstance(context);
        if (settings.isEnabled()) {

            sb.append(new FormFieldSet("IPoints Registration"));

            sb
                    .append("<div class='text'>Tick this box if you wish to register for ipoints. "
                            + "If you have already registered on another site you will be able to enter your existing account details at the checkout to earn points."
                            + new CheckTag(context, "ipointsRegister", true) + ".</div>");

            sb.append("</fieldset>");
        }

        sb.append("<div class='ec_form_commands'>");
		sb.append(new SubmitTag("Complete registration"));
		sb.append(new ButtonTag(RegistrationHandler.class, "start", "Start again"));
		sb.append("</div>");

		sb.append("</form>");
	}
}
