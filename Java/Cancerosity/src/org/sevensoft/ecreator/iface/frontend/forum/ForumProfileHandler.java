package org.sevensoft.ecreator.iface.frontend.forum;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 17 Feb 2007 08:13:57
 *
 */
@Path("forum-profile.do")
public class ForumProfileHandler extends FrontendHandler {

	private Item	account;

	public ForumProfileHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (account == null) {
			return index();
		}

		FrontendDoc doc = new FrontendDoc(context, "Forum profile");
		doc.addTrail(PostSearchHandler.class, null, "Forum profile");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				return sb.toString();
			}

		});
		return doc;
	}
}
