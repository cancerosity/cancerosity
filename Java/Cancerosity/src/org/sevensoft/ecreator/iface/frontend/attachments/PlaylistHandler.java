package org.sevensoft.ecreator.iface.frontend.attachments;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.StreamResult;

/**
 * @author sks 3 Nov 2006 08:08:16
 * 
 * Returns a playlist for an attachment
 *
 */
@Path("attachments-playlist.do")
public class PlaylistHandler extends Handler {

	private Attachment	attachment;

	public PlaylistHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		if (attachment == null)
			return HttpServletResponse.SC_BAD_REQUEST;

		return new StreamResult("c:\\mad.mp3", "html/text", "playlist.m3u", StreamResult.Type.Stream);
		//	return new StreamResult(attachment.getUrl(), "html/text", "playlist.m3u", StreamResult.Type.Stream);
	}

	@Override
	protected boolean runSecure() {
		return false;
	}

}
