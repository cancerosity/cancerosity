package org.sevensoft.ecreator.iface.frontend.meetups;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.misc.FieldInputCell;
import org.sevensoft.ecreator.iface.frontend.misc.FormFieldSet;
import org.sevensoft.ecreator.model.extras.meetups.Meetup;
import org.sevensoft.ecreator.model.extras.meetups.Rsvp;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;

/**
 * @author sks 25 Apr 2007 12:53:19
 */
@Path("rsvp.do")
public class RsvpHandler extends FrontendHandler {

    private Item item;
    private String message;
    private boolean yes;
    private boolean edit;

    public RsvpHandler(RequestContext context) {
        super(context);
    }

    public Object remove() throws ServletException {

        if (!Module.Meetups.enabled(context)) {
            return index();
        }

        if (account == null) {
            return new LoginHandler(context, new Link(RsvpHandler.class)).main();
        }

        if (item == null) {
            return index();
        }

        Meetup meetup = item.getMeetup();
        meetup.removeRsvp(account);

        return edit ? new ExternalRedirect(account.getUrl()) : new ExternalRedirect(item.getUrl());
    }

    public Object add() throws ServletException {

        if (!Module.Meetups.enabled(context)) {
            return index();
        }

        if (account == null) {
            return new LoginHandler(context, new Link(RsvpHandler.class)).main();
        }

        if (item == null) {
            return index();
        }

        Meetup meetup = item.getMeetup();
        if (edit) {
            yes = true;
        }
        meetup.addRsvp(account, yes, message);

        return edit ? new ExternalRedirect(account.getUrl()) : new ExternalRedirect(item.getUrl());
    }

    private String getQuickMessage() {
        Query q = new Query(context, "SELECT * FROM # rsvp WHERE account=? AND item=?");
        q.setTable(Rsvp.class);
        q.setParameter(account);
        q.setParameter(item);
        Rsvp rsvp = q.get(Rsvp.class);
        return rsvp != null ? rsvp.getMessage() : "";
    }

    @Override
    public Object main() throws ServletException {

        if (!Module.Meetups.enabled(context)) {
            return index();
        }

        if (account == null) {
            return new LoginHandler(context, new Link(RsvpHandler.class)).main();
        }

        if (item == null) {
            return index();
        }

        FrontendDoc doc = new FrontendDoc(context, "Rsvp");
        doc.addTrail(item.getUrl(), item.getName());
        doc.addTrail(RsvpHandler.class, null, "Rsvp", "item", item);
        doc.addBody(new MessagesTag(context));
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new FormTag(RsvpHandler.class, "add", "post"));
                sb.append(new HiddenTag("item", item));
                sb.append(new HiddenTag("edit", edit));

                sb.append(new FormFieldSet("Rsvp"));

                if (!edit) {
                    sb.append(new FieldInputCell("Are you going", "Select if you are going to this event.", new BooleanRadioTag(context, "yes", true)
                            .setTitles("Yes", "No")));
                }

                sb.append(new FieldInputCell("Your message", "Add a quick comment!", new TextTag(context, "message", getQuickMessage(), 40)));

                sb.append("</fieldset>");

                sb.append("<div class='ec_form_commands'>" + new SubmitTag("Rsvp") + "</div>");

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }
}