package org.sevensoft.ecreator.iface.frontend.account;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 19-Aug-2005 07:10:43
 * 
 */
@Path("restriction.do")
public class RestrictionHandler extends FrontendHandler {

	private static final String	Title	= "You cannot view that page";
	private transient String	restrictionText;
	private Item			item;

	public RestrictionHandler(RequestContext context) {
		this(context, null);
	}

	public RestrictionHandler(RequestContext context, Item item, String restrictionText) {
		this(context, restrictionText);
		this.item = item;
	}

	public RestrictionHandler(RequestContext context, String restrictionText) {
		super(context);
		this.restrictionText = restrictionText;
	}

	@Override
	public Object main() throws ServletException {

		String title;
		if (item != null) {
			title = item.getName();
		} else {
			title = Title;
		}

		if (restrictionText == null) {

			if (account == null) {
				return new LoginHandler(context).main();
			} else {
				logger.fine("[RestrictionHandler] we have no restriction text as a param, taking from account");
				restrictionText = account.getItemType().getAccountModule().getRestrictionContent();
			}
		}

		FrontendDoc doc = new FrontendDoc(context, title);

		// if we are restricted from viewing an item show the item in the trail
		if (item != null) {
			doc.addTrail(item.getUrl(), item.getName());
		} else {
			doc.addTrail(RestrictionHandler.class, null, title);
		}

		if (restrictionText != null) {
			doc.addBody(restrictionText);
		}

		return doc;
	}
}
