package org.sevensoft.ecreator.iface.frontend.account.buyers;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.net.EmailValidator;
import org.sevensoft.ecreator.iface.frontend.FormRow;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.addresses.AddressesHandler;
import org.sevensoft.ecreator.model.accounts.buyers.BuyerConfig;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.PasswordTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 15 Jan 2007 23:05:48
 *
 */
@Path("buyers.do")
public class BuyersHandler extends FrontendHandler {

	private Item		buyer;
	private String		name;
	private List<Address>	addresses;
	private String		pwd;
	private String		em;

	public BuyersHandler(RequestContext context) {
		super(context);
	}

	public Object delete() throws ServletException {

		if (account == null) {
			return new LoginHandler(context, new Link(BuyersHandler.class)).main();
		}

		if (buyer == null) {
			return main();
		}

		account.removeBuyer(buyer);
		clearParameters();
		return main();
	}

	public Object create() throws ServletException {

		if (account == null) {
			return new LoginHandler(context, new Link(BuyersHandler.class)).main();
		}

		if (name == null) {
			return main();
		}

		account.addBuyer(name);
		clearParameters();
		return main();
	}

	public Object edit() throws ServletException {

		if (account == null) {
			return new LoginHandler(context, new Link(BuyersHandler.class)).main();
		}

		if (buyer == null) {
			return main();
		}

		if (!buyer.getBuyerFor().equals(account)) {
			logger.fine("[BuyersHandler] we don't own this buyer, exiting");
			return main();
		}

		final List<Address> addresses = account.getAddresses();
		final List<Address> buyerAddresses = buyer.getAddresses();

		FrontendDoc doc = new FrontendDoc(context, "Buyers");
		doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		doc.addTrail(BuyersHandler.class, null, captions.getBuyersCaptionPlural());
		doc.addTrail(BuyersHandler.class, "edit", "Edit buyer", "buyer", buyer);
		doc.addBody(new Body() {

			private void addresses() {

				sb.append(new TableTag("form").setCaption("Address"));

				if (addresses.isEmpty()) {

					sb.append("<tr><td>There are no addresses created on your account yet.<br/>" +
							new LinkTag(AddressesHandler.class, "create", "Click here") + " if you want to add an address.</td></tr>");

				} else {

					sb.append("<tr>");
					sb.append("<th width='30'>Authorised</th>");
					sb.append("<th>Address</hd>");
					sb.append("</tr>");

					for (Address address : addresses) {

						sb.append("<tr>");
						sb.append("<td width='30'>" + new CheckTag(context, "addresses", address, buyerAddresses.contains(address)) + "</td>");
						sb.append("<td>" + address.getLabel(", ", false) + "</td>");
						sb.append("</tr>");
					}

					sb.append("<tr><td>" + new SubmitTag("Update") + "</td></tr>");

					sb.append("<tr><td colspan='2'>" + new LinkTag(AddressesHandler.class, "create", "Click here") +
							" if you want to add another address.</td></tr>");
				}

				sb.append("</table>");
			}

			private void details() {

				sb.append(new TableTag("form").setCaption("Buyer"));

				sb.append(new FormRow("Name", new TextTag(context, "name", buyer.getName(), 40) + " " + new ErrorTag(context, "name", "<br/>")));
				sb.append(new FormRow("Email", new TextTag(context, "em", buyer.getEmail(), 40) + " " + new ErrorTag(context, "em", "<br/>")));
				sb.append(new FormRow("Password", "Enter a new password if you want to change the password for this buyer", new PasswordTag(context,
						"pwd", 12)));
				sb.append(new FormRow(null, null, new SubmitTag("Update")));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new MessagesTag(context));

				sb.append(new FormTag(BuyersHandler.class, "save", "post"));
				sb.append(new HiddenTag("buyer", buyer));

				details();
				addresses();

				sb.append("</form>");

				return sb.toString();
			}
		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		if (account == null) {
			return new LoginHandler(context, new Link(BuyersHandler.class)).main();
		}

		final List<Item> buyers = account.getBuyerConfig().getBuyers();

		FrontendDoc doc = new FrontendDoc(context, "Buyers");
		doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		doc.addTrail(BuyersHandler.class, null, captions.getBuyersCaptionPlural());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new MessagesTag(context));
				sb.append(new TableTag("form").setCaption("Buyers"));

				if (buyers.isEmpty()) {

					sb.append("<tr><td colspan='3'>You do not have any buyers setup yet</td></tr>");

				} else {

					sb.append("<tr>");
					sb.append("<td>Name</td>");
					sb.append("<td>Email</td>");
					sb.append("<td width='80'></td>");
					sb.append("</tr>");

					for (Item buyer : buyers) {

						sb.append("<tr>");
						sb.append("<td>" + new LinkTag(BuyersHandler.class, "edit", buyer.getName(), "buyer", buyer) +
								"<br/>(Click name to edit)</td>");
						sb.append("<td>" + (buyer.hasEmail() ? buyer.getEmail() : "n/a") + "</td>");
						sb.append("<td width='80'>" + new LinkTag(BuyersHandler.class, "delete", "Delete", "buyer", buyer) + "</td>");
						sb.append("</tr>");

					}
				}
				sb.append(new FormTag(BuyersHandler.class, "create", "post"));
				sb.append("<tr><td colspan='4'>Enter new buyer name: " + new TextTag(context, "name", 30) + " " + new SubmitTag("Add") + "</td></tr>");
				sb.append("</form>");
				sb.append("</table>");

				return sb.toString();
			}
		});
		return doc;
	}

	public Object save() throws ServletException {

		if (account == null) {
			return new LoginHandler(context, new Link(BuyersHandler.class)).main();
		}

		if (buyer == null) {
			return main();
		}

		if (!buyer.getBuyerFor().equals(account)) {
			logger.fine("[BuyersHandler] we don't own this buyer, exiting");
			return main();
		}

		test(new RequiredValidator(), "em");
		test(new EmailValidator(), "em");
		test(new RequiredValidator(), "em");

		// check email is unique
		if (em != null) {
			boolean unique = Item.isUniqueEmail(context, em, buyer);
			if (false == unique) {
				setError("em", "This email is already in use, please choose another.");
			}
		}

		if (hasErrors()) {
			return edit();
		}

		buyer.setName(name);
		buyer.setEmail(em);
		buyer.save();

		if (pwd != null) {
			buyer.setPassword(pwd);
		}

		BuyerConfig buyerConfig = buyer.getBuyerConfig();
		buyerConfig.removeAddresses();

		for (Address address : addresses) {

			// ensure this account can assign this address
			if (buyer.getBuyerFor().equals(address.getAccount())) {
				buyerConfig.addAddress(address);
			}
		}

		clearParameters();
		addMessage("This buyer has been updated");
		return edit();
	}

}
