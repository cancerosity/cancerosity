package org.sevensoft.ecreator.iface.frontend.account.subscription;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.accounts.subscriptions.Subscription;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * 
 * 
 * @author sks 30-Jan-2006 09:42:36
 * 
 * This handler will check for subscriptions and if our account requires one will forward to the subscription handler
 *
 */
public abstract class SubscriptionStatusHandler extends FrontendHandler {

	public SubscriptionStatusHandler(RequestContext context) {
		super(context);

	}

	@Override
	protected Object init() throws ServletException {

		Object obj = super.init();
		if (obj != null) {
			return obj;
		}

		if (account == null) {
			logger.fine("[SubscriptonStatusHandler] no account");
			return null;
		}

		if (!Module.Subscriptions.enabled(context)) {
			logger.fine("[SubscriptonStatusHandler] subscription module disabled");
			return null;
		}

		/*
		 * If no subscriptions available then do not check for subscription status
		 */
		if (!account.hasVisibleSubscriptionLevels()) {
			logger.fine("[SubscriptonStatusHandler] no visible subscription levels");
			return null;
		}

		Subscription subscription = account.getSubscription();
		logger.fine("[SubscriptionStatusHandler] subscription=" + subscription);

		/*
		 * If this member does not have a subscription or his subscription has expired
		 * then we should show the subscriptions handler
		 */
		if (subscription.isSubscriptionExpired()) {
			logger.fine("[SubscriptonStatusHandler] subscription expired or not set");

			if (account.getItemType().getSubscriptionModule().isForceSubscription()) {
				return new SubscriptionHandler(context).main();
			}
		}

		return null;
	}
}
