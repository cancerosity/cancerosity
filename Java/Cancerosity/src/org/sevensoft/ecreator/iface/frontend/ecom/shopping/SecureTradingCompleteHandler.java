package org.sevensoft.ecreator.iface.frontend.ecom.shopping;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/*
 * @author Stephen K Samuel samspade79@gmail.com 26 Nov 2008 12:53:59
 */
@Path("stcomplete.do")
public class SecureTradingCompleteHandler extends FrontendHandler {

	private String	streference;
	private String	orderinfo;

	public SecureTradingCompleteHandler(RequestContext context) {
		super(context);
	}

	public Object main() throws ServletException {
        context.setAttribute("stcomplete", true);

		FrontendDoc doc = new FrontendDoc(context, "Checkout - Order completed");
		doc.addTrail(BasketHandler.class, shoppingSettings.getBasketCaption());
		doc.addTrail(CheckoutHandler.class, "completed", "Checkout - Order completed");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("Thank you for ordering with Salcura.<br/><br/>");

				sb.append("Your payment reference is <strong>" + streference + "</strong>.<br/><br/>");

				sb.append("<strong>Order details:</strong><br/>");
				sb.append("<strong>" + orderinfo + "</strong>");

				return sb.toString();
			}
		});

		return doc;
	}
}
