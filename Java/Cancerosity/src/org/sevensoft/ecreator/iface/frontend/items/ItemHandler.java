package org.sevensoft.ecreator.iface.frontend.items;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.marketing.SmsHandler;
import org.sevensoft.ecreator.iface.frontend.account.RestrictionHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionStatusHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.shopping.panels.OrderingForm;
import org.sevensoft.ecreator.iface.frontend.items.listings.panels.ListingControlPanel;
import org.sevensoft.ecreator.iface.frontend.items.listings.panels.ListingModerationPanel;
import org.sevensoft.ecreator.iface.frontend.items.listings.panels.ListingVerificationPanel;
import org.sevensoft.ecreator.model.accounts.permissions.DomainUtil;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.crm.forum.renderers.PostsRenderer;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.interaction.sms.SmsProfileView;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemViews;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.ecreator.model.marketing.sms.SmsNumberException;
import org.sevensoft.ecreator.model.marketing.sms.SmsSenderException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.*;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.ecreator.model.stats.StatsCenter;
import org.sevensoft.ecreator.model.stats.views.items.ItemView;
import org.sevensoft.ecreator.model.stats.visits.Visitor;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.comments.blocks.CommentPostBlock;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.SimpleDoc;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author sks 25-May-2004 15:30:26
 */
@Path({"item.do"})
public class ItemHandler extends SubscriptionStatusHandler {

    private String content;
    private Item item;
    private boolean printer;
    private transient Seo seo;
    private String p;
    private boolean e;
    private Category trailCategory;
    private String path;

    public ItemHandler(RequestContext context) {
        this(context, null);
    }

    public ItemHandler(RequestContext context, Item item) {
        super(context);
        this.item = item;
        this.seo = Seo.getInstance(context);
    }

    private Object blocked() {

        FrontendDoc doc = new FrontendDoc(context, item.getDisplayName());
        doc.addTrail(item.getUrl(), item.getDisplayName());
        doc.addBody("This user has blocked you from viewing his/her profile.");
        return doc;
    }

    /**
     * @return
     */
    private boolean checkListingEdit() throws ServletException {

        /*
           * if we have a password supplied then check it is the appropriate password for the account on this item
           */
        if (p != null) {

            logger.fine("[ItemHandler] password supplied=" + p);

            if (item.hasAccount()) {

                if (item.getAccount().isPasswordMatch(p)) {

                    logger.fine("[ItemHandler] password match");
                    return true;

                } else {

                    logger.fine("[ItemHandler] passwords did not match");

                }

            } else {

                logger.fine("[ItemHandler] item has no account, password irrevelant");

            }
        }

        return false;
    }

    /**
     * @return
     */
    private Object checkPermission() throws ServletException {

        /*
           * Check for access on the categories of this item
           */
        if (Module.Permissions.enabled(context)) {

            logger.fine("[ItemHandler] permissions module enabled - checking " + item);

            if (!PermissionType.ItemFull.check(context, account, item.getItemType())) {
                logger.fine("[ItemHandler] ItemFull permission denied");
                if (item != null && item.getItemType().getRestrictionForwardUrl() != null) {
                    return new ExternalRedirect(item.getItemType().getRestrictionForwardUrl());
                } else {
                    return new RestrictionHandler(context).main();
                }
            }

            /*
                * Check category permissions but only if we are in at least one category
                */
            if (item.hasCategories()) {

                List<Category> categories = item.getCategories();
                if (categories.size() > 0) {

                    // as long as we have permission for one of the categories, then continue
                    boolean categoryPermission = false;
                    for (Category c : categories) {
                        if (PermissionType.CategoryItems.check(context, account, c)) {
                            logger.fine("[ItemHandler] CategoryItems permission allowed for cat=" + c);
                            categoryPermission = true;
                            break;
                        }
                    }

                    if (false == categoryPermission) {
                        logger.fine("[ItemHandler] no CategoryItems permission for any cat");
                        return new RestrictionHandler(context).main();
                    }

                }
            }
        }

        logger.fine("[ItemHandler] permissions ok - accepting " + item);

        return null;
    }

    private FrontendDoc createDoc() {

        // create page
        FrontendDoc doc = new FrontendDoc(context, item.getDisplayName());

        // add trail                                               todo add trail
        Category category;
        if (trailCategory == null) {
            category = item.getCategory();
        } else {
            category = trailCategory;
            context.setAttribute("trailCategory", category);
        }
        if (category != null) {
            for (Category cat : category.getTrail()) {
                doc.addTrail(cat.getUrl(), cat.getName());
            }
        }
        doc.addTrail(item.getUrl(), item.getDisplayName());
        doc.addBody(new MessagesTag(context));
        return doc;
    }

    public Object ignore() throws ServletException {

        if (account == null || item == null) {
            return main();
        }

        account.getInteraction().block(item);

        addMessage("This member is now on ignore - they cannot view your profile or contact you.");
        return main();
    }

    private boolean isDailyProfileViewsExceeded() {

        int dailyProfileViewsLimit = DomainUtil.getDailyProfileViewsLimit(context, account);
        if (dailyProfileViewsLimit > 0) {

            logger.fine("[ItemHandler] limiting dailyProfileViews to=" + dailyProfileViewsLimit);

            ItemViews views = ItemViews.get(context);
            if (views == null) {

                logger.fine("[ItemHandler] no views yet");
                new ItemViews(context, context.getSessionId());

            } else {

                logger.fine("[ItemHandler] current views=" + views.getViews());

                if (views.getViews() >= dailyProfileViewsLimit) {

                    return true;

                } else {

                    views.inc();

                }
            }
        }

        return false;
    }

    @Override
    public Object main() throws ServletException {

        if (item == null || item.getStatus() == null || item.getStatus().equals("DELETED")) {
            logger.fine("[ItemHandler] no item found");
            return HttpServletResponse.SC_NOT_FOUND;
        }

        if (item.getStatus().equals("DISABLED")) {
            if (!Module.ListingEdit.enabled(context) || item.getAccount() == null || !item.getAccount().equals(account)) {
                logger.fine("[ItemHandler] no item found");
                return HttpServletResponse.SC_NOT_FOUND;
            }
        }

        if (path != null && !path.equals(item.getFriendlyUrl())) {
            logger.fine("[ItemHandler] no friendlyUrl found");
            return HttpServletResponse.SC_NOT_FOUND;
        }

        context.setAttribute("pagelink", new Link(ItemHandler.class, null, "item", item));

        if (!item.hasItemType() || item.getItemType().isHidden()) {
            logger.fine("[ItemHandler] item is hidden");
            return HttpServletResponse.SC_FORBIDDEN;
        }

        /*
           * Check our profile views are not exceeded
           */
        if (isDailyProfileViewsExceeded()) {
            return new RestrictionHandler(context).main();
        }

        Object obj = checkPermission();
        if (obj != null) {
            logger.fine("[ItemHandler] permission forward=" + obj);
            return obj;
        }

        if (!Module.StatsBlock.enabled(context)) {
            new ItemView(context, item);
        }

        // send SMS if applicable
        if (config.getUrl().contains("lookme-up")) {
            try {
                new SmsProfileView(context, item, account);
            } catch (SmsGatewayException e) {
                e.printStackTrace();
            } catch (SmsMessageException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SmsGatewayAccountException e) {
                e.printStackTrace();
            } catch (SmsGatewayMessagingException e) {
                e.printStackTrace();
            } catch (SmsCreditsException e) {
                e.printStackTrace();
            } catch (SmsNumberException e) {
                e.printStackTrace();
            } catch (SmsSenderException e) {
                e.printStackTrace();
            }
        }

        FrontendDoc doc = createDoc();

        /*
           * otherwise if this item is awaiting moderation then show the moderation panel
           */
        if (item.isAwaitingValidation()) {
            doc.addBody(new ListingVerificationPanel(context, item));
            return doc;
        }

        /*
           * If this item is awaiting verification then show the verification panel
           */
        if (item.isAwaitingModeration()) {
            if (!Module.ListingEdit.enabled(context) || item.getAccount() == null || !item.getAccount().equals(account)) {
                doc.addBody(new ListingModerationPanel(context, item));
                return doc;
            }
        }

        /*
           * If we have come here to edit this listing, then show listing control panel
           */
        if (checkListingEdit()) {
            doc.addBody(new ListingControlPanel(context, item, p));
        }

        /*
           * Check item is live.
           *  Module.ListingEdit allow to edit Disabled listings
           */
        if (!item.isLive() && !Module.ListingEdit.enabled(context)) {
            logger.fine("[ItemHandler] item is not live, exiting");
            return new CategoryHandler(context).main();
        }

        /*
           * Check if we are blocking the logged in member
           */
        if (item.getInteraction().isBlocking(account)) {
            logger.fine("[ItemHandler] this profile is blocking the logged in member");
            return blocked();
        }

        // set item attribute
        setAttribute("item", item);

        // make a link to this attribute
        setAttribute("link", new Link(ItemHandler.class, null, "item", item));

        // meta data
        meta();

        // add in header
        if (item.getItemType().hasHeader()) {
            doc.addBody(item.getItemType().getHeader());
        }

        // if we are logged in lets add a view
        if (account != null) {
            item.getInteraction().view(account);
        }

        // if we are blocking this member lets display a message
        if (account != null && account.getInteraction().isBlocking(account)) {
            doc.addBody("<div class='ec_blockmsg'>You are blocking this member</div>");
        }

        doc.addBody(new OrderingForm(context));

        MarkupRenderer r = new MarkupRenderer(context, item.getItemType().getViewMarkup());
        r.setBody(item);
        doc.addBody(r);

        if (item.isInFamily()) {
            ItemSearcher searcher = new ItemSearcher(context);
            searcher.setStatus("Live");
            searcher.setVisibleOnly(true);
            searcher.setAttributeValues(item.getFamily());

            Attribute sortAttribute = item.getAttribute("Output Voltage 1");
            if (sortAttribute != null) {
                searcher.setSortType(SortType.Attribute);
                searcher.setSortAttribute(sortAttribute);
            }
            
            List<Item> items = searcher.getItems();
            items.remove(item);
            doc.addBody("<table border='2px';>");
            MarkupRenderer familyHeaderRenderer = new MarkupRenderer(context, item.getItemType().getFamilyHeaderMarkup());
            familyHeaderRenderer.setBody(item);
            doc.addBody(familyHeaderRenderer);

            MarkupRenderer familyRenderer = new MarkupRenderer(context, item.getItemType().getFamilyMarkup());
            familyRenderer.setBodyObjects(items);
            doc.addBody(familyRenderer);
            doc.addBody("</table>");
        }

        doc.addBody("</form>");

        List<Block> blocks = item.getItemType().getBlocks();

        if (Block.contains(CommentPostBlock.class, blocks) && !item.isComments()) {
            blocks.remove(Block.get(CommentPostBlock.class, blocks));
        }
        //if user os logged CommentPostBlock should have account        //todo setAccount
        if (Block.contains(CommentPostBlock.class, blocks)) {
            CommentPostBlock block = (CommentPostBlock) Block.get(CommentPostBlock.class, blocks);
            block.setAccount(account);
            block.setOwnerItem(item);
        }
        
        /*
           * Render blocks from the item type
           */
        for (Block block : blocks) {
            logger.fine("[ItemHandler] rendering block=" + block);
            doc.addBody(block.render(context));
        }

        /*
           * Render blocks from the item
           */
        blocks = item.getBlocks();
        for (Block block : blocks) {
            logger.fine("[ItemHandler] rendering block=" + block);
            doc.addBody(block.render(context));
        }

        // messages
        if (item.getItemType().isMessages()) {
            doc.addBody(new PostsRenderer(context, item.getTopic(true), 0));
        }

        if (item.getItemType().getName().equals("Listing") && Module.ListingSms.enabled(context)) {
            doc.addBody(new FormTag(SmsHandler.class, "send", "POST"));
            doc.addBody(new TableTag("sendSms", "left"));
            doc.addBody("<tr><td colspan='2'>Name: </td>");
            doc.addBody("<td colspan='2'>" + new TextTag(context, "name") + "</td></tr>");
            doc.addBody("<tr><td colspan='2'>Phone: </td>");
            doc.addBody("<td colspan='2'>" + new TextTag(context, "phone") + "</td></tr>");
            doc.addBody("<tr><td colspan='2'>Mobile: </td>");
            doc.addBody("<td colspan='2'>" + new TextTag(context, "mobile") + "</td></tr>");
            doc.addBody("<tr><td colspan='2'>E-Mail: </td>");
            doc.addBody("<td colspan='2'>" + new TextTag(context, "email") + "</td></tr>");
            doc.addBody("<tr><td colspan='2'>Comments: </td>");
            doc.addBody("<td colspan='2'>" + new TextTag(context, "comments") + "</td></tr>");
            doc.addBody("<tr><td colspan='2'>" + new HiddenTag("url", item.getUrl()) + "</td></tr>");
            doc.addBody("<tr><td colspan='2'>" + new HiddenTag("toPhone", item.getAttributeValue("Daytime Phone")) + "</td></tr>");
            doc.addBody("<tr><td colspan='2'>" + new SubmitTag("submitSendSms", "Send SMS") + "</td></tr>");
            doc.addBody("</table>");
            doc.addBody("</form>");
        }

        // add in footer
        if (item.getItemType().hasFooter()) {
            doc.addBody(item.getItemType().getFooter());
        }

        return doc;
    }

    private void meta() {

        setAttribute("title_tag", seo.getTitleTagRendered(item));
        setAttribute("description_tag", seo.getDescriptionTagRendered(item));
        setAttribute("keywords", seo.getKeywordsRendered(item));

        item.lockFriendlyUrl();
    }

    /**
     * Printer friendly page
     */
    public Object printer() throws ServletException {

        if (item == null) {
            logger.fine("[ItemHandler] no item found");
            return index();
        }

        // set printer mode
        context.setAttribute("printer", true);

        SimpleDoc doc = new SimpleDoc(context);
        doc.addHead("<meta name='robots' content='noindex' />");

        MarkupRenderer r = new MarkupRenderer(context, item.getItemType().getPrinterMarkup());
        r.setBody(item);
        doc.addBody(r);

        return doc;
    }

    @Override
    protected void stats() {

        if (item != null) {

            new StatsCenter(context).run(item);
            Visitor.process(context, item.getName());

            /*
            if (item.isEvent() || item.getItemType().getName().equals("Listing") || item.getListing() != null) {
                if (account == null) {
                    new StatsCenter(context).runListingStats(item);
                } else {
                    new StatsCenter(context, account, item).runListingStats(item);
                }
            }
            */

        }

    }

}