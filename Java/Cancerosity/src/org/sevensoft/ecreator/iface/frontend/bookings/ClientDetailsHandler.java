package org.sevensoft.ecreator.iface.frontend.bookings;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.bookings.Booking;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

import javax.servlet.ServletException;

/**
 * User: MeleshkoDN
 * Date: 11.10.2007
 * Time: 13:20:19
 */
@Path("bookings-client-details.do")
public class ClientDetailsHandler extends FrontendHandler {

    private Booking booking;

    public ClientDetailsHandler(RequestContext context) {
        super(context);
    }

    public Object main() throws ServletException {
        FrontendDoc doc = new FrontendDoc(context, "Client Details");
        doc.addBody(new Body() {

            @Override
            public String toString() {
                Item account = booking.getAccount();
                Address address = booking.getAddress();
                sb.append(new AdminTable("Client Details"));
                sb.append(new AdminRow("Name", account.getName()));
                sb.append(new AdminRow("Email", account.getEmail()));
                if (address == null) {
                    sb.append(new AdminRow("Address", "(empty)"));
                } else {
                    sb.append(new AdminRow("Country", address.getCountry()));
                    sb.append(new AdminRow("City/Town", address.getTown()));
                    sb.append(new AdminRow("County", address.getCounty()));
                    sb.append(new AdminRow("Postcode", address.getPostcode()));
                    sb.append(new AdminRow("Address", address.getAddressLine1()));
                    if (address.hasAddressLine2()) {
                        sb.append(new AdminRow("Address 2", address.getAddressLine2()));
                    }
                    if (address.hasAddressLine3()) {
                        sb.append(new AdminRow("Address 3", address.getAddressLine3()));
                    }
                    sb.append(new AdminRow("Phone number", address.getTelephone()));
                    if (address.getTelephone2() != null) {
                        sb.append(new AdminRow("Phone number 2", address.getTelephone2()));
                    }
                    if (address.getTelephone3() != null) {
                        sb.append(new AdminRow("Phone number 3", address.getTelephone3()));
                    }
                }
                sb.append("</table>");
                return sb.toString();
            }
        });
        return doc;
    }
}
