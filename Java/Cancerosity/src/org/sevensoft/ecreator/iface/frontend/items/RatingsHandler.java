package org.sevensoft.ecreator.iface.frontend.items;

import java.util.Map;

import javax.servlet.ServletException;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.extras.ratings.RatingModule;
import org.sevensoft.ecreator.model.extras.ratings.RatingType;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 11 Aug 2006 17:16:52
 *
 */
@Path("ratings.do")
public class RatingsHandler extends FrontendHandler {

	private Item					item;
	private Map<Integer, Integer>			ratings;
	private String					content;
	private MultiValueMap<Attribute, String>	attributeValues;

	public RatingsHandler(RequestContext context) {
		super(context);
	}

	public Object add() throws ServletException {

		if (item == null)
			return new CategoryHandler(context).main();

		// 	 we must be logged in
		if (account == null) {
			return new LoginHandler(context, new Link(RatingsHandler.class, null, "item", item)).main();
		}

        if (item.equals(account)) {
            addError("You are not allowed to rate yourself");
            return new ItemHandler(context, item).main();
        }

		RatingModule module = item.getItemType().getRatingModule();

		for (RatingType ratingType : module.getRatingTypes()) {
			item.addRating(account, ratingType, ratings.get(ratingType));
		}

		//		if (content != null) {
		//			item.addReview(account, content, attributeValues);
		//		}

		FrontendDoc page = new FrontendDoc(context, "Add your rating");
		page.addTrail(item.getUrl(), item.getName());
		page.addTrail(RatingsHandler.class, "Add your rating");
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("Thank you for submitting your rating, this will be updated immediately.<br/><br/>");
				sb.append(new LinkTag(item.getUrl(), "Click here"));
				sb.append(" to return to the ");
				sb.append(item.getItemType().getName());
				sb.append(".");
				return sb.toString();
			}

		});
		return page;
	}

	@Override
	public Object main() throws ServletException {

		if (item == null)
			return new CategoryHandler(context).main();

		// we must be logged in
		if (account == null) {
			return new LoginHandler(context, new Link(RatingsHandler.class, null, "item", item)).main();
		}

        if (item.equals(account)) {
            addError("You are not allowed to rate yourself");
            return new ItemHandler(context, item).main();
        }

		final RatingModule module = item.getItemType().getRatingModule();

		FrontendDoc doc = new FrontendDoc(context, "Add your rating");
		doc.addTrail(item.getUrl(), item.getName());
		doc.addTrail(RatingsHandler.class, "Add your rating");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("ratings-add"));
				sb.append(new FormTag(RatingsHandler.class, "add", "post"));
				sb.append(new HiddenTag("item", item));

				for (RatingType ratingType : module.getRatingTypes()) {

					sb.append("<tr>");
					sb.append("<td>" + ratingType.getName() + "</td>");

					SelectTag tag = new SelectTag(context, "ratings_" + ratingType.getId());
					for (int n = 1; n <= module.getRatingScale(); n++)
						tag.addOption(n);

					sb.append("<td>" + tag + "</td>");
					sb.append("</tr>");
				}

				//				if (item.getItemType().getRatingModule().isReviews()) {
				//
				//					sb.append("<tr><td colspan='2'>Enter your review below</td></tr>");
				//					sb.append("<tr><td colspan='2'>" + new TextAreaTag(context, "content", 50, 8) + "</td></tr>");
				//				}

				sb.append("<tr><td colspan='2'>" + new SubmitTag("Add ratings") + "</td></tr>");

				sb.append("</form>");
				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}
}
