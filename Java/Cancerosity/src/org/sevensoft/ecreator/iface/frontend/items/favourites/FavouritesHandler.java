package org.sevensoft.ecreator.iface.frontend.items.favourites;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionStatusHandler;
import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.favourites.Favourite;
import org.sevensoft.ecreator.model.items.favourites.FavouritesGroup;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 25-May-2004 15:30:26
 */
//@Path( { "favs.do", "favourites.do", "members-favourites.do" })
public class FavouritesHandler extends SubscriptionStatusHandler {

	private Item		item;
	private FavouritesGroup	favouritesGroup;
	private Favourite		favourite;
	private String		name;
	private boolean		renamed;

	public FavouritesHandler(RequestContext context) {
		super(context);
	}

	public Object add() throws ServletException {

		if (favouritesGroup == null) {
			logger.fine("[FavouritesHandler] favourites group is null");
			return index();
		}

		if (item == null) {
			logger.fine("[FavouritesHandler] no item");
			return main();
		}

		if (account == null) {
			logger.fine("[FavouritesHandler] not logged in");
			return new LoginHandler(context, new Link(FavouritesHandler.class, "add", "item", item, "favouritesGroup", favouritesGroup)).main();
		}

//		if (account.hasFavourite(item)) {
        if (favouritesGroup.contains(account, item)) {
			addError("This " + item.getItemTypeNameLower() + " is already in  " + favouritesGroup.getName());
			return new ItemHandler(context, item).main();
		}

		// if we can rename this favourite then offer the chance now to rename
		if (favouritesGroup.isRename() && !renamed) {

			FrontendDoc doc = new FrontendDoc(context, favouritesGroup.getName());
            if (account.getItemType().getAccountModule().isShowAccount()) {
                doc.addTrail(AccountHandler.class, captions.getAccountCaption());
            }
			doc.addTrail(FavouritesHandler.class, null, favouritesGroup.getName(), "favouritesGroup", favouritesGroup);
			doc.addBody(new Body() {

				@Override
				public String toString() {

					sb.append("If you want to rename this " + item.getItemType().getNameLower()
							+ " you can do so by changing the name in the box below.<br/><br/>When you are ready just click submit to continue.");

					sb.append(new FormTag(FavouritesHandler.class, "add", "post"));
					sb.append(new HiddenTag("renamed", true));
					sb.append(new HiddenTag("favouritesGroup", favouritesGroup));
					sb.append(new HiddenTag("item", item));

					sb.append(new TextTag(context, "name", item.getName(), 60));
					sb.append("<br/>");
					sb.append(new SubmitTag("Submit"));

					sb.append("</form>");

					return sb.toString();
				}

			});
			return doc;
		}

		Favourite favourite = account.addFavourite(favouritesGroup, item);
		if (name != null) {
			favourite.setName(name);
			favourite.save();
		}

		addMessage("This " + item.getItemTypeNameLower() + " has been added to your " + favouritesGroup.getName());
		return new ItemHandler(context, item).main();
	}

	@Override
	public Object main() throws ServletException {

		if (account == null) {
			return new LoginHandler(context, new Link(FavouritesHandler.class)).main();
		}

		if (favouritesGroup == null) {
			return new AccountHandler(context).main();
		}

		final List<Favourite> favourites = account.getFavourites(favouritesGroup);

		FrontendDoc doc = new FrontendDoc(context, favouritesGroup.getName());
		doc.addTrail(FavouritesHandler.class, null, favouritesGroup.getName(), "favouritesGroup", favouritesGroup);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				if (favouritesGroup.hasHeader()) {
					sb.append(favouritesGroup.getHeader());
				}

				Markup markup = favouritesGroup.getMarkup();
				if (markup == null) {

					sb.append(new FavouritesRenderer(context, favouritesGroup, favourites));

				} else {

					// the markup requires the item context, not favourite
					List<Item> items = new ArrayList();
					for (Favourite favourite : favourites) {
						items.add(favourite.getItem());
					}
					MarkupRenderer r = new MarkupRenderer(context, markup);
					r.setBodyObjects(items);
					sb.append(r);
				}

				if (favouritesGroup.hasFooter()) {
					sb.append(favouritesGroup.getFooter());
				}

				return sb.toString();
			}

		});
		return doc;
	}

	public Object remove() throws ServletException {

		if (account == null)
			return new LoginHandler(context, new Link(FavouritesHandler.class)).main();

		if (favourite != null) {

			favouritesGroup = favourite.getFavouritesGroup();
			account.removeFavourite(favourite);

		}

		if (item != null && favouritesGroup != null) {
			favouritesGroup.removeFavourite(account, item);
		}

        if (item != null) {
            addMessage("This " + item.getItemTypeNameLower() + " has been removed from your " + favouritesGroup.getName());
            return new ItemHandler(context, item).main();
        }

		return main();
	}

	public Object rename() throws ServletException {

		if (account == null) {
			return new LoginHandler(context, new Link(FavouritesHandler.class)).main();
		}

		if (favourite == null || name == null)
			return main();

		favourite.setName(name);
		favourite.save();

		favouritesGroup = favourite.getFavouritesGroup();
		return main();
	}
}
