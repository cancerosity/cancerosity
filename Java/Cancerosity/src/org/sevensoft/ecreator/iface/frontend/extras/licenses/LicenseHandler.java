package org.sevensoft.ecreator.iface.frontend.extras.licenses;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.ErrorDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.extras.sitemap.SiteMapHandler;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.extras.licenses.License;
import org.sevensoft.ecreator.model.extras.licenses.LicenseException;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 27 Mar 2007 13:59:02
 *
 */
@Path("license.do")
public class LicenseHandler extends FrontendHandler {

	private static byte[]	iv	= { 0x0a, 0x01, 0x02, 0x03, 0x04, 0x0b, 0x0c, 0x0d };
	private String		inputKey;
	private Order		order;

	public LicenseHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		FrontendDoc doc = new FrontendDoc(context, "Software license");
		doc.addTrail(SiteMapHandler.class, "Software license");
		doc.addBody(new MessagesTag(context));
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(LicenseHandler.class, "license", "post"));
				sb.append(new TableTag("ecLicense"));
				sb.append("<tr><td>Enter your order number</td><td>" + new TextTag(context, "order", 20) + "</td></tr>");
				sb.append("<tr><td>Enter your application key</td><td>" + new TextTag(context, "inputKey", 120) + "</td></tr>");
				sb.append("<tr><td colspan='2'>" + new SubmitTag("Get license key") + "</td></tr>");
				sb.append("</table>");
				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object license() throws ServletException {

		// check order exists
		if (order == null) {
			addError("Invalid order id");
			return main();
		}

		final License key;
		try {

			key = new License(context, null, null);

		} catch (LicenseException e) {
			addError(e);
			return main();

		} catch (Exception e) {
			e.printStackTrace();
			return new ErrorDoc(context, e.toString());
		}

		FrontendDoc doc = new FrontendDoc(context, "Software license");
		doc.addTrail(SiteMapHandler.class, "Software license");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("Your license number is: " + key.getEncrypted());
				return sb.toString();
			}

		});
		return doc;
	}
}
