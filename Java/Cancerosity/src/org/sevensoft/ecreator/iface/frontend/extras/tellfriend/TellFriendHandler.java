package org.sevensoft.ecreator.iface.frontend.extras.tellfriend;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.net.EmailValidator;
import org.sevensoft.ecreator.iface.EcreatorHandler;
import org.sevensoft.ecreator.model.extras.tellfriend.TellFriendSettings;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.results.StringResult;
import org.sevensoft.jeezy.i18n.Messages;

/**
 * @author sks 17 Jul 2006 15:26:19
 *
 */
@Path("referafriend.do")
public class TellFriendHandler extends EcreatorHandler {

	private String						url;
	private String						name;
	private String						friendsEmail;
	private String						friendsName;

	private final transient TellFriendSettings	settings;

	public TellFriendHandler(RequestContext context) {
		super(context);
		this.settings = TellFriendSettings.getInstance(context);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		if (url == null)
			return HttpServletResponse.SC_BAD_REQUEST;

		StringBuilder sb = new StringBuilder();

		sb.append(new FormTag(TellFriendHandler.class, "send", "POST"));
		sb.append(new HiddenTag("url", url));

		sb.append("<style>");
		sb.append("table.referafriend { font-family: Trebuchet MS; font-size: 12px; border: 1px solid #bbbbbb; background: #f2f2f2; padding: 8px; }");
		sb.append("table.referafriend td { padding-bottom: 8px; } ");
		sb.append("div { font-family: Trebuchet MS; font-size: 16px; font-weight: bold; }");
		sb.append("span.error { color: #cc0000; font-weight: bold; }");
		sb.append("</style>");

		sb.append("<div>Tell a friend</div>");

		sb.append(new TableTag("ec_tellfriend"));
		sb.append("<tr><td>Your name:<br/>");
		sb.append(new TextTag(context, "name", 20));
		sb.append(new ErrorTag(context, "name", "<br/>"));
		sb.append("</td></tr>");

		sb.append("<tr><td>Your friend's name:<br/>");
		sb.append(new TextTag(context, "friendsName", 20));
		sb.append(new ErrorTag(context, "friendsName", "<br/>"));
		sb.append("</td></tr>");

		sb.append("<tr><td>Your friend's email:<br/>");
		sb.append(new TextTag(context, "friendsEmail", 40));
		sb.append(new ErrorTag(context, "friendsEmail", "<br/>"));
		sb.append("</td></tr>");

		sb.append("<tr><td align='right'>" + new SubmitTag("Submit") + "</td></tr>");
		sb.append("</table>");

		sb.append("</form>");

		return new StringResult(sb, "text/html");
	}

	@Override
	protected boolean runSecure() {
		return false;
	}

	public Object send() throws ServletException {

		test(new RequiredValidator(), "name");
		test(new RequiredValidator(), "friendsName");
		test(new RequiredValidator(), "friendsEmail");
		test(new EmailValidator(), "friendsEmail");

		if (hasErrors()) {
			return main();
		}

		try {

			settings.sendEmail(name, friendsEmail, friendsName, url);
            return new StringResult(Messages.get("frontend.extras.tellfriend.send"), "text/html");

		} catch (EmailAddressException e) {
			e.printStackTrace();
			return Messages.get("frontend.extras.tellfriend.send.error.email");

		} catch (SmtpServerException e) {
			e.printStackTrace();
			return Messages.get("frontend.extras.tellfriend.send.error.smtp");
		}

	}
}
