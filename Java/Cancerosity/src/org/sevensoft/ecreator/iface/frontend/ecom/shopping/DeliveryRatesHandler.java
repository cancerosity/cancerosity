package org.sevensoft.ecreator.iface.frontend.ecom.shopping;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sam
 */
@Path("delivery-rates.do")
public class DeliveryRatesHandler extends FrontendHandler {

	public DeliveryRatesHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() {

		final String title = "Delivery rates";
		FrontendDoc page = new FrontendDoc(context, title);
		page.addTrail(DeliveryRatesHandler.class, title);
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("<div class='title'>Delivery rates</div>");
				sb.append("Coming soon!");

				return sb.toString();
			}

		});
		return page;
	}

}