package org.sevensoft.ecreator.iface.frontend.ecom.credits;

import java.util.Set;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.account.AccountHandler;
import org.sevensoft.ecreator.iface.frontend.account.login.LoginHandler;
import org.sevensoft.ecreator.iface.frontend.account.subscription.SubscriptionStatusHandler;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.frontend.ecom.payments.panels.PaymentTable;
import org.sevensoft.ecreator.model.ecom.credits.CreditPackage;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 19-Aug-2005 07:10:43
 * 
 */
@Path( { "credits.do", "member-credits.do" })
public class CreditsHandler extends SubscriptionStatusHandler {

	private CreditPackage	creditPackage;
	private String		link;
	private PaymentType	paymentType;

	public CreditsHandler(RequestContext context) {
		super(context);
	}

	public CreditsHandler(RequestContext context, Link link) {
		super(context);
		this.link = link.toString();
	}

	public Object completed() throws ServletException {

		if (paymentType != null)
			paymentType.inlineCallback(context, getParameters(), getRemoteIp());

		FrontendDoc page = new FrontendDoc(context, "Credits added");
		page.addTrail(AccountHandler.class, captions.getAccountCaption());
		page.addTrail(CreditsHandler.class, null, "Credit balance");
		page.addTrail(CreditsHandler.class, null, "Credits added");
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("Your account has been updated with your credit purchase");
				return sb.toString();
			}
		});
		return page;
	}

	@Override
	public Object main() throws ServletException {

		if (!Module.Credits.enabled(context))
			return new CategoryHandler(context).main();

		if (account == null) {
			return new LoginHandler(context).main();
		}

		FrontendDoc doc = new FrontendDoc(context, "Credits");
		doc.addTrail(AccountHandler.class, captions.getAccountCaption());
		doc.addTrail(CreditsHandler.class, "Credits");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("form1").setCaption("Credit packages"));
				sb.append(new FormTag(CreditsHandler.class, "payment", "post"));
				sb.append(new HiddenTag("link", link));
				//
				//				for (CreditPackage creditPackage : active.getCreditPackages()) {
				//
				//					sb.append("<tr>");
				//					sb.append("<td>" + new RadioTag(context, "creditPackage", creditPackage, false) + "</td>");
				//					sb.append("<td>" + creditPackage.getCredits() + " download credits - \243" + creditPackage.getFee() + "</td>");
				//					sb.append("</tr>");
				//				}

				sb.append("<tr><td colspan='2' align='right'>" + new SubmitTag("Purchase credits") + " </td></tr>");

				sb.append("</form>");
				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object payment() throws ServletException {

		if (creditPackage == null) {
			return main();
		}

		final Set<PaymentType> paymentTypes = account.getItemType().getPaymentTypes();
		if (paymentTypes.isEmpty()) {
			return "No payment types configured";
		}

		FrontendDoc page = new FrontendDoc(context, "Purchase credits - Payment method");
		page.addTrail(AccountHandler.class, captions.getAccountCaption());
		page.addTrail(CreditsHandler.class, "Credits");
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new PaymentTable(context, paymentTypes, CreditsHandler.class, "setPaymentType"));
				return sb.toString();
			}

		});
		return page;

	}
}
