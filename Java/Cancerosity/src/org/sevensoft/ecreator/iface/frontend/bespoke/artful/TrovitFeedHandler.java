package org.sevensoft.ecreator.iface.frontend.bespoke.artful;

import java.util.List;

import javax.servlet.ServletException;

import org.jdom.Document;
import org.joda.time.DateMidnight;
import org.samspade79.feeds.trovit.TrovitFeed;
import org.samspade79.feeds.trovit.TrovitHome;
import org.samspade79.feeds.trovit.TrovitHome.Type;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.XmlResult;

/*
 * @author Stephen K Samuel samspade79@gmail.com 30 Jan 2009 16:34:57
 */
@Path("artful-trovit.do")
public class TrovitFeedHandler extends Handler {

	public TrovitFeedHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		TrovitFeed feed = new TrovitFeed();

		ItemSearcher searcher = new ItemSearcher(context);
		searcher.setItemType(3);
		searcher.setStatus("LIVE");
		List<Item> items = searcher.getItems();

		for (Item item : items) {

			TrovitHome home = new TrovitHome();
			home.setDate(new DateMidnight(item.getDateUpdated().getTimestamp()));
			feed.add(home);

			home.setTitle(item.getName());
			home.setId(item.getIdString());
			home.setUrl(item.getUrl());
			home.setContent(item.getContent());
            try {
                home.setRooms(Integer.parseInt(item.getAttributeValue("bedrooms")));
                home.setBathrooms(Integer.parseInt(item.getAttributeValue("bathrooms")));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            home.setAddress(item.getAttributeValue("address"));
			home.setAddress(item.getAttributeValue("postcode"));
			home.setArea(item.getAttributeValue("area"));
			home.setType(Type.ForRent);

		}

		Document doc = feed.buildDoc();
		return new XmlResult(doc);
	}

	@Override
	protected boolean runSecure() {
		return false;
	}

}
