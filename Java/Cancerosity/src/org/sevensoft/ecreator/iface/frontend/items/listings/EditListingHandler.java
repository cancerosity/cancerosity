package org.sevensoft.ecreator.iface.frontend.items.listings;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.ecreator.iface.frontend.items.listings.panels.ListingAddEditPanel;
import org.sevensoft.ecreator.iface.frontend.items.listings.panels.ListingAttachmentsPanel;
import org.sevensoft.ecreator.iface.frontend.items.listings.panels.ListingImagesPanel;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.test.AttributeValidator;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.listings.Listing;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 26 Dec 2006 12:05:58
 *
 */
@Path( { "el.do", "edit-listing.do" })
public class EditListingHandler extends FrontendHandler {

	private Item					item;
	private String					content;
	private MultiValueMap<Attribute, String>	attributeValues;
	private String					name;
	private String					p;
	private Amount					price;
	private int						stock;
	private String					outStockMsg;
	private Upload					imageUpload;
	private Img						image;
	private Attachment				attachment;
	private Map<Integer, Money>			deliveryRates;
	private Upload					attachmentUpload;
    private boolean editPreview;
    private boolean editImages;
    private boolean editAttachments;

	public EditListingHandler(RequestContext context) {
		super(context);
	}

	/**
	 * 
	 */
	private Object checkPriv() {

		if (!item.hasAccount()) {

			logger.fine("[EditListingHandler] item is not account listing");
			return HttpServletResponse.SC_BAD_REQUEST;
		}

		/*
		 * Ensure this item has a listing package
		 */
		if (!item.getListing().hasListingPackage()) {
			return HttpServletResponse.SC_EXPECTATION_FAILED;
		}

		/* 
		 * ensure we have the right to edit this listing
		 * 1. Check we are logged in as the owner
		 * or
		 * 2. We are supplying the account password
		 */
		if (account == null) {

			if (p == null) {

				logger.fine("[EditListingHandler] no password supplied");
				return HttpServletResponse.SC_BAD_REQUEST;
			}

			if (!item.getAccount().isPasswordMatch(p)) {

				logger.fine("[EditListingHandler] no password match");
				return HttpServletResponse.SC_FORBIDDEN;
			}

		} else {

			if (!account.equals(item.getAccount())) {

				logger.fine("[EditListingHandler] non matching account");
				return HttpServletResponse.SC_FORBIDDEN;
			}
		}

		return null;
	}

	public Object editAttachments() throws ServletException {

		if (item == null) {
			return index();
		}

		Object obj = checkPriv();
		if (obj != null) {
			return obj;
		}

        final FrontendDoc doc = new FrontendDoc(context, "Edit " + captions.getListingsCaption() + " attachments");
        doc.addTrail(EditListingHandler.class, "editAttachments", "Edit " + captions.getListingsCaption() + " attachments", "item", item, "p", p);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new MessagesTag(context));

				sb.append(new ListingAttachmentsPanel(context, item, p).setEditPreview(editPreview));

				sb.append("<div class='ec_input_commands'>");
                if (editPreview) {
                    sb.append(new ButtonTag(AddListingHandler.class, "preview", "Preview", "item", item));
                }
				sb.append(new ButtonTag(ItemHandler.class, null, "Return to " + item.getItemTypeNameLower(), "item", item, "p", p));
				sb.append("</div>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object editImages() throws ServletException {

		if (item == null) {
			return index();
		}

        if (!item.getListing().getListingPackage().isListingUpdate()) {
            return new MyListingsHandler(context).main();
        }

		Object obj = checkPriv();
		if (obj != null) {
			return obj;
		}

        final FrontendDoc doc = new FrontendDoc(context, "Edit " + captions.getListingsCaption() + " images");
        doc.addTrail(EditListingHandler.class, "editImages", "Edit " + captions.getListingsCaption() + " images", "item", item, "p", p);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new MessagesTag(context));

                sb.append(new ListingImagesPanel(context, item, p).setEditPreview(editPreview));

				sb.append("<div class='ec_input_commands'>");

                if (editPreview) {
                    if (item.getListing().getListingPackage().isAttachments()) {
                        sb.append(new ButtonTag(EditListingHandler.class, "editAttachments", "Continue", "item", item, "editPreview", editPreview));
                    }
                    sb.append(new ButtonTag(AddListingHandler.class, "preview", "Preview", "item", item));
                }

				sb.append(new ButtonTag(ItemHandler.class, null, "Return to " + item.getItemTypeNameLower(), "item", item, "p", p));
				sb.append("</div>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		if (item == null) {
			return index();
		}

        if (!item.getListing().getListingPackage().isListingUpdate()) {
            return new MyListingsHandler(context).main();
        }

		Object obj = checkPriv();
		if (obj != null) {
			return obj;
		}

        FrontendDoc doc = new FrontendDoc(context, "Edit " + captions.getListingsCaption());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new MessagesTag(context));

				sb.append(new FormTag(EditListingHandler.class, "save", "multi"));
				sb.append(new HiddenTag("item", item));
				sb.append(new HiddenTag("p", p));
                if (editPreview) {
                    sb.append(new HiddenTag("editPreview", editPreview));
                    if (item.getListing().getListingPackage().isImages()) {
                        sb.append(new HiddenTag("editImages", true));
                    } else if (item.getListing().getListingPackage().isAttachments()) {
                        sb.append(new HiddenTag("editAttachments", true));
                    }
                }

				sb.append(new ListingAddEditPanel(context, item, EditListingHandler.class, 1, 1));

				sb.append("<div class='ec_input_commands'>");
				sb.append(new SubmitTag("Update " + item.getItemTypeNameLower()));
				sb.append(new ButtonTag(ItemHandler.class, null, "Return to " + item.getItemTypeNameLower(), "item", item, "p", p));
				sb.append("</div>");

				sb.append("</form>");

				return sb.toString();
			}

		});

		return doc;
	}

	public Object removeAttachment() throws ServletException {

		if (item == null) {
			return index();
		}

		Object obj = checkPriv();
		if (obj != null) {
			return obj;
		}

		if (attachment != null) {

			item = attachment.getItem();
			if (item == null) {
				return new MyListingsHandler(context).main();
			}

			item.removeAttachment(attachment);
		}

		return editAttachments();
	}

	public Object removeImage() throws ServletException {

		if (item == null) {
			return index();
		}

		Object obj = checkPriv();
		if (obj != null) {
			return obj;
		}

		if (image != null) {

			item = image.getItem();
			if (item == null) {
				return new MyListingsHandler(context).main();
			}

			item.removeImage(image);
		}

		return editImages();
	}

	public Object save() throws ServletException {

        Object save = saveItem();
        if (save != null) {
            return save;
        }

        if (editImages){
            return editImages();
        } else if (editAttachments) {
            return editAttachments();
        }

        FrontendDoc doc = new FrontendDoc(context, captions.getListingsCaption() + " updated");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("<br/>");

                sb.append("Your " + captions.getListingsCaption() + " has been updated.<br/><br/>");

				sb.append(new LinkTag(ItemHandler.class, null, "Click here", "item", item, "p", p));
				sb.append(" to return to the " + item.getItemTypeNameLower());

				if (config.getUrl().contains("artfullodger.co.uk")) {
					sb.append("<br/><br/>");
					sb.append("To return to your properties " + new LinkTag(MyListingsHandler.class, null, "click here") + ".");
				}

				return sb.toString();
			}
		});
		return doc;
	}

    public Object saveItem() throws ServletException {
        if (item == null) {
            return index();
        }

        Object obj = checkPriv();
        if (obj != null) {
            return obj;
        }

        Listing listing = item.getListing();

        if (listing.getListingPackage().isNameChange()) {
            test(new RequiredValidator(), "name");
        }

        //if description is not disabled for all listings in listing package
        if (listing.getListingPackage().isAllowDescription()) {
            if (item.getItemType().isContent()) {
                if (content == null) {
                    setError("content", "You must enter a description");
                } else {
                    content = content.replaceAll("\r\n", "<br />");
                }
            }

            // if we have max characters then test for overrun
            if (listing.getListingPackage().hasMaxCharacters()) {
                test(new LengthValidator(0, listing.getListingPackage().getMaxCharacters()), "content");
            }
        }

        // test we have valid input for all the attributes on this page
		List<Attribute> attributes = item.getEditableAttributes();
		for (Attribute attribute : attributes) {
			test(new AttributeValidator(attribute), attribute.getParamName());
		}

		if (hasErrors()) {
			addError("There were errors with your information - please check and try again.");
			logger.fine("[AddListingHandler] errors with input errors=" + context.getErrors());
			return main();
		}

		if (listing.getListingPackage().isNameChange()) {

			item.setName(name);

			logger.fine("[EditListingHandler] setting name=" + name);
			item.log("Name changed by user to '" + name + "'");
		}

		if (listing.getListingPackage().isStock()) {

			switch (item.getItemType().getStockModule().getStockControl()) {

			case Boolean:
				item.setOurStock(stock);
				item.log("Stock changed by user to " + stock);
				break;

			case RealTime:
			case Manual:

				item.setOurStock(stock);
				item.log("Stock changed by user to " + stock);

				item.setOutStockMsg(outStockMsg);
				item.log("Out of stock messaged changed by user to: " + outStockMsg);
				break;

			case None:
			case String:

				item.setOutStockMsg(outStockMsg);
				item.log("Availability changed by user to: " + outStockMsg);

				break;
			}

			logger.fine("[EditListingHandler] setting stock=" + stock);

			item.log("Out of stock msg changed by user to " + outStockMsg);
		}

		// prices
		if (listing.getListingPackage().isPrices()) {

			item.setSellPrice(price);
			item.log("User changed sell price to " + price);

		}

		// prices
		if (listing.getListingPackage().isDeliveryRates()) {

			item.setDeliveryRates(deliveryRates);
			item.log("User changed delivery rates to=" + deliveryRates);

		}

		if (item.getItemType().isContent()) {
			item.setContent(content);
			item.log("User changed content to " + (content == null ? "nothing" : content.length() + " bytes"));
		}

		item.save();
		item.setEditableAttributeValues(attributeValues);

        return null;
    }

	public Object uploadAttachment() throws ServletException {

		if (item == null) {
			return index();
		}

		Object obj = checkPriv();
		if (obj != null) {
			return obj;
		}

		Listing listing = item.getListing();

		if (Module.ListingAttachments.enabled(context)) {

			if (listing.getListingPackage().isAttachments()) {

				if (attachmentUpload != null) {

					try {

						item.addAttachment(attachmentUpload, false);

					} catch (IOException e) {
						e.printStackTrace();
						addError(e);
						logger.warning("[EditListingHandler] ioexception: " + e);

					} catch (AttachmentLimitException e) {
						addError("You cannot upload anymore attachments");

					} catch (AttachmentExistsException e) {
						addError("A file with this name already exists");

					} catch (AttachmentTypeException e) {
						addError("You are not allowed to upload files of this type");
					}

				}
			}
		}

		return editAttachments();
	}

	public Object uploadImage() throws ServletException {

		if (item == null) {
			return index();
		}

		Object obj = checkPriv();
		if (obj != null) {
			return obj;
		}

		Listing listing = item.getListing();
		if (listing.getListingPackage().isImages()) {

			if (listing.getListingPackage().getMaxImages() > 0) {

				// ensure we have not uploaded too many images
				if (item.getAllImagesCount() >= listing.getListingPackage().getMaxImages()) {
					addError("You cannot upload anymore images");
					return editImages();
				}

			}

			if (imageUpload != null) {

				try {

					item.addImage(imageUpload, true, true, false);

				} catch (IOException e) {
					e.printStackTrace();
					addError(e);

					logger.config("[EditListingHandler] ioexception: " + e);

				} catch (ImageLimitException e) {
					e.printStackTrace();
				}
			}

		}

		return editImages();
	}

}
