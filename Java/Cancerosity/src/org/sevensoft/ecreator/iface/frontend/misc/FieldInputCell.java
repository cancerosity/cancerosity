package org.sevensoft.ecreator.iface.frontend.misc;

/**
 * @author sks 12 Mar 2007 17:12:35
 *
 */
public class FieldInputCell {

	private String	desc;
	private String	label;
	private Object[]	fields;
	private boolean	hidden;
	private String	id;

	public FieldInputCell(String label, String desc, Object... fields) {
		this.label = label;
		this.desc = desc;
		this.fields = fields;
	}

	public final FieldInputCell setHidden(boolean hidden) {
		this.hidden = hidden;
		return this;
	}

	public final FieldInputCell setId(String id) {
		this.id = id;
		return this;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("<div class=\"field_input\"");
		if (hidden) {
			sb.append(" style=\"display: none\"");
		}
		if (id != null) {
			sb.append(" id=\"" + id + "\"");
		}
		sb.append(">");

		if (label != null) {
			sb.append("<div class=\"label\">");
			sb.append(label);
			sb.append("</div>");
		}

		sb.append("<div class=\"input\">");
		for (Object field : fields) {
			sb.append(field);
		}
		sb.append("</div>");

		if (desc != null) {
			sb.append("<div class=\"desc\">");
			sb.append(desc);
			sb.append("</div>");
		}

		sb.append("</div>");

		return sb.toString();
	}

}
