package org.sevensoft.ecreator.iface.frontend.misc;

/**
 * @author sks 12 Mar 2007 17:11:29
 *
 */
public class FormFieldSet {

	private String	legend;
	private String	id;
	private String	style;

	public FormFieldSet() {

	}

	public FormFieldSet(String legend) {
		this.legend = legend;
	}

	public FormFieldSet setId(String id) {
		this.id = id;
		return this;
	}

	public FormFieldSet setStyle(String style) {
		this.style = style;
		return this;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("<fieldset class=\"ec_form\"");
		if (id != null) {
			sb.append(" id=\"" + id + "\"");
		}
		if (id != null) {
			sb.append(" style=\"" + style + "\"");
		}
		sb.append(">");

		if (legend != null) {
			sb.append("<legend>&nbsp;&nbsp;");
			sb.append(legend);
			sb.append("&nbsp;&nbsp;</legend>");
		}

		sb.append("<br/>");

		return sb.toString();
	}
}
