package org.sevensoft.ecreator.iface.frontend;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.iface.EcreatorDoc;
import org.sevensoft.ecreator.iface.admin.misc.seo.panels.GoogleAnalyticsExitLinksTrackingPanel;
import org.sevensoft.ecreator.iface.frontend.interaction.userplane.messenger.UserplaneMessengerCmdHandler;
import org.sevensoft.ecreator.iface.frontend.interaction.userplane.messenger.UserplaneMessengerHandler;
import org.sevensoft.ecreator.iface.frontend.interaction.userplane.messenger.UserplaneMessengerRequestCheckHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.template.Template;
import org.sevensoft.ecreator.model.design.template.TemplateConfig;
import org.sevensoft.ecreator.model.design.template.TemplateSettings;
import org.sevensoft.ecreator.model.extras.mapping.google.GoogleMapSettings;
import org.sevensoft.ecreator.model.extras.facebook.FacebookOpenGraphPanel;
import org.sevensoft.ecreator.model.interaction.userplane.messenger.UserplaneMessenger;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.seo.GoogleSettings;
import org.sevensoft.ecreator.model.misc.seo.SeoToolbar;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.system.config.Toolbar;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.model.search.forms.SearchForm;
import org.sevensoft.ecreator.model.stats.browsers.Browser;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Urls;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author sks 01-Dec-2005 00:55:47
 */
public class FrontendDoc extends EcreatorDoc {

    private static Logger logger = Logger.getLogger("ecreator");

    private Trail trail;
    private boolean printer;
    private final String title;
    private GoogleSettings googleSettings;
    private boolean superman;
    private final Item account;
    private boolean wl;

    public FrontendDoc(RequestContext context, String title) {

        super(context);

        this.googleSettings = GoogleSettings.getInstance(context);
        this.title = title;
        this.trail = new Trail();

        this.account = (Item) context.getAttribute("account");
        this.wl = context.containsAttribute("wl");

        Category root = (Category) context.getAttribute("root");
        if (root == null) {
            root = Category.getRoot(context);
        }

        trail.add(".", root.getName());

        request.setAttribute("title", title);
        request.setAttribute("_trail", trail);

        if (context.getParameters().containsKey("printer")) {
            this.printer = true;
        }

        this.superman = context.containsAttribute("superman");
    }

    public void addTrail(Class<? extends Handler> clazz, String label) {
        addTrail(clazz, null, label);
    }

    public void addTrail(Class<? extends Handler> clazz, String action, String label, Object... objects) {

        String url = Handler.getPath(clazz);

        if (objects != null && objects.length % 2 > 0) {
            throw new RuntimeException("Unequal number of params: " + objects);
        }

        Map params = new LinkedHashMap();

        if (action != null) {
            params.put("_a", action);
        }

        if (objects != null) {
            for (int n = 0; n < objects.length; n = n + 2) {
                if (objects[n] != null && objects[n + 1] != null) {
                    params.put(objects[n], objects[n + 1]);
                }
            }
        }

        addTrail(Urls.buildUrl(url, params), label);
    }

    public void addTrail(Link link, String name) {
        trail.add(link.toString(), name);
    }

    public void addTrail(String url, String name) {
        trail.add(url, name);
    }

    public boolean isPrinter() {
        return printer;
    }

    private StringBuilder noTemplate(User user) {
        StringBuilder sb = new StringBuilder();
//        sb.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
        sb.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
        sb.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
        sb.append("<head>\n");
        sb.append("<title>" + company.getName() + "</title>\n");
        sb.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />");
        sb.append("<style>");
        sb.append(new ToolbarCss());
        sb.append("</style>");
        sb.append("</head>");

        sb.append("<body>");
        sb.append(new Toolbar(user, context));
        sb.append("<table height='400' width='100%' style='font-size: 22px; color: #bbbbbb; font-weight: bold; font-family: Arial;'>"
                + "<tr><td align='center' valign='middle'>No template has been installed</td></tr></table>");
        sb.append("</body></html>");
        return sb;
    }

    @Override
    public StringBuilder output() throws ServletException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {

        if (printer) {
            return outputPrinter();
        } else {
            return outputScreen();
        }
    }

    private StringBuilder outputPrinter() {

        StringBuilder sb = new StringBuilder();
        sb.append("<html><head><title>" + title + "</title>");
        sb.append("<meta name='robots' content='noindex' />");
        sb.append("<style>");
        sb.append("h1 { font-family: Verdana; font-size: 14px; color: 0033cc; margin: 0; padding: 0; }");
        sb.append("h2 { font-family: Verdana; font-size: 12px; color: 333333; margin: 0; padding: 0;}");
        sb.append("</style>");
        sb.append("</head>\n<body>\n\n");

        sb.append(new TableTag().setWidth("600"));
        sb.append("<tr><td>");

        sb.append("<h1>" + company.getName() + "</h1>");

        if (company.hasAddress()) {
            sb.append("<h2>" + company.getLabel(", ") + "</h2>");
        }

        sb.append("<h1>" + title + "</h1>");
        sb.append("<h2>" + config.getUrl() + "</h2><h2>" + new DateTime().toString("HH:mm dd-MMM-yyyy") + "</h2>");
        sb.append("<hr/>");

        for (Object obj : bodys) {

            if (obj != null) {

                String content = obj.toString();
                if (content != null) {
                    sb.append(content);
                }
            }
        }

        sb.append("</td></tr></table>");
        sb.append("</body></html>");

        return sb;
    }

    private StringBuilder outputScreen() throws ServletException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {

        long timestamp = System.currentTimeMillis();

        User user = (User) context.getAttribute("user");

        Category category = (Category) context.getAttribute("category");
        Item item = (Item) context.getAttribute("item");
        SearchForm searchForm = (SearchForm)context.getAttribute("searchForm");

        // generate head first
        StringBuilder head = new StringBuilder();
        for (Object obj : heads) {

            if (obj != null) {

                String content = obj.toString();
                if (content != null) {
                    head.append(content);
                }
            }
        }

        /*
           * I want to generate body content before rendering the template because some tags, etc, need to change the way the template renders,
           * and it can only do this if the control commands are issued before the template outline is rendered.
           */
        StringBuilder body = new StringBuilder();
        for (Object obj : bodys) {

            if (obj != null) {

                String content = obj.toString();
                if (content != null) {
                    body.append(content);
                }
            }
        }

        StringBuilder sb = new StringBuilder();
        Template template = null;

        if (!wl) {

            /*
                * if we have more than one template in the system then look to see if we have a specific one set
                * If we only have one there is no point in any lookups
                */
            if (TemplateSettings.getInstance(context).hasMultipleTemplates()) {
                if (Module.FacebookApplications.enabled(context)) {
                    template = (Template) context.getAttribute("template");
                    logger.severe("template Referrer= " + context.getReferrer());
                    if (context.hasParameter("signed_request")) {
                        logger.severe("signed_request= " + context.getParameter("signed_request"));
                        logger.severe("template=custom template");
                    }
                }
                if (template == null) {
                    if (category != null) {
                        template = TemplateConfig.getApplicableTemplate(category);
                    }

                    if (item != null) {
                        template = TemplateConfig.getApplicableTemplate(item);
                    }

                    if (searchForm != null && searchForm.isParentTemplate()) {
                        if (searchForm.getSearchBlock() != null) {
                            if (searchForm.getSearchBlock().getOwnerCategory() != null) {
                                template = TemplateConfig.getApplicableTemplate(searchForm.getSearchBlock().getOwnerCategory());
                            }
                        }
                    }
                }

            }

            if (template == null) {
                template = Template.getDefault(context);
            }

            if (template == null) {
                return noTemplate(user);
            }

//            sb.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
            sb.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
            sb.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" ");

            sb.append(MiscSettings.getInstance(context).getHtmlAttrString());
            if (Module.FacebookLogin.enabled(context)) {
                sb.append(" xmlns:fb=\"http://www.facebook.com/2008/fbml\"");
                sb.append(" xmlns:og=\"http://ogp.me/ns#\"");
            }

            sb.append(">\n");

            if (Module.FacebookOpenGraphTags.enabled(context)) {
                sb.append(new FacebookOpenGraphPanel(context));
            } else {
                sb.append("<head>\n");
            }
            sb.append("<title>" + context.getAttribute("title_tag") + "</title>\n");

            sb.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />");

            String keywords = (String) context.getAttribute("keywords");
            if (keywords == null) {
                keywords = "";
            } else {
                keywords = keywords.replace("\"", "'");
            }

            String description_tag = (String) context.getAttribute("description_tag");
            if (description_tag == null) {
                description_tag = "";
            } else {
                description_tag = description_tag.replace("\"", "'");
            }

            sb.append("<meta name=\"keywords\" content=\"" + keywords + "\" />\n");
            sb.append("<meta name=\"description\" content=\"" + description_tag + "\" />\n");
            sb.append("<link href='" + context.getContextPath() + "/files/css/stylesheets-site.css' rel='stylesheet' type='text/css' />\n");
            if (scripts.isLoadEasyflashuploader()) {
                sb.append("<link href='" + context.getContextPath() + "/uploader/easyflashuploader.css' rel='stylesheet' type='text/css' />\n");
            }

            // backwards compat auto
            sb.append("<link href='" + context.getContextPath() + "/template-data/auto.css' rel='stylesheet' type='text/css' />\n");

            // new style autoID.css
            sb.append("<link href='" + context.getContextPath() + "/template-data/auto-" + template.getId() +
                    ".css' rel='stylesheet' type='text/css' />\n");

            sb.append("<!--[if lte IE 7]>\n" +
                    "<link rel='stylesheet' type='text/css' href='" + context.getContextPath() + "files/js/dropdown/ie.css' media='screen' />\n" +
                    "<![endif]-->\n");
            if (context.containsAttribute("canonical")) {
                sb.append("<link rel=\"canonical\" href=\"" + context.getAttribute("canonical") + "\"/>\n");
            }
            if (googleSettings.hasSiteMapMetaTag()) {
                sb.append(googleSettings.getSiteMapMetaTag());
            }

            if (context.containsAttribute("gmap")) {
                sb.append("<script src=\"http://maps.google.com/maps?file=api&amp;v=2&amp;key=" +
                        GoogleMapSettings.getInstance(context).getGoogleMapsApiKey() + "\" type=\"text/javascript\"></script>\n");
            }

            if (context.containsAttribute("gmapV3")) {
                sb.append("<script src=\"http://maps.googleapis.com/maps/api/js?sensor=false&amp;region=GB&amp;key=" +
                        GoogleMapSettings.getInstance(context).getGoogleMapsApiKey() + "\" type=\"text/javascript\"></script>\n");
            }

            if (context.containsAttribute("ibex")) {
                sb.append("<script type='text/javascript' src='files/js/ibex/SeekomFrameSizer.js'></script>\n");
            }

            sb.append("<script type='text/javascript' src='files/js/admin/ecreator-site-css.js'></script>\n");
//            sb.append("<script type='text/javascript' src='files/js/admin/ecreator-site.js'></script>\n");

            if (scripts.isLoadjQuery()){
                sb.append("<script type='text/javascript' src='files/js/admin/ecreator-site-jquery.js'></script>\n");
            }
            if (scripts.isLoadjQueryUi()){
                sb.append("<script type='text/javascript' src='files/js/admin/ecreator-site-jqueryui.js'></script>\n");
            }
            if(scripts.isLoadjQuery144()){
                sb.append("<script type='text/javascript' src='files/js/jquery/jquery-1.4.4.min.js'></script>\n");
                sb.append("<script type='text/javascript' src='files/js/jquery/jquery.autoscroller.js'></script>\n");
                sb.append("<script type='text/javascript' src='files/js/jquery/jquery.timers.js'></script>\n");
                sb.append("<script type='text/javascript' src='files/js/jquery/scroller/jscroller2-1.61.js'></script>\n");

            }
            if (scripts.isLoadThickbox()){
                sb.append("<script type='text/javascript' src='files/js/thickbox/thickbox.js'></script>\n");
            }
            if (scripts.isAC_RunActiveContent()) {
                sb.append("<script type='text/javascript' src='files/js/misc/AC_RunActiveContent.js'></script>\n");
            }

            if (scripts.isLoadIosFiletag() && (Browser.iOS.equals(Browser.getBrowser(RequestContext.getInstance().getUserAgent())))) {
                sb.append("<script type='text/javascript' src='files/js/iosfiletag/picup.js'></script>\n");
                sb.append("<script type='text/javascript' src='files/js/iosfiletag/prototype.js'></script>\n");
            }

            sb.append("<script type='text/javascript' src='files/js/admin/ecreator-js-minified.js'></script>\n");


            if (miscSettings.isFavicon()) {
                sb.append("<link rel='shortcut icon' href='favicon.ico' type='image/x-icon' />");
            }

            userplaneMessenger(sb);

//            if (template.hasHead()) {
//                sb.append(template.getHead());
//            }
            sb.append(template.renderHead(context));

            sb.append(head);

            if (scripts.isLoadEasyflashuploader()) {
                sb.append("<script type='text/javascript' src='uploader/easyflashuploader.js'></script>\n");
            }

            sb.append(new GoogleAnalyticsExitLinksTrackingPanel(context));

            sb.append("</head>\n\n");

            if (context.containsAttribute("gmap") || context.containsAttribute("gmapV3")) {

                sb.append("<body onload='gload()' >\n");
            } else if (context.containsAttribute("attrSection")) {

                sb.append("<body onload=\"" + context.getAttribute("attrSection") + "\">\n");

            } else if (context.containsAttribute("optSection")) {

                sb.append("<body onload=\"" + context.getAttribute("optSection") + "\">\n");
            } else {

                sb.append("<body>\n");
            }
//
//            if (Module.LoginFacebook.enabled(context)) {
//                sb.append(new FacebookLoginIntegration(context));
//            }

            /*
                * If demo or logged in as admin user  or superman  show demo bar
                */
            if (config.isDemo() || superman || (user != null && miscSettings.isToolbar())) {
                sb.append(new Toolbar(user, context));
            }

            if (seo.isToolbar() && user != null) {
                sb.append(new SeoToolbar(context));
            }

            sb.append(template.renderBefore(context));

        }

        sb.append(body);

        if (Module.CookieControl.enabled(context)) {
            if (scripts.isLoadCookieControl()) {
                sb.append("\n<script type='text/javascript' src='files/js/cookie/cookieControl-4.1.min.js'></script>\n");
            } else if (scripts.isLoadCookieControlWithRevoke()) {
                sb.append("\n<script type='text/javascript' src='files/js/cookie/cookieControl-with-Revoke-4.1.min.js'></script>\n");
            }
        }

        if (!wl) {

            if (!context.getRequest().isSecure()) {

                if (googleSettings.hasGoogleAnalytics()) {
                    sb.append(googleSettings.getGoogleAnalytics());
                }

            }

            sb.append(template.renderAfter(context));

        }

        logger.fine("[FrontendDoc] template bottom finished, time=" + (System.currentTimeMillis() - timestamp));
        timestamp = System.currentTimeMillis();

        return sb;
    }

    public void removeTitle() {
        request.setAttribute("title", "");
    }

    public void removeTrail() {

        request.setAttribute("trail_pipe", "");
        request.setAttribute("trail_arrow", "");
        request.setAttribute("trail_colon", "");
    }

    public void setAttribute(String key, String value) {
        request.setAttribute(key, value);
    }

    public void setDescription(String description) {
        request.setAttribute("description_tag", description);
    }

    private void setTags() {

        request.setAttribute("copyright", modules.getCopyrightMessageRendered());
        request.setAttribute("credits", modules.getPoweredByMessageRendered());

    }

    /**
     * Writes out the ajax request that will check the server for a new chat request
     */
    private void userplaneMessenger(StringBuilder sb) {

        if (account != null) {

            if (Module.UserplaneMessenger.enabled(context)) {

                sb.append("<script type='text/javascript' language='javascript'>\n");

                sb.append("var userplaneRequest = false; \n");

                sb.append("function createUserplaneRequest() {\n");

                sb.append("	if (window.XMLHttpRequest) { \n");
                sb.append("		userplaneRequest = new XMLHttpRequest();\n");
                sb.append("	} else if (window.ActiveXObject) {\n");
                sb.append("		try {\n");
                sb.append("			userplaneRequest = new ActiveXObject('Msxml2.XMLHTTP'); \n");
                sb.append("		} catch (e) {\n");
                sb.append("			try {\n");
                sb.append("				userplaneRequest = new ActiveXObject('Microsoft.XMLHTTP'); \n");
                sb.append("			} catch (e) {}\n");
                sb.append("		}\n");
                sb.append("	}\n");
                sb.append("	if (!userplaneRequest) {\n");
                sb.append("		alert('Giving up :( Cannot create an XMLHTTP instance');\n");
                sb.append("	}\n");

                sb.append("}\n");

                sb.append("function userplaneCheck() {\n");

                sb.append("	createUserplaneRequest();	\n");
                sb.append("	userplaneRequest.onreadystatechange = userplanePopup;		\n");
                sb.append("	userplaneRequest.open('GET', '" + new Link(UserplaneMessengerRequestCheckHandler.class) + "', true);		\n");
                sb.append("	userplaneRequest.send(null);		\n");

                sb.append("}\n");

                sb.append("function userplanePopup() {\n");

                sb.append("	if (userplaneRequest.readyState == 4 && userplaneRequest.status == 200) {\n");

                sb.append("		var xmldoc = userplaneRequest.responseXML; \n");
                sb.append("		var originatorId = xmldoc.getElementsByTagName('originatorId').item(0).firstChild.data; \n");
                sb.append("		var originatorName = xmldoc.getElementsByTagName('originatorName').item(0).firstChild.data; \n");

                sb.append("		if (confirm(originatorName + ' would like to start a chat with you - do you accept?')) { \n");

                // Open the window
                sb.append("			window.open('" + new Link(UserplaneMessengerHandler.class) + "?target=' + originatorId, " +
                        "'userplane' + originatorId, 'width=" + UserplaneMessenger.PopupWidth + ",height=" + UserplaneMessenger.PopupHeight +
                        ",status=no,resize=no');\n");

                sb.append("		} else { \n");

                sb.append("			createUserplaneRequest();	\n");
                sb.append("			userplaneRequest.open('GET', '" + new Link(UserplaneMessengerCmdHandler.class) +
                        "?originator=' + originatorId, true);		\n");
                sb.append("			userplaneRequest.send(null);		\n");

                sb.append(" 		} \n");
                sb.append("	}\n");
                sb.append("}\n");

                sb.append("setInterval('userplaneCheck()', 6000); \n");

                sb.append("</script>\n");
            }

        }
    }
}
