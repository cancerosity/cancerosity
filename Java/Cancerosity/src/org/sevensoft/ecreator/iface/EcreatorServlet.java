package org.sevensoft.ecreator.iface;

import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.commons.smail.Email;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminCssWriter;
import org.sevensoft.ecreator.iface.frontend.FrontendCssWriter;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.JNDIUtils;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.model.design.template.TemplateUtil;
import org.sevensoft.jeezy.http.HandlerServlet;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.i18n.Messages;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.logging.*;

/**
 * @author sks 31-Jul-2005 20:28:54
 */
public class EcreatorServlet extends HandlerServlet {

    @SuppressWarnings("hiding")
    private static Logger logger = Logger.getLogger("ecreator");

    @Override
    protected void doHandlers(Map<String, String> paths) {
        Paths.setPaths(paths);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        long timestamp = System.currentTimeMillis();
        logger.fine("--------------------------------");
       
        super.doPost(request, response);

        logger.fine("[EcreatorServlet] request time=" + (System.currentTimeMillis() - timestamp));
    }

    @Override
    public void init(ServletConfig config) throws ServletException {

        long time = System.currentTimeMillis();
        logger.config("[EcreatorServlet] init");

        super.init(config);

//        try {
//            loggers(config);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        version(config);
        mkdir();
        superIps(config);

        String timeString = "[EcreatorServlet] init took: " + (System.currentTimeMillis() - time) + "ms";
        logger.config(timeString);
    }

    public void loggers(ServletConfig config) throws IOException {

        String levelParam = config.getServletContext().getInitParameter("logging-level");
        Level level;

        if ("FINE".equalsIgnoreCase(levelParam)) {
            level = Level.FINE;
        } else if ("FINER".equalsIgnoreCase(levelParam)) {
            level = Level.FINER;
        } else if ("FINEST".equalsIgnoreCase(levelParam)) {
            level = Level.FINEST;
        } else if ("SEVERE".equalsIgnoreCase(levelParam)) {
            level = Level.SEVERE;
        } else if ("CONFIG".equalsIgnoreCase(levelParam)) {
            level = Level.CONFIG;
        } else {
            level = Level.OFF;
        }

        // always log cron and payments
        try {
            Handler handler = new FileHandler(config.getServletContext().getRealPath("WEB-INF/logs/ecreator.log"));
            handler.setFormatter(new Formatter() {

                private SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy HH:mm:SSS");

                @Override
                public String format(LogRecord record) {
                    return "[" + format.format(new Date(record.getMillis())) + "] [" + record.getLoggerName().replace("org.sevensoft.", "") + "] " +
                            record.getMessage() + "\n";
                }
            });
            handler.setLevel(level);
            Logger.getLogger("org.sevensoft").setLevel(level);
            Logger.getLogger("org.sevensoft").addHandler(handler);

            Logger.getLogger("com.caucho").setLevel(Level.OFF);
        } catch (IOException e) {
            logger.warning("[EcreatorServlet] Problem opening file WEB-INF/logs/ecreator.log");
        }

    }


    private void parseForIps(File file, HashSet<String> ips) {

        logger.config("[EcreatorServlet] loading ips from: " + file);

        if (file.exists()) {

            try {

                String string = SimpleFile.readString(file);
                for (String line : string.split("\\n")) {

                    line = line.trim();
                    if (line.length() == 0) {
                        continue;
                    }

                    if (line.startsWith("#")) {
                        continue;
                    }

                    logger.config("[EcreatorServlet] Ip detected: " + line);

                    ips.add(line);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {

            logger.config("[EcreatorServlet] file not found " + file);
        }
    }

    @Override
    protected void setup(RequestContext context) {

        long time = System.currentTimeMillis();
        logger.config("[EcreatorServlet] setup");

        final Config config = Config.getInstance(context);

        config.init(context);


        logger.config("[EcreatorServlet] Config initialised");

        // write out css
        try {
            new AdminCssWriter().write(context);
            logger.config("[EcreatorServlet] written admin css");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            new FrontendCssWriter().write(context);
            logger.config("[EcreatorServlet] written frontend css");
        } catch (IOException e) {
            e.printStackTrace();
        }

        //write auto.css
        try {
            TemplateUtil.saveCss(context);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // set emails to dev
        if (context.isLocalhost()) {
            Email.setDevelopment(true);
        }

        FileChecker.run(context);

        String timeString = "[EcreatorServlet] setup took: " + (System.currentTimeMillis() - time) + "ms";
        logger.config(timeString);
    }

    private void mkdir() {
        for (String directory : Config.getInitDirs()) {
            new File(JNDIUtils.getResourcesPath() + File.separator + directory).mkdirs();
            if (directory.equals(Config.EmailLogoPath)) {
                initLogo("logo.gif");
                initLogo("logo.png");
                initLogo("header.jpg");
                initLogo("header.png");
            }
        }
    }

    private void initLogo(String logogif) {
        try {
            if (ResourcesUtils.getRealTemplateData(logogif).exists()) {
                File logo = ResourcesUtils.getRealEmailLogoDir();
//                logo.createNewFile();
                TemplateUtil.copyFile(logo, ResourcesUtils.getRealTemplateData(logogif));
            }
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void superIps(ServletConfig config) {

        /*
           * Load our superman ip's server file and parse for ips
           */
//        File file = new File("/home/ecreator/superips");

        File file = ResourcesUtils.getRealSuperIPsFile();
        HashSet<String> set = new HashSet();
        config.getServletContext().setAttribute(User.SuperIpsKey, set);

        parseForIps(file, set);
    }

    private void version(ServletConfig config) {

        Properties props = new Properties();
        InputStream in = getClass().getResourceAsStream("/build.number");

        if (in == null) {

            config.getServletContext().setAttribute("version", "dev");
            config.getServletContext().setAttribute("dev", "true");

            System.setProperty("java.io.tmpdir", "c:/");

        } else {

            try {

                props.load(in);
                in.close();

                config.getServletContext().setAttribute("version", props.get("version"));

            } catch (IOException e) {

                e.printStackTrace();

                config.getServletContext().setAttribute("version", "dev");
                config.getServletContext().setAttribute("dev", "true");

                System.setProperty("java.io.tmpdir", "c:/");
            }

        }

    }

   
}