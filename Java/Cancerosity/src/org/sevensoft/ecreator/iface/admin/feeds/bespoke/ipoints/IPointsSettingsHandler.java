package org.sevensoft.ecreator.iface.admin.feeds.bespoke.ipoints;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.bespoke.ipoints.IPointsSettings;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 4 Aug 2006 15:28:56
 */
@Path("admin-ipoints-settings.do")
public class IPointsSettingsHandler extends AdminHandler {

    private IPointsSettings settings;
    private String password;
    private String username;
    private int pointsPerPound;
    private String termsUrl;

    public IPointsSettingsHandler(RequestContext context) {
        super(context);
        settings = IPointsSettings.getInstance(context);
    }

    @Override
    public Object main() throws ServletException {

        AdminDoc doc = new AdminDoc(context, user, "Edit expiry bot", null);
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Save changes"));
                sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to menu"));

                sb.append("</div>");
            }

            private void general() {

                sb.append(new AdminTable("Settings"));

                sb.append(new AdminRow("Username", "Your ipoints username. Issued by ipoints.",
                        new TextTag(context, "username", settings.getUsername())));

                sb.append(new AdminRow("Password", "Your ipoints password. Issued by ipoints.",
                        new TextTag(context, "password", settings.getPassword())));

                sb.append(new AdminRow("Points per pound", "This is how many points the shopping will earn for every full pound spent at the store.",
                        new TextTag(context, "pointsPerPound", settings.getPointsPerPound(), 20)));

                sb.append(new AdminRow("T&C Url", "This is a link to a page containing the terms and condition.", new TextTag(context, "termsUrl",
                        settings.getTermsUrl())));

                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(IPointsSettingsHandler.class, "save", "POST"));

                commands();
                general();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object save() {

        settings.setPassword(password);
        settings.setUsername(username);
        settings.setPointsPerPound(pointsPerPound);
        settings.setTermsUrl(termsUrl);

        settings.save();

        return new ActionDoc(context, "Settings updated", new Link(IPointsSettingsHandler.class));
    }
}
