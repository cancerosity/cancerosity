package org.sevensoft.ecreator.iface.admin.accounts;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.model.ecom.payments.results.AvsCheck;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sam
 */
@Path("admin-ecommerce-avs.do")
public class AvsChecksHandler extends AdminHandler {

	private Item	customer;

	public AvsChecksHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (customer == null) {
			return index();
		}

		AdminDoc page = new AdminDoc(context, user, "Avs Checks", null);
		page.addBody(new Body() {

			private void checks() {

				sb.append(new TableTag("form", 1, 0).setCaption("Avs checks"));

				sb.append("<tr>");
				sb.append("<th>Date</th>");
				sb.append("<th>Result</th>");
				sb.append("<th>Address checked</th>");
				sb.append("<th>Postcode checked</th>");
				sb.append("<th>Card</th>");
				sb.append("<th>Transaction Id</th>");
				sb.append("</tr>");

				for (AvsCheck avsCheck : customer.getAvsChecks()) {

					sb.append("<tr>");
					sb.append("<td>" + avsCheck.getDate().toString("dd-MMM-yyyy") + "</td>");
					sb.append("<td>" + avsCheck.getResult() + "</td>");
					sb.append("<td>" + avsCheck.getAddress() + "</td>");
					sb.append("<td>" + avsCheck.getPostcode() + "</td>");
					sb.append("<td>" + avsCheck.getCard() + "</td>");
					sb.append("<td>" + avsCheck.getTransactionId() + "</td>");

					sb.append("</tr>");
				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				checks();

				return sb.toString();
			}

		});

		return page;
	}
}
