package org.sevensoft.ecreator.iface.admin.ecom.payments.help;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.ToolTip;

/**
 * User: Tanya
 * Date: 11.01.2013
 */
public class GlobalIrisCallbackHelp extends ToolTip {

    protected String getHelp() {
        return "Response data is sent back to this response script for each transaction.\n " +
                "In order to receive this data you must provide Realex Payments " +
                "with the URL of your response script.\n " +
                "The response URL is to be emailed to support@realexpayments.com";
    }
}