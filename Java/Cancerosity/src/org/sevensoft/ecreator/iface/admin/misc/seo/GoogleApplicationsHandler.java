package org.sevensoft.ecreator.iface.admin.misc.seo;

import org.sevensoft.ecreator.help.seo.GoogleSitemapTip;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.misc.seo.GoogleSettings;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sam
 */
@Path("admin-settings-googlestuff.do")
public class GoogleApplicationsHandler extends AdminHandler {

	private final transient GoogleSettings	googleSettings;

	private String					googleAnalytics;
	private String					siteMapMetaTag;

	public GoogleApplicationsHandler(RequestContext context) {
		super(context);
		this.googleSettings = GoogleSettings.getInstance(context);
	}

	/*
	 * @see org.sevensoft.commons.jwf.Handler#main()
	 */
	@Override
	public Object main() {

		AdminDoc page = new AdminDoc(context, user, "Google settings", null);
		page.addBody(new Body() {

            private void commands() {
                sb.append("<div>" + new SubmitTag("Update google settings") + "</div>");
            }

            private void analytics() {

				sb.append(new AdminTable("Google analytics"));

				sb.append(new AdminRow("Google analytics code", "Paste here the code google provides for analytics.", new TextAreaTag(context,
						"googleAnalytics", googleSettings.getGoogleAnalytics(), 60, 7)));

				sb.append("</table>");
			}

			private void sitemaps() {

				sb.append(new AdminTable("Sitemaps"));

				sb.append(new AdminRow("Sitemap", "This is a link to a realtime generated sitemap suitable to submission to " +
						new LinkTag("http://www.google.com/webmasters/sitemaps/login", "google sitemaps").setTarget("_blank") + ".", new LinkTag(
						"google-sitemap.do", "Google Sitemap XML")).setHelp(new GoogleSitemapTip()));

				sb.append(new AdminRow("Google sitemap meta tag", "Paste here the meta verification tag google requests to be inserted into the html.",
						new TextTag(context, "siteMapMetaTag", googleSettings.getSiteMapMetaTag(), 50)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(GoogleApplicationsHandler.class, "save", "POST"));

                commands();
				analytics();
				sitemaps();

				commands();

				sb.append("</form>");
				return sb.toString();
			}

		});

		return page;
	}

	public Object save() {

		googleSettings.setGoogleAnalytics(googleAnalytics);
		googleSettings.setSiteMapMetaTag(siteMapMetaTag);
		googleSettings.save();

		clearParameters();
		return main();
	}
}