package org.sevensoft.ecreator.iface.admin.items;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.OwnerAttributesPanel;
import org.sevensoft.ecreator.model.items.ItemTypeSettings;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 29 Dec 2006 19:18:22
 *
 */
@Path("admin-items-types-settings.do")
public class GlobalItemAttributesHandler extends AdminHandler {

	private final transient ItemTypeSettings	itemTypeSettings;
	private String					newAttributes;
	private String					newAttributesSection;
	private int					newAttributesPage;

	public GlobalItemAttributesHandler(RequestContext context) {
		super(context);
		this.itemTypeSettings = ItemTypeSettings.getInstance(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Item type settings", null);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(GlobalItemAttributesHandler.class, "save", "post"));
				sb.append(new OwnerAttributesPanel(context, itemTypeSettings, new Link(GlobalItemAttributesHandler.class)));
				sb.append(new SubmitTag("Add attributes"));
				sb.append("</form>");

				return sb.toString();
			}
		});

		return doc;
	}

	public Object save() throws ServletException {

		// attributes
		if (newAttributes != null) {
			for (Attribute attribute : itemTypeSettings.addAttributes(newAttributes, newAttributesSection, newAttributesPage)) {
				itemTypeSettings.log(user, "Attribute '" + attribute.getName() + "' created.");
			}
		}

		return main();
	}
}
