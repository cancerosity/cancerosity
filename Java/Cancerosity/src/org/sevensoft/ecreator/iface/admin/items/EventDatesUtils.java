package org.sevensoft.ecreator.iface.admin.items;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.Item;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * User: MeleshkoDN
 * Date: 26.09.2007
 * Time: 9:34:20
 */
public class EventDatesUtils {


    public static List<Date> evaluateEventDates(DateTime now, Item event) {
        Attribute startDateAttribute = event.getAttribute("Date start");
        if (event.getAttributeValue("Date end") == null) {
            event.setAttributeValue(event.getAttribute("Date end"), event.getAttributeValue(event.getAttribute("Date start")).toString());
        }
        Attribute endDateAttribute = event.getAttribute("Date end");

        String start = event.getAttributeValue(startDateAttribute);
        String end = event.getAttributeValue(endDateAttribute);
        Date startDate = new Date(Long.parseLong(start));
        Date endDate = new Date(Long.parseLong(end));

        Integer repeatTerm = Integer.parseInt(event.getAttributeValue("Repeat term"));
        String repeatOptionValue = event.getAttributeValue("Repeat option");
        RecurrenceOption repeatOption = RecurrenceOption.valueOf(repeatOptionValue);
        return EventDatesUtils.evaluateEventDates(now, startDate, endDate, repeatOption, repeatTerm);

    }


    public static List<Date> evaluateEventDates(DateTime now, Date startDate, Date endDate, RecurrenceOption option, Integer term) {
        List<Date> eventDates = new ArrayList<Date>(2);
        Integer daysBetween = endDate.getDaysBetween(startDate);
        int iterations = 0;
        if (now.isAfter(endDate.addDays(1))) {
            switch (option) {
                case Day:
                    while (!now.isBefore(endDate.nextDay().getTimestamp())) {
                        startDate = startDate.addDays(term);
                        endDate = startDate.addDays(daysBetween);
                        iterations++;
                        if (iterations > 1000000) {
                            break;
                        }
                    }
                    break;
                case Week:
                    while (!now.isBefore(endDate.nextDay().getTimestamp())) {
                        startDate = startDate.addWeeks(term);
                        endDate = startDate.addDays(daysBetween);
                        iterations++;
                        if (iterations > 1000000) {
                            break;
                        }
                    }
                    break;
                case Month:
                    boolean isLastDayOfMonth = endDate.nextDay().isLastDayOfMonth();
                    boolean isLast = startDate.isLastDayOfMonth();
                    while (!now.isBefore(endDate.getTimestamp())) {
                        startDate = startDate.addMonths(term);
                        if (isLast){
                             startDate = startDate.endMonth();
                        }
                        endDate = endDate.addMonths(term);
                        iterations++;
                        if (iterations > 1000000) {
                            break;
                        }
                    }
                    if (isLastDayOfMonth && !endDate.isLastDayOfMonth()) {
                        endDate = endDate.endMonth();
                    }
                    break;
                case Year:
                    while (!now.isBefore(endDate.nextDay().getTimestamp())) {
                        startDate = startDate.addYears(term);
                        endDate = endDate.addYears(term);
                        iterations++;
                        if (iterations > 1000000) {
                            break;
                        }
                    }
                    break;
                default:
                    throw new IllegalArgumentException();
            }
        }
        eventDates.add(startDate);
        eventDates.add(endDate);
        return eventDates;
    }

    public static void main(String[] args) {
        Date start = new Date(new GregorianCalendar(2007, Calendar.SEPTEMBER, 3).getTimeInMillis());
        Date end = new Date(new GregorianCalendar(2007, Calendar.SEPTEMBER, 4).getTimeInMillis());
        DateTime now = new DateTime(new GregorianCalendar(2007, Calendar.SEPTEMBER, 26).getTimeInMillis());
        RecurrenceOption option = RecurrenceOption.Week;
        Integer term = 3;
        List<Date> dates = evaluateEventDates(now, start, end, option, term);
        System.out.println("Start:" + dates.get(0).toString("dd/MM/yyyy"));
        System.out.println("End:" + dates.get(1).toString("dd/MM/yyyy"));
    }
}
