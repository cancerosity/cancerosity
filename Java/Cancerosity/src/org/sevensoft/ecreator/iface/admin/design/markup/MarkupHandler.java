package org.sevensoft.ecreator.iface.admin.design.markup;

import java.io.IOException;

import javax.servlet.ServletException;

import org.jdom.JDOMException;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.XmlResult;

/**
 * @author sks 20-May-2005 19:11:59
 * 
 */
@Path("admin-markup.do")
public class MarkupHandler extends AdminHandler {

	protected static final String[]	TableAligns	= { "Left", "Center", "Right" };

	private transient Company		company;

	private String				body;
	private String				name;
	private int					tds;
	private Markup				markup;
	private String				between;
	private String				start;
	private String				end;
	private String				containerId;
	private String				containerClass;
	private Upload				upload;
	private String				css;
	private String				containerAlign;
	private int					cycleRows;
	private String				caption;
	private int					perPage;

	public MarkupHandler(RequestContext context) {
		super(context);
		company = Company.getInstance(context);
	}

	public Object create() throws ServletException {

		markup = new Markup(context, "-new markup-");
		return main();
	}

	public Object delete() throws ServletException {

		if (markup == null) {
			return main();
		}

		markup.delete();
		return main();
	}

	public Object edit() throws ServletException {

		if (!isSuperman() && !Module.UserMarkup.enabled(context))
			return main();

		if (markup == null) {
			return main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit Markup: " + markup.getName(), null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update markup"));
				sb.append(new ButtonTag(MarkupHandler.class, "delete", "Delete markup", "markup", markup)
						.setConfirmation("Are you sure want to delete this markup?"));
				sb.append(new ButtonTag(MarkupHandler.class, null, "Return to markup menu"));

				sb.append("</div>");
			}

			private void markup() {

				sb.append(new AdminTable("Edit markup"));

				sb.append("<tr>");
				sb.append("<td colspan='2'>Name " + new TextTag(context, "name", markup.getName(), 40) + " Tds "
						+ new TextTag(context, "tds", markup.getTds(), 3) + " Class "
						+ new TextTag(context, "containerClass", markup.getContainerClass(), 12) + " Align "
						+ new SelectTag(context, "containerAlign", markup.getContainerAlign(), MarkupHandler.TableAligns, "Default") + " Id "
						+ new TextTag(context, "containerId", markup.getContainerId(), 10) + " Cycle rows "
						+ new TextTag(context, "cycleRows", markup.getCycleRows(), 3) + " Caption "
						+ new TextTag(context, "caption", markup.getCaption(), 12) + " Per page "
						+ new TextTag(context, "perPage", markup.getPerPage(), 3) + "</td>");
				sb.append("</tr>");

				sb.append("<tr><td colspan='2'>Css</td></tr>");
				sb.append("<tr><td colspan='2'>" + new TextAreaTag(context, "css", markup.getCss(), 170, 12) + "</td></tr>");

				sb.append("<tr><td colspan='2'>Start</td></tr>");
				sb.append("<tr><td colspan='2'>" + new TextAreaTag(context, "start", markup.getStart(), 170, 6) + "</td></tr>");

				sb.append("<tr><td colspan='2'>" + new TextAreaTag(context, "body", markup.getBody(), 170, 20) + "</td></tr>");

				sb.append("<tr><td>End</td>");
				sb.append("<td>Between</td></tr>");

				sb.append("<tr><td>" + new TextAreaTag(context, "end", markup.getEnd(), 90, 5) + "</td>");
				sb.append("<td>" + new TextAreaTag(context, "between", markup.getBetween(), 90, 5) + "</td></tr>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(MarkupHandler.class, "save", "POST"));
				sb.append(new HiddenTag("markup", markup));

                commands();
				markup();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object exporter() {

		MarkupXmlExporter exporter = new MarkupXmlExporter();
		String filename = company.getName() + "_markup_" + new DateTime().toString("dd_MMM_hha") + ".xml";
		return new XmlResult(exporter.getDocument(context), filename);
	}

	public Object importer() throws ServletException {

		if (upload == null) {
			return main();
		}

		try {
			new MarkupXmlImporter(context).process(upload.getContentAsString());
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return main();
	}

	@Override
	public Object main() throws ServletException {

		if (!isSuperman() && !Module.UserMarkup.enabled(context)) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Markup", null);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Markups"));

				for (Markup markup : Markup.get(context)) {

					sb.append("<tr><td>"
							+ new LinkTag(MarkupHandler.class, "edit", new ImageTag("files/graphics/admin/spanner.gif"), "markup", markup) + " "
							+ markup.getName() + " (#" + markup.getId() + ")</td>");

					sb.append("<td width='20'>"
							+ new LinkTag(MarkupHandler.class, "delete", new DeleteGif(), "markup", markup)
									.setConfirmation("Are you sure you want to delete this markup?") + "</td></tr>");

				}

				sb.append("<tr><td colspan='2'>");
				sb.append(new ButtonTag(MarkupHandler.class, "create", "Add markup"));
				sb.append(new ButtonTag(MarkupHandler.class, "exporter", "Export markup"));
				sb.append("</td></tr>");

				sb.append(new FormTag(MarkupHandler.class, "importer", "multi"));
				sb.append("<tr><td colspan='2'>Import markup " + new FileTag("upload") + new SubmitTag("Import") + "</td></tr>");
				sb.append("</form>");

				sb.append("</table>");

				return sb.toString();
			}
		});
		return doc;
	}

	public Object save() throws ServletException {

		if (markup == null) {
			return main();
		}

		if (!ObjectUtil.equal(markup.getBetween(), between)) {
			markup.setBetween(between);
			markup.log(user, "Between markup changed");
		}

		if (!ObjectUtil.equal(markup.getStart(), start)) {
			markup.setStart(start);
			markup.log(user, "Start markup changed");
		}

		if (!ObjectUtil.equal(markup.getCaption(), caption)) {
			markup.setCaption(caption);
			markup.log(user, "Caption changed");
		}

		if (!ObjectUtil.equal(markup.getEnd(), end)) {
			markup.setEnd(end);
			markup.log(user, "End markup changed");
		}

		if (!ObjectUtil.equal(markup.getBody(), body)) {
			markup.setBody(body);
			markup.log(user, "Body markup changed");
		}

		markup.setTds(tds);
		markup.setName(name);
		markup.setCycleRows(cycleRows);
		markup.setTableClass(containerClass);
		markup.setTableId(containerId);
		markup.setContainerAlign(containerAlign);
		markup.setPerPage(perPage);

		try {
			markup.setCss(css);
		} catch (IOException e) {
			addError(e);
		}

		markup.save();

		clearParameters();
		return edit();
	}
}
