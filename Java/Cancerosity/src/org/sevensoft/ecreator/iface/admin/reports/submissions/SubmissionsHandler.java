package org.sevensoft.ecreator.iface.admin.reports.submissions;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.reports.StatsMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.stats.submissions.SubmissionRecord;
import org.sevensoft.ecreator.model.stats.submissions.SubmissionsCounter;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 03-Oct-2005 16:12:08
 * 
 */
@Path("admin-reports-submissions.do")
public class SubmissionsHandler extends AdminHandler {

	private Date	start;
	private Date	end;

	public SubmissionsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {
		return null;
	}

	/**
	 * Shows submissions by form per month
	 */
	public Object monthly() throws ServletException {

		if (start == null)
			start = new Date().removeYear(1).beginMonth();

		if (end == null)
			end = new Date().beginMonth();

		final List<SubmissionsCounter> counters = SubmissionsCounter.get(context, start, end);

		AdminDoc page = new AdminDoc(context, user, "Submissions per form monthly", Tab.Stats);
		page.setMenu(new StatsMenu());
		page.setIntro("Listed here are your total submissions for all forms from " + start.toString("MMMM yy") + " to " + end.toString("MMMM yy"));
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Submissions per form monthly"));

				sb.append("<tr>");
				sb.append("<td>Month</td>");
				sb.append("<td>Form</td>");
				sb.append("<td>Total submissions</td>");
				sb.append("</tr>");

				for (SubmissionsCounter counter : counters) {

					sb.append("<tr>");
					sb.append("<td>" + counter.getDate().toString("MMMM-yy") + "</td>");
					sb.append("<td>" + counter.getFormName() + "</td>");
					sb.append("<td>" + counter.getTotal() + "</td>");
					sb.append("</tr>");

				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return page;
	}

	/**
	 * Shows recent submissions
	 */
	public Object recent() {

		int limit = 200;
		final List<SubmissionRecord> submissions = SubmissionRecord.get(context, limit);

		AdminDoc page = new AdminDoc(context, user, "Recent form submissions", Tab.Stats);
		page.setMenu(new StatsMenu());
		page.setIntro("This screen shows your most recent " + limit + " form submissions.");
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Recent submissions"));

				sb.append("<tr>");
				sb.append("<td>Date / Time</td>");
				sb.append("<td>IP Address</td>");
				sb.append("<td>Form</td>");
				sb.append("</tr>");

				for (SubmissionRecord submission : submissions) {

					sb.append("<tr>");
					sb.append("<td>" + submission.getDate().toDateTimeString() + "</td>");
					sb.append("<td>" + submission.getIpAddress() + "</td>");
					sb.append("<td>" + submission.getFormName() + "</td>");
					sb.append("</tr>");
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return page;
	}

}
