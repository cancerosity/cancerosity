package org.sevensoft.ecreator.iface.admin.reports.referrals;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.reports.StatsMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.stats.referrals.Referral;
import org.sevensoft.ecreator.model.stats.referrals.ReferralCounterMonthly;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 03-Oct-2005 16:12:08
 * 
 */
@Path("admin-reports-referrals.do")
public class ReferralsHandler extends AdminHandler {

    private static final int RESULTS_PER_PAGE = 200;

	private Date	month;
    private int page;

	public ReferralsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {
		return null;
	}

	public Object monthly() {

		if (month == null)
			month = new Date();
		month = month.beginMonth();

		final List<ReferralCounterMonthly> counters = ReferralCounterMonthly.get(context, month, 100);

		AdminDoc page = new AdminDoc(context, user, "Monthly referrals", Tab.Stats);
		page.setMenu(new StatsMenu());
		page.setIntro("This screen shows referrals per domain name for the month " + month.toString("MMMM-yy"));
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Monthly referrals"));

				sb.append(new FormTag(ReferralsHandler.class, "monthly", "get"));
				SelectTag monthTag = new SelectTag(context, "month");
				monthTag.setAutoSubmit();

				Date month = new Date().beginMonth();
				for (int n = 0; n < 13; n++) {
					monthTag.addOption(month, month.toString("MMMM-yy"));
					month = month.removeMonths(1);
				}

				sb.append("<tr><th align='right' colspan='2'>Choose month ");
				sb.append(monthTag);
				sb.append("</th></tr>");

				sb.append("</form>");

				sb.append("<tr>");
				sb.append("<th>Hostname</th>");
				sb.append("<th>Total</th>");
				sb.append("</tr>");

				for (ReferralCounterMonthly counter : counters) {

					sb.append("<tr>");
					sb.append("<td>" + counter.getHostname() + "</td>");
					sb.append("<td>" + counter.getTotal() + "</td>");
					sb.append("</tr>");
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return page;
	}

	public Object recent() {

        final Results results = new Results(Referral.count(context), page, RESULTS_PER_PAGE);
		final List<Referral> referrals = Referral.get(context, results.getStartIndex(), RESULTS_PER_PAGE);

		AdminDoc page = new AdminDoc(context, user, "Recent referrals", Tab.Stats);
		page.setMenu(new StatsMenu());
		page.setIntro("This screen shows the most recent referrals.");
		page.addBody(new Body() {

			@Override
			public String toString() {
                sb.append(new ResultsControl(context, results, new Link(ReferralsHandler.class, "recent")));

				sb.append(new AdminTable("Recent referrals"));

				sb.append("<tr>");
				sb.append("<th>Date / Time</th>");
				sb.append("<th>IP Address</th>");
				sb.append("<th>Referrer</th>");
				sb.append("</tr>");

				for (Referral referral : referrals) {

					sb.append("<tr>");
					sb.append("<td>" + referral.getDate().toString("HH:mm dd-MMM-yyyy") + "</td>");
					sb.append("<td>" + referral.getIpAddress() + "</td>");
					sb.append("<td>" + new LinkTag(referral.getReferrer(), StringHelper.toSnippet(referral.getReferrer(), 60, "...")) + "</td>");
					sb.append("</tr>");
				}

				sb.append("</table>");

                sb.append(new ResultsControl(context, results, new Link(ReferralsHandler.class, "recent")));

				return sb.toString();
			}

		});
		return page;
	}
}
