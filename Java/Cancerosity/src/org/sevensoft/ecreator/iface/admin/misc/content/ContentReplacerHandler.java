package org.sevensoft.ecreator.iface.admin.misc.content;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.misc.content.ContentReplacer;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 15 Jan 2007 10:24:57
 *
 */
@Path("admin-content-replacer.do")
public class ContentReplacerHandler extends AdminHandler {

	private String	target;
	private String	replacement;
	private boolean	categories;
	private boolean	items;
	private boolean	includeHtmlTags;
	private boolean	categoryContent;
	private boolean	itemContent;
	private boolean	categoryTitles;
	private boolean	itemTitles;

	public ContentReplacerHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Content helper", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Run replacer"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, "edit", "Return to settings"));
				sb.append("</div>");
			}

			private void contents() {

				sb.append(new AdminTable("Content replacer"));

				sb.append(new AdminRow("Target", "Enter text to search for.", new TextTag(context, "target", 20)));
				sb.append(new AdminRow("Replacement", "Enter the replacement text.", new TextTag(context, "replacement", 20)));

				sb
						.append(new AdminRow(
								"Include html tags",
								"If you set this to yes then the find/replace will also include HTML tags, otherwise they will be ignored. You will generally leave this unchecked.",
								new BooleanRadioTag(context, "includeHtmlTags", false)));

				sb.append(new AdminRow("Category content", "Set this to yes to find and replace text in category contents.", new BooleanRadioTag(
						context, "categoryContent", false)));

				sb.append(new AdminRow("Category titles", "Set this to yes to find and replace text in category titles.", new BooleanRadioTag(context,
						"categoryTitles", false)));

				sb.append(new AdminRow("Item content", "Set this to yes to find and replace text in item content.", new BooleanRadioTag(context,
						"itemContent", false)));

				sb.append(new AdminRow("Item titles", "Set this to yes to find and replace text in item titles.", new BooleanRadioTag(context,
						"itemTitles", false)));

				sb.append("</table>");

			}

			@Override
			public String toString() {

				sb.append(new FormTag(ContentReplacerHandler.class, "replace", "post"));

                commands();
				contents();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object replace() throws ServletException {

		if (target == null || replacement == null) {
			return main();
		}

		ContentReplacer replacer = new ContentReplacer(context, target, replacement);
		replacer.setItemContent(itemContent);
		replacer.setCategoryContent(categoryContent);
		replacer.setItemTitles(itemTitles);
		replacer.setCategoryTitles(categoryTitles);
		replacer.setIncludeHtmlTags(includeHtmlTags);

		int n = replacer.run();

		return new ActionDoc(context, "Find / replace has been processsed on " + n + " pages", new Link(ContentReplacerHandler.class));
	}
}
