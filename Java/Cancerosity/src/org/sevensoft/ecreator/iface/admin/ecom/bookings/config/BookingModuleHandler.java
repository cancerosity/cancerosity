package org.sevensoft.ecreator.iface.admin.ecom.bookings.config;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemTypeMenu;
import org.sevensoft.ecreator.iface.admin.misc.agreements.AgreementsHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.bookings.BookingModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.misc.agreements.Agreement;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sam
 */
@Path("admin-bookings-module.do")
public class BookingModuleHandler extends AdminHandler {

	private Agreement	agreement;
	private ItemType	itemType;

	public BookingModuleHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (itemType == null) {
			return index();
		}

		final BookingModule module = itemType.getBookingModule();

		AdminDoc doc = new AdminDoc(context, user, "Booking module", Tab.getItemTab(itemType));
		doc.setMenu(new EditItemTypeMenu(itemType));
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update booking module"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Agreement", "Set an agreement that the user must accept before they can completed the booking.", new SelectTag(
						context, "agreement", module.getAgreement(), Agreement.get(context), "-Select agreement-")
						+ " " + new LinkTag(AgreementsHandler.class, null, "Edit agreements").setTarget("_blank")));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(BookingModuleHandler.class, "save", "POST"));
				sb.append(new HiddenTag("itemType", itemType));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (itemType == null) {
			return index();
		}

		BookingModule module = itemType.getBookingModule();
		module.setAgreement(agreement);

		module.save();

		return new ActionDoc(context, "Booking module has been updated", new Link(BookingModuleHandler.class, null, "itemType", itemType));
	}
}
