package org.sevensoft.ecreator.iface.admin.items.reviews;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.CrossGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.TickGif;
import org.sevensoft.ecreator.model.items.reviews.Review;
import org.sevensoft.ecreator.model.items.reviews.ReviewSearcher;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 27-Feb-2006 16:28:46
 *
 */
@Path("admin-reviews-moderation.do")
public class ReviewModerationHandler extends AdminHandler {

	private Review	review;

	public ReviewModerationHandler(RequestContext context) {
		super(context);
	}

	public Object approve() throws ServletException {

		if (review == null) {
			return main();
		}

		review.approve();

		return new ActionDoc(context, "This review has been approved", new Link(ReviewModerationHandler.class));
	}

	public Object main() throws ServletException {

		ReviewSearcher searcher = new ReviewSearcher(context);
		final List<Review> reviews = searcher.execute();

		if (reviews.isEmpty()) {

			AdminDoc doc = new AdminDoc(context, user, "Moderation queue", null);
			doc.addBody(new Body() {

				@Override
				public String toString() {

					sb.append("You have no reviews awaiting approval");

					return sb.toString();
				}
			});
			return doc;

		} else {

			AdminDoc doc = new AdminDoc(context, user, "Moderation queue", null);
			doc.addBody(new Body() {

				@Override
				public String toString() {

					sb.append(new AdminTable("Reviews awaiting approval"));

					sb.append("<tr>");
					sb.append("<th>Id</th>");
					sb.append("<th>Date</th>");
					sb.append("<th>Item</th>");
					sb.append("<th>Action</th>");
					sb.append("</tr>");

					for (Review review : reviews) {

						sb.append("<tr>");

						// id
						sb.append("<td>" + new LinkTag(ReviewHandler.class, null, review.getIdString(), "review", review) + "</td>");

						// date created
						sb.append("<td>" + review.getDate().toDateString() + "</td>");

						// item type
						sb.append("<td>" + review.getItem().getName() + "</td>");

						// approval link
						sb.append("<td>");
						sb.append(new LinkTag(ReviewModerationHandler.class, "approve", new TickGif(), "review", review)
								.setConfirmation("Are you sure you want to approve this review"));

						sb.append(" ");

						sb.append(new LinkTag(ReviewModerationHandler.class, "reject", new CrossGif(), "review", review)
								.setConfirmation("Are you sure you want to reject this review"));
						sb.append("</td>");

						sb.append("</tr>");
					}

					sb.append("</table>");
					return sb.toString();
				}
			});
			return doc;
		}
	}

	public Object reject() throws ServletException {

		if (review == null) {
			return main();
		}

		review.reject();
		return new ActionDoc(context, "This review has been rejected", new Link(ReviewModerationHandler.class));
	}
}
