package org.sevensoft.ecreator.iface.admin.accounts.permissions;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.accounts.permissions.AccessGroup;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 7 Jan 2007 09:01:48
 *
 */
@Path("admin-accessgroups.do")
public class AccessGroupsHandler extends AdminHandler {

	private AccessGroup	accessGroup;
	private String		name;
	private int			limitedItemViews;
	private String		limitedItemViewsForward;
	private int			dailyProfileViewsLimit;
	private int			dailyPmLimit;

	public AccessGroupsHandler(RequestContext context) {
		super(context);
	}

	public AccessGroupsHandler(RequestContext context, AccessGroup accessGroup) {
		super(context);
		this.accessGroup = accessGroup;
	}

	public Object create() throws ServletException {

		if (name != null) {
			new AccessGroup(context, name);
		}

		return new ActionDoc(context, "A new access group has been created", new Link(AccessGroupsHandler.class));
	}

	public Object delete() throws ServletException {

		if (accessGroup == null) {
			return main();
		}

		accessGroup.delete();
		return new ActionDoc(context, "This access group has been deleted", new Link(AccessGroupsHandler.class));
	}

	public Object edit() throws ServletException {

		if (accessGroup == null) {
			return main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit access group", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update access group"));

				sb.append(new ButtonTag(AccessGroupsHandler.class, "delete", "Delete access group", "accessGroup", accessGroup)
						.setConfirmation("Are you sure you want to delete this access group?"));

				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to menu"));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Name", "Enter the name of this access group.", new TextTag(context, "name", accessGroup.getName(), 40)));

				if (Module.PrivateMessages.enabled(context)) {

					sb.append(new AdminRow("Daily pm limit", "Max number of private messages that can be sent per day.", new TextTag(context,
							"dailyPmLimit", accessGroup.getDailyPmLimit(), 6)));

				}

				sb.append(new AdminRow("Daily Profile Views Limit", "Max number of profiles that can be viewed per day.", new TextTag(context,
						"dailyProfileViewsLimit", accessGroup.getDailyProfileViewsLimit(), 6)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(AccessGroupsHandler.class, "save", "POST"));
				sb.append(new HiddenTag("accessGroup", accessGroup));
                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() {

		AdminDoc doc = new AdminDoc(context, user, "Access groups", null);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Access groups"));

				sb.append("<tr>");
				sb.append("<th>Name</th>");
				sb.append("<th>Delete</th>");
				sb.append("</tr>");

				for (AccessGroup accessGroup : AccessGroup.getAll(context)) {

					sb.append("<tr>");
					sb.append("<td>" + new LinkTag(AccessGroupsHandler.class, "edit", new SpannerGif(), "accessGroup", accessGroup) + " "
							+ accessGroup.getName() + "</td>");
					sb.append("<td>"
							+ new LinkTag(AccessGroupsHandler.class, "delete", new DeleteGif(), "accessGroup", accessGroup)
									.setConfirmation("Are you sure you want to delete this access group?") + "</td>");
					sb.append("</tr>");
				}

				sb.append(new FormTag(AccessGroupsHandler.class, "create", "post"));
				sb.append("<tr><td colspan='2'>Add new access group " + new TextTag(context, "name", 20) + " " + new SubmitTag("Add") + "</td></tr>");
				sb.append("</form>");

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;

	}

	public Object save() throws ServletException {

		if (accessGroup == null) {
			return main();
		}

		accessGroup.setName(name);
		accessGroup.setDailyProfileViewsLimit(dailyProfileViewsLimit);
		accessGroup.setDailyPmLimit(dailyPmLimit);
		accessGroup.save();

		return new ActionDoc(context, "This access group has been updated", new Link(AccessGroupsHandler.class, "edit", "accessGroup", accessGroup));
	}
}
