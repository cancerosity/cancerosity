package org.sevensoft.ecreator.iface.admin.system.scripts;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.model.system.scripts.Script;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;

import javax.servlet.ServletException;

/**
 * User: Tanya
 * Date: 24.06.2011
 */
@Path("admin-scripts.do")
public class ScriptsHandler extends AdminHandler {

    private transient Script scripts;

    private boolean loadjQuery;
    private boolean loadjQuery144;
    private boolean loadjQueryUi;
    private boolean loadThickbox;
    private boolean AC_RunActiveContent;
    private boolean loadEasyflashuploader;
    private boolean loadCookieControl;
    private boolean loadCookieControlWithRevoke;
    private boolean loadIosFiletag;

    public ScriptsHandler(RequestContext context) {
        super(context);
        this.scripts = Script.getInstance(context);
    }

    public Object main() throws ServletException {
        if (!isSuperman())
            return new DashboardHandler(context).main();

        AdminDoc page = new AdminDoc(context, user, "Loading Scripts settings", null);
        page.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update settings"));
                sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
                sb.append("</div>");
            }


            private void general() {

                sb.append(new AdminTable("General"));

                sb.append(new AdminRow("Load jQuery", "", new BooleanRadioTag(context, "loadjQuery", scripts.isLoadjQuery())));

                sb.append(new AdminRow("Load jQuery version 1.4.4", "", new BooleanRadioTag(context, "loadjQuery144", scripts.isLoadjQuery144())));

                sb.append(new AdminRow("Load jQuery UI (Requires jQuery)", "", new BooleanRadioTag(context, "loadjQueryUi", scripts.isLoadjQueryUi())));

                sb.append(new AdminRow("Load Thickbox", "", new BooleanRadioTag(context, "loadThickbox", scripts.isLoadThickbox())));

                sb.append(new AdminRow("Load AC_RunActiveContent", "", new BooleanRadioTag(context, "AC_RunActiveContent", scripts.isAC_RunActiveContent())));

                sb.append(new AdminRow("Load Easy Flash Uploader", "", new BooleanRadioTag(context, "loadEasyflashuploader", scripts.isLoadEasyflashuploader())));

                sb.append("</table>");
            }

            private void cookie() {

                sb.append(new AdminTable("Cookie support"));

                sb.append(new AdminRow("Load Cookie Control", "Choose one of Cookie Controls", new BooleanRadioTag(context, "loadCookieControl", scripts.isLoadCookieControl())));
                sb.append(new AdminRow("Load Cookie Control with Revoke", "Choose one of Cookie Controls", new BooleanRadioTag(context, "loadCookieControlWithRevoke", scripts.isLoadCookieControlWithRevoke())));

                sb.append("</table>");

            }

             private void ios() {

                sb.append(new AdminTable("iOS"));

                sb.append(new AdminRow("Load iOS filetag support", "", new BooleanRadioTag(context, "loadIosFiletag", scripts.isLoadIosFiletag())));

                sb.append("</table>");

            }

            @Override
            public String toString() {

                sb.append(new FormTag(ScriptsHandler.class, "save", "POST"));

                commands();
                general();

                if (Module.CookieControl.enabled(context)){
                    cookie();
                }

                ios();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return page;
    }

    public Object save() throws ServletException {

        if (!isSuperman()) {
            return new DashboardHandler(context).main();
        }

        scripts.setLoadjQuery(loadjQuery);
        scripts.setLoadjQuery144(loadjQuery144);
        scripts.setLoadjQueryUi(loadjQueryUi);
        scripts.setLoadThickbox(loadThickbox);
        scripts.setAC_RunActiveContent(AC_RunActiveContent);
        scripts.setLoadEasyflashuploader(loadEasyflashuploader);
        scripts.setLoadCookieControl(loadCookieControl);
        scripts.setLoadCookieControlWithRevoke(loadCookieControlWithRevoke);
        scripts.setLoadIosFiletag(loadIosFiletag);

        scripts.save();

        addMessage("Scripts settings updated");
        clearParameters();
        return main();
    }


}
