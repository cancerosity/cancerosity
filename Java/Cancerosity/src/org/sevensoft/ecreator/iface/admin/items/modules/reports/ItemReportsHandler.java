package org.sevensoft.ecreator.iface.admin.items.modules.reports;

import java.util.Map;

import javax.servlet.ServletException;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.reports.Report;
import org.sevensoft.ecreator.model.items.reports.Sort;
import org.sevensoft.ecreator.model.system.users.privileges.Privilege;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 23 Sep 2006 09:36:42
 *
 */
@Path("admin-items-reports.do")
public class ItemReportsHandler extends AdminHandler {

	private Report	report;
	private ItemType	itemType;
	private String	name;
	private Attribute	y;
	private Attribute	x;
	private Date	month;
	private Sort	sort;
	private Attribute	dateRange;
	private String	yLabel;
	private String	xLabel;

	public ItemReportsHandler(RequestContext context) {
		super(context);
	}

	public Object add() throws ServletException {

		if (itemType != null)
			new Report(context, itemType, "new report");

		return main();
	}

	public Object delete() throws ServletException {

		if (report == null)
			return main();

		report.delete();
		return main();

	}

	public Object edit() throws ServletException {

		if (!Privilege.Reports.yes(user, context))
			return index();

		if (report == null)
			return main();

		AdminDoc doc = new AdminDoc(context, user, "Edit report", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update report"));

				sb.append(new ButtonTag(ItemReportsHandler.class, "delete", "Delete report", "report", report)
						.setConfirmation("Are you sure you want to delete this report?"));

				sb.append(new ButtonTag(ItemReportsHandler.class, null, "Return to menu"));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("Report details"));

				sb.append(new AdminRow("Name", "Enter a name for this report", new TextTag(context, "name", report.getName(), 40)));
				sb.append(new AdminRow("Item type", "The type this report runs on", report.getItemType().getName()));

				sb.append(new AdminRow("X axis", "Choose the grouping attribute for the x axis", new SelectTag(context, "x", report.getX(), Attribute
						.getSelectionMap(context), "-Select x axis-")));

				sb.append(new AdminRow("X axis label", "Set a custom name for the x axis", new TextTag(context, "xLabel", report.getXLabel(), 40)));

				sb.append(new AdminRow("Y axis", "Set the numerical summing attribute for the y axis", new SelectTag(context, "y", report.getY(),
						Attribute.getSelectionMap(context), "-Select y axis-")));

				sb.append(new AdminRow("Y axis label", "Set a custom name for the y axis", new TextTag(context, "yLabel", report.getYLabel(), 40)));

				sb.append(new AdminRow("Sort", "Set the sort type", new SelectTag(context, "sort", report.getSort(), Sort.values())));

				sb.append(new AdminRow("Date range", "Set the attribute for date range, or leave blank to use date created.", new SelectTag(context,
						"dateRange", report.getDateRange(), Attribute.getSelectionMap(context), "Date created")));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(ItemReportsHandler.class, "save", "post"));
				sb.append(new HiddenTag("report", report));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		if (!Privilege.Reports.yes(user, context))
			return index();

		AdminDoc doc = new AdminDoc(context, user, "Item reports", null);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Reports"));

				for (Report report : Report.get(context)) {

					sb.append("<tr>");
					sb.append("<td width='60'>" + report.getId() + "</td>");
					sb.append("<td>"
							+ new LinkTag(ItemReportsHandler.class, "edit", new ImageTag("files/graphics/admin/spanner.gif"), "report", report)
							+ " " + report.getName() + "</td>");
					sb.append("<td>" + new ButtonTag(ItemReportsHandler.class, "run", "Run report", "report", report) + "</td>");
					sb.append("</tr>");
				}

				sb.append(new FormTag(ItemReportsHandler.class, "add", "post"));
				sb.append("<tr><td colspan='3'>Add report for " + new SelectTag(context, "itemType", null, ItemType.get(context)) + " "
						+ new SubmitTag("Add") + "</td></tr>");
				sb.append("</form>");

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object run() throws ServletException {

		if (!Privilege.Reports.yes(user, context))
			return index();

		if (report == null)
			return main();

		if (month == null)
			month = new Date().beginMonth();

		final Map<String, Double> map = report.getDataSet(month);

		AdminDoc doc = new AdminDoc(context, user, "Report: " + report.getName(), null);
		doc.addBody(new Body() {

            private void commands() {
                sb.append("<div align='center' class='actions'>");
                sb.append(new ButtonTag(ItemReportsHandler.class, null, "Return to menu"));
                sb.append("</div>");
            }

			@Override
			public String toString() {

                commands();

				sb.append(new AdminTable(report.getName() + " for " + month.toString("MMMM yyyy")));

				SelectTag tag = new SelectTag(context, "month");
				tag.setAutoSubmit();
				Date date = new Date().beginMonth();
				for (int n = 0; n < 12; n++) {
					tag.addOption(date, date.toString("MMMM yyyy"));
					date = date.removeMonths(1);
				}

				sb.append(new FormTag(ItemReportsHandler.class, "run", "get"));
				sb.append(new HiddenTag("report", report));
				sb.append("<tr><td colspan='2'>Change report month: " + tag + "</td></tr>");
				sb.append("</form>");

				sb.append("<tr><th>" + report.getXLabel() + "</th><th>" + report.getYLabel() + "</th></tr>");

				for (Map.Entry<String, Double> entry : map.entrySet()) {

					sb.append("<tr><td>" + entry.getKey() + "</td><td>" + entry.getValue() + "</td></tr>");
				}

				sb.append("</table>");

				commands();

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (!Privilege.Reports.yes(user, context))
			return index();

		if (report == null)
			return main();

		report.setName(name);
		report.setX(x);
		report.setXLabel(xLabel);
		report.setYLabel(yLabel);
		report.setDateRange(dateRange);
		report.setSort(sort);
		report.setY(y);
		report.save();

		clearParameters();
		addMessage("Report has been updated");
		return edit();
	}

}
