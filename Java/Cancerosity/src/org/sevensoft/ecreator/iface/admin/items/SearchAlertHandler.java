package org.sevensoft.ecreator.iface.admin.items;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.search.alerts.SearchAlertRunner;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 7 Mar 2007 10:15:45
 *
 */
@Path("admin-search-alerts.do")
public class SearchAlertHandler extends AdminHandler {

	private Item	item;

	public SearchAlertHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {
		return null;
	}

	public Object run() throws ServletException {

		if (item == null) {
			return index();
		}

		final SearchAlertRunner runner = new SearchAlertRunner(context, item);
		runner.run();

		AdminDoc doc = new AdminDoc(context, user, "Search alerts ran", Tab.getItemTab(item));
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(runner.getCount() + " search alerts have been run.<br/>");
				sb.append(new LinkTag(ItemHandler.class, "edit", "Click here", "item", item));
				sb.append(" to return to the item");

				return sb.toString();
			}
		});
		return doc;
	}
}
