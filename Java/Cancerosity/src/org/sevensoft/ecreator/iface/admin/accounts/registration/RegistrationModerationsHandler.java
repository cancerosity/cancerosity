package org.sevensoft.ecreator.iface.admin.accounts.registration;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.CrossGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.TickGif;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.EmailTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 15 Dec 2006 15:27:29
 *
 */
@Path("admin-registration-moderation.do")
public class RegistrationModerationsHandler extends AdminHandler {

	private Item	account;

	public RegistrationModerationsHandler(RequestContext context) {
		super(context);
	}

	public Object approve() throws ServletException {

		if (account == null) {
			return main();
		}

		account.approveAccount();
		return new ActionDoc(context, "This account has been approved", new Link(RegistrationModerationsHandler.class));
	}

	@Override
	public Object main() throws ServletException {

		// get a list of all accounts awaiting moderation
		ItemSearcher searcher = new ItemSearcher(context);
		searcher.setAwaitingModeration(true);
		searcher.setIncludeHidden(true);
		searcher.setSearchableOnly(false);
		searcher.setVisibleOnly(false);
		searcher.setItemModule(ItemModule.Account);
		final List<Item> accounts = searcher.getItems();

		AdminDoc doc = new AdminDoc(context, user, "Accounts awaiting moderation", null);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				StringBuilder sb = new StringBuilder();

				if (accounts.isEmpty()) {

					sb.append("You have no accounts currently awaiting moderation");

				} else {

					sb.append(new AdminTable("Accounts awaiting moderation"));

					sb.append("<tr>");
					sb.append("<th>Name</th>");
					sb.append("<th>Email</th>");
					sb.append("<th>Approve</th>");
					sb.append("<th>Reject</th>");
					sb.append("</tr>");

					for (Item account : accounts) {

						sb.append("<tr>");
						sb.append("<td>" + new LinkTag(ItemHandler.class, "edit", account.getName(), "item", account) + "</td>");
						sb.append("<td>" + new EmailTag(account.getEmail()) + "</td>");
						sb.append("<td>" + new LinkTag(RegistrationModerationsHandler.class, "approve", new TickGif(), "account", account)
								+ "</td>");
						sb.append("<td>"
								+ new LinkTag(RegistrationModerationsHandler.class, "reject", new CrossGif(), "account", account)
										.setConfirmation("Are you sure you want to reject this account") + "</td>");
						sb.append("</tr>");
					}

					sb.append("</table>");
				}

				return sb.toString();
			}

		});
		return doc;
	}

	public Object reject() throws ServletException {

		if (account == null) {
			return main();
		}

		account.rejectAccount();
		return new ActionDoc(context, "This account has been rejected", new Link(RegistrationModerationsHandler.class));
	}
}
