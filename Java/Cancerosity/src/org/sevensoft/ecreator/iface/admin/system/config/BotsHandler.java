package org.sevensoft.ecreator.iface.admin.system.config;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.accounts.subscriptions.bots.SubscriptionExpiryBot;
import org.sevensoft.ecreator.model.items.bots.ExpiryBotRunner;
import org.sevensoft.ecreator.model.misc.Interval;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;

/**
 * @author sks 2 Feb 2007 08:02:22
 *
 */
@Path("admin-bots.do")
public class BotsHandler extends AdminHandler {

	public BotsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Runners", null);
		doc.addBody(new Body() {

			private void database() {

				sb.append(new AdminTable("Deleter"));

				sb.append(new AdminRow("Subscription expiry", "Click to run the subscription expiry bot which will clear any expired subscriptions.",
						new ButtonTag(BotsHandler.class, "subscriptionExpiryBot", "Subscription expiry bot")));

				sb.append(new AdminRow("Subscription reminders",
						"Click to run the subscription reminder bot which will send out any subscription reminders "
								+ "to people whose subscription will expire soon.", new ButtonTag(BotsHandler.class, "subscriptionReminderBot",
								"Subscription reminder bot")));

				sb.append(new AdminRow("Expiry bots", "Click to run the subscription reminder bot which will send out any subscription reminders "
						+ "to people whose subscription will expire soon.", new ButtonTag(BotsHandler.class, "expiryBots", "Run expiry bots")));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				database();

				return sb.toString();
			}
		});

		return doc;
	}

	public Object subscriptionExpiryBot() throws ServletException {

		int cancelled = new SubscriptionExpiryBot(context).run().size();
		addMessage("Subscription expiry bot was run successfully and " + cancelled + " accounts were expired.");
		return main();
	}

	public Object expiryBots() throws ServletException {

		new ExpiryBotRunner(context, Interval.Daily).run();
		new ExpiryBotRunner(context, Interval.Hourly).run();
		addMessage("All expiry boys were run successfully");
		return main();
	}

	public Object subscriptionReminderBot() throws ServletException {

		new SubscriptionExpiryBot(context).run();
		addMessage("Subscription reminder bot was run successfully");
		return main();
	}

}
