package org.sevensoft.ecreator.iface.admin.feeds.bespoke.news;

import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.Feed;
import org.sevensoft.ecreator.model.feeds.bespoke.news.PdoNewsFeed;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 14 Feb 2007 14:11:22
 *
 */
@Path("admin-feeds-pdo.do")
public class PdoNewsFeedHandler extends FeedHandler {

	private PdoNewsFeed	feed;
	private Attribute		authorAttribute;
	private Attribute		dateAttribute;
	private ItemType		itemType;

	public PdoNewsFeedHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Feed getFeed() {
		return feed;
	}

	@Override
	protected void saveSpecific() {

		feed.setAuthorAttribute(authorAttribute);
		feed.setItemType(itemType);

		feed.save();
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("PDO News feed options"));

		sb.append(new AdminRow("Item type", "Select the item type to import to.", new SelectTag(context, "itemType", feed.getItemType(), ItemType
				.getSelectionMap(context), "-None set-")));

		if (feed.hasItemType()) {

			sb.append(new AdminRow("Author attribute", new SelectTag(context, "authorAttribute", feed.getAuthorAttribute(), feed.getItemType()
					.getAttributes(), "-None-")));

		}

		sb.append("</table>");
	}

}
