package org.sevensoft.ecreator.iface.admin.ecom.promotions;

import javax.servlet.ServletException;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.ecom.ShoppingMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.ecom.promotions.Promotion;
import org.sevensoft.ecreator.model.ecom.promotions.Promotion.PromotionType;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.date.DateTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks
 */
@Path("admin-promotions.do")
public class PromotionHandler extends AdminHandler {

	private String		name;
	private Promotion		promotion;
	private Money		minSpend;
	private String		freeItem;
	private Date		startDate;
	private int			qty;
	private PromotionType	type;
	private Date		endDate;
	private Category		category;
	private Category		addCategory;
	private Amount		totalDiscount;
	private Amount		itemsDiscount;
	private Money		fixedPrice;
	private Item		item;
	private String		addItem;
	private int	maxUsesPerSession;

	public PromotionHandler(RequestContext context) {
		super(context);
	}

	public PromotionHandler(RequestContext context, Category category) {
		super(context);
	}

	public Object create() throws ServletException {

		promotion = new Promotion(context, "-new promotion-");
		return edit();
	}

	public Object delete() throws ServletException {

		if (promotion == null) {
			return main();
		}

		promotion.delete();

		return main();
	}

	public Object edit() throws ServletException {

		if (promotion == null) {
			return main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit promotion", Tab.Orders);
		doc.setMenu(new ShoppingMenu());
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update promotion"));
				sb.append(new ButtonTag(PromotionHandler.class, "delete", "Delete promotion", "promotion", promotion)
						.setConfirmation("Are you sure you want to delete this promotion?"));
				sb.append(new ButtonTag(PromotionHandler.class, null, "Return to promotions menu"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Name", "Enter a description of this promotion.", new TextTag(context, "name", promotion.getName(), 40)));

				DateTag startTag = new DateTag(context, "startDate", promotion.getStartDate());
				startTag.setYearRange(4);

				DateTag endTag = new DateTag(context, "endDate", promotion.getEndDate());
				endTag.setYearRange(4);

				sb
						.append(new AdminRow(
								"Promotion dates",
								"Enter the start and end date for this promotion or leave them blank for the promotion to be valid until you manually delete it.",
								startTag + " to " + endTag));

				sb.append(new AdminRow("Max uses per session", "How many times this promotion can be used per set of purchases.", new TextTag(context,
						"maxUsesPerSession", promotion.getMaxUsesPerSession(), 4)));

				sb.append("</table>");
			}

			private void outcome() {

				sb.append(new AdminTable("Outcome"));

				SelectTag typeTag = new SelectTag(context, "type", promotion.getType());
				typeTag.addOptions(Promotion.PromotionType.values());
				sb.append(new AdminRow("Type", "The type of discount to apply", typeTag));

				switch (promotion.getType()) {

				case OrderDiscount:

					sb
							.append(new AdminRow(
									"Discount",
									"Enter the discount the customer will receive. This can be a fixed value or a percentage. A percentage will be calculated against the total order.",
									new TextTag(context, "totalDiscount", promotion.getTotalDiscount(), 12)));

					break;

				case FixedPrice:

					sb.append(new AdminRow("Fixed price", "Enter the fixed price that will be charged for the quantity of items", new TextTag(
							context, "fixedPrice", promotion.getFixedPrice(), 12)));
					break;

				case FreeItems:

					for (Item item : promotion.getFreeItems()) {

						sb.append(new AdminRow("Free item", "Item included at zero price to the customer", item.getName() + " " +
								new ButtonTag(PromotionHandler.class, "removeFreeItem", "Remove", "promotion", promotion, "item", item)));

					}

					sb.append(new AdminRow("Free item", "Add a new free item by entering the ID or reference", new TextTag(context, "freeItem", 12)));
					break;

				case ItemsDiscount:

					sb.append(new AdminRow("Items discount",
							"A discount against the items. A percentage discount is calculated against the total of all the applicable items.",
							new TextTag(context, "itemsDiscount", promotion.getItemsDiscount(), 12)));
					break;

				}

				sb.append("</table>");
			}

			private void qualification() {

				sb.append(new AdminTable("Qualification"));

				sb.append(new AdminRow("Qty to qualify", "The number of items that must be bought for this promotion to be valid.", new TextTag(
						context, "qty", promotion.getQty(), 4)));

				sb.append(new AdminRow("Min order total",
						"This is the minimum amount a customer must spend on applicable items for this promotion to be valid.", new TextTag(
								context, "minSpend", promotion.getMinSpend(), 12)));

				sb.append("<tr><th colspan='2'>" + "If you add categories here than only items in these categories will be "
						+ "included as counting towards this promotion.</th></tr>");

				for (Category category : promotion.getCategories()) {

					sb.append(new AdminRow("Category", null, category.getName() + " " +
							new ButtonTag(PromotionHandler.class, "removeCategory", "Remove", "promotion", promotion, "category", category)));

				}

				sb.append(new AdminRow("Category", "Select a category to add", new SelectTag(context, "addCategory", null, Category
						.getSimpleMap(context), "-Select category to add-")));

				sb.append("<tr><th colspan='2'>If you add items here, then this promotion will only be valid on those items.</th></tr>");

				for (Item item : promotion.getItems()) {

					sb.append(new AdminRow("Item", "Existing valid item", item.getName() + " " +
							new ButtonTag(PromotionHandler.class, "removeItem", "Remove", "promotion", promotion, "item", item)));

				}

				sb.append(new AdminRow("Item", "Add item by entering the ID or reference.", new TextTag(context, "addItem", 12)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(PromotionHandler.class, "save", "post"));
				sb.append(new HiddenTag("promotion", promotion));

                commands();
				general();
				qualification();
				outcome();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc page = new AdminDoc(context, user, "Promotions", Tab.Orders);
		page.setMenu(new ShoppingMenu());
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Promotions"));

				sb.append("<tr>");
				sb.append("<th>Name</th>");
				sb.append("<th>Edit</th>");
				sb.append("</tr>");

				int n = 1;
				for (Promotion promotion : Promotion.get(context)) {

					sb.append("<tr>");
					sb.append("<td>" + promotion.getName() + "</td>");
					sb.append("<td>" + new LinkTag(PromotionHandler.class, "edit", "edit", "promotion", promotion) + "</td>");
					sb.append("</tr>");

					n++;
				}

				sb.append("<tr><td colspan='2'>Create a new promotion " + new ButtonTag(PromotionHandler.class, "create", "Submit") + "</td></tr>");
				sb.append("</table>");

				return sb.toString();
			}

		});
		return page;
	}

	public Object removeCategory() throws ServletException {

		if (promotion == null || category == null) {
			return main();
		}

		promotion.removeCategory(category);
		clearParameters();
		return edit();
	}

	public Object removeFreeItem() throws ServletException {

		if (promotion == null || item == null) {
			return main();
		}

		promotion.removeFreeItem(item);
		clearParameters();
		return edit();
	}

	public Object removeItem() throws ServletException {

		if (promotion == null || item == null) {
			return main();
		}

		promotion.removeItem(item);
		clearParameters();
		return edit();
	}

	public Object save() throws ServletException {

		if (promotion == null)
			return main();

		test(new RequiredValidator(), "name");
		if (hasErrors())
			return main();

		promotion.setName(name);
		promotion.setType(type);
		promotion.setQty(qty);
		promotion.setMinSpend(minSpend);
		promotion.setEndDate(endDate);
		promotion.setStartDate(startDate);
		promotion.setTotalDiscount(totalDiscount);
		promotion.setItemsDiscount(itemsDiscount);
		promotion.setFixedPrice(fixedPrice);
		promotion.setMaxUsesPerSession(maxUsesPerSession);

		if (addCategory != null) {
			promotion.addCategory(addCategory);
		}

		if (addItem != null) {

			Item item = EntityObject.getInstance(context, Item.class, addItem);

			if (item == null) {
				item = Item.getByRef(context, null, addItem);
			}

			if (item != null) {
				promotion.addItem(item);
			}
		}

		if (freeItem != null) {

			Item item = EntityObject.getInstance(context, Item.class, freeItem);

			if (item == null) {
				item = Item.getByRef(context, null, freeItem);
			}

			if (item != null) {
				promotion.addFreeItem(item);
			}
		}

		promotion.save();

		addMessage("Promotion updated.");
		clearParameters();
		return edit();
	}
}