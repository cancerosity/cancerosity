package org.sevensoft.ecreator.iface.admin.ecom.shopping;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.attributes.renderers.OwnerAttributesPanel;
import org.sevensoft.ecreator.model.ecom.shopping.CheckoutSettings;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;

/**
 * @author sks 03-Oct-2005 14:25:41
 * 
 */
@Path("admin-settings-checkout.do")
public class CheckoutSettingsHandler extends AdminHandler {

	private String					newAttributes;
	private final transient CheckoutSettings	checkoutSettings;
	private String					newAttributesSection;
	private int					newAttributesPage;

	public CheckoutSettingsHandler(RequestContext context) {
		super(context);
		this.checkoutSettings = CheckoutSettings.getInstance(context);
	}

	@Override
	public Object main() {

		AdminDoc doc = new AdminDoc(context, user, "Checkout settings", Tab.Orders);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update checkout settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(CheckoutSettingsHandler.class, "save", "post"));

                commands();
				sb.append(new OwnerAttributesPanel(context, checkoutSettings));
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() {

		checkoutSettings.save();

		checkoutSettings.addAttributes(newAttributes, newAttributesSection, newAttributesPage);

		addMessage("Checkout settings updated");
		clearParameters();
		return main();
	}
}
