package org.sevensoft.ecreator.iface.admin.design.markup;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.design.marker.Marker;
import org.sevensoft.ecreator.model.design.marker.MarkerHelper;
import org.sevensoft.ecreator.model.design.marker.MarkerRenderer;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 4 Aug 2006 12:30:49
 *
 */
@Path("admin-markup-reference.do")
public class MarkerReferenceHandler extends AdminHandler {

	public MarkerReferenceHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (!isSuperman()) {
			return new DashboardHandler(context).main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Markup reference", null);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Marker reference"));

				Collection<Marker> markers = MarkerRenderer.getMarkers();
				final SortedSet set = new TreeSet(new Comparator<Marker>() {

					public int compare(Marker o1, Marker o2) {
						return o1.getClass().getSimpleName().compareTo(o2.getClass().getSimpleName());
					}
				});
				set.addAll(markers);

				for (Marker marker : markers) {

					sb.append("<tr>");

					String regexString;
					Object regex = marker.getRegex();
					if (regex instanceof Object[]) {
						regexString = Arrays.toString((Object[]) regex).replace("[", "").replace("]", "");
					} else {
						regexString = regex.toString();
					}

					if (marker instanceof MarkerHelper) {

						MarkerHelper mh = (MarkerHelper) marker;

						sb.append("<td>" + regexString + "</td>");
						sb.append("<td>" + mh.getDescription() + "</td>");
						sb.append("<td>" + mh.getParams() + "</td>");

					} else {

						sb.append("<td>" + regexString + "</td>");
						sb.append("<td></td><td></td>");

					}

					sb.append("</tr>");
				}

				sb.append("</table>");
				return sb.toString();
			}

		});
		return doc;
	}
}
