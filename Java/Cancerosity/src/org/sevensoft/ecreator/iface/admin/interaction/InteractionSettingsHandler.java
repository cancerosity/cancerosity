package org.sevensoft.ecreator.iface.admin.interaction;

import java.util.Map;

import javax.servlet.ServletException;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.accounts.permissions.Domain;
import org.sevensoft.ecreator.model.accounts.permissions.DomainUtil;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionCollections;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.accounts.permissions.panels.PermissionsPanel;
import org.sevensoft.ecreator.model.interaction.InteractionSettings;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks
 */
@Path("admin-interaction-settings.do")
public class InteractionSettingsHandler extends AdminHandler {

	private final transient InteractionSettings	interactionSettings;
	private MultiValueMap<String, PermissionType>	permissions;
	private Map<String, Integer>				dailyPmLimits;
	private Map<String, Integer>				dailyProfileViewsLimits;

	public InteractionSettingsHandler(RequestContext context) {
		super(context);
		this.interactionSettings = InteractionSettings.getInstance(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Interaction settings", null);
		doc.addBody(new Body() {

			private void commands() {
				sb.append("<div class='action'>" + new SubmitTag("Update settings") + "</div>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(InteractionSettingsHandler.class, "save", "POST"));

                commands();
				sb.append(new AdminTable("Interaction restrictions"));

				sb.append("<tr>");
				sb.append("<th>Group</th>");
				sb.append("<th>Daily pm limit</th>");
				sb.append("<th>Daily profile limit</th>");
				sb.append("</tr>");

				for (Domain domain : DomainUtil.get(context)) {

					sb.append("<tr>");
					sb.append("<th>" + domain.getDomainName() + "</th>");
					sb.append("<th>" + new TextTag(context, "dailyPmLimits_" + domain.getFullId(), domain.getDailyPmLimit(), 4) + "</th>");
					sb.append("<th>" + new TextTag(context, "dailyProfileViewsLimits_" + domain.getFullId(), domain.getDailyProfileViewsLimit(), 4)
							+ "</th>");
					sb.append("</tr>");
				}

				sb.append("</table>");

				if (Module.Permissions.enabled(context)) {
					sb.append(new PermissionsPanel(context, null, PermissionCollections.getInteraction(context)));
				}

				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		DomainUtil.savePermissions(context, null, permissions, PermissionCollections.getInteraction(context));

		for (Domain domain : DomainUtil.get(context)) {

			Integer i = dailyPmLimits.get(domain.getFullId());
			if (i != null) {
				domain.setDailyPmLimit(i);
			}

			i = dailyProfileViewsLimits.get(domain.getFullId());
			if (i != null) {
				domain.setDailyProfileViewsLimit(i);
			}

			domain.save();
		}

		clearParameters();
		return main();
	}
}
