package org.sevensoft.ecreator.iface.admin.crm.forms.googleform;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.crm.forms.FormHandler;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.model.crm.forms.googleform.GoogleFormField;
import org.sevensoft.ecreator.model.crm.forms.googleform.GoogleForm;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;

import javax.servlet.ServletException;

/**
 * User: Tanya
 * Date: 14.11.2011
 */
@Path("admin-forms-fields-google.do")
public class GoogleFormFieldHandler extends AdminHandler {

    private GoogleFormField field;
    private GoogleForm form;
    private int columnID;

    public GoogleFormFieldHandler(RequestContext context) {
        super(context);
    }

    public Object main() throws ServletException {
        if (field == null) {
            return index();
        }

        AdminDoc doc = new AdminDoc(context, user, "Edit form field", Tab.Extras);
        if (miscSettings.isAdvancedMode()) {
            doc.setMenu(new ExtrasMenu());
        }
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update field"));
                sb.append(new ButtonTag(GoogleFormFieldHandler.class, "delete", "Delete field", "field", field)
                        .setConfirmation("If you delete this field it cannot be restored. Continue?"));
                sb.append(new ButtonTag(FormHandler.class, "edit", "Return to form", "form", field.getGoogleForm().getForm()));
                sb.append("</div>");
            }

            private void general() {

                sb.append(new AdminTable("Field"));

                sb.append(new AdminRow("Form Field", "", new TextTag(context, "formField", field.getFormField().getName()).setDisabled(true)));

                sb.append(new AdminRow("Google form Column",
                        "Enter the number of column where data of the field should be post to.", new TextTag(context,
                                "columnID", field.getGoogleFormColumn(), 10)));

//				SelectTag typeTag = new SelectTag(context, "type", field.getType());
//				typeTag.addOptions(FieldType.values());

                sb.append("</table>");

            }

            @Override
            public String toString() {

                sb.append(new FormTag(GoogleFormFieldHandler.class, "save", "POST"));
                sb.append(new HiddenTag("field", field));

                commands();
                general();
                commands();

                sb.append("</form>");
                return sb.toString();
            }
        });
        return doc;
    }

    public Object save() throws ServletException {

        if (field == null) {
            return main();
        }

        field.setGoogleFormColumn(columnID);

        field.save();

        clearParameters();
        addMessage("Changes saved.");
        return main();
    }

    public Object delete() throws ServletException {

        if (field == null) {
            return main();
        }

        field.delete();
        addMessage("The field  has been deleted.");
        return new FormHandler(context, field.getGoogleForm().getForm()).edit();
    }

}
