package org.sevensoft.ecreator.iface.admin.stats.panels;

import java.util.List;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.stats.visits.DateHelper;
import org.sevensoft.ecreator.model.stats.visits.Session;
import org.sevensoft.ecreator.model.stats.visits.View;
import org.sevensoft.ecreator.model.stats.visits.Visitor;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 11 Apr 2007 08:57:00
 *
 */
public class ClickPathPanel extends Panel {

	private Visitor		visitor;
	private List<Session>	sessions;

	public ClickPathPanel(RequestContext context, Visitor visitor) {
		super(context);
		this.visitor = visitor;

		// get sessions for this visitor
		this.sessions = visitor.getSessions();
	}

	@Override
	public void makeString() {

		sb.append(new AdminTable("Click path"));

		sb.append("<tr>");
		sb.append("<th>Date / Time</th>");
		sb.append("<th>Duration</th>");
		sb.append("<th>Page</th>");
		sb.append("<th>User agent</th>");
		sb.append("</tr>");

		for (Session session : sessions) {

			sb.append("<tr>");
			sb.append("<th colspan='4'>New session");
			if (session.hasReferrer()) {
				sb.append(" referred from " + new LinkTag(session.getReferrer()));
			}
			sb.append("</th>");
			sb.append("</tr>");

			View lastView = null;
			for (View view : session.getClickPath()) {

				String duration;

				/*
				 * If this was the last page of a session then do not show a duration.
				 * A view will be the last page if it is the first one in the list, so lastView would be null
				 */
				if (lastView == null) {

					duration = "--";

				} else {

					duration = DateHelper.millisToString(lastView.getTimestamp() - view.getTimestamp());
				}

				sb.append("<tr>");
				sb.append("<td>" + new DateTime(view.getTimestamp()).toString("MMM-dd HH:mm") + "</td>");
				sb.append("<td>" + duration + "</td>");
				sb.append("<td>");

				if (view.hasPageName()) {
					sb.append(view.getPageName() + "<br/>");
				}

				sb.append(new LinkTag(view.getPageUrl()));
				sb.append("</td>");

				sb.append("<td>" + view.getUserAgent() + "</td>");
				sb.append("</tr>");

				lastView = view;
			}
		}

		sb.append("</table>");
	}
}
