package org.sevensoft.ecreator.iface.admin.items;

import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 20 Dec 2006 16:58:47
 *
 */
public class ItemTypeSelectTag {

	private final String		name;
	private final RequestContext	context;
	private final Object		value;
	private String			any;

	public ItemTypeSelectTag(RequestContext context, String name, Object value, String any) {
		this.context = context;
		this.name = name;
		this.value = value;
		this.any = any;
	}

	@Override
	public String toString() {

		SelectTag tag = new SelectTag(context, name, value);
		tag.addOptions(ItemType.getSelectionMap(context));
		tag.setAny(any);

		return tag.toString();
	}

}
