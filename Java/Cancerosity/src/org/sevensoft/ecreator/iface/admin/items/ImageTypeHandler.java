package org.sevensoft.ecreator.iface.admin.items;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.media.images.ImageType;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 1 Apr 2007 00:40:33
 *
 */
@Path("admin-immagini-types.do")
public class ImageTypeHandler extends AdminHandler {

	private ItemType	itemType;
	private String	name;
	private ImageType	imageType;

	public ImageTypeHandler(RequestContext context) {
		super(context);
	}

	public Object delete() throws ServletException {

		if (imageType == null) {
			return index();
		}

		itemType = imageType.getItemType();
		itemType.getMediaModule().removeImageType(imageType);

		return new ActionDoc(context, "This image type has been deleted", new Link(ItemTypeHandler.class, "edit", "itemType", itemType));

	}

	@Override
	public Object main() throws ServletException {

		if (imageType == null) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit external link", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update image type"));
				sb.append(new ButtonTag(ItemTypeHandler.class, "edit", "Return to item type", "itemType", imageType.getItemType()));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("Image type"));

				sb.append(new AdminRow("Name", "Enter a label for this image type.", new TextTag(context, "imageType", imageType.getName(), 20)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(ImageTypeHandler.class, "save", "post"));
				sb.append(new HiddenTag("imageType", imageType));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object moveDown() throws ServletException {

		if (imageType == null) {
			return index();
		}

		itemType = imageType.getItemType();
		EntityUtil.moveDown(imageType, itemType.getMediaModule().getImageTypes());

		return new ItemTypeHandler(context, itemType).edit();
	}

	public Object moveUp() throws ServletException {

		if (imageType == null) {
			return index();
		}

		itemType = imageType.getItemType();
		EntityUtil.moveUp(imageType, itemType.getMediaModule().getImageTypes());

		return new ItemTypeHandler(context, itemType).edit();
	}

	public Object save() throws ServletException {

		if (imageType == null) {
			return index();
		}

		imageType.setName(name);
		imageType.save();

		return new ActionDoc(context, "This image type has been updated", new Link(ImageTypeHandler.class, null, "imageType", imageType));
	}

}
