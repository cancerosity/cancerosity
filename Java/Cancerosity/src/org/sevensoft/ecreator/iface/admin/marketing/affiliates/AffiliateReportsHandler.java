package org.sevensoft.ecreator.iface.admin.marketing.affiliates;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.marketing.affiliates.ClickThrough;
import org.sevensoft.ecreator.model.marketing.affiliates.Conversion;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.util.Results;

/**
 * @author sks 23 Feb 2007 08:08:02
 *
 */
@Path("admin-affiliates-reports.do")
public class AffiliateReportsHandler extends AdminHandler {

	private int		page;
	private Date	month;

	public AffiliateReportsHandler(RequestContext context) {
		super(context);
	}

	public Object conversions() throws ServletException {

		if (month == null) {
			month = new Date().beginMonth();
		}

		final Map<String, String> conversions = Conversion.getTotals(context, month);

		AdminDoc doc = new AdminDoc(context, user, "Conversions by affiliate", null);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Conversions by affiliate " + month.toString("MMMM-yy")));

				sb.append(new FormTag(AffiliateReportsHandler.class, "list", "get"));
				SelectTag monthTag = new SelectTag(context, "month");
				monthTag.setAutoSubmit();

				Date month = new Date().beginMonth();
				for (int n = 0; n < 12; n++) {
					monthTag.addOption(month, month.toString("MMMM-yy"));
					month = month.removeMonths(1);
				}

				sb.append("<tr><th align='right' colspan='2'>Choose month ");
				sb.append(monthTag);
				sb.append("</th></tr>");

				sb.append("</form>");

				sb.append("<tr>");
				sb.append("<td>Affiliate</td>");
				sb.append("<td>Conversion value</td>");
				sb.append("</tr>");

				for (Map.Entry<String, String> entry : conversions.entrySet()) {

					sb.append("<tr>");
					sb.append("<td>" + entry.getKey() + "</td>");
					sb.append("<td>" + entry.getValue() + "</td>");
					sb.append("</tr>");

				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object list() throws ServletException {

		if (month == null) {
			month = new Date().beginMonth();
		}

		final int count = ClickThrough.count(context, month);
		final Results results = new Results(count, page, 100);
		final List<ClickThrough> clicks = ClickThrough.get(context, month, results.getStartIndex(), 100);

		AdminDoc doc = new AdminDoc(context, user, "Click throughs", null);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Click throughs " + month.toString("MMMM-yy")));

				sb.append(new FormTag(AffiliateReportsHandler.class, "list", "get"));
				SelectTag monthTag = new SelectTag(context, "month");
				monthTag.setAutoSubmit();

				Date month = new Date().beginMonth();
				for (int n = 0; n < 12; n++) {
					monthTag.addOption(month, month.toString("MMMM-yy"));
					month = month.removeMonths(1);
				}

				sb.append("<tr><th align='right' colspan='5'>Choose month ");
				sb.append(monthTag);
				sb.append("</th></tr>");

				sb.append("</form>");

				sb.append("<tr>");
				sb.append("<td>Date</td>");
				sb.append("<td>Ip</td>");
				sb.append("<td>Affiliate Id</td>");
				sb.append("<td>Landing page</td>");
				sb.append("<td>Referral</td>");
				sb.append("</tr>");

				for (ClickThrough click : clicks) {

					sb.append("<tr>");
					sb.append("<td>" + click.getTime().toString("dd-MMM-yyyy HH:mm") + "</td>");
					sb.append("<td>" + click.getIpAddress() + "</td>");
					sb.append("<td>" + click.getAffiliateId() + "</td>");
					sb.append("<td>" + click.getLandingPage() + "</td>");
					sb.append("<td>" + (click.hasReferrer() ? click.getReferrer() : "") + "</td>");
					sb.append("</tr>");

				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {
		return list();
	}
}
