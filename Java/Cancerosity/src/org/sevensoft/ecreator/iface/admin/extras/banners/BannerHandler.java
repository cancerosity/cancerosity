package org.sevensoft.ecreator.iface.admin.extras.banners;

import java.util.List;
import java.util.Map;
import java.io.IOException;

import javax.servlet.ServletException;

import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.media.images.panels.ImageAdminPanel;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.frontend.ErrorDoc;
import org.sevensoft.ecreator.model.advertising.banners.Banner;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.ImageUtil;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 07-Jul-2005 12:39:17
 * 
 */
@Path("admin-banners.do")
public class BannerHandler extends AdminHandler {

	private Banner			banner;
	private Map<Integer, String>	imageDescriptions;
	private List<Upload>		imageUploads;
	private String			url;
	private String			name;
	private String	bannerCode;
	private String	imageFilename;

	public BannerHandler(RequestContext context) {
		super(context);
	}

	public BannerHandler(RequestContext context, Banner banner) {
		this(context);
		this.banner = banner;
	}

	public Object create() throws ServletException {

		new Banner(context, "new banner");
		return main();
	}

	public Object delete() throws ServletException {

		if (banner == null)
			return main();

		banner.delete();
		addMessage("Banner deleted.");
		return main();
	}

	public Object edit() throws ServletException {

		if (banner == null)
			return main();

		AdminDoc doc = new AdminDoc(context, user, "Edit banner", Tab.Extras);
		doc.setMenu(new ExtrasMenu());
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update banner"));
				sb.append(new ButtonTag(BannerHandler.class, "delete", "Delete banner", "banner", banner)
						.setConfirmation("Are you sure you want to delete this banner?"));
				sb.append(new ButtonTag(BannerHandler.class, null, "Return to menu"));

				sb.append("</div>");
			}

			private void details() {

				sb.append(new AdminTable("Banner details"));

				sb.append(new AdminRow("Name", "Enter a name for this banner for your reference", new TextTag(context, "name", banner.getName(), 40)));
				sb.append(new AdminRow("Url", "Enter the url this banner links to", new TextTag(context, "url", banner.getUrl(), 40)));
				sb.append(new AdminRow("Code", "If this banner is generated using third party code, enter this code here.", new TextAreaTag(context,
						"bannerCode", banner.getBannerCode(), 50, 5)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(BannerHandler.class, "save", "multi"));
				sb.append(new HiddenTag("banner", banner));

                commands();
                
				details();
				sb.append(new ImageAdminPanel(context, banner));

				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		final List<Banner> banners = SimpleQuery.execute(context, Banner.class);

		AdminDoc doc = new AdminDoc(context, user, "Banners", Tab.Extras);
		doc.addBody(new Body() {

			private void banners() {

				sb.append(new AdminTable("Banners"));

				sb.append("<tr>");
				sb.append("<th>Banner name</th>");
				sb.append("<th>Banner url</th>");
				sb.append("<th>Delete</th>");
				sb.append("</tr>");

				for (Banner banner : banners) {

					sb.append("<tr>");
					sb.append("<td>" + new LinkTag(BannerHandler.class, "edit", banner.getName(), "banner", banner) + "</td>");
					sb.append("<td>" + (banner.hasUrl() ? new LinkTag(banner.getUrl(), banner.getUrl()) : "n/a") + "</td>");
					sb.append("<td>" + new LinkTag(BannerHandler.class, "delete", "Delete", "banner", banner) + "</td>");
					sb.append("</tr>");
				}

				sb.append("<tr><td colspan='3'>" + new ButtonTag(BannerHandler.class, "create", "Add new banner") + "</td></tr>");

				sb.append("</table>");

			}

			@Override
			public String toString() {

				banners();

				return sb.toString();
			}
		});
		return doc;
	}

	public Object save() throws ServletException {

		if (banner == null)
			return main();

		test(new RequiredValidator(), "name");
		if (hasErrors())
			return edit();

		banner.setName(name);
		banner.setUrl(url);
		banner.setBannerCode(bannerCode);

		banner.save();

		try {
			ImageUtil.save(context, banner, imageUploads, null, imageFilename, true);
		} catch (ImageLimitException e) {
			addError(e);
		} catch (IOException e) {
            return new ErrorDoc(context, "The image you are attempting to upload is in CYMK colour mode. " +
                    "This is unsopported currently, please use a photo editing application (e.g. Photoshop) to convert the file to RGB colour. For more information or assistance please contact support.");
        }

        clearParameters();
		addMessage("Changes saved.");
		return edit();
	}
}
