package org.sevensoft.ecreator.iface.admin.containers.boxes;

import java.util.List;
import java.io.IOException;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.media.images.panels.ImageAdminPanel;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.ImageUtil;
import org.sevensoft.ecreator.model.media.images.boxes.ImageBox;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 4 Dec 2006 10:11:13
 *
 */
@Path("admin-boxes-image.do")
public class ImageBoxHandler extends BoxHandler {

	private ImageBox		box;
	private List<String>	imageUrls;
	private List<Upload>	imageUploads;
	private boolean		newWindow;
	private String		url;
	private String		imageFilename;

	public ImageBoxHandler(RequestContext context) {
		super(context);
	}

	public ImageBoxHandler(RequestContext context, ImageBox owner) {
		super(context);
		box = owner;
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {

		try {
			ImageUtil.save(context, box, imageUploads, imageUrls, imageFilename, true);
		} catch (ImageLimitException e) {
			addError(e);
		} catch (IOException e) {
            addError("The image you are attempting to upload is in CYMK colour mode. This is unsopported currently, " +
                    "please use a photo editing application (e.g. Photoshop) to convert the file to RGB colour. For more information or assistance please contact support");
        }

        box.setUrl(url);
		box.setNewWindow(newWindow);

	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Image sidebox settings"));
		sb.append(new AdminRow("Link url", "Enter a URL here to link this image to.", new TextTag(context, "url", box.getUrl(), 50)));
		sb.append(new AdminRow("New window", "Set to yes to open the link in a new window.", new BooleanRadioTag(context, "newWindow", box.isNewWindow())));
		sb.append("</table>");

		sb.append(new ImageAdminPanel(context, box));
	}

}
