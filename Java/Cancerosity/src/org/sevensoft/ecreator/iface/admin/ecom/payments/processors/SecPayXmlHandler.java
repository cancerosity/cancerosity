package org.sevensoft.ecreator.iface.admin.ecom.payments.processors;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.ecom.payments.PaymentSettingsHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.ecom.payments.processors.secpay.SecPayXmlProcessor;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 17 Apr 2007 08:04:02
 *
 */
@Path("admin-payments-processors-secpayxml.do")
public class SecPayXmlHandler extends ProcessorHandler {

	private SecPayXmlProcessor	processor;

	private String			username;
	private String			password;
	private String			clientId;
	public SecPayXmlHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object delete() {
		return super.delete(processor);
	}

	@Override
	public Object main() throws ServletException {

		if (processor == null) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Secpay XML", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update processor"));
				sb.append(new ButtonTag(PaymentSettingsHandler.class, null, "Return to payment settings"));
				sb.append("</div>");
			}

			private void general() {
				sb.append(new AdminTable("Secpay XML"));

				sb.append(new AdminRow("Username", "Hsbc MPI Usename.", new TextTag(context, "username", processor.getUsername(), 20)));

				sb.append(new AdminRow("Password", "Hsbc MPI password.", new TextTag(context, "password", processor.getPassword(), 20)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(SecPayXmlHandler.class, "save", "post"));
				sb.append(new HiddenTag("processor", processor));

                commands();
				general();
				commands();
				sb.append("</for" + "m>");

				return sb.toString();
			}
		});
		return doc;
	}

	public Object save() throws ServletException {

		if (processor == null) {
			return index();
		}

		processor.setUsername(username);
		processor.setPassword(password);
		processor.save();

		return new ActionDoc(context, "This processor has been updated", new Link(SecPayXmlHandler.class, null, "processor", processor));
	}
}
