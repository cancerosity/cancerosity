package org.sevensoft.ecreator.iface.admin.search;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.search.forms.blocks.SearchBlock;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;

/**
 * @author sks 5 Sep 2006 15:59:24
 *
 */
@Path("admin-blocks-search.do")
public class SearchBlockHandler extends BlockEditHandler {

	private SearchBlock	block;
	private boolean		searchHeader;

	public SearchBlockHandler(RequestContext context) {
		super(context);
	}

	public SearchBlockHandler(RequestContext context, SearchBlock block) {
		super(context);
		this.block = block;
	}

	@Override
	protected Block getBlock() {
		return block;
	}

	@Override
	protected String getTitle() {
		return "Search block";
	}

	@Override
	protected void saveSpecific() {
		block.setSearchHeader(searchHeader);
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Search form"));

		sb.append("<tr><th width='300'><b>Edit search form</b></th><td>"
				+ new ButtonTag(SearchFormHandler.class, null, "Edit search form", "form", block.getSearchForm()) + "</td></tr>");

		sb.append("</table>");
	}

}
