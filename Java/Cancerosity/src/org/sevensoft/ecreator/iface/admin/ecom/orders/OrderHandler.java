package org.sevensoft.ecreator.iface.admin.ecom.orders;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.servlet.ServletException;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.Period;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.attachments.AttachmentHandler;
import org.sevensoft.ecreator.iface.admin.crm.forms.SubmissionHandler;
import org.sevensoft.ecreator.iface.admin.ecom.addresses.AddressHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.menu.OrderMenu;
import org.sevensoft.ecreator.iface.admin.ecom.orders.panels.AttachmentsLinePanel;
import org.sevensoft.ecreator.iface.admin.ecom.payments.cards.CardHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.AAdminRenderer;
import org.sevensoft.ecreator.model.attributes.renderers.ARenderType;
import org.sevensoft.ecreator.model.crm.forms.submission.Submission;
import org.sevensoft.ecreator.model.crm.messages.panels.MessagesPanel;
import org.sevensoft.ecreator.model.crm.messages.panels.MessagesReplyPanel;
import org.sevensoft.ecreator.model.crm.messages.Msg;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.branches.Branch;
import org.sevensoft.ecreator.model.ecom.delivery.DeliveryOption;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.ecreator.model.ecom.orders.OrderStatusException;
import org.sevensoft.ecreator.model.ecom.orders.parcels.Courier;
import org.sevensoft.ecreator.model.ecom.orders.parcels.Parcel;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.PaymentRedirect;
import org.sevensoft.ecreator.model.ecom.payments.processors.googlecheckout.GoogleCheckoutProcessor;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.results.AvsCheck;
import org.sevensoft.ecreator.model.ecom.vouchers.Voucher;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.model.system.users.privileges.Privilege;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.date.DateTextTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.links.EmailTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.results.docs.SimpleDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 13-Jul-2005 16:24:50
 * 
 */
@Path("admin-orders.do")
public class OrderHandler extends AdminHandler {

	private transient Company			company;
	private String					consignment;
	private Courier					courier;
	private Order					order;
	private Parcel					parcel;
	private String					customerReference;
	private boolean					vatable;
	private String					keywords;
	private Map<Integer, Money>			linePrice;
	private Map<Integer, Integer>			lineQty;
    private Map<Integer, Double>    lineVat;
    private DeliveryOption				deliveryRate;
	private User					salesPerson;
	private Item					account;
	private Address					deliveryAddress;
	private OrderLine					line;
	private Item					item;
	private String					content;
	private PaymentType				paymentType;
	private Card					card;
	private Date					invoiceDate;
	private String					status;
	private Address					invoiceAddress;
	private OrderLine					orderLine;
	private Money					deliveryCharge;
	private String					deliveryDescription;
	private final transient PaymentSettings	paymentSettings;
	private final transient OrderSettings	orderSettings;

	private Map<Integer, String>			lineDesc;
	private Voucher					voucher;
	private MultiValueMap<Attribute, String>	attributeValues;

	private Period					recurPeriod;
	private Date					recurDate;
	private String					category;

	private Date					reinvoiceDate;

    private Msg message;
    private String body;

    private String reason;
    private String comment;

    public OrderHandler(RequestContext context) {
		this(context, null);
	}

	public OrderHandler(RequestContext context, Order order) {
		super(context);
		this.order = order;
		this.paymentSettings = PaymentSettings.getInstance(context);
		this.company = Company.getInstance(context);
		this.orderSettings = OrderSettings.getInstance(context);
	}

	public Object addLine() {

		if (order == null || item == null) {
			return main();
		}

		order.addItemLine(item, 1);

		clearParameters();
		return new ActionDoc(context, "A new line has been added", new Link(OrderHandler.class, null, "order", order));
	}

	public Object addMiscLine() {

		if (order == null) {
			return main();
		}

		line = order.addInfoLine("new line", 1, new Money(0));

        order.log(user, "Line #" + line.getId() + " '" + line.getDescription() + "' added");

		clearParameters();
		return new ActionDoc(context, "A new line has been added", new Link(OrderHandler.class, null, "order", order));
	}

	public Object addParcel() {

		if (courier == null || consignment == null || order == null) {
			return main();
		}

		order.addParcel(courier, consignment);
		order.log(user, "Parcel '" + consignment + "' added");

		clearParameters();
		return new ActionDoc(context, "The parcel has been added", new Link(OrderHandler.class, null, "order", order));
	}

	public Object cancel() {

		if (order == null) {
			return main();
		}

		if (order.cancel()) {
			order.log(user, "Cancelled");
		}

        if (order.getPaymentType() == PaymentType.CardTerminal)
            order.clearCard();

		return new ActionDoc(context, "This order has been cancelled", new Link(OrderHandler.class, null, "order", order));
	}

	public Object create() throws ServletException {

		if (account == null) {
			return index();
		}

		order = account.addOrder(user, getRemoteIp());
		clearParameters();
		return main();
	}

	public Object emailCompleted() {

		if (order == null) {
			return main();
		}

        try {
            order.emailCompletion();
            if (orderSettings.isEmailInvoiceOnCompletion()) {
                order.emailInvoice();
            }
            addMessage("Completion email has been sent.");

        } catch (EmailAddressException e) {
            e.printStackTrace();
            addError(e);

        } catch (SmtpServerException e) {
            e.printStackTrace();
            addError(e);
        }

        catch (IOException e) {
            e.printStackTrace();
            addError(e);
        }
        return main();
	}

	/**
	 * Send an invoice to the email address 
	 */
	public Object emailInvoice() {

		if (order == null) {
			return main();
		}

		try {
			order.emailInvoice();

		} catch (EmailAddressException e) {
			addError(e);

		} catch (SmtpServerException e) {
			addError(e);

		} catch (IOException e) {
			addError(e);
		}

		return new ActionDoc(context, "Invoice has been emailed", new Link(OrderHandler.class, null, "order", order));
	}

	public Object printInvoice() {

		if (order == null) {
			return main();
		}

		if (orderSettings.hasInvoiceMarkup()) {

			SimpleDoc doc = new SimpleDoc(context);

			MarkupRenderer r = new MarkupRenderer(context, orderSettings.getInvoiceMarkup());
			r.setBody(order);
			doc.addBody(r);

			return doc;

		} else {

			return new InvoiceHardcopyDoc(context, order);
		}
	}

	@Override
	public Object main() {

		if (order == null) {
			return new OrderSearchHandler(context).main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Order #" + order.getOrderId() + " " + order.getAccount().getName() + " - " + order.getStatus(),
				Tab.Orders);
		if (miscSettings.isAdvancedMode()) {
			doc.setMenu(new OrderMenu(order));
		}

		doc.setIntro("Order #" + order.getOrderId() + " was created at " + order.getDatePlaced().toString("HH:mm dd-MMM-yyyy") + " and has status '" +
				order.getStatus() + "'");
		doc.addBody(new Body() {

			private Object	branch;

			private void account() {

				Item account = order.getAccount();

				sb.append(new AdminTable("Account details"));

				sb.append("<tr><th width='130'>Name</th><td>" + new LinkTag(ItemHandler.class, "edit", account.getName(), "item", account) +
						"</td></tr>");

				if (account.hasEmail()) {
					sb.append("<tr><th width='130'>Email</th><td>" + new EmailTag(account.getEmail()));

					// notification command
					if (shoppingSettings.hasNotificationEmails()) {
						sb.append("<br/>");
						sb.append(new ButtonTag(OrderHandler.class, "sendNotificationEmails", "Send notification email", "order", order)
								.setConfirmation("Are you sure you want to send a notification email?"));
					}

					sb.append("</td></tr>");
				}
                
                if (account.hasMobilePhone()) {
                    sb.append("<tr><th width='130'>Phone</th><td>" + account.getMobilePhone() + "</td></tr>");
                }

                sb.append("<tr><th width='130'>IPoints Username</th><td>" + new TextTag(context, "ipointsUsername", order.getIpointsUsername())
                        + "</td></tr>");


                sb.append("<tr><th width='130'>IPoints Password</th><td>" + new TextTag(context, "ipointsPassword", order.getIpointsPassword())
                        + "</td></tr>");

                sb.append("</table>");

            }

			private void billingAddress() {

				sb.append(new AdminTable("Invoice / Billing address"));

				if (order.hasBillingAddress()) {
                    sb.append("<tr><td valign='top'><b>Billing Address:</b></td></tr>");
                    sb.append("<tr><td valign='top'>" + order.getBillingAddress().getLabel("<br/>", true) + "</td></tr>");
				} else {
					sb.append("<tr><td valign='top'>Reserved for pickup</td></tr>");
				}
                if (order.hasDeliveryAddress() && !order.getDeliveryAddress().equals(order.getBillingAddress())) {
                    sb.append("<tr><td valign='top'><b>Delivery Address:</b></td></tr>");
                    sb.append("<tr><td valign='top'>" + order.getDeliveryAddress().getLabel("<br/>", true) + "</td></tr>");
                }
                SelectTag addressTag = new SelectTag(null, "invoiceAddress");
				addressTag.setAny("-Choose new address-");
				for (Address address : order.getAccount().getAddresses()) {
					String label = address.getLabel(", ", true);
					addressTag.addOption(address.getIdString(), label.length() < 50 ? label : label.substring(0, 50) + "...");
				}

				sb.append(new FormTag(OrderHandler.class, "setInvoiceAddress", "post"));
				sb.append(new HiddenTag("order", order));

				sb.append("<tr><td>" + addressTag + "</td></tr>");
				sb.append("<tr><td>" + new SubmitTag("Change address").setDisabled(order.isCancelled()) + " " +
						new ButtonTag(AddressHandler.class, null, "Edit addresses", "order", order, "account", order.getAccount()).setDisabled(order.isCancelled()) + "</td></tr>");

				sb.append("</form>");

				sb.append("</table>");
			}

			private void reinvoiceDate() {

				sb.append(new FormTag(OrderHandler.class, "setBillingDate", "POST"));
				sb.append(new HiddenTag("order", order));

				sb.append(new AdminRow("Reinvoice date", "This is the date the order is due to be reinvoiced.", new DateTextTag(context,
						"reinvoiceDate", order.getReinvoiceDate()) +
						" " + new SubmitTag("Update") + " " + new ButtonTag(OrderHandler.class, "invoice", "Invoice", "order", order)));

				sb.append("</form>");
			}

			//			if (orderSettings.isXmlExport()) {
			//				sb.append(new ButtonTag(ExportOrdersHandler.class, "exportOrderXml", "Order XML", "order", order));
			//			}
			//
			//			if (orderSettings.isXmlExport()) {
			//				sb.append(new ButtonTag(ExportOrdersHandler.class, "exportOrderHeaderCsv", "Header CSV", "order", order));
			//				sb.append(new ButtonTag(ExportOrdersHandler.class, "exportOrderLinesCsv", "Lines CSV", "order", order));
			//			}

			//			sb.append(new FormTag(context, OrderHandler.class, "setInvoiceDate", "POST"));
			//			sb.append(new HiddenTag("order", order));
			//			sb.append(new AdminRow("Invoice Date", "This is the invoice / tax date.", new DateTag(context, "invoiceDate", order.getInvoiceDate())
			//					+ " " + new SubmitTag("Update")));
			//			sb.append("</form>");

			private void category() {

				sb.append(new FormTag(OrderHandler.class, "setCategory", "POST"));
				sb.append(new HiddenTag("order", order));

				List<String> categories = orderSettings.getCategories();
				if (order.hasCategory()) {
					if (!categories.contains(order.getCategory())) {
						categories.add(0, order.getCategory());
					}
				}

				AdminRow row = new AdminRow("Sales category", "Group sales under categories for reporting.", new SelectTag(context, "category", order
						.getCategory(), categories, "None") +
						" " + new SubmitTag("Change").setConfirmation("Are you sure you want to change the sales category?"));

				sb.append(row);

				sb.append("</form>");
			}

			private void customerReference() {

				if (shoppingSettings.isCustomerReference()) {

					sb.append(new FormTag(OrderHandler.class, "setCustomerReference", "POST"));
					sb.append(new HiddenTag("order", order));

					sb.append(new AdminRow("Customer reference", new TextTag(context, "customerReference", order.getCustomerReference(), 30) + " " +
							new SubmitTag("Change reference").setDisabled(order.isCancelled())));

					sb.append("</form>");

				}
			}

			private void deliveryAddress() {

				sb.append(new AdminTable("Delivery address"));

				if (order.isReservation()) {

					sb.append("<tr><td valign='top'>Reserved for pickup</td></tr>");

				} else {

					sb.append("<tr><td valign='top'>" + order.getDeliveryAddress().getLabel("<br/>", true));
					if (order.getDeliveryAddress().hasInstructions()) {
						sb.append(order.getDeliveryAddress().getInstructions());
					}
					sb.append("</td></tr>");
				}

				SelectTag addressTag = new SelectTag(null, "deliveryAddress");
				addressTag.setAny("-Choose new address-");
				for (Address address : order.getAccount().getAddresses()) {
					String label = address.getLabel(", ", false);
					addressTag.addOption(address.getIdString(), label.length() < 50 ? label : label.substring(0, 50) + "...");
				}

				sb.append(new FormTag(OrderHandler.class, "setDeliveryAddress", "post"));
				sb.append(new HiddenTag("order", order));

				sb.append("<tr><td>" + addressTag + "</td></tr>");
				sb.append("<tr><td>" + new SubmitTag("Change address").setDisabled(order.isCancelled()) + " " +
						new ButtonTag(AddressHandler.class, null, "Edit addresses", "order", order, "account", order.getAccount()).setDisabled(order.isCancelled()) + "</td></tr>");
				sb.append("</form>");

				sb.append("</table>");
			}

			private void details() {

				sb.append(new AdminTable("Order details"));

                if (order.getPaymentType() == PaymentType.GoogleCheckout) {
                    statusGoogle();
                } else {
                    status();
                }

				if (!order.isCancelled()) {

					if (orderSettings.hasCategories()) {
						category();
					}

					if (orderSettings.isSalesPersons()) {
						salesp();
					}

					if (order.hasBranch()) {
						branch();
					}

					customerReference();
					vatable();
					submissions();

					if (orderSettings.isRecurringOrders()) {
						recurPeriod();
						reinvoiceDate();
					}
				}

				sb.append("</table>");
			}

			private void branch() {

				Branch branch = order.getBranch();
				if (branch != null) {
					sb.append(new AdminRow("Branch", branch.getName()));
				}
			}

			private void lines() {

				sb.append(new AdminTable("Order lines"));

				sb.append("<tr>");
				sb.append("<th width='60'>#</th>");
				sb.append("<th>Description</th>");
				sb.append("<th width='60'>Qty</th>");
				sb.append("<th width='80'>Unit price</th>");
				sb.append("<th width='60'>Vat rate</th>");
				sb.append("<th width='80'>Line total</th>");
				sb.append("<th width='10'> </th>");
				sb.append("</tr>");

				final PositionRender pr = new PositionRender(OrderHandler.class, "orderLine");

				sb.append(new FormTag(OrderHandler.class, "updateLines", "post"));
				sb.append(new HiddenTag("order", order));

				for (OrderLine line : order.getLines()) {

					sb.append("<tr>");

					sb.append("<td width='60'>" + pr.render(line) + "</td>");

					// description
					sb.append("<td>");
					if (line.hasItem()) {

						sb.append(new LinkTag(ItemHandler.class, "edit", line.getDescription(), "item", line.getItem()));
						if (line.hasOptionsDetails()) {
							sb.append("<br/>");
/*
							String[] options = line.getOptionsDetails().split(",");
                            for (int i = options.length-1; i >= 0; i--) {
                                sb.append(options[i] + ",");
                            }
*/
                            sb.append(line.getOptionsDetails());
						}

						if (line.hasAttachments()) {
							AttachmentsLinePanel panel = new AttachmentsLinePanel(context, line);
							sb.append(panel);
						}

					} else {

						sb.append(new TextTag(context, "lineDesc_" + line.getId(), line.getDescription(), 50));

					}

					sb.append("</td>");

					if (order.isOpen()) {

						sb.append("<td width='60'>" + new TextTag(null, "lineQty_" + line.getId(), line.getQty(), 3) + "</td>");
						sb.append("<td width='80'>" + new TextTag(null, "linePrice_" + line.getId(), line.getUnitSellEx(), 8) + "</td>");

					} else {

						sb.append("<td width='60'>" + line.getQty() + "</td>");
						sb.append("<td width='80'>" + line.getUnitSellEx() + "</td>");
					}

					sb.append("<td width='60'>");
					if (company.isVatRegistered()) {
                        if (order.isOpen()) {
                            sb.append(new TextTag(null, "lineVat_" + line.getId(), line.getVatRate(), 3) + "%");
                        }
                        else {
                            sb.append(line.getVatRate() + "%");
                        }
					} else {
						sb.append("-");
					}
					sb.append("</td>");

					sb.append("<td width='80'>" + line.getLineSellEx() + "</td>");

					// remove button
					sb.append("<td width='10'>");

					if (order.isOpen()) {
						sb.append(new LinkTag(OrderHandler.class, "removeLine", new DeleteGif(), "order", order, "line", line)
								.setConfirmation("Are you sure you want to remove this line?"));
					} else {
						sb.append("&nbsp;");
					}

					sb.append("</td>");
					sb.append("</tr>");

				}

				/* 
				 * if delivery module is enabled, then we want to show delivery options on this order, and if the order is 
				 * editable then make them as a text box
				 */
				if (Module.Delivery.enabled(context)) {

					sb.append("<tr>");
					sb.append("<td>&nbsp;</td>");

					if (order.isOpen()) {

						sb.append("<td>" + new TextTag(context, "deliveryDescription", order.getDeliveryDetails(), 60) + "</td>");
						sb.append("<td width='60'>&nbsp;</td>");
						sb.append("<td>" + new TextTag(context, "deliveryCharge", order.getDeliveryChargeEx(), 8) + "</td>");

					} else {

						sb.append("<td>");
						if (order.hasDeliveryDetails())
							sb.append(order.getDeliveryDetails());
						sb.append("</td>");
						sb.append("<td width='60'>&nbsp;</td>");
						sb.append("<td width='80'>" + order.getDeliveryChargeEx() + "</td>");

					}

					if (company.isVatRegistered()) {
						sb.append("<td width='60'>" + order.getDeliveryVatRate() + "%</td>");
					}

					sb.append("<td width='80'>" + order.getDeliveryChargeEx() + "</td>");
					sb.append("<td>&nbsp;</td>");
					sb.append("</tr>");

				}

				/*
				 * Delivery option selector and update line button
				 */
				sb.append("<tr><td></td>");

				if (Module.Delivery.enabled(context)) {

					sb.append("<td>Change delivery: " +
							new SelectTag(context, "deliveryRate", null, DeliveryOption.get(context), "-Select delivery rate-") + "</td>");

				} else {

					sb.append("<td></td>");

				}

                SubmitTag updateSubmitTag = new SubmitTag("Update");
                ButtonTag addLineButtonTag = new ButtonTag(OrderHandler.class, "addMiscLine", "Add info line", "order", order);
                if (order.isCancelled()) {
                    updateSubmitTag.setDisabled(true);
                    addLineButtonTag.setDisabled(true);
                }
                sb.append("<td colspan='5' align='right'>" + updateSubmitTag + " " + addLineButtonTag + "</td>");
                sb.append("</tr>");

				sb.append("</form>");

				/*
				 * VOUCHER
				 * 
				 * I want to show details of the voucher used if any here
				 */
				if (order.hasVoucher() || Module.Vouchers.enabled(context)) {

					if (order.hasVoucher()) {

						sb.append("<tr>");
						sb.append("<td>&nbsp;</td>");
						sb.append("<td>" + new LinkTag(OrderHandler.class, "removeVoucher", "x", "order", order) + "</td>");
						sb.append("<td colspan='2' align='right'>Voucher: " + order.getVoucherDescription() + "</td>");
						sb.append("<td>" + order.getVoucherDiscountEx() + "</td>");
						sb.append("<td>" + order.getVoucherDiscountVat() + "</td>");
						sb.append("<td>" + order.getVoucherDiscountInc() + "</td>");
						sb.append("</tr>");

					}

					if (Module.Vouchers.enabled(context)) {

						SelectTag voucherTag = new SelectTag(context, "voucher", null, Voucher.get(context), "-Select voucher-");

						sb.append(new FormTag(OrderHandler.class, "setVoucher", "get"));
						sb.append(new HiddenTag("order", order));
						sb.append("<tr>");
                        sb.append("<td colspan='4' align='right'>Apply voucher: " + voucherTag + " " + new SubmitTag("Apply").setDisabled(order.isCancelled()) + "</td>");
						sb.append("<td>&nbsp;</td>");
						sb.append("<td>&nbsp;</td>");
						sb.append("<td>&nbsp;</td>");
						sb.append("</tr>");
						sb.append("</form>");

					}
				}

				if (order.isOpen()) {

					if (orderSettings.isItems()) {

						sb.append(new FormTag(OrderHandler.class, "productSearch", "get"));
						sb.append(new HiddenTag("order", order));

						sb.append("<tr>");
						sb.append("<td colspan='7' align='right'>" + new TextTag(null, "keywords", 40) + " " + new SubmitTag("Search for product") +
								"</td>");
						sb.append("</form>");
						sb.append("</tr>");

						sb.append("</form>");

					}
				}

				// reward points
				if (Module.Credits.enabled(context)) {
					sb.append("<tr>");
					sb.append("<td colspan='7' align='right'>Reward points</td><td>" + order.getRewardedCredits() + "</td>");
					sb.append("</tr>");
				}

				sb.append("<tr>");
				if (company.isVatRegistered()) {
					sb.append("<td colspan='5' align='right'>");
				} else {
					sb.append("<td colspan='4' align='right'>");
				}
				sb.append("<strong>Subtotal</strong></td><td colspan='2'>" + order.getTotalEx() + "</td></tr>");

				sb.append("<tr>");
				if (company.isVatRegistered()) {
					sb.append("<td colspan='5' align='right'>");
				} else {
					sb.append("<td colspan='4' align='right'>");
				}
				sb.append("<strong>Vat</strong></td><td colspan='2'>" + order.getTotalVat() + "</td></tr>");

				sb.append("<tr>");
				if (company.isVatRegistered()) {
					sb.append("<td colspan='5' align='right'>");
				} else {
					sb.append("<td colspan='4' align='right'>");
				}
				sb.append("<strong>Total</strong></td><td colspan='2'>" + order.getTotalInc() + "</td></tr>");

				sb.append("</table>");

			}

			private void parcels() {

				if (!orderSettings.isParcels()) {
					return;
				}

				sb.append(new AdminTable("Parcels"));

				sb.append("<tr>");
				sb.append("<th>Date</th>");
				sb.append("<th>Courier</th>");
				sb.append("<th>Consignment</th>");
				sb.append("<th>Delete</th>");
				sb.append("</tr>");

				for (Parcel parcel : order.getParcelsAll()) {

					sb.append("<tr>");
					sb.append("<td>" + parcel.getDate().toDateTimeString() + "</td>");
					sb.append("<td>" + parcel.getCourier() + "</td>");

					sb.append("<td>");
					if (parcel.isCurrent()) {

						if (parcel.getCourier().hasTracker()) {

							sb.append(new LinkTag(parcel.getTrackingUrl(), parcel.getConsignmentNumber()).setTarget("_blank"));

						} else {

							sb.append(parcel.getConsignmentNumber());
						}

					} else {

						sb.append("<strike>" + parcel.getConsignmentNumber() + "</strike>");
					}
					sb.append("</td>");

					if (parcel.isCurrent())
						sb.append("<td>" + new ButtonTag(OrderHandler.class, "removeParcel", "Remove", "order", order, "parcel", parcel) + "</td>");
					else
						sb.append("<td>&nbsp;</td>");

					sb.append("</tr>");
				}

				SelectTag couriers = new SelectTag(context, "courier");
				couriers.addOptions(Courier.values());

				sb.append(new FormTag(OrderHandler.class, "addParcel", "POST"));
				sb.append(new HiddenTag("order", order));

				sb.append("<tr>");
				sb.append("<td>  </td>");
				sb.append("<td>" + couriers + "</td>");
				sb.append("<td>" + new TextTag(context, "consignment", 20) + "</td>");
				sb.append("<td>" + new SubmitTag("Add parcel").setDisabled(order.isCancelled()) + "</td>");
				sb.append("</tr>");

				sb.append("</form>");

				sb.append("</table>");
			}

			private void payment() {

				sb.append(new AdminTable("Payment"));

				/*
				 * If order is open then show payment type options to allow us to change.
				 */
				if (order.isOpen()) {

					SelectTag paymentTypesTag = new SelectTag(context, "paymentType", order.getPaymentType());
					TreeSet<PaymentType> paymentTypes = paymentSettings.getPaymentTypes();
					paymentTypesTag.addOptions(paymentTypes);

					sb.append(new FormTag(OrderHandler.class, "setPaymentType", "Post"));
					sb.append(new HiddenTag("order", order));
					sb.append("<tr><th width='130'>Payment method</th><td>" + paymentTypesTag + " " + new SubmitTag("Change").setDisabled(order.isCancelled()) + "</td></tr>");
                    if (order.getPaymentType().equals(PaymentType.PayPalExpress)) {
                        sb.append("<tr><th width='130'>PayPal transaction link</th><td><a target='_blank' href='https://www.paypal.com/uk/vst/id=" + order.getPayPalTransactionId() + "'>" +
                                "https://www.paypal.com/uk/vst/id=" + order.getPayPalTransactionId() + "</a></td></tr>");
                    }
                    if (order.getPaymentType().equals(PaymentType.PayPalDirect)) {
                        sb.append("<tr><th width='130'>PayPal teransaction ID</th><td>" + order.getPayPalTransactionId() + "</td></tr>");
                    }
                    sb.append("</form>");

				} else {

					/*
					 * ... otherwise just display payment type
					 */
					sb.append("<tr><th width='130'>Payment method</th><td>" + order.getPaymentType() + "</td></tr>");
                    if (order.getPaymentType().equals(PaymentType.PayPalExpress)) {
                        sb.append("<tr><th width='130'>PayPal transaction link</th><td><a target='_blank' href='https://www.paypal.com/uk/vst/id=" + order.getPayPalTransactionId() + "'>" +
                                "https://www.paypal.com/uk/vst/id=" + order.getPayPalTransactionId() + "</a></td></tr>");
                    }
                    if (order.getPaymentType().equals(PaymentType.PayPalDirect)) {
                        sb.append("<tr><th width='130'>PayPal teransaction ID</th><td>" + order.getPayPalTransactionId() + "</td></tr>");
                    }
                }

				if (order.getPaymentType() == PaymentType.CardTerminal) {

					if (order.hasCard()) {

						sb.append("<tr><th width='130'>Card details</th><td>" + order.getCard().getLabel(", ") + "</td></tr>");

					}

					SelectTag tag = new SelectTag(context, "card", order.getCard());
					tag.setAny("-No card details set-");
					tag.addOptions(order.getAccount().getCards());

					sb.append(new FormTag(OrderHandler.class, "setCard", "Post"));
					sb.append(new HiddenTag("order", order));
					sb.append("<tr><th width='130'>Change card</th><td>" + tag + " " + new SubmitTag("Change").setDisabled(order.isCancelled()) + " " +
							new ButtonTag(CardHandler.class, null, "Add card", "order", order).setDisabled(order.isCancelled()));

					sb.append("</td></tr>");
					sb.append("</form>");

					if (order.hasCard()) {

						sb.append("<tr><th width='130'>Make payment</th><td>");
						if (paymentSettings.hasProcessor()) {
							sb.append(new ButtonTag(OrderHandler.class, "transact", "Transact card", "order", order).setDisabled(order.isCancelled()));
						} else {
							sb.append(new ButtonTag(OrderHandler.class, "transact", "Transact card", "order", order).setDisabled(order.isCancelled()));
						}
						sb.append("</td></tr>");
					}

                } else if (order.getPaymentType() == PaymentType.PayPalStandard) {
                    sb.append("<tr><th width='130'>Send data to PayPal </th><td>");
                    sb.append(new ButtonTag(OrderHandler.class, "transact", "Place order", "order", order).setDisabled(order.isCancelled()));
                    sb.append("</td></tr>");

                } else if (order.getPaymentType() == PaymentType.GoogleCheckout) {
                    sb.append("<tr><th width='130'>Google order number</th><td>" + new TextTag(context, "", order.getGoogleOrderNumber()) + "</td></tr>");
                } else if (order.getPaymentType() == PaymentType.CardsaveRedirect) {
                    sb.append("<tr><th width='130'>Cross Reference</th><td>" + new TextTag(context, "", order.getCardsaveCrossReference(), 40) + "</td></tr>");
                }

				Money paid = new Money(0);
				for (Payment payment : order.getPayments()) {
					sb.append("<tr><th width='130'>Payment</th><td>" + payment.getInfoString() + "</td></tr>");
					paid = paid.add(payment.getAmount());
				}

//				if (paid.isPositive()) {
//					sb.append("<tr><th width='130'>Total paid</th><td>" + order.getAmountPaid() + "</td></tr>");
//				}

				// show all avs checks tagged to this order
				for (AvsCheck avsCheck : order.getAvsChecks()) {

					sb.append("<tr><th width='130'>Avs result</th><td>" + avsCheck.getResult() + "</td></tr>");

				}

				sb.append("</table>");

			}

			private void recurPeriod() {

				sb.append(new FormTag(OrderHandler.class, "setRecurPeriod", "POST"));
				sb.append(new HiddenTag("order", order));

				sb.append(new AdminRow("Recur period", "How often to recur this order automatically.", new TextTag(context, "recurPeriod", order
						.getRecurPeriod(), 12) +
						" " + new SubmitTag("Update")));

				sb.append("</form>");
			}

			private void salesp() {

				SelectTag salesPersonsTag = new SelectTag(context, "salesPerson", order.getSalesPerson());
				salesPersonsTag.setAny("Web order");
				salesPersonsTag.addOptions(User.getActive(context));

				sb.append(new FormTag(OrderHandler.class, "setSalesPerson", "POST"));
				sb.append(new HiddenTag("order", order));
				sb.append(new AdminRow("Agent", "The sales agent this order is assigned to", salesPersonsTag + " " +
						new SubmitTag("Change").setConfirmation("Are you sure you want to change the salesperson?")));
				sb.append("</form>");
			}

			/**
			 * 
			 */
			private void status() {

				SelectTag statusTag = new SelectTag(context, "status", order.getStatus());
				statusTag.addOptions(orderSettings.getStatuses());

				sb.append(new FormTag(OrderHandler.class, "setStatus", "POST"));
				sb.append(new HiddenTag("order", order));

				Object cancelButton;
				if (!order.isCancelled()) {
					cancelButton = " " +
							new ButtonTag(OrderHandler.class, "cancel", "Cancel order", "order", order)
									.setConfirmation("Are you sure you want to cancel this order?");
				} else {
					cancelButton = "";
				}

				//				if (order.isCompleted()) {
				//
				//					completedEmailButton = " " + new ButtonTag(OrderHandler.class, "emailCompleted", "Send completion email", "order", order);
				//
				//				} else {
				//					
				//					completedEmailButton = "";
				//				}

				sb.append(new AdminRow("Status", "Set the status of this order", statusTag + " " +
						new SubmitTag("Change status").setConfirmation("Are you sure you want to change the status?") + cancelButton));

				sb.append("</form>");
			}

            private void statusGoogle() {    //todo statusGoogle()

                sb.append(new FormTag(GoogleCheckoutProcessor.class, GoogleCheckoutProcessor.Commands.CancelOrder.getAction(), "post"));
                sb.append(new HiddenTag(StringHelper.toCamelCase(order.getClass()), order));

                SelectTag statusTag = new SelectTag(context, "status", order.getStatus());
                statusTag.addOptions(OrderSettings.GoogleFulfillmentOrderState.values());
                statusTag.setDisabled(true);
                sb.append(new AdminRow("Fulfillment State", statusTag));

                SelectTag statusTag1 = new SelectTag(context, "status", order.getFinansialStatus());
                statusTag1.addOptions(OrderSettings.GoogleFinansialOrderState.values());
                statusTag1.setDisabled(true);
                sb.append(new AdminRow("Finansial State", statusTag1));

                sb.append(new AdminRow("", ""));

//                sb.append(new HiddenTag("linkback", new Link(OrderHandler.class)));
                SelectTag selectTag = new SelectTag(context, "reason", order.getReason());
                selectTag.addOptions(GoogleCheckoutProcessor.Reasons.values());
                selectTag.setAny("Select reason...");
//                selectTag.setOnChange("this.document.getElementsByName('comment')[0].focus=true; this.document.getElementsByName('comment')[0].value='';");

                sb.append(new AdminRow("Reason for cancelation", selectTag));

                TextAreaTag areaTag = new TextAreaTag(context, "comment", order.getComment(), 40, 4);
                areaTag.setStyle("width: 100%; font-size: 100%;");
//                areaTag.setOnFocus("dialogBox.enterCancelComment();");

                sb.append(new AdminRow("Comment","The maximum accepted length for the parameter value is 140 characters.", areaTag));

				if (!order.isCancelled()) {

                            sb.append(new AdminRow("", new SubmitTag("Cancel order").setConfirmation("Are you sure you want to cancel this order?")));
                }
                sb.append(new AdminRow("", ""));
                sb.append("</form>");

                sb.append(new FormTag(GoogleCheckoutProcessor.class, GoogleCheckoutProcessor.Commands.ChargeOrder.getAction(), "post"));
                sb.append(new HiddenTag(StringHelper.toCamelCase(order.getClass()), order));
                sb.append(new HiddenTag("amount", order.getTotalInc()));
                sb.append(new AdminRow("", new SubmitTag("CHARGE ORDER!")));
                sb.append(new AdminRow("", ""));
                sb.append("</form>");
//                sb.append("</table>");
			}

            /**
			 * 
			 */
			private void submissions() {

				List<Submission> submissions = order.getSubmissions();
				if (submissions.size() > 0) {

					sb.append("<tr><td colspan='2'>Attached forms: ");
					List<String> list = new ArrayList();
					for (Submission s : submissions) {
						list.add(new LinkTag(SubmissionHandler.class, "view", s.getName(), "submission", s).toString());
					}
					sb.append(StringHelper.implode(list, ", "));
					sb.append("</td></tr>");

				}
			}

			@Override
			public String toString() {

				// put details on left side of a 2 column layout
				sb.append("<table cellspacing='0' width='100%'><tr><td valign='top' style='padding-right: 6px;'>");

				details();

				// put account info on right

				sb.append("</td><td valign='top' style='width: 350px; padding-left: 6px;'>");

				account();

				sb.append("</td></tr>");

				List<Attribute> attributes = order.getAttributes();

				if (attributes.size() > 0) {

					sb.append(new FormTag(OrderHandler.class, "saveAttributes", "post"));
					sb.append(new HiddenTag("order", order));
					sb.append(new AAdminRenderer(context, order, attributes, order.getAttributeValues(), ARenderType.Input));
					sb.append("<div>" + new SubmitTag("Update details").setDisabled(order.isCancelled()) + "</div>");
					sb.append("</form>");

				}

				sb.append(new TableTag(null, "center").setStyle("width: 100%;"));
				sb.append("<tr><td width='50%' valign='top' style='padding-right: 6px;'>");

				billingAddress();

				if (Module.Delivery.enabled(context)) {
					deliveryAddress();
				}

				sb.append("</td><td width='50%' valign='top' style='padding-left: 6px;'>");

				if (Privilege.OrderPayment.yes(user, context)) {
					payment();
				}

				sb.append("</td></tr>");
				sb.append("</table>");

//				if (!order.isCancelled()) {
					lines();
//				}

				parcels();

				if (orderSettings.isMessages()) {
					sb.append(new MessagesPanel(context, order, false, false));
//					sb.append(new MessagesReplyPanel(context, false, order, new Link(OrderHandler.class, null, "order", order), true));
                    sb.append(new AdminTable("Add message"));

                    sb.append(new FormTag(OrderHandler.class, "addMessage", "post"));
                    sb.append(new HiddenTag(StringHelper.toCamelCase(order.getClass()), order));
                    sb.append("<tr><td>" + new TextAreaTag(context, "body", 80, 5) + "</td></tr>");
                    sb.append("<tr><td>" + new SubmitTag("Add message") + "</td></tr>");
                    sb.append("</form>");
                    sb.append("</table>");
                }

                sb.append(new AdminTable("Referrer"));
                if (order.getReferrer() != null) {
                    sb.append("<tr><td>" + order.getReferrer() + "</td></tr>");
                } else {
                    sb.append("<tr><td/></tr>");
                }
                sb.append("</table>");

                return sb.toString();
			}

			/**
			 * 
			 */
			private void vatable() {

				if (company.isVatRegistered()) {

					if (order.isOpen()) {

						sb.append(new FormTag(OrderHandler.class, "setVatable", "POST"));
						sb.append(new HiddenTag("order", order));

						SelectTag tag = new SelectTag(null, "vatable", order.isVatable());
						tag.addOption(true, "Yes");
						tag.addOption(false, "No");

						sb.append(new AdminRow("Vatable", "Is thiws order subject to VAT", tag.toString() + " " +
								new SubmitTag("Change vat status")));

						sb.append("</form>");

					} else {

						sb.append(new AdminRow("Vatable", "Is this order subject to VAT", (order.isVatable() ? "Yes" : "No:")));

					}

					if (order.hasVatExemptText()) {
						sb.append(new AdminRow("Vat exempt text", null, order.getVatExemptText()));
					}
				}
			}

		});
		return doc;
	}

	public Object moveDown() {

		if (orderLine == null) {
			return main();
		}

		order = orderLine.getOrder();
		List<OrderLine> lines = order.getLines();
		EntityUtil.moveDown(orderLine, lines);

		return main();
	}

	public Object moveUp() {

		if (orderLine == null) {
			return main();
		}

		order = orderLine.getOrder();
		List<OrderLine> lines = order.getLines();
		EntityUtil.moveUp(orderLine, lines);

		return main();
	}

	public Object productSearch() {

		if (order == null) {
			return main();
		}

		ItemSearcher searcher = new ItemSearcher(context);
		searcher.setKeywords(keywords);
		searcher.setStatus("Live");
		searcher.setItemModule(ItemModule.Ordering);

		final List<Item> items = searcher.getItems();

		if (items.size() == 0) {
			addWarning("No products found");
			return main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Order " + order.getIdString() + " - Product search", Tab.Orders);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Products found"));

				sb.append("<tr>");
				sb.append("<th>Name</th>");
				sb.append("<th>Availability</th>");
				sb.append("<th>Price</th>");
				sb.append("</tr>");

				for (Item item : items) {

					sb.append("<tr>");

					sb.append("<td>" + new LinkTag(OrderHandler.class, "addLine", item.getName(), "order", order, "item", item) + "</td>");
					sb.append("<td>" + item.getAvailability() + "</td>");
					sb.append("<td>" + item.getSellPriceInc(order.getAccount(), 1, null) + "</td>");
					sb.append("</tr>");

				}

				sb.append("</table>");

				sb.append(new ButtonTag(OrderHandler.class, null, "Return to order", "order", order));

				return sb.toString();
			}

		});
		return doc;
	}

	public Object removeLine() {

		if (order == null || line == null) {
			return main();
		}

		order.removeLine(line);
		order.log(user, "Line #" + line.getId() + " '" + line.getDescription() + "' removed");
		addMessage("Order line deleted");

		return main();
	}

	public Object removeParcel() {

		if (order == null || parcel == null) {
			return main();
		}

		order.removeParcel(parcel);

		clearParameters();
		return main();
	}

	public Object removeVoucher() {

		if (order == null) {
			return main();
		}

		order.removeVoucher();

		clearParameters();
		return main();
	}

	@Override
	public boolean runSecure() {
		return Config.getInstance(context).isSecured();
	}

	public Object saveAttributes() {

		if (order == null) {
			return main();
		}

		order.setAttributeValues(attributeValues);

		clearParameters();
		return main();
	}

	public Object sendNotificationEmails() throws ServletException {

		if (order == null) {
			return main();
		}

		try {
			order.sendNotificationEmails();
			addMessage("Notification sent");

		} catch (EmailAddressException e) {
			e.printStackTrace();
			addError(e);

		} catch (SmtpServerException e) {
			e.printStackTrace();
			addError(e);
		}

		return main();
	}

	public Object setBillingDate() {

		if (order == null) {
			return main();
		}

		order.setReinvoiceDate(reinvoiceDate);
		order.save();

		return new ActionDoc(context, "The reinvoice date has been updated", new Link(OrderHandler.class, null, "order", order));
	}

	public Object setCard() {

		if (order == null) {
			return main();
		}

		if (order.setCard(card)) {
			order.save();
			addMessage("Card used on order has been changed");
		}

		return main();
	}

	public Object setCategory() {

		if (order == null) {
			return main();
		}

		order.setCategory(category);
		order.save();

		return new ActionDoc(context, "The category has been updated", new Link(OrderHandler.class, null, "order", order));
	}

	public Object setCustomerReference() {

		if (customerReference == null || order == null) {
			return main();
		}

		order.setCustomerReference(customerReference);
		order.save();

		clearParameters();
		return main();
	}

	public Object setDeliveryAddress() {

		if (order == null || deliveryAddress == null) {
			return main();
		}

		if (!deliveryAddress.equals(order.getDeliveryAddress())) {

			order.setDeliveryAddress(deliveryAddress);
			order.save();

			order.log(user, "Delivery address changed to: " + deliveryAddress.getLabel(", ", false));

			addMessage("Delivery address changed");
		}

		return main();
	}

	public Object setInvoiceAddress() {

		if (order == null || invoiceAddress == null) {
			return main();
		}

		if (!invoiceAddress.equals(order.getBillingAddress())) {

			order.setBillingAddress(invoiceAddress);
			order.save();

			order.log(user, "Billing address changed to: " + invoiceAddress.getLabel(", ", false));
		}

		addMessage("Billing address changed");
		return main();
	}

	public Object setPaymentType() {

		if (order == null || paymentType == null) {
			return main();
		}

		order.setPaymentType(paymentType);
		order.save();

		clearParameters();
		return new ActionDoc(context, "The payment type has been updated", new Link(OrderHandler.class, null, "order", order));
	}

	public Object setRecurPeriod() {

		if (order == null) {
			return main();
		}

		order.setRecurPeriod(recurPeriod);
		order.save();

		return new ActionDoc(context, "The recurring date has been updated", new Link(OrderHandler.class, null, "order", order));
	}

	public Object setSalesPerson() {

		if (order == null) {
			return main();
		}

		if (salesPerson == null) {
			return main();
		}

		if (!salesPerson.equals(order.getSalesPerson())) {

			order.setSalesPerson(salesPerson);
			order.save();

			order.log(user, "Sales person changed to " + salesPerson.getName());
		}

		return new ActionDoc(context, "The salesperson has been updated", new Link(OrderHandler.class, null, "order", order));
	}

	public Object setStatus() {

		if (status == null || order == null) {
			return main();
		}

		if ("cancelled".equalsIgnoreCase(status)) {
			return cancel();
		}

		if (!order.getStatus().equals(status)) {

			order.setStatus(status);
			order.save();

			order.log(user, "Status changed to " + status);
		}

        clearParameters();
		return new ActionDoc(context, "The status has been updated", new Link(OrderHandler.class, null, "order", order));
	}

	public Object setVatable() {

		if (order == null || !company.isVatRegistered()) {
			return main();
		}

		if (order.isVatable() != vatable) {

			order.log(user, "Order vat changed: " + vatable);
			order.setVatable(vatable);

		}

		clearParameters();
		return main();
	}

	public Object setVoucher() {

		if (order == null || voucher == null) {
			return main();
		}

		order.setVoucher(voucher);

		clearParameters();
		return main();
	}

	public Object transact() throws ServletException {

		if (order == null) {
			return main();
		}

		if (order.getPaymentType() == PaymentType.CardTerminal) {

			if (paymentSettings.hasProcessor()) {

				try {

					logger.config("[Order] Transact order=" + order.getId() + ", amount=" + order.getTotalInc());

					Payment payment = order.transactCard();
					addMessage("Order billed \243" + order.getTotalInc() + " Txn: " + payment.getProcessorTransactionId());

					order.log(user, "Order transacted");

				} catch (Exception e) {

					e.printStackTrace();
					addError(e);
				}

			} else {

				return new TransactHandler(context, order).main();

			}
        } else if (order.getPaymentType() == PaymentType.PayPalStandard) {
            return new PaymentRedirect(context, order);
        }

		return main();
	}

	public Object unauthorise() {

		if (order == null) {
			return main();
		}

		order.unauthorise(user);

		clearParameters();
		return main();
	}

	public Object updateLines() {

		if (order == null) {
			return main();
		}

		for (OrderLine line : order.getLines()) {

			int qty = lineQty.get(line);
			Money price = linePrice.get(line);
            double vatRate = lineVat.get(line);
            if (price == null) {
				price = new Money(0);
			}

			try {
				order.setQty(line, qty, context);
				order.setSellPrice(line, price);
                order.setVatRate(line, vatRate);
            } catch (OrderStatusException e) {
				e.printStackTrace();
			}

			if (line.isMiscLine()) {

				String desc = lineDesc.get(line);

				logger.fine("setting line description=" + desc);

				if (desc != null) {
					line.setDescription(desc);
				}

				line.save();
			}

		}

		order.setDeliveryCharge(deliveryCharge);
		order.setDeliveryDescription(deliveryDescription);
		order.save();

		if (deliveryRate != null) {
			order.setDeliveryOption(deliveryRate);
		}

		order.recalc();

		addMessage("Order lines updated");
		clearParameters();
		return main();
	}


    public Object addMessage() throws ServletException {

        if (body == null) {
            return getHandler();
        } else if (order != null) {
            message = order.addMessage(user, body);
        } else {
            throw new RuntimeException("Unknown message owner");
        }

        addMessage("Your message has been added.");
        clearParameters();

        return getHandler();
    }

    private Object getHandler() throws ServletException {

        ActionDoc doc = null;

        if (message == null) {

            Link link = null;

            if (order != null) {
                link = new Link(OrderHandler.class, null, "order", order);
            }

            if (link != null) {
                doc = new ActionDoc(context, "Thank you", link);
            }

        } else {

            Link link = null;

            if (message.hasOrder()) {
                link = new Link(OrderHandler.class, null, "order", message.getOrder());
            }

            if (link != null) {
                doc = new ActionDoc(context, "Your message has been added", link);
            }

        }

        if (doc != null) {
            return doc;
        }

        return index();
    }

}
