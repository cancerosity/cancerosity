package org.sevensoft.ecreator.iface.admin.items.advanced;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.RandomHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.ItemsMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.input.AInputRenderer;
import org.sevensoft.ecreator.model.attributes.renderers.search.ASelectionRenderer;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.advanced.ItemActioner;
import org.sevensoft.ecreator.model.items.advanced.ItemActioner.Action;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.ImageSearchType;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 26 Jul 2006 11:20:12
 *
 */
@Path("admin-items-adv.do")
public class AdvItemEditorHandler extends AdminHandler {

	private Action					action;
	private String					searchName;
	private ItemType					searchItemType;
	private boolean					noCategory;
	private Category					searchCategory, actionCategory;
	private String					actionStatus;
	private double					actionVatRate;
	private MultiValueMap<Attribute, String>	attributeValues;
	private ImageSearchType				imageSearchType;
	private String					reference;
	private Money					sellPriceTo;
	private Money					sellPriceFrom;
	private String					searchStatus;
	private String					searchEmail;
	private Item					sourceItem;
	private String					replacement, target;
	private Amount					actionAmount;
	private Map<Attribute, String>		actionAttributeValues;

	private Attribute					actionAttribute;
	private String					actionAttributeValue;

	public AdvItemEditorHandler(RequestContext context) {
		super(context);
	}

	private ItemSearcher getSearcher() {

		ItemSearcher searcher = new ItemSearcher(context);
		searcher.setSeed(RandomHelper.getRandomNumber(0, 99999));

		searcher.setAttributeValues(attributeValues);

		searcher.setNoCategory(noCategory);

		searcher.setEmail(searchEmail);

		searcher.setImageSearchType(imageSearchType);
		searcher.setItemType(searchItemType);

		searcher.setName(searchName);

		searcher.setReference(reference);

		searcher.setSearchCategory(searchCategory);

		searcher.setSellPriceFrom(sellPriceFrom);
		searcher.setSellPriceTo(sellPriceTo);

		searcher.setStatus(searchStatus);

		return searcher;
	}

	@Override
	public Object main() throws ServletException {

		final List<ItemType> itemTypes = ItemType.get(context);
		if (itemTypes.isEmpty()) {
			return HttpServletResponse.SC_EXPECTATION_FAILED;
		}

		if (searchItemType == null) {
			searchItemType = itemTypes.get(0);
		}

		final SelectTag itemTypesTag = new SelectTag(context, "searchItemType", searchItemType);
		itemTypesTag.addOptions(itemTypes);
		itemTypesTag.setOnChange("this.form.elements['_a'].value = 'main'; this.form.submit();");

		if (action == null) {
			action = Action.AddCategory;
		}

		final Map<String, String> categories = Category.getSimpleMap(context);

		AdminDoc doc = new AdminDoc(context, user, "Advanced items editor", null);
//		doc.setMenu(new ItemsMenu(searchItemType));
		doc.addBody(new Body() {

            private void commands() {
                sb.append("<div class='actions'>" + new SubmitTag("Do action").setConfirmation("Are you sure you want to perform this action?") +
                        "</div>");
            }

			private void action() {

				final SelectTag categoriesTag = new SelectTag(context, "actionCategory");
				categoriesTag.setAny("-Select category-");
				categoriesTag.addOptions(categories);

				SelectTag actionTag = new SelectTag(context, "action", action);
				actionTag.addOptions(Action.values());
				actionTag.setLabelComparator();
				actionTag.setOnChange("this.form.elements['_a'].value = 'main'; this.form.submit();");

				sb.append(new AdminTable("Action"));

				sb.append(new AdminRow("Action type", "Choose the action required.", actionTag));

				switch (action) {

				case AttributeValue:

					SelectTag attributeTag = new SelectTag(context, "actionAttribute", actionAttribute, searchItemType.getAttributes(),
							"-Select attribute-");
					attributeTag.setOnChange("this.form.elements['_a'].value = 'main'; this.form.submit();");

					sb.append(new AdminRow("Attribute", "Choose the attribute to change value.", attributeTag));

					if (actionAttribute != null) {

						AInputRenderer inputRenderer = new AInputRenderer(context, actionAttribute, null);
						inputRenderer.setParamName("actionAttributeValue");

						sb.append(new AdminRow(actionAttribute.getName(), actionAttribute.getDescription(), inputRenderer));
					}

					break;

				case AddCategory:

					sb.append(new AdminRow("Category", "Choose the category to add.", categoriesTag));
					break;

				case CopyContent:
					sb.append(new AdminRow("Source item", "Enter the id of the item to copy content from.", new TextTag(context, "sourceItem", 12)));

					break;

				case CopyImages:
					break;

				case CopyItem:

					sb.append(new AdminRow("Source item", "Enter the id of the item to copy from.", new TextTag(context, "sourceItem", 12)));

					sb.append(new AdminRow("Find / replace", "Enter the text to be replaced in the item name.", "Find " +
							new TextTag(context, "target", 20) + " and replace with " + new TextTag(context, "replacement", 20)));

					break;

				case CopyOptions:
					sb.append(new AdminRow("Source item", "Enter the id of the item to copy options from.", new TextTag(context, "sourceItem", 12)));
					break;

				case MoveCategory:

					sb.append(new AdminRow("Category", "Choose the category to add.", categoriesTag));
					break;

				case RemoveAllCategories:
					break;

				case RemoveCategory:

					sb.append(new AdminRow("Category", "Choose the category to add.", categoriesTag));
					break;

				case SetSellPrice:

					sb.append(new AdminRow("Sell price", "Enter the new sell price.", new TextTag(context, "actionAmount", 12)));
					break;

				case SetStatus:

					SelectTag statusTag = new SelectTag(context, "actionStatus");
					statusTag.addOption("Live");
					statusTag.addOption("Disabled");

					sb.append(new AdminRow("Status", "Select the status to set.", statusTag));
					break;

				case SetVatRate:
					sb.append(new AdminRow("Vat rate", "Input the new vat rate.", new TextTag(context, "actionVatRate", 8)));
					break;

				case TitleCase:

				}

				sb.append("</table>");
			}

			private void criteria() {

				sb.append(new AdminTable("Criteria"));

				if (itemTypes.size() > 1) {
					sb.append(new AdminRow("Item Type", "Choose the item type to search for", itemTypesTag));
				}

				sb.append(new AdminRow("Name", "Enter the name or part of the " + searchItemType.getName().toLowerCase() + " name.", new TextTag(
						context, "searchName", 40)));

				if (ItemModule.Account.enabled(context, searchItemType)) {
					sb.append(new AdminRow("Email", "Include by login email.", new TextTag(context, "searchEmail", 40)));
				}

				if (searchItemType.isStatusControl()) {

					SelectTag tag = new SelectTag(context, "searchStatus", "LIVE");
					tag.setAny("-Any status-");
					for (String status : searchItemType.getStatuses()) {
						tag.addOption(status.toUpperCase());
					}

					sb.append(new AdminRow("Status", "Only include " + searchItemType.getNamePluralLower() + " of this status.", tag));
				}

				if (searchItemType.isCategories()) {

					SelectTag categoriesTag = new SelectTag(context, "searchCategory");
					categoriesTag.setAny("-Any category-");
					categoriesTag.addOptions(categories);

					sb.append(new AdminRow("Category", "Limit to " + searchItemType.getNamePluralLower() + " that are only in this category.",
							categoriesTag));

					sb.append(new AdminRow("Not in a category", "Only include " + searchItemType.getNamePluralLower() +
							" that are not in any category.", new CheckTag(context, "noCategory", true, false)));

				}

				if (ItemModule.Images.enabled(context, searchItemType)) {

					sb.append(new AdminRow("Images", "Choose to include images only, no images, or any.", new SelectTag(context, "imageSearchType",
							null, ImageSearchType.values(), "Any")));

				}

				if (ItemModule.Pricing.enabled(context, searchItemType)) {

					sb.append(new AdminRow("Sell price", "Limit to items by selling price.", "From " + new TextTag(context, "sellPriceFrom", 12) +
							" to " + new TextTag(context, "sellPriceTo", 12)));

				}

				int n = 0;
				for (Attribute attribute : searchItemType.getAttributes()) {

					sb.append(new AdminRow(attribute.getName(), attribute.getDescription(), new ASelectionRenderer(context, attribute)));

					n++;
					if (n == 10) {
						break;
					}
				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(AdvItemEditorHandler.class, "perform", "POST"));

                commands();
				criteria();
				action();
                commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	/**
	 * 
	 */
	public Object perform() throws ServletException {

		if (action == null) {
			addError("No action set!");
			return main();
		}

		if (searchItemType == null) {
			addError("No item type set");
			return main();
		}

		ItemSearcher searcher = getSearcher();
		ItemActioner actioner = new ItemActioner(context, searcher);

		switch (action) {

		case AttributeValue:
			if (actionAttribute != null) {
				actioner.setAttributeValue(actionAttribute, actionAttributeValue);
			}
			break;

		case CopyOptions:
			if (sourceItem != null) {
				actioner.copyOptions(sourceItem);
			}
			break;

		case TitleCase:
			actioner.titleCase();
			break;

		case CopyContent:
			if (sourceItem != null) {
				actioner.copyContent(sourceItem);
			}
			break;

		case AddCategory:
			if (actionCategory != null) {
				actioner.addCategory(actionCategory);
			}
			break;

		case SetStatus:
			if (actionStatus != null) {
				actioner.setStatus(actionStatus);
			}
			break;

		case CopyItem:
			if (sourceItem != null) {
				try {
					actioner.copyItem(sourceItem, target, replacement);
				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
				}
			}
			break;

		case MoveCategory:
			if (actionCategory != null) {
				actioner.moveCategory(actionCategory);
			}
			break;

		case SetVatRate:
			actioner.setVatRate(actionVatRate);
			break;

//		case RemoveImages:
//			actioner.removeImages();
//			break;

		case RemoveAllCategories:
			actioner.removeCategories();
			break;

		case RemoveCategory:
			if (actionCategory != null) {
				actioner.removeCategory(actionCategory);
			}
			break;

		case SetSellPrice:
			if (actionAmount != null) {
				actioner.setSellPrice(actionAmount);
			}
			break;

		case CopyImages:

			if (sourceItem != null) {

				try {
					actioner.copyImages(sourceItem);

				} catch (IOException e) {
					addError(e);

				} catch (ImageLimitException e) {
					addError(e);
				}

			}
			break;

		}

		addMessage("Action performed on " + actioner.getCount() + " items.");

		clearParameters();
		return main();
	}

}
