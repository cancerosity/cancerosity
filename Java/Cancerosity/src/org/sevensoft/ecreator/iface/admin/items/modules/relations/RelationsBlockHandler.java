package org.sevensoft.ecreator.iface.admin.items.modules.relations;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.items.relations.blocks.RelationsBlock;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 2 Apr 2007 08:13:26
 *
 */
@Path("admin-blocchi-relations.do")
public class RelationsBlockHandler extends BlockEditHandler {

	private RelationsBlock	block;

	public RelationsBlockHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Block getBlock() {
		return block;
	}

	@Override
	protected void saveSpecific() {
	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

}
