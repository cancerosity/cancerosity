package org.sevensoft.ecreator.iface.admin.crm.forum;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.accounts.permissions.DomainUtil;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionCollections;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.accounts.permissions.panels.PermissionsPanel;
import org.sevensoft.ecreator.model.crm.forum.Forum;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 03-Apr-2006 15:02:57
 *
 */
@Path("admin-forum.do")
public class ForumHandler extends AdminHandler {

	private Forum						forum;
	private String						name;
	private String						description;
	private boolean						blockEmailInPosts;
	private String						category;
	private boolean						locked;
	private boolean						enablePermissions;
	private MultiValueMap<String, PermissionType>	permissions;
	private String						restrictionForwardUrl;

	public ForumHandler(RequestContext context) {
		super(context);
	}

	public Object add() throws ServletException {

		forum = new Forum(context, "new forum");
		return edit();
	}

	public Object delete() throws ServletException {

		if (forum == null)
			return main();

		forum.delete();
		return main();
	}

	public Object edit() throws ServletException {

		if (forum == null)
			return main();

		AdminDoc doc = new AdminDoc(context, user, "Edit forum", Tab.Extras);
		doc.setMenu(new ExtrasMenu());
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update forum"));
				sb.append(new ButtonTag(ForumHandler.class, "delete", "Delete forum", "forum", forum)
						.setConfirmation("Are you sure you want to delete this forum?"));
				sb.append(new ButtonTag(ForumHandler.class, null, "Return to menu"));

				sb.append("</div>");
			}

			private void details() {

				sb.append(new AdminTable("Forum details"));

				sb.append(new AdminRow("Name", "Enter a name for this forum", new TextTag(context, "name", forum.getName(), 30) + ""
						+ new ErrorTag(context, "name", "<br/>")));

				sb.append(new AdminRow("Category", "Enter the category for this forum.", new TextTag(context, "category", forum.getCategory(), 30) + ""
						+ new ErrorTag(context, "category", "<br/>")));

				sb.append(new AdminRow("Description", "Optional description briefly describing this forum for display on the board overview page.",
						new TextAreaTag(context, "description", forum.getDescription(), 40, 3)));

				sb.append(new AdminRow("Block emails in posts", "Stop members from including email addresses in the posts they write.",
						new BooleanRadioTag(context, "blockEmailInPosts", forum.isBlockEmails())));

				sb.append(new AdminRow("Locked forum", "If a forum is locked then new posts cannot be made in it.", new BooleanRadioTag(context,
						"locked", forum.isLocked())));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(ForumHandler.class, "save", "POST"));
				sb.append(new HiddenTag("forum", forum));

                commands();
				details();
				if (Module.Permissions.enabled(context)) {
					sb.append(new PermissionsPanel(context, forum, PermissionCollections.getForum()));
				}
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		final List<Forum> forums = Forum.get(context);

		AdminDoc page = new AdminDoc(context, user, "Forums", Tab.Extras);
		page.addBody(new Body() {

			private void forums() {

				sb.append(new AdminTable("Forums"));

				sb.append("<tr><td colspan='4'>" + "These are the forums that currently exist. "
						+ "You can add a new forum by clicking on the 'add new forum' button at the bottom or you can "
						+ "edit / rename a forum by clicking the edit link to the right of the forum name.</td></tr>");

				sb.append("<tr>");
				sb.append("<th>Position</th>");
				sb.append("<th>Name</th>");
				sb.append("<th>Category</th>");
				sb.append("<th>Edit</th>");
				sb.append("</tr>");

				PositionRender pr = new PositionRender(ForumHandler.class, "forum");
				for (Forum forum : forums) {

					sb.append("<tr>");
					sb.append("<td>" + pr.render(forum) + "</td>");
					sb.append("<td>" + forum.getName() + "</td>");
					sb.append("<td>" + forum.getCategory() + "</td>");
					sb.append("<td>" + new LinkTag(ForumHandler.class, "edit", "Edit", "forum", forum) + "</td>");
					sb.append("</tr>");
				}

				sb.append("<tr>");
				sb.append("<td colspan='4'>" + new ButtonTag(ForumHandler.class, "add", "Add new forum") + "</td></tr>");
				sb.append("</tr>");

				sb.append("</table>");
				sb.append("</form>");
			}

			@Override
			public String toString() {

				forums();

				return sb.toString();
			}
		});
		return page;
	}

	public Object moveDown() throws ServletException {

		if (forum == null)
			return main();

		EntityUtil.moveDown(forum, Forum.get(context));
		return main();
	}

	public Object moveUp() throws ServletException {

		if (forum == null)
			return main();

		EntityUtil.moveUp(forum, Forum.get(context));
		return main();
	}

	public Object save() throws ServletException {

		if (forum == null) {
			return main();
		}

		test(new RequiredValidator(), "name");
		if (hasErrors()) {
			return edit();
		}

		forum.setName(name);
		forum.setCategory(category);
		forum.setDescription(description);
		forum.setBlockEmails(blockEmailInPosts);
		forum.setLocked(locked);

		// permissions
		forum.setPermissions(enablePermissions);
		forum.setRestrictionForwardUrl(restrictionForwardUrl);
		DomainUtil.savePermissions(context, forum, permissions, PermissionCollections.getForum());

		forum.save();

		addMessage("The changes to the forum have been saved.");
		clearParameters();
		return edit();

	}

}
