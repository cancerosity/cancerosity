package org.sevensoft.ecreator.iface.admin.interaction.sms;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.listings.ListingModuleHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.marketing.sms.credits.SmsCreditPackage;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 22-Mar-2006 18:45:22
 *
 */
@Path("admin-sms-credits-packages.do")
public class SmsCreditPackageHandler extends AdminHandler {

	private Money			fee;
	private int				credits;
	private SmsCreditPackage	smsCreditPackage;

	public SmsCreditPackageHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		if (fee == null || credits == 0) {
			return main();
		}

		new SmsCreditPackage(context, credits, fee);
		return new ActionDoc(context, "A new sms credit package has been created", new Link(SmsCreditPackageHandler.class));
	}

	public Object delete() throws ServletException {

		if (smsCreditPackage == null) {
			return main();
		}

		smsCreditPackage.delete();
		return new ActionDoc(context, "This sms credit package has been deleted", new Link(SmsCreditPackageHandler.class));
	}

	public Object edit() throws ServletException {

		if (smsCreditPackage == null) {
			return new SettingsMenuHandler(context).main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit sms credit package", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update package"));
				sb.append(new ButtonTag(SmsCreditPackageHandler.class, "delete", "Delete smsCreditPackage package", "smsCreditPackage",
						smsCreditPackage).setConfirmation("Are you sure you want to delete this listing package?"));
				sb.append(new ButtonTag(ListingModuleHandler.class, null, "Return to smsCreditPackage module", "smsCreditPackage", smsCreditPackage));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Fee", "Enter the name of this listing package", new TextTag(context, "fee", smsCreditPackage.getFee(), 12)));
				sb.append(new AdminRow("Credits", "Enter the name of this listing package", new TextTag(context, "credits", smsCreditPackage
						.getCredits(), 12)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(SmsCreditPackageHandler.class, "save", "POST"));
				sb.append(new HiddenTag("smsCreditPackage", smsCreditPackage));
                commands();
				general();

				commands();

				sb.append("</form>");

				return sb.toString();
			}
		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Sms credit packages", null);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Sms credit packages"));

				sb.append("<tr>");
				sb.append("<th width='60'>Position</th>");
				sb.append("<th>Fee</th>");
				sb.append("<th>Credits</th>");
				sb.append("<th width='10'> </th>");
				sb.append("</tr>");

				PositionRender pr = new PositionRender(SmsCreditPackageHandler.class, "smsCreditPackage");
				for (SmsCreditPackage smsCreditPackage : SmsCreditPackage.get(context)) {

					sb.append("<tr>");
					sb.append("<td width='60'>" + pr.render(smsCreditPackage) + "</td>");
					sb.append("<td>"
							+ new LinkTag(SmsCreditPackageHandler.class, "edit", new SpannerGif(), "smsCreditPackage", smsCreditPackage)
							+ " " + smsCreditPackage.getFee() + "</td>");
					sb.append("<td>" + smsCreditPackage.getCredits() + "</td>");
					sb
							.append("<td width='10'>"
									+ new LinkTag(SmsCreditPackageHandler.class, "delete", new DeleteGif(), "smsCreditPackage", smsCreditPackage).setConfirmation("Are you sure you want to delete this package?") + "</td>");
					sb.append("</tr>");

				}

				sb.append(new FormTag(SmsCreditPackageHandler.class, "create", "POST"));
				sb.append("<tr><td colspan='4'>Add package: Fee " + new TextTag(context, "fee", 12) + " Credits " + new TextTag(context, "credits", 12)
						+ " " + new SubmitTag("Add") + "</td></tr>");
				sb.append("</form>");

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object moveDown() throws ServletException {

		if (smsCreditPackage == null) {
			return main();
		}

		List<SmsCreditPackage> packages = SmsCreditPackage.get(context);
		EntityUtil.moveDown(smsCreditPackage, packages);
		return main();
	}

	public Object moveUp() throws ServletException {

		if (smsCreditPackage == null) {
			return main();
		}

		List<SmsCreditPackage> packages = SmsCreditPackage.get(context);
		EntityUtil.moveUp(smsCreditPackage, packages);
		return main();
	}

	public Object save() throws ServletException {

		if (smsCreditPackage == null) {
			return main();
		}

		smsCreditPackage.setCredits(credits);
		smsCreditPackage.setFee(fee);
		smsCreditPackage.save();

		return new ActionDoc(context, "This sms credit package has been updated", new Link(SmsCreditPackageHandler.class, "edit", "smsCreditPackage",
				smsCreditPackage));
	}
}
