package org.sevensoft.ecreator.iface.admin.containers.blocks.panels;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockHandler;
import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.media.images.galleries.Gallery;
import org.sevensoft.ecreator.model.media.images.galleries.blocks.GalleryBlock;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author dme 31-Aug-2007 10:41:09
 */
public class GalleriesListPanel extends Panel {

    private static Logger logger = Logger.getLogger("ecreator");

    private final List<Gallery> galleries;

    private final List<Block> galleryBlocks;
    private Category category;
    private ItemType itemType;
    private final BlockOwner owner;
    private MiscSettings miscSettings;

    public GalleriesListPanel(RequestContext context, BlockOwner owner) {

        super(context);

        this.owner = owner;
        this.miscSettings = MiscSettings.getInstance(context);

        if (owner instanceof Category) {
            this.category = (Category) owner;
        } else if (owner instanceof ItemType) {
            this.itemType = (ItemType) owner;
        } else {
            throw new RuntimeException("Owner type not recognised: " + owner);
        }

        this.galleryBlocks = new ArrayList<Block>(CollectionsUtil.filter(owner.getBlocks(), new Predicate<Block>() {
            public boolean accept(Block b) {
                return b instanceof GalleryBlock;
            }
        }));

        //load all galleries
        this.galleries = SimpleQuery.execute(context, Gallery.class);

        logger.fine("[GalleriesListPanel] showing gallery blocks for owner=" + owner);
        logger.fine("[GalleriesListPanel] gallery blocks=" + galleryBlocks);
    }

    private SelectTag getAddGalleryTag() {

        SelectTag galleriesTag = new SelectTag(context, "addGallery");
        galleriesTag.setLabelComparator();
        galleriesTag.setAny("-Select gallery-");

        if (Module.Galleries.enabled(context)) {
            for (Gallery gallery : galleries) {
                galleriesTag.addOption(gallery, gallery.getName());
            }
        }

        return galleriesTag;
    }


    public void makeString() {

        sb.append(new AdminTable("Galleries"));

        if (miscSettings.isAdvancedMode()) {
            sb.append("<tr>");
            sb.append("<th width='100'>Position</th>");
            sb.append("<th>Name</th>");
            sb.append("<th width='10'> </th>");
            sb.append("</tr>");
        }

        PositionRender pr = new PositionRender(BlockHandler.class, "block");

        for (Block block : galleryBlocks) {

            if (miscSettings.isAdvancedMode() || block.isSimple()) {

                sb.append("<tr>");

                if (miscSettings.isAdvancedMode()) {
                    sb.append("<td width='100'>" + pr.render(block.getInstanceId()) + "</td>");
                }

                sb.append("<td>");

                if (block.isEditable()) {

                    Class clazz = block.getEditHandler();
                    if (clazz != null) {
                        sb.append(new LinkTag(clazz, null, new SpannerGif(), "block", block));
                        sb.append(" ");
                    }
                }

                GalleryBlock galleryBlock = (GalleryBlock) block;
                Gallery gallery = galleryBlock.getGallery();
                sb.append(gallery != null ? gallery.getLabel() : "gallery block (delete and re-add me)");
                sb.append("</td>");

                sb.append("<td width='10'>" +
                        new LinkTag(BlockHandler.class, "delete", new DeleteGif(), "block", block.getInstanceId())
                                .setConfirmation("Are you sure you want to delete this gallery from this page?") + "</td>");

                sb.append("</tr>");

            }
        }

        sb.append("<tr><td colspan='2'>Add gallery: " + getAddGalleryTag() + " " + new SubmitTag("Add") + "</td><td align='right' colspan=2'>");

        sb.append("</td></tr>");

        sb.append("</table>");

    }
}