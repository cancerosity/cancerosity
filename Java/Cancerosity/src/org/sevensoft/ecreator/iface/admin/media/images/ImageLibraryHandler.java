package org.sevensoft.ecreator.iface.admin.media.images;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.superstrings.comparators.NaturalStringComparator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.media.images.ImageOwner;
import org.sevensoft.ecreator.model.media.images.ImageUtil;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

import javax.servlet.ServletException;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author sks 06-Jan-2006 13:19:30
 */
@Path("image-library.do")
public class ImageLibraryHandler extends AdminHandler {

    private String filename;
    private Upload upload;

    private int page;

    private int limit = 50;
    private String urls;

    private String letter;

    public ImageLibraryHandler(RequestContext context) {
        super(context);
    }

    public Object delete() throws ServletException {

        if (filename != null) {

            /*
                * Delete from items if in use
                */
            for (Img image : Img.getByFilename(context, filename)) {

                final ImageOwner owner = image.getOwner();
                owner.removeImage(image, false, false);
            }

//            File file = new File(config.getRealImagesPath() + "/" + filename);
            File file = ResourcesUtils.getRealImage(filename);
            if (file.exists()) {
                file.delete();
            }

//            file = new File(config.getRealThumbnailsPath() + "/" + filename);
            file = ResourcesUtils.getRealThumbnail(filename);
            if (file.exists()) {
                file.delete();
            }
        }

        addMessage("The image has been deleted and removed from all items");
        clearParameters();
        return main();
    }

    private int download(String url) {

        try {

            Img.download(context, url);
            return 1;

        } catch (IOException e) {
            //e.printStackTrace();
            return 0;
        }
    }

    public Object importImages() throws ServletException {

        logger.fine("[ImageLibraryHandler] importImages, upload=" + upload + ", urls=" + urls);

        int i = 0;
        if (upload != null) {
            i += upload();
        }

        if (urls != null) {

            for (String url : urls.split("\n")) {

                i += download(url);

            }
        }

        if (i > 0) {
            addMessage(i + " image(s) have been successfully imported.");
        }

        clearParameters();
        return main();
    }

    public Object list() {

        /*
           * get a count of files in the image dir. If it is less than 500 then just show all pagated.
           * otherwise show a letter selector
           */
//		File imageDir = context.getRealFile(Config.ImagesPath);
        File imageDir = ResourcesUtils.getRealImagesDir();

        final int numberOfImages = imageDir.list().length;
        final Results results = new Results(numberOfImages, page, 50);

        final String[] filenames;
        if (numberOfImages < 500) {

            filenames = imageDir.list(new FilenameFilter() {

                public boolean accept(File dir, String name) {
                    return true;
                }

            });

        } else if (letter != null) {

            letter = letter.toLowerCase();
            filenames = imageDir.list(new FilenameFilter() {

                public boolean accept(File dir, String name) {
                    return name.substring(0, 1).toLowerCase().equals(letter);
                }

            });

        } else {

            filenames = null;
        }

        /*
           * Show filenames by natural name
           */
        if (filenames != null) {
            Arrays.sort(filenames, NaturalStringComparator.instance);
        }

        AdminDoc doc = new AdminDoc(context, user, "Image library", Tab.Images);
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
                sb.append("</div>");
            }

            @SuppressWarnings("null")
            private void images() {

                sb.append(new AdminTable("Images"));

                if (results.hasMultiplePages()) {

                    sb.append("<tr><td colspan='6'>" +
                            new ResultsControl(context, results, new Link(ImageLibraryHandler.class, "list", "letter", letter)) + "</td></tr>");

                }

                for (int n = results.getStartIndex(); n < results.getEndIndex() && n < filenames.length; n++) {

                    String filename = filenames[n];

//                    File file = new File(config.getRealImagesPath() + "/" + filename);
                    File file = ResourcesUtils.getRealImage(filename);

                    sb.append("<tr>");
                    sb.append("<td width='30'>" + (n + 1) + "</td>");
                    sb.append("<td width='40' align='center'>" +
                            new LinkTag(Config.ImagesPath + "/" + filename, new ImageTag(Config.ImagesPath + "/" + filename, 0, 30, 0)).setTarget("_blank") + "</td>");
                    sb.append("<td>" + filename + "</td>");
                    sb.append("<td width='60'>" + file.length() / 1000 + "kb</td>");
                    sb.append("<td width='60'>" + new Date(file.lastModified()).toString("dd/MM/yyyy") + "</td>");
                    sb.append("<td width='60'>" +
                            new LinkTag(ImageLibraryHandler.class, "delete", "Delete", "filename", filename)
                                    .setConfirmation("Are you sure you want to delete this image?") + "</td>");
                    sb.append("</tr>");
                }

                sb.append("</table>");

            }

            private void letters() {

                sb.append(new AdminTable("Letter"));
                sb.append("<tr><td>");

                for (char c = '0'; c < 91; c++) {

                    if (c == ':') {
                        c = 'A';
                    }

                    sb.append(new LinkTag(ImageLibraryHandler.class, null, c, "letter", c));
                    sb.append(" ");
                }

                sb.append("</td></tr>");
                sb.append("</table>");
            }

            @Override
            public String toString() {

                commands();
                upload();

                if (numberOfImages > 500) {
                    letters();
                }

                if (filenames != null) {
                    if (filenames.length > 0) {
                        images();
                    }
                }

                commands();

                return sb.toString();
            }

            private void upload() {

                sb.append(new FormTag(ImageLibraryHandler.class, "importImages", "multi"));
                sb.append(new AdminTable("Import"));

                sb.append(new AdminRow("Upload from file",
                        "You can upload images or files here. If you have multiple images you can upload a *.zip file and the "
                                + "server will automatically unzip.", new FileTag("upload")));

                sb
                        .append(new AdminRow(
                                "Download from site",
                                "Enter URLs for an image and the images will be imported from that site if the server supports the image format they are in.",
                                new TextAreaTag(context, "urls", 50, 3)));

                sb.append("<tr><td colspan='2'>" + new SubmitTag("Upload") + "</td></tr>");

                sb.append("</table>");
                sb.append("</form>");
            }

        });
        return doc;
    }

    @Override
    public Object main() throws ServletException {
        return list();
    }

    private int upload() {

        try {
            return ImageUtil.upload(context, upload);
        } catch (IOException e) {
            addError(e);
            return 0;
        }

    }

}
