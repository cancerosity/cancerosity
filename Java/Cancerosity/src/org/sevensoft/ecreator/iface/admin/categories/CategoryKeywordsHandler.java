package org.sevensoft.ecreator.iface.admin.categories;

import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.misc.seo.panels.KeywordSearchRenderer;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 17-Nov-2005 18:18:05
 * 
 */
@Path("admin-categories-keywords.do")
public class CategoryKeywordsHandler extends AdminHandler {

	private String		term;
	private List<String>	suggestion;
	private Category		category;

	public CategoryKeywordsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (category == null)
			return new CategoryHandler(context).main();

		setAttribute("search_box", new KeywordSearchRenderer(context, category));

		return "categories/keywords.jsp";
	}

	public Object save() throws ServletException {

		if (category == null)
			return main();

		StringBuilder sb = new StringBuilder();
		Iterator<String> iter = suggestion.iterator();
		if (iter.hasNext())
			sb.append(iter.next());
		while (iter.hasNext()) {
			sb.append(", ");
			sb.append(iter.next());
		}

		category.setKeywords(sb.toString());
		category.save();

		return new CategoryHandler(context, category).edit();
	}

	public Object suggest() throws ServletException {

		if (category == null)
			return main();

		//		SortedBag<String> keywords;
		//		try {
		//
		//			keywords = new KeywordGen().gen(term, 10);
		//			setAttribute("suggestions", new KeywordSuggestionsRenderer(context, keywords, category));
		//
		//		} catch (IOException e) {
		//
		//			e.printStackTrace();
		//
		//		}

		return main();
	}
}
