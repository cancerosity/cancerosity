package org.sevensoft.ecreator.iface.admin.search;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.search.forms.Filter;
import org.sevensoft.ecreator.model.search.forms.SearchForm;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 18 Apr 2007 16:02:42
 *
 */
@Path("admin-search-filters.do")
public class FilterHandler extends AdminHandler {

	private Filter		filter;
	private SearchForm	searchForm;
	private Attribute		attribute;

	public FilterHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		if (filter == null || attribute == null || searchForm == null) {
			return index();
		}

		searchForm.addFilter(attribute);

		return new ActionDoc(context, "A new filter has been created", new Link(SearchFormHandler.class, null, "searchForm", searchForm));

	}

	public Object delete() throws ServletException {

		if (filter == null) {
			return index();
		}

		searchForm = filter.getSearchForm();
		filter.delete();

		return new ActionDoc(context, "This filter has been deleted", new Link(SearchFormHandler.class, null, "searchForm", searchForm));
	}

	@Override
	public Object main() throws ServletException {
		return null;
	}

}
