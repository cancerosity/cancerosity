package org.sevensoft.ecreator.iface.admin.feeds.csv;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.superstrings.comparators.NaturalStringComparator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.csv.CsvImportFeed;
import org.sevensoft.ecreator.model.feeds.csv.CsvImportFeedField;
import org.sevensoft.ecreator.model.feeds.csv.FieldType;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 22-Nov-2005 09:42:19
 * 
 */
@Path("admin-feeds-csv-fields.do")
public class CsvFeedFieldHandler extends AdminHandler {

	private CsvImportFeedField	field;
	private String			newField;
	private CsvImportFeed		feed;
	private String			name;
	private int				startRow;
	private FieldType			fieldType;
	private Attribute			attribute;
	private String			columns;
	private String			prefix;
	private boolean			lookup;
	private boolean			combinationLookup;
	private boolean			individualLookup;
	private boolean			overwrite;
	private String			suffix;
	private boolean			combine;
	private String			separator;
	private String			value;
	private String			dateFormat;
	private boolean			valueFromReference;
	private boolean			partialImageMatch;
	private String			stripCharacters;
    private boolean			partialPdfMatch;
    private String          child;
    private String          parent;

    public CsvFeedFieldHandler(RequestContext context) {
		super(context);
	}

	public Object delete() throws ServletException {

		if (field == null) {
			return main();
		}

		feed = field.getFeed();
		feed.removeField(field);

		addMessage("Field removed");
		return new CsvFeedHandler(context, feed).main();
	}

	@Override
	public Object main() throws ServletException {

		if (!Module.Feeds.enabled(context) && !isSuperman()) {
			return new DashboardHandler(context).main();
		}

		if (field == null) {
			return new CsvFeedHandler(context).main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit importer field", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update field"));
				sb.append(new ButtonTag(CsvFeedHandler.class, null, "Return to custom feed", "feed", field.getFeed()));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("Settings"));

				{
					SelectTag tag = new SelectTag(context, "fieldType", field.getFieldType());
					tag.addOptions(FieldType.get());

					sb.append(new AdminRow("Field type", "Set how often to run this feed in hours.", tag));
				}

				switch (field.getFieldType()) {

				default:
					break;

				case Name:
					indLookup();
					break;

				case Attribute:

					SelectTag tag = new SelectTag(context, "attribute", field.getAttribute());
					tag.setAny("Not set");

					final List<Attribute> attributes = field.getFeed().getItemType().getAttributes();
					Collections.sort(attributes, new Comparator<Attribute>() {

						public int compare(Attribute o1, Attribute o2) {
							return NaturalStringComparator.instance.compare(o1.getName(), o2.getName());
						}
					});

					if (field.getFeed().hasItemType()) {
						tag.addOptions(attributes);
					}

					sb.append(new AdminRow("Attribute", "Select the attribute to be used for this field.", tag));

					sb
							.append(new AdminRow(
									"Overwrite",
									"Replace existing values with the new values in the feed. If disabled will add in these values in addition to what already exists.",
									new BooleanRadioTag(context, "overwrite", field.isOverwrite())));

					indLookup();

					sb.append(new AdminRow("Combination lookup", "Use this field to lookup the item.", new BooleanRadioTag(context,
							"combinationLookup", field.isCombinationLookup())));

					sb.append(new AdminRow("Date format", "Enter the format of the date string", new TextTag(context, "dateFormat", field
							.getDateFormat(), 20)));

					break;

				case Image:

					sb.append(new AdminRow("Partial image match", "Use any image whose filename contains the value of this field.",
							new BooleanRadioTag(context, "partialImageMatch", field.isPartialImageMatch())));

					break;

                case PDF:

					sb.append(new AdminRow("Partial PDF match", "Use any PDF whose filename contains the value of this field.",
							new BooleanRadioTag(context, "partialPdfMatch", field.isPartialPdfMatch())));

					break;

                case AddCategory:

                    sb.append(new AdminRow("Parent", "Parent category row", new TextTag(context, "parent", field.getParent(), 30)));

                    sb.append(new AdminRow("Child", "Child category row", new TextTag(context, "child", field.getChild(), 30)));

                    break;

                }

				sb.append(new AdminRow("Value", "Use a constant value instead of reading from the record", new TextTag(context, "value", field
						.getValue(), 30)));

				sb.append(new AdminRow("Value from reference", "Use this item's reference as the value for this field.", new BooleanRadioTag(context,
						"valueFromReference", field.isValueFromReference())));

				sb.append(new AdminRow("Strip characters", "Strip these characters from the value before processing.", new TextTag(context,
						"stripCharacters", field.getStripCharacters(), 20)));

				if (!field.hasValue()) {

                    sb
                            .append(new AdminRow("Columns", null, new TextTag(context, "columns", StringHelper.implode(field.getColumns(), ",",
                                    true).replaceAll(CsvImportFeedField.WHITESPACE_SEPARATOR + ",", " "), 12)));

					sb.append(new AdminRow("Combine", "Combine the values of each column with a space separator", new BooleanRadioTag(context,
							"combine", field.isCombine())));

					sb.append(new AdminRow("Prefix", "Prefix each value", new TextTag(context, "prefix", field.getPrefix(), 30)));
					sb.append(new AdminRow("Suffix", null, new TextTag(context, "suffix", field.getSuffix(), 30)));

				}

				sb.append("</table>");
			}

			private void indLookup() {
				sb.append(new AdminRow("Individual lookup", "Use this field to lookup the item.", new BooleanRadioTag(context, "individualLookup",
						field.isIndividualLookup())));
			}

			@Override
			public String toString() {

				sb.append(new FormTag(CsvFeedFieldHandler.class, "save", "post"));
				sb.append(new HiddenTag("field", field));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}
		});

		return doc;
	}

	//	public Object moveDown() throws ServletException {
	//
	//		if (field == null) {
	//			return main();
	//		}
	//
	//		List<CsvImportFeedField> fields = field.getFeed().getFields();
	//		EntityUtil.moveDown(field, fields);
	//		return new CsvFeedHandler(context, field.getFeed()).main();
	//	}
	//
	//	public Object moveUp() throws ServletException {
	//
	//		if (field == null) {
	//			return main();
	//		}
	//
	//		List<CsvImportFeedField> fields = field.getFeed().getFields();
	//		EntityUtil.moveUp(field, fields);
	//		return new CsvFeedHandler(context, field.getFeed()).main();
	//	}

	public Object save() throws ServletException {

		if (field == null) {
			return main();
		}

		if (!Module.UserFeeds.enabled(context) && !isSuperman()) {
			return main();
		}

		if (fieldType != null) {
			field.setFieldType(fieldType);
		}

		field.setAttribute(attribute);

		field.setColumns(StringHelper.explodeStrings(columns, "[^a-zA-Z0-9]", false));
		field.setValueFromReference(valueFromReference);
		field.setPrefix(prefix);
		field.setSuffix(suffix);
		field.setOverwrite(overwrite);
		field.setIndividualLookup(individualLookup);
		field.setCombinationLookup(combinationLookup);
		field.setCombine(combine);
		field.setValue(value);
		field.setDateFormat(dateFormat);
		field.setPartialImageMatch(partialImageMatch);
		field.setStripCharacters(stripCharacters);
        field.setPartialPdfMatch(partialPdfMatch);
        field.setChild(child);
        field.setParent(parent);
        field.save();

		clearParameters();
		return main();
	}

}
