package org.sevensoft.ecreator.iface.admin.ecom.payments;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.ecom.payments.results.Callback;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.ResultsControl;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 5 Feb 2007 07:38:43
 *
 */
@Path("admin-payments-callbacks.do")
public class CallbacksHandler extends AdminHandler {

    private static final int RESULTS_PER_PAGE = 50;

    private int page;

	public CallbacksHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

        final Results results = new Results(Callback.size(context), page, RESULTS_PER_PAGE);
		AdminDoc doc = new AdminDoc(context, user, "Callbacks", null);
		doc.addBody(new Body() {

			private void callbacks() {
                sb.append(new ResultsControl(context, results, new Link(CallbacksHandler.class)));
                
				sb.append(new AdminTable("Callbacks"));

				sb.append("<tr>");
				sb.append("<th>Date / Time</th>");
				sb.append("<th>Payment type</th>");
				sb.append("<th>Details</th>");
				sb.append("</tr>");

                for (Callback callback : Callback.get(context, results.getStartIndex(), RESULTS_PER_PAGE)) {

					sb.append("<tr>");
					sb.append("<td>" + callback.getTime().toString("dd-MMM-yyyy HH:mm") + "</td>");
					sb.append("<td>" + callback.getPaymentType() + "</td>");
					sb.append("<td>" + callback.getDetails() + "</td>");
					sb.append("</tr>");
				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				callbacks();

				return sb.toString();
			}

		});

		return doc;
	}
}
