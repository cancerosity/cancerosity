package org.sevensoft.ecreator.iface.admin.design.select;

import org.sevensoft.ecreator.iface.EcreatorRenderer;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.model.extras.select.SelectableOwner;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import java.util.Iterator;
import java.util.Arrays;
import java.util.List;

/**
 * User: Tanya
 * Date: 17.04.2012
 */
public class ObjectsSelectablePanel extends EcreatorRenderer {

    private final List<? extends SelectableOwner> selectableOwner;
    private String deleteParam;

    private Class<? extends AdminHandler> editClazz;
    private String editAction;
    private String editParam;
    private Object[] editExtraParams = new Object[0];

    public ObjectsSelectablePanel(RequestContext context) {
        this(context, null);
    }

    public ObjectsSelectablePanel(RequestContext context, List<? extends SelectableOwner> selectableOwner) {
        super(context);
        this.selectableOwner = selectableOwner;
    }

    public void setEditClazz(Class<? extends AdminHandler> editClazz) {
        this.editClazz = editClazz;
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public void setEditParam(String editParam) {
        this.editParam = editParam;
    }

    public void setEditExtraParams(Object... editExtraParams) {
        this.editExtraParams = editExtraParams;
    }

    public void setDeleteParam(String deleteParam) {
        this.deleteParam = deleteParam;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(
                "\t<script>\n" +
                        "\t$(function() {\n" +

                        "\t\t$( \"#selectable\" ).selectable();\n" +

                        "\t\t$(\"#selectable\").bind(\"selectableselecting\", function(event, ui) {\n" +
//                        "\t\t\t return;\n" +
                        "\t\t});\n" +

                        "\t\t$( \"#selectable\" ).selectable({\n" +
                        "\t\t\tselecting: function(event, ui) {\n" +
                        "\t\t\t\t ui.selecting.childNodes[1].childNodes[0].checked = true;\n" +
                        "\t\t\t}\n" +
                        "\t\t});\n" +

                        "\t\t$(\"#selectable\").bind(\"selectableunselecting\", function(event, ui) {\n" +
//                        "\t\t\t return;\n" +
                        "\t\t});\n" +

                        "\t\t$( \"#selectable\" ).selectable({\n" +
                        "\t\t\tunselecting: function(event, ui) {\n" +
                        "\t\t\t\t ui.unselecting.childNodes[1].childNodes[0].checked = false;\n" +
                        "\t\t\t}\n" +
                        "\t\t});\n" +

                        "\t});\n" +

                        "\t</script>");


        sb.append("<ul id='selectable'>");

        int n = 1;
        for (SelectableOwner owner : selectableOwner) {

            sb.append("<li><span></span>");

            double position = 2.7;

            if (deleteParam != null) {
                sb.append("<span style='position:absolute;left:" + position + "em;'>" + new CheckTag(context, deleteParam, owner, false) + "</span>");
                position = position + 6.5;
            }

            sb.append("<span style='position:absolute;left:" + position + "em;'>" + n++ + "</span>");
            position = position + 6.5;
            LinkTag linkTag = new LinkTag(editClazz, editAction, new SpannerGif(), editParam, owner);

            Iterator iter = Arrays.asList(editExtraParams).iterator();
            while (iter.hasNext()) {
                linkTag.addParameter(String.valueOf(iter.next()), iter.next());
            }
            linkTag.setOnClick("window.location.href='" + linkTag.getUrl() + "';");

            sb.append("<span style='position:absolute;left:" + position + "em;'>" + linkTag +
                    " " + owner.getSelectableName() + "</span>");

            sb.append("</li>");

        }
        sb.append("</ul>");


        return sb.toString();
    }
}
