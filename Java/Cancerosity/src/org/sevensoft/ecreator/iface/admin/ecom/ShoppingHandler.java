package org.sevensoft.ecreator.iface.admin.ecom;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.ecom.delivery.DeliveryOptionsHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.OrderSearchHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.PlaceOrderHandler;
import org.sevensoft.ecreator.iface.admin.ecom.promotions.PromotionHandler;
import org.sevensoft.ecreator.iface.admin.ecom.vouchers.VoucherHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 23 Nov 2006 17:10:50
 *
 */
@Path("admin-ecom.do")
public class ShoppingHandler extends AdminHandler {

	public ShoppingHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Shopping", Tab.Orders);
		if (miscSettings.isAdvancedMode()) {
			doc.setMenu(new ShoppingMenu());
		}
		doc.addBody(new Body() {

			@Override
			public String toString() {

				StringBuilder sb = new StringBuilder();

				sb.append(new TableTag("simplelinks", "center"));
				sb.append("<tr><th align='center'>What would you like to do?</th></tr>");

				sb.append("<tr><td align='center'>" + new LinkTag(PlaceOrderHandler.class, null, "I want to add a new sales order") + "</td></tr>");

				sb.append("<tr><td align='center'>" + new LinkTag(OrderSearchHandler.class, null, "I want to view and process my received orders")
						+ "<br/></br></td></tr>");

				if (Module.Delivery.enabled(context)) {

					sb.append("<tr><td align='center'>" + new LinkTag(DeliveryOptionsHandler.class, null, "I want to edit my delivery rates")
							+ "<br/></br></td></tr>");

				}

				if (Module.Vouchers.enabled(context)) {

					sb.append("<tr><td align='center'>" + new LinkTag(VoucherHandler.class, null, "I want to edit the discount vouchers for my shop")
							+ "<br/></br></td></tr>");

				}

				if (Module.Promotions.enabled(context)) {

					sb.append("<tr><td align='center'>"
							+ new LinkTag(PromotionHandler.class, null, "I want to edit the promotions on offer to my customers")
							+ "<br/></br></td></tr>");

				}

				sb.append("</table>");

				return sb.toString();
			}
		});
		return doc;
	}
}
