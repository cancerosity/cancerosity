package org.sevensoft.ecreator.iface.admin.crm.jobsheets;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.samdate.Period;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.crm.jobsheets.AutoJob;
import org.sevensoft.ecreator.model.crm.jobsheets.Jobsheet;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 6 Nov 2006 16:37:16
 *
 */
@Path("admin-jobsheets-automatico.do")
public class AutoJobHandler extends AdminHandler {

	private AutoJob	autojob;
	private String	name;
	private String	content;
	private ItemType	itemType;
	private Jobsheet	jobsheet;
	private Period	deadline;
	private AutoJob	follows;

	public AutoJobHandler(RequestContext context) {
		super(context);
	}

	public Object add() throws ServletException {

		if (name == null)
			return main();

		new AutoJob(context, name);
		clearParameters();
		return main();
	}

	public Object delete() throws ServletException {

		if (autojob == null)
			return main();

		autojob.delete();
		return main();
	}

	public Object edit() throws ServletException {

		if (autojob == null)
			return main();

		AdminDoc doc = new AdminDoc(context, user, "Edit auto job: " + autojob.getName(), Tab.Extras);
		doc.setMenu(new ExtrasMenu());
		doc.addBody(new Body() {

            private void commands() {
                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update auto job"));
                sb.append(new ButtonTag(AutoJobHandler.class, "delete", "Delete auto job", "autojob", autojob)
                        .setConfirmation("Are you sure you want to delete this auto job?"));
                sb.append(new ButtonTag(AutoJobHandler.class, null, "Return to menu"));
                sb.append("</div>");
            }

			@Override
			public String toString() {

				sb.append(new FormTag(AutoJobHandler.class, "save", "post"));
				sb.append(new HiddenTag("autojob", autojob));

                commands();
                
				sb.append(new AdminTable("Auto job details"));

				sb.append(new AdminRow("Name", "Enter the name of the job to be created", new TextTag(context, "name", autojob.getName(), 40)));

				sb.append(new AdminRow("Content", "Enter the initial content of the job", new TextAreaTag(context, "content", autojob.getContent(), 60,
						4)));

				sb.append(new AdminRow("Deadline", "Enter deadline in period after todays date", new TextTag(context, "deadline",
						autojob.getDeadline(), 6)));

				sb.append(new AdminRow("Jobsheet", "Which jobsheet to assign the job to when created", new SelectTag(context, "jobsheet", autojob
						.getJobsheet(), Jobsheet.get(context), "-None-")));

				sb.append(new AdminRow("Item type", "Select the item type this job should be created for", new SelectTag(context, "itemType", autojob
						.getItemType(), ItemType.getSelectionMap(context), "-None-")));

				sb.append(new AdminRow("Follows", "Select a job that when completed, this job will be created", new SelectTag(context, "follows",
						autojob.getFollows(), AutoJob.get(context), "-None-")));

				sb.append("</table>");

				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	/**
	 * Show all jobsheets
	 */
	@Override
	public Object main() throws ServletException {

		final List<AutoJob> autojobs = AutoJob.get(context);

		AdminDoc doc = new AdminDoc(context, user, "Auto jobs", Tab.Extras);
		doc.setMenu(new ExtrasMenu());
		doc.addBody(new Body() {

			private void autojobs() {

				sb.append(new AdminTable("Auto Jobs"));

				sb.append("<tr>");
				sb.append("<th width='60'>Id</th>");
				sb.append("<th>Jobsheet</th>");
				sb.append("<th>Name</th>");
				sb.append("<th width='100'>Delete</th>");
				sb.append("</tr>");

				for (AutoJob autojob : autojobs) {

					sb.append("<tr>");
					sb.append("<td width='60'>" + new LinkTag(AutoJobHandler.class, "edit", autojob.getId(), "autojob", autojob) + "</td>");
					sb.append("<td>" + (autojob.hasJobsheet() ? autojob.getJobsheet().getName() : "None") + "</td>");
					sb.append("<td>" + new LinkTag(AutoJobHandler.class, "edit", autojob.getName(), "autojob", autojob) + "</td>");
					sb.append("<td width='100'>"
							+ new ButtonTag(AutoJobHandler.class, "delete", "Delete", "autojob", autojob)
									.setConfirmation("Are you sure you want to delete this auto job?") + "</td>");
					sb.append("</tr>");
				}

				sb.append(new FormTag(AutoJobHandler.class, "add", "post"));
				sb.append("<tr><td colspan='4'>Add a new auto job: " + new TextTag(context, "name", 20) + "</td></tr>");
				sb.append("</form>");

				sb.append("</table>");

			}

			@Override
			public String toString() {

				autojobs();
				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (autojob == null)
			return main();

		autojob.setName(name);
		autojob.setContent(content);
		autojob.setItemType(itemType);
		autojob.setJobsheet(jobsheet);
		autojob.setDeadline(deadline);
		autojob.setFollows(follows);
		autojob.save();

		return edit();
	}
}