package org.sevensoft.ecreator.iface.admin.marketing.newsletter;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.net.EmailValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterControl;
import org.sevensoft.ecreator.model.marketing.newsletter.Subscriber;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 10 Nov 2006 11:17:45
 *
 */
@Path("admin-newsletter-subs.do")
public class NewsletterSubscriptionHandler extends AdminHandler {

	private String				submit;
	private String				email;
	private transient NewsletterControl	newsletterControl;

	public NewsletterSubscriptionHandler(RequestContext context) {
		super(context);
		this.newsletterControl = NewsletterControl.getInstance(context);
	}

	public Object action() throws ServletException {

		test(new RequiredValidator(), "email");
		test(new EmailValidator(), "email");
		if (hasErrors()) {
			return main();
		}

		String title;
		if ("Add email address".equals(submit)) {

			List<Newsletter> newsletters = Newsletter.get(context);
			Subscriber.get(context, email, true).subscribe(newsletters);

			title = "Email address has been added";

		} else {

			title = "Email address has been removed";
			Subscriber.get(context, email, true).unsubscribe();

		}

		AdminDoc doc = new AdminDoc(context, user, title, Tab.Marketing);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("simplelinks", "center"));
				sb.append("<tr><th align='center'>What would you like to do ?</th></tr>");

				sb.append("<tr><td align='center'>" + new LinkTag(NewsletterComposeHandler.class, null, "I want to send a newsletter now") +
						"</td></tr>");

				sb.append("<tr><td align='center'>" +
						new LinkTag(NewsletterSubscriptionHandler.class, "main", "I want to add or remove another email address") + "</td></tr>");

				sb.append("</table>");

				return sb.toString();
			}
		});
		return doc;

	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Newsletter subscriptions", Tab.Marketing);
		doc.addBody(new Body() {

            private void commands() {
                sb.append("<div align='center' class='actions'>" + new SubmitTag("submit", "Add email address") + " " +
                        new SubmitTag("submit", "Remove email address") + "</div>");
            }

			@Override
			public String toString() {

				sb.append(new FormTag(NewsletterSubscriptionHandler.class, "action", "POST"));

                commands();

				sb.append(new TableTag("simplebox", "center"));

				sb.append("<tr><td align='center'>Enter the email address you want to add or remove.</td></tr>");
				sb.append("<tr><td align='center'>" + new TextTag(context, "email", 40) + new ErrorTag(context, "email", "<br/>") +
						"<br/><br/></td></tr>");

				sb.append("</table>");

				commands();

				sb.append("</form>");
				return sb.toString();
			}

		});
		return doc;
	}

}
