package org.sevensoft.ecreator.iface.admin.interaction.userplane.recorder;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.interaction.userplane.recorder.UserplaneRecorderSettings;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 26 Nov 2006 14:52:34
 *
 */
@Path("admin-userplane-recorder-settings.do")
public class UserplaneRecorderSettingsHandler extends AdminHandler {

	private final transient UserplaneRecorderSettings	settings;
	private int								maxRecordSeconds;
	private boolean							autoApprove;
	private String							domainId;
	private String							flashcomServer;
	private String							noVideoImage;
	private String							recorderPageHeader;
	private String							recorderPageFooter;
	private String							viewerPageHeader;
	private String							viewerPageFooter;
	private String							videoFps;

	public UserplaneRecorderSettingsHandler(RequestContext context) {
		super(context);
		this.settings = UserplaneRecorderSettings.getInstance(context);
	}

	@Override
	public Object main() {

		if (!Module.UserplaneRecorder.enabled(context)) {
			return new DashboardHandler(context).main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Userplane recorder settings", Tab.Extras);
		doc.setMenu(new ExtrasMenu());
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update userplane recorder settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void content() {

				sb.append(new AdminTable("Content"));

				sb.append("<tr><td>Recorder page header</td></tr>");
				sb.append("<tr><td>"
						+ new TextAreaTag(context, "recorderPageHeader", settings.getRecorderPageHeader()).setId("recorderPageHeader").setStyle(
								"width: 100%; height: 320px;") + "</td></tr>");

				sb.append("<tr><td>Recorder page footer</td></tr>");
				sb.append("<tr><td>"
						+ new TextAreaTag(context, "recorderPageFooter", settings.getRecorderPageFooter()).setId("recorderPageFooter").setStyle(
								"width: 100%; height: 320px;") + "</td></tr>");

				sb.append("<tr><td>Viewer page header</td></tr>");
				sb.append("<tr><td>"
						+ new TextAreaTag(context, "viewerPageHeader", settings.getRecorderPageHeader()).setId("viewerPageHeader").setStyle(
								"width: 100%; height: 320px;") + "</td></tr>");

				sb.append("<tr><td>Viewer page footer</td></tr>");
				sb.append("<tr><td>"
						+ new TextAreaTag(context, "viewerPageFooter", settings.getRecorderPageFooter()).setId("viewerPageFooter").setStyle(
								"width: 100%; height: 320px;") + "</td></tr>");

				sb.append("</table>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Flashcom server", new TextTag(context, "flashcomServer", settings.getFlashcomServer(), 40)));

				sb.append(new AdminRow("Domain Id", new TextTag(context, "domainId", settings.getDomainId(), 20)));

				sb.append(new AdminRow("Max record time", "Enter the maximum amount of time a user has to create their message.", new TextTag(context,
						"maxRecordSeconds", settings.getMaxRecordSeconds(), 4)));

				sb.append(new AdminRow("Video FPS", "Video frames per second - increase number to increase quality.", new TextTag(context, "videoFps",
						settings.getVideoFps(), 4)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

                RendererUtil.tinyMce(context, sb, "header", "footer");

				sb.append(new FormTag(UserplaneRecorderSettingsHandler.class, "save", "POST"));

                commands();
				general();
				content();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() {

		settings.setMaxRecordSeconds(maxRecordSeconds);
		settings.setAutoApprove(autoApprove);
		settings.setFlashcomServer(flashcomServer);
		settings.setDomainId(domainId);
		settings.setNoVideoImage(noVideoImage);
		settings.setRecorderPageFooter(recorderPageFooter);
		settings.setRecorderPageHeader(recorderPageHeader);
		settings.setViewerPageFooter(viewerPageFooter);
		settings.setViewerPageHeader(viewerPageHeader);
		settings.setVideoFps(videoFps);
		settings.save();

		clearParameters();
		addMessage("Userplane messenger settings updated");

		return main();
	}
}
