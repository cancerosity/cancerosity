package org.sevensoft.ecreator.iface.admin.items.modules.favourites;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.ecom.shopping.OrderEmailHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.ecreator.model.ecom.orders.emails.OrderEmail;
import org.sevensoft.ecreator.model.ecom.shopping.ShoppingSettings;
import org.sevensoft.ecreator.model.items.favourites.FavouritesSettings;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.misc.agreements.Agreement;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.commons.superstrings.StringHelper;

import javax.servlet.ServletException;
import java.util.List;

/**
 * User: Tanya
 * Date: 23.08.2010
 */
@Path("admin-favourites-settings.do")
@Singleton
public class FavouritesSettingsHandler extends AdminHandler {

    private transient FavouritesSettings favouritesSettings;
    private Markup markup;

    public FavouritesSettingsHandler(RequestContext context) {
        super(context);
        this.favouritesSettings = FavouritesSettings.getInstance(context);
    }

    public Object main() throws ServletException {
        AdminDoc doc = new AdminDoc(context, user, "Shopping settings", null);
        doc.addBody(new Body() {


            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update favourites settings"));
                sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
                sb.append("</div>");
            }

            private void general() {

                sb.append(new AdminTable("General"));

                if (isSuperman() || Module.UserMarkup.enabled(context)) {
                    sb.append(new AdminRow("Markup", new SelectTag(context, "markup", favouritesSettings.getMarkup(), Markup.get(context), "Default")));
                }

                sb.append("</table>");
            }


            @Override
            public String toString() {

                sb.append(new FormTag(FavouritesSettingsHandler.class, "save", "POST"));

                commands();

                general();

                commands();

                sb.append("</form>");

                return sb.toString();
            }


        });
        return doc;
    }

    public Object save() throws ServletException {

        if (favouritesSettings == null)
            return main();

        favouritesSettings.setMarkup(markup);
        favouritesSettings.save();

        clearParameters();
        addMessage("This favourites settings has been updated with your changes");
        return main();
    }
}
