package org.sevensoft.ecreator.iface.admin.ecom.vouchers;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.ecom.ShoppingMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.stats.vouchers.VoucherStats;
import org.sevensoft.ecreator.model.stats.vouchers.VoucherUser;
import org.sevensoft.ecreator.model.ecom.vouchers.Voucher;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Collections;

/**
 * User: Tanya
 * Date: 11.09.2012
 */
@Path("admin-vouchers-stats.do")
public class VoucherStatsHandler extends AdminHandler {

    private Voucher voucher;

    public VoucherStatsHandler(RequestContext context) {
        super(context);
    }

    public Object main() throws ServletException {
        return list();
    }

    private Object list() {
        if (!Module.ListingVouchers.enabled(context)) {
            return HttpServletResponse.SC_NOT_FOUND;
        }
        AdminDoc page = new AdminDoc(context, user, "Vouchers stats", Tab.Orders);
        page.setMenu(new ShoppingMenu());
        page.addBody(new Body() {
            @Override
            public String toString() {
                sb.append(new AdminTable("Vouchers"));

                sb.append("<tr>");
                sb.append("<th width='20'>ID</th>");
                sb.append("<th>Voucher Reference</th>");
                sb.append("<th>Value of Voucher</th>");
                sb.append("<th>Expiry Date</th>");
                sb.append("<th>How many users used/claimed the voucher</th>");
                sb.append("</tr>");
                for (VoucherStats st : VoucherStats.getAll(context)) {
                    Voucher voucher = st.getVoucher();
                    sb.append("<tr>");
                    sb.append("<td width='20'>" + new LinkTag(VoucherHandler.class, "edit", voucher.getId(), "voucher", voucher) + "</td>");
                    sb.append("<td>" + voucher.getCode() + "</td>");
                    sb.append("<td>");
                    if (voucher.getDiscount() != null) {
                        if (voucher.getDiscount().isFixed()) {
                            sb.append(currencySymbol);
                        }
                        sb.append(voucher.getDiscount());
                    } else {
//                        sb.append("Free"); 100%
                    }
                    sb.append("</td>");
                    sb.append("<td>");
                    if (voucher.hasEndDate()) {
                        sb.append(voucher.getEndDate().toEditString());
                    }
                    sb.append("</td>");
                    sb.append("<td>" + new LinkTag(VoucherStatsHandler.class, "view", st.getUsersCount() + " users claimed", "voucher", voucher) + "</td>");
                    sb.append("</tr>");
                }

                sb.append("</table>");
                return sb.toString();
            }
        });
        return page;
    }

    public Object view() {
        if (!Module.ListingVouchers.enabled(context)) {
            return HttpServletResponse.SC_NOT_FOUND;
        }
        if (voucher == null) {
            return list();
        }

        final List<VoucherUser> users = VoucherUser.getVoucherUsers(context, voucher);
        Collections.sort(users);
        AdminDoc page = new AdminDoc(context, user, "Users used/claimed the voucher", Tab.Orders);
        page.setMenu(new ShoppingMenu());
        page.addBody(new Body() {
            @Override
            public String toString() {

                commands();
                general();
                commands();

                return sb.toString();
            }

            private void commands() {
                sb.append("<div align='center' class='actions'>");
                sb.append(new ButtonTag(VoucherStatsHandler.class, null, "Return"));
                sb.append("</div>");
            }

            private void general() {
                sb.append(new AdminTable("Users"));

                sb.append("<tr>");
                sb.append("<th width='20'>ID</th>");
                sb.append("<th>Last name</th>");
                sb.append("<th>First Name</th>");
                sb.append("<th>Package Chosen</th>");
                sb.append("</tr>");
                for (VoucherUser voucherUser : users) {
                    Item user = voucherUser.getAccount();
                    sb.append("<tr>");
                    sb.append("<td width='20'>" + new LinkTag(ItemHandler.class, "edit", user.getId(), "item", user) + "</td>");
                    sb.append("<td>" + user.getLastName() + "</td>");
                    sb.append("<td>" + user.getFirstName() + "</td>");
                    sb.append("<td>" + voucherUser.getListingPackage() + "</td>");
                    sb.append("</tr>");
                }

                sb.append("</table>");
            }
        });
        return page;
    }
}
