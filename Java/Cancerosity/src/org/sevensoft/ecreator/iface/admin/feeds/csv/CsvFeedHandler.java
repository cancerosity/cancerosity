package org.sevensoft.ecreator.iface.admin.feeds.csv;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.misc.html.EditItemTypeButton;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.ecreator.model.ecom.suppliers.Supplier;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.csv.CsvImportFeed;
import org.sevensoft.ecreator.model.feeds.csv.CsvImportFeedField;
import org.sevensoft.ecreator.model.feeds.csv.FieldType;
import org.sevensoft.ecreator.model.feeds.csv.CsvImportFeed.Type;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 3 Aug 2006 10:28:13
 *
 */
@Path("admin-feeds-csv.do")
public class CsvFeedHandler extends FeedHandler {

	private CsvImportFeed			feed;
	private String				escape;
	private int					skuField;
	private String				quote;
	private String				separator;
	private int					startRow;
	private FieldType				fieldType;
	private int					refField;
	private int					emailField;
	private boolean				create;
	private ItemType				itemType;
	private Supplier				supplier;
	private Type				type;
	private boolean				passwordEmail;
	private String				passwordEmailContent;
	private Money				minSellPrice;
	private boolean				deleteExpired;
	private transient OrderSettings	orderSettings;
	private boolean				removeOmittedLines;
	private String				orderStatus;
	private int					idField;
    private int		            importYnField;

	public CsvFeedHandler(RequestContext context) {
		this(context, null);
	}

	public CsvFeedHandler(RequestContext context, CsvImportFeed feed) {
		super(context);
		this.feed = feed;
		this.orderSettings = OrderSettings.getInstance(context);
	}

	private void fields(StringBuilder sb) {

		sb.append(new AdminTable("Fields"));

		sb.append("<tr>");
		//		sb.append("<td width='80'>Position</td>");
		sb.append("<th>Type</th>");
		sb.append("<th>Columns</th>");
		sb.append("<th width='120'>Lookup</th>");
		sb.append("<th width='10'> </th>");
		sb.append("</tr>");

		//		PositionRender pr = new PositionRender(CsvFeedFieldHandler.class, "field");
		for (CsvImportFeedField field : feed.getFields()) {

			sb.append("<tr>");
			//			sb.append("<td>" + pr.render(field) + "</td>");
			sb.append("<td>" + new LinkTag(CsvFeedFieldHandler.class, null, new SpannerGif(), "field", field) + " " + field + "</td>");
            sb.append("<td>" + StringHelper.implode(field.getColumns(), ",", true).replaceAll(CsvImportFeedField.WHITESPACE_SEPARATOR + ",", " ")
                    + " (" + (field.isCombine() ? "Combined" : "Separate") + ")</td>");
            sb.append("<td width='120'>");

			if (field.isIndividualLookup()) {
				sb.append("Individual");

			} else if (field.isCombinationLookup()) {
				sb.append("Combination");
			}

			sb.append("</td>");

			sb.append("<td width='10'>" + new LinkTag(CsvFeedFieldHandler.class, "delete", new DeleteGif(), "field", field).setConfirmation("Are you sure you want to remove this field?") + "</td>");
			sb.append("</tr>");
		}

		SelectTag tag = new SelectTag(context, "fieldType");
		tag.setAny("-Add new field-");
		tag.addOptions(FieldType.get());

		sb.append("<tr>");
		sb.append("<td colspan='5'>Add new field " + tag + " " + new SubmitTag("Add") + "</td>");
		sb.append("</tr>");

		sb.append("</table>");
	}

	protected ImportFeed getFeed() {
		return feed;
	}

	@Override
	protected void saveSpecific() {

		feed.setType(type);
        feed.setImportYnField(importYnField);
		feed.setSkuField(skuField);
		feed.setStartRow(startRow);
		feed.setSeparator(separator);
		feed.setEscape(escape);
		feed.setQuote(quote);
		feed.setRefField(refField);
		feed.setCreate(create);
		feed.setItemType(itemType);
		feed.setMinCostPrice(minSellPrice);
		feed.setSupplier(supplier);
		feed.setPasswordEmail(passwordEmail);
		feed.setDeleteExpired(deleteExpired);
		feed.setOrderStatus(orderStatus);
		feed.setRemoveOmittedLines(removeOmittedLines);
		feed.setIdField(idField);

		if (fieldType != null) {
			feed.addField(fieldType);
		}
	}

	protected void specifics(StringBuilder sb) {

		fields(sb);

		sb.append(new AdminTable("CSV import details"));

		SelectTag typeTag = new SelectTag(context, "type", feed.getType(), CsvImportFeed.Type.values(), "-Select type-");
		sb.append(new AdminRow("Type", "Set the type of feed", typeTag));

		switch (feed.getType()) {

		case OrderLines:

			sb.append(new AdminRow("Remove omitted lines", "Delete from any referenced orders any lines not included in the feed.", new BooleanRadioTag(
					context, "removeOmittedLines", feed.isRemoveOmittedLines())));

			sb.append(new AdminRow("Order status", "Change the status of any referenced orders to this value.", new SelectTag(context, "orderStatus",
					feed.getOrderStatus(), orderSettings.getStatuses(), "-No status change-")));

			sb.append(new AdminRow("Line Id field", "Enter the column number that supplies the line ID.", new TextTag(context, "idField", feed
					.getIdField(), 4)));
			break;

		case Orders:
			break;

		case Items:

			sb.append(new AdminRow("Item Type", "Set the item type to import to", new SelectTag(context, "itemType", feed.getItemType(), ItemType
					.getSelectionMap(context), "-None set-").setId("itemType")
					+ "" + new EditItemTypeButton("itemType")));

			sb.append(new AdminRow("Import Yes/No", "Enter the field number that should defime import an Item or not", new TextTag(context, "importYnField", feed
					.getImportYnField(), 4)));

            sb.append(new AdminRow("Supplier", "Set the supplier for price / stock fields", new SelectTag(context, "supplier", feed.getSupplier(false),
					Supplier.get(context), "-Create new if required-")));

			sb.append(new AdminRow("Ref field", "Enter the field number that should feed in as your reference.", new TextTag(context, "refField", feed
					.getRefField(), 4)));

			sb.append(new AdminRow("Sku field", "Enter the field number that is the suppliers lookup sku number.", new TextTag(context, "skuField", feed
					.getSkuField(), 4)));

			sb.append(new AdminRow("Min sell price", "Any products below this price will be ignored in the feed.", new TextTag(context, "minSellPrice",
					feed.getMinCostPrice(), 12)));

			sb.append(new AdminRow("Delete expired", "Any items created by this feed which are not updated in a particular run will be deleted.",
					new BooleanRadioTag(context, "deleteExpired", feed.isDeleteExpired())));

			break;

		}

		sb.append(new AdminRow("Create", "If set to yes then records that cannot be located will be created.", new BooleanRadioTag(context, "create", feed
				.isCreate())));

		sb
				.append(new AdminRow("Start row", "Enter the row to start on. The feed starts at row 0", new TextTag(context, "startRow", feed
						.getStartRow(), 4)));

		sb.append(new AdminRow("CSV encoding", "Set the encoding values for the csv file format", "Separator "
				+ new TextTag(context, "separator", feed.getSeparator(), 3) + " Quote " + new TextTag(context, "quote", feed.getQuote(), 3)
				+ " Escape " + new TextTag(context, "escape", feed.getEscape(), 3)));

		sb.append("</table>");

	}
}
