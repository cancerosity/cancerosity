package org.sevensoft.ecreator.iface.admin.extras.polls;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.extras.polls.PollSettings;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;

/**
 * @author sks 17-Jun-2005 17:37:55
 * 
 */
@Path("admin-poll-settings.do")
public class PollSettingsHandler extends AdminHandler {

	private PollSettings			settings		= PollSettings.getInstance(context);
	private String				resultsContent;

	private transient MiscSettings	miscSettings	= MiscSettings.getInstance(context);

	public PollSettingsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() {

		if (!Module.Polls.enabled(context))
			return new DashboardHandler(context).main();

		AdminDoc page = new AdminDoc(context, user, "Poll settings", Tab.Extras);
		page.setMenu(new ExtrasMenu());
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update poll settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append("<tr><th width='300'><b>Poll results content</b><br/>This content will appear on your poll results page.</th><td>"
						+ new TextAreaTag(context, "resultsContent", settings.getResultsContent(), 60, 6) + "</td></tr>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				if (miscSettings.isHtmlEditor())
                    RendererUtil.tinyMce(context, sb, "resultsContent");

				sb.append(new FormTag(PollSettingsHandler.class, "save", "POST"));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return page;
	}

	public Object save() {

		settings.setResultsContent(resultsContent);
		clearParameters();

		addMessage("Poll settings updated");
		clearParameters();
		return main();
	}

}
