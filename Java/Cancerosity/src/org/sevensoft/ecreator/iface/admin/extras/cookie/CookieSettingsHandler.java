package org.sevensoft.ecreator.iface.admin.extras.cookie;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.model.extras.cookie.CookieSettings;
import org.sevensoft.ecreator.model.system.scripts.Script;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;

import javax.servlet.ServletException;

/**
 * User: Tanya
 * Date: 01.06.2012
 */
@Path("admin-settings-cookies.do")
public class CookieSettingsHandler extends AdminHandler {

    private transient CookieSettings cookieSettings;
    private transient Script scriptSettings;

    private boolean loadCookieControl;
    private boolean loadCookieControlWithRevoke;
    private String generateCodeUrl;
    private String cookieControlCode;

    public CookieSettingsHandler(RequestContext context) {
        super(context);
        this.cookieSettings = CookieSettings.getInstance(context);
        this.scriptSettings = Script.getInstance(context);
    }

    public Object main() throws ServletException {
        if (!Module.CookieControl.enabled(context)) {
            return index();
        }

        AdminDoc page = new AdminDoc(context, user, "Cookie settings", null);
        page.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update settings"));
                sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
                sb.append("</div>");
            }


            private void general() {

                sb.append(new AdminTable("General"));

                sb.append(new AdminRow(true, "Load Cookie Control", "Choose one of Cookie Controls",
                        new BooleanRadioTag(context, "loadCookieControl", scriptSettings.isLoadCookieControl())));
                sb.append(new AdminRow(true, "Load Cookie Control with Revoke", "Choose one of Cookie Controls",
                        new BooleanRadioTag(context, "loadCookieControlWithRevoke", scriptSettings.isLoadCookieControlWithRevoke())));

                sb.append(new AdminRow("Generate code Url", "Cookie Control Url where it can be customised",
                        new TextTag(context, "generateCodeUrl", cookieSettings.getGenerateCodeUrl(), 80)));
                sb.append(new AdminRow("Code", "Customised Cookie Control Code",
                        new TextAreaTag(context, "cookieControlCode", cookieSettings.getCookieControlCode(), 80, 5)));

                sb.append("</table>");

            }

            @Override
            public String toString() {

                sb.append(new FormTag(CookieSettingsHandler.class, "save", "POST"));

                commands();
                general();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return page;
    }

    public Object save() throws ServletException {
        if (!Module.CookieControl.enabled(context)) {
            return index();
        }
        cookieSettings.setGenerateCodeUrl(generateCodeUrl);
        cookieSettings.setCookieControlCode(cookieControlCode);
        cookieSettings.save();

        scriptSettings.setLoadCookieControl(loadCookieControl);
        scriptSettings.setLoadCookieControlWithRevoke(loadCookieControlWithRevoke);
        scriptSettings.save();

        addMessage("Cookie control settings updated");
        clearParameters();
        return main();
    }


}
