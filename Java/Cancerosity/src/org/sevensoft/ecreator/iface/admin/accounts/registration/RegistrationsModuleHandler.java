package org.sevensoft.ecreator.iface.admin.accounts.registration;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.iface.admin.misc.agreements.AgreementsHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.accounts.registration.RegistrationModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.misc.agreements.Agreement;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sam
 */
@Path("admin-accounts-registrations-module.do")
public class RegistrationsModuleHandler extends AdminHandler {

	/**
	 * 
	 */
	private boolean	registrations;

	/**
	 * 
	 */
	private boolean	registrationEmail;

	/**
	 * 
	 */
	private String	registrationEmailContent;

	/**
	 * A list of 'where did you hear about us' referrers
	 */
	private String	referrers;
	private String	registrationGroupContent;
	private String	registrationEmailCcs;
	private ItemType	itemType;
	private String	registrationForwardUrl;
	private String	registrationContent;
	private Agreement	agreement;
	/**
	 * 
	 */
	private boolean	hidden;
	private String	registrationExportEmails;
	private boolean	exportOnRegistration;
	private boolean	registrationImages;
	private int		maxImages;
	private int		minImages;
	private String	loginSectionTitle;
	private String	loginSectionIntro;
	private boolean	moderation;
	private String	emailDescription;
	private String	nameDescription;

	private String	moderationRejectionEmail;

	private String	moderationApprovalEmail;

    private String  registrationWelcome;

    private String  registrationInformationContent;

    public RegistrationsModuleHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (itemType == null) {
			return index();
		}

		final RegistrationModule module = itemType.getRegistrationModule();

		AdminDoc page = new AdminDoc(context, user, "Registration module", null);
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update registration module"));
				sb.append(new ButtonTag(ItemTypeHandler.class, "edit", "Return to " + itemType.getName().toLowerCase(), "itemType", itemType));
				sb.append("</div>");
			}

			private void fields() {

				sb.append(new AdminTable("Fields"));

				sb.append(new AdminRow("Name description", "Description shown under text field as part of the registration process.", new TextAreaTag(
						context, "nameDescription", module.getNameDescription(), 40, 3)));

				sb.append(new AdminRow("Name description", "Description shown under text field as part of the registration process.", new TextAreaTag(
						context, "emailDescription", module.getEmailDescription(), 40, 3)));

				sb.append("</table>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Registration forward url",
						"Send users who have just registerd to this url, rather than the standard thank you screen.", new TextTag(context,
								"registrationForwardUrl", module.getRegistrationForwardUrl(), 50)));

				sb.append(new AdminRow("Referrers", "If you want users to select where they heard about you when signing up, enter the options here.",
						new TextAreaTag(context, "referrers", StringHelper.implode(module.getReferrers(), "\n"), 40, 5)));

				sb.append(new AdminRow("Registration email", "Sends an email to the user when they register an account..", new BooleanRadioTag(context,
						"registrationEmail", module.isRegistrationEmail())));

				sb.append(new AdminRow("Registration third party email", "Send an email to third party addresses when an account is registered.",
						new TextAreaTag(context, "registrationEmailCcs", StringHelper.implode(module.getRegistrationEmailCcs(), "\n"), 50, 3)));

				if (module.isRegistrationEmail() || module.hasRegistrationEmailCcs()) {

					sb.append(new AdminRow("Registration email content",
							"The content of the email that is sent on registration.<br/><br/>Special tags:<br/>"
									+ "<i>[name]</i> - Members name<br/>" + "<i>[email]</i> - Members email<br/>"
									+ "<i>[id]</i> - Generated id<br/>" + "<i>[login]</i> - Url to login page<br/>"
									+ "<i>[loginurl]</i> - Login url<br/>" + "<i>[site]</i> - Your site name<br/>",

							new TextAreaTag(context, "registrationEmailContent", module.getRegistrationEmailContent(), 80, 10)));

				}

				sb.append(new AdminRow("Agreement", "Set an agreement that the user must accept before they can complete registration.", new SelectTag(
						context, "agreement", module.getAgreement(), Agreement.get(context), "-Select agreement-") +
						" " + new LinkTag(AgreementsHandler.class, null, "Edit agreements").setTarget("_blank")));

				sb.append(new AdminRow("Hidden", "Hide this registration group so it is not shown in the list.", new BooleanRadioTag(context, "hidden",
						module.isHidden())));

				sb.append("</table>");
			}

			private void images() {

				sb.append(new AdminTable("Images"));

				sb.append(new AdminRow("Show images on registration", "Ask users to upload image as part of the registration process.",
						new BooleanRadioTag(context, "registrationImages", module.isRegistrationImages())));

				sb.append(new AdminRow("Min images", new TextTag(context, "minImages", module.getMinImages(), 4)));

				sb.append(new AdminRow("Max images", new TextTag(context, "maxImages", module.getMaxImages(), 4)));

				sb.append("</table>");
			}

            private void registrationPages() {

                sb.append(new AdminTable("Registration"));

                sb.append(new AdminRow("Welcome", "This text is a brief introduction to the first registration page",
                        new TextAreaTag(context, "registrationWelcome", module.getRegistrationWelcome(), 70, 3)));

                sb.append(new AdminRow("Information Content", "This is an information content for the registration firat page",
                        new TextAreaTag(context, "registrationInformationContent", module.getRegistrationInformationContent(), 70, 3)));

                sb.append("</table>");

            }

            private void loginSection() {

				sb.append(new AdminTable("Login Section"));

				sb.append(new AdminRow("Title", "This is the title of the section containing login details, and default attributes.", new TextTag(
						context, "loginSectionTitle", module.getLoginSectionTitle(), 40)));

				sb.append(new AdminRow("Intro", "This text appears at the top of this section.", new TextAreaTag(context, "loginSectionIntro", module
						.getLoginSectionIntro(), 50, 3)));

				sb.append("</table>");
			}

			private void moderation() {

				sb.append(new AdminTable("Moderation"));

				sb.append(new AdminRow("Moderation",
						"Set this to yes and all new registrations will need to be approved by you before they are active.", new BooleanRadioTag(
								context, "moderation", module.isModeration())));

				if (module.isModeration()) {

					sb.append(new AdminRow("Registration approval email",
							"The text here will be used as the email body when the approval email is sent.", new TextAreaTag(context,
									"moderationApprovalEmail", module.getModerationApprovalEmail(), 50, 4)));

					sb.append(new AdminRow("Moderation Rejection Email",
							"The text here will be used as the email body when the rejection email is sent.", new TextAreaTag(context,
									"moderationRejectionEmail", module.getModerationRejectionEmail(), 50, 4)));

				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(RegistrationsModuleHandler.class, "save", "POST"));
				sb.append(new HiddenTag("itemType", itemType));

                commands();
				general();
				moderation();
                registrationPages();

				images();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return page;
	}

	public Object save() throws ServletException {

		if (itemType == null) {
			return index();
		}

		RegistrationModule module = itemType.getRegistrationModule();

		// moderation
		module.setModeration(moderation);
		module.setModerationApprovalEmail(moderationApprovalEmail);
		module.setModerationRejectionEmail(moderationRejectionEmail);

		// fields
		module.setEmailDescription(emailDescription);
		module.setNameDescription(nameDescription);

		module.setRegistrationEmail(registrationEmail);
		module.setRegistrationEmailContent(registrationEmailContent);
		module.setRegistrationEmailCcs(StringHelper.explodeStrings(registrationEmailCcs, "\n"));
		module.setReferrers(StringHelper.explodeStrings(referrers, "\n"));
		module.setAgreement(agreement);
		module.setRegistrationContent(registrationContent);
		module.setRegistrationForwardUrl(registrationForwardUrl);
		module.setHidden(hidden);

		// images
		module.setRegistrationImages(registrationImages);
		module.setMinImages(minImages);
		module.setMaxImages(maxImages);

		// login section
		module.setLoginSectionIntro(loginSectionIntro);
		module.setLoginSectionTitle(loginSectionTitle);

        //registration pages
        module.setRegistrationWelcome(registrationWelcome);
        module.setRegistrationInformationContent(registrationInformationContent);

        module.save();

		addMessage("Registration module updated");
		clearParameters();
		return main();
	}
}
