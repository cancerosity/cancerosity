package org.sevensoft.ecreator.iface.admin.items.bots;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.samdate.Period;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.bots.ExpiryBot;
import org.sevensoft.ecreator.model.misc.Interval;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 7 Sep 2006 15:45:13
 *
 */
@Path("admin-expiry-bots.do")
public class ExpiryBotHandler extends AdminHandler {

	private ItemType	itemType;
	private ExpiryBot	expiryBot;
	private Attribute	attribute;
	private Period	period;
	private Interval	interval;

	public ExpiryBotHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		if (itemType == null) {
			return main();
		}

		new ExpiryBot(context, itemType);
		return main();
	}

	public Object delete() throws ServletException {

		if (expiryBot != null) {
			itemType = expiryBot.getItemType();
			expiryBot.delete();
		}

		return main();
	}

	public Object edit() throws ServletException {

		if (expiryBot == null) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit expiry bot", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Confirm changes"));
				sb.append(new ButtonTag(ExpiryBotHandler.class, null, "Return to menu"));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Attribute", "The attribute to check date against.", new SelectTag(context, "attribute", expiryBot
						.getAttribute(), expiryBot.getAttributes(), "-None selected-")));

				sb.append(new AdminRow("Interval", "How often to run this expiry bot.", new SelectTag(context, "interval", expiryBot.getInterval(),
						Interval.values())));

				sb
						.append(new AdminRow(
								"Period",
								"How much time needs to have passed after the attribute date before this bot will remove an item.<br/><i>Use 1w for 1week, 2d for 2days, etc.</i>",
								new TextTag(context, "period", expiryBot.getPeriod(), 4)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(ExpiryBotHandler.class, "save", "POST"));
				sb.append(new HiddenTag("expiryBot", expiryBot));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		final List<ExpiryBot> bots = ExpiryBot.get(context);

		AdminDoc doc = new AdminDoc(context, user, "Expiry bots", null);
		doc.addBody(new Body() {

			private void bots() {

				sb.append(new AdminTable("Expiry bots"));

				sb.append("<tr>");
				sb.append("<th>Type</th>");
				sb.append("<th>Attribute</th>");
				sb.append("<th>Period</th>");
				sb.append("<th width='60'>Remove</th>");
				sb.append("<th width='60'>Run</th>");
				sb.append("</tr>");

				for (ExpiryBot expiryBot : bots) {

					sb.append("<tr>");
					sb.append("<td>" + new LinkTag(ExpiryBotHandler.class, "edit", new SpannerGif(), "expiryBot", expiryBot) + " "
							+ expiryBot.getItemType().getName() + "</td>");
					sb.append("<td>" + (expiryBot.hasAttribute() ? expiryBot.getAttribute().getName() : "None selected") + "</td>");
					sb.append("<td>" + (expiryBot.hasPeriod() ? expiryBot.getPeriod() : "-") + "</td>");
					sb.append("<td width='60'>"
							+ new ButtonTag(ExpiryBotHandler.class, "delete", "Delete", "expiryBot", expiryBot)
									.setConfirmation("Are you sure you want to delete this bot?") + "</td>");
					sb.append("<td width='60'>"
							+ new ButtonTag(ExpiryBotHandler.class, "run", "Run", "expiryBot", expiryBot)
									.setConfirmation("Confirm you want to run this bot") + "</td>");
					sb.append("</tr>");
				}

				sb.append(new FormTag(ExpiryBotHandler.class, "create", "post"));
				sb.append("<tr><td colspan='5'>Add expiry bot " + new SelectTag(context, "itemType", null, ItemType.get(context)) + " "
						+ new SubmitTag("Add") + "</td></tr>");
				sb.append("</form>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				bots();

				return sb.toString();
			}

		});
		return doc;
	}

	public Object run() throws ServletException {

		if (expiryBot == null) {
			return main();
		}

		int n = expiryBot.run();
		addMessage("Expriy bot has ran: " + n + " items deleted");
		return main();
	}

	public Object save() throws ServletException {

		if (expiryBot == null) {
			return index();
		}

		expiryBot.setAttribute(attribute);
        if (period == null || period.isCorrect()) {
            expiryBot.setPeriod(period);
        } else {
            addError("Incorrect period");
            return edit();
        }
		expiryBot.setInterval(interval);
		expiryBot.save();

		clearParameters();
		addMessage("This expiry bot has been updated with your changes");
		return main();
	}
}
