package org.sevensoft.ecreator.iface.admin.feeds.csv;

import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.model.feeds.csv.CsvExportFeed;
import org.sevensoft.ecreator.model.feeds.csv.CsvImportFeedField;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.results.StreamResult;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.commons.superstrings.HtmlHelper;

import java.util.Iterator;
import java.util.List;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;

import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.CsvSpec;
import com.ricebridge.csvman.CsvSaver;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
@Path("admin-feeds-csv-export.do")
public class CsvExportFeedHandler extends FeedHandler {

    private CsvExportFeed feed;
    private String separator;
    private String quote;
    private String escape;
    private String endLine;
    private ItemType itemType;
    private CsvExportFeed.Type type;
    private int previewCount;

    public CsvExportFeedHandler(RequestContext context) {
        super(context);
    }

    protected final CsvExportFeed getFeed() {
        return feed;
    }

    public String getSeparator() {
        return separator;
    }

    public String getEndLine() {
        return endLine;
    }

    protected void saveSpecific() {
        feed.setType(type);
        feed.setItemType(itemType);
        feed.setSeparator(separator);
        feed.setQuote(quote);
        feed.setEscape(escape);
        feed.setEndLine(endLine);
    }

    private void fields(StringBuilder sb) {
        sb.append(new AdminTable("Fields"));
        sb.append("<tr>");
        sb.append("<th>Position</th>");
        sb.append("<th>Field type</th>");
        sb.append("<th width='60'>Delete</th>");
        sb.append("</tr>");
        for (Iterator i = feed.getFields().iterator(); i.hasNext(); sb.append("</tr>")) {
            CsvImportFeedField field = (CsvImportFeedField) i.next();
            sb.append("<tr>");
            sb.append((new StringBuilder()).append("<td>").append(new LinkTag(CsvExportFeedFieldHandler.class, null, new SpannerGif(), "field", field)).append(" ").append(field.getPosition()).append("</td>").toString());
            sb.append((new StringBuilder()).append("<td>").append(field).append("</td>").toString());
            sb.append((new StringBuilder()).append("<td width='60'>").append((new LinkTag(CsvExportFeedFieldHandler.class, "delete", new DeleteGif(), "field", field)).setConfirmation("Are you sure you want to delete this field?")).append("</td>").toString());
        }

        sb.append((new StringBuilder()).append("<tr><td colspan='3'>Add field ").append(new ButtonTag(CsvExportFeedFieldHandler.class, "create", "Add", "exFeed", feed)).append("</td></tr>").toString());
        sb.append("</table>");
    }

    private void general(StringBuilder sb) {
        sb.append(new AdminTable("General"));
        SelectTag typeTag = new SelectTag(context, "type", feed.getType(), CsvExportFeed.Type.values(), "-Select type-");
        sb.append(new AdminRow("Type", "Set the type of feed", typeTag));
        sb.append(new AdminRow("Item Type", "Select the item type that will be exported by this exporter", new SelectTag(context,
                "itemType", feed.getItemType(), ItemType.getSelectionMap(context), "None set")));
        sb.append(new AdminRow("End line", "The character used to mark the end of a record.", new TextTag(context, "endLine", feed.getEndLine(), 8)));
        sb.append(new AdminRow("CSV encoding", "Set the encoding values for the csv file format", "Separator "
                + new TextTag(context, "separator", feed.getSeparator(), 3) + " Quote " + new TextTag(context, "quote", feed.getQuote(), 3)
                + " Escape " + new TextTag(context, "escape", feed.getEscape(), 3)));
        sb.append("</table>");
    }

    protected void specifics(StringBuilder sb) {
        fields(sb);
        general(sb);
    }

    public Object run() {
        CsvManager csvman = feed.getCsvManager();
        CsvSpec spec = csvman.getCsvSpec();
        logger.fine("[CsvExportFeedHandler] Using separator=" + feed.getSeparator());
        spec.setSeparator(feed.getSeparator());
        logger.fine("[CsvExportFeedHandler]using endLine=" + feed.getEndLine());
        String endLine = feed.getEndLine();
        if (endLine != null)
            spec.setEndOfLine(endLine.replace("\\n", "\n"));
        csvman.setCsvSpec(spec);
        File file = null;
        try {
            file = File.createTempFile("itemexport", ".csv");
            file.deleteOnExit();
        } catch (IOException e) {
            logger.fine("[CsvExportFeedHandler] can not create temp file " + e.toString());
        }
        logger.fine("[CsvExportFeedHandler] Temp file created=" + file);
        ItemSearcher searcher = new ItemSearcher(context);
        searcher.setVisibleOnly(false);
        searcher.setIncludeHidden(true);
        searcher.setStatus("LIVE");
        searcher.setLimit(previewCount);
        logger.fine("[CsvExportFeedHandler] Using item type=" + feed.getItemType());
        searcher.setItemType(feed.getItemType());
        CsvSaver saver = csvman.makeSaver(file);
        saver.begin();
        String row[];
        List<CsvImportFeedField> fields = feed.getFields();
        row = makeCapRow(fields);
        saver.next(row);
        for (Iterator i = searcher.iterator(); i.hasNext(); saver.next(row)) {
            Item item = (Item) i.next();
            if (item.getOptionSet().getOptions().size() != 0) {
                for (ItemOption itenOption : item.getOptionSet().getOptions()) {
                    for (ItemOptionSelection selection : itenOption.getSelections()) {
                        row = makeRow(fields, item, selection);
                        HtmlHelper.replaseSymbolsWithUTF(row);
                    }
                }
            } else {
                logger.fine("[CsvExportFeedHandler] logging item=" + item);
                row = makeRow(fields, item);
                HtmlHelper.replaseSymbolsWithUTF(row);
            }
        }

        saver.end();

        try {
            return new StreamResult(file, "text/csv", (new StringBuilder()).append(feed.getItemType().getName()).append(".csv").toString(), StreamResult.Type.Attachment);
        } catch (FileNotFoundException e) {
            logger.fine("[CsvExportFeedHandler] can not create result CSV file " + e.toString());
        }

        return null;
    }

    private String[] makeRow(List<CsvImportFeedField> fields, Item item, ItemOptionSelection selection) {
        String row[] = new String[fields.size()];
        int n = 0;
        for (CsvImportFeedField field : fields) {
            row[n] = field.export(item, selection);
            n++;
        }
        return row;
    }

    private String[] makeRow(List<CsvImportFeedField> fields, Item item) {
        String row[] = new String[fields.size()];
        int n = 0;
        for (CsvImportFeedField field : fields) {
            row[n] = field.export(item);
            n++;
        }

        return row;
    }

    private String[] makeCapRow(List<CsvImportFeedField> fields) {
        String row[] = new String[fields.size()];
        int n = 0;
        for (CsvImportFeedField field : fields) {
            row[n] = field.toString();
            n++;
        }

        return row;
    }
}
