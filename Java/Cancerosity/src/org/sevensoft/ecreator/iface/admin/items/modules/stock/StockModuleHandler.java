package org.sevensoft.ecreator.iface.admin.items.modules.stock;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemTypeMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.stock.StockModule;
import org.sevensoft.ecreator.model.items.stock.StockModule.StockCalculationSystem;
import org.sevensoft.ecreator.model.items.stock.StockModule.StockControl;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 28 Dec 2006 12:32:30
 *
 */
@Path("admin-stock-module.do")
public class StockModuleHandler extends AdminHandler {

	private ItemType				itemType;
	private StockControl			stockControl;
	private String				inStockIcon;
	private String				outStockIcon;
	private String				stockDisplayRangesString;
	private String				outStockMsgDefault;
	private boolean				resetStock;
	private String				resetLeadtime;
	private boolean				backorders;
	private StockCalculationSystem	stockSystem;
	private boolean				defaultBackorders;
	private String				outStockMsgOverride;
	private boolean				resetBackorders;
	private String				inStockMsgOverride;
	private String				inStockMsgDefault;
	private int					stock;
	private boolean				buyQtyRestriction;

	public StockModuleHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (itemType == null) {
			return index();
		}

		final StockModule module = itemType.getStockModule();

		AdminDoc doc = new AdminDoc(context, user, "Edit item type: " + itemType.getName(), null);
		doc.setMenu(new EditItemTypeMenu(itemType));
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update item type"));
				sb.append(new ButtonTag(ItemTypeHandler.class, null, "Return to item types menu"));

				sb.append("</div>");
			}

			private void qtyBuyRestriction() {

				sb.append(new AdminRow("Buy qty restriction", "Restrict purchasing to quantity in stock.", new BooleanRadioTag(context,
						"buyQtyRestriction", module.isBuyQtyRestriction())));
			}

			private void stock() {

				sb.append(new AdminTable("Stock and Availability"));

				SelectTag stockControlTag = new SelectTag(context, "stockControl", module.getStockControl());
				stockControlTag.addOptions(StockControl.values());

				sb.append(new AdminRow("Stock control", "Choose from the available stock control methods.", stockControlTag));

				if (module.isStockControl()) {

					sb.append(new AdminRow("Default out of stock message", "This default message will be used by newly created items.", new TextTag(
							context, "outStockMsgDefault", module.getOutStockMsgDefault(), 40)));

					sb.append(new AdminRow("Out of stock message override",
							"Set a message which will override the out of stock message for all items of this type.", new TextTag(context,
									"outStockMsgOverride", module.getOutStockMsgOverride(), 40)));

					if (module.getStockControl() != StockControl.String) {

						sb.append(new AdminRow("Default in stock message", "This default message will be used by newly created items.",
								new TextTag(context, "inStockMsgDefault", module.getInStockMsgDefault(), 40)));

						sb.append(new AdminRow("In stock message override",
								"Set a message which will override the in stock message for all items of this type.", new TextTag(context,
										"inStockMsgOverride", module.getInStockMsgOverride(), 40)));

						switch (module.getStockControl()) {

						case Boolean:
						case Manual:
						case RealTime:

							String icon = " ";
							if (module.hasInStockIcon())
								icon = icon + new ImageTag(module.getInStockIconPath()).toString();

							sb.append(new AdminRow("In stock icon", "This icon will be displayed instead of a stock status if set.", new TextTag(
									context, "inStockIcon", module.getInStockIcon(), 20)
									+ icon));

							icon = " ";
							if (module.hasOutStockIcon())
								icon = icon + new ImageTag(module.getOutStockIconPath()).toString();

							sb.append(new AdminRow("Out stock icon", "This icon will be displayed instead of stock status if set.", new TextTag(
									context, "outStockIcon", module.getOutStockIcon(), 20)
									+ icon));

							sb.append(new AdminRow("Default backorders", "Set default for backorders.", new BooleanRadioTag(context,
									"defaultBackorders", module.isDefaultBackorders())));

							break;

						case None:
						case String:
							break;

						}

						switch (module.getStockControl()) {

						case RealTime:

							stockDisplayRanges();

							if (Module.Suppliers.enabled(context)) {
								stockSystem();
							}

							qtyBuyRestriction();

							break;

						case Manual:

							stockDisplayRanges();
							if (Module.Suppliers.enabled(context)) {
								stockSystem();
							}
							qtyBuyRestriction();

							break;

						case Boolean:
						case None:
						case String:
							break;

						}

					}
				}

				if (isSuperman()) {
					sb.append(new AdminRow("Reset stock", "Reset the stock level on all items.", new CheckTag(context, "resetStock", true, false)
							+ " " + new TextTag(context, "stock", 4)));
				}

				if (module.isStockControl()) {

					if (isSuperman()) {

						sb.append(new AdminRow("Reset leadtime", "Reset the leadtime on all items to this value.", new TextTag(context,
								"resetLeadtime", 30)));

						sb.append(new AdminRow("Reset backorders", "Reset the backorder value on all items to this value.", new CheckTag(null,
								"resetBackorders", true, false)
								+ "" + new BooleanRadioTag(context, "backorders", false)));
					}
				}

				sb.append("</table>");
			}

			private void stockDisplayRanges() {

				sb.append(new AdminRow("Stock display ranges", "If you enter ranges in here then the site will not display the real stock level "
						+ "but will choose the appropriate range.", new TextTag(context, "stockDisplayRangesString", module
						.getStockDisplayRangesString(), 40)));
			}

			private void stockSystem() {

				SelectTag stockSystemTag = new SelectTag(context, "stockSystem", module.getStockSystem());
				stockSystemTag.addOptions(StockCalculationSystem.values());

				sb.append(new AdminRow("Stock system",
						"How should the system calculate available stock from the stocks available to us from suppliers and internally.",
						stockSystemTag));
			}

			@Override
			public String toString() {

				sb.append(new FormTag(StockModuleHandler.class, "save", "post"));
				sb.append(new HiddenTag("itemType", itemType));

                commands();
				stock();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (itemType == null) {
			return index();
		}

		StockModule module = itemType.getStockModule();

		module.setStockControl(stockControl);

		module.setOutStockIcon(outStockIcon);
		module.setOutStockMsgDefault(outStockMsgDefault);
		module.setOutStockMsgOverride(outStockMsgOverride);

		module.setStockDisplayRanges(stockDisplayRangesString);

		module.setInStockIcon(inStockIcon);
		module.setInStockMsgDefault(inStockMsgDefault);
		module.setInStockMsgOverride(inStockMsgOverride);

		module.setDefaultBackorders(defaultBackorders);

		module.setBuyQtyRestriction(buyQtyRestriction);
		module.setStockSystem(stockSystem);

		if (resetStock) {
			module.resetStock(stock);
		}

		if (resetLeadtime != null) {
			module.resetLeadtime(resetLeadtime);
		}

		if (resetBackorders) {
			module.resetBackorders(backorders);
		}

		module.save();

		clearParameters();
		return main();
	}

}
