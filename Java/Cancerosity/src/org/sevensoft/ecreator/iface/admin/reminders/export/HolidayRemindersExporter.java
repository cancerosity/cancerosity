package org.sevensoft.ecreator.iface.admin.reminders.export;

import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.CsvSaver;
import org.sevensoft.ecreator.model.reminders.HolidayReminder;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.RequestContext;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * User: MeleshkoDN
 * Date: 17.09.2007
 * Time: 13:36:51
 */
public class HolidayRemindersExporter {

    private RequestContext context;

    public HolidayRemindersExporter(RequestContext context) {
        this.context = context;
    }

    public File export() throws IOException {
        File file = File.createTempFile("reminders_export", null);
        file.deleteOnExit();
        CsvManager csvman = new CsvManager();
        csvman.setSeparator(",");
        CsvSaver saver = csvman.makeSaver(file);
        saver.begin();
        saver.next(getHeader());
        List<HolidayReminder> holidayReminders = SimpleQuery.execute(context, HolidayReminder.class);
        for (HolidayReminder reminder : holidayReminders) {
            saver.next(new String[]{reminder.getEmail(), reminder.getHolidayDate().toString("dd.MM.yyyy")});
        }
        saver.end();
        return file;
    }

    private String[] getHeader() {
        return new String[]{"Email", "Holiday date"};
    }
}
