package org.sevensoft.ecreator.iface.admin.misc.content;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.iface.admin.misc.content.panels.KeywordDensityPanel;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.design.styles.Style;
import org.sevensoft.ecreator.model.extras.languages.Language;
import org.sevensoft.ecreator.model.misc.content.ContentBlock;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;

/**
 * @author sks 27 Jul 2006 10:41:53
 *
 */
@Path("admin-content-block.do")
public class ContentBlockHandler extends BlockEditHandler {

	private ContentBlock		block;
	private Language			language;
	private String			content;
	private boolean			simpleEditor;
	private final transient Seo	seo;
	private String			code;
	private Style			style;

	public ContentBlockHandler(RequestContext context) {
		super(context);
		this.seo = Seo.getInstance(context);
	}

	@Override
	protected Block getBlock() {
		return block;
	}

	@Override
	protected String getTitle() {
		return "Content";
	}

	@Override
	protected void saveSpecific() {

		block.setLanguage(language);
		block.setContent(content);

		if (simpleEditor != block.isSimpleEditor()) {
			block.setSimpleEditor(simpleEditor);
			block.log("Simple editor changed to " + simpleEditor);
		}
		block.setStyle(style);

		if (isSuperman()) {
			block.setCode(code);
		}

		block.log("Updated");
	}

	@Override
	protected void specifics(StringBuilder sb) {

		if (miscSettings.isHtmlEditor() && !block.isSimpleEditor()) {
			RendererUtil.tinyMce(context, sb, "content");
		}

		sb.append(new AdminTable("Content"));

//      doesn't work properly
//		sb.append(new AdminRow("Style", "Choose the layout style for this content.", new SelectTag(context, "style", block.getStyle(), Style.values(),
//				"-Manual layout-")));

		if (Module.Languages.enabled(context)) {

			sb.append(new AdminRow("Language", "Choose the language for this content", new SelectTag(context, "language", block.getLanguage(), Language
					.values(), "-Not set-")));

		}

		sb.append(new AdminRow("Simple editor", "Disable HTML content editor for editors on this page", new BooleanRadioTag(context, "simpleEditor", block
				.isSimpleEditor())));

		sb.append("<tr><td colspan='2'>" + new TextAreaTag(context, "content", block.getContent()).setId("content").setStyle("width: 100%; height: 320px;")
				+ "</td></tr>");

		if (block.hasContent() && seo.isKeywordDensity()) {
			sb.append(new KeywordDensityPanel(context, block.getContent()));
		}

		if (isSuperman()) {
			sb.append("<tr><td class='developer' colspan='2'>Code (Enter html / style / scripts for this category):<br/>"
					+ new TextAreaTag(context, "code", block.getCode(), 120, 8) + "</td></tr>");
		}

		sb.append("</table>");
	}

}
