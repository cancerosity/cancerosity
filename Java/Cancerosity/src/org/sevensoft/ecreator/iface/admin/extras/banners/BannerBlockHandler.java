package org.sevensoft.ecreator.iface.admin.extras.banners;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.advertising.banners.Banner;
import org.sevensoft.ecreator.model.advertising.banners.blocks.BannerRotationBlock;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;

/**
 * @author sks 22 Aug 2006 09:00:07
 *
 */
@Path("admin-banners-block.do")
public class BannerBlockHandler extends BlockEditHandler {

	private BannerRotationBlock	block;
	private Banner		banner;

	public BannerBlockHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Block getBlock() {
		return block;
	}

	@Override
	protected String getTitle() {
		return "Banner rotation";
	}

	public Object removeBanner() throws ServletException {

		if (block == null)
			return main();

		block.removeBanner(banner);
		return main();
	}

	@Override
	protected void saveSpecific() {

		if (banner != null)
			block.addBanner(banner);
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Banner block details"));

		for (Banner banner : block.getBanners()) {

			sb.append(new AdminRow("Banner", "Banner currently used in this banner rotation", banner.getName() + " "
					+ new ButtonTag(BannerBlockHandler.class, "removeBanner", "Remove", "block", block, "banner", banner)));

		}

		sb.append(new AdminRow("Add Banner", "Select a banner to add to this banner rotation", new SelectTag(context, "banner", null, Banner.get(context),
				"-Select banner to add-")));

		sb.append("</table>");
	}
}
