package org.sevensoft.ecreator.iface.admin.reports.hits.page;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.reports.StatsMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.stats.StatablePage;
import org.sevensoft.ecreator.model.stats.hits.pages.PageHitCounterMonthly;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 03-Oct-2005 16:12:08
 * 
 */
@Path("admin-reports-hits-pages.do")
public class PageHitsHandler extends AdminHandler {

	private Date	month;

	public PageHitsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {
		return monthly();
	}

	/**
	 * Shows the most popular recorders this month
	 */
	public Object monthly() throws ServletException {

		if (month == null) {
			month = new Date();
		}

		month = month.beginMonth();

		int limit = 100;

		final List<PageHitCounterMonthly> counters = PageHitCounterMonthly.get(context, month, limit);

		AdminDoc doc = new AdminDoc(context, user, "Most popular pages " + month.toString("MMMM-yy"), Tab.Stats);
		doc.setMenu(new StatsMenu());
		doc.setIntro("This page shows the " + limit + " most popular pages for the month " + month.toString("MMMM-yy"));
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Most popular pages " + month.toString("MMMM-yy")));

				sb.append(new FormTag(PageHitsHandler.class, "monthly", "get"));
				SelectTag monthTag = new SelectTag(context, "month");
				monthTag.setAutoSubmit();

				Date month = new Date().beginMonth();
				for (int n = 0; n < 13; n++) {
					monthTag.addOption(month, month.toString("MMMM-yy"));
					month = month.removeMonths(1);
				}

				sb.append("<tr><th align='right' colspan='4'>Choose month ");
				sb.append(monthTag);
				sb.append("</th></tr>");

				sb.append("</form>");

				sb.append("<tr>");
				sb.append("<td>Page</td>");
				sb.append("<td>Page views</td>");
				sb.append("</tr>");

				for (PageHitCounterMonthly counter : counters) {

					final StatablePage element = counter.getElement();

					sb.append("<tr>");

					sb.append("<td>");
					if (element != null) {
						sb.append(new LinkTag(element.getUrl(), element.getName()));
					}
					sb.append("</td>");

					sb.append("<td>" + counter.getTotal() + "</td>");
					sb.append("</tr>");

				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

}
