package org.sevensoft.ecreator.iface.admin.ecom.bookings;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.validators.DateValidator;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.ItemsMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.model.bookings.Period;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * User: MeleshkoDN
 * Date: 11.09.2007
 * Time: 15:49:31
 */
@Path("admin-periods.do")
public class PeriodListHandler extends AdminHandler {

    private ItemType itemType;
    private Period period;
    private final transient List<Period> periods;

    private String startDate;
    private String endDate;
    private String periodName;

    public PeriodListHandler(RequestContext context) {
        super(context);
        periods = SimpleQuery.execute(context, Period.class);
    }

    public Object main() throws ServletException {
        if (itemType == null) {
            return index();
        }
        AdminDoc doc = new AdminDoc(context, user, "Periods", Tab.getItemTab(itemType));
        doc.setMenu(new ItemsMenu(itemType));
        doc.setIntro("Edit old and add new periods");
        doc.addBody(new Body() {
            @Override
            public String toString() {
                sb.append(new FormTag(PeriodListHandler.class, "addPeriod", "post"));
                sb.append(new HiddenTag("itemType", itemType));
                sb.append(new AdminTable("Periods"));
                sb.append("<tr>");
                sb.append("<th width='25%' align='center'>Name</th>");
                sb.append("<th width='25%' align='center'>Start date</th>");
                sb.append("<th width='25%' align='center'>End date</th>");
                sb.append("<th width='25%' align='center'></th>");
                for (Period period : periods) {
                    sb.append("<tr>");
                    sb.append("<td>" + period.getName() + "</td>");
                    sb.append("<td>" + period.getStartDate().toString("dd/MM/yyyy") + "</td>");
                    sb.append("<td>" + period.getEndDate().toString("dd/MM/yyyy") + "</td>");
                    sb.append("<td>" + new LinkTag(PeriodListHandler.class, "delete", new DeleteGif(), "period", period, "itemType", itemType)
                            .setConfirmation("Are you sure you want to delete this period?") + "</td>");
                    sb.append("</tr>");
                }
                sb.append(periodInputFields());
                sb.append("</table>");
                sb.append("</form>");
                return sb.toString();
            }
        });
        return doc;
    }

    private void validateInput() {
        test(new DateValidator(), "startDate");
        test(new DateValidator(), "endDate");
        test(new RequiredValidator(), "startDate");
        test(new RequiredValidator(), "endDate");
        test(new RequiredValidator(), "periodName");
    }

    private String periodInputFields() {
        StringBuffer sb = new StringBuffer();
        TextTag periodName = new TextTag(context, "periodName", "Period " + (SimpleQuery.count(context, Period.class) + 1), 12);
        TextTag startDateTag = new TextTag(context, "startDate", null, 10);
        startDateTag.setClass("date-pick");
        startDateTag.setId("date1");
        TextTag endDateTag = new TextTag(context, "endDate", null, 10);
        endDateTag.setClass("date-pick");
        endDateTag.setId("date2");
        sb.append("<tr>");
        sb.append("<td>" + periodName + new ErrorTag(context, "periodName", "<br/>") + "</td>");
        sb.append("<td>" + startDateTag + new ErrorTag(context, "startDate", "<br/>") + "</td>");
        sb.append("<td>" + endDateTag + new ErrorTag(context, "endDate", "<br/>") + "</td>");
        sb.append("<td>" + new SubmitTag("Add period") + "</td>");
        sb.append("</tr>");
        return sb.toString();
    }

    public Object delete() throws ServletException {
        if (period == null) {
            return index();
        }
        period.delete();
        return new ActionDoc(context, "Period was deleted", new Link(PeriodListHandler.class, null, "itemType", itemType));
    }

    public Object addPeriod() throws ParseException, ServletException {

        validateInput();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        if (hasErrors()) {
            logger.fine("[PeriodListHandler] errors=" + context.getErrors());
            return main();
        }

        java.util.Date startDateValue = dateFormat.parse(startDate);
        java.util.Date endDateValue = dateFormat.parse(endDate);

        if (endDateValue.compareTo(startDateValue) <= 0) {
            addError("Start date must be less then end date");
            return main();
        }
        Date begin = new Date(startDateValue.getTime());
        Date end = new Date(endDateValue.getTime());

        List<Period> intersections = getIntersections(begin, end);
        if (!intersections.isEmpty()) {
            addError("Period can't intersect with other periods !");
            String info = "";
            for (Period p : intersections) {
                info += p.getName() + ";";
            }
            addError("Entered dates intersect with periods: " + info);
            return main();
        }

        Period period = new Period(context);
        period.setName(periodName);
        period.setStartDate(begin);
        period.setEndDate(end);
        period.save();
        return new ActionDoc(context, "Period was successfully added", new Link(PeriodListHandler.class, null, "itemType", itemType));
    }

    private List<Period> getIntersections(Date start, Date end) {

        Query q = new Query(context, "SELECT * FROM # WHERE (startDate < ? AND endDate > ?) OR (startDate >= ? AND endDate <= ?) OR (startDate <= ? AND endDate >= ?) OR (startDate <= ? AND endDate >= ?) ORDER BY name");
        q.setTable(Period.class);
        q.setParameter(start);
        q.setParameter(end);
        q.setParameter(start);
        q.setParameter(end);
        q.setParameter(start);
        q.setParameter(start);
        q.setParameter(end);
        q.setParameter(end);
        return q.execute(Period.class);

    }

}
