package org.sevensoft.ecreator.iface.admin.containers.boxes;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.containers.boxes.misc.CustomBox;
import org.sevensoft.ecreator.model.attachments.AttachmentUtil;
import org.sevensoft.ecreator.iface.admin.attachments.AdminAttachmentsPanel;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;

import java.util.Map;
import java.util.List;

/**
 * @author sks 01-Jun-2004 21:28:42
 */
@Path("admin-boxes-custom.do")
public class CustomBoxHandler extends BoxHandler {

	private CustomBox	box;
    private Map<Integer, String> attachmentDescriptions;
    private List<Upload> files, imageUploads;
    private boolean makePreviews;

	public CustomBoxHandler(RequestContext context) {
		super(context);
	}

    public CustomBoxHandler(RequestContext context, CustomBox box) {
        super(context);
        this.box = box;
    }

    @Override
	protected void specifics(StringBuilder sb) {
        sb.append(new AdminAttachmentsPanel(context, box, true));
	}

	@Override
	protected Box getBox() {
		return box;
	}

	protected void saveSpecific() throws ServletException {
        AttachmentUtil.save(box, attachmentDescriptions, files, makePreviews);
	}
}
