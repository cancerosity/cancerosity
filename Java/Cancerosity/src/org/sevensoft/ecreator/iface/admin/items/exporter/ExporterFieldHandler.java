// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   ExporterFieldHandler.java

package org.sevensoft.ecreator.iface.admin.items.exporter;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.export.ExportField;
import org.sevensoft.ecreator.model.items.export.ExportFieldType;
import org.sevensoft.ecreator.model.items.export.Exporter;
import org.sevensoft.ecreator.model.items.export.ExporterDao;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;

@Path("admin-exporters-fields.do")
public class ExporterFieldHandler extends AdminHandler {

    private ExportField field;
    private ExporterDao exporterDao;
    private Attribute attribute;
    private int position;
    private ExportFieldType fieldType;
    private Exporter exporter;
    private boolean fullImageUrl;

    public ExporterFieldHandler(RequestContext context) {
        super(context);
        exporterDao = new ExporterDao(context);
    }

    public Object create() {
        ExportField field = exporter.addField();
        return new ActionDoc(context, "A new field has been added", new Link(ExporterFieldHandler.class, null, "field", field));
    }

    public Object delete() {
        field.delete();
        return new ActionDoc(context, "This field has been deleted", new Link(ExporterHandler.class, "edit", "exporter", field.getExporter()));
    }

    public Object main() throws ServletException {
        AdminDoc doc = new AdminDoc(context, user, "Edit expiry bot", null);
        doc.addBody(new Body() {

            private void commands() {
                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Confirm changes"));
                sb.append(new ButtonTag(ExporterFieldHandler.class, "delete", "Delete field", "field", field));
                sb.append(new ButtonTag(ExporterHandler.class, "edit", "Return to exporter", "exporter", field.getExporter()));
                sb.append("</div>");
            }

            private void general() {
                sb.append(new AdminTable("General"));
                sb.append(new AdminRow("Position", "Enter the column number for this field.", new TextTag(context, "position", field.getPosition(), 4)));
                sb.append(new AdminRow("Field type", "Select the field type", new SelectTag(context, "fieldType", field.getFieldType(), ExportFieldType.values(), "None set")));
                if (field.getFieldType() == ExportFieldType.Attribute)
                    sb.append(new AdminRow("Attribute", "The attribute to use.", new SelectTag(context, "attribute", field.getAttribute(), field.getExporter().getItemType().getAttributes(), "-None selected-")));
                if (field.getFieldType() == ExportFieldType.Image)
                    sb.append(new AdminRow("Image URL", "Full item image URL", new BooleanRadioTag(context, "fullImageUrl", field.isFullImageUrl())));
                sb.append("</table>");
            }

            public String toString() {
                sb.append(new FormTag(ExporterFieldHandler.class, "save", "POST"));
                sb.append(new HiddenTag("field", field));
                commands();
                general();
                commands();
                sb.append("</form>");
                return sb.toString();
            }

        }
        );
        return doc;
    }

    public Object save() throws ServletException {
        field.setAttribute(attribute);
        field.setPosition(position);
        field.setFieldType(fieldType);
        field.setFullImageUrl(fullImageUrl);
        field.save();
        return new ActionDoc(context, "This field has been updated", new Link(ExporterFieldHandler.class, null, "field", field));
    }

}
