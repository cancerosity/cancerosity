package org.sevensoft.ecreator.iface.admin.ecom.orders;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.menu.OrderMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.frontend.items.ItemHandler;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.values.AValueRenderer;
import org.sevensoft.ecreator.model.crm.messages.Msg;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.results.AvsCheck.AvsResult;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.users.privileges.Privilege;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.EmailTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

import javax.servlet.ServletException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author sks 26 Jul 2006 09:21:06
 */
@Path("admin-orders-transact.do")
public class TransactHandler extends AdminHandler {

    private Order order;
    private AvsResult avsMatch;
    private Money amountCharged;

    public TransactHandler(RequestContext context) {
        super(context);
    }

    public TransactHandler(RequestContext context, Order order) {
        super(context);
        this.order = order;
    }

    public Object authorised() throws ServletException {

        if (order == null) {
            return main();
        }

        order.manualTransact(amountCharged, avsMatch);

        order.clearCard();

        return main();
    }

    public Object declined() throws ServletException {

        if (order == null) {
            return main();
        }

        order.declined();

        order.clearCard();
        
        return main();
    }

    @Override
    public Object main() throws ServletException {

        if (order == null) {
            return new OrderSearchHandler(context).main();
        }

        if (!order.hasCard()) {
            return new OrderHandler(context, order).main();
        }

        StringBuilder sb = new StringBuilder();
        sb.append("Order details for transaction #" + order.getOrderId() + "\n");
        sb.append("-----------------------------------\n\n");

        sb.append("Customer details:\n");
        sb.append("Name: " + order.getAccount().getName() + "\n");
        sb.append("Email: " + order.getAccount().getEmail() + "\n");
        sb.append("\n\n\n");

        sb.append("Delivery address:\n");
        if (order.hasDeliveryAddress())
            sb.append(order.getDeliveryAddress().getLabel("<br/>", true));
        else
            sb.append("Collection");
        sb.append("\n\n\n");

        sb.append("Billing address:\n");
        if (order.hasBillingAddress())
            sb.append(order.getBillingAddress().getLabel("<br/>", true));
        else
            sb.append("Collection");
        sb.append("\n\n\n");

        sb.append("Card details:\n");
        sb.append(order.getCard().getLabelSecure("\n"));
        sb.append("\n\n\n");

        sb.append("Order details:\n");
        for (OrderLine line : order.getLines()) {
            sb.append(line.getQty() + " " + line.getDescription() + " @ " + line.getUnitSellEx() + "\n");
        }

        sb.append("\n\n");
        sb.append("Subtotal: " + order.getTotalEx() + "\n");
        sb.append("Vat: " + order.getTotalVat() + "\n");
        sb.append("Total: " + order.getTotalInc() + "\n");
        sb.append("\n\n\n");

        AdminDoc doc = new AdminDoc(context, user, "Order #" + order.getIdString() + " - Details for card payment", Tab.Orders);
        doc.setMenu(new OrderMenu(order));
        doc.addBody(new Body() {

            private void deliveryAddress() {

                sb.append(new AdminTable("Delivery address"));

                if (order.isReservation()) {

                    sb.append("<tr><td valign='top'>Reserved for pickup</td></tr>");

                } else {

                    sb.append("<tr><td valign='top'>" + order.getDeliveryAddress().getLabel("<br/>", true));
                    if (order.getDeliveryAddress().hasInstructions())
                        sb.append(order.getDeliveryAddress().getInstructions());
                    sb.append("</td></tr>");
                }

                sb.append("</table>");
            }

            private void details() {

                sb.append(new AdminTable("Order details"));

                sb.append("<tr><th width='180'>Order ID</th><td>" + order.getOrderId() + "</td></tr>");
                sb.append("<tr><th width='180'>Status</th><td>" + order.getStatus() + "</td></tr>");
                sb.append("<tr><th width='180'>Date placed</th><td>" + order.getDatePlaced().toString("dd-MMM-yy") + "</td></tr>");
                sb.append("<tr><th width='180'>Member name</th><td>"
                        + new LinkTag(ItemHandler.class, null, order.getAccount().getName(), "item", order.getAccount()) + "</td></tr>");
                sb.append("<tr><th width='180'>Member email</th><td>" + new EmailTag(order.getAccount().getEmail()) + "</td></tr>");
                sb.append("</td></tr>");

                if (shoppingSettings.isCustomerReference()) {

                    sb.append(new FormTag(OrderHandler.class, "setCustomerReference", "POST"));
                    sb.append(new HiddenTag("order", order));
                    sb.append("<tr><th width='250'>Customer reference</th><td>"
                            + new TextTag(null, "customerReference", order.getCustomerReference(), 30) + new SubmitTag("Change reference")
                            + "</td></tr>");
                    sb.append("</form>");

                }

                // attribute values

                for (Map.Entry<Attribute, List<String>> entry : order.getAttributeValues().entryListSet()) {

                    Attribute attribute = entry.getKey();
                    List<String> values = entry.getValue();

                    sb.append("<tr><th width='250'>" + attribute.getName() + "</th><td>");
                    sb.append(new AValueRenderer(context, order, attribute, values));
                    sb.append("</td></tr>");

                }

                sb.append("</table>");
            }

            private void invoiceAddress() {

                sb.append(new AdminTable("Billing address"));

                if (order.hasBillingAddress())
                    sb.append("<tr><td valign='top'>" + order.getBillingAddress().getLabel("<br/>", true) + "</td></tr>");
                else
                    sb.append("<tr><td valign='top'>Reserved for pickup</td></tr>");

                sb.append("</table>");
            }

            private void lines() {

                sb.append(new AdminTable("Order lines"));

                sb.append("<tr>");
                sb.append("<th>Description</th>");
                sb.append("<th width='80'>Qty</th>");
                sb.append("<th width='80'>Unit Price</th>");
                sb.append("<th width='80'>Line Price</th>");
                sb.append("</tr>");

                sb.append(new FormTag(OrderHandler.class, "updateLines", "post"));
                sb.append(new HiddenTag("order", order));

                for (OrderLine line : order.getLines()) {

                    sb.append("<tr>");

                    sb.append("<td>");
                    if (line.hasItem()) {

                        sb.append(new LinkTag(ItemHandler.class, "edit", line.getDescription(), "item", line.getItem()));
                        if (line.hasOptionsDetails()) {
                            sb.append("<br/>");
                            sb.append(line.getOptionsDetails());
                        }

                    } else {

                        sb.append(line.getDescription());
                    }

                    if (line.hasAttachments()) {

                        sb.append("<br/>");

                        Iterator<Attachment> iter = line.getAttachments().iterator();
                        while (iter.hasNext()) {

                            Attachment attachment = iter.next();

                            sb.append(new ImageTag("files/graphics/admin/attachment.png"));

                            sb.append(attachment.getName());
                            sb.append(" (");
                            sb.append(attachment.getSizeKb());
                            sb.append(") - ");

                            sb.append(new LinkTag(OrderHandler.class, "downloadFile", "download", "order", order, "file", attachment));

                            if (iter.hasNext())
                                sb.append(", ");
                        }
                    }

                    sb.append("</td>");

                    sb.append("<td width='80'>" + line.getQty() + "</td>");
                    sb.append("<td width='80'>" + line.getUnitSellEx() + "</td>");
                    sb.append("<td width='80'>" + line.getLineSellEx() + "</td>");
                    sb.append("</tr>");

                }

                // delivery

                /*
                     * if delivery module is enabled, then we want to show delivery options on this order, and if the order is
                     * editable then make them as a text box
                     */
                if (Module.Delivery.enabled(context)) {

                    sb.append("<tr>");
                    sb.append("<td>");
                    if (order.hasDeliveryDetails())
                        sb.append(order.getDeliveryDetails());
                    sb.append("</td>");
                    sb.append("<td width='80'>&nbsp;</td>");
                    sb.append("<td width='80'>" + order.getDeliveryChargeEx() + "</td>");

                    sb.append("<td width='80'>" + order.getDeliveryChargeEx() + "</td>");
                    sb.append("</tr>");
                }

                sb.append("<tr>");
                sb.append("<td colspan='3' align='right'>Subtotal</td>");
                sb.append("<td>" + order.getTotalEx() + "</td></th>");
                sb.append("</tr>");

                sb.append("<tr>");
                sb.append("<td colspan='3' align='right'>Vat</td><td>" + order.getTotalVat() + "</td></th>");
                sb.append("</tr>");

                sb.append("<tr>");
                sb.append("<td colspan='3' align='right'>Total</td><td>" + order.getTotalInc() + "</td></th>");
                sb.append("</tr>");

                sb.append("</table>");

            }

            private void messages() {

                sb.append(new AdminTable("Messages"));

                for (Msg message : order.getMessages()) {

                    sb.append("<tr><td valign='top' width='140'>" + message.getAuthor() + "<br/>" + message.getDate().toString("dd/MM/yy HH:mm")
                            + "</td><td valign='top'>");
                    sb.append(HtmlHelper.nl2br(message.getBody()) + "</td></tr>");
                }

                sb.append(new FormTag(OrderHandler.class, "addMessage", "POST"));
                sb.append(new HiddenTag("order", order));
                sb.append("<tr><td colspan='2'>" + new TextAreaTag(context, "content", 60, 4) + "<br/>" + new SubmitTag("Add message") + "</td></tr>");
                sb.append("</form>");

                sb.append("</table>");
            }

            private void payment() {

                sb.append(new AdminTable("Payment"));

                sb.append("<tr><th width='130'>Payment type</th><td>" + order.getPaymentType() + "</td></tr>");

                if (order.getPaymentType() == PaymentType.CardTerminal) {

                    SelectTag tag = new SelectTag(context, "card", order.getCard());
                    tag.setAny("-No card details set-");
                    tag.addOptions(order.getAccount().getCards());

                    if (Privilege.CardDetails.yes(user, context)) {

                        sb.append("<tr><th width='130'>Card details</th><td>" + order.getCard().getLabel("<br/>") + "</td></tr>");
                    } else {
                        sb.append("<tr><th width='130'>Card details</th><td>" + order.getCard().getLabelSecure("<br/>") + "</td></tr>");

                    }

                    sb.append(new FormTag(TransactHandler.class, "authorised", "post"));
                    sb.append(new HiddenTag("order", order));
                    sb.append("<tr><td colspan='2' align='center'>Payment authorised: ");

                    if (PaymentSettings.getInstance(context).isTerminalAvsSupport()) {

                        logger.fine("terminal avs support on, showing avs result selector");
                        sb.append(new SelectTag(context, "avsResult", null, AvsResult.values(), "-Select AVS Result-"));
                    }

                    sb.append(new SubmitTag("Authorised").setConfirmation("Confirm the payment was authorised"));
                    sb.append("<br/>Payment declined: ");
                    sb.append(new ButtonTag(TransactHandler.class, "declined", "Declined", "order", order)
                            .setConfirmation("Confirm the payment was declined"));
                    sb.append("</td></tr>");
                    sb.append("</form>");

                }

                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new TableTag(null, "center").setStyle("width: 100%;"));
                sb.append("<tr><td width='50%' valign='top' style='padding-right: 6px;'>");
                details();
                sb.append("</td><td width='50%' valign='top' style='padding-left: 6px;'>");

                if (Privilege.OrderPayment.yes(user, context)) {

                    payment();

                }

                sb.append("</td></tr>");
                sb.append("</table>");

                sb.append(new TableTag(null, "center").setStyle("width: 100%;"));
                sb.append("<tr><td width='50%' valign='top' style='padding-right: 6px;'>");
                deliveryAddress();
                sb.append("</td><td width='50%' valign='top' style='padding-left: 6px;'>");
                invoiceAddress();
                sb.append("</td></tr>");
                sb.append("</table>");

                if (!order.isCancelled())
                    lines();

                return sb.toString();
            }

        });
        return doc;
    }
}
