package org.sevensoft.ecreator.iface.admin.items.reviews;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.items.reviews.Review;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 27-Feb-2006 16:28:46
 *
 */
@Path("admin-reviews.do")
public class ReviewHandler extends AdminHandler {

	private Review	review;
	private String	author;
	private String	content;

	public ReviewHandler(RequestContext context) {
		super(context);
	}

	public Object delete() throws ServletException {

		if (review == null) {
			return main();
		}

		review.delete();
		return new ActionDoc(context, "This review has been deleted", new Link(ReviewSearchHandler.class));
	}

	public Object main() throws ServletException {

		if (review == null) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit review", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div class='actions' align='center'>");
				sb.append(new SubmitTag("Update review"));
				sb.append(new ButtonTag(ReviewHandler.class, "delete", "Delete review", "review", review)
						.setConfirmation("Are you sure you want to delete this review?"));
				sb.append("</div>");
			}

			/**
			 * 
			 */
			private void details() {
				sb.append(new AdminTable("Review details"));

				sb.append(new AdminRow("Author", "This is the name of the person who submitted the review.", new TextTag(context, "author", review
						.getAuthor(), 30)));

				sb.append(new AdminRow("Content", "This is the review content.", new TextAreaTag(context, "content", review.getContent(), 60, 5)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(ReviewHandler.class, "save", "post"));
				sb.append(new HiddenTag("review", review));

                commands();
				details();
				commands();

				sb.append("</form>");
				return sb.toString();
			}
		});
		return doc;

	}

	public Object save() throws ServletException {

		if (review == null) {
			return main();
		}

		review.setContent(content);
		review.setAuthor(author);
		review.save();

		return new ActionDoc(context, "This review has been updated", new Link(ReviewHandler.class, null, "review", review));
	}
}
