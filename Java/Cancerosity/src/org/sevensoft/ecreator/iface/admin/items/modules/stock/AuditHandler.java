package org.sevensoft.ecreator.iface.admin.items.modules.stock;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.stock.Audit;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 4 Sep 2006 17:25:47
 *
 */
@Path("admin-items-audit.do")
public class AuditHandler extends AdminHandler {

	private Item	item;

	public AuditHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (item == null)
			return index();

		AdminDoc page = new AdminDoc(context, user, "Stock audit trail", Tab.getItemTab(item));
		page.setMenu(new EditItemMenu(item));
		page.setIntro("Here is the stock audit trail for '" + item.getName() + "'");
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("form", 1, 0).setCaption("Stock audit trail"));

				sb.append("<tr>");
				sb.append("<th>Date / Time</th>");
				sb.append("<th>Stock level</th>");
				sb.append("<th>Adjustment</th>");
				sb.append("<th>Message</th>");
				sb.append("</tr>");

				for (Audit audit : item.getAuditTrail()) {

					sb.append("<tr>");
					sb.append("<td>" + audit.getDate().toString("HH:mm dd-MMM-yyyy") + "</td>");
					sb.append("<td>" + audit.getStock() + "</td>");
					sb.append("<td>" + audit.getAdjustment() + "</td>");
					sb.append("<td>" + audit.getMessage() + "</td>");

					sb.append("</tr>");
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return page;
	}

}
