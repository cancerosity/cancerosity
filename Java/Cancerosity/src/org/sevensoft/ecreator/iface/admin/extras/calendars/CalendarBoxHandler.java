package org.sevensoft.ecreator.iface.admin.extras.calendars;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.extras.calendars.Calendar;
import org.sevensoft.ecreator.model.extras.calendars.box.CalendarBox;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

import javax.servlet.ServletException;
import java.util.List;

/**
 * @author sks 14 Jul 2006 13:26:41
 */
@Path("admin-boxes-calendar.do")
public class CalendarBoxHandler extends BoxHandler {

    private Calendar calendar;
    private Calendar.Style style;
    private CalendarBox box;
    private Attribute endAttribute;
    private Attribute startAttribute;
    private String todayColor;
    private String eventColor;

    public CalendarBoxHandler(RequestContext context) {
        super(context);
    }

    @Override
    protected Box getBox() {
        return box;
    }

    @Override
    protected void saveSpecific() throws ServletException {

        box.setStartAttribute(startAttribute);
        box.setEndAttribute(endAttribute);
        box.setItemType(itemType);
        box.setStyle(style);
        box.setTodayColor(todayColor);
        box.setEventColor(eventColor);
    }

    @Override
    protected void specifics(StringBuilder sb) {

        sb.append(new AdminTable("Calendar settings"));

        sb.append(new AdminRow("Item type", "Select the item type to show on the calendar", new SelectTag(context, "itemType", box.getItemType(), ItemType
                .getSelectionMap(context), "-Please select-")));

        //		sb.append(new AdminRow("Style", "Choose how to display this calendar. Grid is a traditional calendar and flat is a list.", new SelectTag(context,
        //				"style", box.getStyle(), Calendar.Style.values())));

        if (box.hasItemType()) {

            final List<Attribute> dateAttributes = box.getItemType().getAttributes(AttributeType.Date, AttributeType.DateTime);

            SelectTag startAttributeTag = new SelectTag(context, "startAttribute", box.getStartAttribute());
            startAttributeTag.setAny("-Select attribute-");
            startAttributeTag.addOptions(dateAttributes);

            SelectTag endAttributeTag = new SelectTag(context, "endAttribute", box.getEndAttribute());
            endAttributeTag.setAny("-Select attribute-");
            endAttributeTag.addOptions(dateAttributes);


            sb.append(new AdminRow("Take start date from", startAttributeTag));
            sb.append(new AdminRow("Take end date to", endAttributeTag));

            sb.append("<tr>");
            sb.append("<td width='150'><strong>Today color</strong></td>");
            sb.append("<td>" + new TextTag(null, "todayColor", box.getTodayColor(), 12));
            sb.append(" <span style='background: " + box.getTodayColor() + ";'>&nbsp; &nbsp;</span>");
            sb.append("</td></tr>");

            sb.append("<tr>");
            sb.append("<td width='150'><strong>Event day color</strong></td>");
            sb.append("<td>" + new TextTag(null, "eventColor", box.getEventColor(), 12));
            sb.append(" <span style='background: " + box.getEventColor() + ";'>&nbsp; &nbsp;</span>");
            sb.append("</td></tr>");
        }

        sb.append("</table>");

    }
}
