package org.sevensoft.ecreator.iface.admin.ecom.removal;

import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.date.DateTag;
import org.sevensoft.jeezy.http.html.form.date.DateTextTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.ecom.ShoppingMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.config.DeleterHandler;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;

import javax.servlet.ServletException;

/**
 * User: Tanya
 * Date: 14.04.2011
 */
@Path("admin-order-removal.do")
public class OrderRemovalHandler extends AdminHandler {

    private transient OrderSettings orderSettings;

    public OrderRemovalHandler(RequestContext context) {
        super(context);
        orderSettings = OrderSettings.getInstance(context);
    }

    public Object main() throws ServletException {
        AdminDoc doc = new AdminDoc(context, user, "Order export", Tab.Orders);
        doc.setMenu(new ShoppingMenu());
        doc.addBody(new Body() {

            @Override
            public String toString() {
                sb.append(new AdminTable(""));


                sb.append(new FormTag(DeleterHandler.class, "deleteOrdersByStatus", "Post"));

                SelectTag statusTag = new SelectTag(context, "status");
                statusTag.addOptions(orderSettings.getStatuses());
                statusTag.setAny("--All--");

                sb.append(new AdminRow("Remove by Status", "Choose a status from the list or keep it empty to remove all the orders and related data.",
                        statusTag + " " + new SubmitTag("Remove").setConfirmation("Are you sure want to delete orders?")));

                sb.append("</form>");

                sb.append(new FormTag(DeleterHandler.class, "deleteOrdersByDate", "Post"));

                DateTextTag startDate = new DateTextTag(context, "dateStart");
                DateTextTag endDate = new DateTextTag(context, "dateEnd");

                sb.append(new AdminRow("Remove by Date", "Choose a date range using format DD/MM/YYYY or keep it empty to remove all the orders and related data.",
                        startDate + " " + endDate + " " + new SubmitTag("Remove").setConfirmation("Are you sure want to delete orders?")));

                sb.append("</form>");

                sb.append("</table>");

                return sb.toString();
            }

        });
        return doc;
    }
}
