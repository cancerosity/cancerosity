package org.sevensoft.ecreator.iface.admin.reports.hits.page;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.reports.StatsMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.stats.hits.pages.PageHitCounterMonthly;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author Stephen K Samuel samspade79@gmail.com May 24, 2010 9:56:56 AM
 * 
 */
@Path("admin-reports-hits-items.do")
public class ItemHitsHandler extends AdminHandler {

	private Item	item;
	private Item	account;

	public ItemHitsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {
		return monthly();
	}

	/**
	 * Shows the hits for the item for the past 12 months
	 */
	public Object monthly() throws ServletException {

		final List<PageHitCounterMonthly> counters;
		if (account != null)
			counters = PageHitCounterMonthly.getForAccount(context, account);
		else
			counters = PageHitCounterMonthly.getForItem(context, item, 24);

		AdminDoc doc = new AdminDoc(context, user, "Hits per month", Tab.Stats);
		doc.setMenu(new StatsMenu());
		doc.setIntro("This page shows the hits per month for this item for the past 24 months");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Hits per month"));

				sb.append("<tr>");
				sb.append("<td>Month</td>");
				if (account != null) {
					sb.append("<td>Listing</td>");
				}
				sb.append("<td>Unique</td>");
				sb.append("<td>Total</td>");
				sb.append("</tr>");

				for (PageHitCounterMonthly counter : counters) {

					sb.append("<tr>");

					sb.append("<td>" + counter.getDate().toMonthYearString() + "</td>");
					if (account != null) {
						sb.append("<td>" + counter.getItem().getName() + "</td>");
					}
					sb.append("<td>" + counter.getUniques() + "</td>");
					sb.append("<td>" + counter.getTotal() + "</td>");
					sb.append("</tr>");

				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}
}