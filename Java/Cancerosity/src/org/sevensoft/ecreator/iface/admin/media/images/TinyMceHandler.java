package org.sevensoft.ecreator.iface.admin.media.images;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.superstrings.comparators.NaturalStringComparator;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.results.StringResult;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * @author sks 28-Jul-2005 10:00:45
 */
@Path("tinymce.do")
public class TinyMceHandler extends AdminHandler {

    static List<String> types, imageTypes, flashTypes;

    static {

        imageTypes = Arrays.asList(".jpg", ".jpeg", ".png", ".gif");
        flashTypes = Arrays.asList(".swf");

        types = new ArrayList<String>();
        types.addAll(imageTypes);
        types.addAll(flashTypes);
    }

    private String filename;
    private List<Upload> upload;

    public TinyMceHandler(RequestContext context) {
        super(context);
    }

    public Object delete() {

        if (this.filename != null) {
//			File file = getRealFile(this.config.getImagesPath() + "/" + this.filename);
            File file = ResourcesUtils.getRealImage(this.filename);
            if (file.exists())
                file.delete();
        }

        return main();
    }

    public Object flash() {

        File imagesRootDir = ResourcesUtils.getRealImagesDir();
        if (!imagesRootDir.exists())
            return "";

        final String[] files = imagesRootDir.list();
        List<String> list = new ArrayList<String>(Arrays.asList(files));

        CollectionsUtil.filter(list, new Predicate<String>() {

            public boolean accept(String obj) {
                String fn = obj.toLowerCase();
                for (String string : flashTypes)
                    if (fn.endsWith(string))
                        return true;
                return false;
            }

        });

        if (list.size() == 0)
            return "";

        Collections.sort(list, NaturalStringComparator.instance);

        StringBuilder sb = new StringBuilder("var tinyMCEFlashList = new Array(\n");
        Iterator<String> iter = list.iterator();
        while (iter.hasNext()) {

            String string = iter.next();
            sb.append("[\"" + string + "\",\"" + this.config.getImagesPath() + "/" + string + "\"]");
            if (iter.hasNext())
                sb.append(",\n");
        }

        sb.append("\n);");

        return sb.toString();
    }

    public Object images() {

        File imagesRootDir = ResourcesUtils.getRealImagesDir();
        if (!imagesRootDir.exists())
            return "";

        final String[] files = imagesRootDir.list();
        List<String> list = new ArrayList<String>(Arrays.asList(files));

        CollectionsUtil.filter(list, new Predicate<String>() {

            public boolean accept(String obj) {
                String fn = obj.toLowerCase();
                for (String string : imageTypes)
                    if (fn.endsWith(string))
                        return true;
                return false;
            }

        });

        if (list.size() == 0)
            return "";

        Collections.sort(list, NaturalStringComparator.instance);

        StringBuilder sb = new StringBuilder("var tinyMCEImageList = new Array(\n");
        Iterator<String> iter = list.iterator();
        while (iter.hasNext()) {

            String string = iter.next();
            sb.append("[\"" + string + "\",\"" + config.getImagesPath() + "/" + string + "\"]");
            if (iter.hasNext())
                sb.append(",\n");
        }

        sb.append("\n);");

        return sb.toString();
    }

    @Override
    public Object main() {
        return images();
    }

    public Object upload() {

        for (Upload u : upload)
            try {
                u.writeUnique(ResourcesUtils.getRealImagesPath());
            } catch (IOException e) {
                e.printStackTrace();
            }

        return new StringResult("<html><head><script>parent.location.reload(true);</script></head></html>", "text/html");
    }
}
