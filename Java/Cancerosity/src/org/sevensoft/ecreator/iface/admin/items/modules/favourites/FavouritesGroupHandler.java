package org.sevensoft.ecreator.iface.admin.items.modules.favourites;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.favourites.FavouritesGroup;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 23 Aug 2006 13:19:04
 *
 */
@Path("admin-favourites-groups.do")
public class FavouritesGroupHandler extends AdminHandler {

	private FavouritesGroup	favouritesGroup;
	private String		name;
	private Markup		markup;
	private Attribute		attribute;
	private ItemType		itemType;
	private boolean		rename;

	public FavouritesGroupHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		if (name != null) {
			favouritesGroup = new FavouritesGroup(context, name);
		}

		clearParameters();
		return main();
	}

	public Object delete() throws ServletException {

		if (favouritesGroup == null)
			return main();

		favouritesGroup.delete();

		clearParameters();
		return main();
	}

	public Object edit() throws ServletException {

		if (favouritesGroup == null) {
			return main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit favourites group", null);
		doc.addBody(new Body() {

			private void attributes() {

				sb.append(new TableTag("form", 1, 0).setCaption("Attributes"));

				for (Attribute attribute : favouritesGroup.getAttributes()) {

					sb.append("<tr><td>"
							+ attribute.getName()
							+ "</td><td>"
							+ new ButtonTag(FavouritesGroupHandler.class, "removeAttribute", "Remove", "attribute", attribute, "favouritesGroup",
									favouritesGroup) + "</td></tr>");

				}

				sb.append("<tr><td>" + new SelectTag(context, "attribute", null, favouritesGroup.getItemType().getAttributes(), "-Add attribute-")
						+ "</td><td></td></tr>");
				sb.append("</table>");
			}

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update favourites group"));
				sb.append(new ButtonTag(FavouritesGroupHandler.class, null, "Return to menu"));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("Favourites group"));

				sb.append(new AdminRow("Name", new TextTag(context, "name", favouritesGroup.getName(), 40)));
				sb.append(new AdminRow("Item type", new SelectTag(context, "itemType", favouritesGroup.getItemType(), ItemType.getSelectionMap(context),
						"-Choose item type-")));
				sb.append(new AdminRow("Rename", "Allow members to rename the items in this favourites group", new BooleanRadioTag(context, "rename",
						favouritesGroup.isRename())));

				if (isSuperman() || Module.UserMarkup.enabled(context)) {
					sb.append(new AdminRow("Markup", new SelectTag(context, "markup", favouritesGroup.getMarkup(), Markup.get(context), "Default")));
				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(FavouritesGroupHandler.class, "save", "post"));
				sb.append(new HiddenTag("favouritesGroup", favouritesGroup));

                commands();
				general();

				if (favouritesGroup.hasItemType()) {
					attributes();
				}
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		if (!Module.Favourites.enabled(context)) {
			return index();
		}

		final List<FavouritesGroup> favouritesGroups = FavouritesGroup.get(context);

		AdminDoc doc = new AdminDoc(context, user, "Favourite groups", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void groups() {

				sb.append(new AdminTable("Favourite groups"));

				sb.append("<tr>");
				sb.append("<th width='40'>Id</th>");
				sb.append("<th>Name</th>");
				sb.append("<th width='120'>Edit</th>");
				sb.append("<th width='120'>Delete</th>");
				sb.append("</tr>");

				for (FavouritesGroup favouritesGroup : favouritesGroups) {

					sb.append("<tr>");
					sb.append("<td width='40'>" + favouritesGroup.getId() + "</td>");
					sb.append("<td>" + favouritesGroup.getName() + "</td>");
					sb.append("<td>" + new ButtonTag(FavouritesGroupHandler.class, "edit", "Edit", "favouritesGroup", favouritesGroup) + "</td>");
					sb.append("<td>"
							+ new ButtonTag(FavouritesGroupHandler.class, "delete", "Delete", "favouritesGroup", favouritesGroup)
									.setConfirmation("Are you sure you want to delete this favourites group?") + "</td>");
					sb.append("</tr>");

				}

				sb.append(new FormTag(FavouritesGroupHandler.class, "create", "post"));
				sb.append("<tr><td colspan='4'>Add favourites group " + new TextTag(context, "name", 20) + " " + new SubmitTag("Add") + "</td></tr>");
				sb.append("</form>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				groups();
				commands();

				return sb.toString();
			}
		});

		return doc;
	}

	public Object save() throws ServletException {

		if (favouritesGroup == null)
			return main();

		favouritesGroup.setName(name);
		favouritesGroup.setMarkup(markup);
		favouritesGroup.setItemType(itemType);
		favouritesGroup.setRename(rename);
		favouritesGroup.save();

		if (attribute != null) {
			favouritesGroup.addAttribute(attribute);
		}

		clearParameters();
		addMessage("This favourites group has been updated with your changes");
		return edit();
	}
}
