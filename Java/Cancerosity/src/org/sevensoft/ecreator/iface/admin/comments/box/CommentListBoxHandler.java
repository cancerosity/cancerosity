package org.sevensoft.ecreator.iface.admin.comments.box;

import org.sevensoft.ecreator.iface.admin.search.box.CommentSearchEditHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.comments.box.CommentListBox;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;

import javax.servlet.ServletException;

/**
 * User: Tanya
 * Date: 17.11.2011
 */
@Path("admin-boxes-comments.do")
public class CommentListBoxHandler extends BoxHandler {

    private CommentListBox box;
    private int limit;

    public CommentListBoxHandler(RequestContext context) {
        super(context);
    }

    @Override
    protected CommentListBox getBox() {
        return box;
    }

    protected void options(StringBuilder sb) {

        sb.append(new AdminTable("Comment box options"));

        sb.append(new AdminRow("Limit", "Max number of results to show", new TextTag(context, "limit", box.getSearch().getLimit(), 8)));

        if (miscSettings.isAdvancedMode() || isSuperman()) {

            sb.append(new AdminRow(miscSettings.isJimMode(), "Search", "The search controls how comments are retrieved for this box", new ButtonTag(
                    CommentSearchEditHandler.class, null, "Edit search", "search", box.getSearch())));

        }

        sb.append("</table>");
    }

    @Override
    protected void saveSpecific() throws ServletException {
        CommentListBox box = getBox();

        box.getSearch().setLimit(limit);
        box.getSearch().save();

    }

    @Override
    protected void specifics(StringBuilder sb) {
        options(sb);
    }

}
