package org.sevensoft.ecreator.iface.admin.ecom.orders;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.EasyDoc;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 15 Apr 2007 17:50:35
 *
 */
@Path("admin-orders-place.do")
public class PlaceOrderHandler extends AdminHandler {

	private Item	account;

	private String	name, email;
	public PlaceOrderHandler(RequestContext context) {
		super(context);
	}

	private Object askAccountType() {

		EasyDoc doc = new EasyDoc(context, user, "Add a new order", Tab.Orders);
		doc.setIntro("Is this order for a new account or an existing account?");

		doc.addLink("Please create a new account for this order", new Link(PlaceOrderHandler.class, "askCreateAccountDetails"));
		doc.addLink("I will use an existing account for this order", new Link(PlaceOrderHandler.class, "askFindAccountDetails"));

		return doc;
	}

	public Object askCreateAccountDetails() {

		EasyDoc doc = new EasyDoc(context, user, "Add a new order", Tab.Orders);
		doc.setIntro("Enter the new account details");
		doc.setForm(new FormTag(PlaceOrderHandler.class, "createAccount", "post"));

		doc.addText("Enter the name for this account");
		doc.addControls(new TextTag(context, "name", 40), new ErrorTag(context, "name", "<br/>"));

		doc.addSpacing();

		doc.addText("Enter the email for this account");
		doc.addControls(new TextTag(context, "email", 40), new ErrorTag(context, "email", "<br/>"));

		doc.addSpacing();

		doc.addControls(new SubmitTag("Continue"));

		return doc;
	}

	public Object askFindAccountDetails() throws ServletException {

		EasyDoc doc = new EasyDoc(context, user, "Add a new order", Tab.Orders);
		doc.setIntro("Search for an existing account");
		doc.setForm(new FormTag(PlaceOrderHandler.class, "findAccount", "post"));

		doc.addText("Enter the name of the account to search for");
		doc.addControls(new TextTag(context, "name", 40));

		doc.addSpacing();

		doc.addControls(new SubmitTag("Continue"));

		return doc;
	}

	public Object createAccount() throws ServletException {

		test(new RequiredValidator(), "name");
		test(new RequiredValidator(), "email");

		if (context.hasErrors()) {
			return askCreateAccountDetails();
		}
		
		ItemType itemType = ItemType.getAccount(context);
		account = new Item(context, itemType, name, "LIVE", user);

		return createOrder();
	}

	public Object createOrder() throws ServletException {

		if (account == null) {
			return askAccountType();
		}

		Order order = account.addOrder(user, context.getRemoteIp());

		// forward to this order		
		return new ActionDoc(context, "A new order has been created", new Link(OrderHandler.class, null, "order", order));
	}

	public Object findAccount() throws ServletException {

		test(new RequiredValidator(), "name");
		if (context.hasErrors()) {
			return askFindAccountDetails();
		}

		ItemType itemType = ItemType.getAccount(context);

		ItemSearcher searcher = new ItemSearcher(context);
		searcher.setItemType(itemType);
		searcher.setName(name);
		searcher.setVisibleOnly(false);
		searcher.setSearchableOnly(false);
		searcher.setAwaitingModeration(false);
		searcher.setIncludeHidden(true);

		List<Item> items = searcher.getItems();

		EasyDoc doc = new EasyDoc(context, user, "Add a new order", Tab.Orders);
		doc.setIntro("Select the account from the matching results");
		doc.setForm(new FormTag(PlaceOrderHandler.class, "createOrder", "post"));

		doc.addText("Select the account to use");
		doc.addControls(new SelectTag(context, "account", null, items, "-Choose account-"));

		doc.addSpacing();

		doc.addControls(new SubmitTag("Continue"), new ButtonTag(PlaceOrderHandler.class, "askFindAccountDetails", "Search again"));

		return doc;
	}

	@Override
	public Object main() throws ServletException {

		// ask if this is a new customer or existing
		return askAccountType();
	}
}
