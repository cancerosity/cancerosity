package org.sevensoft.ecreator.iface.admin.interaction.userplane.chat;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.frontend.interaction.userplane.chat.UserplaneChatActionHandler;
import org.sevensoft.ecreator.model.interaction.userplane.chat.UserplaneChatSettings;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 26 Nov 2006 14:52:34
 *
 */
@Path("admin-userplane-chat-settings.do")
public class UserplaneChatSettingsHandler extends AdminHandler {

	private final transient UserplaneChatSettings	settings;
	private String						domainID;
	private String						flashcomServer;
	private String						imagesServer;
	private String						textZoneId;
	private String						bannerZoneId;
	private String						rooms;
	private String						lobbyName;
	private int							maxDockItems;

	public UserplaneChatSettingsHandler(RequestContext context) {
		super(context);
		this.settings = UserplaneChatSettings.getInstance(context);
	}

	@Override
	public Object main() {

		if (!Module.UserplaneMessenger.enabled(context))
			return new DashboardHandler(context).main();

		AdminDoc page = new AdminDoc(context, user, "Userplane chat settings", Tab.Extras);
		page.setMenu(new ExtrasMenu());
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update userplane settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb
						.append(new AdminRow(
								"XML handler url",
								"This is the url to the script that handles the XML callbacks for Userplane chat. This needs to be sent to Userplane for integration.",
								new TextTag(context, "none", config.getUrl() + "/" + new Link(UserplaneChatActionHandler.class), 50)
										.setReadOnly(true)));

				sb.append(new AdminRow("Flashcom server", "Enter your flashcom server url", new TextTag(context, "flashcomServer", settings
						.getFlashcomServer(), 40)));

				sb.append(new AdminRow("Images server", "Enter the userplane assigned images url", new TextTag(context, "imagesServer", settings
						.getImagesServer(), 40)));

				sb.append(new AdminRow("Domain ID", "Enter your domain name as specified by userplane", new TextTag(context, "domainID", settings
						.getDomainID(), 20)));

				sb.append(new AdminRow("Banner Zone ID", "Banner advertising zone ID for free installation.", new TextTag(context, "bannerZoneId",
						settings.getBannerZoneId(), 4)));

				sb.append(new AdminRow("Text Zone ID", "Text advertising zone ID for free installation", new TextTag(context, "textZoneId", settings
						.getTextZoneId(), 4)));

				sb.append(new AdminRow("Max dock items", "Sets the maximum number of users that can be docked. 0 disables docking", new TextTag(
						context, "maxDockItems", settings.getMaxDockItems(), 4)));

				sb.append(new AdminRow("Lobby name", "Enter a name for the lobby - the holding area you are presented with when you first enter chat",
						new TextTag(context, "lobbyName", settings.getLobbyName(), 20)));

				sb.append(new AdminRow("Rooms", "These are the rooms setup for users to chat in", new TextAreaTag(context, "rooms", StringHelper
						.implode(settings.getRooms(), "\n"), 40, 4)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(UserplaneChatSettingsHandler.class, "save", "POST"));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return page;
	}

	public Object save() {

		settings.setImagesServer(imagesServer);
		settings.setFlashcomServer(flashcomServer);
		settings.setDomainID(domainID);
		settings.setBannerZoneId(bannerZoneId);
		settings.setTextZoneId(textZoneId);
		settings.setLobbyName(lobbyName);
		settings.setMaxDockItems(maxDockItems);
		settings.setRooms(StringHelper.explodeStrings(rooms, "\n"));
		settings.save();

		clearParameters();

		addMessage("Userplane chat settings updated");
		clearParameters();
		return main();
	}
}
