package org.sevensoft.ecreator.iface.admin.misc.seo.panels;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.misc.seo.Meta;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 17 Aug 2006 15:33:22
 *
 */
public class SeoFriendlyUrlsRenderer {

	private Meta		meta;
	private RequestContext	context;

	public SeoFriendlyUrlsRenderer(RequestContext context, Meta meta) {
		this.context = context;
		this.meta = meta;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(new AdminTable("Friendly urls"));

		sb
				.append(new AdminRow("Friendly url", "Edit the auto generated friendly url here.", new TextTag(context, "friendlyUrl", meta
						.getFriendlyUrl(), 60)));

		sb.append("</table>");

		return sb.toString();
	}

}
