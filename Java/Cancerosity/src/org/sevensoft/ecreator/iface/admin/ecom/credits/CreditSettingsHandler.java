package org.sevensoft.ecreator.iface.admin.ecom.credits;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.ecom.ShoppingMenu;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.ecom.credits.CreditSettings;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;

/**
 * @author sks 03-Oct-2005 14:25:41
 * 
 */
@Path("admin-credits-settings.do")
public class CreditSettingsHandler extends AdminHandler {

	private CreditSettings	creditSettings;
	private String		completedText;

	public CreditSettingsHandler(RequestContext context) {
		super(context);
		this.creditSettings = CreditSettings.getInstance(context);
	}

	@Override
	public Object main() {

		if (!Module.Credits.enabled(context)) {
			return new SettingsMenuHandler(context).main();
		}

		AdminDoc page = new AdminDoc(context, user, "Credit settings", null);
		page.setMenu(new ShoppingMenu());
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update credit settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Completed page text", "This content appears on the completed page of the credit redemption system.",
						new TextAreaTag(context, "completedText", creditSettings.getCompletedText(), 60, 4)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(CreditSettingsHandler.class, "save", "POST"));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return page;
	}

	public Object save() {

		creditSettings.setCompletedText(completedText);
		creditSettings.save();

		addMessage("Credit settings updated");
		clearParameters();
		return main();
	}
}
