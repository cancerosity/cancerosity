package org.sevensoft.ecreator.iface.admin.items;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.export.ItemCsvExporter;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.StreamResult;

/**
 * @author sks 15 Nov 2006 10:26:38
 *
 */
@Path("admin-items-export.do")
public class ItemExportHandler extends AdminHandler {

	private ItemType	itemType;

	public ItemExportHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		ItemCsvExporter exporter = new ItemCsvExporter(context, itemType);
		try {

			File file = exporter.export();
			return new StreamResult(file, "text/csv", itemType.getNamePlural() + ".csv", StreamResult.Type.Attachment);

		} catch (IOException e) {

			e.printStackTrace();
			return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		}
	}

}
