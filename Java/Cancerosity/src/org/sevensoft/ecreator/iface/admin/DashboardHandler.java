package org.sevensoft.ecreator.iface.admin;

import java.util.List;
import java.io.IOException;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.ecreator.model.categories.CategorySettings;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterControl;
import org.sevensoft.ecreator.model.marketing.sms.SmsSettings;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.misc.seo.GoogleSettings;
import org.sevensoft.ecreator.model.stats.hits.site.HitUtil;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHit;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.messages.SystemMessage;
import org.sevensoft.ecreator.util.upgraders.Over50sUpgrader;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks
 */
@Path("admin.do")
public class DashboardHandler extends AdminHandler {

	private transient Company		company;
	private transient SmsSettings		smsBulletin;
	private transient CategorySettings	categorySettings;
	private transient GoogleSettings	statsSettings;
	private transient NewsletterControl	newsletterControl;
	private MiscSettings			miscSettings;

	public DashboardHandler(RequestContext context) {
		super(context);

		this.smsBulletin = SmsSettings.getInstance(context);
		this.categorySettings = CategorySettings.getInstance(context);
		this.company = Company.getInstance(context);
		this.statsSettings = GoogleSettings.getInstance(context);
		this.newsletterControl = NewsletterControl.getInstance(context);
		this.miscSettings = MiscSettings.getInstance(context);
	}

	public Object off() {

		new Over50sUpgrader(context).run();
		return main();
	}

	@Override
	public Object main() {

		AdminDoc doc = new AdminDoc(context, user, company.getName(), null);
		doc.addBody(new Body() {

			private void current() {

				List<SiteHit> hits = HitUtil.getCurrent(context, 0);

				sb.append(new TableTag("adasdadasd").setCaption("Current users"));
				for (SiteHit hit : hits) {
					sb.append("<tr>");
					sb.append("<td>" + new Date(hit.getDate()).toString("MMM dd HH:MM") + "</td>");
					sb.append("<td>" + hit.getIpAddress() + "</td>");

					sb.append("<td>" + hit.getPage() + "</td>");

					sb.append("</tr>");
				}
				sb.append("</table>");

			}

			private void messages() {

				List<SystemMessage> messages = SystemMessage.get(context, 15);
				if (messages.isEmpty()) {
					return;
				}

				sb.append(new TableTag("messages", "center"));
				int n = 0;
				for (SystemMessage message : messages) {
					n++;
					sb.append("<tr>");
					sb.append("<td>" + n + "</td>");
					sb.append("<td>" + message.getDate().toString("dd.MMM.yyyy HH:mm") + "</td>");
					sb.append("<td>" + message.getContent() + "</td>");
					sb.append("</tr>");
				}
				sb.append("</table>");
			}

            private void news() {
                sb.append(new TableTag("news", "center"));
                sb.append("<tr><td align='center'>");
                sb.append(insertNews(config.getFilePath()));
                sb.append("</td></tr></table>");
            }

            @Override
			public String toString() {

				welcome();
                if (config.useNewsContent()) {
                    news();
                }
                if (config.isUseMessages()) {
                    messages();
                }

				return sb.toString();

			}

			private void welcome() {

				sb.append(new TableTag("welcome", "center"));
				sb.append("<tr><td align='center'>");
				sb.append("<h2>Welcome to the control panel home page</h2>");
				sb
						.append("Here you have the ability to set-up your web site.<br/><br/>"
								+ "You can create your categories, news bulletins, products, images and other items to populate your site.<br/><br/>"
								+ "In addition to the set-up of your site we have provided a set of tools to allow you to generate newsletters to your subscribers, <br/>"
								+ "send SMS text messages to mobile numbers, add in RSS newsfeeds, and view reports on who has visited your site .<br/><br/>To get started, click on the menu item above to go directly to the required section.");

				sb.append("</td></tr></table>");
			}

		});
		return doc;
	}

    public String insertNews(String filePath) {
        String content = null;
        try {
            content = SimpleFile.readString(filePath);
            if (content != null) {
                if (content.contains("<body>") && content.contains("</body>")) {
                    int beginIndex = content.indexOf("<body>");
                    int endIndex = content.indexOf("</body>");
                    content = content.substring(beginIndex + 6, endIndex);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

}