package org.sevensoft.ecreator.iface.admin.items.search;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.ItemsMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.search.ASelectionRenderer;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.adminsearch.AdminField;
import org.sevensoft.ecreator.model.items.adminsearch.AdminFieldType;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.ecreator.model.media.images.ImageSearchType;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.users.privileges.Privilege;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.html.AbstractTag;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

/**
 * @author sks 27-Feb-2006 16:28:46
 *
 */
@Path("admin-items-search.do")
public class ItemSearchHandler extends AdminHandler {

	private static final int			ResultsPerPage	= 50;

	private Category					category;
	private String					letter;
	private boolean					noCategory;
	private String					status;
	private SortType					sortType;
	private MultiValueMap<Attribute, String>	attributeValues;
	private List<Item>				items;
	private ItemType					itemType;
	private String					name;
	private int						distance;
	private Date					createdAfter;
	private transient int				total;
	private int						page;
	private transient Results			results;
	private String					email;
	private ImageSearchType				imageSearchType;
	private ItemSearcher				searcher;

	private String					reference;
    private String                  categoryName;
    private Item                    account;
    private Item                item;

    private AdminFieldType field;
    private String newValue;

    public ItemSearchHandler(RequestContext context) {
		super(context);
	}

	public ItemSearchHandler(RequestContext context, ItemType itemType) {
		super(context);
		this.itemType = itemType;
	}

	public ItemSearchHandler(RequestContext context, ItemSearcher searcher) {
		super(context);
		this.searcher = searcher;
	}

	protected Link getLink() {

		Link link = new Link(ItemSearchHandler.class, "search");
		link.setParameter("letter", letter);
		link.setParameter("itemType", itemType);
		link.setParameter("category", category);
		link.setParameter("noCategory", noCategory);
		link.setParameter("name", name);
		link.setParameter("sortType", sortType);
		link.setParameter("email", email);
		link.setParameter("status", status);
		link.setParameter("page", page);
		link.setParameter("createdAfter", createdAfter);
		link.setParameter("imageSearchType", imageSearchType);
		if (attributeValues != null) {
			link.setParameters("attributeValues", attributeValues);
		}

		return link;
	}

	private Object list() throws ServletException {

		if (itemType == null) {
			return index();
		}

		ItemSearcher searcher = new ItemSearcher(context);
		searcher.setLimit(20);
		searcher.setItemType(itemType);
		searcher.setVisibleOnly(false);
		searcher.setIncludeHidden(true);

		final List<Item> items = searcher.getItems();
		if (items.isEmpty()) {

			AdminDoc doc = new AdminDoc(context, user, "Choose a " + itemType.getName().toLowerCase() + " to edit", Tab.getItemTab(itemType));
			if (miscSettings.isAdvancedMode()) {
				doc.setMenu(new ItemsMenu(itemType));
			}
			doc.addBody(new Body() {

				@Override
				public String toString() {

					sb.append(new TableTag("simplelinks", "center"));
					sb.append("<tr><th align='center'>You have not created any " + itemType.getNamePluralLower() + " yet</th></tr>");

					if (Privilege.CreateItem.yes(user, context)) {

						sb.append("<tr><td align='center'>" +
								new LinkTag(ItemHandler.class, "create", "Click here to create a new " + itemType.getNameLower(), "itemType",
										itemType) + "</td></tr>");
					}

					sb.append("</table>");

					return sb.toString();
				}

			});

			return doc;
		}

		AdminDoc doc = new AdminDoc(context, user, "Choose a " + itemType.getName().toLowerCase() + " to edit", Tab.getItemTab(itemType));

		if (miscSettings.isAdvancedMode()) {
			doc.setMenu(new ItemsMenu(itemType));
		}

		doc.setIntro("Click on the " + itemType.getNameLower() + " to open that up for editing");
		doc.addBody(new Body() {

			private void items() {

				/*
				 * Decide which columns to show
				 */
				boolean salePriceCol;
				if (Module.Pricing.enabled(context) && ItemModule.Pricing.enabled(context, itemType)) {
					logger.fine("enabling sell price col");
					salePriceCol = true;
				} else {
					salePriceCol = false;
				}

				sb.append(new AdminTable(itemType.getNamePlural()));

				sb.append("<tr>");

				sb.append("<th width='80'>Id</th>");
				sb.append("<th width='140'>Date created</th>");
				sb.append("<th>Name</th>");

				if (miscSettings.isAdvancedMode()) {

					if (salePriceCol) {
						sb.append("<th>Sale price</th>");
					}

					sb.append("<th>Images</th>");
					sb.append("<th>Status</th>");

				}

				sb.append("</tr>");

				for (Item item : items) {

					sb.append("<tr>");

					// id
					sb.append("<td width='80'>" + new LinkTag(ItemHandler.class, "edit", item.getIdString(), "item", item) + "</td>");

					// date created
					sb.append("<td width='140'>" + item.getDateCreated().toDateString() + "</td>");

					// name
					sb.append("<td>" + new LinkTag(ItemHandler.class, "edit", item.getName(), "item", item));

					if (miscSettings.isAdvancedMode()) {
						if (ItemModule.Categories.enabled(context, itemType)) {
							sb.append("<br/>");
							sb.append(item.getCategoryNames());
						}
					}

					sb.append("</td>");

					if (miscSettings.isAdvancedMode()) {

						if (salePriceCol) {

							Money salePrice = item.getSellPrice();

							if (salePrice.isZero()) {

								sb.append("<td>n/a</td>");

							} else {

								sb.append("<td>" + item.getSellPrice() + "</td>");

							}
						}

						sb.append("<td>" + item.getApprovedImageCount() + "</td>");
						sb.append("<td>" + item.getStatus() + "</td>");
					}

					sb.append("</tr>");
				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				items();

				return sb.toString();
			}
		});
		return doc;
	}

	public Object main() throws ServletException {

		if (itemType == null) {
			return index();
		}

		/* 
		 * 17/10/06
		 * IF WE HAVE LESS THAN 20 ITEMS I JUST WANT TO DISPLAY THEM IN A LIST!
		 */

		final int count = SimpleQuery.count(context, Item.class, "itemType", itemType);

		if (count < 20) {
			return list();
		}

		AdminDoc doc = new AdminDoc(context, user, itemType.getName() + " search", Tab.getItemTab(itemType));

		if (miscSettings.isAdvancedMode()) {
			doc.setMenu(new ItemsMenu(itemType));
		}

		doc.setIntro("Here you can search for all the " + itemType.getNamePlural().toLowerCase() + " on your site");
		doc.addBody(new Body() {

			private void advanced() {

				sb.append(new FormTag(ItemSearchHandler.class, "search", "POST"));
				sb.append(new HiddenTag("itemType", itemType));

				sb.append(new AdminTable("Advanced search"));

				sb.append(new AdminRow("Name", "Enter the name or part of the name for the " + itemType.getNameLower() + " wish to search for.",
						new TextTag(context, "name", 40)));

				if (ItemModule.Account.enabled(context, itemType)) {
					sb.append(new AdminRow("Email", "Search by users login email.", new TextTag(context, "email", 40)));
				}

				if (itemType.isStatusControl()) {

					SelectTag tag = new SelectTag(context, "status", "LIVE");
					tag.setAny("-Any status-");
					for (String status : itemType.getStatuses()) {
						if (status != null) {
							tag.addOption(status.toUpperCase());
						}
					}

					sb.append(new AdminRow("Status", "Limit searches by items of a particular status.", tag));
				}

				if (ItemModule.Categories.enabled(context, itemType)) {

//					if (count > 500) {
//
//						sb.append(new AdminRow("Category", "Enter the name or part of name of a category that items should be in", new TextTag(
//								context, "categoryName", 40)));
//
//					} else {

						SelectTag tag = new SelectTag(context, "category");
						tag.setAny("-Any category-");
						final Map<String, String> categories = Category.getSimpleMap(context);
						tag.addOptions(categories);

						sb.append(new AdminRow("Category", "Limit search results to items only in this specified category.", tag));

					}
//				}

                SelectTag ownerAccountTag = new SelectTag(context, "account", null, Item.getAccountsForOptions(context), "-Select owner account-");
                sb.append(new AdminRow("Account owner", "This is the account that is assigned to this " + itemType.getNameLower(), ownerAccountTag));

                if (itemType.isReferences()) {

					sb.append(new AdminRow("Reference", "Enter the reference or part of the reference to search for.", new TextTag(context,
							"reference", 30)));

				}

				if (miscSettings.isAdvancedMode()) {

					if (ItemModule.Categories.enabled(context, itemType)) {

						sb.append(new AdminRow("Not in a category", "Only show " + itemType.getNamePluralLower() +
								" results to items that are not in any category.", new CheckTag(context, "noCategory", true, false)));

					}

					if (ItemModule.Images.enabled(context, itemType)) {

						sb.append(new AdminRow("Images", "Choose to include images only, no images, or any.", new SelectTag(context,
								"imageSearchType", null, ImageSearchType.values(), "Any")));

					}

					int n = 0;
					for (Attribute attribute : itemType.getAttributes()) {

						sb.append(new AdminRow(attribute.getName(), attribute.getDescription(), new ASelectionRenderer(context, attribute)));

						n++;
						if (n == 10) {
							break;
						}
					}
				}

				sb.append("<tr><td class='command' colspan='2'>" + new SubmitTag("Search for " + itemType.getNamePlural().toLowerCase()) +
						new ButtonTag(ItemHandler.class, "create", "Create new " + itemType.getNamePlural().toLowerCase(), "itemType", itemType) +
						"</td></tr>");

				sb.append("</table>");
				sb.append("</form>");
			}

			private void quick() {

				sb.append(new AdminTable("Quick search"));

				sb.append("<tr><td>");

				for (char c = '0'; c < 91; c++) {

					if (c == ':') {
						c = 'A';
					}

					sb.append(new LinkTag(ItemSearchHandler.class, "search", c, "letter", c, "itemType", itemType, "status", "Live"));
					sb.append(" ");
				}

				sb.append(new LinkTag(ItemSearchHandler.class, "search", "Show all", "itemType", itemType));

				sb.append("</td></tr>");
				sb.append("</table>");
			}

			private void results() {

				sb.append("<div class='pagination'>" + new ResultsControl(context, results, getLink()) + "</div>");

				sb.append(new AdminTable("Search results"));

				// HEADERS
				sb.append("<tr>");
				final List<AdminField> adminFields = itemType.getAdminFields();
				for (AdminField field : adminFields) {

					SortType sortType = field.getFieldType().getSortType();

					if (sortType == null) {

						sb.append("<th>" + field.getName() + "</th>");

					} else {

						Link link = getLink();
						link.setParameter("sortType", sortType);

						LinkTag linkTag = new LinkTag(link, field.getName());

						sb.append("<th>" + linkTag + "</th>");
					}
				}

				if (config.getUrl().contains("shoppingbox")) {
					if (itemType.getId() == 1) {
						sb.append("<td>Suite</td>");
						sb.append("<td>Account</td>");
					}
				}

				sb.append("<th width='10'> </th>");
				sb.append("</tr>");

				// ITEMS
                int count = 0;
				for (Item item : items) {

					sb.append("<tr>");
					for (AdminField field : adminFields) {

                        Attribute attribute = field.getAttribute();
                        String value = field.getFieldType().getText(item, attribute);
                        AbstractTag tag = field.getFieldType().getTag(context,value);
                        if (field.isSearchResultsEditable() && tag != null) {
                            Link link = new Link(ItemSearchHandler.class, "save", "field", field.getFieldType());
                            String id = tag.getName() + String.valueOf(count);

                            sb.append("<td>" + tag.setId(id).
                                    setOnChange("ajaxRequest('" + link + "&newValue='+this.value+'&item=" + item.getId() + "','" + id + "'); window.alert('The item has been updated');") + "</td>");
                        } else {
                            sb.append("<td>" + value + "</td>");
                        }
                        count++;
					}

					if (config.getUrl().contains("shoppingbox")) {

						if (itemType.getId() == 1) {

							String suite = item.getAttributeValue(15);
							if (suite == null) {
								suite = "";
							}

							sb.append("<td>" + suite + "</td>");

							Item account = item.getAccount();
							if (account == null) {
								sb.append("<td> </td>");
							} else {
								sb.append("<td>" + account.getName() + "</td>");
							}
						}
					}

					sb.append("<td width='10'>" +
							new LinkTag(ItemHandler.class, "delete", new DeleteGif(), "item", item)
									.setConfirmation("Are you sure you want to delete this " + item.getItemTypeNameLower()) + "</td>");
					sb.append("</tr>");
				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				quick();
				advanced();
				if (items != null && items.size() > 0)
					results();

				return sb.toString();
			}
		});
		return doc;

	}

	public Object search() throws ServletException {

		if (itemType == null) {
			return main();
		}

        if (category == null && categoryName != null) {
            category = Category.getByName(context, categoryName);
        }

        ItemSearcher searcher = new ItemSearcher(context);
		searcher.setAttributeValues(attributeValues);
		searcher.setCreatedAfter(createdAfter);
		searcher.setDistance(distance);
		searcher.setName(name);
		searcher.setStatus(status);
		searcher.setItemType(itemType);
		searcher.setNoCategory(noCategory);
		searcher.setSearchCategory(category);
		searcher.setStartsWith(letter);
		searcher.setSortType(sortType);
		searcher.setEmail(email);
		searcher.setReference(reference);
		searcher.setIncludeHidden(true);
		searcher.setVisibleOnly(false);
		searcher.setImageSearchType(imageSearchType);
        searcher.setSearchAccount(account);

        total = searcher.size();

		results = new Results(total, page, ItemSearchHandler.ResultsPerPage);

		searcher.setStart(results.getStartIndex());
		searcher.setLimit(ItemSearchHandler.ResultsPerPage);

		items = searcher.getItems();

		return main();
	}

    public void save() throws ServletException {
        if (item == null || field == null) {
            return;
        }
        switch (field) {
            case SellPrice:
                item.setStandardPrice(Amount.parse(newValue));
                break;
            case CostPrice:
                item.setOurCostPrice(new Money(newValue));
                break;
            case Summary:
                item.setSummary(newValue);
                break;
            case Status:
                item.setStatus(newValue);
                break;
//            case Reference:
//                item.setReference(newValue);
//                break;
            case Stock:
                item.setOurStock(Integer.valueOf(newValue));
            default:
                break;
        }
        item.save();
    }
}
