package org.sevensoft.ecreator.iface.admin.items;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.validators.DateValidator;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.number.IntValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.comments.CommentHandler;
import org.sevensoft.ecreator.iface.admin.comments.AddCommentsPanel;
import org.sevensoft.ecreator.iface.admin.attachments.AdminAttachmentsPanel;
import org.sevensoft.ecreator.iface.admin.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.admin.categories.menu.CategoryMenu;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemMenu;
import org.sevensoft.ecreator.iface.admin.items.menus.ItemsMenu;
import org.sevensoft.ecreator.iface.admin.items.modules.accessories.AccessoryHandler;
import org.sevensoft.ecreator.iface.admin.items.modules.options.ItemOptionsHandler;
import org.sevensoft.ecreator.iface.admin.items.search.ItemSearchHandler;
import org.sevensoft.ecreator.iface.admin.items.listings.ListingPackageHandler;
import org.sevensoft.ecreator.iface.admin.media.images.panels.ImageAdminPanel;
import org.sevensoft.ecreator.iface.admin.misc.content.panels.KeywordDensityPanel;
import org.sevensoft.ecreator.iface.admin.misc.seo.panels.MetaTagsPanel;
import org.sevensoft.ecreator.iface.admin.misc.seo.panels.SeoFriendlyUrlsRenderer;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.frontend.ErrorDoc;
import org.sevensoft.ecreator.iface.frontend.extras.rss.RssExportHandler;
import org.sevensoft.ecreator.model.accounts.permissions.AccessGroup;
import org.sevensoft.ecreator.model.accounts.subscriptions.Subscription;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.AttachmentUtil;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeValue;
import org.sevensoft.ecreator.model.attributes.renderers.AAdminRenderer;
import org.sevensoft.ecreator.model.attributes.renderers.ARenderType;
import org.sevensoft.ecreator.model.attributes.test.AttributeValidator;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategoryItem;
import org.sevensoft.ecreator.model.crm.messages.panels.MessagesPanel;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.ecom.branches.Branch;
import org.sevensoft.ecreator.model.ecom.credits.CreditStore;
import org.sevensoft.ecreator.model.ecom.delivery.Delivery;
import org.sevensoft.ecreator.model.ecom.delivery.DeliveryOption;
import org.sevensoft.ecreator.model.ecom.delivery.DeliverySettings;
import org.sevensoft.ecreator.model.ecom.suppliers.Supplier;
import org.sevensoft.ecreator.model.extras.shoppingbox.ShoppingBox;
import org.sevensoft.ecreator.model.extras.shoppingbox.ShoppingBoxDao;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.renderers.OwnerOptionsPanel;
import org.sevensoft.ecreator.model.items.listings.Listing;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.OptionGroup;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.ecreator.model.items.pricing.*;
import org.sevensoft.ecreator.model.items.pricing.costs.SupplierCostPrice;
import org.sevensoft.ecreator.model.items.pricing.panels.EditPricesRenderer;
import org.sevensoft.ecreator.model.items.stock.StockModule;
import org.sevensoft.ecreator.model.items.stock.SupplierStock;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.ImageUtil;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.media.videos.VideoUtil;
import org.sevensoft.ecreator.model.media.videos.panels.VideoAdminPanel;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.ecreator.model.search.alerts.SearchAlert;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.model.system.users.privileges.Privilege;
import org.sevensoft.ecreator.model.comments.Comment;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.*;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.date.DateTextTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.PasswordTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

import javax.servlet.ServletException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.io.IOException;

/**
 * @author sam
 */
@Path("admin-items.do")
public class ItemHandler extends AdminHandler {

    protected static final int CategoryListLimit = 300;
    private List<Upload> files, imageUploads;
    private boolean featured;
    private Img image;
    private String imageFilename;
    private ItemOption option;
    private String keywords, descriptionTag, titleTag, content, name;
    private Attachment file;
    private String names;
    private List<String> imageUrls;
    private Category category;
    private String status;
    private MultiValueMap<Attribute, String> attributeValues;
    private String newOptions;
    private ItemType itemType;
    private Money ourCostPrice;
    private Money rrp;
    private double vatRate;

    private Item accessory;
    private int orderQtyMax;
    private int orderQtyMin;

    private String outStockMsg;
    private int ourStock;

    private String summary;

    private boolean brochure;
    private Map<Integer, String> attachmentDescriptions;

    private boolean booleanStock;
    private boolean backorders;

    private String changeMember;
    private int bidCredits;
    private int auctionRequiredBids;
    private int auctionLength;

    private Money promotionPrice;

    private int maxBidsPerMember;
    private Money bidFee;
    private Money maxBid;
    private Money minBid;
    private int bidsRequired;

    private Supplier supplier;

    private Category addCategory;

    private int bidBonusCredits;
    private String friendlyUrl;

    private Money deliveryCharge;
    private Markup optionsMarkup;
    private int minimumOptionSelection;

    private final transient DeliverySettings deliverySettings;

    private final transient MiscSettings miscSettings;
    private boolean makePreviews;
    private Item item;
    private MultiValueMap<Integer, String> priceMap;
    private String priceBreaksString;
    private int priceRows;
    private Map<Integer, PriceBand> memberGroupMap;
    private Map<Integer, Money> costPriceFromMap;
    private Map<Integer, Money> costPriceToMap;

    private String shortName;
    private Amount sellPrice;

    private int page;

    private final transient Seo seo;
    private Money deliverySurcharge;
    private String reference;
    private OptionGroup optionGroup;
    private String messageBody;
    private User owner;
    private int stockNotifyLevel;
    private boolean prioritised;
    private Date subscriptionExpiryDate;
    private SubscriptionLevel subscriptionLevel;
    private boolean lifetimeSubscription;
    private String em;
    private String pwd;
    private int creditsRewarded;
    private int creditsRequired;
    private final transient PriceSettings priceSettings;
    private Money costPrice;

    private Map<Supplier, Money> costPrices;

    private String priceBreaks;

    private Map<Integer, String> prices;

    private Supplier addSupplier;
    private int credits;
    private List<Attribute> removeAttributeValues;
    private Map<DeliveryOption, Money> deliveryRates;
    private List<Upload> videoUploads;
    private Map<Supplier, Integer> stocks;
    private Item replacement;
    private int unit;
    private AccessGroup accessGroup;
    private AccessGroup addAccessGroup;
    private Item account;
    private Date expiryDate;
    private Money collectionCharge;
    private Branch branch;
    private int weight;
    private String suite;
    private PriceBand priceBand;
    private boolean excludeFromGoogleFeed;
    private ListingPackage listingPackage;
    private String mobilePhone;
    private String comment;
    private String title;
    private boolean comments;
    private Amount discount;
    private boolean discountApplied;
    private ItemOption addPresetOption;
    private boolean showOptionsList;

    public ItemHandler(RequestContext context) {
        this(context, null);
    }

    public ItemHandler(RequestContext context, Item item) {
        super(context);
        this.item = item;

        setAttribute("view", new Link(org.sevensoft.ecreator.iface.frontend.items.ItemHandler.class, null, "item", item));

        this.deliverySettings = DeliverySettings.getInstance(context);
        this.miscSettings = MiscSettings.getInstance(context);
        this.seo = Seo.getInstance(context);
        this.priceSettings = PriceSettings.getInstance(context);
    }

    public Object copy() throws ServletException {

        if (item == null) {
            return main();
        }

        try {

            final Item copy = item.clone();
            item.log(user, "Copied");

            clearParameters();

            AdminDoc page = new AdminDoc(context, user, "item" + " copied", getTab());
            page.addBody(new Body() {

                @Override
                public String toString() {

                    sb.append("<div align='center' class='confirmation'>");
                    sb.append("<h2>'" + item.getName() + "' has been copied.</h2>");
                    sb.append(new LinkTag(ItemHandler.class, "edit", "Click here to edit the original", "item", item));
                    sb.append("<br/><br/>");
                    sb.append(new LinkTag(ItemHandler.class, "edit", "Click here to edit the new copy", "item", copy));
                    sb.append("</div>");

                    return sb.toString();
                }
            });
            return page;

        } catch (CloneNotSupportedException e) {
            throw new ServletException(e);

        }
    }

    public Object create() throws ServletException {

        if (itemType == null) {
            return index();
        }

        AdminDoc doc = new AdminDoc(context, user, "Create " + itemType.getNameLower(), Tab.getItemTab(itemType));

        final boolean useFilter = SimpleQuery.count(context, Category.class) > 600;
        final int count;
        if (itemType.isCategories()) {

            if (name == null) {
                count = Category.getCount(context);
            } else {
                count = new Query(context, "select count(*) from # where name like ?").setTable(Category.class).setParameter(name).getInt();
            }

        } else {

            count = 0;
        }

        // results should be based on count for this name
        final Results results = new Results(count, page, CategoryListLimit);
        final Map<String, String> categoryOptions = Category.getCategoryOptions(context, name, results.getStartIndex(), CategoryListLimit);

        if (miscSettings.isAdvancedMode()) {
            doc.setMenu(new ItemsMenu(itemType));
        }
        String intro = "Create a new " + itemType.getNameLower();
        if (itemType.isCategories()) {
            intro = intro + " and choose its initial location";
        }
        doc.setIntro(intro);
        doc.addBody(new Body() {

            @Override
            public String toString() {

                if (itemType.isCategories()) {

                    if (useFilter) {

                        sb.append(new FormTag(ItemHandler.class, "create", "GET"));
                        sb.append(new HiddenTag("itemType", itemType));
                        sb.append("<div align='center'>Filter category by name: " + new TextTag(context, "name", 20) + " " +
                                new SubmitTag("Update") + "</div><br/>");
                        sb.append("</form>");

                    }
                }

                sb.append(new FormTag(ItemHandler.class, "doCreate", "POST"));
                sb.append(new HiddenTag("itemType", itemType));

                sb.append(new TableTag("simplebox", "center"));

                sb.append("<tr><td align='center'>Enter the " + itemType.getNameCaption().toLowerCase() + " of the " + itemType.getNameLower() +
                        " to create.</td></tr>");

                sb.append("<tr><td align='center'>");

                if (miscSettings.isAdvancedMode()) {
                    sb.append(new TextAreaTag(context, "names", 40, 4));
                } else {
                    sb.append(new TextTag(context, "names", 40));
                }

                sb.append(new ErrorTag(context, "names", "<br/>"));

                sb.append("<br/><br/></td></tr>");

                if (itemType.isCategories()) {

                    if (results.hasMultiplePages()) {
                        sb
                                .append("<div align='center'>" +
                                        new ResultsControl(context, results, new Link(ItemHandler.class, "create", "itemType", itemType,
                                                "name", name)) + "</div>");
                    }

                    sb.append("<tr><td align='center'>Where do you want these " + itemType.getNamePluralLower() + " to appear?</td></tr>");
                    sb.append("<tr><td align='center'>" + new SelectTag(context, "category", null, categoryOptions, "-I'll decide later") +
                            "</td></tr>");
                }

                sb.append("</table>");

                sb.append("<div align='center' class='actions'>" + new SubmitTag("Create " + itemType.getNameLower()) + "</div>");

                sb.append("</form>");
                return sb.toString();

            }

        });
        return doc;
    }

    public Object delete() throws ServletException {

        if (item == null) {
            return main();
        }

        itemType = item.getItemType();
        item.log(user, "Deleted");
        item.delete();

        AdminDoc doc = new AdminDoc(context, user, "This " + itemType.getNameLower() + " has been deleted", getTab());

        if (miscSettings.isAdvancedMode()) {
            doc.setMenu(new ItemsMenu(itemType));
        }

        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new TableTag("simplelinks", "center"));
                sb.append("<tr><th align='center'>What would you like to do ?</th></tr>");

                if (Privilege.EditAllItems.yes(user, context) || Privilege.EditOwnerItems.yes(user, context) ||
                        Privilege.EditCreatedItems.yes(user, context)) {

                    sb
                            .append("<tr><td align='center'>" +
                                    new LinkTag(ItemSearchHandler.class, null, "I want to edit another " + itemType.getNameLower(),
                                            "itemType", itemType) + "</td></tr>");
                }

                if (Privilege.CreateItem.yes(user, context)) {

                    sb.append("<tr><td align='center'>" +
                            new LinkTag(ItemHandler.class, "create", "I want to create a new " + itemType.getNameLower(), "itemType", itemType) +
                            "</td></tr>");
                }

                sb.append("</table>");

                return sb.toString();
            }
        });
        return doc;
    }

    public Object doCreate() throws ServletException {

        test(new RequiredValidator(), "names");
        if (hasErrors()) {
            return create();
        }

        if (itemType == null) {
            return index();
        }

        for (String name : StringHelper.explodeStrings(names, "\n")) {

            if (miscSettings.isDuplicationCheck() && Item.isNameExisting(context, name)) {

                addError("Item: " + name + " already exists.");

            } else {

                item = new Item(context, itemType, name, "Live", user);
                item.log(user, "Created");

                if (category != null) {
                    item.addCategory(category);
                }
            }
        }

        if (item == null) {
            return main();
        }

        if (miscSettings.isAdvancedMode()) {
            return edit();
        }

        AdminDoc doc = new AdminDoc(context, user, "This " + item.getItemType().getNameLower() + " has been created", getTab());
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new TableTag("simplelinks", "center"));
                sb.append("<tr><th align='center'>What would you like to do ?</th></tr>");

                if (Privilege.EditAllItems.yes(user, context) || Privilege.EditOwnerItems.yes(user, context) ||
                        Privilege.EditCreatedItems.yes(user, context)) {

                    sb
                            .append("<tr><td align='center'>" +
                                    new LinkTag(ItemHandler.class, "edit", "I want to edit this " + itemType.getNameLower(), "item", item) +
                                    "</td></tr>");

                }

                if (Privilege.CreateItem.yes(user, context)) {

                    sb.append("<tr><td align='center'>" +
                            new LinkTag(ItemHandler.class, "create", "I want to create another " + item.getItemType().getNameLower(), "itemType",
                                    itemType) + "</td></tr>");
                }

                sb.append("</table>");

                return sb.toString();
            }
        });
        return doc;
    }

    public Object edit() throws ServletException {

        if (item == null) {
            return main();
        }

        boolean access = false;
        if (Privilege.EditAllItems.yes(user, context)) {
            access = true;
        }

        if (!access && user.equals(item.getCreatedBy())) {
            if (Privilege.EditCreatedItems.yes(user, context))
                access = true;
        }

        if (!access && user.equals(item.getOwner())) {
            if (Privilege.EditOwnerItems.yes(user, context))
                access = true;
        }

        if (!access) {
            logger.fine("[ItemHandler] access denied");
            return index();
        }

        setAttribute("view", getView());

        AdminDoc doc = new AdminDoc(context, user, "Edit " + item.getItemType().getName().toLowerCase() + " #" + item.getId() + " - '" + item.getName() +
                "'", getTab());
        doc.setIntro("This " + item.getItemType().getNameLower() + " was created on " + item.getDateCreated().toString("dd MMM yyyy HH:mm"));
        if (miscSettings.isAdvancedMode()) {
            doc.setMenu(new EditItemMenu(item));
        }

        itemType = item.getItemType();
        final StockModule module = item.getItemType().getStockModule();

        doc.addBody(new Body() {

            private void accessGroups() {

                List<AccessGroup> accessGroups = AccessGroup.get(context);
                if (accessGroups.isEmpty()) {
                    return;
                }

                sb.append(new AdminTable("Access groups"));

                sb.append("<tr>");
                sb.append("<th>Access group</th>");
                sb.append("<th width='10'></th>");
                sb.append("</tr>");

                for (AccessGroup accessGroup : item.getAccessGroups()) {

                    sb.append("<tr>");
                    sb.append("<td>" + accessGroup.getName() + "</td>");
                    sb.append("<td width='10'>" +
                            new LinkTag(ItemHandler.class, "removeAccessGroup", new DeleteGif(), "item", item, "accessGroup", accessGroup)
                                    .setConfirmation("Are you sure you want to delete this access group?") + "</td>");
                    sb.append("</tr>");
                }

                sb.append("<tr><td colspan='2'>Add access group: " +
                        new SelectTag(context, "addAccessGroup", null, accessGroups, "-Select access group-") + "</td></tr>");

                sb.append("</table>");

            }

            private void comments() {

                sb.append(new AdminTable("Comments"));
                sb.append(new AdminRow("Add comments", "Allow to leave comments to the item",
                        new BooleanRadioTag(context, "comments", item.isComments())));
            }

            private void accessories() {

                sb.append(new AdminTable("Accessories"));

                for (Item accessory : item.getAccessories()) {

                    sb.append("<tr>");
                    sb.append("<td>" + new LinkTag(ItemHandler.class, "edit", accessory.getName(), "item", accessory) + "</td>");
                    sb.append("<td width='10'>" +
                            new LinkTag(AccessoryHandler.class, "remove", new DeleteGif(), "item", item, "accessory", accessory) + "</td>");
                    sb.append("</tr>");

                }

                sb.append("<tr><td colspan='2'>Search for accessory " +
                        new TextTag(context, "name", 20).setId("accessorySearchText") +
                        " " +
                        new ButtonTag("Search").setOnClick("window.location='admin-items-accessories.do?item=" + item.getId() +
                                "&name=' + document.getElementById('accessorySearchText').value;") + "</td></tr>");

                sb.append("</table>");
            }

            private void backorders() {
                sb.append(new AdminRow("Backorders", "Allow customers to order when out of stock (ie, place a back order).", new BooleanRadioTag(
                        context, "backorders", item.isBackorders())));
            }

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                String lowerName = item.getItemType().getName().toLowerCase();

                sb.append(new SubmitTag("Update " + lowerName));

                if (miscSettings.isAdvancedMode()) {
                    sb.append(new ButtonTag(ItemHandler.class, "copy", "Copy " + lowerName, "item", item));
                }

                sb.append(new ButtonTag(ItemHandler.class, "delete", "Delete " + lowerName, "item", item)
                        .setConfirmation("Are you sure you want to delete this " + lowerName + "?"));

                if (Module.SearchAlerts.enabled(context)) {
                    if (SearchAlert.hasAlerts(context)) {
                        sb.append(new ButtonTag(SearchAlertHandler.class, "run", "Run alerts", "item", item));
                    }
                }

                sb.append("</div>");
            }

            private void content() {

                sb.append(new AdminTable("Content"));

                sb.append("<tr><td colspan='2'>" +
                        new TextAreaTag(context, "content", item.getContent()).setId("content").setStyle("width: 100%; height: 320px;") +
                        "</td></tr>");

                if (item.getItemType().isSummaries()) {

                    sb.append(new AdminRow("Summary", "Enter your own summary to override the automatically generated summary.", new TextAreaTag(
                            context, "summary", item.getSummary(), 50, 4)));
                }

                if (item.hasContent() && seo.isKeywordDensity()) {
                    sb.append(new KeywordDensityPanel(context, item.getContent()));
                }

                sb.append("</table>");
            }

            private void creditStore() {

                sb.append(new AdminTable(captions.getCreditsCaptionPlural()));

                sb.append(new AdminRow("Current " + captions.getCreditsCaptionPlural().toLowerCase(), "Current number of " +
                        captions.getCreditsCaptionPlural() + " in this account.", new TextTag(context, "credits", item.getCreditStore()
                        .getCredits(), 4)));

                sb.append("</table>");
            }

            private void deliveryRates() {

                Delivery delivery = item.getDelivery();
                Map<DeliveryOption, Money> deliveryRates = delivery.getDeliveryRatesMap();
                final List<DeliveryOption> itemBased = DeliveryOption.getItemBased(context);

                if (itemBased.isEmpty() && !deliverySettings.isSurcharges()) {
                    return;
                }

                sb.append(new AdminTable("Delivery"));

                if (deliverySettings.isSurcharges()) {

                    sb
                            .append(new AdminRow(
                                    "Delivery surcharge",
                                    "Set a delivery surcharge on this item. This will apply in addition to any delivery options setup in delivery settings.",
                                    new TextTag(context, "deliverySurcharge", delivery.getDeliverySurcharge(), 12)));

                }

                for (DeliveryOption option : itemBased) {

                    Money chargeEx = deliveryRates.get(option);
                    String param = "deliveryRates_" + option.getId();
                    sb.append(new AdminRow(option.getName(), null, new TextTag(context, param, chargeEx, 8)));
                }

                sb.append("</table>");
            }

            private void general() {

                sb.append(new AdminTable("General"));

                {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(new TextTag(context, "name", item.getName(), 60));
                    if (isSuperman()) {
                        sb2.append(" x=" + item.getX() + ", y=" + item.getY());
                    }

                    sb
                            .append(new AdminRow(item.getItemType().getNameCaption(), "Enter the name or title of your " +
                                    item.getItemTypeNameLower(), sb2));
                }

                if (ItemModule.Account.enabled(context, item)) {

                    sb.append(new AdminRow("Email", "This is the login email used by this account", new TextTag(context, "em", item.getEmail(), 40)));

                    if (item.isMobilePnoneEnabled()) {
                        sb.append(new AdminRow("Mobile Phone", "This is the mobile phone of the account owner", new TextTag(context, "mobilePhone", item.getMobilePhone(), 40)));
                    }

                    sb.append(new AdminRow("Change password", "Enter a new password here if you want to change the account's password. "
                            + "For security reasons you cannot retrieve the current password.", new PasswordTag(context, "pwd", 16)));

                    if (item.getItemType().getAccountModule().isShowLastAccessTime()) {
                        sb.append(new AdminRow("Last access time", "This is the last time the user was active on the site.", item
                                .getLastActiveTime().toString("HH:mm dd-MMM-yyyy")));
                    }

                }

                if (item.getItemType().isShortNames()) {

                    sb.append(new AdminRow("Short name", "A shortened version of the item name for use when the space is limited.", new TextTag(
                            context, "shortName", item.getShortName(), 40)));
                }

                if (item.getItemType().isOwners()) {

                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(new SelectTag(context, "owner", item.getOwner(), User.getActive(context), "-No owner-"));
                    if (item.hasOwner()) {
                        sb2.append(" ");
                        sb2.append(new ButtonTag(ItemHandler.class, "emailOwner", "Email owner", "item", item));
                    }

                    sb.append(new AdminRow("Owner", "This is which user this item is assigned to.", sb2));

                }

                if (Module.Branches.enabled(context)) {

                    if (ItemModule.Branches.enabled(context, item)) {

                        sb.append(new AdminRow("Branch", new SelectTag(context, "branch", item.getBranch(), Branch.get(context), "None")));

                    }
                }

                if (item.getItemType().isReferences()) {

                    sb.append(new AdminRow(itemType.getReferencesCaption(),
                            "This is a reference assigned by you, or by your offline / external database.", new TextTag(context, "reference",
                            item.getReference(), 40)));

                }

                if (item.getItemType().isStatusControl()) {

                    SelectTag tag = new SelectTag(context, "status", item.getStatus());
                    tag.addOption("LIVE");
                    tag.addOption("DISABLED");
                    tag.addOption("DELETED");

                    sb.append(new AdminRow("Status", "Set the status of this " + item.getItemType().getNameLower() + ".", tag));

                }

                if (item.getItemType().isCategories()) {

                    SelectTag tag;
                    int n = 0;
                    for (Category category : item.getCategories()) {

                        StringBuilder sb2 = new StringBuilder();

                        if (category.isRoot()) {

                            sb2.append(new LinkTag(CategoryHandler.class, "edit", category.getName(), "category", category));

                        } else {

                            Iterator<Category> iter = category.getTrail().iterator();
                            while (iter.hasNext()) {

                                Category trail = iter.next();

                                sb2.append(new LinkTag(CategoryHandler.class, "edit", trail.getName(), "category", trail));

                                if (iter.hasNext()) {
                                    sb2.append(" > ");
                                }
                            }
                        }

                        sb2.append(" ");
                        sb2.append(new ButtonTag(ItemHandler.class, "removeCategory", "Remove", "item", item, "category", category));
                        sb2.append("</td></tr>");

                        sb.append(new AdminRow("Category " + n, null, sb2));
                        n++;
                    }

                    int count = Category.getCount(context);
                    final Results results = new Results(count, page, 500);
                    final Map<String, String> categoryOptions = Category.getCategoryOptions(context, null, results.getStartIndex(), 500);

                    Link link = new Link(ItemHandler.class, "edit", "item", item);

                    tag = new SelectTag(context, "addCategory");
                    tag.setAny("-Add to a new category-");
                    tag.addOptions(categoryOptions);

                    StringBuilder sb2 = new StringBuilder();
                    if (results.hasMultiplePages()) {
                        sb2.append(new ResultsControl(context, results, link));
                    }

                    sb2.append(tag);

                    if (n < item.getItemType().getMaxCategories() || item.getItemType().getMaxCategories() == 0) {
                        sb.append(new AdminRow("Category " + n, "Choose a category to add", sb2));
                    }
                }

                if (item.getItemType().isFeatured()) {

                    sb.append(new AdminRow(itemType.getFeaturedCaption(), null, new BooleanRadioTag(context, "featured", item.isFeatured())));
                }

                if (item.getItemType().isPrioritised()) {

                    sb.append(new AdminRow("Prioritised", null, new BooleanRadioTag(context, "prioritised", item.isPrioritised())));
                }

                if (ItemModule.MyShoppingBox.enabled(context, item)) {

                    TextTag tag = new TextTag(context, "suite", null, 16);

                    if (item.hasAccount()) {
                        tag.setValue(item.getAccount().getAttributeValue(15));
                    }

                    Object obj = tag;
                    if (item.hasAccount()) {
                        obj = obj + " " + new LinkTag(ItemHandler.class, "edit", item.getAccount().getName(), "item", item.getAccount());
                    }
                    sb.append(new AdminRow("Account owner", "This is the account that this item is assigned to", obj));

                    ShoppingBox box = ShoppingBoxDao.get(context, item);
                    if (box != null) {
                        sb.append(new AdminRow("Weight", null, new TextTag(context, "weight", box.getWeight(), 8)));
                    }
                }

                if (ItemModule.Account.enabled(context, item)) {

                    PriceSettings priceSettings = PriceSettings.getInstance(context);
                    if (priceSettings.isUsePriceBands()) {

                        List<PriceBand> bands = PriceBand.get(context);
                        if (bands.size() > 0) {

                            sb.append(new AdminRow("Price band", null, new SelectTag(context, "priceBand", item.getPriceBand(), bands,
                                    "Standard pricing")));
                        }
                    }
                }

                sb.append("</table>");
            }

            private void listing() {

                sb.append(new AdminTable("Listing"));

                final Listing listing = item.getListing();

//                sb.append(new AdminRow("Account owner", "This is the account that is assigned to this " + item.getItemType().getNameLower(),
//                        new ItemTag(context, "account", item.getAccount(), 40)));

                SelectTag ownerAccountTag = new SelectTag(context, "account", item.getAccount(), Item.getAccountsForOptions(context), "-No owner account-");
                sb.append(new AdminRow("Account owner", "This is the account that is assigned to this " + item.getItemType().getNameLower(), ownerAccountTag));

                List<ListingPackage> listingsPackages = ListingPackage.get(context);
                ListingPackage lPackage = item.getListing().getListingPackage();

                SelectTag listingPackageTag = new SelectTag(context, "listingPackage", lPackage, listingsPackages, "-No listing package selected-");

                if (!listingsPackages.contains(lPackage)) {
                    LinkTag deletedPackage = new LinkTag(ListingPackageHandler.class, "edit", "Edit Deleted Liting Package", "listingPackage", lPackage);
                    sb.append(new AdminRow("Listing package", "This is the listing package that is assigned to this " + item.getItemType().getNameLower(), listingPackageTag.toString() + deletedPackage));
                } else {
                    sb.append(new AdminRow("Listing package", "This is the listing package that is assigned to this " + item.getItemType().getNameLower(), listingPackageTag));
                }

                DateTextTag expiryDateTag = new DateTextTag(context, "expiryDate", listing.getExpiryDate());
                sb.append(new AdminRow("Listing expiry date", "Set to blank if you want this listing to not expire.", expiryDateTag));

                if (ItemModule.SocialNetworkFeed.enabled(context, item.getItemType())) {
                    sb.append(new AdminRow("RSS link", "Use this link to import the news of this user to Social Network: Twitter, Facebook, ect",
                            new LinkTag(config.getUrl() + "/" + new Link(RssExportHandler.class, "accountFeed", "account", item.getAccount()))));

                    sb.append(new AdminRow("Full RSS", "Use this link to import the news of this user to Social Network: Twitter, Facebook, ect",
                            new LinkTag(config.getUrl() + "/" + new Link(RssExportHandler.class, "accountFeed", "all", true))));

                }

                sb.append("</table>");
            }

            private void ordering() {

                sb.append(new AdminTable("Ordering"));

                sb.append(new AdminRow("Orderable / Brochure", "Set this item to either orderable or brochure only.", new BooleanRadioTag(context,
                        "brochure", item.isBrochure()).setTitles("Brochure", "Orderable")));

                sb.append(new AdminRow("Order quantities", "Set a minimum and maximum order quantity for this item.", "Min" +
                        new TextTag(context, "orderQtyMin", item.getOrderQtyMin(), 4) + " Max " +
                        new TextTag(context, "orderQtyMax", item.getOrderQtyMax(), 4)));

                sb.append("</table>");
            }

            private void outStockMsg() {

                if (module.hasOutStockMsgOverride()) {
                    return;
                }

                sb.append(new AdminRow("Out of stock message",
                        "The message showed to customers when out of stock, eg, <i>'Back in stock - 2 weeks'</i>.", new TextTag(context,
                        "outStockMsg", item.getOutStockMsg(), 40)));
            }

            private void discountApplied() {

                sb.append(new AdminTable("Discount"));
                sb.append(new AdminRow("Discount applied", "Cagegory or Item Discount has already been applied to the prices of the item",
                        new BooleanRadioTag(context, "discountApplied", item.isDiscountApplied())));
                sb.append("</table>");

            }

            private void pricing() {

                boolean adv = PriceUtil.isAdvancedPricing(item, context);
                PriceSettings priceSettings = PriceSettings.getInstance(context);
                Pricing pricing = item.getPricing();

                sb.append(new AdminTable("Pricing"));

                if (!adv) {

                    final Price price = item.getStandardPrice();
                    String priceString = PriceUtil.getPriceString(price);
                    sb.append(new AdminRow("Price", "Enter the price you sell this item for.", new TextTag(context, "sellPrice", priceString, 12)));
                }

                if (item.getItemType().isRrp()) {
                    sb.append(new AdminRow(item.getItemType().getRrpCaption(),
                            "Set a recommended retail price and the site will display your discount.", new TextTag(context, "rrp", item.getRrp()
                            .toEditString(), 12)));
                }

                if (item.getItemType().isVat()) {
                    sb.append(new AdminRow("Vat rate", "Set the current vat rate.", new TextTag(context, "vatRate", item.getVatRate(), 8)));
                }

                if (priceSettings.isCostPricing()) {
                    sb.append(new AdminRow("Cost price", "Sets your cost price for this item.", new TextTag(context, "ourCostPrice", item
                            .getCostPrice(), 8)));
                }

                if (priceSettings.isCollectionCharges()) {

                    if (config.getUrl().contains("blacknround")) {

                        sb.append(new AdminRow("Fitting charge", "Enter a cost to be added when this item is fitted.", new TextTag(context,
                                "collectionCharge", pricing.getCollectionCharge(), 12)));

                    } else {

                        sb.append(new AdminRow("Collection charges", "Enter a cost to be added when this item is collected.", new TextTag(context,
                                "collectionCharge", pricing.getCollectionCharge(), 12)));
                    }
                }

                if (Module.CategoryAndItemDiscount.enabled(context)){
                				sb.append(new AdminRow("Item discount", "Enter a discount here and it will be applied to the product with the first priority",
                                new TextTag(context, "discount", pricing.getDiscount(), 8).toString() + "&nbsp;&nbsp;&nbsp;&nbsp;" +
                                        new ButtonTag(ItemHandler.class, "applyDiscount", "Apply Discount", "item", item)));
                }


                sb.append("</table>");

                if (adv) {
                    sb.append(new EditPricesRenderer(context, item));
                }
            }

            private void stock() {

                sb.append(new AdminTable("Stock / Availability"));

                switch (module.getStockControl()) {

                    default:
                    case None:
                        break;

                    case Boolean:
                        sb.append(new AdminRow("In stock", "Tick this box is this product is in stock / available.", new CheckTag(context,
                                "booleanStock", true, item.isAvailable())));

                        outStockMsg();
                        backorders();

                        if (module.hasStockNotifyEmails()) {

                            sb.append(new AdminRow("Stock notify level", "Once stock reaches this level, you will be emailed.", new TextTag(context,
                                    "stockNotifyLevel", item.getStockNotifyLevel(), 4)));

                        }

                        break;

                    case Manual:
                    case RealTime:
                        boolean isStockOverridden = false;
                        int overriddenStock = 0;
                        for (ItemOption option : item.getOptionSet().getOptions()) {
                            if (option.isStockOverride()) {
                                isStockOverridden = true;
                                for (ItemOptionSelection selection : option.getSelections()) {
                                    overriddenStock += selection.getStock();
                                }
                            }
                        }
                        if (isStockOverridden) {
                            sb.append(new AdminRow("Cumulative stock", "Your stock level for this item calculated from options", new TextTag(context, "ourStock", overriddenStock, 4).setDisabled(true)));
                        }
                        else {
                            sb.append(new AdminRow("Stock", "Your stock level for this item.", new TextTag(context, "ourStock", item.getOurStock(), 4)));
                        }

                        outStockMsg();
                        backorders();

                        break;

                    case String:

                        sb.append(new AdminRow("Availability", "The message showed to customers.", new TextTag(context, "outStockMsg", item
                                .getOutStockMsg(), 40)));
                }

                sb.append("</table>");
            }

            private void subscriptions() {

                sb.append(new AdminTable("Subscription"));

                Subscription sub = item.getSubscription();

                SelectTag subscriptionLevelTag = new SelectTag(context, "subscriptionLevel", sub.getSubscriptionLevel());
                subscriptionLevelTag.setAny("-No Subscription-");
                subscriptionLevelTag.addOptions(item.getItemType().getSubscriptionModule().getSubscriptionLevels());

                sb.append(new AdminRow(captions.getSubscriptionCaption(), subscriptionLevelTag));

                if (sub.getSubscriptionPurchaseDate() != null) {
                    sb.append(new AdminRow("Subscription purchase date", "Date and time when the currently active subscription for the account was purchased",
                            sub.getSubscriptionPurchaseDate().toString("HH:mm dd-MMM-yyyy")));
                }

                if (sub.hasSubscriptionLevel()) {

                    final DateTextTag expiryDateTag = new DateTextTag(context, "subscriptionExpiryDate", sub.getSubscriptionExpiryDate());

                    sb.append(new AdminRow(captions.getSubscriptionCaption() + " expiry",
                            "Enter the date when this account's subscription will expire, "
                                    + "or check lifetime if you want this subscription to never expire.", expiryDateTag + " Lifetime " +
                            new CheckTag(context, "lifetimeSubscription", true, sub.isLifetimeSubscription())));

                }

                sb.append("</table>");
            }

            private void supplierCosts() {

                List<Supplier> suppliers = Supplier.get(context);
                if (suppliers.isEmpty()) {
                    return;
                }

                sb.append(new AdminTable("Supplier cost prices"));

                sb.append("<tr>");
                sb.append("<td>Supplier</td>");
                sb.append("<td>Cost price</td>");
                sb.append("<td>Last updated</td>");
                sb.append("<td>Delete</td>");
                sb.append("</tr>");

                for (SupplierCostPrice supply : item.getCostPrices()) {

                    sb.append("<tr>");
                    sb.append("<td>" + supply.getSupplier().getName() + " (#" + supply.getSupplier().getId() + ")</td>");
                    sb.append("<td>" + new TextTag(context, "costPrices_" + supply.getSupplier().getId(), supply.getCostPrice(), 12) + "</td>");
                    sb.append("<td>" + (supply.hasTime() ? supply.getTime().toString("HH:mm dd-MMM") : "") + "</td>");
                    sb.append("<td>" +
                            new ButtonTag(ItemHandler.class, "removeCostPrice", "Delete", "supplier", supply.getSupplier(), "item", item)
                                    .setConfirmation("Are you sure you want to remove this cost price?") + "</td>");
                    sb.append("</tr>");

                }

                suppliers.removeAll(item.getCostSuppliers());
                if (suppliers.size() > 0) {

                    SelectTag supplierTag = new SelectTag(context, "addSupplier");
                    supplierTag.setAny("Choose supplier to add");
                    supplierTag.addOptions(Supplier.get(context));

                    sb.append("<tr><td colspan='4'>Add cost price for supplier: " + supplierTag + " " + new TextTag(context, "costPrice", 12) +
                            "</td></tr>");

                }

                sb.append("</table>");
            }

            private void supplierStocks() {

                List<Supplier> suppliers = Supplier.get(context);
                if (suppliers.isEmpty()) {
                    return;
                }

                sb.append(new AdminTable("Supplier stocks"));

                sb.append("<tr>");
                sb.append("<td>Supplier</td>");
                sb.append("<td>Stock level</td>");
                sb.append("<td>Last updated</td>");
                sb.append("<td>Delete</td>");
                sb.append("</tr>");

                for (SupplierStock supply : item.getSupplierStocks()) {

                    sb.append("<tr>");
                    sb.append("<td>" + supply.getSupplier().getName() + " (#" + supply.getSupplier().getId() + ")</td>");
                    sb.append("<td>" + new TextTag(context, "stocks_" + supply.getSupplier().getId(), supply.getStock(), 12) + "</td>");
                    sb.append("<td>" + (supply.hasTime() ? supply.getTime().toString("HH:mm dd-MMM") : "") + "</td>");
                    sb.append("<td>" +
                            new ButtonTag(ItemHandler.class, "removeSupplierStock", "Delete", "supplier", supply.getSupplier(), "item", item)
                                    .setConfirmation("Are you sure you want to remove this stock level?") + "</td>");
                    sb.append("</tr>");

                }

                //				suppliers.removeAll(item.getCostSuppliers());
                //				if (suppliers.size() > 0) {
                //
                //					SelectTag supplierTag = new SelectTag(context, "addSupplier");
                //					supplierTag.setAny("Choose supplier to add");
                //					supplierTag.addOptions(Supplier.get(context));
                //
                //					sb.append("<tr><td colspan='4'>Add cost price for supplier: " + supplierTag + " " + new TextTag(context, "costPrice", 12)
                //							+ "</td></tr>");
                //
                //				}

                sb.append("</table>");
            }

            private void eventDates(Item item, List<Attribute> attributes, Attribute dateStart, Attribute dateEnd) {
                String start = item.getAttributeValue(dateStart);
                String end = item.getAttributeValue(dateEnd);
                String startDate = start != null ? new Date(Long.parseLong(start)).toDefaultFormat() : null;
                String endDate = end != null ? new Date(Long.parseLong(end)).toDefaultFormat() : null;

                TextTag dateStartTag = new TextTag(context, dateStart.getParamName(), startDate, 10);
                dateStartTag.setClass("date-pick");
                TextTag dateEndTag = new TextTag(context, dateEnd.getParamName(), endDate, 10);
                dateEndTag.setClass("date-pick");

                Attribute enableRecurrence = item.getAttribute("Recurrence");
                String value = item.getAttributeValue(enableRecurrence);
                boolean checked = value != null && "true".equals(value);
                CheckTag enableRecurrenceBox = new CheckTag(context, enableRecurrence.getParamName(), "true", checked);

                Attribute repeatTerm = item.getAttribute("Repeat term");
                TextTag repeatTermTag = new TextTag(context, repeatTerm.getParamName(), item.getAttributeValue(repeatTerm), 2);

                Attribute recurrenceTerms = item.getAttribute("Repeat option");
                String repeatTermOptionValue = item.getAttributeValue(recurrenceTerms);
                RecurrenceOption repeatTermOption = repeatTermOptionValue != null ? RecurrenceOption.valueOf(repeatTermOptionValue) : null;
                SelectTag selectTag = new SelectTag(context, recurrenceTerms.getParamName(), null, RecurrenceOption.values());
                selectTag.setValue(repeatTermOption);


                sb.append(new AdminTable("Event dates"));
                sb.append(new AdminRow(dateStart.getName(), dateStartTag + "<br/>" + new ErrorTag(context, dateStart.getParamName())));
                sb.append(new AdminRow(dateEnd.getName(), dateEndTag + "<br/>" + new ErrorTag(context, dateEnd.getParamName())));
                sb.append(new AdminRow("Enable repeat events", enableRecurrenceBox));
                sb.append(new AdminRow("Repeat event every", repeatTermTag + " " + selectTag + " " + new ErrorTag(context, repeatTerm.getParamName())));
                sb.append("</table>");

                attributes.remove(dateStart);
                attributes.remove(dateEnd);
                attributes.remove(recurrenceTerms);
                attributes.remove(repeatTerm);
                attributes.remove(enableRecurrence);
            }

            @Override
            public String toString() {
            	
            	System.out.println("===============");
                if (miscSettings.isHtmlEditor() && item.getItemType().isContent() && !item.getItemType().isSimpleEditor()) {
                    RendererUtil.tinyMce(context, sb, "content");
                }

                sb.append(new FormTag(ItemHandler.class, "save", "multi").setId("items_form"));
                sb.append(new HiddenTag("item", item));
                sb.append(new HiddenTag("selectImage", "false").setId("selectimage_toggle"));

                commands();
                general();

                if (Module.Credits.enabled(context) && ItemModule.Credits.enabled(context, item)) {

                    if (ItemModule.Account.enabled(context, item)) {
                        creditStore();
                    }
                }

                if (Module.Listings.enabled(context)) {
                    if (ItemModule.Listings.enabled(context, item)) {
                        listing();
                    }
                }

                Attribute dateStart = item.getAttribute("Date start");
                Attribute dateEnd = item.getAttribute("Date end");

                List<Attribute> attributes = item.getAttributes();
                List<Attribute> attributesWithoutDuplicates = new ArrayList<Attribute>();
                for (Attribute attribute : attributes) {
                    if (!attribute.isDublicate()) {
                        attributesWithoutDuplicates.add(attribute);
                    }
                }
                if (item != null && item.getItemType().isFullEvent() && dateStart != null && dateEnd != null) {
                    eventDates(item, attributesWithoutDuplicates, dateStart, dateEnd);
                }

                sb.append(new AAdminRenderer(context, item, attributesWithoutDuplicates, item.getAttributeValues(), ARenderType.Input));
                if (Module.Pricing.enabled(context) && ItemModule.Pricing.enabled(context, item)) {

                    if (Module.CategoryAndItemDiscount.enabled(context)) {
                        //todo discount has already been applied
                        discountApplied();
                    }

                    pricing();

                    switch (item.getItemType().getStockModule().getStockControl()) {

                        case Manual:
                        case RealTime:

                            if (Module.Suppliers.enabled(context) && item.getItemType().isCostPricing()) {
                                supplierCosts();
                            }
                            break;

                        default:
                            break;
                    }
                }

                if (ItemModule.Stock.enabled(context, item) && item.getItemType().getStockModule().isStockControl()) {
                    stock();

                    if (Module.Suppliers.enabled(context)) {
                        supplierStocks();
                    }
                }

                if (ItemModule.Delivery.enabled(context, item)) {
                    deliveryRates();
                }

                if (miscSettings.isAdvancedMode()) {
                    if (ItemModule.Ordering.enabled(context, item)) {
                        ordering();
                    }
                }

                if (ItemModule.Subscriptions.enabled(context, item)) {
                    subscriptions();
                }

                if (ItemModule.Accessories.enabled(context, item)) {
                    accessories();
                }

                if (ItemModule.Attachments.enabled(context, item)) {
                    sb.append(new AdminAttachmentsPanel(context, item, true));
                }

                if (ItemModule.Account.enabled(context, item)) {
                    if (Module.Permissions.enabled(context)) {
                        accessGroups();
                    }
                }

                if (ItemModule.Comments.enabled(context, item)){
                    comments();
                }

                if (seo.isOverrideTags()) {
                    if (itemType.isOverrideTags()) {
                        sb.append(new MetaTagsPanel(context, item));
                    }
                }

                if (seo.isOverrideFriendlyUrls()) {
                    if (itemType.isVisible()) {
                        sb.append(new SeoFriendlyUrlsRenderer(context, item));
                    }
                }

                if (item.getItemType().isContent()) {
                    content();
                }

                if (ItemModule.Images.enabled(context, item)) {
                    sb.append(new ImageAdminPanel(context, item));
                }

                if (ItemModule.Videos.enabled(context, item)) {
                    sb.append(new VideoAdminPanel(context, item));
                }

                if (ItemModule.Messages.enabled(context, itemType)) {
                    sb.append(new MessagesPanel(context, item, false, false));
                    sb.append(new AdminTable("Add message"));
                    sb.append("<tr><td>" + new TextAreaTag(context, "messageBody", 60, 5) + "</td></tr>");
                    sb.append("</table>");
                }

                if (ItemModule.IgnoreItemsInFeed.enabled(context, itemType)) {
                    sb.append(new AdminTable("Ignore items"));
                    sb.append(new AdminRow("Ignore the item", "Allow the item to be excluded from Google base feed.", new BooleanRadioTag(context,
                            "excludeFromGoogleFeed", item.isExcludeFromGoogleFeed())));
                    sb.append("</table>");
                }

                if (ItemModule.Options.enabled(context, itemType)) {
                    sb.append(new AdminTable("Markers"));
                    sb.append(new AdminRow("Show [options_list]", "Show [options_list] for the item on the front side", new BooleanRadioTag(context,
                            "showOptionsList", item.isShowOptionsList())));
                    sb.append("</table>");
                }

                commands();

                sb.append("</form>");

                return sb.toString();
            }


        });
        return doc;
    }

    public Object emailOwner() throws ServletException {

        if (item == null) {
            return index();
        }

        try {

            item.emailOwner(user);
            addMessage("Email link sent to " + item.getOwner().getName());

        } catch (EmailAddressException e) {
            e.printStackTrace();
            addError(e);

        } catch (SmtpServerException e) {
            e.printStackTrace();
            addError(e);
        }

        return edit();
    }

    protected Tab getTab() {
        return new Tab(new Link(ItemSearchHandler.class, null, "itemType", item.getItemType()), item.getItemType().getNamePlural());
    }

    protected Link getView() {
        return new Link(org.sevensoft.ecreator.iface.frontend.items.ItemHandler.class, null, "item", item);
    }

    @Override
    public Object main() throws ServletException {

        if (itemType == null)
            return index();

        if (miscSettings.isAdvancedMode()) {
            return new ItemSearchHandler(context, itemType).main();
        }

        AdminDoc doc = new AdminDoc(context, user, itemType.getNamePlural(), Tab.getItemTab(itemType));
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new TableTag("simplelinks", "center"));
                sb.append("<tr><th align='center'>What would you like to do ?</th></tr>");

                if (Privilege.CreateItem.yes(user, context)) {

                    sb.append("<tr><td align='center'>" +
                            new LinkTag(ItemHandler.class, "create", "I want to create a " + itemType.getNameLower(), "itemType", itemType) +
                            "</td></tr>");
                }

                if (Privilege.EditAllItems.yes(user, context) || Privilege.EditOwnerItems.yes(user, context) ||
                        Privilege.EditCreatedItems.yes(user, context)) {

                    sb.append("<tr><td align='center'>" +
                            new LinkTag(ItemSearchHandler.class, null, "I want to edit an existing " + itemType.getNameLower(), "itemType",
                                    itemType) + "</td></tr>");
                }

                sb.append("</table>");

                return sb.toString();
            }
        });
        return doc;
    }

    public Object moveOptionDown() throws ServletException {

        if (item == null || image == null) {
            return edit();
        }

        EntityUtil.moveDown(option, item.getOptionSet().getOptions());
        clearParameters();
        return edit();
    }

    public Object moveOptionup() throws ServletException {

        if (item == null || option == null) {
            return edit();
        }

        EntityUtil.moveUp(option, item.getOptionSet().getOptions());
        clearParameters();
        return edit();
    }

    public Object options() throws ServletException {

        if (item == null) {
            return main();
        }

        setAttribute("view", getView());

        AdminDoc page = new AdminDoc(context, user, "Edit " + item.getItemType().getName().toLowerCase() + " #" + item.getId() + " - '" + item.getName() +
                "'", getTab());
        page.setMenu(new EditItemMenu(item));
        page.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                String lowerName = item.getItemType().getName().toLowerCase();
                sb.append(new SubmitTag("Update " + lowerName));
                sb.append(new ButtonTag(ItemHandler.class, "delete", "Delete " + lowerName, "item", item)
                        .setConfirmation("Are you sure you want to delete this " + lowerName + "?"));
                sb.append(new ButtonTag(ItemSearchHandler.class, null, "Return to search", "itemType", item.getItemType()));

                sb.append("</div>");
            }

            private void general() {

                sb.append(new AdminTable("General"));

                List<OptionGroup> optionGroups = OptionGroup.get(context);
                if (optionGroups.size() > 0) {

                    sb.append(new AdminRow("Option group", "Choose an option group to use options from.", new SelectTag(context, "optionGroup", item
                            .getOptionGroup(), optionGroups, "-Select option group-")));

                }

                if (!item.hasOptionGroup()) {

                    sb.append(new AdminRow("Minimum selected options",
                            "Set a minimum number of options that must be choosen in order to buy this item.", new TextTag(context,
                            "minimumOptionSelection", item.getOptionSet().getMinimumOptionSelection(), 4)));

                    if (isSuperman() || Module.UserMarkup.enabled(context)) {

                        SelectTag markupTag = new SelectTag(context, "optionsMarkup", item.getOptionSet().getOptionsMarkup());
                        markupTag.setAny("-Use built in renderer-");
                        markupTag.addOptions(Markup.get(context));
                        sb.append(new AdminRow(true, "Markup", "Markup used to generate options", markupTag));

                    }

                }

                sb.append("</table>");
            }

            private void options() {

                sb.append(new OwnerOptionsPanel(context, item));
            }

            private void presets() {
                sb.append(new AdminTable("Options Presets"));

                sb.append(new AdminRow("Choose Preset", "Choose a preset option to add it to the item", new SelectTag(context, "addPresetOption", null, item.getItemType().getOptionSet().getOptions(), "-- Choose --")));

                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(ItemHandler.class, "saveOptions", "post"));
                sb.append(new HiddenTag("item", item));

                commands();

                general();
                if (!item.hasOptionGroup()) {
                    options();
                    presets();
                }
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return page;
    }

    public Object removeAccessGroup() throws ServletException {

        if (item == null) {
            return index();
        }

        if (accessGroup == null) {
            return edit();
        }

        item.removeAccessGroup(accessGroup);

        return edit();
    }

    public Object removeCategory() throws ServletException {

        if (category == null || item == null) {
            return edit();
        }

        item.removeCategory(category);
        return edit();
    }

    public Object removeCostPrice() throws ServletException {

        if (item == null || supplier == null) {
            return main();
        }

        item.removeCostPrice(supplier, true);
        addMessage("Cost pricing from this supplier has been removed");
        return main();
    }

    public Object removeSupplierStock() throws ServletException {

        if (item == null || supplier == null) {
            return main();
        }

        item.removeStock(supplier, true);
        addMessage("Stock level from this supplier has been removed");
        return main();
    }

    public void testEventAttributes() throws ParseException {
        Attribute startDateAttribute = item.getAttribute("Date start");
        test(new RequiredValidator(), startDateAttribute.getParamName());
        test(new DateValidator(), startDateAttribute.getParamName());
        Attribute endDateAttribute = item.getAttribute("Date end");
        test(new RequiredValidator(), endDateAttribute.getParamName());
        test(new DateValidator(), endDateAttribute.getParamName());
        Attribute repeatTerm = item.getAttribute("Repeat term");
        test(new IntValidator(), repeatTerm.getParamName());

        if (!hasErrors()) {
            String start = attributeValues.get(startDateAttribute);
            String end = attributeValues.get(endDateAttribute);
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date startDate = start != null ? new Date(dateFormat.parse(start).getTime()) : null;
            Date endDate = end != null ? new Date(dateFormat.parse(end).getTime()) : null;
            if (startDate != null && endDate != null && startDate.isAfter(endDate)) {
                setError("", "");
                addError("Start date must be less then end date");
            }
            Attribute enableRecurrence = item.getAttribute("Recurrence");
            String value = attributeValues.get(enableRecurrence);
            boolean checked = value != null && "true".equals(value);
            if (checked && attributeValues.get(repeatTerm) == null) {
                setError("", "");
                addError("Please fill 'Repeat event every' field if 'Enable repeat events' is checked");
            }
        }
    }

    public Object save() throws ServletException {

        test(new RequiredValidator(), "name");

        // test attribute for errors
        for (Attribute attribute : item.getAttributes()) {
            test(new AttributeValidator(attribute), attribute.getParamName());
        }

        if (item.isEvent()) {
            try {
                testEventAttributes();
            } catch (Exception e) {

            }
        }

        if (hasErrors()) {
            addError("Please correct the errors and click update");
            return edit();
        }

        if (item == null) {
            return main();
        }

        if (item.setName(name)) {
            item.log(user, "Name changed to '" + name + "'.");
        }
        item.setShortName(shortName);

        if (item.getItemType().isReferences()) {

            if (!ObjectUtil.equal(item.getReference(), reference)) {
                item.setReference(reference);
                item.log(user, "Reference changed to " + reference);
            }
        }

        if (isSuperman() && itemType != null) {
            item.setItemType(itemType);
        }

        if (item.getItemType().isStatusControl() && status != null) {
            if (!item.getStatus().equalsIgnoreCase(status)) {
                item.setStatus(status);
                item.log(user, "Status changed to " + status);
            }
        }

        if (ItemModule.MyShoppingBox.enabled(context, item)) {

            ShoppingBox box = ShoppingBoxDao.get(context, item);
            if (box != null) {
                box.setWeight(weight);
                box.save();
            }

            Query q = new Query(context, "select i.* from # i join # av on i.id=av.item where av.attribute=? and av.value=?");
            q.setTable(Item.class);
            q.setTable(AttributeValue.class);
            q.setParameter(15);
            q.setParameter(suite);

            Item account = q.get(Item.class);

            item.setAccount(account);
        }

        if (addCategory != null) {
            item.addCategory(addCategory);
            item.log(user, "Category added: #" + addCategory.getIdString() + " '" + addCategory.getName() + "'");
        }

        item.setBranch(branch);

        // accounts
        if (ItemModule.Account.enabled(context, item)) {
            item.setEmail(em);

            item.setMobilePhone(mobilePhone);

            if (pwd != null) {
                item.setPassword(pwd);
                item.log("Password reset");
            }
        }

        if (item.isFeatured() != featured) {
            item.setFeatured(featured);
            item.log(user, "featured changed to " + featured);
        }

        if (item.isPrioritised() != prioritised) {
            item.setPrioritised(prioritised);
            item.log(user, "prioritised changed to " + prioritised);
        }

        // content
        if (!ObjectUtil.equal(item.getContent(), content)) {
            item.setContent(content);
            if (content == null) {
                item.log("Content cleared");
            } else {
                item.log(user, "Content changed, new content is " + content.split("\n").length + " lines");
            }
        }
        item.setSummary(summary);

        item.setOwner(owner);

        if (Module.Permissions.enabled(context)) {
            if (ItemModule.Account.enabled(context, item)) {
                if (addAccessGroup != null) {
                    item.addAccessGroup(addAccessGroup);
                }
            }
        }

        // ordering
        item.setOrderQtyMax(orderQtyMax);
        item.setOrderQtyMin(orderQtyMin);
        item.setBrochure(brochure);

        if (seo.isOverrideTags()) {

            if (!ObjectUtil.equal(item.getTitleTag(), titleTag)) {
                item.setTitleTag(titleTag);
                item.log(user, "Title tag changed to " + titleTag);
            }

            if (!ObjectUtil.equal(item.getDescriptionTag(), descriptionTag)) {
                item.setDescriptionTag(descriptionTag);
                item.log(user, "Description changed to " + descriptionTag);
            }

            if (!ObjectUtil.equal(item.getKeywords(), keywords)) {
                item.setKeywords(keywords);
                item.log(user, "Keywords changed to " + keywords);
            }
        }

        if (seo.isOverrideFriendlyUrls()) {
            item.setFriendlyUrl(friendlyUrl);
        }

        if (ItemModule.Delivery.enabled(context, item)) {

            Delivery delivery = item.getDelivery();

            for (DeliveryOption option : DeliveryOption.getItemBased(context)) {

                Money rate = deliveryRates.get(option);
                delivery.setDeliveryRate(option, rate);
            }

            delivery.setDeliverySurcharge(deliverySurcharge);
            delivery.save();
        }

        if (ItemModule.Stock.enabled(context, item)) {

            StockModule module = item.getItemType().getStockModule();

            item.setOutStockMsg(outStockMsg);
            item.setBackorders(backorders);
            item.setStockNotifyLevel(stockNotifyLevel);

            switch (module.getStockControl()) {

                case None:
                case String:
                    break;

                case Boolean:

                    if (item.setBooleanStock(booleanStock)) {
                        item.log(user, "Stock set to " + (booleanStock ? "in" : "out") + ".");
                    }

                    break;

                case RealTime:
                case Manual:
                    if (item.setOurStock(ourStock)) {
                        item.log(user, "Stock set to " + ourStock);
                    }
                    break;
            }

            for (Map.Entry<Supplier, Integer> entry : stocks.entrySet()) {
                item.setSupplierStock(entry.getKey(), entry.getValue());
            }
        }

        if (ItemModule.Pricing.enabled(context, item)) {

            boolean adv = PriceUtil.isAdvancedPricing(item, context);
            Pricing pricing = item.getPricing();

            logger.fine("[ItemHandler] setting vatRate=" + vatRate);
            logger.fine("[ItemHandler] setting rrp=" + rrp);
            logger.fine("[ItemHandler] setting simple sell price=" + sellPrice);

            item.setDiscountApplied(discountApplied);
            item.setOurCostPrice(ourCostPrice);
            item.setRrp(rrp);
            item.setUnit(unit);

            pricing.setCollectionCharge(collectionCharge);
            pricing.setDiscount(discount);
            pricing.save();

            if (vatRate != item.getVatRate()) {
                item.setVatRate(vatRate, true);
                item.log(user, "Vat rate set to " + vatRate);
            }

            for (Map.Entry<Supplier, Money> entry : costPrices.entrySet()) {
                item.setSupplierCostPrice(entry.getKey(), entry.getValue());
            }

            if (addSupplier != null) {
                item.setSupplierCostPrice(addSupplier, costPrice);
            }

            if (adv) {

                item.setPriceBreaks(StringHelper.explodeIntegers(priceBreaks, "\\D"));
                PriceUtil.savePrices(context, item, prices);

            } else {

                item.setStandardPrice(sellPrice);
                logger.fine("[Item] setting standard price to " + sellPrice);

            }
        }

        if (ItemModule.Credits.enabled(context, item)) {

            if (ItemModule.Account.enabled(context, item)) {

                CreditStore store = item.getCreditStore();
                store.setCredits(credits);
                store.save();

            }
        }

        if (ItemModule.IgnoreItemsInFeed.enabled(context, item)) {
            item.setExcludeFromGoogleFeed(excludeFromGoogleFeed);
        }

        if (ItemModule.Options.enabled(context, item)) {
            item.setShowOptionsList(showOptionsList);
        }

        if (ItemModule.Account.enabled(context, item)) {
            item.setPriceBand(priceBand);
        }

        //comments
        item.setComments(comments);

        item.save();

        // subscriptions
        if (ItemModule.Subscriptions.enabled(context, item)) {

            Subscription sub = item.getSubscription();

            sub.subscribe(subscriptionLevel);

            if (lifetimeSubscription) {
                sub.setLifetimeSubscription();
            }

            sub.setSubscriptionExpiryDate(subscriptionExpiryDate);
            sub.setSubscriptionLevel(subscriptionLevel);
            sub.save();
        }

        // listings
        if (ItemModule.Listings.enabled(context, item)) {

            item.setAccount(account);
            item.getListing().setListingPackage(listingPackage);
            item.save();

            Listing listing = item.getListing();
            listing.setExpiryDate(expiryDate);
            listing.save();

        }

        item.log(user, "Item details updated");

        // attributes

        //clear repeat terms if non recurrent event
        if (item.isEvent()) {
            String value = attributeValues.get(item.getAttribute("Recurrence"));
            boolean checked = value != null && "true".equals(value);
            if (!checked) {
                Attribute repeatTermAttribute = item.getAttribute("Repeat term");
                if (repeatTermAttribute != null) {
                    attributeValues.set(repeatTermAttribute, null);
                }
                Attribute repeatOptionAttribute = item.getAttribute("Repeat option");
                if (repeatOptionAttribute != null) {
                    attributeValues.set(repeatOptionAttribute, null);
                }
            }
        }

        Attribute date = item.getAttribute("Date Amended");
        if (date != null && date.getAutoValue() != null && date.getAutoValue().equals(Attribute.AutoValue.CurrentDateTime)) {
            attributeValues.set(date, new DateTime().toString("dd/MM/yyyy HH:mm"));
        }

        item.setAttributeValues(attributeValues);
        for (Attribute attribute : removeAttributeValues) {
            item.removeAttributeValues(attribute);
        }

        if (ItemModule.Videos.enabled(context, item)) {
            VideoUtil.save(context, item, videoUploads);
        }

        try {
            ImageUtil.save(context, item, imageUploads, imageUrls, imageFilename, true);
        } catch (ImageLimitException e) {
            addError(e);
        } catch (IOException e) {
            return new ErrorDoc(context, "The image you are attempting to upload is in CYMK colour mode. " +
                    "This is unsopported currently, please use a photo editing application (e.g. Photoshop) to convert the file to RGB colour. For more information or assistance please contact support.");
        }

        List<Attachment> attachments = AttachmentUtil.save(item, attachmentDescriptions, files, makePreviews);
        for (Attachment a : attachments) {
            item.log(user, "Attachment uploaded: " + a.getFilename());
        }
        clearParameters();

        if (messageBody != null) {
            item.addMessage(user, messageBody);
        }

        if (miscSettings.isAdvancedMode()) {
            return new ActionDoc(context, "This " + item.getItemTypeNameLower() + " has been updated.", new Link(ItemHandler.class, "edit", "item", item));
        }

        AdminDoc doc = new AdminDoc(context, user, "This " + item.getItemType().getNameLower() + " has been updated", getTab());
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new TableTag("simplelinks", "center"));
                sb.append("<tr><th align='center'>What would you like to do ?</th></tr>");

                if (Privilege.EditAllItems.yes(user, context) || Privilege.EditOwnerItems.yes(user, context) ||
                        Privilege.EditCreatedItems.yes(user, context)) {

                    sb.append("<tr><td align='center'>" +
                            new LinkTag(ItemHandler.class, "edit", "I want to edit this " + item.getItemType().getNameLower() + " again", "item",
                                    item) + "</td></tr>");

                    sb.append("<tr><td align='center'>" +
                            new LinkTag(ItemSearchHandler.class, null, "I want to edit a different " + item.getItemType().getNameLower(),
                                    "itemType", item.getItemType()) + "</td></tr>");
                }

                if (Privilege.CreateItem.yes(user, context)) {
                    sb.append("<tr><td align='center'>" +
                            new LinkTag(ItemHandler.class, "create", "I want to create a new " + item.getItemType().getNameLower(), "itemType",
                                    item.getItemType()) + "</td></tr>");
                }

                sb.append("</table>");

                return sb.toString();
            }
        });
        return doc;
    }

    //	public Object saveImages() throws ServletException {
    //
    //		if (item == null)
    //			return main();
    //
    //		clearParameters();
    //
    //		return new ActionDoc(context, "The image(s) have been added to this " + item.getItemType().getNameLower() + ".", new Link(ItemHandler.class,
    //				"images", "item", item));
    //
    //	}

    public Object saveOptions() throws ServletException {

        if (item == null)
            return main();

        item.setOptionGroup(optionGroup);
        item.save();

        item.getOptionSet().setMinimumOptionSelection(minimumOptionSelection);

        // OPTIONS
        if (newOptions != null) {
            for (String string : StringHelper.explodeStrings(newOptions, "\n")) {
                item.getOptionSet().addOption(string);
            }
        }

        if (isSuperman() || Module.UserMarkup.enabled(context)) {
            item.getOptionSet().setOptionsMarkup(optionsMarkup);
        }

        if (addPresetOption != null) {
            item.getOptionSet().addOption(addPresetOption, item);
        }
        
        item.getOptionSet().save();

        addMessage("Your changes have been saved");
        clearParameters();
        return options();
    }

    public Object applyDiscount() throws ServletException {

        if (item == null) {
            return main();
        }
        Amount discount = item.getPricing().getDiscount();

        if (!item.isDiscountApplied()) {
            item.applyDiscount(discount);
        }

        item.getPricing().setDiscount(null);

        clearParameters();
        return edit();
    }


    public Object comments() throws ServletException {
        if (item == null) {
            return main();
        }

        final List<Comment> comments = Comment.get(context, false, item);
        AdminDoc doc = new AdminDoc(context, user, item.getItemTypeName() + " Comments", Tab.getItemTab(item));
        doc.setMenu(new EditItemMenu(item));

        doc.addBody(new Body() {

            private void list() {
                Link linkback = new Link(ItemHandler.class, "comments", "item", item);
                sb.append(new AdminTable("Comments to " + item.getName()));
                sb.append("<tr>");
                sb.append("<th width='20'>Id</th>");
                sb.append("<th width='35'>Date</th>");
                sb.append("<th>Comment</th>");
                sb.append("<th width='10'>Delete</th>");
                sb.append("</tr>");

                for (Comment comment : comments) {

                    sb.append("<tr>");

                    // id
                    sb.append("<td>" + new LinkTag(CommentHandler.class, "edit", comment.getIdString(), "comment", comment,
                            "item", item, "linkback", linkback) + "</td>");

                    // date created
                    sb.append("<td>" + comment.getDateCreated().toDateString() + "</td>");

                    //comment
                    sb.append("<td>" + new LinkTag(CommentHandler.class, "edit", comment.getComment(), "comment", comment,
                            "item", item, "linkback", linkback) + "</td>");

                    // delete link
                    sb.append("<td>");
                    sb.append(new LinkTag(CommentHandler.class, "delete", new DeleteGif(), "comment", comment,
                            "linkback", linkback).setConfirmation("Are you sure you want to delete this comment"));

                    sb.append("</td>");

                    sb.append("</tr>");

                }

                sb.append("</table>");
            }

            private void add() {
                sb.append(new AdminTable("Add comment"));

                sb.append(new FormTag(ItemHandler.class, "addComment", "post"));
                sb.append(new HiddenTag("item", item));

               sb.append(new AddCommentsPanel(context));

                sb.append("</form>");
                sb.append("</table>");
            }

            @Override
            public String toString() {

                list();
                if (item.isComments()) {
                    add();
                }

                return sb.toString();
            }
        });
        return doc;
    }

    public Object addComment() throws ServletException {
        item.addComment(context, title, comment);
        clearParameters();
        return comments();
    }

    public ItemHandler setItemType(ItemType itemType) {
        this.itemType = itemType;
        return this;
    }
}
