package org.sevensoft.ecreator.iface.admin.reminders.export;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.StreamResult;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * User: MeleshkoDN
 * Date: 17.09.2007
 * Time: 13:31:33
 */
@Path("admin-holiday-reminders-export.do")
public class HolidayRemindersExportHandler extends AdminHandler {

    public HolidayRemindersExportHandler(RequestContext context) {
        super(context);
    }

    @Override
    public Object main() throws ServletException {

        HolidayRemindersExporter exporter = new HolidayRemindersExporter(context);
        try {
            File file = exporter.export();
            return new StreamResult(file, "text/csv", "holiday_reminders.csv", StreamResult.Type.Attachment);
        } catch (IOException e) {
            e.printStackTrace();
            return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
        }
    }
}

