package org.sevensoft.ecreator.iface.admin.attributes;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.design.reorder.ObjectsReorderPanel;
import org.sevensoft.ecreator.iface.admin.design.select.ObjectsSelectablePanel;
import org.sevensoft.ecreator.iface.admin.accounts.AccountModuleHandler;
import org.sevensoft.ecreator.iface.admin.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.admin.crm.jobsheets.JobsheetHandler;
import org.sevensoft.ecreator.iface.admin.ecom.bookings.config.BookingSettingsHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.OrderSettingsHandler;
import org.sevensoft.ecreator.iface.admin.items.GlobalItemAttributesHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeSelectTag;
import org.sevensoft.ecreator.iface.admin.items.modules.options.OptionGroupsHandler;
import org.sevensoft.ecreator.iface.admin.items.modules.reviews.ReviewsModuleHandler;
import org.sevensoft.ecreator.iface.admin.marketing.newsletter.NewsletterSettingsHandler;
import org.sevensoft.ecreator.iface.admin.marketing.sms.SmsSettingsHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionCollections;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.accounts.permissions.panels.PermissionsPanel;
import org.sevensoft.ecreator.model.attributes.*;
import org.sevensoft.ecreator.model.attributes.Attribute.AutoValue;
import org.sevensoft.ecreator.model.attributes.renderers.OwnerAttributesPanel;
import org.sevensoft.ecreator.model.bookings.BookingSettings;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.ItemTypeSettings;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.results.RequestForward;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author sks
 */
@Path("admin-attributes.do")
public class AttributeHandler extends AdminHandler {

    private Attribute attribute;
    private int max;
    private int min;
    private boolean multi, linkable, filter;
    private AttributeType type;
    private String name, description;
    private boolean registration;
    private boolean summary;
    private boolean optional;
    private int cols;
    private int rows;
    private boolean displayable;
    private String newOptions;
    private boolean location;
    private ItemType itemType;
    private boolean hideLabel;
    private boolean invoiceColumn;
    private String invoiceName;
    private String linkText;
    private boolean linkNewWindow;
    private boolean customOptionsOrder;
    private int page;
    private boolean memberName;
    private boolean permissions;
    private MultiValueMap<String, PermissionType> permissionsMap;
    private String resetAttributeValues;
    private boolean checkout;
    private String anyLabel;
    private String intro;
    private boolean delivery;
    private String suffix;
    private String prefix;
    private int maxDp;
    private int minDp;
    private String regExp;
    private String section;
    private String linkback;

    private boolean listable;
    private int cellSpan;
    private int cells;
    private boolean imagesOnAddListing;
    private int step;
    private List<AttributeOption> options;
    private List<AttributeOption> positions;
    private Money sellPriceAdjustment;
    private ItemType associationItemType;
    private String subject;
    private boolean grouping;
    private List<Attribute> attributesToDelete;
    private AutoValue autoValue;
    private int incrementStep;
    private int incrementStart;
    private int randomLength;
    private String format;
    private int startYear;
    private int endYear;
    private int yearRange;
    private boolean editable;
    private String emailCc;
    private String emailBcc;
    private SearchControl searchControl;
    private boolean displayOnSubscriberOnly;
    private String customErrorMessage;
    private boolean checkAllOptions;
    private String checkAllFrontLabel;
    private boolean noFollow;
    private boolean ibex;
    private boolean dateStart, dateEnd;

    public AttributeHandler(RequestContext context) {
        super(context);
    }

    public AttributeHandler(RequestContext context, Attribute attribute) {
        super(context);
        this.attribute = attribute;
    }

    public Object createOptionsFromValues() throws ServletException {

        if (attribute == null) {
            return main();
        }

        attribute.createOptionsFromValues();
        return edit();
    }

    public Object delete() throws ServletException {

        if (attribute == null) {
            return main();
        }

        AttributeOwner owner = attribute.getOwner();
        owner.removeAttribute(attribute);

        attribute.log(user, "Deleted");

        if (linkback != null) {
            return new RequestForward(linkback);
        }

        if (owner instanceof ItemType) {
            return new ItemTypeHandler(context, (ItemType) owner).edit();
        } else if (attribute.hasCategory()) {
            return new CategoryHandler(context, (Category) owner).edit();
        } else if (owner instanceof OrderSettings) {
            return new OrderSettingsHandler(context).main();
        } else if (owner instanceof ItemTypeSettings) {
            return new GlobalItemAttributesHandler(context).main();
        } else if (owner instanceof BookingSettings) {
            return new BookingSettingsHandler(context).main();
        }

        return index();
    }

    public Object edit() throws ServletException {

        if (attribute == null) {
            return main();
        }

        AdminDoc doc = new AdminDoc(context, user, "Edit attribute", null);
        doc.addBody(new Body() {

            private void autoValue() {

                sb.append(new AdminTable("Option setup"));

                sb.append(new AdminRow("Auto value", "Set items to have values automatically assigned for this attribute.", new SelectTag(context,
                        "autoValue", attribute.getAutoValue(), AutoValue.values(), "-None-")));

                if (attribute.hasAutoValue()) {
                    switch (attribute.getAutoValue()) {

                        case Incremental:

                            sb.append(new AdminRow("Increment start", "The initial value for this attribute.", new TextTag(context, "incrementStart",
                                    attribute.getIncrementStart(), 12)));

                            sb.append(new AdminRow("Increment step", "Increase the value by this amount each time.", new TextTag(context,
                                    "incrementStep", attribute.getIncrementStep(), 12)));
                            break;

                        case RandomNumber:

                            sb.append(new AdminRow("Random length", "How many characters should be used in the value.", new TextTag(context,
                                    "randomLength", attribute.getRandomLength(), 4)));
                            break;
                    }
                }

                sb.append("</table>");
            }

            private void checkout() {

                if (Module.Shopping.enabled(context)) {

                    if (attribute.isOrder()) {

                        sb.append(new AdminRow("Checkout", "Show this attribute as a checkout option.", new BooleanRadioTag(context, "checkout",
                                attribute.isCheckout())));
                    }
                }
            }

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update attribute"));
                sb.append(new ButtonTag(AttributeHandler.class, "delete", "Delete this attribute", "attribute", attribute)
                        .setConfirmation("Are you sure you want to delete this attribute?"));

                if (attribute.hasItemType()) {
                    sb.append(new ButtonTag(ItemTypeHandler.class, "edit", "Return to item type", "itemType", attribute.getItemType()));
                } else if (attribute.hasCategory()) {
                    sb.append(new ButtonTag(CategoryHandler.class, "edit", "Return to category", "category", attribute.getCategory()));
                } else if (attribute.isOrder()) {
                    sb.append(new ButtonTag(OrderSettingsHandler.class, null, "Return to order settings"));
                } else if (attribute.hasJobsheet()) {
                    sb.append(new ButtonTag(JobsheetHandler.class, "edit", "Return to jobsheet", "jobsheet", attribute.getJobsheet()));
                } else if (attribute.hasOptionGroup()) {
                    sb.append(new ButtonTag(OptionGroupsHandler.class, "edit", "Return to option group", "optionGroup", attribute.getOptionGroup()));
                } else if (attribute.hasReviewsModule()) {
                    sb.append(new ButtonTag(ReviewsModuleHandler.class, null, "Return to reviews module", "itemType", attribute.getReviewsModule()
                            .getItemType()));
                } else if (attribute.isGlobalItemType()) {
                    sb.append(new ButtonTag(AccountModuleHandler.class, null, "Return to member settings"));
                } else if (attribute.isNewsletter()) {
                    sb.append(new ButtonTag(NewsletterSettingsHandler.class, null, "Return to newsletter settings"));
                } else if (attribute.isSms()) {
                    sb.append(new ButtonTag(SmsSettingsHandler.class, null, "Return to SMS settings"));
                } else if (attribute.isBookingAttribute()) {
                    sb.append(new ButtonTag(BookingSettingsHandler.class, null, "Return to Booking settings"));
                } else {
                    throw new RuntimeException();
                }

                sb.append(new ButtonTag(AttributeHandler.class, null, "Go to attributes editor"));

                sb.append("</div>");
            }

            private void date() {

                sb.append(new AdminTable("Date settings"));

                sb.append(new AdminRow("Start year", "The first year to display in the selection.", new TextTag(context, "startYear", attribute
                        .getStartYear(), 4)));

                sb.append(new AdminRow("Year range",
                        "If end year is zero, then the end year is calculated from the start year plus this number of years.", new TextTag(context,
                        "yearRange", attribute.getYearRange(), 4)));

                sb.append("</table>");
            }

            private void delivery() {

                if (Module.Delivery.enabled(context)) {

                    sb.append(new AdminRow("Custom delivery", "Use the value of this attribute to calculate validity for custom shipping.",
                            new BooleanRadioTag(context, "delivery", attribute.isDelivery())));

                }
            }

            private void display() {

                if (attribute.hasItemType() || attribute.hasItemType()) {

                    sb.append(new AdminRow("Cell span", "Number of cells to take up when rendering", new TextTag(context, "cellSpan", attribute
                            .getCellSpan(), 4)));

                    sb.append(new AdminRow("Cells per row", "Number of cells per row. Only takes affect if this is the lead attribute in a section",
                            new TextTag(context, "cells", attribute.getCellsPerRow(), 4)));

                }
            }

            private void displayName() {

                if (attribute.hasItemType() || attribute.isGlobalItemType()) {

                    sb.append(new AdminRow("Display name", "Use this attribute to generate the display name.", new BooleanRadioTag(context,
                            "memberName", attribute.isDisplayName())));

                }
            }

            private void emailBcc() {

                sb.append(new AdminRow("Email BCC", "Enter any blind carbon copy addresses.", new TextTag(context, "emailBcc", attribute.getEmailBcc(),
                        20)));
            }

            private void emailCc() {

                sb.append(new AdminRow("Email CC", "Enter any carbon copy addresses.", new TextTag(context, "emailCc", attribute.getEmailCc(), 20)));
            }

            /**
             *
             */
            private void format() {
                sb.append(new AdminRow("Format", "Format this attribute using a mask string.",
                        new TextTag(context, "format", attribute.getFormat(), 20)));
            }

            /**
             *
             */
            private void general() {

                sb.append(new AdminTable("General"));

                sb.append(new AdminRow("Name", "The name of this attribute.", new TextTag(context, "name", attribute.getName(), 40)));

                if (attribute.hasItemType()) {

                    if (isSuperman()) {

                        SelectTag classesTag = new SelectTag(context, "itemType", attribute.getItemType());
                        classesTag.addOptions(ItemType.getSelectionMap(context));
                        sb.append(new AdminRow("Item type", classesTag));

                    } else {

                        sb.append(new AdminRow("Item type", "The item type this attribute is defined on." + attribute.getItemType().getName()));

                    }
                }

                SelectTag tag = new SelectTag(context, "type", attribute.getType());
                tag.addOptions(AttributeType.values());

                sb.append(new AdminRow("Type", "The data type of this attribute.", tag));

                if(AttributeType.Date.equals(attribute.getType()) || AttributeType.DateTime.equals(attribute.getType())){
                    sb.append(new AdminRow("Calendar Date Start", "", new BooleanRadioTag(
                            context, "dateStart", attribute.isDateStart())));

                    sb.append(new AdminRow("Calendar Date End", "", new BooleanRadioTag(
                            context, "dateEnd", attribute.isDateEnd())));
                }

                sb.append(new AdminRow("Description", "Brief comments to clarify the use of this attribute.", new TextAreaTag(context, "description",
                        attribute.getDescription(), 60, 3)));

                sb.append(new AdminRow("Section", "Group attributes by section.", new TextTag(context, "section", attribute.getSection(), 40)));

                if (attribute.hasItemType() || attribute.isGlobalItemType()) {

                    sb.append(new AdminRow("Displayable", "When set to yes this attribute is visible on the site to users.", new BooleanRadioTag(
                            context, "displayable", attribute.isDisplayable())));

                    sb.append(new AdminRow("Editable", "Can this attribute be edited by the user in listings or profile.", new BooleanRadioTag(
                            context, "editable", attribute.isEditable())));

                    if (Module.Subscriptions.enabled(context)) {
                        sb.append(new AdminRow("Displayable on subscribers only", "Only show this attribute on items that have a subscription.",
                                new BooleanRadioTag(context, "displayOnSubscriberOnly", attribute.isDisplayOnSubscriberOnly())));
                    }
                }

                checkout();

                /*
                     * Attributes only applicable to those for items
                     */
                if (attribute.hasItemType()) {

                    if (Module.Shopping.enabled(context)) {

                        sb.append(new AdminRow("Invoice column", "Show this attribute on printable invoices.", new BooleanRadioTag(context,
                                "invoiceColumn", attribute.isInvoiceColumn())));

                        if (attribute.isInvoiceColumn()) {

                            sb
                                    .append(new AdminRow(
                                            "Invoice name",
                                            "Set the name of this attribute to appear on the invoice if you require a different name to the normal one.",
                                            new TextTag(context, "invoiceName", attribute.getInvoiceName())));
                        }

                    }
                }

                sb.append(new AdminRow("Page", "Set which page you want this attribute to appear on.", new TextTag(context, "page",
                        attribute.getPage(), 40)));

                if ((attribute.isGlobalItemType() && Module.Listings.enabled(context)) ||
                        (attribute.hasItemType() && ItemModule.Listings.enabled(context, attribute.getItemType()))) {

                    sb.append(new AdminRow("Listable", "When set to yes this attribute is shown to the user adding a listing.", new BooleanRadioTag(
                            context, "listable", attribute.isListable())));

                }

                if ((attribute.isGlobalItemType() && Module.Accounts.enabled(context)) ||
                        (attribute.hasItemType() && ItemModule.Registrations.enabled(context, attribute.getItemType()))) {

                    sb.append(new AdminRow("Registration", "Show this attribute on the account registration page.", new BooleanRadioTag(context,
                            "registration", attribute.isRegistration())));

                }

                sb.append(new AdminRow("Search control",
                        "Set an optional search control which determines the default search behaviour for this attribute.", new SelectTag(context,
                        "searchControl", attribute.getSearchControl(), SearchControl.values(), "-Default-")));

                switch (attribute.getType()) {

                    case Association:

                        optional();
                        multi();

                        sb.append(new AdminRow("Association item type", "Limit selections to this type.", new ItemTypeSelectTag(context,
                                "associationItemType", attribute.getAssociationItemType(), "-Select item type-")));

                        break;

                    case Numerical:

                        optional();
                        multi();
                        linkable();
                        format();
                        presuff();
                        size();

                        sb.append(new AdminRow("Max and min values",
                                "Enter the mininum and maximum values users can enter. Set max to zero to have no limit.", "Min " +
                                new TextTag(context, "min", attribute.getMin(), 7) + " Max " +
                                new TextTag(context, "max", attribute.getMax(), 7)));

                        sb
                                .append(new AdminRow(
                                        "Step",
                                        "If you enter a value here then users will be given a list of values to choose starting at min and ending at max, and increasing by this step value each time.",
                                        new TextTag(context, "step", attribute.getStep(), 6)));

                        sb.append(new AdminRow("Decimal places",
                                "Enter the minimum and maximum decimal places for formatting of values on this attribute.", "Min " +
                                new TextTag(context, "minDp", attribute.getMinDp(), 4) + " Max " +
                                new TextTag(context, "maxDp", attribute.getMaxDp(), 4)));

                        groupingSize();

                        delivery();

                        break;

                    case Date:
                        optional();
                        multi();
                        linkable();
                        break;

                    case DateTime:
                        optional();
                        multi();
                        linkable();
                        break;

                    case Email:
                        optional();
                        multi();
                        subject();
                        linkText();
                        emailCc();
                        emailBcc();
                        break;

                    case Boolean:
                        optional();
                        multi();
                        break;

                    case Selection:

                        optional();
                        multi();
                        linkable();
                        prices();
                        location();

                        displayName();

                        sb.append(new AdminRow("Any label",
                                "Set the value of the any / no selection option when showing a drop down list for searching.", new TextTag(context,
                                "anyLabel", attribute.getAnyLabel(), 20)));

                        sb.append(new AdminRow("Intro", "When non optional, this text will create a non selectable first option in the drop down.",
                                new TextTag(context, "intro", attribute.getIntro(), 20)));

                        if (isSuperman()) {

                            sb.append(new AdminRow(true, "Reset attribute values", "Set all attribute values to this value", new TextTag(context,
                                    "resetAttributeValues", 40)));

                            sb.append(new AdminRow(true, "Randomise values", "Randomly set attribute values across all items.", new ButtonTag(
                                    AttributeHandler.class, "randomiseValues", "Randomise now baby!", "attribute", attribute)
                                    .setConfirmation("Are you sure you want to randomise values - this will overwrite all existing values?")));
                        }

                        break;

                    case Text:

                        optional();
                        multi();
                        linkable();

                        size();
                        regex();
                        format();

                        prices();
                        location();

                        displayName();

                        break;

                    case Link:

                        linkText();

                        sb.append(new AdminRow("New window", "Set to yes if you want the link to open in a new window when the user clicks.",
                                new BooleanRadioTag(context, "linkNewWindow", attribute.isLinkNewWindow())));

                        if (Module.NoFollow.enabled(context)) {
                            sb.append(new AdminRow("No Follow", "Set to yes if you want the search engines stop following this link.",
                                    new BooleanRadioTag(context, "noFollow", attribute.isNoFollow())));
                        }

                        optional();
                        multi();
                        break;

                    case Postcode:

                        optional();
                        multi();
                        location();
                        linkable();
                        utils();

                        break;

                    case Image:

                        multi();
                        break;
                }

                display();

                sb.append("</table>");
            }

            private void groupingSize() {

                sb.append(new AdminRow("Grouping", "Group characters together with a comma", new BooleanRadioTag(context, "grouping", attribute
                        .isGrouping())));
            }

            private void linkable() {

                if (attribute.hasItemType() || attribute.isGlobalItemType()) {

                    sb.append(new AdminRow("Linkable", "Make this attribute a link to a search for other items with the same attribute value.",
                            new BooleanRadioTag(context, "linkable", attribute.isLinkable())));
                }
            }

            /**
             *
             */
            private void linkText() {
                sb.append(new AdminRow("Link text", "You can override the text that appears for the link here.", new TextTag(context, "linkText",
                        attribute.getLinkText(), 20)));
            }

            private void location() {

                if (attribute.hasItemType() || attribute.hasCategory() || attribute.isGlobalItemType()) {

                    StringBuilder sb2 = new StringBuilder();

                    if (!config.isLocations()) {

                        sb2.append("Install location files to use this attribute for location searching.");

                    } else {

                        sb2.append(new BooleanRadioTag(context, "location", attribute.isLocation()));

                        if (isSuperman() && attribute.isLocation()) {

                            sb2.append(" ");
                            sb2.append(new ButtonTag(AttributeHandler.class, "resetLocations", "Reset locations", "attribute", attribute));
                        }

                    }

                    sb.append(new AdminRow("Location",
                            "If set then the value of this attribute will set the location of this attribute for distance searching.", sb2));

                }
            }

            private void multi() {

                if (attribute.hasItemType() || attribute.hasCategory() || attribute.isGlobalItemType()) {

                    sb.append(new AdminRow("Multi values", "Allow more than one value to be added for this attribute.", new BooleanRadioTag(context,
                            "multi", attribute.isMultiValues())));

                }
            }

            private void optional() {

                sb.append(new AdminRow("Optional", "If optional this attribute can be left blank.", new BooleanRadioTag(context, "optional", attribute
                        .isOptional())));
            }

            private void options() {

                sb.append(new AdminTable("Option setup"));

                int span = 3;
                if (attribute.isCustomOptionsOrder()) {
                    span++;
                }

                sb.append(new AdminRow("Custom options order", "Set to yes and you can manually set the order of the options. "
                        + "Set to no and they are automatically ordered alphabetically.", new BooleanRadioTag(context, "customOptionsOrder",
                        attribute.isCustomOptionsOrder())));

                if (Module.Listings.enabled(context)) {
                    sb.append(new AdminRow("Images for add listing", "Show the images for these attributes when adding a new listing.",
                            new BooleanRadioTag(context, "imagesOnAddListing", attribute.isImagesOnAddListing())));
                }

                sb.append(new AdminRow("Check all", "", new BooleanRadioTag(context, "checkAllOptions",
                        attribute.isCheckAllOptions())));

                if (Module.IBex.enabled(context)) {
                    sb.append(new AdminRow("iBex attribute", "Attribute is used by iBex booking system", new BooleanRadioTag(context, "ibex",
                            attribute.isIbex())));
                }

                if (attribute.isCheckAllOptions()) {
                    sb.append(new AdminRow("CheckAll Front Label", "", new TextTag(context, "checkAllFrontLabel",
                            attribute.getCheckAllFrontLabel(), 40)));
                }

                if (isSuperman()) {

                    sb.append(new AdminRow(true, "Remove unused", "Click here to remove all options that are not set on an item or member",
                            new ButtonTag(AttributeHandler.class, "removeUnusedOptions", "Remove unused", "attribute", attribute)
                                    .setConfirmation("Confirm you want to remove unused options")));

                    sb
                            .append(new AdminRow(
                                    true,
                                    "Create options from values",
                                    "Creates options to match all currently used values of this attribute. Very useful if you have changed a text attribute to a selection.",
                                    new ButtonTag(AttributeHandler.class, "createOptionsFromValues", "Create options", "attribute", attribute)));

                }

                sb.append("</table>");

                if (attribute.isCustomOptionsOrder()) {
                    sb.append(new AdminTable("Options (drag & drop to reorder)"));
                } else {
                    sb.append(new AdminTable("Options (use CTRL to choose number of lines)"));
                }

                sb.append("<tr>");
                sb.append("<th width='60'>Delete</th>");
                sb.append("<th width='60'>Position</th>");
                sb.append("<th>Value</th>");
                sb.append("</tr>");
                if (attribute.isCustomOptionsOrder()) {
                    sb.append("<td colspan='3'>");

                    ObjectsReorderPanel panel = new ObjectsReorderPanel(context, null, attribute.getOptions());
                    panel.setEditClazz(AttributeOptionHandler.class);
                    panel.setEditAction(null);
                    panel.setEditParam("option");
                    panel.setEditExtraParams("attribute", attribute);
                    panel.setDeleteParam("options");

                    sb.append(panel);

                    sb.append("</td>");
                } else {
                    sb.append("<td colspan='3'>");

                    ObjectsSelectablePanel panel = new ObjectsSelectablePanel(context, attribute.getOptions());
                    panel.setEditClazz(AttributeOptionHandler.class);
                    panel.setEditAction(null);
                    panel.setEditParam("option");
                    panel.setEditExtraParams("attribute", attribute);
                    panel.setDeleteParam("options");

                    sb.append(panel);

                    sb.append("</td>");
/*
//                    PositionRender pr = new PositionRender(AttributeOptionHandler.class, "option");
                    int n = 1;
                    for (AttributeOption option : attribute.getOptions()) {

                        sb.append("<tr>");
                        sb.append("<th width='60'>" + new CheckTag(context, "options", option, false) + "</th>");
                        sb.append("<td width='60'>" + n++ + "</td>");

//                        if (attribute.isCustomOptionsOrder())
//                            sb.append("<td width='80'>" + pr.render(option) + "</td>");

                        sb.append("<td>" +
                                new LinkTag(AttributeOptionHandler.class, null, new ImageTag("files/graphics/admin/spanner.gif"), "option", option) +
                                " " + option.getValue() + "</td>");

                        sb.append("</tr>");

                    }
*/
                }

                sb.append("<tr><td colspan='" + span + "'>Add new options<br/>");
                sb.append(new TextAreaTag(context, "newOptions", 40, 4));
                sb.append("</td></tr>");

                sb.append("</table>");

            }

            private void presuff() {

                sb.append(new AdminRow("Prefix", "If you enter text here then all values of this attribute will have this prefix displayed.",
                        new TextTag(context, "prefix", attribute.getPrefix(), 12)));

                sb.append(new AdminRow("Suffix", "If you enter text here then all values of this attribute will have this suffix appended.",
                        new TextTag(context, "suffix", attribute.getSuffix(), 12)));

            }

            private void prices() {

                if (attribute.hasOptionGroup()) {

                    sb.append(new AdminRow("Sell price adjustment", "Adjust the sell price when this option is used.", new TextTag(context,
                            "sellPriceAdjustment", attribute.getSellPriceAdjustment(), 8)));
                }
            }

            private void regex() {

                if (isSuperman()) {

                    sb.append(new AdminRow(true, "Regular expression", "This regular expression is used to check the validity of entered values.",
                            new TextTag(context, "regExp", attribute.getRegExp(), 40)));

                    sb.append(new AdminRow(true, "Custom error message", "This message will be shown if attribute value don't match regular expression.",
                            new TextTag(context, "customErrorMessage", attribute.getCustomErrorMessage(), 70)));
                }
            }

            private void size() {

                sb.append(new AdminRow("Size", "How big do you want the box to enter values for this attribute?", "Width " +
                        new TextTag(context, "cols", attribute.getCols(), 5) + " Height " + new TextTag(context, "rows", attribute.getRows(), 5)));
            }

            private void subject() {

                sb.append(new AdminRow("Subject", "Enter the subject of the email link. <i>You can use special tags [item] and [id]</i>", new TextTag(
                        context, "subject", attribute.getSubject(), 20)));
            }

            @Override
            public String toString() {

                sb.append(new FormTag(AttributeHandler.class, "save", "POST"));
                sb.append(new HiddenTag("attribute", attribute));

                commands();
                
                general();

                if (attribute.getType() == AttributeType.Selection) {
                    options();
                } else {
                    autoValue();
                }

                if (attribute.getType() == AttributeType.Date) {
                    date();
                }

                if (Module.Permissions.enabled(context)) {
                    sb.append(new PermissionsPanel(context, attribute, PermissionCollections.getAttribute()));
                }

                commands();

                sb.append("</form>");
                return sb.toString();
            }

            private void utils() {

                sb.append(new AdminRow("Format postcodes", "Reformat all the values set on this attribute to be formatted postcodes.", new ButtonTag(
                        AttributeHandler.class, "formatPostcodes", "Format postcodes", "attribute", attribute)));
            }

        });

        return doc;
    }

    public Object formatPostcodes() throws ServletException {

        if (attribute == null) {
            return main();
        }

        AttributeUtil.formatPostcodes(attribute, context);
        return new ActionDoc(context, "The values on this attribute have been reformatted",
                new Link(AttributeHandler.class, "edit", "attribute", attribute));
    }

    @Override
    public Object main() throws ServletException {

        AdminDoc doc = new AdminDoc(context, user, "Attributes", null);
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new FormTag(AttributeHandler.class, "remove", "get"));
                sb.append(new OwnerAttributesPanel(context));

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Remove selected").setConfirmation("Are you sure you want to delete the selected attributes?"));
                sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
                sb.append("</form>");
                sb.append("</div>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object moveDown() throws ServletException {

        if (attribute == null) {
            return main();
        }

        final List<Attribute> attributes = attribute.getOwner().getAttributes();
        EntityUtil.moveDown(attribute, attributes);

        if (linkback != null) {
            return new ExternalRedirect(linkback);
        }

        return main();
    }

    public Object moveUp() throws ServletException {

        if (attribute == null) {
            return main();
        }

        List<Attribute> attributes = attribute.getOwner().getAttributes();
        /*
           * Filter out attributes from other sections
           */
        CollectionsUtil.filter(attributes, new Predicate<Attribute>() {

            public boolean accept(Attribute e) {

                if (attribute.hasSection()) {
                    return attribute.getSection().equalsIgnoreCase(e.getSection());
                } else {
                    return !e.hasSection();
                }
            }
        });

        /*
           * If attribute is at the top of the section then we shall move the entire section up, by swapping this attribute with the lead attribute of the section above
           */
        if (attributes.indexOf(attribute) == 0) {
            attributes = Attribute.getLead(attribute.getOwner().getAttributes());
            EntityUtil.moveUp(attribute, attributes);
        } else {
            EntityUtil.moveUp(attribute, attributes);
        }

        if (linkback != null) {
            return new ExternalRedirect(linkback);
        }

        return main();
    }

    public Object randomiseValues() throws ServletException {

        if (attribute == null) {
            return main();
        }

        int n = attribute.randomiseValues();
        List<String> words = Arrays.asList(new String[]{"items", "randomised", "have", "been", String.valueOf(n)});
        Collections.shuffle(words);
        addMessage(StringHelper.implode(words, " ", true));

        return edit();
    }

    public Object remove() throws ServletException {

        int deleted = 0;
        for (Attribute attribute : attributesToDelete) {

            attribute.log(user, "Deleted through attribute page");
            attribute.delete();

            deleted++;
        }

        addMessage(deleted + " attributes deleted");
        return main();
    }

    public Object removeUnusedOptions() throws ServletException {

        if (attribute == null) {
            return main();
        }

        attribute.removeUnusedOptions();
        addMessage("Unused options removed");
        return edit();
    }

    public Object resetLocations() throws ServletException {

        if (attribute == null) {
            return main();
        }

        attribute.resetLocations();

        addMessage("Location values reset for all objects with values set on this attribute");
        clearParameters();
        return edit();
    }

    public Object save() throws ServletException {

        if (attribute == null) {
            return main();
        }

        test(new RequiredValidator(), "name");
        if (context.hasErrors()) {
            return edit();
        }

        if (!ObjectUtil.equal(attribute.getName(), name)) {
            attribute.setName(name);
            attribute.log(user, "Name changed to " + name);
        }

        if (isSuperman() && attribute.hasItemType()) {
            if (!ObjectUtil.equal(attribute.getItemType(), itemType)) {
                attribute.setItemType(itemType);
                attribute.log(user, "Item type changed to #" + itemType.getId() + " " + itemType.getName());
            }
        }

        if (!ObjectUtil.equal(attribute.getDescription(), description)) {
            attribute.setDescription(description);
            attribute.log(user, "Description changed - " + page);
        }

        attribute.setMulti(multi);

        if (attribute.isOptional() != optional) {
            attribute.setOptional(optional);
            attribute.log(user, "Optional changed - " + optional);
        }

        attribute.setFilter(filter);
        attribute.setLinkable(linkable);
        attribute.setSummary(summary);

        if (!ObjectUtil.equal(attribute.getSection(), section)) {
            attribute.setSection(section);
            attribute.log(user, "Section changed - " + section);
        }

        attribute.setCellsPerRow(cells);
        attribute.setCellSpan(cellSpan);
        attribute.setWidth(cols);
        attribute.setHeight(rows);

        if (isSuperman()) {

            if (!ObjectUtil.equal(attribute.getRegExp(), regExp)) {
                attribute.setRegExp(regExp);
                attribute.log(user, "Reg exp changed - " + regExp);
            }
        }

        // invoice
        attribute.setInvoiceColumn(invoiceColumn);
        attribute.setInvoiceName(invoiceName);

        attribute.setCustomOptionsOrder(customOptionsOrder);
        
        attribute.setCheckAllOptions(checkAllOptions);
        attribute.setCheckAllFrontLabel(checkAllFrontLabel);

        //iBex booking system
        attribute.setIbex(ibex);

        // registration
        attribute.setRegistration(registration);

        if (attribute.getPage() != page) {
            attribute.setPage(page);
            attribute.log(user, "Page changed - " + page);
        }

        attribute.setHideLabel(hideLabel);

        attribute.setSellPriceAdjustment(sellPriceAdjustment);

        attribute.setImagesOnAddListing(imagesOnAddListing);

        // location
        attribute.setLocation(location);

        if (!ObjectUtil.equal(attribute.getPrefix(), prefix)) {
            attribute.setPrefix(prefix);
            attribute.log(user, "prefix changed - " + prefix);

        }

        if (!ObjectUtil.equal(attribute.getSuffix(), suffix)) {
            attribute.setSuffix(suffix);
            attribute.log(user, "suffix changed - " + suffix);
        }

        attribute.setAssociationItemType(associationItemType);

        // auto value
        attribute.setAutoValue(autoValue);
        attribute.setRandomLength(randomLength);
        attribute.setIncrementStart(incrementStart);
        attribute.setIncrementStep(incrementStep);

        // numberical
        if (attribute.isNumerical()) {

            attribute.setMaxDp(maxDp);
            attribute.setMinDp(minDp);
            attribute.setMin(min);
            attribute.setMax(max);
            attribute.setStep(step);
            attribute.setGrouping(grouping);
        }

        attribute.setDisplayName(memberName);

        // emails
        attribute.setSubject(subject);
        attribute.setEmailBcc(emailBcc);
        attribute.setEmailCc(emailCc);

        // display / listings
        attribute.setDisplayable(displayable);
        attribute.setListable(listable);
        attribute.setEditable(editable);

        attribute.setDelivery(delivery);

        attribute.setFormat(format);

        // dates
        attribute.setYearRange(yearRange);
        attribute.setStartYear(startYear);

        /*
           * links
           */
        attribute.setLinkText(linkText);
        attribute.setLinkNewWindow(linkNewWindow);
        attribute.setNoFollow(noFollow);

        // selection
        attribute.setIntro(intro);

        attribute.setDisplayOnSubscriberOnly(displayOnSubscriberOnly);

        // text box size

        attribute.setAnyLabel(anyLabel);

        if (type != null) {
            if (!ObjectUtil.equal(attribute.getType(), type)) {
                attribute.setType(type);
                attribute.log(user, "Type changed to " + type);
            }
        }

        attribute.setDateStart(dateStart);
        attribute.setDateEnd(dateEnd);

        // checkout
        attribute.setCheckout(checkout);

        attribute.setSearchControl(searchControl);

        attribute.setCustomErrorMessage(customErrorMessage);

        attribute.save();

        if (resetAttributeValues != null) {
            attribute.resetAttributeValues(resetAttributeValues);
        }

        // add new options
        if (newOptions != null) {
            for (String string : newOptions.split("\n")) {
                string = string.trim();
                if (string.length() > 0) {
                    attribute.addOption(string);
                    attribute.log(user, "Option created - " + string);
                }
            }
        }

        reorder(positions);

        for (AttributeOption option : options) {
            attribute.removeOption(option);
            attribute.log(user, "Option deleted - " + option.getValue());
        }

        clearParameters();
        return new ActionDoc(context, "This attribute has been updated", new Link(AttributeHandler.class, "edit", "attribute", attribute));
    }
}