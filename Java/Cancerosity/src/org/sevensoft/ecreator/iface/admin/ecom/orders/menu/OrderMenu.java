package org.sevensoft.ecreator.iface.admin.ecom.orders.menu;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.ecreator.iface.admin.ecom.orders.OrderHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.OrderHistoryHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.OrderSearchHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.TransactHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Menu;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 4 Sep 2006 17:40:03
 *
 */
public class OrderMenu extends Menu {

	private final Order	order;

	public OrderMenu(Order order) {
		this.order = order;
	}

	@Override
	public List<LinkTag> getLinkTags(RequestContext context) {

		//		boolean superman = context.containsAttribute("superman");

		List<LinkTag> links = new ArrayList();

		links.add(new LinkTag(OrderHandler.class, null, "Details", "order", order));

		if (order.getPaymentType() == PaymentType.CardTerminal && order.hasCard()) {
			links.add(new LinkTag(TransactHandler.class, null, "Transact", "order", order));
		}
		
		links.add(new LinkTag(OrderHandler.class, "printInvoice", "Print invoice", "order", order));

		links.add(new LinkTag(OrderHistoryHandler.class, null, "Order history", "account", order.getAccount()));
		links.add(new LinkTag(OrderSearchHandler.class, null, "Order search"));

		return links;
	}
}
