package org.sevensoft.ecreator.iface.admin.items;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemTypeMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.adminsearch.AdminField;
import org.sevensoft.ecreator.model.items.adminsearch.AdminFieldType;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;

/**
 * @author sks 9 Jan 2007 15:36:27
 *
 */
@Path("admin-items-fields.do")
public class AdminFieldHandler extends AdminHandler {

	private ItemType		itemType;
	private AdminField	field;
	private AdminFieldType	fieldType;
	private Attribute		attribute;
    private boolean searchResultsEditable;

	public AdminFieldHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		if (itemType == null || fieldType == null) {
			return new ItemTypeHandler(context, itemType).edit();
		}

		itemType.addAdminField(fieldType);
		return new ItemTypeHandler(context, itemType).edit();
	}

	public Object delete() throws ServletException {

		if (field == null) {
			return index();
		}

		field.delete();
		return new ItemTypeHandler(context, field.getItemType()).edit();
	}

	@Override
	public Object main() throws ServletException {

		if (field == null) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit admin field", null);
		doc.setMenu(new EditItemTypeMenu(field.getItemType()));
		doc.addBody(new Body() {

            private void commands() {
                sb.append(new SubmitTag("Update"));
            }

			@Override
			public String toString() {

				sb.append(new FormTag(AdminFieldHandler.class, "save", "post"));
				sb.append(new HiddenTag("field", field));

                commands();

                sb.append(new AdminTable("General"));

				SelectTag fieldTypeTag = new SelectTag(context, "fieldType", field.getFieldType(), AdminFieldType.values());
				sb.append(new AdminRow("Field type", null, fieldTypeTag));

				switch (field.getFieldType()) {

				default:
					break;

				case Attribute:
					sb.append(new AdminRow("Attribute", "Attribute used by field type", new SelectTag(context, "attribute", field.getAttribute(),
							field.getItemType().getAttributes(), "-None set-")));

				}

                if (Module.EditInMass.enabled(context) && field.getFieldType().getTag(context, null) != null) {
                    sb.append(new AdminRow("Editable", "The field is editable through search results list", new BooleanRadioTag(context, "searchResultsEditable", field.isSearchResultsEditable())));
                }

				sb.append("</table>");

				commands();
				sb.append("</form>");

				return sb.toString();
			}
		});
		return doc;
	}

	public Object moveDown() throws ServletException {

		if (field == null) {
			return index();
		}

		List<AdminField> fields = field.getItemType().getAdminFields();
		EntityUtil.moveDown(field, fields);
		return new ItemTypeHandler(context, field.getItemType()).edit();
	}

	public Object moveUp() throws ServletException {

		if (field == null) {
			return index();
		}

		List<AdminField> fields = field.getItemType().getAdminFields();
		EntityUtil.moveUp(field, fields);
		return new ItemTypeHandler(context, field.getItemType()).edit();
	}

	public Object save() throws ServletException {

		if (field == null) {
			return index();
		}

		field.setFieldType(fieldType);
		field.setAttribute(attribute);
        field.setSearchResultsEditable(searchResultsEditable);
		field.save();

		clearParameters();
		addMessage("Field updated");
		return main();
	}
}
