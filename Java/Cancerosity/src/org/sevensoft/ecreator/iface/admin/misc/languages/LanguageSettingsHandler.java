package org.sevensoft.ecreator.iface.admin.misc.languages;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.extras.languages.Language;
import org.sevensoft.ecreator.model.extras.languages.LanguageSettings;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 17-Jun-2005 17:37:55
 * 
 */
@Path("admin-lanuages-settings.do")
public class LanguageSettingsHandler extends AdminHandler {

	private Language				language;
	private transient LanguageSettings	languageSettings;

	public LanguageSettingsHandler(RequestContext context) {
		super(context);
		languageSettings = LanguageSettings.getInstance(context);
	}

	public Object add() {

		if (language != null)
			languageSettings.addLanguage(language);

		clearParameters();
		return main();
	}

	@Override
	public Object main() {

		AdminDoc page = new AdminDoc(context, user, "Language settings", null);
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void languages() {

				sb.append(new AdminTable("Languages"));

				sb.append("<tr>");
				sb.append("<th>Language</th>");
				sb.append("<th>Remove</th>");
				sb.append("</tr>");

				for (Language language : languageSettings.getLanguages()) {

					sb.append("<tr>");
					sb.append("<td>" + language.toString() + "</td>");
					sb.append("<td>" + new LinkTag(LanguageSettingsHandler.class, "remove", "Remove", "language", language) + "</td>");
					sb.append("</tr>");
				}

				sb.append(new FormTag(LanguageSettingsHandler.class, "add", "get"));
				SelectTag addLanguageTag = new SelectTag(context, "language", null, Language.values(), "-Select language");
				sb.append("<tr><td width='300' colspan='3'>Add language " + addLanguageTag + " " + new SubmitTag("Add language") + "</td></tr>");
				sb.append("</form>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

                commands();
				languages();
				commands();

				return sb.toString();
			}

		});
		return page;

	}

	public Object remove() {

		if (language != null)
			languageSettings.removeLanguage(language);

		clearParameters();
		return main();
	}

}
