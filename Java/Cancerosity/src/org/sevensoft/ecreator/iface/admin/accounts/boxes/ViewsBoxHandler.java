package org.sevensoft.ecreator.iface.admin.accounts.boxes;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.interaction.views.ViewsBox;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 23 Sep 2006 19:54:11
 *
 */
@Path("boxes-members-views.do")
public class ViewsBoxHandler extends BoxHandler {

	private ViewsBox	box;
	private int			limit;

	public ViewsBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {

		box.setLimit(limit);
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new TableTag("form",1,0).setCaption("Member views box options"));

		sb.append("<tr><td width='300'><b>Limit</b><br/>How many views to show</td>");
		sb.append("<td>" + new TextTag(context, "limit", box.getLimit(), 4) + "</td></tr>");

		sb.append("<tr><td width='300'><b>Empty message</b><br/>"
				+ "A message to display when a member's profile has had no views, or if blank then it will be hidden.</td>");
		sb.append("<td>" + new TextTag(context, "emptyMessage", box.getEmptyMessage(), 40) + "</td></tr>");

		sb.append("</table>");
	}

}
