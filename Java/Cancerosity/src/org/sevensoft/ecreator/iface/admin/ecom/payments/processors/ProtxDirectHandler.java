package org.sevensoft.ecreator.iface.admin.ecom.payments.processors;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.ecom.payments.PaymentSettingsHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.ecom.payments.processors.protx.ProtxDirectProcessor;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 17 Apr 2007 08:04:02
 *
 */
@Path("admin-payments-processors-protxdirect.do")
public class ProtxDirectHandler extends ProcessorHandler {

	private ProtxDirectProcessor	processor;
	private String			username;
	private String			password;
	private String			clientId;
	private String			vendorName;
	private boolean			live;
	private boolean	showPost;

	public ProtxDirectHandler(RequestContext context) {
		super(context);
	}

	public Object delete() {
		return super.delete(processor);
	}

	@Override
	public Object main() throws ServletException {

		if (processor == null) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Protx direct", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update processor"));
				sb.append(new ButtonTag(ProtxDirectHandler.class, "delete", "Delete"));
				sb.append(new ButtonTag(PaymentSettingsHandler.class, null, "Return to payment settings"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("Protx direct"));

				sb.append(new AdminRow("Vendor name", "This is your vendor name as provided by protx.", new TextTag(context, "vendorName", processor
						.getVendorName(), 20)));

				sb
						.append(new AdminRow("Live mode", "Set this protx processor to live mode.", new BooleanRadioTag(context, "live", processor
								.isLive())));

				sb.append(new AdminRow("Show post", "Send transaction detils to protx for text mode.", new BooleanRadioTag(context, "showPost",
						processor.isShowPost())));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(ProtxDirectHandler.class, "save", "post"));
				sb.append(new HiddenTag("processor", processor));

                commands();
				general();
				commands();
				sb.append("</form>");

				return sb.toString();
			}
		});
		return doc;
	}

	public Object save() throws ServletException {

		if (processor == null) {
			return index();
		}

		processor.setVendorName(vendorName);
		processor.setLive(live);
		processor.setShowPost(showPost);
		processor.save();

		return new ActionDoc(context, "This processor has been updated", new Link(ProtxDirectHandler.class, null, "processor", processor));
	}
}
