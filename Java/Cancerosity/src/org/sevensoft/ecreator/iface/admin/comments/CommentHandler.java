package org.sevensoft.ecreator.iface.admin.comments;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.items.menus.ItemsMenu;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.categories.menu.CategoryMenu;
import org.sevensoft.ecreator.iface.admin.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.comments.Comment;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;

import javax.servlet.ServletException;

/**
 * User: Tanya
 * Date: 27.07.2010
 */
@Path("admin-comment-edit.do")
public class CommentHandler extends AdminHandler {

    private Seo seo;
    private Comment comment;
    private String commentString;
    private String title;
    private Item account;
    private Category category;
    private Item item;
    private String linkback;

    private boolean autoContentLinking;
    private boolean contentLinkingKeyword;

    public CommentHandler(RequestContext context) {
        super(context);
        this.seo = Seo.getInstance(context);
    }

    public Object main() throws ServletException {
        return edit();
    }

    public Object edit() throws ServletException {
        if (comment == null)
            return index();
        if (item == null && category == null) {
            return index();
        }
        AdminDoc doc;
        if (category != null) {
            doc = new AdminDoc(context, user, "Edit comment #" + comment.getIdString(), Tab.Categories);
            doc.setMenu(new CategoryMenu(category));
        } else {
            doc = new AdminDoc(context, user, "Edit comment #" + comment.getIdString(), Tab.getItemTab(item));
            doc.setMenu(new ItemsMenu(item.getItemType()));
        }

        doc.addBody(new Body() {

            private void commands() {
                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update comment"));

                sb.append(new ButtonTag(CommentHandler.class, "delete", "Delete comment", "comment", comment, "linkback", linkback)
                        .setConfirmation("Are you sure you want to delete this comment?"));

                if (comment.isAwaitingModeration()) {

                    sb.append(new ButtonTag(CommentModerationHandler.class, "main", "Returm to moderation list"));

                } else {

                    if (item != null) {
                        sb.append(new ButtonTag(ItemHandler.class, "comments", "Returm to list", "item", item));
                    } else if (category != null) {
                        sb.append(new ButtonTag(CategoryHandler.class, "comments", "Returm to list", "category", category));
                    }

                }
                sb.append("</div>");
            }

            private void general() {
                sb.append(new AdminTable("General"));

                sb.append(new AdminRow("Account", new LinkTag(ItemHandler.class, "edit", comment.getAuthorName(), "item", comment.getAccount())));
                sb.append(new AdminRow("Title", new TextTag(context, "title", comment.getTitle(), 80)));
                sb.append(new AdminRow("Comment", new TextAreaTag(context, "commentString", comment.getComment(), 80, 5)));

                sb.append("</table>");
            }

            private void linking() {

                // auto content linking overrides
                if (seo.isAutoContentLinkingOverride()) {

                    sb.append(new AdminTable("Auto Linking"));

                    sb.append(new AdminRow("Auto content linking", "Enable or disable auto content linking in this comment."
                            , new BooleanRadioTag(context, "autoContentLinking", comment.isAutoContentLinking())));

                    sb.append(new AdminRow("Auto content linking keyword", "Include this comment as a keyword for auto content linking.",
                            new BooleanRadioTag(context, "contentLinkingKeyword", comment.isContentLinkingKeyword())));

                    sb.append("</table>");
                }
            }

            @Override
            public String toString() {
                sb.append(new FormTag(CommentHandler.class, "save", "multi"));
                sb.append(new HiddenTag("comment", comment));
                sb.append(new HiddenTag("linkback", linkback));
                sb.append(new HiddenTag("item", item));
                sb.append(new HiddenTag("category", category));
                commands();
                general();
                linking();
                commands();
                sb.append("</form>");

                return sb.toString();
            }
        });

        return doc;
    }

    public Object delete() throws ServletException {
        if (comment == null) {
            return index();
        }
        comment.delete();
        if (linkback != null) {
            return new ActionDoc(context, "The comment has been deleted.", linkback);
        }
        return index();
    }

    public Object save() throws ServletException {
        if (comment == null)
            return index();

        comment.setComment(commentString);
        comment.setTitle(title);

        // content linking
        if (seo.isAutoContentLinkingOverride()) {

            if (comment.isAutoContentLinking() != autoContentLinking) {
               comment.setAutoContentLinking(autoContentLinking);
                logger.fine("Auto content linking set to " + autoContentLinking);
            }
            comment.setContentLinkingKeyword(contentLinkingKeyword);
        }

        comment.save();

        return edit();
    }
}
