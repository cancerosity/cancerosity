package org.sevensoft.ecreator.iface.admin.attributes;

import java.util.List;
import java.util.Map;
import java.io.IOException;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.media.images.panels.ImageAdminPanel;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.frontend.ErrorDoc;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeOption;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.ImageUtil;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks
 */
@Path("admin-attributes-options.do")
public class AttributeOptionHandler extends AdminHandler {

	private List<Upload>		imageUploads;
	private AttributeOption		option;
	private Attribute			attribute;
	private String			value;
	private Map<Integer, String>	imageDescriptions;
	private boolean			hidden;
	private boolean			showValue;
	private boolean			defaultForSearch;
	private String			location;
	private String			revealIds;
    private String rnid;
	private boolean			defaultForNew;
	private int				score;
	private String			imageFilename;
	private List<String>		imageUrls;

	public AttributeOptionHandler(RequestContext context) {
		super(context);
	}

	public AttributeOptionHandler(RequestContext context, AttributeOption option) {
		super(context);
		this.option = option;
	}

	public Object delete() throws ServletException {

		if (option == null)
			return main();

		attribute = option.getAttribute();
		attribute.removeOption(option);
		return new AttributeHandler(context, attribute).edit();
	}

	public Object main() throws ServletException {

		if (option == null) {
			return new AttributeHandler(context).main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit attribute option", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update option"));
				sb.append(new ButtonTag(AttributeOptionHandler.class, "delete", "Delete this option", "option", option)
						.setConfirmation("Are you sure you want to delete this option?"));

				sb.append(new ButtonTag(AttributeHandler.class, "edit", "Return to attribute", "attribute", option.getAttribute()));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Value", "The value of this option.", new TextTag(context, "value", option.getValue(), 40)));

				if (option.hasApprovedImages()) {
					sb.append(new AdminRow("Show value", "Show textual value in addition to the image icon.", new BooleanRadioTag(context,
							"showValue", option.isShowValue())));
				}

				sb.append(new AdminRow("Default for search", "Select this option to be the default selected when showing a user a search form.",
						new BooleanRadioTag(context, "defaultForSearch", option.isDefaultForSearch())));

				sb.append(new AdminRow("Default for new",
						"Select this option to be automatically set when an object is created that uses this attribute.", new BooleanRadioTag(
								context, "defaultForNew", option.isDefaultForNew())));

				if (option.getAttribute().isLocation()) {

					sb.append(new AdminRow("Location", "Enter the location to be used when this attribute option is selected.", new TextTag(context,
							"location", option.getLocation())));

				}

				sb.append(new AdminRow("Score", "Value to used when calculating the score for the item.", new TextTag(context, "score", option
						.getScore())));

				sb.append(new AdminRow("Reveal Ids", "Hide these attributes until revealed by selecting this option", new TextTag(context, "revealIds",
						StringHelper.implode(option.getRevealIds(), ",", true))));

                if (option.getAttribute().isIbex())
                    sb.append(new AdminRow("iBex region", "The region id (rnid) used for the iBex booking system", new TextTag(context, "rnid",
                            option.getRnid())));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(AttributeOptionHandler.class, "save", "multi"));
				sb.append(new HiddenTag("option", option));

                commands();
				general();
				sb.append(new ImageAdminPanel(context, option));

				commands();

				sb.append("</form>");
				return sb.toString();
			}

		});

		return doc;
	}

	public Object moveDown() throws ServletException {

		if (option == null) {
			return main();
		}

		EntityUtil.moveDown(option, option.getAttribute().getOptions());
		return new AttributeHandler(context, option.getAttribute()).edit();
	}

	public Object moveUp() throws ServletException {

		if (option == null) {
			return main();
		}

		EntityUtil.moveUp(option, option.getAttribute().getOptions());
		return new AttributeHandler(context, option.getAttribute()).edit();
	}

	public Object save() throws ServletException {

		if (option == null) {
			return main();
		}

		option.setValue(value);
		option.setShowValue(showValue);
		option.setDefaultForSearch(defaultForSearch);
		option.setLocation(location);
		option.setDefaultForNew(defaultForNew);
		option.setRevealIds(StringHelper.explodeIntegers(revealIds, "\\D"));
        if (option.getAttribute().isIbex())
            option.setRnid(rnid);
		option.setScore(score);
		option.save();

		try {
			ImageUtil.save(context, option, imageUploads, imageUrls, imageFilename, true);
		} catch (ImageLimitException e) {
			e.printStackTrace();
		} catch (IOException e) {
            return new ErrorDoc(context, "The image you are attempting to upload is in CYMK colour mode. " +
                    "This is unsopported currently, please use a photo editing application (e.g. Photoshop) to convert the file to RGB colour. For more information or assistance please contact support.");
        }

        clearParameters();
		return main();
	}

}