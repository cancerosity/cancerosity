package org.sevensoft.ecreator.iface.admin.feeds.bespoke.prices;

import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.Feed;
import org.sevensoft.ecreator.model.feeds.bespoke.prices.pricerunner.PriceRunnerFeed;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 3 Aug 2006 11:07:23
 *
 */
@Path("admin-feeds-pricerunner.do")
public class PriceRunnerFeedHandler extends FeedHandler {

	private PriceRunnerFeed	feed;
	private Attribute		isbnAttribute;
	private Attribute		mpnAttribute;
	private Attribute		manufAttribute;
	private ItemType		itemType;
	private Attribute		upcAttribute;

	public PriceRunnerFeedHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Feed getFeed() {
		return feed;
	}

	@Override
	protected void saveSpecific() {

		feed.setManufAttribute(manufAttribute);
		feed.setMpnAttribute(mpnAttribute);
		feed.setIsbnAttribute(isbnAttribute);
		feed.setItemType(itemType);
		feed.setUpcAttribute(upcAttribute);
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Pricerunner specifics"));

		sb.append(new AdminRow("Item type", "Select the item type to import to", new SelectTag(context, "itemType", feed.getItemType(), ItemType
				.getSelectionMap(context), "-None set-")));

		if (feed.hasItemType()) {

			SelectTag attributeTag = new SelectTag(context, "mpnAttribute", feed.getMpnAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append(new AdminRow("Mpn attribute", "Set the attribute to be used for manufacturer part number.", attributeTag));

			attributeTag = new SelectTag(context, "manufAttribute", feed.getManufAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append(new AdminRow("Manufacturer attribute", "Set the attribute to be used for the manufacturer name.", attributeTag));

			attributeTag = new SelectTag(context, "isbnAttribute", feed.getIsbnAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append(new AdminRow("Isbn attribute", "Set the attribute to be used for the ISBN number.", attributeTag));

			attributeTag = new SelectTag(context, "upcAttribute", feed.getUpcAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append(new AdminRow("Upc attribute", "Set the attribute to be used for the UPC code.", attributeTag));
		}

		sb.append("</table>");
	}
}
