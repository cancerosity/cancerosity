package org.sevensoft.ecreator.iface.admin.extras.maps;

import java.io.IOException;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.extras.mapping.google.GoogleMapIcon;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 24 Apr 2007 22:21:39
 *
 */
@Path("admin-google-map-icons.do")
public class GoogleMapIconsHandler extends AdminHandler {

	private GoogleMapIcon	icon;
	private String		filename;
	private int			width, height;
	private int			windowAnchorY;
	private int			windowAnchorX;
	private int			anchorY;
	private int			anchorX;
	private String		name;
	private ItemType		itemType;

	public GoogleMapIconsHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		if (itemType == null) {
			return new GoogleMapSettingsHandler(context).main();
		}

		new GoogleMapIcon(context, itemType, "Icon for " + itemType.getName());
		return new ActionDoc(context, "A new icon has been created", new Link(GoogleMapSettingsHandler.class));
	}

	@Override
	public Object main() throws ServletException {

		if (icon == null) {
			return new GoogleMapSettingsHandler(context).main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit icon", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update icon"));
				sb.append(new ButtonTag(GoogleMapIconsHandler.class, "delete", "Delete icon", "icon", icon)
						.setConfirmation("Are you sure you want to delete this icon?"));
				sb.append(new ButtonTag(GoogleMapIconsHandler.class, null, "Return to menu"));

				sb.append("</div>");
			}

			private void details() {

				sb.append(new AdminTable("Icon settings"));

				sb.append(new AdminRow("Name", "", new TextTag(context, "name", icon.getName(), 40)));

				sb.append(new AdminRow("Filename", "Relative to template-data", new TextTag(context, "filename", icon.getFilename(), 40)));
				sb.append(new AdminRow("Width", "", new TextTag(context, "width", icon.getWidth(), 12).setReadOnly(true)));
				sb.append(new AdminRow("Height", "", new TextTag(context, "height", icon.getHeight(), 12).setReadOnly(true)));

				sb.append(new AdminRow("anchorX", "", new TextTag(context, "anchorX", icon.getAnchorX(), 12)));
				sb.append(new AdminRow("anchorY", "", new TextTag(context, "anchorY", icon.getAnchorY(), 12)));

				sb.append(new AdminRow("windowAnchorX", "", new TextTag(context, "windowAnchorX", icon.getWindowAnchorX(), 12)));
				sb.append(new AdminRow("windowAnchorY", "", new TextTag(context, "windowAnchorY", icon.getWindowAnchorY(), 12)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(GoogleMapIconsHandler.class, "save", "POST"));
				sb.append(new HiddenTag("icon", icon));

                commands();
				details();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (icon == null) {
			return new GoogleMapSettingsHandler(context).main();
		}

		try {

			icon.setName(name);
			icon.setFilename(filename);
			icon.setAnchorX(anchorX);
			icon.setAnchorY(anchorY);
			icon.setWindowAnchorX(windowAnchorX);
			icon.setWindowAnchorY(windowAnchorY);
			icon.save();

			return new ActionDoc(context, "This icon has been updated", new Link(GoogleMapIconsHandler.class, null, "icon", icon));

		} catch (IOException e) {

			e.printStackTrace();
			addError(e);

			return main();
		}

	}
}
