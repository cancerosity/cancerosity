package org.sevensoft.ecreator.iface.admin.reminders;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Transformer;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.number.IntValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.ecom.bookings.menu.BookingMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.reminders.HolidayReminderPeriod;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;
import java.util.List;

/**
 * User: MeleshkoDN
 * Date: 06.09.2007
 * Time: 19:28:04
 */
@Path("admin-holiday-reminder-timings.do")
public class HolidayReminderSettingsEditor extends AdminHandler {

    private final transient List<HolidayReminderPeriod> periods;

    private String message;
    private String name;
    private int timing;

    public HolidayReminderSettingsEditor(RequestContext context) {
        super(context);
        this.periods = HolidayReminderPeriod.getAllPeriods(context);
    }

    public Object main() throws ServletException {
        AdminDoc doc = new AdminDoc(context, user, "Holiday Reminder Settings", Tab.Bookings);
        doc.setMenu(new BookingMenu());
        doc.setIntro("Edit timings and messages of holiday reminders");
        doc.addBody(new Body() {

            private void commands() {
                sb.append("<div align='center' class='actions'>");
                sb.append(new ButtonTag(HolidayReminderSettingsEditor.class, "addNew", "Add new"));
                sb.append(new SubmitTag("Save changes"));
                sb.append("</div>");
            }

            @Override
            public String toString() {
                sb.append(new FormTag(HolidayReminderSettingsEditor.class, "save", "post"));

                commands();

                sb.append(new AdminTable("Holiday Reminder Settings"));
                sb.append("<tr>");
                sb.append("<th width='20' align='center'>Active</th>");
                sb.append("<th width='50' align='center'>Timing</th>");
                sb.append("<th>Message</th>");
                sb.append("</tr>");
                List<Integer> periodIds = CollectionsUtil.transform(periods, new Transformer<HolidayReminderPeriod, Integer>() {
                    public Integer transform(HolidayReminderPeriod obj) {
                        return obj.getId();
                    }
                });
                for (HolidayReminderPeriod period : periods) {
                    sb.append("<tr>");
                    ButtonTag buttonTag = new ButtonTag("Copy To All");
                    buttonTag.setId("button_" + period.getId());
                    String onclickAction = "var message = this.form.elements['period_message_" + period.getId() + "'].value;";
                    for (Integer id : periodIds) {
                        onclickAction += "this.form.elements['period_message_" + id + "'].value = message;";
                    }
                    buttonTag.setOnClick(onclickAction);
                    sb.append("<td width='20' align='center'>" + new CheckTag(context, "period_active_" + period.getId(), period, period.isActive()) +
                            "<br/> " + buttonTag + "</td>");
                    sb.append("<td width='50' align='center'>" + period.getDisplayName() + "</td>");
                    sb.append("<td>" + new TextAreaTag(context, "period_message_" + period.getId(), period.getMessage(), 80, 4) + " " + new ErrorTag(context, "period_message_" + period.getId(), "<br/>") + "</td>");
                    sb.append("</tr>");
                }
                sb.append("</table>");

                commands();

                sb.append("</form>");
                return sb.toString();
            }
        });
        return doc;
    }

    private void validateInput() {
        for (HolidayReminderPeriod period : periods) {
            test(new RequiredValidator(), "period_message_" + period.getId());
        }
    }

    public Object save() throws ServletException {

        validateInput();

        if (hasErrors()) {
            logger.fine("[HolidayReminderBlockHandler] errors=" + context.getErrors());
            return main();
        }

        for (HolidayReminderPeriod period : periods) {
            boolean checked = context.getParameter("period_active_" + period.getId()) != null;
            period.setActive(checked);
            String message = context.getParameter("period_message_" + period.getId());
            period.setMessage(message);
            period.save();
        }
        return new ActionDoc(context, "Changes saved", new Link(HolidayReminderSettingsEditor.class));
    }

    public Object addNew() {

        AdminDoc doc = new AdminDoc(context, user, "Add new period", Tab.Bookings);
        doc.setIntro("Create a new period for holiday reminders");

        doc.setMenu(new BookingMenu());

        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new FormTag(HolidayReminderSettingsEditor.class, "doCreate", "POST"));

                sb.append(new TableTag("simplebox", "center"));
                sb.append("<tr><td align='center'>Enter timeout (in days)</td></tr>");
                sb.append("<tr><td align='center'>" + new TextTag(context, "timing", null, 3) + new ErrorTag(context, "timing", "<br/>") + "</td></tr>");
                sb.append("<tr><td align='center'>Enter name</td></tr>");
                sb.append("<tr><td align='center'>" + new TextTag(context, "name", null, 12) + new ErrorTag(context, "name", "<br/>") + "</td></tr>");
                sb.append("<tr><td align='center'>Enter message</td></tr>");
                sb.append("<tr><td align='center'>" + new TextAreaTag(context, "message", 80, 4) + new ErrorTag(context, "message", "<br/>") + "<br/><br/></td></tr>");
                sb.append("</table>");
                sb.append("<div align='center' class='actions'>" + new SubmitTag("Save") + " " + new ButtonTag(HolidayReminderSettingsEditor.class, "main", "Return to reminders settings") + "</div>");

                sb.append("</form>");
                return sb.toString();
            }

        });
        return doc;
    }

    public Object doCreate() throws ServletException {
        test(new RequiredValidator(), "name");
        test(new RequiredValidator(), "timing");
        test(new IntValidator(), "timing");
        test(new RequiredValidator(), "message");
        if (hasErrors()) {
            logger.fine("[HolidayReminderBlockHandler] errors=" + context.getErrors());
            return addNew();
        }
        HolidayReminderPeriod period = new HolidayReminderPeriod(context);
        period.setTiming(timing);
        period.setDisplayName(name);
        period.setMessage(message);
        period.save();
        return new ActionDoc(context, "Period was successfully saved", new Link(HolidayReminderSettingsEditor.class));
    }
}
