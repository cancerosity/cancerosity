package org.sevensoft.ecreator.iface.admin.accounts;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;

/**
 * @author sks
 */
@Path("admin-accounts-paymenttypes.do")
public class AccountPaymentTypesHandler extends AdminHandler {

	private ItemType	itemType;

	public AccountPaymentTypesHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (itemType == null) {
			return index();
		}

		final PaymentSettings paymentSettings = PaymentSettings.getInstance(context);

		AdminDoc page = new AdminDoc(context, user, "Item type: Payment Types", Tab.getItemTab(itemType));
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update payment types"));
				sb.append(new ButtonTag(ItemTypeHandler.class, "edit", "Return to item type", "itemType", itemType));

				sb.append("</div>");
			}

			private void payments() {

				sb.append(new AdminTable("Payment types"));

				sb.append(new AdminRow("Override payment types", "Change the accepted payment types for this group only.", new BooleanRadioTag(context,
						"overridePaymentTypes", itemType.getAccountModule().isOverridePaymentTypes())));

				for (PaymentType paymentType : paymentSettings.getPaymentTypes()) {

					sb.append("<tr>");
					sb.append("<td>" + paymentType + "</td>");
					sb.append("<td>" + new CheckTag(context, "paymentTypes", paymentType, itemType.getAccountModule().hasPaymentType(paymentType))
							+ "</td>");
					sb.append("</tr>");

				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

                commands();
				payments();
				commands();

				return sb.toString();
			}

		});
		return page;
	}

	public Object save() throws ServletException {

		clearParameters();
		return main();
	}
}
