package org.sevensoft.ecreator.iface.admin.extras.messages;

import java.io.IOException;

import javax.servlet.ServletException;

import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.crm.forms.SubmissionHandler;
import org.sevensoft.ecreator.iface.admin.crm.jobsheets.JobHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.OrderHandler;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.crm.forms.submission.Submission;
import org.sevensoft.ecreator.model.crm.jobsheets.Job;
import org.sevensoft.ecreator.model.crm.messages.Msg;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 13-Jul-2005 16:24:50
 * 
 */
@Path("admin-messages.do")
public class MessageHandler extends AdminHandler {

	private Upload		upload;
	private Msg			message;
	private String		body;
	private Submission	submission;
	private Order		order;
	private Job			job;
	private Item		item;

	public MessageHandler(RequestContext context) {
		super(context);
	}

	public Object addAttachment() throws ServletException {

		/*
		 * Will add attachment to the last message if it is a user message
		 */

		if (upload == null || message == null)
			return main();

		try {

			message.addAttachment(upload, false);
			addMessage("Attachment '" + upload.getFilename() + "' added");

		} catch (IOException e) {
			addError(e);

		} catch (AttachmentLimitException e) {
			addError(e);

		} catch (AttachmentExistsException e) {
			addError(e);

		} catch (AttachmentTypeException e) {
			addError(e);
		}

		return getHandler();
	}

	public Object addMessage() throws ServletException {

		if (body == null) {
			return getHandler();
		}

		if (submission != null) {
			message = submission.addMessage(user, body);
		}

		else if (order != null) {
			message = order.addMessage(user, body);
		}

		else if (job != null) {
			message = job.addMessage(user, body);
		}

		else if (item != null) {
			message = item.addMessage(user, body);
		}

		else {
			throw new RuntimeException("Unknown message owner");
		}

		addMessage("Your message has been added.");
		clearParameters();

		return getHandler();
	}

	private Object getHandler() throws ServletException {

		ActionDoc doc = null;

		if (message == null) {

			Link link = null;

			if (order != null) {
				link = new Link(OrderHandler.class, null, "order", order);
			}

			if (job != null) {
				link = new Link(JobHandler.class, null, "job", job);
			}

			if (submission != null) {
				link = new Link(SubmissionHandler.class, "view", "submission", submission);
			}

			if (link != null) {
				doc = new ActionDoc(context, "Thank you", link);
			}

		} else {

			Link link = null;

			if (message.hasOrder()) {
				link = new Link(OrderHandler.class, null, "order", message.getOrder());
			}

			if (message.hasJob()) {
				link = new Link(JobHandler.class, null, "job", message.getJob());
			}

			if (message.hasSubmission()) {
				link = new Link(SubmissionHandler.class, "view", "submission", message.getSubmission());
			}

			if (link != null) {
				doc = new ActionDoc(context, "Your message has been added", link);
			}

		}

		if (doc != null) {
			return doc;
		}

		return index();
	}

	@Override
	public Object main() throws ServletException {
		return getHandler();
	}

	public Object setPublic() throws ServletException {

		if (message == null) {
			return main();
		}

		try {
			message.setPublic(true);

		} catch (EmailAddressException e) {
			addError(e);
		} catch (SmtpServerException e) {
			addError(e);
		}

		return getHandler();
	}
}
