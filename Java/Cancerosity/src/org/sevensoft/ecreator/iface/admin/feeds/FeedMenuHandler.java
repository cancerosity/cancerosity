package org.sevensoft.ecreator.iface.admin.feeds;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.model.feeds.ExportFeed;
import org.sevensoft.ecreator.model.feeds.Feed;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 3 Aug 2006 10:39:53
 *
 */
@Path("admin-feeds.do")
public class FeedMenuHandler extends AdminHandler {

	private Class	feedClass;
	private int		feedId;

	public FeedMenuHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		if (feedClass != null) {

			try {

				Constructor c = feedClass.getConstructor(new Class[] { RequestContext.class, Boolean.TYPE });
				Feed feed = (Feed) c.newInstance(new Object[] { context, true });

				addMessage("New feed created: " + feed.getName());
				return main();

			} catch (InstantiationException e) {
				addError(e);

			} catch (IllegalAccessException e) {
				addError(e);

			} catch (IllegalArgumentException e) {
				addError(e);

			} catch (SecurityException e) {
				addError(e);

			} catch (InvocationTargetException e) {
				addError(e);

			} catch (NoSuchMethodException e) {
				addError(e);
			}

		}

		return main();
	}

	@Override
	public Object main() throws ServletException {

        if (!(Module.Feeds.enabled(context) && (isSuperman() || Module.UserFeeds.enabled(context)))) {
			return new DashboardHandler(context).main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Feeds", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void feeds() {

				List<Feed> feeds = Feed.getAll(context);

				sb.append(new AdminTable("Feeds"));

				int span = 5;
				sb.append("<tr>");

				if (Module.UserFeeds.enabled(context) || isSuperman()) {
					sb.append("<th>Position</th>");
					span++;
				}

				sb.append("<th>Name</th>");
				sb.append("<th width='120'>Last run time</th>");
				sb.append("<th>Data</th>");
				sb.append("<th>Run</th>");
				sb.append("<th width='20'></th>");
				sb.append("</tr>");

				PositionRender pr = new PositionRender("feed");
				for (Feed feed : feeds) {

					Class handlerClass = feed.getClass().getAnnotation(HandlerClass.class).value();

					sb.append(new FormTag(handlerClass, "run", "multi"));
					sb.append(new HiddenTag("feed", feed));

					sb.append("<tr>");

					if (Module.UserFeeds.enabled(context) || isSuperman()) {
						sb.append("<td>" + pr.render(handlerClass, feed) + "</td>");
					}

					String editLink;
					if (Module.UserFeeds.enabled(context) || isSuperman()) {
						editLink = new LinkTag(handlerClass, null, new ImageTag("files/graphics/admin/spanner.gif"), "feed", feed) + " ";
					} else {
						editLink = "";
					}

					sb.append("<td>" + editLink + feed.getName() + "</td>");
					sb.append("<td>" + feed.getLastRuntimeString("HH:mm dd-MMM-yyyy") + "</td>");
					sb.append("<td>");

					if (feed instanceof ImportFeed) {

						sb.append(new FileTag("upload", 40));

					} else {

                        String exportUrl = ((ExportFeed) feed).getExportUrl();
                        if (exportUrl != null) {
                            String previewUrl = ((ExportFeed) feed).getPreviewUrl();
                            sb.append(new LinkTag(previewUrl, "Preview URL").toString());

                            sb.append("<br>");
                            sb.append(new LinkTag(exportUrl, "Export URL").toString());
                        } else {
                            sb.append("Preview items count ").append(new TextTag(context, "previewCount", 0, 5));
                        }

                    }

					sb.append("</td>");

					sb.append("<td>" + new SubmitTag("Run").setConfirmation("Please confirm you want to run this feed?") + "</td>");
					sb.append("<td width='20'>"
							+ new LinkTag(handlerClass, "delete", new DeleteGif(), "feed", feed)
									.setConfirmation("Please confirm you want to delete this feed?") + "</td>");

					sb.append("</tr>");
					sb.append("</form>");
				}

				sb.append(new FormTag(FeedMenuHandler.class, "create", "post"));

				// ADD FEED 
				if (isSuperman()) {

					List<Class> classes = Feed.getClasses(context);
					if (!classes.isEmpty()) {

						SelectTag tag = new SelectTag(context, "feedClass");
						tag.addOptions(classes);
						tag.setLabelComparator();

						sb.append("<tr><td colspan='" + span + "'>" + tag + new SubmitTag("Add feed") + "</td></tr>");

					}
				}

				sb.append("</form>");
				sb.append("</table>");
			}

			@Override
			public String toString() {

                commands();
				feeds();
				commands();

				return sb.toString();
			}
		});

		return doc;
	}
}
