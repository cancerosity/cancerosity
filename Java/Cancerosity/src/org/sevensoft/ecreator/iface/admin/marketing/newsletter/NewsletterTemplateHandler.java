package org.sevensoft.ecreator.iface.admin.marketing.newsletter;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.marketing.MarketingMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.marketing.newsletter.templates.NewsletterTemplate;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 13 Sep 2006 23:32:12
 *
 */
@Path("admin-newsletters-templates.do")
public class NewsletterTemplateHandler extends AdminHandler {

	private NewsletterTemplate	template;
	private String			bottom;
	private String			top;
	private String			name;

	public NewsletterTemplateHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		new NewsletterTemplate(context, "-New newsletter template-");
		return main();
	}

	public Object edit() throws ServletException {

		if (template == null)
			return main();

		AdminDoc page = new AdminDoc(context, user, "Edit newsletter template: " + template.getName(), null);
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update template"));
				sb.append(new ButtonTag(NewsletterTemplateHandler.class, "delete", "Delete template", "template", template)
						.setConfirmation("Are you sure want to delete this template?"));
				sb.append(new ButtonTag(NewsletterTemplateHandler.class, null, "Return to menu"));

				sb.append("</div>");
			}

			private void markup() {

				sb.append(new TableTag("form",1,0).setCaption("Edit markup"));

				sb.append("<tr><td>Name</td><td>" + new TextTag(context, "name", template.getName(), 40) + "</td></tr>");
				sb.append("<tr><td>Top</td><td>" + new TextAreaTag(context, "top", template.getTop(), 80, 10) + "</td></tr>");
				sb.append("<tr><td>Bottom</td><td>" + new TextAreaTag(context, "bottom", template.getBottom(), 80, 10) + "</td></tr>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(NewsletterTemplateHandler.class, "save", "POST"));
				sb.append(new HiddenTag("template", template));

                commands();
				markup();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return page;
	}

	@Override
	public Object main() throws ServletException {

		if (!isSuperman())
			return new DashboardHandler(context).main();

		AdminDoc page = new AdminDoc(context, user, "Newsletter templates", Tab.Marketing);
		page.setMenu(new MarketingMenu());
		page.addBody(new Body() {

			private void templates() {

				sb.append(new TableTag("form",1,0).setCaption("Templates"));

				for (NewsletterTemplate template : NewsletterTemplate.get(context)) {

					sb.append("<tr><td>"
							+ template.getName()
							+ "</td><td width='120'>"
							+ new ButtonTag(NewsletterTemplateHandler.class, "edit", "Edit", "template", template)
							+ "</td><td width='120'>"
							+ new ButtonTag(NewsletterTemplateHandler.class, "delete", "Delete", "template", template)
									.setConfirmation("Are you sure you want to delete this template?") + "</td></tr>");

				}

				sb.append("<tr><td colspan='3'>Create template " + new ButtonTag(NewsletterTemplateHandler.class, "create", "Create") + "</td></tr>");

				sb.append("</form>");
				sb.append("</table>");
			}

			@Override
			public String toString() {

				templates();

				return sb.toString();
			}
		});
		return page;
	}

	public Object save() throws ServletException {

		if (template == null)
			return main();

		template.setTop(top);
		template.setBottom(bottom);
		template.setName(name);
		template.save();

		addMessage("This newsletter template has been updated with your changes.");
		clearParameters();
		return edit();
	}
}
