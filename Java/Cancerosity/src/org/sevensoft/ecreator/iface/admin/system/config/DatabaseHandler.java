package org.sevensoft.ecreator.iface.admin.system.config;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 19 Sep 2006 14:14:23
 *
 */
@Path("admin-database.do")
public class DatabaseHandler extends AdminHandler {

	private String	table;

	public DatabaseHandler(RequestContext context) {
		super(context);
	}

	public Object drop() throws ServletException {

		if (!isSamIp() && !context.isLocalhost()) {
			return HttpServletResponse.SC_FORBIDDEN;
		}

		if (table == null) {
			return main();
		}

		new Query(context, "drop table `" + table + "`").run();
		addMessage("Table '" + table + "' has been dropped");
		return main();
	}

	public Object empty() throws ServletException {

		if (!isSamIp() && !context.isLocalhost()) {
			return HttpServletResponse.SC_FORBIDDEN;
		}

		if (table == null) {
			return main();
		}

		new Query(context, "truncate table `" + table + "`").run();
		addMessage("Table '" + table + "' has been truncated");
		return main();
	}

	@Override
	public Object main() throws ServletException {

		if (!isSamIp() && !context.isLocalhost()) {
			return HttpServletResponse.SC_FORBIDDEN;
		}

		AdminDoc doc = new AdminDoc(context, user, "Database", null);
		doc.addBody(new Body() {

			private void database() {

				sb.append(new AdminTable("Database"));

				Query q = new Query(context, "show tables");
				for (String table : q.getStrings()) {

					q = new Query(context, "select count(*) from `" + table + "`");
					int count = q.getInt();

					sb.append("<tr><td>" + table + "</td><td>" + count + "</td>");
					sb.append("<td>" + new LinkTag(DatabaseHandler.class, "empty", "Empty", "table", table).setConfirmation("Uh oh") + "</td>");
					sb.append("<td>" + new LinkTag(DatabaseHandler.class, "drop", "Drop", "table", table).setConfirmation("Triple uh oh") + "</td>");
					sb.append("</tr>");

				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				database();

				return sb.toString();
			}
		});

		return doc;
	}
}
