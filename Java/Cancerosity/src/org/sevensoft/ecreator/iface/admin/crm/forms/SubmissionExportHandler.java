package org.sevensoft.ecreator.iface.admin.crm.forms;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.submission.Submission;
import org.sevensoft.ecreator.model.crm.forms.export.SubmissionExporter;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.StreamResult;
import com.ricebridge.csvman.CsvManager;
import com.ricebridge.csvman.CsvSaver;

/**
 * @author sks 22 May 2007 02:17:23
 *
 */
@Path("admin-forms-subs-export.do")
public class SubmissionExportHandler extends AdminHandler {

	private Form	form;

	public SubmissionExportHandler(RequestContext context) {
		super(context);
	}

	@Override
    public Object main() throws ServletException {
        List<Form> forms = Form.get(context);
        if (forms != null) {

            File file = null;
            FileWriter writer = null;
            CsvManager csv = new CsvManager();
            CsvSaver saver = null;
            try {
                file = File.createTempFile("sub_export", ".csv");
                if (!forms.isEmpty()) {
                    writer = new FileWriter(file);
                    saver = csv.makeSaver(writer);
                    saver.begin();
                    
                    if (Module.FormsExportHeaders.enabled(context)) {
                        SubmissionExporter exporter = new SubmissionExporter(context, forms.get(0), forms.get(0).getFields());
                        exporter.header(saver);
                    }
                }
                for (Form form : forms) {

                    List<Submission> submissions = form.getSubmissions();
                    if (submissions != null && !submissions.isEmpty()) {

                        SubmissionExporter exporter = new SubmissionExporter(context, form, form.getFields(), submissions);

// fix NPE
//                        if (file == null) {
//                            file = File.createTempFile("sub_export", ".csv");
//                        }
//                        if (writer == null) {
//                            writer = new FileWriter(file);
//                        }
//                        if (saver == null) {
//                            saver = csv.makeSaver(writer);
//                            saver.begin();
//                        }

                        exporter.export(saver);
                    }
                }
                if (saver != null) {
                    saver.end();
                }

                return new StreamResult(file, "text/csv", "export.csv", StreamResult.Type.Stream);

            } catch (IOException e) {
                e.printStackTrace();
                return e.toString();
            } finally {

                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return null;
    }
}
