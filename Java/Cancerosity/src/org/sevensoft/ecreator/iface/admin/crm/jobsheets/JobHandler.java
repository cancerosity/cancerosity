package org.sevensoft.ecreator.iface.admin.crm.jobsheets;

import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.crm.jobsheets.menu.JobsMenu;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.crm.jobsheets.Job;
import org.sevensoft.ecreator.model.crm.jobsheets.Job.Priority;
import org.sevensoft.ecreator.model.crm.jobsheets.Job.Status;
import org.sevensoft.ecreator.model.crm.jobsheets.Jobsheet;
import org.sevensoft.ecreator.model.crm.messages.panels.MessagesPanel;
import org.sevensoft.ecreator.model.crm.messages.panels.MessagesReplyPanel;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;

/**
 * @author sks 19 Oct 2006 13:43:47
 */
@Path("admin-jobsheets-job.do")
public class JobHandler extends AdminHandler {

    private Job job;
    private Jobsheet jobsheet;
    private String name;
    private String deadline;
    private String eta;
    private Priority priority;
    private Status status;
    private User owner;

    public JobHandler(RequestContext context) {
        super(context);
    }

    public JobHandler(RequestContext context, Job job) {
        super(context);
        this.job = job;
    }

    public Object changeStatus() throws ServletException {

        if (job == null) {
            return main();
        }

        if (job.isCompleted()) {
            job.setStatus(Job.Status.Outstanding);
        } else {
            job.setStatus(Job.Status.Completed);
        }

        return main();
    }

    public Object create() throws ServletException {

        if (jobsheet == null) {
            return index();
        }

        if (name == null) {
            return new JobsheetHandler(context, jobsheet, Job.Status.Outstanding).view();
        }

        jobsheet.addJob(user, name);

        clearParameters();
        return new JobsheetHandler(context, jobsheet, Job.Status.Outstanding).view();
    }

    public Object delete() throws ServletException {

        if (job == null) {
            return index();
        }

        jobsheet = job.getJobsheet();
        job.delete();

        return new ActionDoc(context, "This job has been deleted", new Link(JobsheetHandler.class, "view", "jobsheet", jobsheet));
    }

    @Override
    public Object main() throws ServletException {

        if (job == null) {
            return index();
        }

        String title = HtmlHelper.convertLinks(job.getName());

        AdminDoc doc = new AdminDoc(context, user, "Job: " + title, Tab.Extras);
        doc.setIntro("Created on " + job.getDateCreated().toString("dd MMM yyyy HH:mm") + " by " + job.getCreatedBy());
        doc.setMenu(new JobsMenu(job));
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new ButtonTag(JobHandler.class, "delete", "Delete job", "job", job)
                        .setConfirmation("Are you sure you want to delete this job?"));
                sb.append(new ButtonTag(JobsheetHandler.class, "view", "Return to jobsheet", "jobsheet", job.getJobsheet(), "status", job.getStatus()));

                sb.append("</div>");
            }

            private void general() {

                sb.append(new AdminTable("Job details"));
                sb.append(new FormTag(JobHandler.class, "save", "post"));
                sb.append(new HiddenTag("job", job));

                sb.append(new AdminRow("Name", "Enter a brief overview of the job.", new TextTag(context, "name", job.getName(), 40)));

                if (job.hasItem()) {
                    sb.append(new AdminRow(job.getItem().getItemTypeName(), null, new LinkTag(ItemHandler.class, "edit", job.getItem().getName(),
                            "item", job.getItem())));
                }

                ButtonTag statusTag;
                if (job.isCompleted()) {
                    statusTag = new ButtonTag(JobHandler.class, "changeStatus", "Change to outstanding", "job", job);
                } else {
                    statusTag = new ButtonTag(JobHandler.class, "changeStatus", "Job completed", "job", job);
                }

                sb.append(new AdminRow("Status", "The current status of this job.", job.getStatus() + " " + statusTag));

                sb.append(new AdminRow("Owner", "The owner of this job.", new SelectTag(context, "owner", job.getOwner(), User.getActive(context),
                        "-No owner-")));

                sb.append(new AdminRow("Jobsheet", "The jobsheet this job is assigned to.", new SelectTag(context, "jobsheet", job.getJobsheet(),
                        Jobsheet.get(context), null)));

                sb.append(new AdminRow("Priority", "Choose the priority of the job.", new SelectTag(context, "priority", job.getPriority(),
                        Job.Priority.values())));

                sb.append("<tr><td align='center' colspan='2'>" + new SubmitTag("Update") + "</td></tr>");

                sb.append("</form>");
                sb.append("</table>");
            }

            @Override
            public String toString() {

                commands();
                general();

                sb.append(new MessagesPanel(context, job, true, false));
                sb.append(new MessagesReplyPanel(context, false, job, null, true));

                commands();

                return sb.toString();
            }

        });
        return doc;
    }

    public Object save() throws ServletException {

        if (job == null) {
            return new JobsheetHandler(context).main();
        }

        job.setJobsheet(jobsheet);
        job.setName(name);
        job.setPriority(priority);
        job.setOwner(owner);

        job.save();

        return new ActionDoc(context, "This job has been updated with your changes.", new Link(JobsheetHandler.class, "view", "jobsheet",
                job.getJobsheet(), "status", job.getStatus()));
    }
}
