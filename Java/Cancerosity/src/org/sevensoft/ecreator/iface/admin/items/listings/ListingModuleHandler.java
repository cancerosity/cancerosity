package org.sevensoft.ecreator.iface.admin.items.listings;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemTypeMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.listings.ListingModule;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 28 Dec 2006 12:32:30
 *
 */
@Path("admin-listings-module.do")
public class ListingModuleHandler extends AdminHandler {

	private ItemType	itemType;
	private int		maxListings;

	public ListingModuleHandler(RequestContext context) {
		super(context);
	}

	public ListingModuleHandler(RequestContext context, ItemType itemType) {
		super(context);
		this.itemType = itemType;
	}

	@Override
	public Object main() throws ServletException {

		if (itemType == null) {
			return index();
		}

		final ListingModule module = itemType.getListingModule();

		AdminDoc doc = new AdminDoc(context, user, "Edit listing module", null);
		doc.setMenu(new EditItemTypeMenu(itemType));
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update listing module"));
				sb.append(new ButtonTag(ItemTypeHandler.class, null, "Return to item types menu"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("Options"));

				sb.append(new AdminRow("Max listings",
						"Maximum number of listings this account can have at any one time. <i>Set to zero for unlimited.</i>", new TextTag(context,
								"maxListings", module.getMaxListings(), 4)));

				sb.append("</table>");
			}

			public String packages() {

				sb.append(new AdminTable("Listing packages"));

				sb.append("<tr>");
				sb.append("<th width='60'>Position</th>");
				sb.append("<th>Name</th>");
				sb.append("<th width='10'> </th>");
				sb.append("</tr>");

				PositionRender pr = new PositionRender(ListingPackageHandler.class, "listingPackage");
				for (ListingPackage listingPackage : itemType.getListingModule().getListingPackages()) {

					sb.append("<tr>");
					sb.append("<td width='60'>" + pr.render(listingPackage) + "</td>");
					sb.append("<td>" + new LinkTag(ListingPackageHandler.class, "edit", new SpannerGif(), "listingPackage", listingPackage) + " "
							+ listingPackage.getName() + "</td>");
					sb.append("<td width='10'>"
							+ new LinkTag(ListingPackageHandler.class, "delete", new DeleteGif(), "listingPackage", listingPackage)
									.setConfirmation("Are you sure you want to delete this listing package?") + "</td>");
					sb.append("</tr>");

				}

				final List<ItemType> listable = ItemType.getListable(context);
				if (listable.size() > 0) {

					SelectTag tag = new SelectTag(context, "itemType");
					tag.addOptions(listable);

					sb.append("<tr><td colspan='3'>Create new listing package "
							+ new ButtonTag(ListingPackageHandler.class, "create", "Create", "itemType", itemType) + "</td></tr>");

				}

				sb.append("</table>");

				return sb.toString();
			}

			@Override
			public String toString() {

				sb.append(new FormTag(ListingModuleHandler.class, "save", "post"));
				sb.append(new HiddenTag("itemType", itemType));

                commands();
				general();
				packages();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (itemType == null) {
			return index();
		}

		final ListingModule module = itemType.getListingModule();
		module.setMaxListings(maxListings);
		module.save();

		clearParameters();
		addMessage("Listing module updated");
		return main();
	}

}
