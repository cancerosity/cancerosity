package org.sevensoft.ecreator.iface.admin.reminders;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.reminders.boxes.HolidayReminderBox;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

import javax.servlet.ServletException;

/**
 * @author dme 06 Sep 2006 23:01:45
 */
@Path("admin-boxes-holiday-reminders.do")
public class HolidayReminderBoxHandler extends BoxHandler {

    private HolidayReminderBox box;

    public HolidayReminderBoxHandler(RequestContext context) {
        super(context);
    }

    @Override
    protected Box getBox() {
        return box;
    }

    @Override
    protected void saveSpecific() throws ServletException {
    }

    @Override
    protected void specifics(StringBuilder sb) {
    }

}