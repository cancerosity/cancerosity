// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   ExporterHandler.java

package org.sevensoft.ecreator.iface.admin.items.exporter;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.export.ExportField;
import org.sevensoft.ecreator.model.items.export.Exporter;
import org.sevensoft.ecreator.model.items.export.ExporterDao;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.StreamResult;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

@Path("admin-exporters.do")
public class ExporterHandler extends AdminHandler {

    private Exporter exporter;
    private ExporterDao exporterDao;
    private String separator;
    private String endLine;
    private ItemType itemType;

    public ExporterHandler(RequestContext context) {
        super(context);
        exporterDao = new ExporterDao(context);
    }

    public Object create() {
        exporter = new Exporter(context);
        exporter.save();
        return new ActionDoc(context, "A new exporter has been created", new Link(ExporterHandler.class));
    }

    public Object delete() {
        exporter.delete();
        return new ActionDoc(context, "A new exporter was deleted", new Link(ExporterHandler.class));
    }

    public Object edit() throws ServletException {
        AdminDoc doc = new AdminDoc(context, user, "Edit exporter", null);
        doc.addBody(new Body() {

            private void commands() {
                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Confirm changes"));
                sb.append(new ButtonTag(ExporterHandler.class, null, "Return to menu"));
                sb.append("</div>");
            }

            private void fields() {
                sb.append(new AdminTable("Fields"));
                sb.append("<tr>");
                sb.append("<th>Position</th>");
                sb.append("<th>Field type</th>");
                sb.append("<th width='60'>Delete</th>");
                sb.append("</tr>");
                for (Iterator i = exporter.getFields().iterator(); i.hasNext(); sb.append("</tr>")) {
                    ExportField field = (ExportField) i.next();
                    sb.append("<tr>");
                    sb.append((new StringBuilder()).append("<td>").append(new LinkTag(ExporterFieldHandler.class, null, new SpannerGif(), "field", field)).append(" ").append(field.getPosition()).append("</td>").toString());
                    sb.append((new StringBuilder()).append("<td>").append(field.getFieldType()).append("</td>").toString());
                    sb.append((new StringBuilder()).append("<td width='60'>").append((new LinkTag(ExporterFieldHandler.class, "delete", new DeleteGif(), "field", field)).setConfirmation("Are you sure you want to delete this field?")).append("</td>").toString());
                }

                sb.append((new StringBuilder()).append("<tr><td colspan='3'>Add field ").append(new ButtonTag(ExporterFieldHandler.class, "create", "Add", "exporter", exporter)).append("</td></tr>").toString());
                sb.append("</table>");
            }

            private void general() {
                sb.append(new AdminTable("General"));
                sb.append(new AdminRow("Item Type", "Select the item type that will be exported by this exporter", new SelectTag(context, "itemType", exporter.getItemType(), ItemType.getSelectionMap(context), "None set")));
                sb.append(new AdminRow("End line", "The character used to mark the end of a record.", new TextTag(context, "endLine", exporter.getEndLine(), 8)));
                sb.append(new AdminRow("Separator", "The character used to separate fields in each record.", new TextTag(context, "separator", exporter.getSeparator(), 8)));
                sb.append("</table>");
            }

            public String toString() {
                sb.append(new FormTag(ExporterHandler.class, "save", "POST"));
                sb.append(new HiddenTag("exporter", exporter));
                commands();
                general();
                fields();
                commands();
                sb.append("</form>");
                return sb.toString();
            }

        }
        );
        return doc;
    }

    public Object export() throws ServletException {
        try {
            File file = exporter.export();
            return new StreamResult(file, "text/csv", (new StringBuilder()).append(exporter.getItemType().getName()).append(".csv").toString(), StreamResult.Type.Attachment);
        }
        catch (IOException e) {
            e.printStackTrace();
            return e.toString();
        }
    }

    public Object main() throws ServletException {
        final List<Exporter> exporters = exporterDao.findAll();
        AdminDoc doc = new AdminDoc(context, user, "Exporters", null);
        doc.addBody(new Body() {

            private void general() {
                sb.append(new AdminTable("Exporters"));
                sb.append("<tr>");
                sb.append("<th></th>");
                sb.append("<th>Item Type</th>");
                sb.append("<th width='60'>Export</th>");
                sb.append("<th width='60'>Delete</th>");
                sb.append("</tr>");
                for (Iterator<Exporter> i = exporters.iterator(); i.hasNext(); sb.append("</tr>")) {
                    Exporter exporter = i.next();
                    ItemType itemType = exporter.getItemType();
                    sb.append("<tr>");
                    sb.append((new StringBuilder()).append("<td>").append(new LinkTag(ExporterHandler.class, "edit", new SpannerGif(), "exporter", exporter)).append(" ").append(exporter.getId()).append("</td>").toString());
                    if (itemType == null)
                        sb.append("<td>None</td>");
                    else
                        sb.append((new StringBuilder()).append("<td>").append(itemType.getName()).append("</td>").toString());
                    sb.append((new StringBuilder()).append("<td width='60'>").append((new ButtonTag(ExporterHandler.class, "export", "Export", "exporter", exporter)).setConfirmation("Confirm you want to run this exporter").setDisabled(exporter.getItemType() == null)).append("</td>").toString());
                    sb.append((new StringBuilder()).append("<td width='60'>").append((new LinkTag(ExporterHandler.class, "delete", new DeleteGif(), "exporter", exporter)).setConfirmation("Are you sure you want to delete this exporter?")).append("</td>").toString());
                }

                sb.append((new StringBuilder()).append("<tr><td colspan='4'>Add exporter ").append(new ButtonTag(ExporterHandler.class, "create", "Add")).append("</td></tr>").toString());
                sb.append("</table>");
            }

            public String toString() {
                general();
                return sb.toString();
            }

        }
        );
        return doc;
    }

    public Object save() throws ServletException {
        exporter.setEndLine(endLine);
        exporter.setItemType(itemType);
        exporter.setSeparator(separator);
        exporter.save();
        return new ActionDoc(context, "This exporter has been updated", new Link(ExporterHandler.class, "edit", "exporter", exporter));
    }

}
