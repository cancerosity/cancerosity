package org.sevensoft.ecreator.iface.admin.attachments;

import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.AttachmentOwner;
import org.sevensoft.ecreator.model.attachments.AttachmentSettings;
import org.sevensoft.ecreator.model.system.config.Captions;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 12-Dec-2005 13:33:45
 * 
 */
public class AdminAttachmentsPanel extends Body {

	@SuppressWarnings("hiding")
	private static Logger			logger	= Logger.getLogger(AdminAttachmentsPanel.class.getName());

	private List<Attachment>		attachments;
	private RequestContext			context;
	private final boolean			editable;
	private final AttachmentSettings	attachmentSettings;
	private final Captions			captions;

	public AdminAttachmentsPanel(RequestContext context, AttachmentOwner owner, boolean editable) {
		this.context = context;
		this.editable = editable;
		this.attachments = owner.getAttachments();
		this.attachmentSettings = AttachmentSettings.getInstance(context);
		this.captions = Captions.getInstance(context);
	}

	@Override
	public String toString() {

		int span = 4;

		sb.append(new AdminTable(captions.getAttachmentsCaptionPlural()));

		sb.append("<tr>");
		sb.append("<th width='70'>Type</th>");
		sb.append("<th width='120'>Date Uploaded</th>");
		sb.append("<th>Filename</th>");
		sb.append("<th width='60'>Size</th>");

		if (editable) {
			sb.append("<th width='20'></th>");
			span += 1;
		}

		sb.append("</tr>");

		int n = 1;
		for (Attachment a : attachments) {

			LinkTag downloadLink = new LinkTag(org.sevensoft.ecreator.iface.frontend.attachments.AttachmentHandler.class, "download",
					"Download attachment", "attachment", a);
			LinkTag previewLink = new LinkTag(AttachmentHandler.class, null, "Download preview", "attachment", a, "preview", true);

			sb.append("<tr>");
			sb.append("<td width='70'>" + new ImageTag("files/graphics/filetypes/" + a.getFiletype().name().toLowerCase() + ".gif") + "</td>");
			sb.append("<td width='120'>" + a.getDateUploaded().toDateString() + "</td>");
			sb.append("<td>");
			if (editable) {

				sb.append(new LinkTag(AttachmentHandler.class, "edit", new SpannerGif(), "attachment", a));
				sb.append(" ");
			}

			sb.append(a.getFilename() + "<br/><font size='1'>" + downloadLink);

			if (a.hasPreview()) {
				sb.append(" | ");
				sb.append(previewLink);
			}

			sb.append(" </font></td>");
			sb.append("<td width='60'>" + a.getSizeKb() + "</td>");

			if (editable) {

				sb.append("<td width='20'>" +
						new LinkTag(AttachmentHandler.class, "removeAttachment", new DeleteGif(), "attachment", a)
								.setConfirmation("Are you sure you want to remove this attachment?") + "</td>");
			}

			sb.append("</tr>");
			n++;
		}

		if (editable) {

			sb.append("<tr><td colspan='" + span + "'>Upload " + captions.getAttachmentsCaption() + " " + new FileTag("files", 20) + " ");

			if (Module.AttachmentPreviews.enabled(context)) {
				sb.append(" Make preview ");
				sb.append(new CheckTag(context, "makePreviews", true, false));
				sb.append(" ");
			}

			sb.append(new SubmitTag("Done"));
			sb.append("</td></tr>");

		}
		sb.append("</table>");

		return sb.toString();
	}
}
