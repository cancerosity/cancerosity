package org.sevensoft.ecreator.iface.admin.misc.agreements;

import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.misc.agreements.Agreement;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;
import java.util.List;

/**
 * @author sks
 */
@Path("admin-agreements.do")
public class AgreementsHandler extends AdminHandler {

    private Agreement agreement;
    private String title, content;
    private String confirmButtonText;
    private String rejectButtonText;
    private boolean inline;
    private String inlineText;

    private transient MiscSettings miscSettings = MiscSettings.getInstance(context);

    public AgreementsHandler(RequestContext context) {
        super(context);
    }

    public AgreementsHandler(RequestContext context, Category category) {
        super(context);
    }

    public Object create() throws ServletException {

        agreement = new Agreement(context, "new agreement");
        agreement.log(user, "Created");

        return new ActionDoc(context, "A new agreement has been created", new Link(AgreementsHandler.class));
    }

    public Object delete() throws ServletException {

        if (agreement == null) {
            return main();
        }

        agreement.log(user, "Deleted");
        agreement.delete();

        return new ActionDoc(context, "This agreement has been deleted", new Link(AgreementsHandler.class));
    }

    public Object edit() throws ServletException {

        if (agreement == null) {
            return main();
        }

        AdminDoc doc = new AdminDoc(context, user, "Edit agreement", Tab.Extras);
        doc.setMenu(new ExtrasMenu());
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update agreement"));
                sb.append(new ButtonTag(AgreementsHandler.class, "delete", "Delete agreement", "agreement", agreement)
                        .setConfirmation("If you delete this agreement it cannot be un-done. Continue?"));
                sb.append(new ButtonTag(AgreementsHandler.class, null, "Return to agreements menu"));
                sb.append("</div>");
            }

            private void general() {

                sb.append(new AdminTable("Agreement details"));

                sb.append(new AdminRow("Title", "Enter the title of this agreement.", new TextTag(context, "title", agreement.getTitle(), 40)));

                sb.append(new AdminRow("Content", "Enter the main text of this agreement.", new TextAreaTag(context, "content", agreement.getContent())
                        .setId("content").setStyle("width: 100%; height: 320px;")));

                sb.append(new AdminRow("Inline", "When set to inline, this agreement will be displayed as a simple link.", new BooleanRadioTag(context,
                        "inline", agreement.isInline())));

                if (agreement.isInline()) {

                    sb.append(new AdminRow("Inline caption", "Enter the line of text that appears next to the check box for this agreement. "
                            + "[link] will be replaced with 'click here' which will be a popup with the agreement text.", new TextAreaTag(
                            context, "inlineText", agreement.getInlineText(), 50, 2)));

                }

                sb.append(new AdminRow("Confirm button text",
                        "This is the label for the button that users click on to accept or confirm the agreement.", new TextTag(context,
                        "confirmButtonText", agreement.getConfirmButtonText())));

                sb.append(new AdminRow("Reject button text",
                        "This is the label of the button that users will click if they do not accept this agreement.", new TextTag(context,
                        "rejectButtonText", agreement.getRejectButtonText())));

                sb.append("</table>");
            }

            @Override
            public String toString() {

                if (miscSettings.isHtmlEditor()) {
                    RendererUtil.tinyMce(context, sb, "content");
                }

                sb.append(new FormTag(AgreementsHandler.class, "save", "post"));
                sb.append(new HiddenTag("agreement", agreement));

                commands();
                general();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    @Override
    public Object main() throws ServletException {

        AdminDoc doc = new AdminDoc(context, user, "Agreements", Tab.Extras);
        doc.setMenu(new ExtrasMenu());
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new AdminTable("Agreements"));

                List<Agreement> agreements = Agreement.get(context);
                if (agreements.size() > 0) {

                    sb.append(new FormTag(AgreementsHandler.class, "edit", "POST"));

                    SelectTag tag = new SelectTag(null, "agreement");
                    tag.addOptions(agreements);
                    sb.append(new AdminRow("Choose agreement to edit", null, tag + " " + new SubmitTag("Edit")));
                    sb.append("</form>");

                }

                sb.append(new AdminRow("Create a new agreement", new ButtonTag(AgreementsHandler.class, "create", "Create agreement")));
                sb.append("</table>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object save() throws ServletException {

        if (agreement == null) {
            return main();
        }

        test(new RequiredValidator(), "title");
        if (hasErrors()) {
            return main();
        }

        if (!ObjectUtil.equal(agreement.getTitle(), title)) {
            agreement.setTitle(title);
            agreement.log(user, "Title changed - " + title);
        }

        if (!ObjectUtil.equal(agreement.getContent(), content)) {
            agreement.setContent(content);
            agreement.log(user, "Content updated");
        }

        agreement.setRejectButtonText(rejectButtonText);
        agreement.setConfirmButtonText(confirmButtonText);
        agreement.setInline(inline);
        agreement.setInlineText(inlineText);
        agreement.save();

        return new ActionDoc(context, "This agreement has been updated", new Link(AgreementsHandler.class, "edit", "agreement", agreement));
    }
}