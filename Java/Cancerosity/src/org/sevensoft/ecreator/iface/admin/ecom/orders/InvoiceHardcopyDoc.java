package org.sevensoft.ecreator.iface.admin.ecom.orders;

import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.results.docs.Doc;

import javax.servlet.ServletException;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author sks 10-Apr-2006 12:37:23
 */
public class InvoiceHardcopyDoc extends Doc {

	private OrderSettings orderSettings;
	private final Order order;
	private final List<Order> orders;

	public InvoiceHardcopyDoc(RequestContext context, Order order) {
		super(context);
		this.order = order;
		this.orders = null;
		this.orderSettings = OrderSettings.getInstance(context);
	}

	public InvoiceHardcopyDoc(RequestContext context, List<Order> orders) {
		super(context);
		this.orders = orders;
		this.order = null;
		this.orderSettings = OrderSettings.getInstance(context);
	}

	@Override
	public StringBuilder output() throws ServletException, IOException,
			InstantiationException, IllegalAccessException,
			ClassNotFoundException {
		if (order != null) {
			StringBuilder sb = new StringBuilder();

			sb.append("<html>");
			sb.append("<head>");
			sb.append("<style>");
			sb.append("* { font-family: Arial; margin: 0; padding: 0; font-size: 11px; } ");
			sb.append("body { margin: 10; } ");
			sb.append("table.company { width: 600px; margin-bottom: 15; } ");

			sb.append("div.ref { text-align: right; width: 600px; font-size: 16px; font-weight: bold; }");

			sb.append("table.addresses { width: 600px; margin-top: 15px;	} ");
			sb.append("table.addresses td { width: 50%; } ");

			sb.append("table.details { width: 600px; border: 1px solid black; border-collapse: collapse; margin-bottom: 15;} ");
			sb.append("table.details td {border: 1px solid black; padding: 4; width: 50%;} ");

			sb.append("table.items { border: 1px solid black; width: 600px; border-collapse: collapse; margin-bottom: 15px; }");
			sb.append("table.items th { text-align: left; font-weight: bold; font-size: 12px; border: 1px solid black; padding: 4px; }");
			sb.append("table.items td { padding: 4px; }");

			sb.append("table.totals { width: 600px; margin-bottom: 15; } ");
			sb.append("table.totals td { padding: 4px; } ");
			
			sb.append("</style>");
			sb.append("<title>Order " + order.getId() + "</title>");

			sb.append("</head>");
			sb.append("<body>");

			Company company = Company.getInstance(context);
			sb.append(HtmlHelper.nl2br(orderSettings.getInvoiceHeader()
					.replace("[comp_vatreg]", "" + company.getVatNumber())
					.replace("[comp_number]", "" + company.getCompanyNumber())));

			sb.append("<div class='ref'>INVOICE " + order.getOrderId()
					+ "</div>");

			sb.append("<br/>");

			sb.append(new TableTag("addresses"));
			sb.append("<tr>");
			sb.append("<td valign='top' width='50%'><b>Billing address:</b><br/>");

			if (order.hasBillingAddress()) {

				sb.append(order.getBillingAddress().getLabel("<br/>",
						orderSettings.isPrintCustomerTelepnone()));

			} else {

				sb.append(order.getAccount().getName());

			}

			sb.append("</td>");
			sb.append("<td valign='top' width='50%'><b>Delivery address:</b><br/>");

			if (order.isDelivery()) {

				sb.append(order.getDeliveryAddress().getLabel("<br/>",
						orderSettings.isPrintCustomerTelepnone()));

			} else {

				sb.append("Collection");

			}

			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");

			sb.append("<br/>");

			sb.append(new TableTag("details"));
			sb.append("<tr>");
			sb.append("<td><b>Order / Invoice Number:</b> " + order.getId()
					+ "</td>");
			sb.append("<td><b>Date:</b> "
					+ order.getDatePlaced().toString("dd-MMM-yyyy") + "</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td><b>Account No:</b> " + order.getAccount().getId()
					+ "</td>");
			sb.append("<td><b>Customer:</b> " + order.getAccount().getName()
					+ "</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td><b>Your reference:</b> "
					+ (order.getCustomerReference() == null ? "" : order
							.getCustomerReference()) + "</td>");
			sb.append("<td></td>");
			sb.append("</tr>");

			sb.append("</table>");

			/*
			 * get attributes defined as invoice columns
			 */

			Set<Attribute> columns = new LinkedHashSet<Attribute>();
			for (OrderLine line : order.getLines()) {

				if (line.hasItem()) {

					ItemType type = line.getItem().getItemType();
					for (Attribute attribute : type.getAttributes()) {

						if (attribute.isInvoiceColumn())
							columns.add(attribute);
					}
				}
			}

			/*
			 * Items
			 */
			sb.append(new TableTag("items"));

			sb.append("<tr>");
			sb.append("<th>Qty</th>");
			sb.append("<th>Code</th>");

			for (Attribute column : columns) {
				sb.append("<th>" + column.getInvoiceName() + "</th>");
			}

			sb.append("<th>Description</th>");
			sb.append("<th align='right'>Unit</th>");
			sb.append("<th align='right'>Line</th>");
			sb.append("</tr>");

			for (OrderLine line : order.getLines()) {

				sb.append("<tr>");
				sb.append("<td width='80'>" + line.getQty() + "</td>");
				if (line.hasItem())
					sb.append("<td>" + line.getItem().getId() + "</td>");
				else
					sb.append("<td>&nbsp;</td>");

				/*
				 * Attribute columns
				 */

				for (Attribute column : columns) {

					sb.append("<td>");

					if (line.hasItem()) {

						if (column.getItemType().equals(column.getItemType())) {

							Item item = line.getItem();
							String value = item.getAttributeValue(column);
							if (value != null)
								sb.append(value);
						}
					}

					sb.append("</td>");
				}

				sb.append("<td>" + line.getDescription());
				if (line.hasOptionsDetails()) {
					sb.append("<br/>");
					sb.append(line.getOptionsDetails());
				}
				sb.append("</td>");
				sb.append("<td align='right'>" + line.getUnitSellEx() + "</td>");
				sb.append("<td align='right'>" + line.getLineSellEx() + "</td>");
				sb.append("</tr>	");
			}

			if (order.hasDeliveryDetails()) {

				sb.append("<tr>");
				sb.append("<td>1</td>");
				sb.append("<td></td>");

				for (int n = 0; n < columns.size(); n++) {
					sb.append("<td></td>");
				}

				sb.append("<td>" + order.getDeliveryDetails() + "</td>");
				sb.append("<td align='right'>" + order.getDeliveryChargeEx()
						+ "</td>");
				sb.append("<td align='right'>" + order.getDeliveryChargeEx()
						+ "</td>");
				sb.append("</tr>	");
			}

			sb.append("</table>");

			/*
			 * Totals
			 */
			sb.append(new TableTag("totals"));

			sb.append("<tr>");
			sb.append("<td align='right'><b>Voucher discount:</b></td>");
			sb.append("<td align='right' width='80'>\243"
					+ order.getVoucherDiscountIncAbs() + "</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td align='right'><b>Subtotal:</b></td>");
			sb.append("<td align='right' width='80'>\243" + order.getTotalEx()
					+ "</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td align='right'><b>Vat:</b></td>");
			sb.append("<td align='right' width='80'>\243" + order.getTotalVat()
					+ "</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td align='right'><b>Total:</b></td>");
			sb.append("<td align='right' width='80'>\243" + order.getTotalInc()
					+ "</td>");
			sb.append("</tr>");

			sb.append("</table>");

			sb.append(HtmlHelper.nl2br(orderSettings.getInvoiceFooter()
					.replace("[comp_vatreg]", "" + company.getVatNumber())
					.replace("[comp_number]", "" + company.getCompanyNumber())));

			sb.append("</body>");
			sb.append("</html>");

			return sb;
		} else {
			if (orders == null) {
				return new StringBuilder("No orders selected");
			}
			StringBuilder sb = new StringBuilder();

			sb.append("<html>");
			sb.append("<head>");
			sb.append("<style>");
			sb.append("* { font-family: Arial; margin: 0; padding: 0; font-size: 11px; } ");
			sb.append("body { margin: 10; } ");
			sb.append("table.company { width: 600px; margin-bottom: 15; } ");

			sb.append("div.ref { text-align: right; width: 600px; font-size: 16px; font-weight: bold; }");

			sb.append("table.addresses { width: 600px; margin-top: 15px;	} ");
			sb.append("table.addresses td { width: 50%; } ");

			sb.append("table.details { width: 600px; border: 1px solid black; border-collapse: collapse; margin-bottom: 15;} ");
			sb.append("table.details td {border: 1px solid black; padding: 4; width: 50%;} ");

			sb.append("table.items { border: 1px solid black; width: 600px; border-collapse: collapse; margin-bottom: 15px; }");
			sb.append("table.items th { text-align: left; font-weight: bold; font-size: 12px; border: 1px solid black; padding: 4px; }");
			sb.append("table.items td { padding: 4px; }");

			sb.append("table.totals { width: 600px; margin-bottom: 15; } ");
			sb.append("table.totals td { padding: 4px; } ");
			sb.append("div.page{page-break-after: always; page-break-inside: avoid;}");
			sb.append("</style>");
			sb.append("<title>Bulk Order </title>");

			sb.append("</head>");
			sb.append("<body>");
			int i = 0;
			for (Order order : orders) {
				if(i<orders.size()-1){
					sb.append("<div class='page'>");
				} else {
					sb.append("<div");
				}
				i++;
				Company company = Company.getInstance(context);
				sb.append(HtmlHelper.nl2br(orderSettings
						.getInvoiceHeader()
						.replace("[comp_vatreg]", "" + company.getVatNumber())
						.replace("[comp_number]",
								"" + company.getCompanyNumber())));

				sb.append("<div class='ref'>INVOICE " + order.getOrderId()
						+ "</div>");

				sb.append("<br/>");

				sb.append(new TableTag("addresses"));
				sb.append("<tr>");
				sb.append("<td valign='top' width='50%'><b>Billing address:</b><br/>");

				if (order.hasBillingAddress()) {

					sb.append(order.getBillingAddress().getLabel("<br/>",
							orderSettings.isPrintCustomerTelepnone()));

				} else {

					sb.append(order.getAccount().getName());

				}

				sb.append("</td>");
				sb.append("<td valign='top' width='50%'><b>Delivery address:</b><br/>");

				if (order.isDelivery()) {

					sb.append(order.getDeliveryAddress().getLabel("<br/>",
							orderSettings.isPrintCustomerTelepnone()));

				} else {

					sb.append("Collection");

				}

				sb.append("</td>");
				sb.append("</tr>");
				sb.append("</table>");

				sb.append("<br/>");

				sb.append(new TableTag("details"));
				sb.append("<tr>");
				sb.append("<td><b>Order / Invoice Number:</b> " + order.getId()
						+ "</td>");
				sb.append("<td><b>Date:</b> "
						+ order.getDatePlaced().toString("dd-MMM-yyyy")
						+ "</td>");
				sb.append("</tr>");

				sb.append("<tr>");
				sb.append("<td><b>Account No:</b> "
						+ order.getAccount().getId() + "</td>");
				sb.append("<td><b>Customer:</b> "
						+ order.getAccount().getName() + "</td>");
				sb.append("</tr>");

				sb.append("<tr>");
				sb.append("<td><b>Your reference:</b> "
						+ (order.getCustomerReference() == null ? "" : order
								.getCustomerReference()) + "</td>");
				sb.append("<td></td>");
				sb.append("</tr>");

				sb.append("</table>");

				/*
				 * get attributes defined as invoice columns
				 */

				Set<Attribute> columns = new LinkedHashSet<Attribute>();
				for (OrderLine line : order.getLines()) {

					if (line.hasItem()) {

						ItemType type = line.getItem().getItemType();
						for (Attribute attribute : type.getAttributes()) {

							if (attribute.isInvoiceColumn())
								columns.add(attribute);
						}
					}
				}

				/*
				 * Items
				 */
				sb.append(new TableTag("items"));

				sb.append("<tr>");
				sb.append("<th>Qty</th>");
				sb.append("<th>Code</th>");

				for (Attribute column : columns) {
					sb.append("<th>" + column.getInvoiceName() + "</th>");
				}

				sb.append("<th>Description</th>");
				sb.append("<th align='right'>Unit</th>");
				sb.append("<th align='right'>Line</th>");
				sb.append("</tr>");

				for (OrderLine line : order.getLines()) {

					sb.append("<tr>");
					sb.append("<td width='80'>" + line.getQty() + "</td>");
					if (line.hasItem())
						sb.append("<td>" + line.getItem().getId() + "</td>");
					else
						sb.append("<td>&nbsp;</td>");

					/*
					 * Attribute columns
					 */

					for (Attribute column : columns) {

						sb.append("<td>");

						if (line.hasItem()) {

							if (column.getItemType().equals(
									column.getItemType())) {

								Item item = line.getItem();
								String value = item.getAttributeValue(column);
								if (value != null)
									sb.append(value);
							}
						}

						sb.append("</td>");
					}

					sb.append("<td>" + line.getDescription());
					if (line.hasOptionsDetails()) {
						sb.append("<br/>");
						sb.append(line.getOptionsDetails());
					}
					sb.append("</td>");
					sb.append("<td align='right'>" + line.getUnitSellEx()
							+ "</td>");
					sb.append("<td align='right'>" + line.getLineSellEx()
							+ "</td>");
					sb.append("</tr>	");
				}

				if (order.hasDeliveryDetails()) {

					sb.append("<tr>");
					sb.append("<td>1</td>");
					sb.append("<td></td>");

					for (int n = 0; n < columns.size(); n++) {
						sb.append("<td></td>");
					}

					sb.append("<td>" + order.getDeliveryDetails() + "</td>");
					sb.append("<td align='right'>"
							+ order.getDeliveryChargeEx() + "</td>");
					sb.append("<td align='right'>"
							+ order.getDeliveryChargeEx() + "</td>");
					sb.append("</tr>	");
				}

				sb.append("</table>");

				/*
				 * Totals
				 */
				sb.append(new TableTag("totals"));

				sb.append("<tr>");
				sb.append("<td align='right'><b>Voucher discount:</b></td>");
				sb.append("<td align='right' width='80'>\243"
						+ order.getVoucherDiscountIncAbs() + "</td>");
				sb.append("</tr>");

				sb.append("<tr>");
				sb.append("<td align='right'><b>Subtotal:</b></td>");
				sb.append("<td align='right' width='80'>\243"
						+ order.getTotalEx() + "</td>");
				sb.append("</tr>");

				sb.append("<tr>");
				sb.append("<td align='right'><b>Vat:</b></td>");
				sb.append("<td align='right' width='80'>\243"
						+ order.getTotalVat() + "</td>");
				sb.append("</tr>");

				sb.append("<tr>");
				sb.append("<td align='right'><b>Total:</b></td>");
				sb.append("<td align='right' width='80'>\243"
						+ order.getTotalInc() + "</td>");
				sb.append("</tr>");

				sb.append("</table>");

				sb.append(HtmlHelper.nl2br(orderSettings
						.getInvoiceFooter()
						.replace("[comp_vatreg]", "" + company.getVatNumber())
						.replace("[comp_number]",
								"" + company.getCompanyNumber())));
				// paging break for printing
				sb.append("</div>");
			}
			sb.append("</body>");
			sb.append("</html>");

			return sb;
		}
	}
}