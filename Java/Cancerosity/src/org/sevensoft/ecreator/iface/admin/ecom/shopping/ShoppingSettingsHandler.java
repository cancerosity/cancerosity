package org.sevensoft.ecreator.iface.admin.ecom.shopping;

import java.util.List;
import java.util.Arrays;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.validators.net.EmailValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.ecom.orders.emails.OrderEmail;
import org.sevensoft.ecreator.model.ecom.shopping.ShoppingSettings.CheckoutAccounts;
import org.sevensoft.ecreator.model.ecom.shopping.ShoppingSettings.DispatchType;
import org.sevensoft.ecreator.model.misc.agreements.Agreement;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.ErrorTag;

/**
 * @author sks 03-Oct-2005 14:25:41
 * 
 */
@Path("admin-settings-shopping.do")
public class ShoppingSettingsHandler extends AdminHandler {

	private transient Company	company;

	private Money			minOrderValue;
	private String			orderNotificationEmails, checkoutContent, basketContent;
	private DispatchType		dispatchOptions;
	private boolean			customerReference;
	private boolean			quickBasketAdd;
	private String			checkoutScripts;
	private String			vatExemptionText;
	private boolean			vatExemptionDeclaration;
	private boolean			addressBook;
	private boolean			separateAddresses;
	private String			checkoutCompletedText;
	private boolean			simpleCheckout;
	private boolean			orderAcknowledgement;
	private String			reservationInstructions;
	private String			reservationText;
	private String			orderAckFooter;
	private String			orderAckHeader;
	private Money			spendPerCredit;
	private String			checkoutConfirmationHeader;
	private boolean			noCountries;

	private boolean			postCheckoutRegistration;

	private CheckoutAccounts	checkoutAccounts;

	private String			placeOrderText;

	private String			basketClearedCaption;

	private String			addBasketItemCaption;

	private String			emptyBasketCaption;

	private String			basketUpdatedCaption;

	private boolean			createAccountForGuest;

	private String			deliveryAddressHeader;

	private String			billingAddressHeader;

	private boolean			clearBasketOnLogout;

	private boolean			addresses;

	private String			sellerEmailText;

	private boolean			emailOrdersToSellers;

	private boolean			formsFirst;

	private boolean			vatExemptionDetails;

	private Agreement			agreement;

	private String			referrers;

	private String			cancellationContent;

	private boolean	branchSearch;

    private boolean orderAcknowledgementHtml;

	public ShoppingSettingsHandler(RequestContext context) {
		super(context);
		this.company = Company.getInstance(context);
	}

	@Override
	public Object main() {

		AdminDoc doc = new AdminDoc(context, user, "Shopping settings", null);
		doc.addBody(new Body() {

			private void addressBook() {
				sb.append(new AdminRow("Address book", "The address book will save addresses previously entered by the customer on their account "
						+ "so they can quickly select them next time they checkout - rather than re-entering each time.", new BooleanRadioTag(
						context, "addressBook", shoppingSettings.isAddressBook())));
			}

			private void addresses() {

				sb.append(new AdminTable("Addresses"));

				sb.append(new AdminRow("Use addresses", "Ask for address as part of the checkout process.", new BooleanRadioTag(context, "addresses",
						shoppingSettings.isAddresses())));

				if (shoppingSettings.isAddresses()) {

					sb.append(new AdminRow("Delivery address header", "This text appears at the top of delivery address screen.", new TextAreaTag(
							context, "deliveryAddressHeader", shoppingSettings.getDeliveryAddressHeader(), 80, 5)));

					if (shoppingSettings.isSeparateAddresses()) {

						sb.append(new AdminRow("Billing address header", "This text appears at the top of billing address screen.",
								new TextAreaTag(context, "billingAddressHeader", shoppingSettings.getBillingAddressHeader(), 80, 5)));

					}

				}

				sb.append("</table>");
			}

			private void basket() {

				sb.append(new AdminTable("Shopping basket"));

				sb.append(new AdminRow("Quick add to basket", "If enabled then adding a product to the basket will not show the basket screen.",
						new BooleanRadioTag(context, "quickBasketAdd", shoppingSettings.isQuickBasketAdd())));

				sb.append(new AdminRow("Add to basket caption", "This is the message displayed when a product is added to the basket.", new TextTag(
						null, "addBasketItemCaption", captions.getAddBasketItemCaption(), 80)));

				sb.append(new AdminRow("Empty basket caption", "This message is displayed when the basket is empty.", new TextTag(null,
						"emptyBasketCaption", captions.getEmptyBasketCaption(), 80)));

				sb.append(new AdminRow("Basket updated caption", "This message is displayed when the basket is updated.", new TextTag(null,
						"basketUpdatedCaption", captions.getBasketUpdatedCaption(), 80)));

				sb.append(new AdminRow("Basket cleared caption", "This message is used when the basket is cleared.", new TextTag(null,
						"basketClearedCaption", captions.getBasketClearedCaption(), 80)));

				sb.append(new AdminRow("Clear basket on logout", "Empty a users basket when they logout of their account.", new BooleanRadioTag(
						context, "clearBasketOnLogout", shoppingSettings.isClearBasketOnLogout())));

				sb.append("</table>");
			}

			private void checkout() {

				sb.append(new AdminTable("Checkout"));

				if (Module.Accounts.enabled(context)) {

					sb.append(new AdminRow("Checkout accounts", "Set how you want to use accounts when checking out.", new SelectTag(context,
							"checkoutAccounts", shoppingSettings.getCheckoutAccounts(), CheckoutAccounts.values())));

				}

				sb.append(new AdminRow("Dispatch options", "Set how customers can receive their order. <i>Delivery, Collections, or both</i>.",
						new SelectTag(context, "dispatchOptions", shoppingSettings.getDispatchType(), DispatchType.values())));

				sb.append(new AdminRow("Minimum Order Value", "If you wish to set a minimum order value then customers "
						+ "will be unable to checkout if their basket total is below this level.", new TextTag(context, "minOrderValue",
						shoppingSettings.getMinOrderValue(), 8)));

				if (shoppingSettings.isAddresses()) {

					sb.append(new AdminRow("Separate billing / delivery addresses",
							"Enabling this feature allows customers to specify a separate billing and delivery address. "
									+ "Leaving set to no means customers will only be asked to enter a delivery address.",
							new BooleanRadioTag(context, "separateAddresses", shoppingSettings.isSeparateAddresses())));

					switch (shoppingSettings.getCheckoutAccounts()) {

					case None:
						referrers();
						break;

					case Required:
						addressBook();
						break;

					case Optional:

						createGuestAccount();
						addressBook();

					}

				}

				if (Module.Credits.enabled(context)) {

					sb.append(new AdminRow("Spend per credit",
							"How much must be spent in order for a customer to be awarded a credit. This is calculated ex VAT. "
									+ "So if you enter 0.85 then the customer must spend 85 pence (ex VAT) in order get one credit.",
							new TextTag(context, "spendPerCredit", shoppingSettings.getSpendPerCredit(), 8)));

				}

				if (Module.Forms.enabled(context)) {

					sb.append(new AdminRow("Forms first",
							"Show checkout forms as first pages of checkout procedure. When set to no, they appear last.", new BooleanRadioTag(
									context, "formsFirst", shoppingSettings.isFormsFirst())));

				}

				if (Module.Agreements.enabled(context)) {

					List<Agreement> agreements = Agreement.get(context);

					sb.append(new AdminRow("Agreement",
							"Choose an agreement that the customer must tick their acceptance to before the order can be placed.", new SelectTag(
									context, "agreement", shoppingSettings.getAgreement(), agreements, "-Choose agreement-")));

				}

				sb.append(new AdminRow("Checkout completed text",
						"Enter here the message or information you want to display to the customer when an order has been completed/placed.<br/>"
								+ "Special tags: <i>[ref]</i> - shows the reference number of the order.", new TextAreaTag(context,
								"checkoutCompletedText", shoppingSettings.getCheckoutCompletedText(), 80, 7)));

				sb.append(new AdminRow("Checkout scripts",
						"If you need to run any scripts on checkout, eg, a shopping feed conversion script, then paste it here.<br/>"
								+ "Available tags: [subtotal], [total], [vat], [id], [items_subtotal]", new TextAreaTag(context,
								"checkoutScripts", shoppingSettings.getCheckoutScripts(), 80, 7)));

				sb.append(new AdminRow("Place order text", "This text appears above hte place order button on the confirmation page.", new TextAreaTag(
						context, "placeOrderText", shoppingSettings.getPlaceOrderText(), 80, 4)));

				sb.append(new AdminRow("Cancellation content", "This content appears when a customer cancels the checkout process.", new TextAreaTag(
						context, "cancellationContent", shoppingSettings.getCancellationContent(), 80, 5)));

				sb.append("</table>");
			}

			private void referrers() {

				sb.append(new AdminRow("Referrers",
						"If you want users to select where they heard about you when checking out, enter the referral sources here.",
						new TextAreaTag(context, "referrers", StringHelper.implode(shoppingSettings.getReferrers(), "\n"), 40, 5)));
			}

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update shopping settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void content() {

				sb.append(new AdminTable("Checkout / basket content"));

				sb.append("<tr><th><b>Basket screen content</b><br/>Content that appears in the checkout page.</th></tr>");
				sb.append("<tr><td>"
						+ new TextAreaTag(context, "basketContent", shoppingSettings.getBasketContent()).setStyle("width: 100%; height: 320px;")
								.setId("basketContent") + "</td></tr>");

				sb.append("<tr><th><b>Checkout confirmation header</b><br/>Content that appears above the confirmation panel in checkout.</th></tr>");
				sb.append("<tr><td>"
						+ new TextAreaTag(context, "checkoutConfirmationHeader", shoppingSettings.getCheckoutConfirmationHeader()).setStyle(
								"width: 100%; height: 320px;").setId("checkoutConfirmationHeader") + "</td></tr>");

				sb.append("<tr><th><b>Checkout screen content</b><br/>Content that appears in the checkout page.</th></tr>");
				sb.append("<tr><td>"
						+ new TextAreaTag(context, "checkoutContent", shoppingSettings.getCheckoutContent())
								.setStyle("width: 100%; height: 320px;").setId("checkoutContent") + "</td></tr>");

				sb.append("</table>");
			}

			private void createGuestAccount() {
				sb.append(new AdminRow("Create account for guest", "If someone checks out in guest mode (ie, did not sign up for an account), "
						+ "then create an account automatically and email them a password so they can login next time.", new BooleanRadioTag(
						context, "createAccountForGuest", shoppingSettings.isCreateAccountForGuest())));
			}

			private void emails() {

				sb.append(new AdminTable("Order acknowledgement"));

				sb.append(new AdminRow("Enable customer order acknowledgement", "Send an email to the customer acknowledging receipt of the order.",
						new BooleanRadioTag(context, "orderAcknowledgement", shoppingSettings.isOrderAcknowledgement())));

				if (shoppingSettings.isOrderAcknowledgement()) {

                    sb.append(new AdminRow("HTML email format", "",
						new BooleanRadioTag(context, "orderAcknowledgementHtml", shoppingSettings.isOrderAcknowledgementHtml())));

                    sb.append(new AdminRow("Order acknowledgement header",
                            "This text will be inserted at the top of the order acknowledgement.<br/><br/>Special tags:<br/>"
                                    + "<i>[name]</i> - Members name<br/>" + "<i>[email]</i> - Members email<br/>"
                                    + "<i>[id]</i> - Generated id<br/>" + "<i>[login]</i> - Url to login page<br/>"
                                    + "<i>[loginurl]</i> - Login url<br/>" + "<i>[site]</i> - Your Company name<br/>",
                            new TextAreaTag(context, "orderAckHeader", shoppingSettings.getOrderAckHeader(), 80, 7)));

                    sb.append(new AdminRow("Order acknowledgement footer",
                            "This text will be inserted at the bottom of the order acknowledgement.<br/><br/>Special tags:<br/>"
                                    + "<i>[name]</i> - Members name<br/>" + "<i>[email]</i> - Members email<br/>"
                                    + "<i>[id]</i> - Generated id<br/>" + "<i>[login]</i> - Url to login page<br/>"
                                    + "<i>[loginurl]</i> - Login url<br/>" + "<i>[site]</i> - Your Company name<br/>",
                            new TextAreaTag(context, "orderAckFooter", shoppingSettings.getOrderAckFooter(), 80, 7)));

				}

                sb.append(new AdminRow("Third party notifications" + new ErrorTag(context, "orderNotificationEmails", "<br/>"),
						"Enter third party email addresses in here and they will be sent an email each time an order is placed.", new TextAreaTag(
								context, "orderNotificationEmails", shoppingSettings.getOrderNotificationEmailsString(), 50, 4)));

				if (Module.Listings.enabled(context)) {

					sb.append(new AdminRow("Seller emails",
							"Send an email to the user who listed each item bought informing them an order has been placed.",
							new BooleanRadioTag(context, "emailOrdersToSellers", shoppingSettings.isEmailSellers())));

					if (shoppingSettings.isEmailSellers()) {

						sb.append(new AdminRow("Seller email text", "This is the content of the email sent to the lister of an item sold.",
								new TextAreaTag(context, "sellerEmailText", shoppingSettings.getSellerEmailText(), 80, 7)));

					}

				}

				sb.append("</table>");
			}

			private void collections() {

				sb.append(new AdminTable("Collections"));

				sb.append(new AdminRow("Collection instructions", "This is the instruction to the customer for collections.", new TextAreaTag(context,
						"reservationInstructions", shoppingSettings.getCollectionInstructions(), 60, 5)));

				sb.append(new AdminRow("Reservation complete message", "This is the message shown to the customer when they have reserved an order.",
						new TextAreaTag(context, "reservationText", shoppingSettings.getReservationText(), 60, 5)));

				if (Module.Branches.enabled(context)) {

					if (config.isLocations()) {

						sb.append(new AdminRow("Branch search", "Should users search for a branch by location, or choose from a list.",
								new BooleanRadioTag(context, "branchSearch", shoppingSettings.isBranchSearch())));

					}
				}

				sb.append("</table>");
			}

			private void specialfields() {

				sb.append(new AdminTable("Special fields"));

				sb.append(new AdminRow("Customer references",
						"Allows the customer to enter their own reference for this order. For example, their Purchase Order number.",
						new BooleanRadioTag(context, "customerReference", shoppingSettings.isCustomerReference())));

				sb.append("</table>");
			}

			private void thirdPartyEmails() {

				sb.append(new AdminTable("Third party emails"));

				for (OrderEmail email : OrderEmail.get(context)) {
					sb.append("<tr>");
					sb
							.append("<td>" + new LinkTag(OrderEmailHandler.class, null, new SpannerGif(), "email", email) + " " + email.getName()
									+ "</td>");
					sb.append("<td>" + StringHelper.implode(email.getRecipients(), ", ") + "</td>");
					sb.append("<td width='20'>"
							+ new LinkTag(OrderEmailHandler.class, "delete", new DeleteGif(), "email", email)
									.setConfirmation("Are you sure you want to delete this email?") + "</td>");
					sb.append("</tr>");
				}

				sb.append("<tr><td colspan='3'>Add new email " + new ButtonTag(OrderEmailHandler.class, "create", "Add") + "</td></tr>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

                RendererUtil.tinyMce(context, sb, "basketContent", "checkoutContent", "checkoutConfirmationHeader");
				sb.append(new FormTag(ShoppingSettingsHandler.class, "save", "POST"));

                commands();
				basket();
				checkout();

				addresses();

				emails();

				thirdPartyEmails();

				if (shoppingSettings.isCollections()) {
					collections();
				}

				specialfields();

				if (company.isVatRegistered()) {
					vat();
				}

				content();

				commands();

				sb.append("</form>");

				return sb.toString();
			}

			private void vat() {

				sb.append(new AdminTable("Vat"));

				sb.append(new AdminRow("Vat exemption declaration", "Show the option to the customer for vat exemption", new BooleanRadioTag(context,
						"vatExemptionDeclaration", shoppingSettings.isVatExemptionDeclaration())));

				sb.append(new AdminRow("Vat exemption details", "Extra details for vat exemption", new BooleanRadioTag(context, "vatExemptionDetails",
						shoppingSettings.isVatExemptionDetails())));

				sb.append(new AdminRow("Vat exemption instructions", "Accompanying text to go with the vat exemption declaration.", new TextAreaTag(
						context, "vatExemptionText", shoppingSettings.getVatExemptionText(), 60, 5)));

				sb.append("</table>");
			}

		});
		return doc;
	}

	public Object save() {

        if (orderNotificationEmails != null) {
            List<String> emails = Arrays.asList(orderNotificationEmails.split("\r\n"));
            for (String email : emails) {
                String error = new EmailValidator().validate(email);
                if (error != null) {
                    context.setError("orderNotificationEmails", "Invalid email");
                    addError("Please correct the errors and click update");
                    return main();
                }
            }
        }

        shoppingSettings.setBasketContent(basketContent);

		shoppingSettings.setMinOrderValue(minOrderValue);

		shoppingSettings.setSeparateAddresses(separateAddresses);
		shoppingSettings.setAddressBook(addressBook);

		// basket
		shoppingSettings.setClearBasketOnLogout(clearBasketOnLogout);

		// vat
		shoppingSettings.setVatExemptionDeclaration(vatExemptionDeclaration);
		shoppingSettings.setVatExemptionText(vatExemptionText);
		shoppingSettings.setVatExemptionDetails(vatExemptionDetails);

		// checkout
		shoppingSettings.setCheckoutAccounts(checkoutAccounts);
		shoppingSettings.setCheckoutCompletedText(checkoutCompletedText);
		shoppingSettings.setCheckoutContent(checkoutContent);
		shoppingSettings.setCheckoutScripts(checkoutScripts);
		shoppingSettings.setCheckoutConfirmationHeader(checkoutConfirmationHeader);
		shoppingSettings.setPlaceOrderText(placeOrderText);
		shoppingSettings.setCreateAccountForGuest(createAccountForGuest);
		shoppingSettings.setFormsFirst(formsFirst);
		shoppingSettings.setAgreement(agreement);
		shoppingSettings.setAddressBook(addressBook);
		shoppingSettings.setReferrers(StringHelper.explodeStrings(referrers, "\n"));
		shoppingSettings.setSpendPerCredit(spendPerCredit);
		shoppingSettings.setCancellationContent(cancellationContent);

		// address
		shoppingSettings.setDeliveryAddressHeader(deliveryAddressHeader);
		shoppingSettings.setBillingAddressHeader(billingAddressHeader);
		shoppingSettings.setAddresses(addresses);

		/*
		 * acknowledgements
		 */
		shoppingSettings.setOrderAcknowledgement(orderAcknowledgement);
        shoppingSettings.setOrderAcknowledgementHtml(orderAcknowledgementHtml);
		shoppingSettings.setOrderAckHeader(orderAckHeader);
		shoppingSettings.setOrderAckFooter(orderAckFooter);
		shoppingSettings.setOrderNotificationEmails(StringHelper.explodeStrings(orderNotificationEmails, "[,\\s\\n]"));

		shoppingSettings.setCustomerReference(customerReference);
		shoppingSettings.setNoCountries(noCountries);

		shoppingSettings.setQuickBasketAdd(quickBasketAdd);

		// collections
		shoppingSettings.setDispatchType(dispatchOptions);
		shoppingSettings.setReservationText(reservationText);
		shoppingSettings.setReservationInstructions(reservationInstructions);
		shoppingSettings.setBranchSearch(branchSearch);

		// Seller ORDER EMAIL
		shoppingSettings.setSellerEmailText(sellerEmailText);
		shoppingSettings.setEmailSellers(emailOrdersToSellers);

		shoppingSettings.save();

		captions.setBasketUpdatedCaption(basketUpdatedCaption);
		captions.setEmptyBasketCaption(emptyBasketCaption);
		captions.setAddBasketItemCaption(addBasketItemCaption);
		captions.setBasketClearedCaption(basketClearedCaption);
		captions.save();

		addMessage("Shopping settings updated");
		clearParameters();
		return main();
	}
}
