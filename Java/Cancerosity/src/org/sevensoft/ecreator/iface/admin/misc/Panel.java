package org.sevensoft.ecreator.iface.admin.misc;

import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 6 Mar 2007 06:18:52
 *
 */
public abstract class Panel {

	protected StringBuilder			sb;
	protected final RequestContext	context;

	protected Panel(RequestContext context) {
		this.context = context;
	}

	public abstract void makeString();

	@Override
	public final String toString() {

		if (sb == null) {
			sb = new StringBuilder();
			makeString();
		}

		return sb.toString();
	}

}
