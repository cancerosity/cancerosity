package org.sevensoft.ecreator.iface.admin.extras.polls;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.categories.menu.CategoryMenu;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasHandler;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.extras.polls.Option;
import org.sevensoft.ecreator.model.extras.polls.Poll;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 07-Jul-2005 12:39:17
 * 
 */
@Path("admin-polls.do")
public class PollHandler extends AdminHandler {

	private transient MiscSettings	miscSettings	= MiscSettings.getInstance(context);

	private boolean				live;
	private Option				option;
	private Poll				poll;
	private String				title;
	private boolean				sidebar;
	private Map<Integer, String>		optionText;
	private String				content;

	public PollHandler(RequestContext context) {
		super(context);
	}

	public Object create() {

		AdminDoc doc = new AdminDoc(context, user, "Create a poll", Tab.Extras);
		doc.setIntro("Create a new poll by giving it a title");
		if (miscSettings.isAdvancedMode())
			doc.setMenu(new ExtrasMenu());
		doc.addBody(new Body() {

            private void commands() {
                sb.append("<div align='center' class='actions'>" + new SubmitTag("Create poll") + "</div>");
            }

			@Override
			public String toString() {

				sb.append(new FormTag(PollHandler.class, "doCreate", "POST"));

                commands();

				sb.append(new TableTag("simplebox", "center"));

				sb.append("<tr><td align='center'>Enter the name of the poll to create.</td></tr>");
				sb.append("<tr><td align='center'>" + new TextTag(context, "title", 40) + "<br/><br/></td></tr>");
				sb.append("</table>");

				commands();

				sb.append("</form>");
				return sb.toString();
			}

		});
		return doc;

	}

	public Object delete() throws ServletException {

		if (poll == null)
			return main();

		poll.delete();
		if (miscSettings.isAdvancedMode())
			return new ExtrasHandler(context).main();

		AdminDoc doc = new AdminDoc(context, user, "The poll has been deleted", Tab.Extras);
		doc.setMenu(new CategoryMenu());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("simplelinks", "center"));
				sb.append("<tr><th align='center'>What would you like to do ?</th></tr>");

				sb.append("<tr><td align='center'>" + new LinkTag(PollHandler.class, "create", "I want to create a new poll") + "</td></tr>");

				sb.append("<tr><td align='center'>" + new LinkTag(ExtrasHandler.class, null, "I want to return to the extras menu") + "</td></tr>");

				sb.append("</table>");

				return sb.toString();
			}
		});
		return doc;
	}

	public Object doCreate() throws ServletException {

		if (title == null)
			return create();

		poll = new Poll(context, title);

		AdminDoc doc = new AdminDoc(context, user, "This poll has been created", Tab.Extras);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("simplelinks", "center"));

				sb.append("<tr><th>What do you want to do now?</th></tr>");
				sb.append("<tr><td align='center'>" + new LinkTag(PollHandler.class, "edit", "I want to edit this poll", "poll", poll) + "</td></tr>");
				sb.append("<tr><td align='center'>" + new LinkTag(ExtrasHandler.class, null, "I want to return to the extras menu") + "</td></tr>");

				sb.append("</table>");

				return sb.toString();
			}
		});
		return doc;
	}

	public Object edit() {

		if (poll == null)
			return main();

		AdminDoc doc = new AdminDoc(context, user, "Polls", Tab.Extras);
		if (miscSettings.isAdvancedMode()) {
			doc.setMenu(new ExtrasMenu());
		}
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update poll"));
				sb.append(new ButtonTag(PollHandler.class, "delete", "Delete poll", "poll", poll)
						.setConfirmation("Are you sure you want to delete this poll?"));
				sb.append(new ButtonTag(PollHandler.class, null, "Return to menu"));

				sb.append("</div>");
			}

			private void content() {

				sb.append(new AdminTable("Results page content"));
				sb.append("<tr><td>" + new TextAreaTag(context, "content", poll.getContent()).setStyle("width: 100%; height: 260px;") + "</td></tr>");
				sb.append("</table>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Title", "Enter the question or heading of this poll", new TextTag(context, "title", poll.getTitle(), 60)));

				sb.append(new AdminRow("Reset", "Reset all votes to zero for this poll.", new ButtonTag(PollHandler.class, "reset", "Reset all votes",
						"poll", poll).setConfirmation("Are you sure you want to reset the votes for this poll?")));

				sb.append("</table>");
			}

			private void options() {

				sb.append(new AdminTable("Options"));

				sb.append("<tr>");
				sb.append("<th>Position</th>");
				sb.append("<th>Text</th>");
				sb.append("<th>Votes</th>");
				sb.append("<th>Reset</th>");
				sb.append("</tr>");

				PositionRender pr = new PositionRender(PollHandler.class, "option");

				for (Option option : poll.getOptions()) {

					sb.append("<tr>");
					sb.append("<td width='140'>" + pr.render(option) + "</td>");
					sb.append("<td>" + new TextTag(context, "optionText~" + option.getIdString(), option.getText(), 40) + "</td>");
					sb.append("<td>" + option.getVotes() + "</td>");
					sb.append("<td>"
							+ new ButtonTag(PollHandler.class, "reset", "Reset votes", "poll", poll, "option", option)
									.setConfirmation("Are you sure you want to reset the votes for this option?") + "</td>");
					sb.append("</tr>");
				}

				sb.append("<tr><td colspan='4'>Add new option " + new TextTag(context, "optionText_0", 30) + "</td></tr>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				if (miscSettings.isHtmlEditor())
                    RendererUtil.tinyMce(context, sb, "content");

				sb.append(new FormTag(PollHandler.class, "save", "POST"));
				sb.append(new HiddenTag("poll", poll));

                commands();
				general();
				options();
				content();
				commands();

				sb.append("</form>");
				return sb.toString();
			}

		});
		return doc;
	}

	public Object list() {

		AdminDoc doc = new AdminDoc(context, user, "Edit poll", Tab.Extras);
		if (miscSettings.isAdvancedMode())
			doc.setMenu(new ExtrasMenu());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(PollHandler.class, "edit", "POST"));

				sb.append(new TableTag("simplebox", "center"));

				sb.append("<tr><td align='center'>Select the poll you want to edit.</td></tr>");
				sb.append("<tr><td align='center'>" + new SelectTag(context, "poll", null, Poll.get(context), "-Choose poll-") + "</td></tr>");

				sb.append("<tr><td align='center'>" + new SubmitTag("Continue >") + "</td></tr>");
				sb.append("</table>");

				sb.append("</form>");
				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() {
		return list();
	}

	public Object moveDown() {

		if (option == null)
			return edit();

		poll = option.getPoll();
		List<Option> options = poll.getOptions();
		EntityUtil.moveDown(option, options);

		return edit();
	}

	public Object moveUp() {

		if (option == null)
			return edit();

		poll = option.getPoll();
		List<Option> options = poll.getOptions();
		EntityUtil.moveUp(option, options);

		return edit();
	}

	public Object removeOption() {

		if (poll == null || option == null)
			return edit();

		poll.removeOption(option);
		return edit();
	}

	public Object reset() {

		if (poll == null)
			return main();

		if (option == null)
			poll.reset();
		else
			poll.reset(option);

		addMessage("All votes reset");
		return edit();
	}

	public Object save() {

		if (poll == null)
			return main();

		test(new RequiredValidator(), "title");
		if (hasErrors())
			return edit();

		poll.setTitle(title);
		poll.setContent(content);
		poll.save();

		for (Option option : poll.getOptions()) {
			option.setText(optionText.get(option.getId()));
			option.save();
		}

		String text = optionText.get(0);
		if (text != null)
			poll.addOption(text);

		clearParameters();
		addMessage("Changes saved.");
		return edit();
	}
}
