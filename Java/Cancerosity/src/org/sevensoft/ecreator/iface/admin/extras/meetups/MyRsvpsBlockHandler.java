package org.sevensoft.ecreator.iface.admin.extras.meetups;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.extras.meetups.blocks.ItemRsvpsBlock;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 26 Apr 2007 09:58:50
 *
 */
@Path("admin-blocks-myrsvps.do")
public class MyRsvpsBlockHandler extends BlockEditHandler {

	private ItemRsvpsBlock	block;

	public MyRsvpsBlockHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Block getBlock() {
		return block;
	}

	@Override
	protected void saveSpecific() {
	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

}
