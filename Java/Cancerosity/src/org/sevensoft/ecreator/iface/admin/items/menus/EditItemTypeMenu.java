package org.sevensoft.ecreator.iface.admin.items.menus;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.ecreator.iface.admin.accounts.AccountModuleHandler;
import org.sevensoft.ecreator.iface.admin.accounts.registration.RegistrationsModuleHandler;
import org.sevensoft.ecreator.iface.admin.accounts.subscriptions.SubscriptionModuleHandler;
import org.sevensoft.ecreator.iface.admin.design.markup.MarkupHandler;
import org.sevensoft.ecreator.iface.admin.ecom.bookings.config.BookingModuleHandler;
import org.sevensoft.ecreator.iface.admin.extras.tellfriend.TellFriendSettingsHandler;
import org.sevensoft.ecreator.iface.admin.interaction.InteractionModuleHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.iface.admin.items.listings.ListingModuleHandler;
import org.sevensoft.ecreator.iface.admin.items.modules.alternatives.AlternativesHandler;
import org.sevensoft.ecreator.iface.admin.items.modules.ordering.OrderingModuleHandler;
import org.sevensoft.ecreator.iface.admin.items.modules.reviews.ReviewsModuleHandler;
import org.sevensoft.ecreator.iface.admin.items.modules.stock.StockModuleHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Menu;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 23 Oct 2006 18:31:39
 *
 */
public class EditItemTypeMenu extends Menu {

	private final ItemType	itemType;

	public EditItemTypeMenu(ItemType itemType) {
		this.itemType = itemType;
	}

	@Override
	public List<LinkTag> getLinkTags(RequestContext context) {

		List<LinkTag> tags = new ArrayList();

		tags.add(new LinkTag(ItemTypeHandler.class, "edit", "Details", "itemType", itemType));

		if (ItemModule.Ordering.enabled(context, itemType)) {
			tags.add(new LinkTag(OrderingModuleHandler.class, null, "Ordering", "itemType", itemType));
		}

		if (ItemModule.Pricing.enabled(context, itemType)) {
			tags.add(new LinkTag(ItemTypeHandler.class, "pricing", "Pricing", "itemType", itemType));
		}

        if (ItemModule.Options.enabled(context, itemType)) {
            tags.add(new LinkTag(ItemTypeHandler.class, "options", "Options", "itemType", itemType));
        }

		if (Module.Availabilitity.enabled(context) && ItemModule.Stock.enabled(context, itemType)) {
			tags.add(new LinkTag(StockModuleHandler.class, null, "Stock", "itemType", itemType));
		}

		if (ItemModule.Alternatives.enabled(context, itemType)) {
			tags.add(new LinkTag(AlternativesHandler.class, null, "Alternatives", "itemType", itemType));
		}

		tags.add(new LinkTag(ItemTypeHandler.class, "sorts", "Sorts", "itemType", itemType));

		if (ItemModule.Account.enabled(context, itemType)) {
			tags.add(new LinkTag(AccountModuleHandler.class, null, "Account", "itemType", itemType));
		}

		if (ItemModule.Listings.enabled(context, itemType)) {
			tags.add(new LinkTag(ListingModuleHandler.class, null, "Listings", "itemType", itemType));
		}

		if (ItemModule.Bookings.enabled(context, itemType)) {
			tags.add(new LinkTag(BookingModuleHandler.class, null, "Bookings", "itemType", itemType));
		}

		if (ItemModule.Subscriptions.enabled(context, itemType)) {
			tags.add(new LinkTag(SubscriptionModuleHandler.class, null, "Subscriptions", "itemType", itemType));
		}

		if (ItemModule.Registrations.enabled(context, itemType)) {
			tags.add(new LinkTag(RegistrationsModuleHandler.class, null, "Registration", "itemType", itemType));
		}

		if (ItemModule.Reviews.enabled(context, itemType)) {
			tags.add(new LinkTag(ReviewsModuleHandler.class, null, "Reviews", "itemType", itemType));
		}

		if (ItemModule.Ratings.enabled(context, itemType)) {
			tags.add(new LinkTag(ItemTypeHandler.class, "ratings", "Ratings", "itemType", itemType));
		}

		if (Module.PrivateMessages.enabled(context) || Module.Buddies.enabled(context)) {
			tags.add(new LinkTag(InteractionModuleHandler.class, null, "Interactions", "itemType", itemType));
		}

		if (ItemModule.TellFriend.enabled(context, itemType)) {
			tags.add(new LinkTag(TellFriendSettingsHandler.class, null, "Tell a Friend", "itemType", itemType));
		}

		tags.add(new LinkTag(ItemTypeHandler.class, "expiryBots", "Expiry bots", "itemType", itemType));

		if (Module.UserMarkup.enabled(context) || context.containsAttribute("superman")) {

			tags.add(new LinkTag(MarkupHandler.class, "edit", "List markup", "markup", itemType.getListMarkup()));
			tags.add(new LinkTag(MarkupHandler.class, "edit", "View markup", "markup", itemType.getViewMarkup()));

			tags.add(new LinkTag(ItemTypeHandler.class, null, "All item types"));
		}

		return tags;
	}

}
