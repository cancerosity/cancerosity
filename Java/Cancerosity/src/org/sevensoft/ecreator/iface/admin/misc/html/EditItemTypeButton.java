package org.sevensoft.ecreator.iface.admin.misc.html;

import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 26 Oct 2006 11:45:53
 *
 */
public class EditItemTypeButton {

	private String	id;

	public EditItemTypeButton(String id) {
		this.id = id;
	}

	@Override
	public String toString() {

		return new ButtonTag("Edit item type").setOnClick(
				"window.location='" + new Link(ItemTypeHandler.class, "edit") + "&itemType=' + document.getElementById('" + id + "').value;")
				.toString();
	}

}
