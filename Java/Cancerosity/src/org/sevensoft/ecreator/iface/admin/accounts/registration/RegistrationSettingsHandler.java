package org.sevensoft.ecreator.iface.admin.accounts.registration;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.accounts.registration.RegistrationSettings;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 29 Dec 2006 01:35:15
 *
 */
@Path("admin-accounts-registration-settings.do")
public class RegistrationSettingsHandler extends AdminHandler {

	private final transient RegistrationSettings	registrationSettings;

	private String						itemTypeHeader;
	private String						itemTypeFooter;
	private String						detailsHeader;
	private String						detailsFooter;
	private String						serverEmailAddress;
	private String						referrers;

	public RegistrationSettingsHandler(RequestContext context) {
		super(context);
		this.registrationSettings = RegistrationSettings.getInstance(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Account Settings", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update account settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, "edit", "Return to settings menu"));
				sb.append("</div>");
			}

			private void content() {

				sb.append(new AdminTable("Account type page"));

				sb.append("<tr><th colspan='2'>Account type header</th></tr><tr><td colspan='2'>"
						+ new TextAreaTag(context, "itemTypeHeader", registrationSettings.getItemTypeHeader(), 70, 8) + "</td></tr>");

				sb.append("<tr><th colspan='2'>Account type footer</th></tr><tr><td colspan='2'>"
						+ new TextAreaTag(context, "itemTypeFooter", registrationSettings.getItemTypeFooter(), 70, 8) + "</td></tr>");

				sb.append("<tr><th colspan='2'>Details page header</th></tr><tr><td colspan='2'>"
						+ new TextAreaTag(context, "detailsHeader", registrationSettings.getDetailsHeader(), 70, 8) + "</td></tr>");

				sb.append("<tr><th colspan='2'>Details page  footer</th></tr><tr><td colspan='2'>"
						+ new TextAreaTag(context, "detailsFooter", registrationSettings.getDetailsFooter(), 70, 8) + "</td></tr>");

				sb.append("</table>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Referrers",
						"Referrers on signup. Used by all account types unless overridden in the registration module of that account type.<br/>"
								+ "<i>Put each referrer on a new line</i>", new TextAreaTag(context, "referrers", StringHelper.implode(
								registrationSettings.getReferrers(), "\n"), 50, 4)));

				sb.append(new AdminRow("Server email",
						"Enter the email address you want registration emails to be sent from. Ie, admin@yourdomain.com. "
								+ "If you leave this blank it will use the default outgoing address.", new TextTag(context,
								"serverEmailAddress", 40)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(RegistrationSettingsHandler.class, "save", "post"));

                commands();
				general();
				content();
				commands();

				sb.append("</form>");

				return sb.toString();
			}
		});
		return doc;
	}

	public Object save() throws ServletException {

		registrationSettings.setItemTypeHeader(itemTypeHeader);
		registrationSettings.setItemTypeFooter(itemTypeFooter);

		registrationSettings.setDetailsFooter(detailsFooter);
		registrationSettings.setDetailsHeader(detailsHeader);

		registrationSettings.setServerEmailAddress(serverEmailAddress);
		registrationSettings.setReferrers(StringHelper.explodeStrings(referrers, "\n"));

		registrationSettings.save();

		addMessage("Registration settings updated");
		clearParameters();
		return main();
	}
}
