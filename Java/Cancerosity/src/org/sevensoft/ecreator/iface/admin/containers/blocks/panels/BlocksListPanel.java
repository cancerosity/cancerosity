package org.sevensoft.ecreator.iface.admin.containers.blocks.panels;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockHandler;
import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.advertising.banners.Banner;
import org.sevensoft.ecreator.model.advertising.banners.blocks.BannerBlock;
import org.sevensoft.ecreator.model.advertising.banners.blocks.BannerRotationBlock;
import org.sevensoft.ecreator.model.bookings.block.AvailabilityChartBlock;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.blocks.SiblingsBlock;
import org.sevensoft.ecreator.model.categories.blocks.SubcategoriesBlock;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.BlockOwner;
import org.sevensoft.ecreator.model.containers.blocks.shortcuts.BlockShortcut;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.blocks.FormBlock;
import org.sevensoft.ecreator.model.crm.jobsheets.blocks.JobsheetSubmitBlock;
import org.sevensoft.ecreator.model.extras.calculators.Calculator;
import org.sevensoft.ecreator.model.extras.calculators.blocks.CalculatorBlock;
import org.sevensoft.ecreator.model.extras.calendars.blocks.CalendarBlock;
import org.sevensoft.ecreator.model.extras.mapping.google.blocks.GoogleMapBlock;
import org.sevensoft.ecreator.model.extras.mapping.google.blocks.GoogleMapBlockV3;
import org.sevensoft.ecreator.model.extras.meetups.blocks.MyRsvpsBlock;
import org.sevensoft.ecreator.model.extras.polls.Poll;
import org.sevensoft.ecreator.model.extras.polls.blocks.PollBlock;
import org.sevensoft.ecreator.model.extras.rss.Newsfeed;
import org.sevensoft.ecreator.model.extras.rss.NewsfeedBlock;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.ibex.blocks.IbexBlock;
import org.sevensoft.ecreator.model.items.highlighted.Ticker;
import org.sevensoft.ecreator.model.items.highlighted.blocks.HighlightedItemsBlock;
import org.sevensoft.ecreator.model.items.relations.RelationGroup;
import org.sevensoft.ecreator.model.items.reviews.ReviewsBlock;
import org.sevensoft.ecreator.model.marketing.newsletter.blocks.NewsletterBlock;
import org.sevensoft.ecreator.model.marketing.sms.blocks.SmsBlock;
import org.sevensoft.ecreator.model.media.images.galleries.blocks.GalleryBlock;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.misc.adwords.GoogleAdwordsBlock;
import org.sevensoft.ecreator.model.misc.content.ContentBlock;
import org.sevensoft.ecreator.model.misc.content.clouds.TagCloudBlock;
import org.sevensoft.ecreator.model.reminders.blocks.HolidayReminderBlock;
import org.sevensoft.ecreator.model.search.forms.blocks.SearchBlock;
import org.sevensoft.ecreator.model.search.wizards.Wizard;
import org.sevensoft.ecreator.model.search.wizards.WizardBlock;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.comments.blocks.CommentPostBlock;
import org.sevensoft.ecreator.model.sitemap.blocks.SitemapBlock;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author sks 26 Jun 2006 18:48:53
 */
public class BlocksListPanel extends Panel {

    private static Logger logger = Logger.getLogger("ecreator");

    private final List<Block> blocks;
    private Category category;
    private ItemType itemType;
    private final BlockOwner owner;
    private MiscSettings miscSettings;

    public BlocksListPanel(RequestContext context, BlockOwner owner) {

        super(context);

        this.owner = owner;
        this.miscSettings = MiscSettings.getInstance(context);

        if (owner instanceof Category) {
            this.category = (Category) owner;
        } else if (owner instanceof ItemType) {
            this.itemType = (ItemType) owner;
        } else {
            throw new RuntimeException("Owner type not recognised: " + owner);
        }

        this.blocks = new ArrayList<Block>(CollectionsUtil.filter(owner.getBlocks(), new Predicate<Block>() {
            public boolean accept(Block b) {
                return !(b instanceof GalleryBlock);
            }
        }));

        logger.fine("[BlocksListPanel] showing blocks for owner=" + owner);
        logger.fine("[BlocksListPanel] blocks=" + blocks);
    }

    private SelectTag getAddBlockTag() {

        SelectTag blocksTag = new SelectTag(context, "addBlock");
        blocksTag.setLabelComparator();

        blocksTag.setAny("-Select add on-");

        /*
           * Blocks available on all types
           */
        if (Module.Forms.enabled(context)) {

            for (Form form : Form.get(context)) {
                blocksTag.addOption(FormBlock.class.getName() + ";" + form.getId(), "Form: " + form.getName());
            }
        }

        if (Module.GoogleMap.enabled(context)) {
            blocksTag.addOption(GoogleMapBlock.class.getName(), "Google map");
        }

        if (Module.GoogleMapV3.enabled(context)) {
            blocksTag.addOption(GoogleMapBlockV3.class.getName(), "Google map V3");
        }

        if (Module.HolidayReminders.enabled(context)) {
            blocksTag.addOption(HolidayReminderBlock.class.getName(), "Holiday reminders");
        }

        /*
		 * Item only blocks
		 */
        if (itemType != null) {

            if (Module.Bookings.enabled(context)) {
                blocksTag.addOption(AvailabilityChartBlock.class.getName(), "Availability chart");
            }

            if (ItemModule.Reviews.enabled(context, itemType)) {
                blocksTag.addOption(ReviewsBlock.class.getName(), "Reviews");
            }

            if (Module.Meetups.enabled(context)) {
                blocksTag.addOption(MyRsvpsBlock.class.getName(), "My Rsvps");
                blocksTag.addOption(org.sevensoft.ecreator.model.extras.meetups.blocks.ItemRsvpsBlock.class.getName(), "Item Rsvps");
            }

            if (Module.Relations.enabled(context)) {
                for (RelationGroup relationGroup : RelationGroup.get(context)) {
                    blocksTag.addOption(RelationGroup.class.getName() + ";" + relationGroup.getId(), "Relation group: " + relationGroup.getName());
                }
            }

            if (ItemModule.Comments.enabled(context, itemType)){
                blocksTag.addOption(CommentPostBlock.class, "Add comment");
            }

            if (ItemModule.IBex.enabled(context, itemType)) {
                blocksTag.addOption(IbexBlock.class, "iBex");
            }
        }

        /*
           * Here are category only blocks
           */
        if (category != null) {

            /*
                * Advanced mode blocks only
                */
            if (miscSettings.isAdvancedMode()) {

                blocksTag.addOption(SubcategoriesBlock.class, "Subcategories");
                blocksTag.addOption(SiblingsBlock.class, "Siblings");

                blocksTag.addOption(SearchBlock.class, "Search");
                blocksTag.addOption(ContentBlock.class, "Content");

                if (Module.Banners.enabled(context)) {

                    blocksTag.addOption(BannerRotationBlock.class, "Banner rotation");

                    for (Banner banner : Banner.get(context)) {
                        blocksTag.addOption(BannerBlock.class.getName() + ";" + banner.getId(), "Banner: " + banner.getName());
                    }
                }

            }

            blocksTag.addOption(Ticker.class, "Ticker");
            blocksTag.addOption(HighlightedItemsBlock.class, "Highlighted items");

            if (Module.Calculators.enabled(context)) {

                for (Calculator calculator : Calculator.get(context)) {
                    blocksTag.addOption(CalculatorBlock.class.getName() + ";" + calculator.getId(), "Calculator: " + calculator.getName());
                }
            }

            if (Module.Jobsheets.enabled(context)) {
                blocksTag.addOption(JobsheetSubmitBlock.class, "Jobsheet submit");
            }

            if (Module.TagClouds.enabled(context)) {
                blocksTag.addOption(TagCloudBlock.class, "Tag cloud");
            }

            if (Module.GoogleAdwords.enabled(context)) {
                blocksTag.addOption(GoogleAdwordsBlock.class.getName(), "Google adwords");
            }

            if (Module.Calendars.enabled(context)) {
                blocksTag.addOption(CalendarBlock.class.getName(), "Calendar");
            }

            if (Module.Newsfeed.enabled(context)) {

                for (Newsfeed newsfeed : Newsfeed.get(context)) {
                    blocksTag.addOption(NewsfeedBlock.class.getName() + ";" + newsfeed.getId(), "Newsfeed: #" + newsfeed.getIdString() + " " + newsfeed.getTitle());
                }
            }

            if (Module.Newsletter.enabled(context)) {
                blocksTag.addOption(NewsletterBlock.class.getName(), "Newsletter");
            }

            if (Module.Sms.enabled(context)) {
                blocksTag.addOption(SmsBlock.class.getName(), "Sms");
            }

            if (Module.Wizards.enabled(context)) {
                for (Wizard wizard : Wizard.get(context)) {
                    blocksTag.addOption(WizardBlock.class.getName() + ";" + wizard.getId(), "Wizard: " + wizard.getName());
                }
            }

            if (Module.Polls.enabled(context)) {
                for (Poll poll : Poll.get(context)) {
                    blocksTag.addOption(PollBlock.class.getName() + ";" + poll.getId(), "Poll: " + poll.getTitle());
                }
            }

//            if (Module.Galleries.enabled(context)) {
//                blocksTag.addOption(GalleryBlock.class.getName(), "Gallery");
//            }

            if(Module.Comments.enabled(context)){
                blocksTag.addOption(CommentPostBlock.class, "Add comment");
            }

            if(Module.SiteMap.enabled(context)){
                blocksTag.addOption(SitemapBlock.class, "SiteMap");
            }
        }

        return blocksTag;
    }

    private SelectTag getShortcutsTag() {

        List<BlockShortcut> shortcuts = null;
        if (category != null) {
            shortcuts = BlockShortcut.getCategory();
        }

        if (shortcuts == null || shortcuts.isEmpty()) {
            return null;
        }

        SelectTag tag = new SelectTag(context, "addBlockShortcut");
        tag.setAny("-Choose shortcut-");
        tag.addOptions(shortcuts);

        return tag;
    }

    public void makeString() {

        sb.append(new AdminTable("Add ons"));

        if (miscSettings.isAdvancedMode()) {
            sb.append("<tr>");
            sb.append("<th width='100'>Position</th>");
            sb.append("<th>Name</th>");
            sb.append("<th width='10'> </th>");
            sb.append("</tr>");
        }

        PositionRender pr = new PositionRender(BlockHandler.class, "block");

        for (Block block : blocks) {

            if (miscSettings.isAdvancedMode() || block.isSimple()) {

                sb.append("<tr>");

                if (miscSettings.isAdvancedMode()) {
                    sb.append("<td width='100'>" + pr.render(block.getInstanceId()) + "</td>");
                }

                sb.append("<td>");

                if (block.isEditable()) {

                    Class clazz = block.getEditHandler();
                    if (clazz != null) {
                        sb.append(new LinkTag(clazz, null, new SpannerGif(), "block", block));
                        sb.append(" ");
                    }
                }

                sb.append(block.getName());
                sb.append("</td>");

                sb.append("<td width='10'>" +
                        new LinkTag(BlockHandler.class, "delete", new DeleteGif(), "block", block.getInstanceId())
                                .setConfirmation("Are you sure you want to delete this category add on?") + "</td>");

                sb.append("</tr>");

            }
        }

        sb.append("<tr><td colspan='2'>Add category add on: " + getAddBlockTag() + " " + new SubmitTag("Add") + "</td><td align='right' colspan=2'>");
//        SelectTag shortcutsTag = getShortcutsTag();
//        if (shortcutsTag != null) {
//            sb.append("Shortcuts: " + shortcutsTag);
//        }
        sb.append("</td></tr>");

        sb.append("</table>");

    }
}
