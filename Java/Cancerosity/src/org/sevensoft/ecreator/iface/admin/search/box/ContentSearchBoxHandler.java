package org.sevensoft.ecreator.iface.admin.search.box;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.search.forms.boxes.ContentSearchBox;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 26 Jan 2007 09:05:09
 *
 */
@Path("admin-boxes-search-content.do")
public class ContentSearchBoxHandler extends BoxHandler {

	private ContentSearchBox	box;
	private String			submitLabel;
	private String			footer;
	private String			header;
	private int	resultsPerPage;

	public ContentSearchBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {

		box.setHeader(header);
		box.setFooter(footer);
		box.setSubmitLabel(submitLabel);
		box.setResultsPerPage(resultsPerPage);

	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Settings"));

		sb.append(new AdminRow("Header", "This text appears above the input field.", new TextAreaTag(context, "header", box.getHeader(), 50, 3)));

		sb.append(new AdminRow("Footer", "This text appears below the input field.", new TextAreaTag(context, "footer", box.getFooter(), 50, 3)));

		sb.append(new AdminRow("Results per page", "Set the number of results to appear per page.", new TextTag(context, "resultsPerPage", box
				.getResultsPerPage(), 6)));

		sb.append(new AdminRow("Submit label", "This is the text that is used as the label for the submit button.", new TextTag(context, "submitLabel", box
				.getSubmitLabel())));

		sb.append("</table>");
	}

}
