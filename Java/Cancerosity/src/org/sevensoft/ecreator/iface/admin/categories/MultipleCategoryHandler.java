package org.sevensoft.ecreator.iface.admin.categories;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.categories.menu.CategoryMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 8 Sep 2006 17:16:33
 *
 */
@Path("admin-categories-multi.do")
public class MultipleCategoryHandler extends AdminHandler {

    private static final int	Limit	= 500;

	/**
	 * The categories to move about
	 */
	private List<Category>	categories;
	private Category		parent;
	private String		move;
	private String		delete;
	private String		rename;
	private String		replacement;
	private String		target;
    private int page;

	public MultipleCategoryHandler(RequestContext context) {
		super(context);
	}

	public Object action() throws ServletException {

		if (move != null) {
			move();
		}

		if (delete != null) {
			delete();
		}

		if (rename != null) {
			rename();
		}

		clearParameters();
		return main();
	}

	/**
	 * 
	 */
	private void rename() {

		int n = 0;
		for (Category category : categories) {
			category.rename(target, replacement);
			n++;

		}
		addMessage(n + " categories renamed");
	}

	private void delete() {

		if (categories.isEmpty()) {
			return;
		}

		for (Category category : categories) {
			if (category.isChild()) {
				category.delete(false);
			}
		}

		addMessage("Categories deleted");
	}

	@Override
	public Object main() throws ServletException {

//		AdminDoc doc = new AdminDoc(context, user, "Multiple category editor", Tab.Categories);
//		doc.setMenu(new CategoryMenu());

		int count = Category.getCount(context);

		final Results results = new Results(count, page, Limit);

		final List<Category> list = Category.get(context, results.getStartIndex(), Limit);

        AdminDoc doc = new AdminDoc(context, user, "Multiple category editor", null);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Categories"));
				sb.append(new FormTag(MultipleCategoryHandler.class, "action", "post"));

				sb.append("<tr><td>Move selected categories to: " + new SelectTag(context, "parent", null, Category.getCategoryOptions(context), "Choose new parent category") + " "
						+ new SubmitTag("move", "Move") + "</td></tr>");

				sb.append("<tr><td>Delete selected categories " + new SubmitTag("delete", "Delete") + "</td></tr>");

				sb.append("<tr><td>Rename titles, replace " + new TextTag(context, "target", 14) + " with " + new TextTag(context, "replacement", 14)
						+ " " + new SubmitTag("rename", "Rename") + "</td></tr>");

                sb.append("<tr><td> " + new ResultsControl(context, results, new Link(MultipleCategoryHandler.class)) + "</td></tr>");

				for (Category category : list) {

					sb.append("<tr><td>" + new CheckTag(context, "categories", category, false) + " "
							+ new LinkTag(CategoryHandler.class, "edit", category.getFullName(" > "), "category", category) + "</td></tr>");
				}

				sb.append("</form>");
				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	private void move() throws ServletException {

		if (parent == null) {
			addMessage("You must select a new parent category");
			return;
		}

		if (categories.isEmpty()) {
			return;
		}

		// check each category is not a parent of the new parent
		for (Category category : categories) {

			if (category.getChildrenDeep().contains(parent)) {
				addMessage("You cannot move categories into their own subcategories");
				return;
			}
		}

		int n = 0;
		for (Category category : categories) {

			category.setParent(parent);
			n++;
		}

		addMessage(n + " categories moved to '" + parent.getName() + "'");
	}
}
