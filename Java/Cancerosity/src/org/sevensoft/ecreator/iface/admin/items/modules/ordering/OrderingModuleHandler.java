package org.sevensoft.ecreator.iface.admin.items.modules.ordering;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemTypeMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.products.ordering.OrderingModule;
import org.sevensoft.ecreator.model.products.ordering.OrderingModule.OrderingMethod;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;

/**
 * @author sks 28 Dec 2006 12:32:30
 *
 */
@Path("admin-ordering-module.do")
public class OrderingModuleHandler extends AdminHandler {

	private ItemType		itemType;
	private OrderingMethod	orderingMethod;

	public OrderingModuleHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (itemType == null) {
			return index();
		}

		final OrderingModule module = itemType.getOrderingModule();

		AdminDoc docs = new AdminDoc(context, user, "Edit item type: " + itemType.getName(), null);
		docs.setMenu(new EditItemTypeMenu(itemType));
		docs.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update item type"));
				sb.append(new ButtonTag(ItemTypeHandler.class, null, "Return to item types menu"));

				sb.append("</div>");
			}

			private void stock() {

				sb.append(new AdminTable("Ordering module"));

				sb.append(new AdminRow("Ordering method", "Choose how you want to buy these products", new SelectTag(context, "orderingMethod", module
						.getOrderingMethod(), OrderingModule.OrderingMethod.values())));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(OrderingModuleHandler.class, "save", "post"));
				sb.append(new HiddenTag("itemType", itemType));

                commands();
				stock();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return docs;
	}

	public Object save() throws ServletException {

		if (itemType == null) {
			return index();
		}

		OrderingModule module = itemType.getOrderingModule();
		module.setOrderingMethod(orderingMethod);
		module.save();

		clearParameters();
		return main();
	}

}
