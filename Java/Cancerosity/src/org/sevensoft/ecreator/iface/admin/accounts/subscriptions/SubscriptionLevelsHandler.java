package org.sevensoft.ecreator.iface.admin.accounts.subscriptions;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionFeature;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionRate;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 28 May 2006 18:47:29
 *
 */
@Path("admin-subscriptions-level.do")
public class SubscriptionLevelsHandler extends AdminHandler {

	private SubscriptionLevel					subscriptionLevel;
	private ItemType							itemType;
	private String							name;
	private String							icon;
	private String							comments;
	private boolean							live;
	private String							subscriptionReminderBody;
	private String							subscriptionReminderDays;
	private LinkedHashMap<SubscriptionFeature, String>	featureValues;
	private boolean							autoSubscribe;
	private int								limitedItemViews;
	private String							limitedItemViewsForward;
	private boolean							hidden;
	private String							linkback;
	private int								dailyProfileViewsLimit;
	private int								dailyPmLimit;
	private boolean							prioritise;

	public SubscriptionLevelsHandler(RequestContext context) {
		super(context);
	}

	public SubscriptionLevelsHandler(RequestContext context, SubscriptionLevel subscriptionLevel) {
		super(context);
		this.subscriptionLevel = subscriptionLevel;
	}

	public Object create() throws ServletException {

		if (itemType == null) {
			return main();
		}

		subscriptionLevel = itemType.getSubscriptionModule().addSubscriptionLevel();

		if (linkback != null) {
			return new ActionDoc(context, "A subscription level has been created.", linkback);
		}

		return new SubscriptionModuleHandler(context, itemType).main();
	}

	public Object delete() throws ServletException {

		if (subscriptionLevel == null) {
			return main();
		}

		itemType = subscriptionLevel.getItemType();
		if (itemType.getSubscriptionModule().removeSubscriptionLevel(subscriptionLevel)) {

			addMessage("Subscription deleted");

		} else {

			addMessage("Subscription marked as hidden - you cannot delete while it is still used by some accounts");
		}

		return main();
	}

	public Object edit() throws ServletException {

		if (subscriptionLevel == null) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit subscription level", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update subscription level"));

				sb.append(new ButtonTag(SubscriptionLevelsHandler.class, "delete", "Delete subscription", "subscriptionLevel", subscriptionLevel)
						.setConfirmation("Are you sure you want to delete this subscription level?"));

				if (isSuperman() || Module.UserItemTypes.enabled(context)) {
					sb.append(new ButtonTag(SubscriptionModuleHandler.class, null, "Return to module", "itemType", subscriptionLevel.getItemType()));
				}

				sb.append("</div>");
			}

			private void features() {

				List<SubscriptionFeature> features = SubscriptionFeature.get(context);
				Map<SubscriptionFeature, String> featureValues = subscriptionLevel.getFeatureValues();

				if (features.isEmpty()) {
					return;
				}

				sb.append(new AdminTable("Features"));

				for (SubscriptionFeature feature : features) {

					sb.append("<tr>");
					sb.append("<td valign='top'>" + feature.getName() + "</td>");
					sb.append("<td>" + new TextTag(context, "featureValues_" + feature.getId(), featureValues.get(feature), 60) + "</td>");
					sb.append("</tr>");

				}

				sb.append("</table>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Name", "Enter the name of this subscription, eg 'Gold Subscription'", new TextTag(context, "name",
						subscriptionLevel.getName(), 40)));

				sb.append(new AdminRow("Hidden", "Set this to no if you want this subscription to be admin accessible only.", new BooleanRadioTag(
						context, "hidden", subscriptionLevel.isHidden())));

				sb.append(new AdminRow("Auto subscribe", "Automatically subscribe new accounts to this subscription level using the first rate.",
						new BooleanRadioTag(context, "autoSubscribe", subscriptionLevel.isAutoSubscribe())));

				sb.append(new AdminRow("Comments", "Enter some comments to describe this subscription if you wish.", new TextAreaTag(context,
						"comments", subscriptionLevel.getComments(), 60, 4)));

				sb.append(new AdminRow("Prioritise", "Set any accounts subscribing to this level as prioritised.", new BooleanRadioTag(context,
						"prioritise", subscriptionLevel.isPrioritise())));

				sb.append("</table>");
			}

			private void rates() {

				sb.append(new AdminTable("Subscription rates"));

				sb.append("<tr>");
				sb.append("<th>Description</th>");
				sb.append("<th width='80'>Delete</th>");
				sb.append("</tr>");

				for (SubscriptionRate rate : subscriptionLevel.getSubscriptionRates()) {

					sb.append("<tr>");
					sb.append("<td>" + new LinkTag(SubscriptionRateHandler.class, null, new SpannerGif(), "subscriptionRate", rate) + " " +
							rate.getDescription() + "</td>");
					sb.append("<td width='80'>" +
							new ButtonTag(SubscriptionRateHandler.class, "delete", "Delete", "subscriptionRate", rate)
									.setConfirmation("Are you sure you want to delete this subscription rate?") + "</td>");
					sb.append("</tr>");
				}

				sb.append("<tr><td colspan='3'>" +
						new ButtonTag(SubscriptionRateHandler.class, "create", "Add subscription rate", "subscriptionLevel", subscriptionLevel) +
						"</td></tr>");

				sb.append("</table>");

			}

			private void reminders() {

				sb.append(new AdminTable("Reminders"));

				sb.append(new AdminRow("Subscription reminder days", "The number of days before a subscription expires when a reminder email is sent. "
						+ "For example, enter 5, 2 to send out a reminder email 5 and 2 days before the subscription expires.", new TextTag(
						context, "subscriptionReminderDays", StringHelper.implode(subscriptionLevel.getSubscriptionReminderDays(), ", ", true))));

				sb.append(new AdminRow("Subscription reminder email",
						"This is the content of the email that is sent out to remind users their subscription is expiring."
								+ "<br/>Use these tags<br/><i>[subscription_url], [expiry_date], [expiry_days], [name]</i>", new TextAreaTag(
								context, "subscriptionReminderBody", subscriptionLevel.getSubscriptionReminderBody(), 60, 6)));

				sb.append("</table>");
			}

			private void restrictions() {

				sb.append(new AdminTable("Restrictions"));

				sb.append(new AdminRow("Daily pm limit", new TextTag(context, "dailyPmLimit", subscriptionLevel.getDailyPmLimit(), 6)));

				sb.append(new AdminRow("Daily profile views limit", new TextTag(context, "dailyProfileViewsLimit", subscriptionLevel
						.getDailyProfileViewsLimit(), 6)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(SubscriptionLevelsHandler.class, "save", "POST"));
				sb.append(new HiddenTag("subscriptionLevel", subscriptionLevel));

                commands();
				general();
				reminders();
				rates();
				restrictions();
				features();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() {

		AdminDoc doc = new AdminDoc(context, user, "Subscription levels", null);
		doc.addBody(new Body() {

			private void levels() {

				sb.append(new AdminTable("Subscription levels"));

				sb.append("<tr>");
				sb.append("<th>Account type</th>");
				sb.append("<th>Name</th>");
				sb.append("<th width='10'> </th>");
				sb.append("</tr>");

				for (SubscriptionLevel subscriptionLevel : SubscriptionLevel.get(context)) {

					sb.append("<tr>");
					sb.append("<td>" + subscriptionLevel.getItemType().getName() + "</td>");
					sb.append("<td>" +
							new LinkTag(SubscriptionLevelsHandler.class, "edit", new ImageTag("files/graphics/admin/spanner.gif"),
									"subscriptionLevel", subscriptionLevel) + " " + subscriptionLevel.getName() + "</td>");
					sb.append("<td width='10'>" +
							new LinkTag(SubscriptionLevelsHandler.class, "delete", new DeleteGif(), "subscriptionLevel", subscriptionLevel)
									.setConfirmation("Are you sure you want to delete this subscription level?") + "</td>");
					sb.append("</tr>");
				}

				List<ItemType> itemTypes = ItemType.getSubscriptions(context);
				if (itemTypes.size() > 0) {

					sb.append(new FormTag(SubscriptionLevelsHandler.class, "create", "post"));
					sb.append(new HiddenTag("linkback", new Link(SubscriptionLevelsHandler.class)));
					sb.append("<tr><td colspan='3'>Add new subscription level: " +
							new SelectTag(context, "itemType", null, itemTypes, "-Select account type-") + " " + new SubmitTag("Add") +
							"</td></tr>");
					sb.append("</form>");

				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				levels();

				return sb.toString();
			}

		});
		return doc;

	}

	public Object moveDown() throws ServletException {

		if (subscriptionLevel == null) {
			return main();
		}

		EntityUtil.moveDown(subscriptionLevel, subscriptionLevel.getItemType().getSubscriptionModule().getSubscriptionLevels());
		return main();
	}

	public Object moveUp() throws ServletException {

		if (subscriptionLevel == null) {
			return main();
		}

		EntityUtil.moveUp(subscriptionLevel, subscriptionLevel.getItemType().getSubscriptionModule().getSubscriptionLevels());
		return main();
	}

	public Object save() throws ServletException {

		if (subscriptionLevel == null) {
			return main();
		}

		// subscriptionLevel.setLive(live);
		subscriptionLevel.setHidden(hidden);
		subscriptionLevel.setName(name);
		subscriptionLevel.setComments(comments);
		subscriptionLevel.setAutoSubscribe(autoSubscribe);
		subscriptionLevel.setPrioritise(prioritise);

		// reminders
		subscriptionLevel.setSubscriptionReminderBody(subscriptionReminderBody);
		subscriptionLevel.setSubscriptionReminderDays(StringHelper.explodeIntegersRanged(subscriptionReminderDays, "\\D"));

		// restrictions
		subscriptionLevel.setDailyPmLimit(dailyPmLimit);
		subscriptionLevel.setDailyProfileViewsLimit(dailyProfileViewsLimit);

		if (isSuperman()) {
			subscriptionLevel.setIcon(icon);
		}

		subscriptionLevel.setFeatureValues(featureValues);
		subscriptionLevel.save();

		addMessage("This subscription has been updated with your changes.");
		clearParameters();
		return edit();
	}
}
