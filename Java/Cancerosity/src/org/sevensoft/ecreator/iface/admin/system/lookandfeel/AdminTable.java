package org.sevensoft.ecreator.iface.admin.system.lookandfeel;

import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 23 Oct 2006 07:29:19
 *
 */
public class AdminTable {

	private final String	caption;
	private String		style;

	public AdminTable(String caption) {
		this.caption = caption;
	}

	@Override
	public String toString() {
		TableTag tag = new TableTag("form", 1, 0);
		tag.setCaption(caption);
		if (style != null) {
			tag.setStyle(style);
		}
		return tag.toString();
	}

	public AdminTable setStyle(String style) {
		this.style = style;
		return this;
	}

}
