package org.sevensoft.ecreator.iface.admin.items.modules.options;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.renderers.OwnerAttributesPanel;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupTag;
import org.sevensoft.ecreator.model.items.options.OptionGroup;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 22 Nov 2006 22:11:25
 *
 */
@Path("admin-options-groups.do")
public class OptionGroupsHandler extends AdminHandler {

	private OptionGroup	optionGroup;
	private String		newAttributes;
	private Markup		markup;
	private String	newAttributesSection;
	private int	newAttributesPage;

	public OptionGroupsHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		new OptionGroup(context, "New options group");
		return main();
	}

	public Object edit() throws ServletException {

		if (optionGroup == null)
			return main();

		AdminDoc doc = new AdminDoc(context, user, "Edit option group", null);
		doc.addBody(new Body() {

            private void commands() {
                sb.append("<div class='actions'>" + new SubmitTag("Update") + "</div>");
            }

			@Override
			public String toString() {

				sb.append(new FormTag(OptionGroupsHandler.class, "save", "post"));
				sb.append(new HiddenTag("optionGroup", optionGroup));

                commands();

				sb.append(new AdminTable("Option group settings"));
				if (isSuperman() || Module.UserMarkup.enabled(context)) {
					sb.append(new AdminRow("Markup", "Choose a custom markup or leave blank to use the default", new MarkupTag(context, "markup",
							optionGroup.getMarkup(), "-Standard renderer", "markup")));
				}
				sb.append("</table>");

				OwnerAttributesPanel ar = new OwnerAttributesPanel(context, optionGroup);
				ar.setCaption("Options");
				sb.append(ar);

				commands();

				sb.append("</form>");

				return sb.toString();
			}
		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Option groups", null);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Option groups"));

				for (OptionGroup group : OptionGroup.get(context)) {

					sb.append("<tr>");
					sb.append("<td>"
							+ new LinkTag(OptionGroupsHandler.class, "edit", new ImageTag("files/graphics/admin/spanner.gif"), "optionGroup",
									group) + " " + group.getName() + "</td>");
					sb.append("<td>" + new ButtonTag(OptionGroupsHandler.class, "delete", "Delete", "optionGroup", group) + "</td>");
					sb.append("</tr>");

				}

				sb.append("<tr><td colspan='2'>Add new options group " + new ButtonTag(OptionGroupsHandler.class, "create", "Add") + "</td></tr>");

				sb.append("</table>");
				return sb.toString();
			}
		});
		return doc;
	}

	public Object save() throws ServletException {

		if (optionGroup == null)
			return main();

		optionGroup.setMarkup(markup);
		optionGroup.save();

		// attributes
		if (newAttributes != null) {
			optionGroup.addAttributes(newAttributes, newAttributesSection, newAttributesPage);
		}
		return edit();
	}

}
