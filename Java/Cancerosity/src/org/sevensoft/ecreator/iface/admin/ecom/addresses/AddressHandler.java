package org.sevensoft.ecreator.iface.admin.ecom.addresses;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.text.PostcodeValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.OrderHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.AddressLineException;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.ContactNameException;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.CountryException;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.PostCodeException;
import org.sevensoft.ecreator.model.ecom.addresses.exceptions.TownException;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 06-Feb-2006 16:08:04
 *
 */
@Path("admin-addresses.do")
public class AddressHandler extends AdminHandler {

	private Item	account;
	private Address	address;
	private Country	country;
	private String	name;
	private String	address1;
	private String	address2;
	private String	address3;
	private String	town;
	private String	county;
	private String	postcode;
	private String	telephone1;
	private String	telephone2, telephone3;
	Order			order;
	private String	instructions;

	public AddressHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		test(new RequiredValidator(), "name");
		test(new RequiredValidator(), "address1");
		test(new RequiredValidator(), "town");
		test(new RequiredValidator(), "country");
		if (Country.UK.equals(country)) {
			test(new RequiredValidator(), "postcode");
			test(new PostcodeValidator(), "postcode");
		}

		if (hasErrors())
			return main();

		try {

			address = account.addAddress(name, address1, address2, address3, town, county, postcode, country, telephone1, telephone2, telephone3,
					instructions);
			return main();

		} catch (PostCodeException e) {
			setError("postcode", e);

		} catch (ContactNameException e) {
			setError("name", e);

		} catch (TownException e) {
			setError("town", e);

		} catch (CountryException e) {
			setError("country", e);

		} catch (AddressLineException e) {
			setError("address1", e);
		}

		return main();
	}

	public Object delete() throws ServletException {

		if (address == null || account == null)
			return main();

		account.removeAddress(address);
		addMessage("Address deleted");
		return main();
	}

	@Override
	public Object main() throws ServletException {

		if (account == null) {
			return index();
		}

		AdminDoc page = new AdminDoc(context, user, "Addresses", null);
		page.addBody(new Body() {

			private void addresses() {

				List<Address> addresses = account.getAddresses();
				if (addresses.size() == 0)
					return;

				sb.append(new TableTag("form", 1, 0).setCaption("Existing addresses"));

				for (Address address : addresses) {

					sb.append("<tr><th>" + address.getLabel(", ", false) + "</th><td>" +
							new LinkTag(AddressHandler.class, "delete", "Delete", "address", address, "account", account, "order", order) +
							"</td></tr>");

				}

				sb.append("</table>");
			}

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Create address"));
				if (order != null) {
					sb.append(new ButtonTag(OrderHandler.class, null, "Return to order", "order", order));
				}
				sb.append(new ButtonTag(ItemHandler.class, null, "Return to customer", "account", account));
				sb.append("</div>");
			}

			private void create() {

				sb.append(new AdminAddressEntryPanel(context));
			}

			@Override
			public String toString() {

				sb.append(new FormTag(AddressHandler.class, "create", "POST"));
				sb.append(new HiddenTag("account", account));
				sb.append(new HiddenTag("order", order));

                commands();
				addresses();
				create();
				commands();
				sb.append("</form>");

				return sb.toString();
			}

		});

		return page;
	}

	@Override
	public boolean runSecure() {
		return config.isSecured();
	}
}
