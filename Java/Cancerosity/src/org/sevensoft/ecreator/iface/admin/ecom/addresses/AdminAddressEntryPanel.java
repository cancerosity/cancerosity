package org.sevensoft.ecreator.iface.admin.ecom.addresses;

import org.sevensoft.commons.gaia.Country;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 17 Apr 2007 14:39:33
 */
public class AdminAddressEntryPanel {

    private RequestContext context;

    public AdminAddressEntryPanel(RequestContext context) {
        this.context = context;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        sb.append(new AdminTable("Create addresses"));

        sb.append(new AdminRow("Name", "Enter the contact name for this address", new TextTag(context, "name", 40) + ""
                + new ErrorTag(context, "name", "<br/>")));

        sb.append(new AdminRow("Address line 1", new TextTag(context, "address1", 40) + "" + new ErrorTag(context, "address1", "<br/>")));

        sb.append(new AdminRow("Address line 2", new TextTag(context, "address2", 40) + "" + new ErrorTag(context, "address2", "<br/>")));

        sb.append(new AdminRow("Address line 3", new TextTag(context, "address3", 40) + "" + new ErrorTag(context, "address3", "<br/>")));

        sb.append(new AdminRow("Town", "Enter the town or city for this address", new TextTag(context, "town", 40) + ""
                + new ErrorTag(context, "town", "<br/>")));

        sb.append("<tr><th width='300'>Postcode</th><td>" + new TextTag(context, "postcode", 12) + new ErrorTag(context, "postcode", "<br/>")
                + "</td></tr>");

        SelectTag countryTag = new SelectTag(context, "country");
        countryTag.addOptions(Country.getAll());

        sb.append(new AdminRow("County", new TextTag(context, "county", 40) + "" + new ErrorTag(context, "county", "<br/>")));

        sb.append("<tr><th width='300'>Country</th><td>" + countryTag + new ErrorTag(context, "country", "<br/>") + "</td></tr>");

        sb.append("<tr><th width='300'>Telephone</th><td>" + new TextTag(context, "telephone1", 20) + new ErrorTag(context, "telephone1", "<br/>")
                + "</td></tr>");

        sb.append("</table>");

        return sb.toString();
    }
}
