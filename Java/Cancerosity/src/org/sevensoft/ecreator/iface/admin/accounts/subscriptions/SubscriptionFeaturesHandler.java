package org.sevensoft.ecreator.iface.admin.accounts.subscriptions;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionFeature;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 15 Dec 2006 08:26:49
 *
 */
@Path("admin-subscriptions-features.do")
public class SubscriptionFeaturesHandler extends AdminHandler {

	private SubscriptionFeature	feature;
	private String			name;

	public SubscriptionFeaturesHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		if (name != null) {
			new SubscriptionFeature(context, name);
		}

		clearParameters();
		return main();
	}

	public Object delete() throws ServletException {

		if (feature == null) {
			return main();
		}

		feature.delete();

		clearParameters();
		return main();
	}

	public Object edit() throws ServletException {

		if (feature == null) {
			return main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Feature", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update feature"));
				sb.append(new ButtonTag(SubscriptionFeaturesHandler.class, "delete", "Delete feature", "feature", feature)
						.setConfirmation("Are you sure you want to delete this feature?"));
				sb.append(new ButtonTag(SubscriptionFeaturesHandler.class, null, "Return to features"));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));
				sb.append(new AdminRow("Name", "Enter a name for this feature", new TextTag(context, "name", 40)));
				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(SubscriptionFeaturesHandler.class, "save", "POST"));
				sb.append(new HiddenTag("feature", feature));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Features", null);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Features"));

				sb.append("<tr>");
				sb.append("<th width='60'>Position</th>");
				sb.append("<th>Name</th>");
				sb.append("<th width='80'>Delete</th>");
				sb.append("</tr>");

				PositionRender pr = new PositionRender(SubscriptionFeaturesHandler.class, "feature");
				for (SubscriptionFeature feature : SubscriptionFeature.get(context)) {

					sb.append("<tr>");
					sb.append("<td width='60'>" + pr.render(feature) + "</td>");
					sb.append("<td>" + new LinkTag(SubscriptionFeaturesHandler.class, "edit", new SpannerGif(), "feature", feature) + " "
							+ feature.getName() + "</td>");
					sb.append("<td width='80'>"
							+ new ButtonTag(SubscriptionFeaturesHandler.class, "delete", "Delete", "feature", feature)
									.setConfirmation("Are you sure you want to delete this subscription feature?") + "</td>");
					sb.append("</tr>");
				}

				sb.append(new FormTag(SubscriptionFeaturesHandler.class, "create", "post"));
				sb.append("<tr><td colspan='4'>Add new feature " + new TextTag(context, "name", 20) + " " + new SubmitTag("Add") + "</td></tr>");
				sb.append("</form>");

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;

	}

	public Object moveDown() throws ServletException {

		if (feature == null) {
			return main();
		}

		EntityUtil.moveDown(feature, SubscriptionFeature.get(context));
		return main();
	}

	public Object moveUp() throws ServletException {

		if (feature == null) {
			return main();
		}

		EntityUtil.moveUp(feature, SubscriptionFeature.get(context));
		return main();
	}

	public Object save() throws ServletException {

		if (feature == null) {
			return main();
		}

		feature.setName(name);
		feature.save();

		return main();
	}

}
