package org.sevensoft.ecreator.iface.admin.feeds.bespoke.products;

import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.ecom.suppliers.Supplier;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.products.vip.VipFeed;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 3 Aug 2006 11:15:04
 *
 */
@Path("admin-feeds-vip.do")
public class VipFeedHandler extends FeedHandler {

	private VipFeed	feed;
	private Attribute	mpnAttribute;
	private Attribute	manufAttribute;
	private Attribute	warrantyAttribute;
	private ItemType	itemType;

	private String	username;
	private String	password;
	private Supplier	supplier;

	public VipFeedHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected ImportFeed getFeed() {
		return feed;
	}

	@Override
	protected void saveSpecific() {

		feed.setManufAttribute(manufAttribute);
		feed.setMpnAttribute(mpnAttribute);
		feed.setWarrantyAttribute(warrantyAttribute);
		feed.setUsername(username);
		feed.setPassword(password);
		feed.setItemType(itemType);

		//		if (Module.Suppliers.enabled(context))
		//			feed.setSupplier(supplier);
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("VIP specifics"));

		sb.append("<tr><th width='300'><b>Item type</b><br/>Select the item type to import to.</th><td>"
				+ new SelectTag(context, "itemType", feed.getItemType(), ItemType.getSelectionMap(context), "-None set-") + "</td></tr>");

		//		if (Module.Suppliers.enabled(context)) {
		//
		//			SelectTag supplierTag = new SelectTag(context, "supplier", feed.getSupplier(true));
		//			supplierTag.setAny("-None selected-");
		//			supplierTag.addOptions(Supplier.get(context));
		//
		//			sb.append(new AdminRow("Supplier</b><br/>Select the supplier that provides this feed.</th><td>" + supplierTag + "</td></tr>");
		//		}

		sb.append("<tr><th width='300'><b>Username</b></th><td>" + new TextTag(context, "username", feed.getUsername(), 16) + "</td></tr>");
		sb.append("<tr><th width='300'><b>Password</b></th><td>" + new TextTag(context, "password", feed.getPassword(), 16) + "</td></tr>");

		if (feed.hasItemType()) {

			SelectTag attributeTag = new SelectTag(context, "mpnAttribute", feed.getMpnAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append("<tr><th width='300'><b>Mpn attribute</b></th><td>" + attributeTag + "</td></tr>");

			attributeTag = new SelectTag(context, "manufAttribute", feed.getManufAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append("<tr><th width='300'><b>Manufacturer attribute</b></th><td>" + attributeTag + "</td></tr>");

			attributeTag = new SelectTag(context, "warrantyAttribute", feed.getWarrantyAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append("<tr><th width='300'><b>Warranty attribute</b></th><td>" + attributeTag + "</td></tr>");

		}

		sb.append("</table>");
	}

}
