package org.sevensoft.ecreator.iface.admin.feeds.csv;

import org.sevensoft.ecreator.model.feeds.csv.CsvExportFeed;
import org.sevensoft.ecreator.model.feeds.csv.CsvImportFeedField;
import org.sevensoft.ecreator.model.feeds.csv.FieldType;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;

import javax.servlet.ServletException;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
@Path("admin-feeds-csv-export-fields.do")
public class CsvExportFeedFieldHandler extends AdminHandler {

    private CsvImportFeedField field;
    private String newField;
    private CsvExportFeed exFeed;
    private String name;
    private int startRow;
    private FieldType fieldType;
    private Attribute attribute;
    private int position;

    public CsvExportFeedFieldHandler(RequestContext context) {
        super(context);
    }

    public Object main() throws ServletException {
        AdminDoc doc = new AdminDoc(context, user, "Edit export feed field", null);
        doc.addBody(new Body() {

            private void commands() {
                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Confirm changes"));
                sb.append(new ButtonTag(CsvExportFeedFieldHandler.class, "delete", "Delete field", "field", field));
                sb.append(new ButtonTag(CsvExportFeedHandler.class, null, "Return to feed", "feed", field.getExFeed()));
                sb.append("</div>");
            }

            private void general() {
                sb.append(new AdminTable("General"));
                sb.append(new AdminRow("Position", "Enter the column number for this field.", new TextTag(context, "position", field.getPosition(), 4)));
                sb.append(new AdminRow("Field type", "Select the field type", new SelectTag(context, "fieldType", field.getFieldType(), FieldType.values(), "None set")));
                if (field.getFieldType() == FieldType.Attribute)
                    sb.append(new AdminRow("Attribute", "The attribute to use.", new SelectTag(context, "attribute", field.getAttribute(), field.getExFeed().getItemType().getAttributes(), "-None selected-")));
                sb.append("</table>");
            }

            public String toString() {
                sb.append(new FormTag(CsvExportFeedFieldHandler.class, "save", "POST"));
                sb.append(new HiddenTag("field", field));
                commands();
                general();
                commands();
                sb.append("</form>");
                return sb.toString();
            }

        }
        );
        return doc;
    }

    public Object delete() {
        field.delete();
        return new ActionDoc(context, "This field has been deleted", new Link(CsvExportFeedHandler.class, null, "feed", field.getExFeed()));
    }

    public Object create() {
        CsvImportFeedField field = exFeed.addField();
        return new ActionDoc(context, "A new field has been added", new Link(CsvExportFeedFieldHandler.class, null, "field", field));
    }

    public Object save() throws ServletException {
        field.setAttribute(attribute);
        field.setPosition(position);
        field.setFieldType(fieldType);
        field.save();
        return new ActionDoc(context, "This field has been updated", new Link(CsvExportFeedFieldHandler.class, null, "field", field));
    }


}
