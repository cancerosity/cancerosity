package org.sevensoft.ecreator.iface.admin.feeds;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.feeds.categories.CategoryReference;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 25 Sep 2006 13:56:20
 *
 */
@Path("admin-feeds-catmapping.do")
public class CategoryMappingHandler extends AdminHandler {

	private CategoryReference	categoryMapping;
	private Category		category;

	public CategoryMappingHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (categoryMapping == null)
			return index();

		AdminDoc p = new AdminDoc(context, user, "Edit category mapping", null);
		p.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new TableTag("form",1,0).setCaption("General"));

				sb.append("<tr><th width='300'><b>Name</b></th><td>" + categoryMapping.getName() + "</td></tr>");
				sb.append("<tr><th width='300'><b>Map to</b></th><td>"
						+ new SelectTag(context, "category", categoryMapping.getCategory(), Category.getCategoryOptions(context), "None set") + "</td></tr>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(CategoryMappingHandler.class, "save", "post"));
				sb.append(new HiddenTag("categoryMapping", categoryMapping));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return p;
	}

	public Object save() throws ServletException {

		if (categoryMapping == null)
			return index();

		categoryMapping.setCategory(category);
		categoryMapping.save();

		clearParameters();
		return main();

	}
}
