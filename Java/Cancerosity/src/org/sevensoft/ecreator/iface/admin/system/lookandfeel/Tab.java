package org.sevensoft.ecreator.iface.admin.system.lookandfeel;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.ecreator.iface.admin.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxMenuHandler;
import org.sevensoft.ecreator.iface.admin.crm.forms.FormHandler;
import org.sevensoft.ecreator.iface.admin.ecom.ShoppingHandler;
import org.sevensoft.ecreator.iface.admin.ecom.bookings.BookingSearchHandler;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasHandler;
import org.sevensoft.ecreator.iface.admin.extras.newsfeed.NewsfeedsHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.marketing.MarketingHandler;
import org.sevensoft.ecreator.iface.admin.media.galleries.GalleryHandler;
import org.sevensoft.ecreator.iface.admin.media.images.ImageLibraryHandler;
import org.sevensoft.ecreator.iface.admin.misc.seo.SeoHandler;
import org.sevensoft.ecreator.iface.admin.stats.VisitorsHandler;
import org.sevensoft.ecreator.iface.admin.system.company.CompanyHandler;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.Config.SoftwareVersion;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 24 Jul 2006 12:49:13
 *
 * A menu tab
 */
public class Tab {

	public static final Tab	Sms			= new Tab(new Link(MarketingHandler.class), "Sms");
	public static final Tab	Newsletter		= new Tab(new Link(MarketingHandler.class), "Newsletter");
	public static final Tab	CompanyDetails	= new Tab(new Link(CompanyHandler.class), "Company Details");
	public static final Tab	Categories		= new Tab(new Link(CategoryHandler.class), "Categories");
	public static final Tab	Orders		= new Tab(new Link(ShoppingHandler.class), "Orders");
	public static final Tab	Extras		= new Tab(new Link(ExtrasHandler.class), "Extras");
	public static final Tab	Stats			= new Tab(new Link(VisitorsHandler.class), "Stats");
	public static final Tab	Marketing		= new Tab(new Link(MarketingHandler.class), "Marketing");
	public static final Tab	Bookings		= new Tab(new Link(BookingSearchHandler.class), "Bookings");
	public static final Tab	Images		= new Tab(new Link(ImageLibraryHandler.class), "Images");
	public static Tab		Galleries		= new Tab(new Link(GalleryHandler.class, "menu"), "Galleries");
	public static final Tab	Forms			= new Tab(new Link(FormHandler.class, "menu"), "Forms");
	public static final Tab	Newsfeeds		= new Tab(new Link(NewsfeedsHandler.class), "Newsfeeds");
	public static Tab		Sideboxes		= new Tab(new Link(BoxMenuHandler.class), "Sideboxes");
	public static Tab		Seo			= new Tab(new Link(SeoHandler.class), "Seo");

	public static List<Tab> getBrochureTabs(RequestContext context) {

		List<Tab> tabs = new ArrayList();

		tabs.add(CompanyDetails);

		// get news item type
		List<ItemType> itemTypes = ItemType.get(context);
		for (ItemType itemType : itemTypes) {

			if (itemType.getName().startsWith("News")) {

				tabs.add(getItemTab(itemType));
				itemTypes.remove(itemType);
				break;
			}
		}

		tabs.add(Categories);

		// limit to one item type other than news
		if (itemTypes.size() > 0) {
			tabs.add(getItemTab(itemTypes.get(0)));
		}

		if (Module.Newsletter.enabled(context) && Module.Sms.enabled(context)) {
			tabs.add(Marketing);
		}

		else if (Module.Newsletter.enabled(context)) {
			tabs.add(Tab.Newsletter);
		}

		else if (Module.Sms.enabled(context)) {
			tabs.add(Tab.Sms);
		}

		if (Module.Forms.enabled(context)) {
			tabs.add(Forms);
		}

		if (Module.Galleries.enabled(context)) {
			tabs.add(Galleries);
		}

		if (Module.Newsfeed.enabled(context)) {
			tabs.add(Newsfeeds);
		}

		tabs.add(Images);
		tabs.add(Sideboxes);
		tabs.add(Seo);
		tabs.add(Stats);

		return tabs;
	}

	public static List<Tab> getFullTabs(RequestContext context) {

		Config config = Config.getInstance(context);

		List<Tab> tabs = new ArrayList<Tab>();

		if (!MiscSettings.getInstance(context).isHideCompanyDetailsTab()) {
			tabs.add(CompanyDetails);
		}

		// get news item type
		List<ItemType> itemTypes = ItemType.get(context);
		for (ItemType itemType : itemTypes) {

			if (itemType.getName().startsWith("News")) {

				tabs.add(getItemTab(itemType));
				itemTypes.remove(itemType);
				break;
			}
		}

		tabs.add(Categories);

		if (config.getSoftwareVersion() == SoftwareVersion.Full) {

			for (ItemType itemType : itemTypes) {
				tabs.add(getItemTab(itemType));
			}

		}

		if (Module.Newsletter.enabled(context) && Module.Sms.enabled(context)) {
			tabs.add(Marketing);
		}

		else if (Module.Newsletter.enabled(context)) {
			tabs.add(Tab.Newsletter);
		}

		else if (Module.Sms.enabled(context)) {
			tabs.add(Tab.Sms);
		}

		if (config.getSoftwareVersion() == SoftwareVersion.Full) {

			if (Module.Bookings.enabled(context)) {
				tabs.add(Bookings);
			}

			if (isEcommerce(context)) {
				tabs.add(Orders);
			}

		}

		if (config.getSoftwareVersion() == SoftwareVersion.Brochure) {

			tabs.add(Forms);
			tabs.add(Galleries);

		} else {

			if (isModules(context)) {
				tabs.add(Extras);
			}

		}

		if (config.getSoftwareVersion() == SoftwareVersion.Brochure) {

			tabs.add(Images);
			tabs.add(Sideboxes);

		}

        if (!Module.StatsBlock.enabled(context)) {
            tabs.add(Stats);
        }

		return tabs;
	}

	public static Tab getItemTab(Item item) {
		return getItemTab(item.getItemType());
	}

	public static Tab getItemTab(ItemType itemType) {
		return new Tab(new Link(ItemHandler.class, null, "itemType", itemType), itemType.getNamePlural());
	}

	private static boolean isEcommerce(RequestContext context) {

		if (Module.Shopping.enabled(context)) {
			return true;
		}

		if (Module.Subscriptions.enabled(context)) {
			return true;
		}

		return false;
	}

	private static boolean isModules(RequestContext context) {

		List<Module> extrasModules = new ArrayList();
		extrasModules.add(Module.Forum);
		extrasModules.add(Module.RssExport);
		extrasModules.add(Module.Forms);
		extrasModules.add(Module.Galleries);
		extrasModules.add(Module.Jobsheets);
		extrasModules.add(Module.Calculators);
		extrasModules.add(Module.Polls);
		extrasModules.add(Module.Newsfeed);

		for (Module module : extrasModules) {
			if (module.enabled(context)) {
				return true;
			}
		}

		return false;
	}
	private String		text;

	private final Link	link;

	public Tab(Link link, String text) {
		this.link = link;
		this.text = text;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj instanceof Tab) {
			return ((Tab) obj).text.equals(text);
		}

		return false;
	}

	public LinkTag getLinkTag() {
		return new LinkTag(link, text);
	}

	private void setText(String text) {
		this.text = text;
	}

}