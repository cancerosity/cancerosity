package org.sevensoft.ecreator.iface.admin.system.config;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.ecom.removal.OrderRemovalHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.AttributeOption;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.deleters.CategoryExterminator;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.ecom.orders.parcels.Parcel;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLine;
import org.sevensoft.ecreator.model.ecom.shopping.BasketLineOption;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.advanced.ItemExterminator;
import org.sevensoft.ecreator.model.marketing.newsletter.Subscriber;
import org.sevensoft.ecreator.model.marketing.newsletter.Subscription;
import org.sevensoft.ecreator.model.marketing.sms.SmsRegistration;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.media.images.galleries.Gallery;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.stats.hits.pages.PageHitCounterMonthly;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHit;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHitCounterDaily;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHitCounterMonthly;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.commons.samdate.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.List;

/**
 * @author sks 19 Sep 2006 14:14:23
 */
@Path("admin-deleter.do")
public class DeleterHandler extends AdminHandler implements Logging {

    private ItemType itemType;

    private String status;

    private Date dateStart;

    private Date dateEnd;

    public DeleterHandler(RequestContext context) {
        super(context);
    }

    public Object deleteCategories() throws ServletException {

        new CategoryExterminator(context).delete();
        log("Categories deleted");

        addMessage("All categories deleted. If you made a mistake you're shit outta luck.");
        return main();
    }

    public Object deleteImages() throws ServletException {

        log("Images deleted");

        new Query(context, "delete from #").setTable(Img.class).run();

        new Query(context, "update # set imageCount=0").setTable(Item.class).run();
        new Query(context, "update # set imageCount=0").setTable(Category.class).run();
        new Query(context, "update # set imageCount=0").setTable(Gallery.class).run();
        new Query(context, "update # set imageCount=0").setTable(AttributeOption.class).run();

        for (File file : ResourcesUtils.getRealImagesDir().listFiles()) {
            file.delete();
        }

        return main();
    }

    public Object deleteItems() throws ServletException {

        if (itemType == null) {
            return main();
        }

        log("Items of type '" + itemType.getName() + "' deleted");

        new ItemExterminator(context, itemType).delete();
        addMessage("All items deleted. If you made a mistake you're shit outta luck.");
        return main();
    }

    public Object deleteOrders() throws ServletException {

        deleteOrdersPriv();
        return main();
    }

    private void deleteOrdersPriv() {
        log("Orders deleted");

        SimpleQuery.delete(context, Order.class);
        SimpleQuery.delete(context, OrderLine.class);
        SimpleQuery.delete(context, Basket.class);
        SimpleQuery.delete(context, BasketLine.class);
        SimpleQuery.delete(context, BasketLineOption.class);
        SimpleQuery.delete(context, Parcel.class);

        addMessage("All orders deleted.");
    }

    public Object deleteOrdersByStatus() throws ServletException {
        if (status == null || status.trim().length() == 0) {
            addError("Choose any status! To delete all the orders enter the appropriate date range or go to Deleter page");
//            deleteOrdersPriv();
            return new OrderRemovalHandler(context).main();
        }

        Query parcel = new Query(context, "delete op from # op  join # o on op.ord=o.id where o.status like ?");
        parcel.setTable(Parcel.class);
        parcel.setTable(Order.class);
        parcel.setParameter(status);
        int parcelCount = parcel.run();

        Query orderLine = new Query(context, "delete ol from # ol  join # o on ol.ord=o.id where o.status like ?");
        orderLine.setTable(OrderLine.class);
        orderLine.setTable(Order.class);
        orderLine.setParameter(status);
        int orderLineCount = orderLine.run();

        Query basketLineOptions = new Query(context, "delete blo from # blo join (select bl.* from # bl join (select b.* from # b join # o on b.order=o.id where o.status like ?)B on bl.basket = B.id)C on blo.line=C.id");
        basketLineOptions.setTable(BasketLineOption.class);
        basketLineOptions.setTable(BasketLine.class);
        basketLineOptions.setTable(Basket.class);
        basketLineOptions.setTable(Order.class);
        basketLineOptions.setParameter(status);
        int basketLineOptionsCount = basketLineOptions.run();

        Query basketLine = new Query(context, "delete bl from # bl join (select b.* from # b join # o on b.order=o.id where o.status like ? )B on bl.basket = B.id");
        basketLine.setTable(BasketLine.class);
        basketLine.setTable(Basket.class);
        basketLine.setTable(Order.class);
        basketLine.setParameter(status);
        int basketLineCount = basketLine.run();

        Query basket = new Query(context, "delete b from # b join # o on b.order=o.id where o.status like ?");
        basket.setTable(Basket.class);
        basket.setTable(Order.class);
        basket.setParameter(status);
        int basketCount = basket.run();

        Query order = new Query(context, "delete o from # o where o.status like ?");
        order.setTable(Order.class);
        order.setParameter(status);
        int orderCount = order.run();

        log(status + " orders deleted. \n" + orderCount + " orders deleted. " + orderLineCount + " orderLine deleted. " + parcelCount + " parcels deleted. " +
                basketCount + " baskets deleted. " + basketLineCount + " basketLines deleted. " + basketLineOptionsCount + " basketLineOptions deleted.");

        addMessage(orderCount + " " + status + " orders deleted. ");

        return new OrderRemovalHandler(context).main();
    }

    public Object deleteOrdersByDate() throws ServletException {
        /*if (dateStart == null && dateEnd == null) {
            deleteOrdersPriv();
            return new OrderRemovalHandler(context).main();
        }  else*/
        if (dateStart == null || dateEnd == null) {
            addError("Both date fields should be filled! To delete all the orders enter the appropriate date range or go to Deleter page");
            return new OrderRemovalHandler(context).main();
        }

        Query parcel = new Query(context, "delete op from # op  join # o on op.ord=o.id where o.datePlaced>=? and o.datePlaced<=?");
        parcel.setTable(Parcel.class);
        parcel.setTable(Order.class);
        parcel.setParameter(dateStart);
        parcel.setParameter(dateEnd);
        int parcelCount = parcel.run();

        Query orderLine = new Query(context, "delete ol from # ol  join # o on ol.ord=o.id where o.datePlaced>=? and o.datePlaced<=?");
        orderLine.setTable(OrderLine.class);
        orderLine.setTable(Order.class);
        orderLine.setParameter(dateStart);
        orderLine.setParameter(dateEnd);
        int orderLineCount = orderLine.run();

        Query basketLineOptions = new Query(context, "delete blo from # blo join (select bl.* from # bl join (select b.* from # b join # o on b.order=o.id where o.datePlaced>=? and o.datePlaced<=?)B on bl.basket = B.id)C on blo.line=C.id");
        basketLineOptions.setTable(BasketLineOption.class);
        basketLineOptions.setTable(BasketLine.class);
        basketLineOptions.setTable(Basket.class);
        basketLineOptions.setTable(Order.class);
        basketLineOptions.setParameter(dateStart);
        basketLineOptions.setParameter(dateEnd);
        int basketLineOptionsCount = basketLineOptions.run();

        Query basketLine = new Query(context, "delete bl from # bl join (select b.* from # b join # o on b.order=o.id where o.datePlaced>=? and o.datePlaced<=? )B on bl.basket = B.id");
        basketLine.setTable(BasketLine.class);
        basketLine.setTable(Basket.class);
        basketLine.setTable(Order.class);
        basketLine.setParameter(dateStart);
        basketLine.setParameter(dateEnd);
        int basketLineCount = basketLine.run();

        Query basket = new Query(context, "delete b from # b join # o on b.order=o.id where o.datePlaced>=? and o.datePlaced<=?");
        basket.setTable(Basket.class);
        basket.setTable(Order.class);
        basket.setParameter(dateStart);
        basket.setParameter(dateEnd);
        int basketCount = basket.run();

        Query order = new Query(context, "delete o from # o where o.datePlaced>=? and o.datePlaced<=?");
        order.setTable(Order.class);
        order.setParameter(dateStart);
        order.setParameter(dateEnd);
        int orderCount = order.run();

        log(status + " orders deleted. \n" + orderCount + " orders deleted. " + orderLineCount + " orderLine deleted. " + parcelCount + " parcels deleted. " +
                basketCount + " baskets deleted. " + basketLineCount + " basketLines deleted. " + basketLineOptionsCount + " basketLineOptions deleted.");

        addMessage(orderCount + " orders deleted. ");

        return new OrderRemovalHandler(context).main();
    }

    public Object deleteSmsRegistrations() throws ServletException {

        SimpleQuery.delete(context, SmsRegistration.class);
        log("Sms registrations deleted");

        addMessage("All SMS registrations deleted. If you made a mistake you're shit outta luck.");
        return main();
    }

    public Object deleteStats() throws ServletException {

        log("Stats deleted");

        SimpleQuery.delete(context, SiteHitCounterMonthly.class);
        SimpleQuery.delete(context, SiteHitCounterDaily.class);
        SimpleQuery.delete(context, SiteHit.class);
        SimpleQuery.delete(context, PageHitCounterMonthly.class);
        SimpleQuery.delete(context, SiteHit.class);

        addMessage("All stats deleted. If you made a mistake you're shit outta luck.");
        return main();
    }

    public Object deleteSubscribers() throws ServletException {

        SimpleQuery.delete(context, Subscriber.class);
        SimpleQuery.delete(context, Subscription.class);

        addMessage("All subscribers deleted. If you made a mistake you're shit outta luck.");
        return main();
    }

    public Object deleteOptions() throws ServletException {
        List<ItemOption> list = SimpleQuery.execute(context, ItemOption.class);
        for (ItemOption option : list) {
            option.delete();
        }
        log("Item Options deleted");

        addMessage("All Item Options deleted. If you made a mistake you're shit outta luck.");
        return main();
    }

    public String getFullId() {
        return "Deleter";
    }

    public List<LogEntry> getLogEntries() {
        return null;
    }

    public String getLogId() {
        return "Deleter";
    }

    public String getLogName() {
        return "Deleter";
    }

    @Override
    protected Object init() throws ServletException {

        Object obj = super.init();
        if (obj != null) {
            return obj;
        }

        if (context.isLocalhost()) {
            return null;
        }

        if (config.isLive()) {
            return HttpServletResponse.SC_FORBIDDEN;
        }

        return null;
    }

    public void log(String message) {
        new LogEntry(context, this, message);
    }

    public void log(User user, String message) {
        new LogEntry(context, this, user, message);
    }

    @Override
    public Object main() throws ServletException {

        AdminDoc doc = new AdminDoc(context, user, "Deleter", null);
        doc.addBody(new Body() {

            private void general() {

                sb.append(new AdminTable("Deleter"));

                sb.append(new AdminRow("Delete categories", "Delete all categories including the content from home page.", new ButtonTag(
                        DeleterHandler.class, "deleteCategories", "Delete the categories scotty!").setConfirmation("I canna undo it cap'n")));

                for (ItemType itemType : ItemType.get(context)) {

                    sb.append(new AdminRow("Delete " + itemType.getNamePluralLower(), "Delete all " + itemType.getNamePluralLower()
                            + " and related data.", new ButtonTag(DeleterHandler.class, "deleteItems", "Delete all "
                            + itemType.getNamePluralLower() + " scotty!", "itemType", itemType).setConfirmation("I canna undo it cap'n")));

                }

                sb.append(new AdminRow("Delete orders", "Delete all orders but keeping all members and items.", new ButtonTag(DeleterHandler.class,
                        "deleteOrders", "Delete the orders scotty!").setConfirmation("I canna undo it cap'n")));

                sb.append(new AdminRow("Delete stats", "Remove all stats and traffic reports from the site.", new ButtonTag(DeleterHandler.class,
                        "deleteStats", "Delete the stats scotty!").setConfirmation("I canna undo it cap'n")));

                sb.append(new AdminRow("Delete newsletter subscribers", "Delete subscribers to all newsletters.", new ButtonTag(DeleterHandler.class,
                        "deleteSubscribers", "Delete the subscribers scotty!").setConfirmation("I canna undo it cap'n")));

                sb.append(new AdminRow("Delete SMS registrations", "Delete registrations in all SMS bulletins.", new ButtonTag(DeleterHandler.class,
                        "deleteSmsRegistrations", "Delete the registrations scotty!").setConfirmation("I canna undo it cap'n")));

                sb.append(new AdminRow("Delete images", "Delete all files in the image / thumbnail folders.", new ButtonTag(DeleterHandler.class,
                        "deleteImages", "Delete the images scotty!").setConfirmation("I canna undo it cap'n")));

                sb.append(new AdminRow("Delete item options", "Delete all the options and option selections across the system.", new ButtonTag(DeleterHandler.class,
                        "deleteOptions", "Delete the options scotty!").setConfirmation("I canna undo it cap'n")));

                sb.append("</table>");
            }

            @Override
            public String toString() {

                general();

                return sb.toString();
            }
        });

        return doc;
    }

}
