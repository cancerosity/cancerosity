package org.sevensoft.ecreator.iface.admin.marketing;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.marketing.newsletter.NewsletterComposeHandler;
import org.sevensoft.ecreator.iface.admin.marketing.newsletter.NewsletterSubscriptionHandler;
import org.sevensoft.ecreator.iface.admin.marketing.sms.SmsComposeHandler;
import org.sevensoft.ecreator.iface.admin.marketing.sms.SmsRegistrationsHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.users.privileges.Privilege;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 18 Oct 2006 10:14:04
 *
 */
@Path("admin-marketing.do")
public class MarketingHandler extends AdminHandler {

	public MarketingHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Marketing", Tab.Marketing);
		if (miscSettings.isAdvancedMode())
			doc.setMenu(new MarketingMenu());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("simplelinks", "center"));
				sb.append("<tr><th align='center'>What would you like to do ?</th></tr>");

				if (Module.Sms.enabled(context)) {

					if (Privilege.EditSms.yes(user, context)) {

						sb.append("<tr><td align='center'>"
								+ new LinkTag(SmsRegistrationsHandler.class, null, "I want to add or remove mobile numbers") + "</td></tr>");
					}

					if (Privilege.ComposeSms.yes(user, context)) {

						sb.append("<tr><td align='center'>" + new LinkTag(SmsComposeHandler.class, null, "I want to send an SMS bulletin")
								+ "</td></tr>");
					}

					sb.append("<tr><td class='spacer'></td></tr>");
				}

				if (Module.Newsletter.enabled(context)) {

					sb.append("<tr><td align='center'>"
							+ new LinkTag(NewsletterSubscriptionHandler.class, null, "I want to add or remove email addresses") + "</td></tr>");

					if (Privilege.ComposeNewsletter.yes(user, context)) {

						sb.append("<tr><td align='center'>" + new LinkTag(NewsletterComposeHandler.class, null, "I want to send a newsletter")
								+ "</td></tr>");
					}

				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}
}
