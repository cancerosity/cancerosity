package org.sevensoft.ecreator.iface.admin.feeds.bespoke.property;

import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.Feed;
import org.sevensoft.ecreator.model.feeds.bespoke.property.rightmove.RightMoveFeed;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 28 Jan 2007 18:10:15
 *
 */
@Path("admin-feeds-rightmove.do")
public class RightMoveFeedHandler extends FeedHandler {

	private RightMoveFeed	feed;
	private Attribute		postcodeAttribute;
	private Attribute		priceAttribute;
	private Attribute		bedroomsAttribute;
	private ItemType		itemType;
	private String		directory;
	private Attribute		furnishedAttribute;
	private Attribute		addressAttribute;
	private Attribute		agentAttribute;
	private Item		agent;
	private Attribute		areaAttribute;
	private String		websitePrefix;
	private Attribute		websiteAttribute;
	private String		email;
	private String		telephone;

	public RightMoveFeedHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Feed getFeed() {
		return feed;
	}

	@Override
	protected void saveSpecific() {

		feed.setItemType(itemType);
		feed.setFurnishedAttribute(furnishedAttribute);
		feed.setAddressAttribute(addressAttribute);
		feed.setBedroomsAttribute(bedroomsAttribute);
		feed.setPriceAttribute(priceAttribute);
		feed.setPostcodeAttribute(postcodeAttribute);
		feed.setAgent(agent);
		feed.setAreaAttribute(areaAttribute);
		feed.setDirectory(directory);
		feed.setWebsiteAttribute(websiteAttribute);
		feed.setWebsitePrefix(websitePrefix);
		feed.setTelephone(telephone);
		feed.setEmail(email);
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Right move feed specifics"));

		sb.append(new AdminRow("Item type", "Select the item type to import to.", new SelectTag(context, "itemType", feed.getItemType(), ItemType
				.getSelectionMap(context), "-None set-")));

		sb.append(new AdminRow("Directory", "Set the sub directory of feed-data used for uploading for this feed.", new TextTag(context, "directory", feed
				.getDirectory(), 20)));

		if (feed.hasItemType()) {

			sb.append(new AdminRow("Price attribute", "This sets the asking price or rental fee of the property.", new SelectTag(context,
					"priceAttribute", feed.getPriceAttribute(), feed.getItemType().getAttributes(), "-None-")));

			sb.append(new AdminRow("Address attribute", new SelectTag(context, "addressAttribute", feed.getAddressAttribute(), feed.getItemType()
					.getAttributes(), "-None-")));

			sb.append(new AdminRow("Area attribute", "", new SelectTag(context, "areaAttribute", feed.getAreaAttribute(), feed.getItemType()
					.getAttributes(), "-None-")));

			sb.append(new AdminRow("Postcode attribute", "This sets the property postcode", new SelectTag(context, "postcodeAttribute", feed
					.getPostcodeAttribute(), feed.getItemType().getAttributes(), "-None-")));

			sb.append(new AdminRow("Furnished attribute", "This sets the furnished status for the property.", new SelectTag(context,
					"furnishedAttribute", feed.getFurnishedAttribute(), feed.getItemType().getAttributes(), "-None-")));

			sb.append(new AdminRow("Website attribute", "Website attribute.", new SelectTag(context, "websiteAttribute", feed.getWebsiteAttribute(), feed
					.getItemType().getAttributes(), "-None-")));

			sb.append(new AdminRow("Website prefix", "Prefix for making website urls.",
					new TextTag(context, "websitePrefix", feed.getWebsitePrefix(), 20)));

			sb.append(new AdminRow("Beds attribute", "This sets the number of bedrooms", new SelectTag(context, "bedroomsAttribute", feed
					.getBedroomsAttribute(), feed.getItemType().getAttributes(), "-None-")));

			sb.append(new AdminRow("Agent", new TextTag(context, "agent", feed.getAgent(), 20)));
			
			sb.append(new AdminRow("Telephone", new TextTag(context, "telephone", feed.getTelephone(), 20)));
			
			sb.append(new AdminRow("Email", new TextTag(context, "email", feed.getEmail(), 20)));

		}

		sb.append("</table>");
	}

}
