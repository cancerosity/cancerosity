package org.sevensoft.ecreator.iface.admin.crm.forum;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.crm.forum.Post;
import org.sevensoft.ecreator.model.crm.forum.Topic;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 03-Apr-2006 15:02:57
 *
 */
@Path("admin-forum-posts.do")
public class PostHandler extends AdminHandler {

	private Post	post;
	private String	content;

	public PostHandler(RequestContext context) {
		super(context);
	}

	public Object delete() throws ServletException {

		if (post == null)
			return main();

		Topic topic = post.getTopic();
		topic.removePost(post);
		return new PostSearchHandler(context).main();
	}
	
	public Object save() throws ServletException {

		if (post == null)
			return main();

		post.setContent(content);
		post.save();
		
		addMessage("The post has been saved with the changes");
		return main();
	}

	@Override
	public Object main() throws ServletException {

		if (post == null)
			return new PostSearchHandler(context).main();

		AdminDoc page = new AdminDoc(context, user, "Edit post", Tab.Orders);
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update post"));
				sb.append(new ButtonTag(PostHandler.class, "delete", "Delete post", "post", post)
						.setConfirmation("Are you sure you want to delete this delivery rate?"));
				sb.append(new ButtonTag(PostSearchHandler.class, null, "Return to posts search"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new TableTag("form",1,0).setCaption("Post details"));

				sb.append("<tr><th width='300'><b>Author</b></th><td>" + post.getAuthor() + "</td></tr>");

				sb.append("<tr><th width='300'><b>Content</b></th><td>" + new TextAreaTag(context, "content", post.getContent(), 60, 8) + "</td></tr>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(PostHandler.class, "save", "post"));
				sb.append(new HiddenTag("post", post));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return page;
	}

}
