package org.sevensoft.ecreator.iface.admin.ecom.branches;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.ecom.ShoppingMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.ecom.branches.Branch;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks
 */
@Path("admin-branches.do")
public class BranchesHandler extends AdminHandler {

	private String	location;
	private String	name;
	private Branch	branch;
	private String	orderEmails;
	private String	postcode;
	private String	address;
	private String	telephone;

	public BranchesHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		branch = new Branch(context, name);
		return new ActionDoc(context, "A new branch has been added", new Link(BranchesHandler.class));
	}

	public Object delete() throws ServletException {

		if (branch == null) {
			return main();
		}

		branch.delete();
		return new ActionDoc(context, "This branch has been deleted", new Link(BranchesHandler.class));
	}

	public Object edit() throws ServletException {

		if (branch == null) {
			return main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit branch", Tab.Orders);
		doc.setMenu(new ShoppingMenu());
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update branch"));
				sb.append(new ButtonTag(BranchesHandler.class, "delete", "Delete branch", "branch", branch)
						.setConfirmation("Are you sure you want to delete this branch?"));
				sb.append(new ButtonTag(BranchesHandler.class, null, "Return to branches menu"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("Branch setup"));

				sb.append(new AdminRow("Name", "Enter a name for this branch.", new TextTag(context, "name", branch.getName(), 40)));

				sb
						.append(new AdminRow("Address", "Enter an address for this branch.", new TextAreaTag(context, "address", branch
								.getAddress(), 40, 5)));

				sb.append(new AdminRow("Postcode", "Enter a postcode for this branch.", new TextTag(context, "postcode", branch.getPostcode(), 40)));

				sb
						.append(new AdminRow("Telephone", "Enter a telephone for this branch.", new TextTag(context, "telephone", branch
								.getTelephone(), 14)));

				sb.append(new AdminRow("Order emails", "Send an order notification to these emails.", new TextAreaTag(context, "orderEmails",
						StringHelper.implode(branch.getOrderEmails(), "\n"), 40, 4)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(BranchesHandler.class, "save", "post"));
				sb.append(new HiddenTag("branch", branch));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		if (!Module.Branches.enabled(context)) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Branches", Tab.Orders);
		doc.setMenu(new ShoppingMenu());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Branches"));

				sb.append("<tr>");
				sb.append("<th>Name</th>");
				sb.append("<th width='10'>Delete</th>");
				sb.append("</tr>");

				for (Branch branch : Branch.get(context)) {

					sb.append("<tr>");
					sb.append("<td>" + new LinkTag(BranchesHandler.class, "edit", new SpannerGif(), "branch", branch) + " " + branch.getName() +
							"</td>");
					sb.append("<td width='10'>" +
							new LinkTag(BranchesHandler.class, "delete", new DeleteGif(), "branch", branch)
									.setConfirmation("Are you sure you want to delete this branch?") + "</td>");
					sb.append("</tr>");

				}

				sb.append(new FormTag(BranchesHandler.class, "create", "post"));
				sb.append("<tr><td colspan='2'>Add a new branch " + new TextTag(context, "name", 20) + " " + new SubmitTag("Add") + "</td></tr>");
				sb.append("</form>");
				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (branch == null) {
			return main();
		}

		test(new RequiredValidator(), "name");
		if (hasErrors()) {
			return main();
		}

		branch.setName(name);
		branch.setPostcode(postcode);
		branch.setAddress(address);
		branch.setTelephone(telephone);
		branch.setOrderEmails(StringHelper.explodeStrings(orderEmails, "\n"));

		branch.save();

		return new ActionDoc(context, "This branch has been updated", new Link(BranchesHandler.class, "edit", "branch", branch));
	}
}