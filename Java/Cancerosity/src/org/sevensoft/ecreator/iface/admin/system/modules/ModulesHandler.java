package org.sevensoft.ecreator.iface.admin.system.modules;

import java.util.EnumSet;
import java.util.TreeSet;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.comparators.EnumStringComparator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 22-Jul-2005 09:40:32
 * 
 */
@Path("admin-modules.do")
public class ModulesHandler extends AdminHandler {

	private Module	module;

	public ModulesHandler(RequestContext context) {
		super(context);
	}

	public Object addModule() throws ServletException {

		logger.fine("[ModulesHandler] add module=" + module);

		if (module != null) {
			modules.addModule(module);
			modules.save();
		}

		clearParameters();
		addMessage("Module added");
		return main();
	}

	@Override
	public Object main() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Modules", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void modules() {

				sb.append(new AdminTable("Modules"));

				TreeSet<Module> currentModules = new TreeSet(new EnumStringComparator());
				currentModules.addAll(modules.getModules());

				for (Module module : currentModules) {

					String name = module.toString();
					if (module.beta()) {
						name = name + " *beta";
					}

					String description = module.getDescription();
					if (description == null) {
						description = "";
					}

					sb.append("<tr>");
					sb.append("<td>" + name + "</td>");
					sb.append("<td>" + description + "</td>");
					sb.append("<td>"
							+ new LinkTag(ModulesHandler.class, "removeModule", new DeleteGif(), "module", module)
									.setConfirmation("Are you sure you want to remove this module?") + "</td>");
					sb.append("</tr>");
				}

				TreeSet<Module> availableModules = new TreeSet(new EnumStringComparator());
				availableModules.addAll(EnumSet.allOf(Module.class));

				sb.append(new FormTag(ModulesHandler.class, "addModule", "post"));
				sb.append("<tr><td colspan='3'>Add module " + new SelectTag(context, "module", null, availableModules, "Select module") + " "
						+ new SubmitTag("Add") + "</td></tr>");
				sb.append("</form>");

				sb.append("</table>");

			}

			@Override
			public String toString() {

                commands();
				modules();
				commands();

				return sb.toString();
			}

		});
		return doc;
	}

	public Object removeModule() throws ServletException {

		logger.fine("[ModulesHandler] rmove module=" + module);

		if (module != null) {
			modules.removeModule(module);
			modules.save();
		}

		clearParameters();
		addMessage("Module removed");
		return main();
	}

}
