package org.sevensoft.ecreator.iface.admin.categories;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategoryItem;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 11-Jan-2006 16:14:50
 *
 */
@Path("admin-categories-contents.do")
public class CategoryContentsHandler extends AdminHandler {

	public enum Operation {
		Copy() {

			@Override
			public String toString() {
				return "Copy selected contents to";
			}
		},
		Move() {

			@Override
			public String toString() {
				return "Move selected contents to";
			}
		},
		Remove() {

			@Override
			public String toString() {
				return "Remove selected contents";
			}
		};
	}

	private CategoryItem	categoryItem;

	private Category		targetCategory;

	private Category		category;

	private Item		item;
	private List<Item>	items;
	private Operation		operation;
	private boolean		noCategory;

	public CategoryContentsHandler(RequestContext context) {
		super(context);
	}

	public Object add() throws ServletException {

		if (category == null || item == null) {
			return main();
		}

		item.addCategory(category);

		clearParameters();
		return main();
	}

	public Object copyTo() throws ServletException {

		if (category == null) {
			return main();
		}

		if (targetCategory == null) {
			return main();
		}

		int n = 0;
		for (Item item : items) {
			item.addCategory(targetCategory);
			n++;
		}

		addMessage(n + " items copied");
		clearParameters();
		return main();
	}

	@Override
	public Object main() throws ServletException {

		if (category == null) {
			return new CategoryHandler(context).main();
		}

		final List<CategoryItem> categoryItems = category.getCategoryItems();

		AdminDoc doc = new AdminDoc(context, user, "Category contents", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new ButtonTag(CategoryHandler.class, "edit", "Return to category", "category", category));
				sb.append("</div>");
			}

			private void contents() {

				sb.append(new AdminTable("Contents"));

				sb.append(new FormTag(CategoryContentsHandler.class, "add", "get"));
				sb.append(new HiddenTag("category", category));

				SelectTag tag = new SelectTag(context, "item");
				List<Item> items = Item.get(context, null, "Live", 0, 0);
				if (noCategory) {
					CollectionsUtil.filter(items, new Predicate<Item>() {

						public boolean accept(Item e) {
							return !e.hasCategory();
						}
					});
				}
				tag.addOptions(items);

				sb.append("<tr><td colspan='5'>Add item " + tag + new SubmitTag("Add item"));
				sb.append(" Show items not in a category only "
						+ new CheckTag(context, "noCategory", true, noCategory)
								.setOnChange("this.form.elements['action'].value='main'; this.form.submit();") + "</td></tr>");

				sb.append("</form>");

				sb.append(new FormTag(CategoryContentsHandler.class, "perform", "get").setId("contents_form"));
				sb.append(new HiddenTag("category", category));

				sb.append("<tr>");
				sb.append("<th width='60'>Position</th>");
				sb.append("<th width='60'>Id</th>");
				sb.append("<th width='80'>Select</th>");
				sb.append("<th>Content</th>");
				sb.append("<th width='10'> </th>");
				sb.append("</tr>");

				PositionRender pr = new PositionRender(CategoryContentsHandler.class, "categoryItem");
				for (CategoryItem categoryItem : categoryItems) {

					Item item = categoryItem.getItem();

					sb.append("<tr>");
					sb.append("<td width='60'>" + pr.render(categoryItem) + "</td>");
					sb.append("<td width='60'>" + new LinkTag(ItemHandler.class, "edit", item.getIdString(), "item", item) + "</td>");
					sb.append("<td width='80'>" + new CheckTag(context, "items", item.getIdString(), false) + "</td>");
					sb.append("<td>" + new LinkTag(ItemHandler.class, "edit", item.getName(), "item", item) + "</td>");
					sb.append("<td width='10'>"
							+ new LinkTag(CategoryContentsHandler.class, "removeCategory", new DeleteGif(), "category", category, "item", item)
							+ "</td>");
					sb.append("</tr>");
				}

				SelectTag targetCategoryTag = new SelectTag(context, "targetCategory");
				targetCategoryTag.setAny("-Choose category-");
				targetCategoryTag.addOptions(Category.getSimpleMapVisible(context));

				SelectTag operationTag = new SelectTag(context, "operation");
				operationTag.addOptions(Operation.values());

				sb.append("<tr><td colspan='5'>" + operationTag + " " + targetCategoryTag + " " + new SubmitTag("Go") + "</td></tr>");
				sb.append("</form>");

				sb.append("</table>");

			}

			@Override
			public String toString() {

                commands();
				contents();
				commands();

				return sb.toString();
			}

		});
		return doc;
	}

	public Object removeCategory() throws ServletException {
		
		if (category == null || item == null) {
			return index();
		}
		
		item.removeCategory(category);
		return main();
	}

	public Object moveDown() throws ServletException {

		if (categoryItem == null) {
			return index();
		}

		category = categoryItem.getCategory();

		List<CategoryItem> categoryItems = CategoryItem.get(context, category);
		EntityUtil.moveDown(categoryItem, categoryItems);

		return main();
	}

	public Object moveTo() throws ServletException {

		if (category == null) {
			return main();
		}

		if (targetCategory == null) {
			return main();
		}

		int n = 0;
		for (Item item : items) {
			item.removeCategory(category);
			item.addCategory(targetCategory);
			n++;
		}

		addMessage(n + " items moved");
		clearParameters();
		return main();
	}

	public Object moveUp() throws ServletException {

		if (categoryItem == null) {
			return index();
		}

		category = categoryItem.getCategory();

		List<CategoryItem> categoryItems = CategoryItem.get(context, category);
		EntityUtil.moveUp(categoryItem, categoryItems);

		return main();
	}

	public Object perform() throws ServletException {

		if (category == null || operation == null) {
			return main();
		}

		switch (operation) {

		case Copy:

			if (targetCategory == null)
				return main();

			for (Item item : items)
				item.addCategory(targetCategory);

			addMessage(items.size() + " items copied");
			break;

		case Move:

			if (targetCategory == null)
				return main();

			for (Item item : items) {
				item.removeCategory(category);
				item.addCategory(targetCategory);
			}

			addMessage(items.size() + " items moved");
			break;

		case Remove:

			for (Item item : items) {
				item.removeCategory(category);
			}

			addMessage(items.size() + " items removed");
			break;
		}

		clearParameters();
		return main();
	}

	public Object remove() throws ServletException {

		if (category == null) {
			return main();
		}

		int n = 0;
		for (Item item : items) {
			item.removeCategory(category);
			n++;
		}

		addMessage(n + " items moved");
		clearParameters();
		return main();
	}
}
