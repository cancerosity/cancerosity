package org.sevensoft.ecreator.iface.admin.marketing.newsletter;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.marketing.MarketingMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterControl;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterOption;
import org.sevensoft.ecreator.model.marketing.newsletter.example.NewsletterExample;
import org.sevensoft.ecreator.model.marketing.newsletter.templates.NewsletterTemplate;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTags;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.util.Keypad;

import java.util.List;
import java.util.Set;

/**
 * @author sks 17-Jun-2005 17:37:55
 */
@Path("admin-newsletters-compose.do")
public class NewsletterComposeHandler extends AdminHandler {

    private String body, subject, otherEmails;
    private boolean accounts, subscribers;
    private MultiValueMap<Attribute, String> attributeValues;
    private Set<Newsletter> newsletters;
    private Set<NewsletterOption> newsletterOptions;
    private String testers;
    private boolean save;
    private transient NewsletterControl newsletterControl;
    private NewsletterTemplate template;
    private ItemType itemType;
    private NewsletterExample example;

    public NewsletterComposeHandler(RequestContext context) {
        super(context);
        newsletterControl = NewsletterControl.getInstance(context);
    }

    @Override
    public Object main() {

        final List<Newsletter> newsletters = Newsletter.get(context);

        AdminDoc doc = new AdminDoc(context, user, "Compose newsletter", Tab.Marketing);

        if (miscSettings.isAdvancedMode()) {
            doc.setMenu(new MarketingMenu());
        }

        doc.setIntro("Here you can compose a new newsletter to send to the subscribers, or to your general members.");
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Preview message"));
                sb.append("</div>");
            }

            private void content() {

                sb.append(new AdminTable("Content"));

                sb.append("<tr><td>" + new TextAreaTag(context, "body").setId("body").setStyle("width: 100%; height: 320px;") + "</td></tr>");
                sb.append("</table>");
            }

            private void details() {

                sb.append(new AdminTable("Newsletter details"));

                sb.append(new AdminRow("Subject", "Enter the subject for the email.", new TextTag(context, "subject", 40) + " "
                        + new ErrorTag(context, "subject", "<br/>")));

                if (newsletters.size() == 1) {

                    sb.append(new HiddenTag("newsletters", newsletters.get(0)));

                } else {

                    StringBuilder sb2 = new StringBuilder();

                    for (Newsletter newsletter : newsletters) {
                        sb2.append(new CheckTag(context, "newsletters", newsletter, false) + " " + newsletter.getName() + "<br/>");

                        if (Module.NewsletterNewItems.enabled(context)) {
                            for (NewsletterOption newsletterOption : NewsletterOption.get(context, newsletter)) {
                                sb2.append("&nbsp;&nbsp;&nbsp;&nbsp;");
                                sb2.append(new CheckTag(context, "newsletterOptions", newsletterOption, false) + " " + newsletterOption.getName() + "<br/>");
                            }
                        }
                    }

                    sb.append(new AdminRow("Newsletters", "Choose the newsletter categories to send this newsletter.", sb2));
                }

                if (Module.NewsletterTemplates.enabled(context)) {

                    sb.append(new AdminRow("Newsletter template", "Choose a pre-defined layout to wrap around your newsletter content.",
                            new SelectTag(context, "template", null, NewsletterTemplate.get(context), "-No template-")));

                }

                if (Module.NewsletterExamples.enabled(context)) {
                    SelectTag selection = new SelectTag(context, "example", null, NewsletterExample.get(context), "-No example-");
                    String javascript= "tinyMCE.setContent(this.options[this.selectedIndex].value);";
                    selection.setOnChange(javascript);
                    sb.append(new AdminRow("Newsletter example", "Choose a pre-defined newsletter content.",
                           selection));

                }

                //				if (newsletterControl.isSaveNewsletters()) {
                //
                //					sb.append("<tr><td width='300'><b>Save newsletter</b><br/>"
                //							+ "Tick this box and the newsletter will be added to your saved list of "
                //							+ "newsletters so you can re-use it as a template very easily.</td>");
                //					sb.append("<td>" + new CheckTag(context, "save", true, false) + "</td></tr>");
                //
                //				}

                if (miscSettings.isAdvancedMode()) {

                    sb.append(new AdminRow("Test newsletter",
                            "You can test the newsletter by sending it to your own addresses to make sure everything is formatted correctly before you "
                                    + "send it to the public.", new TextAreaTag(context, "testers", 50, 4)));

                }

                sb.append("</td></tr>");
                sb.append("</table>");
            }

            private void recipients() {

                sb.append("<script>");
                sb.append("function display(id, show) {");
                sb.append("	var e = document.getElementById(id); ");
                sb.append("	if (show) e.style.display = 'block'; ");
                sb.append("	else e.style.display = 'none'; ");
                sb.append(" } ");
                sb.append("</script>");

                sb.append("<style>");
                sb.append("div.hidden { border: 1px solid #feba02; background: #fff9e7; display: none; padding: 5px; margin: 8px 5px 2px 5px; } ");
                sb.append("</style>");

                sb.append(new AdminTable("Recipients"));

                if (newsletters.size() == 1) {

                    sb
                            .append(new AdminRow("Subscribers", "Include subscribers to your newsletter.", new CheckTag(context, "subscribers",
                                    true, true)));

                } else {

//                    Keypad keypad = new Keypad(3);
                    Keypad keypad = new Keypad(newsletters.size());

                    for (Newsletter newsletter : newsletters) {
                        keypad.addObject(new CheckTag(context, "newsletters", newsletter, false) + " " + newsletter.getName());
                    }

                    for (Newsletter newsletter : newsletters) {
                        List<NewsletterOption> newsletterOptions = NewsletterOption.getVisible(context, newsletter);
                        StringBuilder sb3 = new StringBuilder();
                        for (NewsletterOption newsletterOption : newsletterOptions) {
                            sb3.append("&nbsp;&nbsp;&nbsp;&nbsp;");
                            sb3.append(new CheckTag(context, "newsletterOptions", newsletterOption, false) + " " + newsletterOption.getName() + "<br/>");
                        }
                        keypad.addObject(sb3);
                    }

                    sb.append(new AdminRow("Subscribers", "Include subscribers to these newsletters.", keypad));
                }

                final List<ItemType> accountTypes = ItemType.getAccounts(context);
                String accountTypesTag;
                if (accountTypes.isEmpty()) {
                    accountTypesTag = "";
                } else {
                    accountTypesTag = new SelectTag(context, "itemType", null, accountTypes, "-All account types-").toString();
                }

                sb.append(new AdminRow("Accounts", "Include registered accounts as recipients.", new CheckTag(context, "accounts", true, false) + " "
                        + accountTypesTag));

                sb.append("</td></tr>");

                sb.append("</table>");
            }

            @Override
            public String toString() {

                RendererUtil.tinyMce(context, sb, "body");

                sb.append(new FormTag(NewsletterComposeHandler.class, "preview", "post"));

                commands();
                details();

                recipients();

                content();
                commands();

                sb.append("</form>");

                return sb.toString();
            }
        });
        return doc;
    }

    public Object preview() {

        test(new RequiredValidator(), "body");
        test(new RequiredValidator(), "subject");

        if (hasErrors()) {
            return main();
        }

        if (newsletters.isEmpty() && newsletterOptions.isEmpty()) {
            addMessage("You must select at least one newsletter to send to");
            return main();
        }

        AdminDoc doc = new AdminDoc(context, user, "Compose newsletter - Preview", Tab.Marketing);
        doc.setMenu(new MarketingMenu());
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Send message"));
                sb.append(new ButtonTag(NewsletterComposeHandler.class, null, "Change message", "body", body, "members", accounts, "subscribers",
                        subscribers, "otherEmails", otherEmails, "subject", subject, "testers", testers, "template", template));

                sb.append("</div>");
            }

            private void preview() {

                sb.append(new AdminTable("Newsletter preview"));

                List<String> list = StringHelper.explodeStrings(testers, "\\s");

                if (testers != null) {
                    sb.append("<tr><td colspan='2'>Testing newsletter by sending to:  " + StringHelper.implode(list, ", ", true) + "</td></tr>");
                }

                if (Module.NewsletterTemplates.enabled(context) && template != null) {
                    sb.append("<tr><td width='300'><b>Template</b></td><td>" + template.getName() + "</td></tr>");
                }

                sb.append("<tr><td width='300'><b>Subject</b></td><td>" + subject + "</td></tr>");
                sb.append("<tr><td width='300'><b>Message</b></td><td>" + body + "</td></tr>");
                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(NewsletterComposeHandler.class, "send", "post"));
                sb.append(new HiddenTag("body", HtmlHelper.escape(body)));
                sb.append(new HiddenTag("subject", subject));
                sb.append(new HiddenTag("testers", testers));
                sb.append(new HiddenTag("accounts", accounts));
                sb.append(new HiddenTag("subscribers", subscribers));
                if (itemType != null) {
                    sb.append(new HiddenTag("itemType", itemType));
                }
                if (template != null) {
                    sb.append(new HiddenTag("template", template));
                }
                sb.append(new HiddenTags("attributeValues", attributeValues));
                for (Newsletter newsletter : newsletters) {                
                    sb.append(new HiddenTag("newsletters", newsletter));
                }
                for (NewsletterOption newsletterOption : newsletterOptions) {
                    sb.append(new HiddenTag("newsletterOptions", newsletterOption));
                }


                commands();
                preview();
                commands();

                sb.append("</form>");

                return sb.toString();
            }
        });
        return doc;
    }

    public Object send() {

        test(new RequiredValidator(), "body");
        test(new RequiredValidator(), "subject");

        if (hasErrors()) {
            return main();
        }

        try {

            List<String> testerEmails = StringHelper.explodeStrings(testers, "\\s");

            newsletterControl.sendNewsletter(template, newsletters, newsletterOptions, testerEmails, subscribers, accounts, subject, body, attributeValues, itemType);
            AdminDoc doc = new AdminDoc(context, user, "Compose newsletter - Sent", Tab.Marketing);
            doc.setMenu(new MarketingMenu());
            doc.addBody(new Body() {

                @Override
                public String toString() {
                    return "Newsletter sent.<br/><br/>Messages can take several hours to arrive if the users ISP is experiencing heavy load.";
                }
            });
            return doc;

        } catch (final Exception e) {

            e.printStackTrace();

            AdminDoc doc = new AdminDoc(context, user, "Compose newsletter - Mail server error", Tab.Marketing);
            doc.setMenu(new MarketingMenu());
            doc.addBody(new Body() {

                @Override
                public String toString() {
                    return "There was a problem with the mail server. Please try sending the newsletter again in a few moments.<br/><br/>"
                            + "If the error persists please contact support.<br/><br/>Error message: " + e.getMessage();
                }
            });
            return doc;
        }

    }
}