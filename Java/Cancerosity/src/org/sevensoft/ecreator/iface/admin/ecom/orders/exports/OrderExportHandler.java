package org.sevensoft.ecreator.iface.admin.ecom.orders.exports;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.ecom.branches.Branch;
import org.sevensoft.ecreator.model.ecom.orders.export.OrderExport;
import org.sevensoft.ecreator.model.ecom.orders.export.OrderExport.Format;
import org.sevensoft.ecreator.model.ecom.orders.export.OrderExport.Method;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 26 Mar 2007 13:21:30
 *
 */
@Path("admin-orders-exports.do")
public class OrderExportHandler extends AdminHandler {

	private Method		method;
	private Format		format;
	private OrderExport	export;
	private String		emails;

	/**
	 * Send data as emails to the branch
	 */
	private Branch		branch;

	public OrderExportHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		if (method == null) {
			return main();
		}

		export = new OrderExport(context, method);
		return new ActionDoc(context, "A new order export has been created", new Link(OrderExportHandler.class));
	}

	public Object delete() throws ServletException {

		if (export == null) {
			return main();
		}

		export.delete();
		return new ActionDoc(context, "This order export has been deleted", new Link(OrderExportHandler.class));
	}

	public Object edit() throws ServletException {

		if (export == null) {
			return main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit order export", Tab.Orders);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update email"));
				sb.append(new ButtonTag(OrderExportHandler.class, "delete", "Delete export", "export", export)
						.setConfirmation("Are you sure you want to delete this export?"));
				sb.append(new ButtonTag(OrderExportHandler.class, null, "Return to exports"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Method", "Select how the data is delivered.", new SelectTag(context, "method", export.getMethod(),
						OrderExport.Method.values())));

				sb.append(new AdminRow("Format", "Choose the data format for this export.'", new SelectTag(context, "format", export.getFormat(),
						OrderExport.Format.values())));

				if (Module.Branches.enabled(context)) {
					sb.append(new AdminRow("Branch", "Apply this export to this branch only.", new SelectTag(context, "branch", export.getBranch(),
							Branch.get(context), "All branches")));
				}

				switch (export.getMethod()) {

				default:
					break;

				case Email:

					sb.append(new AdminRow("Email addresses", "Set the email addresses that will receive this export.'", new TextAreaTag(context,
							"emails", StringHelper.implode(export.getEmails(), "\n"), 50, 4)));

				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(OrderExportHandler.class, "save", "post"));
				sb.append(new HiddenTag("export", export));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		final List<OrderExport> exports = OrderExport.get(context);

		AdminDoc doc = new AdminDoc(context, user, "Order exports", Tab.Orders);
		doc.addBody(new Body() {

			private void menu() {

				int span = 3;

				sb.append(new AdminTable("Order exports"));

				sb.append("<tr>");
				sb.append("<th>Method</th>");
				sb.append("<th>Format</th>");

				if (Module.Branches.enabled(context)) {
					sb.append("<th>Branch</th>");
					span++;
				}

				sb.append("<th width='10'> </th>");
				sb.append("</tr>");

				for (OrderExport export : exports) {

					sb.append("<tr>");
					sb.append("<td>" + new LinkTag(OrderExportHandler.class, "edit", new SpannerGif(), "export", export) + " " + export.getMethod()
							+ "</td>");
					sb.append("<td>" + export.getFormat() + "</td>");

					if (Module.Branches.enabled(context)) {
						sb.append("<td>" + (export.hasBranch() ? export.getBranch().getName() : "--") + "</td>");
					}

					sb.append("<td width='10'>"
							+ new LinkTag(OrderExportHandler.class, "delete", new DeleteGif(), "export", export)
									.setConfirmation("Are you sure you want to delete this export?") + "</td>");
					sb.append("</tr>");
				}

				sb.append(new FormTag(OrderExportHandler.class, "create", "post"));
				sb.append("<tr><td colspan='" + span + "'>Add export " + new SelectTag(context, "method", null, Method.values()) + " "
						+ new SubmitTag("Add") + "</td></tr>");
				sb.append("</form>");

				sb.append("</form>");
				sb.append("</table>");
			}

			@Override
			public String toString() {

				menu();
				return sb.toString();
			}

		});

		return doc;

	}

	public Object save() throws ServletException {

		if (export == null) {
			return main();
		}

		export.setFormat(format);
		export.setMethod(method);
		export.setBranch(branch);
		export.setEmails(StringHelper.explodeStrings(emails, "\n"));
		export.save();

		return new ActionDoc(context, "This order export has been updated", new Link(OrderExportHandler.class, "edit", "export", export));
	}
}
