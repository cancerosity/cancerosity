package org.sevensoft.ecreator.iface.admin.media.galleries;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.misc.html.EditMarkupButton;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.media.images.galleries.blocks.GalleryBlock;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 6 Feb 2007 12:49:23
 */
@Path("admin-galleries-blocks.do")
public class GalleryBlockHandler extends BlockEditHandler {

    private GalleryBlock block;

    private boolean useGrid;
    private Markup markup;

    public GalleryBlockHandler(RequestContext context) {
        super(context);
    }

    @Override
    protected Block getBlock() {
        return block;
    }

    @Override
    protected void saveSpecific() {
        block.setUseGrid(useGrid);
        if (!ObjectUtil.equal(block.getMarkup(), markup)) {
            block.setMarkup(markup);
        }
    }

    @Override
    protected void specifics(StringBuilder sb) {
        sb.append(new AdminTable("Gallery Renderer"));

        sb.append(new AdminRow("Use Grid renderer", "", new BooleanRadioTag(context, "useGrid", block.isUseGrid())));

        SelectTag markupTag = new SelectTag(context, "markup", block.getMarkup(), Markup.get(context), "-Recreate-");
        markupTag.setId("markup");
        sb.append(new AdminRow(true, "Markup", "Custom markup for rendering this box.", markupTag + " " + new EditMarkupButton("markup")));

        sb.append("</table>");
    }

}
