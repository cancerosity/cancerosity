package org.sevensoft.ecreator.iface.admin.items.search;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 7 Jun 2006 15:49:33
 *
 */
public class ItemSearchAjaxTag {

	private static int		counter;

	/**
	 * The element we are going to put the results into
	 */
	private final String		targetElementId;
	private final RequestContext	context;
	private final String		inputElementId;
	private final String		param;
	private final Link		link;

	public ItemSearchAjaxTag(RequestContext context, String targetElementId, Link link, String param) {
		this.context = context;
		this.targetElementId = targetElementId;
		this.link = link;
		this.param = param;
		this.inputElementId = "searchText" + counter;
		counter++;
	}

	@Override
	public String toString() {

		String onclick = "ajaxRequest('" + new Link(ItemSearchAjaxHandler.class, null, "param", param, "link", link)
				+ "&searchText=' + document.getElementById('" + inputElementId + "').value, '" + targetElementId + "');";

		StringBuilder sb = new StringBuilder();
		sb.append(new TextTag(context, "searchText", 20).setId(inputElementId));
		sb.append(new ButtonTag("Search").setOnClick(onclick));

		return sb.toString();
	}
}
