package org.sevensoft.ecreator.iface.admin.media.galleries;

import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.misc.seo.panels.MetaTagsPanel;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasHandler;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.media.images.panels.ImageAdminPanel;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.EasyDoc;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.ImageUtil;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.media.images.galleries.Gallery;
import org.sevensoft.ecreator.model.media.images.galleries.Gallery.Style;
import org.sevensoft.ecreator.model.media.images.galleries.ImageSortType;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;
import java.util.List;
import java.util.Map;
import java.io.IOException;

/**
 * @author sks 07-Jul-2005 12:39:17
 */
@Path("admin-galleries.do")
public class GalleryHandler extends AdminHandler {

    private Map<Integer, String> description;
    private Gallery gallery;
    private Img image;
    private String name;
    private int cols;
    private Style style;
    private List<String> imageUrls;
    private List<Upload> imageUploads;
    private Map<Integer, String> imageDescriptions;
    private int rows;
    private int height;
    private int width;
    private boolean showDateUploaded;
    private boolean printable;
    private int slideshowInterval;
    private boolean enableSlideshow;
    private String imageFilename;
    private ImageSortType sortType;
    private boolean enableSortOption;
    private boolean enableSearchOption;
    private String titleTag;
    private String descriptionTag;
    private String keywords;

    public GalleryHandler(RequestContext context) {
        super(context);
    }

    public GalleryHandler(RequestContext context, Gallery gallery) {
        this(context);
        this.gallery = gallery;
    }

    public Object create() {

        AdminDoc doc = new AdminDoc(context, user, "Create a gallery", Tab.Extras);
        doc.setIntro("Create a new gallery by giving it a name");
        if (miscSettings.isAdvancedMode()) {
            doc.setMenu(new ExtrasMenu());
        }
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new FormTag(GalleryHandler.class, "doCreate", "POST"));

                sb.append(new TableTag("simplebox", "center"));

                sb.append("<tr><td align='center'>Enter the name of the gallery to create.</td></tr>");
                sb.append("<tr><td align='center'>" + new TextTag(context, "name", 40) + "<br/><br/></td></tr>");
                sb.append("</table>");

                sb.append("<div align='center' class='actions'>" + new SubmitTag("Create gallery") + "</div>");

                sb.append("</form>");
                return sb.toString();
            }

        });
        return doc;

    }

    public Object delete() throws ServletException {

        if (gallery == null) {
            return main();
        }

        gallery.delete();

        if (miscSettings.isAdvancedMode()) {
            return new ExtrasHandler(context).main();
        }

        AdminDoc doc = new AdminDoc(context, user, "The gallery has been deleted", Tab.Extras);
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new TableTag("simplelinks", "center"));
                sb.append("<tr><th align='center'>What would you like to do ?</th></tr>");

                sb.append("<tr><td align='center'>" + new LinkTag(GalleryHandler.class, "create", "I want to create a new gallery")
                        + "</td></tr>");

                sb.append("<tr><td align='center'>" + new LinkTag(ExtrasHandler.class, null, "I want to return to the extras menu")
                        + "</td></tr>");

                sb.append("</table>");

                return sb.toString();
            }
        });
        return doc;
    }

    public Object doCreate() throws ServletException {

        if (name == null) {
            return create();
        }

        gallery = new Gallery(context, name);

        AdminDoc doc = new AdminDoc(context, user, "This gallery has been created", Tab.Extras);
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new TableTag("simplelinks", "center"));

                sb.append("<tr><th>What do you want to do now?</th></tr>");
                sb.append("<tr><td align='center'>"
                        + new LinkTag(GalleryHandler.class, "edit", "I want to add images to this gallery", "gallery", gallery)
                        + "</td></tr>");
                sb.append("<tr><td align='center'>" + new LinkTag(ExtrasHandler.class, null, "I want to return to the extras menu")
                        + "</td></tr>");

                sb.append("</table>");

                return sb.toString();
            }
        });
        return doc;
    }

    public Object edit() throws ServletException {

        if (gallery == null) {
            return main();
        }

        AdminDoc doc = new AdminDoc(context, user, "Edit gallery", Tab.Extras);
        if (miscSettings.isAdvancedMode()) {
            doc.setMenu(new ExtrasMenu());
        }
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update gallery"));
                sb.append(new ButtonTag(GalleryHandler.class, "delete", "Delete gallery", "gallery", gallery)
                        .setConfirmation("Are you sure you want to delete this gallery?"));

                sb.append("</div>");
            }

            private void details() {

                sb.append(new AdminTable("Gallery details"));

                sb.append(new AdminRow("Name", "Enter a name for this gallery", new TextTag(context, "name", gallery.getName(), 30) + ""
                        + new ErrorTag(context, "name", "<br/>")));

                sb.append(new AdminRow("Date uploaded", "Show the date the image was uploaded underneath the image.", new BooleanRadioTag(context,
                        "showDateUploaded", gallery.isShowDateUploaded())));

                sb.append(new AdminRow("Print option", "Allows photos to be printable", new BooleanRadioTag(context,
                        "printable", gallery.isPrintable())));
                sb.append(new AdminRow("Slideshow option", "Enables slideshow", new BooleanRadioTag(context,
                        "enableSlideshow", gallery.isEnableSlideshow())));
                sb.append(new AdminRow("Slideshow timeout", "The delay between slides (in millis)", new TextTag(context, "slideshowInterval", gallery.getSlideshowInterval(), 4)));

                switch (gallery.getStyle()) {

                    case Grid:

                        sb
                                .append(new AdminRow("Grid size", "Number of rows and columns to show per page.", "Rows "
                                        + new TextTag(context, "rows", gallery.getRows(), 4) + " Cols "
                                        + new TextTag(context, "cols", gallery.getCols(), 4)));

                        if (miscSettings.isAdvancedMode()) {

                            sb.append(new AdminRow("Dimensions of thumbnail", "Enter the dimensions of the thumbnail images", "Width: "
                                    + new TextTag(context, "width", gallery.getWidth(), 4) + " Height: "
                                    + new TextTag(context, "height", gallery.getHeight(), 4)));

                        }

                        break;
                }
                sb.append(new AdminRow("Enable sort option", "", new BooleanRadioTag(context, "enableSortOption", gallery.isEnableSortOption())));
                sb.append(new AdminRow("Sort option", "Defines order of images in gallery", getSortOptionsTag()));

                sb.append(new AdminRow("Enable search option", "", new BooleanRadioTag(context, "enableSearchOption", gallery.isEnableSearchOption())));
                
                sb.append("</table>");
            }

            private void meta() {
                                
                sb.append(new MetaTagsPanel(context, gallery));

            }

            @Override
            public String toString() {

                sb.append(new FormTag(GalleryHandler.class, "save", "multi"));
                sb.append(new HiddenTag("gallery", gallery));

                commands();
                details();
                meta();
                sb.append(new ImageAdminPanel(context, gallery));

                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object list() {

        AdminDoc doc = new AdminDoc(context, user, "Edit gallery", Tab.Extras);
        if (miscSettings.isAdvancedMode()) {
            doc.setMenu(new ExtrasMenu());
        }
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new FormTag(GalleryHandler.class, "edit", "POST"));

                sb.append(new TableTag("simplebox", "center"));

                sb.append("<tr><td align='center'>Select the gallery you want to edit.</td></tr>");
                sb.append("<tr><td align='center'>" + new SelectTag(context, "gallery", null, Gallery.get(context), "-Choose gallery-") + "</td></tr>");

                sb.append("<tr><td align='center'>" + new SubmitTag("Continue >") + "<br/><br/></td></tr>");
                sb.append("</table>");

                sb.append("</form>");
                return sb.toString();
            }

        });
        return doc;
    }

    @Override
    public Object main() throws ServletException {
        return list();
    }

    public Object menu() throws ServletException {

        EasyDoc doc = new EasyDoc(context, user, "Galleries", Tab.Galleries);
        doc.setIntro("What would you like to do?");
        doc.setForm(new FormTag(GalleryHandler.class, "edit", "POST"));
        doc.addLink("I want to create a new image gallery", new Link(GalleryHandler.class, "create"));

        List<Gallery> gallerys = Gallery.get(context);
        if (gallerys.size() > 0) {
            doc.addText("I want to add or remove images to an existing gallery");
            doc.addControls(new SelectTag(context, "gallery", null, gallerys, "-Choose gallery-"));
            doc.addControls(new SubmitTag("Continue >"));
        }

        return doc;
    }

    private SelectTag getSortOptionsTag() {
        SelectTag tag = new SelectTag(context, "sortType");
        tag.addOptions(ImageSortType.values());
        if (gallery != null) {
            tag.setValue(gallery.getSortType());
        }
        return tag;
    }

    public Object save() throws ServletException {

        if (gallery == null) {
            return main();
        }

        test(new RequiredValidator(), "name");
        if (hasErrors()) {
            return edit();
        }

        gallery.setName(name);

        gallery.setCols(cols);
        gallery.setRows(rows);
        gallery.setShowDateUploaded(showDateUploaded);
        gallery.setPrintable(printable);
        gallery.setEnableSlideshow(enableSlideshow);
        gallery.setSlideshowInterval(slideshowInterval);
        gallery.setEnableSortOption(enableSortOption);
        gallery.setSortType(sortType);
        gallery.setEnableSearchOption(enableSearchOption);

        if (miscSettings.isAdvancedMode()) {

            gallery.setWidth(width);
            gallery.setHeight(height);

        }

        gallery.setTitleTag(titleTag);
        gallery.setDescriptionTag(descriptionTag);
        gallery.setKeywords(keywords);

        gallery.save();

        try {
            ImageUtil.save(context, gallery, imageUploads, imageUrls, imageFilename, true);
        } catch (ImageLimitException e) {
            addError(e);
        } catch (IOException e) {
            addError("The image you are attempting to upload is in CYMK colour mode. This is unsopported currently, " +
                    "please use a photo editing application (e.g. Photoshop) to convert the file to RGB colour. For more information or assistance please contact support");
        }

        clearParameters();
        addMessage("Changes saved.");
        return edit();
    }
}
