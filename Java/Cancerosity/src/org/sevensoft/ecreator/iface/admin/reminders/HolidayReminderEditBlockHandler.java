package org.sevensoft.ecreator.iface.admin.reminders;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.reminders.blocks.HolidayReminderBlock;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author dme 31 Aug 2007 10:31:25
 */
@Path("admin-holiday-reminder-blocks.do")
public class HolidayReminderEditBlockHandler extends BlockEditHandler {

    private HolidayReminderBlock block;

    public HolidayReminderEditBlockHandler(RequestContext context) {
        super(context);
    }

    @Override
    protected Block getBlock() {
        return block;
    }

    @Override
    protected void saveSpecific() {
    }

    @Override
    protected void specifics(StringBuilder sb) {
    }

}