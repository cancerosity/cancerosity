package org.sevensoft.ecreator.iface.admin.items.stats;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.stats.hits.pages.PageHitCounterMonthly;
import org.sevensoft.ecreator.model.stats.submissions.FormSubmissionCounter;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author Stephen K Samuel samspade79@gmail.com May 23, 2010 9:52:09 PM
 * 
 */
@Path("admin-items-submissions.do")
public class ItemSubmissionsHandler extends AdminHandler {

	private Item	item;
	private Item	account;

	public ItemSubmissionsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {
		return monthly();
	}

	/**
	 * Shows submissions by form per month
	 */
	public Object monthly() throws ServletException {

		final List<FormSubmissionCounter> counters;
        AdminDoc page;
        
        if (account != null) {
            counters = FormSubmissionCounter.getForAccount(context, account);
            page = new AdminDoc(context, user, "Submissions monthly", Tab.getItemTab(account));
        } else {
            counters = FormSubmissionCounter.getForItem(context, item, 12);
            page = new AdminDoc(context, user, "Submissions monthly", Tab.getItemTab(item));
        }

		// page.setMenu(new StatsMenu());
		page.setIntro("Listed here are your total submissions for this item for the past 12 months");
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Submissions by item monthly"));

				sb.append("<tr>");
				sb.append("<td>Month</td>");
				sb.append("<td>Total submissions</td>");
				sb.append("</tr>");

				for (FormSubmissionCounter counter : counters) {

					sb.append("<tr>");
					sb.append("<td>" + counter.getDate().toString("MMMM-yy") + "</td>");
					sb.append("<td>" + counter.getTotal() + "</td>");
					sb.append("</tr>");

				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return page;
	}
}
