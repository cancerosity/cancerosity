package org.sevensoft.ecreator.iface.admin.system.lookandfeel;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 15 Oct 2006 20:50:33
 * 
 * Helpful renderer of our standard layout for admin rows
 * Ie, 2 cells, the first containing the label / description, the second the tag.
 *
 */
public class AdminRow {

	private final String		label;
	private final String		desc;
	private final boolean		superman;
	private int				span;
	private String			id;
	private String			style;
	private ToolTip			help;
	private final List<Object>	controls;

	public AdminRow(boolean superman, String label, String desc, Object obj) {
		this.superman = superman;
		this.label = label;
		this.desc = desc;
		this.span = 1;
		this.controls = new ArrayList();
		addControl(obj);
	}

	public void addControl(Object obj) {
		this.controls.add(obj);
	}

	public void addControls(Object... objs) {
		for (Object obj : objs) {
			addControl(obj);
		}
	}

	public AdminRow(String label, Object obj) {
		this(false, label, null, obj);
	}

	public AdminRow(String label, String desc, Object obj) {
		this(false, label, desc, obj);
	}

	public AdminRow setHelp(ToolTip help) {

		this.help = help;
		return this;
	}

	public void setId(String string) {
		id = string;
	}

	public final AdminRow setSpan(int span) {
		this.span = span;
		return this;
	}

	public void setStyle(String string) {
		this.style = string;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("<tr");
		if (id != null) {
			sb.append(" id=\"");
			sb.append(id);
			sb.append("\"");
		}
		if (style != null) {
			sb.append(" style=\"");
			sb.append(style);
			sb.append("\"");
		}
		sb.append("><td width='240' valign='top'");
		if (superman) {
			sb.append(" class='superman'");
		}

		sb.append(">");

		sb.append("<div style='position: relative'><strong>");
		sb.append(label);

		if (help != null) {

			LinkTag link = new LinkTag("#", "?");
			link.setOnClick("return false");
			link.setOnMouseOver("document.getElementById('" + help.getId() + "').style.display='block'");
			link.setOnMouseOut("document.getElementById('" + help.getId() + "').style.display='none'");

			sb.append(" ");
			sb.append(link);

			sb.append("<div class='help' id='" + help.getId() + "'>" + help + "</div>");

		}
		sb.append("</strong></div> ");

		if (desc != null) {
			sb.append(desc);
		}

		sb.append("</td><td");
		if (span > 1) {
			sb.append(" colspan='");
			sb.append(span);
			sb.append("'");
		}
		sb.append(">");

		for (Object obj : controls) {
			if (obj != null) {
				sb.append(obj);
			}
		}

		sb.append("</td></tr>");

		return sb.toString();

	}
}
