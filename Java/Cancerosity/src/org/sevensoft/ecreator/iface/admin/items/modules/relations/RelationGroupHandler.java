package org.sevensoft.ecreator.iface.admin.items.modules.relations;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemTypeMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.relations.RelationGroup;
import org.sevensoft.ecreator.model.items.relations.RelationsModule;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 29 Mar 2007 08:22:35
 *
 */
@Path("admin-relations-groups.do")
public class RelationGroupHandler extends AdminHandler {

	private RelationGroup	relationGroup;
	private ItemType		itemType;
	private boolean		reciprocal;
	private String		name;

	public RelationGroupHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		if (itemType == null) {
			return index();
		}

		relationGroup = new RelationGroup(context, itemType, "new");
		return new ActionDoc(context, "A new relation group has been added", new Link(ItemTypeHandler.class, "edit", "itemType", itemType));
	}

	public Object delete() throws ServletException {

		if (relationGroup == null) {
			return index();
		}

		itemType = relationGroup.getItemType();
		RelationsModule module = itemType.getRelationsModule();

		module.removeGroup(relationGroup);
		return new ActionDoc(context, "This relation group has been deleted", new Link(ItemTypeHandler.class, "edit", "itemType", itemType));
	}

	@Override
	public Object main() throws ServletException {

		if (relationGroup == null) {
			return index();
		}

		itemType = relationGroup.getItemType();
		//		AssociationsModule module = itemType.getAssociationsModule();

		AdminDoc doc = new AdminDoc(context, user, "Edit relation group: " + relationGroup.getName(), Tab.getItemTab(itemType));
		doc.setMenu(new EditItemTypeMenu(itemType));
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update relation group"));
				sb.append(new ButtonTag(RelationGroupHandler.class, "delete", "Delete relation group", "relationGroup", relationGroup)
						.setConfirmation("Are you sure you want to delete this relation group?"));
				sb.append(new ButtonTag(ItemTypeHandler.class, "edit", "Return to item type", "itemType", itemType));

				sb.append("</div>");
			}

			private void group() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Name", "Enter the name of this relation group.", new TextTag(context, "name", relationGroup.getName(), 50)));

				sb.append(new AdminRow("Reciprocal",
						"If a relations group is set to reciprocal then any items added as relations will be two-way relations.",
						new BooleanRadioTag(context, "reciprocal", relationGroup.isReciprocal())));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(RelationGroupHandler.class, "save", "post"));
				sb.append(new HiddenTag("relationGroup", relationGroup));

                commands();
				group();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (relationGroup == null) {
			return index();
		}

		relationGroup.setName(name);
		relationGroup.setReciprocal(reciprocal);
		relationGroup.save();

		return new ActionDoc(context, "This relation group has been updated", new Link(RelationGroupHandler.class, null, "relationGroup", relationGroup));
	}
}
