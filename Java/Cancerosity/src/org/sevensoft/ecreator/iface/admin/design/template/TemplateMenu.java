package org.sevensoft.ecreator.iface.admin.design.template;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Menu;
import org.sevensoft.ecreator.model.design.template.Template;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 16 Sep 2006 22:21:32
 *
 */
public class TemplateMenu extends Menu {

	private final Template	template;

	TemplateMenu(Template template) {
		this.template = template;
	}

	@Override
	public List<LinkTag> getLinkTags(RequestContext context) {

		List<LinkTag> links = new ArrayList();

		links.add(new LinkTag(TemplateHandler.class, "before", "Top", "template", template));
		links.add(new LinkTag(TemplateHandler.class, "after", "Bottom", "template", template));
		links.add(new LinkTag(TemplateHandler.class, "css", "Css", "template", template));
		links.add(new LinkTag(TemplateHandler.class, "head", "Head", "template", template));
		links.add(new LinkTag(TemplateHandler.class, "panels", "Panels", "template", template));
        links.add(new LinkTag(TemplateHandler.class, "headBackend", "Backend head", "template", template));

		return links;
	}

}
