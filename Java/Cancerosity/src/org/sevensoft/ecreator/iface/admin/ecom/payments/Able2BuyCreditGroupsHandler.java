package org.sevensoft.ecreator.iface.admin.ecom.payments;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.ecom.payments.forms.able2buy.CreditGroup;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 12 Dec 2006 13:40:23
 *
 */
@Path("admin-payments-able2buy-creditgroups.do")
public class Able2BuyCreditGroupsHandler extends AdminHandler {

	private String			creditGroupId;
	private CreditGroup		creditGroup;
	private String			description;
	private double			factor;
	private List<CreditGroup>	groups;
	private double			apr;
	private int				repaymentMonths;
	private int				deferredMonths;

	public Able2BuyCreditGroupsHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		if (creditGroupId != null) {
			new CreditGroup(context, creditGroupId);
		}

		return new ActionDoc(context, "A new credit group has been created", new Link(Able2BuyCreditGroupsHandler.class));
	}

	public Object delete() throws ServletException {

		if (creditGroup == null) {
			return main();
		}

		return new ActionDoc(context, "This credit group has been deleted", new Link(Able2BuyCreditGroupsHandler.class));
	}

	public Object edit() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Edit credit group", null);
		doc.addBody(new Body() {

            private void commands() {

                sb.append("<div class='actions'>"
                        + new SubmitTag("Update")
                        + " "
                        + new ButtonTag(Able2BuyCreditGroupsHandler.class, "delete", "Delete", "creditGroup", creditGroup)
                        .setConfirmation("Are you sure you want to delete this credit group?") + " "
                        + new ButtonTag(Able2BuyCreditGroupsHandler.class, null, "Return to menu") + "</div>");
            }

			@Override
			public String toString() {

				sb.append(new FormTag(Able2BuyCreditGroupsHandler.class, "save", "post"));
				sb.append(new HiddenTag("creditGroup", creditGroup));

                commands();

				sb.append(new AdminTable("Credit group details"));

				sb.append(new AdminRow("Credit group ID", "Enter the unique id provided for this credit group", new TextTag(context, "creditGroupId",
						creditGroup.getCreditGroupId(), 20)));

				sb.append(new AdminRow("Description", "Enter a description, for example, 'Buy Now Pay 12 months Later at 29.90% APR variable'",
						new TextTag(context, "description", creditGroup.getDescription(), 60)));

				sb.append(new AdminRow("Repayment months", "Enter the total number of monthly repayments.", new TextTag(context, "repaymentMonths",
						creditGroup.getRepaymentMonths(), 4)));

				sb.append(new AdminRow("Deferred months", "Enter the number of months before payments start", new TextTag(context, "deferredMonths",
						creditGroup.getDeferredMonths(), 4)));

				sb.append(new AdminRow("Apr", "Enter the APR (Annual percentage rate).", new TextTag(context, "apr", creditGroup.getApr(), 6)));

				sb.append(new AdminRow("Factor", "The factor used to calculate monthly repayments.", new TextTag(context, "factor", creditGroup
						.getFactor(), 6)));

				sb.append("</table>");

				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});

		return doc;
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Able2Buy credit groups", null);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Credit groups"));

				for (CreditGroup cg : CreditGroup.get(context)) {

					sb.append("<tr>");
					sb.append("<td>" + new LinkTag(Able2BuyCreditGroupsHandler.class, "edit", new SpannerGif(), "creditGroup", cg) + " "
							+ cg.getCreditGroupId() + "</td>");
					sb.append("<td>" + cg.getDescription() + "</td>");
					sb.append("<td width='10'>"
							+ new LinkTag(Able2BuyCreditGroupsHandler.class, "delete", new DeleteGif(), "creditGroup", cg)
									.setConfirmation("Are you sure you want to delete this credit group?") + "</td>");
					sb.append("</tr>");
				}

				sb.append(new FormTag(Able2BuyCreditGroupsHandler.class, "create", "post"));
				sb.append("<tr><td colspan='3'>Add new credit group id " + new TextTag(context, "creditGroupId", 12) + " " + new SubmitTag("Add")
						+ "</td></tr>");
				sb.append("</form>");

				sb.append("</table>");

				return sb.toString();
			}

		});

		return doc;
	}

	public Object save() throws ServletException {

		if (creditGroup == null) {
			return main();
		}

		test(new RequiredValidator(), "creditGroupId");
		if (context.hasErrors()) {
			return main();
		}

		creditGroup.setDescription(description);
		creditGroup.setApr(apr);
		creditGroup.setDeferredMonths(deferredMonths);
		creditGroup.setRepaymentMonths(repaymentMonths);
		creditGroup.setFactor(factor);
		creditGroup.setCreditGroupId(creditGroupId);
		creditGroup.save();

		return new ActionDoc(context, "This credit group has been updated", new Link(Able2BuyCreditGroupsHandler.class, "edit", "creditGroup", creditGroup));
	}

}
