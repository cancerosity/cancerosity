package org.sevensoft.ecreator.iface.admin.media.images;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.superstrings.comparators.NaturalStringComparator;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.results.docs.SimpleDoc;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

import javax.servlet.ServletException;
import java.io.File;
import java.util.Arrays;

/**
 * @author sks 07-Dec-2005 09:09:54
 */
@Path("admin-images-selection.do")
public class ImageSelectionHandler extends AdminHandler {

    private static int ResultsPerPage = 50;

    private String filename;
    private int page;
    private String phrase;
    private Img image;
    private String description;
    private String alt;
    private String thumbnailFilename;
    private String caption;

    public ImageSelectionHandler(RequestContext context) {
        super(context);
    }

    @Override
    public Object main() throws ServletException {

        final String[] filenames = Img.getImageFilenames(ResourcesUtils.getRealImagesDir(), phrase);
        Arrays.sort(filenames, NaturalStringComparator.instance);

        final Results results = new Results(filenames, page, ResultsPerPage);
        page = results.normalisePage(page);

        SimpleDoc doc = new SimpleDoc(context);
        doc.addHead("<style> * { font-size: 12px; font-family: Arial; } table.images { margin-top: 10px; width: 600px; border-collapse: collapse; } "
                + "table.images td { padding: 3px 6px; border: 1px solid #aaaaaa; } </style>");
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append("<style>");
                sb.append("table.pages { width: 98%; font-family: Arial; font-size: 12px; }");
                sb.append("table.pages td { padding: 2px 3px; }");
                sb.append("table.pages td.label { font-weight: bold; }");
                sb.append("table.pages td.current { width: 10px; background: #f9ed9d; color: #836679; border: 1px solid #d2d1c3; }");
                sb.append("table.pages td.link { width: 10px; background: white; color: black; border: 1px solid #999999; }");
                sb.append("table.pages td a { color: #000000; text-decoration: none; } ");
                sb.append("table.pages td.link_over { width: 10px; background: #f9ed9d; color: #836679; border: 1px solid black; }");

                sb.append("</style>");

                sb.append(new FormTag(ImageSelectionHandler.class, null, "get"));
                sb.append("<div>Filter by name: " + new TextTag(context, "phrase", 20) + " " + new SubmitTag("Update") + "</div>");
                sb.append("</form>");

                sb.append(new ResultsControl(context, results, new Link(ImageSelectionHandler.class)));

                //				// pages
                //				sb.append(new TableTag("pages", "center", 2, 0));
                //				sb.append("<tr><td align='right' class='label'>Page:</td>");
                //				for (int n = 1; n <= pages; n++) {
                //
                //					if (n == page) {
                //						sb.append("<td class='current' align='center'>");
                //						sb.append(n);
                //						sb.append("</td>");
                //
                //					} else {
                //
                //						sb
                //								.append("<td class='link' align='center' onmouseover='this.className=\"link_over\"' onmouseout='this.className=\"link\"'>");
                //						sb.append(new LinkTag(ImageSelectionHandler.class, null, String.valueOf(n), "item", item, "page", n));
                //						sb.append("</td>");
                //					}
                //
                //				}
                sb.append("</tr>");
                sb.append("</table>");

                sb.append(new FormTag(ImageSelectionHandler.class, "listImages", "GET"));

                sb.append(new TableTag("images"));

                int start = (page - 1) * ResultsPerPage;
                int end = page * ResultsPerPage;
                for (int n = start; n < end && n < filenames.length; n++) {

                    String filename = filenames[n];

//                    File file = new File(config.getRealImagesPath() + "/" + filename);
                    File file = ResourcesUtils.getRealImage(filename);

                    String javascript = "var e = window.opener.document.getElementById('imageFilename');" + "e.value='" + filename
                            + "'; e.form.submit(); window.close(); ";

                    sb.append("<tr>");
                    sb.append("<td width='30'>" + (n + 1) + "</td>");
                    sb.append("<td width='40' align='center'>"
                            + new LinkTag("#", new ImageTag(Config.ImagesPath + "/" + filename, 0, 30, 0)).setOnClick(javascript) + "</td>");
                    sb.append("<td>" + filename + "</td>");
                    sb.append("<td width='60'>" + file.length() / 1000 + "kb</td>");
                    sb.append("<td width='60'>" + new Date(file.lastModified()).toString("dd/MM/yyyy") + "</td>");
                    sb.append("<td width='60'>" + new LinkTag(Config.ImagesPath + "/" + filename, "Preview").setTarget("_blank") + "</td>");
                    sb.append("</tr>");
                }

                sb.append("</table>");
                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }
}
