package org.sevensoft.ecreator.iface.admin.categories;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.help.categories.*;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.comments.CommentHandler;
import org.sevensoft.ecreator.iface.admin.comments.AddCommentsPanel;
import org.sevensoft.ecreator.iface.admin.attachments.AdminAttachmentsPanel;
import org.sevensoft.ecreator.iface.admin.categories.menu.CategoryMenu;
import org.sevensoft.ecreator.iface.admin.containers.blocks.panels.BlocksListPanel;
import org.sevensoft.ecreator.iface.admin.containers.blocks.panels.GalleriesListPanel;
import org.sevensoft.ecreator.iface.admin.media.images.panels.ImageAdminPanel;
import org.sevensoft.ecreator.iface.admin.misc.content.panels.KeywordDensityPanel;
import org.sevensoft.ecreator.iface.admin.misc.seo.panels.MetaTagsPanel;
import org.sevensoft.ecreator.iface.admin.misc.seo.panels.SeoFriendlyUrlsRenderer;
import org.sevensoft.ecreator.iface.admin.search.SearchEditHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.frontend.ErrorDoc;
import org.sevensoft.ecreator.model.accounts.permissions.DomainUtil;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionCollections;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionsModule;
import org.sevensoft.ecreator.model.accounts.permissions.panels.PermissionsPanel;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.AttachmentUtil;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.OwnerAttributesPanel;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.Category.ForwardWho;
import org.sevensoft.ecreator.model.categories.Category.SubcategoryOrdering;
import org.sevensoft.ecreator.model.categories.CategoryModule;
import org.sevensoft.ecreator.model.categories.CategorySettings;
import org.sevensoft.ecreator.model.categories.CategoryItem;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.containers.blocks.shortcuts.BlockShortcut;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.styles.Style;
import org.sevensoft.ecreator.model.design.template.Template;
import org.sevensoft.ecreator.model.design.template.TemplateConfig;
import org.sevensoft.ecreator.model.feeds.Feed;
import org.sevensoft.ecreator.model.feeds.bespoke.prices.kelkoo.KelkooCategories;
import org.sevensoft.ecreator.model.feeds.bespoke.prices.shopzilla.ShopzillaCategories;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.pricing.PriceBand;
import org.sevensoft.ecreator.model.items.pricing.PriceUtil;
import org.sevensoft.ecreator.model.items.pricing.panels.EditPricesRenderer;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.ImageUtil;
import org.sevensoft.ecreator.model.media.images.galleries.Gallery;
import org.sevensoft.ecreator.model.media.videos.VideoUtil;
import org.sevensoft.ecreator.model.media.videos.panels.VideoAdminPanel;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.misc.content.ContentBlock;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.users.privileges.Privilege;
import org.sevensoft.ecreator.model.comments.Comment;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.*;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.bool.RadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

import javax.servlet.ServletException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.IOException;

/**
 * @author sks
 */
@Path("admin-categories.do")
public class CategoryHandler extends AdminHandler {

    private static final int CategoryListLimit = 1000;
    private String names, footer, header, descriptionTag, keywords, content, titleTag, name, contentLinkingKeywords;
    private boolean autoContentLinking;
    private boolean contentLinkingKeyword;
    private Attachment file;
    private List<Upload> files;
    private Map<Integer, String> prices;
    private Category subcategory, parent;
    private SubcategoryOrdering subcategoryOrdering;
    private String forwardUrl;
    private String noResultsMessage;
    private boolean contents;
    private Category category;
    private String kelkooCategory;
    private Map<Attribute, String> attributeValues;
    private boolean hidden;
    private boolean overrideStyles;
    private int itemsStyle;
    private int itemDetailsStyle;
    private boolean memberGroupPricing;
    private String shopzillaCategory;
    private String newAttributes;
    private List<String> imageUrls;
    private List<Upload> imageUploads;
    private String summary;
    private boolean hideSubcategories;
    private boolean hidePageTitle;
    private MultiValueMap<String, PermissionType> permissions;
    private Template template;
    private Map<Integer, String> attachmentDescriptions;
    private boolean renderTags;
    private String css;
    private Gallery gallery;
    private Map<Integer, String> imageDescriptions;
    private Form addForm;
    private boolean useDefaultHighlightedItemBlocks;
    private boolean useDefaultHighlightedMembersBlocks;
    private Category moveChildrenTo;
    private Class<Block> blockClass;
    private String priceBreaks;
    private String body;
    private String searchEngineFriendlyUrl;
    private boolean simpleEditor;
    private String start;
    private String end;
    private int tds;
    private String between;
    private boolean multiBuy;
    private transient LinkTag imagesMenu;
    private boolean contentFirstPageOnly;
    private String addBlock;
    private Gallery addGallery;
    private transient LinkTag SearchWrapperMenu;
    private boolean searchWrapper;
    private Category searchCategory;
    private ItemType itemType;
    private int distance;
    private String location;
    private Markup resultsMarkup;
    private transient CategorySettings categorySettings;
    private transient MiscSettings miscSettings;
    private Markup listProfileMarkup;
    private boolean makePreviews;
    private Category mergeInto;
    private int age;
    private boolean subcats;
    private int limit;
    private transient Seo seo;
    private ForwardWho forwardWho;
    private Markup subcategoryMarkup;
    private MultiValueMap<Integer, String> priceMap;
    private Map<Integer, Money> costPriceToMap;
    private Map<Integer, Money> costPriceFromMap;
    private Map<Integer, PriceBand> memberGroupMap;
    private int priceRows;
    private String priceBreaksString;
    private String costConstraintsString;
    private Category navigation;
    private BlockShortcut addBlockShortcut;
    private int page;
    private String newBlock;

    private String friendlyUrl;
    private String suggestions;
    private boolean enablePermissions;
    private String restrictionText;
    private boolean noGuestLogin;
    private String restrictionForwardUrl;
    private String restrictionForward;
    private String allowOnlyReferrer;
    private Item accountAllowed;
    private boolean categoryInheritance;
    private boolean itemInheritance;
    private String newAttributesSection;
    private int newAttributesPage;
    private boolean forum;
    private String imageFilename;
    private boolean supermanLock;
    private List<Upload> videoUploads;
    private boolean confirmed;
    private Style style;
    private boolean useManualSort;
    private String comment;
    private String title;
    private Amount discount;
    private boolean hideDiscountInfo;

    public CategoryHandler(RequestContext context) {
        this(context, null);
    }

    public CategoryHandler(RequestContext context, Category category) {
        super(context);
        this.category = category;
        this.miscSettings = MiscSettings.getInstance(context);
        this.categorySettings = CategorySettings.getInstance(context);
        this.seo = Seo.getInstance(context);
    }

    private Object advCreate(AdminDoc doc) {

        int count = Category.getCount(context);
        final Results results = new Results(count, page, 500);

        logger.fine("[CategoryHandler] results=" + results);

        final Map<String, String> categoryOptions = Category.getCategoryOptions(context, null, results.getStartIndex(), 500);
        doc.setMenu(new CategoryMenu());
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new FormTag(CategoryHandler.class, "doCreate", "POST"));
                sb.append(new AdminTable("New category details"));

                sb.append(new AdminRow(captions.getCategoriesCaption() + " names", "Enter each new category on a new line", new TextAreaTag(context,
                        "names", 60, 4)));

                StringBuilder sb2 = new StringBuilder();
                if (results.hasMultiplePages()) {

                    Link link = new Link(CategoryHandler.class, "create");

                    sb2.append(new ResultsControl(context, results, link));
                    sb2.append("<br/><br/>");
                }

                sb2.append(new SelectTag(context, "parent", null, categoryOptions, "-I'll decide later"));

                sb.append(new AdminRow("Position", "Set where these new " + captions.getCategoriesCaptionPlural().toLowerCase() + " should be created",
                        sb2));

                sb.append("</table>");
                sb.append("<div align='center' class='actions'>" +
                        new SubmitTag("create", "Create " + captions.getCategoriesCaptionPlural().toLowerCase()) + "</div>");

                sb.append("</form>");
                return sb.toString();
            }

        });
        return doc;
    }

    /**
     * Simple create
     */
    public Object create() {

        AdminDoc doc = new AdminDoc(context, user, "Create a " + captions.getCategoriesCaption().toLowerCase(), Tab.Categories);
        doc.setIntro("Create a new category and choose where it is positioned");

        if (miscSettings.isAdvancedMode()) {

            return advCreate(doc);

        } else {

            return simpleCreate(doc);
        }
    }

    public Object css() throws ServletException {

        if (category == null)
            return list();

        setView();

        AdminDoc doc = new AdminDoc(context, user, getTitle(), Tab.Categories);
        doc.setIntro("Edit category CSS");
        doc.setMenu(new CategoryMenu(category));
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update " + captions.getCategoriesCaption().toLowerCase()));
                sb.append("</div>");
            }

            private void css() {

                sb.append(new AdminTable(captions.getCategoriesCaption().toLowerCase() + " CSS"));
                sb.append("<tr><td>" + new TextAreaTag(context, "css", category.getCss(), 140, 15) + "</td></tr>");
                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(CategoryHandler.class, "saveCss", "post"));
                sb.append(new HiddenTag("category", category));

                commands();
                css();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object delete() throws ServletException {

        if (category == null) {
            return main();
        }

        try {

            category.log(user, "Deleted");
            category.delete(contents);

        } catch (RuntimeException e) {
            e.printStackTrace();
            return main();
        }

        AdminDoc doc = new AdminDoc(context, user, "This category has been deleted", Tab.Categories);
        doc.setMenu(new CategoryMenu());
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new TableTag("simplelinks", "center"));
                sb.append("<tr><th align='center'>What would you like to do ?</th></tr>");

                if (Privilege.EditCategory.yes(user, context))
                    sb.append("<tr><td align='center'>" + new LinkTag(CategoryHandler.class, "list", "I want to edit another category") +
                            "</td></tr>");

                if (Privilege.CreateCategory.yes(user, context))
                    sb.append("<tr><td align='center'>" + new LinkTag(CategoryHandler.class, "create", "I want to create a new category") +
                            "</td></tr>");

                sb.append("</table>");

                return sb.toString();
            }
        });
        return doc;
    }

    public Object doCreate() throws ServletException {

        if (names == null) {
            return create();
        }

        // the first category cannot begin with a plus
        if (names.startsWith("+")) {
            return create();
        }

        Map<Integer, Category> parentsMap = new HashMap();
        parentsMap.put(-1, parent);
        for (String name : names.split("\n")) {

            name = name.trim();
            if (name.length() > 0) {

                logger.fine("[CategoryHandler] creating category name=" + name);

                // if the name begins with a plus then get the parent associated with that
                int level = 0;
                while (name.startsWith("+")) {
                    name = name.substring(1);
                    level++;
                }

                logger.fine("[CategoryHandler] level=" + level);
                Category p = parentsMap.get(level - 1);

                logger.fine("[CategoryHandler] parent for this level=" + p);
                category = new Category(context, name, p);
                category.log(user, "Created");

                // update parents map with this category
                parentsMap.put(level, category);
            }
        }

        if (miscSettings.isAdvancedMode()) {
            return edit();
        }

        AdminDoc doc = new AdminDoc(context, user, "Category '" + category.getName() + "' has been created", Tab.Categories);
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new TableTag("simplelinks", "center"));
                sb.append("<tr><th align='center'>What would you like to do ?</th></tr>");

                if (Privilege.EditCategory.yes(user, context)) {

                    sb.append("<tr><td align='center'>" +
                            new LinkTag(CategoryHandler.class, "edit", "I want to edit this " + captions.getCategoriesCaption().toLowerCase(),
                                    "category", category) + "</td></tr>");

                    sb.append("<tr><td align='center'>" +
                            new LinkTag(CategoryHandler.class, "list", "I want to edit a different " +
                                    captions.getCategoriesCaption().toLowerCase()) + "</td></tr>");
                }

                if (Privilege.CreateCategory.yes(user, context)) {
                    sb.append("<tr><td align='center'>" +
                            new LinkTag(CategoryHandler.class, "create", "I want to create a new " +
                                    captions.getCategoriesCaption().toLowerCase()) + "</td></tr>");
                }

                sb.append("</table>");

                return sb.toString();
            }
        });
        return doc;
    }

    public Object edit() throws ServletException {

        if (category == null) {
            return list();
        }

        setView();

        int count = SimpleQuery.count(context, Category.class);
        final Results results = new Results(count, page, 500);
        final Map<String, String> possibleParentsMap = Category.getCategoryOptions(context, null, results.getStartIndex(), 500);
        logger.fine("[CategoryHandler] results=" + results);

        AdminDoc doc = new AdminDoc(context, user, getTitle(), Tab.Categories);
        doc.setIntro(captions.getCategoriesCaption() + " created on " + category.getDateCreated().toString("dd-MMM-yyyy HH:mm"));
        if (miscSettings.isAdvancedMode()) {
            doc.setMenu(new CategoryMenu(category));
        }
        doc.addBody(new Body() {

            private void attachments() {

                if (category.hasForwardUrl()) {
                    return;
                }

                sb.append(new AdminAttachmentsPanel(context, category, true));
            }

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update " + captions.getCategoriesCaption().toLowerCase()));
                sb.append(new ButtonTag(CategoryContentsHandler.class, null, "Edit " + captions.getCategoriesCaption().toLowerCase() + " contents",
                        "category", category));

                if (category.isLeaf() || isSuperman()) {

                    if (category.hasChildren()) {

                        sb.append(new ButtonTag(CategoryHandler.class, "delete", "Delete " + captions.getCategoriesCaption().toLowerCase(),
                                "category", category).setConfirmation("THIS WILL DELETE ALL SUBCATEGORIES - PLEASE CONFIRM?"));

                    } else {

                        sb.append(new ButtonTag(CategoryHandler.class, "delete", "Delete " + captions.getCategoriesCaption().toLowerCase(),
                                "category", category).setConfirmation("Are you sure you want to delete this category?"));
                    }

                    if (isSuperman()) {
                        sb.append(new ButtonTag(CategoryHandler.class, "delete", "Delete " + captions.getCategoriesCaption().toLowerCase() +
                                " and contents", "category", category, "contents", true)
                                .setConfirmation("Are you sure you want to delete this " + captions.getCategoriesCaption().toLowerCase() +
                                " and set all contents to deleted?"));
                    }

                }

                sb.append(new ButtonTag(CategoryHandler.class, null, "Return to " + captions.getCategoriesCaption().toLowerCase() + " menu"));

                sb.append("</div>");
            }

            private void content() {

                if (category.hasForwardUrl()) {

                    logger.fine("[CategoryHandler] not showing content, category has forward url");
                    return;
                }

                List<ContentBlock> contentBlocks = category.getContentBlocks();
                logger.fine("[CategoryHandler] content blocks=" + contentBlocks);
                if (contentBlocks.size() == 1) {

                    ContentBlock cb = contentBlocks.get(0);

                    logger.fine("[CategoryHandler] showing content block 1=" + cb);

                    sb.append(new AdminTable("Content"));

                    if (category.isSupermanLock()) {
                        sb.append("<tr><td style='background: #00aa00' colspan='2'>");
                    } else {
                        sb.append("<tr><td colspan='2'>");
                    }
                    sb.append(new TextAreaTag(context, "content", cb.getContent()).setId("content").setStyle("width: 100%; height: 320px;"));
                    sb.append("</td></tr>");

//                      doesn't work properly
//                    sb.append(new AdminRow("Style", "Choose a style to layout your content automatically (optional).", new SelectTag(context,
//                            "style", cb.getStyle(), Style.values(), "-Choose style-")));

                    sb.append("</table>");

                    if (cb.hasContent() && seo.isKeywordDensity()) {
                        sb.append(new KeywordDensityPanel(context, cb.getContent()));
                    }

                }
            }

            private void general() {

                sb.append(new AdminTable("General"));

                sb.append(new AdminRow("Name", "Enter the name of your " + captions.getCategoriesCaption().toLowerCase(), new TextTag(context, "name",
                        category.getName(), 60)));

                if (isSuperman()) {
                    sb.append(new AdminRow(true, "Superman lock", "Lock down this " + captions.getCategoriesCaption().toLowerCase() +
                            " so it can only be edited by supermen users.", new BooleanRadioTag(context, "supermanLock", category
                            .isSupermanLock())).setHelp(new SupermanLockHelp()));
                }

                // show parents if not root
                if (!category.isRoot()) {

                    StringBuilder sb2 = new StringBuilder();
                    if (results.hasMultiplePages()) {

                        sb2.append(category.getParent().getFullName(" > "));
                        sb2.append("<br/><br/>");
                        sb2.append(new ResultsControl(context, results, new Link(CategoryHandler.class, "edit", "category", category)));

                        SelectTag selectTag = new SelectTag(context, "parent", category.getParent(), possibleParentsMap, "-Select new position-");
                        sb2.append(selectTag);

                    } else {

                        SelectTag selectTag = new SelectTag(context, "parent", category.getParent(), possibleParentsMap, null);
                        sb2.append(selectTag);

                    }

                    sb.append(new AdminRow("Position", "Select where you want this " + captions.getCategoriesCaption().toLowerCase() + " to appear.",
                            sb2));
                }

                // order of subcategories
                if (categorySettings.isSubcategoryOrdering() && category.hasChildren()) {

                    sb.append("<tr><td width='240'><b>Subcategory ordering</b><br/>"
                            + "Choose how you want the subcategories to be ordered.</td><td>");

                    sb.append("Order alphabetically ");

                    sb.append(new RadioTag(context, "subcategoryOrdering", SubcategoryOrdering.Alphabetical,
                            category.getSubcategoryOrdering() == SubcategoryOrdering.Alphabetical) +
                            "<br/>");

                    sb.append("Let me choose");
                    sb.append(new RadioTag(context, "subcategoryOrdering", SubcategoryOrdering.Manual,
                            category.getSubcategoryOrdering() == SubcategoryOrdering.Manual) +
                            "<br/><br/>");

                    if (category.getSubcategoryOrdering() == SubcategoryOrdering.Manual) {
                        sb.append(new ButtonTag(CategoryReorderHandler.class, null, "Reorder subcategories", "category", category));
                    }

                    sb.append("</td></tr>");
                }

//				if (seo.isSuggestions()) {
//					sb.append(new AdminRow("Suggestions", "Enter extra words to show this " + captions.getCategoriesCaption().toLowerCase() +
//							" in content linking.",
//							new TextTag(context, "suggestions", StringHelper.implode(category.getSuggestions(), ", "), 40)));
//				}

                if (Module.Forum.enabled(context)) {
                    sb.append(new AdminRow("Forum", "Make this " + captions.getCategoriesCaption().toLowerCase() + " a link to the forum.",
                            new BooleanRadioTag(context, "forum", category.isForum())).setHelp(new ForumHelp()));
                }

                if (CategoryModule.Forwards.enabled(context)) {

                    sb.append(new AdminRow("Forward url", "Enter an url you wish this " + captions.getCategoriesCaption().toLowerCase() +
                            " to forward to.", new TextTag(context, "forwardUrl", category.getForwardUrl(), 40)));

                    if (category.hasForwardUrl()) {

                        sb.append(new AdminRow("Forward who", "Who should be targetted by this forward.", new SelectTag(context, "forwardWho",
                                category.getForwardWho(), ForwardWho.values(), null)));
                    }
                }

                if (CategoryModule.Summaries.enabled(context)) {

                    sb.append(new AdminRow("Summary", "Enter a few lines summarising this " + captions.getCategoriesCaption().toLowerCase() +
                            " for use when blurb is required.", new TextAreaTag(context, "summary", category.getSummary(), 60, 3)));

                }

                if (miscSettings.isAdvancedMode()) {

                    if (category.isChild() && categorySettings.isHidden()) {

                        sb.append(new AdminRow("Hidden", "Stops this category from appearing in the " +
                                captions.getCategoriesCaption().toLowerCase() + " lists on the front end of the site.", new BooleanRadioTag(
                                context, "hidden", category.isHidden())).setHelp(new HiddenHelp()));

                        if (category.isHidden()) {

                            sb.append(new AdminRow("URL to category", "Copy this url if you want to link to this hidden category", new LinkTag(
                                    category.getUrl())));

                        }

                    }

                    sb
                            .append(new AdminRow(
                                    "Simple editor",
                                    "Enable a plain text box rather than the HTML editor. Useful if you want to paste in javascript code or complicated html from another source.",
                                    new BooleanRadioTag(context, "simpleEditor", category.isSimpleEditor())).setHelp(new SimpleEditorHelp()));

                    /*
                          * If the kelkoo feed is enabled for this site then show the kelkoo category options
                          */
                    if (Feed.isKelkoo(context)) {

                        SelectTag kelkooTag = new SelectTag(context, "kelkooCategory", category.getKelkooCategory());
                        kelkooTag.setAny("-None-");
                        kelkooTag.addOptions(KelkooCategories.getCategories());

                        sb.append(new AdminRow("Kelkoo category", "Select the kelkoo category products in this category should be placed in.",
                                kelkooTag));

                    }

                    /*
                          * Shopzilla categories if shopzilla feed is enabled
                          */
                    if (Feed.isShopzilla(context)) {

                        SelectTag shopzillaTag = new SelectTag(null, "shopzillaCategory", category.getShopzillaCategory());
                        shopzillaTag.setAny("-None-");
                        shopzillaTag.addOptions(ShopzillaCategories.get());

                        sb.append(new AdminRow("Shopzilla category", "Select the shopzilla category this product should be placed in.",
                                shopzillaTag));

                    }

                    if (!category.hasForwardUrl()) {

                        // auto content linking overrides
                        if (seo.isAutoContentLinkingOverride()) {

                            sb.append(new AdminRow("Auto content linking", "Enable or disable auto content linking in this " +
                                    captions.getCategoriesCaption().toLowerCase() + ".", new BooleanRadioTag(context, "autoContentLinking",
                                    category.isAutoContentLinking())));

                            sb.append(new AdminRow("Auto content linking keyword", "Include this " +
                                    captions.getCategoriesCaption().toLowerCase() + " as a keyword for auto content linking.",
                                    new BooleanRadioTag(context, "contentLinkingKeyword", category.isContentLinkingKeyword())));
                        }

                        sb.append(new AdminRow("Auto content linking keywords","Other words that are aasociated with that category for auto content linking",
                                new TextTag(context, "contentLinkingKeywords", StringHelper.implode(category.getContentLinkingKeywords(),","), 60)));

                    }
                    if (isSuperman()) {
                        sb.append(new AdminRow(true, "Merge into",
                                "This category will be removed, and all its contents and subcategories will be moved into the " +
                                        captions.getCategoriesCaption().toLowerCase() + " selected.", new SelectTag(context, "mergeInto", null,
                                possibleParentsMap, "-Choose category to merge into")).setHelp(new MergeIntoHelp()));

                        SelectTag tag = new SelectTag(context, "moveChildrenTo");
                        tag.setAny("-Select new parent-");
                        tag.addOptions(possibleParentsMap);

                        sb.append(new AdminRow("Move subcategories", "Select a new " + captions.getCategoriesCaption().toLowerCase() +
                                " to move all subcategories of this " + captions.getCategoriesCaption().toLowerCase() + " into.", tag)
                                .setHelp(new MoveSubcategoriesHelp()));
                    }

                }

                if (Module.ManualSort.enabled(context)) {
                    sb.append(new AdminRow("Use manual sort", "Enable using manual sort", new BooleanRadioTag(context,
                            "useManualSort", category.useManualSort())));
                }

                sb.append("</table>");
            }

            private void comments() {

//                sb.append(new AdminTable("Comments"));
//                sb.append(new AdminRow("Add comments", "Allow to leave comments to the category",
//                        new BooleanRadioTag(context, "comments", category.isComments())));
            }

            private void template() {

                List<Template> templates = Template.get(context);
                if (templates.isEmpty()) {
                    return;
                }

                TemplateConfig config = category.getTemplateConfig();

                sb.append(new AdminTable("Template"));

                SelectTag templateTag = new SelectTag(context, "template");
                if (config != null)
                    templateTag.setValue(config.getTemplate());
                templateTag.setAny("-Default-");
                templateTag.addOptions(templates);

                sb.append(new AdminRow(true, "Template", "Override the normal site template with a template for this category.", templateTag));

                if (config != null) {
                    sb.append(new AdminRow(true, "Category inheritance", "Apply this template to subcategories of this category.", new BooleanRadioTag(
                            context, "categoryInheritance", config.isCategoryInheritance())));

                    sb.append(new AdminRow(true, "Item inheritance", "Apply this template to items in this category.", new BooleanRadioTag(context,
                            "itemInheritance", config.isItemInheritance())));
                }

                sb.append(new AdminRow(true, "Render tags", "Enable rendering of markup tags in this " + captions.getCategoriesCaption().toLowerCase(),
                        new BooleanRadioTag(context, "renderTags", category.isRenderTags())));

                sb.append("</table>");
            }

            @Override
            public String toString() {

                if (category.isAdvancedEditor() && miscSettings.isHtmlEditor()) {
                    RendererUtil.tinyMce(context, sb, "content");
                }

                sb.append(new FormTag(CategoryHandler.class, "save", "multi"));
                sb.append(new HiddenTag("category", category));

                commands();

                general();

                if (miscSettings.isAdvancedMode()) {

                    if (Module.CategoryAttributes.enabled(context)) {
                        logger.fine("[CategoryHandler] Category attributes enabled");
                        sb.append(new OwnerAttributesPanel(context, category));
                    }

                    if (Module.Permissions.enabled(context)) {
                        logger.fine("[CategoryHandler] Permissions enabled");
                        sb.append(new PermissionsPanel(context, category, PermissionCollections.getCategory(context)));
                    }
                }

                if (seo.isOverrideTags()) {
                    logger.fine("[CategoryHandler] seo tags overriden");
                    sb.append(new MetaTagsPanel(context, category));
                }

                if (seo.isOverrideFriendlyUrls()) {
                    logger.fine("[CategoryHandler] overridding friendly urls");
                    sb.append(new SeoFriendlyUrlsRenderer(context, category));
                }

                content();

                if (CategoryModule.Attachments.enabled(context)) {
                    attachments();
                }

                sb.append(new BlocksListPanel(context, category));
                sb.append(new GalleriesListPanel(context, category));

                if (!category.isRoot() && CategoryModule.Images.enabled(context)) {
                    sb.append(new ImageAdminPanel(context, category));
                }

                if (Module.Videos.enabled(context)) {
                    if (CategoryModule.Videos.enabled(context)) {
                        sb.append(new VideoAdminPanel(context, category));
                    }
                }

                if (Module.Comments.enabled(context)){
                    comments();
                }

                if (isSuperman() || Module.UserMarkup.enabled(context)) {
                    template();
                }

                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    private String getTitle() {
        return "Edit category #" + category.getIdString() + " - " + category.getName();
    }

    public Object headers() throws ServletException {

        if (category == null)
            return main();

        setView();

        AdminDoc doc = new AdminDoc(context, user, "Edit category: Headers and footers", Tab.Categories);
        doc.setMenu(new CategoryMenu(category));
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update category"));
                sb.append(new ButtonTag(CategoryHandler.class, null, "Return to category menu"));

                sb.append("</div>");
            }

            private void headers() {

                sb.append(new AdminTable("Headers and footers"));

                sb.append(new AdminRow("Header", null, new TextAreaTag(context, "header", category.getHeader(), 70, 7)));
                sb.append(new AdminRow("Footer", null, new TextAreaTag(context, "footer", category.getFooter(), 70, 7)));

                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(CategoryHandler.class, "saveHeaders", "post"));
                sb.append(new HiddenTag("category", category));

                headers();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    /**
     * Advanced items control
     */
    public Object list() {

        final int count;
        if (name == null) {
            count = Category.getCount(context);
        } else {
            count = SimpleQuery.count(context, Category.class, "name", name);
        }

        final Results results = new Results(count, page, CategoryListLimit);
        final Map<String, String> categoryOptions = Category.getCategoryOptions(context, name, results.getStartIndex(), CategoryListLimit);

        AdminDoc doc = new AdminDoc(context, user, "Choose " + captions.getCategoriesCaption().toLowerCase() + " to edit", Tab.Categories);
        if (miscSettings.isAdvancedMode()) {
            doc.setMenu(new CategoryMenu());
        }
        doc.addBody(new Body() {

            @Override
            public String toString() {

                if (categorySettings.isCategoryMenuFlat()) {

                    sb.append(new TableTag("form", "center").setCaption(captions.getCategoriesCaptionPlural()));
                    for (Category category : Category.get(context)) {

                        sb.append("<tr><td>" + new LinkTag(CategoryHandler.class, "edit", category.getFullName(" > "), "category", category) +
                                "</td></tr>");

                    }
                    sb.append("</table>");

                } else {

                    if (results.hasMultiplePages()) {
                        sb.append("<div align='center'>" +
                                new ResultsControl(context, results, new Link(CategoryHandler.class, "list", "name", name)) + "</div>");
                    }

                    if (count > CategoryListLimit) {

                        sb.append(new FormTag(CategoryHandler.class, "list", "GET"));
                        sb.append("<div align='center'>Filter by name: " + new TextTag(context, "name", 20) + " " + new SubmitTag("Update") +
                                "</div><br/>");
                        sb.append("</form>");

                    }

                    sb.append(new FormTag(CategoryHandler.class, "edit", "GET"));

                    sb.append("<div align='center'>" + new SelectTag(context, "category", null, categoryOptions, "-Choose category-") + "</div>");
                    sb.append("<br/><br/>");
                    sb.append("<div align='center' class='actions'>" + new SubmitTag("Edit this category") + "</div>");

                    sb.append("</form>");

                }

                return sb.toString();
            }

        });
        return doc;
    }

    @Override
    public Object main() throws ServletException {

        if (miscSettings.isAdvancedMode()) {
            return list();
        }

        AdminDoc doc = new AdminDoc(context, user, "Categories", Tab.Categories);
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new TableTag("simplelinks", "center"));
                sb.append("<tr><th align='center'>What would you like to do ?</th></tr>");

                if (Privilege.CreateCategory.yes(user, context)) {
                    sb.append("<tr><td align='center'>" + new LinkTag(CategoryHandler.class, "create", "I want to create a category") + "</td></tr>");
                }
                if (Privilege.EditCategory.yes(user, context)) {
                    sb.append("<tr><td align='center'>" + new LinkTag(CategoryHandler.class, "list", "I want to edit an existing category") +
                            "</td></tr>");
                }

                sb.append("</table>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object moveSubcategoryDown() throws ServletException {

        if (category == null || subcategory == null)
            return edit();

        EntityUtil.moveDown(subcategory, category.getChildren());
        return edit();
    }

    public Object moveSubcategoryUp() throws ServletException {

        if (category == null || subcategory == null)
            return edit();

        EntityUtil.moveUp(subcategory, category.getChildren());
        return edit();
    }

    public Object pricing() throws ServletException {

        if (category == null)
            return main();

        setView();

        AdminDoc doc = new AdminDoc(context, user, "Edit category: Pricing", Tab.Categories);
        doc.setMenu(new CategoryMenu(category));
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update"));

                if (category.isLeaf() || isSuperman()) {
                    sb.append(new ButtonTag(CategoryHandler.class, "delete", "Delete category", "category", category)
                            .setConfirmation("Are you sure you want to delete this category?"));

                    if (isSuperman()) {
                        sb.append(new ButtonTag(CategoryHandler.class, "delete", "Delete category and contents", "category", category, "contents",
                                true).setConfirmation("Are you sure you want to delete this category and set all contents to deleted?"));
                    }
                }

                sb.append("</div>");
            }

            private void pricing() {

                if (category.hasForwardUrl()) {
                    return;
                }

                if (category.isRoot()) {
                    return;
                }

                sb.append(new AdminTable("Discount"));
                if (Module.CategoryAndItemDiscount.enabled(context)) {
                    sb.append(new AdminRow("Category discount", "Enter a discount here and it will be applied to products without their own discount",
                            new TextTag(context, "discount", category.getDiscount(), 8).toString() + "&nbsp;&nbsp;&nbsp;&nbsp;" +
                            new ButtonTag(CategoryHandler.class, "applyDiscount", "Apply Discount", "category", category)));

                    sb.append(new AdminRow("Hide discount info", "Hide  discount info from frontend. To this info apply a [percentage_discount] marker and set the option to Yes",
                            new BooleanRadioTag(context, "hideDiscountInfo", category.isHideDiscountInfo())));
                }
                sb.append("</table>");

                sb.append(new EditPricesRenderer(context, category));
            }

            @Override
            public String toString() {

                sb.append(new FormTag(CategoryHandler.class, "savePricing", "post"));
                sb.append(new HiddenTag("category", category));

                commands();
                pricing();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object removeFile() throws ServletException {

        if (file == null || category == null) {
            return edit();
        }

        category.removeAttachment(file);
        return edit();
    }

    public Object save() throws ServletException {

        if (category == null) {
            return edit();
        }

        if (category.isSupermanLock()) {
            if (!isSuperman()) {
                return new ActionDoc(context, "This category cannot be updated", new Link(CategoryHandler.class, "edit", "category", category));
            }
        }

        test(new RequiredValidator(), "name");
        if (hasErrors()) {
            return edit();
        }

        if (!ObjectUtil.equal(category.getName(), name)) {
            category.setName(name);
            category.log(user, "Name changed to " + category.getName());
        }

        if (isSuperman() || Module.UserMarkup.enabled(context)) {

            category.setRenderTags(renderTags);
            category.setSimpleEditor(simpleEditor);
            
//            if (template != null) {

            TemplateConfig config = category.getTemplateConfig();

            if (config == null && template != null) {
                config = TemplateConfig.create(context, category);
            }

            if (config != null) {
                if (!ObjectUtil.equal(config.getTemplate(), template)) {

                    config.setTemplate(template);
                    if (template == null) {
                        config.delete();
                        config = null;
                        category.log(user, "Template removed");
                    } else {
                        category.log(user, "Template changed to '" + template.getName() + "' #" + template.getId());
                    }
                }

                if (config != null) {
                    config.setCategoryInheritance(categoryInheritance);
                    config.setItemInheritance(itemInheritance);
                    config.save();
                }
            }
        }
//        }

        category.setSummary(summary);

        if (seo.isOverrideTags()) {

            if (!ObjectUtil.equal(category.getTitleTag(), titleTag)) {
                category.setTitleTag(titleTag);
                category.log(user, "Title tag changed - " + titleTag);
            }

            if (!ObjectUtil.equal(category.getKeywords(), keywords)) {
                category.setKeywords(keywords);
                category.log(user, "Keywords changed - " + keywords);
            }

            if (!ObjectUtil.equal(category.getDescriptionTag(), descriptionTag)) {
                category.setDescriptionTag(descriptionTag);
                category.log(user, "Description tag changed - " + descriptionTag);
            }

        }

        category.setContentLinkingKeywords(StringHelper.explodeStrings(contentLinkingKeywords, ","));

        if (seo.isOverrideFriendlyUrls()) {
            category.setFriendlyUrl(friendlyUrl);
        }

        if (seo.isSuggestions()) {
            category.setSuggestions(StringHelper.explodeStrings(suggestions, ","));
        }

        if (addBlockShortcut != null) {
            addBlockShortcut.install(context, category);
        }

        category.addBlock(addBlock);

        if (addGallery != null) {
            category.addGalleryBlock(addGallery);
        }

        if (isSuperman()) {
            category.setSupermanLock(supermanLock);
        }

        // only update content if a single content block

        List<ContentBlock> contentBlocks = category.getContentBlocks();
        if (contentBlocks.size() == 1) {

            contentBlocks.get(0).setContent(content);
            contentBlocks.get(0).setStyle(style);
            contentBlocks.get(0).save();
        }

        // feeds
        if (!ObjectUtil.equal(category.getKelkooCategory(), kelkooCategory)) {
            category.setKelkooCategory(kelkooCategory);
            category.log(user, "Kelkoo category changed to '" + kelkooCategory + "'");
        }

        // feeds
        if (!ObjectUtil.equal(category.getShopzillaCategory(), shopzillaCategory)) {
            category.setShopzillaCategory(shopzillaCategory);
            category.log(user, "Shopzilla category changed to '" + shopzillaCategory + "'");
        }

        // forum
        category.setForum(forum);

        if (subcategoryOrdering != category.getSubcategoryOrdering()) {
            if (category.setSubcategoryOrdering(subcategoryOrdering)) {
                category.log(user, "Subcategory ordering changed to " + subcategoryOrdering);
            }
        }

        // parent
        if (parent != null) {
            logger.fine("[CategoryHandler] setting new parent to " + parent);
            if (category.setParent(parent)) {
                category.log(user, "Parent category changed to category #" + parent.getIdString() + " '" + parent.getName() + "'");
            }
        }

        // forwarding
        category.setForwardUrl(forwardUrl, user);
        category.setForwardWho(forwardWho);

        if (miscSettings.isAdvancedMode()) {
            if (category.isHidden() != hidden) {
                category.setHidden(hidden);
                category.log(user, "Category hidden set to " + hidden);
            }
        }

        // content linking
        if (seo.isAutoContentLinkingOverride()) {

            if (category.isAutoContentLinking() != autoContentLinking) {
                category.setAutoContentLinking(autoContentLinking);
                category.log(user, "Auto content linking set to " + autoContentLinking);
            }
            category.setContentLinkingKeyword(contentLinkingKeyword);
        }

        // attributes
        if (newAttributes != null) {
            category.addAttributes(newAttributes, newAttributesSection, newAttributesPage);
        }

        // images
        if (CategoryModule.Images.enabled(context)) {

            try {
                ImageUtil.save(context, category, imageUploads, imageUrls, imageFilename, true);
            } catch (ImageLimitException e) {
                addError(e);
            } catch (IOException e) {
                return new ErrorDoc(context, "The image you are attempting to upload is in CYMK colour mode. " +
                    "This is unsopported currently, please use a photo editing application (e.g. Photoshop) to convert the file to RGB colour. For more information or assistance please contact support.");
            }

        }

        if (CategoryModule.Videos.enabled(context)) {
            VideoUtil.save(context, category, videoUploads);
        }

        AttachmentUtil.save(category, attachmentDescriptions, files, makePreviews);

        // permissions
        if (Module.Permissions.enabled(context)) {

            if (category.isPermissions() != enablePermissions) {
                category.setPermissions(enablePermissions);
                if (enablePermissions) {
                    category.log(user, "Permissions enabled");
                } else {
                    category.log(user, "Permissions disabled");
                }
            }

            if (category.isPermissions()) {

                if (!ObjectUtil.equal(category.getRestrictionForwardUrl(), restrictionForwardUrl)) {
                    category.setRestrictionForwardUrl(restrictionForwardUrl);
                    category.log(user, "Restriction forward url changed to " + restrictionForwardUrl);
                }

                DomainUtil.savePermissions(context, category, permissions, PermissionCollections.getCategory(context));
                if (permissions.isEmpty()) {
                    category.log(user, "permissions cleared");
                } else {
                    category.log(user, "saving permissions=" + permissions);
                }
            }
        }

        if (moveChildrenTo != null) {
            category.moveChildrenTo(moveChildrenTo);
        }

        if (mergeInto != null) {
            category.mergeInto(mergeInto);
            category.log(user, "merged into " + mergeInto.getName() + " #" + mergeInto.getId());
        }

        category.setUseManualSort(useManualSort);

        category.setRestrictionForwardUrl(restrictionForwardUrl);

        category.save();

        PermissionsModule module = category.getPermissionsModule();
        module.setNoGuestLogin(noGuestLogin);
        module.setRestrictionForward(restrictionForward);
        module.setAllowOnlyReferrer(allowOnlyReferrer);
        module.setAccountAllowed(accountAllowed);
        module.save();

        clearParameters();
        setView();

        if (miscSettings.isAdvancedMode()) {
            return new ActionDoc(context, "This category has been updated with your changes", new Link(CategoryHandler.class, "edit", "category",
                    category));
        }

        AdminDoc doc = new AdminDoc(context, user, "The category has been updated", Tab.Categories);
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new TableTag("simplelinks", "center"));
                sb.append("<tr><th align='center'>What would you like to do ?</th></tr>");

                if (Privilege.EditCategory.yes(user, context)) {

                    sb
                            .append("<tr><td align='center'>" +
                                    new LinkTag(CategoryHandler.class, "edit", "I want to edit this category again", "category", category) +
                                    "</td></tr>");

                    sb.append("<tr><td align='center'>" + new LinkTag(CategoryHandler.class, "list", "I want to edit a different category") +
                            "</td></tr>");
                }

                if (Privilege.CreateCategory.yes(user, context))
                    sb.append("<tr><td align='center'>" + new LinkTag(CategoryHandler.class, "create", "I want to create a new category") +
                            "</td></tr>");

                sb.append("</table>");

                return sb.toString();
            }
        });
        return doc;
    }

    public Object saveCss() throws ServletException {

        if (category == null) {
            return edit();
        }

        category.setCss(css);
        category.save();

        return css();
    }

    public Object saveHeaders() throws ServletException {

        if (category == null) {
            return main();
        }

        category.setHeader(header);
        category.setFooter(footer);
        category.save();

        clearParameters();
        return headers();
    }

    public Object savePricing() throws ServletException {

        if (category == null) {
            return main();
        }

        category.setPriceBreaks(StringHelper.explodeIntegers(priceBreaks, "\\D"));
        PriceUtil.savePrices(context, category, prices);

        category.setDiscount(discount);
        category.setHideDiscountInfo(hideDiscountInfo);
        
        clearParameters();
        return pricing();
    }

    public Object applyDiscount() throws ServletException {

        if (category == null) {
            return main();
        }
        Amount discount = category.getDiscount();

        List<CategoryItem> categoryItems = category.getCategoryItems();

        for (CategoryItem ci: categoryItems){
            Item i = ci.getItem();

            if (i!=null && !i.isDiscountApplied()){
                i.applyDiscount(discount);
            }
        }

        category.setDiscount(null);

        clearParameters();
        return pricing();
    }

    public Object saveSearchWrapper() throws ServletException {

        if (category == null) {
            return main();
        }

        if (category.hasForwardUrl()) {
            return edit();
        }

        category.setSearchWrapper(searchWrapper);
        category.save();

        return new ActionDoc(context, "The search wrapper has been updated", new Link(CategoryHandler.class, "searchWrapper", "category", category));
    }

    public Object searchWrapper() throws ServletException {

        if (category == null) {
            return main();
        }

        if (category.hasForwardUrl()) {
            return edit();
        }

        setView();

        AdminDoc page = new AdminDoc(context, user, "Category '" + category.getName() + "' - Wrapped search", Tab.Categories);
        page.setMenu(new CategoryMenu(category));
        page.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update category"));
                if (category.isLeaf() || isSuperman()) {
                    sb.append(new ButtonTag(CategoryHandler.class, "delete", "Delete category", "category", category)
                            .setConfirmation("Are you sure you want to delete this category?"));

                }

                sb.append("</div>");
            }

            private void details() {

                sb.append(new AdminTable("Wrapped search"));

                sb.append(new AdminRow("Use wrapped search", "Set to yes to make this category a search wrapper.", new BooleanRadioTag(context,
                        "searchWrapper", category.isSearchWrapper())));

                if (category.isSearchWrapper()) {

                    sb.append(new AdminRow("Edit search", "Edit the search wrapper criterion", new ButtonTag(SearchEditHandler.class, null, "Submit",
                            "search", category.getSearch())));
                }

                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(CategoryHandler.class, "saveSearchWrapper", "post"));
                sb.append(new HiddenTag("category", category));

                details();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return page;
    }

    private void setView() {
        setAttribute("view", new Link(org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler.class, null, "category", category));
    }

    private Object simpleCreate(AdminDoc doc) {

        int count = Category.getCount(context);
        final Results results = new Results(count, page, 500);
        final Map<String, String> categoryOptions = Category.getCategoryOptions(context, null, results.getStartIndex(), 500);
        doc.setHelp("createBalloon", "parentBalloon", "buttonBalloon");
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new FormTag(CategoryHandler.class, "doCreate", "POST"));

                TextAreaTag namesInput = new TextAreaTag(context, "names", 40, 4);

                SelectTag parentSelect = new SelectTag(context, "parent", null, categoryOptions, "-I'll decide later");

                sb.append(new SimpleDiv("Enter the name of the categories to create."));
                sb.append(new SimpleDiv(namesInput));

                sb.append(new SimpleDiv("Where should we position this category?"));
                sb.append(new SimpleDiv(parentSelect));

                //				if (results.hasMultiplePages()) {
                //					Link link = new Link(CategoryHandler.class, "create");
                //					sb.append("<tr><td>" + new ResultsControl(context, results, link) + "</td></tr>");
                //				}

                SubmitTag submitTag = new SubmitTag("Create category");
                sb.append(new ActionsDiv(submitTag));

                sb.append("</form>");
                return sb.toString();
            }

        });
        return doc;
    }

     public Object comments() throws ServletException {
         if (category == null){
             return index();
         }

         final List<Comment> comments = Comment.get(context, false, category);
         AdminDoc doc = new AdminDoc(context, user, "Category Comments", Tab.Categories);
         doc.setMenu(new CategoryMenu(category));

         doc.addBody(new Body() {

             private void list() {
                 Link linkback = new Link(CategoryHandler.class, "comments", "category", category);
                 sb.append(new AdminTable("Comments to " + category.getName()));
                 sb.append("<tr>");
                 sb.append("<th width='20'>Id</th>");
                 sb.append("<th width='35'>Date</th>");
//                 sb.append("<th>Title</th>");
                 sb.append("<th>Comment</th>");
                 sb.append("<th width='10'>Delete</th>");
                 sb.append("</tr>");

                 for (Comment comment : comments) {

                     sb.append("<tr>");

                     // id
                     sb.append("<td>" + new LinkTag(CommentHandler.class, "edit", comment.getIdString(), "comment", comment,
                             "category", category, "linkback", linkback) + "</td>");

                     // date created
                     sb.append("<td>" + comment.getDateCreated().toDateString() + "</td>");

                     //title
//                     sb.append("<td>" + new LinkTag(CommentHandler.class, "edit", comment.getTitle(), "comment", comment,
//                             "category", category, "linkback", linkback) + "</td>");

                     //comment
                     sb.append("<td>" + comment.getComment() + "</td>");
//                     sb.append("<td>" + new LinkTag(CommentHandler.class, "edit", comment.getComment(), "comment", comment,
//                             "category", category, "linkback", linkback) + "</td>");

                     // delete link
                     sb.append("<td>");
                     sb.append(new LinkTag(CommentHandler.class, "delete", new DeleteGif(), "comment", comment,
                             "linkback", linkback).setConfirmation("Are you sure you want to delete this comment"));

                     sb.append("</td>");

                     sb.append("</tr>");

                 }

                 sb.append("</table>");
             }

             private void add() {
                 sb.append(new AdminTable("Add comment"));
                 sb.append(new FormTag(CategoryHandler.class, "addComment", "post"));
                 sb.append(new HiddenTag("category", category));

                 sb.append(new AddCommentsPanel(context));

                 sb.append("</form>");
                 sb.append("</table>");
             }

            @Override
            public String toString() {

                list();
                if (category.isComments()) {
                    add();
                }

                return sb.toString();
            }
        });
        return doc;
    }

    public Object addComment() throws ServletException {
        category.addComment(context, title, comment);
        clearParameters();
        return comments();
    }
}
