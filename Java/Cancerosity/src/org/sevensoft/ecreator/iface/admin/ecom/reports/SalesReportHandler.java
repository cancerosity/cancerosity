package org.sevensoft.ecreator.iface.admin.ecom.reports;

import javax.servlet.ServletException;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.Period;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.ecom.ShoppingMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.ecom.reports.SalesReport;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 19 Nov 2006 16:50:21
 * 
 * Shows sales per agent and overall per week  / month
 *
 */
@Path("admin-sales-report.do")
public class SalesReportHandler extends AdminHandler {

	public SalesReportHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {
		return weekly();
	}

	/**
	 *  Shows a breakdown of sales per agent for each week (ending on Sunday) for the past 10 weeks)
	 */
	public Object weekly() {

		Date end = new Date().nextSunday();
		Date start = end.removeWeeks(10);

		final SalesReport report = new SalesReport(context, start, end, new Period("1w"));

		AdminDoc doc = new AdminDoc(context, user, "Weekly agent sales report", Tab.Orders);
		doc.setMenu(new ShoppingMenu());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Weekly sales report"));

				// render weekly headings
				sb.append("<tr>");
				sb.append("<th>Agent / Week ending</th>");
				for (Date date : report.getDates()) {
					sb.append("<th>" + date.toString("dd MMM") + "</th>");
				}
				sb.append("</tr>");

				for (User agent : report.getAgents()) {
					sb.append("<tr>");
					sb.append("<th>" + agent.getName() + "</th>");

					for (Money total : report.getTotals(agent)) {
						sb.append("<td>" + total + "</td>");
					}

					sb.append("</tr>");
				}

				sb.append("<tr>");
				sb.append("<th>Total</th>");

				for (Money total : report.getTotals()) {
					sb.append("<td>" + total + "</td>");
				}

				sb.append("</tr>");

				return sb.toString();
			}

		});

		return doc;
	}
}
