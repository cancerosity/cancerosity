package org.sevensoft.ecreator.iface.admin.ecom.bookings;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.ItemsMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.stats.hits.pages.PageHitCounterMonthly;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.Row;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import javax.servlet.ServletException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * User: MeleshkoDN
 * Date: 24.09.2007
 * Time: 15:45:29
 */
@Path("admin-reports-property-hits-pages.do")
public class PropertyHitCounterMonthly extends AdminHandler {

    private Date month;
    private ItemType itemType;

    public PropertyHitCounterMonthly(RequestContext context) {
        super(context);
    }

    @Override
    public Object main() throws ServletException {
        return getPropertyPageHitsList();
    }


    public Object getPropertyPageHitsList() throws ServletException {

        if (itemType == null) {
            return index();
        }
        if (month == null) {
            month = new Date();
        }

        month = month.beginMonth();
        final Map<Item, Integer> hitsPerPropertyMap = getHitsPerProperty(context, month);
        final List<Item> nonAttendedProperties = getNonAttendedProperties(context, month);

        AdminDoc doc = new AdminDoc(context, user, "Monthly accomodation page hits", Tab.getItemTab(itemType));
        doc.setMenu(new ItemsMenu(itemType));
        doc.setIntro("Monthly accomodation page hits for month " + month.toString("MMMM-yy"));
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new AdminTable("Monthly accomodation page hits " + month.toString("MMMM-yy")));

                sb.append(new FormTag(PropertyHitCounterMonthly.class, "getPropertyPageHitsList", "get"));
                sb.append(new HiddenTag("itemType", itemType));
                SelectTag monthTag = new SelectTag(context, "month");
                monthTag.setAutoSubmit();

                Date month = new Date().beginMonth();
                for (int n = 0; n < 12; n++) {
                    monthTag.addOption(month, month.toString("MMMM-yy"));
                    month = month.removeMonths(1);
                }

                sb.append("<tr><th align='right' colspan='4'>Choose month ");
                sb.append(monthTag);
                sb.append("</th></tr>");

                sb.append("</form>");

                sb.append("<tr>");
                sb.append("<th>" + itemType.getName() + "</th>");
                sb.append("<th>Page views</th>");
                sb.append("</tr>");


                for (Map.Entry<Item, Integer> hitsPerProperty : hitsPerPropertyMap.entrySet()) {
                    final Item property = hitsPerProperty.getKey();
                    sb.append("<tr>");
                    sb.append("<td>");
                    sb.append(new LinkTag(property.getUrl(), property.getName()));
                    sb.append("</td>");
                    sb.append("<td>" + hitsPerProperty.getValue() + "</td>");
                    sb.append("</tr>");
                }

                for (Item property : nonAttendedProperties) {
                    sb.append("<tr>");
                    sb.append("<td>");
                    sb.append(new LinkTag(property.getUrl(), property.getName()));
                    sb.append("</td>");
                    sb.append("<td> 0 </td>");
                    sb.append("</tr>");
                }

                sb.append("</table>");

                return sb.toString();
            }

        });
        return doc;
    }

    private List<Item> getNonAttendedProperties(RequestContext context, Date month) {
        Query q = new Query(context, "SELECT * FROM items WHERE id NOT IN (SELECT i.id FROM # i JOIN # r ON i.id=r.item WHERE i.itemType=? AND r.date=?) AND itemType=? ORDER BY name ASC");
        q.setTable(Item.class);
        q.setTable(PageHitCounterMonthly.class);
        q.setParameter(itemType);
        q.setParameter(month);
        q.setParameter(itemType);
        return q.execute(Item.class);

    }

    public Map<Item, Integer> getHitsPerProperty(RequestContext context, Date month) {
        Map<Item, Integer> map = new LinkedHashMap<Item, Integer>();
        Query q = new Query(context, "SELECT i.id, SUM(r.total) FROM # i JOIN # r ON i.id=r.item WHERE i.itemType=? AND r.date=? GROUP BY r.item ORDER BY r.total DESC");
        q.setTable(Item.class);
        q.setTable(PageHitCounterMonthly.class);
        q.setParameter(itemType);
        q.setParameter(month);
        for (Row row : q.execute()) {
            map.put(row.getObject(0, Item.class), row.getInt(1));
        }
        return map;
    }

}
