package org.sevensoft.ecreator.iface.admin.search;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.model.search.forms.SearchGroup;
import org.sevensoft.ecreator.model.search.forms.SearchField;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;

import javax.servlet.ServletException;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
@Path("admin-search-field-group.do")
public class SearchGroupHandler extends AdminHandler {

    private SearchGroup group;
    private String name;
    private String values;

    public SearchGroupHandler(RequestContext context) {
        super(context);
    }

    public Object main() throws ServletException {
        if (group == null) {
            return index();
        }

        AdminDoc doc = new AdminDoc(context, user, "Edit search form", Tab.Extras);
        doc.addBody(new Body() {

            private void general() {


                sb.append(new AdminTable("Edit Group"));

                sb.append(new AdminRow("Name", new TextTag(context, "name", group.getName())));
                sb.append(new AdminRow("Values", "Use commas to separate values", new TextTag(context, "values", group.getValues())));

                sb.append("</table>");
            }

            private void commands() {
                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update"));
                sb.append(new ButtonTag(SearchFormFieldHandler.class, null, "Return to search field", "field", group.getSearchField()));
                sb.append(new ButtonTag(SearchGroupHandler.class, "delete", "Delete", "group", group)
                        .setConfirmation("Are you sure you want to delete this group?"));
                sb.append("</div>");
            }

            public String toString() {

                sb.append(new FormTag(SearchGroupHandler.class, "save", "POST"));
                sb.append(new HiddenTag("group", group));

                commands();
                general();
                commands();

                sb.append("</form>");

                return sb.toString();
            }
        });
        return doc;
    }

    public Object delete() throws ServletException {
        SearchField field = null;
        if (group != null) {
            field = group.getSearchField();
            group.delete();
        }

        return new SearchFormFieldHandler(context, field).main();
    }

    public Object save() throws ServletException {

        if (group != null) {
            group.setName(name);
            group.setValues(values);
            group.save();
        }

        return main();
    }
}
