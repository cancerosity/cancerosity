package org.sevensoft.ecreator.iface.admin.extras.polls;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.extras.polls.boxes.PollBox;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 24 Jul 2006 23:42:04
 *
 */
@Path("admin-polls-boxes.do")
public class PollBoxHandler extends BoxHandler {

	private PollBox	box;

	public PollBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {

	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

}
