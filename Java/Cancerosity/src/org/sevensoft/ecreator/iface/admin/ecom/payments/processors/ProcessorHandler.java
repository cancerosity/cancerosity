package org.sevensoft.ecreator.iface.admin.ecom.payments.processors;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.ecom.payments.PaymentSettingsHandler;
import org.sevensoft.ecreator.model.ecom.payments.processors.Processor;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 17 Apr 2007 08:39:20
 *
 */
public abstract class ProcessorHandler extends AdminHandler {

	public ProcessorHandler(RequestContext context) {
		super(context);
	}

	public abstract Object delete();

	protected Object delete(Processor processor) {

		if (processor != null) {
			processor.delete();
		}

		return new ActionDoc(context, "This processor has been deleted", new Link(PaymentSettingsHandler.class));
	}

}
