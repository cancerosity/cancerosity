package org.sevensoft.ecreator.iface.admin.system.config;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.misc.content.ContentBlockHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.misc.content.ContentBlock;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.Row;

import javax.servlet.ServletException;

/**
 * User: Tanya
 * Date: 07.06.2011
 */
@Path("admin-content-table.do")
public class ContentTablesHandler extends AdminHandler {

    public ContentTablesHandler(RequestContext context) {
        super(context);
    }

    public Object main() throws ServletException {
        AdminDoc doc = new AdminDoc(context, user, "Content Blocks", null);
        doc.addBody(new Body() {

            private void database() {

                sb.append(new AdminTable("Blocks"));

                Query q = new Query(context, "select id, char_length(content) as clc, char_length(contentLinked) from # order by clc desc").setTable(ContentBlock.class);

                int i = 0;
                sb.append("<tr><th>#</th><th>ID</th><th>Content length</th><th>Content Linked length</th></tr>");
                for (Row block : q.execute()) {

                    sb.append("<tr><td>" + ++i + "<td>" + new LinkTag(ContentBlockHandler.class, null, block.getInt(0), "block", block.getInteger(0)) + "</td>");
                    sb.append("<td>" + block.getInteger(1) + "</td>");
                    sb.append("<td>" + block.getInteger(2) + "</td>");
                    sb.append("</tr>");

                }

                sb.append("</table>");
            }

            @Override
            public String toString() {

                database();

                return sb.toString();
            }
        });

        return doc;
    }
}
