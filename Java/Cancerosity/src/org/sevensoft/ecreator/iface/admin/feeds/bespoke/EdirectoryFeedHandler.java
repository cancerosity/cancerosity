package org.sevensoft.ecreator.iface.admin.feeds.bespoke;

import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.model.feeds.bespoke.prices.edir.EdirectoryFeed;
import org.sevensoft.ecreator.model.feeds.Feed;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.ItemType;

/**
 * @author Dmitry Lebedev
 *         Date: 27.10.2009
 */
@Path("admin-feeds-edirectory.do")
public class EdirectoryFeedHandler extends FeedHandler {

    private EdirectoryFeed feed;
    private Attribute modelAttribute;
    private Attribute manufacturerAttribute;
    private Attribute weightAttribute;
    private ItemType itemType;
    private int option1;
    private int option2;
    private int option3;
    private int option4;
    private String email;

    public EdirectoryFeedHandler(RequestContext context) {
        super(context);
    }

    protected Feed getFeed() {
        return feed;
    }

    protected void saveSpecific() {
        feed.setManufacturerAttribute(manufacturerAttribute);
        feed.setModelAttribute(modelAttribute);
        feed.setWeightAttribute(weightAttribute);
        feed.setItemType(itemType);
        feed.setOptionName1(option1);
        feed.setOptionName2(option2);
        feed.setOptionName3(option3);
        feed.setOptionName4(option4);
        feed.setEmail(email);
    }

    protected void specifics(StringBuilder sb) {
        sb.append(new AdminTable("eDirectory settings"));
        sb.append(new AdminRow("Item type", "Select the item type to import to.", new SelectTag(context, "itemType", feed.getItemType(), ItemType.getSelectionMap(context), "-None set-")));
        if (feed.hasItemType()) {
            sb.append(new AdminRow("Manufacturer attribute", "Select the attribute to be used for manufacturers.", new SelectTag(context, "manufacturerAttribute", feed.getManufacturerAttribute(), feed.getItemType().getAttributes(), "-None set-")));
            sb.append(new AdminRow("Manuf model attribute", "Select the attribute to be used for manufacturers model codefs.", new SelectTag(context, "modelAttribute", feed.getModelAttribute(), feed.getItemType().getAttributes(), "-None set-")));
            sb.append(new AdminRow("Weight attribute", "Select the attribute to be used for weight.", new SelectTag(context, "weightAttribute", feed.getWeightAttribute(), feed.getItemType().getAttributes(), "-None set-")));
        }
        sb.append(new AdminRow("Option 1", "Enter the name of option 1 or leave blank", new TextTag(context, "option1", feed.getOptionName1(), 20)));
        sb.append(new AdminRow("Option 2", "Enter the name of option 2 or leave blank", new TextTag(context, "option2", feed.getOptionName2(), 20)));
        sb.append(new AdminRow("Option 3", "Enter the name of option 3 or leave blank", new TextTag(context, "option3", feed.getOptionName3(), 20)));
        sb.append(new AdminRow("Option 4", "Enter the name of option 4 or leave blank", new TextTag(context, "option4", feed.getOptionName4(), 20)));
        sb.append(new AdminRow("Email", "To this email created csv file will be sent", new TextTag(context, "email", feed.getEmail(), 35)));
        sb.append("</table>");
    }
}
