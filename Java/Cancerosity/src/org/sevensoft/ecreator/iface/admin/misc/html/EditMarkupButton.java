package org.sevensoft.ecreator.iface.admin.misc.html;

import org.sevensoft.ecreator.iface.admin.design.markup.MarkupHandler;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 23 Oct 2006 14:09:36
 *
 */
public class EditMarkupButton {

	private final String	id;

	public EditMarkupButton(String id) {
		this.id = id;
	}

	@Override
	public String toString() {

		return new ButtonTag("Edit markup").setOnClick(
				"window.location='" + new Link(MarkupHandler.class, "edit") + "&markup=' + document.getElementById('" + id + "').value;").toString();
	}

}
