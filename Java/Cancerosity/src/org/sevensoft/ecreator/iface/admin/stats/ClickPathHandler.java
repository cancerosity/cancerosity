package org.sevensoft.ecreator.iface.admin.stats;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.reports.StatsMenu;
import org.sevensoft.ecreator.iface.admin.stats.panels.ClickPathPanel;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.stats.visits.Visitor;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 10 Apr 2007 12:18:48
 *
 */
@Path("admin-stats-clickpath.do")
public class ClickPathHandler extends AdminHandler {

	private Visitor	visitor;

	public ClickPathHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Visitor click path", Tab.Stats);
		doc.setMenu(new StatsMenu());
		doc.addBody(new ClickPathPanel(context, visitor));
		return doc;
	}

}
