package org.sevensoft.ecreator.iface.admin.items.modules.options;

import java.util.List;
import java.util.ArrayList;

import javax.servlet.ServletException;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.ecreator.model.items.options.ItemOption.Type;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sam
 */
@Path("admin-options.do")
public class ItemOptionsHandler extends AdminHandler {

	private int				letterLimit;
	private String			name;
	private ItemOption		option;
	private boolean			optional;
	private Money			price;
	private ItemOptionSelection	selection;
	private Type			type;
	private int				wordLimit;
	private int				width;
	private int				height;
	private String			newSelections;
	private String			intro;
	private boolean			images;
	private boolean			nameOrder;
	private boolean			priceOverride;
    private boolean         stockOverride;
    private transient List<ItemOptionSelection> itemOptionSelections;

    public ItemOptionsHandler(RequestContext context) {
		super(context);
        itemOptionSelections = new ArrayList<ItemOptionSelection>();
    }

	public ItemOptionsHandler(RequestContext context, ItemOption option) {
		super(context);
		this.option = option;
        itemOptionSelections = new ArrayList<ItemOptionSelection>();
    }

    public Object delete() throws ServletException {

        if (option == null)
            return main();

        Item item = option.getItem();
        ItemType itemType = option.getItemType();
        if (item != null) {
            item.getOptionSet().removeOption(option);
            return new ItemHandler(context, item).options();
        } else {
            itemType.getOptionSet().removeITypeOption(option);
            return new ItemTypeHandler(context, itemType).options();
        }

    }

    public Object deleteRelated() throws ServletException {
        if (option == null)
            return main();

        List<ItemOption> related = option.getRelated(context);
        for (ItemOption itemOption : related) {
            Item item = itemOption.getItem();
            if (item != null) {
                item.getOptionSet().removeOption(itemOption);
            }
        }

        addMessage(related.size() + " related options have been removed");

        return new ItemTypeHandler(context, option.getItemType()).options();
    }

    public Object addEverywhere() throws ServletException {
        if (option == null)
            return main();

        List<Item> items = Item.get(context, option.getItemType(), "Live");
        for (Item item : items) {
            option.copyTo(item);
        }

        addMessage(items.size() + " " + option.getItemType().getNameLower() + " have a new option");

        return new ItemTypeHandler(context, option.getItemType()).options();
    }

    @Override
	public Object main() {

		if (option == null)
			return new DashboardHandler(context).main();

		final Item item = option.getItem();
        final ItemType itemType = option.getItemType();

        AdminDoc page;
        if (item != null) {
            page = new AdminDoc(context, user, "Edit option", Tab.getItemTab(item));
        } else {
            page = new AdminDoc(context, user, "Edit option", Tab.getItemTab(itemType));
        }
        page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update option"));
                if (item != null) {
                    sb.append(new ButtonTag(ItemHandler.class, "options", "Return to " + item.getItemType().getName(), "item", item));
                } else {
                    sb.append(new ButtonTag(ItemTypeHandler.class, "options", "Return to " + itemType.getName(), "itemType", itemType));
                }
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Name", "The name of this option.", new TextTag(context, "name", option.getName(), 50)));

				SelectTag typeTag = new SelectTag(context, "type", option.getType());
				typeTag.addOptions(ItemOption.Type.values());

				sb.append(new AdminRow("Type", "The type of this option.", typeTag));

				sb
						.append(new AdminRow(
								"Price override",
								"If you set this to yes then any price set on this option will be used as the total price, rather than an additional cost.",
								new BooleanRadioTag(context, "priceOverride", option.isPriceOverride())));

                sb.append(new AdminRow("Stock overrride", "If you set this to yes then any stock set on this option will be used to combine the total stock.",
                        new BooleanRadioTag(context, "stockOverride", option.isStockOverride())));

                switch (option.getType()) {

				default:
				case Selection:
                case List:

					sb.append(new AdminRow("Optional", "Set this attribute to optional so it can be ignored by the customer.", new BooleanRadioTag(
							context, "optional", option.isOptional())));

					sb.append(new AdminRow("Intro option", "Lets you change the text that appears at the top of the drop down list as instruction",
							new TextTag(context, "intro", option.getIntro(), 40)));

					sb.append(new AdminRow("Images", "Assign an image to each selection so the customer can view images of what the selections are.",
							new BooleanRadioTag(context, "images", option.isImages())));

					sb.append(new AdminRow("Order by name", "Order selections by name instead of manual ordering.", new BooleanRadioTag(context,
							"nameOrder", option.isNameOrder())));

					break;

				case Text:

                    sb.append(new AdminRow("Optional", "Set this attribute to optional so it can be ignored by the customer.", new BooleanRadioTag(
                            context, "optional", option.isOptional())));

                    if (ItemModule.Pricing.enabled(context, item) || ItemModule.Pricing.enabled(context, itemType)) {

						sb.append(new AdminRow("Sell price adjustment", "Change sell price by the value entered here.", new TextTag(context,
                                "price", option.getPrice(), 40) + " " +
                                new ButtonTag(ItemOptionsHandler.class, "savePrices", "Save Related Prices", "option", option, "price", option.getPrice())));

					}

					sb.append(new AdminRow("Width / Height", "Enter the dimensions of the box where customers can enter their text", "Width: "
							+ new TextTag(context, "width", option.getWidth(), 4) + " Height: "
							+ new TextTag(context, "height", option.getHeight(), 4)));

					break;

				case YesNo:

					sb.append(new AdminRow("Optional", "Set this attribute to optional so it can be ignored by the customer.", new BooleanRadioTag(
							context, "optional", option.isOptional())));

                    if (ItemModule.Pricing.enabled(context, item) || ItemModule.Pricing.enabled(context, itemType)) {

						surcharge();

					}
				}

				sb.append("</table>");
			}

			private void selections() {

				sb.append(new AdminTable("Selections"));

				sb.append("<tr>");
				sb.append("<th width='80'>Position</th>");
				sb.append("<th>Selection</th>");
				sb.append("<th width='120'>Edit</th>");
				sb.append("<th width='120'>Remove</th>");
				sb.append("</tr>");

				PositionRender pr = new PositionRender(ItemOptionSelectionHandler.class, "selection");
				for (ItemOptionSelection selection : option.getSelections()) {

					sb.append("<tr>");
					sb.append("<td width='80'>" + pr.render(selection) + "</td>");
					sb.append("<td>" + selection.getText() + "</td>");
					sb.append("<td>" + new ButtonTag(ItemOptionSelectionHandler.class, null, "Edit", "selection", selection) + "</td>");
					sb.append("<td>"
							+ new ButtonTag(ItemOptionSelectionHandler.class, "delete", "Remove", "option", option, "selection", selection)
									.setConfirmation("Are you sure you want to delete this selection?") + "</td>");
					sb.append("</tr>");

				}

				sb.append("<tr>");
				sb.append("<td colspan='4'>Add new selections<br/>");
				sb.append(new TextAreaTag(context, "newSelections", 40, 3));
				sb.append("</td></tr>");
				sb.append("</tr>");

				sb.append("</table>");

			}

			private void surcharge() {

				sb.append(new AdminRow("Surcharge / Discount", "Modify the price when selecting this option.", new TextTag(context, "price", option
                        .getPrice(), 20) + " " + new ButtonTag(ItemOptionsHandler.class, "savePrices", "Save Related Prices", "option", option, "price", option
                        .getPrice())));
			}

			@Override
			public String toString() {

				sb.append(new FormTag(ItemOptionsHandler.class, "save", "POST"));
				sb.append(new HiddenTag("option", option));

                commands();
				general();
				if (option.isSelection()) {
					selections();

				}

				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return page;
	}

	public Object moveDown() throws ServletException {

		if (option == null)
			return main();

		List<ItemOption> options = option.getItem().getOptionSet().getOptions();
		EntityUtil.moveDown(option, options);

		return new ItemHandler(context, option.getItem()).options();
	}

	public Object moveUp() throws ServletException {

		if (option == null)
			return main();

		List<ItemOption> options = option.getItem().getOptionSet().getOptions();
		EntityUtil.moveUp(option, options);

		return new ItemHandler(context, option.getItem()).options();
	}

	public Object removeSelection() {

		if (option == null || selection == null)
			return main();

		option.removeSelection(selection);
		return main();
	}

	public Object save() {

		if (option == null)
			return main();

		test(new RequiredValidator(), "name");
		test(new RequiredValidator(), "type");

		if (hasErrors())
			return main();

        saveOption(option);

        if (option.getParent() == null) {
            List<ItemOption> relatedOptions = option.getRelated(context);
            for (ItemOption relatedOption : relatedOptions) {
                saveOption(relatedOption, true);
            }
            addMessage(relatedOptions.size() + " related options have been updated");
        } else {
            addMessage("Option updated");
        }

		clearParameters();
		return main();
	}

    private void saveOption(ItemOption option) {
        saveOption(option, false);
    }

    private void saveOption(ItemOption option, boolean copyOptionSelections) {
        option.setName(name);
        option.setType(type);
        option.setOptional(optional);
        option.setLetterLimit(letterLimit);
        option.setWordLimit(wordLimit);
        if (!copyOptionSelections)
            option.setPrice(price);
        option.setHeight(height);
        option.setWidth(width);
        option.setIntro(intro);
        option.setImages(images);
        option.setNameOrder(nameOrder);
        option.setPriceOverride(priceOverride);
        option.setStockOverride(stockOverride);
        option.save();

        if (option.isSelection()) {

            if (newSelections != null) {
                if (copyOptionSelections) {
                    for (ItemOptionSelection itemOptionSelection : itemOptionSelections) {
                        itemOptionSelection.copyTo(option);
                    }
                } else {
                    for (String string : newSelections.split("\n")) {
                        string = string.trim();
                        if (string.length() > 0)
                            itemOptionSelections.add(option.addSelection(string));
                    }
                }
            }
        }
    }

    public Object savePrices() throws ServletException {
        if (option == null) {
            return main();
        }
        option.setPrice(price);
        option.savePrice();
        for (ItemOption itemOption : option.getRelated(context)) {
            itemOption.setPrice(price);
            itemOption.savePrice();
        }
        return main();
    }
}
