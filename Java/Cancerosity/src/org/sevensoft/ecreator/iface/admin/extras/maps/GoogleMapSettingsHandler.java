package org.sevensoft.ecreator.iface.admin.extras.maps;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.extras.mapping.google.GoogleMapIcon;
import org.sevensoft.ecreator.model.extras.mapping.google.GoogleMapSettings;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 24 Apr 2007 22:21:39
 *
 */
@Path("admin-google-map-settings.do")
public class GoogleMapSettingsHandler extends AdminHandler {

	private final transient GoogleMapSettings	settings;
	private String					googleMapsApiKey;

	public GoogleMapSettingsHandler(RequestContext context) {
		super(context);
		this.settings = GoogleMapSettings.getInstance(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Google map settings", null);
		doc.addBody(new Body() {

			/**
			 * 
			 */
			private void create() {
				sb.append(new FormTag(GoogleMapIconsHandler.class, "create", "post"));
				sb.append(new AdminTable("Icons"));

				for (GoogleMapIcon icon : GoogleMapIcon.get(context)) {

					sb.append("<tr>");
					sb.append("<td>" + new LinkTag(GoogleMapIconsHandler.class, null, new SpannerGif(), "icon", icon) + " " + icon.getName()
							+ "</td>");
					sb.append("<td width='10'>"
							+ new LinkTag(GoogleMapIconsHandler.class, "delete", new DeleteGif(), "icon", icon)
									.setConfirmation("Are you sure you want to delete this icon?") + "</td>");
					sb.append("</tr>");
				}

				sb.append("<tr><th colspan='2'>Add new icon: " + new SelectTag(context, "itemType", null, ItemType.getSelectionMap(context), "-Select item type-")
						+ " " + new SubmitTag("Add") + "</th></tr>");

				sb.append("</table>");
				sb.append("</form>");
			}

			/**
			 * 
			 */
			private void settings() {

				sb.append(new FormTag(GoogleMapSettingsHandler.class, "save", "post"));
				sb.append(new AdminTable("Settings"));

                String desc = "    To create your API key, visit the APIs Console at https://code.google.com/apis/console and log in with your Google Account.\n" +
                        "    Click the <b>Services</b> link from the left-hand menu, then activate the <b>Google Maps API v3</b> service.\n" +
                        "    Once the service has been activated, your API key is available from the <b>API Access</b> page," +
                        " in the <b>Simple API Access</b> section. Maps API applications use the <b>Key for browser apps</b>.";
                
				sb.append(new AdminRow("Google maps api key", desc, new TextTag(context,
						"googleMapsApiKey", settings.getGoogleMapsApiKey(), 40)));

				sb.append("</table>");

				sb.append(new SubmitTag("Update"));
				sb.append("</form>");
			}

			@Override
			public String toString() {

				settings();

				create();

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		settings.setGoogleMapsApiKey(googleMapsApiKey);
		settings.save();

		return main();
	}
}
