package org.sevensoft.ecreator.iface.admin.misc.content;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 15 Mar 2007 08:24:22
 *
 */
@Path("admin-boxes-tagcloud.do")
public class TagCloudBoxHandler extends BoxHandler {

	public TagCloudBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Box getBox() {
		return null;
	}

	@Override
	protected void saveSpecific() throws ServletException {
	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

}
