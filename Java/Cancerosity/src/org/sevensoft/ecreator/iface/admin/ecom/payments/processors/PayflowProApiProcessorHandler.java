package org.sevensoft.ecreator.iface.admin.ecom.payments.processors;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.ecreator.model.ecom.payments.processors.paypal.PayflowProApiProcessor;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.ecom.payments.PaymentSettingsHandler;

import javax.servlet.ServletException;

/**
 * User: Tanya
 * Date: 21.04.2012
 */
@Path("admin-payments-processors-payflowproapi.do")
public class PayflowProApiProcessorHandler extends ProcessorHandler {

    private PayflowProApiProcessor processor;

    private String username;
    private String password;
    private String signature;
    private boolean live;

    public PayflowProApiProcessorHandler(RequestContext context) {
        super(context);
    }

    public Object delete() {
        return super.delete(processor);
    }

    public Object main() throws ServletException {
        if (processor == null) {
            return index();
        }

        AdminDoc doc = new AdminDoc(context, user, "Payflo Pro API", null);
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update processor"));
                sb.append(new ButtonTag(PaymentSettingsHandler.class, null, "Return to payment settings"));
                sb.append("</div>");
            }

            private void general() {

                sb.append(new AdminTable("Payflo Pro API"));

                sb.append(new AdminRow("API Username", "The data is provided in your PayPal merchant account", new TextTag(context, "username",
                        processor.getUsername(), 40)));

                sb.append(new AdminRow("API Password", "", new TextTag(context, "password", processor.getPassword(), 20)));

                sb.append(new AdminRow("Signature", "", new TextTag(context, "signature", processor.getSignature(), 80)));

                //todo boolean Live ?

                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(PayflowProApiProcessorHandler.class, "save", "post"));
                sb.append(new HiddenTag("processor", processor));

                commands();
                general();
                commands();
                sb.append("</form>");

                return sb.toString();
            }
        });
        return doc;
    }

    public Object save() throws ServletException {

        if (processor == null) {
            return index();
        }

        processor.setUsername(username);
        processor.setPassword(password);
        processor.setSignature(signature);
        processor.save();

        return new ActionDoc(context, "This processor has been updated", new Link(PayflowProApiProcessorHandler.class, null, "processor", processor));
    }
}
