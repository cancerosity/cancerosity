package org.sevensoft.ecreator.iface.admin.ecom.payments;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.ecom.payments.help.GlobalIrisAutoSettleHelp;
import org.sevensoft.ecreator.iface.admin.ecom.payments.help.GlobalIrisCallbackHelp;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.iface.callbacks.PaypalCallbackHandler;
import org.sevensoft.ecreator.iface.callbacks.WorldpayCallbackHandler;
import org.sevensoft.ecreator.iface.callbacks.GlobalIrisRealAuthCallbackHandler;
import org.sevensoft.ecreator.iface.callbacks.GoogleCheckoutCallbackHandler;
import org.sevensoft.ecreator.iface.callbacks.CardsaveCallbackHandler;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.ecom.payments.forms.hsbc.HsbcCpi;
import org.sevensoft.ecreator.model.ecom.payments.processors.Processor;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.bool.RadioTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.text.PasswordTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 03-Oct-2005 14:25:38
 * 
 */
@Path("admin-settings-payment.do")
public class PaymentSettingsHandler extends AdminHandler {

	private String					payPalAccountEmail, protxVendorEmail, protxVendorName, worldPayInstallationId, chequeAddress, transferDetails,
			chequePayee;
	private String					protxEncryptionPassword;
	private boolean					protxLive, worldPayLive, protxSandbox;
	private String					epdqPassphrase;
	private String					epdqStoreId;
	private String					epdqPostUsername;
	private String					epdqPostPassword;
	private String					epdqCallbackPassword;
	private String					worldPayCallbackPassword;
	private String					offlineContactMessage;
	private PaymentType				paymentType;
	private String					secCardPassword;
	private boolean					secPayLive;
	private String					secCardUsername;
	private String					able2BuyId2;
	private String					able2BuyId;
	private boolean					able2BuyLive;
	private String					able2BuyGoodsCode;
	private boolean					payPalLive;
	private String					secureTradingPagesMerchant;
	private String					secureTradingPagesEmail;
    private String					secureTradingCallbackLine;
    private String					secureTradingFailedCallbackLine;
    private String secureTradingFormref;
    private boolean					secureTradingPagesLive;
	private boolean					netPaymentsExpressLive;
	private String					netPaymentsExpressAccount;
	private String					payPalPdtToken;
	private String					hsbcCpiHashKey;
	private boolean					hsbcCpiLive;
    private boolean					hsbcCpiIris;
	private String					hsbcCpiStorefrontId;
    private String hsbcCpiTransactionType;
    private String					mEnableAccount;
	private String					mEnableRequestCode;
	private String					mEnableMpcId;
	private final transient PaymentSettings	paymentSettings;
	private boolean					terminalAvsSupport;
	private String					worldpayAccountId;
	private boolean					immediateTransact;
	private String					protxEmailHeader;
	private String					metaChargeInstallId;
	private String					worldpayAuthMode;
	private Money					able2BuyMaxSpend;
	private Money					able2BuyMinSpend;
	private String					secPayUsername;
	private String					noChexMerchantId;
	private String					twoCheckoutSid;
	private String					processorClass;
	private String					declineEmails;
	private boolean					verifyCard;
    private String                  optimalPaymentsAccountId;
    private String                  optimalPaymentsMerchantId;
    private String                  googleCheckoutId;
    private String                  googleCheckoutPw;
    private boolean                 googleCheckoutLive;
    private String					protxPaymentType;
    private String                  payPalExpressAccountPassword;
    private String                  payPalExpressAccountSignature;
    private String                  payPalExpressAccountUsername;
    private String                  payPalDirectAccountPassword;
    private String                  payPalDirectAccountSignature;
    private String                  payPalDirectAccountUsername;
    private boolean                 payPalExpressLive;
    private boolean                 payPalDirectLive;
    private String                  cardsaveMerchantId;
    private String                  cardsaveMerchantPw;
    private String                  cardsavePreSharedKey;
      /**
     * PayPal Pro Express
     */
    private String payPalProExpressApiUsername;

    private String payPalProExpressApiPassword;

    private String payPalProExpressSignature;

    private boolean payPalProExpressLive;

    /**
     * PayPal Pro Hosted
     */
    private String payPalProHostedMerchantId;
    private boolean payPalProHostedLive;

    /**
     * Global iris. RealEx payment
     */
    private String globalirisMerchantId;
    private String globalirisSubaccountName;
    private boolean globalirisAutoSettle;
    private String globalirisSecretKey;

    public PaymentSettingsHandler(RequestContext context) {
		super(context);
		this.paymentSettings = PaymentSettings.getInstance(context);
	}

	public Object addPaymentType() {

		if (paymentType == null)
			return main();

		paymentSettings.addPaymentType(paymentType);
		paymentSettings.save();

		return main();
	}

	public Object main() {

		AdminDoc doc = new AdminDoc(context, user, "Payment settings", null);
		doc.addBody(new Body() {

			/**
			 * 
			 */
			private void able2Buy() {

				sb.append(new AdminTable("Able2Buy"));

				sb.append(new AdminRow("Username (ID)", "The able2buy username is also referred to as the ID.", new TextTag(context, "able2BuyId",
						paymentSettings.getAble2BuyId())));

				sb.append(new AdminRow("Password (ID2)", "The able2buy password is also referred to as ID2.", new TextTag(context, "able2BuyId2",
						paymentSettings.getAble2BuyId2())));

				sb.append(new AdminRow("Goods code",
						"Able2buy will issue you a goods code to identify the types of goods you are selling. Enter that here.", new TextTag(
								context, "able2BuyGoodsCode", paymentSettings.getAble2BuyGoodsCode())));

				sb.append(new AdminRow("Spend limits", "Enter the minimum and maximum amounts customers can spend to qualify for able2buy finance.",
						"From " + new TextTag(context, "able2BuyMinSpend", paymentSettings.getAble2BuyMinSpend()) + " to "
								+ new TextTag(context, "able2BuyMaxSpend", paymentSettings.getAble2BuyMaxSpend())));

				sb.append(new AdminRow("Able2Buy mode", "In test mode all payments are simulated and no money is taken from the cards.", "Live "
						+ new RadioTag(context, "able2BuyLive", "true", paymentSettings.isAble2BuyLive()) + " Test "
						+ new RadioTag(context, "able2BuyLive", "false", !paymentSettings.isAble2BuyLive())));

				sb.append(new AdminRow("Credit groups", "Edit the credit groups your shop can use.", new ButtonTag(Able2BuyCreditGroupsHandler.class,
						null, "Edit credit groups")));

				sb.append(new AdminRow("Success url", "Send this link to the able2buy integration team.", new LinkTag(paymentSettings
						.getAble2BuySuccessUrl(), paymentSettings.getAble2BuySuccessUrl())));

				sb.append("</table>");
			}

			//			private void card() {
			//
			//				sb.append(new AdminTable("Credit card"));
			//
			//				sb.append("<tr><th width='300'><b>Avs support</b><br/>Enable AVS / CV2 support if your terminal supports this.</th><td>"
			//						+ new BooleanRadioTag(context, "terminalAvsSupport", paymentSettings.isTerminalAvsSupport()) + "</td></tr>");
			//
			//				sb.append("</table>");
			//			}

			private void cheque() {

				sb.append(new AdminTable("Cheque payments"));

				sb.append(new AdminRow("Cheque payee", "Enter the name that cheques should be made payable to.", new TextTag(context, "chequePayee",
						paymentSettings.getChequePayee(), 40)));

				sb.append(new AdminRow("Cheque address", "Enter the postal address of where cheques should be sent.", new TextAreaTag(context,
						"chequeAddress", paymentSettings.getChequeAddress(), 40, 4)));

				sb.append("</table>");
			}

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update payment settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void epdqCPI() {

				sb.append(new AdminTable("Epdq CPI"));

				sb.append(new AdminRow("Epdq store/client id", "This is your unique store and client ID as provided by Epdq.", new TextTag(context,
						"epdqStoreId", paymentSettings.getEpdqStoreId())));

				sb.append(new AdminRow("Epdq passphrase", "This passphrase is used by our server to encrypt the data sent to Epdq.", new TextTag(
						context, "epdqPassphrase", paymentSettings.getEpdqPassphrase())));

				sb.append(new AdminRow("Epdq post username", "Sets a username which is used by Epdq for server callbacks.", new TextTag(context,
						"epdqPostUsername", paymentSettings.getEpdqPostUsername())));

				sb.append(new AdminRow("Epdq post password", "Sets a passworrd which is used by Epdq for server callbacks.", new TextTag(context,
						"epdqPostPassword", paymentSettings.getEpdqPostPassword())));

				sb.append("</table>");
			}

			private void hsbcCpi() {

				sb.append(new AdminTable("HSBC CPI"));

				sb.append(new AdminRow("Storefront ID", "Your unique username for your account.", new TextTag(context, "hsbcCpiStorefrontId",
						paymentSettings.getHsbcCpiStorefrontId(), 40)));

				sb.append(new AdminRow("CPI Hash Key",
						"Sent to you by letter. The hash key is used by the system when generating the payment request.", new TextTag(context,
								"hsbcCpiHashKey", paymentSettings.getHsbcCpiHashKey(), 40)));

				sb.append(new AdminRow("Mode", "In test mode all payments are simulated and no money is taken from the cards.", "Live "
						+ new RadioTag(context, "hsbcCpiLive", "true", paymentSettings.isHsbcCpiLive()) + " Test "
						+ new RadioTag(context, "hsbcCpiLive", "false", !paymentSettings.isHsbcCpiLive())));

                sb.append(new AdminRow("Payment Gateway", "Select payment gateway. Please do not choose Global Iris until you have confirmed with a Global\n" +
                        "Iris technical support team that your account is set up and ready for you to change.", "ePayments "
						+ new RadioTag(context, "hsbcCpiIris", "false", !paymentSettings.isHsbcCpiIris()) + " Global Iris "
						+ new RadioTag(context, "hsbcCpiIris", "true", paymentSettings.isHsbcCpiIris())));

                SelectTag tag = new SelectTag(context, "hsbcCpiTransactionType", paymentSettings.getHsbcCpiTransactionType(), HsbcCpi.TransactionType.values());
                tag.setAny("-None-");

                sb.append(new AdminRow("CPI TransactionType",
                        "Optional. An Auth transaction places a reserve on the cardholders open-to-buy balance, the cardholders available balance remains unchanged.\n" +
                                "A Capture transaction verifies the cardholder�s account to be in good standing, and automatically marks the funds ready for settlement.\n" +
                                "This is typically used for goods that do not need to be physically shipped", tag));

                sb.append("</table>");
			}

			private void metaCharge() {

				sb.append(new AdminTable("Meta charge"));
				sb.append(new AdminRow("Install ID", "Your install ID is provided by meta charge", new TextTag(context, "metaChargeInstallId",
						paymentSettings.getMetaChargeInstallId(), 20)));
				sb.append("</table>");
			}

			private void netPayments() {

				if (!paymentSettings.getPaymentTypes().contains(PaymentType.NetPaymentsExpress))
					return;

				sb.append(new AdminTable("Net Payments Express"));

				sb.append(new AdminRow("Express account", "This is your unique account ID as provided by Net Payments.", new TextTag(context,
						"netPaymentsExpressAccount", paymentSettings.getNetPaymentsExpressAccount(), 12)));

				sb.append(new AdminRow("Mode", "In test mode all payments are simulated and no money is taken from the cards.", "Live "
						+ new RadioTag(context, "netPaymentsExpressLive", "true", paymentSettings.isNetPaymentsExpressLive()) + " Test "
						+ new RadioTag(context, "netPaymentsExpressLive", "false", !paymentSettings.isNetPaymentsExpressLive())));

				sb.append("</table>");
			}

			private void nochex() {

				sb.append(new AdminTable("NoChex"));

				sb.append(new AdminRow("NoChex Merchant Id", "This is your unique account number provided by nochex.", new TextTag(context,
						"noChexMerchantId", paymentSettings.getNoChexMerchantId(), 20)));

				sb.append("</table>");
			}

			private void offline() {

				sb.append(new AdminTable("Offline payments"));

				sb.append(new AdminRow("Offline payment message",
						"Enter a message to be shown to the customer, eg, that you will contact them later for their payment details.",
						new TextAreaTag(context, "offlineContactMessage", paymentSettings.getOfflineContactMessage(), 50, 5)));

				sb.append("</table>");
			}

			private void paymentTypes() {

				sb.append(new AdminTable("Payment types"));

				for (PaymentType type : paymentSettings.getPaymentTypes()) {

                    sb.append("<tr><td>").append(type.getName()).append("</td>");
                    sb.append("<td>").append(type.getPublicName()).append("</td>");
                    sb.append("<td width='10'>\n").append(new LinkTag(PaymentSettingsHandler.class, "removePaymentType", new DeleteGif(), "paymentType", type)).append("</td>");
                    sb.append("</tr>");
				}

				SelectTag tag = new SelectTag(context, "paymentType");
				List<PaymentType> types = new ArrayList(Arrays.asList(PaymentType.values()));
				types.removeAll(paymentSettings.getPaymentTypes());

                if (!Module.GoogleCheckout.enabled(context)) {
                    types.remove(PaymentType.GoogleCheckout);
                }

                if (!Module.PayPalExpress.enabled(context)) {
                    types.remove(PaymentType.PayPalExpress);
                }

                if (!Module.PayPalProExpress.enabled(context)) {
                    types.remove(PaymentType.PayPalProExpress);
                }

                if (!Module.PayPalProHosted.enabled(context)) {
                    types.remove(PaymentType.PayPalProHosted);
                }

                if(!Module.CardsavePayment.enabled(context)){
                    types.remove(PaymentType.CardsaveRedirect);
                }

                Collections.sort(types, new Comparator<PaymentType>() {

					public int compare(PaymentType o1, PaymentType o2) {
						return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
					}

				});

				for (PaymentType type : types)
					tag.addOption(type, type.getName());

				sb.append(new FormTag(PaymentSettingsHandler.class, "addPaymentType", "POST"));

				sb.append("<tr><td colspan='3'>Add payment type: " + tag + " " + new SubmitTag("Add payment type") + "</td></tr>");
				sb.append("</form>");

				sb.append("</table>");
			}

			private void payPalStandart() {

				sb.append(new AdminTable("PayPal"));

				sb.append(new AdminRow("PayPal account", "This is the email of your pay pal account.", new TextTag(context, "payPalAccountEmail",
						paymentSettings.getPayPalAccountEmail(), 40)));

				sb.append(new AdminRow("PayPal mode", "In test mode all payments are simulated and no money is taken from the cards.", "Live "
						+ new RadioTag(context, "payPalLive", "true", paymentSettings.isPayPalLive()) + " Test "
						+ new RadioTag(context, "payPalLive", "false", !paymentSettings.isPayPalLive())));

				sb
						.append(new AdminRow(
								"PayPal linkback",
								"Use this url as the callback URL for paypal IPN. <i>You must update this if the domain of the site changes, eg, a site moves from build url to live url.</i>",
								new TextTag(context, "none", config.getUrl() + "/" + new Link(PaypalCallbackHandler.class), 40)
										.setReadOnly(true)));

				sb.append("</table>");
			}

            private void payPalDirect() {

                sb.append(new AdminTable("PayPal Direct Payment"));

                sb.append(new AdminRow("PayPal account", "This is the email of your pay pal account.", new TextTag(context, "payPalAccountEmail",
                        paymentSettings.getPayPalAccountEmail(), 40)));

                sb.append(new AdminRow("PayPal account username", "This is the user name of your pay pal account",
                        new TextTag(context, "payPalDirectAccountUsername", paymentSettings.getPayPalDirectAccountUsername(), 40)));

                sb.append(new AdminRow("PayPal account password", "This password is used to log in your pay pal account.",
                        new TextTag(context, "payPalDirectAccountPassword", paymentSettings.getPayPalDirectAccountPassword(), 40)));

                sb.append(new AdminRow("PayPal account signature", "The signature for your pay pal account",
                        new TextTag(context, "payPalDirectAccountSignature", paymentSettings.getPayPalDirectAccountSignature(), 40)));

                sb.append(new AdminRow("PayPal mode", "In test mode all payments are simulated and no money is taken from the cards.", "Live "
                        + new RadioTag(context, "payPalDirectLive", "true", paymentSettings.isPayPalDirectLive()) + " Test "
                        + new RadioTag(context, "payPalDirectLive", "false", !paymentSettings.isPayPalDirectLive())));

                sb.append(new AdminRow("PayPal linkback",
                        "Use this url as the callback URL for paypal IPN. <i>You must update this if the domain of the site changes, eg, a site moves from build url to live url.</i>",
                        new TextTag(context, "none", config.getUrl() + "/" + new Link(PaypalCallbackHandler.class), 40).setReadOnly(true)));

                sb.append("</table>");

            }

            private void payPalExpress() {

                sb.append(new AdminTable("PayPal Express"));

                sb.append(new AdminRow("PayPal account", "This is the email of your pay pal account.", new TextTag(context, "payPalAccountEmail",
                        paymentSettings.getPayPalAccountEmail(), 40)));

                sb.append(new AdminRow("PayPal account username", "This is the user name of your pay pal account",
                        new TextTag(context, "payPalExpressAccountUsername", paymentSettings.getPayPalExpressAcountUsername(), 40)));

                sb.append(new AdminRow("PayPal account password", "This password is used to log in your pay pal account.",
                        new TextTag(context, "payPalExpressAccountPassword", paymentSettings.getPayPalExpressAccountPassword(), 40)));

                sb.append(new AdminRow("PayPal account signature", "The signature for your pay pal account",
                        new TextTag(context, "payPalExpressAccountSignature", paymentSettings.getPayPalExpressAccountSignature(), 40)));

                sb.append(new AdminRow("PayPal mode", "In test mode all payments are simulated and no money is taken from the cards.", "Live "
                        + new RadioTag(context, "payPalExpressLive", "true", paymentSettings.isPayPalExpressLive()) + " Test "
                        + new RadioTag(context, "payPalExpressLive", "false", !paymentSettings.isPayPalExpressLive())));

                sb.append(new AdminRow("PayPal linkback",
                                "Use this url as the callback URL for paypal IPN. <i>You must update this if the domain of the site changes, eg, a site moves from build url to live url.</i>",
                                new TextTag(context, "none", config.getUrl() + "/" + new Link(PaypalCallbackHandler.class), 40)
                                        .setReadOnly(true)));

                sb.append("</table>");
            }

             private void payPalProExpress() {

                sb.append(new AdminTable("PayPal Pro Express"));

                sb.append(new AdminRow("PayPal Pro API username", "Get <b>API username</b> under the Profile section in your account at www.paypal.com",
                        new TextTag(context, "payPalProExpressApiUsername", paymentSettings.getPayPalProExpressApiUsername(), 40)));

                sb.append(new AdminRow("PayPal Pro API password", "Get <b>API password</b> under the Profile section in your account at www.paypal.com",
                        new TextTag(context, "payPalProExpressApiPassword", paymentSettings.getPayPalProExpressApiPassword(), 40)));

                sb.append(new AdminRow("PayPal Pro Signature", "Get <b>SIGNATURE</b> under the Profile section in your account at www.paypal.com",
                        new TextTag(context, "payPalProExpressSignature", paymentSettings.getPayPalProExpressSignature(), 60)));

                sb.append(new AdminRow("PayPal mode", "In test mode all payments are simulated and no money is taken from the cards.", "Live "
                        + new RadioTag(context, "payPalProExpressLive", "true", paymentSettings.isPayPalProExpressLive()) + " Test "
                        + new RadioTag(context, "payPalProExpressLive", "false", !paymentSettings.isPayPalProExpressLive())));

                sb.append("</table>");
            }

            private void payPalProHosted() {

                sb.append(new AdminTable("PayPal Pro Hosted"));

                sb.append(new AdminRow("PayPal Pro Merchant ID", "Value that is specified at the top of the Profile page",
                        new TextTag(context, "payPalProHostedMerchantId", paymentSettings.getPayPalProHostedMerchantId(), 40)));


                sb.append(new AdminRow("PayPal mode", "In test mode all payments are simulated and no money is taken from the cards.", "Live "
                        + new RadioTag(context, "payPalProHostedLive", "true", paymentSettings.isPayPalProHostedLive()) + " Test "
                        + new RadioTag(context, "payPalProHostedLive", "false", !paymentSettings.isPayPalProHostedLive())));

                sb.append("</table>");
            }

            private void processors() {

				sb.append(new AdminTable("Processors"));

				sb.append("<tr>");
				sb.append("<th>Processor name</th>");
				sb.append("<th width='10'> </th>");
				sb.append("</tr>");

				for (Processor processor : Processor.getAll(context)) {
					sb.append("<tr>");
					sb.append("<td>" + new LinkTag(processor.getHandlerClass(), null, new SpannerGif(), "processor", processor) + " "
							+ processor.getName() + "</td>");
					sb.append("<td width='10'>"
							+ new LinkTag(processor.getHandlerClass(), "delete", new DeleteGif(), "processor", processor)
									.setConfirmation("Are you sure you want to delete this processor?") + "</td>");
					sb.append("</tr>");
				}

				sb.append("<tr><td colspan='2'>Add processor: "
						+ new SelectTag(context, "processorClass", null, Processor.processorClasses, "-Select processor-") + " </td></tr>");

				sb.append("</table>");

				sb.append(new AdminTable("Options"));

				sb.append(new AdminRow("Verify card", "Test card details as they are entered.", new BooleanRadioTag(context, "verifyCard",
						paymentSettings.isVerifyCard())));

				sb.append(new AdminRow("Immediate transact", "Bill the card as soon as the order is placd.", new BooleanRadioTag(context,
						"immediateTransact", paymentSettings.isImmediateTransact())));

				sb.append(new AdminRow("Decline emails", "Enter emails here to be notified when a payment is declined.", new TextAreaTag(context,
						"declineEmails", StringHelper.implode(paymentSettings.getDeclineEmails(), "\n"), 40, 4)));

				sb.append("</table>");
			}

            // Protx have changed their name to SagePay
            private void protxForm() {

                String[] types = new String[] { PaymentSettings.PROTX_TXTYPE_PAYMENT, PaymentSettings.PROTX_TXTYPE_DEFERRED,
						PaymentSettings.PROTX_TXTYPE_AUTHORISE };

                sb.append(new AdminTable("SagePay"));

                sb.append(new AdminRow("SagePay tx type", "SagePay transaction type.", new SelectTag(context, "protxPaymentType", paymentSettings
                        .getProtxPaymentType(), types)));

                sb.append(new AdminRow("SagePay vendor name", "This is your unique vendor name as provided by SagePay.", new TextTag(context,
						"protxVendorName", paymentSettings.getProtxVendorName())));

				sb.append(new AdminRow("SagePay encryption password", "The encryption password ensures the data sent from your site to SagePay is secure.",
						new TextTag(context, "protxEncryptionPassword", paymentSettings.getProtxEncryptionPassword())));

				sb.append(new AdminRow("SagePay vendor email", "This is the email SagePay will send payment confirmations to.", new TextTag(context,
						"protxVendorEmail", paymentSettings.getProtxVendorEmail(), 40)));

				sb.append(new AdminRow("SagePay Sandbox", "In Sandbox mode all payments are simulated and no money is taken from the cards.",
                        new BooleanRadioTag(context, "protxSandbox", paymentSettings.isProtxSandbox())));           //todo

                sb.append(new AdminRow("SagePay mode", "In test mode all payments are simulated and no money is taken from the cards.", "Live "
						+ new RadioTag(context, "protxLive", "true", paymentSettings.isProtxLive()) + " Test "
						+ new RadioTag(context, "protxLive", "false", !paymentSettings.isProtxLive())));

				sb.append(new AdminRow("SagePay email",
						"This message will be included at the top of the email that is sent by protx to the customer when a payment is received.",
						new TextAreaTag(context, "protxEmailHeader", paymentSettings.getProtxEmailHeader(), 50, 5)));

				sb.append("</table>");
			}

			private void secCard() {

				sb.append(new AdminTable("Sec Card"));

				sb.append(new AdminRow("SecCard username", "This is your account name as provided by SecPay.", new TextTag(context, "secPayUsername",
						paymentSettings.getSecPayUsername())));

				sb.append(new AdminRow("SecCard password", "This is the digest password as provided by SecPay.", new TextTag(context,
						"secCardPassword", paymentSettings.getSecCardPassword())));

				sb.append(new AdminRow("SecCard mode", "In test mode all payments are simulated and no money is taken from the cards.", "Live "
						+ new RadioTag(context, "secPayLive", "true", paymentSettings.isSecPayLive()) + " Test "
						+ new RadioTag(context, "secPayLive", "false", !paymentSettings.isSecPayLive())));

				sb.append("</table>");

			}

			private void secureTrading() {

				sb.append(new AdminTable("Secure trading"));

				sb.append(new AdminRow("Merchant Id", "This is your unique merchant id provided by secure trading.", new TextTag(context,
						"secureTradingPagesMerchant", paymentSettings.getSecureTradingPagesMerchant())));

				sb.append(new AdminRow("Email", "Enter a single email to receive payment notifications.", new TextTag(context,
						"secureTradingPagesEmail", paymentSettings.getSecureTradingPagesEmail())));

				sb.append(new AdminRow("Mode", "In test mode all payments are simulated and no money is taken from the cards.", "Live "
						+ new RadioTag(context, "secureTradingPagesLive", "true", paymentSettings.isSecureTradingPagesLive()) + " Test "
						+ new RadioTag(context, "secureTradingPagesLive", "false", !paymentSettings.isSecureTradingPagesLive())));

                sb.append(new AdminRow("Successful Callback line number", "Enter a number of line in your callback.txt " +
                        "specified for 'Callbackurl' param. Default value is '1' ", new TextTag(context,
						"secureTradingCallbackLine", paymentSettings.getSecureTradingCallbackLine())));

                sb.append(new AdminRow("Failed Callback line number", "Enter a number of line in your callback-f.txt " +
                        "specified for 'failureurl' param. Default value is '4' ", new TextTag(context,
                        "secureTradingFailedCallbackLine", paymentSettings.getSecureTradingFailedCallbackLine())));

                sb.append(new AdminRow("Formref number", "Enter a number in the end of your file name " +
                        "specified for 'formref' param. This param is optional", new TextTag(context,
                        "secureTradingFormref", paymentSettings.getSecureTradingFormref())));

                sb.append("</table>");
			}

			@Override
			public String toString() {

				paymentTypes();

				sb.append(new FormTag(PaymentSettingsHandler.class, "save", "POST"));

                commands();

				for (PaymentType paymentType : paymentSettings.getPaymentTypes()) {

					switch (paymentType) {

					case CardTerminal:
						break;

					case TwoCheckout:
						twoCheckout();
						break;

					case NoChex:
						nochex();
						break;

					case Test:
						break;

					case Able2Buy:
						able2Buy();
						break;

					case Account:
						break;

					case ProtxForm:
						protxForm();
						break;

					case MetaCharge:
						metaCharge();
						break;

					case Cash:
						break;

					case Cheque:
						cheque();
						break;

					case EpdqCpi:
						epdqCPI();
						break;

					case Offline:
						offline();
						break;

					case HsbcCpi:
						hsbcCpi();
						break;

					case NetPaymentsExpress:
						netPayments();
						break;

					case PayPalStandard:
						payPalStandart();
						break;

                    case PayPalDirect:
                        payPalDirect();
                        break;

                    case PayPalExpress:
                        payPalExpress();
                        break;

                    case PayPalProExpress:
                        payPalProExpress();
                        break;

                    case PayPalProHosted:
                        payPalProHosted();
                        break;

                    case OptimalPayments:
                        optimalPayments();
                        break;

					case WorldPaySelectJunior:
						worldpaySelectJunior();
						break;

					case SecureTradingPages:
						secureTrading();
						break;

					case SecCard:
						secCard();
						break;

					case Transfer:
						transfer();
						break;

                    case GoogleCheckout:
                        googleCheckout();
                        break;

                    case CardsaveRedirect:
                        cardsave();
                        break;

                    case GlobalIris:
                        globalIris();
                        break;
                    }
				}

				if (paymentSettings.contains(PaymentType.CardTerminal)) {
					processors();
				}

				commands();

				sb.append("</form>");

				return sb.toString();
			}

            private void optimalPayments() {

                sb.append(new AdminTable("Optimal Payments"));

                sb.append(new AdminRow("Optimal payments Account ID", "This is your unique account ID.", new TextTag(context,
                        "optimalPaymentsAccountId", paymentSettings.getOptimalPaymentsAccountId())));

                sb.append(new AdminRow("Optimal payments Merchant ID", "This is your unique merchant ID.", new TextTag(
                        context, "optimalPaymentsMerchantId", paymentSettings.getOptimalPaymentsMerchantId())));

                sb.append("</table>");
            }

            private void transfer() {

				sb.append(new AdminTable("Transfer payments"));

				sb.append(new AdminRow("Transfer details",
						"Enter the details a customer needs to complete a bank transfer, such as sort code and account number.", new TextAreaTag(
								context, "transferDetails", paymentSettings.getTransferDetails(), 50, 4)));

				sb.append("</table>");
			}

			private void twoCheckout() {

				sb.append(new AdminTable("2Checkout"));

				sb.append(new AdminRow("2Checkout Vendor Account", "This is your unique account number provided by 2checkout.com.", new TextTag(
						context, "twoCheckoutSid", paymentSettings.getTwoCheckoutSid(), 20)));

				sb.append("</table>");
			}

			private void worldpaySelectJunior() {

				sb.append(new AdminTable("Worldpay select junior"));

				sb.append(new AdminRow("Worldpay installation ID", "This is your unique account ID as provided by Worldpay.", new TextTag(context,
						"worldPayInstallationId", paymentSettings.getWorldPayInstallationId())));

				sb.append(new AdminRow("Worldpay account ID", "Optional value for worldpay, to specify which account this will go in.", new TextTag(
						context, "worldpayAccountId", paymentSettings.getWorldpayAccountId())));

				sb.append(new AdminRow("Worldpay auth mode", "Optional value to specify the auth mode of your transactions.", new TextTag(context,
						"worldpayAuthMode", paymentSettings.getWorldpayAuthMode(), 4)));

				sb.append(new AdminRow("Worldpay callback password", "The password for worldpay server callbacks.", new TextTag(context,
						"worldPayCallbackPassword", paymentSettings.getWorldPayCallbackPassword())));

				sb.append(new AdminRow("Worldpay mode", "In test mode all payments are simulated and no money is taken from the cards.", "Live "
						+ new RadioTag(context, "worldPayLive", "true", paymentSettings.isWorldPayLive()) + " Test "
						+ new RadioTag(context, "worldPayLive", "false", !paymentSettings.isWorldPayLive())));

				sb.append(new AdminRow("Worldpay callback", "Use this url to set up the payment call back in your world account.", config.getUrl()
						+ "/" + new Link(WorldpayCallbackHandler.class)));

				sb.append("</table>");
			}

            private void googleCheckout() {
                sb.append(new AdminTable("Google Checkout"));
                sb.append(new AdminRow("Google Checkout Merchant ID", "To obtain a Merchant ID, you must complete steps 1, 2 and 3 of the "
                        + new LinkTag("http://code.google.com/apis/checkout/developer/Google_Checkout_Basic_HTML_Signing_Up.html","Google Checkout sign-up process."), new TextTag(
						context, "googleCheckoutId", paymentSettings.getGoogleCheckoutId(), 30)));

                sb.append(new AdminRow("Google Checkout Password", "", new TextTag(
                        context, "googleCheckoutPw", paymentSettings.getGoogleCheckoutPw(), 30)));

                sb.append(new AdminRow("Google Checkout mode", "In test mode all payments are simulated and no money is taken from the cards.", "Live "
                        + new RadioTag(context, "googleCheckoutLive", "true", paymentSettings.isGoogleCheckoutLive()) + " Test "
                        + new RadioTag(context, "googleCheckoutLive", "false", !paymentSettings.isGoogleCheckoutLive())));

                sb.append(new AdminRow("Google Checkout callback", "Use this url to set up the payment call back in your google merchant account.", (config.getUrl()
                        + "/" + new Link(GoogleCheckoutCallbackHandler.class)).replace("http://", "https://")));

                sb.append("</table>");
            }

            private void cardsave() {
                sb.append(new AdminTable("Cardsave payment"));
                sb.append(new AdminRow("Gateway account ID", "", new TextTag(
						context, "cardsaveMerchantId", paymentSettings.getCardsaveMerchantId(), 30)));

                sb.append(new AdminRow("Gateway account Password", "", new TextTag(
                        context, "cardsaveMerchantPw", paymentSettings.getCardsaveMerchantPw(), 30)));

                sb.append(new AdminRow("PreSharedKey", "", new TextTag(
                        context, "cardsavePreSharedKey", paymentSettings.getCardsavePreSharedKey(), 30)));

                sb.append(new AdminRow("Cardsave callback", "This url will be used to post transaction data back to the site.", (config.getUrl()
                        + "/" + new Link(CardsaveCallbackHandler.class))));

                sb.append("</table>");
            }

            private void globalIris() {

                sb.append(new AdminTable("Global Iris. RealEx payment"));

                sb.append(new AdminRow("Bank merchant number", "", new TextTag(
						context, "globalirisMerchantId", paymentSettings.getGlobalirisMerchantId(), 30)));

                sb.append(new AdminRow("Sub-account name", "", new TextTag(
						context, "globalirisSubaccountName", paymentSettings.getGlobalirisSubaccountName(), 30)));

                sb.append(new AdminRow("Auto Settle", "The transaction to be captured in the next batch or not",
                        new BooleanRadioTag(context, "globalirisAutoSettle", paymentSettings.isGlobalirisAutoSettle()))
                        .setHelp(new GlobalIrisAutoSettleHelp()));

                sb.append(new AdminRow("Shared secret", "Stored in RealEx database", new PasswordTag(
                        context, "globalirisSecretKey", 30)));

                sb.append(new AdminRow("Realex callback", "Response script URL",
                        ((config.isSecured()? config.getSecureUrl(): config.getUrl()) + "/" + new Link(GlobalIrisRealAuthCallbackHandler.class))).
                        setHelp(new GlobalIrisCallbackHelp()));

                sb.append("</table>");
            }

        });
		return doc;
	}

	public Object removePaymentType() {

		if (paymentType == null) {
			return main();
		}

		paymentSettings.removePaymentType(paymentType);
		paymentSettings.save();

		return main();
	}

	public Object save() {

		// protx form
		paymentSettings.setProtxVendorName(protxVendorName);
		paymentSettings.setProtxVendorEmail(protxVendorEmail);
		paymentSettings.setProtxEncryptionPassword(protxEncryptionPassword);
		paymentSettings.setProtxLive(protxLive);
        paymentSettings.setProtxSandbox(protxSandbox);
		paymentSettings.setProtxEmailHeader(protxEmailHeader);
        paymentSettings.setProxtPaymentType(protxPaymentType);

        // terminal
		paymentSettings.setTerminalAvsSupport(terminalAvsSupport);

		// sec card
		paymentSettings.setSecCardUsername(secPayUsername);
		paymentSettings.setSecCardPassword(secCardPassword);
		paymentSettings.setSecPayLive(secPayLive);

		// offline payments
		paymentSettings.setOfflineContactMessage(offlineContactMessage);

		// secure trading
		paymentSettings.setSecureTradingPagesEmail(secureTradingPagesEmail);
		paymentSettings.setSecureTradingPagesMerchant(secureTradingPagesMerchant);
		paymentSettings.setSecureTradingPagesLive(secureTradingPagesLive);
        paymentSettings.setSecureTradingCallbackLine(secureTradingCallbackLine);
        paymentSettings.setSecureTradingFailedCallbackLine(secureTradingFailedCallbackLine);
        paymentSettings.setSecureTradingFormref(secureTradingFormref);

		// hsbc
		paymentSettings.setHsbcCpiHashKey(hsbcCpiHashKey);
		paymentSettings.setHsbcCpiLive(hsbcCpiLive);
        paymentSettings.setHsbcCpiIris(hsbcCpiIris);
		paymentSettings.setHsbcCpiStorefrontId(hsbcCpiStorefrontId);
        paymentSettings.setHsbcCpiTransactionType(hsbcCpiTransactionType);

		/*
		 * menable
		 */
		paymentSettings.setMEnableAccount(mEnableAccount);
		paymentSettings.setMEnableRequestCode(mEnableRequestCode);
		paymentSettings.setMEnableMpcId(mEnableMpcId);

		// paypal
		paymentSettings.setPayPalAccountEmail(payPalAccountEmail);
		paymentSettings.setPayPalLive(payPalLive);
        paymentSettings.setPayPalExpressLive(payPalExpressLive);
        paymentSettings.setPayPalDirectLive(payPalDirectLive);
        paymentSettings.setPayPalExpressAccountPassword(payPalExpressAccountPassword);
        paymentSettings.setPayPalExpressAccountSignature(payPalExpressAccountSignature);
        paymentSettings.setPayPalExpressAccountUsername(payPalExpressAccountUsername);
        paymentSettings.setPayPalDirectAccountPassword(payPalDirectAccountPassword);
        paymentSettings.setPayPalDirectAccountUsername(payPalDirectAccountUsername);
        paymentSettings.setPayPalDirectAccountSignature(payPalDirectAccountSignature);

        //paypal pro expresss
        paymentSettings.setPayPalProExpressApiUsername(payPalProExpressApiUsername);
        paymentSettings.setPayPalProExpressApiPassword(payPalProExpressApiPassword);
        paymentSettings.setPayPalProExpressSignature(payPalProExpressSignature);
        paymentSettings.setPayPalProExpressLive(payPalProExpressLive);

        //paypal pro hosted
        paymentSettings.setPayPalProHostedMerchantId(payPalProHostedMerchantId);
        paymentSettings.setPayPalProHostedLive(payPalProHostedLive);

        // worldpay
		paymentSettings.setWorldPayLive(worldPayLive);
		paymentSettings.setWorldPayInstallationId(worldPayInstallationId);
		paymentSettings.setWorldPayCallbackPassword(worldPayCallbackPassword);
		paymentSettings.setWorldpayAccountId(worldpayAccountId);
		paymentSettings.setWorldpayAuthMode(worldpayAuthMode);

		// cheque
		paymentSettings.setChequePayee(chequePayee);
		paymentSettings.setChequeAddress(chequeAddress);

		// epdq cpi
		paymentSettings.setEpdqPassphrase(epdqPassphrase);
		paymentSettings.setEpdqStoreId(epdqStoreId);
		paymentSettings.setEpdqPostPassword(epdqPostPassword);
		paymentSettings.setEpdqPostUsername(epdqPostUsername);
		paymentSettings.setEpdqCallbackPassword(epdqCallbackPassword);

		//able2buy
		paymentSettings.setAble2BuyId(able2BuyId);
		paymentSettings.setAble2BuyId2(able2BuyId2);
		paymentSettings.setAble2BuyGoodsCode(able2BuyGoodsCode);
		paymentSettings.setAble2BuyLive(able2BuyLive);
		paymentSettings.setAble2BuyMinSpend(able2BuyMinSpend);
		paymentSettings.setAble2BuyMaxSpend(able2BuyMaxSpend);

		// nochex
		paymentSettings.setNoChexMerchantId(noChexMerchantId);

		// net payments
		paymentSettings.setNetPaymentsExpressAccount(netPaymentsExpressAccount);
		paymentSettings.setNetPaymentsExpressLive(netPaymentsExpressLive);

		// meta charge
		paymentSettings.setMetaChargeInstallId(metaChargeInstallId);

		// transfer
		paymentSettings.setTransferDetails(transferDetails);

		// 2checkout
		paymentSettings.setTwoCheckoutSid(twoCheckoutSid);

		// processor options
		paymentSettings.setVerifyCard(verifyCard);
		paymentSettings.setImmediateTransact(immediateTransact);
		paymentSettings.setDeclineEmails(StringHelper.explodeStrings(declineEmails, "\n"));

        //optomal payments
        paymentSettings.setOptimalPaymentsAccountId(optimalPaymentsAccountId);
        paymentSettings.setOptimalPaymentsMerchantId(optimalPaymentsMerchantId);
        
        //google checkout
        paymentSettings.setGoogleCheckoutId(googleCheckoutId);
        paymentSettings.setGoogleCheckoutPw(googleCheckoutPw);
        paymentSettings.setGoogleCheckoutLive(googleCheckoutLive);

        //cardsave
        paymentSettings.setCardsaveMerchantId(cardsaveMerchantId);
        paymentSettings.setCardsaveMerchantPw(cardsaveMerchantPw);
        paymentSettings.setCardsavePreSharedKey(cardsavePreSharedKey);

        //global iris
        paymentSettings.setGlobalirisMerchantId(globalirisMerchantId);
        paymentSettings.setGlobalirisSubaccountName(globalirisSubaccountName);
        paymentSettings.setGlobalirisAutoSettle(globalirisAutoSettle);
        if (globalirisSecretKey != null) {
            paymentSettings.setGlobalirisSecretKey(globalirisSecretKey);
        }

        paymentSettings.save();

		if (processorClass != null) {
			try {
				Processor processor = (Processor) Class.forName(processorClass).getConstructor(RequestContext.class).newInstance(context);
				processor.save();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}

		addMessage("Payment settings updated");
		clearParameters();
		return main();
	}
}
