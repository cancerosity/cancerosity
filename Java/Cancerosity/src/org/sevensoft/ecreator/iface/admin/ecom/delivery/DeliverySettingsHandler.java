package org.sevensoft.ecreator.iface.admin.ecom.delivery;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.ecom.ShoppingMenu;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.ecom.delivery.DeliverySettings;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;

/**
 * @author sks 03-Oct-2005 14:25:41
 * 
 */
@Path("admin-settings-delivery.do")
public class DeliverySettingsHandler extends AdminHandler {

	private boolean					showFreeDeliveryRequirement;
	private boolean					overrideItemShippingQtys;
	private final transient DeliverySettings	deliverySettings;
	private boolean					estimateDueDate;
	private boolean					charges;
	private boolean					surcharges;
	private boolean					codes;
    private boolean                 taxInc;

	public DeliverySettingsHandler(RequestContext context) {
		super(context);
		this.deliverySettings = DeliverySettings.getInstance(context);
	}

	@Override
	public Object main() {

		if (!Module.Delivery.enabled(context)) {
			return new SettingsMenuHandler(context).main();
		}

		AdminDoc page = new AdminDoc(context, user, "Delivery settings", Tab.Orders);
		page.setMenu(new ShoppingMenu());
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update delivery settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Delivery surcharges",
						"Allows you to enter a delivery surcharge, which is added on per item in addition to the delivery bands.",
						new BooleanRadioTag(context, "surcharges", deliverySettings.isSurcharges())));

				sb
						.append(new AdminRow(
								"Show free delivery requirements",
								"Show a message in the basket telling the customer how much more they must spend / how many more items they must order, for them to qualify for free delivery.",
								new BooleanRadioTag(context, "showFreeDeliveryRequirement", deliverySettings.isShowFreeDeliveryRequirement())));

				sb.append(new AdminRow("Delivery codes", new BooleanRadioTag(context, "codes", deliverySettings.isCodes())));

				//				sb.append(new AdminRow("Override item shipping qtys",
				//						"This lets us set a value on items for shipping qty purposes other than 1. Eg, you might have a "
				//								+ "box of 6 bottles of wine that should count as a 6 qty when calculating shipping by qty.",
				//						new BooleanRadioTag(context, "overrideItemShippingQtys", deliverySettings.isOverrideItemShippingQtys())));

				sb.append(new AdminRow("Estimate due date", "Show the customer an estimated delivery date for their order", new BooleanRadioTag(
						context, "estimateDueDate", deliverySettings.isEstimateDueDate())));

                sb.append(new AdminRow("Free delivery requirements include tax", new BooleanRadioTag(context, "taxInc", deliverySettings.isTaxInc())));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(DeliverySettingsHandler.class, "save", "POST"));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return page;
	}

	public Object save() {

		deliverySettings.setShowFreeDeliveryRequirement(showFreeDeliveryRequirement);
		deliverySettings.setOverrideItemShippingQtys(overrideItemShippingQtys);
		deliverySettings.setEstimateDueDate(estimateDueDate);
		deliverySettings.setSurcharges(surcharges);
		deliverySettings.setCodes(codes);
        deliverySettings.setTaxInc(taxInc);
		deliverySettings.save();

		addMessage("Delivery settings updated");
		clearParameters();
		return main();
	}
}
