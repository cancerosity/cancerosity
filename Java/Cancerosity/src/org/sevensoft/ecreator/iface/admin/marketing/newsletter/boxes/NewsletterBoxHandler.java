package org.sevensoft.ecreator.iface.admin.marketing.newsletter.boxes;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.marketing.newsletter.box.NewsletterBox;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 24 Jul 2006 22:57:10
 *
 */
@Path("admin-boxes-newsletter.do")
public class NewsletterBoxHandler extends BoxHandler {
	
	private NewsletterBox box;

	public NewsletterBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {
	}

}
