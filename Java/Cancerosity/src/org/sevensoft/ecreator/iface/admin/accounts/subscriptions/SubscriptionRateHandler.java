package org.sevensoft.ecreator.iface.admin.accounts.subscriptions;

import javax.servlet.ServletException;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionRate;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 28 May 2006 18:47:29
 *
 */
@Path("admin-subscriptions-rates.do")
public class SubscriptionRateHandler extends AdminHandler {

	private SubscriptionLevel	subscriptionLevel;
	private SubscriptionRate	subscriptionRate;
	private int				period;
	private Money			fee;
	private boolean			introductory;
	private double			vatRate;

	public SubscriptionRateHandler(RequestContext context) {
		super(context);
	}

	public SubscriptionRateHandler(RequestContext context, SubscriptionRate subscription) {
		super(context);
		this.subscriptionRate = subscription;
	}

	public Object create() throws ServletException {

		if (subscriptionLevel == null) {
			return index();
		}

		subscriptionLevel.addSubscriptionPackage();
		return new SubscriptionLevelsHandler(context, subscriptionLevel).edit();
	}

	public Object delete() throws ServletException {

		if (subscriptionRate == null) {
			return index();
		}

		subscriptionLevel = subscriptionRate.getSubscriptionLevel();
		subscriptionLevel.removeSubscriptionPackage(subscriptionRate);

		return new SubscriptionLevelsHandler(context, subscriptionLevel).edit();
	}

	@Override
	public Object main() throws ServletException {

		if (subscriptionRate == null)
			return new DashboardHandler(context).main();

		AdminDoc page = new AdminDoc(context, user, "Subscription rate", null);
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update subscription rate"));

				sb.append(new ButtonTag(SubscriptionRateHandler.class, "delete", "Delete subscription rate", "subscriptionPackage", subscriptionRate)
						.setConfirmation("Are you sure you want to delete this subscription rate?"));

				sb.append(new ButtonTag(SubscriptionLevelsHandler.class, null, "Return to subscription level", "subscriptionLevel", subscriptionRate
						.getSubscriptionLevel()));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Introductory", "Set this to yes if you want this subscription to be a once only introductory offer.",
						new BooleanRadioTag(context, "introductory", subscriptionRate.isIntroductory())));

				sb.append(new AdminRow("Subscription fee", "How much to subscribe.", new TextTag(context, "fee", subscriptionRate.getFee(), 8)));

				sb.append(new AdminRow("Vat rate", "If you need to charge VAT on subscription rates then enter the VAT rate here.", new TextTag(
						context, "vatRate", subscriptionRate.getVatRate(), 8)));

				sb.append(new AdminRow("Subscription period", "How long this subscription runs for.", new TextTag(context, "period", subscriptionRate
						.getPeriod(), 4)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(SubscriptionRateHandler.class, "save", "POST"));
				sb.append(new HiddenTag("subscriptionRate", subscriptionRate));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return page;
	}

	public Object moveDown() throws ServletException {

		if (subscriptionRate == null) {
			return main();
		}

		EntityUtil.moveDown(subscriptionRate, subscriptionRate.getSubscriptionLevel().getSubscriptionRates());
		return new SubscriptionLevelsHandler(context, subscriptionRate.getSubscriptionLevel()).main();
	}

	public Object moveUp() throws ServletException {

		if (subscriptionRate == null) {
			return main();
		}

		EntityUtil.moveUp(subscriptionRate, subscriptionRate.getSubscriptionLevel().getSubscriptionRates());
		return new SubscriptionLevelsHandler(context, subscriptionRate.getSubscriptionLevel()).main();
	}

	public Object save() throws ServletException {

		if (subscriptionRate == null) {
			return main();
		}

		subscriptionRate.setFee(fee);
		subscriptionRate.setPeriod(period);
		subscriptionRate.setIntroductory(introductory);
		subscriptionRate.setVatRate(vatRate);
		subscriptionRate.save();

		addMessage("This subscription rate has been updated with your changes.");
		clearParameters();
		return main();
	}
}
