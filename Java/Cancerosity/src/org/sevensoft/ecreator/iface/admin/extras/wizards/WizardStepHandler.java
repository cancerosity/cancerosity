package org.sevensoft.ecreator.iface.admin.extras.wizards;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.search.wizards.Wizard;
import org.sevensoft.ecreator.model.search.wizards.WizardStep;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 13 Aug 2006 18:15:35
 *
 */
@Path("admin-wizard-steps.do")
public class WizardStepHandler extends AdminHandler {

	private WizardStep	step;
	private Wizard		wizard;
	private Attribute		attribute;
	private String		description;
	private boolean		category;

	public WizardStepHandler(RequestContext context) {
		super(context);
	}

	public Object delete() throws ServletException {

		if (step == null) {
			return main();
		}

		wizard = step.getWizard();
		step.delete();

		return new ActionDoc(context, "This step has been deleted", new Link(WizardStepHandler.class));
	}

	@Override
	public Object main() throws ServletException {

		if (step == null) {
			return new WizardHandler(context).main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit wizard step", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update step"));
				sb.append(new ButtonTag(WizardHandler.class, "edit", "Return to wizard", "wizard", step.getWizard()));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Description", "Enter some text to describe this step", new TextTag(context, "description", step
						.getDescription(), 40)));

				SelectTag attributeTag = new SelectTag(context, "attribute", step.getAttribute());
				attributeTag.setAny("-None selected-");
				attributeTag.addOptions(step.getWizard().getItemType().getAttributes());
				sb.append("<tr><th width='300'><b>Attribute</b></th><td>" + attributeTag + "</td></tr>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(WizardStepHandler.class, "save", "post"));
				sb.append(new HiddenTag("step", step));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (step == null) {
			return main();
		}

		step.setDescription(description);
		step.setAttribute(attribute);
		step.save();

		return new ActionDoc(context, "This step has been updated", new Link(WizardStepHandler.class, null, "step", step));

	}

}
