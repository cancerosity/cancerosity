package org.sevensoft.ecreator.iface.admin.categories.menu;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.ecreator.iface.admin.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.admin.categories.MultipleCategoryHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Menu;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategoryModule;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 13 Oct 2006 14:00:59
 *
 */
public class CategoryMenu extends Menu {

	private Category	category;
	private boolean	superman;

	public CategoryMenu() {
		this(null);
	}

	public CategoryMenu(Category category) {
		this.category = category;

	}

	public List<LinkTag> getLinkTags(RequestContext context) {

		superman = context.containsAttribute("superman");

		List<LinkTag> links = new ArrayList();

		if (category != null) {

			links.add(new LinkTag(CategoryHandler.class, "edit", "Details", "category", category));

			if (category.isChild()) {
				links.add(new LinkTag(CategoryHandler.class, "pricing", "Pricing", "category", category));

			}

			if (!category.hasForwardUrl() && CategoryModule.Headers.enabled(context)) {
				links.add(new LinkTag(CategoryHandler.class, "headers", "Headers and footers", "category", category));

			}

			if (!category.hasForwardUrl() && CategoryModule.Images.enabled(context)) {
				links.add(new LinkTag(CategoryHandler.class, "images", "Images", "category", category));

			}

			if (!category.hasForwardUrl() && Module.WrappedSearches.enabled(context)) {
				links.add(new LinkTag(CategoryHandler.class, "searchWrapper", "Search wrapper", "category", category));

			}

			if (context.containsAttribute("superman")) {
				links.add(new LinkTag(CategoryHandler.class, "css", "Edit category CSS", "category", category));
			}

            if (Module.Comments.enabled(context)) {
                links.add(new LinkTag(CategoryHandler.class, "comments", "Category comments", "category", category));
            }
		}

		links.add(new LinkTag(CategoryHandler.class, "list", "Edit a category"));
		links.add(new LinkTag(CategoryHandler.class, "create", "Create a category"));
//		links.add(new LinkTag(MultipleCategoryHandler.class, null, "Multiple categories editor"));

		return links;
	}
}
