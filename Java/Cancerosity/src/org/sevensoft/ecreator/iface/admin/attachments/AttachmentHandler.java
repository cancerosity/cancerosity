package org.sevensoft.ecreator.iface.admin.attachments;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.containers.boxes.CustomBoxHandler;
import org.sevensoft.ecreator.iface.admin.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.Attachment.Method;
import org.sevensoft.ecreator.model.attachments.AttachmentOwner;
import org.sevensoft.ecreator.model.attachments.AttachmentSettings;
import org.sevensoft.ecreator.model.attachments.previews.PreviewException;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupTag;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.containers.boxes.misc.CustomBox;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.StreamResult;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * @author sks 10-Jan-2006 14:59:36
 */
@Path("admin-attachments.do")
public class AttachmentHandler extends AdminHandler {

    private Attachment attachment;
    private String downloadLinkLabel;
    private int credits;
    private String name;
    private int validityDays;
    private Method method;
    private Markup markup;
    private boolean preview;
    private transient final AttachmentSettings attachmentSettings;

    public AttachmentHandler(RequestContext context) {
        super(context);
        this.attachmentSettings = AttachmentSettings.getInstance(context);
    }

    public Object download() throws ServletException {

        if (attachment == null) {
            return HttpServletResponse.SC_BAD_REQUEST;
        }

        File physicalFile;
        if (preview) {
            physicalFile = ResourcesUtils.getRealAttachment(attachment.getPreviewPath());
        } else {
            physicalFile = ResourcesUtils.getRealAttachment(attachment.getPath());
            if (!physicalFile.exists()) {
                physicalFile = ResourcesUtils.getRealPdf(attachment.getPath());
            }
            if (!physicalFile.exists()) {
                physicalFile = ResourcesUtils.getRealVideo(attachment.getPath());
            }
            if (!physicalFile.exists()) {
                physicalFile = ResourcesUtils.getRealImage(attachment.getPath());
            }
        }

        if (physicalFile.exists()) {
            try {
                return new StreamResult(physicalFile, attachment.getContentType(), attachment.getFilename(), StreamResult.Type.Attachment);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        logger.fine("[AttachmentHandler] file not found=" + physicalFile);
        return HttpServletResponse.SC_BAD_REQUEST;
    }

    public Object edit() throws ServletException {

        if (attachment == null) {
            return index();
        }

        AdminDoc doc = new AdminDoc(context, user, "Edit attachment", null);
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update attachment"));
                sb.append(new ButtonTag(AttachmentHandler.class, "removeAttachment", "Delete this attachment", "attachment", attachment)
                        .setConfirmation("Are you sure you want to delete this attachment?"));

                if (attachment.hasCategory()) {
                    sb.append(new ButtonTag(CategoryHandler.class, "edit", "Return to category", "category", attachment.getCategory()));
                } else if (attachment.hasItem()) {
                    sb.append(new ButtonTag(ItemHandler.class, "edit", "Return to " + attachment.getItem().getItemTypeNameLower(), "item", attachment
                            .getItem()));
                }

                sb.append("</div>");
            }

            private void general() {

                sb.append(new AdminTable("General"));

                sb.append(new AdminRow("Name", "Rename this attachment. By default this is the name of the file.", new TextTag(context, "name",
                        attachment.getName(), 40)));

                sb.append(new AdminRow("Path", "The path on the system to this attachment.", attachment.getPath()));

                if (attachment.hasPreview()) {

                    sb
                            .append(new AdminRow("Preview path", "The path on the system to the preview for this attachment.", attachment
                                    .getPreviewPath()));

                }

                if (isSuperman()) {

                    sb.append(new AdminRow(true, "Generate preview", "Click here to (re)generate the preview file for this attachment",
                            new ButtonTag(AttachmentHandler.class, "makePreview", "Generate preview", "attachment", attachment)));

                }

                SelectTag methodTag = new SelectTag(context, "method", attachment.getMethod());
                methodTag.addOptions(Attachment.Method.values());

                sb.append(new AdminRow("Method", "How you want this attachment to be delivered to the user", methodTag));

                sb.append(new AdminRow("Download link label", "Rename the download link text.", new TextTag(context, "downloadLinkLabel", attachment
                        .getDownloadLinkText(), 40)));

                if (Module.Credits.enabled(context)) {

                    sb.append(new AdminRow("Credits",
                            "Enter the number of credits needed to download this attachment or leave at zero for free download.", new TextTag(
                            context, "credits", attachment.getCredits(), 4)));
                }

                if (attachment.hasCredits()) {

                    sb.append(new AdminRow("Validity days", "Number of days a member can re-download the file before it expires.", new TextTag(
                            context, "validityDays", attachment.getValidityDays(), 4)));

                }

                if (isSuperman() || Module.UserMarkup.enabled(context)) {

                    sb.append(new AdminRow("Markup", new MarkupTag(context, "markup", attachment.getMarkup(),
                            "-Default markup set in attachment settings-")));

                }

                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(AttachmentHandler.class, "save", "POST"));
                sb.append(new HiddenTag("attachment", attachment));

                commands();
                general();
                commands();

                sb.append("</form>");
                return sb.toString();
            }

        });

        return doc;
    }

    @Override
    public Object main() throws ServletException {
        return download();
    }

    public Object makePreview() throws ServletException {

        if (attachment == null) {
            return index();
        }

        try {
            attachment.makePreview(null, true);
            addMessage("Attachment preview generated");
        } catch (PreviewException e) {
            e.printStackTrace();
            addError(e);
        }
        return main();
    }

    public Object removeAttachment() throws ServletException {

        if (attachment == null) {
            return index();
        }

        AttachmentOwner owner = attachment.getOwner();
        owner.removeAttachment(attachment);
        if (owner instanceof Logging) {
            ((Logging) owner).log(user, "Attachment removed: " + attachment.getFilename());
        }

        if (owner instanceof Item) {
            return new ItemHandler(context, (Item) owner).edit();
        } else

        if (owner instanceof Category) {
            return new CategoryHandler(context, (Category) owner).edit();
        } else

        if (owner instanceof CustomBox) {
            return new CustomBoxHandler(context, (CustomBox) owner).main();
        }

        return HttpServletResponse.SC_BAD_GATEWAY;
    }

    public Object save() throws ServletException {

        if (attachment == null)
            return edit();

        attachment.setName(name);
        attachment.setMethod(method);
        attachment.setCredits(credits);
        attachment.setDownloadLinkText(downloadLinkLabel);
        attachment.setValidityDays(validityDays);

        if (Module.UserMarkup.enabled(context) || isSuperman())
            attachment.setMarkup(markup);

        attachment.save();

        addMessage("This attachment has been updated with the changes.");
        clearParameters();
        return edit();
    }
}
