package org.sevensoft.ecreator.iface.admin.design.markup;

import org.jdom.Document;
import org.jdom.Element;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 23 Aug 2006 07:35:13
 *
 */
public class MarkupXmlExporter {

	public Document getDocument(RequestContext context) {

		Element root = new Element("Site");
		Document doc = new Document(root);

		for (Markup markup : Markup.get(context)) {

			Element markupEl = new Element("Markup");
			root.addContent(markupEl);

			markupEl.addContent(new Element("Name").setText(markup.getName()));
			markupEl.addContent(new Element("ContainerClass").setText(markup.getContainerClass()));
			markupEl.addContent(new Element("ContainerId").setText(markup.getContainerId()));
			markupEl.addContent(new Element("Start").setText(markup.getStart()));
			markupEl.addContent(new Element("Start").setText(markup.getStart()));
			markupEl.addContent(new Element("Body").setText(markup.getBody()));
			markupEl.addContent(new Element("End").setText(markup.getEnd()));
			markupEl.addContent(new Element("Between").setText(markup.getBetween()));
			markupEl.addContent(new Element("Tds").setText(String.valueOf(markup.getTds())));

		}

		return doc;
	}
}
