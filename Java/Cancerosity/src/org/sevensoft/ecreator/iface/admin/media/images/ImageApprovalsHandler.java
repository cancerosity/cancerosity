package org.sevensoft.ecreator.iface.admin.media.images;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 15 Dec 2006 15:27:29
 *
 */
@Path("admin-images-approvals.do")
public class ImageApprovalsHandler extends AdminHandler {

	private Img	image;

	public ImageApprovalsHandler(RequestContext context) {
		super(context);
	}

	public Object approve() throws ServletException {

		if (image == null) {
			return main();
		}

		image.approve();
		return main();
	}

	@Override
	public Object main() throws ServletException {

		// get a list of all images awaiting moderation
		final List<Img> images = Img.getModeration(context);

		AdminDoc doc = new AdminDoc(context, user, "Image approval queue", null);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				StringBuilder sb = new StringBuilder();

				if (images.isEmpty()) {

					sb.append("You have no images currently awaiting approval");

				} else {

					sb.append(new AdminTable("Images awaiting approval"));

					sb.append("<tr>");
					sb.append("<th>Name</th>");
					sb.append("<th>Image</th>");
					sb.append("<th>Approve</th>");
					sb.append("<th>Reject</th>");
					sb.append("</tr>");

					for (Img image : images) {

						Item item = (Item) image.getOwner();

						sb.append("<tr>");
						sb.append("<td>" + new LinkTag(ItemHandler.class, null, item.getName(), "item", item) + "</td>");
						sb.append("<td>" + new LinkTag(image.getUrl(), image.getFilename()).setTarget("_blank") + "</td>");
						sb.append("<td>" + new LinkTag(ImageApprovalsHandler.class, "approve", "Approve", "image", image) + "</td>");
						sb.append("<td>"
								+ new LinkTag(ImageApprovalsHandler.class, "reject", "Reject", "image", image)
										.setConfirmation("Are you sure you want to reject this image") + "</td>");
						sb.append("</tr>");
					}

					sb.append("</table>");
				}

				return sb.toString();
			}

		});
		return doc;
	}

	public Object reject() throws ServletException {

		if (image == null) {
			return main();
		}

		image.reject();
		return main();
	}
}
