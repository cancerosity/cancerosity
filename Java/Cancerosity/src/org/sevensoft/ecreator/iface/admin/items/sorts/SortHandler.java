package org.sevensoft.ecreator.iface.admin.items.sorts;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.comparators.EnumStringComparator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.sorts.ItemSort;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 17 Sep 2006 17:36:00
 *
 */
@Path("admin-sort.do")
public class SortHandler extends AdminHandler {

	private ItemSort	sort;
	private boolean	isDefault;
	private String	name;
	private boolean	reverse;
	private SortType	type;
	private ItemType	itemType;
	private Attribute	attribute;

	public SortHandler(RequestContext context) {
		super(context);
	}

	public Object addSort() throws ServletException {

		if (itemType != null) {
			itemType.addSort();
			return new ItemTypeHandler(context, itemType).sorts();
		}

		return main();
	}

	public Object delete() throws ServletException {

		if (sort == null) {
			return main();
		}

		sort.getItemType().removeSort(sort);
		return new ItemTypeHandler(context, sort.getItemType()).sorts();

	}

	public Object edit() throws ServletException {
		return main();
	}

	@Override
	public Object main() throws ServletException {

		if (sort == null) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit sort", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update sort"));
				sb.append(new ButtonTag(SortHandler.class, "delete", "Delete sort", "sort", sort)
						.setConfirmation("If you delete this sort it cannot be un-done. Continue?"));
				sb.append(new ButtonTag(ItemTypeHandler.class, "sorts", "Return to item type", "itemType", sort.getItemType()));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("Sort details"));

				sb.append(new AdminRow("Name", "Enter a user friendly name for this sort", new TextTag(context, "name", sort.getName(), 40)));

				Set<SortType> types = new TreeSet<SortType>(new EnumStringComparator());
				types.addAll(Arrays.asList(SortType.values()));

				sb.append(new AdminRow("Type", new SelectTag(context, "type", sort.getType(), types, "-Choose sort type-")));

				if (sort.getType() == SortType.Attribute) {

					sb.append(new AdminRow("Attribute", new SelectTag(context, "attribute", sort.getAttribute(), sort.getItemType().getAttributes(),
							"-Choose attribute-")));

				}

				sb.append(new AdminRow("Default", "Use this as the default sort in a category when another is not specified", new BooleanRadioTag(
						context, "isDefault", sort.isDefault())));
				sb.append(new AdminRow("Reverse", new BooleanRadioTag(context, "reverse", sort.isReverse())));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(SortHandler.class, "save", "post"));
				sb.append(new HiddenTag("sort", sort));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object moveDown() throws ServletException {

		if (sort == null) {
			return main();
		}

		ItemType itemType = sort.getItemType();
		EntityUtil.moveDown(sort, itemType.getSorts());
		return new ItemTypeHandler(context, itemType).sorts();
	}

	public Object moveUp() throws ServletException {

		if (sort == null) {
			return main();
		}

		ItemType itemType = sort.getItemType();
		EntityUtil.moveUp(sort, itemType.getSorts());
		return new ItemTypeHandler(context, itemType).sorts();
	}

	public Object save() throws ServletException {

		if (sort == null) {
			return main();
		}

		sort.setReverse(reverse);
		sort.setDefault(isDefault);
		sort.setType(type);
		sort.setName(name);
		sort.setAttribute(attribute);
		sort.save();

		clearParameters();
		return main();
	}

}
