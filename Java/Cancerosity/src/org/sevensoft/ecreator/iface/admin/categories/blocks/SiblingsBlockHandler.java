package org.sevensoft.ecreator.iface.admin.categories.blocks;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.iface.admin.misc.html.EditMarkupButton;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.categories.blocks.SubcategoriesBlock;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 26 Jul 2006 10:34:47
 *
 */
@Path("admin-blocks-siblings.do")
public class SiblingsBlockHandler extends BlockEditHandler {

	private SubcategoriesBlock	block;
	private Markup			markup;

	public SiblingsBlockHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Block getBlock() {
		return block;
	}

	@Override
	protected String getTitle() {
		return "Edit siblings block";
	}

	@Override
	protected void saveSpecific() {
		block.setMarkup(markup);
	}

	@Override
	protected void specifics(StringBuilder sb) {

		if (isSuperman() || Module.UserMarkup.enabled(context)) {

			sb.append(new AdminTable("Siblings block details"));

			sb.append(new AdminRow(true, "Markup", null, new SelectTag(context, "markup", block.getMarkup(), Markup.get(context), "-Use default-")
					.setId("markup")
					+ " " + new EditMarkupButton("markup")));

			sb.append("</table>");

		}
	}

}
