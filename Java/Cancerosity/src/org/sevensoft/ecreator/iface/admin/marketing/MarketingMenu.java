package org.sevensoft.ecreator.iface.admin.marketing;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.ecreator.iface.admin.marketing.newsletter.NewsletterComposeHandler;
import org.sevensoft.ecreator.iface.admin.marketing.newsletter.NewsletterSettingsHandler;
import org.sevensoft.ecreator.iface.admin.marketing.newsletter.NewsletterTemplateHandler;
import org.sevensoft.ecreator.iface.admin.marketing.newsletter.NewsletterExampleHandler;
import org.sevensoft.ecreator.iface.admin.marketing.sms.SmsComposeHandler;
import org.sevensoft.ecreator.iface.admin.marketing.sms.SmsSettingsHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Menu;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 4 Sep 2006 17:40:03
 *
 */
public class MarketingMenu extends Menu {

	@Override
	public List<LinkTag> getLinkTags(RequestContext context) {

		List<LinkTag> links = new ArrayList<LinkTag>();

		if (Module.Newsletter.enabled(context)) {

			links.add(new LinkTag(NewsletterComposeHandler.class, null, "Compose newsletter"));

			if (Module.NewsletterTemplates.enabled(context)) {
				links.add(new LinkTag(NewsletterTemplateHandler.class, null, "Newsletter templates"));
			}

            if (Module.NewsletterExamples.enabled(context)) {
				links.add(new LinkTag(NewsletterExampleHandler.class, null, "Newsletter examples"));
			}

			links.add(new LinkTag(NewsletterSettingsHandler.class, null, "Newsletter settings"));
		}

		if (Module.Sms.enabled(context)) {
			links.add(new LinkTag(SmsComposeHandler.class, null, "Compose sms bulletin"));
			links.add(new LinkTag(SmsSettingsHandler.class, null, "Sms bulletin settings"));
		}

		return links;
	}
}
