package org.sevensoft.ecreator.iface.admin.misc.seo.panels;

import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.misc.seo.GoogleSettings;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderLine;
import org.sevensoft.ecreator.model.ecom.payments.Payable;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * User: Tanya
 * Date: 23.05.2011
 */
public class GoogleAnalyticsEcommerceTrackingPanel {

    private RequestContext context;
    private Payable payable;

    public GoogleAnalyticsEcommerceTrackingPanel(RequestContext context, Payable payable) {
        this.context = context;
        this.payable = payable;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        if (Module.GATrackingCodeEcommerce.enabled(context)) {
            sb.append("\n<script type=\"text/javascript\">\n");

            sb.append("  var _gaq = _gaq || [];\n");
            sb.append("  _gaq.push(['_setAccount', '" + GoogleSettings.getInstance(context).getAccount() + "']);\n");
            sb.append("  _gaq.push(['_trackPageview']);\n");
            sb.append("  _gaq.push(['_addTrans',\n");
            sb.append("    '" + payable.getId() + "',\n");           // order ID - required\n" +
            sb.append("    '" + GoogleSettings.getInstance(context).getStoreName() + "',\n");  // affiliation or store name\n" +
            sb.append("    '" + ((Order) payable).getTotalInc() + "',\n");          // total - required\n" +
            sb.append("    '" + ((Order) payable).getTotalVat() + "',\n");           // tax\n" +
            sb.append("    '" + ((Order) payable).getDeliveryChargeInc() + "',\n");              // shipping\n" +
            if (((Order) payable).getDeliveryAddress() != null) {
                sb.append("    '" + ((Order) payable).getDeliveryAddress().getTown() + "',\n");       // city\n" +
                sb.append("    '" + ((Order) payable).getDeliveryAddress().getCounty() + "',\n");     // state or province\n" +
                sb.append("    '" + ((Order) payable).getDeliveryAddress().getCountry() + "' \n");
            } else {
                sb.append("    '',\n'',\n''\n");
            }
            sb.append("  ]);\n\n");

            for (OrderLine line : ((Order) payable).getLines()) {
                // add item might be called for every item in the shopping cart\n" +
                // where your ecommerce engine loops through each item in the cart and\n" +
                // prints out _addItem for each\n" +
                sb.append("  _gaq.push(['_addItem',\n");
                sb.append("    '" + payable.getId() + "',\n");           // order ID - required
                sb.append("    '" + line.getItem().getAttributeValue(GoogleSettings.getInstance(context)
                        .getSkuAttribute(context, line.getItem().getItemType())) + "',\n");           // SKU/code - required\
                sb.append("    '" + line.getItem().getName() + "',\n");        // product name\n" +
                sb.append("    '" + line.getOptionsDetails() + "',\n");   // category or variation\n" +
                sb.append("    '" + line.getUnitSellEx() + "',\n");          // unit price - required\n" +
                sb.append("    '" + line.getQty() + "'\n");               // quantity - required\n" +
                sb.append("  ]);\n");
            }
            sb.append("  _gaq.push(['_trackTrans']);\n\n");    //submits transaction to the Analytics servers

            sb.append("  (function() {\n");
            sb.append("    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;\n");
            sb.append("    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';\n");
            sb.append("    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);\n");
            sb.append("  })();\n\n");

            sb.append("</script>\n");


        }
        return sb.toString();
    }
}
