package org.sevensoft.ecreator.iface.admin.extras.tellfriend;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.extras.tellfriend.TellFriendSettings;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 28 Dec 2006 12:32:30
 *
 */
@Path("admin-tellfriend-settings.do")
public class TellFriendSettingsHandler extends AdminHandler {

	private final transient TellFriendSettings	settings;
	private String						ccEmails;
    private String                      emailBody;
    private String                      emailGoodbye;
    private String                      emailGreeting;

    public TellFriendSettingsHandler(RequestContext context) {
		super(context);
		this.settings = TellFriendSettings.getInstance(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc docs = new AdminDoc(context, user, "Edit tell friend settings", null);
		docs.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("Tell friend settings"));

				sb.append(new AdminRow("CC Emails", "Enter email addresses here to receive an a copy of the email when a page is sent to a friend.",
						new TextAreaTag(context, "ccEmails", StringHelper.implode(settings.getCcEmails(), "\n"), 40, 3)));

				sb.append("</table>");
			}

             private void emailformat() {

                sb.append(new AdminTable("Email format"));

                sb.append(new AdminRow("Email greeting", "Enter the email greeting", new TextAreaTag(context, "emailGreeting", settings.getEmailGreeting(), 60, 10)));

                sb.append(new AdminRow("Email body", "Enter the email body", new TextAreaTag(context, "emailBody", settings.getEmailBody(), 60, 10)));

                sb.append(new AdminRow("Email goodbye", "Enter the email goodbye", new TextAreaTag(context, "emailGoodbye", settings.getEmailGoodbye(), 60, 10)));

                sb.append("</table>");
            }

            @Override
			public String toString() {

				sb.append(new FormTag(TellFriendSettingsHandler.class, "save", "post"));

                commands();
				general();
                emailformat();
                commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return docs;
	}

	public Object save() throws ServletException {

		settings.setCcEmails(StringHelper.explodeStrings(ccEmails, "\n"));
        settings.setEmailBody(emailBody);
        settings.setEmailGoodbye(emailGoodbye);
        settings.setEmailGreeting(emailGreeting);
        settings.save();

		clearParameters();
		return main();
	}

}
