package org.sevensoft.ecreator.iface.admin.items.modules.accessories;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.ItemsMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 5 Jun 2007 07:07:45
 *
 */
@Path("admin-items-accessories.do")
public class AccessoryHandler extends AdminHandler {

	private Item	item;
	private String	name;
	private Item	accessory;

	public AccessoryHandler(RequestContext context) {
		super(context);
	}

	public Object add() throws ServletException {

		if (item == null) {
			return index();
		}

		if (accessory != null) {
			item.addAccessory(accessory);
		}
		
		return new ActionDoc(context, "This accessory has been added", new Link(ItemHandler.class, "edit", "item", item));
	}

	public Object remove() throws ServletException {

		if (item == null) {
			return index();
		}

		if (accessory != null) {
			item.removeAccessory(accessory);
		}

		return new ActionDoc(context, "This accessory has been removed", new Link(ItemHandler.class, "edit", "item", item));
	}

	@Override
	public Object main() throws ServletException {

		if (item == null) {
			return index();
		}

		ItemSearcher searcher = new ItemSearcher(context);
		searcher.setItemModule(ItemModule.Ordering);
		searcher.setName(name);

		searcher.setLimit(20);

		final List<Item> accessories = searcher.getItems();

		AdminDoc doc = new AdminDoc(context, user, "Accessory search results", Tab.getItemTab(item.getItemType()));

		if (miscSettings.isAdvancedMode()) {
			doc.setMenu(new ItemsMenu(item.getItemType()));
		}

		doc.setIntro("These are the matching items for '" + name + "'");
		doc.addBody(new Body() {

            private void results() {
                sb.append(new AdminTable("Accessory search results"));

                for (Item accessory : accessories) {

                    sb.append("<tr>");
                    sb.append("<td>" + new LinkTag(AccessoryHandler.class, "add", accessory.getName(), "item", item, "accessory", accessory) +
                            "</td>");
                    sb.append("</tr>");
                }

                sb.append("</table>");

            }

            private void commands() {
                sb.append(new ButtonTag(ItemHandler.class, "edit", "Cancel", "item", item));
            }

            @Override
            public String toString() {

                commands();
                results();
                commands();

                return sb.toString();
            }
		});
		return doc;

	}
}
