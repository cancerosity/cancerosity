package org.sevensoft.ecreator.iface.admin.categories.blocks;

import java.util.List;

import org.sevensoft.ecreator.help.categories.IncludeSubcategoryItemsHelp;
import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.iface.admin.misc.html.EditMarkupButton;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategorySettings;
import org.sevensoft.ecreator.model.categories.blocks.ItemsBlock;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupTag;
import org.sevensoft.ecreator.model.items.sorts.ItemSort;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 6 Nov 2006 14:05:09
 *
 */
@Path("admin-blocks-items.do")
public class ItemsBlockHandler extends BlockEditHandler {

	private ItemsBlock	block;
	private Markup		listItemMarkup;
	private boolean		multiBuy;
	private int			itemsPerPage;
	private boolean		includeSubcategoryItems;
	private ItemSort		sort;
	private boolean		showSorts;
	private boolean		resultsTop;
	private boolean		resultsBottom;
	private String		noItemsMessage;
	private Markup		subcategoryListItemMarkup;
	private SortType		sortType;
    private boolean excludeFeatured;

	public ItemsBlockHandler(RequestContext context) {
		super(context);
	}

	private void general(StringBuilder sb) {

		CategorySettings categorySettings = CategorySettings.getInstance(context);

		sb.append(new AdminTable("Item options"));

		if (categorySettings.isResultsOverride()) {

			sb.append(new AdminRow("Results bar", "Show results bar above and/or below items.", "Top: "
					+ new CheckTag(context, "resultsTop", true, block.isResultsTop()) + " Bottom: "
					+ new CheckTag(context, "resultsBottom", true, block.isResultsBottom())));

		}

		sb.append(new AdminRow("Multi buy", "All items in this category can be added to the basket at once.", new BooleanRadioTag(context, "multiBuy",
				block.isMultiBuy())));

		sb.append(new AdminRow("Include subcategory items", "Include items from subcategories in addition to items directly in this category.",
				new BooleanRadioTag(context, "includeSubcategoryItems", block.isIncludeSubcategoryItems())).setHelp(new IncludeSubcategoryItemsHelp()));

		if (categorySettings.isItemsPerPageOverride()) {

			sb.append(new AdminRow("Items per page", "Set how many items appear per page.", new TextTag(context, "itemsPerPage", block.getItemsPerPage(),
					4)));
		}

		sb.append(new AdminRow("Show sorts", "Show sort options in this category.", new BooleanRadioTag(context, "showSorts", block.isShowSorts())));

		List<ItemSort> sorts = ItemSort.get(context);
		if (sorts.size() > 0) {
			sb.append(new AdminRow("Default sort", "Use this item sort for initial viewing.", new SelectTag(context, "sort", block.getSort(), sorts,
					"-None-")));
		}

		sb.append(new AdminRow("Generic sort type", "Use this generic sort type for initial viewing.", new SelectTag(context, "sortType", block
				.getSortType(), SortType.getSortTypes(context), "-None-")));

		sb.append(new AdminRow("No items message", "If no items are found, then this message will be displayed instead.", new TextAreaTag(context,
				"noItemsMessage", block.getNoItemsMessage(), 60, 4)));

        if (Module.ListingsFeaturedExclude.enabled(context)) {
            if (categorySettings.isOverrideExclude()) {
                sb.append(new AdminRow("Exclude Featured Items", "Exclude Featured Items from Items blocks across the site.", new BooleanRadioTag(context, "excludeFeatured",
                        block.isExcludeFeatured())));
            }
        }

		sb.append("</table>");
	}

	@Override
	protected Block getBlock() {
		return block;
	}

	private void markup(StringBuilder sb) {

		Category category = getBlock().getOwnerCategory();

		sb.append(new AdminTable("Markup"));

		SelectTag markupTag = new SelectTag(context, "listItemMarkup", block.getListMarkup(), Markup.get(context), "-Default-");
		markupTag.setId("listItemMarkup");

		sb.append(new AdminRow("List item markup", "This markup overrides the list markup from the item type for any items in this category",
				markupTag + " " + new EditMarkupButton("listItemMarkup")));

		sb.append(new AdminRow("Subcategory list item markup",
				"This markup applies to items in subcategories of this category, unless overriden there.", new MarkupTag(context,
						"subcategoryListItemMarkup", category.getSubcategoryListItemMarkup(), "-Default-")));

		sb.append("</table>");

	}

	@Override
	protected void saveSpecific() {

		if (isSuperman() || Module.UserMarkup.enabled(context)) {
			block.setListMarkup(listItemMarkup);
			block.getOwnerCategory().setSubcategoryListItemMarkup(subcategoryListItemMarkup);
		}

		block.setMultiBuy(multiBuy);
		block.setSort(sort);
		block.setIncludeSubcategoryItems(includeSubcategoryItems);
		block.setItemsPerPage(itemsPerPage);
		block.setShowSorts(showSorts);
		block.setResultsBottom(resultsBottom);
		block.setResultsTop(resultsTop);
		block.setSortType(sortType);
		block.setNoItemsMessage(noItemsMessage);
        block.setExcludeFeatured(excludeFeatured);
	}

	/**
	 *
	 */
	@Override
	protected void specifics(StringBuilder sb) {

		general(sb);

		if (isSuperman() || Module.UserMarkup.enabled(context)) {
			markup(sb);
		}
	}
}
