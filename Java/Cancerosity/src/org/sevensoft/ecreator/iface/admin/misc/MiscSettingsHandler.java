package org.sevensoft.ecreator.iface.admin.misc;

import java.io.IOException;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 17-Jun-2005 17:37:55
 * 
 */
@Path("admin-settings-misc.do")
public class MiscSettingsHandler extends AdminHandler {

	private boolean				jimMode, printerFriendly, simpleEditor;
	private String				offlineMessage;
	private int					imageInputs;
	private boolean				titleCase;
	private boolean				hideTrail;
	private boolean				online;
	private boolean				duplicationCheck;
	private boolean				toolbar;
	private transient MiscSettings	miscSettings	= MiscSettings.getInstance(context);
	private boolean				hideCompanyDetailsTab;
	private Upload				upload;
	private int					sessionHourLimit;
	private String				ipBlacklist;
    private String              htmlAttr;

	public MiscSettingsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() {

		AdminDoc doc = new AdminDoc(context, user, "Misc settings", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update misc settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void favicon() {

				sb.append(new AdminTable("Favicon"));
				sb.append(new AdminRow("Upload favicon", "Upload an ICO file for use as your favicon", new FileTag("upload") + " "
						+ new SubmitTag("Upload")));
				sb.append("</table>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				/*
				 * sb.append("<tr><th width='300'><b>Printer friendly pages actions</b><br/>Froogle username provided by google.</th><td>" +
				 * new BooleanRadioTag(null, "quickActions", miscSettings.isQuickActions()) + "</td></tr>");
				 */

				sb.append(new AdminRow("Site visible", "When set to yes this site is active and online for all to see. "
						+ "If you set this to no then only people logged in through the control panel will be able to see the site.",
						new BooleanRadioTag(context, "online", miscSettings.isOnline())));

				sb.append(new AdminRow("Offline message", "Message to display while the site is offline. This can include HTML.", new TextAreaTag(
						context, "offlineMessage", miscSettings.getOfflineMessage(), 60, 4)));

				sb.append(new AdminRow("Simple mode", "Enable the simplified admin system.", new BooleanRadioTag(context, "jimMode", miscSettings
						.isJimMode())));

				sb.append(new AdminRow("Simple editor", "Turn off the HTML editor on all pages", new BooleanRadioTag(context, "simpleEditor",
						miscSettings.isSimpleEditor())));

				sb.append(new AdminRow("Image inputs", "Number of image upload / chooser input boxes to show on items / categories.", new TextTag(
						context, "imageInputs", miscSettings.getImageInputs(), 3)));

				sb.append(new AdminRow("Title case",
						"Converts names / titles of items and categories to title case - where the first letter of each word is capitalised.",
						new BooleanRadioTag(context, "titleCase", miscSettings.isTitleCase())));

				sb.append(new AdminRow("Duplication check", "Does not let you create duplicated items.", new BooleanRadioTag(context,
						"duplicationCheck", miscSettings.isDuplicationCheck())));

				sb.append(new AdminRow("Hide company details tab", "Do not display the menu tab for company details.", new BooleanRadioTag(context,
						"hideCompanyDetailsTab", miscSettings.isHideCompanyDetailsTab())));

				sb.append(new AdminRow("Printer friendly", "Enable printer friendly links on each page.", new BooleanRadioTag(context,
						"printerFriendly", miscSettings.isPrinterFriendly())));

				sb.append(new AdminRow("Toolbar", "Show the admin toolbar along the top of the screen.", new BooleanRadioTag(context, "toolbar",
						miscSettings.isToolbar())));

				sb.append(new AdminRow("Session hour limit",
						"If you enter a value here then users only only be able to see that many pages in a rolling one hour period.", new TextTag(
								context, "sessionHourLimit", miscSettings.getSessionHourLimit(), 6)));

				sb.append(new AdminRow("Reset sessions", "Clear all session data.", new ButtonTag(MiscSettingsHandler.class, "resetSessions",
						"Reset sessions")));

                sb.append(new AdminRow("Ip black list", "Any IPS entered here will be blocked from viewing your site. Please note addition of more " +
                        "that x 10 at any one time can impact your site performance. If you want additional adding, please contact 7 Soft via the support board",
                        new TextAreaTag(context, "ipBlacklist", StringHelper.implode(miscSettings.getIpBlacklist(), "\n"), 40, 5)));

                sb.append(new AdminRow("&lt;HTML&gt; attributes", "This content will be added to &lt;html&gt; tag. Use this setting only if you understand its meaning.",
                        new TextTag(context, "htmlAttr", miscSettings.getHtmlAttr(), 80)));

                sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(MiscSettingsHandler.class, "save", "multi"));

                commands();
				general();
				favicon();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;

	}

	public Object resetSessions() {

		miscSettings.resetSessions();

		return new ActionDoc(context, "Sessions have been reset", new Link(MiscSettingsHandler.class));
	}

	public Object save() {

		miscSettings.setSimpleMode(jimMode);
		miscSettings.setOnline(online);
		miscSettings.setOfflineMessage(offlineMessage);
		miscSettings.setPrinterFriendly(printerFriendly);
		miscSettings.setTitleCase(titleCase);
		miscSettings.setImageInputs(imageInputs);
		miscSettings.setSimpleEditor(simpleEditor);
		miscSettings.setHideTrail(hideTrail);
		miscSettings.setDuplicationCheck(duplicationCheck);
		miscSettings.setHideCompanyDetailsTab(hideCompanyDetailsTab);
		miscSettings.setToolbar(toolbar);
		miscSettings.setSessionHourLimit(sessionHourLimit);
		miscSettings.setIpBlacklist(StringHelper.explodeStrings(ipBlacklist, "\n"));
        miscSettings.setHtmlAttr(htmlAttr);
		miscSettings.save();

		if (upload != null) {

			try {
				upload.write(context.getRealFile("favicon.ico"));
				miscSettings.setFavicon(true);
			} catch (IOException e) {
				addError(e);
			}
		}

		clearParameters();
		return main();
	}
}
