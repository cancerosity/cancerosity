package org.sevensoft.ecreator.iface.admin.reports.members;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.reports.StatsMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.accounts.debug.Login;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 03-Oct-2005 16:12:08
 * 
 */
@Path("admin-reports-members-logins.do")
public class LoginReportHandler extends AdminHandler {

	public LoginReportHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		final List<Login> attempts = Login.get(context, 200);

		AdminDoc page = new AdminDoc(context, user, "Recent logins", Tab.Stats);
		page.setMenu(new StatsMenu());
		page.setIntro("This page shows you details of recent login attempts that were not successful. "
				+ "You can use these details to help your customer who may be having difficulty using the site.");
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Hits daily"));

				sb.append("<tr>");
				sb.append("<td>Date</td>");
				sb.append("<td>IP Address</td>");
				sb.append("<td>Email</td>");
				sb.append("<td>Password</td>");
				sb.append("<td>Successful</td>");
				sb.append("</tr>");

				for (Login attempt : attempts) {

					sb.append("<tr>");
					sb.append("<td>" + attempt.getDate().toString("dd-MMM-yyyy") + "</td>");
					sb.append("<td>" + attempt.getIpAddress() + "</td>");
					sb.append("<td>" + attempt.getEmail() + "</td>");
					sb.append("<td>" + attempt.getPassword() + "</td>");
					sb.append("<td>" + (attempt.isSuccess() ? "Yes" : "No") + "</td>");
					sb.append("</tr>");

				}

				sb.append("</table>");

				return sb.toString();
			}
		});
		return page;
	}

}
