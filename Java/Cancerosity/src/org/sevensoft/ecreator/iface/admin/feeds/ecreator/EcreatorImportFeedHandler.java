package org.sevensoft.ecreator.iface.admin.feeds.ecreator;

import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.feeds.Feed;
import org.sevensoft.ecreator.model.feeds.ecreator.EcreatorImportFeed;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 27 Jan 2007 19:50:33
 *
 */
@Path("admin-feeds-ecreator-import.do")
public class EcreatorImportFeedHandler extends FeedHandler {

	private EcreatorImportFeed	feed;
	private ItemType			itemType;
	private boolean			ignoreCategories;

	public EcreatorImportFeedHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Feed getFeed() {
		return feed;
	}

	@Override
	protected void saveSpecific() {
		feed.setItemType(itemType);
		feed.setIgnoreCategories(ignoreCategories);
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Settings"));

		sb.append(new AdminRow("Item type", "Select the item type to import to.", new SelectTag(context, "itemType", feed.getItemType(), ItemType
				.getSelectionMap(context), "-None set-")));

		sb.append(new AdminRow("Ignore source categories", "Ignore the category information supplied by the feed.", new BooleanRadioTag(context,
				"ignoreCategories", feed.isIgnoreCategories())));

		sb.append("</table>");
	}
}
