package org.sevensoft.ecreator.iface.admin.items;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.AAdminRenderer;
import org.sevensoft.ecreator.model.attributes.renderers.ARenderType;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.ecom.delivery.DeliverySettings;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.listings.Listing;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;
import org.sevensoft.ecreator.model.items.pricing.*;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.model.system.users.privileges.Privilege;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.*;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.date.DateTextTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

import javax.servlet.ServletException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;

/**
 * @author Tanya
 */
@Path("admin-items-multi.do")
public class ItemMultiHandler extends ItemHandler {

    protected static final int CategoryListLimit = 300;
    //    private Map<Integer, List<Upload>> files, imageUploads; //todo populate List<Upload>
    //    private Img image;
    //    private String imageFilename;
    private Map<Integer, String> name; //todo itemId + itemName
    private Map<Integer, String> content; //todo itemId + itemcontent
    //    private Attachment file;
    private List<String> names; //todo
    //    private List<String> imageUrls;
    private Map<Integer, Category> category; //todo
    private Map<Integer, String> status; //todo itemId + itemStatus
    private MultiValueMap<Item, MultiValueMap<Attribute, String>> attributeValues;
    private ItemType itemType;  //todo is common for all the items
    private Map<Integer, Money> ourCostPrice; //todo itemId + itemourCostPrice
    private Map<Integer, Money> rrp; //todo itemId + itemRrp
    private Map<Integer, Double> vatRate; //todo itemId + itemVatRate

    private Map<Integer, String> summary; //todo itemId + itemsummary

//    private Map<Integer, String> attachmentDescriptions;

    private Map<Integer, Category> addCategory;

//    private String friendlyUrl;

    private Money deliveryCharge;

    private final transient DeliverySettings deliverySettings;

    private final transient MiscSettings miscSettings;
    private List<Item> items;
    private Item item;
    private MultiValueMap<Integer, String> priceMap;
    //    private String priceBreaksString;
    private int priceRows;

    private Map<Integer, String> shortName; //todo itemId +itemShortName
    private Map<Integer, Amount> sellPrice; //todo itemId +itemSellPrice

    private int page;

    private final transient Seo seo;
    //    private Money deliverySurcharge;
    private Map<Integer, User> owner; //todo itemId+itemOwner User , populate Map  User value

    private final transient PriceSettings priceSettings;
//    private Money costPrice;

//    private Map<Supplier, Money> costPrices;

//    private String priceBreaks; //todo add

//    private Map<Integer, String> prices;

    private List<Attribute> removeAttributeValues;
    //    private Map<DeliveryOption, Money> deliveryRates;
    //    private List<Upload> videoUploads;
    private Map<Integer, Item> account; //todo itemId + item (listing) accountOwner , populate
    private Map<Integer, Date> expiryDate; //todo itemId + item (listing) expiryDate
    private Map<Integer, Money> collectionCharge; //todo itemId + item collectionCharge
    //    private PriceBand priceBand;
    private Map<Integer, PriceBand> priceBand; //todo  itemId + itemPriceBand, populate Map PriceBand value
    //    private boolean excludeFromGoogleFeed;
    private Map<Integer, ListingPackage> listingPackage; //todo  itemId + listingPackage,

    private int newItemsNumber = 1;

    public ItemMultiHandler(RequestContext context) {
        this(context, null);
    }

    public ItemMultiHandler(RequestContext context, Item item) {
        super(context);
        this.item = item;

        setAttribute("view", new Link(org.sevensoft.ecreator.iface.frontend.items.ItemHandler.class, null, "item", item));

        this.deliverySettings = DeliverySettings.getInstance(context);
        this.miscSettings = MiscSettings.getInstance(context);
        this.seo = Seo.getInstance(context);
        this.priceSettings = PriceSettings.getInstance(context);
    }

    public Object create() throws ServletException {

        if (itemType == null) {
            return index();
        }

        AdminDoc doc = new AdminDoc(context, user, "Create " + itemType.getNameLower(), Tab.getItemTab(itemType));

        final boolean useFilter = SimpleQuery.count(context, Category.class) > 600;
        final int count;

        if (itemType.isCategories()) {
            count = Category.getCount(context);
        } else {
            count = 0;
        }

        // results should be based on count for this name
        final Results results = new Results(count, page, CategoryListLimit);
        final Map<String, String> categoryOptions = Category.getCategoryOptions(context, null, results.getStartIndex(), CategoryListLimit);

        String intro = "Create a new " + itemType.getNameLower();
        if (itemType.isCategories()) {
            intro = intro + " and choose its initial location";
        }
        doc.setIntro(intro);
        Body body = new Body() {

            @Override
            public String toString() {

                if (itemType.isCategories()) {

                    if (useFilter) {

                        sb.append(new FormTag(ItemMultiHandler.class, "create", "GET"));
                        sb.append(new HiddenTag("itemType", itemType));
                        sb.append("<div align='center'>Filter category by name: ").append(new TextTag(context, "name", 20)).append(" ").append(new SubmitTag("Update")).append("</div><br/>");
                        sb.append("</form>");

                    }
                }

                sb.append(new FormTag(ItemMultiHandler.class, "doCreate", "POST"));
                sb.append(new HiddenTag("itemType", itemType));

                sb.append("<div align='center' class='actions'>").append(new SubmitTag("Create " + itemType.getNameLower())).append("</div>");

                //todo  create()     for (int i = 0; i < newItemsNumber; i++) {
                for (int i = 0; i < newItemsNumber; i++) {

                    sb.append(new TableTag("simplebox", "center"));

                    sb.append("<tr><td align='center'>Enter the ").append(itemType.getNameCaption().toLowerCase()).append(" of the ").append(itemType.getNameLower()).append(" to create.</td></tr>");

                    sb.append("<tr><td align='center'>");

                    if (miscSettings.isAdvancedMode()) {
                        sb.append(new TextAreaTag(context, "names", 40, 2));
                    } else {
                        sb.append(new TextTag(context, "names", 40));
                    }

                    sb.append(new ErrorTag(context, "names", "<br/>"));

                    sb.append("<br/><br/></td></tr>");

                    if (itemType.isCategories()) {

                        if (results.hasMultiplePages()) {
                            sb.append("<div align='center'>")
                                    .append(new ResultsControl(context, results, new Link(ItemMultiHandler.class, "create", "itemType", itemType, "name", name)))
                                    .append("</div>");
                        }

                        sb.append("<tr><td align='center'>Where do you want these ").append(itemType.getNamePluralLower()).append(" to appear?</td></tr>");
                        sb.append("<tr><td align='center'>").append(new SelectTag(context, "category_" + i, null, categoryOptions, "-I'll decide later")).append("</td></tr>");
                    }

                    sb.append("</table>");
                }

                sb.append("<div align='center' class='actions'>").append(new SubmitTag("Create " + itemType.getNameLower())).append("</div>");

                sb.append("</form>");
                return sb.toString();

            }

        };
        doc.addBody(body);
        return doc;
    }


    public Object doCreate() throws ServletException {

        test(new RequiredValidator(), "names");
        if (hasErrors()) {
            return create();
        }

        if (itemType == null) {
            return index();
        }

        for (int i = 0; i < names.size(); i++) {
            for (String name : StringHelper.explodeStrings(names.get(i), "\n")) {
                if (miscSettings.isDuplicationCheck() && Item.isNameExisting(context, name)) {

                    addError("Item: " + name + " already exists.");

                } else {

                    item = new Item(context, itemType, name, "Live", user);
                    item.log(user, "Created");

                    if (category.get(i) != null) {
                        item.addCategory(category.get(i));
                    }

                    items.add(item);
                }
            }
        }

        setNewItemsNumber(names.size());

        if (items == null || items.isEmpty()) {
            return main();
        }

        if (miscSettings.isAdvancedMode()) {
            return edit();
        }

        AdminDoc doc = new AdminDoc(context, user, "This " + item.getItemType().getNameLower() + " has been created", getTab());
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new TableTag("simplelinks", "center"));
                sb.append("<tr><th align='center'>What would you like to do ?</th></tr>");

                if (Privilege.EditAllItems.yes(user, context) || Privilege.EditOwnerItems.yes(user, context) ||
                        Privilege.EditCreatedItems.yes(user, context)) {

                    sb
                            .append("<tr><td align='center'>" +
                                    new LinkTag(ItemMultiHandler.class, "edit", "I want to edit this " + itemType.getNameLower(), "item", item) +
                                    "</td></tr>");

                }

                if (Privilege.CreateItem.yes(user, context)) {

                    sb.append("<tr><td align='center'>" +
                            new LinkTag(ItemMultiHandler.class, "create", "I want to create another " + item.getItemType().getNameLower(), "itemType",
                                    itemType) + "</td></tr>");
                }

                sb.append("</table>");

                return sb.toString();
            }
        });
        return doc;
    }

    public Object edit() throws ServletException {

        if (items == null || items.isEmpty()) {
            return main();
        }

        boolean access = false;
        if (Privilege.EditAllItems.yes(user, context)) {
            access = true;
        }
//todo access add
//        if (!access && user.equals(item.getCreatedBy())) {
//            if (Privilege.EditCreatedItems.yes(user, context))
//                access = true;
//        }
//
//        if (!access && user.equals(item.getOwner())) {
//            if (Privilege.EditOwnerItems.yes(user, context))
//                access = true;
//        }
//
//        if (!access) {
//            logger.fine("[ItemHandler] access denied");
//            return index();
//        }

        setAttribute("view", getView());

        AdminDoc doc = new AdminDoc(context, user, "Edit ", getTab());
//        doc.setIntro("This " + item.getItemType().getNameLower() + " was created on " + item.getDateCreated().toString("dd MMM yyyy HH:mm"));

        itemType = item.getItemType();

        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                String lowerName = item.getItemType().getName().toLowerCase();

                sb.append(new SubmitTag("Update " + lowerName));

                sb.append("</div>");
            }

            private void content() {

                sb.append(new AdminTable("Content"));

                sb.append("<tr><td colspan='2'>" +
                        new TextAreaTag(context, "content_" + item.getId(), item.getContent()).setId("content").setStyle("width: 100%; height: 220px;") +
                        "</td></tr>");

                if (item.getItemType().isSummaries()) {

                    sb.append(new AdminRow("Summary", "Enter your own summary to override the automatically generated summary.", new TextAreaTag(
                            context, "summary_" + item.getId(), item.getSummary(), 50, 4)));
                }

                sb.append("</table>");
            }

            private void general() {

                sb.append(new AdminTable("General"));

                {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(new TextTag(context, "name_" + item.getId(), item.getName(), 60));

                    sb.append(new AdminRow(item.getItemType().getNameCaption(), "Enter the name or title of your " +
                            item.getItemTypeNameLower(), sb2));
                }

                if (item.getItemType().isShortNames()) {

                    sb.append(new AdminRow("Short name", "A shortened version of the item name for use when the space is limited.", new TextTag(
                            context, "shortName_" + item.getId(), item.getShortName(), 40)));
                }

                if (item.getItemType().isOwners()) {

                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(new SelectTag(context, "owner_" + item.getId(), item.getOwner(), User.getActive(context), "-No owner-"));

                    sb.append(new AdminRow("Owner", "This is which user this item is assigned to.", sb2));

                }

                if (item.getItemType().isStatusControl()) {

                    SelectTag tag = new SelectTag(context, "status_" + item.getId(), item.getStatus());
                    tag.addOption("LIVE");
                    tag.addOption("DISABLED");
                    tag.addOption("DELETED");

                    sb.append(new AdminRow("Status", "Set the status of this " + item.getItemType().getNameLower() + ".", tag));

                }

                if (item.getItemType().isCategories()) {

                    SelectTag tag;
                    int n = 0;
                    for (Category category : item.getCategories()) {

                        StringBuilder sb2 = new StringBuilder();

                        if (category.isRoot()) {

                            sb2.append(new LinkTag(CategoryHandler.class, "edit", category.getName(), "category", category));

                        } else {

                            Iterator<Category> iter = category.getTrail().iterator();
                            while (iter.hasNext()) {

                                Category trail = iter.next();

                                sb2.append(new LinkTag(CategoryHandler.class, "edit", trail.getName(), "category", trail));

                                if (iter.hasNext()) {
                                    sb2.append(" > ");
                                }
                            }
                        }

                        sb2.append("</td></tr>");

                        sb.append(new AdminRow("Category " + n, null, sb2));
                        n++;
                    }

                    int count = Category.getCount(context);
                    final Results results = new Results(count, page, 500);
                    final Map<String, String> categoryOptions = Category.getCategoryOptions(context, null, results.getStartIndex(), 500);

                    Link link = new Link(ItemMultiHandler.class, "edit", "item", item);

                    tag = new SelectTag(context, "addCategory_" + item.getId());
                    tag.setAny("-Add to a new category-");
                    tag.addOptions(categoryOptions);

                    StringBuilder sb2 = new StringBuilder();
                    if (results.hasMultiplePages()) {
                        sb2.append(new ResultsControl(context, results, link));
                    }

                    sb2.append(tag);

                    if (n < item.getItemType().getMaxCategories() || item.getItemType().getMaxCategories() == 0) {
                        sb.append(new AdminRow("Category " + n, "Choose a category to add", sb2));
                    }
                }

                if (ItemModule.Account.enabled(context, item)) {

                    PriceSettings priceSettings = PriceSettings.getInstance(context);
                    if (priceSettings.isUsePriceBands()) {

                        List<PriceBand> bands = PriceBand.get(context);
                        if (bands.size() > 0) {

                            sb.append(new AdminRow("Price band", null, new SelectTag(context, "priceBand_" + item.getId(), item.getPriceBand(), bands,
                                    "Standard pricing")));
                        }
                    }
                }

                sb.append("</table>");
            }

            private void listing() {

                sb.append(new AdminTable("Listing"));

                final Listing listing = item.getListing();

                SelectTag ownerAccountTag = new SelectTag(context, "account_" + item.getId(), item.getAccount(), Item.getAccountsForOptions(context), "-No owner account-");
                sb.append(new AdminRow("Account owner", "This is the account that is assigned to this " + item.getItemType().getNameLower(), ownerAccountTag));

                SelectTag listingPackageTag = new SelectTag(context, "listingPackage_" + item.getId(), item.getListing().getListingPackage(), ListingPackage.get(context), "-No listing package selected-");
                sb.append(new AdminRow("Listing package", "This is the listing package that is assigned to this " + item.getItemType().getNameLower(), listingPackageTag));

                DateTextTag expiryDateTag = new DateTextTag(context, "expiryDate_" + item.getId(), listing.getExpiryDate());
                sb.append(new AdminRow("Listing expiry date", "Set to blank if you want this listing to not expire.", expiryDateTag));

                sb.append("</table>");
            }

            private void attributes() {
                List<Attribute> attributes = item.getAttributes();
                List<Attribute> attributesWithoutDuplicates = new ArrayList<Attribute>();
                for (Attribute attribute : attributes) {
                    if (!attribute.isDublicate()) {
                        attributesWithoutDuplicates.add(attribute);
                    }
                }
//                sb.append(new HiddenTags("attributeValuess", item.getAttributeValues()));
                sb.append(new AAdminRenderer(context, item, attributesWithoutDuplicates, item.getAttributeValues(), ARenderType.Input, item));
            }

            private void pricing() {

                boolean adv = PriceUtil.isAdvancedPricing(item, context);
                PriceSettings priceSettings = PriceSettings.getInstance(context);
                Pricing pricing = item.getPricing();

                sb.append(new AdminTable("Pricing"));

                if (!adv) {

                    final Price price = item.getStandardPrice();
                    String priceString = PriceUtil.getPriceString(price);
                    sb.append(new AdminRow("Price", "Enter the price you sell this item for.", new TextTag(context, "sellPrice_" + item.getId(), priceString, 12)));
                }

                if (item.getItemType().isRrp()) {
                    sb.append(new AdminRow(item.getItemType().getRrpCaption(),
                            "Set a recommended retail price and the site will display your discount.", new TextTag(context, "rrp_" + item.getId(), item.getRrp()
                                    .toEditString(), 12)));
                }

                if (item.getItemType().isVat()) {
                    sb.append(new AdminRow("Vat rate", "Set the current vat rate.", new TextTag(context, "vatRate_" + item.getId(), item.getVatRate(), 8)));
                }

                if (priceSettings.isCostPricing()) {
                    sb.append(new AdminRow("Cost price", "Sets your cost price for this item.", new TextTag(context, "ourCostPrice_" + item.getId(), item
                            .getCostPrice(), 8)));
                }

                if (priceSettings.isCollectionCharges()) {

                    if (config.getUrl().contains("blacknround")) {

                        sb.append(new AdminRow("Fitting charge", "Enter a cost to be added when this item is fitted.", new TextTag(context,
                                "collectionCharge_" + item.getId(), pricing.getCollectionCharge(), 12)));

                    } else {

                        sb.append(new AdminRow("Collection charges", "Enter a cost to be added when this item is collected.", new TextTag(context,
                                "collectionCharge_" + item.getId(), pricing.getCollectionCharge(), 12)));
                    }
                }

                sb.append("</table>");

                //todo EditPricesRenderer add
//                if (adv) {
//                    sb.append(new EditPricesRenderer(context, item));
//                }
            }


            //todo edit body toString
            @Override
            public String toString() {

//                if (miscSettings.isHtmlEditor() && item.getItemType().isContent() && !item.getItemType().isSimpleEditor()) {
//                    RendererUtil.tinyMce(context, sb, "content");
//                }

                sb.append(new FormTag(ItemMultiHandler.class, "save", "multi").setId("items_form"));

                for (Item i : items) {
                    sb.append(new HiddenTag("items", i.getId()));

                }

                sb.append(new HiddenTag("itemType", itemType));

                commands(); //todo

                for (Item it : items) {
                    item = it;

                    sb.append(new HiddenTag("selectImage", "false").setId("selectimage_toggle"));

                    general();

                    if (Module.Listings.enabled(context)) {
                        if (ItemModule.Listings.enabled(context, item)) {
                            listing();    //todo
                        }
                    }

                    //todo attributes
                    attributes();

                    if (Module.Pricing.enabled(context) && ItemModule.Pricing.enabled(context, item)) {

                        pricing();  //todo

                    }


//todo attachmentPanel add
//                if (ItemModule.Attachments.enabled(context, item)) {
//                    sb.append(new AdminAttachmentsPanel(context, item, true));
//                }


//todo MetaTagsPanel add
//                if (seo.isOverrideTags()) {
//                    if (itemType.isOverrideTags()) {
//                        sb.append(new MetaTagsPanel(context, item));
//                    }
//                }
//


                    if (item.getItemType().isContent()) {
                        content();  //todo
                    }
//todo ImageAdminPanel
//                if (ItemModule.Images.enabled(context, item)) {
//                    sb.append(new ImageAdminPanel(context, item));
//                }
//
                    //todo VideoAdminPanel
//                if (ItemModule.Videos.enabled(context, item)) {
//                    sb.append(new VideoAdminPanel(context, item));
//                }
                }
                commands(); //todo

                sb.append("</form>");
                return sb.toString();
            }


        });
        return doc;
    }


    public Object removeCategory() throws ServletException {

        if (category == null || item == null) {
            return edit();
        }

//        item.removeCategory(category); //todo
        return edit();
    }


    public Object save() throws ServletException {
        for (Item item : items) {
            Integer key = item.getId();
//            test(new RequiredValidator(), "name");

            // test attribute for errors
//            for (Attribute attribute : item.getAttributes()) {
//                test(new AttributeValidator(attribute), attribute.getParamName());
//            }

            if (hasErrors()) {
                addError("Please correct the errors and click update");
                return edit();
            }

            if (item == null) {
                return main();
            }

            if (item.setName(name.get(key))) {
                item.log(user, "Name changed to '" + name + "'.");
            }
            item.setShortName(shortName.get(key));


            if (isSuperman() && itemType != null) {
                item.setItemType(itemType);
            }

            if (item.getItemType().isStatusControl() && status != null && status.get(key) != null) {
                if (!item.getStatus().equalsIgnoreCase(status.get(key))) {
                    item.setStatus(status.get(key));
                    item.log(user, "Status changed to " + status.get(key));
                }
            }


            //todo add category
            if (addCategory != null && !addCategory.isEmpty() && addCategory.get(key) != null) {
                item.addCategory(addCategory.get(key));
                item.log(user, "Category added: #" + addCategory.get(key).getIdString() + " '" + addCategory.get(key).getName() + "'");
            }


            // content
            if (!ObjectUtil.equal(item.getContent(), content.get(key))) {
                item.setContent(content.get(key));
                if (content.get(key) == null) {
                    item.log("Content cleared");
                } else {
                    item.log(user, "Content changed, new content is " + content.get(key).split("\n").length + " lines");
                }
            }
            item.setSummary(summary.get(key));

            item.setOwner(owner.get(key));

            if (ItemModule.Pricing.enabled(context, item)) {

                boolean adv = PriceUtil.isAdvancedPricing(item, context);
                Pricing pricing = item.getPricing();

                logger.fine("[ItemHandler] setting vatRate=" + vatRate.get(key));
                logger.fine("[ItemHandler] setting rrp=" + rrp.get(key));
                logger.fine("[ItemHandler] setting simple sell price=" + sellPrice.get(key));

                item.setOurCostPrice(ourCostPrice.get(key));
                item.setRrp(rrp.get(key));

                pricing.setCollectionCharge(collectionCharge.get(key));
                pricing.save();

                if (vatRate.get(key) != item.getVatRate()) {
                    item.setVatRate(vatRate.get(key), true);
                    item.log(user, "Vat rate set to " + vatRate.get(key));
                }
                //todo save pricebreaks
//                if (adv) {
//
//                    item.setPriceBreaks(StringHelper.explodeIntegers(priceBreaks.get(key), "\\D"));
//                    PriceUtil.savePrices(context, item, prices);
//
//                } else {

                item.setStandardPrice(sellPrice.get(key));
                logger.fine("[Item] setting standard price to " + sellPrice.get(key));

//                }
            }

            if (ItemModule.Account.enabled(context, item)) {
                item.setPriceBand(priceBand.get(key));
            }

            item.save();

            // listings
            if (ItemModule.Listings.enabled(context, item)) {

                item.setAccount(account.get(key));
                item.getListing().setListingPackage(listingPackage.get(key));
                item.save();

                Listing listing = item.getListing();
                listing.setExpiryDate(expiryDate.get(key));
                listing.save();

            }

            item.log(user, "Item details updated");

            // attributes


            item.setAttributeValues(attributeValues.get(item));
            for (Attribute attribute : removeAttributeValues) {
                item.removeAttributeValues(attribute);
            }

            clearParameters();
        }
        return new ActionDoc(context, "This " + itemType.getName().toLowerCase() + " has been updated.", new Link(ItemMultiHandler.class, "create", "itemType", itemType, "newItemsNumber", 5));

    }

    @Override
    protected Tab getTab() {
        return Tab.getItemTab(itemType);
    }

    public ItemMultiHandler setItemType(ItemType itemType) {
        this.itemType = itemType;
        return this;
    }

    public ItemMultiHandler setNewItemsNumber(int newItemsNumber) {
        this.newItemsNumber = newItemsNumber;
        return this;
    }
}
