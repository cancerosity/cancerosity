package org.sevensoft.ecreator.iface.admin.accounts;

import java.util.logging.Logger;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemTypeMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.accounts.AccountModule;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupTag;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sam
 */
@Path("admin-accounts-module.do")
public class AccountModuleHandler extends AdminHandler {

	@SuppressWarnings("hiding")
	private static Logger	logger	= Logger.getLogger(AccountModuleHandler.class.getName());

	private Markup		accountMarkup;

	private ItemType		itemType;

	private boolean		showAccountId;

	private boolean		hideAccount;

	private String		restrictionContent;

	private String		emailErrorMessage;

	private String		emailRegExp;

	private String		inactiveMessage;

	private String		loginForwardUrl;

	private boolean		showAccount;

	private int			limitedItemViews;

	private String		limitedItemViewsForward;

	private boolean		showLastAccessTime;

	private String		logoutForwardUrl;

	private String		profileEditHeader;

	private String		profileEditFooter;

	private String		profileImagesFooter;

	private String		profileImagesHeader;

	private String		accountHeader;

	private String		accountFooter;

    private boolean     notChangeEmailAfterRegistered;

	public AccountModuleHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (itemType == null) {
			return index();
		}

		final AccountModule module = itemType.getAccountModule();

		AdminDoc doc = new AdminDoc(context, user, "Account module", null);
		doc.setMenu(new EditItemTypeMenu(itemType));
		doc.addBody(new Body() {

			private void account() {

                sb.append(new AdminTable("Account"));

				sb.append("<tr><th colspan='2'>Account page header</th></tr><tr><td colspan='2'>" +
						new TextAreaTag(context, "accountHeader", module.getAccountHeader(), 70, 8) + "</td></tr>");

				sb.append("<tr><th colspan='2'>Account page footer</th></tr><tr><td colspan='2'>" +
						new TextAreaTag(context, "accountFooter", module.getAccountFooter(), 70, 8) + "</td></tr>");

				if (Module.UserMarkup.enabled(context) || isSuperman()) {
					sb.append(new AdminRow("Account markup", new MarkupTag(context, "accountMarkup", module.getAccountMarkup(), "-Default-")));
				}

				sb.append("</table>");
			}

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update account module"));
				sb.append(new ButtonTag(ItemTypeHandler.class, "edit", "Return to item type", "itemType", itemType));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Show control panel", "Show links to the account control panel.", new BooleanRadioTag(context, "showAccount",
						module.isShowAccount())));

				sb.append(new AdminRow("Show account Id", "Show the account ID as part of the  name", new BooleanRadioTag(context, "showAccountId",
						module.isShowAccountId())));

				sb.append(new AdminRow("Show last access time", "Show the last active time when viewing this account in admin.", new BooleanRadioTag(
						context, "showLastAccessTime", module.isShowLastAccessTime())));

				sb.append(new AdminRow("Email reg exp", "A regular expression to check validity of entered email address. "
						+ "This reg exp is an extra constraint in addition to the normal email format check.", new TextTag(context, "emailRegExp",
						module.getEmailRegExp(), 60)));

				sb.append(new AdminRow("Email error message", "Customise the error message that appears if the user enters a non valid email",
						new TextTag(context, "emailErrorMessage", module.getEmailErrorMessage(), 60)));

				sb.append(new AdminRow("Inactive message",
						"This message will be shown to a customer when they try to login to an account that is inactive.", new TextAreaTag(context,
								"inactiveMessage", module.getInactiveMessage(), 40, 2)));

                sb.append(new AdminRow("Don't allow email address change after registered", "Once a user has registered on the site, when they login to " +
                        "their account and choose to edit their profile this option will determine if they are allowed to edit the email address used for " +
                        "the account.", new BooleanRadioTag(context, "notChangeEmailAfterRegistered", module.isNotChangeEmailAfterRegistered())));

				sb.append("</table>");
			}

			private void login() {

				sb.append(new AdminTable("Login / Logout settings"));

				sb.append(new AdminRow("Login forward url", "Enter an url here to forward users to when they log in.", new TextTag(context,
						"loginForwardUrl", module.getLoginForwardUrl(), 60)));

				sb.append(new AdminRow("Logout forward url", "Enter an url here to forward users to when they log out rather than the standard page.",
						new TextTag(context, "logoutForwardUrl", module.getLogoutForwardUrl(), 60)));

				sb.append("</table>");
			}

			private void profile() {

				sb.append(new AdminTable("Profile page"));

				sb.append("<tr><th colspan='2'>Profile page header</th></tr><tr><td colspan='2'>" +
						new TextAreaTag(context, "profileEditHeader", module.getProfileEditHeader(), 70, 8) + "</td></tr>");

				sb.append("<tr><th colspan='2'>Profile page footer</th></tr><tr><td colspan='2'>" +
						new TextAreaTag(context, "profileEditFooter", module.getProfileEditFooter(), 70, 8) + "</td></tr>");

				sb.append("<tr><th colspan='2'>Profile images header</th></tr><tr><td colspan='2'>" +
						new TextAreaTag(context, "profileImagesHeader", module.getProfileImagesHeader(), 70, 8) + "</td></tr>");

				sb.append("<tr><th colspan='2'>Profile images footer</th></tr><tr><td colspan='2'>" +
						new TextAreaTag(context, "profileImagesFooter", module.getProfileImagesFooter(), 70, 8) + "</td></tr>");

				sb.append("</table>");
			}

			private void restriction() {

				sb.append(new AdminTable("General"));

				sb.append("<tr><td>Restriction content</td></tr>");
				sb.append("<tr><td>" +
						new TextAreaTag(context, "restrictionContent", module.getRestrictionContent()).setId("restrictionContent").setStyle(
								"width: 100%; height: 320px;") + "</td></tr>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				RendererUtil.tinyMce(context, sb, "restrictionContent");

				sb.append(new FormTag(AccountModuleHandler.class, "save", "POST"));
				sb.append(new HiddenTag("itemType", itemType));

                commands();

				general();
				account();
				login();
				profile();

				restriction();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (itemType == null) {
			return index();
		}

		AccountModule module = itemType.getAccountModule();

		module.setShowAccount(showAccount);
		module.setShowAccountId(showAccountId);
		module.setShowLastAccessTime(showLastAccessTime);

		module.setEmailRegExp(emailRegExp);
		module.setEmailErrorMessage(emailErrorMessage);
		module.setInactiveMessage(inactiveMessage);
		module.setLoginForwardUrl(loginForwardUrl);
		module.setLogoutForwardUrl(logoutForwardUrl);
        module.setNotChangeEmailAfterRegistered(notChangeEmailAfterRegistered);

		module.setProfileEditHeader(profileEditHeader);
		module.setProfileEditFooter(profileEditFooter);

		// 
		module.setRestrictionContent(restrictionContent);

		module.setProfileImagesHeader(profileImagesHeader);
		module.setProfileImagesFooter(profileImagesFooter);

		// account page
		module.setAccountFooter(accountFooter);
		module.setAccountHeader(accountHeader);

		if (Module.UserMarkup.enabled(context) || isSuperman()) {
			module.setAccountMarkup(accountMarkup);
		}

		module.save();

		return new ActionDoc(context, "This account module has been updated", new Link(AccountModuleHandler.class, null, "itemType", itemType));
	}
}
