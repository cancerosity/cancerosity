package org.sevensoft.ecreator.iface.admin.sitemap;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.misc.html.EditMarkupButton;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.model.sitemap.SitemapSettings;
import org.sevensoft.ecreator.model.sitemap.SitemapCategory;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;

import javax.servlet.ServletException;
import java.util.List;
import java.util.Map;

/**
 * User: Tanya
 * Date: 16.11.2010
 */

@Path("admin-sitemap-settings.do")
public class SitemapSettingsHandler extends AdminHandler {

    private transient SitemapSettings sitemapSettings;

    private Markup markup;
    private boolean showItems;
    private boolean showHiddenCategories;

    private transient List<SitemapCategory> actualCategories;
    private Category category;
    private List<SitemapCategory> categoriesToDelete;
    private Category addCategory;

    private transient Map<String, String> categoryOptions;


    public SitemapSettingsHandler(RequestContext context) {
        super(context);
        this.sitemapSettings = SitemapSettings.getInstance(context);
        this.actualCategories = SitemapCategory.getSitemapCategories(context);

        this.categoryOptions = Category.getCategoryOptions(context);

        for (SitemapCategory actualCategory : actualCategories) {
            categoryOptions.remove(actualCategory.getCategory().getIdString());
        }
    }

    public Object main() throws ServletException {
        AdminDoc doc = new AdminDoc(context, user, "Sitemap Settings", null);
        doc.addBody(new Body() {

            @Override
            public String toString() {
                sb.append(new FormTag(SitemapSettingsHandler.class, "save", "POST"));
                sb.append(new HiddenTag("sitemapSettings", sitemapSettings));

                commands();

                general();
                categories();

                commands();

                sb.append("</form>");

                return sb.toString();
            }

            private void commands() {
                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update listing package"));
//                sb.append(new ButtonTag(SitemapSettingsHandler.class, "remove", "Remove selected"/*, "category", category*/)
                sb.append(new SubmitTag("Remove selected")
                        .setConfirmation("Are you sure you want to remove selected categories?"));

                sb.append("</div>");
            }

            private void general() {
                sb.append(new AdminTable("General"));

                sb.append(new AdminRow(true, "Sitemap Markup", null, new SelectTag(context, "markup", sitemapSettings.getMarkup(), Markup.get(context), "-Use default-")
                        .setId("sitemapMarkup") + " " + new EditMarkupButton("sitemapMarkup")));

                sb.append(new AdminRow(true, "Show Items", "Set Yes to display items of selected categories", new BooleanRadioTag(context, "showItems",
						sitemapSettings.isShowItems())));

                sb.append(new AdminRow("Show hidden categories", "Set Yes to display hidden categories in sitemap", new BooleanRadioTag(context, "showHiddenCategories",
						sitemapSettings.isShowHiddenCategories())));

                sb.append("<table>");
            }

            private void categories() {

                sb.append(new AdminTable("Sitemap categories"));

                CheckTag tag = new CheckTag(context, "check", "");
                tag.setAlign("absmiddle");
                tag.setOnClick("function checkAll(checked) {\n" +
                        "   elem=document.getElementsByName('categoriesToDelete');\n" +
                        "   for (i=0; i< elem.length; i++){\n" +
                        "       elem[i].checked = checked;\n" +
                        "   }\n" +
                        "}\n" +
                        "checkAll(this.checked);");


                sb.append("<tr>");
                sb.append("<th>Delete " + tag + "</th>");
                sb.append("<th>Category</th>");
                sb.append("<th>Category Full Path</th>");
                sb.append("</tr>");

                for (SitemapCategory sitemapCategory : actualCategories) {
                    Category category = sitemapCategory.getCategory();
                    sb.append("<tr>");
                    sb.append("<td width='30'>" + new CheckTag(context, "categoriesToDelete", sitemapCategory, false) + "</td>");
                    sb.append("<td>" + category.getName());
                    sb.append("</td>");
                    sb.append("<td>" + category.getFullName());
                    sb.append("</td>");
                    sb.append("</tr>");
                }

                sb.append("<tr>");
                sb.append("<td colspan='3'>");

                SelectTag addCategoryTag = new SelectTag(context, "addCategory");
                addCategoryTag.setAny("-- Choose category --");
                addCategoryTag.addOptions(categoryOptions);
                sb.append("Choose a category to add to the list:  " + addCategoryTag + new SubmitTag("Add Category"));

                sb.append("</td>");
                sb.append("</tr>");
                sb.append("<tr>");
                sb.append("<td colspan='3'>");
                sb.append(new ButtonTag(SitemapSettingsHandler.class, "addAllCategories", "Add all categories"));
                sb.append("</td>");
                sb.append("</tr>");


                sb.append("</table>");
            }

        });
        return doc;
    }

    public Object save() throws ServletException {

        if (addCategory != null) {
            new SitemapCategory(context, addCategory);

            return new ActionDoc(context, "The category has been added", new Link(SitemapSettingsHandler.class));
        }

        if (!categoriesToDelete.isEmpty()) {
            for (SitemapCategory sitemapCategory : categoriesToDelete) {
                sitemapCategory.delete();
            }

            return new ActionDoc(context, "Selected categories have been removed", new Link(SitemapSettingsHandler.class));
        }

        if (sitemapSettings != null) {
            sitemapSettings.setMarkup(markup);
            sitemapSettings.setShowItems(showItems);
            sitemapSettings.setShowHiddenCategories(showHiddenCategories);
            sitemapSettings.save();
        }

        context.clearParameters();

        return main();
    }

//    public Object remove() throws ServletException {
//        return main();
//    }

    public Object addAllCategories() throws ServletException {
        List<Category> categoryList = Category.get(context, false);
        List<Category> actual = SitemapCategory.getCategories(context);

        categoryList.removeAll(actual);

        for (Category category : categoryList) {
            new SitemapCategory(context, category);
        }

        return new ActionDoc(context, "All categories have been added", new Link(SitemapSettingsHandler.class));
    }
}
