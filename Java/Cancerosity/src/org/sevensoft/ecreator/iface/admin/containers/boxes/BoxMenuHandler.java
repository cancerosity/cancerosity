package org.sevensoft.ecreator.iface.admin.containers.boxes;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.containers.boxes.shortcuts.BoxShortcut;
import org.sevensoft.ecreator.model.design.template.TemplateSettings;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;
import java.lang.reflect.Constructor;
import java.util.List;

/**
 * @author sks 9 Jul 2006 09:25:43
 */
@Path("admin-boxes-menu.do")
public class BoxMenuHandler extends AdminHandler {

    private String addBox;
    private String panel;

    public BoxMenuHandler(RequestContext context) {
        super(context);
    }

    public Object create() throws ServletException {

        if (panel == null) {
            panel = "Right";
        }

        if (addBox == null) {
            return main();
        }

        logger.fine("[BoxMenuHandler] creating box, addBox=" + addBox + ", panel=" + panel);

        try {

            String[] string = addBox.split(";");

            Class<?> clazz = Class.forName(string[0]);
            if (BoxShortcut.class.isAssignableFrom(clazz)) {

                try {

                    BoxShortcut shortcut = (BoxShortcut) clazz.newInstance();
                    shortcut.install(context, panel);

                } catch (InstantiationException e) {
                    e.printStackTrace();
                    addError(e);

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    addError(e);
                }

            } else {

                Constructor constructor = clazz.getDeclaredConstructor(RequestContext.class, String.class);
                constructor.setAccessible(true);

                logger.fine("using constructor=" + constructor);

                Box box = (Box) constructor.newInstance(new Object[]{context, panel});

                if (string.length == 2) {
                    box.setObjId(Integer.parseInt(string[1]));
                }

                box.log(user, "Created");
            }

        } catch (Exception e) {
            logger.fine(e.toString());
            throw new RuntimeException(e);

        }

        clearParameters();
        return new ActionDoc(context, "This sidebox has been created", new Link(BoxMenuHandler.class));
    }

    @Override
    public final Object main() throws ServletException {

        final List<Box> boxes = Box.get(context);

        AdminDoc doc = new AdminDoc(context, user, "Boxes", null);
        doc.addBody(new Body() {

            private void boxes(String panel) {

                sb.append(new AdminTable(panel));

                sb.append("<tr>");
                sb.append("<th width='60'>Position</th>");
                sb.append("<th width='140' colspan='2'>Location</th>");
                sb.append("<th>Name</th>");
                sb.append("<th width='20'></th>");
                sb.append("</tr>");

                PositionRender pr = new PositionRender("box");
                for (Box box : Box.filter(boxes, panel)) {

                    sb.append("<tr>");
                    sb.append("<td width='60'>" + pr.render(box.getEditHandler(), box) + "</td>");
                    sb.append("<td width='100'>" + box.getPanel() + "</td>");
                    sb.append("<td width='140'>" + new ButtonTag(box.getEditHandler(), "switchLocation", "Switch", "box", box) + "</td>");
                    sb.append("<td>" + new LinkTag(box.getEditHandler(), null, new ImageTag("files/graphics/admin/spanner.gif"), "box", box) + " "
                            + box.getName() + "</td>");
                    sb.append("<td width='20'>"
                            + new LinkTag(box.getEditHandler(), "delete", new DeleteGif(), "box", box)
                            .setConfirmation("Are you sure you want to delete this box?") + "</td>");
                    sb.append("</tr>");

                }

                SelectTag addBoxTag = new SelectTag(context, "addBox");
                addBoxTag.setAny("-Choose box type-");
                addBoxTag.setLabelComparator();
                addBoxTag.addOptions(Box.getOptions(context));
                addBoxTag.addOptions(BoxShortcut.getShortcutClasses());

                sb.append(new FormTag(BoxMenuHandler.class, "create", "get"));
                sb.append(new HiddenTag("panel", panel));
                sb.append("<tr>");
                sb.append("<td colspan='5'>Add new sidebox: " + addBoxTag + " " + new SubmitTag("Add box") + "</td></tr>");
                sb.append("</form>");

                sb.append("</form>");
                sb.append("</table>");
            }

            @Override
            public String toString() {

                for (String panel : TemplateSettings.getInstance(context).getPanels()) {
                    boxes(panel);
                }

                return sb.toString();
            }

        });
        return doc;
    }
}
