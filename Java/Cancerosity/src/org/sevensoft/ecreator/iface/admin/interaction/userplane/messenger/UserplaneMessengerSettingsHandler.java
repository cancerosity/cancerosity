package org.sevensoft.ecreator.iface.admin.interaction.userplane.messenger;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.frontend.interaction.userplane.messenger.UserplaneMessengerActionHandler;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.interaction.userplane.messenger.UserplaneMessengerSettings;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 26 Nov 2006 14:52:34
 *
 */
@Path("admin-userplane-msg-settings.do")
public class UserplaneMessengerSettingsHandler extends AdminHandler {

	private final transient UserplaneMessengerSettings	userplaneMsgSettings;

	private String							domainID;
	private String							flashcomServer;
	private String							imagesServer;
	private String							textZoneId;
	private String							bannerZoneId;
	private Attribute							profileAttribute4;
	private Attribute							profileAttribute3;
	private Attribute							profileAttribute2;
	private Attribute							profileAttribute1;

	public UserplaneMessengerSettingsHandler(RequestContext context) {
		super(context);
		this.userplaneMsgSettings = UserplaneMessengerSettings.getInstance(context);
	}

	@Override
	public Object main() {

		if (!Module.UserplaneMessenger.enabled(context))
			return new DashboardHandler(context).main();

		AdminDoc page = new AdminDoc(context, user, "Userplane messenger settings", Tab.Extras);
		page.setMenu(new ExtrasMenu());
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update userplane settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb
						.append(new AdminRow(
								"XML handler url",
								"This is the url to the script that handles the XML callbacks for Userplane messenger. This needs to be sent to Userplane for integration.",
								new TextTag(context, "none", config.getUrl() + "/" + new Link(UserplaneMessengerActionHandler.class), 50)
										.setReadOnly(true)));

				sb.append(new AdminRow("Flashcom server", new TextTag(context, "flashcomServer", userplaneMsgSettings.getFlashcomServer(), 40)));

				sb.append(new AdminRow("Images server", new TextTag(context, "imagesServer", userplaneMsgSettings.getImagesServer(), 40)));

				sb.append(new AdminRow("Domain ID", new TextTag(context, "domainID", userplaneMsgSettings.getDomainID(), 20)));

				sb.append(new AdminRow("Banner Zone ID", new TextTag(context, "bannerZoneId", userplaneMsgSettings.getBannerZoneId(), 4)));

				sb.append(new AdminRow("Text Zone ID", new TextTag(context, "textZoneId", userplaneMsgSettings.getTextZoneId(), 4)));

				sb.append(new AdminRow("Profile attribute 1", "Choose the attribute to display as line 1 on the messenger window", new SelectTag(
						context, "profileAttribute1", userplaneMsgSettings.getProfileAttribute1(), Attribute.getSelectionMap(context), "-Select attribute-")));

				sb.append(new AdminRow("Profile attribute 2", "Choose the attribute to display as line 2 on the messenger window", new SelectTag(
						context, "profileAttribute2", userplaneMsgSettings.getProfileAttribute2(), Attribute.getSelectionMap(context), "-Select attribute-")));

				sb.append(new AdminRow("Profile attribute 3", "Choose the attribute to display as line 3 on the messenger window", new SelectTag(
						context, "profileAttribute3", userplaneMsgSettings.getProfileAttribute3(), Attribute.getSelectionMap(context), "-Select attribute-")));

				sb.append(new AdminRow("Profile attribute 4", "Choose the attribute to display as line 4 on the messenger window", new SelectTag(
						context, "profileAttribute4", userplaneMsgSettings.getProfileAttribute4(), Attribute.getSelectionMap(context), "-Select attribute-")));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(UserplaneMessengerSettingsHandler.class, "save", "POST"));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return page;
	}

	public Object save() {

		userplaneMsgSettings.setImagesServer(imagesServer);
		userplaneMsgSettings.setFlashcomServer(flashcomServer);
		userplaneMsgSettings.setDomainID(domainID);
		userplaneMsgSettings.setBannerZoneId(bannerZoneId);
		userplaneMsgSettings.setTextZoneId(textZoneId);
		userplaneMsgSettings.setProfileAttribute1(profileAttribute1);
		userplaneMsgSettings.setProfileAttribute2(profileAttribute2);
		userplaneMsgSettings.setProfileAttribute3(profileAttribute3);
		userplaneMsgSettings.setProfileAttribute4(profileAttribute4);
		userplaneMsgSettings.save();

		clearParameters();

		addMessage("Userplane messenger settings updated");
		clearParameters();
		return main();
	}
}
