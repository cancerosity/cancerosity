package org.sevensoft.ecreator.iface.admin.misc;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 29 Jan 2007 09:38:20
 *
 */
@Path("ajax-item-search.do")
public class AjaxItemSearchHandler extends Handler {

	private static final Logger	logger	= Logger.getLogger("ecreator");
	private String			q;

	public AjaxItemSearchHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Object init() throws ServletException {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		logger.fine("[AjaxItemSearchHandler] searching q=" + q);

		ItemSearcher searcher = new ItemSearcher(context);
		searcher.setName(q);
		searcher.setLimit(10);

		List<String> list = new ArrayList<String>();
		for (Item item : searcher.getItems()) {

			list.add(item.getIdString());
			list.add("#" + item.getIdString() + " " + item.getName());
		}

		return StringHelper.implode(list, "\n");
	}

	@Override
	protected boolean runSecure() {
		return false;
	}

}
