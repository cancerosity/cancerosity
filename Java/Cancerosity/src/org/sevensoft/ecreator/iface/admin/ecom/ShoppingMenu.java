package org.sevensoft.ecreator.iface.admin.ecom;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.ecreator.iface.admin.ecom.branches.BranchesHandler;
import org.sevensoft.ecreator.iface.admin.ecom.delivery.DeliveryOptionsHandler;
import org.sevensoft.ecreator.iface.admin.ecom.delivery.DeliverySettingsHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.BulkOrderHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.OrderSearchHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.sync.ExportOrdersHandler;
import org.sevensoft.ecreator.iface.admin.ecom.promotions.PromotionHandler;
import org.sevensoft.ecreator.iface.admin.ecom.vouchers.VoucherHandler;
import org.sevensoft.ecreator.iface.admin.ecom.vouchers.VoucherStatsHandler;
import org.sevensoft.ecreator.iface.admin.ecom.removal.OrderRemovalHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Menu;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 13 Sep 2006 11:22:31
 *
 */
public class ShoppingMenu extends Menu {

	public List<LinkTag> getLinkTags(RequestContext context) {

		List<LinkTag> links = new ArrayList();

		links.add(new LinkTag(OrderSearchHandler.class, null, "Find orders"));
		links.add(new LinkTag(ExportOrdersHandler.class, null, "Export orders"));
		links.add(new LinkTag(BulkOrderHandler.class, null, "Bulk Order Processing"));
		
		if (Module.Delivery.enabled(context)) {
			links.add(new LinkTag(DeliveryOptionsHandler.class, null, "Delivery options"));
			links.add(new LinkTag(DeliverySettingsHandler.class, null, "Delivery settings"));
		}

		if (Module.Promotions.enabled(context)) {
			links.add(new LinkTag(PromotionHandler.class, null, "Promotions"));
		}

		if (Module.Vouchers.enabled(context)) {
			links.add(new LinkTag(VoucherHandler.class, null, "Vouchers"));
		}

        if (Module.ListingVouchers.enabled(context)) {
            links.add(new LinkTag(VoucherStatsHandler.class, null, "Vouchers stats"));
        }

		if (Module.Branches.enabled(context)) {
			links.add(new LinkTag(BranchesHandler.class, null, "Branches"));
		}

        if (Module.OrdersRemoval.enabled(context)) {
            links.add(new LinkTag(OrderRemovalHandler.class, null, "Order removal"));
        }

		return links;
	}
}
