package org.sevensoft.ecreator.iface.admin.items.modules.ratings;

import java.util.Map;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.model.extras.ratings.RatingModule;
import org.sevensoft.ecreator.model.extras.ratings.RatingType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 11 Aug 2006 14:15:10
 *
 */
@Path("admin-ratings-types.do")
public class RatingTypeHandler extends AdminHandler {

	private RatingType		ratingType;
	private int				scale;
	private String			name;
	private Map<Integer, String>	graphics;

	public RatingTypeHandler(RequestContext context) {
		super(context);
	}

	public Object delete() throws ServletException {

		if (ratingType == null)
			return main();

		RatingModule ratingModule = ratingType.getRatingModule();
		ratingModule.removeRatingType(ratingType);
		return new ItemTypeHandler(context, ratingModule.getItemType()).ratings();
	}

	@Override
	public Object main() throws ServletException {

		if (ratingType == null) {
			return new ItemTypeHandler(context).main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit rating type", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update rating type"));
				sb
						.append(new ButtonTag(ItemTypeHandler.class, "ratings", "Return to item type", "itemType", ratingType.getRatingModule()
								.getItemType()));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new TableTag("form", 1, 0).setCaption("Rating type"));

				sb.append("<tr><th width='300'><b>Name</b></th><td>" + new TextTag(context, "name", ratingType.getName(), 40) + "</td></tr>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(RatingTypeHandler.class, "save", "post"));
				sb.append(new HiddenTag("ratingType", ratingType));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (ratingType == null)
			return main();

		ratingType.setName(name);
		ratingType.save();

		addMessage("This rating type has been updated with your changes");
		return main();
	}
}
