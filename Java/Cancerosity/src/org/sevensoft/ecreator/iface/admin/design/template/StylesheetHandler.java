package org.sevensoft.ecreator.iface.admin.design.template;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.template.TemplateUtil;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.StringResult;

import javax.servlet.ServletException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author sks 25 Oct 2006 17:50:11
 */
@Path("admin-templates-stylesheet.do")
public class StylesheetHandler extends AdminHandler {

    private Map<String, String> stylesheet;
    private String styles, path, image;
    private List<Upload> images;

    public StylesheetHandler(RequestContext context) {
        super(context);
    }

    public StylesheetHandler(RequestContext context, Category category) {
        super(context);
    }

    public Object exportStyleSheet() throws ServletException {

        if (!isSuperman() && !Module.UserMarkup.enabled(context))
            return index();

        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : TemplateUtil.getStyleSheet(context).entrySet()) {
            sb.append(entry.getKey());
            sb.append(":");
            sb.append(entry.getValue());
            sb.append("\n");
        }

        return new StringResult(sb, "text/plain");

    }

    public Object importStyleSheet() throws ServletException {

        if (!isSuperman() && !Module.UserMarkup.enabled(context))
            return index();

        if (styles == null)
            return main();

        Map<String, String> stylesheet = StringHelper.explodeMap(styles, "\n", ":");

        try {

            TemplateUtil.saveStyleSheet(context, stylesheet);
            addMessage("Styles imported");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            addError(e);

        } catch (IOException e) {
            e.printStackTrace();
            addError(e);
        }

        return main();

    }

    @Override
    public Object main() {

        if (!isSuperman() && !Module.UserMarkup.enabled(context))
            return new DashboardHandler(context).main();

        AdminDoc page = new AdminDoc(context, user, "Stylesheet", null);
        page.addBody(new Body() {

            private void colors() {

                sb.append(new FormTag(StylesheetHandler.class, "saveStyleSheet", "POST"));
                sb.append(new AdminTable("Style sheet"));
                sb.append("<tr><th width='150'>Name</th><th>Value</th></tr>");

                for (Map.Entry<String, String> entry : TemplateUtil.getStyleSheet(context).entrySet()) {

                    sb.append("<tr>");
                    sb.append("<td width='150'>" + entry.getKey() + "</td>");

                    sb.append("<td>" + new TextTag(null, "stylesheet_" + entry.getKey(), entry.getValue(), 12));
                    sb.append(" <span style='background: " + entry.getValue() + ";'>&nbsp; &nbsp;</span>");
                    sb.append("</td></tr>");
                }

                sb.append("<tr><td colspan='3'>" + new SubmitTag("Save style sheet") + "</td></tr>");
                sb.append("</table>");
                sb.append("</form>");

            }

            private void export() {

                sb.append(new AdminTable("Style Sheet Management"));

                sb.append(new FormTag(StylesheetHandler.class, "importStyleSheet", "post"));
                sb.append("<tr>");
                sb.append("<th>Import</th>");
                sb.append("<td>" + new TextAreaTag(context, "styles", 50, 3) + "<br/>" + new SubmitTag("Import") + "</td>");
                sb.append("</tr>");
                sb.append("</form>");

                sb.append(new FormTag(StylesheetHandler.class, "exportStyleSheet", "get"));
                sb.append("<tr>");
                sb.append("<th>Export</th>");
                sb.append("<td>" + new SubmitTag("Export") + "</td>");
                sb.append("</tr>");
                sb.append("</form>");

                sb.append("</table>");

            }

            @Override
            public String toString() {

                colors();
                export();

                return sb.toString();
            }

        });
        return page;
    }

    public Object saveStyleSheet() {

        if (!isSuperman() && !Module.UserMarkup.enabled(context))
            return main();

        try {

            TemplateUtil.saveStyleSheet(context, stylesheet);
            addMessage("Stylesheet updated");

        } catch (IOException e) {
            e.printStackTrace();
            addError(e);
        }

        return main();
    }

    public Object uploadImage() {

        if (!isSuperman())
            return new DashboardHandler(context).main();

        if (image == null)
            return main();

        for (Upload upload : images) {

            try {
                upload.write(ResourcesUtils.getRealTemplateData(image));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return main();
    }

}