package org.sevensoft.ecreator.iface.admin.design.template;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.design.template.panels.TemplatesUploadPanel;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.model.design.template.TemplateUtil;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.media.images.ImageUtil;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.results.StreamResult;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.activation.MimetypesFileTypeMap;
import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

/**
 * @author sks 10 Oct 2006 10:08:21
 */
@Path("admin-templates-data.do")
public class TemplateDataHandler extends AdminHandler {

    private Map<String, Upload> uploads;
    private Map<String, Upload> logouploads;
    private List<String> filenames;
    private int hue;
    private String filename;

    private List<Upload> imageUploads;
    private String imageFilename;
    private List<String> imageUrls;

    private String downloadFileName;

    public TemplateDataHandler(RequestContext context) {
        super(context);
    }

    public Object delete() throws ServletException {

        if (filename == null) {
            return main();
        }
//		new File(config.getRealImagesPath() + "/" + filename)
        ResourcesUtils.getRealTemplateData(filename).delete();
        return main();
    }

    public Object deleteLogo() throws ServletException {

        if (filename == null) {
            return main();
        }
        ResourcesUtils.getRealEmailLogo(filename).delete();
        return main();
    }

    @Override
    public Object main() throws ServletException {

        if (!isSuperman() && !Module.UserMarkup.enabled(context)) {
            return index();
        }

        AdminDoc doc = new AdminDoc(context, user, "Template settings", null);
        doc.addBody(new Body() {

            private void images() {

                sb.append(new AdminTable("Images"));

                for (String filename : TemplateUtil.getImages(context)) {

                    sb.append("<tr>");
                    //					sb.append("<td>" + new CheckTag(context, "filenames", filename, false) + "</td>");
                    sb.append("<td  width='130px'>" + filename + "<br><br>" + new FileTag("uploads_" + filename) + "</td>");
                    sb.append("<td width='10px'>" + new LinkTag(TemplateDataHandler.class, "delete", new DeleteGif(), "filename", filename)
                            .setConfirmation("Are you sure you want to delete this file?") + "</td>");

                    sb.append("<td>" + new ImageTag(Config.TemplateDataPath + "/" + filename) + "</td>");

                    sb.append("</tr>");
                }
                sb.append("</table>");

            }

            private void notImages() {
                sb.append(new AdminTable("Files"));

                for (String filename : TemplateUtil.getNotImages(context)) {

                    sb.append("<tr>");
                    sb.append("<td width='130px'>" + filename + "<br><br>" + new FileTag("uploads_" + filename) + "</td>");
                    sb.append("<td width='10px'>" + new LinkTag(TemplateDataHandler.class, "delete", new DeleteGif(), "filename", filename)
                            .setConfirmation("Are you sure you want to delete this file?") + "</td>");

                    sb.append("<td>" + new ImageTag("files/graphics/admin/attachment.png") +
                            new LinkTag(TemplateDataHandler.class, "download", filename, "downloadFileName", filename) + "</td>");

                    sb.append("</tr>");
                }

                sb.append("</table>");
            }

            private void upload() {
                sb.append(new TemplatesUploadPanel(context));
            }

            private void emailLogo() {

                sb.append(new AdminTable("Emails LOGO"));

                for (String filename : TemplateUtil.getLogo(context)) {

                    sb.append("<tr>");
                    //					sb.append("<td>" + new CheckTag(context, "filenames", filename, false) + "</td>");
                    sb.append("<td  width='130px'>" + filename + "<br><br>" + new FileTag("logouploads_" + filename) + "</td>");
                    sb.append("<td width='10px'>" + new LinkTag(TemplateDataHandler.class, "deleteLogo", new DeleteGif(), "filename", filename)
                            .setConfirmation("Are you sure you want to delete this file?") + "</td>");

                    sb.append("<td>" + new ImageTag(Config.EmailLogoPath + "/" + filename) + "</td>");

                    sb.append("</tr>");
                }
                sb.append("</table>");

            }

            @Override
            public String toString() {
                sb.append(new FormTag(TemplateDataHandler.class, "save", "multi"));

                images();
                notImages();
                emailLogo();

                sb.append(new SubmitTag("Update"));
                sb.append("</br>");

                upload();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object download() throws ServletException {

        if (downloadFileName == null) {
            return HttpServletResponse.SC_BAD_REQUEST;
        }

        File physicalFile = ResourcesUtils.getRealTemplateData(downloadFileName);

        if (physicalFile.exists()) {
            try {
                return new StreamResult(physicalFile, new MimetypesFileTypeMap().getContentType(physicalFile), physicalFile.getName(), StreamResult.Type.Attachment);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        logger.fine("[AttachmentHandler] file not found=" + physicalFile);
        return HttpServletResponse.SC_BAD_REQUEST;
    }

    public Object save() throws ServletException {

        if (!isSuperman() && !Module.UserMarkup.enabled(context)) {
            return index();
        }

        logger.fine("uploading: " + uploads);

        for (Map.Entry<String, Upload> entry : uploads.entrySet()) {

            String filename = entry.getKey();
            Upload upload = entry.getValue();

            logger.fine("processing, filename=" + filename + ", upload=" + upload);

            try {
                upload.write(ResourcesUtils.getRealTemplateData(filename));
            } catch (IOException e) {
                e.printStackTrace();
                logger.warning(e.toString());
            }

        }

        logger.fine("uploading: " + logouploads);

        for (Map.Entry<String, Upload> entry : logouploads.entrySet()) {

            String filename = entry.getKey();
            Upload upload = entry.getValue();

            logger.fine("processing, filename=" + filename + ", upload=" + upload);

            try {
                upload.write(ResourcesUtils.getRealEmailLogo(filename));
            } catch (IOException e) {
                e.printStackTrace();
                logger.warning(e.toString());
            }

        }

        if (imageFilename != null) {

            try {
                ImageUtil.copyImageTo(imageFilename, ResourcesUtils.getRealTemplateDataDir());
            } catch (IOException e) {
                e.printStackTrace();
                logger.warning(e.toString());
            }
        }

        for (Upload imgUpload : imageUploads) {
            try {
                imgUpload.write(ResourcesUtils.getRealTemplateData(imgUpload.getFilename()));
            } catch (IOException e) {
                e.printStackTrace();
                logger.warning(e.toString());
            }
        }

        for (String imageUrl : imageUrls) {
            try {
                Img.download(context, imageUrl, ResourcesUtils.getRealTemplateDataPath());
            } catch (IOException e) {
                e.printStackTrace();
                logger.warning(e.toString());
            }
        }

        clearParameters();
        return new ActionDoc(context, "The template images / files have been updated", new Link(TemplateDataHandler.class));
    }

}