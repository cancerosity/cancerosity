package org.sevensoft.ecreator.iface.admin.categories;

/**
 * @author sks 1 May 2007 11:10:20
 *
 */
public class SimpleDiv {

	private final Object[]	objs;

	public SimpleDiv(Object... objs) {
		this.objs = objs;

	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder("<div class='simpleDiv'>");
		for (Object obj : objs) {
			sb.append(obj);
		}
		sb.append("</div>");
		return sb.toString();
	}

}
