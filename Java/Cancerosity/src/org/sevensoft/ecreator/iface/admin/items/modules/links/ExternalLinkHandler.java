package org.sevensoft.ecreator.iface.admin.items.modules.links;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.extras.links.ExternalLink;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 8 Nov 2006 20:40:50
 *
 */
@Path("admin-esterno-links.do")
public class ExternalLinkHandler extends AdminHandler {

	private ExternalLink	externalLink;
	private ItemType		itemType;
	private String		target;
	private String		linkText;
	private String		label;

	public ExternalLinkHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (externalLink == null) {
			return main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit external link", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update external link"));
				sb.append(new ButtonTag(ItemTypeHandler.class, "edit", "Return to item type", "itemType", externalLink.getItemType()));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("External link"));

				sb.append(new AdminRow("Label", "Enter a label for this external link.", new TextTag(context, "label", externalLink.getLabel(), 20)));

				sb.append(new AdminRow("Target", "Enter link target.", new TextTag(context, "target", externalLink.getTarget(), 12)));

				sb.append(new AdminRow("Link text", "This is the text of the actual link.", new TextTag(context, "linkText",
						externalLink.getLinkText(), 20)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(ExternalLinkHandler.class, "save", "post"));
				sb.append(new HiddenTag("externalLink", externalLink));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object moveDown() throws ServletException {

		if (externalLink == null) {
			return index();
		}

		itemType = externalLink.getItemType();
		EntityUtil.moveDown(externalLink, itemType.getExternalLinks());

		return new ItemTypeHandler(context, itemType).edit();
	}

	public Object moveUp() throws ServletException {

		if (externalLink == null) {
			return index();
		}

		itemType = externalLink.getItemType();
		EntityUtil.moveUp(externalLink, itemType.getExternalLinks());

		return new ItemTypeHandler(context, itemType).edit();
	}

	public Object save() throws ServletException {

		if (externalLink == null) {
			return main();
		}

		externalLink.setLabel(label);
		externalLink.setLinkText(linkText);
		externalLink.setTarget(target);
		externalLink.save();

		return new ActionDoc(context, "This link has been updated", new Link(ExternalLinkHandler.class, null, "externalLink", externalLink));
	}
}
