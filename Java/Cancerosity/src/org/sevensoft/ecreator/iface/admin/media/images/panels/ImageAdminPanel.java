package org.sevensoft.ecreator.iface.admin.media.images.panels;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.EcreatorRenderer;
import org.sevensoft.ecreator.iface.admin.media.images.ImageEditHandler;
import org.sevensoft.ecreator.iface.admin.media.images.ImageSelectionHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.media.images.ImageOwner;
import org.sevensoft.ecreator.model.media.images.ImageType;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.ButtonOpenerTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Link;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author sks 27-Mar-2006 18:19:15
 */
public class ImageAdminPanel extends EcreatorRenderer {

    private final ImageOwner owner;
    private MiscSettings miscSettings;
    private Config config;
    private final List<Img> images;
    private boolean captions;

    public ImageAdminPanel(RequestContext context, ImageOwner owner) {
        super(context);

        this.owner = owner;
        this.miscSettings = MiscSettings.getInstance(context);
        this.config = Config.getInstance(context);

        images = owner.getAllImages();
        for (Img image : images) {
            if (image.hasCaption()) {
                captions = true;
                break;
            }
        }
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        if (owner.hasAnyImage()) {

            if (owner instanceof Item) {

                Item item = (Item) owner;
                List<ImageType> imageTypes = item.getItemType().getMediaModule().getImageTypes();

                for (ImageType imageType : imageTypes) {
                    type(sb, imageType.getName(), item.getAllImages(imageType));
                }

            } else {

                type(sb, "Images", owner.getApprovedImages());
            }

        }

        /*
           * Only show the 'add new image' tools if this owner can have more images
           */
        if (!owner.isAtImageLimit()) {

            sb.append(new AdminTable("Upload image"));


            if (owner instanceof Item) {

                Item item = (Item) owner;
                List<ImageType> imageTypes = item.getItemType().getMediaModule().getImageTypes();

                if (imageTypes.size() > 1) {
                    sb.append(new AdminRow("Image type", "Choose what this image is you are uploading", new SelectTag(context, "imageType", null,
                            imageTypes)));
                }

            }

            /*
                * Choose existing image
                */
//            File imageRoot = new File(config.getRealImagesPath());
            File imageRoot = ResourcesUtils.getRealImagesDir();
            if (imageRoot.exists()) {

                File[] filenames = imageRoot.listFiles();
                if (filenames != null && filenames.length > 0) {

                    ButtonOpenerTag openerTag = new ButtonOpenerTag(context, new Link(ImageSelectionHandler.class), "Select existing image", 650, 500);
                    openerTag.setScrollbars(true);
                    openerTag.setTarget("_blank");

                    // SELECT EXISTING IMAGE TAG
                    sb.append(new AdminRow("Select existing image", "Click this button to use an image that you have used previously.",
                            new HiddenTag("imageFilename", "").setId("imageFilename").toString() + openerTag));

                }
            }

            List<Object> tags = new ArrayList();
            for (int n = 0; n < miscSettings.getImageInputs(); n++) {
                tags.add(new FileTag("imageUploads", 40));
            }

            /*
                * Upload image from hard drive. Show file inputs (number to be determined by setting in misc settings
                */
            sb.append(new AdminRow("Upload image", "This will upload an image from your computer to the website.", StringHelper.implode(tags, " ")));

            tags = new ArrayList();
            for (int n = 0; n < miscSettings.getImageInputs(); n++) {
                tags.add(new TextTag(context, "imageUrls", 40));
            }

            /*
                * Image retrieval from url. Inputs number to be determined by setting in misc settings
                */
            sb
                    .append(new AdminRow("Download image from website", "This will copy an image from the URL you paste here.", StringHelper.implode(
                            tags, " ")));

            sb.append("</table>");
        }


        return sb.toString();
    }

    private void type(StringBuilder sb, String imageTypeName, List<Img> images) {
        if (images == null || images.isEmpty())
            return;

        PositionRender pr = new PositionRender(ImageEditHandler.class, "image");

        sb.append(new AdminTable(imageTypeName));

        sb.append("<tr>");
        sb.append("<th width='60'>Position</th>");

        if (images.size() < 10) {
            sb.append("<th>Preview</th>");
        } else {
            sb.append("<th>Filename</th>");
        }

        if (captions) {
            sb.append("<th>Caption and description</th>");
        }

        sb.append("<th width='10'> </th>");
        sb.append("</tr>");

        for (Img image : images) {

            sb.append("<tr>");

            sb.append("<td width='60'>" + pr.render(image) + "</td>");

            sb.append("<td>");
            sb.append(new LinkTag(ImageEditHandler.class, null, new SpannerGif(), "image", image));
            sb.append(" ");

            if (images.size() < 10) {
                sb.append(new LinkTag(image.getPath(), new ImageTag(image.getThumbnailPath())).setTarget("_blank"));
            } else {
                sb.append(new LinkTag(image.getPath(), image.getImagePath()).setTarget("_blank"));
            }

            sb.append("</td>");

            if (captions) {

                sb.append("<td valign='top'>");

                if (image.hasCaption()) {
                    sb.append(image.getCaption());
                }

                if (image.hasDescription()) {
                    sb.append("<br/><br/><i>");
                    sb.append(StringHelper.toSnippet(image.getDescription(), 150, "..."));
                    sb.append("</i>");
                }

                sb.append("</td>");

            }

            sb.append("<td width='10'>" + new LinkTag(ImageEditHandler.class, "removeImage", new DeleteGif(), "image", image) + "</td>");
            sb.append("</tr>");
        }
        sb.append("</table>");
    }
}
