package org.sevensoft.ecreator.iface.admin.ecom.bookings.config;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.attributes.renderers.OwnerAttributesPanel;
import org.sevensoft.ecreator.model.bookings.BookingSettings;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 03-Oct-2005 14:25:41
 */
@Path("admin-bookings-settings.do")
public class BookingSettingsHandler extends AdminHandler {

    private transient final BookingSettings bookingSettings;
    private String initialStatus;
    private int newAttributesPage;
    private String newAttributesSection;
    private String newAttributes;
    private String referrers;
    private String completedText;
    private String thirdPartyEmailNotifications;

    public BookingSettingsHandler(RequestContext context) {
        super(context);
        bookingSettings = BookingSettings.getInstance(context);
    }

    @Override
    public Object main() {

        AdminDoc doc = new AdminDoc(context, user, "Booking settings", Tab.Bookings);
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update booking settings"));
                sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
                sb.append("</div>");
            }

            private void general() {

                sb.append(new AdminTable("General"));

                sb.append(new AdminRow("Completed text",
                        "This text is shown to the user when a booking is completed.<br/><i>[id], [start], [end], [item], [name], [email]</i>.",
                        new TextAreaTag(context, "completedText", bookingSettings.getCompletedText(), 60, 5)));

                sb.append(new AdminRow("Referrers",
                        "If you want users to select where they heard about you when checking out, enter the referral sources here.",
                        new TextAreaTag(context, "referrers", StringHelper.implode(bookingSettings.getReferrers(), "\n"), 40, 5)));

                sb.append("</table>");
            }

            private void notifications() {

                sb.append(new AdminTable("Notifications"));

                sb.append(new AdminRow("Third party email notifications", "Enter the emails to receive a notification when a new booking is made.",
                        new TextAreaTag(context, "thirdPartyEmailNotifications", StringHelper.implode(bookingSettings
                                .getThirdPartyEmailNotifications(), "\n"), 50, 5)));

                sb.append("</table>");
            }

            private void status() {

                sb.append(new AdminTable("Booking status'"));

                sb.append(new AdminRow("Initial status", "A booking will initially be set to this status", new TextTag(context, bookingSettings
                        .getInitialStatus(), 40)));

                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(BookingSettingsHandler.class, "save", "POST"));

                commands();
                general();
                notifications();
                status();

                sb.append(new OwnerAttributesPanel(context, bookingSettings));

                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object save() {

        bookingSettings.setInitialStatus(initialStatus);
        bookingSettings.setReferrers(StringHelper.explodeStrings(referrers, "\n"));
        bookingSettings.setCompletedText(completedText);
        bookingSettings.setThirdPartyEmailNotifications(StringHelper.explodeStrings(thirdPartyEmailNotifications, "\n"));
        bookingSettings.save();

        bookingSettings.addAttributes(newAttributes, newAttributesSection, newAttributesPage);

        return new ActionDoc(context, "Booking settings have been updated", new Link(BookingSettingsHandler.class));
    }
}
