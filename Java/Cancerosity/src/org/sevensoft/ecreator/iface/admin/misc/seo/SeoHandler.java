package org.sevensoft.ecreator.iface.admin.misc.seo;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.help.seo.AutoContentOverrideTip;
import org.sevensoft.ecreator.help.seo.KeywordDensityTip;
import org.sevensoft.ecreator.help.seo.KeywordTagHelp;
import org.sevensoft.ecreator.help.seo.TitleTagHelp;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.misc.seo.GoogleSettings;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sam
 */
@Path("admin-settings-seo.do")
public class SeoHandler extends AdminHandler {

	private static final String			DemoYahooAppId	= "sevensoft";

	private String					titleTag, descriptionTag;
	private boolean					autoContentLinking, enabled, overrideTags;
	private String					keywords;
	private boolean					autoContentLinkingOverride;
	private boolean					autoKeywordCompletion;
	private String					googleApiLicenseKey;
	private String					yahooWebSearchAppId;
	private boolean					toolbar;
	private boolean					overrideFriendlyUrls;
	private final transient Seo			seo;
	private String					friendlyUrlSeparator;
	private boolean					keywordDensity;
	private boolean					autoKeywordHighlighting;
	private String					highlightedKeywords;
	private boolean					suggestions;
	private int						autoContentLinkingCategoryDepth;
	private final transient GoogleSettings	googleSettings;

	public SeoHandler(RequestContext context) {
		super(context);
		seo = Seo.getInstance(context);
		googleSettings = GoogleSettings.getInstance(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Search engine optimisation", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update seo settings"));
				sb.append("</div>");
			}

			private void friendlyUrls() {

				sb.append(new AdminTable("Search engine friendly urls"));

				sb.append(new AdminRow(true, "Override friendly urls",
						"Allows you to set your own friendly url rather than use the site generated one.", new BooleanRadioTag(context,
								"overrideFriendlyUrls", seo.isOverrideFriendlyUrls())));

				sb.append(new AdminRow(true, "Space character",
						"This is the character used to replace spaces when generating the URL (spaces are not valid in URLs).", new TextTag(
								context, "friendlyUrlSeparator", seo.getFriendlyUrlSeparator(), 3)));

				sb
						.append(new AdminRow(
								true,
								"Reset urls",
								"This will clear all previously set friend urls. <b>Warning:</b> If you reset this your new pages will need to be reindexed by the search index.",
								new ButtonTag(SeoHandler.class, "resetFriendlyUrls", "Reset friendly urls").setConfirmation("Are you sure?")));

				sb.append("</table>");
			}

			private void keywords() {

				sb.append(new AdminTable("Keywords"));

				sb.append(new AdminRow("Keyword density", "Shows your most used keywords and their density when editing a category.",
						new BooleanRadioTag(context, "keywordDensity", seo.isKeywordDensity())).setHelp(new KeywordDensityTip()));

				sb.append(new AdminRow("Auto Keyword highlighting",
						"Automatically mark your keywords with extra HTML tags designed to increase their importance to search engines.",
						new BooleanRadioTag(context, "autoKeywordHighlighting", seo.isAutoKeywordHighlighting())));

				sb
						.append(new AdminRow(
								"Highlighted keywords",
								"Enter each keyword phrase you want to be highlighted on a separate line. You can include multiple keywords in a single phrase.",
								new TextAreaTag(context, "highlightedKeywords", StringHelper.implode(seo.getHighlightedKeywords(), "\n"), 40, 6)));

				if (isSuperman()) {

					sb.append(new AdminRow(true, "Yahoo Web Search Application ID", "Required for the keyword suggestion tool.", new TextTag(context,
							"yahooWebSearchAppId", seo.getYahooWebSearchAppId(), 40)));
				}

				sb.append("</table>");
			}

			private void linking() {

				sb.append(new AdminTable("Content linking"));

				sb.append(new AdminRow("Auto content linking", "Automatically replace category names with links.", new BooleanRadioTag(context,
						"autoContentLinking", seo.isAutoContentLinking())).setHelp(new AutoContentOverrideTip()));

				sb.append(new AdminRow(true, "Content linking override", "Allows you to disable keyword linking on a per category basis.",
						new BooleanRadioTag(context, "autoContentLinkingOverride", seo.isAutoContentLinkingOverride()))
						.setHelp(new AutoContentOverrideTip()));

				if (isSuperman()) {
					sb
							.append(new AdminRow(
									true,
									"Max category depth",
									"Set to how many levels of categories you want to include as keywords. Leave as 0 for all (up to a site wide limit of 500).",
									new TextTag(context, "autoContentLinkingCategoryDepth", seo.getAutoContentLinkingCategoryDepth(), 4)));
				}

				sb.append("</table>");
			}

			private void meta() {

				sb.append(new AdminTable("Meta tags"));

				sb.append(new AdminRow("Title tag",
						"The title is displayed in your browser window and is used by search engines as the name of your pages.", new TextTag(
								context, "titleTag", seo.getTitleTag(), 60)).setHelp(new TitleTagHelp()));

				sb.append(new AdminRow("Description tag", "This is hidden descriptive data used by search engines.", new TextTag(context,
						"descriptionTag", seo.getDescriptionTag(), 60)));

				sb.append(new AdminRow("Keyword tag", "Keywords indicate to search engines the subject matter of your site.", new TextTag(context,
						"keywords", seo.getKeywords(), 60)).setHelp(new KeywordTagHelp()));

				sb.append(new AdminRow("Let me override these tags",
						"If you set this to yes then you can enter different tags for each individual page.", new BooleanRadioTag(context,
								"overrideTags", seo.isOverrideTags())));

				//				if (seo.isOverrideTags()) {
				//
				//					sb.append(new AdminRow("Auto keyword tag completion", "The site will auto complete the keywords when you first create a page.",
				//							new BooleanRadioTag(context, "autoKeywordCompletion", seo.isAutoKeywordCompletion())));
				//
				//					if (seo.isAutoKeywordCompletion()) {
				//
				//						//	 sb.append("<tr><th width='300'><b>Google API License Key</b></br>Required for auto keyword completion.</th><td>"
				//						//								+ new TextTag(context, "googleApiLicenseKey", seo.getGoogleApiLicenseKey(), 40) + "</td></tr>");
				//
				//						sb.append("<tr><th width='300'><b>Yahoo Web Search Application ID</b></br>"
				//								+ "Required for auto keyword completion through Yahoo web search.</th><td>"
				//								+ new TextTag(context, "yahooWebSearchAppId", seo.getYahooWebSearchAppId(), 40) + "</td></tr>");
				//
				//						if (isSuperman()) {
				//
				//							sb.append("<tr><th width='300'><b>Run keyword daemon</b></br>"
				//									+ "Schedule the keyword daemon for immediate running.</th><td>"
				//									+ new ButtonTag(SeoHandler.class, "runKeywordDaemon", "Run keyword daemon") + "</td></tr>");
				//
				//							sb.append("<tr><th width='300'><b>Queue all empty</b></br>"
				//									+ "Queue all empty items and categories for keyword generation.</th><td>"
				//									+ new ButtonTag(SeoHandler.class, "queueAll", "Queue now") + "</td></tr>");
				//
				//						}
				//					}
				//				}

				sb
						.append(new AdminRow(
								"Meta tag toolbar",
								"Puts a toolbar at the top of your site which shows you keywords and description tags for each page as you browse. <i>This is not viewable by the public</i>.",
								new BooleanRadioTag(context, "toolbar", seo.isToolbar())));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(SeoHandler.class, "save", "POST"));

                commands();
				linking();
				meta();
				keywords();

				if (isSuperman()) {
					friendlyUrls();
				}

				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object resetFriendlyUrls() {

		if (isSuperman()) {
			Seo.resetFriendlyUrls(context);
		}

		return new ActionDoc(context, "All search engine friendly urls have been reset", new Link(SeoHandler.class));
	}

	public Object save() throws ServletException {

		// tags and keywords
		seo.setTitleTag(titleTag);
		seo.setDescriptionTag(descriptionTag);
		seo.setKeywords(keywords);
		seo.setOverrideTags(overrideTags);

		// content linking
		seo.setAutoContentLinking(autoContentLinking);

		if (isSuperman()) {
			seo.setAutoContentLinkingOverride(autoContentLinkingOverride);
		}

		// keywords
		seo.setKeywordDensity(keywordDensity);
		seo.setAutoKeywordHighlighting(autoKeywordHighlighting);
		seo.setHighlightedKeywords(StringHelper.explodeStrings(highlightedKeywords, "\n"));
		seo.setToolbar(toolbar);

		if (isSuperman()) {

			// mr friendly's cars
			seo.setOverrideFriendlyUrls(overrideFriendlyUrls);
			seo.setFriendlyUrlSeparator(friendlyUrlSeparator);

			seo.setSuggestions(suggestions);
			seo.setYahooWebSearchAppId(yahooWebSearchAppId);

			seo.setAutoContentLinkingCategoryDepth(autoContentLinkingCategoryDepth);
		}

		seo.save();

		clearParameters();
		return main();
	}
}