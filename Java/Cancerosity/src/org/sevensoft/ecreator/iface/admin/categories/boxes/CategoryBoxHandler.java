package org.sevensoft.ecreator.iface.admin.categories.boxes;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.box.CategoryBox;
import org.sevensoft.ecreator.model.categories.box.CategoryBox.Context;
import org.sevensoft.ecreator.model.categories.box.CategoryBox.Style;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 01-Jun-2004 21:28:42
 */
@Path("admin-categories-boxes.do")
public class CategoryBoxHandler extends BoxHandler {

	private CategoryBox	box;
	private int			depth;
	private Context		boxContext;
	private Style		style;
	private boolean		includeHome;
	private int			maxLevel;
	private Category		root;
	private String		exclusions;
	private boolean		excludeCurrent;
	private Markup		markup;

	public CategoryBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {

		box.setDepth(depth);
		box.setContext(boxContext);
		box.setStyle(style);
		box.setIncludeHome(includeHome);
		box.setMaxLevel(maxLevel);
		box.setExcludeCurrent(excludeCurrent);
		box.setRoot(root);

		if (isSuperman()) {
			box.setMarkup(markup);
		}

		if (miscSettings.isAdvancedMode()) {
            if (exclusions != null)
                box.setExclusions(StringHelper.explodeStrings(exclusions.toLowerCase(), "\n"));
        }
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Box settings"));

		/*
		 * box style
		 */
		SelectTag styleTag = new SelectTag(context, "style", box.getStyle());
		styleTag.addOptions(CategoryBox.Style.values());
		sb.append(new AdminRow("Style", "Choose the appearance of the categories.<br/><i>Static</i> - All categories will be displayed."
				+ "<br/><i>Expanding</i> - The subcategories will display instantly as you click on them.<br/>"
				+ "<i>Dynamic</i> - The categories will display as you browse deeper.", styleTag));

		/*
		 * Box context
		 */
		SelectTag contextTag = new SelectTag(context, "boxContext", box.getContext());
		contextTag.addOptions(CategoryBox.Context.values());
		sb
				.append(new AdminRow(
						"Context",
						"Set to fixed and the category list will always begin with the home page, or if set to relative, the categories list will be the subcategories of whatever category the user is browsing.",
						contextTag));

		// Sidebar depth
		sb.append(new AdminRow("Depth", "How many levels of categories to display in the categories browser", new TextTag(context, "depth", box.getDepth(),
				5)));

		/*
		 * Max level for relative boxes
		 */
		if (box.isRelative()) {

			sb.append(new AdminRow("Max level", "How far the box should follow before cutting off.", new TextTag(context, "maxLevel", box.getMaxLevel(),
					5)));

		} else {

			SelectTag rootTag = new SelectTag(context, "root", box.getRoot());
			rootTag.setAny("-Default to home page-");
			rootTag.addOptions(Category.getCategoryOptions(context));

			sb.append(new AdminRow("Parent", "Set the parent category for this category box.", rootTag));
		}

		sb.append(new AdminRow("Include home", "Include a link to the home page in this category box.", new BooleanRadioTag(context, "includeHome", box
				.isIncludeHome())));

		sb.append(new AdminRow("Exclude current", "", new BooleanRadioTag(context, "excludeCurrent", box.isExcludeCurrent())));

		if (miscSettings.isAdvancedMode()) {

			sb.append(new AdminRow("Exclusions", "Enter the names of categories that would normally appear in this box, but you want to exclude. "
					+ "<i>Enter each category name on a new line</i>", new TextAreaTag(context, "exclusions", StringHelper.implode(box
					.getExclusions(), "\n"), 50, 4)));
		}

		if (isSuperman()) {
			sb.append(new AdminRow("Markup", "Custom markup for rendering this box.", new SelectTag(context, "markup", box.getMarkup(), Markup
					.get(context), "Default markup")));
		}

		sb.append("</table>");
	}
}
