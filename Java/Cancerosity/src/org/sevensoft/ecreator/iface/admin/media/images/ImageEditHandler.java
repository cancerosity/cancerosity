package org.sevensoft.ecreator.iface.admin.media.images;

import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.attributes.AttributeOptionHandler;
import org.sevensoft.ecreator.iface.admin.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.admin.containers.boxes.ImageBoxHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.items.modules.options.ItemOptionSelectionHandler;
import org.sevensoft.ecreator.iface.admin.media.galleries.GalleryHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.users.UserHandler;
import org.sevensoft.ecreator.model.attributes.AttributeOption;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.ImageOwner;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.media.images.boxes.ImageBox;
import org.sevensoft.ecreator.model.media.images.galleries.Gallery;
import org.sevensoft.ecreator.model.misc.log.Logging;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.List;

/**
 * @author sks 07-Dec-2005 09:09:54
 */
@Path("admin-images-edit.do")
public class ImageEditHandler extends AdminHandler {

    private String filename;
    private Item item;
    private Img image;
    private String description;
    private String alt;
    private String thumbnailFilename;
    private String caption;

    public ImageEditHandler(RequestContext context) {
        super(context);
    }

    public Object addImage() throws ServletException {

        if (item == null) {
            return index();
        }

        try {

            item.addImage(filename, true, true, false);

        } catch (IOException e) {
            addError(e);

        } catch (ImageLimitException e) {
            addError(e);
        }

        return new ItemHandler(context, item).edit();
    }

    private Object getHandler(ImageOwner owner) throws ServletException {

        if (owner instanceof Item) {
            return new ItemHandler(context, (Item) owner).edit();
        }

        if (owner instanceof Category) {
            return new CategoryHandler(context, (Category) owner).edit();
        }

        if (owner instanceof AttributeOption) {
            return new AttributeOptionHandler(context, (AttributeOption) owner).main();
        }

        if (owner instanceof User) {
            return new UserHandler(context, (User) owner).edit();
        }

        if (owner instanceof Gallery) {
            return new GalleryHandler(context, (Gallery) owner).edit();
        }

        if (owner instanceof ItemOptionSelection) {
            return new ItemOptionSelectionHandler(context, (ItemOptionSelection) owner).main();
        }

        if (owner instanceof ImageBox) {
            return new ImageBoxHandler(context, (ImageBox) owner).main();
        }

        throw new RuntimeException();
    }

    public Object main() throws ServletException {

        if (image == null) {
            return index();
        }

        AdminDoc doc = new AdminDoc(context, user, "Edit image details", null);
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update image"));

                if (image.hasItem()) {
                    sb.append(new ButtonTag(ItemHandler.class, "edit", "Return to item", "item", image.getItem()));
                }

                if (image.hasGallery()) {
                    sb.append(new ButtonTag(GalleryHandler.class, "edit", "Return to gallery", "gallery", image.getGallery()));
                }

                sb.append("</div>");
            }

            private void general() {

                sb.append(new AdminTable("Image details"));

                sb.append(new AdminRow("Fullsize filename", "This is the filename of the full size image", image.getFilename()));
                sb.append(new AdminRow("Thumbnail filename", "This is the filename of the thumbnail", new TextTag(context, "thumbnailFilename", image
                        .getThumbnailFilename(), 40)));

                sb.append(new AdminRow("Caption", "Enter a caption for this image", new TextTag(context, "caption", image.getCaption(), 60) + " " + new ErrorTag(context, "caption", "<br/>")));

                //				sb.append(new AdminRow("Alt text", "Override the alt text set on this image", new TextTag(context, "alt", image.getAlt(), 40)));

                sb.append(new AdminRow("Description", "Enter a description for this image. It is used as ALT", new TextAreaTag(context, "description", image
                        .getDescription(), 80, 14) + " " + new ErrorTag(context, "description", "<br/>")));

                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(ImageEditHandler.class, "save", "post"));
                sb.append(new HiddenTag("image", image));

                commands();
                general();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object moveDown() throws ServletException {

        if (image == null) {
            return index();
        }

        ImageOwner owner = image.getOwner();
        List<Img> images = owner.getApprovedImages();

        EntityUtil.moveDown(image, images);

        return getHandler(owner);
    }

    public Object moveUp() throws ServletException {

        if (image == null) {
            return index();
        }

        ImageOwner owner = image.getOwner();
        List<Img> images = owner.getApprovedImages();

        EntityUtil.moveUp(image, images);
        return getHandler(owner);

    }

    public Object removeImage() throws ServletException {

        if (image == null) {
            return index();
        }

        ImageOwner owner = image.getOwner();
        owner.removeImage(image, false, false);

        if (owner instanceof Logging) {
            ((Logging) owner).log(user, "Image removed: " + image.getFilename());
        }

        return getHandler(owner);
    }

    public Object removeImages() throws ServletException {

        if (image == null) {
            return index();
        }

        ImageOwner owner = image.getOwner();
        owner.removeImages();

        return getHandler(owner);
    }

    private void validateInput() {
//        test(new RequiredValidator(), "caption");
//        test(new LengthValidator(0, 100), "caption");
//        test(new RequiredValidator(), "description");
//        test(new LengthValidator(0, 500), "description");
    }

    public Object save() throws ServletException {

        validateInput();

        if (hasErrors()) {
            logger.fine("[ImageEditHandler] errors=" + context.getErrors());
            return main();
        }

        if (image == null) {
            return index();
        }

        image.setDescription(description);
        //		image.setAlt(alt);
        //		image.setThumbnailFilename(thumbnailFilename);
        image.setCaption(caption);
        image.save();

        clearParameters();
        addMessage("This image has been updated with your changes");
        return main();
    }

}
