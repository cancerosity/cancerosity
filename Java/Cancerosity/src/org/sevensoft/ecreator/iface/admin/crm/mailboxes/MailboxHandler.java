package org.sevensoft.ecreator.iface.admin.crm.mailboxes;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.crm.jobsheets.Jobsheet;
import org.sevensoft.ecreator.model.crm.mailboxes.Mailbox;
import org.sevensoft.ecreator.model.crm.mailboxes.MailboxProcessorResult;
import org.sevensoft.ecreator.model.crm.mailboxes.MailboxProcessorRunner;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 19 Apr 2006 20:46:01
 *
 */
@Path("admin-mailboxes.do")
public class MailboxHandler extends AdminHandler {

	private Mailbox	mailbox;
	private String	hostname;
	private String	username;
	private String	password;
	private String	autoResponse;
	private boolean	enabled;
	private Jobsheet	createInJobsheet;

	public MailboxHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		mailbox = new Mailbox(context, "localhost");
		return main();
	}

	public Object delete() throws ServletException {

		if (mailbox == null) {
			return main();
		}

		mailbox.delete();
		return main();
	}

	public Object edit() throws ServletException {

		if (mailbox == null) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Configure mailbox", Tab.Extras);
		doc.addBody(new Body() {

			private void auto() {

				sb.append(new AdminTable("Auto responder"));

				sb.append(new AdminRow("Auto response", "If you enter a message here it is sent to users when an email is received from them.",
						new TextAreaTag(null, "autoResponse", mailbox.getAutoResponse(), 50, 5)));

				sb.append("</table>");
			}

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update mailbox"));
				sb.append(new ButtonTag(MailboxHandler.class, "delete", "Delete mailbox", "mailbox", mailbox)
						.setConfirmation("Are you sure you want to delete this mailbox?"));
				sb.append(new ButtonTag(MailboxHandler.class, null, "Return to menu"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("Mailbox account"));

				sb.append(new AdminRow("Enabled", "Enable this mailbox for scanning", new BooleanRadioTag(context, "enabled", mailbox.isEnabled())));

				SelectTag ownerTag = new SelectTag(context, "owner", mailbox.getOwner());
				ownerTag.setAny("None");
				ownerTag.addOptions(User.getActive(context));

				sb.append(new AdminRow("Hostname", "The hostname for the POP3 box for this account.", new TextTag(context, "hostname", mailbox
						.getHostname(), 30)));
				sb.append(new AdminRow("Username", "The username for the POP3 account we are checking", new TextTag(context, "username", mailbox
						.getUsername(), 30)));
				sb.append(new AdminRow("Password", "The password for the POP3 account we are checking", new TextTag(context, "password", mailbox
						.getPassword(), 30)));

				if (Module.Jobsheets.enabled(context)) {

					sb.append(new AdminRow("Create in jobsheet",
							"If you set a jobsheet here, then any messagse received to this account that do not match an existing "
									+ "message thread will be created as a new job in the jobsheet.", new SelectTag(context,
									"createInJobsheet", mailbox.getCreateInJobsheet(), Jobsheet.get(context), "-No not create job-")));

				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(MailboxHandler.class, "save", "POST"));
				sb.append(new HiddenTag("mailbox", mailbox));

                commands();
				general();
				auto();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});

		return doc;
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Mailboxes", Tab.Extras);
		doc.addBody(new Body() {

			private void mailboxes() {

				sb.append(new AdminTable("Mailboxes"));

				sb.append("<tr>");
				sb.append("<th>Hostname</th>");
				sb.append("<th>Username</th>");
				sb.append("<th>Owner</th>");
				sb.append("<th width='10'> </th>");
				sb.append("</tr>");

				for (Mailbox mailbox : Mailbox.get(context)) {

					sb.append("<tr>");
					sb.append("<td>" + new LinkTag(MailboxHandler.class, "edit", new SpannerGif(), "mailbox", mailbox) + " " + mailbox.getHostname()
							+ "</td>");
					sb.append("<td>" + mailbox.getUsername() + "</td>");
					sb.append("<td>" + mailbox.getOwnerName() + "</td>");
					sb.append("<td width='10'>"
							+ new LinkTag(MailboxHandler.class, "delete", new DeleteGif(), "mailbox", mailbox)
									.setConfirmation("Are you sure you want to delete this mailbox") + "</td>");
					sb.append("</tr>");
				}

				sb.append("<tr><td colspan='4'>" + new ButtonTag(MailboxHandler.class, "create", "Add new mailbox") + "</td></tr>");
				sb.append("<tr><td colspan='4'>" + new ButtonTag(MailboxHandler.class, "process", "Process mailboxes now") + "</td></tr>");
				sb.append("</table>");

			}

			@Override
			public String toString() {

				mailboxes();
				return sb.toString();
			}

		});
		return doc;
	}

	public Object process() throws ServletException {

		MailboxProcessorRunner runner = new MailboxProcessorRunner(context);
		runner.run();

		MailboxProcessorResult result = runner.getResult();

		final String mailboxString, messagesString;
		if (result.getMailboxes() == 1) {
			mailboxString = "mailbox";
		} else {
			mailboxString = "mailboxes";
		}

		if (result.getMessages() == 1) {
			messagesString = "message";
		} else {
			messagesString = "messages";
		}

		addMessage("Processing complete: " + result.getMailboxes() + " " + mailboxString + " scanned and " + result.getMessages() + " " + messagesString
				+ " retrieved.");

		for (String error : result.getErrors()) {
			addError(error);
		}

		return main();
	}

	public Object save() throws ServletException {

		if (mailbox == null) {
			return main();
		}

		mailbox.setHostname(hostname);
		mailbox.setUsername(username);
		mailbox.setPassword(password);

		/*
		 * Auto responder
		 */
		mailbox.setAutoResponse(autoResponse);
		mailbox.setCreateInJobsheet(createInJobsheet);

		mailbox.setEnabled(enabled);
		mailbox.save();

		addMessage("The mailbox has been updated with your changes.");
		clearParameters();
		return edit();
	}
}
