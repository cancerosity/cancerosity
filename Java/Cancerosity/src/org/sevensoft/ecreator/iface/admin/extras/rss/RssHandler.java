package org.sevensoft.ecreator.iface.admin.extras.rss;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.misc.html.EditMarkupButton;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.extras.rss.export.RssExport;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 07-Jul-2005 12:39:17
 * 
 */
@Path("admin-rss.do")
public class RssHandler extends AdminHandler {

	private String				copyright;
	private final transient RssExport	rssExport;
	private String				author;
	private String				title;
    private String description;
	private String				feedType;
	private int	limit;
    private Markup descriptionMarkup;
    private Attribute dateLimitAttribute;

	public RssHandler(RequestContext context) {
		super(context);
		this.rssExport = RssExport.getInstance(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Edit rss export settings", Tab.Extras);
		if (miscSettings.isAdvancedMode()) {
			doc.setMenu(new ExtrasMenu());
		}
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update settings"));
				sb.append("</div>");
			}

			private void details() {

				sb.append(new AdminTable("RSS Export Setup"));

				sb.append(new AdminRow("Title", "Enter a name that the user will see as the title of the feed.", new TextTag(context, "title",
						rssExport.getTitle(), 30)
						+ "" + new ErrorTag(context, "title", "<br/>")));

                sb.append(new AdminRow("Description", "Enter the general author of the feed, this is generally your company or organisation's name.",
						new TextTag(context, "description", rssExport.getDescription(), 70)));

				sb.append(new AdminRow("Author", "Enter the general author of the feed, this is generally your company or organisation's name.",
						new TextTag(context, "author", rssExport.getAuthor(), 30)));

				sb.append(new AdminRow("Copyright", "Enter a copyright message as part of your feed.", new TextTag(context, "copyright", rssExport
						.getCopyright(), 30)));

				sb.append(new AdminRow("Limit", "Max number of items to include in the feed.", new TextTag(context, "limit", rssExport.getLimit(), 4)));

				sb.append(new AdminRow("Feed type", "Enter the RSS export format. You can leave this as default if you do not know what it does.",
						new TextTag(context, "feedType", rssExport.getFeedType(), 30)));

                SelectTag markupTag = new SelectTag(context, "descriptionMarkup", rssExport.getDescriptionMarkup(), Markup.get(context), "-Recreate-");
                markupTag.setId("descriptionMarkup");
                sb.append(new AdminRow(true, "Description Markup", "RSS Entry Description Markup.",
						markupTag + " "+  new EditMarkupButton("descriptionMarkup")));

				sb.append("</table>");
			}

            private void limits() {

                sb.append(new AdminTable("Limits"));

                SelectTag markupTag = new SelectTag(context, "dateLimitAttribute", rssExport.getDateLimitAttribute(), Attribute.getSelectionMap(context), "-Do not limit-");
                sb.append(new AdminRow("Limit", "Limit items with Date Attribute.", markupTag));

                sb.append("</table>");
            }

            @Override
			public String toString() {

				sb.append(new FormTag(RssHandler.class, "save", "post"));

                commands();
				details();

                limits();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		rssExport.setTitle(title);
        rssExport.setDescription(description);
		rssExport.setCopyright(copyright);
		rssExport.setAuthor(author);
		rssExport.setFeedType(feedType);
		rssExport.setLimit(limit);
        rssExport.setDescriptionMarkup(descriptionMarkup);
        rssExport.setDateLimitAttribute(dateLimitAttribute);

		rssExport.save();

		return new ActionDoc(context, "The RSS Export settings have been updated", new Link(RssHandler.class));
	}
}
