package org.sevensoft.ecreator.iface.admin.ecom.currencies;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 01-Jun-2004 21:28:42
 */
@Path("admin-settings-currencies.do")
public class CurrenciesHandler extends AdminHandler {

	Currency		currency;
	private String	name;
	private String	symbol;
	private double	rate;
	private boolean	dfault;
    private String  code;

	public CurrenciesHandler(RequestContext context) {
		super(context);
	}

	public Object add() throws ServletException {

		new Currency(context, "new currency", "symbol", 1);
		return main();
	}

	public Object delete() throws ServletException {

		if (currency == null) {
			return main();
		}

		currency.delete();
		addMessage("Currency deleted");
		return main();
	}

	public Object edit() throws ServletException {

		if (currency == null) {
			return main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit currency", Tab.Orders);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update currency"));
				sb.append(new ButtonTag(CurrenciesHandler.class, "delete", "Delete currency", "currency", currency)
						.setConfirmation("Are you sure you want to delete this currency?"));
				sb.append(new ButtonTag(CurrenciesHandler.class, null, "Return to currencies menu"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Name", "Enter the name of this currency, Eg. Euros", new TextTag(context, "name", currency.getName())));

				sb.append(new AdminRow("Symbol", "Enter the currency prefix, eg \243 or GBP.", new TextTag(context, "symbol", currency.getSymbol(), 5)));

				sb.append(new AdminRow("Rate", "Enter the exchange rate of this currency which is in relation to your default currency.", new TextTag(
						context, "rate", currency.getRate(), 5)));

                sb.append(new AdminRow("Code", "Enter the currency code for payment gateways, eg GBP.", new TextTag(context, "code", currency.getCode(), 5)));

				sb.append(new AdminRow("Default", "Set this currency as the default. All your prices will be converted in relation to your default.",
						new BooleanRadioTag(context, "dfault", currency.isDefault())));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(CurrenciesHandler.class, "save", "post"));
				sb.append(new HiddenTag("currency", currency));

				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		if (!Module.Currencies.enabled(context)) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Currencies", null);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Currencies"));

				sb.append("<tr>");
				sb.append("<th>Name</th>");
				sb.append("<th>Symbol</th>");
				sb.append("<th>Rate</th>");
				sb.append("<th>Default</th>");
				sb.append("<th width='10'> </th>");
				sb.append("</tr>");

				for (Currency currency : Currency.get(context)) {

					sb.append("<tr>");
					sb.append("<td>" + new LinkTag(CurrenciesHandler.class, "edit", new SpannerGif(), "currency", currency) + " "
							+ currency.getName() + "</td>");
					sb.append("<td>" + currency.getSymbol() + "</td>");
					sb.append("<td>" + currency.getRate() + "</td>");
					sb.append("<td>" + (currency.isDefault() ? "Yes" : "No") + "</td>");
					sb.append("<td width='10'>" + new LinkTag(CurrenciesHandler.class, "delete", new DeleteGif(), "currency", currency) + "</td>");
					sb.append("</tr>");
				}

				sb.append(new FormTag(CurrenciesHandler.class, "add", "POST"));
				sb.append("<tr>");
				sb.append("<td colspan='5'>Add a new currency " + new ButtonTag(CurrenciesHandler.class, "add", "Create currency") + "</td>");
				sb.append("</tr>");
				sb.append("</form>");

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (hasErrors())
			return main();

		currency.setSymbol(symbol);
		currency.setName(name);
		currency.setRate(rate);
        currency.setCode(code);
		if (dfault)
			currency.setDefault();
		currency.save();

		clearParameters();
		return main();
	}
}