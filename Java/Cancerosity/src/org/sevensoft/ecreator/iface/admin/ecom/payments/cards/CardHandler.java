package org.sevensoft.ecreator.iface.admin.ecom.payments.cards;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.OrderHandler;
import org.sevensoft.ecreator.iface.admin.ecom.payments.cards.panels.CardEntryPanel;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.cards.CardType;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardHolderException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CardTypeException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.CscException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiredCardException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.ExpiryDateException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.IssueNumberException;
import org.sevensoft.ecreator.model.ecom.payments.cards.exceptions.StartDateException;
import org.sevensoft.ecreator.model.ecom.payments.processors.ProcessorException;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 06-Feb-2006 16:08:04
 *
 */
@Path("admin-cards.do")
public class CardHandler extends AdminHandler {

	private Order	order;
	private Card	card;
	private Item	account;
	private String	cardHolder, cardNumber, expiryMonth, expiryYear, startMonth, startYear, issueNumber, csc;
	private String	cardIssuer;
	private CardType	cardType;

	public CardHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		Card.addTests(context, expiryMonth, expiryYear);
		if (hasErrors()) {
			return main();
		}

		try {

			String expiryDate = expiryMonth + expiryYear;
			String startDate = null;
			if (startMonth != null && startYear != null) {
				startDate = startMonth + startYear;
			}

			card = account.addCard(getRemoteIp(), cardType, cardHolder, cardNumber, expiryDate, startDate, issueNumber, csc, cardIssuer);

			// if we have an order then return to that order and set card on it
			if (order != null) {
				if (order.setCard(card)) {
					order.save();
					order.log(user, "Card details set to " + card.getSecureDescription());
				}
				return new ActionDoc(context, "The card details have been set on this order", new Link(OrderHandler.class, null, "order", order));
			}

			return new ActionDoc(context, "The new card has been added to this account", new Link(CardHandler.class, null, "account", account));

		} catch (IssueNumberException e) {
			setError("issueNumber", e);

		} catch (ExpiryDateException e) {
			setError("expiryDate", e);

		} catch (ExpiredCardException e) {
			setError("expiryDate", e);

		} catch (CardNumberException e) {
			setError("cardNumber", e);

		} catch (IOException e) {
			addError(e);

		} catch (CardTypeException e) {
			setError("cardType", e);

		} catch (StartDateException e) {
			setError("startDate", e);

		} catch (CardHolderException e) {
			setError("cardHolder", e);

		} catch (CscException e) {
			setError("csc", e);

		} catch (CardException e) {
			e.printStackTrace();
			addError(e);
			
		} catch (ProcessorException e) {
			e.printStackTrace();
			addError(e);
		}

		return main();
	}

	public Object delete() throws ServletException {

		if (card == null || account == null) {
			return main();
		}

		account.removeCard(card);
		addMessage("Card deleted");
		return main();
	}

	@Override
	public Object main() throws ServletException {

		if (account == null && order != null) {
			account = order.getAccount();
		}

		if (account == null) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Account credit cards", null);
		doc.addBody(new Body() {

			private void cards() {

				List<Card> cards = account.getCards();
				if (cards.isEmpty()) {
					return;
				}

				sb.append(new AdminTable("Existing cards"));

				for (Card card : cards) {

					sb
							.append("<tr><th>" + card.getLabelSecure(", ") + "</th><td>"
									+ new LinkTag(CardHandler.class, "delete", "Delete", "card", card, "account", account, "order", order)
									+ "</td></tr>");

				}

				sb.append("</table>");
			}

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Create card"));
				if (order != null) {
					sb.append(new ButtonTag(OrderHandler.class, null, "Return to order", "order", order));
				}
				sb.append(new ButtonTag(ItemHandler.class, null, "Return to " + account.getItemTypeNameLower(), "item", account));
				sb.append("</div>");
			}

			private void create() {

				CardEntryPanel panel = new CardEntryPanel(context);
				sb.append(panel);
			}

			@Override
			public String toString() {

				sb.append(new FormTag(CardHandler.class, "create", "POST"));
				sb.append(new HiddenTag("account", account));
				sb.append(new HiddenTag("order", order));

                commands();

				cards();

				create();

				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});

		return doc;
	}

	@Override
	public boolean runSecure() {
		return Config.getInstance(context).isSecured();
	}
}
