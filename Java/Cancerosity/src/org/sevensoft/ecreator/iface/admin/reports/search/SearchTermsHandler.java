package org.sevensoft.ecreator.iface.admin.reports.search;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.reports.StatsMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.stats.seo.SearchTerm;
import org.sevensoft.ecreator.model.stats.seo.SearchTermCounterMonthly;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 03-Oct-2005 16:12:08
 * 
 */
@Path("admin-reports-terms.do")
public class SearchTermsHandler extends AdminHandler {

	private Date	month;

	public SearchTermsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {
		return null;
	}

	public Object monthly() {

		if (month == null)
			month = new Date();
		month = month.beginMonth();

		int limit = 100;
		final List<SearchTermCounterMonthly> counters = SearchTermCounterMonthly.get(context, month, limit);

		AdminDoc doc = new AdminDoc(context, user, "Monthly search terms", Tab.Stats);
		doc.setMenu(new StatsMenu());
		doc.setIntro("This page shows the " + limit + " most popular search terms for the month " + month.toString("MMMM-yy"));
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Monthly search terms"));

				sb.append(new FormTag(SearchTermsHandler.class, "monthly", "get"));
				SelectTag monthTag = new SelectTag(context, "month");
				monthTag.setAutoSubmit();

				Date month = new Date().beginMonth();
				for (int n = 0; n < 13; n++) {
					monthTag.addOption(month, month.toString("MMMM-yy"));
					month = month.removeMonths(1);
				}

				sb.append("<tr><th align='right' colspan='3'>Choose month ");
				sb.append(monthTag);
				sb.append("</th></tr>");

				sb.append("<tr>");
				sb.append("<th>Search engine</th>");
				sb.append("<th>Search term</th>");
				sb.append("<th>Total</th>");
				sb.append("</tr>");

				for (SearchTermCounterMonthly counter : counters) {

					sb.append("<tr>");
					sb.append("<td>" + counter.getEngine() + "</td>");
					sb.append("<td>" + counter.getTerm() + "</td>");
					sb.append("<td>" + counter.getTotal() + "</td>");
					sb.append("</tr>");
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object recent() {

		int limit = 200;
		final List<SearchTerm> terms = SearchTerm.get(context, limit);

		AdminDoc page = new AdminDoc(context, user, "Recent search terms", Tab.Stats);
		page.setMenu(new StatsMenu());
		page.setIntro("This screen shows the " + limit + " most recent referrals from search engines and the terms that were used to search.");
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Recent search terms"));

				sb.append("<tr>");
				sb.append("<th>Date / Time</th>");
				sb.append("<th>IP Address</th>");
				sb.append("<th>Search engine</th>");
				sb.append("<th>Search term</th>");
				sb.append("</tr>");

				for (SearchTerm term : terms) {

					sb.append("<tr>");
					sb.append("<td>" + term.getDate().toString("HH:mm dd-MMM-yyyy") + "</td>");
					sb.append("<td>" + term.getIpAddress() + "</td>");
					sb.append("<td>" + term.getEngine() + "</td>");
					sb.append("<td>" + term.getTerm() + "</td>");
					sb.append("</tr>");
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return page;
	}

}
