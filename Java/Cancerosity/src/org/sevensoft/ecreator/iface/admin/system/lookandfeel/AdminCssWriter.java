package org.sevensoft.ecreator.iface.admin.system.lookandfeel;

import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.jeezy.http.RequestContext;

import java.io.File;
import java.io.IOException;

/**
 * @author sks 18 Oct 2006 16:45:57
 */
public class AdminCssWriter {

    private void brochureHeader(StringBuilder sb) {

        sb
                .append("table.brochureHeader { border-top: 1px solid #999999; width: 100%; background: #255480 url(../graphics/admin/brochure_header_bg.gif) repeat-x; }");
        sb.append("table.brochureHeader div.company { padding: 12px 8px; font-family: Arial; font-weight: bold; color: white; font-size: 13px; } ");
        sb.append("table.brochureHeader div.company span { font-size: 16px; } ");

        sb.append("table.brochureTabs { font-family: Verdana; font-size: 11px; font-weight: bold; }");
        sb.append("table.brochureTabs td { padding-left: 4px; } ");
        sb.append("table.brochureTabs td a { background: #6788a6; color: #ffffff; display: block; padding: 4px; border-bottom: 2px solid #255480; }");
        sb
                .append("table.brochureTabs td a:hover { padding: 0 2px; color: #0066aa; background: #eaf1f8; display: block; padding: 4px; border-bottom: 2px solid #eaf1f8; }");

        sb.append("table.brochureHeader div.brochureSubmenu { background: #eaf1f8; height: 7px; border-bottom: 1px solid #c9d3e2; }");

    }

    private void developerDiv(StringBuilder sb) {

        sb.append("div.developer { padding: 2px 6px; font-size: 10px; font-family: Verdana; }");
        sb.append("div.developer a { color: black; }");
        sb.append("div.developer a:hover { color: orange; }");

        sb.append("table.brochureHeader { color: blue; } ");
        sb.append("table.brochureHeader div.developer a { color: #162d43; font-weight: bold; } ");
        sb.append("table.brochureHeader div.developer a:hover { color: #c53434; }");
    }

    private void help(StringBuilder sb) {

        sb
                .append("div.help { z-index: 9; position: absolute; width: 540px; display: none; margin-left: 25px; margin-top: 15px; "
                        + "background: #ffffe1; padding: 5px 10px; border: 1px solid #222222; font-family: Verdana; font-size: 11px; font-weight: normal; } \n");

        sb.append("table.balloon { z-index: 10; position: absolute; right: 40%; top: -100px; width: 400px; display: none;  } ");
        sb.append("table.balloon td.left { background: url(../graphics/helpballoon/balloon_l.gif) repeat-y right top; } ");
        sb.append("table.balloon td.right { background: url(../graphics/helpballoon/balloon_r.gif) repeat-y left top; } ");
        sb.append("table.balloon td.bottom { height: 22px; background: url(../graphics/helpballoon/balloon_b.gif) repeat-x right; } ");
        sb.append("table.balloon td.top { background: url(../graphics/helpballoon/balloon_t.gif) repeat-x top; } ");
        sb.append("table.balloon td { padding: 0!important; } ");

        sb.append("table.balloon td.content { height: 70px; padding: 6px 12px!important; font-size: 16px; font-weight: normal; font-family: Arial; "
                + "width: 100%; background: #fcf179 url(../graphics/helpballoon/balloon_back.gif) repeat-x top; } ");

    }

    private void orders(StringBuilder sb) {

        sb.append("div.orderTotals { font-family: Verdana; font-weight: bold; font-size: 14px; text-align: right; } ");
    }

    private void toolbar(StringBuilder sb) {

        sb.append("table.cpbar { width: 100%; background: #efefef; } \n");
        sb.append("table.cpbar td { padding: 3px 8px; color: #333333; font-family: Tahoma; font-size: 11px; font-weight: normal; } \n");
        sb.append("table.cpbar td a { color: #333333; text-decoration: underline; } \n");
        sb.append("table.cpbar td a:hover { color: white; text-decoration: none; } \n");
        sb.append("table.cpbar td img { margin-left: 3px; } \n");
        sb.append("table.cpbar span { color: #333333; font-family: Trebuchet MS; font-size: 14px; font-weight: bold; letter-spacing: -1px; } \n");

    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        toolbar(sb);

        visitors(sb);

        help(sb);

        orders(sb);

        brochureHeader(sb);

        reorderObjects(sb);

        selectObjects(sb);

        sb.append("body { margin: 0px; }");

        sb.append("table.messages td { padding: 3px 10px; font-family: verdana; font-size: 11px; } ");
        sb.append("table.messages { margin-top: 10px; } ");

        sb.append("table.ec_bkmonth { font-family: Arial; border: 2px solid #cc3300; } ");
        sb.append("table.ec_bkmonth td.empty { background: #eaeaea; } ");

        sb.append("table.ec_bkmonth td {  font-family: Arial;  background: #83c0b9; padding: 3px; font-size: 15px; font-weight: bold; } ");

        sb.append("table.ec_bkmonth td a { text-decoration: none; } ");
        sb.append("table.ec_bkmonth td a { color: white; } ");
        sb.append("table.ec_bkmonth td a:hover { color: white; } ");

        sb.append("table.ec_bkmonth td.disabled { background: url(files/graphics/booking/notavailable.gif); } ");
        sb.append("table.ec_bkmonth td.reserved { background: #ff69b4; } ");
        sb.append("table.ec_bkmonth td.unavailable { background: #4f4f4f; } ");
        sb.append("table.ec_bkmonth td.booked { background: #ff0000; } ");

        sb.append("table.ec_bkmonth td.disabled a { color: #999999; } ");
        sb.append("table.ec_bkmonth td.disabled a:hover { color: #999999; } ");

        sb.append("table.ec_bkmonth caption { text-align: left; padding: 3px 0 3px 8px; "
                + "background: #cc3300; font-weight: bold; color: white; font-size: 14px;  } ");

        sb.append("table.ec_bkmonth td.heading { font-weight: bold; background: #faeae5; font-size: 9px; color: #cc6600; } ");

        sb.append("table.ec_bkyears td { padding: 2px 10px; font-family: Arial; font-size: 12px; font-weight: bold; } ");

        sb.append("div.simpleDiv { position: relative; padding: 4px; font-weight: bold; text-align: center; font-family: Verdana; font-size: 12px; } ");
        sb.append("div.actions { position: relative; margin-top: 10px; margin-bottom: 10px; text-align: center; } \n");
        sb.append("div.actions input { font-size: 16px; padding: 3px 6px; margin-right: 3px; margin-left: 3px; }\n");

        sb.append("table.header { font-family: Verdana; width: 100%; }");
        sb.append("table.header td.name { padding-left: 8px; padding-bottom: 5px; font-size: 16px; font-weight: bold; "
                + "color: #394d6b; letter-spacing: -0.05em; }");
        sb.append("table.header td.name span { font-size: 22px; font-weight: bold; padding-right: 6px; }");
        sb.append("table.header td.status { padding-right: 15px; font-size: 22px; color: #cccccc; font-weight: bold;  }");
        sb.append("table.header td.icon { padding-left: 5px; padding-bottom: 5px; width: 60px; font-size: 10px;  }");
        sb.append("table.header td.icon a { color: black; text-decoration: none; }");
        sb.append("table.header td.icon a:hover { color: orange; text-decoration: none; }");

        sb.append("div.menu { margin-left: 5px; margin-right: 5px; background: url(../graphics/admin/tab_bg.gif) repeat-x bottom; }");
        sb.append("div.menu table { font-family: Arial; font-size: 12px; }");
        sb.append("div.menu table td { padding: 4px 10px; border-right: 1px solid white; text-align: center; }");
        sb.append("div.menu table td.active { background: url(../graphics/admin/tab_bg_active.gif) repeat-x bottom; "
                + "padding: 4px 10px; text-align: center; }");
        sb.append("div.menu table a { color: white; text-decoration: none; }");
        sb.append("div.menu table a:hover { color: orange; text-decoration: underline; }");
        sb.append("table.items_submenu { font-size: 200px; background: #66627a; display: none; position: absolute; z-index: 5; } ");

        sb.append("div.menuline { border-top: 5px solid #66627a; padding: 0; margin: 0 5px; }");

        sb.append("div.submenucontainer { margin-top: 2px; background: #eff3f7;  margin-left: 5px; margin-right: 5px; border: 1px solid #cad4d9; } ");
        sb
                .append("table.submenu { background: #eff3f7; font-family: Arial; font-size: 12px; border-top: 1px solid white; border-bottom: 1px solid white; } \n");
        sb.append("table.submenu td.hover { background: #f4f8f9; } \n");
        sb.append("table.submenu td {  padding: 3px 8px; border-left: 1px solid white;  } \n");
        sb.append("table.submenu a { display: block; color: black; text-decoration: none; float: left;  } \n");
        sb.append("table.submenu a:hover { color: orange; text-decoration: none; } \n");

        developerDiv(sb);

        sb.append("h2 { color: #394d6b; font-family: Arial; font-size: 22px; }");

        sb.append("table.bottom { margin-top: 15px; font-family: Verdana; font-size: 10px; }");

        sb.append("table.main { width: 100%; height: 400px; border: 5px solid white; }");
        sb.append("table.main td.left { padding: 0 0 0 5px; }");
        sb.append("table.main td.middle { padding: 0 5px 0 0; width: 100%; }");

        sb.append("table.title { width: 100%; border: 1px solid #dddddd; margin-bottom: 5px; }");
        sb.append("table.title td { padding: 4px; border: 1px solid white; }");
        sb.append("table.title td.icon { text-align: center; border-right: 0; padding: 0px; width: 1px; }");
        sb.append("table.title td.icon img { padding: 5px; }");
        sb.append("table.title td.text { border-left: 0; }");
        sb.append("table.title td.text h1 { text-align: center;padding-bottom: 2px; margin: 0; "
                + "font-family: Arial, Tahoma, sans-serif; font-size: 24px; color: black; }");
        sb.append("table.title td.text h2 { text-align: center; font-weight: normal; margin: 0; "
                + "font-family: Arial, Tahoma, sans-serif; font-size: 13px; color: black; }");

        sb.append("table.simplebox { font-family: Verdana; font-size: 12px; margin-bottom: 20px;  }");
        sb.append("table.simplebox td { padding: 4px; font-weight: bold; }");

        sb.append("table.form { border : 1px solid #cad6ec; border-top: 0; width: 100%; "
                + "font-family: verdana; font-size: 11px; color: #000000; margin-bottom: 14px; }");
        sb.append("table.form table.attribute_selections { font-size: 11px; font-family: Verdana; } ");
        sb.append("table.form table.attribute_selections td { padding: 0; } ");
        sb.append("table.form caption {");
        sb.append("	border: 1px solid #bac6dc;");
        sb.append("	border-bottom: 0;");
        sb.append("	padding: 4px 20px 4px 5px;");
        sb.append("	background: #d6dfef;");
        sb.append("	font-weight: bold;");
        sb.append("	color: #344762; text-align: left; font-size: 11px; font-family: Verdana;");
        sb.append("}");
        sb.append("table.form td { background: #eff3f7; padding: 5px 20px 5px 5px; }");
        sb.append("table.form td li { background: #eff3f7; padding: 5px 20px 5px 5px; }");
        sb.append("table.form td input, table.form td select, table.form td textarea, table.form td span { font-family: Verdana; margin-bottom: 2px; "
                + "font-size: 12px; padding: 2px; } ");
        sb.append("table.form td.superman { color: #bb0000; } ");
        sb.append("table.form th {");
        sb.append("	background: #eaf1f7;");
        sb.append("	font-weight: bold;");
        sb.append("	text-align: left;");
        sb.append("	padding: 4px 20px 4px 5px;");
        sb.append("}");
        sb.append("table.form th a {");
        sb.append("	color: #003366;");
        sb.append("}");
        sb.append("table.form th a:hover {");
        sb.append("	color: orange;");
        sb.append("}");
        sb.append("table.form td a { color: #003366; text-decoration: underline; }");
        sb.append("table.form td a:hover { color: orange; text-decoration: underline; }");
        sb.append("table.form tr:hover td { background: #f4f8f9; }");

        sb.append("table.welcome {");
        sb.append("	width: 100%;");
        sb.append("	}");

        sb.append("span.ec_error {");
        sb.append("	color: #cc0000; font-weight: bold;");
        sb.append("	}");

        sb.append("table.msg { margin-bottom: 5px; width: 100%; border: 1px solid #e4e4e4; "
                + "font-family: Tahoma; font-size: 12px; padding: 2px; color: #333333; } ");
        sb.append("table.msg td.title_int { background: #eaf1f7; padding: 4px 6px; } ");
        sb.append("table.msg td.title_ext { background: #fde275; padding: 4px 6px; } ");
        sb.append("table.msg td.attachments { font-size: 11px; padding: 2px 6px; } ");
        sb.append("table.msg td.body { padding: 6px 8px; } ");
        sb.append("table.msg td.number { padding: 0 2px 0 1px; width: 40px; font-size: 22px; border-right: 1px solid #e4e4e4; color: #cccccc; } ");

        sb.append("table.msg a { color: #1b57b1; text-decoration: underline; } ");
        sb.append("table.msg a:hover { color: #1b57b1; text-decoration: none; } ");

        sb.append("div.message_error { margin-bottom: 10px; font-family: Verdana; font-size: 12px; color: #cc0000; font-weight: bold; "
                + "border: 1px solid #cc9933; border-left: 0; border-right: 0; background: #ffffcc; padding: 10px; text-align: center; } ");

        sb.append("div.message_info { margin-bottom: 10px; font-family: Verdana; font-size: 12px; color: black; "
                + "border: 1px solid #cc9933; border-left: 0; border-right: 0; background: #ffffcc; padding: 10px; text-align: center; } ");

        sb.append("span.page_control { font-family: Arial; font-size:12px; display: block; padding-bottom: 4px; } ");
        sb.append("span.page_control a { color: #394d6b; font-weight: bold; } ");
        sb.append("span.page_control a:hover { color: orange; text-decoration: none; } ");

        sb.append("table.simplelinks { font-family: Verdana; font-size: 13px; } ");
        sb.append("table.simplelinks th { font-size: 16px; padding: 14px 0; } ");
        sb.append("table.simplelinks td { padding: 8px; 0; } ");
        sb.append("table.simplelinks td.spacer { padding: 10px; 0; } ");
        sb.append("table.simplelinks td a { color: #0033cc; text-decoration: underline; } ");
        sb.append("table.simplelinks td a:hover { color: orange; text-decoration: underline; } ");

        sb.append("table.attribute_checks { width: auto; font-size: 11px; } ");
        sb.append("table.attribute_checks td { width: auto; } ");

        sb.append("table.ec_bookingcharts td { padding: 8px; font-family: Verdana; font-size: 11px;  }");
        sb.append("table.ec_bookingcharts table.month td { padding: 1px 2px; }");

        return sb.toString();
    }

    private void visitors(StringBuilder sb) {

        sb.append("table.visitors_profile { font-size: 11px; background: #ffffe1; padding: 5px 10px; border: 1px solid #222222; padding: 0; "
                + "display: none; position: absolute; margin-left: 15px;} ");

        sb.append("table.visitors_profile td { background: #ffffe1; } ");

    }

    private void reorderObjects(StringBuilder sb) {
        sb.append("\t#sortable { list-style-type: none; margin: 0; padding: 0; width: 100%; }\n" +
                "\t#sortable li { margin: 0 7px 7px 7px; padding: 0.4em; }\n" +
                "\t#sortable li span { margin-right: 1.4em; }\n\n");
    }

    private void selectObjects(StringBuilder sb) {
        sb.append("\t#selectable .ui-selecting { background: #CCCCFF; }\n" +
                "\t#selectable .ui-selected { background: #adadee; color: white; }\n" +
                "\t#selectable { list-style-type: none; margin: 0; padding: 0; width: 100%; }\n" +
                "\t#selectable li { margin: 3px; padding: 0.4em; font-size: 1.4em; height: 18px; }\n\n");
    }

    public void write(RequestContext context) throws IOException {

        File file = context.getRealFile("files/css");
        file.mkdirs();

        file = context.getRealFile("files/css/ecreator-admin.css");
        SimpleFile.writeString(toString(), file);
    }
}
