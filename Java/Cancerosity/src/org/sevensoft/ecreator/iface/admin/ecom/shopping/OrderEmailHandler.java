package org.sevensoft.ecreator.iface.admin.ecom.shopping;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.ecom.orders.emails.OrderEmail;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 8 Feb 2007 14:36:01
 *
 */
@Path("admin-orders-email.do")
public class OrderEmailHandler extends AdminHandler {

	private OrderEmail	email;
	private String		subject;
	private String		recipients;
	private String		body;
	private String		name;
	private String	linesBody;

	public OrderEmailHandler(RequestContext context) {
		super(context);
	}

	public Object create() {

		email = new OrderEmail(context, "new email");
		return new ShoppingSettingsHandler(context).main();
	}

	public Object delete() throws ServletException {

		if (email == null) {
			return main();
		}

		email.delete();
		return new ShoppingSettingsHandler(context).main();
	}

	@Override
	public Object main() throws ServletException {

		if (email == null) {
			return main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit order email", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update email"));
				sb.append(new ButtonTag(OrderEmailHandler.class, "delete", "Delete email", "email", email)
						.setConfirmation("Are you sure you want to delete this email?"));
				sb.append(new ButtonTag(ShoppingSettingsHandler.class, null, "Return to shopping settings"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Name", "Give this email a name for easy reference.", new TextTag(context, "name", email.getName(), 40) +
						new ErrorTag(context, "name", "<br/>").toString()));

				sb.append(new AdminRow("Subject", "Enter the subject of this email, eg, 'New order received'", new TextTag(context, "subject", email
						.getSubject(), 40)));

				sb.append(new AdminRow("Recipients", "Enter each recipient of this email on a separate line.", new TextAreaTag(context, "recipients",
						StringHelper.implode(email.getRecipients(), "\n"), 40, 4)));

				sb
						.append(new AdminRow("Body", "Enter the content of the email in this box.", new TextAreaTag(context, "body", email
								.getBody(), 60, 7)));

				sb.append(new AdminRow("Lines body", "Enter the content of the email in this box.", new TextAreaTag(context, "linesBody", email
						.getLinesBody(), 60, 3)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(OrderEmailHandler.class, "save", "post"));
				sb.append(new HiddenTag("email", email));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (email == null) {
			return main();
		}

		test(new RequiredValidator(), "name");
		if (hasErrors()) {
			return main();
		}

		email.setName(name);
		email.setBody(body);
		email.setSubject(subject);
		email.setLinesBody(linesBody);
		email.setRecipients(StringHelper.explodeStrings(recipients, "\n"));

		email.save();

		clearParameters();
		addMessage("Order email updated");
		return new ShoppingSettingsHandler(context).main();
	}

}
