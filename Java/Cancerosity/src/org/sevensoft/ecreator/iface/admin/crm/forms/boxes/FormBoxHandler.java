package org.sevensoft.ecreator.iface.admin.crm.forms.boxes;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.crm.forms.box.FormBox;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 24 Jul 2006 22:38:03
 *
 */
@Path("admin-forms-box.do")
public class FormBoxHandler extends BoxHandler {

	private FormBox	box;

	public FormBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {
	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

}
