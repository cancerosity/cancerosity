package org.sevensoft.ecreator.iface.admin.accounts;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.accounts.AccountSettings;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupTag;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;

/**
 * @author sks 29 Dec 2006 01:35:15
 *
 */
@Path("admin-accounts-settings.do")
public class AccountSettingsHandler extends AdminHandler {

	private final transient AccountSettings	accountSettings;

	private Markup					loginMarkup;
	private String					loginHeader;
	private String					loginFooter;

	private String					profileImagesFooter;
	private String					profileImagesHeader;
	private String					profileEditFooter;
	private String					profileEditHeader;

	public AccountSettingsHandler(RequestContext context) {
		super(context);
		this.accountSettings = AccountSettings.getInstance(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Account Settings", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update account settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, "edit", "Return to settings menu"));
				sb.append("</div>");
			}

			private void login() {
				sb.append(new AdminTable("Login"));

				sb.append("<tr><th colspan='2'>Login page header</th></tr><tr><td colspan='2'>"
						+ new TextAreaTag(context, "loginHeader", accountSettings.getLoginHeader(), 70, 8) + "</td></tr>");

				sb.append("<tr><th colspan='2'>Login page footer</th></tr><tr><td colspan='2'>"
						+ new TextAreaTag(context, "loginFooter", accountSettings.getLoginFooter(), 70, 8) + "</td></tr>");

				if (Module.UserMarkup.enabled(context) || isSuperman()) {
					sb.append(new AdminRow("Login markup", new MarkupTag(context, "loginMarkup", accountSettings.getLoginMarkup(), "-Default-")));
				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(AccountSettingsHandler.class, "save", "post"));

                commands();
				login();
				commands();

				sb.append("</form>");

				return sb.toString();
			}
		});
		return doc;
	}

	public Object save() throws ServletException {

		if (Module.UserMarkup.enabled(context) || isSuperman()) {
			accountSettings.setLoginMarkup(loginMarkup);
		}

		accountSettings.setLoginHeader(loginHeader);
		accountSettings.setLoginFooter(loginFooter);

		accountSettings.save();

		addMessage("Account settings updated");
		clearParameters();
		return main();
	}
}
