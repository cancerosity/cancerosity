package org.sevensoft.ecreator.iface.admin.design.template;

import java.io.File;
import java.io.IOException;

import org.sevensoft.ecreator.model.design.template.Template;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.installer.InstallerException;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 23 Jan 2008 21:14:41
 *
 */
public class TemplateInstaller {

	public static void install(RequestContext context, String filename, User user) throws IOException, InstallerException {

		File file = context.getRealFile(Config.TemplateStorePath + "/" + filename);
		if (file.exists()) {

			Template template = new Template(context, file, user);
			template.setDefault(true);
			template.save();

		}
	}

}
