package org.sevensoft.ecreator.iface.admin.media.images;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.results.StringResult;

import java.io.IOException;

/**
 * @author sks 28-Jul-2005 10:00:45
 */
@Path("tinymce-upload.do")
public class TinyMceUploadHandler extends AdminHandler {

    private Upload upload;

    public TinyMceUploadHandler(RequestContext context) {
        super(context);
    }

    @Override
    public Object main() {

        if (upload != null)
            try {
                upload.writeUnique(ResourcesUtils.getRealImagesPath());
            } catch (IOException e) {
                e.printStackTrace();
            }

        return new StringResult("<html><head><script>parent.location.reload(true);</script></head></html>", "text/html");

        //		Page page = new Page(context) {
        //
        //			@Override
        //			public void output() throws ServletException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {
        //
        //				StringBuilder sb = new StringBuilder();
        //				sb.append("<html>");
        //				sb.append("<head>");
        //				sb.append("<title>Upload image</title>");
        //				sb.append("</head>");
        //				sb.append("<body>");
        //
        //				sb.append(new FormTag(TinyMceUploadHandler.class, "multi", "upload"));
        //				sb.append(new FileTag("upload"));
        //
        //				sb.append(new SubmitTag("Upload"));
        //				sb.append("</form>");
        //
        //				sb.append("</body>");
        //				sb.append("</html>");
        //
        //			}
        //
        //		};
        //		return page;
    }

    public Object upload() {

        if (upload != null)
            try {
                upload.writeUnique(ResourcesUtils.getRealImagesPath());
            } catch (IOException e) {
                e.printStackTrace();
            }

        return "<html><head></head><body onload='window.close();'/>";
    }
}
