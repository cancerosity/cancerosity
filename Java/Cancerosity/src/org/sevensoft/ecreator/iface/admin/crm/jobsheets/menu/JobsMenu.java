package org.sevensoft.ecreator.iface.admin.crm.jobsheets.menu;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.ecreator.iface.admin.crm.jobsheets.JobHandler;
import org.sevensoft.ecreator.iface.admin.crm.jobsheets.JobsHandler;
import org.sevensoft.ecreator.iface.admin.crm.jobsheets.JobsheetHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Menu;
import org.sevensoft.ecreator.model.crm.jobsheets.Job;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 19 Oct 2006 17:17:02
 *
 */
public class JobsMenu extends Menu {

	private final Job	job;

	public JobsMenu(Job job) {
		this.job = job;
	}

	@Override
	public List<LinkTag> getLinkTags(RequestContext context) {

		List<LinkTag> tags = new ArrayList();

		tags.add(new LinkTag(JobHandler.class, null, "This job", "job", job));
		tags.add(new LinkTag(JobsheetHandler.class, "view", "Outstanding jobs", "jobsheet", job.getJobsheet(), "status", Job.Status.Outstanding));
		tags.add(new LinkTag(JobsheetHandler.class, "view", "Completed jobs", "jobsheet", job.getJobsheet(), "status", Job.Status.Completed));
		tags.add(new LinkTag(JobsHandler.class, "myjobs", "My Jobs"));
		tags.add(new LinkTag(JobsHandler.class, "unassigned", "New jobs"));

		return tags;
	}

}
