package org.sevensoft.ecreator.iface.admin.system.lookandfeel;

import java.util.List;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 13 Oct 2006 13:45:55
 *
 */
public abstract class Menu {

	public abstract List<LinkTag> getLinkTags(RequestContext context);
}
