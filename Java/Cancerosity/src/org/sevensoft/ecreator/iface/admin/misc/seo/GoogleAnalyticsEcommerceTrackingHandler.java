package org.sevensoft.ecreator.iface.admin.misc.seo;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.model.misc.seo.GoogleSettings;
import org.sevensoft.ecreator.model.misc.seo.sku.SkuArrtibutes;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.commons.collections.maps.MultiValueMap;

import java.util.List;

/**
 * User: Tanya
 * Date: 23.05.2011
 */
@Path("admin-settings-google-ecommerce.do")
public class GoogleAnalyticsEcommerceTrackingHandler extends AdminHandler {

    private final transient GoogleSettings googleSettings;

    private String account;
    private String storeName;
    @Deprecated
    private Attribute skuAttribute;

    private MultiValueMap<ItemType, Attribute> skuAttributes;

    private Attribute externalLinkAttributeTag;

    private Attribute emailAttribute;


    public GoogleAnalyticsEcommerceTrackingHandler(RequestContext context) {
        super(context);
        this.googleSettings = GoogleSettings.getInstance(context);
    }

    public Object main() {
        AdminDoc page = new AdminDoc(context, user, "Google analytics: Ecommerce", null);
        page.addBody(new Body() {

            private void commands() {
                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update google settings"));
                sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return"));
                sb.append("</div>");
            }

            private void general() {

                if (Module.GoogleAnalyticsTracking.enabled(context)) {

                    sb.append(new AdminTable("General"));

                    sb.append(new AdminRow("Account", "Google Analytics ID (UA-xxxxxx-x)", new TextTag(context, "account", googleSettings.getAccount())));

                    sb.append("</table>");
                }
            }

            private void analytics() {

                if (Module.GATrackingCodeEcommerce.enabled(context)) {

                    sb.append(new AdminTable("Ecommerce Tracking"));

                    sb.append(new AdminRow("Store name", "", new TextTag(context, "storeName", googleSettings.getStoreName())));

                    List<ItemType> orderable = ItemType.getOrderable(context);

                    for (ItemType type : orderable) {
                        SkuArrtibutes skuAttArrtibute = SkuArrtibutes.getSkuArrtibute(context, type);

                        SelectTag skuAttributeTag = new SelectTag(context, "skuAttributes_" + type.getId(), skuAttArrtibute.getSkuAttribute());
                        skuAttributeTag.setAny("-Select attribute-");
                        skuAttributeTag.addOptions(type.getAttributesSelectionMap(context));

                        sb.append(new AdminRow("SKU/code attribute of " + type.getNameLower(), "", skuAttributeTag));
                    }

                    sb.append("</table>");
                }
            }

            private void outboundlinks() {

                if (Module.GATrackingOutboundLinks.enabled(context)) {

                    sb.append(new AdminTable("Tracking Outbound Links"));

                    SelectTag externalLinkAttributeTag = new SelectTag(context, "externalLinkAttributeTag", googleSettings.getExternalLinkAttributeTag());
                    externalLinkAttributeTag.setAny("-Select attribute-");
                    externalLinkAttributeTag.addOptions(Attribute.get(context, AttributeType.Link));

                    sb.append(new AdminRow("External link attribute", "", externalLinkAttributeTag));


                    sb.append("</table>");
                }
            }

            private void email() {

                if (Module.GATrackingEmailLinks.enabled(context)) {

                    sb.append(new AdminTable("Tracking Email Link Clicks"));

                    SelectTag emailAttributeTag = new SelectTag(context, "emailAttribute", googleSettings.getEmailAttributeTag());
                    emailAttributeTag.setAny("-Select attribute-");
                    emailAttributeTag.addOptions(Attribute.get(context, AttributeType.Email));

                    sb.append(new AdminRow("External link attribute", "", emailAttributeTag));


                    sb.append("</table>");
                }
            }


            @Override
            public String toString() {

                sb.append(new FormTag(GoogleAnalyticsEcommerceTrackingHandler.class, "save", "POST"));

                commands();
                general();
                analytics();
                outboundlinks();
                email();
                commands();

                sb.append("</form>");
                return sb.toString();
            }

        });

        return page;
    }

    public Object save() {

        googleSettings.setAccount(account);

        googleSettings.setStoreName(storeName);
//        googleSettings.setSkuAttribute(skuAttribute);
        googleSettings.setSkuAttributes(skuAttributes);

        googleSettings.setExternalLinkAttributeTag(externalLinkAttributeTag);

        googleSettings.setEmailAttributeTag(emailAttribute);
        googleSettings.save();

        clearParameters();
        return main();
    }
}
