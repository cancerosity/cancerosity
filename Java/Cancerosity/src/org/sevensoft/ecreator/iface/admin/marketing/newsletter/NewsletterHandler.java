package org.sevensoft.ecreator.iface.admin.marketing.newsletter;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 25 Aug 2006 15:46:46
 *
 */
@Path("admin-newsletters.do")
public class NewsletterHandler extends AdminHandler {

	private Newsletter	newsletter;
	private boolean		validateEmails;
	private String		description;
	private String		name;
	private ItemType		autoMemberGroup;
	private ItemType		itemType;
    private String		autoResponder;
    private String      newsletterContent;

    private final transient MiscSettings miscSettings;

    public NewsletterHandler(RequestContext context) {
		super(context);
        this.miscSettings = MiscSettings.getInstance(context);
    }

	public Object create() throws ServletException {

		newsletter = new Newsletter(context, "new newsletter");
		return new NewsletterSettingsHandler(context).main();
	}

	public Object delete() throws ServletException {

		if (newsletter == null)
			return main();

		newsletter.delete();
		addMessage("The '" + newsletter.getName() + "' newsletter has been deleted, and all subscribers removed.");
		return new NewsletterSettingsHandler(context).main();
	}

	public Object main() throws ServletException {

		if (newsletter == null)
			return new NewsletterSettingsHandler(context).main();

        AdminDoc page = new AdminDoc(context, user, "Edit newsletter #" + newsletter.getIdString(), Tab.Marketing);

        if (miscSettings.isAdvancedMode()) {
            page.setMenu(new NewslettersMenu(newsletter));
        }
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update newsletter"));
				sb.append(new ButtonTag(NewsletterSettingsHandler.class, null, "Return to newsletter settings"));
				sb.append("</div>");
			}

            private void content() {
                sb.append(new AdminTable("Auto responde"));
                sb.append("<tr><td>Enter an auto response email that will be sent to subscribers of this newsletter.</td></tr>");
                sb.append("<tr><td>" + new TextAreaTag(context, "autoResponder",
                        newsletter.getAutoResponder()).setId("body").setStyle("width: 100%; height: 320px;") + "</td></tr>");
                sb.append("</table>");
            }

             private void newsletterContent() {
                sb.append(new AdminTable("Newsletter content"));
                sb.append("<tr><td>Enter the newsletter text that will be sent to subscribers of this newsletter.</td></tr>");
                sb.append("<tr><td>Allowed markers: <ul>" +
                        "<li>[newsletter_items?limit=N]-items was created this week, or this day depending on subscriber's settings</li>" +
                        "" +
                         "</ul></td></tr>");
                sb.append("<tr><td>" + new TextAreaTag(context, "newsletterContent",
                        newsletter.getNewsletterContent()).setId("newsletterContent").setStyle("width: 100%; height: 320px;") + "</td></tr>");
                sb.append("</table>");
            }

            private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Name", "Enter the name of this newsletter", new TextTag(context, "name", newsletter.getName())));

				sb.append(new AdminRow("Descriptoin", "Enter a brief description about this newsletter.",
                        new TextAreaTag(context, "description", newsletter.getDescription(), 50, 5)));

                //				sb.append("<tr><th width='300'><b>Validate</b><br/>"
				//						+ "Set to yes to require subscribers click a link by email to ensure their email address is valid "
				//						+ "and that they are the genuine recipient.</th><td>"
				//						+ new BooleanRadioTag(context, "validateEmails", newsletter.isValidateEmails()) + "</td></tr>");

				List<ItemType> autoMemberGroups = newsletter.getAutoMemberGroups();
				for (ItemType itemType : autoMemberGroups) {
					sb.append("<tr><th width='300'><b>Auto signup item type</b><br/></th><td>"
							+ itemType.getName()
							+ " "
							+ new ButtonTag(NewsletterHandler.class, "removeAutoItemType", "Remove", "newsletter", newsletter, "itemType",
									itemType) + "</td></tr>");
				}

				List<ItemType> memberGroups = ItemType.get(context);
				memberGroups.removeAll(autoMemberGroups);
				if (memberGroups.size() > 0) {

					SelectTag tag = new SelectTag(context, "itemType");
					tag.setAny("-Select item type-");
					tag.addOptions(memberGroups);

					sb.append("<tr><th width='300'><b>Auto signup item type</b><br/></th><td>" + tag + "</td></tr>");
				}

				sb.append("</table>");
			}

			@Override
			public String toString() {
                if (miscSettings.isHtmlEditor()) {
                    RendererUtil.tinyMce(context, sb, "body", "newsletterContent");
                }
				sb.append(new FormTag(NewsletterHandler.class, "save", "POST"));
				sb.append(new HiddenTag("newsletter", newsletter));

                commands();
				general();
                content();
                if (Module.NewsletterNewItems.enabled(context))
                    newsletterContent();
                commands();

				sb.append("</form>");

				return sb.toString();
			}
		});
		return page;
	}

	public Object removeAutoItemType() throws ServletException {

		if (newsletter == null || itemType == null) {
			return main();
		}

		newsletter.removeAutoItemType(itemType);
		return main();
	}

	public Object save() throws ServletException {

		if (newsletter == null) {
			return main();
		}

		newsletter.setName(name);
		newsletter.setDescription(description);
        newsletter.setAutoResponder(autoResponder);
        newsletter.setNewsletterContent(newsletterContent);
        newsletter.save();

		if (itemType != null) {
			newsletter.addAutoItemType(itemType);
		}

		addMessage("Newsletter updated");
		clearParameters();
		return main();
	}

}
