<script type='text/javascript' language='javascript'> 
	 
	function checkSelectedIds(){
		var form = document.getElementById('orderForm');
		 
		var total = 0;
		 
		var max = form.ids.length;
		 
		for (var idx = 0; idx < max; idx++) {
		if (eval('document.orderForm.ids[' + idx + '].checked') == true) {
		    total += 1;
		   }
		}
		 
		return total > 0;
	}
	
	function printOrder(){
		 
		var form = document.getElementById('orderForm');
		
		if(checkSelectedIds()){
			form['_a'].value='print';
			form.submit();
		} else {
			alert('No order selected');
			return;
		}
	}
	
	function setStatusOrder(){
		var form = document.getElementById('orderForm');
		if(checkSelectedIds()){
			form['_a'].value='changeStatus';
			form.submit();
		} else {
			alert('No order selected');
			return;
		}
	}
	 
</script>