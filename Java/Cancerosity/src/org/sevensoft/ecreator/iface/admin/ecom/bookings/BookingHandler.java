package org.sevensoft.ecreator.iface.admin.ecom.bookings;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.bookings.Booking;
import org.sevensoft.ecreator.model.bookings.BookingSlot;
import org.sevensoft.ecreator.model.bookings.BookingStatus;
import org.sevensoft.ecreator.model.ecom.addresses.Address;
import org.sevensoft.ecreator.model.ecom.payments.Payment;
import org.sevensoft.ecreator.model.ecom.payments.PaymentSettings;
import org.sevensoft.ecreator.model.ecom.payments.PaymentType;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.reminders.HolidayReminder;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.ButtonOpenerTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;
import java.util.List;

/**
 * @author sks 20 Mar 2007 11:10:40
 */
@Path("admin-bookings.do")
public class BookingHandler extends AdminHandler {

    private Booking booking;
    private String paymentReference;
    private String action;
    private int adults, children;
    private transient PaymentSettings paymentSettings;

    public BookingHandler(RequestContext context) {
        super(context);
        this.paymentSettings = PaymentSettings.getInstance(context);
    }

    public Object transact() throws ServletException {

        if (booking == null) {
            return main();
        }

        if (booking.getPaymentType() == PaymentType.CardTerminal) {

            if (paymentSettings.hasProcessor()) {

                try {

                    logger.config("[Order] Transact booking=" + booking.getId() + ", amount=" + booking.getTotalInc());

                    Payment payment = booking.transactCard();
                    addMessage("Booking billed \243" + booking.getTotalInc() + " Txn: " + payment.getProcessorTransactionId());

//					booking.log(user, "Transacted");

                } catch (Exception e) {

                    e.printStackTrace();
                    addError(e);
                }

            } else {

//				return new TransactHandler(context, booking).main();

            }
        }

        return main();
    }

    @Override
    public Object main() throws ServletException {

        if (booking == null) {
            return index();
        }

        AdminDoc doc = new AdminDoc(context, user, "Booking #" + booking.getIdString(), Tab.Bookings);
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                if (booking.getStatus() == BookingStatus.UNCONFIRMED) {
                    sb.append(new SubmitTag("action", "Confirm booking"));
                }
                if (user.isAdministrator()) {
                    sb.append(new ButtonTag(BookingHandler.class, "cancel", "Cancel booking", "booking", booking));
                }
                sb.append(new SubmitTag("action", "Update booking"));
                sb.append("</div>");
            }

            private void customer() {

                Address address = booking.getAddress();
                Item account = booking.getAccount();

                sb.append(new AdminTable("Customer"));

                sb.append(new AdminRow("Name", "This is the name of the person who made the booking.", account.getName()));

                ButtonOpenerTag clientDetails = new ButtonOpenerTag(context, new Link(ClientDetailsHandler.class, null, "booking", booking), "Client Details", 650, 500);
                clientDetails.setScrollbars(true);
                clientDetails.setTarget("_blank");

                sb.append(new AdminRow("Email", "This is the email of the person who made the booking.", account.getEmail() + "   " + clientDetails));

                if (address != null) {

                    sb.append(new AdminRow("Address", address.getLabel("<br/>", true)));

                }

                sb.append("</table>");
            }

            private void payment() {

                sb.append(new AdminTable("Payment"));

                sb.append("<tr><th width='130'>Payment method</th><td>" + booking.getPaymentType() + "</td></tr>");

                sb.append("<tr><th width='130'>Payment reference</th><td>");
                sb.append(new TextTag(context, "paymentReference", booking.getPaymentReference(), 10));
                if (booking.getStatus() == BookingStatus.CONFIRMED) {
                    sb.append(new SubmitTag("action", "Confirm payment"));
                }
                sb.append("</td></tr>");
                if (booking.getPaymentType() == PaymentType.CardTerminal) {

                    if (booking.hasCard()) {

                        sb.append("<tr><th width='130'>Card details</th><td>" + booking.getCard().getLabel(", ") + "</td></tr>");

                        sb.append("<tr><th width='130'>Make payment</th><td>");
                        if (paymentSettings.hasProcessor()) {
                            sb.append(new ButtonTag(BookingHandler.class, "transact", "Transact card", "booking", booking));
                        }
                        sb.append("</td></tr>");
                    }

                }

                sb.append("</table>");

            }

            private void payments() {

                List<Payment> payments = booking.getPayments();
                if (payments.isEmpty()) {
                    return;
                }

                sb.append(new AdminTable("Payments"));

                for (Payment payment : payments) {

                    sb.append("<tr><td>" + payment.getDate().toString("dd.MMM.yy") + "</td><td>" + payment.getDetails() + "</td><td>" +
                            payment.getAmount() + "</td></tr>");

                }

                sb.append("</table>");
            }

            private void general() {

                sb.append(new AdminTable("General"));
                sb.append(new AdminRow("Booking status", null, booking.getStatus()));
                sb.append(new AdminRow(booking.getItem().getItemTypeName(), null, booking.getItem().getName()));

                sb.append(new AdminRow("Start date", null, booking.getStart().toString("dd-MMM-yyyy")));
                sb.append(new AdminRow("End date", null, booking.getEnd().toString("dd-MMM-yyyy")));

                sb.append(new AdminRow(booking.getItem().getItemTypeName() + " charge", null, booking.getItemChargeEx()));

                if (booking.hasOptions()) {
                    sb.append(new AdminRow("Extra options", null, booking.getOptionsString()));
                    sb.append(new AdminRow("Options charges", null, booking.getOptionsChargesEx()));
                }

                sb.append(new AdminRow("Commission", null, booking.getCommissionValue()));
                sb.append(new AdminRow("Subtotal", null, booking.getTotalEx()));
                sb.append(new AdminRow("Vat", null, booking.getTotalVat()));
                sb.append(new AdminRow("Total", null, booking.getTotalInc()));

                sb.append(new AdminRow("Occupancy", "The name of this option.", "Adults " + new TextTag(context, "adults", booking.getAdults(), 4) +
                        " Children " + new TextTag(context, "children", booking.getChildren(), 4)));

                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(BookingHandler.class, "doAction", "POST"));
                sb.append(new HiddenTag("booking", booking));

                commands();
                general();
                payment();
                payments();
                customer();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object doAction() throws ServletException {
        if (action == null) {
            return index();
        }
        if (action.equals("Update booking")) {
            return save();
        } else if (action.equals("Confirm booking")) {
            return confirm();
        } else if (action.equals("Confirm payment")) {
            return confirmPayment();
        } else {
            return index();
        }
    }

    public Object cancel() {

        booking.clearBookingSlots();

        booking.setStatus(BookingStatus.CANCELLED);
        booking.save();
        return new ActionDoc(context, "Booking was cancelled", new Link(BookingHandler.class, null, "booking", booking.getId()));
    }


    public Object confirmPayment() {
        booking.setPaymentReference(paymentReference);
        booking.setStatus(BookingStatus.PAID);
        booking.save();
        BookingNotificationEmails.informOwnerOfPayment(context, booking);
        HolidayReminder reminder = new HolidayReminder(context);
        reminder.setEmail(booking.getAccount().getEmail());
        reminder.setHolidayDate(new DateTime(booking.getStart()));
        reminder.save();
        return new ActionDoc(context, "Booking was paid", new Link(BookingHandler.class, null, "booking", booking.getId()));
    }

    public Object confirm() {
        booking.setPaymentReference(paymentReference);
        booking.setStatus(BookingStatus.CONFIRMED);
        booking.setConfirmationPerson(user);
        booking.setConfirmationTime(new DateTime());
        booking.save();
        BookingNotificationEmails.informOwnerOfConfirmation(context, booking);
        BookingNotificationEmails.informClientOfConfirmation(context, booking);
        BookingSlot.setStatus(BookingSlot.Status.Booked, booking.getItem(), booking.getStart(), booking.getEnd());
        return new ActionDoc(context, "Booking was confirmed", new Link(BookingHandler.class, null, "booking", booking.getId()));
    }

    public Object save() {
        booking.setPaymentReference(paymentReference);
        booking.setAdults(adults);
        booking.setChildren(children);
        booking.save();
        return new ActionDoc(context, "Booking was updated", new Link(BookingHandler.class, null, "booking", booking.getId()));
    }

}
