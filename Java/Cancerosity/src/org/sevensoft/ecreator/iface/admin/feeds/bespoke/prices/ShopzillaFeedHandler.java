package org.sevensoft.ecreator.iface.admin.feeds.bespoke.prices;

import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.Feed;
import org.sevensoft.ecreator.model.feeds.bespoke.prices.shopzilla.ShopzillaFeed;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 3 Aug 2006 11:24:16
 *
 */
@Path("admin-feeds-shopzilla.do")
public class ShopzillaFeedHandler extends FeedHandler {

	private ShopzillaFeed	feed;
	private Attribute		manufAttribute;
	private Attribute		upcAttribute;
	private ItemType		itemType;

	public ShopzillaFeedHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Feed getFeed() {
		return feed;
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Shopzilla settings"));

		sb.append(new AdminRow("Item type", "Select the item type to import to.", new SelectTag(context, "itemType", feed.getItemType(), ItemType
				.getSelectionMap(context), "-None set-")));

		if (feed.hasItemType()) {

			SelectTag attributeTag = new SelectTag(context, "manufAttribute", feed.getManufAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append(new AdminRow("Manufacturer attribute", attributeTag));

			attributeTag = new SelectTag(context, "upcAttribute", feed.getUpcAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append(new AdminRow("UPC attribute", attributeTag));

		}

		sb.append("</table>");
	}

	@Override
	protected void saveSpecific() {

		feed.setManufAttribute(manufAttribute);
		feed.setUpcAttribute(upcAttribute);
		feed.setItemType(itemType);

	}

}
