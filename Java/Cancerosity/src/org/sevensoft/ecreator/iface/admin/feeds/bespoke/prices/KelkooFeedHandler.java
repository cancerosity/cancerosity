package org.sevensoft.ecreator.iface.admin.feeds.bespoke.prices;

import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.Feed;
import org.sevensoft.ecreator.model.feeds.bespoke.prices.kelkoo.KelkooFeed;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 3 Aug 2006 11:11:02
 *
 */
@Path("admin-feeds-kelkoo.do")
public class KelkooFeedHandler extends FeedHandler {

	private KelkooFeed	feed;
	private Attribute		manufAttribute;
	private ItemType		itemType;

	public KelkooFeedHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Feed getFeed() {
		return feed;
	}

	@Override
	protected void saveSpecific() {

		feed.setManufAttribute(manufAttribute);
		feed.setItemType(itemType);

	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Kelkoo specifics"));

		sb.append(new AdminRow("Item type", "Select the item type to import to.", new SelectTag(context, "itemType", feed.getItemType(), ItemType
				.getSelectionMap(context), "-None set-")));

		if (feed.hasItemType()) {

			SelectTag attributeTag = new SelectTag(context, "manufAttribute", feed.getManufAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append(new AdminRow("Manuf attribute", attributeTag));

		}

		sb.append("</table>");
	}
}
