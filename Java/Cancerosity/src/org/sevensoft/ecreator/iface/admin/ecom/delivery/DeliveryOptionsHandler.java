package org.sevensoft.ecreator.iface.admin.ecom.delivery;

import javax.servlet.ServletException;

import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.design.reorder.ObjectsReorderPanel;
import org.sevensoft.ecreator.iface.admin.ecom.ShoppingMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.ecom.delivery.DeliveryOption;
import org.sevensoft.ecreator.model.ecom.delivery.DeliverySettings;
import org.sevensoft.ecreator.model.ecom.delivery.DeliveryOption.Type;
import org.sevensoft.ecreator.model.items.pricing.PriceSettings;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

import java.util.List;

/**
 * @author sks
 */
@Path("admin-delivery-options.do")
public class DeliveryOptionsHandler extends AdminHandler {

	private int					cutOff, period, middleWeight, startQty, middleQty, endQty, qtyIncrement;
	private double				startWeight, endWeight, interval;
	private Money				flatCharge, qtyIncrementCharge, intervalCharge, startPrice, endPrice, middlePrice, priceIncrementCharge,
			priceIncrement;
	private String				name;
	private boolean				saturdayDelivery;
	private Type				type;
	private double				vatRate;
	private Country				country;
	private DeliveryOption			option;
	private Attribute				attribute;
	private Money				costPriceEx;
	private String				code;
	private transient DeliverySettings	deliverySettings;
	private boolean				saturdayDispatch;
	private boolean				saturdayService;
	private boolean				addAllCountries;
    private String postcodes;
    private List<DeliveryOption> positions;

    public DeliveryOptionsHandler(RequestContext context) {
		this(context, null);
	}

	public DeliveryOptionsHandler(RequestContext context, DeliveryOption deliveryBand) {
		super(context);
		option = deliveryBand;
		this.deliverySettings = DeliverySettings.getInstance(context);
	}

	public Object copy() throws ServletException {

		if (option == null) {
			return main();
		}

		try {
			option.clone();
			option.log(user, "Copied");
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}

		return new ActionDoc(context, "This delivery option has been copied", new Link(DeliveryOptionsHandler.class));
	}

	public Object create() throws ServletException {

		option = new DeliveryOption(context, "-new delivery option-");
		option.log(user, "Created");

		return new ActionDoc(context, "A new delivery option has been created", new Link(DeliveryOptionsHandler.class));
	}

	public Object delete() throws ServletException {

		if (option == null) {
			return main();
		}

		option.log(user, "Deleted");
		option.delete();

		return new ActionDoc(context, "This delivery option has been deleted", new Link(DeliveryOptionsHandler.class));
	}

	public Object edit() throws ServletException {

		if (option == null) {
			return main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit delivery option", Tab.Orders);
		doc.setMenu(new ShoppingMenu());
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update delivery option"));
				sb.append(new ButtonTag(DeliveryOptionsHandler.class, "copy", "Copy delivery option", "option", option));
				sb.append(new ButtonTag(DeliveryOptionsHandler.class, "delete", "Delete delivery option", "option", option)
						.setConfirmation("Are you sure you want to delete this delivery option?"));
				sb.append(new ButtonTag(DeliveryOptionsHandler.class, null, "Return to delivery menu"));
				sb.append("</div>");
			}

			private void countries() {

				sb.append(new AdminTable("Countries"));

                sb.append(new AdminRow("Applicable postcodes", "Enter possible postcodes for the list of countries or leave it empty, so it won't be used to check whether a correct delivery option was choosen in the basket",
                        new TextAreaTag(context, "postcodes", option.getPostcodes(), 80, 5)));

				StringBuilder sb2 = new StringBuilder();
				for (Country country : option.getCountries()) {

					sb2.append(country);
					sb2.append(" ");
					sb2.append(new LinkTag(DeliveryOptionsHandler.class, "removeCountry", "remove", "option", option, "country", country));
					sb2.append("<br/>");

				}

				sb.append(new AdminRow("Applicable countries", "These are the countries this delivery option is applicable to. "
						+ "Leave this blank if you want this option to apply to all countries", sb2));

				SelectTag tag = new SelectTag(context, "country");
				tag.setAny("-Choose country-");
				tag.addOptions(Country.getAll());

				sb.append("<tr><td colspan='2'>Add country " + tag + "</td></tr>");
				sb.append("<tr><td colspan='2'>Add all countries " + new CheckTag(context, "addAllCountries", true, false) + "</td></tr>");
				sb.append("</table>");
			}

			private void due() {

				sb.append(new AdminTable("Due dates"));

				sb.append(new AdminRow("Delivery period", "Set the maximum number of days this delivery can take to arrive.", new TextTag(context,
						"period", option.getPeriod(), 4)));

				sb
						.append(new AdminRow(
								"Cut off hour",
								"Enter here the hour after which deliveries will be counted as starting from the next available day. <i>Enter using 24 clock hours.</i>",
								new TextTag(context, "cutOff", option.getCutOff(), 4)));

				sb.append(new AdminRow("Saturday dispatch", "If you dispatch orders on a saturday then set this to yes.", new BooleanRadioTag(context,
						"saturdayDispatch", option.isSaturdayDispatch())));

				sb.append(new AdminRow("Saturday delivery", "If this order can be delivered on a saturday, then set this to yes.", new BooleanRadioTag(
						context, "saturdayDelivery", option.isSaturdayDelivery())));

				sb.append(new AdminRow("Saturday service", "If this is a special service for saturday delivery only, then set this to yes. "
						+ "Then only saturdays will be counted as valid delivery days ignoring the delivery period set above.",
						new BooleanRadioTag(context, "saturdayService", option.isSaturdayService())));

				sb.append("</table>");

			}

			private void general() {

				sb.append(new AdminTable("Option setup"));

				sb.append(new AdminRow("Name", "Enter a description of this delivery option.", new TextTag(context, "name", option.getName(), 40)));

				if (Company.getInstance(context).isVatRegistered()) {

					sb.append(new AdminRow("Vat rate", "This is the VAT rate applicable to this delivery option.", new TextTag(context, "vatRate",
							option.getVatRate(), 8)));

				}

				SelectTag tag = new SelectTag(context, "type", option.getType());
				tag.addOptions(DeliveryOption.Type.get());

				sb.append(new AdminRow("Type", "Choose how you want this delivery option to be calculated.", tag));

				// if cost pricing is enabled on any item type then show cost pricing in here for shipping
				if (PriceSettings.getInstance(context).isCostPricing()) {

					sb.append(new AdminRow("Cost price", "Set the cost to you for this delivery.", new TextTag(context, "costPriceEx", option
							.getCostPriceEx(), 8)));

				}

				if (deliverySettings.isCodes()) {
					sb.append(new AdminRow("Delivery code", "If you use 'product' codes for delivery, you can set them here.", new TextTag(context,
							"code", option.getCode(), 12)));
				}

				switch (option.getType()) {

				case Distance:
				case Item:
					break;

				case FlatRate:

					sb.append(new AdminRow("Charge", "Set the charge to the customer for this delivery.", new TextTag(context, "flatCharge", option
							.getFlatCharge(), 8)));

					break;

				case Weight:

					sb.append(new AdminRow("Range",
							"Enter a start and end range that will be used to calculate if this delivery option can be used.", "From "
									+ new TextTag(context, "startWeight", option.getStartValue(), 12) + " to "
									+ new TextTag(context, "endWeight", option.getEndValue(), 12)));

					sb.append(new AdminRow("Flat rate", "The flat rate is always charged as the starting rate.", new TextTag(context, "flatCharge",
							option.getFlatCharge(), 8)));

					sb.append(new AdminRow("Increments", "Incremental increases to the charge per interval.", "Charge an extra "
							+ new TextTag(context, "intervalCharge", option.getIntervalCharge(), 8) + " for every interval of "
							+ new TextTag(context, "interval", option.getInterval(), 8)));

					break;

				case Qty:

					sb.append(new AdminRow("Range", "Set quantity from and to that this option is applicable for.", "From "
							+ new TextTag(context, "startQty", option.getStartQty(), 6) + " to "
							+ new TextTag(context, "endQty", option.getEndQty(), 6)));

					sb.append(new AdminRow("Flat rate", "The flat rate is always charged in addition to the increment charges.", new TextTag(context,
							"flatCharge", option.getFlatCharge(), 8)));

					sb.append(new AdminRow("Increments", "Charge extra as the quantity increases.", "Charge an extra "
							+ new TextTag(context, "qtyIncrementCharge", option.getQtyIncrementCharge(), 8) + " for every quantity increment of "
							+ new TextTag(context, "qtyIncrement", option.getQtyIncrement(), 8)));

					break;

				case Price:

					sb.append(new AdminRow("Range", "Set price from and to that option is applicable for.", "From "
							+ new TextTag(context, "startPrice", option.getStartPrice(), 12) + " to "
							+ new TextTag(context, "endPrice", option.getEndPrice(), 12)));
                    
                    sb.append(new AdminRow("Flat rate", "The flat rate is always charged in addition to the increment charges.", new TextTag(context,
							"flatCharge", option.getFlatCharge(), 8)));

					sb.append(new AdminRow("Increments", "Charge extra based for each price increase.", "Charge an extra "
							+ new TextTag(context, "priceIncrementCharge", option.getPriceIncrementCharge(), 8)
							+ " for every price increment of " + new TextTag(context, "priceIncrement", option.getPriceIncrement(), 8)));

					break;

				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(DeliveryOptionsHandler.class, "save", "post"));
				sb.append(new HiddenTag("option", option));

				commands();
                general();
				if (deliverySettings.isEstimateDueDate()) {
					due();
				}

				countries();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		if (!Module.Delivery.enabled(context)) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Delivery options", Tab.Orders);
		doc.setMenu(new ShoppingMenu());
		doc.addBody(new Body() {

			@Override
			public String toString() {

                sb.append(new FormTag(DeliveryOptionsHandler.class, "reorder", "POST"));
                commands();

                general();

                commands();

                sb.append("</form>");
                return sb.toString();
            }

            private void commands() {
                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update"));
                sb.append("</div>");
            }

            private void general() {
                /*
				sb.append(new AdminTable("Delivery options"));

				sb.append("<tr>");
				sb.append("<th width='20'>Position</th>");
				sb.append("<th>Name</th>");
				sb.append("<th>Type</th>");
				sb.append("<th width='10'>Delete</th>");
				sb.append("</tr>");

				PositionRender pr = new PositionRender(DeliveryOptionsHandler.class, "option");

				int n = 1;
				for (DeliveryOption option : DeliveryOption.get(context)) {

					sb.append("<tr>");
					sb.append("<td width='20'>" + pr.render(option) + "</td>");
					sb.append("<td>" + new LinkTag(DeliveryOptionsHandler.class, "edit", new SpannerGif(), "option", option) + " " + option.getName()
							+ "</td>");
					sb.append("<td>" + option.getType() + "</td>");
					sb.append("<td width='10'>"
							+ new LinkTag(DeliveryOptionsHandler.class, "delete", new DeleteGif(), "option", option)
									.setConfirmation("Are you sure you want to delete this delivery option?") + "</td>");
					sb.append("</tr>");

					n++;
				}

				sb.append("<tr><td colspan='4'>Create a new delivery option " + new ButtonTag(DeliveryOptionsHandler.class, "create", "Create")
						+ "</td></tr>");
				sb.append("</table>");

				return sb.toString();*/
                sb.append(new AdminTable("Delivery options"));

                sb.append("<tr>");
                sb.append("<th width='20'>Position</th>");
                sb.append("<th>Name</th>");
                sb.append("<th>Type</th>");
                sb.append("<th width='10'>Delete</th>");
                sb.append("</tr>");

                ObjectsReorderPanel panel = new ObjectsReorderPanel(context, null, DeliveryOption.get(context));
                panel.setEditClazz(DeliveryOptionsHandler.class);
                panel.setEditAction("edit");
                panel.setEditParam("option");

                panel.setDeleteClazz(DeliveryOptionsHandler.class);
                panel.setDeleteAction("delete");
                panel.setDeleteParam("option");
                panel.setDeleteConfirmation("Are you sure you want to delete this delivery option?");
                panel.setCustomSpan("getType");

//                PositionRender pr = new PositionRender(DeliveryOptionsHandler.class, "option");
//
//				int n = 1;
//				for (DeliveryOption option : DeliveryOption.get(context)) {

                sb.append("<tr>");
                sb.append("<td colspan='4'>");
                sb.append(panel);
                sb.append("</td>");
                sb.append("</tr>");

//					n++;
//				}

                sb.append("<tr><td colspan='4'>Create a new delivery option " + new ButtonTag(DeliveryOptionsHandler.class, "create", "Create")
                        + "</td></tr>");
                sb.append("</table>");
            }

        });
		return doc;
	}

    public Object reorder() throws ServletException {
        reorder(positions);
        return new ActionDoc(context, "Delivary options positions have been updated", new Link(DeliveryOptionsHandler.class));
    }

    public Object moveDown() throws ServletException {

		if (option == null) {
			return main();
		}

		EntityUtil.moveDown(option, DeliveryOption.get(context));
		return main();
	}

	public Object moveUp() throws ServletException {

		if (option == null) {
			return main();
		}

		EntityUtil.moveUp(option, DeliveryOption.get(context));
		return main();
	}

	public Object removeCountry() throws ServletException {

		if (country == null || option == null) {
			return main();
		}

		option.removeCountry(country);
		option.save();

		option.log(user, "Country removed - " + country);

		return new ActionDoc(context, "The selected country has been removed", new Link(DeliveryOptionsHandler.class, "edit", "option", option));
	}

	public Object save() throws ServletException {

		if (option == null) {
			return main();
		}

		test(new RequiredValidator(), "name");
		if (hasErrors()) {
			return main();
		}

		option.setName(name);

		if (type != null) {
			if (!ObjectUtil.equal(option.getType(), type)) {
				option.setType(type);
				option.log(user, "Type changed - " + type);
			}
		}

		option.setPeriod(period);
		option.setSaturdayDelivery(saturdayDelivery);
		option.setFlatCharge(flatCharge);
		option.setVatRate(vatRate);
		option.setCutOff(cutOff);
		option.setCostPriceEx(costPriceEx);
		option.setCode(code);
		option.setSaturdayDispatch(saturdayDispatch);
		option.setSaturdayService(saturdayService);

		// price fields
		option.setStartPrice(startPrice);
		option.setEndPrice(endPrice);
		option.setPriceIncrement(priceIncrement);
		option.setPriceIncrementCharge(priceIncrementCharge);

        // qty fields
		option.setStartQty(startQty);
		option.setEndQty(endQty);
		option.setQtyIncrement(qtyIncrement);
		option.setQtyIncrementCharge(qtyIncrementCharge);

		// custom
		option.setStartWeight(startWeight);
		option.setEndWeight(endWeight);
		option.setInterval(interval);
		option.setIntervalCharge(intervalCharge);

		if (country != null) {
			option.addCountry(country);
			option.log(user, "Country added - " + country);
		}

		if (addAllCountries) {
			option.log(user, "Adding all countries");
			option.addAllCountries();
		}

        option.setPostcodes(postcodes);

		// calling save will recalc all baskets
		option.save();

		return new ActionDoc(context, "This delivery option has been updatd", new Link(DeliveryOptionsHandler.class, "edit", "option", option));
	}
}