package org.sevensoft.ecreator.iface.admin.design.template.panels;

import org.sevensoft.ecreator.iface.EcreatorRenderer;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.media.images.ImageSelectionHandler;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.ButtonOpenerTag;
import org.sevensoft.commons.superstrings.StringHelper;

import java.util.List;
import java.util.ArrayList;
import java.io.File;

/**
 * User: Tanya
 * Date: 17.08.2010
 */
public class TemplatesUploadPanel extends EcreatorRenderer {

    private MiscSettings miscSettings;

    public TemplatesUploadPanel(RequestContext context) {
        super(context);
        this.miscSettings = MiscSettings.getInstance(context);
    }

    @Override
    public String toString() {
        sb.append(new AdminTable("Upload image"));


        /*
        * Choose existing image
        */
//            File imageRoot = new File(config.getRealImagesPath());
        File imageRoot = ResourcesUtils.getRealTemplateDataDir();
        if (imageRoot.exists()) {

            File[] filenames = imageRoot.listFiles();
            if (filenames != null && filenames.length > 0) {

                ButtonOpenerTag openerTag = new ButtonOpenerTag(context, new Link(ImageSelectionHandler.class), "Select existing file", 650, 500);
                openerTag.setScrollbars(true);
                openerTag.setTarget("_blank");

                // SELECT EXISTING IMAGE TAG
                sb.append(new AdminRow("Select existing file", "Click this button to use an image that you have used previously.",
                        new HiddenTag("imageFilename", "").setId("imageFilename").toString() + openerTag));

            }
        }

        List<Object> tags = new ArrayList();
        for (int n = 0; n < miscSettings.getImageInputs(); n++) {
            tags.add(new FileTag("imageUploads", 40));
        }

        /*
        * Upload image from hard drive. Show file inputs (number to be determined by setting in misc settings
        */
        sb.append(new AdminRow("Upload image", "This will upload an image from your computer to the website.", StringHelper.implode(tags, " ")));

        tags = new ArrayList();
        for (int n = 0; n < miscSettings.getImageInputs(); n++) {
            tags.add(new TextTag(context, "imageUrls", 40));
        }

        /*
        * Image retrieval from url. Inputs number to be determined by setting in misc settings
        */
        sb.append(new AdminRow("Download image from website", "This will copy an image from the URL you paste here.",
                StringHelper.implode(tags, " ")));

        sb.append("<tr><td colspan='2'>" + new SubmitTag("Upload") + "</td></tr>");

        sb.append("</table>");

        return sb.toString();
    }
}
