package org.sevensoft.ecreator.iface.admin.items.menus;

import org.sevensoft.ecreator.iface.admin.ecom.bookings.PeriodListHandler;
import org.sevensoft.ecreator.iface.admin.ecom.bookings.PropertyHitCounterMonthly;
import org.sevensoft.ecreator.iface.admin.items.ItemExportHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.iface.admin.items.MultiItemsAdditionHandler;
import org.sevensoft.ecreator.iface.admin.items.listings.ListingModerationHandler;
import org.sevensoft.ecreator.iface.admin.items.modules.reports.ItemReportsHandler;
import org.sevensoft.ecreator.iface.admin.items.modules.stock.StockReportHandler;
import org.sevensoft.ecreator.iface.admin.items.search.ItemSearchHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Menu;
import org.sevensoft.ecreator.iface.admin.accounts.registration.RegistrationModerationsHandler;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sks 13 Oct 2006 14:18:07
 */
public class ItemsMenu extends Menu {

    private final ItemType itemType;

    public ItemsMenu(ItemType itemType) {
        this.itemType = itemType;

    }

    @Override
    public List<LinkTag> getLinkTags(RequestContext context) {

        Config config = Config.getInstance(context);
        boolean superman = context.containsAttribute("superman");

        List<LinkTag> links = new ArrayList();
        links.add(new LinkTag(ItemSearchHandler.class, null, "Find " + itemType.getNamePluralLower(), "itemType", itemType));
        links.add(new LinkTag(ItemHandler.class, "create", "Create " + itemType.getNamePluralLower(), "itemType", itemType));

        if (Module.ListingModeration.enabled(context)) {
            if (itemType.hasAwaitingModeration()) {
                if (itemType.isAccount(context)) {
                    links.add(new LinkTag(RegistrationModerationsHandler.class, null, "Moderate " + itemType.getNamePlural()));
                } else {
                    links.add(new LinkTag(ListingModerationHandler.class, null, "Moderate " + itemType.getNamePlural()));
                }
            }
        }

        if (Module.ItemExport.enabled(context) || superman) {
            links.add(new LinkTag(ItemExportHandler.class, null, "Export", "itemType", itemType));
        }

        if (ItemModule.Stock.enabled(context, itemType)) {
            links.add(new LinkTag(StockReportHandler.class, null, "Stock report", "itemType", itemType));
        }

        if (config.getUrl().contains("7soft")) {
            links.add(new LinkTag(ItemReportsHandler.class, null, "Reports", "itemType", itemType));
        }

//        if (MiscSettings.getInstance(context).isAdvancedMode() || superman) {
//            links.add(new LinkTag(AdvItemEditorHandler.class, null, "Advanced editor", "itemType", itemType));
//        }

        if (itemType.isBookable()) {
            links.add(new LinkTag(PeriodListHandler.class, null, "Period settings", "itemType", itemType));
            if (!Module.StatsBlock.enabled(context)) {
                links.add(new LinkTag(PropertyHitCounterMonthly.class, null, "Monthly hits for properties", "itemType", itemType));
            }
        }

        if (superman || Module.UserItemTypes.enabled(context)) {
            links.add(new LinkTag(ItemTypeHandler.class, "edit", itemType.getName() + " settings", "itemType", itemType));
        }

        if (ItemModule.MultiItemsAddition.enabled(context, itemType)) {
            links.add(new LinkTag(MultiItemsAdditionHandler.class, "edit", "Multiple " + itemType.getName() + " addition", "itemType", itemType));
        }

        return links;
    }

}
