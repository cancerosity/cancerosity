package org.sevensoft.ecreator.iface.admin.items.listings;

import javax.servlet.ServletException;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;
import org.sevensoft.ecreator.model.items.listings.ListingRate;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 28 Aug 2006 11:49:01
 *
 */
@Path("admin-listings-packages-rates.do")
public class ListingRateHandler extends AdminHandler {

	private ListingRate			listingRate;
	private ListingPackage			listingPackage;
	private int					credits;
	private int					period;
	private Money				fee;
	private final transient Company	company;
	private double	vatRate;

	public ListingRateHandler(RequestContext context) {
		super(context);
		this.company = Company.getInstance(context);
	}

	public Object create() throws ServletException {

		if (listingPackage == null)
			return new DashboardHandler(context).main();

		listingRate = listingPackage.addListingRate();
		return main();
	}

	public Object delete() throws ServletException {

		if (listingRate == null)
			return main();

		listingPackage = listingRate.getListingPackage();
		listingPackage.removeRate(listingRate);

		return new ListingPackageHandler(context, listingPackage).edit();
	}

	@Override
	public Object main() throws ServletException {

		if (!Module.Listings.enabled(context))
			return new DashboardHandler(context).main();

		if (listingRate == null)
			return new DashboardHandler(context).main();

		AdminDoc page = new AdminDoc(context, user, "Edit listing rate", null);
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update listing rate"));
				sb.append(new ButtonTag(ListingRateHandler.class, "delete", "Delete listing rate", "listingRate", listingRate)
						.setConfirmation("Are you sure you want to delete this listing rate?"));
				sb.append(new ButtonTag(ListingPackageHandler.class, "edit", "Exit and return", "listingPackage", listingRate.getListingPackage()));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("Details"));

				sb.append(new AdminRow("Fee", "Set a cost for this listing rate.", new TextTag(context, "fee", listingRate.getFeeEx(), 8)));

				sb.append(new AdminRow("Period", "How long a listing added through this package should last for (days). "
						+ "If you set this to zero then the listing will be a lifetime listing (will not expire).", new TextTag(context, "period",
						listingRate.getPeriod(), 4)));

				if (company.isVatRegistered()) {

					if (listingRate.hasFee()) {

						sb.append(new AdminRow("Vat rate", "set the VAT rate used for this rate.", new TextTag(context, "vatRate", listingRate
								.getVatRate(), 8)));

					}
				}

				sb.append("</table>");

			}

			@Override
			public String toString() {

				sb.append(new FormTag(ListingRateHandler.class, "save", "POST"));
				sb.append(new HiddenTag("listingRate", listingRate));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return page;
	}

	public Object save() throws ServletException {

		if (listingRate == null)
			return main();

		listingRate.setCredits(credits);
		listingRate.setFeeEx(fee);
		listingRate.setPeriod(period);
		listingRate.setVatRate(vatRate);
		listingRate.save();

		addMessage("This listing rate has been updated with your changes.");
		clearParameters();
		return main();
	}
}
