package org.sevensoft.ecreator.iface.admin.reports.downloads;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.reports.StatsMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.stats.downloads.DownloadCounterMonthly;
import org.sevensoft.ecreator.model.stats.downloads.DownloadCounterWeekly;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 03-Oct-2005 16:12:08
 * 
 */
@Path("admin-reports-downloads.do")
public class DownloadsCounterHandler extends AdminHandler {

	private Date	month;
	private Date	week;

	public DownloadsCounterHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {
		return monthly();
	}

	/**
	 * Shows the most popular recorders this month
	 */
	public Object monthly() throws ServletException {

		if (month == null) {
			month = new Date();
		}

		month = month.beginMonth();

		int limit = 100;

		final List<DownloadCounterMonthly> counters = DownloadCounterMonthly.get(context, month, limit);

		AdminDoc doc = new AdminDoc(context, user, "Downloads per month for " + month.toString("MMMM-yy"), Tab.Stats);
		doc.setMenu(new StatsMenu());
		doc.setIntro("This page shows the " + limit + " most popular pages for the month " + month.toString("MMMM-yy"));
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Downloads per month for " + month.toString("MMMM-yy")));

				sb.append(new FormTag(DownloadsCounterHandler.class, "monthly", "get"));
				SelectTag monthTag = new SelectTag(context, "month");
				monthTag.setAutoSubmit();

				Date month = new Date().beginMonth();
				for (int n = 0; n < 13; n++) {
					monthTag.addOption(month, month.toString("MMMM-yy"));
					month = month.removeMonths(1);
				}

				sb.append("<tr><th align='right' colspan='4'>Choose month ");
				sb.append(monthTag);
				sb.append("</th></tr>");

				sb.append("</form>");

				sb.append("<tr>");
				sb.append("<td>Page</td>");
				sb.append("<td>File</td>");
				sb.append("<td>Downloads</td>");

				if (Module.AttachmentPreviews.enabled(context)) {
					sb.append("<td>Previews</td>");
				}

				sb.append("</tr>");

				for (DownloadCounterMonthly counter : counters) {

					String page = counter.getPage();
					if (page == null) {
						page = "";
					}

					String filename = counter.getFilename();
					if (filename == null) {
						filename = "";
					}

					sb.append("<tr>");
					sb.append("<td>" + filename + "</td>");
					sb.append("<td>" + page + "</td>");

					sb.append("<td>" + counter.getFull() + "</td>");

					if (Module.AttachmentPreviews.enabled(context)) {
						sb.append("<td>" + counter.getPreviews() + "</td>");
					}

					sb.append("</tr>");
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object weekly() throws ServletException {

		if (week == null) {
			week = new Date();
		}

		week = week.beginWeek();

		int limit = 100;

		final List<DownloadCounterWeekly> counters = DownloadCounterWeekly.get(context, week, limit);

		final String title = "Downloads per week for week ending " + week.addDays(6).toString("dd-MMMM-yy");
		AdminDoc doc = new AdminDoc(context, user, title, Tab.Stats);
		doc.setMenu(new StatsMenu());
		doc.setIntro("This page shows the " + limit + " most popular pages for the week " + week.toString("MMMM-yy"));
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable(title));

				sb.append(new FormTag(DownloadsCounterHandler.class, "weekly", "get"));
				SelectTag weekTag = new SelectTag(context, "week");
				weekTag.setAutoSubmit();

				Date d = new Date().beginWeek();
				for (int n = 0; n < 56; n++) {
					weekTag.addOption(d, d.toString("dd--MMMM-yy"));
					d = d.removeWeeks(1);
				}

				sb.append("<tr><th align='right' colspan='4'>Choose week ");
				sb.append(weekTag);
				sb.append("</th></tr>");

				sb.append("</form>");

				sb.append("<tr>");
				sb.append("<td>Page</td>");
				sb.append("<td>File</td>");
				sb.append("<td>Full downloads</td>");

				if (Module.AttachmentPreviews.enabled(context)) {
					sb.append("<td>Previews</td>");
				}

				sb.append("</tr>");

				for (DownloadCounterWeekly counter : counters) {

					String page = counter.getPage();
					if (page == null) {
						page = "";
					}

					String filename = counter.getFilename();
					if (filename == null) {
						filename = "";
					}

					sb.append("<tr>");
					sb.append("<td>" + filename + "</td>");
					sb.append("<td>" + page + "</td>");

					sb.append("<td>" + counter.getFull() + "</td>");

					if (Module.AttachmentPreviews.enabled(context)) {
						sb.append("<td>" + counter.getPreviews() + "</td>");
					}

					sb.append("</tr>");
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}
}
