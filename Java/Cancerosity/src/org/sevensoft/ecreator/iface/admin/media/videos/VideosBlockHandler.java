package org.sevensoft.ecreator.iface.admin.media.videos;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.media.videos.blocks.VideosBlock;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 4 Apr 2007 15:30:05
 *
 */
@Path("admin-videos-blocchi.do")
public class VideosBlockHandler extends BlockEditHandler {

	private VideosBlock	block;

	public VideosBlockHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Block getBlock() {
		return block;
	}

	@Override
	protected void saveSpecific() {
	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

}
