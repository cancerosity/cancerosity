package org.sevensoft.ecreator.iface.admin.marketing.sms;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.marketing.sms.blocks.SmsBlock;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Apr 2007 10:31:25
 *
 */
@Path("admin-sms-blocks.do")
public class SmsBlockHandler extends BlockEditHandler {

	private SmsBlock	block;

	public SmsBlockHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Block getBlock() {
		return block;
	}

	@Override
	protected void saveSpecific() {
	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

}
