package org.sevensoft.ecreator.iface.admin.crm.jobsheets;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.model.attributes.renderers.OwnerAttributesPanel;
import org.sevensoft.ecreator.model.crm.jobsheets.Job;
import org.sevensoft.ecreator.model.crm.jobsheets.Jobsheet;
import org.sevensoft.ecreator.model.crm.jobsheets.Job.Status;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 18 Jul 2006 16:57:43
 *
 */
@Path("admin-jobsheets.do")
public class JobsheetHandler extends AdminHandler {

	private Jobsheet		jobsheet;
	private String		name;
	private Job.Status	status;
	private String		newAttributes;
	private int			amberStatusDays;
	private int			redStatusDays;
	private String		creationEmails;
	private String		newAttributesSection;
	private int			newAttributesPage;
	private List<Job>		jobs;
	private boolean		completionEmailToContributors;
	private String		completionEmails;

	public JobsheetHandler(RequestContext context) {
		super(context);
	}

	public JobsheetHandler(RequestContext context, Jobsheet jobsheet, Status status) {
		super(context);
		this.jobsheet = jobsheet;
		this.status = status;
	}

	public Object add() throws ServletException {

		if (name == null) {
			return main();
		}

		new Jobsheet(context, name);
		clearParameters();
		return main();
	}

	public Object delete() throws ServletException {

		if (jobsheet == null) {
			return main();
		}

		jobsheet.delete();
		return main();
	}

	public Object deleteJobs() throws ServletException {

		if (jobsheet == null) {
			return main();
		}

		for (Job job : jobs) {
			jobsheet.removeJob(job);
		}

		return view();
	}

	public Object edit() throws ServletException {

		if (jobsheet == null)
			return main();

		AdminDoc doc = new AdminDoc(context, user, "Edit jobsheet: " + jobsheet.getName(), Tab.Extras);
		doc.setMenu(new ExtrasMenu());
		doc.addBody(new Body() {

			/**
			 * 
			 */
			private void commands() {
				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update item type"));
				sb.append(new ButtonTag(JobsheetHandler.class, "delete", "Delete jobsheet", "jobsheet", jobsheet)
						.setConfirmation("Are you sure you want to delete this jobsheet?"));
				sb.append(new ButtonTag(JobsheetHandler.class, null, "Return to jobsheets"));
				sb.append("</div>");
			}

			/**
			 * 
			 */
			private void general() {
				sb.append(new AdminTable("Jobsheet"));

				sb.append(new AdminRow("Name", "Enter the name of this jobsheet", new TextTag(context, "name", jobsheet.getName(), 40)));

				sb.append(new AdminRow("Amber status", "Change jobs to amber status after how many days?", new TextTag(context, "amberStatusDays",
						jobsheet.getAmberStatusDays(), 4)));

				sb.append(new AdminRow("Red status", "Change jobs to red status after how many days?", new TextTag(context, "redStatusDays", jobsheet
						.getRedStatusDays(), 4)));

				sb.append(new AdminRow("Completion email to contributors", "Send a completion email to all people who contributed a reply to the job.",
						new BooleanRadioTag(context, "completionEmailToContributors", jobsheet.isCompletionEmailToContributors())));

				sb.append(new AdminRow("Creation emails", "Email addresses to be sent an email when a new job is created.", new TextAreaTag(context,
						"creationEmails", StringHelper.implode(jobsheet.getCreationEmails(), "\n"), 50, 5)));

				sb.append(new AdminRow("Completion emails", "Email addresses to be sent an email when a job is completed", new TextAreaTag(context,
						"completionEmails", StringHelper.implode(jobsheet.getCompletionEmails(), "\n"), 50, 5)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(JobsheetHandler.class, "save", "post"));
				sb.append(new HiddenTag("jobsheet", jobsheet));

                commands();
				general();

				sb.append(new OwnerAttributesPanel(context, jobsheet));

				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	/**
	 * Show all jobsheets
	 */
	@Override
	public Object main() throws ServletException {

		final List<Jobsheet> jobsheets = Jobsheet.get(context);

		AdminDoc doc = new AdminDoc(context, user, "Jobsheets", Tab.Extras);
		doc.setMenu(new ExtrasMenu());
		doc.addBody(new Body() {

			private void jobsheets() {

				sb.append(new AdminTable("Jobsheets"));

				sb.append("<tr>");
				sb.append("<th width='80'>Position</th>");
				sb.append("<th>Jobsheet</th>");
				sb.append("<th width='60'>Completed</th>");
				sb.append("<th width='60'>Outstanding</th>");
				sb.append("<th>Next job</th>");
				sb.append("<th width='10'> </th>");
				sb.append("</tr>");

				PositionRender pr = new PositionRender(JobsheetHandler.class, "jobsheet");
				for (Jobsheet jobsheet : jobsheets) {

					Job job = jobsheet.getNextJob();

					sb.append("<tr>");
					sb.append("<td width='80'>" + pr.render(jobsheet) + "</td>");
					sb.append("<td valign='middle'>"
							+ new LinkTag(JobsheetHandler.class, "edit", new ImageTag("files/graphics/admin/spanner.gif"), "jobsheet", jobsheet)
									.setAlign("absmiddle")
							+ " "
							+ new LinkTag(JobsheetHandler.class, "view", jobsheet.getName(), "jobsheet", jobsheet, "status",
									Job.Status.Outstanding) + "</td>");

					sb.append("<td width='60'>"
							+ new LinkTag(JobsheetHandler.class, "view", jobsheet.getCompletedJobCount(), "jobsheet", jobsheet, "status",
									Job.Status.Completed) + "</td>");

					sb.append("<td width='60'>"
							+ new LinkTag(JobsheetHandler.class, "view", jobsheet.getOutstandingJobCount(), "jobsheet", jobsheet, "status",
									Job.Status.Outstanding) + "</td>");

					sb.append("<td>" + (job == null ? "N/A" : job.getName() + " by " + job.getCreatedBy()) + "</td>");

					sb.append("<td width='10'>"
							+ new LinkTag(JobsheetHandler.class, "delete", new DeleteGif(), "jobsheet", jobsheet)
									.setConfirmation("If you delete this jobsheet it will be lost. Continue?") + "</td>");

					sb.append("</tr>");
				}

				sb.append(new FormTag(JobsheetHandler.class, "add", "post"));
				sb.append("<tr><td colspan='6'>Add a new jobsheet: " + new TextTag(context, "name", 20) + "</td></tr>");
				sb.append("</form>");

				sb.append("</table>");

			}

			@Override
			public String toString() {

				jobsheets();
				return sb.toString();
			}

		});
		return doc;
	}

	public Object moveDown() throws ServletException {

		if (jobsheet == null) {
			return main();
		}

		EntityUtil.moveDown(jobsheet, Jobsheet.get(context));
		return main();
	}

	public Object moveUp() throws ServletException {

		if (jobsheet == null) {
			return main();
		}

		EntityUtil.moveUp(jobsheet, Jobsheet.get(context));
		return main();
	}

	public Object save() throws ServletException {

		if (jobsheet == null) {
			return main();
		}

		jobsheet.setName(name);
		jobsheet.setRedStatusDays(redStatusDays);
		jobsheet.setAmberStatusDays(amberStatusDays);
		jobsheet.setCreationEmails(StringHelper.explodeStrings(creationEmails, "\n"));
		jobsheet.setCompletionEmails(StringHelper.explodeStrings(completionEmails, "\n"));
		jobsheet.setCompletionEmailToContributors(completionEmailToContributors);
		jobsheet.save();

		if (newAttributes != null) {
			jobsheet.addAttributes(newAttributes, newAttributesSection, newAttributesPage);
		}

		return edit();
	}

	public Object view() throws ServletException {

		if (jobsheet == null) {
			return main();
		}

		final List<Job> jobs = jobsheet.getJobs(status);

		AdminDoc doc = new AdminDoc(context, user, "Jobsheet: " + jobsheet.getName(), Tab.Extras);
		doc.setMenu(new ExtrasMenu());
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new ButtonTag(JobsheetHandler.class, null, "Return to jobsheets list"));
				sb.append("</div>");
			}

			private void jobs() {

				sb.append(new AdminTable(status + " jobs: " + jobsheet.getName()));

				sb.append("<tr>");
				sb.append("<th width='10'> </th>");
				sb.append("<th width='10'> </th>");
				sb.append("<th width='120'>Date created</th>");
				sb.append("<th>Created by</th>");
				sb.append("<th>Name</th>");
				sb.append("<th>Last message date</th>");
				sb.append("<th>Last message author</th>");
				sb.append("<th width='10'> </th>");
				sb.append("</tr>");

				sb.append(new FormTag(JobsheetHandler.class, "deleteJobs", "post"));
				sb.append(new HiddenTag("jobsheet", jobsheet));

				int n = 1;
				for (Job job : jobs) {

					sb.append("<tr>");
					sb.append("<th width='10'>" + new CheckTag(context, "jobs", job) + "</th>");
					sb.append("<td width='10'>" + n + "</td>");
					sb.append("<td width='120'>" + job.getDateCreated().toString("dd MMM yyyy HH:mm") + "</td>");
					sb.append("<td>" + job.getCreatedBy() + "</td>");

					ImageTag imageTag;
					switch (job.getPriority()) {
					default:
					case Green:
						imageTag = new ImageTag("files/graphics/admin/traffic_green.gif");
						break;

					case Amber:
						imageTag = new ImageTag("files/graphics/admin/traffic_amber.gif");
						break;

					case Red:
						imageTag = new ImageTag("files/graphics/admin/traffic_red.gif");
						break;
					}

					imageTag.setAlign("absmiddle");

					sb.append("<td>" + imageTag + " " + new LinkTag(JobHandler.class, null, job.getName(), "job", job) + "</td>");

					sb.append("<td>");
					if (job.hasMessages()) {
						sb.append(job.getLastMessageDate().toString("dd-MMM-yyyy HH:mm"));
					}
					sb.append("</td>");

					sb.append("<td>");
					if (job.hasMessages()) {
						sb.append(job.getLastMessageAuthor());
					}
					sb.append("</td>");

					sb.append("<td width='10'>" + new LinkTag(JobHandler.class, "delete", new DeleteGif(), "job", job) + " </td>");
					sb.append("</tr>");

					n++;
				}

				sb.append("<tr><td colspan='8'>" + new SubmitTag("Delete selected jobs") + "</td></tr>");
				sb.append("</form>");

				sb.append(new FormTag(JobHandler.class, "create", "get"));
				sb.append(new HiddenTag("jobsheet", jobsheet));

				if (status == Job.Status.Outstanding) {

					sb.append("<tr><td style='padding: 10px 2px;' colspan='8'>Add job: "
							+ new TextTag(context, "name", 40).setStyle("font-size: 13px; font-weight: bold;") + " " + new SubmitTag("Add")
							+ "</td></tr>");

				}

				sb.append("</form>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				jobs();
				commands();

				return sb.toString();
			}

		});
		return doc;
	}
}
