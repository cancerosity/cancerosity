package org.sevensoft.ecreator.iface.admin.crm.forms;

import javax.servlet.ServletException;

import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.crm.forms.FieldType;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.FormField;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 07-Jul-2005 12:39:17
 * 
 */
@Path("admin-forms-fields.do")
public class FormFieldHandler extends AdminHandler {

	private int		width, height, cols;
	private FormField	field;
	private Form	form;
	private String	name;
	private FieldType	type;
	private String	options;
	private boolean	optional;
	private String	instruction;
	private String	errorMessage;
	private String	regExp;
	private int		span;
	private String	section;
	private int		gridColumns;
	private String	description;
    private boolean repeatEmail;

	public FormFieldHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		if (form == null) {
			return index();
		}

		field = form.addField("new field", type);
		return main();
	}

	public Object delete() throws ServletException {

		if (field == null) {
			return main();
		}

		field.delete();
		addMessage("The field called '" + field.getName() + "' has been deleted.");
		return new FormHandler(context, field.getForm()).edit();
	}

	public Object main() throws ServletException {

		if (field == null) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit form field", Tab.Categories);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update field"));
				sb.append(new ButtonTag(FormFieldHandler.class, "delete", "Delete field", "field", field)
						.setConfirmation("If you delete this field it cannot be restored. Continue?"));
				sb.append(new ButtonTag(FormHandler.class, "edit", "Return to form", "form", field.getForm()));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("Field"));

				sb.append(new AdminRow("Name", "Enter the name of this form field", new TextAreaTag(context, "name", field.getName(), 50, 2)));

				if (miscSettings.isAdvancedMode()) {

					sb.append(new AdminRow("Section", "Place your fields in sections for layout.", new TextTag(context, "section",
							field.getSection(), 30)));

					sb.append(new AdminRow("Grid cell span", "Set how many cells this field will use for layout.", new TextTag(context, "span", field
							.getCellSpan(), 4)));

					sb.append(new AdminRow("Grid columns", "Set how many columns the layout grid will use.", new TextTag(context, "gridColumns",
							field.getGridColumns(), 4)));
				}

				SelectTag typeTag = new SelectTag(context, "type", field.getType());
				typeTag.addOptions(FieldType.values());

				sb.append(new AdminRow("Type", "Set the type of your field here.", typeTag));

				switch (field.getType()) {

				default:
					break;

				case Attachment:
					description();
					break;

				case Description:
					break;

				case DropDownMenu:

					description();
					sb.append(new AdminRow("Instruction", "This instruction will appear as a non selectable option in the menu.", new TextTag(
							context, "instruction", field.getInstruction(), 30)));
					break;

				case Email:
					description();
                    repeat();
					break;

				case Header:
					break;

				case Radio:
					description();
					break;

				case Separator:
					break;

				case Text:
					description();
                    repeat();
					break;

				case TickBox:
					description();
					break;

				case TickBoxes:
					description();
					break;

				}

				if (field.offerOptional()) {

					sb.append(new AdminRow("Optional", "If you set the field to optional then the site will not require the user to complete it.",
							new BooleanRadioTag(context, "optional", field.isOptional())));

                    //not mandatory fields also can be filled with wrong data
//					if (field.isMandatory()) {

						sb.append(new AdminRow("Error message", "Enter a custom error message here to display when this field is not completed. "
								+ "If you leave this blank it will use a standard error message.", new TextTag(context, "errorMessage", field
								.getErrorMessage(), 40)));

//					}

					if (field.isText()) {

						sb.append(new AdminRow("Match string",
								"Enter a regular expression and a value will need to match it before it is accepted.", new TextTag(context,
										"regExp", field.getRegExp(), 30)));

					}
				}

				if (field.useDimensions()) {

					sb.append(new AdminRow("Dimensions", "Enter the width and height of the input field.", "Width "
							+ new TextTag(context, "width", field.getWidth(), 4) + " Height"
							+ new TextTag(context, "height", field.getHeight(), 4)));

				}

				if (field.useCols()) {

					sb.append(new AdminRow("Columns", "How many options should be shown per line. "
							+ "Eg, if you set this to 3 and your field has 6 options then you will get 2 rows of 3 options per row.",
							new TextTag(context, "cols", field.getCols(), 4)));
				}

				if (field.useOptions()) {

					sb.append(new AdminRow("Options", "Enter each option for this field on a separate line.", new TextAreaTag(context, "options",
							field.getOptionsString(), 60, 6)));

				}

				sb.append("</table>");
			}

			private void description() {

				sb.append(new AdminRow("Description", "Enter a description to help the user.", new TextAreaTag(context, "description", field
						.getDescription(), 50, 3)));
			}
           //todo repeat email 
           private void repeat() {

               sb.append(new AdminRow("Repeat email", "Set YES if you need a double email field at the front",
                       new BooleanRadioTag(context, "repeatEmail", field.isRepeatEmail())));
           }

			@Override
			public String toString() {

				sb.append(new FormTag(FormFieldHandler.class, "save", "POST"));
				sb.append(new HiddenTag("field", field));

                commands();
				general();
				commands();

				sb.append("</form>");
				return sb.toString();
			}
		});
		return doc;
	}

	public Object moveDown() throws ServletException {

		if (field == null) {
			return main();
		}

		EntityUtil.moveDown(field, field.getForm().getFields());
		return new FormHandler(context, field.getForm()).edit();
	}

	public Object moveUp() throws ServletException {

		if (field == null) {
			return main();
		}

		EntityUtil.moveUp(field, field.getForm().getFields());
		return new FormHandler(context, field.getForm()).edit();
	}

	public Object save() throws ServletException {

		if (field == null) {
			return main();
		}

		test(new RequiredValidator(), "name");
		test(new RequiredValidator(), "type");
		if (hasErrors()) {
			return main();
		}

		field.setCols(cols);
		field.setHeight(height);
		field.setName(name);
		field.setWidth(width);
		field.setOptional(optional);
		field.setInstruction(instruction);
		field.setErrorMessage(errorMessage);
		field.setRegExp(regExp);
		field.setDescription(description);
        field.setRepeatEmail(repeatEmail);

		if (type != null) {
			field.setType(type);
		}

		if (miscSettings.isAdvancedMode()) {
			field.setSection(section);
			field.setCellSpan(span);
			field.setGridColumns(gridColumns);
		}

		field.setOptionsString(options);
		field.save();

		clearParameters();
		addMessage("Changes saved.");
		return main();
	}
}
