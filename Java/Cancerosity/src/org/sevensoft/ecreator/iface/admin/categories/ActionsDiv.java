package org.sevensoft.ecreator.iface.admin.categories;

/**
 * @author sks 1 May 2007 11:10:20
 *
 */
public class ActionsDiv {

	private final Object[]	objs;

	public ActionsDiv(Object... objs) {
		this.objs = objs;

	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder("<div class='actions'>");
		for (Object obj : objs) {
			sb.append(obj);
		}
		sb.append("</div>");
		return sb.toString();
	}

}
