package org.sevensoft.ecreator.iface.admin.items.highighted.block;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.iface.admin.misc.html.EditMarkupButton;
import org.sevensoft.ecreator.iface.admin.search.SearchEditHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.highlighted.HighlightMethod;
import org.sevensoft.ecreator.model.items.highlighted.blocks.HighlightedItemsBlock;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 26 Jun 2006 17:32:16
 *
 */
@Path("admin-blocks-highlighted-items.do")
public class HighlightedItemsBlockHandler extends BlockEditHandler {

	private HighlightedItemsBlock	block;
	private ItemType			itemType;
	private boolean			imagesOnly;
	private int				limit;
	private HighlightMethod		method;
	private Markup			markup;

	public HighlightedItemsBlockHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Block getBlock() {
		return block;
	}

	@Override
	protected String getTitle() {
		return "Edit highlighted items block";
	}

	@Override
	protected void saveSpecific() {

		logger.fine("[HighlightedItemsBlockHandler] saving block details");

		Search search = block.getSearch();

		search.setLimit(limit);
		search.setImageOnly(imagesOnly);
		search.setMethod(method);
		search.setItemType(itemType);
		search.save();

		if (Module.UserMarkup.enabled(context) || isSuperman()) {
			block.setMarkup(markup);
		}
	}

	@Override
	public void specifics(StringBuilder sb) {

		sb.append(new AdminTable("General"));

		Search search = block.getSearch();
		
		SelectTag selectTag = new SelectTag(context, "method", search.getMethod(), HighlightMethod.getItem(context), "-None selected-");
		sb.append(new AdminRow("Method", "Select what type of highlighted items to show", selectTag));

		sb.append(new AdminRow("Limit", "Max number of results to show", new TextTag(context, "limit", search.getLimit(), 8)));

		sb.append(new AdminRow("Images only", "Show show items with an image", new BooleanRadioTag(context, "imagesOnly", search.isImageOnly())));

		sb.append(new AdminRow("Item type", "Limit items to a particular item type", new SelectTag(context, "itemType", search.getItemType(), ItemType
				.getSelectionMap(context), "-Select item Type-")));

		if (miscSettings.isAdvancedMode() || isSuperman()) {

			sb.append(new AdminRow(miscSettings.isJimMode(), "Search", "The search controls how items are retrieved for this block.", new ButtonTag(
					SearchEditHandler.class, null, "Edit search", "search", search)));

		}

		if (Module.UserMarkup.enabled(context) || isSuperman()) {

			sb.append(new AdminRow(!Module.UserMarkup.enabled(context), "Markup", null, new SelectTag(context, "markup", block.getMarkup(), Markup
					.get(context), "Use default").setId("markup")
					+ " " + new EditMarkupButton("markup")));

		}

		sb.append("</table>");

	}

}
