package org.sevensoft.ecreator.iface.admin.comments;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;

/**
 * User: Tanya
 * Date: 29.07.2010
 */
public class AddCommentsPanel extends Panel {

    public AddCommentsPanel(RequestContext context) {
        super(context);
    }

    public void makeString() {

        sb.append("<tr><td>" + new TextTag(context, "title", 80) + "</td></tr>");
        sb.append("<tr><td>" + new TextAreaTag(context, "comment", 80, 5) + "</td></tr>");
        sb.append("<tr><td>" + new SubmitTag("Add comment") + "</td></tr>");

    }
}
