package org.sevensoft.ecreator.iface.admin.search;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.Attribute.DateSearchType;
import org.sevensoft.ecreator.model.search.forms.FieldType;
import org.sevensoft.ecreator.model.search.forms.SearchField;
import org.sevensoft.ecreator.model.search.forms.SearchForm;
import org.sevensoft.ecreator.model.search.forms.SearchGroup;
import org.sevensoft.ecreator.model.search.forms.SearchField.DateMethod;
import org.sevensoft.ecreator.model.search.forms.SearchField.SelectionMethod;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.ImageTag;

/**
 * @author sks 5 Sep 2006 16:10:18
 *
 */
@Path("admin-search-forms-fields.do")
public class SearchFormFieldHandler extends AdminHandler {

	private SearchField	field;
	private SearchForm	form;
	private Attribute		attribute;
	private String		name;
	private String		ages;
	private String		any;
	private FieldType		type;
	private String		distances;
	private String		ranges;
	private boolean		optional;
	private boolean		multiValues;
	private int			cols;
	private int			endYear;
	private int			startYear;
	private boolean		hideYear;
	private int			height;
	private int			width;
	private boolean		autoSubmit;
	private boolean		fixedBands;
	private boolean		hidden;
	private String		value;
	private DateSearchType	dateSearchType;
	private int			yearRange;
	private String		durations;
	private SelectionMethod	selectionStyle;
	private DateMethod	dateMethod;
    private boolean padding;

	public SearchFormFieldHandler(RequestContext context) {
		super(context);
	}

    public SearchFormFieldHandler(RequestContext context, SearchField field) {
        super(context);
        this.field = field;
    }

	public Object delete() throws ServletException {

		if (field == null)
			return index();

		form = field.getSearchForm();
		field.delete();

		return new SearchFormHandler(context, form).main();
	}

	@Override
	public Object main() throws ServletException {

		if (field == null)
			return index();

		AdminDoc doc = new AdminDoc(context, user, "Edit search field", null);
		doc.addBody(new Body() {

			private void attribute() {

				SelectTag attributeTag = new SelectTag(context, "attribute", field.getAttribute(), field.getSearchForm().getAttributes(),
						"-Select attribute-");
				sb.append(new AdminRow("Attribute", "Select attribute for this field", attributeTag));

				if (field.hasAttribute()) {

					switch (field.getAttribute().getType()) {

					default:
						break;

					case Date:

						if (field.isInput()) {

							sb.append(new AdminRow("Date search type", "Select how the date search will work.", new SelectTag(context,
									"dateSearchType", field.getDateSearchType(), DateSearchType.values(), null)));

							sb.append(new AdminRow("Selection method", "Select how the user can choose a date.", new SelectTag(context,
									"dateMethod", field.getDateMethod(), DateMethod.values(), null)));

							sb.append(new AdminRow("Start year",
									"The first value for the year drop down. Leave at 0 to start from the current year.", new TextTag(
											context, "startYear", field.getStartYear(), 8)));

							sb.append(new AdminRow("End year", "The last value for the year drop down.", new TextTag(context, "endYear", field
									.getEndYear(), 8)));

							sb.append(new AdminRow("Year range", "Show a number of years beginning with the 'start year' entered above."
									+ "<i>Eg, a start year of 2005 and a year range of 10 will populate the drop down menu with years "
									+ "from 2005 to 2015 inclusive.</i>", new TextTag(context, "yearRange", field.getYearRange(), 8)));

							optional();
						}

						break;

					case Selection:

						if (field.isInput()) {

							optional();

							sb.append(new AdminRow("Selection type", "Controls how the user can select options for this search field.",
									new SelectTag(context, "selectionStyle", field.getSelectionMethod(), SelectionMethod.values())));

							autoSubmit();

							if (field.getSelectionMethod() == SelectionMethod.Check) {

								sb.append(new AdminRow("Cols", "Set how many columns you want the options laid out in.", new TextTag(context,
										"cols", field.getCols(), 4)));

							}

						}

						break;

					case Text:

						if (field.isInput()) {
							dimensions();
						}

						break;

					case Numerical:

						if (field.isInput()) {
							ranges();
						}

						break;
					}
				}
			}

			private void autoSubmit() {

				sb.append(new AdminRow("Auto submit", "Automatically submit the form on a change.", new BooleanRadioTag(context, "autoSubmit", field
						.isAutoSubmit())));
			}

            private void padding() {

				sb.append(new AdminRow("Display padding", "Display categories as a structure.", new BooleanRadioTag(context, "padding", field
						.isPadding())));
			}

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update field"));
				sb.append(new ButtonTag(SearchFormFieldHandler.class, "delete", "Delete field", "field", field)
						.setConfirmation("If you delete this field it cannot be un-done. Continue?"));
				sb.append(new ButtonTag(SearchFormHandler.class, null, "Return to search form", "form", field.getSearchForm()));
				sb.append("</div>");
			}

			private void dimensions() {

				sb.append(new AdminRow("Size", "Customize the size of the input box.", "Width " + new TextTag(context, "width", field.getWidth(), 4)
						+ " Height " + new TextTag(context, "height", field.getHeight(), 4)));
			}

			private void fixedBands() {

				sb.append(new AdminRow("Fixed bands", "Show each band as a single fixed choice.", new BooleanRadioTag(context, "fixedBands", field
						.isFixedBands())));
			}

			private void general() {

				sb.append(new AdminTable("Field details"));

				sb.append(new AdminRow("Name", "Enter the name of this search field.", new TextTag(context, "name", field.getName(), 40)));

				SelectTag typeTag = new SelectTag(context, "type", field.getFieldType());
				typeTag.addOptions(FieldType.values());

				sb.append(new AdminRow("Type", "Type of the field", typeTag));

				sb.append(new AdminRow("Hidden", "Set the field to hidden to make this a preset value in the search.", new BooleanRadioTag(context,
						"hidden", field.isHidden())));

				switch (field.getFieldType()) {

				case ImagesOnly:
				case Sort:
					break;

				case Location:
					optional();
					dimensions();
					break;

				case Reference:
					optional();
					dimensions();
					break;

				case Id:
				case Towns:
				case MajorTowns:
				case LastActive:
					optional();
					break;

				case Duration:
					duration();
					break;

				case BookingStart:
					break;

				case Categories:

					optional();
					if (field.isInput()) {
						autoSubmit();
					}
                    padding();

					break;

				case Keywords:
				case Name:
				case Details:

					if (field.isInput()) {
						dimensions();
					}

					break;

				case Attribute:

					attribute();
					break;

				case Distance:

					if (field.isInput()) {
						sb.append(new AdminRow("Distances", "The options for this distance field. <i>Enter each distance on a new line.</i>",
								new TextAreaTag(context, "distances", StringHelper.implode(field.getDistances(), "\n", true), 40, 6)));

					}

					optional();

					break;

				case Age:
				case Price:

					if (field.isInput()) {
						ranges();
					}

					break;

                case GroupedKeywords:
                    groupedKeywords();
                    optional();
                    autoSubmit();
                    break;
                }

				if (field.isHidden()) {

					sb.append(new AdminRow("Value", "Enter the value to use for this field when searching.", new TextTag(context, "value", field
							.getValue(), 20)));

				}

				if (field.isOptional()) {

					sb.append(new AdminRow("Any", "Enter a value to display for selecting 'all values', eg, 'All manufacturers'", new TextTag(
							context, "any", field.getAny(), 20)));

				}

				sb.append("</table>");
			}

			private void duration() {

				sb.append(new AdminRow("Durations",
						"Enter the different duration options in days. These will intelligently display on the search form as 2 weeks, etc."
								+ "<i>Separate each day with a space</i>", new TextTag(context, "durations", StringHelper.implode(field
								.getDurations(), " ", true), 60)));
			}

			private void optional() {
				sb.append(new AdminRow("Optional", "Show an extra 'any' choice for displaying all values.", new BooleanRadioTag(context, "optional",
						field.isOptional())));
			}

			private void ranges() {

				searchBands();
				optional();
				fixedBands();
				autoSubmit();
			}

			private void searchBands() {
				sb.append(new AdminRow("Search bands",
						"The different bands for the search. If you leave this blank, then the search will be text boxes. "
								+ "<i>Enter each value on a new line</i>", new TextAreaTag(context, "ranges", StringHelper.implode(field
								.getRanges(), "\n", true), 40, 5)));
			}

            private void groupedKeywords() {
                for (SearchGroup group : field.getSearchGroup()) {
                    sb.append(new AdminRow("Search Group", new LinkTag(SearchGroupHandler.class, null, new ImageTag("files/graphics/admin/spanner.gif"), "group", group) + group.getName()));
                }
                sb.append(new AdminRow("", new ButtonTag(SearchFormFieldHandler.class, "addGroup", "Add Group", "field", field)));
            }

            @Override
			public String toString() {

				sb.append(new FormTag(SearchFormFieldHandler.class, "save", "post"));
				sb.append(new HiddenTag("field", field));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object moveDown() throws ServletException {

		if (field == null)
			return main();

		form = field.getSearchForm();
		EntityUtil.moveDown(field, field.getSearchForm().getSearchFields());

		return new SearchFormHandler(context, field.getSearchForm()).main();
	}

	public Object moveUp() throws ServletException {

		if (field == null)
			return main();

		EntityUtil.moveUp(field, field.getSearchForm().getSearchFields());
		return new SearchFormHandler(context, field.getSearchForm()).main();
	}

	public Object save() throws ServletException {

		if (field == null) {
			return index();
		}

		field.setType(type);
		field.setCols(cols);
		field.setName(name);
		field.setAttribute(attribute);
		field.setOptional(optional);
		field.setAny(any);
		field.setSelectionMethod(selectionStyle);

		field.setFixedBands(fixedBands);
		field.setWidth(width);
		field.setHeight(height);

		field.setAutoSubmit(autoSubmit);
		field.setDistances(StringHelper.explodeIntegers(distances, "\n"));
		field.setRanges(StringHelper.explodeStrings(ranges, "\n"));

		field.setHidden(hidden);
		field.setValue(value);

		// date
		field.setDateSearchType(dateSearchType);
		field.setDateMethod(dateMethod);

		field.setYearEnd(endYear);
		field.setYearStart(startYear);
		field.setYearRange(yearRange);

		// durations
		field.setDurations(StringHelper.explodeIntegers(durations, "\\D"));

        //category
        field.setPadding(padding);

		field.save();

		addMessage("This field has been updated with your changes");
		clearParameters();
		return main();
	}

}
