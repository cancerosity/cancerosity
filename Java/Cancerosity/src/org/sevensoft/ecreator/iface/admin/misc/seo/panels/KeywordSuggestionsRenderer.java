package org.sevensoft.ecreator.iface.admin.misc.seo.panels;

import java.util.Collection;

import org.sevensoft.ecreator.iface.admin.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.admin.categories.CategoryKeywordsHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;

/**
 * @author sks 17-Nov-2005 18:20:36
 * 
 */
public class KeywordSuggestionsRenderer {

	private final RequestContext		context;
	private final Collection<String>	keywords;
	private final StringBuilder		sb;
	private final Category			category;

	public KeywordSuggestionsRenderer(RequestContext context, Collection<String> k, Category category) {
		this.context = context;
		this.keywords = k;
		//		Collections.reverse(keywords);
		this.category = category;
		sb = new StringBuilder();
	}

	@Override
	public String toString() {

		sb.append(new FormTag(CategoryKeywordsHandler.class, "save", "GET"));
		sb.append(new HiddenTag("category", category));

		sb.append("<table cellspacing='0' cellpadding='0' class='form'>");
		sb.append("<caption>Keyword suggestions</caption>");

		sb.append("<tr>");
		sb.append("</tr>");

		int n = 0;
		for (String keyword : keywords) {

			if (n % 3 == 0)
				sb.append("<tr>");

			sb.append("<td width='40'>" + (n + 1) + "</td><td width='40'>" + new CheckTag(context, "suggestion", keyword, false) + "</td><td>" + keyword
					+ "</td>");
			n++;

			if (n % 3 == 0)
				sb.append("</tr>");
		}

		if (n % 3 == 1)
			sb.append("<td colspan='6'></td></tr>");
		else if (n % 3 == 2)
			sb.append("<td colspan='3'></tr>");

		sb.append("<tr>");
		sb.append("<td colspan='9' align='center'>");
		sb.append(new SubmitTag("Set keywords"));
		sb.append(new ButtonTag(CategoryHandler.class, null, "Return to category"));
		sb.append("</td>");
		sb.append("</tr>");

		sb.append("</form>");

		return sb.toString();
	}

}
