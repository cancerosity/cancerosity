package org.sevensoft.ecreator.iface.admin.accounts.subscriptions;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemTypeMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 28 May 2006 18:47:29
 *
 */
@Path("admin-subscriptions-module.do")
public class SubscriptionModuleHandler extends AdminHandler {

	private ItemType	itemType;
	private boolean	forceSubscription;
	private String	completionContent;
	private String	ratesHeader;
	private String	ratesFooter;

	public SubscriptionModuleHandler(RequestContext context) {
		super(context);
	}

	public SubscriptionModuleHandler(RequestContext context, ItemType itemType) {
		super(context);
		this.itemType = itemType;
	}

	@Override
	public Object main() {

		if (itemType == null) {
			return main();
		}

		final SubscriptionModule module = itemType.getSubscriptionModule();

		AdminDoc doc = new AdminDoc(context, user, "Edit subscription module", null);
		doc.setMenu(new EditItemTypeMenu(itemType));
		doc.addBody(new Body() {

			private void commands() {
				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update listing module"));
				sb.append(new ButtonTag(ItemTypeHandler.class, null, "Return to item types menu"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("Options"));
				sb.append(new AdminRow("Force subscription", "Force customers to signup for a subscription immediately.", new BooleanRadioTag(context,
						"forceSubscription", module.isForceSubscription())));

				sb.append("<tr><th colspan='2'>Subscription completed content</th></tr>");
				sb.append("<tr><td colspan='2'>" + new TextAreaTag(context, "completionContent", module.getCompletionContent(), 70, 8) + "</td></tr>");

				sb.append("<tr><td colspan='2'>Rates page header</td></tr>");
				sb.append("<tr><td colspan='2'>"
						+ new TextAreaTag(context, "ratesHeader", module.getRatesHeader()).setId("ratesHeader").setStyle(
								"width: 100%; height: 320px;") + "</td></tr>");

				sb.append("<tr><td colspan='2'>Rates page footer</td></tr>");
				sb.append("<tr><td colspan='2'>"
						+ new TextAreaTag(context, "ratesFooter", module.getRatesFooter()).setId("ratesFooter").setStyle(
								"width: 100%; height: 320px;") + "</td></tr>");

				sb.append("</table>");
			}

			private void levels() {

				sb.append(new AdminTable("Subscription levels"));

				sb.append("<tr>");
				sb.append("<th width='60'>Position</th>");
				sb.append("<th>Account type</th>");
				sb.append("<th>Name</th>");
				sb.append("<th>Delete</th>");
				sb.append("</tr>");

				PositionRender pr = new PositionRender(SubscriptionModuleHandler.class, "subscription");
				for (SubscriptionLevel subscriptionLevel : module.getSubscriptionLevels()) {

					sb.append("<tr>");
					sb.append("<td width='60'>" + pr.render(subscriptionLevel) + "</td>");
					sb.append("<td>" + subscriptionLevel.getItemType().getName() + "</td>");
					sb.append("<td>"
							+ new LinkTag(SubscriptionLevelsHandler.class, "edit", new ImageTag("files/graphics/admin/spanner.gif"),
									"subscriptionLevel", subscriptionLevel) + " " + subscriptionLevel.getName() + "</td>");
					sb.append("<td>"
							+ new ButtonTag(SubscriptionLevelsHandler.class, "delete", "Delete", "subscriptionLevel", subscriptionLevel)
									.setConfirmation("Are you sure you want to delete this subscription level?") + "</td>");
					sb.append("</tr>");
				}

				sb.append("<tr><td colspan='4'>Add new subscription level "
						+ new ButtonTag(SubscriptionLevelsHandler.class, "create", "Add", "itemType", itemType) + "</td></tr>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				if (miscSettings.isHtmlEditor()) {
					RendererUtil.tinyMce(context, sb, "ratesFooter", "ratesHeader");
				}

				sb.append(new FormTag(SubscriptionModuleHandler.class, "save", "post"));
				sb.append(new HiddenTag("itemType", itemType));

                commands();
				general();
				levels();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;

	}

	public Object save() throws ServletException {

		if (itemType == null) {
			return main();
		}

		final SubscriptionModule module = itemType.getSubscriptionModule();

		module.setForceSubscription(forceSubscription);
		module.setCompletionContent(completionContent);

		module.setRatesFooter(ratesFooter);
		module.setRatesHeader(ratesHeader);
		module.save();

		addMessage("This subscription module has been updated with your changes.");
		clearParameters();
		return main();
	}
}
