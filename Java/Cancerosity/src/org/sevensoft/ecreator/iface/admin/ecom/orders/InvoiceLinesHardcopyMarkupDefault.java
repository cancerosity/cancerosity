package org.sevensoft.ecreator.iface.admin.ecom.orders;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * User: Tanya
 * Date: 25.02.2011
 */
public class InvoiceLinesHardcopyMarkupDefault implements MarkupDefault {

    public String getBetween() {
        return null;
    }

    public String getBody() {
        StringBuilder sb = new StringBuilder();

        sb.append("<tr>");
        sb.append("<td width='80'>[order_line_qty]</td>\n");
        sb.append("<td>[order_line_id]</td>\n");

        /*
        * Attribute columns
        */
        sb.append("[invoice_lines_attributes]\n");

        sb.append("<td>[order_line_desc]");
        sb.append("[order_line_options?prefix=<br>]");
        sb.append("</td>\n");
        sb.append("<td align='right'>[order_line_sellunit]</td>\n");
        sb.append("<td align='right'>[order_line_sellline]</td>\n");
        sb.append("</tr>\n");

        return sb.toString();
    }

    public String getEnd() {
        StringBuilder sb = new StringBuilder();
        sb.append("[invoice_delivery_lines]\n");
        sb.append("</table>");

        return sb.toString();
    }

    public String getStart() {
        StringBuilder sb = new StringBuilder();
        sb.append(new TableTag("items"));

        sb.append("<tr>");
        sb.append("<th>Qty</th>");
        sb.append("<th>Code</th>\n");

        sb.append("[invoice_attributes_names]\n");

        sb.append("<th>Description</th>");
        sb.append("<th align='right'>Unit</th>");
        sb.append("<th align='right'>Line</th>");
        sb.append("</tr>\n");
        return sb.toString();
    }

    public int getTds() {
        return 0;
    }

    public String getCss() {
        return null;
    }

    public String getName() {
        return "Invoice Line Markup Default";
    }
}
