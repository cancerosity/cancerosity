package org.sevensoft.ecreator.iface.admin.crm.forms;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.crm.forms.googleform.GoogleFormFieldHandler;
import org.sevensoft.ecreator.iface.admin.categories.menu.CategoryMenu;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasHandler;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.EasyDoc;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.accounts.permissions.DomainUtil;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionCollections;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.accounts.permissions.panels.PermissionsPanel;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.crm.forms.FieldType;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.FormField;
import org.sevensoft.ecreator.model.crm.forms.FormPreset;
import org.sevensoft.ecreator.model.crm.forms.NewsletterSignup;
import org.sevensoft.ecreator.model.crm.forms.googleform.GoogleFormField;
import org.sevensoft.ecreator.model.crm.forms.googleform.GoogleForm;
import org.sevensoft.ecreator.model.crm.forms.blocks.FormBlock;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Grid;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 07-Jul-2005 12:39:17
 * 
 */
@Path("admin-forms.do")
public class FormHandler extends AdminHandler {

	private Form						form;
	private String						submissionMessage, name, submissionEmailMessage;
	private String						submitButtonText;
	private boolean						sidebar;
	private FormField						emailField;
	private FormPreset					preset;
	private FormField						field;
	private FieldType						fieldType;
	private MultiValueMap<String, PermissionType>	permissions;
	private boolean						simpleEditor;
	private boolean						customMarkup;
	private boolean						ccItemMember;
	private boolean						ccItemListers;
    private String itemListersMailHeader, itemListersMailFooter;
	private Markup						markup;
	private boolean						accountRequired;
	private String						restrictedContent;
	private String						header;
	private String						footer;
	private String						submissionForward;
	private boolean						checkout;
	private boolean						enablePermissions;
	private String						restrictionForwardUrl;
	private String						smsNumbers;
	private String						emails;
	private boolean						captcha;
	private NewsletterSignup				newsletterSignup;
	private NewsletterSignup				addNewsletterSignup;
	private Newsletter					newsletter;
    private String	                    script;
    private String                      cssId;
    private String                      cssClass;
    private GoogleForm googleForm;
    private FormField googleFieldType;
    private String formkey;
    private int pageColumn;
    private int siteColumn;
    private boolean includeSubmId;
    private String trIdFirst;
    private String trIdLast;

    public FormHandler(RequestContext context) {
        super(context);
    }

    public FormHandler(RequestContext context, Form form) {
        this(context);
        this.form = form;
        this.googleForm = GoogleForm.get(context, form);
    }

	public Object copy() throws ServletException {

		if (form == null) {
			return main();
		}

		try {
			form.copy();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return main();
	}

	public Object create() {

		AdminDoc doc = new AdminDoc(context, user, "Create a form", Tab.Extras);
		doc.setIntro("Create a new form by giving it a name");
		if (miscSettings.isAdvancedMode()) {
			doc.setMenu(new ExtrasMenu());
		}
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(FormHandler.class, "doCreate", "POST"));

				sb.append(new TableTag("simplebox", "center"));

				sb.append("<tr><td align='center'>Enter the name of the form to create.</td></tr>");
				sb.append("<tr><td align='center'>" + new TextTag(context, "name", 40) + "<br/><br/></td></tr>");

				sb.append("<tr><td align='center'>Would you like to use one of our templates to help you on your way?</td></tr>");

				SelectTag presetTag = new SelectTag(context, "preset");
				presetTag.setAny("No thanks, I want to start with a blank form");
				for (FormPreset preset : FormPreset.values()) {
					presetTag.addOption(preset, "Yes, please start me off with a " + preset.toString());
				}

				sb.append("<tr><td align='center'>" + presetTag + "<br/><br/></td></tr>");
				sb.append("</table>");

				sb.append("<div align='center' class='actions'>" + new SubmitTag("Create form") + "</div>");

				sb.append("</form>");
				return sb.toString();
			}

		});
		return doc;

	}

	public Object copyToAllCats() throws ServletException {

		if (form == null) {
			return main();
		}

		List<Category> categories = org.sevensoft.ecreator.model.categories.Category.get(context);
		for (Category category : categories) {

			boolean hasForm = false;

			for (Block block : category.getBlocks()) {

				if (block instanceof FormBlock) {

					FormBlock fb = (FormBlock) block;
					if (fb.getForm().equals(form)) {
						hasForm = true;
						break;
					}
				}
			}

			if (false == hasForm) {
				new FormBlock(context, category, form);
			}

		}

		return new ActionDoc(context, "Form copied to all cats", new Link(FormHandler.class, "edit", "form", form));

	}

	public Object delete() throws ServletException {

		if (form == null) {
			return main();
		}

		form.log("Deleted");
		form.delete();

		if (miscSettings.isAdvancedMode()) {
			return new ExtrasHandler(context).main();
		}

		AdminDoc doc = new AdminDoc(context, user, "The form has been deleted", Tab.Extras);
		doc.setMenu(new CategoryMenu());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("simplelinks", "center"));
				sb.append("<tr><th align='center'>What would you like to do ?</th></tr>");

				sb.append("<tr><td align='center'>" + new LinkTag(FormHandler.class, "create", "I want to create a new form") + "</td></tr>");

				sb.append("<tr><td align='center'>" + new LinkTag(ExtrasHandler.class, null, "I want to return to the extras menu") + "</td></tr>");

				sb.append("</table>");

				return sb.toString();
			}
		});
		return doc;
	}

	public Object doCreate() throws ServletException {

		if (name == null) {
			return create();
		}

		if (preset != null) {
			form = preset.create(context, name);
		} else {
			form = new Form(context, name);
		}

		form.log("Created");

		AdminDoc doc = new AdminDoc(context, user, "This form has been created", Tab.Extras);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("simplelinks", "center"));

				sb.append("<tr><th>What do you want to do now?</th></tr>");
				sb.append("<tr><td align='center'>" + new LinkTag(FormHandler.class, "edit", "I want to edit this form", "form", form) + "</td></tr>");
				sb.append("<tr><td align='center'>" + new LinkTag(ExtrasHandler.class, null, "I want to return to the menu") + "</td></tr>");

				sb.append("</table>");

				return sb.toString();
			}
		});
		return doc;
	}

	public Object edit() throws ServletException {

		if (form == null)
			return main();

		AdminDoc doc = new AdminDoc(context, user, "Edit form #" + form.getId(), Tab.Extras);
		if (miscSettings.isAdvancedMode()) {
			doc.setMenu(new ExtrasMenu());
		}
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update form"));
				sb.append(new ButtonTag(FormHandler.class, "copy", "Copy form", "form", form));
				sb.append(new ButtonTag(FormHandler.class, "delete", "Delete form", "form", form)
						.setConfirmation("Are you sure you want to delete this form?"));
				sb.append(new ButtonTag(FormHandler.class, null, "Return to menu"));
				sb.append("</div>");
			}

            private void css() {

                sb.append(new AdminTable("Css"));

                sb.append(new AdminRow(true, "Css identifiers", "Set the id and classname attribute for this box for use by your css code.", "Id: " +
                        new TextTag(context, "cssId", form.getCssId()) + " Classname: " + new TextTag(context, "cssClass",
                        form.getCssClass())));

                sb.append("</table>");
            }

            private void fields() {

				sb.append(new AdminTable("Fields"));

				sb.append("<tr>");
				sb.append("<th>Position</th>");
				sb.append("<th>Type</th>");
				sb.append("<th>Name</th>");
				sb.append("<th width='10'> </th>");
				sb.append("</tr>");

				List<FormField> fields = form.getFields();
				List<String> sections = Grid.getSections(fields);

				PositionRender pr = new PositionRender(FormFieldHandler.class, "field");

				for (String section : sections) {

					sb.append("<tr><th colspan='4'>");
					if (section == null) {
						sb.append("Default section");
					} else {
						sb.append(section);
					}
					sb.append("</th></tr>");

					for (FormField field : Grid.getCells(fields, section)) {

						sb.append("<tr>");

						sb.append("<td width='120'>" + pr.render(field) + "</td>");

						sb.append("<td>" + new LinkTag(FormFieldHandler.class, null, new SpannerGif(), "field", field) + " " +
								field.getType().toString() + "</td>");

						sb.append("<td>" + field.getName() + " (#" + field.getId() + ")</td>");

						sb.append("<td width='10'>" +
								new LinkTag(FormFieldHandler.class, "delete", new DeleteGif(), "field", field)
										.setConfirmation("If you delete this field it cannot be restored. Continue?") + "</td>");
					}
				}

				SelectTag tag = new SelectTag(null, "fieldType");
				tag.setAny("-Choose field type-");
				tag.addOptions(FieldType.values());

				sb.append("<tr><td colspan='4'>Add new field: " + tag + "</td></tr>");

				sb.append("</table>");

			}

            private void googleform() {
                sb.append(new AdminTable("Google Form"));
                sb.append("<tr><td><b>Form key</b></td>");
                sb.append("<td colspan='3'>");
                sb.append(new TextTag(context, "formkey", googleForm.getFormkey(), 50));
                sb.append("</td></tr>");
                 sb.append("<tr><td><b>Page column</b></td>");
                sb.append("<td colspan='3'>");
                sb.append(new TextTag(context, "pageColumn", googleForm.getPageColumn(), 10));
                sb.append("</td></tr>");
                 sb.append("<tr><td><b>Site column</b></td>");
                sb.append("<td colspan='3'>");
                sb.append(new TextTag(context, "siteColumn", googleForm.getSiteColumn(), 10));
                sb.append("</td></tr>");

                sb.append("<tr>");
                sb.append("<th width='30'> </th>");
                sb.append("<th>Position</th>");
                sb.append("<th>Name</th>");
                sb.append("<th width='10'> </th>");
                sb.append("</tr>");


				List<GoogleFormField> fields = googleForm.getFields();

					for (GoogleFormField field : fields) {
						sb.append("<tr>");
                        sb.append("<td width='30' align='right'>" +
								new LinkTag(GoogleFormFieldHandler.class, null, new SpannerGif(), "field", field) + "</td>");

						sb.append("<td width='120'>" + field.getGoogleFormColumn() + "</td>");

						sb.append("<td>" + field.getFormField().getName() + " (#" + field.getId() + ")</td>");

						sb.append("<td width='10'>" +
								new LinkTag(GoogleFormFieldHandler.class, "delete", new DeleteGif(), "field", field)
										.setConfirmation("If you delete this field it cannot be restored. Continue?") + "</td>");
					}

				SelectTag tag = new SelectTag(null, "googleFieldType");
				tag.setAny("-Choose field type-");
				tag.addOptions(form.getFields());

				sb.append("<tr><td colspan='4'>Add new field: " + tag + "</td></tr>");


				sb.append("</table>");

			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Name", "Enter a name for this form", new TextTag(context, "name", form.getName(), 40)));

				sb.append(new AdminRow("Recipient emails",
						"If you enter email addresses in here, then they will receive a copy to the form when it is completed. "
								+ "<i>Put each email on a separate line.</i>", new TextAreaTag(context, "emails", StringHelper.implode(form
								.getEmails(), "\n"), 60, 3)));

                sb.append(new AdminRow("Include submission ID on email", "", new BooleanRadioTag(context, "includeSubmId", form.isIncludeSubmId())));

				if (Module.Sms.enabled(context)) {
					sb
							.append(new AdminRow(
									"Recipient mobiles",
									"If you enter mobile numbers in here then this form will be sent out as an SMS message.<br/>"
											+ "<b>Please note:</b> SMS messages are limited to 160 characters, so if you have a large number of fields, some data might be truncated.</i>",
									new TextAreaTag(context, "smsNumbers", StringHelper.implode(form.getSmsNumbers(), "\n"), 40, 3)));
				}

				if (miscSettings.isAdvancedMode()) {

					if (Module.Accounts.enabled(context)) {

						sb.append(new AdminRow("CC listers", "CC the submission to the account who listed this item, if it is a listing.",
								new BooleanRadioTag(context, "ccItemListers", form.isCcItemListers())));

                        sb.append(new AdminRow("CC listers mail header", "Enter the message that will be attached to email for listing owner.",
						new TextAreaTag(context, "itemListersMailHeader", form.getItemListersMailHeader(), 60, 6)));

                        sb.append(new AdminRow("CC listers mail footer", "Enter the message that will be attached to email for listing owner.",
						new TextAreaTag(context, "itemListersMailFooter", form.getItemListersMailFooter(), 60, 6)));
					}
				}

				sb.append(new AdminRow("Submission message", "Enter the message that will be displayed to the user when they complete the form.",
						new TextAreaTag(context, "submissionMessage", form.getSubmissionMessage(), 60, 6)));

                sb.append(new AdminRow("Submission mail message", "Enter the message that will be sent to the user when they complete the form.",
						new TextAreaTag(context, "submissionEmailMessage", form.getSubmissionEmailMessage(), 60, 6)));

				if (miscSettings.isAdvancedMode()) {

					sb.append(new AdminRow("Submission forward", "Forward users to this page when a form is completed.", new TextTag(context,
							"submissionForward", form.getSubmissionForward(), 40)));

					sb.append(new AdminRow("Submit button text", "The text displayed on the button to submit the form.", new TextTag(context,
							"submitButtonText", form.getSubmitButtonText(), 20)));

					if (Module.Shopping.enabled(context)) {
						sb.append(new AdminRow("Checkout", "Use this form at checkout and attach to the order.", new BooleanRadioTag(context,
								"checkout", form.isCheckout())));
					}

					sb.append(new AdminRow("Form quantities", "Show quantity field when adding items to this form.", new BooleanRadioTag(context,
							"lineQuantities", form.isLineQuantities())));

				}

				if (isSuperman()) {
					sb.append(new AdminRow("Copy all cats", "Copy ths form to all cats", new ButtonTag(FormHandler.class, "copyToAllCats",
							"Copy all cats", "form", form)));
				}

				if (Module.Captcha.enabled(context)) {

					sb.append(new AdminRow("Captcha", "Enable anti spam Captcha.", new BooleanRadioTag(context, "captcha", form.isCaptcha())));

				}

				SelectTag emailResponseFieldTag = new SelectTag(context, "emailField", form.getEmailField());
				emailResponseFieldTag.setAny("-Do not send email to user-");
				emailResponseFieldTag.addOptions(form.getFields());

				sb.append(new AdminRow("Email field",
						"Send an email to the user when the form is submitted using the value of this field as the email address.",
						emailResponseFieldTag));

				if (Module.UserMarkup.enabled(context) || isSuperman()) {

					SelectTag markupTag = new SelectTag(context, "markup", form.getMarkup());
					markupTag.setAny("Default renderer");
					markupTag.addOptions(Markup.get(context));

					sb.append(new AdminRow(!Module.UserMarkup.enabled(context), "Markup", "Use custom markup instead of standard renderer.",
							markupTag));

				}

                sb.append(new AdminRow(true, "First &lt;tr&gt; id", "Set the value of first &lt;tr&gt; ID.", new TextTag(context, "trIdFirst",
                        form.getTrIdFirst(), 30)));

                sb.append(new AdminRow(true, "Last &lt;tr&gt; id", "Set the value of last &lt;tr&gt; ID.", new TextTag(context, "trIdLast",
                        form.getTrIdLast(), 30)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

                sb.append(new FormTag(FormHandler.class, "save", "POST"));
				sb.append(new HiddenTag("form", form));
                if (Module.GoogleForm.enabled(context)) {
                    googleForm = GoogleForm.get(context, form);
                    sb.append(new HiddenTag("googleForm", googleForm));
                }

                commands();
                if (miscSettings.isAdvancedMode()) {
                    script();
                }
                general();
				fields();

                if (Module.GoogleForm.enabled(context)){
                    googleform();
                }

				if (Module.Permissions.enabled(context)) {
					sb.append(new PermissionsPanel(context, form, PermissionCollections.getForm()));
				}

				if (Module.Newsletter.enabled(context)) {
					newsletters();
				}

                css();
                commands();

				sb.append("</form>");
				return sb.toString();
			}

			private void newsletters() {

				sb.append(new AdminTable("Newsletters"));

				for (NewsletterSignup signup : form.getNewsletterSignUps()) {

					sb.append("<tr>");
					sb.append("<td>" + signup.getNewsletter().getName() + "</td>");
					sb.append("<td width='10'>" +
							new LinkTag(FormHandler.class, "removeNewsletterSignup", new DeleteGif(), "newsletterSignup", signup, "form", form) + "</td>");
					sb.append("<tr>");
				}

				sb.append("<tr><td colspan='2'>Add newsletter signup " +
						new SelectTag(context, "newsletter", null, Newsletter.get(context), "-Select newsletter-") + " </td></tr>");

				sb.append("</table>");
			}

            private void script() {

                sb.append(new AdminTable("Script"));
                sb.append(new AdminRow("Submission script", "This script will be added to the form submission page shown to the user. "
                        + "You will need to include the &lt;script&gt; tags if you are adding javascript code.", new TextAreaTag(context, "script", form
                        .getSubmissionScript(), 60, 5)));
                sb.append("</table>");
            }

        });
		return doc;
	}

	public Object removeNewsletterSignup() throws ServletException {

		if (newsletterSignup == null) {
			return main();
		}

		newsletterSignup.delete();
		return edit();
	}

	public Object list() {

		AdminDoc doc = new AdminDoc(context, user, "Edit form", Tab.Extras);
		if (miscSettings.isAdvancedMode()) {
			doc.setMenu(new ExtrasMenu());
		}
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(FormHandler.class, "edit", "POST"));

				sb.append(new TableTag("simplebox", "center"));

				sb.append("<tr><td align='center'>Select the form you want to edit.</td></tr>");
				sb.append("<tr><td align='center'>" + new SelectTag(context, "form", null, Form.get(context), "-Choose form-") + "</td></tr>");

				sb.append("<tr><td align='center'>" + new SubmitTag("Continue >") + "<br/><br/></td></tr>");
				sb.append("</table>");

				sb.append("</form>");
				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {
		return list();
	}

	public Object menu() throws ServletException {

		EasyDoc doc = new EasyDoc(context, user, "Forms", Tab.Forms);
		doc.setIntro("What would you like to do?");
		doc.setForm(new FormTag(FormHandler.class, "edit", "POST"));
		doc.addLink("I want to create a new form", new Link(FormHandler.class, "create"));

		List<Form> forms = Form.get(context);
		if (forms.size() > 0) {
			doc.addText("I want to edit an existing form");
			doc.addControls(new SelectTag(context, "form", null, forms, "-Choose form-"));
			doc.addControls(new SubmitTag("Continue >"));
		}

		return doc;
	}

	public Object removeField() throws ServletException {

		if (form == null || field == null) {
			return edit();
		}

		form.removeField(field);
		form.log("Field removed " + field.getType());

		return edit();
	}

	public Object save() throws ServletException {

		if (form == null) {
			return main();
		}

		test(new RequiredValidator(), "name");
		if (hasErrors()) {
			return edit();
		}

		if (!ObjectUtil.equal(form.getName(), name)) {
			form.setName(name);
			form.log("Name changed - " + name);
		}

		if (!ObjectUtil.equal(form.getSubmissionMessage(), submissionMessage)) {
			form.setSubmissionMessage(submissionMessage);
			form.log("Submission message changed - " + submissionMessage);
		}

        if (!ObjectUtil.equal(form.getSubmissionEmailMessage(), submissionEmailMessage)) {
			form.setSubmissionEmailMessage(submissionEmailMessage);
			form.log("Submission email message changed - " + submissionEmailMessage);
		}

		form.setEmails(StringHelper.explodeStrings(emails, "\n"));
		form.setSmsNumbers(StringHelper.explodeStrings(smsNumbers, "\n"));

        form.setIncludeSubmId(includeSubmId);

        form.setTrIdFirst(trIdFirst);
        form.setTrIdLast(trIdLast);

        for (FormField field : form.getFields()) {
            if (field.getType().toString().toLowerCase().equals("email")) {
                form.setEmailField(field);
            }
        }

        form.setSimpleEditor(simpleEditor);
		form.setCheckout(checkout);

		// PERMISSIONS
		if (form.isPermissions() != enablePermissions) {
			form.setPermissions(enablePermissions);
			if (enablePermissions) {
				form.log("Permissions enabled");
			} else {
				form.log("Permissions disabled");
			}
		}
		form.setRestrictionForwardUrl(restrictionForwardUrl);
		DomainUtil.savePermissions(context, form, permissions, PermissionCollections.getForm());

		if (Module.UserMarkup.enabled(context) || isSuperman()) {
			if (!ObjectUtil.equal(form.getMarkup(), markup)) {
				form.setMarkup(markup);
				form.log("Markup changed - " + markup);
			}
		}

		if (Module.Captcha.enabled(context)) {
			if (form.isCaptcha() != captcha) {
				form.setCaptcha(captcha);
				if (captcha) {
					form.log("Captcha enabled");
				} else {
					form.log("Captcha disabled");
				}
			}
		}

		if (miscSettings.isAdvancedMode()) {

			form.setCcItemListers(ccItemListers);
            form.setItemListersMailFooter(itemListersMailFooter);
            form.setItemListersMailHeader(itemListersMailHeader);
			form.setSubmitButtonText(submitButtonText);
			form.setSubmissionForward(submissionForward);
            form.setSubmissionScript(script);
        }

        form.setCssClass(cssClass);
        form.setCssId(cssId);

        form.save();

		if (fieldType != null) {
			form.addField("-new field-", fieldType);
		}

        if (Module.GoogleForm.enabled(context)) {
            googleForm.setFormkey(formkey);
            googleForm.setPageColumn(pageColumn);
            googleForm.setSiteColumn(siteColumn);
            
            if (googleFieldType != null) {
                googleForm.addField(googleFieldType);
            }

            googleForm.save();
        }

		if (newsletter != null) {
			form.addNewsletterSignUp(newsletter);
		}

        clearParameters();
		return new ActionDoc(context, "This form has been updated", new Link(FormHandler.class, "edit", "form", form));
	}
}
