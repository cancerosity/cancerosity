package org.sevensoft.ecreator.iface.admin.reports.submissions;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.crm.forms.SubmissionHandler;
import org.sevensoft.ecreator.iface.admin.reports.StatsMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.FlagGif;
import org.sevensoft.ecreator.model.stats.submissions.PageSubmissionsCounter;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.crm.forms.submission.Submission;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 03-Oct-2005 16:12:08
 * 
 */
@Path("admin-reports-submissions-pages.do")
public class PageSubmissionsHandler extends AdminHandler {

	private Date	month;
    private Category category;

    public PageSubmissionsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {
		return null;
	}

	/**
	 * Shows the pages with the most submissions for a month
	 */
	public Object monthly() throws ServletException {

		if (month == null)
			month = new Date();

		month = month.beginMonth();

		int limit = 100;

		final List<PageSubmissionsCounter> counters = PageSubmissionsCounter.get(context, month, limit);

		final String title = "Submissions by pages for " + month.toString("MMMM-yy");
		AdminDoc doc = new AdminDoc(context, user, title, Tab.Stats);
		doc.setMenu(new StatsMenu());
		doc.setIntro("This page shows the pages with the most submissions for any form for the month " + month.toString("MMMM-yy"));
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable(title));

				sb.append(new FormTag(PageSubmissionsHandler.class, "monthly", "get"));
				SelectTag monthTag = new SelectTag(context, "month");
				monthTag.setAutoSubmit();

				Date month = new Date().beginMonth();
				for (int n = 0; n < 12; n++) {
					monthTag.addOption(month, month.toString("MMMM-yy"));
					month = month.removeMonths(1);
				}

				sb.append("<tr><th align='right' colspan='4'>Choose month ");
				sb.append(monthTag);
				sb.append("</th></tr>");

				sb.append("</form>");

				sb.append("<tr>");
				sb.append("<td>Page</td>");
				sb.append("<td>Total submissions</td>");
				sb.append("</tr>");

				for (PageSubmissionsCounter counter : counters) {

					sb.append("<tr>");
					sb.append("<td>" + new LinkTag(counter.getPage().getUrl(), counter.getPage().getName()) + "</td>");
					sb.append("<td>" + counter.getTotal() + "</td>");
					sb.append("</tr>");

				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

    /**
     * Shows the pages with the most submissions for a month
     */
    public Object byCategory() throws ServletException {
        final List<Category> categories = Category.getOrderedByName(context);//Submission.getCategories(context);
        if (category == null && categories != null && categories.get(0) != null) {
            category = categories.get(0);
        }

        int limit = 100;

        final List<Submission> counters = Submission.getByCategory(context, category, limit);

        final String title = "Submissions by pages for " + category.getName();
        AdminDoc doc = new AdminDoc(context, user, title, Tab.Stats);
        doc.setMenu(new StatsMenu());
        doc.setIntro("This page shows the pages with the most submissions for any form for the category " + category.getName());
        doc.addBody(new Body() {


            @Override
            public String toString() {

                sb.append(new AdminTable(title));

                sb.append(new FormTag(PageSubmissionsHandler.class, "byCategory", "get"));
                SelectTag categoryTag = new SelectTag(context, "category");
                categoryTag.setAutoSubmit();


                for (Category category : categories) {
                    if (category.getName() != null) {
                        categoryTag.addOption(category, category.getName());
                    }
                }

                sb.append("<tr><th align='right' colspan='4'>Choose category ");
                sb.append(categoryTag);
                sb.append("</th></tr>");

                sb.append("</form>");


                sb.append("<tr>");
                sb.append("<th>ID</th>");
                sb.append("<th>Date</th>");
                sb.append("<th width='10'> </th>");
                sb.append("<th>Page</th>");
                sb.append("</tr>");

                for (Submission submission : counters) {

                    sb.append("<tr>");

                    sb.append("<td>" + new LinkTag(SubmissionHandler.class, "view", submission.getId(), "submission", submission) + "</td>");
                    sb.append("<td>" + submission.getDate().toString("dd-MMM-yyyy HH:mm") + "</td>");
                    sb.append("<td>");
                    if (submission.hasCountry()) {
                        sb.append(new FlagGif(submission.getCountry()));
                    }
                    sb.append("</td>");

                    sb.append("<td>");
                    if (submission.hasPageName()) {
                        sb.append(submission.getPageName());
                    }
                    sb.append("</td>");
                    sb.append("</tr>");
                }

                sb.append("</table>");

                return sb.toString();
            }

        });
        return doc;
    }
}
