package org.sevensoft.ecreator.iface.admin.ecom.orders;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 16 Apr 2007 19:10:02
 *
 */
@Path("admin-order-history.do")
public class OrderHistoryHandler extends AdminHandler {

	private Item				account;
	private transient OrderSettings	orderSettings;

	public OrderHistoryHandler(RequestContext context) {
		super(context);
		this.orderSettings = OrderSettings.getInstance(context);
	}

	@Override
	public Object main() throws ServletException {

		if (account == null) {
			return index();
		}

		final List<Order> orders = account.getOrders();

		AdminDoc doc = new AdminDoc(context, user, "Order history", Tab.Orders);
		doc.setMenu(new EditItemMenu(account));
		doc.addBody(new Body() {

			@Override
			public String toString() {

				int span = 5;

				sb.append(new AdminTable("Order history"));

				sb.append("<tr>");
				sb.append("<th>Date created</th>");
				sb.append("<th>Order number</th>");
				sb.append("<th>Status</th>");

				if (orderSettings.hasCategories()) {
					sb.append("<th>Sales category</th>");
					span++;
				}

				if (orderSettings.isSalesPersons()) {
					sb.append("<th>Sales person</th>");
					span++;
				}

				sb.append("<th>Total</th>");
				sb.append("<th>Invoiced</th>");
				sb.append("</tr>");

				for (Order order : orders) {

					sb.append("<tr>");
					sb.append("<td>" + order.getDatePlaced().toString("dd-MMM-yy") + "</td>");
					sb.append("<td>" + new LinkTag(OrderHandler.class, null, order.getOrderId(), "order", order));
					sb.append("<td>" + order.getStatus() + "</td>");

					sb.append("<td>");

					if (order.hasCategory()) {
						sb.append(order.getCategory());
					} else {
						sb.append("--");
					}

					sb.append("</td>");

					sb.append("<td>" + order.getSalesPersonString() + "</td>");

					sb.append("<td>" + order.getTotalInc() + "</td>");
					sb.append("<td>" + StringHelper.yesno(order.isInvoiced()) + "</td>");
					sb.append("</tr>");
				}

				sb.append("<tr><td colspan='" + span + "'>Add new order: " + new ButtonTag(OrderHandler.class, "create", "Add", "account", account)
						+ "</td></tr>");

				sb.append("</table>");

				return sb.toString();
			}

		});

		return doc;
	}
}
