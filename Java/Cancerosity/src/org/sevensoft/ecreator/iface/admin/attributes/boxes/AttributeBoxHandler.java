package org.sevensoft.ecreator.iface.admin.attributes.boxes;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.box.AttributeBox;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 24 Jul 2006 23:22:39
 *
 */
@Path("admin-attributes-box.do")
public class AttributeBoxHandler extends BoxHandler {

	private AttributeBox	box;
	private Attribute		attribute;
	private boolean		list;

	public AttributeBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {
		box.setAttribute(attribute);
		box.setList(list);
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Options"));

		SelectTag attributeTag = new SelectTag(context, "attribute", box.getAttribute());
		attributeTag.setAny("-None selected-");
		attributeTag.addOptions(Attribute.getSelectionMap(context));

		sb.append(new AdminRow("Attribute", "Select the attribute to display.", attributeTag));

		sb.append(new AdminRow("List", "Show as a list with text links.", new BooleanRadioTag(context, "link", box.isList())));

		sb.append("</table>");
	}

}
