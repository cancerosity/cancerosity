package org.sevensoft.ecreator.iface.admin.ecom.reports;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sam
 */
@Path("admin-shopping-reports.do")
public class ReportsHandler extends AdminHandler {

	public ReportsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() {

		AdminDoc page = new AdminDoc(context, user, "Sales reports", Tab.Orders);
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Reports"));

				sb.append("<tr><td>" + new LinkTag(SalesReportHandler.class, null, "Salesperson totals (weekly)") + "</td></tr>");

				sb.append("</table>");

				return sb.toString();
			}

		});

		return page;
	}

}
