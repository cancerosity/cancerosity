package org.sevensoft.ecreator.iface.admin.system.config;

import java.awt.GraphicsEnvironment;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 17 Apr 2007 09:20:29
 *
 */
@Path("admin-fonts.do")
public class FontsHandler extends AdminHandler {

	public FontsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		final String fontNames[] = ge.getAvailableFontFamilyNames();

		AdminDoc doc = new AdminDoc(context, user, "Fonts", null);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("fonts"));

				for (String name : fontNames) {

					sb.append("<tr>");
					sb.append("<td>" + name + "</td>");
					sb.append("</tr>");
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}
}
