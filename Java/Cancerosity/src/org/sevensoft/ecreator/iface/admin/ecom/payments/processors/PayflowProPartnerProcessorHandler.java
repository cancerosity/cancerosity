package org.sevensoft.ecreator.iface.admin.ecom.payments.processors;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.RadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.ecreator.model.ecom.payments.processors.paypal.PayflowProPartnerProcessor;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.ecom.payments.PaymentSettingsHandler;

import javax.servlet.ServletException;

/**
 * User: Tanya
 * Date: 21.04.2012
 */
@Path("admin-payments-processors-payflowpropartner.do")
public class PayflowProPartnerProcessorHandler extends ProcessorHandler {

    private PayflowProPartnerProcessor processor;

    private String username;
    private String vendor;
    private String partner;
    private String password;
    private boolean live;

    public PayflowProPartnerProcessorHandler(RequestContext context) {
        super(context);
    }

    public Object delete() {
        return super.delete(processor);
    }

    public Object main() throws ServletException {
        if (processor == null) {
            return index();
        }

        AdminDoc doc = new AdminDoc(context, user, "Payflow Pro API", null);
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update processor"));
                sb.append(new ButtonTag(PaymentSettingsHandler.class, null, "Return to payment settings"));
                sb.append("</div>");
            }

            private void general() {

                sb.append(new AdminTable("Payflow Pro API"));

                sb.append(new AdminRow("User", "The data is provided in your PayPal merchant account", new TextTag(context, "username",
                        processor.getUser(), 40)));

                sb.append(new AdminRow("Vendor", "", new TextTag(context, "vendor", processor.getVendor(), 20)));

                sb.append(new AdminRow("Partner", "", new TextTag(context, "partner", processor.getPartner(), 40)));

                sb.append(new AdminRow("Password", "", new TextTag(context, "password", processor.getPassword(), 40)));

                sb.append(new AdminRow("Mode", "In test mode all payments are simulated and no money is taken from the cards.", "Live "
                        + new RadioTag(context, "live", "true", processor.isLive()) + " Test "
                        + new RadioTag(context, "live", "false", !processor.isLive())));

                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(PayflowProPartnerProcessorHandler.class, "save", "post"));
                sb.append(new HiddenTag("processor", processor));

                commands();
                general();
                commands();
                sb.append("</form>");

                return sb.toString();
            }
        });
        return doc;
    }

    public Object save() throws ServletException {

        if (processor == null) {
            return index();
        }

        processor.setUser(username);
        processor.setPassword(password);
        processor.setPartner(partner);
        processor.setPassword(password);
        processor.setLive(live);
        processor.save();

        return new ActionDoc(context, "This processor has been updated", new Link(PayflowProPartnerProcessorHandler.class, null, "processor", processor));
    }
}
