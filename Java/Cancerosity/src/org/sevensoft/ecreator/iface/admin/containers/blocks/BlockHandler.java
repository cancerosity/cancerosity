package org.sevensoft.ecreator.iface.admin.containers.blocks;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 6 Nov 2006 11:28:16
 *
 */
@Path("admin-blocchi.do")
public class BlockHandler extends AdminHandler {

	private Block	block;

	public BlockHandler(RequestContext context) {
		super(context);
	}

	public Object delete() throws ServletException {

		if (block == null) {
			return index();
		}

		block.delete();
		return handler();
	}

	private Object handler() throws ServletException {

		if (block.hasOwnerCategory()) {
			return new CategoryHandler(context, block.getOwnerCategory()).edit();
		}

		else if (block.hasOwnerItemType()) {
			return new ItemTypeHandler(context, block.getOwnerItemType()).edit();
		}

		return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
	}

	@Override
	public Object main() throws ServletException {
		return HttpServletResponse.SC_NO_CONTENT;
	}

	public Object moveDown() throws ServletException {

		if (block == null) {
			return index();
		}

		List<Block> blocks = block.getOwner().getBlocks();
		EntityUtil.moveDown(block, blocks);

		return handler();
	}

	public Object moveUp() throws ServletException {

		if (block == null) {
			return index();
		}

		List<Block> blocks = block.getOwner().getBlocks();
		EntityUtil.moveUp(block, blocks);

		return handler();
	}

}
