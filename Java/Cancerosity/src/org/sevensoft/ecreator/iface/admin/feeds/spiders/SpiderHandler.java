package org.sevensoft.ecreator.iface.admin.feeds.spiders;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.accounts.AccountModuleHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.feeds.spiders.EbayStoreSpider;
import org.sevensoft.ecreator.model.feeds.spiders.FoodPlaceSpider;
import org.sevensoft.ecreator.model.feeds.spiders.NgrcSpider;
import org.sevensoft.ecreator.model.feeds.spiders.Spider;
import org.sevensoft.ecreator.model.feeds.spiders.YellSpider;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 22-Nov-2005 09:42:19
 * 
 */
@Path("admin-spiders.do")
public class SpiderHandler extends AdminHandler {

	private String	url;
	private Spider	spider;
	private ItemType	itemType;
	private Category	category;

	public SpiderHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() {

		final List<Spider> spiders = new ArrayList();
		spiders.add(new EbayStoreSpider());
		spiders.add(new NgrcSpider());
		spiders.add(new FoodPlaceSpider());
		spiders.add(new YellSpider());

		AdminDoc doc = new AdminDoc(context, user, "Spider", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Run spider"));
				sb.append(new ButtonTag(AccountModuleHandler.class, null, "Return to member settings"));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("Spider"));

				SelectTag itemTypeTag = new SelectTag(context, "itemType");
				itemTypeTag.setAny("-Select item type-");
				itemTypeTag.addOptions(ItemType.getSelectionMap(context));

				sb.append(new AdminRow("Item type", null, itemTypeTag));

				SelectTag spiderTag = new SelectTag(context, "spider");
				spiderTag.setAny("-Select spider class-");
				spiderTag.addOptions(spiders);

				sb.append(new AdminRow("Spider class", "Choose class to implement spider", spiderTag));
				sb.append(new AdminRow("Url", "Enter source url", new TextTag(context, "url", 40)));
				sb.append(new AdminRow("Category", "Put newly created items in this category", new SelectTag(context, "category", null, Category
						.getCategoryOptions(context), "-Select category-")));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(SpiderHandler.class, "run", "POST"));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object run() {

		if (url == null) {
			addError("No url");
			return main();
		}

		if (itemType == null) {
			addError("No item type set");
			return main();
		}

		if (spider == null) {
			addError("No spider class");
			return main();
		}

		try {

			int created = spider.spider(context, itemType, url, category);
			return new ActionDoc(context, created + " items created", new Link(SpiderHandler.class));

		} catch (MalformedURLException e) {
			e.printStackTrace();
			addError(e);
		} catch (IOException e) {
			e.printStackTrace();
			addError(e);
		}

		return main();
	}
}
