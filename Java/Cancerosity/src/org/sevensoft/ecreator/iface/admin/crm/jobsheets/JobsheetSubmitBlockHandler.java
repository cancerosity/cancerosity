package org.sevensoft.ecreator.iface.admin.crm.jobsheets;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.crm.jobsheets.Jobsheet;
import org.sevensoft.ecreator.model.crm.jobsheets.blocks.JobsheetSubmitBlock;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 12 Mar 2007 13:04:26
 *
 */
@Path("admin-blocks-jobsheet-submit.do")
public class JobsheetSubmitBlockHandler extends BlockEditHandler {

	private JobsheetSubmitBlock	block;

	private Jobsheet			jobsheet;

	public JobsheetSubmitBlockHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Block getBlock() {
		return block;
	}

	@Override
	protected void saveSpecific() {
		block.setJobsheet(jobsheet);
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Options"));

		sb.append(new AdminRow("Jobsheet", "This is the jobsheet that the submitted form goes into", new SelectTag(context, "jobsheet",
				block.getJobsheet(), Jobsheet.get(context), "-Select jobsheet-")));

		sb.append("</table>");
	}

}
