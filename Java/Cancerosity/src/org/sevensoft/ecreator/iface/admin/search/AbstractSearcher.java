package org.sevensoft.ecreator.iface.admin.search;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.QueryBuilder;

import java.util.List;
import java.util.Map;

/**
 * User: Tanya
 * Date: 15.03.2012
 */
public abstract class AbstractSearcher {

    private final RequestContext context;
    private int limit;
    private int start;

    public AbstractSearcher(RequestContext context) {
        this.context = context;
    }

    public final int getLimit() {
        return limit;
    }

    public final int getStart() {
        return start;
    }

    public final void setLimit(int limit) {
        this.limit = limit;
    }

    public final void setStart(int start) {
        this.start = start;
    }

    public final List<EntityObject> execute() {
        return (List<EntityObject>)run(false);
    }

    public final int size() {
        return (Integer) run(true);
    }

    private Object run(boolean count) {

        QueryBuilder b = new QueryBuilder(context);

        if (count) {
            b.select("count(*)");
        } else {
            b.select("*");
        }

        b.from("#", getTable());

        if (getWhere() != null && !getWhere().isEmpty()) {
            for (Map.Entry<String, String> entry : getWhere().entrySet()) {
                b.clause(entry.getKey(), entry.getValue());
            }
        }

        if (getOrderby() != null)
            b.order(getOrderby());

        if (count) {
            return b.toQuery().getInt();
        } else {
            return b.toQuery().execute(getTable(), start, limit);
        }
    }

    public String getOrderby() {
        return null;
    }

    public Map<String, String> getWhere() {
        return null;
    }

    public abstract Class<EntityObject> getTable();
}
