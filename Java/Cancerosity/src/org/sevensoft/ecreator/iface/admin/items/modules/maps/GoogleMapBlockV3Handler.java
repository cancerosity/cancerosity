package org.sevensoft.ecreator.iface.admin.items.modules.maps;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.model.extras.mapping.google.blocks.GoogleMapBlock;
import org.sevensoft.ecreator.model.extras.mapping.google.blocks.GoogleMapBlockV3;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.containers.blocks.Block;

import java.util.Map;

/**
 * User: Tanya
 * Date: 22.12.2011
 */
@Path("admin-blocks-google-map-v3.do")
public class GoogleMapBlockV3Handler extends BlockEditHandler {

    private GoogleMapBlockV3 block;
    private String caption;
    private int zoom;
    private int iconWindowAnchorY;
    private int iconWindowAnchorX;
    private int iconAnchorY;
    private int iconAnchorX;
    private String iconFilename;
    private int iconHeight;
    private int iconWidth;
    private String location;
    private int width;
    private int height;
    private boolean showMapTypeControl;
    private GoogleMapBlockV3.MapType mapType;
    private boolean useLatLangAttr;
    private Attribute locationAttribute;
    private Attribute latAttribute;
    private Attribute langAttribute;


    public GoogleMapBlockV3Handler(RequestContext context) {
        super(context);
    }

    private void customIcon(StringBuilder sb) {

        sb.append(new AdminTable("Custom Icon"));

        sb.append(new AdminRow("Icon filename", "The filename of the icon, relative to template-data.", new TextTag(context, "iconFilename", block
                .getIconFilename(), 20)));

        sb.append(new AdminRow("Icon size", "Width " + new TextTag(context, "iconWidth", block.getIconWidth(), 5) + " Height " +
                new TextTag(context, "iconHeight", block.getIconHeight(), 5)));

        sb.append(new AdminRow("Icon anchor", "x " + new TextTag(context, "iconAnchorX", block.getIconAnchorX(), 3) + " y " +
                new TextTag(context, "iconAnchorY", block.getIconAnchorY(), 3)));

        sb.append(new AdminRow("Icon window anchor", "x " + new TextTag(context, "iconWindowAnchorX", block.getIconWindowAnchorX(), 3) + " y " +
                new TextTag(context, "iconWindowAnchorY", block.getIconWindowAnchorY(), 3)));

        sb.append("</table>");
    }

    @Override
    protected Block getBlock() {
        return block;
    }

    @Override
    protected void saveSpecific() {

//		block.setApiKey(apiKey);
        block.setCaption(caption);
        block.setZoom(zoom);
        block.setLocation(location);
        block.setUseLatLangAttr(useLatLangAttr);
        if (!useLatLangAttr) {
            block.setLocationAttribute(locationAttribute);
        } else {
            block.setLangAttribute(langAttribute);
            block.setLatAttribute(latAttribute);
        }

        block.setIconFilename(iconFilename);

        block.setIconWidth(iconWidth);
        block.setIconHeight(iconHeight);

        block.setIconAnchorX(iconAnchorX);
        block.setIconAnchorY(iconAnchorY);

        block.setIconWindowAnchorX(iconWindowAnchorX);
        block.setIconWindowAnchorY(iconWindowAnchorY);

        block.setWidth(width);
        block.setHeight(height);

        block.setShowMapTypeControl(showMapTypeControl);
        block.setMapType(mapType);
    }

    @Override
    protected void specifics(StringBuilder sb) {

        sb.append(new AdminTable("General"));

//		sb.append(new AdminRow("Api Key", "Google provides an API key to allow you to use their mapping code.", new TextTag(context, "apiKey", block
//				.getApiKey(), 40)));

        sb.append(new AdminRow("Zoom level", "This determines the initial zoom level; of course the user can zoom in and out as they wish.", new TextTag(
                context, "zoom", block.getZoom(), 4)));

        sb.append(new AdminRow("Caption", "This caption will appear above the map.", new TextTag(context, "caption", block.getCaption(), 40)));

        sb.append(new AdminRow("Location", "Manually set the location for this map.", new TextTag(context, "location", block.getLocation(), 20)));

        Map<String, String> locationAttributes = Attribute.getSelectionMap(context);

        sb.append(new AdminRow("Use Lat/Long or Location ", "To use Location Attribute for GoogleMap set No", new BooleanRadioTag(context,
                "useLatLangAttr", block.isUseLatLangAttr())));

        if (!block.isUseLatLangAttr()) {
            SelectTag location = new SelectTag(context, "locationAttribute", block.getAttribute());
            location.setAny("-None selected-");
            location.addOptions(locationAttributes);
            sb.append(new AdminRow("Location Attribute", location));
        } else {
            SelectTag lat = new SelectTag(context, "latAttribute", block.getLatAttribute());
            lat.setAny("-None selected-");
            lat.addOptions(locationAttributes);
            sb.append(new AdminRow("Latitude Attribute", lat));              //todo

            SelectTag lang = new SelectTag(context, "langAttribute", block.getLangAttribute());
            lang.setAny("-None selected-");
            lang.addOptions(locationAttributes);
            sb.append(new AdminRow("Longitude Attribute", lang));
        }
        sb.append(new AdminRow("Show map type control on the map", "", new BooleanRadioTag(context,
                "showMapTypeControl", block.isShowMapTypeControl())));

        sb.append(new AdminRow("Map type", "Choose the initial map type", new SelectTag(context, "mapType", block.getMapType(), GoogleMapBlock.MapType.values())));

        sb.append(new AdminRow("Map size", "Sets the size in pixels of the map. By default, the map will assume the size of its container.",
                "Width " + new TextTag(context, "width", block.getWidth(), 5) + " Height " + new TextTag(context, "height", block.getHeight(), 5)));

        sb.append("</table>");

        customIcon(sb);
    }

}
