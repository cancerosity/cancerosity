package org.sevensoft.ecreator.iface.admin.misc.location;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.misc.location.box.ActiveLocationBox;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 1 Apr 2007 11:59:24
 *
 */
@Path("admin-boxes-posizione-attiva.do")
public class ActiveLocationBoxHandler extends BoxHandler {

	private ActiveLocationBox	box;
	private String			forward;
	private String			header;
	private String			footer;

	public ActiveLocationBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {

		box.setForward(forward);
		box.setFooter(footer);
		box.setHeader(header);
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Content"));

		sb.append(new AdminRow("Forward url", "This is the url the user is sent to after they have set the new location.", new TextTag(context, "forward",
				box.getForward(), 40)));

		sb.append(new AdminRow("Header", "This content appears above the input box.", new TextAreaTag(context, "header", box.getHeader(), 50, 4)));
		sb.append(new AdminRow("Footer", "This content appears below the input box.", new TextAreaTag(context, "footer", box.getFooter(), 50, 4)));

		sb.append("</table>");
	}

}
