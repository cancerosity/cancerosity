package org.sevensoft.ecreator.iface.admin.system.users;

import java.util.List;
import java.util.Map;
import java.io.IOException;

import javax.servlet.ServletException;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Transformer;
import org.sevensoft.commons.validators.UniqueValidator;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.media.images.panels.ImageAdminPanel;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.model.system.users.privileges.Privilege;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 15-Nov-2004 13:31:28
 */
@Path("admin-settings-users.do")
public class UserHandler extends AdminHandler {

	private boolean			administrator;
	private User			editUser;
	private String			username, password, name;
	private List<Privilege>		privileges;
	private String			email;
	private Map<Integer, String>	imageDescriptions;
	private List<String>		imageUrls;
	private List<Upload>		imageUploads;
	private String			imageFilename;
	private boolean			crm;

	public UserHandler(RequestContext context) {
		super(context);
	}

	public UserHandler(RequestContext context, User user) {
		this(context);
		this.user = editUser;
	}

	public Object add() throws ServletException {

		if (!user.isAdministrator()) {
			return main();
		}

		// this username must be unique
		test(new UniqueValidator(username, CollectionsUtil.transform(User.getActive(context), new Transformer<User, String>() {

			public String transform(User obj) {
				return obj.getUsername();
			}
		})), "username");

		if (hasErrors()) {
			return main();
		}

		if (username != null) {
			new User(context, username);
		}

		return main();
	}

	public Object delete() throws ServletException {

		if (!user.isAdministrator()) {
			return main();
		}

		if (editUser != null) {
			editUser.delete();
		}

		return main();
	}

	/*
	 * @see org.sevensoft.commons.jwf.Handler#main()
	 */
	public Object edit() throws ServletException {

		if (editUser == null) {
			return main();
		}

		if (!user.isAdministrator()) {
			return main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit user", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update user details"));
				sb.append(new ButtonTag(UserHandler.class, "delete", "Delete user", "editUser", editUser)
						.setConfirmation("Are you sure you want to delete this user?"));
				sb.append(new ButtonTag(UserHandler.class, null, "Return to users menu"));
				sb.append("</div>");
			}

			private void details() {

				sb.append(new AdminTable("User details"));

				sb.append(new AdminRow("Display name", "This is the name of the user that will be shown in logs.", new TextTag(context, "name",
						editUser.getName()) +
						" " + new ErrorTag(context, "name", "<br/>")));

				sb.append(new AdminRow("Username", "This is the name used by the user to login.", new TextTag(context, "username", editUser
						.getUsername(), 12) +
						" " + new ErrorTag(context, "username", "<br/>")));

				sb.append(new AdminRow("Password", "This is the password used by the user to login.", new TextTag(context, "password", editUser
						.getPassword(), 12) +
						" " + new ErrorTag(context, "password", "<br/>")));

				sb.append(new AdminRow("Email", "Enter your email so the system can alert you with important messages", new TextTag(context, "email",
						editUser.getEmail(), 40)));

				sb.append(new AdminRow("Administrator", "Administrator privileges lets the user edit and create other user accounts.",
						new BooleanRadioTag(context, "administrator", editUser.isAdministrator())));

				sb.append(new AdminRow("CRM", "This user appears in the drop down list of users for assigning owners in the CRM module.",
						new BooleanRadioTag(context, "crm", editUser.isCrm())));

				sb.append("</table>");
			}

			private void privileges() {

				sb.append(new AdminTable("Privileges"));

				for (Privilege p : Privilege.values()) {
					sb.append(new AdminRow(p.toString(), p.getDescription(), new CheckTag(context, "privileges", p, editUser.hasPrivilege(p))));
				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(UserHandler.class, "save", "multi"));
				sb.append(new HiddenTag("editUser", editUser));

                commands();
				details();
				if (Module.Privileges.enabled(context)) {
					if (!editUser.isAdministrator()) {
						privileges();
					}
				}

				//  are user images enabled?
				if (Module.UserImages.enabled(context)) {
					sb.append(new ImageAdminPanel(context, editUser));
				}

				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		if (!user.isAdministrator()) {
			return index();
		}

		final List<User> users = User.getActive(context);

		AdminDoc doc = new AdminDoc(context, user, "Users", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			@Override
			public String toString() {

                commands();
				users();
				commands();

				return sb.toString();
			}

			private void users() {

				sb.append(new AdminTable("Users"));

				sb.append("<tr>");
				sb.append("<th>Name</th>");
				sb.append("<th>Username</th>");
				sb.append("<th>Password</th>");
				sb.append("<th>Type</th>");
				sb.append("<th width='20'></th>");
				sb.append("</tr>");

				for (User u : users) {

					sb.append("<tr>");
					sb.append("<td>" + new LinkTag(UserHandler.class, "edit", new SpannerGif(), "editUser", u) + " " + u.getName() + "</td>");
					sb.append("<td>" + u.getUsername() + "</td>");
					sb.append("<td>" + u.getPassword() + "</td>");
					sb.append("<td>" + u.getType() + "</td>");
					sb.append("<td width='20'>" +
							new LinkTag(UserHandler.class, "delete", new DeleteGif(), "editUser", u)
									.setConfirmation("Are you sure you want to delete this user?") + "</td>");
					sb.append("</tr>");

				}

				sb.append(new FormTag(UserHandler.class, "add", "get"));
				sb.append("<tr><td colspan='5'>Add new user: " + new TextTag(context, "username") + " " + new SubmitTag("Add user") +
						new ErrorTag(context, "username", "<br/>") + "</td></tr>");
				sb.append("</form>");

				sb.append("</table>");
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (!user.isAdministrator()) {
			return main();
		}

		if (editUser == null) {
			return main();
		}

		test(new LengthValidator(3), "name");
		test(new LengthValidator(3), "username");
		test(new LengthValidator(3), "password");

		List<User> users = User.getAll(context);
		users.remove(editUser);

		test(new UniqueValidator(username, CollectionsUtil.transform(users, new Transformer<User, String>() {

			public String transform(User obj) {
				return obj.getUsername();
			}

		})), "username");

		if (hasErrors()) {
			return edit();
		}

		editUser.setUsername(username);
		editUser.setName(name);
		editUser.setPassword(password);
		editUser.setAdministrator(administrator);
		editUser.setEmail(email);
		editUser.setCrm(crm);

		if (!editUser.isAdministrator()) {

			if (Module.Privileges.enabled(context)) {
				editUser.setPrivileges(privileges);
			}
		}

		editUser.save();

		try {

			org.sevensoft.ecreator.model.media.images.ImageUtil.save(context, editUser, imageUploads, imageUrls, imageFilename, true);

		} catch (ImageLimitException e) {
			addError(e);
		} catch (IOException e) {
            addError("The image you are attempting to upload is in CYMK colour mode. This is unsopported currently, " +
                    "please use a photo editing application (e.g. Photoshop) to convert the file to RGB colour. For more information or assistance please contact support");
        }

        addMessage("User updated.");
		clearParameters();
		return edit();
	}
}