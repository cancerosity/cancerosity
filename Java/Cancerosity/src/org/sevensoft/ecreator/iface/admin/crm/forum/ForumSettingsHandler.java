package org.sevensoft.ecreator.iface.admin.crm.forum;

import java.io.IOException;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.crm.forum.ForumSettings;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;

/**
 * @author sks 03-Oct-2005 14:25:41
 * 
 */
@Path("admin-settings-forum.do")
public class ForumSettingsHandler extends AdminHandler {

	private ForumSettings	forumSettings;
	private String		css;
	private String		boardHeader;

	public ForumSettingsHandler(RequestContext context) {
		super(context);
		this.forumSettings = ForumSettings.getInstance(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc page = new AdminDoc(context, user, "Forum settings", Tab.Orders);
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update forum settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(ForumSettingsHandler.class, "save", "POST"));
                commands();
				sb.append(new TextAreaTag(context, "css", forumSettings.getCss(), 120, 20));
				sb.append("Board header: " + new TextAreaTag(context, "boardHeader", forumSettings.getBoardHeader(), 60, 6));

				commands();
				sb.append("</form>");

				return sb.toString();
			}

		});
		return page;
	}

	public Object save() throws ServletException {

		try {
			forumSettings.setCss(css);
		} catch (IOException e) {
			addError(e);
		}

		forumSettings.setBoardHeader(boardHeader);
		forumSettings.save();

		addMessage("The forum settings have been updated with your changes.");
		clearParameters();
		return main();
	}
}
