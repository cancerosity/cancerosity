package org.sevensoft.ecreator.iface.admin.accounts.boxes;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.interaction.buddies.box.BuddiesBox;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 25 Jul 2006 06:41:55
 *
 */
@Path("boxes_buddies.do")
public class BuddiesBoxHandler extends BoxHandler {

	private BuddiesBox	box;
	private String		onlineImage;
	private String		offlineImage;

	public BuddiesBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {

	}

	@Override
	protected void specifics(StringBuilder sb) {

	}

}
