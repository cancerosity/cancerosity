package org.sevensoft.ecreator.iface.admin.ecom.bookings;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.ecreator.model.bookings.Booking;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.logging.Logger;

/**
 * User: MeleshkoDN
 * Date: 14.09.2007
 * Time: 17:49:17
 */
public class BookingNotificationEmails {

    private static final Logger logger = Logger.getLogger("ecreator");

    public static void informOwnerOfBooking(RequestContext context, Booking booking) {
        Company company = Company.getInstance(context);
        String companyName = company.getName();
        StringBuffer body = new StringBuffer();
        body.append(companyName + " have recieved a booking request for the period " + getPeriod(booking) + " in the " + booking.getItem().getName() + " property.");
        body.append(" Can you please confirm that this is available and provisionally book it for our client," +
                "or advise of any alternative dates close to this time.\n");
        body.append("Kind Regards\n");
        body.append(companyName);
        sendEmailToPropertyOwner(context, booking, body);
    }

    public static void informClientOfBooking(RequestContext context, Booking booking) {
        Company company = Company.getInstance(context);
        String companyName = company.getName();
        StringBuffer body = new StringBuffer();
        body.append("We are delighted to confirm that the " + booking.getItem().getName() + " property has provisionally been booked for " +
                "you for the period " + getPeriod(booking) + ".");
        body.append("We will contact you soon to collect payment, upon receipt of which your booking will be confirmed.\n");
        body.append("Thank you for booking through " + companyName);
        sendEmailToClient(context, booking, body);
    }

    public static void informOwnerOfConfirmation(RequestContext context, Booking booking) {
        Company company = Company.getInstance(context);
        String companyName = company.getName();
        StringBuffer body = new StringBuffer();
        body.append("We can confirm that the " + booking.getItem().getName() + " property has been booked for " + booking.getAccount().getName() +
                " for the period " + getPeriod(booking) + ".");
        body.append("Please update your availability accordingly.\n");
        body.append("Kind Regards\n");
        body.append(companyName);
        sendEmailToPropertyOwner(context, booking, body);
    }

    public static void informClientOfConfirmation(RequestContext context, Booking booking) {
        Company company = Company.getInstance(context);
        String companyName = company.getName();
        StringBuffer body = new StringBuffer();
        body.append("We are delighted to advise that " + booking.getItem().getName() + " " +
                "property has been booked for the period " + getPeriod(booking) + ".");
        body.append("We will contact you 8 weeks before the commencement date to collect the balance of payment.\n");
        body.append("Thank you for booking with " + companyName);
        sendEmailToClient(context, booking, body);
    }

    public static void informOwnerOfPayment(RequestContext context, Booking booking) {
        Company company = Company.getInstance(context);
        String companyName = company.getName();
        StringBuffer body = new StringBuffer();
        body.append("We are delighted to confirm the receipt of the deposit/final payment for the " + booking.getItem().getName() + " property from " + booking.getAccount().getName() +
                " for the period " + getPeriod(booking) + ".\n");
        body.append("Kind Regards\n");
        body.append(companyName);
        sendEmailToPropertyOwner(context, booking, body);
    }

    private static void sendEmailToPropertyOwner(RequestContext context, Booking booking, StringBuffer body) {
        Item propertyOwner = booking.getPropertyOwner();
        if (propertyOwner != null && propertyOwner.getEmail() != null) {
            Email email = Email.composeEmail(context, "Booking", propertyOwner.getEmail(), body);
            Email.sendEmail(context, email);
        }
    }

    private static void sendEmailToClient(RequestContext context, Booking booking, StringBuffer body) {
        Email email = Email.composeEmail(context, "Booking", booking.getAccount().getEmail(), body);
        Email.sendEmail(context, email);
    }

    private static String getPeriod(Booking booking) {
        return booking.getStart().toString("dd.MM.yyyy") + " to " + booking.getEnd().toString("dd.MM.yyyy");
    }
}
