package org.sevensoft.ecreator.iface.admin;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.EcreatorDoc;
import org.sevensoft.ecreator.iface.admin.design.markup.MarkerReferenceHandler;
import org.sevensoft.ecreator.iface.admin.design.markup.MarkupHandler;
import org.sevensoft.ecreator.iface.admin.design.template.StylesheetHandler;
import org.sevensoft.ecreator.iface.admin.design.template.TemplateDataHandler;
import org.sevensoft.ecreator.iface.admin.design.template.TemplateHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.config.*;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Menu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.modules.ModulesHandler;
import org.sevensoft.ecreator.model.system.config.Toolbar;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.model.design.template.Template;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.MessagesTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Ajax;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author sks 01-Dec-2005 00:55:47
 */
public class AdminDoc extends EcreatorDoc {

    private String title;
    private User user;
    private List<LinkTag> links;
    private Tab currentTab;
    private String intro;
    private boolean deprecated;
    private Menu menu;
    private boolean superman;
    private boolean sam;
    private String[] helpBalloons;

    public AdminDoc(RequestContext context, User user, String title, Tab tab) {
        super(context);

        this.user = user;
        this.title = title;
        this.currentTab = tab;
        this.sam = context.isLocalhost() || context.getRemoteIp().equals("81.168.52.32");

        this.links = new ArrayList();
        this.superman = context.containsAttribute("superman");
    }

    private void brochureHeader(StringBuilder sb) {

        sb.append("<table class='brochureHeader' cellspacing='0' cellpadding='0'><tr><td>");

        if (superman) {
            developerDiv(sb);
        }

        sb.append("<div class='company'><span>" + company.getName() + " Control Panel</span> powered by " + config.getDevelopers() + "</div>");

        sb.append("<table class='brochureTabs' cellspacing='0' cellpadding='0'><tr>");

        List<Tab> tabs = Tab.getBrochureTabs(context);
        for (Tab tab : tabs) {
            sb.append("<td>" + tab.getLinkTag() + "</td>");
        }

        sb.append("</tr></table>");

        sb.append("<div class='brochureSubmenu'></div>");

        sb.append("</td></tr></table>");
    }

    private void developerDiv(StringBuilder sb) {

        sb.append("<div class='developer'>");

        sb.append(new LinkTag(ModulesHandler.class, null, "Modules"));
        sb.append(" | ");
        sb.append(new LinkTag(ConfigHandler.class, null, "Config"));
        sb.append(" | ");
        sb.append(new LinkTag(CommandHandler.class, null, "Commands"));

        sb.append(" | ");
        sb.append(new LinkTag(MarkupHandler.class, null, "Markup"));

        sb.append(" | ");
        sb.append(new LinkTag(TemplateHandler.class, null, "Templates"));
        sb.append(" | ");
        sb.append(new LinkTag(TemplateDataHandler.class, null, "Template images"));
        sb.append(" | ");
        sb.append(new LinkTag(StylesheetHandler.class, null, "Colors"));

        if (sam) {

            sb.append(" | ");
            sb.append(new LinkTag(DatabaseHandler.class, null, "Database"));
            sb.append(" | ");
            sb.append(new LinkTag(StatsHandler.class, null, "Stats"));
            sb.append(" | ");
            sb.append(new LinkTag(UpgradeHandler.class, null, "Upgrades"));

            sb.append(" | ");
            sb.append(new LinkTag(MarkerReferenceHandler.class, null, "Marker reference"));

            if (context.isLocalhost()) {
                sb.append(" | ");
                sb.append(new LinkTag(ModulesHandler.class, null, "Write handlers", "write_handlers", "1"));
            }
        }

        sb.append(" | ");
        sb.append(new LinkTag(SettingsMenuHandler.class, null, "Settings"));

        sb.append("</div>");

    }

    private void fullHeader(StringBuilder sb) {

        if (superman) {
            developerDiv(sb);
        }

        sb.append("<table class='header' align='center' cellspacing='0' cellpadding='0'>");
        sb.append("<tr><td class='name' valign='bottom'><span>" + company.getName() + "</span> powered by " + config.getDevelopers());

        sb.append("</td><td class='status' align='right'>");
        infos(sb);
        sb.append("</td>");

        if (helpBalloons != null) {

            sb.append("<td class='icon' align='center'>");
            sb.append(new LinkTag("#", "<img src='files/graphics/admin/icon_help.gif' border='0'><br/>Help").setOnClick("help(); return false;"));
            sb.append("</td>");

        }

        sb.append("<td class='icon' align='center'>");
        sb.append(new LinkTag(SettingsMenuHandler.class, null, "<img src='files/graphics/admin/icon_settings.gif' border='0'><br/>Settings"));
        sb.append("</td>");

        sb.append("<td></td>");

        sb.append("<td class='icon' align='center'>");
        sb.append(new LinkTag("http://7soft.helpserve.com/", "<img src='files/graphics/admin/icon_support.gif' border='0'><br/>Support"));
        sb.append("</td>");

        sb.append("</tr></table>");

        List<Tab> tabs = Tab.getFullTabs(context);

        sb.append("<div class='menu'><table cellspacing='0' cellpadding='0'><tr>");

        for (Tab tab : tabs) {

            if (tab.equals(currentTab)) {

                sb.append("<td class='active'>");

            } else {

//                sb.append("<td onmouseover=\"this.className='hover'\" onmouseout=\"this.className=''\">");
                sb.append("<td>");
            }

            sb.append(tab.getLinkTag());
            sb.append("</td>");
        }

        sb.append("</tr></table></div>");

        sb.append("<div class='menuline'></div>");

        if (menu != null) {

            List<LinkTag> links = menu.getLinkTags(context);
            if (links != null) {

                sb.append("<div class='submenucontainer'>");
                sb.append("<table class='submenu' cellspacing='0' cellpadding='0'><tr>");

                for (LinkTag l : menu.getLinkTags(context)) {

                    sb.append("<td onmouseover=\"this.className='hover'\" onmouseout=\"this.className=''\">");
                    sb.append(l);
                    sb.append("</td>");
                }

                sb.append("</tr></table>");
                sb.append("</div>");

            }

        }
    }

    public String getIntro() {
        return intro;
    }

    private void head(StringBuilder sb) {

        sb.append("<head>\n");
        sb.append("	<title>" + company.getName() + " - designed by " + config.getDevelopers() + "</title>\n");
        sb.append("	<link rel='stylesheet' href='files/css/stylesheets-admin.css' type='text/css'/>\n");
        sb.append("	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n");
        sb.append(" <meta http-equiv=\"X-UA-Compatible\" content=\"IE=9\">");

        sb.append(Ajax.ajax());

        sb.append("<script type='text/javascript' src='files/js/admin/ecreator-admin.js'></script>\n");

        if (context.isLocalhost()) {
            //			sb.append("<style> body { background: #ffeecc; } </style>");
        }

        // make menu really small for rallyinguk
        if (config.getUrl().contains("rallyinguk.com")) {
            sb.append("<style> div.menu table { font-size: 9px; } </style>");
        }
        Template dfault = Template.getDefault(context);
        if (dfault != null && dfault.hasHeadBackend()) {
            sb.append(dfault.renderHeadBackend(context));
        }

        for (Object obj : getHeads()) {
            sb.append(obj);
        }

        sb.append("</head>\n");
    }

    private void infos(StringBuilder sb) {

        List<String> infos = new ArrayList();

        if (miscSettings.isAdvancedMode()) {
            infos.add("Advanced mode");
        }

        if (miscSettings.isOffline()) {
            infos.add("Site offline ");

        } else if (config.isDemo()) {
            infos.add("Demo version ");
        }

        sb.append(StringHelper.implode(infos, ", "));
    }

    public boolean isSuperman() {
        return context.containsAttribute("superman");
    }

    /**
     * Returns a string builder for output
     */
    public StringBuilder output() throws IOException {

        StringBuilder sb = new StringBuilder();

        sb.append("<!DOCTYPE html5>\n");
        sb.append("<html>\n");
        head(sb);
        sb.append("<body>\n");
        sb
                .append("<div style='height: 100%; width: 100%; position: absolute; display: none; background: #cccccc;filter: alpha(opacity=50); -moz-opacity: .50; opacity: .50;' id='back'></div>");

        sb.append(new Toolbar(user, context));

        switch (config.getSoftwareVersion()) {

            default:
            case Full:
                fullHeader(sb);
                break;

            case Brochure:
                brochureHeader(sb);
                break;
        }

        sb.append("<table cellspacing='0' cellpadding='0' class='main' align='center'><tr><td valign='top'>");

        sb.append("<table cellspacing='0' cellpadding='0' class='title' align='center'><tr><td class='text'><h1>" + title + "</h1><h2>" +
                (intro == null ? "" : intro) + "</h2></td></tr></table>");

        sb.append(new MessagesTag(context));

        for (Object obj : getBodies()) {
            sb.append(obj);
        }

        sb.append("</td></tr></table>");

        sb.append("<table class='bottom' cellspacing='0' cellpadding='0' align='center'><tr><td align='enter'>" + "Designed and built by " +
                new LinkTag("http://www.7soft.co.uk", "www.7soft.co.uk") + " | All content copyright " + company.getName() + " " +
                new Date().getYear() + "</td></tr></table>");

        sb.append("</td></tr></table>");
        sb.append("</body>");
        sb.append("</html>");

        return sb;
    }

    public void setHelp(String... helpBalloons) {
        this.helpBalloons = helpBalloons;

    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
