package org.sevensoft.ecreator.iface.admin.items.listings;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.items.listings.ListingSettings;
import org.sevensoft.ecreator.model.items.listings.ListingSettings.CategorySelectionStyle;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sam
 */
@Path("admin-settings-listings.do")
public class ListingSettingsHandler extends AdminHandler {

	private String				expiryReminderDays;
    private String expiryReminderBody;
	private CategorySelectionStyle	categorySelectionStyle;
	private String				notificationEmails;

	private transient ListingSettings	listingSettings	= ListingSettings.getInstance(context);
	private String				notificationBody;
	private String				confirmationHeader;
	private String				confirmationFooter;
    private String choosePackageText;

	public ListingSettingsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (!Module.Listings.enabled(context)) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Listing settings", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update listing settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Expiry reminder days",
						"The number of days before a listing is due to expire that the system will send a reminder email. "
								+ "<i>You can enter multiple days separated by a space</i>", new TextTag(context, "expiryReminderDays",
								StringHelper.implode(listingSettings.getExpiryReminderDays(), " "), 20)));

                sb.append(new AdminRow("Expiry reminder email body", "This is the body text used in the listing exriry reminder email.<br/><br/>Special tags:<br/>" +
                        "<i>[name]</i> - Listing name<br/>" + "<i>[itemurl]</i> - Url to view listing<br/>" +
                        "<i>[listing_expiry_date]</i> - Listing expiry date<br/>" +
                        "<i>[id]</i> - Generated id<br/>" + "<i>[login]</i> - Url to login page<br/>" +
                        "<i>[loginurl]</i> - Login url<br/>" + "<i>[site]</i> - Your site name<br/>"
                        , new TextAreaTag(context, "expiryReminderBody", listingSettings.getExpiryReminderBody(), 60, 5)));

				sb.append(new AdminRow("Notification emails",
						"Any email addresses you enter here will be sent a notification email each time a listing is added.",
						new TextAreaTag(context, "notificationEmails", StringHelper.implode(listingSettings.getNotificationEmails(), "\n", true),
								60, 5)));

				if (listingSettings.hasNotificationEmails()) {

					sb.append(new AdminRow("Notification body", "This is the body text used in the email for notifications.", new TextAreaTag(
							context, "notificationBody", listingSettings.getNotificationBody(), 60, 5)));

				}

				sb.append("</table>");
			}

            private void introductions() {
                sb.append(new AdminTable("Introductions"));

                sb.append(new AdminRow("'Choose listing package' header", "", new TextAreaTag(context, "choosePackageText",
                        listingSettings.getChoosePackageText(), 60, 3)));

                sb.append("</table>");
            }

			@Override
			public String toString() {

				if (miscSettings.isHtmlEditor()) {
                    RendererUtil.tinyMce(context, sb, "confirmationHeader", "confirmationFooter");
				}

				sb.append(new FormTag(ListingSettingsHandler.class, "save", "POST"));

                commands();
				general();
                introductions();
				confirmation();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

			private void confirmation() {

				sb.append(new AdminTable("Confirmation page"));

				sb.append("<tr><td>Confirmation page header</td></tr>");
				sb.append("<tr><td>"
						+ new TextAreaTag(context, "confirmationHeader", listingSettings.getConfirmationHeader()).setId("confirmationHeader")
								.setStyle("width: 100%; height: 320px;") + "</td></tr>");

				sb.append("<tr><td>Confirmation page footer</td></tr>");
				sb.append("<tr><td>"
						+ new TextAreaTag(context, "confirmationFooter", listingSettings.getConfirmationFooter()).setId("confirmationFooter")
								.setStyle("width: 100%; height: 320px;") + "</td></tr>");

				sb.append("</table>");
			}
		});
		return doc;
	}

	public Object save() throws ServletException {

		if (!Module.Listings.enabled(context)) {
			return index();
		}

		listingSettings.setCategorySelectionStyle(categorySelectionStyle);
		listingSettings.setExpiryReminderDays(StringHelper.explodeIntegers(expiryReminderDays, "\\D"));
        listingSettings.setExpiryReminderBody(expiryReminderBody);
		listingSettings.setNotificationEmails(StringHelper.explodeStrings(notificationEmails, "\n"));
		listingSettings.setNotificationBody(notificationBody);

		listingSettings.setConfirmationFooter(confirmationFooter);
		listingSettings.setConfirmationHeader(confirmationHeader);
        listingSettings.setChoosePackageText(choosePackageText);

		listingSettings.save();

		addMessage("Listing settings updated");
		clearParameters();
		return main();
	}
}
