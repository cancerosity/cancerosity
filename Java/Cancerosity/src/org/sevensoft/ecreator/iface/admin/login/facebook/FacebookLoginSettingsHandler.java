package org.sevensoft.ecreator.iface.admin.login.facebook;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.model.login.facebook.FacebookLoginSettings;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import javax.servlet.ServletException;

/**
 * User: Tanya
 * Date: 01.02.2011
 */
@Path("admin-facebook-login.do")
public class FacebookLoginSettingsHandler extends AdminHandler {

    private transient FacebookLoginSettings facebookLoginSettings;

    private String appId;
    private ItemType registerAccountType;

    private Attribute realNameAttribute;
    private Attribute genderAttribute;
    private Attribute birthdayAttribute;
    private Attribute locationAttribute;
    private Attribute phoneAttribute;
    private Attribute anniversaryAttribute;
    private Attribute aboutMeAttribute;
    private Attribute imageAttribute;

    public FacebookLoginSettingsHandler(RequestContext context) {
        super(context);
        this.facebookLoginSettings = FacebookLoginSettings.getInstance(context);
    }

    public Object main() throws ServletException {
        AdminDoc doc = new AdminDoc(context, user, "Facebook Login Settings", null);
        doc.addBody(new Body() {

            @Override
            public String toString() {
                sb.append(new FormTag(FacebookLoginSettingsHandler.class, "save", "POST"));
                sb.append(new HiddenTag("facebookLoginSettings", facebookLoginSettings));

                commands();

                general();

                attributes();

                commands();

                sb.append("</form>");

                return sb.toString();
            }

            private void commands() {
                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update"));
                sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return"));

                sb.append("</div>");
            }

            private void general() {
                sb.append(new AdminTable("General"));

                sb.append(new AdminRow("Application ID", "Firstly you�ve to setup a facebook application to get the application id. You can " + new LinkTag("http://developers.facebook.com/setup/", "setup application from here.").setTarget("_blank"),
                        new TextTag(context, "appId", facebookLoginSettings.getAppId())));

                sb.append(new AdminRow("Login button code", "You can copy this value and paste where you need ",
                        new TextAreaTag(context, null, facebookLoginSettings.getLoginButtonCode(), 60, 5)));

                SelectTag accountTypes = new SelectTag(context, "registerAccountType", facebookLoginSettings.getRegisterAccountType());
                accountTypes.addOptions(ItemType.getAccounts(context));
                accountTypes.setAny("--Choose--");

                sb.append(new AdminRow("Account Item Type", "Choose an account type to register users with",
                        accountTypes));

                sb.append("<table>");
            }

            private void attributes() {
                sb.append(new AdminTable("Registration data"));

                SelectTag name = new SelectTag(context, "realNameAttribute", facebookLoginSettings.getRealNameAttribute());
                name.addOptions(Attribute.getSelectionMap(context));
                name.setAny("--Choose--");
                sb.append(new AdminRow("Real Name", "Choose real name attribute if it's necessary", name));

                SelectTag gender = new SelectTag(context, "genderAttribute", facebookLoginSettings.getGenderAttribute());
                gender.addOptions(Attribute.getSelectionMap(context));
                gender.setAny("--Choose--");
                sb.append(new AdminRow("Gender", "Choose gender attribute if it's necessary", gender));

                SelectTag birthday = new SelectTag(context, "birthdayAttribute", facebookLoginSettings.getBirthdayAttribute());
                birthday.addOptions(Attribute.get(context, AttributeType.Date));
                birthday.setAny("--Choose--");
                sb.append(new AdminRow("Birthday", "Choose birthday attribute if it's necessary", birthday));

                SelectTag location = new SelectTag(context, "locationAttribute", facebookLoginSettings.getLocationAttribute());
                location.addOptions(Attribute.get(context));
                location.setAny("--Choose--");
                sb.append(new AdminRow("Location", "Choose location attribute if it's necessary", location));

                SelectTag phone = new SelectTag(context, "phoneAttribute", facebookLoginSettings.getPhoneAttribute());
                phone.addOptions(Attribute.get(context));
                phone.setAny("--Choose--");
                sb.append(new AdminRow("Phone", "Choose phone attribute if it's necessary", phone));

                SelectTag anniversary = new SelectTag(context, "anniversaryAttribute", facebookLoginSettings.getAnniversaryAttribute());
                anniversary.addOptions(Attribute.get(context, AttributeType.Date));
                anniversary.setAny("--Choose--");
                sb.append(new AdminRow("Anniversary", "Choose anniversary attribute if it's necessary", anniversary));

                SelectTag aboutMe = new SelectTag(context, "aboutMeAttribute", facebookLoginSettings.getAboutMeAttribute());
                aboutMe.addOptions(Attribute.get(context, AttributeType.Text));
                aboutMe.setAny("--Choose--");
                sb.append(new AdminRow("About Me", "Choose About Me attribute if it's necessary", aboutMe));

                SelectTag image = new SelectTag(context, "imageAttribute", facebookLoginSettings.getImageAttribute());
                image.addOptions(Attribute.get(context, AttributeType.Image));
                image.setAny("--Choose--");
                sb.append(new AdminRow("Profile Image", "Choose Image attribute if it's necessary", image));

                sb.append("<table>");
            }


        });
        return doc;
    }

    public Object save() throws ServletException {
        facebookLoginSettings.setAppId(appId);
        facebookLoginSettings.setRegisterAccountType(registerAccountType);

        facebookLoginSettings.setRealNameAttribute(realNameAttribute);
        facebookLoginSettings.setGenderAttribute(genderAttribute);
        facebookLoginSettings.setBirthdayAttribute(birthdayAttribute);
        facebookLoginSettings.setLocationAttribute(locationAttribute);
        facebookLoginSettings.setPhoneAttribute(phoneAttribute);
        facebookLoginSettings.setAnniversaryAttribute(anniversaryAttribute);
        facebookLoginSettings.setAboutMeAttribute(aboutMeAttribute);
        facebookLoginSettings.setImageAttribute(imageAttribute);

        facebookLoginSettings.save();
        addMessage("Facebook Login settings have been updated");
        return main();
    }
}
