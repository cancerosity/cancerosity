package org.sevensoft.ecreator.iface.admin.stats.panels;

import java.util.List;

import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.admin.stats.ClickPathHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.stats.visits.Session;
import org.sevensoft.ecreator.model.stats.visits.Visitor;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 10 Apr 2007 12:24:54
 * 
 * Shows current sessions
 *
 */
public class SessionsPanel extends Panel {

	private List<Session>	sessions;

	public SessionsPanel(RequestContext context) {
		super(context);
		this.sessions = Session.getRecent(context, 0, 20);
	}

	@Override
	public void makeString() {

		sb.append(new AdminTable("Sessions").setStyle("position: relative"));

		sb.append("<tr>");
		sb.append("<th>Time</th>");
		sb.append("<th>Duration</th>");
		sb.append("<th>User</th>");
		sb.append("<th width='10'></th>");
		sb.append("<th width='10'></th>");
		sb.append("<th>Visits</th>");
		sb.append("<th>Pages</th>");
		sb.append("<th>Current page (exit page)</th>");
		sb.append("</tr>");

		for (Session session : sessions) {

			Visitor visitor = session.getVisitor();

			sb.append("<tr>");
			sb.append("<td>" + new DateTime(session.getLastAccess()).toString("MMM-dd HH:mm") + "</td>");
			sb.append("<td>" + session.getDurationString() + "</td>");
			sb.append("<td>" + visitor.getIpAddress() + "</td>");

			// country graphics
			sb.append("<td width='10'>");
			Country country = visitor.getCountry();
			if (country != null) {
				sb.append(new ImageTag("files/graphics/flags/" + country.getIsoAlpha2().toLowerCase() + ".gif"));
			}
			sb.append("</td>");

			// online / offline graphic
			sb.append("<td width='10'>");

			ImageTag imageTag;

			if (visitor.isOnline()) {
				imageTag = new ImageTag("files/graphics/admin/visitor_online.gif");
			} else {
				imageTag = new ImageTag("files/graphics/admin/visitor_offline.gif");
			}

			imageTag.setOnMouseOver("document.getElementById('visitorProfile" + visitor.getId() + "').style.display='block';");
			imageTag.setOnMouseOut("document.getElementById('visitorProfile" + visitor.getId() + "').style.display='none';");

			LinkTag linkTag = new LinkTag(ClickPathHandler.class, null, imageTag, "visitor", visitor);
			sb.append(linkTag);

			// generate visitor profile div
			visitorDiv(visitor);

			sb.append("</td>");

			sb.append("<td>" + visitor.getVisits() + "</td>");
			sb.append("<td>" + session.getViews() + " / " + visitor.getTotalViews() + "</td>");

			sb.append("<td>");
			if (session.hasExitPageName()) {
				sb.append("<div class='pagename'>" + session.getExitPageName() + "</div>");
			}
			sb.append("<div class='pageurl'>" + new LinkTag(session.getExitPageUrl()) + "</div>");
			sb.append("</td>");

			sb.append("</tr>");
		}

		sb.append("</table>");
	}

	private void visitorDiv(Visitor visitor) {

		sb.append("<table id='visitorProfile" + visitor.getId() + "' class='visitors_profile' cellspacing='0'>");
		sb.append("<tr><td>ID Address</td><td>" + visitor.getIpAddress() + "</td></tr>");
		sb.append("<tr><td>Session ID</td><td>" + visitor.getSessionId() + "</td></tr>");
		sb.append("<tr><td>Session duration</td><td>" + visitor.getSessionDurationString() + "</td></tr>");
		sb.append("<tr><td>Session views</td><td>" + visitor.getSessionViews() + "</td></tr>");
		sb.append("<tr><td>Total duration</td><td>" + visitor.getTotalDurationString() + "</td></tr>");
		sb.append("<tr><td>First access</td><td>" + new DateTime(visitor.getFirstAccess()).toString("MMM-yy HH:mm") + "</td></tr>");
		sb.append("<tr><td>Last access</td><td>" + new DateTime(visitor.getLastAccess()).toString("MMM-yy HH:mm") + "</td></tr>");

		if (visitor.hasCountry()) {
			sb.append("<tr><td>Country</td><td>" + visitor.getCountry() + "</td></tr>");
		}

		sb.append("<tr><td>Total views</td><td>" + visitor.getTotalViews() + "</td></tr>");
		sb.append("</table>\n\n");
	}
}
