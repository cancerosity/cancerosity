package org.sevensoft.ecreator.iface.admin.search.box;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.iface.admin.search.SearchFormHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.search.forms.SearchForm;
import org.sevensoft.ecreator.model.search.forms.boxes.ItemSearchBox;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;

/**
 * @author sks 13 Sep 2006 15:13:20
 *
 */
@Path("admin-boxes-search.do")
public class ItemSearchBoxHandler extends BoxHandler {

	private ItemSearchBox	box;
	private boolean	searchHeader;
	private boolean	autoSubmit;

	public ItemSearchBoxHandler(RequestContext context) {
		super(context);
	}

	public ItemSearchBoxHandler(RequestContext context, ItemSearchBox searchBox) {
		super(context);
		box = searchBox;
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {

		if (miscSettings.isAdvancedMode()) {
			box.setAutoSubmit(autoSubmit);
		}

		//		box.setSearchHeader(searchHeader);
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Search box"));

		//		if (Module.SearchHeader.enabled(context))
		//			sb.append("<tr><th width='300'><b>Search header</b></th><td>" + new BooleanRadioTag(context, "searchHeader", box.isSearchHeader())
		//					+ "</td></tr>");

		for (SearchForm form : box.getSearchForms()) {

			sb.append(new AdminRow("Search form", "This form is set to search: " + form.getName(), new ButtonTag(SearchFormHandler.class, null, "Edit",
					"form", form)
					+ " " + new ButtonTag(SearchFormHandler.class, "delete", "Delete", "form", form)));
		}

		if (miscSettings.isAdvancedMode()) {

			sb.append(new AdminRow("Auto submit", "Automatically submit this search box as soon as it is displayed", new BooleanRadioTag(context,
					"autoSubmit", box.isAutoSubmit())));
		}

		sb.append(new AdminRow("Add search form", "Click here to add a new search form", new ButtonTag(SearchFormHandler.class, "create",
				"Add search form", "box", box)));

		sb.append("</table>");
	}
}
