package org.sevensoft.ecreator.iface.admin.extras.newsfeed;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.extras.rss.NewsfeedBlock;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 26 Jun 2006 17:32:16
 *
 */
@Path("admin-newsfeed-block.do")
public class NewsfeedBlockHandler extends BlockEditHandler {

	private NewsfeedBlock	block;
	private int cols;

	public NewsfeedBlockHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Block getBlock() {
		return block;
	}

	@Override
	protected String getTitle() {
		return "Edit newsfeed";
	}

	@Override
	protected void saveSpecific() {
		block.setCols(cols);
	}

	@Override
	public void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Newsfeed settings"));
		sb.append(new AdminRow("Cols", "Set the number of columns for displaying this newsfeed", new TextTag(context, "cols", block.getCols(), 4)));
		sb.append("</table>");
	}

}
