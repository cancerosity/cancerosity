package org.sevensoft.ecreator.iface.admin.items;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.superstrings.comparators.EnumStringComparator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.containers.blocks.panels.BlocksListPanel;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemTypeMenu;
import org.sevensoft.ecreator.iface.admin.items.modules.links.ExternalLinkHandler;
import org.sevensoft.ecreator.iface.admin.items.modules.ratings.RatingTypeHandler;
import org.sevensoft.ecreator.iface.admin.items.modules.relations.RelationGroupHandler;
import org.sevensoft.ecreator.iface.admin.misc.html.EditMarkupButton;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.accounts.permissions.DomainUtil;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionCollections;
import org.sevensoft.ecreator.model.accounts.permissions.PermissionType;
import org.sevensoft.ecreator.model.accounts.permissions.panels.PermissionsPanel;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.OwnerAttributesPanel;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.crm.forum.Forum;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupTag;
import org.sevensoft.ecreator.model.design.template.Template;
import org.sevensoft.ecreator.model.design.template.TemplateConfig;
import org.sevensoft.ecreator.model.extras.links.ExternalLink;
import org.sevensoft.ecreator.model.extras.links.LinkType;
import org.sevensoft.ecreator.model.extras.ratings.RatingModule;
import org.sevensoft.ecreator.model.extras.ratings.RatingType;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.ItemTypePreset;
import org.sevensoft.ecreator.model.items.renderers.OwnerOptionsPanel;
import org.sevensoft.ecreator.model.items.adminsearch.AdminField;
import org.sevensoft.ecreator.model.items.adminsearch.AdminFieldType;
import org.sevensoft.ecreator.model.items.options.OptionsModule;
import org.sevensoft.ecreator.model.items.pricing.PriceSettings.PriceRounding;
import org.sevensoft.ecreator.model.items.relations.RelationGroup;
import org.sevensoft.ecreator.model.items.sorts.AdminSortsRenderer;
import org.sevensoft.ecreator.model.items.sorts.ItemSort;
import org.sevensoft.ecreator.model.items.stock.StockModule.StockCalculationSystem;
import org.sevensoft.ecreator.model.items.stock.StockModule.StockControl;
import org.sevensoft.ecreator.model.media.images.ImageType;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;
import java.util.*;

/**
 * @author sam
 */
@Path("admin-items-types.do")
public class ItemTypeHandler extends AdminHandler {

    private ItemType itemType;
    private String name;
    private String newAttributes;
    private boolean images;
    private boolean costPricing;
    private boolean rrp;
    private double defaultVatRate;
    private String leadtimeOverride;
    private String nameCaption;
    private boolean vat;
    private String attachmentsCaption;
    private ItemTypePreset preset;
    private boolean messages;
    private String defaultLeadtime;
    private boolean summaries;
    private boolean content;
    private boolean blockEmailsInContent;
    private String initialTopicContent;
    private Forum forum;
    private boolean enablePermissions;
    private String namePlural;
    private String inStockIcon;
    private String outStockIcon;
    private boolean defaultBackorders;
    private String imageHolder;
    private StockControl stockControl;
    private String stockMessage;
    private int resetStock;
    private int roundTo;
    private PriceRounding priceRounding;
    private boolean overrideTags;
    private int maxCategories;
    private boolean categories;
    private boolean featured;
    private Category defaultCategory;
    private Template template;
    private boolean promotionPrices;
    private String resetLeadtime;
    private boolean backorders;
    private boolean resetBackorders;
    private String zeroStockNotificationEmails;
    private ItemModule addModule;
    private ItemModule module;
    private int defaultMaxBids;
    private int defaultAuctionLength;
    private int maxQtyPerMember;
    private String bidConfirmedText;
    private int maxBidsPerSession;
    private boolean statusControl;
    private StockCalculationSystem stockSystem;
    private String addRatingTypeName;
    private int ratingScale;
    private Map<Integer, String> ratingGraphics;
    private boolean reviews;
    private String winnerBody;
    private Money defaultBidFee;
    private String losersBody;
    private boolean references;
    private Markup listMarkup;
    private Markup viewMarkup;
    private String addBlock;
    private String defaultContent;
    private ItemSort sort;
    private String featuredCaption;
    private String referencesCaption;
    private boolean shortNames;
    private Markup accessoriesMarkup;
    private String accessoriesBlurb;
    private ExternalLink externalLink;
    private String stockDisplayRangesString;
    private LinkType linkType;
    private String emails;
    private boolean hidden;
    private boolean displayExVat;
    private boolean owners;
    private boolean simpleEditor;
    private Markup printerMarkup;
    private MultiValueMap<String, PermissionType> permissions;
    private String restrictionForwardUrl;
    private boolean prioritised;
    private boolean moderateImages;
    private AdminFieldType fieldType;
    private String footer;
    private String header;
    private String newAttributesSection;
    private int newAttributesPage;
    private String newImageType;
    private boolean collectionCharges;
    private boolean bookable;
    private boolean noForum;
    private boolean assignCodeToOption;
    private Attribute productCodeAttribute;
    private String acceptListingEmailContent;
    private String rejectListingEmailContent;
    private String addListingEmailContent;
    private String newOptions;
    private Markup searchListMarkup;
    private boolean mobilePhoneDisable;
    private boolean hiddenCs;

    public ItemTypeHandler(RequestContext context) {
        super(context);
    }

    public ItemTypeHandler(RequestContext context, ItemType type) {
        super(context);
        this.itemType = type;
    }

    public Object add() throws ServletException {

        if (name != null) {
            new ItemType(context, name.trim());
        }

        if (preset != null) {
            preset.create(context);
        }

        addMessage("Item type added");
        clearParameters();
        return main();
    }

    public Object addModule() throws ServletException {

        if (module == null || itemType == null) {
            return edit();
        }

        itemType.addModule(module);
        itemType.save();

        itemType.log("Added module: " + module);

        return edit();
    }

    public Object addSort() throws ServletException {

        if (itemType == null) {
            return main();
        }

        itemType.addSort();

        clearParameters();
        return sorts();
    }

    public Object copy() throws ServletException {

        if (itemType == null)
            return main();

        try {

            itemType = itemType.clone();

            clearParameters();
            addMessage("Item type copied");
            return edit();

        } catch (CloneNotSupportedException e) {

            addError(e);
            return main();
        }

    }

    public Object delete() throws ServletException {

        if (itemType == null) {
            return main();
        }

        itemType.delete();
        return main();
    }

    public Object edit() throws ServletException {

        if (itemType == null) {
            return main();
        }

        if (!isSuperman() && !Module.UserItemTypes.enabled(context)) {
            return new DashboardHandler(context).main();
        }

        AdminDoc doc = new AdminDoc(context, user, "Edit item type: " + itemType.getName(), Tab.getItemTab(itemType));
        doc.setMenu(new EditItemTypeMenu(itemType));
        doc.addBody(new Body() {

            private void listings() {
                sb.append(new AdminTable("Listing"));

                sb.append(new AdminRow("Accept listings email content",
                        "The content of the email that is sent on listing approval.<br/><br/>Special tags:<br/>"
                                + "<i>[name]</i> - Members name<br/>" + "<i>[email]</i> - Members email<br/>"
                                + "<i>[id]</i> - Generated id<br/>" + "<i>[login]</i> - Url to login page<br/>"
                                + "<i>[loginurl]</i> - Login url<br/>" + "<i>[site]</i> - Your Company name<br/>",

                        new TextAreaTag(context, "acceptListingEmailContent", itemType.getAcceptListingEmailContent(), 80, 10)));

                sb.append(new AdminRow("Reject listings email content",
                        "The content of the email that is sent on listing rejection.<br/><br/>Special tags:<br/>"
                                + "<i>[name]</i> - Members name<br/>" + "<i>[email]</i> - Members email<br/>"
                                + "<i>[id]</i> - Generated id<br/>" + "<i>[login]</i> - Url to login page<br/>"
                                + "<i>[loginurl]</i> - Login url<br/>" + "<i>[site]</i> - Your Company name<br/>",

                        new TextAreaTag(context, "rejectListingEmailContent", itemType.getRejectListingEmailContent(), 80, 10)));

                sb.append(new AdminRow("Add listings email content",
                        "The content of the email that is sent on listing adding.<br/><br/>Special tags:<br/>"
                                + "<i>[name]</i> - Members name<br/>" + "<i>[email]</i> - Members email<br/>"
                                + "<i>[id]</i> - Generated id<br/>" + "<i>[login]</i> - Url to login page<br/>"
                                + "<i>[loginurl]</i> - Login url<br/>" + "<i>[site]</i> - Your Company name<br/>",

                        new TextAreaTag(context, "addListingEmailContent", itemType.getAddListingEmailContent(), 80, 10)));

                sb.append("</table>");
            }

            private void accessories() {

                sb.append(new AdminTable("Accessories"));

                SelectTag markupTag = new SelectTag(context, "accessoriesMarkup", itemType.getAccessoriesMarkup(), Markup.get(context), "-Recreate-");
                markupTag.setId("accessoriesMarkup");
                sb.append(new AdminRow(true, "Accessories markup", null, markupTag + " " + new EditMarkupButton("accessoriesMarkup")));

                sb.append(new AdminRow(true, "Accessories blurb", "Rename the intro text that appears above accessories", new TextTag(context,
                        "accessoriesBlurb", itemType.getAccessoriesBlurb(), 40)));

                sb.append("</table>");
            }

            private void adminfields() {

                sb.append(new AdminTable("Admin fields"));
                sb.append("<tr><td colspan='3'>These are the fields that appear in search results when searching for items of this type.</td></tr>");

                sb.append("<tr>");
                sb.append("<th width='60'>Position</th>");
                sb.append("<th>Field type</th>");
                sb.append("<th width='10'> </th>");
                sb.append("</tr>");

                PositionRender pr = new PositionRender(AdminFieldHandler.class, "field");

                for (AdminField field : AdminField.get(context, itemType)) {

                    sb.append("<tr>");
                    sb.append("<td width='60'>" + pr.render(field) + "</td>");
                    sb.append("<td>" + new LinkTag(AdminFieldHandler.class, null, new SpannerGif(), "field", field) + " " + field.getFieldType() +
                            "</td>");
                    sb.append("<td width='10'>" + new LinkTag(AdminFieldHandler.class, "delete", new DeleteGif(), "field", field) + "</td>");
                    sb.append("</tr>");
                }

                sb.append("<tr><td colspan='3'>Add new field type: " +
                        new SelectTag(context, "fieldType", null, AdminFieldType.values(), "-Select field type-") + " " + new SubmitTag("Add") +
                        "</td></tr>");

                sb.append("</table>");
            }

            private void bookingfields() {

                sb.append(new AdminTable("Booking"));

                sb.append(new AdminRow("Enable booking", "Enable booking of items of this type", new BooleanRadioTag(context, "bookable", itemType.isBookable())));

                sb.append("</table>");
            }

            private void advanced() {

                sb.append(new AdminTable("Advanced"));

                sb.append(new AdminRow("No forum", "Forum topic will not be created when 'Forum' module added", new BooleanRadioTag(context, "noForum", itemType.isNoForum())));

                sb.append(new AdminRow("Status control", null, new BooleanRadioTag(context, "statusControl", itemType.isStatusControl())));

                sb
                        .append(new AdminRow("Owners", "Allow these items to be tagged to users", new BooleanRadioTag(context, "owners", itemType
                                .isOwners())));

                sb.append(new AdminRow("Featured", "Allow items to be set to featured for use in highlighted item blocks.", new BooleanRadioTag(
                        context, "featured", itemType.isFeatured())));

                if (itemType.isFeatured()) {
                    sb.append(new AdminRow("Featured caption", null, new TextTag(context, "featuredCaption", itemType.getFeaturedCaption())));
                }

                sb.append(new AdminRow("Prioritised", "Allow specific items to appear first when browsing categories, regardless of sort used.",
                        new BooleanRadioTag(context, "prioritised", itemType.isPrioritised())));

                if (itemType.isPrioritised()) {
                    sb
                            .append(new AdminRow("Prioritised caption", null, new TextTag(context, "prioritisedCaption", itemType
                                    .getPrioritisedCaption())));
                }

                sb.append(new AdminRow("Creation emails", "Enter emails to be notified when a new item is created.", new TextAreaTag(context, "emails",
                        StringHelper.implode(itemType.getEmails(), "\n"), 40, 3)));

                if (itemType.isAccount(context)){
                     sb.append(new AdminRow("Mobile phone Disable", "Disable phones on Accounts whether no SMS and Orders on the site.",
                        new BooleanRadioTag(context, "mobilePhoneDisable", itemType.isMobilePhoneDisable())));
                }

                sb.append(new AdminRow(
                                "Hidden",
                                "When an item type is set to hidden then it cannot be accessed from the front end. IE, it becomes an admin item type only.",
                                new BooleanRadioTag(context, "hidden", itemType.isHidden())));

                 sb.append(new AdminRow("Hidden for Content Searcher",
                                "When an item type is set to hidden then it cannot be searched via Content Servcher cs.do.",
                                new BooleanRadioTag(context, "hiddenCs", itemType.isHiddenCs())));

                sb.append("</table>");
            }

            private void attachments() {

                sb.append(new AdminTable("Attachments"));

                sb.append(new AdminRow("Attachments caption", "Rename attachments.", new TextTag(context, "attachmentsCaption", itemType
                        .getAttachmentsCaption(), 40)));

                sb.append("</table>");
            }

            private void categories() {

                sb.append(new AdminTable("Category options"));

                SelectTag tag = new SelectTag(context, "defaultCategory", itemType.getDefaultCategory());
                tag.setAny("-No default category-");
                tag.addOptions(Category.getCategoryOptions(context, null, 0, 200));

                sb.append(new AdminRow("Default category", "Any new item will be auto placed in this category.", tag));

                sb.append(new AdminRow("Max categories", "Limit the max number of categories these items can be put in", new TextTag(context,
                        "maxCategories", itemType.getMaxCategories(), 4)));

                sb.append("</table>");
            }

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update " + itemType.getNameLower()));
                sb.append(new ButtonTag(ItemTypeHandler.class, "copy", "Copy " + itemType.getNameLower(), "itemType", itemType));
                sb.append(new ButtonTag(ItemTypeHandler.class, null, "Return to item types menu"));

                sb.append("</div>");
            }

            private void content() {

                sb.append(new AdminTable("Content options"));

                sb.append(new AdminRow("Simple editor", "Use the simple (non-HTML) editor when editing content.", new BooleanRadioTag(context,
                        "simpleEditor", itemType.isSimpleEditor())));

                sb.append(new AdminRow("Default content", "This content will automatically be added to an item when it is first created",
                        new TextAreaTag(context, "defaultContent", itemType.getDefaultContent(), 40, 3)));

                sb
                        .append(new AdminRow(
                                "Summaries",
                                "Allow users to enter their own summary for these items, rather than just using the first so many characters of the content",
                                new BooleanRadioTag(context, "summaries", itemType.isSummaries())));

                if (Module.Listings.enabled(context)) {
                    sb.append(new AdminRow("Block emails in listings",
                            "Stops users from entering email addresses in content of listings of this item type.", new BooleanRadioTag(context,
                            "blockEmailsInContent", itemType.isBlockEmailsInContent())));
                }

                sb.append("</table>");

            }

            private void exlinks() {

                sb.append(new AdminTable("External links"));

                sb.append("<tr><td colspan='3'>These are the special links made from " + itemType.getNamePluralLower() +
                        " when you enter an address or postcode</td></tr>");

                sb.append("<tr>");
                sb.append("<th width='80'>Position</th>");
                sb.append("<th>Link type</th>");
                sb.append("<th width='10'> </th>");
                sb.append("</tr>");

                PositionRender pr = new PositionRender(ExternalLinkHandler.class, "externalLink");
                for (ExternalLink externalLink : itemType.getExternalLinks()) {

                    sb.append("<tr>");
                    sb.append("<td width='80'>" + pr.render(externalLink) + "</td>");
                    sb.append("<td>" + new LinkTag(ExternalLinkHandler.class, null, new SpannerGif(), "externalLink", externalLink) + " " +
                            externalLink.getLinkType() + "</td>");
                    sb.append("<td width='10'>" +
                            new LinkTag(ItemTypeHandler.class, "removeExternalLink", new DeleteGif(), "externalLink", externalLink, "itemType",
                                    itemType) + "</td>");
                    sb.append("</tr>");
                }

                Collection<LinkType> links = new TreeSet(new EnumStringComparator());
                links.addAll(Arrays.asList(LinkType.values()));

                SelectTag linkTag = new SelectTag(context, "linkType", null, links, "-Select link to add-");
                sb.append("<tr><td colspan='3'>Add external link: " + linkTag + " " + new SubmitTag("Add") + "</td></tr>");

                sb.append("</table>");
            }

            private void forum() {

                sb.append(new AdminTable("Forum and messages"));

                sb.append("<tr><th width='300'><b>Messages</b><br/>" +
                        "Allow posting of messages directly onto this item as a virtual topic in the forums.</th><td>" +
                        new BooleanRadioTag(context, "messages", itemType.isMessages()) + "</td></tr>");

                SelectTag tag = new SelectTag(context, "forum", itemType.getForum());
                tag.setAny("-Create on next new topic-");
                tag.addOptions(Forum.get(context));

                sb.append(new AdminRow("Forum", "The forum where topics for these items will appear.", tag));

                sb.append(new AdminRow("Initial topic content",
                        "Any text entered here will be added to a new item created by a member as an initial post.", new TextAreaTag(context,
                        "initialTopicContent", itemType.getInitialTopicContent(), 50, 3)));

                sb.append("</table>");
            }

            private void general() {

                sb.append(new AdminTable("General"));

                sb.append(new AdminRow("Name", "Enter the name of this item type, Eg Product, Event or Property.", new TextTag(context, "name",
                        itemType.getName(), 40)));

                sb.append(new AdminRow("Name plural", "Leave blank for auto.", new TextTag(context, "namePlural", itemType.getNamePlural(), 40)));

                sb.append(new AdminRow("Name caption", null, new TextTag(context, "nameCaption", itemType.getNameCaption(), 40)));

                sb.append(new AdminRow("Short names", "Enable a new field to enter a shortened version of the name.", new BooleanRadioTag(context,
                        "shortNames", itemType.isShortNames())));

                sb.append(new AdminRow("Override tags", "Allow users to override meta tags for these items", new BooleanRadioTag(context,
                        "overrideTags", itemType.isOverrideTags())));

                sb.append("</table>");
            }

            private void headerfooter() {

                sb.append(new AdminTable("Header / Footer"));

                sb.append(new AdminRow("Header", "This content will be added at the top of each item of this type.", new TextAreaTag(context, "header",
                        itemType.getHeader(), 60, 6)));

                sb.append(new AdminRow("Footer", "This content will be added at the bottom of each item of this type.", new TextAreaTag(context,
                        "footer", itemType.getFooter(), 60, 6)));

                sb.append("</table>");
            }

            private void images() {

                sb.append(new AdminTable("Images"));

                sb.append(new AdminRow("Moderate images", "Images added by users should be moderated.", new BooleanRadioTag(context, "moderateImages",
                        itemType.isModerateImages())));

                sb.append(new AdminRow("Image holder", "Use this place holder image for when items of this item type do not have an image set",
                        new TextTag(context, "imageHolder", itemType.getImageHolder(), 20)));

                sb.append("</table>");

                sb.append(new AdminTable("Image types"));

                sb.append("<tr>");
                sb.append("<th width='80'>Name</th>");
                sb.append("<th>Name</th>");
                sb.append("<th width='10'> </th>");
                sb.append("</tr>");

                PositionRender pr = new PositionRender(ImageTypeHandler.class, "imageType");
                for (ImageType imageType : itemType.getMediaModule().getImageTypes()) {

                    sb.append("<tr>");
                    sb.append("<td width='80'>" + pr.render(imageType) + "</td>");
                    sb.append("<td>" + new LinkTag(ImageTypeHandler.class, null, new SpannerGif(), "imageType", imageType) + " " +
                            imageType.getName() + "</td>");
                    sb.append("<td width='10'>" + new LinkTag(ImageTypeHandler.class, "delete", new DeleteGif(), "imageType", imageType) + "</td>");
                    sb.append("</tr>");
                }

                sb
                        .append("<tr><td colspan='3'>Add image type: " + new TextTag(context, "newImageType", 20) + " " + new SubmitTag("Add") +
                                "</td></tr>");

                sb.append("</table>");
            }

            private void markup() {

                sb.append(new AdminTable("Template and markup"));

                List<Template> templates = Template.get(context);
                if (templates.size() > 1) {
                    TemplateConfig config = itemType.getTemplateConfig();
                    SelectTag templateTag = new SelectTag(context, "template");
                    if (config != null) {
                        templateTag.setValue(config.getTemplate());
                    }
                    templateTag.setAny("-Default-");
                    templateTag.addOptions(templates);
                    sb.append(new AdminRow(true, "Template", null, templateTag));
                }

                SelectTag listMarkupTag = new SelectTag(context, "listMarkup", itemType.getListMarkup());
                listMarkupTag.addOptions(Markup.get(context));
                listMarkupTag.setId("listMarkup");
                sb.append(new AdminRow(true, "List markup", "This markup is used when an item is displayed in search results or in a category",
                        listMarkupTag + " " + new EditMarkupButton("listMarkup")));

                SelectTag viewMarkupTag = new SelectTag(context, "viewMarkup", itemType.getViewMarkup());
                viewMarkupTag.addOptions(Markup.get(context));
                viewMarkupTag.setId("viewMarkup");
                sb.append(new AdminRow(true, "View markup", "This markup is used when viewing the full details of an item.", viewMarkupTag + " " +
                        new EditMarkupButton("viewMarkup")));

                sb.append(new AdminRow(true, "Printer markup", "This markup is used for print friendly views", new MarkupTag(context, "printerMarkup",
                        itemType.getPrinterMarkup(), null, "printerMarkup")));

                SelectTag resultsListMarkupTag = new SelectTag(context, "searchListMarkup", itemType.getSearchListMarkup());
                resultsListMarkupTag.addOptions(Markup.get(context));
                resultsListMarkupTag.setId("searchListMarkup");
//                resultsListMarkupTag.setAny("--Recreate--");
                sb.append(new AdminRow(true, "Search results list markup", "This markup is used to display search result for custom search forms (created with merkers)",
                        resultsListMarkupTag + " " + new EditMarkupButton("searchListMarkup")));

                sb.append("</table>");

            }

            private void modules() {

                sb.append(new AdminTable("Modules"));

                for (ItemModule module : itemType.getModules()) {

                    sb.append("<tr>");
                    sb.append("<td>" + module + "</td>");

                    String description = module.getDescription();
                    sb.append("<td>" + (description == null ? "" : description) + "</td>");

                    sb
                            .append("<td width='10'>" +
                                    new LinkTag(ItemTypeHandler.class, "removeModule", new DeleteGif(), "module", module, "itemType",
                                            itemType) + "</td>");
                    sb.append("</tr>");
                }

                Set<ItemModule> modules = itemType.getUnusedModules();
                if (modules.size() > 0) {

                    SelectTag tag = new SelectTag(context, "module");
                    tag.setAny("-Choose module to add-");
                    tag.addOptions(modules);

                    sb.append(new FormTag(ItemTypeHandler.class, "addModule", "get"));
                    sb.append(new HiddenTag("itemType", itemType));
                    sb.append("<tr><td colspan='3'>Add module: " + tag + " " + new SubmitTag("Add module") + "</td></tr>");
                    sb.append("</form>");

                }

                sb.append("</table>");
            }

            private void options() {

                sb.append(new AdminTable("Options module"));

                sb.append(new AdminRow("Show ex vat pricing", "Show ex vat prices when the option has an increase / decrease in price.",
                        new BooleanRadioTag(context, "displayExVat", itemType.getOptionsModule().isDisplayExVat())));

                sb.append("</table>");
            }

            private void permissions() {

                sb.append(new PermissionsPanel(context, itemType, PermissionCollections.getItem()));
            }

            private void references() {

                sb.append(new AdminTable("References"));

                sb.append(new AdminRow("References", "The reference is a pointer to an external database's id / ref for an item. "
                        + "This reference value is used for synchronisation with external sources.", new BooleanRadioTag(context, "references",
                        itemType.isReferences())));

                sb.append(new AdminRow("References caption", "Rename the reference caption for this item type.", new TextTag(context,
                        "referencesCaption", itemType.getReferencesCaption(), 20)));

                sb.append("</table>");
            }

            private void relations() {

                sb.append(new AdminTable("Relations"));

                sb.append("<tr>");
                sb.append("<th>Name</th>");
                sb.append("<th width='10'> </th>");
                sb.append("</tr>");

                for (RelationGroup relationGroup : itemType.getRelationsModule().getGroups()) {

                    sb.append("<tr>");
                    sb.append("<td>" + new LinkTag(RelationGroupHandler.class, null, new SpannerGif(), "relationGroup", relationGroup) + " " +
                            relationGroup.getName() + "</td>");
                    sb.append("<td width='10'>" +
                            new LinkTag(RelationGroupHandler.class, "delete", new DeleteGif(), "relationGroup", relationGroup)
                                    .setConfirmation("Are you sure you want to remove this relation group?") + "</td>");
                    sb.append("</tr>");
                }

                sb.append("<tr><td colspan='2'>Add relation group " + new ButtonTag(RelationGroupHandler.class, "create", "Add", "itemType", itemType) +
                        "</td></tr>");

                sb.append("</table>");
            }

            @Override
            public String toString() {

                modules();

                sb.append(new FormTag(ItemTypeHandler.class, "save", "multi").setId("items_form"));
                sb.append(new HiddenTag("itemType", itemType));

                commands();
                general();
                exlinks();

                sb.append(new OwnerAttributesPanel(context, itemType, new Link(ItemTypeHandler.class, "edit", "itemType", itemType)));

                if (isSuperman() || Module.UserMarkup.enabled(context)) {
                    markup();
                }

                sb.append(new BlocksListPanel(context, itemType));

                if (ItemModule.Images.enabled(context, itemType)) {
                    images();
                }

                if (ItemModule.Attachments.enabled(context, itemType)) {
                    attachments();
                }

                if (ItemModule.Content.enabled(context, itemType)) {
                    content();
                }

                if (ItemModule.Categories.enabled(context, itemType)) {
                    categories();
                }

                references();

                if (ItemModule.HeadersFooters.enabled(context, itemType)) {
                    headerfooter();
                }

                if (Module.Permissions.enabled(context)) {
                    permissions();
                }

                if (ItemModule.Relations.enabled(context, itemType)) {
                    relations();
                }

                if (ItemModule.Accessories.enabled(context, itemType)) {
                    accessories();
                }

                if (ItemModule.Options.enabled(context, itemType)) {
                    options();
                }

                if (Module.Forum.enabled(context)) {
                    forum();
                }

                if (ItemModule.Listings.enabled(context, itemType)) {
                    listings();
                }

                adminfields();
                bookingfields();
                advanced();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    @Override
    public Object main() throws ServletException {

        if (!isSuperman() && !Module.UserItemTypes.enabled(context)) {
            return new DashboardHandler(context).main();
        }

        AdminDoc doc = new AdminDoc(context, user, "Item types", null);
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new AdminTable("Item types"));

                sb.append("<tr>");
                sb.append("<th>Type</th>");
                sb.append("<th width='20'></th>");
                sb.append("</tr>");

                for (ItemType type : ItemType.get(context)) {

                    sb.append("<tr>");
                    sb.append("<td>" +
                            new LinkTag(ItemTypeHandler.class, "edit", new ImageTag("files/graphics/admin/spanner.gif"), "itemType", type) + " " +
                            type.getName() + "</td>");
                    sb.append("<td width='20'>" +
                            new LinkTag(ItemTypeHandler.class, "delete", new DeleteGif(), "itemType", type)
                                    .setConfirmation("Are you sure you want to delete this item type?") + "</td>");
                    sb.append("</tr>");

                }

                SelectTag presetTag = new SelectTag(context, "preset");
                presetTag.addOptions(ItemTypePreset.values());
                presetTag.setLabelComparator();

                sb.append(new FormTag(ItemTypeHandler.class, "add", "post"));
                sb.append("<tr><td colspan='2'>Add preset item type: " + presetTag + " " + new SubmitTag("Add preset") + "</td></tr>");
                sb.append("</form>");

                sb.append(new FormTag(ItemTypeHandler.class, "add", "post"));
                sb.append("<tr><td colspan='2'>Add custom item type: " + new TextTag(context, "name", 20) + " " + new SubmitTag("Add custom") +
                        "</td></tr>");
                sb.append("</form>");

                sb.append("</table>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object options() throws ServletException {

        if (itemType == null) {
            return main();
        }

        if (!isSuperman() && !Module.UserItemTypes.enabled(context)) {
            return index();
        }

        AdminDoc page = new AdminDoc(context, user, "Edit item type: " + itemType.getName(), null);
        page.setMenu(new EditItemTypeMenu(itemType));
        page.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update item type"));
                sb.append(new ButtonTag(ItemTypeHandler.class, null, "Return to item types menu"));

                sb.append("</div>");
            }

            private void options() {

                sb.append(new AdminTable("Options"));

                sb.append(new AdminRow("Assign product codes", "Assign product codes to each individual option", new BooleanRadioTag(context, "assignCodeToOption", itemType.isAssignCodeToOption())));

                sb.append(new AdminRow("Product code attribute", "Select attribute which will be assigned to options as product code",
                        new SelectTag(context, "productCodeAttribute", itemType.getProductCodeAttribute(), itemType.getAttributes(), "-Not set-")));

                sb.append("</table>");

            }

            @Override
            public String toString() {

                sb.append(new FormTag(ItemTypeHandler.class, "saveOptions", "post"));
                sb.append(new HiddenTag("itemType", itemType));

                commands();
                options();

                sb.append(new OwnerOptionsPanel(context, itemType));

                commands();

                sb.append("</form>");

                return sb.toString();

            }

        });

        return page;

    }

    public Object pricing() throws ServletException {

        if (itemType == null) {
            return main();
        }

        if (!isSuperman() && !Module.UserItemTypes.enabled(context)) {
            return index();
        }

        AdminDoc page = new AdminDoc(context, user, "Edit item type: " + itemType.getName(), null);
        page.setMenu(new EditItemTypeMenu(itemType));
        page.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update item type"));
                sb.append(new ButtonTag(ItemTypeHandler.class, null, "Return to item types menu"));

                sb.append("</div>");
            }

            private void pricing() {

                sb.append(new AdminTable("Pricing"));

                sb.append(new AdminRow("Rrp", "Enable rrps prices", new BooleanRadioTag(context, "rrp", itemType.isRrp())));

                sb.append(new AdminRow("Cost Pricing",
                        "Enables use of cost prices and then sell prices can be set as a markup or margin from the cost price",
                        new BooleanRadioTag(context, "costPricing", itemType.isCostPricing())));

                //				sb.append(new AdminRow("Promotion prices", "By entering a promotion price, the item will be advertised as on special offer.",
                //						new BooleanRadioTag(context, "promotionPrices", itemType.isPromotionPrices())));

                Company company = Company.getInstance(context);

                sb.append(new AdminRow("Use VAT rates", "Have VAT apply to this item", company.isVatRegistered() ? new BooleanRadioTag(context, "vat",
                        itemType.isVat()) : "You must enter a VAT reg number in company details to use VAT rates"));

                sb.append(new AdminRow("Default VAT rate", "This is the vat rate initially applied to newly created items.", new TextTag(context,
                        "defaultVatRate", itemType.getDefaultVatRate(), 6)));

                SelectTag priceRoundingTag = new SelectTag(context, "priceRounding", itemType.getPriceRounding());
                priceRoundingTag.addOptions(PriceRounding.values());

                sb.append(new AdminRow("Price rounding", "Round prices off to a nearest value, such as a whole pound, or perhaps to .99",
                        priceRoundingTag));

                switch (itemType.getPriceRounding()) {

                    case None:
                        break;

                    case Down:
                    case Up:
                    case Nearest:
                        sb.append(new AdminRow("Round to nearest", null, new TextTag(context, "roundTo", itemType.getRoundTo(), 4)));
                }

                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(ItemTypeHandler.class, "savePricing", "post"));
                sb.append(new HiddenTag("itemType", itemType));

                commands();
                pricing();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return page;
    }

    public Object ratings() throws ServletException {

        if (itemType == null) {
            return main();
        }

        final RatingModule module = itemType.getRatingModule();

        AdminDoc page = new AdminDoc(context, user, "'" + itemType.getName() + "' - Ratings", null);
        page.setMenu(new EditItemTypeMenu(itemType));
        page.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update item type"));
                sb.append(new ButtonTag(ItemTypeHandler.class, null, "Return to item types menu"));

                sb.append("</div>");
            }

            private void graphics() {

                sb.append(new AdminTable("Graphics"));

                sb.append("<tr><th colspan='2'>All graphics are set relative to template-data</th></tr>");
                for (int n = 1; n <= module.getRatingScale(); n++) {

                    sb.append("<tr><th width='300'><b>Graphic for rating: " + n + "</b></th><td>" +
                            new TextTag(context, "ratingGraphics_" + n, module.getRatingGraphic(n), 40) + "</td></tr>");

                }

                sb.append("</table>");
            }

            private void options() {

                sb.append(new AdminTable("Options"));

                sb.append("<tr><th width='300'><b>Ratings scale</b><br/>" +
                        "Set the scale limit (eg, 5 for 1 to 5 starts) that ratings for this item type will use." + "</th><td>" +
                        new TextTag(context, "ratingScale", module.getRatingScale(), 4) + "</td></tr>");

                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(ItemTypeHandler.class, "saveRatings", "post"));
                sb.append(new HiddenTag("itemType", itemType));

                commands();
                types();
                options();
                graphics();

                commands();

                sb.append("</form>");

                return sb.toString();
            }

            private void types() {

                sb.append(new AdminTable("Rating types"));

                sb.append("<tr>");
                sb.append("<th>Name</th>");
                sb.append("<th width='60'>Remove</th>");
                sb.append("<th width='60'>Edit</th>");
                sb.append("</tr>");

                for (RatingType ratingType : module.getRatingTypes()) {

                    sb.append("<tr>");
                    sb.append("<td>" + ratingType.getName() + "</td>");
                    sb.append("<td width='60'>" + new ButtonTag(RatingTypeHandler.class, null, "Edit", "ratingType", ratingType) + "</td>");
                    sb.append("<td width='60'>" +
                            new ButtonTag(RatingTypeHandler.class, "delete", "Delete", "ratingType", ratingType)
                                    .setConfirmation("Are you sure you want to delete this rating type?") + "</td>");
                    sb.append("</tr>");
                }

                sb.append("<tr><th colspan='3'>" + new TextTag(context, "addRatingTypeName", 20) + "</th></tr>");
                sb.append("</table>");
            }

        });
        return page;
    }

    public Object removeExternalLink() throws ServletException {

        if (externalLink == null || itemType == null)
            return main();

        itemType.removeExternalLink(externalLink);
        itemType.save();

        return edit();
    }

    public Object removeModule() throws ServletException {

        if (module == null || itemType == null) {
            return main();
        }

        itemType.removeModule(module);
        itemType.save();

        itemType.log("Removed module: " + module);

        return edit();
    }

    public Object removeSort() throws ServletException {

        if (sort == null || itemType == null) {
            return main();
        }

        itemType.removeSort(sort);

        clearParameters();
        return sorts();
    }

    public Object reviews() throws ServletException {

        if (itemType == null) {
            return main();
        }

        final RatingModule module = itemType.getRatingModule();

        AdminDoc page = new AdminDoc(context, user, "'" + itemType.getName() + "' - Ratings", null);
        page.setMenu(new EditItemTypeMenu(itemType));
        page.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update item type"));
                sb.append(new ButtonTag(ItemTypeHandler.class, null, "Return to item types menu"));

                sb.append("</div>");
            }

            private void graphics() {

                sb.append(new AdminTable("Graphics"));

                sb.append("<tr><th colspan='2'>All graphics are set relative to template-data</th></tr>");
                for (int n = 1; n <= module.getRatingScale(); n++) {

                    sb.append("<tr><th width='300'><b>Graphic for rating: " + n + "</b></th><td>" +
                            new TextTag(context, "ratingGraphics_" + n, module.getRatingGraphic(n), 40) + "</td></tr>");

                }

                sb.append("</table>");
            }

            private void options() {

                sb.append(new AdminTable("Options"));

                sb.append("<tr><th width='300'><b>Scale</b></th><td>" + new TextTag(context, "ratingScale", module.getRatingScale(), 4) + "</td></tr>");

                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(ItemTypeHandler.class, "saveRatings", "post"));
                sb.append(new HiddenTag("itemType", itemType));

                commands();
                types();
                options();
                graphics();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

            private void types() {

                sb.append(new AdminTable("Rating types"));

                sb.append("<tr>");
                sb.append("<th>Name</th>");
                sb.append("<th width='60'>Remove</th>");
                sb.append("<th width='60'>Edit</th>");
                sb.append("</tr>");

                for (RatingType ratingType : module.getRatingTypes()) {

                    sb.append("<tr>");
                    sb.append("<td>" + ratingType.getName() + "</td>");
                    sb.append("<td width='60'>" + new ButtonTag(RatingTypeHandler.class, null, "Edit", "ratingType", ratingType) + "</td>");
                    sb.append("<td width='60'>" +
                            new ButtonTag(RatingTypeHandler.class, "delete", "Delete", "ratingType", ratingType)
                                    .setConfirmation("Are you sure you want to delete this rating type?") + "</td>");
                    sb.append("</tr>");
                }

                sb.append("<tr><th colspan='3'>" + new TextTag(context, "addRatingTypeName", 20) + "</th></tr>");
                sb.append("</table>");
            }

        });
        return page;
    }

    public Object save() throws ServletException {

        if (itemType == null) {
            return main();
        }

        if (!isSuperman() && !Module.UserItemTypes.enabled(context)) {
            return main();
        }

        if (fieldType != null) {
            itemType.addAdminField(fieldType);
        }

        // name plural first in case name changes, we then want to override name plural
        itemType.setNamePlural(namePlural);

        if (name != null) {
            itemType.setName(name);
        }

        itemType.setNameCaption(nameCaption);

        itemType.setReferences(references);
        itemType.setReferencesCaption(referencesCaption);

        itemType.setShortNames(shortNames);

        itemType.setAccessoriesMarkup(accessoriesMarkup);
        itemType.setAccessoriesBlurb(accessoriesBlurb);

        // seo
        itemType.setOverrideTags(overrideTags);

        itemType.setMaxCategories(maxCategories);
        itemType.setDefaultCategory(defaultCategory);

        itemType.setFeatured(featured);
        itemType.setFeaturedCaption(featuredCaption);

        itemType.setPrioritised(prioritised);

        itemType.setOwners(owners);

        // headers footers
        itemType.setHeader(header);
        itemType.setFooter(footer);

        itemType.setEmails(StringHelper.explodeStrings(emails, "\n"));

        itemType.setAttachmentsCaption(attachmentsCaption);
        itemType.setHidden(hidden);
        itemType.setHiddenCs(hiddenCs);
        itemType.setMobilePhoneDisable(mobilePhoneDisable);

        itemType.setBookable(bookable);

        //

        // options
        {
            OptionsModule optionsModule = itemType.getOptionsModule();

            optionsModule.setDisplayExVat(displayExVat);
            optionsModule.save();
        }

        // images
        {
            itemType.setImageHolder(imageHolder);
            itemType.setModerateImages(moderateImages);

            if (newImageType != null) {
                itemType.getMediaModule().addImageType(newImageType);
            }
        }

        /*
           * Forum stuff
           */
        itemType.setMessages(messages);
        itemType.setForum(forum);
        itemType.setInitialTopicContent(initialTopicContent);

        itemType.setStatusControl(statusControl);

        /*
           * Content related
           */
        {
            itemType.setSimpleEditor(simpleEditor);
            itemType.setDefaultContent(defaultContent);
            itemType.setSummaries(summaries);
        }

        if (linkType != null) {
            itemType.addExternalLink(linkType);
        }

        /*
           * Listing stuff
           */
        itemType.setBlockEmailsInContent(blockEmailsInContent);

        itemType.setNoForum(noForum);

        if (isSuperman() || Module.UserMarkup.enabled(context)) {

            itemType.setViewMarkup(viewMarkup);
            itemType.setListMarkup(listMarkup);
            itemType.setPrinterMarkup(printerMarkup);
            itemType.setSearchListMarkup(searchListMarkup);

            TemplateConfig config = itemType.getTemplateConfig();
            if (config == null && template != null) {
                config = TemplateConfig.create(context, itemType);
            }

            if (config != null) {
                if (!ObjectUtil.equal(config.getTemplate(), template)) {

                    if (template == null) {
                        config.delete();
                        config = null;
                        itemType.log(user, "Template removed");
                    } else {
                        config.setTemplate(template);
                        config.save();
                        itemType.log(user, "Template changed to '" + template.getName() + "' #" + template.getId());
                    }
                }
            }
        }

        /*
           * Permissions
           */
        {

            itemType.setPermissions(enablePermissions);
            itemType.setRestrictionForwardUrl(restrictionForwardUrl);
            itemType.save();

            DomainUtil.savePermissions(context, itemType, permissions, PermissionCollections.getItem());
        }

        if (ItemModule.Listings.enabled(context, itemType)) {
            itemType.setAcceptListingEmailContent(acceptListingEmailContent);
            itemType.setRejectListingEmailContent(rejectListingEmailContent);
            itemType.setAddListingEmailContent(addListingEmailContent);
        }

        itemType.save();

        // attributes
        if (newAttributes != null) {
            for (Attribute attribute : itemType.addAttributes(newAttributes, newAttributesSection, newAttributesPage)) {
                itemType.log(user, "Attribute created - " + attribute.getName());
                attribute.log(user, "Attributed created - " + attribute.getName());
            }
        }

        // modules
        if (addModule != null) {
            itemType.addModule(addModule);
        }

        itemType.addBlock(addBlock);



        clearParameters();
        return edit();
    }

    public Object saveOptions() throws ServletException {

        if (itemType == null) {
            return main();
        }

        itemType.setAssignCodeToOption(assignCodeToOption);
        itemType.setProductCodeAttribute(productCodeAttribute);
        itemType.save();

         // OPTIONS
        if (newOptions != null) {
            for (String string : StringHelper.explodeStrings(newOptions, "\n")) {
                itemType.getOptionSet().addOption(string, itemType);
            }
        }

        return new ActionDoc(context, "The options module has been updated", new Link(ItemTypeHandler.class, "options", "itemType", itemType));
    }

    public Object savePricing() throws ServletException {

        if (itemType == null) {
            return main();
        }

        itemType.setCostPricing(costPricing);
        itemType.setRrp(rrp);
        itemType.setDefaultVatRate(defaultVatRate);
        //		itemType.setPromotionPrices(promotionPrices);
        itemType.setVat(vat);

        // rounding
        itemType.setPriceRounding(priceRounding);
        itemType.setRoundTo(roundTo);

        itemType.save();

        return new ActionDoc(context, "The pricing module has been updated", new Link(ItemTypeHandler.class, "pricing", "itemType", itemType));
    }

    public Object saveRatings() throws ServletException {

        if (itemType == null) {
            return main();
        }

        RatingModule module = itemType.getRatingModule();

        for (Map.Entry<Integer, String> entry : ratingGraphics.entrySet())
            module.setRatingGraphic(entry.getKey(), entry.getValue());

        if (addRatingTypeName != null)
            module.addRatingType(addRatingTypeName);

        module.setRatingScale(ratingScale);
        module.save();

        clearParameters();
        return ratings();
    }

    public Object sorts() throws ServletException {

        if (itemType == null) {
            return main();
        }

        if (!isSuperman() && !Module.UserItemTypes.enabled(context)) {
            return new DashboardHandler(context).main();
        }

        AdminDoc doc = new AdminDoc(context, user, "Edit item type: " + itemType.getName(), null);
        doc.setMenu(new EditItemTypeMenu(itemType));
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new ButtonTag(ItemTypeHandler.class, null, "Return to item types menu"));
                sb.append("</div>");
            }

            @Override
            public String toString() {

                commands();
                sb.append(new AdminSortsRenderer(context, itemType));
                commands();

                return sb.toString();
            }

        });
        return doc;
    }

}
