package org.sevensoft.ecreator.iface.admin.extras.newsfeed;

import com.sun.syndication.io.FeedException;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.extras.rss.Newsfeed;
import org.sevensoft.ecreator.model.extras.rss.NewsfeedPresets;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

import java.io.IOException;
import java.net.MalformedURLException;

/**
 * @author sks 17-Jun-2005 17:37:55
 */
@Path("admin-settings-newsfeed.do")
public class NewsfeedsHandler extends AdminHandler {

    private boolean enabled;
    private Newsfeed feed;
    private String customUrl;
    private String presetUrl;

    public NewsfeedsHandler(RequestContext context) {
        super(context);
    }

    @Override
    public Object main() {

        if (!Module.Newsfeed.enabled(context))
            return new DashboardHandler(context).main();

        AdminDoc doc = new AdminDoc(context, user, "Newsfeeds", Tab.Extras);
        if (miscSettings.isAdvancedMode()) {
            doc.setMenu(new ExtrasMenu());
        }
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update newsfeeds"));
                sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
                sb.append("</div>");
            }

            private void feeds() {
                sb.append(new AdminTable("Feeds"));

                sb.append("<tr>");
                sb.append("<th>ID</th>");
                sb.append("<th>Title</th>");
                sb.append("<th>Url</th>");
                sb.append("<th>Refresh</th>");
                sb.append("<th>Remove</th>");
                sb.append("</tr>");

                for (Newsfeed feed : Newsfeed.get(context)) {

                    sb.append("<tr>");
                    sb.append("<td>" + feed.getIdString() + "</td>");
                    sb.append("<td>" + feed.getTitle() + "</td>");
                    sb.append("<td>" + feed.getUrl() + "</td>");
                    sb.append("<td>" + new ButtonTag(NewsfeedsHandler.class, "refresh", "Refresh", "feed", feed) + "</td>");
                    sb.append("<td>" + new ButtonTag(NewsfeedsHandler.class, "remove", "Remove", "feed", feed) + "</td>");
                    sb.append("</tr>");

                }

                SelectTag presets = new SelectTag(context, "presetUrl");
                presets.setAny("-Choose preset feed-");
                presets.addOptions(NewsfeedPresets.getNewsfeeds());

                sb.append("<tr><td colspan='4'>Add a preset feed: " + presets + "</td>");
                sb.append("<tr><td colspan='4'>Add a custom feed: " + new TextTag(context, "customUrl", 80) + "</td>");
                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(NewsfeedsHandler.class, "save", "POST"));

                commands();
                feeds();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object refresh() {

        if (feed == null)
            return main();

        try {

            feed.refresh();
            addMessage("This feed has been updated with the latest articles");

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            addError(e);

        } catch (MalformedURLException e) {
            e.printStackTrace();
            addError(e);

        } catch (FeedException e) {
            e.printStackTrace();
            addError(e);

        } catch (IOException e) {
            e.printStackTrace();
            addError(e);
        }

        return main();
    }

    public Object remove() {

        if (feed != null)
            feed.delete();

        return main();
    }

    public Object save() {

        try {

            if (customUrl != null) {

//                customUrl = customUrl.toLowerCase();
                new Newsfeed(context, customUrl);
            }

            if (presetUrl != null) {
                new Newsfeed(context, presetUrl);
            }

        } catch (IOException e) {
            e.printStackTrace();
            addError(e);

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            addError(e);

        } catch (FeedException e) {
            e.printStackTrace();
            addError(e);
        }

        clearParameters();

        addMessage("Changes saved.");
        clearParameters();
        return main();
    }

}
