package org.sevensoft.ecreator.iface.admin.reports.visitors;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.reports.StatsMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.stats.visits.Session;
import org.sevensoft.ecreator.model.stats.visits.VisitorsCounterDaily;
import org.sevensoft.ecreator.model.stats.visits.VisitorsCounterMonthly;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHitCounter;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.superstrings.StringHelper;

import javax.servlet.ServletException;
import java.util.List;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */

@Path("admin-visitors.do")
public class VisitorsHandler extends AdminHandler {

    private Date start;
    private Date end;

    public VisitorsHandler(RequestContext context) {
        super(context);
    }

    public Object main() throws ServletException {
        return recent();
    }

    public Object daily() {
        if (start == null)
            start = new Date().removeDays(30);

        if (end == null)
            end = new Date();

        final List<VisitorsCounterDaily> visitorList = VisitorsCounterDaily.get(context, start, end);
        AdminDoc doc = new AdminDoc(context, user, "Visitors daily", Tab.Stats);
        doc.setMenu(new StatsMenu());
        doc.setIntro("Listed here are your total visitors and unique sessions per day dating from " + start.toString("dd-MMM-yyyy") + " to "
                + end.toString("dd-MMM-yyyy"));
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new AdminTable("Visitors daily"));

                sb.append("<tr>");
                sb.append("<th>Date</th>");
                sb.append("<th>Total visitors</th>");
                sb.append("<th>Sessions</th>");
                sb.append("</tr>");

                for (VisitorsCounterDaily counter : visitorList) {

                    sb.append("<tr>");
                    sb.append("<td>" + counter.getDate().toString("dd-MMM-yyyy") + "</td>");
                    sb.append("<td>" + counter.getTotal() + "</td>");
                    sb.append("<td>" + counter.getUnique() + "</td>");
                    sb.append("</tr>");

                }

                sb.append("</table>");

                return sb.toString();
            }
        });
        return doc;

    }

    public Object monthly() {
        if (start == null)
            start = new Date().removeYear(1).beginMonth();

        if (end == null)
            end = new Date().beginMonth();

        final List<VisitorsCounterMonthly> counters = VisitorsCounterMonthly.get(context, start, end);

        AdminDoc doc = new AdminDoc(context, user, "Visitors monthly", Tab.Stats);
        doc.setMenu(new StatsMenu());
        doc.setIntro("Listed here are your total visitors and unique sessions per month dating from " + start.toString("MMMM yy") + " to "
                + end.toString("MMMM yy"));
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new AdminTable("Visitors monthly"));

                sb.append("<tr>");
                sb.append("<th>Date</th>");
                sb.append("<th>Total visitors</th>");
                sb.append("<th>Sessions</th>");
                sb.append("</tr>");

                for (SiteHitCounter counter : counters) {

                    sb.append("<tr>");
                    sb.append("<td>" + counter.getDate().toString("MMM-yyyy") + "</td>");
                    sb.append("<td>" + counter.getTotal() + "</td>");
                    sb.append("<td>" + counter.getUnique() + "</td>");
                    sb.append("</tr>");

                }

                sb.append("</table>");

                return sb.toString();
            }
        });
        return doc;
    }

    public Object recent() {
        int limit = 200;
        final List<Session> sessions = Session.getRecent(context, 0, limit);

        AdminDoc doc = new AdminDoc(context, user, "Recent visitors", Tab.Stats);
        doc.setMenu(new StatsMenu());
        doc.setIntro("This screen shows your most recent " + limit + " visitors.");
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new AdminTable("Recent visitors"));

                sb.append("<tr>");
                sb.append("<th>Last Access</th>");
                sb.append("<th>IP Address</th>");
                sb.append("<th>Country</th>");
                sb.append("<th>Page</th>");
                sb.append("<th>Browser</th>");
                sb.append("<th>Referrer</th>");
                sb.append("</tr>");

                for (Session session : sessions) {

                    sb.append("<tr>");
                    sb.append("<td>" + new DateTime(session.getLastAccess()).toString("HH:mm dd-MMM-yy") + "</td>");
                    sb.append("<td>" + session.getVisitor().getIpAddress() + "</td>");
                    sb.append("<td>");
                    if (session.getVisitor().getCountry() != null) {
                        sb.append(session.getVisitor().getCountry());
                    }
                    sb.append("</td>");
                    sb.append("<td>" + new LinkTag(session.getEntryPageUrl(), StringHelper.toSnippet(session.getEntryPageUrl().replace("http://", ""), 40, "...")) + "</td>");
                    sb.append("<td>" + session.getBrowser() + "</td>");
                    sb.append("<td>"
                            + (session.hasReferrer() ? new LinkTag(session.getReferrer(), StringHelper.toSnippet(
                            session.getReferrer().replace("http://", ""), 40, "...")) : "") + "</td>");
                    sb.append("</tr>");
                }

                sb.append("</table>");

                return sb.toString();
            }

        });
        return doc;
    }

}
