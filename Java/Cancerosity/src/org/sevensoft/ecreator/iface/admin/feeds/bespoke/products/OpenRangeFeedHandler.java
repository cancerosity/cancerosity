package org.sevensoft.ecreator.iface.admin.feeds.bespoke.products;

import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.products.openrange.OpenRangeFeed;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;

import java.util.List;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
@Path("admin-feeds-open-range.do")
public class OpenRangeFeedHandler extends FeedHandler {

    private OpenRangeFeed feed;
    private ItemType itemType;
    private Attribute lastUpdatedAttribute;
    private Attribute manufFamilyAttribute;
    private Attribute manufFamilyMemberAttribute;

    public OpenRangeFeedHandler(RequestContext context) {
        super(context);
    }

    protected ImportFeed getFeed() {
        return feed;
    }

    protected void saveSpecific() {
        feed.setItemType(itemType);
        feed.setLastUpdatedAttribute(lastUpdatedAttribute);
        feed.setManufFamilyAttribute(manufFamilyAttribute);
        feed.setManufFamilyMemberAttribute(manufFamilyMemberAttribute);
    }

    protected void specifics(StringBuilder sb) {
        sb.append(new AdminTable("Open Range specifics"));
        sb.append(new AdminRow("Item type", "Select the item type to import to.", new SelectTag(context, "itemType", feed.getItemType(), ItemType
                .getSelectionMap(context), "-None set-")));

        if (feed.getItemType() != null) {
            List<Attribute> attributes = feed.getItemType().getAttributes();

            SelectTag attributeTag = new SelectTag(context, "lastUpdatedAttribute", feed.getLastUpdatedAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(attributes);
            sb.append(new AdminRow(OpenRangeFeed.LAST_UPDATED_ATTRIBUTE, attributeTag));

            attributeTag = new SelectTag(context, "manufFamilyAttribute", feed.getManufFamilyAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(attributes);
            sb.append(new AdminRow(OpenRangeFeed.MANUFACTURER_FAMILY_ATTRIBUTE, attributeTag));

            attributeTag = new SelectTag(context, "manufFamilyMemberAttribute", feed.getManufFamilyMemberAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(attributes);
            sb.append(new AdminRow(OpenRangeFeed.MANUFACTURER_FAMILY_MEMBER_ATTRIBUTE, attributeTag));

            sb.append(new AdminRow("Create categories", new BooleanRadioTag(context, "createCategories",
                    ((ImportFeed) feed).isCraeteCategories())));

        }
        sb.append("</table>");
    }
}
