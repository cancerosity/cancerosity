package org.sevensoft.ecreator.iface.admin;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.EcreatorHandler;
import org.sevensoft.ecreator.model.crm.mailboxes.MailboxProcessorRunner;
import org.sevensoft.ecreator.model.misc.currencies.Currency;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.extras.reorder.ReorderOwner;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.List;

/**
 * @author sks 21-Jul-2004 15:24:23
 */
public abstract class AdminHandler extends EcreatorHandler {

	/**
	 * Last time the mailbox runner was run
	 */
	private static long		mailboxRuntime		= 0;

	/**
	 * Scan mailboxes every 60 seconds
	 */
	private static final long	MailboxScanInterval	= 1000l * 60;

	public AdminHandler(RequestContext context) {
		super(context);

		/*
		 * Set default currency symbol for admin
		 */
		currencySymbol = Currency.getDefault(context).getSymbol();
	}

	protected final Object index() throws ServletException {
		return new DashboardHandler(context).main();
	}

	@Override
	protected Object init() throws ServletException {

		Object obj = super.init();
		if (obj != null) {
			return obj;
		}

		if (user == null) {
			return new AdminLoginHandler(context).main();
		}

		/*
		 * Do mailbox scan if needed
		 */
		if (Module.CrmMailboxes.enabled(context)) {

			if (mailboxRuntime < (System.currentTimeMillis() - MailboxScanInterval)) {

				mailboxRuntime = System.currentTimeMillis();
				Thread t = new Thread(new MailboxProcessorRunner(context));
				t.setDaemon(true);
				t.start();
			}

		}

		return null;
	}

	protected boolean isMarkup() {
		return Module.UserMarkup.enabled(context) || isSuperman();
	}

	@Override
	public boolean runSecure() {
		return false;
	}

    public Object reorder(List<? extends ReorderOwner> list) throws ServletException {
        int n = 1;

        for (ReorderOwner option : list) {

            option.setPosition(n);
            option.save();

            n++;
        }
        return this;
    }

}