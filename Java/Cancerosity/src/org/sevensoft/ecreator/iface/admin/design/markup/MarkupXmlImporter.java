package org.sevensoft.ecreator.iface.admin.design.markup;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 23 Aug 2006 07:44:35
 *
 */
public class MarkupXmlImporter {

	private RequestContext	context;

	public MarkupXmlImporter(RequestContext context) {
		this.context = context;
	}

	public void process(String data) throws JDOMException, IOException {

		Document doc = new SAXBuilder().build(new StringReader(data));
		Element root = doc.getRootElement();

		List<Element> markupEls = root.getChildren("Markup");
		for (Element markupEl : markupEls) {

			String name = markupEl.getChildText("Name");
			String body = markupEl.getChildText("Body");
			String end = markupEl.getChildText("End");
			String start = markupEl.getChildText("Start");
			String between = markupEl.getChildText("Between");
			String containerId = markupEl.getChildText("ContainerId");
			String containerClass = markupEl.getChildText("ContainerClass");
			int tds = Integer.parseInt(markupEl.getChildText("Tds"));

			Markup markup = new Markup(context, name, start, body, end, between, tds);
			markup.setTableClass(containerClass);
			markup.setTableId(containerId);
			markup.save();
		}
	}

}
