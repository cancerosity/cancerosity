package org.sevensoft.ecreator.iface.admin.crm.jobsheets;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.crm.jobsheets.panels.JobsPanel;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.crm.jobsheets.Job;
import org.sevensoft.ecreator.model.crm.jobsheets.JobSearcher;
import org.sevensoft.ecreator.model.crm.jobsheets.Jobsheet;
import org.sevensoft.ecreator.model.crm.jobsheets.Job.Status;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

/**
 * @author sks 18 Jul 2006 16:57:43
 *
 */
@Path("admin-jobs-search.do")
public class JobSearchHandler extends AdminHandler {

	private static final int	ResultsPerPage	= 40;

	private String			author;
	private String			content;
	private transient List<Job>	jobs;
	private transient int		total;
	private int				page;
	private transient Results	results;
	private String			name;
	private User			owner;
	private Jobsheet			jobsheet;

	private Status			status;

	public JobSearchHandler(RequestContext context) {
		super(context);
	}

	private Link getLink() {
		Link link = new Link(JobSearchHandler.class, "search");
		link.setParameter("author", author);
		link.setParameter("content", content);
		link.setParameter("name", name);
		link.setParameter("status", status);
		link.setParameter("jobsheet", jobsheet);
		link.setParameter("owner", owner);
		return link;
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Job search", Tab.Extras);
		doc.setMenu(new ExtrasMenu());
		doc.addBody(new Body() {

			private void criteria() {

				sb.append(new FormTag(JobSearchHandler.class, "search", "get"));

				sb.append(new AdminTable("Job search"));

				sb.append(new AdminRow("Title", "Search in the title of the job.", new TextTag(context, "name", 20)));

				sb.append(new AdminRow("Status", "Status of the job.", new SelectTag(context, "status", null, Job.Status.values(), "Any")));

				sb.append(new AdminRow("Jobsheet", "Narrow results by jobsheet.", new SelectTag(context, "jobsheet", null, Jobsheet.get(context),
						"-Select jobsheet-")));

				sb.append(new AdminRow("Owner", "Search by owner of the job.", new SelectTag(context, "owner", null, User.getAll(context),
						"-Select owner-")));

				sb.append(new AdminRow("Author", "Enter the name of an author of a message in a job.", new TextTag(context, "author", 20)));

				sb
						.append(new AdminRow("Message contains", "Search for jobs containing a message with this text.", new TextTag(context,
								"content", 30)));

				sb.append("<tr><td colspan='2'>" + new SubmitTag("Search") + "</td></tr>");

				sb.append("</form>");
				sb.append("</table>");

			}

			private void results() {

				sb.append(new ResultsControl(context, results, getLink()));
				sb.append(new JobsPanel(context, jobs));
			}

			@Override
			public String toString() {

				criteria();
				if (jobs != null && jobs.size() > 0) {
					results();
				}
				return sb.toString();
			}

		});
		return doc;
	}

	public Object search() throws ServletException {

		JobSearcher searcher = new JobSearcher(context);
		searcher.setAuthor(author);
		searcher.setContent(content);
		searcher.setName(name);
		searcher.setOwner(owner);
		searcher.setJobsheet(jobsheet);
		searcher.setStatus(status);

		total = searcher.size();

		results = new Results(total, page, ResultsPerPage);

		searcher.setStart(results.getStartIndex());
		searcher.setLimit(ResultsPerPage);

		jobs = searcher.execute();

		return main();
	}
}
