package org.sevensoft.ecreator.iface.admin.system.log;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

/**
 * @author sks 8 Aug 2006 14:34:43
 * View log
 */
@Path("admin-logging.do")
public class LogHandler extends AdminHandler {

	private static final int	Limit	= 50;
	private Item			item;
	private int				page;
	private Category			category;
	private String			name;
	private String			ip;

	public LogHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		LogSearcher searcher = new LogSearcher(context);
		searcher.setIp(ip);
		searcher.setName(name);

		int count = searcher.size();

		final Results results = new Results(count, page, Limit);

		searcher.setStart(results.getStartIndex());
		searcher.setLimit(Limit);

		final List<LogEntry> entries = searcher.execute();

		AdminDoc doc = new AdminDoc(context, user, "Log", null);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new FormTag(LogHandler.class, null, "GET"));
				sb.append("<table>");
				sb.append("<tr><td>Filter by name</td><td>" + new TextTag(context, "name", 20) + "</td></tr>");
				sb.append("<tr><td>Filter by ip</td><td>" + new TextTag(context, "ip", 20) + new SubmitTag("Update") + "</td></tr>");
				sb.append("</table>");
				sb.append("</form>");

				sb.append(new ResultsControl(context, results, new Link(LogHandler.class, null, "name", name)));

				sb.append(new AdminTable("Log"));

				sb.append("<tr>");
				sb.append("<th width='120'>Date</th>");
				sb.append("<th>Ip Address</th>");
				sb.append("<th>Log</th>");
				sb.append("<th>User</th>");
				sb.append("<th>Message</th>");
				sb.append("</tr>");

				for (LogEntry entry : entries) {

					sb.append("<tr>");
					sb.append("<td>" + entry.getTimeString() + "</td>");
					sb.append("<td>" + entry.getIpAddress() + "</td>");
					sb.append("<td>" + entry.getName() + "</td>");
					sb.append("<td>" + entry.getLoggerName() + "</td>");
					sb.append("<td>" + entry.getMessage() + "</td>");
					sb.append("</tr>");
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}
}
