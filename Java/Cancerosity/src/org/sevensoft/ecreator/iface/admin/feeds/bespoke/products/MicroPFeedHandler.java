package org.sevensoft.ecreator.iface.admin.feeds.bespoke.products;

import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.products.microp.MicroPFeed;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;

import java.util.List;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
@Path("admin-feeds-micro-p.do")
public class MicroPFeedHandler extends FeedHandler {

    private MicroPFeed feed;
    private Attribute manufPartNumber;
    protected ItemType itemType;
    protected boolean disableProducts;

    public MicroPFeedHandler(RequestContext context) {
        super(context);
    }

    protected ImportFeed getFeed() {
        return feed;
    }

    protected void saveSpecific() {
        feed.setItemType(itemType);
        feed.setDisableProducts(disableProducts);
        feed.setManufPartNumberAttribute(manufPartNumber);
    }

    protected void specifics(StringBuilder sb) {
        sb.append(new AdminTable("Micro-p specifics"));
        sb.append(new AdminRow("Item type", "Select the item type to import to.", new SelectTag(context, "itemType", feed.getItemType(), ItemType
                .getSelectionMap(context), "-None set-")));
        sb.append(new AdminRow("Disable products", "Disable products not updated within one month",
                new BooleanRadioTag(context, "disableProducts", feed.disableProducts())));

        if (feed.getItemType() != null) {
            List<Attribute> attributes = feed.getItemType().getAttributes();

            SelectTag attributeTag = new SelectTag(context, "manufPartNumber", feed.getManufPartNumberAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(attributes);
            sb.append(new AdminRow(MicroPFeed.MANUF_PART_NUMBER, attributeTag));
        }
        sb.append("</table>");
    }
}
