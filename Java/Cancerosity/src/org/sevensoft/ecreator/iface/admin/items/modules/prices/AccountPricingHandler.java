package org.sevensoft.ecreator.iface.admin.items.modules.prices;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.pricing.Price;
import org.sevensoft.ecreator.model.items.pricing.PriceSettings;
import org.sevensoft.ecreator.model.items.pricing.PriceUtil;
import org.sevensoft.ecreator.model.items.pricing.panels.PriceBreaksPanel;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 28 Oct 2006 22:01:57
 * 
 * Individual pricing for accounts
 *
 */
@Path("admin-accounts-pricing.do")
public class AccountPricingHandler extends AdminHandler {

	private Item					account;
	private Map<Integer, String>			prices;
	private String					addItem;
	private Item					item;
	private final transient PriceSettings	priceSettings;
	private String					priceBreaks;

	public AccountPricingHandler(RequestContext context) {
		super(context);
		this.priceSettings = PriceSettings.getInstance(context);
	}

	@Override
	public Object main() throws ServletException {

		if (account == null) {
			return index();
		}

		final List<Item> items = account.getAccountPricedItems();
		if (item != null && !items.contains(item)) {
			items.add(item);
			logger.fine("[AccountPricingHandler] adding item=" + item);
		}
		logger.fine("[AccountPricingHandler] items=" + items);
		final Set<Integer> priceBreaks = account.getPriceBreaks();

		AdminDoc doc = new AdminDoc(context, user, "Account pricing", null);
		doc.setMenu(new EditItemMenu(account));
		doc.addBody(new Body() {

            private void commands() {
                sb.append(new SubmitTag("Update"));
            }

			@Override
			public String toString() {

				sb.append(new FormTag(AccountPricingHandler.class, "save", "post"));
				sb.append(new HiddenTag("account", account));
				sb.append(new HiddenTag("item", item));

                commands();
				sb.append(new AdminTable("Price options"));

				if (priceSettings.isUsePriceBreaks()) {
					sb.append(new PriceBreaksPanel(context, account, priceBreaks.size()));
				}

				sb.append("</table>");

				sb.append(new AdminTable("Prices"));

				sb.append("<tr>");
				sb.append("<th>Item</th>");
				for (int qty : priceBreaks) {
					sb.append("<th>Qty up to " + qty + "</th>");
				}
				sb.append("</tr>");

				int n = 0;
				for (Item item : items) {

					sb.append("<tr>");

					sb.append("<td>" + new LinkTag(ItemHandler.class, "edit", item.getName(), "item", item) + " "
							+ new LinkTag(AccountPricingHandler.class, "remove", "x", "item", item, "account", account) + "</td>");

					for (int qty : priceBreaks) {

						List<Price> prices = item.getPrices(account);
						Price price = PriceUtil.getPrice(prices, null, qty);
						String priceString = PriceUtil.getPriceString(price);

						Money costPrice = item.getCostPrice();
						String calculatedPrice = "";
						if (price != null && costPrice != null && costPrice.isPositive()) {
							calculatedPrice = price.calculateSellPrice(costPrice).toString();
						}

						sb.append("<td>" + new TextTag(context, "prices_" + n, priceString, 8) + " " + calculatedPrice + "</td>");
						n++;
					}

					sb.append("</tr>");

				}

				sb.append("<tr><td colspan='" + (priceBreaks.size() + 1) + "'>To add pricing for a new item enter the item's reference here: "
						+ new TextTag(context, "addItem", 12) + "</td></tr>");

				sb.append("</table>");

                commands();
				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object remove() throws ServletException {

		if (item == null || account == null) {
			return main();
		}

		account.removeAccountPricing(item);
		item = null;
		return main();
	}

	public Object save() throws ServletException {

		if (account == null) {
			return index();
		}

		final List<Item> items = account.getAccountPricedItems();
		if (item != null && !items.contains(item)) {
			items.add(item);
		}
		logger.fine("[AccountPricingHandler] items=" + items);

		account.removeAccountPrices();
		account.setPriceBreaks(StringHelper.explodeIntegers(priceBreaks, "\\D"));

		final Set<Integer> priceBreaks = account.getPriceBreaks();

		int n = 0;
		logger.fine("[AccountPricingHandler] saving prices for items=" + items);
		for (Item item : items) {

			for (int qty : priceBreaks) {

				String string = prices.get(n);
				Amount amount = Amount.parse(string);

				logger.fine("[AccountPricingHandler] setting price for n=" + n + ", item=" + item + ", qty=" + qty + ", string=" + string);

				if (amount != null) {
					item.setAccountPrice(account, qty, amount);
				}

				n++;
			}
		}

		if (addItem != null) {

			item = EntityObject.getInstance(context, Item.class, addItem);
			if (item == null) {
				item = Item.getByRef(context, null, addItem);
			}
		}

		clearParameters();
		return main();
	}
}
