package org.sevensoft.ecreator.iface.admin.interaction.userplane.recorder;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.frontend.interaction.userplane.recorder.UserplaneRecorderRenderer;
import org.sevensoft.ecreator.model.interaction.userplane.recorder.Recording;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 26 Nov 2006 14:52:34
 *
 */
@Path("admin-userplane-recorder-admin.do")
public class UserplaneRecorderAdminHandler extends AdminHandler {

	private Recording	recording;
	private Item	account;

	public UserplaneRecorderAdminHandler(RequestContext context) {
		super(context);
	}

	public Object approve() throws ServletException {

		if (recording == null) {
			return main();
		}

		recording.setStatus("approved");
		recording.save();

		return main();
	}

	public Object delete() throws ServletException {

		if (recording == null) {
			return main();
		}

		recording.setStatus("deleted");
		recording.save();

		return main();
	}

	@Override
	public Object main() throws ServletException {

		if (!Module.UserplaneRecorder.enabled(context)) {
			return new DashboardHandler(context).main();
		}

		final List<Recording> pending = Recording.getPending(context);

		AdminDoc doc = new AdminDoc(context, user, "Userplane recorder admin", Tab.Extras);
		doc.setMenu(new ExtrasMenu());

		if (pending.isEmpty()) {

			doc.addBody(new Body() {

				@Override
				public String toString() {

					sb.append(new TableTag("simplelinks", "center"));
					sb.append("<tr><th align='center'>There are no pending videos</th></tr>");

					sb.append("<tr><td align='center'>" + new LinkTag(SettingsMenuHandler.class, null, "Click here to return to the settings menu")
							+ "</td></tr>");

					sb.append("</table>");

					return sb.toString();
				}

			});

		} else {

			doc.addBody(new Body() {

				private void commands() {

					sb.append("<div align='center' class='actions'>");
					sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
					sb.append("</div>");
				}

				private void general() {

					sb.append(new AdminTable("Videos awaiting approval"));

					for (Recording recording : pending) {

						sb.append("<tr>");
						sb.append("<td>" + recording.getAccount().getName() + "</td>");
						sb.append("<td>" + new LinkTag(UserplaneRecorderAdminHandler.class, "view", "View", "account", recording.getAccount())
								+ "</td>");
						sb.append("</tr>");
					}

					sb.append("</table>");
				}

				@Override
				public String toString() {

                    commands();
					general();
					commands();

					return sb.toString();
				}

			});

		}

		return doc;
	}

	public Object view() throws ServletException {

		if (!Module.UserplaneRecorder.enabled(context)) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Userplane recorder admin", Tab.Extras);
		doc.setMenu(new ExtrasMenu());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new UserplaneRecorderRenderer(context, account.getId(), true));
				return sb.toString();
			}

		});
		return doc;
	}

}
