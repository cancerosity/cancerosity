package org.sevensoft.ecreator.iface.admin.marketing.sms;

import java.io.IOException;

import javax.servlet.ServletException;

import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.marketing.MarketingMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.marketing.sms.SmsBulletin;
import org.sevensoft.ecreator.model.marketing.sms.SmsNumberException;
import org.sevensoft.ecreator.model.marketing.sms.SmsRegistration;
import org.sevensoft.ecreator.model.marketing.sms.SmsSenderException;
import org.sevensoft.ecreator.model.marketing.sms.SmsSettings;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsCreditsException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayAccountException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsGatewayMessagingException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.exceptions.SmsMessageException;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks
 */
@Path("admin-sms-compose.do")
public class SmsComposeHandler extends AdminHandler {

	private String				message;
	protected transient SmsSettings	smsSettings;

	public SmsComposeHandler(RequestContext context) {
		super(context);
		smsSettings = SmsSettings.getInstance(context);
	}

	@Override
	public Object main() throws ServletException {

		// check we have subscribers
		if (SimpleQuery.count(context, SmsRegistration.class) == 0) {

			AdminDoc doc = new AdminDoc(context, user, "You do not have any SMS registrations yet", Tab.Marketing);
			doc.addBody(new Body() {

				@Override
				public String toString() {

					sb.append(new TableTag("simplelinks", "center"));
					sb.append("<tr><th align='center'>You cannot send an SMS bulletin yet because you haven't got any numbers registered</th></tr>");

					sb.append("<tr><td align='center'>" + new LinkTag(SmsRegistrationsHandler.class, "main", "I want to add a mobile number") +
							"</td></tr>");

					sb.append("</table>");

					return sb.toString();
				}
			});
			return doc;
		}

		AdminDoc doc = new AdminDoc(context, user, "Compose SMS bulletin", Tab.Marketing);
		if (miscSettings.isAdvancedMode()) {
			doc.setMenu(new MarketingMenu());
		}
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Preview message"));
				sb.append("</div>");
			}

			private void compose() {

				sb.append(new AdminTable("Sms message"));
				//				sb.append(new AdminRow("Gateway", "This is your network provider.", smsBulletin.getGateway()));

				StringBuilder sb2 = new StringBuilder();

				TextAreaTag messageTag = new TextAreaTag(context, "message", 60, 5);
				messageTag.setOnKeyDown("var x = 160 - this.value.length; var e = document.getElementById('wordcount'); "
						+ "if (x < 0) e.innerHTML = '<b>Too many characters</b>'; else e.innerHTML=  x + ' characters remaining';");

				sb2.append(messageTag);
				sb2.append(new ErrorTag(context, "message", "<br/>"));
				sb2.append("");

				sb.append("<tr><td>Compose your SMS bulletin by entering your text into the box below. "
						+ "SMS messages are limited to 160 characters.<i><div id='wordcount'>160 characters remaining</div></i></td></tr>");
				sb.append("<tr><td>" + sb2 + "</td></tr>");
				sb.append("</table>");

			}

			@Override
			public String toString() {

				sb.append(new FormTag(SmsComposeHandler.class, "preview", "post"));

                commands();
				compose();
				commands();

				sb.append("</form>");

				return sb.toString();
			}
		});
		return doc;
	}

	public Object preview() throws ServletException {

		test(new RequiredValidator(), "message");
		test(new LengthValidator(0, 160), "message");
		if (hasErrors()) {
			return main();
		}

		AdminDoc page = new AdminDoc(context, user, "Sms bulletin - Preview message", Tab.Marketing);
		page.setMenu(new MarketingMenu());
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Send message"));
				sb.append(new ButtonTag(SmsComposeHandler.class, null, "Change message", "message", message));

				sb.append("</div>");
			}

			private void preview() {
				sb.append(new AdminTable("Sms message preview"));
				sb.append("<tr><td>" + message.replace("\n", "<br/>") + "</td></tr>");
				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(SmsComposeHandler.class, "send", "POST"));
				sb.append(new HiddenTag("message", message));

                commands();
				preview();
				commands();

				sb.append("</form>");

				return sb.toString();
			}
		});
		return page;
	}

	public Object send() throws ServletException {

		test(new RequiredValidator(), "message");
		if (hasErrors())
			return main();

		try {

			smsSettings.sendMessage(SmsBulletin.get(context), message);

			AdminDoc doc = new AdminDoc(context, user, "Sms bulletin - Message sent", Tab.Marketing);
			doc.setMenu(new MarketingMenu());
			doc.addBody(new Body() {

				@Override
				public String toString() {

					sb.append("Sms message sent. <br/><br/>" + "Messages can take up to 30 minutes to reach all recipients depending on the "
							+ "number of registered numbers in your bulletin and how busy the mobile networks are at this time.");

					sb.append("<tr><td align='center'>" + new LinkTag(SmsComposeHandler.class, null, "I want to send another sms message") +
							"</td></tr>");

					return sb.toString();
				}
			});
			return doc;

		} catch (IOException e) {
			e.printStackTrace();
			addError(e);

		} catch (SmsMessageException e) {
			e.printStackTrace();
			addError(e);

		} catch (SmsGatewayAccountException e) {
			e.printStackTrace();
			addError(e);

		} catch (SmsGatewayMessagingException e) {
			e.printStackTrace();
			addError(e);

		} catch (SmsGatewayException e) {
			e.printStackTrace();
			addError(e);

		} catch (SmsCreditsException e) {
			e.printStackTrace();

		} catch (SmsNumberException e) {
			e.printStackTrace();

		} catch (SmsSenderException e) {
			e.printStackTrace();
		}

		return preview();
	}
}