package org.sevensoft.ecreator.iface.admin.marketing.sms;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.marketing.MarketingMenu;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.attributes.renderers.OwnerAttributesPanel;
import org.sevensoft.ecreator.model.marketing.sms.SmsBulletin;
import org.sevensoft.ecreator.model.marketing.sms.SmsSettings;
import org.sevensoft.ecreator.model.marketing.sms.exceptions.ExistingRegistrationException;
import org.sevensoft.ecreator.model.marketing.sms.gateways.SmsGateway;
import org.sevensoft.ecreator.model.marketing.sms.sync.SmsCsvExporter;
import org.sevensoft.ecreator.model.marketing.sms.sync.SmsCsvImporter;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.StreamResult;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 17-Jun-2005 17:37:55
 * 
 */
@Path("admin-sms-settings.do")
public class SmsSettingsHandler extends AdminHandler {

	private boolean				sidebar, enabled;
	private String				bulkSmsUsername, bulkSmsPassword, clickatellApiId, clickatellUsername, clickatellPassword, content;
	private SmsGateway			gateway;
	private String				sidebarContent;
	protected transient SmsSettings	smsSettings;
	private Set<String>			keywords;
	private String				numberAdd;
	private String				numberRemove;
	private String				newAttributes;
	private Upload				upload;
	private String				newAttributesSection;
	private int					newAttributesPage;

	public SmsSettingsHandler(RequestContext context) {
		super(context);
		this.smsSettings = SmsSettings.getInstance(context);
	}

	public Object export() {

		SmsCsvExporter exporter = new SmsCsvExporter(context);
		try {
			File file = exporter.export();
			file.deleteOnExit();
			return new StreamResult(file, "text/csv", "sms.csv", StreamResult.Type.Attachment);

		} catch (IOException e) {
			e.printStackTrace();
			return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		}

	}

	@Override
	public Object main() {

		AdminDoc doc = new AdminDoc(context, user, "SMS bulletin settings", Tab.Marketing);
		if (miscSettings.isAdvancedMode()) {
			doc.setMenu(new MarketingMenu());
		}
		doc.addBody(new Body() {

			private void bulksms() {

				sb.append(new AdminRow("BulkSMS username", null, new TextTag(context, "bulkSmsUsername", smsSettings.getBulkSmsUsername(), 20)));
				sb.append(new AdminRow("BulkSMS Password", null, new TextTag(context, "bulkSmsPassword", smsSettings.getBulkSmsPassword(), 20)));

			}

			private void clickatell() {

				sb.append(new AdminRow("Clickatell api ID", null, new TextTag(context, "clickatellApiId", smsSettings.getClickatellApiId(), 12)));

				sb
						.append(new AdminRow("Clickatell username", null, new TextTag(context, "clickatellUsername", smsSettings
								.getClickatellUsername(), 20)));
				sb
						.append(new AdminRow("Clickatell password", null, new TextTag(context, "clickatellPassword", smsSettings
								.getClickatellPassword(), 20)));

			}

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update sms settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void content() {

				sb.append(new AdminTable("Signup page content"));
				sb.append("<tr><td>This content will appear on the page where users sign up for the SMS bulletin(s)</td></tr>");
				sb.append("<tr><td>"
						+ new TextAreaTag(context, "content", smsSettings.getSignupContent()).setId("content").setStyle(
								"width: 100%; height: 320px;") + "</td></tr>");
				sb.append("</table>");
			}

			private void export() {

				sb.append(new AdminTable("Export"));
				sb.append(new AdminRow("Export registrations", "Click here to export the list of registered numbers in excel / CSV format.",
						new ButtonTag(SmsSettingsHandler.class, "export", "Export registrations")));
				sb.append("</table>");
			}

			private void gateway() {

				sb.append(new AdminTable("Gateway"));

				SelectTag tag = new SelectTag(context, "gateway", smsSettings.getGateway());
				tag.addOptions(SmsGateway.values());

				sb.append(new AdminRow("Sms gateway", "Choose your sms network provider.", tag));

				if (smsSettings.getGateway() == SmsGateway.BulkSms)
					bulksms();

				if (smsSettings.getGateway() == SmsGateway.Clickatell)
					clickatell();

				sb.append("</table>");
			}

			private void registrations() {

				sb.append(new AdminTable("Registrations"));

				sb.append(new AdminRow("Add registrations", "Enter the mobile number you want to register.", "" + new TextTag(context, "numberAdd", 20)
						+ new SubmitTag("Add registration") + new ErrorTag(context, "numberAdd", "<br/>")));

				sb.append(new AdminRow("Remove registrations", "Enter the mobile number you want to unregister.", ""
						+ new TextTag(context, "numberRemove", 20) + new SubmitTag("Remove registration") + new ErrorTag(context, "numberRemove", "<br/>")));

				sb
						.append(new AdminRow("Export", "Export all registrations in CSV format.", new ButtonTag(SmsSettingsHandler.class, "export",
								"Export")));

				sb.append(new AdminRow("Import", "Import registrations from text file.", new FileTag("upload") + " "
						+ new SubmitTag("submit", "Import")));

				sb.append("</table>");
			}

			@Override
			public String toString() {

                RendererUtil.tinyMce(context, sb, "content");

				sb.append(new FormTag(SmsSettingsHandler.class, "save", "multi"));

                commands();
                
				gateway();
				registrations();
				content();

				if (miscSettings.isAdvancedMode()) {
					sb.append(new OwnerAttributesPanel(context, smsSettings, new Link(SmsSettingsHandler.class)));
				}

				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() {

		smsSettings.setClickatellApiId(clickatellApiId);
		smsSettings.setClickatellUsername(clickatellUsername);
		smsSettings.setClickatellPassword(clickatellPassword);

		smsSettings.setBulkSmsPassword(bulkSmsPassword);
		smsSettings.setBulkSmsUsername(bulkSmsUsername);

		smsSettings.setGateway(gateway);

		smsSettings.setSignupContent(content);

		smsSettings.save();

		if (miscSettings.isAdvancedMode()) {
			// attributes
			if (newAttributes != null) {
				smsSettings.addAttributes(newAttributes, newAttributesSection, newAttributesPage);
			}
		}

		if (numberAdd != null) {
			try {
				smsSettings.addRegistration(numberAdd, SmsBulletin.get(context));
			} catch (ExistingRegistrationException e) {
				addError("This number is already registered");
			}
		}

		if (numberRemove != null) {
			smsSettings.removeRegistration(numberRemove, SmsBulletin.get(context));
		}

		if (upload != null) {

			File file = upload.getFile();
			SmsCsvImporter importer = new SmsCsvImporter(context);
			int n = importer.run(file);

			addMessage(n + " mobile numbers have been imported.");
		}

		addMessage("Sms settings have been updated");
		clearParameters();
		return main();
	}

}
