package org.sevensoft.ecreator.iface.admin.feeds;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;

import org.jdom.JDOMException;
import org.sevensoft.commons.httpc.HttpMethod;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.feeds.*;
import org.sevensoft.ecreator.model.feeds.annotations.Ftp;
import org.sevensoft.ecreator.model.feeds.annotations.HandlerClass;
import org.sevensoft.ecreator.model.feeds.annotations.Http;
import org.sevensoft.ecreator.model.feeds.categories.CategoryReference;
import org.sevensoft.ecreator.model.feeds.csv.FieldType;
import org.sevensoft.ecreator.model.misc.Interval;
import org.sevensoft.ecreator.model.system.messages.SystemMessage;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

import com.enterprisedt.net.ftp.FTPException;

/**
 * @author sks 22-Nov-2005 09:42:19
 * 
 */
public abstract class FeedHandler extends AdminHandler {

	private Upload		upload;
	private Category		category;
	private String		ftpPassword;
	private String		ftpHostname;
	private String		ftpUsername;
	private String		ftpFilename;
	private String		httpUrl;
    private int previewCount;

	/**
	 * A nickname for this feed
	 */
	private String		name;

	private FieldType		fieldType;

	private HttpMethod	httpMethod;
	private Interval		interval;

	private Class		feedClass;
	private String		ftpPath;
	private int			feedId;
	private String		ignoredCategories;
	private Category		root;
	private boolean		resetName;
    private boolean createCategories;
    private int	ftpPort;

    public FeedHandler(RequestContext context) {
		super(context);
	}

	public Object delete() throws ServletException {

		Feed feed = getFeed();
		if (feed == null) {
			return main();
		}

		feed.delete();
		return new FeedMenuHandler(context).main();
	}

	protected abstract Feed getFeed();

	@Override
	public Object main() throws ServletException {

		if (!Module.UserFeeds.enabled(context) && !isSuperman()) {
			return new SettingsMenuHandler(context).main();
		}

		final Feed feed = getFeed();
		if (feed == null) {
			return new FeedMenuHandler(context).main();
		}

		final Class handlerClass = feed.getClass().getAnnotation(HandlerClass.class).value();

		AdminDoc doc = new AdminDoc(context, user, "Configure feed #" + feed.getId() + " " + feed.getName(), null);
		doc.addBody(new Body() {

			private void categories() {

				sb.append(new AdminTable("Categories"));

				sb
						.append(new AdminRow(
								"Ignored categories",
								"Any items in these categories will be ignored. Only applies to new items being created - any existing items will not be affected.",
								new TextAreaTag(context, "ignoredCategories", StringHelper.implode(feed.getIgnoredCategories(), "\n", true),
										60, 4)));

				if (feed instanceof ImportFeed) {

					ImportFeed importFeed = (ImportFeed) feed;

					sb.append(new AdminRow("Root category", "Any categories created will be created inside this root category.", new SelectTag(
							context, "root", importFeed.getRoot(), Category.getCategoryOptions(context), null)));

					sb.append("<tr><th colspan='2'>Select the categories that any newly created items will be added to.</th></tr>");

					int n = 1;
					for (Category category : importFeed.getGenericCategories()) {

						sb.append(new AdminRow("Category " + n, null, category.getName() + " " +
								new ButtonTag(handlerClass, "removeCategory", "Remove", "category", category, "feed", feed)));
						n++;
					}

					SelectTag tag = new SelectTag(context, "category");
					tag.setAny("-None-");
					tag.addOptions(Category.getCategoryOptions(context));

					sb.append(new AdminRow("Category " + n, null, tag));

					sb.append(new AdminRow("Show category mappings",
							"Edit the associations between the category names in the feed, and the category names used by your site.",
							new ButtonTag(handlerClass, "mappings", "Submit", "feed", feed)));

				}

				sb.append("</table>");

			}

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update feed"));
				sb.append(new ButtonTag(handlerClass, "delete", "Delete feed", "feed", feed)
						.setConfirmation("Are you sure you want to delete this feed?"));
				sb.append(new ButtonTag(handlerClass, null, "Return to menu"));
				sb.append("</div>");
			}

			private void http() {

				sb.append(new AdminTable("Http details"));

				SelectTag httpMethodTag = new SelectTag(context, "httpMethod", feed.getHttpMethod());
				httpMethodTag.addOptions(HttpMethod.values());

				sb.append(new AdminRow("Http", "Details of the HTTP request to make to retrieve the feed data", "Url: " +
						new TextTag(context, "httpUrl", feed.getHttpUrl(), 40) + " Method" + httpMethodTag));

				sb.append("</table>");

			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Name", "Enter a name for this feed.", new TextTag(context, "name", feed.getName(), 40)));

				if (feed instanceof ImportFeed) {

					sb
							.append(new AdminRow(
									"Reset name field",
									"By default, name fields are set when the item is first created and then ignored on future updates. Set this and the feed will overwrite the name each time it runs.",
									new BooleanRadioTag(context, "resetName", ((ImportFeed) feed).isResetName())));

				}

				if (isSuperman()) {

					SelectTag intervalTag = new SelectTag(context, "interval", feed.getInterval());
					intervalTag.addOptions(Interval.values());
					sb.append(new AdminRow("Run interval", "Set how often to run this feed.", intervalTag));
				}

				sb.append("</table>");
			}

			private void ftp() {

				sb.append(new AdminTable("Ftp details"));

				sb.append(new AdminRow("Ftp hostname", "Enter the hostname of the FTP server and optional port number.", new TextTag(context,
						"ftpHostname", feed.getFtpHostname(), 30) +
						" Port " + new TextTag(context, "ftpPort", feed.getFtpPort(), 4)));

				sb.append(new AdminRow("Ftp auth", "Authentication details for the FTP server", "Username: " +
						new TextTag(context, "ftpUsername", feed.getFtpUsername(), 16) + " Password" +
						new TextTag(context, "ftpPassword", feed.getFtpPassword(), 16)));

				sb.append(new AdminRow("Ftp file", "Enter details of the file to download", "Path: " +
						new TextTag(context, "ftpPath", feed.getFtpPath(), 30) + " Filename: " +
						new TextTag(context, "ftpFilename", feed.getFtpFilename(), 20)));

				sb.append("</table>");

			}

			@Override
			public String toString() {

				sb.append(new FormTag(handlerClass, "save", "post"));
				sb.append(new HiddenTag("feed", feed));

                commands();
				general();

				specifics(sb);

				if (feed.getClass().getAnnotation(Ftp.class) != null) {
					ftp();
				}

				if (feed instanceof ImportFeed && feed.getClass().getAnnotation(Http.class) != null) {
					http();
				}

				categories();

				commands();

				sb.append("</form>");

				return sb.toString();
			}
		});
		return doc;
	}

	public Object mappings() {

		if (!Module.Feeds.enabled(context) && !isSuperman()) {
			return new DashboardHandler(context).main();
		}

		final ImportFeed importFeed = (ImportFeed) getFeed();

		AdminDoc doc = new AdminDoc(context, user, "Category mappings", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to feed"));
				sb.append("</div>");
			}

			private void feeds() {

				sb.append(new AdminTable("Feeds"));

				sb.append("<tr>");
				sb.append("<th>Name</th>");
				sb.append("<th>Category</th>");
				sb.append("<th>Edit</th>");
				sb.append("</tr>");

				for (CategoryReference cm : importFeed.getCategoryReferences()) {

					sb.append("<tr>");
					sb.append("<td>" + cm.getName() + "</td>");
					sb.append("<td>" + cm.getCategory().getFullName(" > ") + "</td>");
					sb.append("<td>" + new ButtonTag(CategoryMappingHandler.class, null, "Edit", "categoryMapping", cm) + "</td>");
					sb.append("</tr>");
				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				feeds();
				commands();

				return sb.toString();
			}
		});
		return doc;
	}

	public Object moveDown() throws ServletException {

		Feed feed = getFeed();
		if (feed == null) {
			return main();
		}

		EntityUtil.moveDown(feed, Feed.getAll(context));
		return new FeedMenuHandler(context).main();
	}

	public Object moveUp() throws ServletException {

		Feed feed = getFeed();
		if (feed == null)
			return main();

		EntityUtil.moveUp(feed, Feed.getAll(context));
		return new FeedMenuHandler(context).main();
	}

	public Object removeCategory() throws ServletException {

		final Feed feed = getFeed();

		if (feed == null) {
			return main();
		}

		if (feed instanceof ImportFeed) {
			((ImportFeed) feed).removeGenericCategory(category);
		}

		return main();
	}

	/**
	 * Run this feed
	 */
	public Object run() throws ServletException {

		final Feed feed = getFeed();

		if (feed == null) {
			return main();
		}

		try {

			FeedResult result;

            if (upload == null) {
                if (feed instanceof ExportFeed)
                    ((ExportFeed) feed).setPreviewCount(previewCount);
                
                result = feed.run(null);

			} else {

				File file = upload.getFile();
				logger.fine("[FeedHandler] file from upload=" + file);
				result = feed.run(file);
			}

			if (result == null) {

				addMessage("Feed has run successfully - no information returned");
				new SystemMessage(context, "The " + feed.getName() + " feed was manually run by " + user.getName() + ".");

			} else {

				addMessage("Feed has run successfully - " + result.getDetails());
				new SystemMessage(context, "The " + feed.getName() + " feed manually run by " + user.getName() + ". " + result.getDetails());
			}

			feed.setLastRuntime();

		} catch (IOException e) {
			addError(e);
			e.printStackTrace();

		} catch (FTPException e) {
			addError(e);
			e.printStackTrace();

		} catch (FeedException e) {
			addError(e);
			e.printStackTrace();

		} catch (JDOMException e) {
			addError(e);
			e.printStackTrace();
		}

		clearParameters();
		return new FeedMenuHandler(context).main();
	}

	public Object save() throws ServletException {

		if (!Module.UserFeeds.enabled(context) && !isSuperman()) {
			return main();
		}

		final Feed feed = getFeed();
		if (feed == null) {
			return main();
		}

		feed.setName(name);

		/*
		 * Auto connection
		 */
		feed.setFtpFilename(ftpFilename);
		feed.setFtpUsername(ftpUsername);
		feed.setFtpHostname(ftpHostname);
		feed.setFtpPassword(ftpPassword);
		feed.setFtpPath(ftpPath);
		feed.setFtpPort(ftpPort);

		// htt[
		feed.setHttpUrl(httpUrl);
		feed.setHttpMethod(httpMethod);

		feed.setIgnoredCategories(StringHelper.explodeStrings(ignoredCategories, "\n"));

		// interval
		if (isSuperman()) {
			feed.setInterval(interval);
		}

		if (feed instanceof ImportFeed) {

			ImportFeed importFeed = (ImportFeed) feed;

			importFeed.setResetName(resetName);
            importFeed.setCreateCategories(createCategories);
            importFeed.setRoot(root);

			if (category != null) {
				importFeed.addGenericCategory(context, category);
			}
		}

		saveSpecific();

		feed.save();

		clearParameters();
		addMessage("This feed has been updated with your changes");
		return main();
	}

	protected abstract void saveSpecific();

	protected abstract void specifics(StringBuilder sb);

}
