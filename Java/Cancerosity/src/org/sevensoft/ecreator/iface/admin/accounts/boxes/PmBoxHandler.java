package org.sevensoft.ecreator.iface.admin.accounts.boxes;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.interaction.pm.boxes.PmBox;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 25 Jul 2006 00:04:18
 *
 */
@Path("admin-boxes-membermessages.do")
public class PmBoxHandler extends BoxHandler {

	private PmBox	box;

	public PmBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {
	}

}
