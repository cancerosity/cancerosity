package org.sevensoft.ecreator.iface.admin.media.videos;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.media.videos.Video;
import org.sevensoft.ecreator.model.media.videos.VideoOwner;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 14 Feb 2007 12:30:34
 *
 */
@Path("admin-videos.do")
public class VideoHandler extends AdminHandler {

	private Video		video;
	private VideoOwner	owner;
	private int			width;
	private int			height;
	private boolean		autoStart;
	private boolean		controls;

	public VideoHandler(RequestContext context) {
		super(context);
	}

	private Object handler(RequestContext context, VideoOwner owner) throws ServletException {

		if (owner instanceof Item) {
			return new ItemHandler(context, (Item) owner).edit();
		}

		if (owner instanceof Category) {
			return new org.sevensoft.ecreator.iface.admin.categories.CategoryHandler(context, (Category) owner).edit();
		}

		return null;
	}

	@Override
	public Object main() throws ServletException {

		if (video == null) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit video", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update video"));

				if (video.hasItem()) {
					sb.append(new ButtonTag(ItemHandler.class, "edit", "Return to item", "item", video.getOwner()));
				}

				else if (video.hasCategory()) {
					sb.append(new ButtonTag(CategoryHandler.class, "edit", "Return to category", "category", video.getOwner()));
				}

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("Video settings"));

				sb.append(new AdminRow("Dimensions", "Set the dimensions of the player for this video.", "Width "
						+ new TextTag(context, "width", video.getWidth(), 6) + " Height " + new TextTag(context, "height", video.getHeight(), 6)));

				sb.append(new AdminRow("Auto start", "Starts this video as soon as it has loaded.", new BooleanRadioTag(context, "autoStart", video
						.isAutoStart())));

				sb.append(new AdminRow("Controls", "Show the controls bar on the player (play, stop, rewind, etc).", new BooleanRadioTag(context,
						"controls", video.isControls())));

				sb.append("</table>");

			}

			@Override
			public String toString() {

				sb.append(new FormTag(VideoHandler.class, "save", "post"));
				sb.append(new HiddenTag("video", video));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object moveDown() throws ServletException {

		if (video == null) {
			return index();
		}

		owner = video.getOwner();
		List<Video> videos = owner.getVideos();
		EntityUtil.moveDown(video, videos);

		return handler(context, owner);
	}

	public Object moveUp() throws ServletException {

		if (video == null) {
			return index();
		}

		owner = video.getOwner();
		List<Video> videos = owner.getVideos();
		EntityUtil.moveUp(video, videos);

		return handler(context, owner);
	}

	public Object removeVideo() throws ServletException {

		if (video == null) {
			return index();
		}

		owner = video.getOwner();
		owner.removeVideo(video);

		return handler(context, owner);
	}

	public Object save() throws ServletException {

		if (video == null) {
			return index();
		}

		video.setWidth(width);
		video.setHeight(height);
		video.setControls(controls);
		video.setAutoStart(autoStart);
		video.save();

		return new ActionDoc(context, "This video has been updated", new Link(VideoHandler.class, null, "video", video));
	}
}
