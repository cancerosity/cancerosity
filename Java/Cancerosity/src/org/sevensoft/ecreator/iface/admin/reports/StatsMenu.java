package org.sevensoft.ecreator.iface.admin.reports;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.ecreator.iface.admin.reports.downloads.DownloadsCounterHandler;
import org.sevensoft.ecreator.iface.admin.reports.hits.page.PageHitsHandler;
import org.sevensoft.ecreator.iface.admin.reports.hits.site.SiteHitsHandler;
import org.sevensoft.ecreator.iface.admin.reports.members.LoginReportHandler;
import org.sevensoft.ecreator.iface.admin.reports.referrals.ReferralsHandler;
import org.sevensoft.ecreator.iface.admin.reports.search.SearchTermsHandler;
import org.sevensoft.ecreator.iface.admin.reports.submissions.PageSubmissionsHandler;
import org.sevensoft.ecreator.iface.admin.reports.submissions.SubmissionsHandler;
import org.sevensoft.ecreator.iface.admin.reports.visitors.VisitorsHandler;
import org.sevensoft.ecreator.iface.admin.stats.downloads.DownloadsBoughtHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Menu;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 8 Nov 2006 14:52:22
 *
 */
public class StatsMenu extends Menu {

	@Override
	public List<LinkTag> getLinkTags(RequestContext context) {
        if (Module.StatsBlock.enabled(context)){
            return null;
        }

		List<LinkTag> links = new ArrayList();

        links.add(new LinkTag(SiteHitsHandler.class, "recent", "Recent hits"));
		links.add(new LinkTag(SiteHitsHandler.class, "daily", "Hit totals daily"));
		links.add(new LinkTag(SiteHitsHandler.class, "monthly", "Hit totals monthly"));

		links.add(new LinkTag(ReferralsHandler.class, "recent", "Recent referrals"));
		links.add(new LinkTag(ReferralsHandler.class, "monthly", "Referral by month"));

		links.add(new LinkTag(SearchTermsHandler.class, "recent", "Recent search terms"));
		links.add(new LinkTag(SearchTermsHandler.class, "monthly", "Search terms by month"));

		links.add(new LinkTag(PageHitsHandler.class, "monthly", "Popular pages by month"));

		if (Module.Forms.enabled(context)) {

			links.add(new LinkTag(SubmissionsHandler.class, "recent", "Recent submissions"));
			links.add(new LinkTag(SubmissionsHandler.class, "monthly", "Submissions / month"));
            links.add(new LinkTag(PageSubmissionsHandler.class, "byCategory", "Submissions by page / category"));
		}

		if (Module.Accounts.enabled(context)) {

			links.add(new LinkTag(LoginReportHandler.class, null, "Logins"));
		}

		if (Attachment.hasAttachments(context)) {
			links.add(new LinkTag(DownloadsCounterHandler.class, "monthly", "Downloads by month"));
			links.add(new LinkTag(DownloadsCounterHandler.class, "weekly", "Downloads by week"));

			if (Module.AttachmentPayment.enabled(context)) {
				links.add(new LinkTag(DownloadsBoughtHandler.class, null, "Paid downloads by month"));
			}
		}

//        links.add(new LinkTag(VisitorsHandler.class, "recent", "Recent visitors"));
//        links.add(new LinkTag(VisitorsHandler.class, "daily", "Visitors totals daily"));
//		links.add(new LinkTag(VisitorsHandler.class, "monthly", "Visitors totals monthly"));

        return links;
	}

}
