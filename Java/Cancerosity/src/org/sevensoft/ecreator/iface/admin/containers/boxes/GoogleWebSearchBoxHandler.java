package org.sevensoft.ecreator.iface.admin.containers.boxes;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.containers.boxes.misc.GoogleWebSearchBox;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 24 Jul 2006 23:01:45
 *
 */
@Path("admin-boxes-googlewebsearch.do")
public class GoogleWebSearchBoxHandler extends BoxHandler {

	private GoogleWebSearchBox	box;

	public GoogleWebSearchBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {
	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

}
