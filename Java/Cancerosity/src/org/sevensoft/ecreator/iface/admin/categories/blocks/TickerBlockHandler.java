package org.sevensoft.ecreator.iface.admin.categories.blocks;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.iface.admin.search.SearchEditHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.items.highlighted.HighlightMethod;
import org.sevensoft.ecreator.model.items.highlighted.Ticker;
import org.sevensoft.ecreator.model.items.highlighted.TickerSettings;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupTag;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 26 Jun 2006 17:32:16
 *
 */
@Path("admin-blocks-ticker.do")
public class TickerBlockHandler extends BlockEditHandler {

	private Ticker		block;
	private HighlightMethod	method;
	private String		fontColor;
	private String		highlightBorderColor;
	private String		highlightFontColor;
	private String		font;
	private String		fontSize;
	private String		backgroundColor;
	private String		borderColor;
    private Markup markup;
    private transient TickerSettings tickerSettings;
    private int interval;

	public TickerBlockHandler(RequestContext context) {
		super(context);
        this.tickerSettings = TickerSettings.getInstance(context);
	}

	@Override
	protected Block getBlock() {
		return block;
	}

	private Object getHandler() throws ServletException {
		return new CategoryHandler(context, category).edit();
	}

	@Override
	protected String getTitle() {
		return "Edit ticker";
	}

	@Override
	protected void saveSpecific() {

/*
		block.setHighlightFontColor(highlightFontColor);
		block.setFontColor(fontColor);
		block.setFont(font);
		block.setFontSize(fontSize);
		block.setBackgroundColor(backgroundColor);
*/

        block.setMarkup(markup);

        tickerSettings.setInterval(interval);
        tickerSettings.save();

	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Ticker options"));

/*  //Deprecated
		sb.append(new AdminRow("Background color", "Set the color used as the background of the applet", new TextTag(context, "backgroundColor", block
				.getBackgroundColor())));

		sb.append(new AdminRow("Font", "Set the font used by the applet", new TextTag(context, "font", block.getFont())));

		sb.append(new AdminRow("Font size", "Set the size of the font", new TextTag(context, "fontSize", block.getFontSize())));

		sb.append(new AdminRow("Font color", "Set the color of the font", new TextTag(context, "fontColor", block.getFontColor())));

		sb.append(new AdminRow("Highlight font color", "Set the color of the font when you move your mouse over to click", new TextTag(context,
				"highlightFontColor", block.getHighlightFontColor())));
*/				

		if (miscSettings.isAdvancedMode() || isSuperman()) {

			sb.append(new AdminRow(miscSettings.isJimMode(), "Search", "The search controls how items are retrieved for this block.", new ButtonTag(
					SearchEditHandler.class, null, "Edit search", "search", block.getSearch())));

		}

		sb.append("</table>");


        sb.append(new AdminTable("Markups"));

        sb.append(new AdminRow(true, "Markup", "", new MarkupTag(context, "markup", block.getMarkup(), "- Default -")));
        sb.append(new AdminRow("Ticker interval", "This interval use all the tickers across the system. Altering its value will affect the other tickers",
                new TextTag(context, "interval", tickerSettings.getInterval(), 20)));

        sb.append("</table>");
	}
}
