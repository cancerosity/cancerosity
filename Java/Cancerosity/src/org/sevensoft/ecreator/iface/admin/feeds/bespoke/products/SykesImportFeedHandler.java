package org.sevensoft.ecreator.iface.admin.feeds.bespoke.products;

import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.model.feeds.Feed;
import org.sevensoft.ecreator.model.feeds.bespoke.products.sykescottages.SykesImportFeed;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;

/**
 * User: Tanya
 * Date: 17.08.2011
 */
@Path("admin-feeds-sykes.do")
public class SykesImportFeedHandler extends FeedHandler {
    private SykesImportFeed feed;

    private ItemType itemType;

    private boolean setupDone;
    // attributes
    private Attribute areaAttribute;
    private Attribute subareaAttribute;
    private Attribute latitudeAttribute;
    private Attribute longitudeAttribute;
    private Attribute priceinfoAttribute;
    private Attribute minpriceAttribute;
    private Attribute maxpriceAttribute;
    private Attribute bedroomsAttribute;
    private Attribute kettleAttribute;
    private Attribute parkingspaceAttribute;
    private Attribute smokingAttribute;
    private Attribute singlebedsAttribute;
    private Attribute doublebedsAttribute;
    private Attribute twinbedsAttribute;
    private Attribute bunkbedsAttribute;
    private Attribute familybedsAttribute;
    private Attribute maxsleepsAttribute;
    private Attribute proppostcodeAttribute;
    private Attribute propfeedbackAttribute;
    private Attribute tenniscourtAttribute;
    private Attribute petsAttribute;
    private Attribute changeoverdayAttribute;
    private Attribute jacuzziAttribute;
    private Attribute telephoneAttribute;
    private Attribute saunaAttribute;
    private Attribute linenprovidedAttribute;
    private Attribute towelsprovidedAttribute;
    private Attribute dishwasherAttribute;
    private Attribute cookerAttribute;
    private Attribute fridgeAttribute;
    private Attribute washingmachineAttribute;
    private Attribute tvAttribute;
    private Attribute highchairAttribute;
    private Attribute bathroomsAttribute;
    private Attribute cotsAttribute;
    private Attribute henstagAttribute;
    private Attribute childrenAttribute;
    private Attribute gamesroomAttribute;
    private Attribute pingpongAttribute;
    private Attribute snookerAttribute;
    private Attribute wifiAttribute;
    private Attribute webpriceAttribute;
    private Attribute furtherfacilitiesinternalAttribute;
    private Attribute regiondescAttribute;
    private Attribute towndescAttribute;
    private Attribute homesummaryAttribute;
    private Attribute homedescAttribute;
    private Attribute furtherfacilitiesexternalAttribute;
    private Attribute furtherdetailsAttribute;
    private Attribute specialconditionsAttribute;
    private Attribute amenityNotesAttribute;
    private Attribute countryAttribute;
    private Attribute villagenameAttribute;
    private Attribute sykesratingAttribute;
    private Attribute villageidAttribute;
    private Attribute weblinkAttribute;

    private Attribute photoAttribute;

    public SykesImportFeedHandler(RequestContext context) {
        super(context);
    }

    protected Feed getFeed() {
        return feed;
    }

    protected void saveSpecific() {
        feed.setItemType(itemType);
        feed.setSetupDone(setupDone);

        feed.setAreaAttribute(areaAttribute);
        feed.setSubareaAttribute(subareaAttribute);
        feed.setLatitudeAttribute(latitudeAttribute);
        feed.setLongitudeAttribute(longitudeAttribute);
        feed.setPriceinfoAttribute(priceinfoAttribute);
        feed.setMinpriceAttribute(minpriceAttribute);
        feed.setMaxpriceAttribute(maxpriceAttribute);
        feed.setBedroomsAttribute(bedroomsAttribute);
        feed.setKettleAttribute(kettleAttribute);
        feed.setParkingspaceAttribute(parkingspaceAttribute);
        feed.setSmokingAttribute(smokingAttribute);
        feed.setSinglebedsAttribute(singlebedsAttribute);
        feed.setDoublebedsAttribute(doublebedsAttribute);
        feed.setTwinbedsAttribute(twinbedsAttribute);
        feed.setBunkbedsAttribute(bunkbedsAttribute);
        feed.setFamilybedsAttribute(familybedsAttribute);
        feed.setMaxsleepsAttribute(maxsleepsAttribute);
        feed.setProppostcodeAttribute(proppostcodeAttribute);
        feed.setPropfeedbackAttribute(propfeedbackAttribute);
        feed.setTenniscourtAttribute(tenniscourtAttribute);
        feed.setPetsAttribute(petsAttribute);
        feed.setChangeoverdayAttribute(changeoverdayAttribute);
        feed.setJacuzziAttribute(jacuzziAttribute);
        feed.setTelephoneAttribute(telephoneAttribute);
        feed.setSaunaAttribute(saunaAttribute);
        feed.setLinenprovidedAttribute(linenprovidedAttribute);
        feed.setTowelsprovidedAttribute(towelsprovidedAttribute);
        feed.setDishwasherAttribute(dishwasherAttribute);
        feed.setCookerAttribute(cookerAttribute);
        feed.setFridgeAttribute(fridgeAttribute);
        feed.setWashingmachineAttribute(washingmachineAttribute);
        feed.setTvAttribute(tvAttribute);
        feed.setHighchairAttribute(highchairAttribute);
        feed.setBathroomsAttribute(bathroomsAttribute);
        feed.setCotsAttribute(cotsAttribute);
        feed.setHenstagAttribute(henstagAttribute);
        feed.setChildrenAttribute(childrenAttribute);
        feed.setGamesroomAttribute(gamesroomAttribute);
        feed.setPingpongAttribute(pingpongAttribute);
        feed.setSnookerAttribute(snookerAttribute);
        feed.setWifiAttribute(wifiAttribute);
        feed.setWebpriceAttribute(webpriceAttribute);
        feed.setFurtherfacilitiesinternalAttribute(furtherfacilitiesinternalAttribute);
        feed.setRegiondescAttribute(regiondescAttribute);
        feed.setTowndescAttribute(towndescAttribute);
        feed.setHomesummaryAttribute(homesummaryAttribute);
        feed.setHomedescAttribute(homedescAttribute);
        feed.setFurtherfacilitiesexternalAttribute(furtherfacilitiesexternalAttribute);
        feed.setFurtherdetailsAttribute(furtherdetailsAttribute);
        feed.setSpecialconditionsAttribute(specialconditionsAttribute);
        feed.setAmenityNotesAttribute(amenityNotesAttribute);
        feed.setCountryAttribute(countryAttribute);
        feed.setVillagenameAttribute(villagenameAttribute);
        feed.setSykesratingAttribute(sykesratingAttribute);
        feed.setVillageidAttribute(villageidAttribute);
        feed.setWeblinkAttribute(weblinkAttribute);
        feed.setPhotoAttribute(photoAttribute);
    }

    protected void specifics(StringBuilder sb) {
        sb.append(new AdminTable("Sykes cottages specifics"));

        sb.append(new AdminRow("Setup Done", "Set YES if you don't want attributes to be created automatically",
                new BooleanRadioTag(context, "setupDone", feed.isSetupDone())));

        sb.append(new AdminRow("Item type", "Select the item type to import to.", new SelectTag(context, "itemType", feed.getItemType(), ItemType
                .getSelectionMap(context), "-None set-")));

        if (feed.hasItemType()) {

            SelectTag attributeTag = new SelectTag(context, "areaAttribute", feed.getAreaAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Area Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "subareaAttribute", feed.getSubareaAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Subarea Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "latitudeAttribute", feed.getLatitudeAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Latitude Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "longitudeAttribute", feed.getLongitudeAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Longitude Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "priceinfoAttribute", feed.getPriceinfoAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Price info Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "minpriceAttribute", feed.getMinpriceAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Min price Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "maxpriceAttribute", feed.getMaxpriceAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Max price Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "bedroomsAttribute", feed.getBedroomsAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Bedrooms Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "kettleAttribute", feed.getKettleAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Kettle Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "parkingspaceAttribute", feed.getParkingspaceAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Parking space Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "smokingAttribute", feed.getSmokingAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Smoking Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "singlebedsAttribute", feed.getSinglebedsAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Single beds Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "doublebedsAttribute", feed.getDoublebedsAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Double beds Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "twinbedsAttribute", feed.getTwinbedsAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Twin beds Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "bunkbedsAttribute", feed.getBunkbedsAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Bunk beds Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "familybedsAttribute", feed.getFamilybedsAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Family beds Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "maxsleepsAttribute", feed.getMaxsleepsAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Max sleeps Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "proppostcodeAttribute", feed.getProppostcodeAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Postcode));
            sb.append(new AdminRow("Property postcode Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "propfeedbackAttribute", feed.getPropfeedbackAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Property feedback Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "tenniscourtAttribute", feed.getTenniscourtAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Tennis court Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "petsAttribute", feed.getPetsAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Pets Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "changeoverdayAttribute", feed.getChangeoverdayAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Selection));
            sb.append(new AdminRow("Change over day Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "jacuzziAttribute", feed.getJacuzziAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Jacuzzi Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "telephoneAttribute", feed.getTelephoneAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Telephone Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "saunaAttribute", feed.getSaunaAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Sauna Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "linenprovidedAttribute", feed.getLinenprovidedAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Linen provided Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "towelsprovidedAttribute", feed.getTowelsprovidedAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Towels provided Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "dishwasherAttribute", feed.getDishwasherAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Dishwasher Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "cookerAttribute", feed.getCookerAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Cooker Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "fridgeAttribute", feed.getFridgeAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Fridge Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "washingmachineAttribute", feed.getWashingmachineAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Washing machine Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "tvAttribute", feed.getTvAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("TV Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "highchairAttribute", feed.getHighchairAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("High chair Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "bathroomsAttribute", feed.getBathroomsAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("VillageID Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "cotsAttribute", feed.getCotsAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Cots Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "henstagAttribute", feed.getHenstagAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Henstag Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "childrenAttribute", feed.getChildrenAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Children Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "gamesroomAttribute", feed.getGamesroomAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Games room Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "pingpongAttribute", feed.getPingpongAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Pingpong Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "snookerAttribute", feed.getSnookerAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Snooker Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "wifiAttribute", feed.getWifiAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Boolean));
            sb.append(new AdminRow("Wifi Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "webpriceAttribute", feed.getWebpriceAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Webprice Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "furtherfacilitiesinternalAttribute", feed.getFurtherfacilitiesinternalAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Further facilities internal Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "regiondescAttribute", feed.getRegiondescAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Region description Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "towndescAttribute", feed.getTowndescAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Town description Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "homesummaryAttribute", feed.getHomesummaryAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Home summary Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "homedescAttribute", feed.getHomedescAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Home description Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "furtherfacilitiesexternalAttribute", feed.getFurtherfacilitiesexternalAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Further facilities external Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "furtherdetailsAttribute", feed.getFurtherdetailsAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Further details Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "specialconditionsAttribute", feed.getSpecialconditionsAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Special conditions Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "amenityNotesAttribute", feed.getAmenityNotesAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Amenity Notes Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "countryAttribute", feed.getCountryAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Country Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "villagenameAttribute", feed.getVillagenameAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("Village name Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "sykesratingAttribute", feed.getSykesratingAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Selection));
            sb.append(new AdminRow("Sykes Rating Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "villageidAttribute", feed.getVillageidAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Text));
            sb.append(new AdminRow("VillageID Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "weblinkAttribute", feed.getWeblinkAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Link));
            sb.append(new AdminRow("Weblink Attribute", "", attributeTag));

            attributeTag = new SelectTag(context, "photoAttribute", feed.getWeblinkAttribute());
            attributeTag.setAny("-None-");
            attributeTag.addOptions(feed.getItemType().getAttributes(AttributeType.Image));
            sb.append(new AdminRow("Photos Attribute", "", attributeTag));


        }

        sb.append("</table>");
    }
}
