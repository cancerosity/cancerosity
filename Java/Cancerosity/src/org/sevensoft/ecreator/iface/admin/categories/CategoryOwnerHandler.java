package org.sevensoft.ecreator.iface.admin.categories;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.ecom.delivery.DeliveryOptionsHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.ecom.delivery.DeliveryOption;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 2 May 2006 16:13:23
 *
 */
@Path("admin-categories-owner.do")
public class CategoryOwnerHandler extends AdminHandler {

	private Item		item;
	private Category		category;
	private DeliveryOption	deliveryBand;

	public CategoryOwnerHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {
		return new UnsupportedOperationException();
	}

	public Object removeCategory() throws ServletException {

		if (category == null) {
			return index();
		}

		if (item != null) {
			item.removeCategory(category);
			return new ItemHandler(context, item).edit();
		}

		if (deliveryBand != null) {
			deliveryBand.removeCategory(category);
			return new DeliveryOptionsHandler(context, deliveryBand).edit();
		}

		clearParameters();
		throw new ServletException("No category owner");
	}
}
