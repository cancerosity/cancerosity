package org.sevensoft.ecreator.iface.admin.items.search;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.EcreatorHandler;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 7 Jun 2006 15:36:41
 * 
 * Searches for items and returns a list of matching items
 *
 */
@Path("admin-items-search-ajax.do")
public class ItemSearchAjaxHandler extends EcreatorHandler {

	private String	searchText;
	private String	link;
	private String	param;
	private String	name;

	public ItemSearchAjaxHandler(RequestContext context) {
		super(context);
	}

	/**
	 * Returns search results of items as a csv text output
	 */
	public Object csv() {

		ItemSearcher searcher = new ItemSearcher(context);
		searcher.setStatus("Live");
		searcher.setKeywords(name);
		searcher.setLimit(20);

		List<Item> items = searcher.getItems();

		StringBuilder sb = new StringBuilder();
		for (Item item : items) {

			sb.append(item.getId());
			sb.append(",");
			sb.append(item.getName());

			sb.append("\n");
		}

		return sb.toString();
	}

	@Override
	public Object main() throws ServletException {

		ItemSearcher searcher = new ItemSearcher(context);
		searcher.setStatus("Live");
		searcher.setKeywords(searchText);
		searcher.setLimit(21);

		List<Item> items = searcher.getItems();

		StringBuilder sb = new StringBuilder();

		boolean max = false;
		if (items.size() > 20) {
			max = true;
			items = items.subList(0, 20);
		}

		for (Item item : items) {

			String string = link + "&" + param + "=" + item.getId();
			sb.append("<div>" + new LinkTag(string, item.getName()) + "</div>");

		}

		if (max) {
			sb.append("<div>There are more than 20 matches, "
					+ "if you do not see the item you are looking for, try searching again with more specific keywords.</div>");
		}

		return sb.toString();
	}

	@Override
	protected boolean runSecure() {
		return false;
	}

}
