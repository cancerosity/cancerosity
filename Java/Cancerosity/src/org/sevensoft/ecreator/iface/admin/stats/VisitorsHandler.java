package org.sevensoft.ecreator.iface.admin.stats;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.reports.StatsMenu;
import org.sevensoft.ecreator.iface.admin.stats.panels.SessionsPanel;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 10 Apr 2007 12:18:48
 *
 */
@Path("admin-stats-visitors.do")
public class VisitorsHandler extends AdminHandler {

	public VisitorsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Online and recent visitors", Tab.Stats);
		doc.setMenu(new StatsMenu());
		doc.addBody(new SessionsPanel(context));
		return doc;
	}

}
