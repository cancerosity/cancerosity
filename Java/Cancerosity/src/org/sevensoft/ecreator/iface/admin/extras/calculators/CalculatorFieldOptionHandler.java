package org.sevensoft.ecreator.iface.admin.extras.calculators;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.extras.calculators.CalculatorField;
import org.sevensoft.ecreator.model.extras.calculators.CalculatorFieldOption;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks
 */
@Path("admin-calculators-fields-options.do")
public class CalculatorFieldOptionHandler extends AdminHandler {

	private String			value;
	private String			text;
	private CalculatorFieldOption	option;
	private CalculatorField		field;
	private String			expression;

	public CalculatorFieldOptionHandler(RequestContext context) {
		super(context);
	}

	public Object delete() throws ServletException {

		if (option == null)
			return main();

		field = option.getField();
		field.removeOption(option);

		return new CalculatorFieldHandler(context, field).main();
	}

	public Object main() throws ServletException {

		if (option == null)
			return new CalculatorHandler(context).main();

		AdminDoc page = new AdminDoc(context, user, "Edit calculator field option", Tab.Extras);
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update option"));
				sb.append(new ButtonTag(CalculatorFieldOptionHandler.class, "delete", "Delete this option", "option", option)
						.setConfirmation("Are you sure you want to delete this option?"));

				sb.append(new ButtonTag(CalculatorFieldHandler.class, null, "Return to field", "field", option.getField()));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new TableTag("form", 1, 0).setCaption("General"));

				sb.append("<tr><td width='300'><b>Text</b><br/>The text to show for this option.</td>");
				sb.append("<td>" + new TextTag(context, "text", option.getText(), 40) + "</td>");

				sb.append("<tr><td width='300'><b>Value</b><br/>The value of this option when it is selected.</td>");
				sb.append("<td>" + new TextTag(context, "value", option.getValue(), 4) + "</td>");

				sb.append("<tr><td width='300'><b>Expression</b><br/>An expression to be evaluated when this option is selected.</td>");
				sb.append("<td>" + new TextTag(context, "expression", option.getExpression(), 40) + "</td>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(CalculatorFieldOptionHandler.class, "save", "post"));
				sb.append(new HiddenTag("option", option));

                commands();
				general();
				commands();

				sb.append("</form>");
				return sb.toString();
			}

		});

		return page;
	}

	public Object moveDown() throws ServletException {

		if (option == null)
			return main();

		field = option.getField();
		EntityUtil.moveDown(field, field.getOptions());

		return new CalculatorFieldHandler(context, field).main();
	}

	public Object moveUp() throws ServletException {

		if (option == null)
			return main();

		field = option.getField();
		EntityUtil.moveUp(field, field.getOptions());

		return new CalculatorFieldHandler(context, field).main();
	}

	public Object save() throws ServletException {

		if (option == null)
			return main();

		option.setValue(value);
		option.setText(text);
		option.setExpression(expression);
		option.save();

		clearParameters();
		return main();
	}

}