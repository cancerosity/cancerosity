package org.sevensoft.ecreator.iface.admin.ecom.bookings;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.ecom.bookings.menu.BookingMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.bookings.Booking;
import org.sevensoft.ecreator.model.bookings.Booking.Sort;
import org.sevensoft.ecreator.model.bookings.BookingSearcher;
import org.sevensoft.ecreator.model.bookings.BookingSettings;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.EmailTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import java.util.List;

/**
 * @author sks 13-Jul-2005 16:24:50
 */
@Path("admin-bookings-search.do")
public class BookingSearchHandler extends AdminHandler {

    private final transient BookingSettings bookingSettings;
    private String name;
    private transient List<Booking> bookings;
    private String postcode;
    private Sort sort;
    private String email;
    private String address;
    private String status;
    private String bookingId;

    public BookingSearchHandler(RequestContext context) {
        super(context);
        this.bookingSettings = BookingSettings.getInstance(context);
    }

    @Override
    public Object main() {

        if (bookings == null) {

            BookingSearcher searcher = new BookingSearcher(context);
            searcher.setSort(Booking.Sort.Newest);
            searcher.setLimit(30);

            bookings = searcher.execute();
        }

        AdminDoc doc = new AdminDoc(context, user, "Bookings overview", Tab.Bookings);
        doc.setMenu(new BookingMenu());
        doc.setIntro("Search for bookings using the search form below");
        doc.addBody(new Body() {

            private void results() {

                if (bookings == null) {
                    return;
                }

                sb.append(new FormTag(BookingSearchHandler.class, "export", "post"));
                sb.append(new AdminTable("Viewing bookings"));

                sb.append("<tr>");
                sb.append("<th>Booking No</th>");
                sb.append("<th>Date placed</th>");
                sb.append("<th>Customer</th>");
                sb.append("<th>Status</th>");
                sb.append("<th>Item</th>");
                sb.append("<th>Booking dates</th>");
                sb.append("</tr>");

                int n = 0;
                for (Booking booking : bookings) {

                    sb.append("<tr>");
                    sb.append("<td>" + new LinkTag(BookingHandler.class, null, booking.getIdString(), "booking", booking) + "</td>");
                    sb.append("<td>" + booking.getDatePlaced().toString("dd-MMM-yy") + "</td>");
                    sb.append("<td>" + booking.getAccount().getName());

                    if (booking.getAccount().hasEmail()) {
                        sb.append("<br/>");
                        sb.append(new EmailTag(booking.getAccount().getEmail()));
                    }

                    sb.append("</td>");
                    sb.append("<td>" + booking.getStatus() + "</td>");
                    sb.append("<td>" + booking.getItem().getName() + "</td>");
                    sb.append("<td>" + booking.getStart().toString("dd-MMM-yyyy") + " to " + booking.getEnd().toString("dd-MMM-yyyy") + "</td>");
                    sb.append("</tr>");

                    n++;
                }

                sb.append("</table>");
                sb.append("</form>");
            }

            private void search() {

                sb.append(new AdminTable("Booking search"));
                sb.append(new FormTag(BookingSearchHandler.class, "search", "POST"));

                SelectTag statusTag = new SelectTag(context, "status");
                statusTag.setAny("-Any status-");
                statusTag.addOptions(bookingSettings.getStatuses());

                SelectTag sortTag = new SelectTag(context, "sort", Booking.Sort.Newest);
                sortTag.addOptions(Booking.Sort.values());

                sb.append(new AdminRow("Status", "Set the status of bookings to search for.", statusTag));
                sb.append(new AdminRow("Booking No", "Search for a specific booking id.", new TextTag(context, "bookingId", 12)));
                sb.append(new AdminRow("Name", "Search by customer name.", new TextTag(context, "name", 40)));
                sb.append(new AdminRow("Email", "Search by customer email address.", new TextTag(context, "email", 40)));
                sb.append(new AdminRow("Address", "Search by booking address.", new TextTag(context, "address", 40)));
                sb.append(new AdminRow("Postcode", "Search by booking postcode.", new TextTag(context, "postcode", 40)));

                sb.append("<tr><th width='300'><b>Sort by</b></th><td>" + sortTag + "</td></tr>");
                sb.append("<tr><th width='300'></th><td>" + new SubmitTag("Search") + "</td></tr>");

                sb.append("</form>");
                sb.append("</table>");

            }

            @Override
            public String toString() {

                search();
                results();

                return sb.toString();
            }

        });

        return doc;
    }

    public Object search() {

        BookingSearcher searcher = new BookingSearcher(context);

        searcher.setSort(Booking.Sort.Newest);
        searcher.setPostcode(postcode);
        searcher.setName(name);
        searcher.setEmail(email);
        searcher.setAddress(address);
        searcher.setSort(sort);
        searcher.setLimit(30);
        searcher.setBookingId(bookingId);
        searcher.setStatus(status);

        bookings = searcher.execute();

        return main();
    }
}
