package org.sevensoft.ecreator.iface.admin.crm.forms;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.collections.bags.ArrayBag;
import org.sevensoft.commons.collections.bags.Bag;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.search.AbstractSearcher;
import org.sevensoft.ecreator.iface.admin.attachments.AdminAttachmentsPanel;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.items.modules.stock.ReportDoc;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.FlagGif;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.crm.forms.submission.Submission;
import org.sevensoft.ecreator.model.crm.forms.submission.SubmissionData;
import org.sevensoft.ecreator.model.crm.forms.submission.SubmissionLine;
import org.sevensoft.ecreator.model.crm.forms.submission.SubmissionLineOption;
import org.sevensoft.ecreator.model.crm.messages.panels.MessagesPanel;
import org.sevensoft.ecreator.model.crm.messages.panels.MessagesReplyPanel;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.db.EntityObject;

/**
 * @author sks 4 Aug 2006 09:40:42
 *
 */
@Path("admin-forms-submissions.do")
public class SubmissionHandler extends AdminHandler {

    private static final int LIMIT = 50;

	private Submission	submission;

    private int				page;

	public SubmissionHandler(RequestContext context) {
		super(context);
	}

	public SubmissionHandler(RequestContext context, Submission submission) {
		super(context);
		this.submission = submission;
	}

	public Object delete() throws ServletException {

		if (submission == null) {
			return main();
		}

		submission.log("Deleted");

		submission.delete();
		return main();
	}

	@Override
	public Object main() throws ServletException {

        final AbstractSearcher searcher = SubmissionSearcher.getSearcher(context);
		int submCount = searcher.size();
    //    final List<Submission> submissions = Submission.get(context);
		if (submCount == 0) {

			AdminDoc doc = new AdminDoc(context, user, "Form submissions", Tab.Extras);
			if (miscSettings.isAdvancedMode()) {
				doc.setMenu(new ExtrasMenu());
			}
			doc.addBody(new Body() {

				@Override
				public String toString() {

					sb.append(new TableTag("simplelinks", "center"));
					sb.append("<tr><th align='center'>There are no form submissions to view at the moment</th></tr>");
					sb.append("</table>");

					return sb.toString();
				}

			});
			return doc;

		}

        final Results results = new Results(submCount, page, LIMIT);

		searcher.setStart(results.getStartIndex());
		searcher.setLimit(LIMIT);

		final List<EntityObject> submissions = searcher.execute();

		final List<Form> forms = Form.get(context);

		AdminDoc doc = new AdminDoc(context, user, "Form submissions", Tab.Extras);
		if (miscSettings.isAdvancedMode()) {
			doc.setMenu(new ExtrasMenu());
		}

		doc.addBody(new Body() {

			private void submissions() {

                sb.append(new ResultsControl(context, results, new Link(SubmissionHandler.class)));

                sb.append(new AdminTable("Submissions"));
				sb.append("<tr><td colspan='6'>These the forms that have been completed by users on your site</td></tr>");

				sb.append("<tr>");
				sb.append("<th>ID</th>");
				sb.append("<th>Date</th>");
				sb.append("<th width='10'> </th>");

				if (forms.size() > 1) {
					sb.append("<th>Form</th>");
				}

				sb.append("<th>Page</th>");
				sb.append("<th width='10'> </th>");
				sb.append("</tr>");

				for (Object obj : submissions) {
                    Submission submission = (Submission)obj; 
					sb.append("<tr>");

					sb.append("<td>" + new LinkTag(SubmissionHandler.class, "view", submission.getId(), "submission", submission) + "</td>");
					sb.append("<td>" + submission.getDate().toString("dd-MMM-yyyy HH:mm") + "</td>");
					sb.append("<td>");
					if (submission.hasCountry()) {
						sb.append(new FlagGif(submission.getCountry()));
					}
					sb.append("</td>");

					if (forms.size() > 1) {

						sb.append("<td>");
						if (submission.hasName()) {
							sb.append(submission.getName());
						}
						sb.append("</td>");

					}

					sb.append("<td>");
					if (submission.hasPageName()) {
						sb.append(submission.getPageName());
					}
					sb.append("</td>");

					sb.append("<td width='10'>" +
							new LinkTag(SubmissionHandler.class, "delete", new DeleteGif(), "submission", submission)
									.setConfirmation("If you delete this submission it cannot be restored. Continue?") + "</td>");
				}

				sb.append("</table>");

                sb.append(new ResultsControl(context, results, new Link(SubmissionHandler.class)));
			}

			@Override
			public String toString() {

				submissions();

				return sb.toString();
			}

		});
		return doc;
	}

	public Object printer() throws ServletException {

		if (submission == null) {
			return index();
		}

		ReportDoc doc = new ReportDoc(context, "Submission #" + submission.getIdString());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("<div>Form: " + submission.getSubject() + "</div>");
				sb.append("<div>Id: " + submission.getId() + "</div>");
				sb.append("<div>Date received: " + submission.getDate().toString("dd-MMM-yyyy HH:mm") + "</div>");

				sb.append(new TableTag("Data"));

				for (SubmissionData data : submission.getData()) {
					sb.append("<tr><td>" + data.getName() + "</td><td>" + data.getValue() + "</td></tr>");
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object view() throws ServletException {

		if (submission == null) {
			return main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Form submission #" + submission.getId(), Tab.Extras);
		doc.setIntro("This form was submitted on " + submission.getDate().toString("dd MMM yyyy HH:mm"));
		if (miscSettings.isAdvancedMode()) {
			doc.setMenu(new ExtrasMenu());
		}
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new ButtonTag(SubmissionHandler.class, "delete", "Delete submission", "submission", submission)
						.setConfirmation("Are you sure you want to delete this submission?"));
				sb.append(new ButtonTag(SubmissionHandler.class, null, "Return to submissions"));
				sb.append("</div>");
			}

			private void data() {

				sb.append(new AdminTable("Data"));

				List<SubmissionLine> lines = submission.getLines();
				Bag<ItemType> bag = new ArrayBag();
				for (SubmissionLine line : lines) {

					Item item = line.getItem();

					bag.add(item.getItemType());

					StringBuilder sb2 = new StringBuilder();
					if (line.getQty() > 1) {
						sb2.append(line.getQty() + " @ ");
					}
					sb2.append(new LinkTag(ItemHandler.class, "edit", item.getName(), "item", item));

					List<String> list = new ArrayList<String>();
					for (SubmissionLineOption option : line.getOptions()) {
						list.add(option.getName() + ": " + option.getValue());
					}

					if (list.size() > 0) {

						sb2.append("<br/>");
						sb2.append(StringHelper.implode(list, ", "));
					}

					sb.append(new AdminRow(item.getItemType().getName() + " " + bag.size(item.getItemType()), sb2));

				}

				for (SubmissionData data : submission.getData()) {
					sb.append(new AdminRow(data.getName(), data.getValue()));
				}

				sb.append("</table>");
			}

			private void general() {

				sb.append(new LinkTag(SubmissionHandler.class, "printer", "Click here to print this submission", "submission", submission)
						.setTarget("_blank"));

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Form", "This is the name of the form that was submitted", submission.getSubject()));
				sb.append(new AdminRow("Id", "This is the automatically assigned ID number of this submission.", submission.getId()));
				sb.append(new AdminRow("Date received", "This is the date the form was completed by the user", submission.getDate().toString(
						"dd-MMM-yyyy HH:mm")));

				if (submission.hasPageName()) {

					String page;

					if (submission.hasItem()) {
						page = new LinkTag(ItemHandler.class, null, submission.getItem().getName(), "item", submission.getItem()).toString();
					} else if (submission.hasCategory()) {
						page = new LinkTag(CategoryHandler.class, null, submission.getCategory().getName(), "category", submission.getCategory())
								.toString();
					} else {
						page = submission.getPageName();
					}

					sb.append(new AdminRow("Page", "This is the page the form was submitted on.", page));
				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

                commands();
				general();
				data();

				if (submission.hasAttachments()) {
					sb.append(new AdminAttachmentsPanel(context, submission, false));
				}

				sb.append(new MessagesPanel(context, submission, false, false));
				sb.append(new MessagesReplyPanel(context, false, submission, null, true));

				commands();

				return sb.toString();
			}
		});
		return doc;
	}
}
