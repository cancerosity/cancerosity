package org.sevensoft.ecreator.iface.admin.items.modules.reviews;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemTypeMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.attributes.renderers.OwnerAttributesPanel;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.reviews.ReviewsModule;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;

/**
 * @author sks 11 Aug 2006 14:15:10
 *
 */
@Path("admin-reviews-module.do")
public class ReviewsModuleHandler extends AdminHandler {

	private ItemType	itemType;
	private String	newAttributes;
	private String	newAttributesSection;
	private int	newAttributesPage;

	public ReviewsModuleHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (itemType == null) {
			return new ItemTypeHandler(context).main();
		}

		final ReviewsModule reviewsModule = itemType.getReviewsModule();

		AdminDoc doc = new AdminDoc(context, user, "Edit reviews module", Tab.getItemTab(itemType));
		doc.setMenu(new EditItemTypeMenu(itemType));
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update reviews module"));
				sb.append(new ButtonTag(ItemTypeHandler.class, "edit", "Return to item type", "itemType", itemType));

				sb.append("</div>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(ReviewsModuleHandler.class, "save", "post"));
				sb.append(new HiddenTag("itemType", itemType));

                commands();
				sb.append(new OwnerAttributesPanel(context, reviewsModule));
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (itemType == null) {
			return new ItemTypeHandler(context).main();
		}

		final ReviewsModule reviewsModule = itemType.getReviewsModule();
		if (newAttributes != null) {
			reviewsModule.addAttributes(newAttributes, newAttributesSection, newAttributesPage);
		}

		addMessage("Reviews module updated");
		clearParameters();
		return main();
	}
}
