package org.sevensoft.ecreator.iface.admin.system.log;

import java.util.List;

import org.sevensoft.ecreator.model.misc.log.LogEntry;
import org.sevensoft.jeezy.db.QueryBuilder;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27 Mar 2007 10:38:09
 *
 */
public class LogSearcher {

	private final RequestContext	context;
	private int				limit;
	private int				start;
	private String			name;
	private String			ip;

	public LogSearcher(RequestContext context) {
		this.context = context;
	}

	public List<LogEntry> execute() {
		return (List<LogEntry>) run(false);
	}

	public final String getIp() {
		return ip;
	}

	public final int getLimit() {
		return limit;
	}

	public final String getName() {
		return name;
	}

	public final int getStart() {
		return start;
	}

	private Object run(boolean count) {

		QueryBuilder b = new QueryBuilder(context);
		if (count) {
			b.select("count(*)");
		} else {
			b.select("*");
		}

		b.from("#", LogEntry.class);

		if (name != null) {
			b.clause("name like ?", "%" + name + "%");
		}

		if (ip != null) {
			b.clause("ipAddress like ?", "%" + ip + "%");
		}

		b.order("time desc");

		if (count) {
			return b.toQuery().getInt();
		} else {
			return b.toQuery().execute(LogEntry.class, start, limit);
		}
	}

	public final void setIp(String ip) {
		this.ip = ip;
	}

	public final void setLimit(int limit) {
		this.limit = limit;
	}

	public final void setName(String name) {
		this.name = name;
	}

	public final void setStart(int start) {
		this.start = start;
	}

	public int size() {
		return (Integer) run(true);
	}

}
