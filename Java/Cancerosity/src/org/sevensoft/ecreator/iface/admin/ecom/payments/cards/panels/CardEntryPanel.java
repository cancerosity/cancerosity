package org.sevensoft.ecreator.iface.admin.ecom.payments.cards.panels;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.ecom.payments.cards.Card;
import org.sevensoft.ecreator.model.ecom.payments.cards.CardType;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 17 Apr 2007 13:42:20
 *
 */
public class CardEntryPanel {

	private final RequestContext	context;
	private Card			card;

	public CardEntryPanel(RequestContext context) {
		this.context = context;
	}

	public final void setCard(Card card) {
		this.card = card;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(new AdminTable("Card details"));

		TextTag cardHolderTag = new TextTag(context, "cardHolder", 40);
		if (card != null) {
			cardHolderTag.setValue(card.getCardHolder());
		}

		sb.append(new AdminRow("Card Holder", "Enter the name on the card exactly as it appears.", cardHolderTag + ""
				+ new ErrorTag(context, "cardHolder", "<br/>")));

		SelectTag tag = new SelectTag(context, "cardType");
		tag.addOptions(CardType.get());
		if (card != null) {
			tag.setValue(card.getCardType());
		}

		sb.append(new AdminRow("Card type", "Choose the type of card.", tag + "" + new ErrorTag(context, "cardType", "<br/>")));

		TextTag cardNumberTag = new TextTag(context, "cardNumber", 40);
		if (card != null) {
			cardNumberTag.setValue(card.getCardNumber());
		}

		sb.append(new AdminRow("Card number", "Enter the long card number without any spaces.", cardNumberTag + ""
				+ new ErrorTag(context, "cardNumber", "<br/>")));

		SelectTag month = new SelectTag(context, "startMonth");
		month.setAny("--");
		month.addOptions(Card.months);

		SelectTag year = new SelectTag(context, "startYear");
		year.setAny("--");
		year.addOptions(Card.startYears);

		sb.append(new AdminRow("Start date", "Enter the valid from or start date if present on the card.", month + " " + year + " "
				+ new ErrorTag(context, "startDate", "<br/>")));

		month = new SelectTag(context, "expiryMonth");
		month.addOptions(Card.months);

		year = new SelectTag(context, "expiryYear");
		year.addOptions(Card.expiryYears);

		sb.append(new AdminRow("Expiry date", "Expiry date of the card must be completed.", month + " " + year + " "
				+ new ErrorTag(context, "expiryDate", "<br/>")));

		TextTag issueNumberTag = new TextTag(context, "issueNumber", 3);
		if (card != null) {
			issueNumberTag.setValue(card.getIssueNumber());
		}

		sb.append(new AdminRow("Issue number", "Enter the issue number if present on the card.", issueNumberTag + ""
				+ new ErrorTag(context, "issueNumber", "<br/>")));

		TextTag cscTag = new TextTag(context, "csc", 5);
		if (card != null) {
			cscTag.setValue(card.getCscNumber());
		}

		sb.append(new AdminRow("Card security code",
				"The card security code is found on the reverse of the card as the last 3 digits on the signature strip.", cscTag + ""
						+ new ErrorTag(context, "csc", "<br/>")));

		sb.append("</table>");

		return sb.toString();
	}

}
