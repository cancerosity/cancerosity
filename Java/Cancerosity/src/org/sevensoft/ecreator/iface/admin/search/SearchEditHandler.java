package org.sevensoft.ecreator.iface.admin.search;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.attributes.AttributeHandler;
import org.sevensoft.ecreator.iface.admin.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeOption;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.ecreator.model.attributes.renderers.AAdminRenderer;
import org.sevensoft.ecreator.model.attributes.renderers.ARenderType;
import org.sevensoft.ecreator.model.attributes.renderers.input.AInputRenderer;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.interaction.LastActive;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.highlighted.HighlightMethod;
import org.sevensoft.ecreator.model.items.highlighted.blocks.HighlightedItemsBlock;
import org.sevensoft.ecreator.model.items.highlighted.boxes.HighlightedItemsBox;
import org.sevensoft.ecreator.model.items.sorts.ItemSort;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.ecreator.model.search.Search;
import org.sevensoft.ecreator.model.search.Search.FeaturedMethod;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

/**
 * 
 * This handler allows us to edit details of a particular saved search - used by search wrappers for example
 * 
 * @author sks 3 Oct 2006 09:59:37
 *
 */
@Path("admin-search-edit.do")
public class SearchEditHandler extends AdminHandler {

	private static final int			CategoryLimit	= 1000;

	private Search					search;
	private Date					joinedAfter;
	private String					status;
	private MultiValueMap<Attribute, String>	attributeValues;
	private SubscriptionLevel			subscription;
	private Category					searchCategory;
	private boolean					noImages;
	private String					name;
	private LastActive				lastActive;
	private String					keywords;
	private Date					createdAfter;
	private String					details;
	private int						age;
	private boolean					subcats;
	private Money					sellPriceTo;
	private Money					sellPriceFrom;
	private String					location;
	private int						limit;
	private boolean					noCategory;
	private boolean					imageOnly;
	private int						distance;
	private String					email;
	private ItemType					itemType;
	private String					startsWith;
	private ItemSort					sort;
	private int						joinedWithinDays;
	private Attribute					addAttribute;
	private boolean					recordingOnly;
	private Set<Integer>				itemTypes;
	private boolean					multipleItemTypes;
	private boolean					excludeActiveItemType;
	private boolean					excludeActiveItem;
	private Set<Integer>				itemTypeIds;
	private List<Attribute>				removeAttributeValues;
    private Attribute                   removeAttribute;
    private FeaturedMethod				featuredMethod;
    private boolean                     inStockOnly;

    private int						page;

	private SortType					sortType;

	private HighlightMethod				method;

    private Item searchAccount;

    private String searchAccounts;

    private ItemType itemTypeDisplayed;

    private boolean currDateWrapper;

    private boolean showOnlyChildren;
    private boolean showOnlyRelatives;

	public SearchEditHandler(RequestContext context) {
		this(context, null);
	}

	public SearchEditHandler(RequestContext context, Search search) {
		super(context);
		this.search = search;
	}

	@Override
	public Object main() throws ServletException {

		if (search == null) {
			return index();
		}

		final int count = Category.getCount(context);
		final Results results = new Results(count, page, CategoryLimit);
		final Map<String, String> categoryOptions = Category.getCategoryOptions(context, null, results.getStartIndex(), CategoryLimit);

		AdminDoc doc = new AdminDoc(context, user, "Edit search", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update search"));

				if (search.hasCategory()) {
					sb.append(new ButtonTag(CategoryHandler.class, "searchWrapper", "Return to category", "category", search.getCategory()));
				}

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("Search details"));

				sb.append(new AdminRow("Multiple item type search",
						"Configure this search for multiple item types (less flexible on attributes but wider results range).",
						new BooleanRadioTag(context, "multipleItemTypes", search.isMultipleItemTypes())));

				if (search.isMultipleItemTypes()) {

					StringBuilder sb2 = new StringBuilder();
					for (ItemType itemType : ItemType.get(context)) {

						sb2.append(new CheckTag(context, "itemTypeIds", itemType.getId(), search.getItemTypes().contains(itemType.getId())) + " "
								+ itemType.getName() + "<br/>");
					}

					sb.append(new AdminRow("Item types", "Select which item types we should search for", sb2));

				} else {

					sb.append(new AdminRow("Item type", "Set the item type this search will search for", new SelectTag(context, "itemType", search
							.getItemType(), ItemType.getSelectionMap(context), "-Select item type-")));

				}
                //todo display item types (accounts iTypes), items will be searched depending on what Item of the iType is displayed
                sb.append(new AdminRow("Item of Item type Displayed", "display items of Searched itemType(s) depending on which ItemType page is shown", new SelectTag(context, "itemTypeDisplayed", search
							.getItemTypeDisplayed(), ItemType.getSelectionMap(context), "-Select item type-")));

				sb.append(new AdminRow("Exclude active item type", "Exclude the item type that the current account is set to.", new BooleanRadioTag(
						context, "excludeActiveItemType", search.isExcludeAccountItemType())));

				sb.append(new AdminRow("Exclude active item", "Exclude the account that is currently logged in.", new BooleanRadioTag(context,
						"excludeActiveItem", search.isExcludeAccount())));

                sb.append(new AdminRow("Currently viewed account listings", "Show only those listing items of the chosen item type that are attached to the account of the currently viewed item.", new BooleanRadioTag(
                        context, "showOnlyChildren", search.isShowOnlyChildren())));

                sb.append(new AdminRow("Owner account related listings", "Show only listing items of the currently viewed item account owner.", new BooleanRadioTag(
                        context, "showOnlyRelatives", search.isShowOnlyRelatives())));

				if (categoryOptions != null && !categoryOptions.isEmpty()) {

					String control;

					if (results.hasMultiplePages()) {
						control = "<div align='center'>"
								+ new ResultsControl(context, results, new Link(SearchEditHandler.class, null, "search", search)) + "</div>";
					} else {
						control = "";
					}

					SelectTag catSelect = new SelectTag(context, "searchCategory", search.getSearchCategory(), categoryOptions, "-All categories");
					sb.append(new AdminRow("Category", "Limit searching to a particular category.", catSelect + control));

					if (search.hasSearchCategory()) {

						sb.append(new AdminRow("Include subcategories", "Include subcategories of the specified category when searching.",
								new BooleanRadioTag(context, "subcats", search.isSubcats())));
					}

				}

                sb.append(new AdminRow("Owner account", "Enter an account number to limit searches to only those items tagged to this account",
						/*new TextTag(context, "searchAccount", search.getSearchAccount(), 8)*/
                new TextAreaTag(context, "searchAccounts", StringHelper.implode(search.getSearchAccounts(), "\n"), 40, 5)));

				sb.append(new AdminRow("Age", "How long an item has been on the site (in days).", new TextTag(context, "age", search.getAge(), 4)));

				if (search.hasItemType()) {

					if (ItemModule.Pricing.enabled(context, search.getItemType())) {

						sb.append(new AdminRow("Sell price range", null, "From "
								+ new TextTag(context, "sellPriceFrom", search.getSellPriceFrom(), 12) + " to "
								+ new TextTag(context, "sellPriceTo", search.getSellPriceTo(), 12)));
					}
				}

				if (Module.Availabilitity.enabled(context)) {

					sb.append(new AdminRow("In stock only", "Limit search results to items which are in stock", new BooleanRadioTag(context,
							"inStockOnly", search.isInStockOnly())));
				}

				sb.append(new AdminRow("Email", "Search by member email address", new TextTag(context, "email", search.getEmail(), 40)));

				sb.append(new AdminRow("Last active", "Limit members by their last active date", new SelectTag(context, "lastActive", search
						.getLastActive(), LastActive.values(), "-Anytime-")));

				sb.append(new AdminRow("Joined within days", "Only search members who joined within the last X days", new TextTag(context,
						"joinedWithinDays", search.getJoinedWithinDays(), 4)));

				if (Module.Subscriptions.enabled(context)) {

					if (!search.isMultipleItemTypes()) {

						if (search.hasItemType()) {

							sb.append(new AdminRow("Subscription level", "Limit searching to accounts that are subscribed to this level only.",
									new SelectTag(context, "subscription", search.getSubscriptionLevel(), search.getItemType()
											.getSubscriptionModule().getSubscriptionLevels(), "-All subscriptions-")));

						}
					}
				}

				sb
						.append(new AdminRow("Keywords", "Keywords search in titles and contents", new TextTag(context, "keywords", search
								.getKeywords(), 40)));

				sb.append(new AdminRow("Details", "Details search in titles, contents and all attribute values", new TextTag(context, "details", search
						.getDetails(), 40)));

				TextTag limitTag = new TextTag(context, "limit", search.getLimit(), 4);
				sb.append(new AdminRow("Limit", "Limit search to a max number of results", limitTag));

				sb.append(new AdminRow("Images only", "Only show results that have an image", new BooleanRadioTag(context, "imageOnly", search
						.isImageOnly())));

				SelectTag selectTag = new SelectTag(context, "method", search.getMethod(), HighlightMethod.getItem(context), "-None selected-");
				sb.append(new AdminRow("Method", "Select what type of highlighted items to show.", selectTag));

				sb.append(new AdminRow("Featured", "Choose to dispaly featured, not featured, or any.", new SelectTag(context, "featuredMethod", search
						.getFeaturedMethod(), FeaturedMethod.values())));

				if (Module.UserplaneRecorder.enabled(context)) {

					sb.append(new AdminRow("Recording only", "Limit results to items which have a userplane video recording.", new BooleanRadioTag(
							context, "recordingOnly", search.isRecordingOnly())));

				}

				TextTag locationTag = new TextTag(context, "location", search.getLocation(), 40);
				sb.append(new AdminRow("Location", "Enter a place or postcode to center search", locationTag + " x=" + search.getX() + ", y="
						+ search.getY()));

				if (search.hasLocation()) {

					TextTag distanceTag = new TextTag(context, "distance", search.getDistance(), 4);
					sb.append(new AdminRow("Distance (miles)", "Enter a maximum distance from the location entered above", distanceTag));

				}

				if (search.hasItemType()) {

					SelectTag sortTag = new SelectTag(context, "sort", search.getSort());
					sortTag.setAny("-Default-");
					sortTag.addOptions(search.getItemType().getSorts());

					sb.append(new AdminRow("Sort", "Apply a specific item sort to this search.", sortTag));

				}

				sb.append(new AdminRow("Sort type", "Use a generic sort type in this search.", new SelectTag(context, "sortType", search.getSortType(),
						SortType.values(), "-Default-")));

                 sb.append(new AdminRow("Current Date", "Use current Date value for category Search wrapper", new BooleanRadioTag(context,
                        "currDateWrapper", search.isCurrDateWrapper())));

				// ADD ATTRIBUTE
				{

					List<Attribute> attributes = search.getAttributes();
					if (attributes.size() > 0) {
						sb.append("<tr><td colspan='2'>Add attribute: "
								+ new SelectTag(context, "addAttribute", null, attributes, "-Select attribute-") + "</td></tr>");
					}
				}
                MultiValueMap<String, Attribute> sections = AttributeUtil.getSectionsMap(search.getAttributeValues().keySet());

                for (String section : sections.keySet()) {
                    sb.append(new AdminTable(section == null ? "Details" : section));
                    for (Attribute attribute : sections.list(section)) {

                        String edit;
                        if (miscSettings.isAdvancedMode() || context.containsAttribute("superman")) {

                            edit = new LinkTag(AttributeHandler.class, "edit", new ImageTag("files/graphics/admin/spanner.gif").setAlign("absmiddle"),
                                    "attribute", attribute, "search", search)
                                    + " ";
                        } else {
                            edit = "";
                        }
                        edit += attribute.getName();
                        sb.append("<tr>");
                        sb.append("<td>" + edit + "</td>");

                        List<String> values = null;
                        values = search.getAttributeValues().list(attribute);
                        StringBuilder sb2 = new StringBuilder();
                        AInputRenderer ir = new AInputRenderer(context, attribute, values);
                        ir.setMulti(true);
                        sb2.append(ir);
                        sb2.append(new ErrorTag(context, attribute.getParamName(), "<br/>"));

                        sb.append("<td>" + sb2 + "</td>");

                        sb
                                .append("<td width='10'>" +
                                        new LinkTag(SearchEditHandler.class, "removeAttribute", new DeleteGif(), "removeAttribute", attribute, "search", search) + "</td>");
                        sb.append("</tr>");
                    }
                }
                sb.append("</table>");
//				AAdminRenderer r = new AAdminRenderer(context, search, search.getAttributeValues().keySet(), search.getAttributeValues(),
//						ARenderType.Input);
//				r.setMulti(true);
//
//				sb.append(r);


                sb.append("</table>");
            }

			@Override
			public String toString() {

				sb.append(new FormTag(SearchEditHandler.class, "save", "post"));
				sb.append(new HiddenTag("search", search));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	/**
	 * This mehod is called when a user is adding another attribute 
	 */
	public Object save() throws ServletException {

		if (search == null) {
			return index();
		}

		search.setAge(age);
		search.setCreatedAfter(createdAfter);
		search.setDetails(details);
		search.setDistance(distance);

		search.setExcludeAccount(excludeActiveItem);
		search.setExcludeAccountItemType(excludeActiveItemType);

        search.setShowOnlyChildren(showOnlyChildren);
        search.setShowOnlyRelatives(showOnlyRelatives);

		search.setEmail(email);
		
		search.setFeaturedMethod(featuredMethod);
		search.setImageOnly(imageOnly);

		search.setItemType(itemType);
		search.setItemTypes(itemTypes);

        search.setItemTypeDisplayed(itemTypeDisplayed);

		search.setJoinedWithinDays(joinedWithinDays);
		search.setKeywords(keywords);
		search.setLastActive(lastActive);
		search.setLimit(limit);
		search.setLocation(location);

		search.setMultipleItemTypes(multipleItemTypes);
		search.setMethod(method);
		search.setItemTypes(itemTypeIds);

		search.setName(name);
		search.setNoCategory(noCategory);
		search.setNoImages(noImages);

		search.setRecordingOnly(recordingOnly);

		search.setStatus(status);
		search.setStartsWith(startsWith);
		search.setSellPriceFrom(sellPriceFrom);
		search.setSellPriceTo(sellPriceTo);
		search.setSearchCategory(searchCategory);
		search.setSubcats(subcats);
        search.setSearchAccount(searchAccount);
        search.setSearchAccounts(StringHelper.explodeIntegers(searchAccounts, "\n"));
		search.setSubscriptionLevel(subscription);
		search.setSort(sort);
		search.setSortType(sortType);
        search.setInStockOnly(inStockOnly);
        
        search.setCurrDateWrapper(currDateWrapper);

        search.save();

		logger.fine("[SearchEditHandler] setting av=" + attributeValues);
		search.setAttributeValues(attributeValues);
		for (Attribute attribute : removeAttributeValues) {
			search.removeAttributeValues(attribute);
		}

        if (addAttribute != null) {
//            try {
//                addAttribute = addAttribute.copyTo(addAttribute.getOwner(), true);
//            } catch (CloneNotSupportedException e) {
//                logger.fine("[SearchEditHandler]" + " Attribute " + addAttribute + "clone error");
//                e.printStackTrace();
//            }

			switch (addAttribute.getType()) {

			default:
				break;

			case Boolean:
				search.addAttributeValue(addAttribute, "true");
				break;

			case Email:
				search.addAttributeValue(addAttribute, "test@domain.com");
				break;

			case Numerical:
				search.addAttributeValue(addAttribute, "0");
				break;

			case Postcode:
				search.addAttributeValue(addAttribute, "S80 3GB");
				break;

			case Text:
				search.addAttributeValue(addAttribute, "new value");
				break;

			case Selection:
				List<AttributeOption> options = addAttribute.getOptions();
				if (options.size() > 0) {
					search.addAttributeValue(addAttribute, options.get(0).getValue());
				}
				break;

            case Date:
                search.addAttributeValue(addAttribute, new Date().toEditString());
                break;
            }
		}

		// if this search is for an items box then flush cache
		if (search.hasHighlightedItemsBox()) {
			HighlightedItemsBox.clearHtmlCache();
		}

		if (search.hasHighlightedItemsBlock()) {
			HighlightedItemsBlock.clearHtmlCache();
		}

		return new ActionDoc(context, "This search has been updated", new Link(SearchEditHandler.class, null, "search", search));
	}

    public Object removeAttribute() throws ServletException {
        if (search == null || removeAttribute == null) {
            return main();
        }
        search.removeAttributeValues(removeAttribute);
//        removeAttribute.delete();
        return main();
    }
}
