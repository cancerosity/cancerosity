package org.sevensoft.ecreator.iface.admin.categories;

import java.util.Collection;
import java.util.Set;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.model.categories.CategoryModule;
import org.sevensoft.ecreator.model.categories.CategorySettings;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.sorts.ItemSort;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 01-Jun-2004 21:28:42
 */
@Path("admin-settings-category.do")
public class CategorySettingsHandler extends AdminHandler {

	private String				footer, header;
	private boolean				permissions, attributes, files, headers, subcategoryOrdering;
	private boolean				forwards;
	private boolean				categoryMenuFlat;
	private boolean				hidden;
    private boolean             showHomePage;
    private int					itemsPerPage;
	private boolean				itemsPerPageOverride;
	private boolean				summaries;
	private boolean				pricing;
	private boolean				sorts;
	private transient LinkTag		detailsMenu;
	private boolean				images;
	private boolean				resultsBottom;
	private boolean				resultsTop;
	private transient CategorySettings	categorySettings;
	private String				salePrice;
	private boolean				subcategoriesSelector;
	private ItemSort				itemSort;
	private CategoryModule			module;
	private String				titlesGenerator;
    private boolean excludeFeatured;
    private boolean overrideExclude;

	public CategorySettingsHandler(RequestContext context) {
		super(context);
		this.categorySettings = CategorySettings.getInstance(context);
	}

	public Object addModule() {

		if (module == null) {
			return main();
		}

		categorySettings.addModule(module);
		categorySettings.save();

		return main();

	}

	public Object exportCategoryStructure() {
		return categorySettings.exportCategoryTree();
	}

	@Override
	public Object main() {

		AdminDoc doc = new AdminDoc(context, user, "Category settings", Tab.Categories);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update category settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void export() {

				sb.append(new AdminTable("Categories export"));

				sb.append(new AdminRow(true, "Export structure", "Export categories in text format to be imported into another site", new ButtonTag(
						CategorySettingsHandler.class, "exportCategoryStructure", "exportCategoryStructure")));

				sb.append("</table>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				if (isSuperman()) {

					if (miscSettings.isAdvancedMode()) {

						sb.append(new AdminRow(true, "Category menu flat", "Browse all categories in a list rather than a drop down menu",
								new BooleanRadioTag(context, "categoryMenuFlat", categorySettings.isCategoryMenuFlat())));
					}
				}

				sb.append(new AdminRow("Hidden",
						"Enables hidden categories which are categories that do not display in the category sideboxes but can still be accessed manually via the url. "
								+ "Useful for pages that you want to link to yourself.", new BooleanRadioTag(context, "hidden",
								categorySettings.isHidden())));

                sb.append(new AdminRow("Show Home Page", "If 'Yes' shows Home Page when category is not found, if 'No' shows error page.",
                        new BooleanRadioTag(context, "showHomePage", categorySettings.isShowHomePage())));

                sb.append("</table>");
			}

			private void headers() {

				sb.append(new AdminTable("Headers / Footers"));

				sb.append(new AdminRow("Header", "Displayed first in a category.", new TextAreaTag(context, "header", categorySettings.getHeader(), 50,
						4)));

				sb.append(new AdminRow("Footer", "Displayed last in a category.", new TextAreaTag(context, "footer", categorySettings.getFooter(), 50,
						4)));

				sb.append("</table>");
			}

			private void items() {

				sb.append(new AdminTable("Items"));

				sb.append(new AdminRow("Items per page", "Set how many items appear per page.", new TextTag(context, "itemsPerPage", categorySettings
						.getItemsPerPage(), 4)));

				sb.append(new AdminRow("Override items per page in categories",
						"Allows you to set a different items per page value for each category.", new BooleanRadioTag(context,
								"itemsPerPageOverride", categorySettings.isItemsPerPageOverride())));

				sb.append(new AdminRow("Show sorts", "Show sort options when browsing categories.", new BooleanRadioTag(context, "sorts",
						categorySettings.isShowSorts())));

				sb.append(new AdminRow("Default sort", "Apply this sort unless overriden by a user or a category specific setting.", new SelectTag(
						context, "itemSort", categorySettings.getItemSort(), ItemSort.get(context), "-Default to alphabetical-")));

				sb.append(new AdminRow("Results info", "Show the results bar which tells the user how many results were found in this category",
						"Top: " + new CheckTag(context, "resultsTop", true, categorySettings.isResultsTop()) + " Bottom: "
								+ new CheckTag(context, "resultsBottom", true, categorySettings.isResultsBottom())));

                if (Module.ListingsFeaturedExclude.enabled(context)) {
                    sb.append(new AdminRow("Exclude Featured Items", "Exclude Featured Items from Items blocks across the site.", new BooleanRadioTag(context, "excludeFeatured",
                            categorySettings.isExcludeFeatured())));

                    sb.append(new AdminRow("Override 'Exclude'", "Override featue 'Exclude Featured Items' on Items blocks.", new BooleanRadioTag(context, "overrideExclude",
                            categorySettings.isOverrideExclude())));
                }

				sb.append("</table>");
			}

			private void modules() {

				sb.append(new AdminTable("Modules"));

				Set<CategoryModule> existingModules = categorySettings.getModules();
				for (CategoryModule module : existingModules) {

					sb.append("<tr>");
					sb.append("<td>" + module + "</td>");

					String description = module.getDescription();
					sb.append("<td>" + (description == null ? "" : description) + "</td>");

					sb.append("<td width='10'>"
							+ new LinkTag(CategorySettingsHandler.class, "removeModule", new DeleteGif(), "module", module) + "</td>");

					sb.append("</tr>");
				}

				Collection<CategoryModule> modules = categorySettings.getUnusedModules();
				if (modules.size() > 0) {

					SelectTag tag = new SelectTag(context, "module");
					tag.setAny("-Choose module to add-");
					tag.addOptions(modules);
					tag.setLabelComparator();

					sb.append(new FormTag(CategorySettingsHandler.class, "addModule", "get"));

					sb.append("<tr><td colspan='3'>Add module: " + tag + " " + new SubmitTag("Add module") + "</td></tr>");
					sb.append("</form>");
				}

				sb.append("</table>");
			}

			private void pricing() {

				sb.append(new AdminTable("Pricing"));

				sb.append("<tr><th width='300'><b>Set category margin / default price on all categories</b></th><td>"
						+ new TextTag(context, "salePrice", 8) + "</td></tr>");

				sb.append("</table>");
			}

			private void subcategories() {

				sb.append(new AdminTable("Subcategory options"));

				// subcategory ordering
				sb.append(new AdminRow("Subcategory ordering", "Enable manual ordering of subcategories.", new BooleanRadioTag(context,
						"subcategoryOrdering", categorySettings.isSubcategoryOrdering())));

				sb.append(new AdminRow("Hide subcategories", "Hide the subcategory lists in all category pages.", new BooleanRadioTag(context,
						"hideSubcategories", categorySettings.isHideSubcategories())));

				if (miscSettings.isAdvancedMode()) {

					sb.append(new AdminRow("Subcategories selector", "Browse subcategories using a drop down list.", new BooleanRadioTag(context,
							"subcategoriesSelector", categorySettings.isSubcategoriesSelector())));
				}

				sb.append("</table>");
			}

			private void template() {

				sb.append(new AdminTable("Template / Graphics"));

				sb.append(new AdminRow("Titles generator", new TextTag(context, "titlesGenerator", categorySettings.getTitlesGenerator(), 60)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				modules();

				sb.append(new FormTag(CategorySettingsHandler.class, "save", "POST"));

                commands();
				general();
				subcategories();

				if (CategoryModule.Headers.enabled(context)) {
					headers();
				}

				if (ItemType.hasItemTypes(context)) {
					items();
				}

				if (isSuperman()) {
					if (Module.Pricing.enabled(context)) {
						pricing();
					}
				}

				if (isSuperman()) {
					export();
				}

				if (isSuperman()) {
					template();
				}

				commands();

				sb.append("</form>");

				return sb.toString();
			}
		});
		return doc;

	}

	public Object removeModule() {

		if (module == null) {
			return main();
		}

		categorySettings.removeModule(module);
		categorySettings.save();

		return main();

	}

	public Object save() {

		// subcategory options
		categorySettings.setSubcategoryOrdering(subcategoryOrdering);
		if (miscSettings.isAdvancedMode()) {
			categorySettings.setSubcategoriesSelector(subcategoriesSelector);
		}

		categorySettings.setHidden(hidden);

		categorySettings.setHeader(header);
		categorySettings.setFooter(footer);

		/*
		 * Items
		 */
		categorySettings.setItemsPerPage(itemsPerPage);
		categorySettings.setItemsPerPageOverride(itemsPerPageOverride);
		categorySettings.setSorts(sorts);
		categorySettings.setItemSort(itemSort);
        categorySettings.setExcludeFeatured(excludeFeatured);
        categorySettings.setOverrideExclude(overrideExclude);

		if (isSuperman()) {
			if (miscSettings.isAdvancedMode()) {
				categorySettings.setCategoryMenuFlat(categoryMenuFlat);
			}

			categorySettings.setTitlesGenerator(titlesGenerator);
		}

         categorySettings.setShowHomePage(showHomePage);

        // results
		categorySettings.setResultsTop(resultsTop);
		categorySettings.setResultsBottom(resultsBottom);

		categorySettings.save();

		if (salePrice != null) {
			categorySettings.setPrice(salePrice);
		}

		return new ActionDoc(context, "Category settings have been updated", new Link(CategorySettingsHandler.class));
	}

}
