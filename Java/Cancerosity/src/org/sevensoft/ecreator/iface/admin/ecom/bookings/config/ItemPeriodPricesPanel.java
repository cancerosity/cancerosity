package org.sevensoft.ecreator.iface.admin.ecom.bookings.config;

import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.model.bookings.ItemPeriodPrice;
import org.sevensoft.ecreator.model.bookings.Period;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * User: MeleshkoDN
 * Date: 12.09.2007
 * Time: 11:38:14
 */
public class ItemPeriodPricesPanel extends Panel {

    private transient LinkedHashMap<Period, ItemPeriodPrice> map = new LinkedHashMap<Period, ItemPeriodPrice>();
    private transient List<Period> periods;
    private Item item;
    private Class<? extends Handler> handler;

    public ItemPeriodPricesPanel(RequestContext context, Item item, Class handler) {
        super(context);
        this.handler = handler;
        this.item = item;
        periods = SimpleQuery.execute(context, Period.class);
        for (Period period : periods) {
            map.put(period, SimpleQuery.get(context, ItemPeriodPrice.class, "period", period, "item", item));
        }
    }

    @Override
    public void makeString() {
        sb.append(new HiddenTag("item", item));
        sb.append(new TableTag("form").setCaption("Prices"));
        for (Period period : periods) {
            ItemPeriodPrice itemPeriodPrice = map.get(period);
            sb.append("<tr>");
            sb.append("<td>");
            sb.append("<b>" + period.getName() + "</b>");
            sb.append("<br/>" + period.getStartDate().toString("dd/MM") + "-" + period.getEndDate().toString("dd/MM"));
            sb.append("</td>");
            if (itemPeriodPrice != null) {
                sb.append("<td>");
                sb.append("Daily: " + itemPeriodPrice.getDailyPrice());
                sb.append("</td>");
                sb.append("<td>");
                if (period.isWeekly()) {
                    sb.append("Weekly: " + itemPeriodPrice.getWeeklyPrice());
                }
                sb.append("</td>");
            } else {
                sb.append("<td></td><td></td>");
            }
            sb.append("<td>");
            sb.append(new ButtonTag(handler, null, "Change prices", "itemPeriodPrice", itemPeriodPrice, "item", item, "period", period));
            sb.append("</td>");
            sb.append("</tr>");
        }
        sb.append("</table>");
    }
}
