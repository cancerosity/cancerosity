package org.sevensoft.ecreator.iface.admin.items.menus;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.ecreator.iface.admin.ecom.bookings.config.BookingSetupHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.OrderHistoryHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.iface.admin.items.modules.relations.RelationHandler;
import org.sevensoft.ecreator.iface.admin.items.modules.stock.ItemStockHandler;
import org.sevensoft.ecreator.iface.admin.items.stats.ItemSubmissionsHandler;
import org.sevensoft.ecreator.iface.admin.reports.hits.page.ItemHitsHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Menu;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 4 Sep 2006 17:40:03
 * 
 */
public class EditItemMenu extends Menu {

	private final Item	item;

	public EditItemMenu(Item item) {
		this.item = item;
	}

	@Override
	public List<LinkTag> getLinkTags(RequestContext context) {

		boolean superman = context.containsAttribute("superman");
		List<LinkTag> links = new ArrayList();

		links.add(new LinkTag(ItemHandler.class, "edit", "Details", "item", item));

		if (Module.Shopping.enabled(context) && ItemModule.Account.enabled(context, item)) {
			links.add(new LinkTag(OrderHistoryHandler.class, null, "Order history", "account", item));
		}

		if (Module.Availabilitity.enabled(context) && item.getItemType().getStockModule().isStockControl()) {

			links.add(new LinkTag(ItemStockHandler.class, null, "Stock / availability", "item", item));
			// links.add(new LinkTag(AuditHandler.class, "audit", "Stock audit", "item", item));
		}

		if (Module.Bookings.enabled(context) && ItemModule.Bookings.enabled(context, item)) {
			links.add(new LinkTag(BookingSetupHandler.class, null, "Booking Setup", "item", item));
		}

		if (ItemModule.Relations.enabled(context, item)) {
			links.add(new LinkTag(RelationHandler.class, null, "Relations", "item", item));
		}

		if (ItemModule.Options.enabled(context, item)) {
			links.add(new LinkTag(ItemHandler.class, "options", "Options", "item", item));
		}

        if (!Module.StatsBlock.enabled(context)) {
            if (ItemModule.Account.enabled(context, item)) {
                links.add(new LinkTag(ItemSubmissionsHandler.class, "monthly", "Submissions", "account", item));
                links.add(new LinkTag(ItemHitsHandler.class, "monthly", "Hits", "account", item));
            } else {
                links.add(new LinkTag(ItemSubmissionsHandler.class, "monthly", "Submissions", "item", item));
                links.add(new LinkTag(ItemHitsHandler.class, "monthly", "Hits", "item", item));
            }
        }

        if (ItemModule.Comments.enabled(context, item)) {
            links.add(new LinkTag(ItemHandler.class, "comments", item.getItemTypeName() + " Comments", "item", item));
        }

		if (superman) {
			links.add(new LinkTag(ItemTypeHandler.class, "edit", item.getItemTypeName() + " settings", "itemType", item.getItemType()));
		}

		if (links.size() == 1) {
			return null;
		}

		return links;
	}

}
