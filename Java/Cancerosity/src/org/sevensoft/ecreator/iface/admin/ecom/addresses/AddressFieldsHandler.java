package org.sevensoft.ecreator.iface.admin.ecom.addresses;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.ecom.addresses.AddressFields;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 03-Oct-2005 14:25:41
 * 
 */
@Path("admin-settings-address.do")
public class AddressFieldsHandler extends AdminHandler {

	private String			telephoneLabel1;
	private String			telephoneLabel2;
	private boolean			telephoneOptional3;
	private boolean			telephoneOptional2;
	private String			telephoneLabel3;
	private boolean			noCountries;

	private String			nameLabel;

	private String			countyLabel;

    private String			stateLabel;

    private String			stateDescription;

	private String			postcodeLabel;

	private String			telephoneDescription2;

	private String			telephoneDescription1;

	private String			telephoneDescription3;

	private String addressLabel3;

	private String addressLabel2;

	private String addressLabel1;

	private String			townLabel;

	private AddressFields.Type	type;
	private String			postcodeDescription;
	private String			townDescription;
	private String			nameDescription;
	private boolean			telephoneOptional1;
	private String			addressDescription2;
	private String			addressDescription3;
	private String			addressDescription1;
	private String			countryLabel;

	public AddressFieldsHandler(RequestContext context) {
		super(context);
	}

	public Object edit() throws ServletException {

		if (type == null) {
			return main();
		}

		final AddressFields fields = AddressFields.getInstance(context, type);

		AdminDoc doc = new AdminDoc(context, user, "Address settings", Tab.Orders);
		doc.addBody(new Body() {

            private void general() {
                sb.append(new AdminTable("General"));

                sb.append(new AdminRow("Remove countries", "If set to yes then the countries option will be removed when adding an address.",
                        new BooleanRadioTag(context, "noCountries", shoppingSettings.isNoCountries())));

                sb.append("</table>");
            }
			private void addressFields() {

				sb.append(new AdminTable("Address fields"));

				//				if (false) {
				//					sb.append(new AdminRow("Address type field",
				//							"Show an extra field when creating addresses that lets the user inform you what the address is, "
				//									+ "eg, home, work, relative's house.", new BooleanRadioTag(context, "addressTypeInput", addressSettings
				//									.isAddressTypeField())));
				//				}

				sb.append(new AdminRow("Name label", "This is the label for the 'contact name' field", new TextTag(context, "nameLabel", fields
						.getNameLabel(), 20)));

				sb.append(new AdminRow("Name description", "This is the instructions for this field shown to the user", new TextAreaTag(context,
						"nameDescription", fields.getNameDescription(), 50, 2)));

				sb.append(new AdminRow("Address 1 label", "This is the label for the 'address line 1' field", new TextTag(context, "addressLabel1",
						fields.getAddressLabel1(), 20)));

				sb.append(new AdminRow("Address 2 description", "This is the instructions for this field shown to the user", new TextAreaTag(context,
						"addressDescription2", fields.getAddressDescription2(), 50, 2)));

				sb.append(new AdminRow("Address 2 label", "This is the label for the 'address line 2' field", new TextTag(context, "addressLabel2",
						fields.getAddressLabel2(), 20)));

				sb.append(new AdminRow("Address 3 label", "This is the label for the 'address line 3' field", new TextTag(context, "addressLabel3",
						fields.getAddressLabel3(), 20)));

				sb.append(new AdminRow("Town label", "This is the label for the 'town/city' field", new TextTag(context, "townLabel", fields
						.getTownLabel(), 20)));

				sb.append(new AdminRow("Town description", "This is the instructions for this field shown to the user", new TextAreaTag(context,
						"townDescription", fields.getTownDescription(), 50, 2)));

				sb.append(new AdminRow("County label", "This is the label for the 'contact name' field", new TextTag(context, "countyLabel", fields
						.getCountyLabel(), 20)));

				sb.append(new AdminRow("Postcode label", "This is the label for the 'postcode' field", new TextTag(context, "postcodeLabel", fields
						.getPostcodeLabel(), 20)));

				sb.append(new AdminRow("Postcode description", "This is the instructions for this field shown to the user", new TextAreaTag(context,
						"postcodeDescription", fields.getPostcodeDescription(), 50, 2)));

				sb.append(new AdminRow("Country label", "This is the label for the 'contact name' field", new TextTag(context, "countryLabel", fields
						.getCountryLabel(), 20)));

                sb.append(new AdminRow("State label", "This is the label for the 'US State' field", new TextTag(context, "stateLabel", fields
						.getStateLabel(), 20)));

                sb.append(new AdminRow("State description", "This is the instructions for this field shown to the user", new TextAreaTag(context,
						"stateDescription", fields.getStateDescription(), 50, 2)));

				sb.append("</table>");
			}

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update address fields"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void telephones() {

				sb.append(new AdminTable("Telephones"));

				sb.append(new AdminRow("Telephone 1 label", "This is the label for the 'telephone 1' field", new TextTag(context, "telephoneLabel1",
						fields.getTelephoneLabel1(), 20)));

				sb.append(new AdminRow("Telephone 1 description", "This is the instructions for this field shown to the user", new TextAreaTag(context,
						"telephoneDescription1", fields.getTelephoneDescription1(), 50, 2)));

				sb.append(new AdminRow("Telephone 1 optional", "Set if this field is optional or not.", new BooleanRadioTag(context,
						"telephoneOptional1", fields.isTelephoneOptional1())));

				sb.append(new AdminRow("Telephone 2 label", "This is the label for the 'telephone 2' field", new TextTag(context, "telephoneLabel2",
						fields.getTelephoneLabel2(), 20)));

				sb.append(new AdminRow("Telephone 2 description", "This is the instructions for this field shown to the user", new TextAreaTag(context,
						"telephoneDescription2", fields.getTelephoneDescription2(), 50, 2)));

				sb.append(new AdminRow("Telephone 2 optional", "Set if this field is optional or not.", new BooleanRadioTag(context,
						"telephoneOptional2", fields.isTelephoneOptional2())));

				sb.append(new AdminRow("Telephone 3 label", "This is the label for the 'telephone 3' field", new TextTag(context, "telephoneLabel3",
						fields.getTelephoneLabel3(), 20)));

				sb.append(new AdminRow("Telephone 3 description", "This is the instructions for this field shown to the user", new TextAreaTag(context,
						"telephoneDescription3", fields.getTelephoneDescription3(), 50, 2)));

				sb.append(new AdminRow("Telephone 3 optional", "Set if this field is optional or not.", new BooleanRadioTag(context,
						"telephoneOptional3", fields.isTelephoneOptional3())));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(AddressFieldsHandler.class, "save", "POST"));
				sb.append(new HiddenTag("type", type));

                commands();

                general();
                addressFields();
                telephones();

                commands();

                sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}



    @Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Address settings", Tab.Orders);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				StringBuilder sb = new StringBuilder();

				sb.append(new FormTag(AddressFieldsHandler.class, "edit", "get"));
				sb.append(new AdminTable("Edit address fields"));

				sb.append(new AdminRow("Select address type", "Choose which type of fields you want to edit.", new SelectTag(context, "type", null,
						AddressFields.Type.values())
						+ " " + new SubmitTag("Edit")));

				sb.append("</table>");
				sb.append("</form>");

				return sb.toString();
			}

		});

		return doc;
	}

	public Object save() throws ServletException {

		if (type == null) {
			return main();
		}

		final AddressFields fields = AddressFields.getInstance(context, type);

		// address

		fields.setNameLabel(nameLabel);
		fields.setNameDescription(nameDescription);

		fields.setAddressLabel1(addressLabel1);
		fields.setAddressDescription1(addressDescription1);

		fields.setAddressLabel2(addressLabel2);
		fields.setAddressDescription2(addressDescription2);

		fields.setAddressLabel3(addressLabel3);
		fields.setAddressDescription3(addressDescription3);

		fields.setTownLabel(townLabel);
		fields.setTownDescription(townDescription);

		fields.setCountyLabel(countyLabel);
        fields.setStateLabel(stateLabel);
        fields.setStateDescription(stateDescription);

		fields.setPostcodeLabel(postcodeLabel);
		fields.setPostcodeDescription(postcodeDescription);

		fields.setCountryLabel(countryLabel);

		// telephones
		fields.setTelephoneLabel1(telephoneLabel1);
		fields.setTelephoneOptional1(telephoneOptional1);
		fields.setTelephoneDescription1(telephoneDescription1);

		fields.setTelephoneLabel2(telephoneLabel2);
		fields.setTelephoneOptional2(telephoneOptional2);
		fields.setTelephoneDescription2(telephoneDescription2);

		fields.setTelephoneLabel3(telephoneLabel3);
		fields.setTelephoneOptional3(telephoneOptional3);
		fields.setTelephoneDescription3(telephoneDescription3);

		fields.save();

        shoppingSettings.setNoCountries(noCountries);
        shoppingSettings.save();

        return new ActionDoc(context, "Address fields updated", new Link(AddressFieldsHandler.class, "edit", "type", type));
	}
}
