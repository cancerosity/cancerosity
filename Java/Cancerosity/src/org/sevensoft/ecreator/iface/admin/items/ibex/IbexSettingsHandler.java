package org.sevensoft.ecreator.iface.admin.items.ibex;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.model.items.ibex.IbexSettings;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

import javax.servlet.ServletException;

/**
 * User: Tanya
 * Date: 04.03.2011
 */
@Path("admin-ibex.do")
public class IbexSettingsHandler extends AdminHandler {

    private transient IbexSettings ibexSettings;

    private String op;
    private String cyid;
    private int frameWidth;
    private int frameHeight;
    private Attribute pid;

    public IbexSettingsHandler(RequestContext context) {
        super(context);
        this.ibexSettings = IbexSettings.getInstance(context);
    }

    public Object main() throws ServletException {
        if (!Module.IBex.enabled(context)) {
            return index();
        }

        AdminDoc doc = new AdminDoc(context, user, "Edit iBex Settings", null);
        doc.addBody(new Body() {
            @Override
            public String toString() {
                sb.append(new FormTag(IbexSettingsHandler.class, "save", "POST"));
                commands();
                general();
                commands();
                sb.append("</form>");
                return sb.toString();
            }

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update settins"));
                sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return"));
                sb.append("</div>");
            }

            private void general() {
                sb.append(new AdminTable("Settings"));
                sb.append(new AdminRow("Operator code", "", new TextTag(context, "op", ibexSettings.getOp())));
                sb.append(new AdminRow("Country Id", "", new TextTag(context, "cyid", ibexSettings.getCyid())));
                sb.append(new AdminRow("Frame Width", "", new TextTag(context, "frameWidth", ibexSettings.getFrameWidth(), 0)));
                sb.append(new AdminRow("Frame Height", "", new TextTag(context, "frameHeight", ibexSettings.getFrameHeight(), 0)));

                SelectTag propertyIdTag  = new SelectTag(context, "pid", ibexSettings.getPropertyId());
                propertyIdTag.addOptions(Attribute.getSelectionMap(context));
                propertyIdTag.setAny("--Choose--");
                sb.append(new AdminRow("Property ID", "", propertyIdTag));
                sb.append("</table>");
            }
        });
        return doc;
    }

    public Object save() {
        ibexSettings.setOp(op);
        ibexSettings.setCyid(cyid);
        ibexSettings.setFrameWidth(frameWidth);
        ibexSettings.setFrameHeight(frameHeight);
        ibexSettings.setPropertyId(pid);

        ibexSettings.save();
        
        addMessage("iBex settings has been updated");
        return new ActionDoc(context, "iBex settings has been updated", new Link(IbexSettingsHandler.class));
    }
}
