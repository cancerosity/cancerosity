package org.sevensoft.ecreator.iface.admin.misc.languages;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.ecom.ShoppingMenu;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.extras.languages.Language;
import org.sevensoft.ecreator.model.extras.languages.tables.Translation;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks
 */
@Path("admin-languages-translations.do")
public class TranslationHandler extends AdminHandler {

	private Translation	translation;
	private String		original, translated;
	private Language		language;

	public TranslationHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		if (language == null || original == null || translated == null) {
			return main();
		}

		translation = new Translation(context, language, original, translated);
		
		clearParameters();
		return main();
	}

	public Object delete() throws ServletException {

		if (translation == null) {
			return main();
		}

		translation.delete();
		return main();
	}

	public Object edit() throws ServletException {

		if (translation == null) {
			return main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit translation", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update translation"));
				sb.append(new ButtonTag(TranslationHandler.class, "delete", "Delete translation", "translation", translation)
						.setConfirmation("Are you sure you want to delete this translation?"));
				sb.append(new ButtonTag(TranslationHandler.class, null, "Return to translations"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("Due dates"));

				sb.append(new AdminRow("Language", "The language this translation is used for.", new SelectTag(context, "language", translation
						.getLanguage(), Language.values())));

				sb.append(new AdminRow("Original", "Enter the original phrase here.", new TextTag(context, "original", translation.getOriginal(), 40)));

				sb.append(new AdminRow("Translated", "Enter the translation for this phrase here.", new TextTag(context, "tranlated", translation
						.getTranslated(), 40)));

				sb.append("</table>");

			}

			@Override
			public String toString() {

				sb.append(new FormTag(TranslationHandler.class, "save", "post"));
				sb.append(new HiddenTag("translation", translation));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		if (!Module.Languages.enabled(context)) {
			return new SettingsMenuHandler(context).main();
		}

		if (language == null) {
			language = Language.French;
		}

		AdminDoc doc = new AdminDoc(context, user, "Translations", null);
		doc.setMenu(new ShoppingMenu());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Translations"));

				sb.append(new FormTag(TranslationHandler.class, null, "get"));

				sb.append("<tr><td colspan='3'>Language: "
						+ new SelectTag(context, "language", language, Language.values()).setAutoSubmit() + "</td></tr>");

				sb.append("</form>");

				sb.append("<tr>");
				sb.append("<th>Name</th>");
				sb.append("<th>Type</th>");
				sb.append("<th width='10'>Delete</th>");
				sb.append("</tr>");

				for (Translation translation : Translation.get(context, language)) {

					sb.append("<tr>");
					sb.append("<td>" + new LinkTag(TranslationHandler.class, "edit", new SpannerGif(), "translation", translation) + " "
							+ translation.getOriginal() + "</td>");
					sb.append("<td>" + translation.getTranslated() + "</td>");
					sb.append("<td width='10'>"
							+ new LinkTag(TranslationHandler.class, "delete", new DeleteGif(), "translation", translation)
									.setConfirmation("Are you sure you want to delete this translation?") + "</td>");
					sb.append("</tr>");
				}

				sb.append(new FormTag(TranslationHandler.class, "create", "post"));
				sb.append(new HiddenTag("language", language));

				sb.append("<tr><td colspan='3'>Add translation from " + new TextTag(context, "original", 20) + " to "
						+ new TextTag(context, "translated", 20) + new SubmitTag("Add") + "</td></tr>");

				sb.append("</form>");

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (translation == null) {
			return main();
		}

		translation.setOriginal(original);
		translation.setTranslated(translated);
		translation.setLanguage(language);
		translation.save();

		addMessage("Translation updated");
		clearParameters();
		return edit();
	}
}