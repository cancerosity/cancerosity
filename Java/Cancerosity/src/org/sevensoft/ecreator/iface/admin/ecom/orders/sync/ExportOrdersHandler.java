package org.sevensoft.ecreator.iface.admin.ecom.orders.sync;

import java.io.StringWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.ecom.ShoppingMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderSearcher;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.ecreator.model.ecom.orders.export.csv.ecreator.OrderHeadersCsv;
import org.sevensoft.ecreator.model.ecom.orders.export.csv.ecreator.OrderLinesCsv;
import org.sevensoft.ecreator.model.ecom.orders.export.csv.ecreator.PendingOrdersCsv;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.StreamResult;

/**
 * @author sks 03-Oct-2005 14:25:41
 * 
 */
@Path("admin-orders-export.do")
public class ExportOrdersHandler extends AdminHandler {

	private String				status;
	private Order				order;
	private String				headersCsv, linesCsv, xml;
	private transient OrderSettings	orderSettings;
	private boolean				neverExported;
	private User				salesPerson;
	private String				name, email, orderId;
	private boolean				webOrders;

	public ExportOrdersHandler(RequestContext context) {
		super(context);
		orderSettings = OrderSettings.getInstance(context);
	}

	public Object export() {

		OrderSearcher searcher = getSearcher();
		List<Order> orders = searcher.execute();

		if (orders.isEmpty()) {
			addPopup("No orders found to export");
			return main();
		}

		if (headersCsv != null) {

			OrderHeadersCsv export = new OrderHeadersCsv(context, orders);
			StringWriter writer = new StringWriter();
			export.output(writer);

			return new StreamResult(writer.toString(), "text/csv", "order_headers.csv", StreamResult.Type.Attachment);

		} else if (linesCsv != null) {

			OrderLinesCsv export = new OrderLinesCsv(context, orders);
			StringWriter writer = new StringWriter();
			export.output(writer);

			return new StreamResult(writer.toString(), "text/csv", "order_lines.csv", StreamResult.Type.Attachment);

		}

		return null;
	}

	public Object exportOrderHeaderCsv() {

		if (order == null) {
			return HttpServletResponse.SC_BAD_REQUEST;
		}

		OrderHeadersCsv export = new OrderHeadersCsv(context, order);
		StringWriter writer = new StringWriter();
		export.output(writer);

		return new StreamResult(writer.toString(), "text/csv", order.getId() + "_header.csv", StreamResult.Type.Attachment);
	}

	public Object exportOrderLinesCsv() {

		if (order == null) {
			return HttpServletResponse.SC_BAD_REQUEST;
		}

		OrderLinesCsv export = new OrderLinesCsv(order, context);
		StringWriter writer = new StringWriter();
		export.output(writer);

		return new StreamResult(writer.toString(), "text/csv", order.getId() + "_lines.csv", StreamResult.Type.Attachment);
	}

    public Object exportPending() {

        OrderSearcher searcher = new OrderSearcher(context);
        searcher.setStatus("Pending");

        List<Order> orders = searcher.execute();

        if (orders.isEmpty()) {
            addPopup("No orders found to export");
            return main();
        }

        PendingOrdersCsv export = new PendingOrdersCsv(context, orders);
        StringWriter writer = new StringWriter();
        export.output(writer);

        return new StreamResult(writer.toString(), "text/csv", "pending_orders.csv", StreamResult.Type.Attachment);
    }

    private OrderSearcher getSearcher() {

		OrderSearcher searcher = new OrderSearcher(context);
		searcher.setEmail(email);
		searcher.setName(name);
		searcher.setNeverExported(neverExported);
		searcher.setOrderId(orderId);
		searcher.setSalesperson(salesPerson);
		searcher.setWebOrders(webOrders);
		searcher.setStatus(status);

		return searcher;
	}

	@Override
	public Object main() {

		AdminDoc doc = new AdminDoc(context, user, "Order export", Tab.Orders);
		doc.setMenu(new ShoppingMenu());
		doc.addBody(new Body() {

			private void criteria() {

				sb.append(new AdminTable("Order export"));
				sb.append(new FormTag(ExportOrdersHandler.class, "export", "POST"));

				// STATUS
				SelectTag statusTag = new SelectTag(context, "status");
				statusTag.setAny("-Any status-");
				statusTag.addOptions(orderSettings.getStatuses());

				sb.append(new AdminRow("Status", "Only include orders with this status", statusTag));

				sb.append(new AdminRow("New", "Only include orders that have not been exported previously.", new CheckTag(context, "neverExported",
						"true", true)));

				sb.append(new AdminRow("Order ID", "Only include orders that match this order ID", new TextTag(context, "orderId", 12)));

				sb.append(new AdminRow("Name", "Include orders where the member name matches (wildcard).", new TextTag(context, "name", 40)));

				sb.append(new AdminRow("Email", "Include orders where the member email matches (wildcard).", new TextTag(context, "email", 40)));

				if (orderSettings.isSalesPersons()) {

					SelectTag salespersonTag = new SelectTag(context, "salesPerson");
					salespersonTag.setAny("-Any salesperson-");
					salespersonTag.addOptions(User.getActive(context));

					sb.append("<tr><th width='300'><b>Salesperson</b><br/>" + "Only orders placed by this user will be included.</th><td>" +
							salespersonTag + "</td></tr>");

					sb.append("<tr><th width='300'><b>Only web orders</b><br/>" +
							"Only orders placed via the web front end will be included.</th><td>" +
							new CheckTag(context, "webOrders", "true", false) + "</td></tr>");

				}

				sb.append("<tr><td colspan='2' align='center'>" + new SubmitTag("headersCsv", "Export headers CSV") +
						new SubmitTag("linesCsv", "Export Lines CSV") + "</td></tr>");

				sb.append("</form>");
				sb.append("</table>");
			}

            private void pending() {
                sb.append(new AdminTable("Export Pending Orders"));

                sb.append(new AdminRow("Export Pending Orders CSV", new ButtonTag(ExportOrdersHandler.class, "exportPending", "Export Pending Orders CSV")));

                sb.append("</table>");
            }

            @Override
			public String toString() {

				criteria();
                pending();
                return sb.toString();
			}

		});
		return doc;
	}
}
