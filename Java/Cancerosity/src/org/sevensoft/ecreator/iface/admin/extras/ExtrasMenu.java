package org.sevensoft.ecreator.iface.admin.extras;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.ecreator.iface.admin.crm.forms.FormHandler;
import org.sevensoft.ecreator.iface.admin.crm.forms.SubmissionExportHandler;
import org.sevensoft.ecreator.iface.admin.crm.forms.SubmissionHandler;
import org.sevensoft.ecreator.iface.admin.crm.forms.restriction.SubmissionRestrictionModuleHandler;
import org.sevensoft.ecreator.iface.admin.crm.forms.report.SubmissionReportModuleHandler;
import org.sevensoft.ecreator.iface.admin.crm.forum.ForumHandler;
import org.sevensoft.ecreator.iface.admin.crm.forum.PostSearchHandler;
import org.sevensoft.ecreator.iface.admin.crm.jobsheets.AutoJobHandler;
import org.sevensoft.ecreator.iface.admin.crm.jobsheets.JobSearchHandler;
import org.sevensoft.ecreator.iface.admin.crm.jobsheets.JobsHandler;
import org.sevensoft.ecreator.iface.admin.crm.jobsheets.JobsheetHandler;
import org.sevensoft.ecreator.iface.admin.extras.banners.BannerHandler;
import org.sevensoft.ecreator.iface.admin.extras.calculators.CalculatorHandler;
import org.sevensoft.ecreator.iface.admin.extras.newsfeed.NewsfeedsHandler;
import org.sevensoft.ecreator.iface.admin.extras.polls.PollHandler;
import org.sevensoft.ecreator.iface.admin.extras.rss.RssHandler;
import org.sevensoft.ecreator.iface.admin.extras.wizards.WizardHandler;
import org.sevensoft.ecreator.iface.admin.media.galleries.GalleryHandler;
import org.sevensoft.ecreator.iface.admin.misc.agreements.AgreementsHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Menu;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 13 Oct 2006 13:45:43
 *
 */
public class ExtrasMenu extends Menu {

	public List<LinkTag> getLinkTags(RequestContext context) {

		List<LinkTag> links = new ArrayList();

		if (Module.Forms.enabled(context)) {

			links.add(new LinkTag(FormHandler.class, "list", "Edit form"));
			links.add(new LinkTag(FormHandler.class, "create", "Create form"));
			links.add(new LinkTag(SubmissionHandler.class, null, "Form submissions"));
			links.add(new LinkTag(SubmissionExportHandler.class, null, "Form export"));
		}
        if (Module.FormsReport.enabled(context)){
            links.add(new LinkTag(SubmissionReportModuleHandler.class, null, "Form submissions Report"));
        }

        if (Module.FormSubmissionRestriction.enabled(context)) {
            links.add(new LinkTag(SubmissionRestrictionModuleHandler.class, null, "IP Form Submission Restrictions"));
        }

		if (Module.Newsfeed.enabled(context)) {
			links.add(new LinkTag(NewsfeedsHandler.class, null, "Newsfeeds"));
		}

		if (Module.Polls.enabled(context)) {
			links.add(new LinkTag(PollHandler.class, "list", "Edit poll"));
			links.add(new LinkTag(PollHandler.class, "create", "Create poll"));
		}

		if (Module.Galleries.enabled(context)) {
			links.add(new LinkTag(GalleryHandler.class, "list", "Edit gallery"));
			links.add(new LinkTag(GalleryHandler.class, "create", "Create gallery"));
		}

		if (Module.Agreements.enabled(context)) {
			links.add(new LinkTag(AgreementsHandler.class, null, "Agreements"));
		}

		if (Module.RssExport.enabled(context)) {
			links.add(new LinkTag(RssHandler.class, null, "Rss Export"));
		}

		if (Module.Forum.enabled(context)) {

			links.add(new LinkTag(ForumHandler.class, null, "Forums"));
			links.add(new LinkTag(PostSearchHandler.class, null, "Find forum posts"));
		}

		if (Module.Jobsheets.enabled(context)) {

			links.add(new LinkTag(JobsHandler.class, null, "My jobs"));
			links.add(new LinkTag(JobsHandler.class, "unassigned", "New jobs"));
			links.add(new LinkTag(JobsheetHandler.class, null, "Jobsheets"));
			links.add(new LinkTag(JobSearchHandler.class, null, "Job search"));
			links.add(new LinkTag(AutoJobHandler.class, null, "Auto jobs"));
		}

		if (Module.Calculators.enabled(context)) {
			links.add(new LinkTag(CalculatorHandler.class, null, "Calculators"));
		}

		if (Module.Banners.enabled(context)) {
			links.add(new LinkTag(BannerHandler.class, null, "Banners"));
		}

		if (Module.Wizards.enabled(context)) {
			links.add(new LinkTag(WizardHandler.class, null, "Wizards"));
		}

		return links;
	}

}
