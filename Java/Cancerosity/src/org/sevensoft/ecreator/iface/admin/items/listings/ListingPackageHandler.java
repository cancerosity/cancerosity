package org.sevensoft.ecreator.iface.admin.items.listings;

import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.design.reorder.ObjectsReorderPanel;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;
import org.sevensoft.ecreator.model.items.listings.ListingRate;
import org.sevensoft.ecreator.model.misc.agreements.Agreement;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;
import java.util.List;

/**
 * @author sks 22-Mar-2006 18:45:22
 */
@Path("admin-listings-packages.do")
public class ListingPackageHandler extends AdminHandler {

    private Category category;
    private ListingPackage listingPackage;
    private String name;
    private ItemType itemType;
    private SubscriptionLevel subscription;
    private SubscriptionLevel addSubscriptionLevel;
    private int maxImages;
    private int maxCharacters;
    private Agreement agreement;
    private boolean vetting;
    private String completedMessage;
    private boolean moderation;
    private int maxAttachments;
    private boolean featured;
    private ItemType addItemType;
    private Category addCategory;
    private boolean allCategories;
    private String description;
    private ItemType subscriptionLevel;
    private boolean guest;
    private boolean validateGuest;
    private boolean stock;
    private boolean prices;
    private Amount commission;
    private boolean prioritised;
    private boolean nameChange;
    private String confirmButtonLabel;
    private boolean deliveryRates;
    private String deliveryRatesHeader;
    private String detailPagesHeader;
    private String detailPagesFooter;
    private boolean categoriesFirst;
    private int minImages;
    private int minCharacters;
    private int minAttachments;
    private boolean deliveryRatesOptional;
    private int maxCategories;
    private boolean allowDescription;
    private boolean allowGuestToEdit;
    private String forwardUrl;
    private boolean listingUpdate;
    private String nameDescription;
    private String contentSectionLabel;
    private String contentFieldLabel;
    private String contentFieldDescription;
    private List<ListingPackage> positions;

    public ListingPackageHandler(RequestContext context) {
        super(context);
    }

    public ListingPackageHandler(RequestContext context, ListingPackage listingPackage) {
        super(context);
        this.listingPackage = listingPackage;
    }

    public Object addAllCategories() throws ServletException {

        if (listingPackage == null) {
            return main();
        }

        listingPackage.addCategories(Category.get(context));

        addMessage("All categories have been added to this listing package.");
        return edit();
    }

    public Object create() throws ServletException {

        if (itemType == null) {
            return index();
        }

        itemType.getListingModule().addListingPackage("new " + itemType.getNameLower() + " package");
        return new ListingModuleHandler(context, itemType).main();
    }

    public Object delete() throws ServletException {

        if (listingPackage == null) {
            return main();
        }

        itemType = listingPackage.getItemType();
        itemType.getListingModule().removeListingPackage(listingPackage);

        return new ActionDoc(context, "This listing package has been deleted", new Link(ListingPackageHandler.class));
    }

    public Object edit() throws ServletException {

        if (listingPackage == null) {
            return new SettingsMenuHandler(context).main();
        }

        AdminDoc doc = new AdminDoc(context, user, "Listing package for " + listingPackage.getItemType().getNamePluralLower(), null);
        doc.addBody(new Body() {

            private void access() {

                sb.append(new AdminTable("Access"));

                sb.append(new AdminRow("Guests", "Allow users to add a listing with this package without needing to register for an account.",
                        new BooleanRadioTag(context, "guest", listingPackage.isGuest())));

                if (listingPackage.isGuest()) {

                    sb.append(new AdminRow("Validate guest emails",
                            "Set this to yes if you want to check the user has entered a valid email before guest's listings can go live.",
                            new BooleanRadioTag(context, "validateGuest", listingPackage.isValidateGuest())));

                    sb.append(new AdminRow("Allow guests to view/edit listings", "Set this to yes if you want to allow guests to view/edit listings.",
                            new BooleanRadioTag(context, "allowGuestToEdit", listingPackage.isAllowGuestToEdit())));
                }

                if (Module.Subscriptions.enabled(context)) {

                    List<SubscriptionLevel> currentSubscriptions = listingPackage.getSubscriptions();
                    if (currentSubscriptions.size() > 0) {

                        StringBuilder sb2 = new StringBuilder();
                        for (SubscriptionLevel subscriptionLevel : currentSubscriptions) {

                            sb2.append(subscriptionLevel.getName()
                                    + " "
                                    + new LinkTag(ListingPackageHandler.class, "removeAccess", "Remove", "listingPackage", listingPackage,
                                    "subscriptionLevel", subscriptionLevel) + "<br/>");
                        }

                        sb.append(new AdminRow("Current subscription levels",
                                "These are the subscriptions this package is currently available to.", sb2));

                    }

                    List<SubscriptionLevel> availableSubscriptions = SubscriptionLevel.get(context);
                    availableSubscriptions.removeAll(currentSubscriptions);

                    if (!availableSubscriptions.isEmpty()) {

                        SelectTag tag = new SelectTag(context, "addSubscriptionLevel");
                        tag.setAny("-Choose subscription level-");
                        tag.addOptions(availableSubscriptions);

                        sb.append(new AdminRow("Add subscription level", "Choose a subscription level to add to this package.", tag + " "
                                + new SubmitTag("Add")));
                    }

                }

                List<ItemType> itemTypes = listingPackage.getAccountItemTypes();
                if (itemTypes.size() > 0) {

                    StringBuilder sb2 = new StringBuilder();
                    for (ItemType itemType : itemTypes) {

                        sb2.append(itemType.getName()
                                + " "
                                + new LinkTag(ListingPackageHandler.class, "removeAccess", "Remove", "listingPackage", listingPackage,
                                "itemType", itemType) + "<br/>");
                    }

                    sb.append(new AdminRow("Current item types", "These are the account types that have access to this package.", sb2));

                }

                List<ItemType> availableItemTypes = ItemType.getAccounts(context);
                availableItemTypes.removeAll(itemTypes);

                if (!availableItemTypes.isEmpty()) {

                    SelectTag tag = new SelectTag(context, "addItemType");
                    tag.setAny("-Choose item type-");
                    tag.addOptions(availableItemTypes);

                    sb.append(new AdminRow("Add access", "Choose an account type to give access to this package.", tag + " " + new SubmitTag("Add")));
                }

                sb.append("</table>");

            }

            private void categories() {

                sb.append(new AdminTable("Categories"));

                sb.append(new AdminRow("Use all categories",
                        "Set this to yes to use all categories on the site regardless if you have added them into this package. "
                                + "This is useful as new categories created on the site will automatically be included.", new BooleanRadioTag(
                        context, "allCategories", listingPackage.isAllCategories())));

                sb.append(new AdminRow("Categories first", "Select the category before any other details of the listing.", new BooleanRadioTag(context,
                        "categoriesFirst", listingPackage.isCategoriesFirst())));

                sb.append(new AdminRow("Max categories", "Select the maximum number of categories a user can put a listing in.", new TextTag(context,
                        "maxCategories", listingPackage.getMaxCategories(), 4)));

                if (!listingPackage.isAllCategories()) {

                    List<Category> currentCategories = listingPackage.getCategories();
                    if (currentCategories.size() > 0) {

                        StringBuilder sb2 = new StringBuilder();
                        for (Category category : currentCategories) {

                            sb2.append(category.getName()
                                    + " "
                                    + new LinkTag(ListingPackageHandler.class, "removeCategory", "Remove", "listingPackage", listingPackage,
                                    "category", category) + "<br/>");
                        }

                        sb.append(new AdminRow("Current categories", "These are the categories that items using this package can be listed in.",
                                sb2));

                    }

                    List<Category> availableCategories = Category.get(context, false);
                    availableCategories.removeAll(currentCategories);

                    if (!availableCategories.isEmpty()) {

                        SelectTag tag = new SelectTag(context, "addCategory");
                        tag.setAny("-Choose category-");
                        tag.addOptions(availableCategories);

                        sb.append(new AdminRow("Add category", "Choose a category to add to this package.", tag + " " + new SubmitTag("Add")));

                        sb.append(new AdminRow("Add all categories", "Click here to quickly add all categories to this package.", new ButtonTag(
                                ListingPackageHandler.class, "addAllCategories", "Add all categories", "listingPackage", listingPackage)));

                        sb.append(new AdminRow("Remove all categories", "Click here to quickly remove all categories from this package.",
                                new ButtonTag(ListingPackageHandler.class, "removeAllCategories", "Remove all categories", "listingPackage",
                                        listingPackage)));

                    }

                }

                sb.append("</table>");
            }

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update listing package"));
                sb.append(new ButtonTag(ListingPackageHandler.class, "copy", "Copy listing package", "listingPackage", listingPackage));
                sb.append(new ButtonTag(ListingPackageHandler.class, "delete", "Delete listing package", "listingPackage", listingPackage)
                        .setConfirmation("Are you sure you want to delete this listing package?"));
                sb.append(new ButtonTag(ListingModuleHandler.class, null, "Return to listing module", "itemType", listingPackage.getItemType()));

                sb.append("</div>");
            }

            private void detailsPage() {

                sb.append(new AdminTable("Detail pages"));

                sb.append("<tr><td>Detail pages header</td></tr>");
                sb.append("<tr><td>"
                        + new TextAreaTag(context, "detailPagesHeader", listingPackage.getDetailPagesHeader()).setId("detailPagesHeader").setStyle(
                        "width: 100%; height: 320px;") + "</td></tr>");

                sb.append("<tr><td>Detail pages footer</td></tr>");
                sb.append("<tr><td>"
                        + new TextAreaTag(context, "detailPagesFooter", listingPackage.getDetailPagesFooter()).setId("detailPagesFooter").setStyle(
                        "width: 100%; height: 320px;") + "</td></tr>");

                sb.append("</table>");

            }

            private void general() {

                sb.append(new AdminTable("General"));

                sb.append(new AdminRow("Name", "Enter the name of this listing package", new TextTag(context, "name", listingPackage.getName(), 40)));

                sb.append(new AdminRow("Description", "Enter a general description about this listing package for the customer to read.",
                        new TextAreaTag(context, "description", listingPackage.getDescription(), 60, 3)));

                List<Agreement> agreements = Agreement.get(context);
                if (agreements.size() > 0) {

                    SelectTag agreementsTag = new SelectTag(context, "agreement", listingPackage.getAgreement());
                    agreementsTag.setAny("-No agreement-");
                    agreementsTag.addOptions(agreements);

                    sb.append(new AdminRow("Agreement",
                            "If you set an agreement then the user must agree to the terms of the agreement before they can add their listing.",
                            agreementsTag));

                }

                sb.append(new AdminRow("Allow listing updates",
                        "Set to yes if users can update detailes of their listing once it has been created.", new BooleanRadioTag(context,
                        "listingUpdate", listingPackage.isListingUpdate())));

                sb.append(new AdminRow("Allow name changes",
                        "Set to yes if users can rename the title/name of their listing once it has been created.", new BooleanRadioTag(context,
                        "nameChange", listingPackage.isNameChange())));

                if (listingPackage.getItemType().isContent()) {

                    sb.append(new AdminRow("Allow description", "Enable/Disable description altogether for listings in this package.",
                            new BooleanRadioTag(context, "allowDescription", listingPackage.isAllowDescription())));

                    sb.append(new AdminRow("Max characters",
                            "If you want to limit the number of characters a member can enter for their listings, you can set that here. "
                                    + "A value of zero means unlimited characters.", new TextTag(context, "maxCharacters", listingPackage
                            .getMaxCharacters(), 4)));

                    sb
                            .append(new AdminRow(
                                    "Min characters",
                                    "Set a minimum amount of characters that a user must enter as part of the description in order to proceed with a listing.",
                                    new TextTag(context, "minCharacters", listingPackage.getMinCharacters(), 4)));

                }

                if (ItemModule.Images.enabled(context, listingPackage.getItemType())) {

                    sb.append(new AdminRow("Max images",
                            "If you want to limit the number of images a user can attach to their listings, you can set that here. "
                                    + "A value of zero means images is disabled for this package.", new TextTag(context, "maxImages",
                            listingPackage.getMaxImages(), 4)));

                    sb.append(new AdminRow("Min images", "Set a minimum amount of images that a user must upload to proceed with a listing.",
                            new TextTag(context, "minImages", listingPackage.getMinImages(), 4)));

                }

                if (ItemModule.Pricing.enabled(context, listingPackage.getItemType())) {

                    sb.append(new AdminRow("Prices", "Allow users to enter the selling price of their listing.", new BooleanRadioTag(context,
                            "prices", listingPackage.isPrices())));

                    sb.append(new AdminRow("Comissison", "This commission is added to the selling price.", new TextTag(context, "commission",
                            listingPackage.getCommission())));

                }

                if (ItemModule.Stock.enabled(context, listingPackage.getItemType())) {

                    sb.append(new AdminRow("Stock / Availability", "Allow users to enter the stock level and / or availability of their listing.",
                            new BooleanRadioTag(context, "stock", listingPackage.isStock())));

                }

                if (Module.ListingAttachments.enabled(context)) {

                    sb.append(new AdminRow("Max attachments", "Set how many attachments users can upload as part of this listing package. "
                            + "A value of zero disables attachments.", new TextTag(context, "maxAttachments", listingPackage.getMaxAttachments(),
                            4)));

                    sb.append(new AdminRow("Min images", "Set a minimum amount of attachments that a user must upload to proceed with a listing.",
                            new TextTag(context, "minAttachments", listingPackage.getMinAttachments(), 4)));
                }

                if (Module.ListingModeration.enabled(context)) {

                    sb.append(new AdminRow("Moderate listings",
                            "If you set this to yes then any listings submitted by members (using this listing package) "
                                    + "will have to be approved by you before they become live on the site.", new BooleanRadioTag(context,
                            "moderation", listingPackage.isModeration())));

                }

                if (listingPackage.getItemType().isFeatured()) {

                    sb.append(new AdminRow("Featured", "Set items created through this package as 'featured' items.", new BooleanRadioTag(context,
                            "featured", listingPackage.isFeatured())));

                }

                if (listingPackage.getItemType().isPrioritised()) {

                    sb.append(new AdminRow("Prioritised", "Set items created through this package as 'prioritised' items.", new BooleanRadioTag(
                            context, "prioritised", listingPackage.isPrioritised())));

                }

                if (ItemModule.Delivery.enabled(context, listingPackage.getItemType())) {

                    sb.append(new AdminRow("Delivery rates", "Allow users to enter the delivery rates for their listings.", new BooleanRadioTag(
                            context, "deliveryRates", listingPackage.isDeliveryRates())));

                    sb.append(new AdminRow("Delivery rates optional", "Set to yes if you want customers to be able to leave delivery rates blank.",
                            new BooleanRadioTag(context, "deliveryRatesOptional", listingPackage.isDeliveryRatesOptional())));

                    if (listingPackage.isDeliveryRates()) {

                        sb.append(new AdminRow("Delivery rates header", "Enter the text you want to appear above the delivery rates input boxes.",
                                new TextAreaTag(context, "deliveryRatesHeader", listingPackage.getDeliveryRatesHeader(), 50, 5)));
                    }

                }

                sb.append(new AdminRow("Completed message", "Enter a message you want to be displayed to the member "
                        + "when they have succcessfully listed an item using this package.", new TextAreaTag(context, "completedMessage",
                        listingPackage.getCompletedMessage(), 50, 3)));

                sb.append(new AdminRow("Confirm button label", "Change the text on the confirm button", new TextTag(context, "confirmButtonLabel",
                        listingPackage.getConfirmButtonLabel(), 20)));

                sb.append(new AdminRow("Forward URL", "When a listing has been completed the client can be forwarded to a specific page rather than the current page",
                        new TextTag(context, "forwardUrl", listingPackage.getForwardUrl(), 50)));

                sb.append("</table>");
            }

            private void rates() {

                sb.append(new AdminTable("Rates"));

                sb.append("<tr><td colspan='4'>These are the different charges that you can setup for this package. "
                        + "Each rate can have a separate price and length of advert. "
                        + "If you do not create any rates then this package will be free and listings will not expire.</td></tr>");

                List<ListingRate> rates = listingPackage.getListingRates();
                if (rates.size() > 0) {

                    sb.append("<tr>");
                    sb.append("<th width='10'> </th>");
                    sb.append("<th>Fee</th>");
                    sb.append("<th>Period</th>");
                    sb.append("<th width='10'> </th>");
                    sb.append("</tr>");

                    for (ListingRate listingRate : rates) {

                        sb.append("<tr>");
                        sb.append("<td width='10'>" + new LinkTag(ListingRateHandler.class, null, new SpannerGif(), "listingRate", listingRate)
                                + "</td>");
                        sb.append("<td>" + listingRate.getFeeEx() + "</td>");
                        sb.append("<td>" + listingRate.getPeriod() + "</td>");
                        sb.append("<td width='10'>"
                                + new LinkTag(ListingRateHandler.class, "delete", new DeleteGif(), "listingRate", listingRate)
                                .setConfirmation("Are you sure you want to delete this listing rate?") + "</td>");
                        sb.append("</tr>");
                    }

                }

                sb.append("<tr><td colspan='4'>" + new ButtonTag(ListingRateHandler.class, "create", "Add rate", "listingPackage", listingPackage)
                        + "</td></tr>");

                sb.append("</table>");
            }

            private void fieldsDescriptions() {

                sb.append(new AdminTable("Fields descriptions"));

                sb.append(new AdminRow("Name Description", "", new TextAreaTag(context, "nameDescription", listingPackage.getNameDescription(), 80, 3)));
                sb.append(new AdminRow("Content section Label", "", new TextTag(context, "contentSectionLabel", listingPackage.getContentSectionLabel(), 80)));
                sb.append(new AdminRow("Content field Label", "", new TextTag(context, "contentFieldLabel", listingPackage.getContentFieldLabel(), 80)));
                sb.append(new AdminRow("Content field Description", "", new TextAreaTag(context, "contentFieldDescription", listingPackage.getContentFieldDescription(), 80, 5)));

                sb.append("</table>");

            }


            @Override
            public String toString() {

                sb.append(new FormTag(ListingPackageHandler.class, "save", "POST"));
                sb.append(new HiddenTag("listingPackage", listingPackage));

                commands();
                general();
                rates();
                access();
                categories();
                detailsPage();
                fieldsDescriptions();

                commands();

                sb.append("</form>");

                return sb.toString();
            }
        });
        return doc;
    }

    @Override
    public Object main() throws ServletException {

        AdminDoc doc = new AdminDoc(context, user, "Listing packages", null);
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new FormTag(ListingPackageHandler.class, "reorder", "POST"));
                commands();

                general();

                commands();

                sb.append("</form>");

                createPackage();

                return sb.toString();


            }

            private void general() {
                sb.append(new AdminTable("Listing packages"));

                sb.append("<tr>");
                sb.append("<th width='60'>Position</th>");
                sb.append("<th>Name</th>");
                sb.append("<th width='10'> </th>");
                sb.append("</tr>");

//                PositionRender pr = new PositionRender(ListingPackageHandler.class, "listingPackage");
//                for (ListingPackage listingPackage : ListingPackage.get(context)) {
                ObjectsReorderPanel panel = new ObjectsReorderPanel(context, null, ListingPackage.get(context));

                panel.setEditClazz(ListingPackageHandler.class);
                panel.setEditAction("edit");
                panel.setEditParam("listingPackage");

                panel.setDeleteClazz(ListingPackageHandler.class);
                panel.setDeleteAction("delete");
                panel.setDeleteParam("listingPackage");
                panel.setDeleteConfirmation("Are you sure you want to delete this listing package?");

                sb.append("<tr>");
                sb.append("<td colspan='3'>");
                sb.append(panel);
                sb.append("</td>");
                sb.append("</tr>");

//                }

                sb.append("</table>");

            }

            private void createPackage() {
                final List<ItemType> listable = ItemType.getListable(context);

                if (listable.size() > 0) {
                    sb.append(new AdminTable("Create Listing packages"));
                    SelectTag tag = new SelectTag(context, "itemType");
                    tag.addOptions(listable);

                    sb.append(new FormTag(ListingPackageHandler.class, "create", "post"));
                    sb.append("<tr><td colspan='3'>Create new listing package for " + tag + " " + new SubmitTag("Create") + "</td></tr>");
                    sb.append("</form>");
                    sb.append("</table>");
                }
            }

            private void commands() {
                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update"));
                sb.append("</div>");
            }

        });
        return doc;
    }

    public Object moveDown() throws ServletException {

        if (listingPackage == null) {
            return main();
        }

        List<ListingPackage> packages = ListingPackage.get(context);
        EntityUtil.moveDown(listingPackage, packages);
        return main();
    }

    public Object moveUp() throws ServletException {

        if (listingPackage == null) {
            return main();
        }

        List<ListingPackage> packages = ListingPackage.get(context);
        EntityUtil.moveUp(listingPackage, packages);
        return main();
    }

    public Object removeAccess() throws ServletException {

        if (listingPackage == null) {
            return main();
        }

        if (subscriptionLevel != null) {
            listingPackage.removeAccess(subscriptionLevel);
        }

        if (itemType != null) {
            listingPackage.removeAccess(itemType);
        }

        clearParameters();
        return edit();
    }

    public Object removeAllCategories() throws ServletException {

        if (listingPackage == null) {
            return main();
        }

        listingPackage.removeCategories();

        addMessage("All categories have been removed from this listing package.");
        return edit();
    }

    public Object removeCategory() throws ServletException {

        if (listingPackage == null || category == null) {
            return main();
        }

        listingPackage.removeCategory(category);
        addMessage("Category removed");
        clearParameters();
        return edit();
    }

    public Object reorder() throws ServletException {
        reorder(positions);

        return new ActionDoc(context, "Listing packages positions have been updated", new Link(ListingPackageHandler.class));
    }

    public Object save() throws ServletException {

        if (listingPackage == null) {
            return main();
        }

        listingPackage.setName(name);
        listingPackage.setListingUpdate(listingUpdate);
        listingPackage.setNameChange(nameChange);
        listingPackage.setMaxCharacters(maxCharacters);
        listingPackage.setMinCharacters(minCharacters);
        listingPackage.setAllowDescription(allowDescription);

        listingPackage.setMaxImages(maxImages);
        listingPackage.setMinImages(minImages);

        listingPackage.setAgreement(agreement);
        listingPackage.setModeration(moderation);

        listingPackage.setMaxAttachments(maxAttachments);
        listingPackage.setMinAttachments(minAttachments);

        listingPackage.setCompletedMessage(completedMessage);
        listingPackage.setFeatured(featured);
        listingPackage.setPrioritised(prioritised);
        listingPackage.setForwardUrl(forwardUrl);

        // categories
        listingPackage.setAllCategories(allCategories);
        listingPackage.setCategoriesFirst(categoriesFirst);
        listingPackage.setMaxCategories(maxCategories);

        listingPackage.setDescription(description);
        listingPackage.setGuest(guest);
        listingPackage.setValidateGuest(validateGuest);
        listingPackage.setAllowGuestToEdit(allowGuestToEdit);
        listingPackage.setPrices(prices);
        listingPackage.setStock(stock);
        listingPackage.setCommission(commission);
        listingPackage.setConfirmButtonLabel(confirmButtonLabel);

        listingPackage.setDeliveryRates(deliveryRates);
        listingPackage.setDeliveryRatesHeader(deliveryRatesHeader);
        listingPackage.setDeliveryRatesOptional(deliveryRatesOptional);

        listingPackage.setDetailPagesFooter(detailPagesFooter);
        listingPackage.setDetailPagesHeader(detailPagesHeader);

        listingPackage.setNameDescription(nameDescription);
        listingPackage.setContentSectionLabel(contentSectionLabel);
        listingPackage.setContentFieldLabel(contentFieldLabel);
        listingPackage.setContentFieldDescription(contentFieldDescription);

        listingPackage.save();

        if (addCategory != null) {
            listingPackage.addCategory(addCategory);
        }

        if (addItemType != null) {
            listingPackage.addAccess(addItemType);
        }

        if (addSubscriptionLevel != null) {
            listingPackage.addAccess(addSubscriptionLevel);
        }

        return new ActionDoc(context, "This listing package has been updated", new Link(ListingPackageHandler.class, "edit", "listingPackage",
                listingPackage));
    }

    public Object copy() throws ServletException {

        if (listingPackage == null) {
            return main();
        }

        try {

            final ListingPackage copy = listingPackage.clone();

            clearParameters();

            AdminDoc page = new AdminDoc(context, user, "listingPackage" + " copied", null);
            page.addBody(new Body() {

                @Override
                public String toString() {

                    sb.append("<div align='center' class='confirmation'>");
                    sb.append("<h2>'" + listingPackage.getName() + "' has been copied.</h2>");
                    sb.append(new LinkTag(ListingPackageHandler.class, "edit", "Click here to edit the original", "listingPackage", listingPackage));
                    sb.append("<br/><br/>");
                    sb.append(new LinkTag(ListingPackageHandler.class, "edit", "Click here to edit the new copy", "listingPackage", copy));
                    sb.append("</div>");

                    return sb.toString();
                }
            });
            return page;

        } catch (CloneNotSupportedException e) {
            throw new ServletException(e);

        }
    }
}
