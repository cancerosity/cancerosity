package org.sevensoft.ecreator.iface.admin.ecom.bookings.config;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.bookings.BookingSlot;
import org.sevensoft.ecreator.model.bookings.ItemPeriodPrice;
import org.sevensoft.ecreator.model.bookings.Period;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;


/**
 * User: MeleshkoDN
 * Date: 12.09.2007
 * Time: 12:28:38
 */
@Path("admin-items-period-price.do")
public class ItemPeriodPriceHandler extends AdminHandler {

    private ItemPeriodPrice itemPeriodPrice;
    private Period period;
    private Item item;
    private Money dailyPrice;
    private Money weeklyPrice;

    public ItemPeriodPriceHandler(RequestContext context) {
        super(context);
    }


    public Object main() throws ServletException {

        if (item == null) {
            return index();
        }


        AdminDoc doc = new AdminDoc(context, user, "Period prices", Tab.getItemTab(item));
        doc.setMenu(new EditItemMenu(item));
        doc.setIntro("Manage period prices  ");
        doc.addBody(new Body() {

            private void commands() {
                sb.append("<div align='center' class='actions'>");
                sb.append(new ButtonTag(BookingDatesHandler.class, null, "Return to calendar", "item", item));
                sb.append(new SubmitTag("Apply prices"));
                sb.append("</div>");
            }

            private void general() {
                sb.append(new AdminTable("Period prices"));

                sb.append(new AdminRow("Period", null, period.getStartDate().toString("dd.MM") + "-" + period.getEndDate().toString("dd.MM")));
                sb.append(new AdminRow("Daily price", null, new TextTag(context, "dailyPrice", itemPeriodPrice != null ? itemPeriodPrice.getDailyPrice().toEditString() : new Money(0), 12)));
                if (period.isWeekly()) {
                    sb.append(new AdminRow("Weekly price", null, new TextTag(context, "weeklyPrice", itemPeriodPrice != null ? itemPeriodPrice.getWeeklyPrice().toEditString() : new Money(0), 12)));
                }
                sb.append("</table>");
            }

            @Override
            public String toString() {
                sb.append(new FormTag(ItemPeriodPriceHandler.class, "save", "POST"));
                sb.append(new HiddenTag("itemPeriodPrice", itemPeriodPrice));
                sb.append(new HiddenTag("item", item));
                sb.append(new HiddenTag("period", period));

                commands();
                general();
                commands();

                sb.append("</form>");
                return sb.toString();
            }
        });
        return doc;
    }

    public Object save() {
        if (itemPeriodPrice == null) {
            itemPeriodPrice = new ItemPeriodPrice(context);
            itemPeriodPrice.setPeriod(period);
            itemPeriodPrice.setItem(item);
        }
        itemPeriodPrice.setDailyPrice(dailyPrice);
        if (period.isWeekly()) {
            if (weeklyPrice == null || weeklyPrice.isZero()) {
                weeklyPrice = dailyPrice.multiply(7d);
            }
            itemPeriodPrice.setWeeklyPrice(weeklyPrice);
        }
        itemPeriodPrice.save();
        updateBookingSlots(itemPeriodPrice);
        return new ActionDoc(context, "Prices were successfully applied", new Link(BookingDatesHandler.class, null, "item", item));
    }


    private void updateBookingSlots(ItemPeriodPrice itemPeriodPrice) {
        Date date = period.getStartDate();
        Date endPeriod = period.getEndDate().nextDay();
        while (!date.equals(endPeriod)) {
            BookingSlot slot = SimpleQuery.get(context, BookingSlot.class, "date", date, "item", item);
            if (slot == null) {
                slot = new BookingSlot(context, item, date);
            }
            if (slot.getStatus() == BookingSlot.Status.Open) {
                slot.setRate(itemPeriodPrice.getDailyPrice());
            }
            slot.save();
            date = date.nextDay();
        }
    }
}
