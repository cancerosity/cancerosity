package org.sevensoft.ecreator.iface.admin.items.modules.alternatives;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.items.alternatives.AlternativesBlock;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 25 May 2007 06:51:35
 *
 */
@Path("blocks-alt.do")
public class AlternativesBlockHandler extends BlockEditHandler {

	private int				limit;
	private AlternativesBlock	block;

	public AlternativesBlockHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Block getBlock() {
		return block;
	}

	@Override
	protected void saveSpecific() {
	}

	@Override
	protected void specifics(StringBuilder sb) {

	}

}
