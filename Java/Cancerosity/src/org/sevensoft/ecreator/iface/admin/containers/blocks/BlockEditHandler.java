package org.sevensoft.ecreator.iface.admin.containers.blocks;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.admin.categories.CategorySettingsHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.blocks.ItemsBlock;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

import javax.servlet.ServletException;
import java.util.List;

/**
 * @author sks 26 Jun 2006 17:32:16
 */
public abstract class BlockEditHandler extends AdminHandler {

    protected Category category;
    protected int markupTds;
    protected String cssId;
    protected String cssClass;
    protected String name;
    private Markup markup;
    private boolean firstPageOnly;
    private boolean visible;
    private Category copyCategory;

    public BlockEditHandler(RequestContext context) {
        super(context);
    }

    public final Object delete() throws ServletException {

        Block block = getBlock();
        if (block == null) {
            return main();
        }

        category = block.getOwnerCategory();
        block.delete();

        return getHandler();
    }

    protected abstract Block getBlock();

    private final Object getHandler() throws ServletException {

        Block block = getBlock();
        if (block.hasOwnerCategory()) {
            return new CategoryHandler(context, block.getOwnerCategory()).edit();
        }

        if (block.hasOwnerItemType()) {
            return new ItemTypeHandler(context, block.getOwnerItemType()).edit();
        }

        return new CategorySettingsHandler(context).main();
    }

    protected AdminDoc getPage() {

        final Block block = getBlock();
        String title = getTitle();

        AdminDoc doc = new AdminDoc(context, user, title, Tab.Categories);
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update block"));

                if (block.hasOwnerCategory()) {
                    sb.append(new ButtonTag(CategoryHandler.class, "edit", "Return to category", "category", block.getOwnerCategory()));

                } else if (block.hasOwnerItemType()) {
                    sb.append(new ButtonTag(ItemTypeHandler.class, "edit", "Return to " + block.getOwnerItemType().getNameLower(), "itemType", block
                            .getOwnerItemType()));

                } else {
                    sb.append(new ButtonTag(CategorySettingsHandler.class, null, "Return to category settings"));
                }

                sb.append("</div>");
            }

            private void general() {

                sb.append(new AdminTable("General"));

                if (block.isRenameable()) {
                    sb.append(new AdminRow("Name", "Rename this block ", new TextTag(context, "name", block.getName(), 30)));
                }

                sb.append(new AdminRow("Visible", "Is this block viewable on the site? "
                        + "You can turn blocks invisible so that you can hide them temporary and re-enable later", new BooleanRadioTag(context,
                        "visible", block.isVisible())));

                sb.append(new AdminRow("Content first page only", "Show the content on the first page of results only.", new BooleanRadioTag(context,
                        "firstPageOnly", block.isFirstPageOnly())));

                if (block.hasOwnerCategory()) {
                    if (block.isCopyable()) {
                        sb.append(new AdminRow("Copy block", "Choose another category to copy this block to", new SelectTag(context,
                                "copyCategory", null, Category.getCategoryOptions(context), "-Select block-")));
                    }
                }

                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(block.getEditHandler(), "save", "post"));
                sb.append(new HiddenTag("block", block));

                commands();
                
                if (miscSettings.isAdvancedMode()) {
                    general();
                }

                BlockEditHandler.this.specifics(sb);
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    protected String getTitle() {
        return getBlock().getLabel();
    }

    @Override
    public Object main() throws ServletException {

        if (getBlock() == null) {
            return index();
        }

        if (getBlock().hasOwnerCategory()) {
            category = getBlock().getOwnerCategory();
            context.setAttribute("view", category.getUrl());
        }

        return getPage();
    }

    public final Object moveDown() throws ServletException {

        Block block = getBlock();
        if (block == null)
            return main();

        category = block.getOwnerCategory();
        List<Block> blocks = category.getBlocks();

        EntityUtil.moveDown(block, blocks);
        return getHandler();
    }

    public final Object moveUp() throws ServletException {

        Block block = getBlock();
        if (block == null) {
            return main();
        }

        category = block.getOwnerCategory();
        List<Block> blocks = category.getBlocks();

        EntityUtil.moveUp(block, blocks);
        return getHandler();
    }

    public final Object save() throws ServletException {

        final Block block = getBlock();
        if (block == null) {
            return main();
        }

        if (miscSettings.isAdvancedMode()) {

            block.setFirstPageOnly(firstPageOnly);
            block.setVisible(visible);

            if (block.isRenameable()) {
                block.setName(name);
            }

        }

        saveSpecific();
        block.save();

        if (copyCategory != null) {

            try {

                block.clone(copyCategory);

                AdminDoc doc = new AdminDoc(context, user, "The block has been copied", Tab.Categories);
                doc.addBody(new Body() {

                    @Override
                    public String toString() {

                        sb.append(new TableTag("simplelinks", "center"));
                        sb.append("<tr><th align='center'>What would you like to do ?</th></tr>");

                        sb.append("<tr><td align='center'>"
                                + new LinkTag(CategoryHandler.class, "edit", "I want to return to the category with the original block",
                                "category", block.getOwnerCategory())
                                + "</td></tr>");

                        sb.append("<tr><td align='center'>"
                                + new LinkTag(CategoryHandler.class, "edit", "I want to go to the category with the new block", "category",
                                copyCategory) + "</td></tr>");

                        sb.append("</table>");

                        return sb.toString();
                    }
                });
                return doc;

            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }

        clearParameters();
        return main();
    }

    protected abstract void saveSpecific();

    protected abstract void specifics(StringBuilder sb);
}
