package org.sevensoft.ecreator.iface.admin.feeds.bespoke.events;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;

import org.jdom.JDOMException;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.events.CamdramFeed;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;

/**
 * @author sks 4 Aug 2006 15:28:56
 *
 */
@Path("admin-feeds-camdram.do")
public class CamdramFeedHandler extends FeedHandler {

	private CamdramFeed	feed;
	private Attribute		societyAttribute;
	private Attribute		showIdAttribute;
	private Attribute		timeAttribute;
	private Attribute		venueAttribute;
	private Attribute		linkAttribute;
	private Attribute		dateAttribute;
	private Attribute		authorAttribute;
	private String		venues;
	private ItemType		itemType;

	public CamdramFeedHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected ImportFeed getFeed() {
		return feed;
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Camdram specifics"));

		sb.append(new AdminRow("Item type", "Select the item type to import to.", new SelectTag(context, "itemType", feed.getItemType(), ItemType
				.getSelectionMap(context), "-None set-")));

		TextAreaTag venuesTag = new TextAreaTag(context, "venues", StringHelper.implode(feed.getVenues(), "\n", true), 50, 6);
		sb.append(new AdminRow("Accepted venues", "Enter the venues you want events to be shown at. Put each venue on a new line.", venuesTag + "<br/>"
				+ new ButtonTag(CamdramFeedHandler.class, "showAvailableVenues", "Show available venues", "feed", feed)));

		if (feed.hasItemType()) {

			SelectTag attributeTag = new SelectTag(context, "authorAttribute", feed.getAuthorAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append("<tr><th width='300'><b>Author attribute</b></th><td>" + attributeTag + "</td></tr>");

			attributeTag = new SelectTag(context, "venueAttribute", feed.getVenueAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append("<tr><th width='300'><b>Venue attribute</b></th><td>" + attributeTag + "</td></tr>");

			attributeTag = new SelectTag(context, "dateAttribute", feed.getDateAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append("<tr><th width='300'><b>Date / Time attribute</b></th><td>" + attributeTag + "</td></tr>");

			attributeTag = new SelectTag(context, "showIdAttribute", feed.getShowIdAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append("<tr><th width='300'><b>Show ID attribute</b></th><td>" + attributeTag + "</td></tr>");

			attributeTag = new SelectTag(context, "societyAttribute", feed.getSocietyAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append("<tr><th width='300'><b>Society attribute</b></th><td>" + attributeTag + "</td></tr>");

			attributeTag = new SelectTag(context, "linkAttribute", feed.getLinkAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append("<tr><th width='300'><b>Link attribute</b></th><td>" + attributeTag + "</td></tr>");

		}

		sb.append("</table>");

	}

	@Override
	protected void saveSpecific() {

		feed.setAuthorAttribute(authorAttribute);
		feed.setDateAttribute(dateAttribute);
		feed.setLinkAttribute(linkAttribute);
		feed.setVenueAttribute(venueAttribute);
		feed.setShowIdAttribute(showIdAttribute);
		feed.setSocietyAttribute(societyAttribute);
		feed.setVenues(StringHelper.explodeStrings(venues, "\n"));
		feed.setItemType(itemType);
	}

	/**
	 * Output a list of available venues
	 * @throws ServletException 
	 */
	public Object showAvailableVenues() throws ServletException {

		if (feed == null)
			return main();

		try {
			Set<String> venues = feed.getAvailableVenues();
			return StringHelper.implode(venues, "\n", true);
		} catch (IOException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (JDOMException e) {
			e.printStackTrace();
			return e.getMessage();
		}

	}
}
