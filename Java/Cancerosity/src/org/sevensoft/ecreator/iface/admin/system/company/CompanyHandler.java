package org.sevensoft.ecreator.iface.admin.system.company;

import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.frontend.categories.CategoryHandler;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 01-Jun-2004 21:28:42
 */
@Path("admin-settings-company.do")
public class CompanyHandler extends AdminHandler {

	private transient Company		company		= Company.getInstance(context);

	private Country				country;
	private String				vatNumber, companyNumber, name, postcode, email, address, telephone, fax;

	private transient MiscSettings	miscSettings	= MiscSettings.getInstance(context);

	public CompanyHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() {

		AdminDoc page = new AdminDoc(context, user, "Company details", Tab.CompanyDetails);
		page.setIntro("Enter the details of your site here");

		page.addBody(new Body() {

			private void advanced() {

				sb.append(new AdminTable("Advanced"));

				sb.append("<tr><td width='240' class='key'><b>Registration number</b></br>"
						+ "The company registration number if your company is limited or a plc.</td><td>"
						+ new TextTag(context, "companyNumber", company.getCompanyNumber()) + "</td></tr>");

				sb.append("<tr><td width='240' class='key'><b>Vat number</b><br/>"
						+ "If you are VAT registered then enter your VAT number to enable VAT specific functionality.</td><td>"
						+ new TextTag(context, "vatNumber", company.getVatNumber()) + "</td></tr>");

				sb.append("</table>");
			}

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update company details"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append("<tr><td width='240' class='key'><b>Name</b></br>This is the name of your site.</td><td>"
						+ new TextTag(context, "name", company.getName(), 40) + new ErrorTag(context, "name", "<br/>") + "</td></tr>");

				sb.append("<tr><td width='240' class='key'><b>Address</b><br/>The contact address for your site.</td><td>"
						+ new TextAreaTag(context, "address", company.getAddress(), 40, 4) + "</td></tr>");

				sb.append("<tr><td width='240' class='key'><b>Postcode</b><br/>The postcode for your site.</td><td>"
						+ new TextTag(context, "postcode", company.getPostcode(), 12) + "</td></tr>");

				SelectTag tag = new SelectTag(context, "country", company.getCountry());
				tag.addOptions(Country.getAll());

				sb.append("<tr><td width='240' class='key'><b>Country</b><br/>The country location of your site.</td><td>" + tag + "</td></tr>");

				sb.append("<tr><td width='240' class='key'><b>Email</b><br/>The main contact email for your site.</td><td>"
						+ new TextTag(context, "email", company.getEmail(), 40) + "</td></tr>");

				sb.append("<tr><td width='240' class='key'><b>Telephone</b><br/>Your contact telephone if applicable.</td><td>"
						+ new TextTag(context, "telephone", company.getTelephone()) + "</td></tr>");

				sb.append("<tr><td width='240' class='key'><b>Fax</b><br/>Your contact fax if applicable.</td><td>"
						+ new TextTag(context, "fax", company.getFax()) + "</td></tr>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(CompanyHandler.class, "save", "POST"));
				commands();
                general();
				advanced();
				commands();
				sb.append("</form>");

				return sb.toString();
			}

		});

		return page;
	}

	public Object save() {

		test(new RequiredValidator(), "name");

		if (hasErrors())
			return main();

		company.setCountry(country);
		company.setAddress(address);
		company.setEmail(email);
		company.setPostcode(postcode);
		company.setTelephone(telephone);
		company.setFax(fax);
		company.setName(name);
		company.setVatNumber(vatNumber);
		company.setCompanyNumber(companyNumber);
		company.save();

        //invoise header and footer will be updated with new company detailes
        OrderSettings settings = OrderSettings.getInstance(context);
        settings.setInvoiceHeader(null);
        settings.setInvoiceFooter(null);
        settings.save();

        clearParameters();

		AdminDoc page = new AdminDoc(context, user, "The company details have been updated", null);
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("simplelinks", "center"));
				sb.append("<tr><th align='center'>What would you like to do ?</th></tr>");

				sb.append("<tr><td align='center'>" + new LinkTag(CompanyHandler.class, null, "I want to edit the company details again")
						+ "</td></tr>");

				sb.append("<tr><td align='center'>" + new LinkTag(CategoryHandler.class, null, "I want to view the front page of the site")
						+ "</td></tr>");

				sb.append("</table>");

				return sb.toString();
			}

		});
		return page;

	}
}