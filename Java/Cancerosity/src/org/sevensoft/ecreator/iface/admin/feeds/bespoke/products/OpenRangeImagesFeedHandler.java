package org.sevensoft.ecreator.iface.admin.feeds.bespoke.products;

import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.model.feeds.bespoke.products.openrange.OpenRangeImagesFeed;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
@Path("admin-feeds-open-range-images.do")
public class OpenRangeImagesFeedHandler extends FeedHandler {

    private OpenRangeImagesFeed feed;

    public OpenRangeImagesFeedHandler(RequestContext context) {
        super(context);
    }

    protected OpenRangeImagesFeed getFeed() {
        return feed;
    }

    protected void saveSpecific() {
    }

    protected void specifics(StringBuilder sb) {
    }
}
