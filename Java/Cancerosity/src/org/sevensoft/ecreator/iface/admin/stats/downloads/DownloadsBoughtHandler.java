package org.sevensoft.ecreator.iface.admin.stats.downloads;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.reports.StatsMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.attachments.downloads.Download;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import javax.servlet.ServletException;
import java.util.List;

/**
 * @author sks 11 Jul 2007 14:45:14
 */
@Path("admin-stats-downloads-bought.do")
public class DownloadsBoughtHandler extends AdminHandler {

    private Date month;

    public DownloadsBoughtHandler(RequestContext context) {
        super(context);
    }

    @Override
    public Object main() throws ServletException {

        if (month == null) {
            month = new Date().beginMonth();
        }

        final List<Download> downloads = Download.getByMonth(context, month);
//        Collections.reverse(downloads);

        final String title = "Paid downloads per month for " + month.toString("MMMM-yy");
        AdminDoc doc = new AdminDoc(context, user, title, Tab.Stats);
        doc.setMenu(new StatsMenu());
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new AdminTable(title));

                sb.append(new FormTag(DownloadsBoughtHandler.class, null, "get"));
                SelectTag monthTag = new SelectTag(context, "month");
                monthTag.setAutoSubmit();

                Date d = new Date().beginWeek();
                for (int n = 0; n < 24; n++) {
                    monthTag.addOption(d, d.toString("MMMM-yy"));
                    d = d.previousMonth();
                }

                sb.append("<tr><th align='right' colspan='3'>Choose week ");
                sb.append(monthTag);
                sb.append("</th></tr>");

                sb.append("</form>");

                sb.append("<tr>");
                sb.append("<td>Date</td>");
                sb.append("<td>File</td>");
                sb.append("<td>Customer</td>");
                sb.append("</tr>");

                for (Download download : downloads) {

                    sb.append("<tr>");
                    sb.append("<td>" + download.getDateCreated().toDateTimeString() + "</td>");
                    sb.append("<td>" + download.getAttachment().getFilename() + "</td>");
                    sb.append("<td>" + new LinkTag(ItemHandler.class, "edit", download.getAccount().getName(), "item", download.getAccount()) +
                            "</td>");
                    sb.append("</tr>");
                }

                sb.append("</table>");

                return sb.toString();
            }

        });
        return doc;
    }

}