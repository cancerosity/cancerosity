package org.sevensoft.ecreator.iface.admin.marketing.affiliates;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 23 Feb 2007 08:08:02
 *
 */
@Path("admin-affiliates-settings.do")
public class AffiliateSettingsHandler extends AdminHandler {

	public AffiliateSettingsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {
		return null;
	}

}
