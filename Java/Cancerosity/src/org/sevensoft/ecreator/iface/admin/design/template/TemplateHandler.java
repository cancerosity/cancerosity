package org.sevensoft.ecreator.iface.admin.design.template;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.template.Template;
import org.sevensoft.ecreator.model.design.template.TemplateSettings;
import org.sevensoft.ecreator.model.design.template.TemplateUtil;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.system.installer.InstallerException;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import javax.servlet.ServletException;
import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * @author sks 23 Aug 2006 12:46:09
 */
@Path("admin-templates.do")
public class TemplateHandler extends AdminHandler {

    private Upload upload;
    private String templateFilename;
    private Template template;
    private boolean dfault;
    private String before;
    private String after;
    private String name;
    private String css;
    private String to;
    private String from;
    private String head;
    private String panels;
    private String headBackend;

    public TemplateHandler(RequestContext context) {
        super(context);
    }

    public Object after() throws ServletException {

        if (template == null) {
            return main();
        }

        AdminDoc doc = new AdminDoc(context, user, "Edit template bottom: " + template.getName(), null);
        doc.setMenu(new TemplateMenu(template));
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update template"));
                sb.append(new ButtonTag(TemplateHandler.class, "delete", "Delete template", "template", template)
                        .setConfirmation("Are you sure want to delete this template?"));
                sb.append(new ButtonTag(TemplateHandler.class, null, "Return to menu"));

                sb.append("</div>");
            }

            private void markup() {

                sb.append(new AdminTable("Bottom markup"));
                sb.append("<tr><td>" + new TextAreaTag(context, "after", template.getAfter(), 160, 30) + "</td></tr>");
                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(TemplateHandler.class, "saveAfter", "POST"));
                sb.append(new HiddenTag("template", template));

                commands();
                markup();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object before() throws ServletException {

        if (template == null) {
            return main();
        }

        AdminDoc doc = new AdminDoc(context, user, "Edit template top: " + template.getName(), null);
        doc.setMenu(new TemplateMenu(template));
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update template"));
                sb.append(new ButtonTag(TemplateHandler.class, "delete", "Delete template", "template", template)
                        .setConfirmation("Are you sure want to delete this template?"));
                sb.append(new ButtonTag(TemplateHandler.class, null, "Return to menu"));

                sb.append("</div>");
            }

            private void markup() {

                sb.append(new AdminTable("Edit markup"));

                sb.append("<tr><td>Name</td><td>" + new TextTag(context, "name", template.getName(), 40) + "</td></tr>");
                sb.append("<tr><td>Default</td><td>" + new BooleanRadioTag(context, "dfault", template.isDefault()) + "</td></tr>");
                sb.append("<tr><td colspan='2'>" + new TextAreaTag(context, "before", template.getBefore(), 160, 30) + "</td></tr>");
                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(TemplateHandler.class, "saveBefore", "POST"));
                sb.append(new HiddenTag("template", template));

                commands();
                markup();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object create() throws ServletException {

        template = new Template(context, "New template", user);
        return main();
    }

    public Object css() throws ServletException {

        if (template == null) {
            return main();
        }

        AdminDoc doc = new AdminDoc(context, user, "Edit template css: " + template.getName(), null);
        doc.setMenu(new TemplateMenu(template));
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update template"));
                sb.append(new ButtonTag(TemplateHandler.class, "delete", "Delete template", "template", template)
                        .setConfirmation("Are you sure want to delete this template?"));
                sb.append(new ButtonTag(TemplateHandler.class, null, "Return to menu"));

                sb.append("</div>");
            }

            private void markup() {

                sb.append(new AdminTable("Css"));
                sb.append("<tr><td>" + new TextAreaTag(context, "css", template.getCss(), 160, 40) + "</td></tr>");
                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(TemplateHandler.class, "saveCss", "POST"));
                sb.append(new HiddenTag("template", template));

                commands();
                markup();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object delete() throws ServletException {

        if (template == null) {
            return main();
        }

        template.delete();
        return main();

    }

    public Object head() throws ServletException {

        if (template == null) {
            return main();
        }

        AdminDoc doc = new AdminDoc(context, user, "Edit template head: " + template.getName(), null);
        doc.setMenu(new TemplateMenu(template));
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update template"));
                sb.append(new ButtonTag(TemplateHandler.class, "delete", "Delete template", "template", template)
                        .setConfirmation("Are you sure want to delete this template?"));
                sb.append(new ButtonTag(TemplateHandler.class, null, "Return to menu"));

                sb.append("</div>");
            }

            private void markup() {

                sb.append(new AdminTable("Template head HTML"));
                sb.append("<tr><td>" + new TextAreaTag(context, "head", template.getHead(), 160, 30) + "</td></tr>");
                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(TemplateHandler.class, "saveHead", "POST"));
                sb.append(new HiddenTag("template", template));

                commands();
                markup();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

        public Object headBackend() throws ServletException {

        if (template == null) {
            return main();
        }

        AdminDoc doc = new AdminDoc(context, user, "Edit backend head: " + template.getName(), null);
        doc.setMenu(new TemplateMenu(template));
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update template"));
                sb.append(new ButtonTag(TemplateHandler.class, "delete", "Delete template", "template", template)
                        .setConfirmation("Are you sure want to delete this template?"));
                sb.append(new ButtonTag(TemplateHandler.class, null, "Return to menu"));

                sb.append("</div>");
            }

            private void markup() {

                sb.append(new AdminTable("Template head HTML"));
                sb.append("<tr><td>" + new TextAreaTag(context, "headBackend", template.getHeadBackend(), 160, 30) + "</td></tr>");
                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(TemplateHandler.class, "saveHeadBackend", "POST"));
                sb.append(new HiddenTag("template", template));

                commands();
                markup();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    @Override
    public Object main() throws ServletException {

        if (!isSuperman() && !Module.UserMarkup.enabled(context)) {
            return index();
        }

        AdminDoc doc = new AdminDoc(context, user, "Template", null);
        doc.addBody(new Body() {

            private void templates() {

                sb.append(new AdminTable("Templates"));

                for (Template template : Template.get(context)) {

                    sb.append("<tr><td>"
                            + new LinkTag(TemplateHandler.class, "before", new SpannerGif(), "template", template)
                            + " "
                            + template.getName()
                            + "<td width='20'>"
                            + new LinkTag(TemplateHandler.class, "delete", new DeleteGif(), "template", template)
                            .setConfirmation("Are you sure you want to delete this template?") + "</td></tr>");

                }

                sb.append("<tr><td colspan='3'>Create template " + new ButtonTag(TemplateHandler.class, "create", "Create") + "</td></tr>");

                // PRESET

                if (isSuperman()) {

                    Map<String, String> presets = TemplateUtil.getPresetOptions(context);
                    if (presets.size() > 0) {

                        sb.append(new FormTag(TemplateHandler.class, "setTemplate", "get"));

                        SelectTag templateTag = new SelectTag(context, "templateFilename");
                        templateTag.setAny("Choose preset template");
                        templateTag.addOptions(presets);

                        sb.append("<tr><td colspan='3' class='superman'>Preset template " + templateTag + " " + new SubmitTag("Install")
                                + "</td></tr>");
                        sb.append("</form>");
                    }
                }

                // UPLOAD
                sb.append(new FormTag(TemplateHandler.class, "upload", "multi"));
                sb.append("<tr><td colspan='4'>Upload template " + new FileTag("upload") + " " + new SubmitTag("Upload") + "</td></tr>");
                sb.append("</form>");

                if (isSuperman()) {

                    sb.append(new FormTag(TemplateHandler.class, "replace", "post"));
                    sb.append("<tr><td colspan='4' class='superman'>Markup replace, Replace " + new TextTag(context, "from") + " with "
                            + new TextTag(context, "to") + " " + new SubmitTag("Replace") + "</td></tr>");
                    sb.append("</form>");

                }

                sb.append("</table>");
            }

            @Override
            public String toString() {

                templates();
                return sb.toString();
            }
        });
        return doc;
    }

    public Object panels() throws ServletException {

        if (template == null) {
            return main();
        }

        AdminDoc doc = new AdminDoc(context, user, "Edit template panels: " + template.getName(), null);
        doc.setMenu(new TemplateMenu(template));
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update panels"));
                sb.append(new ButtonTag(TemplateHandler.class, null, "Return to menu"));

                sb.append("</div>");
            }

            private void markup() {

                TemplateSettings templateSettings = TemplateSettings.getInstance(context);

                sb.append(new AdminTable("Box panels"));
                sb.append("<tr><td>" + new TextAreaTag(context, "panels", StringHelper.implode(templateSettings.getPanels(), "\n", true), 40, 6)
                        + "</td></tr>");
                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(TemplateHandler.class, "savePanels", "POST"));
                sb.append(new HiddenTag("template", template));

                commands();
                markup();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    public Object replace() throws ServletException {

        int done = 0;

        for (Template template : Template.get(context)) {
            template.replace(from, to);
            template.save();
            done++;
        }

        for (Markup markup : Markup.get(context)) {
            markup.replace(from, to);
            markup.save();
            done++;
        }

        for (Box box : Box.get(context)) {
            box.replaceStyle(from, to);
            box.save();
            done++;
        }

        addMessage(done + " searched replaced");
        return main();
    }

    public Object saveAfter() throws ServletException {

        if (template == null) {
            return main();
        }

        template.setAfter(after);
        template.save();

        addMessage("This template has been updated with your changes");
        clearParameters();
        return after();
    }

    public Object saveBefore() throws ServletException {

        if (template == null) {
            return main();
        }

        template.setBefore(before);
        template.setDefault(dfault);
        template.setName(name);
        template.save();

        addMessage("This template has been updated with your changes");
        clearParameters();
        return before();
    }

    public Object saveCss() throws ServletException {

        if (template == null) {
            return main();
        }

        try {

            template.setCss(css);
            template.save();

            clearParameters();

        } catch (IOException e) {
            addError(e);
        }

        return css();
    }

    public Object saveHead() throws ServletException {

        if (template == null) {
            return main();
        }

        template.setHead(head);
        template.save();

        addMessage("This template has been updated with your changes");
        clearParameters();
        return head();
    }

     public Object saveHeadBackend() throws ServletException {

        if (template == null) {
            return main();
        }

        template.setHeadBackend(headBackend);
        template.save();

        addMessage("This template has been updated with your changes");
        clearParameters();
        return headBackend();
    }

    public Object savePanels() throws ServletException {

        if (template == null) {
            return main();
        }

        TemplateSettings templateSettings = TemplateSettings.getInstance(context);

        templateSettings.setPanels(StringHelper.explodeStrings(panels, "\n"));
        templateSettings.save();

        addMessage("Panels have been updated");
        clearParameters();
        return panels();
    }

    public Object setTemplate() throws ServletException {

        if (templateFilename == null) {
            return main();
        }

        File file = ResourcesUtils.getRealTemplateStore(templateFilename);
        if (file.exists()) {

            try {

                Template template = new Template(context, file, user);
                template.setDefault(true);
                template.save();

                addMessage("Template '" + template.getName() + "' has been installed and set as default");

            } catch (IOException e) {
                e.printStackTrace();
                addError(e);

            } catch (InstallerException e) {
                e.printStackTrace();
                addError(e);
            }

        }

        clearParameters();
        return main();
    }

    public Object upload() throws ServletException {

        if (upload == null) {
            return main();
        }

        try {

            File file = upload.getFile();
            String name = upload.getFilename().replace(".zip", "");

            Template template = new Template(context, file, user);
            template.setDefault(true);
            template.setName(name);
            template.save();

            addMessage("Template '" + name + "' has been uploaded and set as default");

        } catch (IOException e) {
            e.printStackTrace();
            addError(e);

        } catch (InstallerException e) {
            e.printStackTrace();
            addError(e);
        }
        return main();
    }

}