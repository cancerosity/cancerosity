package org.sevensoft.ecreator.iface.admin.search;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;

import org.sevensoft.commons.superstrings.comparators.EnumStringComparator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.misc.html.EditMarkupButton;
import org.sevensoft.ecreator.iface.admin.misc.seo.panels.MetaTagsPanel;
import org.sevensoft.ecreator.iface.admin.search.box.ItemSearchBoxHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupTag;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.ecreator.model.search.forms.FieldType;
import org.sevensoft.ecreator.model.search.forms.Filter;
import org.sevensoft.ecreator.model.search.forms.SearchField;
import org.sevensoft.ecreator.model.search.forms.SearchForm;
import org.sevensoft.ecreator.model.search.forms.boxes.ItemSearchBox;
import org.sevensoft.ecreator.model.search.wizards.AutoForward;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.misc.seo.Seo;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 13 Sep 2006 17:51:37
 *
 */
@Path("admin-search-forms.do")
public class SearchFormHandler extends AdminHandler {

	private SearchForm	form;
    private transient Seo seo;
	private FieldType		addInputField;
	private ItemType		itemType;
	private boolean		detailsBox;
	private ItemSearchBox	box;
	private int			resultsPerPage;
	private String		submitSrc;
	private String		submitLabel;
	private Markup		listMarkup;
	private SortType		sortType;
	private Category		searchCategory;
	private String		resultsPageTitle;
	private boolean		hideSubmit;
	private boolean		showResultsBar;
	private boolean		savable;
	private boolean		excludeActiveItemType;
	private FieldType		addHiddenField;
	private boolean		multipleItemTypes;
	private Set<Integer>	itemTypeIds;
	private boolean		excludeActiveItem;
	private FieldType		newFieldType;
	private boolean		resultsTop;
	private boolean		resultsBottom;
	private boolean		sortsTop;
	private boolean		sortsBottom;
	private Attribute		sortAttribute;
	private Markup		markup;
	private boolean		alerts;
	private String		noResultsForwardUrl;
	private String		noResultsText;
	private AutoForward	autoForward;
	private boolean		filter;
	private Attribute		newFilterAttribute;
	private boolean		googleMap;
    private int numberOfHighlightedResults;
    private Markup highlightedResultsMarkup;
    private boolean     includeSubcategories;
    private String descriptionTag, keywords, titleTag;
    private boolean parentTemplate;

    public SearchFormHandler(RequestContext context) {
        super(context);
        this.seo = Seo.getInstance(context);
    }

    public SearchFormHandler(RequestContext context, SearchForm form) {
        this(context);
        this.form = form;
    }

	public Object create() throws ServletException {

		if (box == null)
			return index();

		form = box.addSearchForm();
		return new ItemSearchBoxHandler(context, box).main();
	}

	public Object delete() throws ServletException {

		if (form == null)
			return index();

		box = form.getSearchBox();
		form.delete();

		return new ItemSearchBoxHandler(context, box).main();
	}

	@Override
	public Object main() throws ServletException {

		if (form == null) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit search form", Tab.Extras);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update search form"));

				if (form.hasSearchBox()) {
					sb.append(new ButtonTag(ItemSearchBoxHandler.class, null, "Return to box", "box", form.getSearchBox()));
				} else {
					sb.append(new ButtonTag(SearchBlockHandler.class, null, "Return to block", "block", form.getSearchBlock()));
				}

				sb.append("</div>");
			}

			private void fields() {

				sb.append(new AdminTable("Fields"));

				sb.append("<tr>");
				sb.append("<th width='80'>Position</th>");
				sb.append("<th>Name</th>");
				sb.append("<th>Type</th>");
				sb.append("<th width='10'> </th>");
				sb.append("</tr>");

				PositionRender pr = new PositionRender(SearchFormFieldHandler.class, "field");

				for (SearchField field : form.getSearchFields()) {

					sb.append("<tr>");
					sb.append("<td width='80'>" + pr.render(field) + "</td>");
					sb.append("<td>"
							+ new LinkTag(SearchFormFieldHandler.class, null, new ImageTag("files/graphics/admin/spanner.gif"), "field", field) + " " + field.getName() + "</td>");
					sb.append("<td>" + field.getDescription() + "</td>");
					sb.append("<td width='10'>"
							+ new LinkTag(SearchFormFieldHandler.class, "delete", new DeleteGif(), "field", field)
									.setConfirmation("Are you sure you want to delete this field?") + "</td>");
					sb.append("</tr>");

				}

				TreeSet<FieldType> fields = new TreeSet<FieldType>(new EnumStringComparator());
				fields.addAll(Arrays.asList(FieldType.values()));

				SelectTag newFieldTag = new SelectTag(context, "newFieldType");
				newFieldTag.setAny("-Select field type-");
				newFieldTag.addOptions(fields);

				sb.append("<tr><td colspan='4'>Add new field " + newFieldTag + "</td></tr>");

				sb.append("</table>");
			}

			private void filters() {

				sb.append(new AdminTable("General"));

				for (Filter filter : form.getFilters()) {

					sb.append("<tr>");
					sb.append("<td>" + filter.getAttribute().getName() + "</td>");
					sb.append("<td width='10'>"
							+ new LinkTag(FilterHandler.class, "delete", new DeleteGif(), "filter", filter)
									.setConfirmation("Are you sure you want to delete this filter?") + "</td>");
					sb.append("</tr>");
				}

				sb.append("<tr><td colspan='2'>Add filter: "
						+ new SelectTag(context, "newFilterAttribute", null, form.getAttributes(), "-Select attribute-") + "</td></tr>");

				sb.append("</table>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Multiple item type search",
						"Configure this search for multiple item types (less flexible on attributes but wider results range).",
						new BooleanRadioTag(context, "multipleItemTypes", form.isMultipleItemTypes())));

				if (form.isMultipleItemTypes()) {

					StringBuilder sb2 = new StringBuilder();
					for (ItemType itemType : ItemType.get(context)) {

						boolean checked = form.getItemTypes().contains(itemType.getId());
						sb2.append(new CheckTag(context, "itemTypeIds", itemType.getId(), checked) + " " + itemType.getName() + "<br/>");
					}

					sb.append(new AdminRow("Item types", "Select which item types we should search for", sb2));

				} else {

					sb.append(new AdminRow("Item type", "Set the item type this search will search for", new SelectTag(context, "itemType", form
							.getItemType(), ItemType.getSelectionMap(context), "-Select item type-")));

					if (Module.SearchFilters.enabled(context)) {
						// only let us filter if this form has a single item type
						if (form.hasItemType()) {

							sb.append(new AdminRow("Filter", "Search filters", new BooleanRadioTag(context, "filter", form.isFilter())));
						}
					}
				}

				sb.append(new AdminRow("Search category", "Only show results from this category.", new SelectTag(context, "searchCategory", form
						.getSearchCategory(), Category.getCategoryOptions(context), "-Select category-")));

                sb.append(new AdminRow("Include subcategories", "Search in subcategories of selected category.", new BooleanRadioTag(context, "includeSubcategories",
                        form.isIncludeSubcategories())));

				sb.append(new AdminRow("Exclude account item type", "Exclude the item type that the current account is set to.", new BooleanRadioTag(
						context, "excludeActiveItemType", form.isExcludeAccountItemType())));

				sb.append(new AdminRow("Exclude account item", "Exclude the account that is currently logged in.", new BooleanRadioTag(context,
						"excludeActiveItem", form.isExcludeAccount())));

				sb.append(new AdminRow("Active location", "Take the location from the active location if available.", new BooleanRadioTag(context,
						"activeLocation", form.isActiveLocation())));

				if (Module.SavedSearches.enabled(context)) {
					sb.append(new AdminRow("Savable", "Allow users to put searches from this form into their saved searches page.",
							new BooleanRadioTag(context, "savable", form.isSavable())));
				}

				if (Module.SearchAlerts.enabled(context)) {
					sb.append(new AdminRow("Alerts", "Allow users to request alerts when new items are added that match this search.",
							new BooleanRadioTag(context, "alerts", form.isAlerts())));
				}

				sb.append(new AdminRow(true, "Hide submit", "Hide the submit button or graphic. Useful if all your fields are set to auto submit.",
						new BooleanRadioTag(context, "hideSubmit", form.isHideSubmit())));

				sb.append(new AdminRow(true, "Submit button text", "The text that appears in the submit button of the search form", new TextTag(
						context, "submitLabel", form.getSubmitLabel())));

				sb.append(new AdminRow(true, "Submit image source", "Show an image instead of a button. Relative to template-data", new TextTag(
						context, "submitSrc", form.getSubmitSrc())));

				sb.append("</table>");
			}

			private void markup() {

				sb.append(new AdminTable("Markup"));

                sb.append(new AdminRow(true, "Template", "Set Yes if you would like to use SearchForm Owner Template instead of default site template",
                        new BooleanRadioTag(context, "parentTemplate", form.isParentTemplate())));
				sb.append(new AdminRow(true, "Markup", "Use custom markup instead of standard renderer for the layout of this search form.",
						new MarkupTag(context, "markup", form.getMarkup(), "-Standard renderer-", "markup")));

				sb.append(new AdminRow(!Module.UserMarkup.enabled(context), "List markup", "Markup for search results.", new SelectTag(context,
						"listMarkup", form.getListMarkup(), Markup.get(context), "-Use default for item type-").setId("listMarkup")
						+ " " + new EditMarkupButton("listMarkup")));

				sb.append("</table>");

			}

			private void results() {

				sb.append(new AdminTable("Results"));

				sb.append(new AdminRow("Page title", "Override the search results page title which is normally 'Search results'.", new TextTag(context,
						"resultsPageTitle", form.getResultsPageTitle(), 40)));

				sb.append(new AdminRow("Results per page", "Override how many results are shown per page when using this search form", new TextTag(
						context, "resultsPerPage", form.getResultsPerPage(), 5)));

				sb.append(new AdminRow("Results info", "Show the number of results at top and/or bottom.", "Top "
						+ new CheckTag(context, "resultsTop", true, form.isResultsTop()) + "Bottom "
						+ new CheckTag(context, "resultsBottom", true, form.isResultsBottom())));

				sb.append(new AdminRow("Auto forward", "Forward directly to an item.", new SelectTag(context, "autoForward", form.getAutoForward(),
						AutoForward.values())));

				sb.append(new AdminRow("No results forward url",
						"Enter a URL here and if the search has no results, the user will be forwarded to this url.", new TextTag(context,
								"noResultsForwardUrl", form.getNoResultsForwardUrl(), 40)));

				sb.append(new AdminRow("No results text",
						"This text will appear if there are no results, unless a forward url is set which will be used instead.", new TextAreaTag(
								context, "noResultsText", form.getNoResultsText(), 50, 3)));

				sb.append(new AdminRow("Google map", "Include google map in results.", new BooleanRadioTag(context, "googleMap", form.isGoogleMap())));

				sb.append("</table>");
			}

			private void sorts() {

				sb.append(new AdminTable("Sorts"));

				sb.append(new AdminRow("Sort", "Apply this item sort automatically.", new SelectTag(context, "sortType", form.getSortType(), SortType
						.values(), "-Default-")));

				if (form.getSortType() == SortType.Attribute) {

					if (form.hasItemType()) {

						sb.append(new AdminRow("Sort attribute", "Set the attribute for use by the search.", new SelectTag(context,
								"sortAttribute", form.getSortAttribute(), form.getItemType().getAttributes(), "-None set-")));

					}
				}

				sb.append(new AdminRow("Sorts top", "Show the sorts options at the top of the page.", new BooleanRadioTag(context, "sortsTop", form
						.isSortsTop())));

				sb.append(new AdminRow("Sorts bottom", "Show the sorts options at the top of the page.", new BooleanRadioTag(context, "sortsBottom",
						form.isSortsBottom())));

				sb.append("</table>");
			}

            private void highlighted() {

                sb.append(new AdminTable("Highlighted results"));

                sb.append(new AdminRow("Highlighted results", "How many highlighted results to show. Zero will disable.", new TextTag(context,
                        "numberOfHighlightedResults", form.getNumberOfHighlightedResults(), 6)));

                sb.append(new AdminRow("Markup", "Use this markup for highlighted results.", new MarkupTag(context, "highlightedResultsMarkup", form
                        .getHighlightedResultsMarkup(), "-Standard renderer-", "highlightedResultsMarkup")));

                sb.append("</table>");

            }


            @Override
			public String toString() {

				sb.append(new FormTag(SearchFormHandler.class, "save", "post"));
				sb.append(new HiddenTag("form", form));

                commands();
				general();

                if (seo.isOverrideTags()) {
                    logger.fine("[CategoryHandler] seo tags overriden");
                    sb.append(new MetaTagsPanel(context, form));
                }

				sorts();
				results();
                highlighted();

                fields();

				if (form.isFilter()) {
					filters();
				}

				if (isSuperman() || Module.UserMarkup.enabled(context)) {
					markup();
				}

				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;

	}

	public Object save() throws ServletException {

		if (form == null) {
			return main();
		}

		// account options
		form.setExcludeAccountItemType(excludeActiveItemType);
		form.setExcludeAccount(excludeActiveItem);

		// item type control
		form.setItemType(itemType);
		form.setMultipleItemTypes(multipleItemTypes);
		form.setItemTypes(itemTypeIds);

		// submit labels
		form.setSubmitLabel(submitLabel);
		form.setSubmitSrc(submitSrc);
		form.setHideSubmit(hideSubmit);

		// sorts
		form.setSortType(sortType);
		form.setSortsBottom(sortsBottom);
		form.setSortsTop(sortsTop);
		form.setSortAttribute(sortAttribute);

		// categories
		form.setSearchCategory(searchCategory);
        form.setIncludeSubcategories(includeSubcategories);

		// filter
		if (Module.SearchFilters.enabled(context)) {
			form.setFilter(filter);

			if (newFilterAttribute != null) {
				form.addFilter(newFilterAttribute);
			}
		}

		// results
		form.setResultsBottom(resultsBottom);
		form.setResultsTop(resultsTop);
		form.setResultsPageTitle(resultsPageTitle);
		form.setNoResultsForwardUrl(noResultsForwardUrl);
		form.setNoResultsText(noResultsText);
		form.setResultsPerPage(resultsPerPage);
		form.setAutoForward(autoForward);

		// extras
		form.setGoogleMap(googleMap);

		form.setSavable(savable);
		form.setAlerts(alerts);

		if (newFieldType != null) {
			form.addInputField(newFieldType);
		}

		if (isSuperman() || Module.UserMarkup.enabled(context)) {
            form.setParentTemplate(parentTemplate);
			form.setListMarkup(listMarkup);
			form.setMarkup(markup);
		}

        form.setHighlightedResultsMarkup(highlightedResultsMarkup);
        form.setNumberOfHighlightedResults(numberOfHighlightedResults);

        if (seo.isOverrideTags()) {

            if (!ObjectUtil.equal(form.getTitleTag(), titleTag)) {
                form.setTitleTag(titleTag);
            }

            if (!ObjectUtil.equal(form.getKeywords(), keywords)) {
                form.setKeywords(keywords);
            }

            if (!ObjectUtil.equal(form.getDescriptionTag(), descriptionTag)) {
                form.setDescriptionTag(descriptionTag);
            }

        }

        form.save();

		clearParameters();
		return main();
	}

}
