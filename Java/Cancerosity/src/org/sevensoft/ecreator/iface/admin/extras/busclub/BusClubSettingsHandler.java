package org.sevensoft.ecreator.iface.admin.extras.busclub;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.extras.busclub.BusClubSettings;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 17-Jun-2005 17:37:55
 * 
 */
@Path("admin-settings-busclub.do")
public class BusClubSettingsHandler extends AdminHandler {

	private Money				affiliateDiscount;
	private transient BusClubSettings	settings;

	public BusClubSettingsHandler(RequestContext context) {
		super(context);
		this.settings = BusClubSettings.getInstance(context);
	}

	@Override
	public Object main() {

		AdminDoc doc = new AdminDoc(context, user, "Business Club Settings", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("Business club settings"));

				sb.append(new AdminRow("Affiliate discount", null, new TextTag(context, "affiliateDiscount", settings.getAffiliationDiscount(), 8)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(BusClubSettingsHandler.class, "save", "POST"));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() {

		settings.setAffiliationDiscount(affiliateDiscount);
		settings.save();

		return new ActionDoc(context, "Bus club settings updated", new Link(BusClubSettingsHandler.class));
	}

}
