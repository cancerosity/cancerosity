package org.sevensoft.ecreator.iface.admin.marketing.newsletter;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.marketing.MarketingMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.marketing.newsletter.example.NewsletterExample;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

import javax.servlet.ServletException;

/**
 * Date: 14.06.2010
 */
@Path("admin-newsletters-examples.do")
public class NewsletterExampleHandler extends AdminHandler {

    private NewsletterExample example;
    private String name;
    private String content;

    public NewsletterExampleHandler(RequestContext context) {
        super(context);
    }

    public Object create() throws ServletException {

        new NewsletterExample(context, "-New newsletter example-");
        return main();
    }

    public Object edit() throws ServletException {

        if (example == null)
            return main();

        AdminDoc page = new AdminDoc(context, user, "Edit newsletter example: " + example.getName(), null);
        page.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update example"));
                sb.append(new ButtonTag(NewsletterExampleHandler.class, "delete", "Delete example", "example", example)
                        .setConfirmation("Are you sure want to delete this example?"));
                sb.append(new ButtonTag(NewsletterExampleHandler.class, null, "Return to menu"));

                sb.append("</div>");
            }

            private void markup() {

                sb.append(new TableTag("form", 1, 0).setCaption("Edit example"));

                sb.append("<tr><td>Name</td><td>" + new TextTag(context, "name", example.getName(), 40) + "</td></tr>");
//                    sb.append("<tr><td>Content</td><td>" + new TextAreaTag(context, "content", example.getContent(), 80, 10) + "</td></tr>");

                sb.append("</table>");
            }

            private void content() {

                sb.append(new AdminTable("Content"));

                sb.append("<tr><td>" + new TextAreaTag(context, "content", example.getContent()).setId("body").setStyle("width: 100%; height: 320px;") + "</td></tr>");
                sb.append("</table>");
            }

            @Override
            public String toString() {
                RendererUtil.tinyMce(context, sb, "body");

                sb.append(new FormTag(NewsletterExampleHandler.class, "save", "POST"));
                sb.append(new HiddenTag("example", example));

                commands();
                markup();
                content();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return page;
    }

    @Override
    public Object main() throws ServletException {

        if (!isSuperman())
            return new DashboardHandler(context).main();

        AdminDoc page = new AdminDoc(context, user, "Newsletter example", Tab.Marketing);
        page.setMenu(new MarketingMenu());
        page.addBody(new Body() {

            private void templates() {

                sb.append(new TableTag("form", 1, 0).setCaption("Examples"));

                for (NewsletterExample example : NewsletterExample.get(context)) {

                    sb.append("<tr><td>"
                            + example.getName()
                            + "</td><td width='120'>"
                            + new ButtonTag(NewsletterExampleHandler.class, "edit", "Edit", "example", example)
                            + "</td><td width='120'>"
                            + new ButtonTag(NewsletterExampleHandler.class, "delete", "Delete", "example", example)
                            .setConfirmation("Are you sure you want to delete this example?") + "</td></tr>");

                }

                sb.append("<tr><td colspan='3'>Create example " + new ButtonTag(NewsletterExampleHandler.class, "create", "Create") + "</td></tr>");

                sb.append("</form>");
                sb.append("</table>");
            }

            @Override
            public String toString() {

                templates();

                return sb.toString();
            }
        });
        return page;
    }

    public Object save() throws ServletException {

        if (example == null)
            return main();

        example.setContent(content);
        example.setName(name);
        example.save();

        addMessage("This newsletter example has been updated with your changes.");
        clearParameters();
        return edit();
    }
}

