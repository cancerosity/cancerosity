package org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics;

import org.sevensoft.jeezy.http.html.ImageTag;

/**
 * @author sks 28 Dec 2006 17:48:47
 *
 */
public class SpannerGif {

	@Override
	public String toString() {
		return new ImageTag("files/graphics/admin/spanner.gif").toString();
	}

}
