package org.sevensoft.ecreator.iface.admin.items.modules.prices;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.items.pricing.PriceBand;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 17 Jan 2007 16:38:58
 *
 */
@Path("admin-pricing-bands.do")
public class PriceBandHandler extends AdminHandler {

	private PriceBand	priceBand;
	private String	name;

	public PriceBandHandler(RequestContext context) {
		super(context);
	}

	public Object delete() throws ServletException {

		if (priceBand == null) {
			return main();
		}

		priceBand.delete();
		return new PriceSettingsHandler(context).main();
	}

	@Override
	public Object main() throws ServletException {

		if (priceBand == null) {
			return new PriceSettingsHandler(context).main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit price band", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Save changes"));
				sb.append(new ButtonTag(PriceSettingsHandler.class, null, "Return to price settings"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));
				sb.append(new AdminRow("Name", "Enter the name of this price band", new TextTag(context, "name", priceBand.getName())));
				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(PriceBandHandler.class, "save", "post"));
				sb.append(new HiddenTag("priceBand", priceBand));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}
		});
		return doc;
	}

	public Object save() throws ServletException {

		if (priceBand == null) {
			return main();
		}

		priceBand.setName(name);
		priceBand.save();

		return main();
	}

}
