package org.sevensoft.ecreator.iface.admin.items.modules.prices;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.items.pricing.PriceBand;
import org.sevensoft.ecreator.model.items.pricing.PriceSettings;
import org.sevensoft.ecreator.model.items.pricing.PriceUtil;
import org.sevensoft.ecreator.model.items.pricing.PriceSettings.PricingType;
import org.sevensoft.ecreator.model.items.pricing.panels.EditPricesRenderer;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 21 Jul 2006 17:21:58
 *
 */
@Path("admin-prices-settings.do")
public class PriceSettingsHandler extends AdminHandler {

	private String					priceBreaksString;
	private MultiValueMap<Integer, String>	priceMap;
	private PriceSettings				priceSettings;
	private Map<Integer, Money>			costPriceToMap, costPriceFromMap;
	private boolean					costPriceConstraints;
	private String					costConstraintsString;
	private PricingType				pricingType;
	private boolean					usePriceBands;
	private boolean					usePriceBreaks;
	private Amount					globalDiscount;
	private Map<Integer, String>			prices;
	private String					priceBreaks;
	private boolean					accountPricing;
	private String					newPriceBand;
	/**
		 * Enable charges for collections on items of this type
		 */
	private boolean					collectionCharges;

	public PriceSettingsHandler(RequestContext context) {
		super(context);
		this.priceSettings = PriceSettings.getInstance(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Edit price settings", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Save changes"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Price breaks", "Enable price breaks for pricing based on quantities bought.", new BooleanRadioTag(context,
						"usePriceBreaks", priceSettings.isUsePriceBreaks())));

				sb.append(new AdminRow("Price bands",
						"Enable price bands for different levels of pricing (ie, different groups of customers can have different prices).",
						new BooleanRadioTag(context, "usePriceBands", priceSettings.isUsePriceBands())));

				sb.append(new AdminRow("Global discount", "Enter a discount here and it will be applied to all your products across the entire site.",
						new TextTag(context, "globalDiscount", priceSettings.getGlobalDiscount(), 8)));

				sb.append(new AdminRow("Collection charges", "Enable collection charges on items.", new BooleanRadioTag(context, "collectionCharges",
						priceSettings.isCollectionCharges())));

				sb.append(new AdminRow("Percentage pricing type", "Select markup or margin. Markup adds the percentage onto the cost. "
						+ "Margin sets the sale price to ensure you make the percentage profit.", new SelectTag(context, "pricingType",
						priceSettings.getPricingType(), PricingType.values())));

				sb.append("</table>");
			}

			private void pricebands() {

				List<PriceBand> priceBands = PriceBand.get(context);

				sb.append(new AdminTable("Price bands"));

				if (priceBands.isEmpty()) {

					sb.append("<tr><td colspan='2'>You have no prices yet</td></tr>");

				} else {

					sb.append("<tr>");
					sb.append("<td>Name</td>");
					sb.append("<td>Delete</td>");
					sb.append("</tr>");

					for (PriceBand priceBand : priceBands) {

						sb.append("<tr>");
						sb.append("<td>" + new LinkTag(PriceBandHandler.class, null, new SpannerGif(), "priceBand", priceBand) + " " +
								priceBand.getName() + "</td>");
						sb.append("<td>" +
								new ButtonTag(PriceBandHandler.class, "delete", "Delete", "priceBand", priceBand)
										.setConfirmation("Are you sure you want to delete this price band") + "</td>");
						sb.append("</tr>");
					}
				}

				sb.append("<tr><td colspan='2'>Add new price band: " + new TextTag(context, "newPriceBand", 20) + " " + new SubmitTag("Add") +
						"</td></tr>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(PriceSettingsHandler.class, "save", "POST"));

                commands();
				general();

				if (priceSettings.isUsePriceBands()) {
					pricebands();
				}

				sb.append(new EditPricesRenderer(context, priceSettings));

				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		priceSettings.setUsePriceBands(usePriceBands);
		priceSettings.setUsePriceBreaks(usePriceBreaks);
		priceSettings.setPricingType(pricingType);
		priceSettings.setGlobalDiscount(globalDiscount);
		priceSettings.setCollectionCharges(collectionCharges);
		priceSettings.save();

		priceSettings.setPriceBreaks(StringHelper.explodeIntegers(priceBreaks, "\\D"));
		PriceUtil.savePrices(context, priceSettings, prices);

		if (newPriceBand != null) {
			new PriceBand(context, newPriceBand);
		}

		clearParameters();
		return main();
	}

}
