package org.sevensoft.ecreator.iface.admin.crm.jobsheets.panels;

import java.util.List;

import org.sevensoft.ecreator.iface.admin.crm.jobsheets.JobHandler;
import org.sevensoft.ecreator.iface.admin.crm.jobsheets.JobsHandler;
import org.sevensoft.ecreator.iface.admin.crm.jobsheets.JobsheetHandler;
import org.sevensoft.ecreator.iface.admin.misc.Panel;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.model.crm.jobsheets.Job;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 20 Apr 2007 08:13:46
 *
 */
public class JobsPanel extends Panel {

	private List<Job>	jobs;

	public JobsPanel(RequestContext context, List<Job> jobs) {
		super(context);
		this.jobs = jobs;
	}

	@Override
	public void makeString() {

		sb.append(new AdminTable("Jobs"));

		sb.append("<tr>");
		sb.append("<th width='10'> </th>");
		sb.append("<th width='10'> </th>");
		sb.append("<th>Created</th>");
		sb.append("<th>Job</th>");
		sb.append("<th>Jobsheet</th>");
		sb.append("<th>Owner</th>");
		sb.append("<th>Last message</th>");
		sb.append("<th width='10'> </th>");
		sb.append("</tr>");

		sb.append(new FormTag(JobsHandler.class, "deleteJobs", "post"));

		int n = 1;
		for (Job job : jobs) {

			sb.append("<tr>");
			sb.append("<th width='10'>" + new CheckTag(context, "jobs", job, false) + "</th>");
			sb.append("<td width='10'>" + n + "</td>");
			sb.append("<td>" + job.getCreatedBy() + "<br/>" + job.getDateCreated().toString("dd MMM yyyy HH:mm") + "</td>");

			ImageTag imageTag;
			switch (job.getPriority()) {
			default:
			case Green:
				imageTag = new ImageTag("files/graphics/admin/traffic_green.gif");
				break;

			case Amber:
				imageTag = new ImageTag("files/graphics/admin/traffic_amber.gif");
				break;

			case Red:
				imageTag = new ImageTag("files/graphics/admin/traffic_red.gif");
				break;
			}

			imageTag.setAlign("absmiddle");

			sb.append("<td>" + imageTag + " " + new LinkTag(JobHandler.class, null, job.getName(), "job", job) + "</td>");
			sb.append("<td>");
			if (job.hasJobsheet()) {
				sb.append(new LinkTag(JobsheetHandler.class, null, job.getJobsheet().getName(), "jobsheet", job.getJobsheet()));
			}
			sb.append("</td>");

			sb.append("<td>");
			if (job.hasOwner()) {
				sb.append(job.getOwner().getName());
			} else {
				sb.append("--");
			}
			sb.append("</td>");

			sb.append("<td>");
			if (job.hasMessages()) {
				sb.append(job.getLastMessageAuthor());
				sb.append("<br/>");
				sb.append(job.getLastMessageDate().toString("dd-MMM-yyyy HH:mm"));
			}
			sb.append("</td>");
			sb.append("<td width='10'>"
					+ new LinkTag(JobHandler.class, "delete", new DeleteGif(), "job", job)
							.setConfirmation("Are you sure you want to delete this job?") + " </td>");
			sb.append("</tr>");

			n++;
		}

		sb.append("<tr><td colspan='9'>" + new SubmitTag("Delete selected jobs") + "</td></tr>");
		sb.append("</form>");

		sb.append(new FormTag(JobsHandler.class, "add", "get"));

		sb.append("<tr><td style='padding: 10px 2px;' colspan='9'>Add job: "
				+ new TextTag(context, "name", 40).setStyle("font-size: 13px; font-weight: bold;") + " " + new SubmitTag("Add") + "</td></tr>");

		sb.append("</form>");

		sb.append("</table>");

	}

}
