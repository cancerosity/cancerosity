package org.sevensoft.ecreator.iface.admin.feeds.bespoke.property;

import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.model.feeds.Feed;
import org.sevensoft.ecreator.model.feeds.bespoke.property.propertyfinder.PropertyFinderFeed;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 3 Aug 2006 11:05:01
 *
 */
@Path("admin-feeds-propfinder.do")
public class PropertyFinderFeedHandler extends FeedHandler {

	private PropertyFinderFeed	feed;

	public PropertyFinderFeedHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Feed getFeed() {
		return feed;
	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

	@Override
	protected void saveSpecific() {
	}

}
