package org.sevensoft.ecreator.iface.admin.misc.seo;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import org.jdom.JDOMException;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.misc.seo.keywords.KeywordSuggester;
import org.sevensoft.ecreator.model.misc.seo.searchengines.SearchException;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.SimpleDoc;

/**
 * @author sam
 */
@Path("admin-seo-keywords-suggestions.do")
public class KeywordSuggestionsHandler extends AdminHandler {

	private String				phrase;
	private transient List<String>	keywords;
	private boolean				whitelabel;

	public KeywordSuggestionsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc page = new AdminDoc(context, user, "Keyword suggestions", null);
		page.addBody(new Body() {

			private void phrase() {

				sb.append(new AdminTable("Keyword phrase"));
				sb.append("<tr><td>Enter the phrase you want to get suggestions for and click submit. "
						+ "Please allow a few minutes while our program searches the internet for the most appropriate keywords.");
				sb.append("<tr><td>" + new TextTag(context, "phrase", 50) + " " + new SubmitTag("Submit") + "</td></tr>");

				if (keywords != null) {
					sb.append("<tr><td>Suggested keywords: " + StringHelper.implode(keywords, ", ") + "</td></tr>");
				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(KeywordSuggestionsHandler.class, "search", "get"));
				phrase();
				sb.append("</form>");

				return sb.toString();
			}

		});
		return page;
	}

	public Object search() throws ServletException {

		try {

			KeywordSuggester gen = new KeywordSuggester(context);
			keywords = gen.getKeywords(phrase);

		} catch (IOException e) {
			e.printStackTrace();
			addError(e);

		} catch (JDOMException e) {
			e.printStackTrace();
			addError(e);

		} catch (SearchException e) {
			e.printStackTrace();
			addError(e);
		}

		if (whitelabel)
			return whitelabel();
		else
			return main();
	}

	public Object whitelabel() throws ServletException {

		SimpleDoc doc = new SimpleDoc(context);

		doc.append(new FormTag(KeywordSuggestionsHandler.class, "search", "get"));
		doc.append(new HiddenTag("whitelabel", true));

		doc.append("<style>table { font-family: Arial; font-size: 12px; } ");
		doc.append("table caption { font-weight: bold; font-size: 18px; padding: 5px; } ");
		doc.append("</style>");
		doc.append(new AdminTable("Keyword phrase"));
		doc.append("<tr><td>Enter the phrase you want to get suggestions for and click submit. "
				+ "Please allow a few minutes while our program searches the internet for the most appropriate keywords.");
		doc.append("<tr><td>" + new TextTag(context, "phrase", 50) + " " + new SubmitTag("Submit") + "</td></tr>");

		if (keywords != null) {
			doc.append("<tr><td>Suggested keywords: " + StringHelper.implode(keywords, ", ") + "</td></tr>");
		}

		doc.append("</table>");
		doc.append("</form>");

		return doc;
	}

}