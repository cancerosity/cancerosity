package org.sevensoft.ecreator.iface.admin.system.lookandfeel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 15 Apr 2007 18:01:26
 *
 */
public class EasyDoc extends AdminDoc {

	private String		intro;
	private List<Object>	rows;
	private FormTag		formTag;

	public EasyDoc(RequestContext context, User user, String title, Tab tab) {
		super(context, user, title, tab);
		this.rows = new ArrayList();
	}

	public void addControls(Object... controls) {
		this.rows.add(StringHelper.concat(controls));
	}

	public void addLink(String text, Link link) {
		this.rows.add(new LinkTag(link, text));
	}

	public void addSeparator() {
		this.rows.add("<hr/>");
	}

	public void addSpacing() {
		this.rows.add("<div> </div>");
	}

	public void addText(String text) {
		this.rows.add(text);
	}

	@Override
	public StringBuilder output() throws IOException {

		StringBuilder sb = new StringBuilder();

		if (formTag != null) {
			sb.append(formTag);
		}

		sb.append(new TableTag("simplelinks", "center"));

		if (intro != null) {
			sb.append("<tr><th align='center'>" + intro + "</th></tr>");
		}

		for (Object row : rows) {
			sb.append("<tr><td align='center'>" + row + "</td></tr>");
		}

		sb.append("</table>");

		if (formTag != null) {
			sb.append("</form>");
		}

		addBody(sb);

		return super.output();
	}

	/**
	 * 
	 */
	public void setForm(FormTag formTag) {
		this.formTag = formTag;
	}

	public final void setIntro(String intro) {
		this.intro = intro;
	}

}
