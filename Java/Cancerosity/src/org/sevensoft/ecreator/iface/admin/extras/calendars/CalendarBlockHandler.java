package org.sevensoft.ecreator.iface.admin.extras.calendars;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeType;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.extras.calendars.blocks.CalendarBlock;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

import java.util.List;

/**
 * @author sks 26 Jun 2006 17:32:16
 */
@Path("admin-blocks-calendars.do")
public class CalendarBlockHandler extends BlockEditHandler {

    private CalendarBlock block;
    private ItemType itemType;
    private Attribute endAttribute;
    private Attribute startAttribute;

    public CalendarBlockHandler(RequestContext context) {
        super(context);
    }

    @Override
    protected Block getBlock() {
        return block;
    }

    @Override
    protected void saveSpecific() {

        block.setStartAttribute(startAttribute);
        block.setEndAttribute(endAttribute);
        block.setItemType(itemType);
    }

    @Override
    public void specifics(StringBuilder sb) {

        sb.append(new AdminTable("Calendar settings"));

        sb.append(new AdminRow("Item type", "Select the item type to show on the calendar.", new SelectTag(context, "itemType", block.getItemType(),
                ItemType.getSelectionMap(context), "-Select item type-")));

        if (block.hasItemType()) {

            final List<Attribute> dateAttributes = block.getItemType().getAttributes(AttributeType.Date, AttributeType.DateTime);

            SelectTag startAttributeTag = new SelectTag(context, "startAttribute", block.getStartAttribute());
            SelectTag endAttributeTag = new SelectTag(context, "endAttribute", block.getEndAttribute());
            startAttributeTag.setAny("-Select attribute-");
            startAttributeTag.addOptions(dateAttributes);

            sb.append("<tr>");
            sb.append("<td colspan='2'>Take start date from " + startAttributeTag + "</td>");
            sb.append("</tr>");

            sb.append("<tr>");
            sb.append("<td colspan='2'>Take end date to " + endAttributeTag + "</td>");
            sb.append("</tr>");

        }

        sb.append("</table>");
    }

}
