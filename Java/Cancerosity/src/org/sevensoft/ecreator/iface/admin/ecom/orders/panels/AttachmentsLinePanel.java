package org.sevensoft.ecreator.iface.admin.ecom.orders.panels;

import org.sevensoft.ecreator.iface.admin.attachments.AttachmentHandler;
import org.sevensoft.ecreator.model.attachments.Attachment;
import org.sevensoft.ecreator.model.attachments.AttachmentOwner;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.ImageTag;

/**
 * @author sks 15 Jan 2007 14:58:04
 *
 */
public class AttachmentsLinePanel {

	private String			prefix;
	private final AttachmentOwner	owner;
	private final RequestContext	context;

	public final void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public AttachmentsLinePanel(RequestContext context, AttachmentOwner owner) {
		this.context = context;
		this.owner = owner;
	}

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        if (prefix != null) {
            sb.append(prefix);
        }

        for (Attachment a : owner.getAttachments()) {
            sb.append("<br>");
            sb.append(new ImageTag("files/graphics/admin/attachment.png"));

            sb.append(a.getName());
            sb.append(" (");
            sb.append(a.getSizeKb());
            sb.append(") - ");

            sb.append(new LinkTag(AttachmentHandler.class, "download", "download", "attachment", a));

        }

        return sb.toString();
    }

}
