package org.sevensoft.ecreator.iface.admin.extras.wizards;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.search.wizards.AutoForward;
import org.sevensoft.ecreator.model.search.wizards.Wizard;
import org.sevensoft.ecreator.model.search.wizards.WizardStep;
import org.sevensoft.ecreator.model.search.wizards.Wizard.Results;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 13 Aug 2006 16:44:49
 *
 */
@Path("admin-wizard.do")
public class WizardHandler extends AdminHandler {

	private String		newWizardName;
	private Wizard		wizard;
	private String		name;
	private ItemType		itemType;
	private Results		results;
	private AutoForward	autoForward;
	private Category	category;

	public WizardHandler(RequestContext context) {
		super(context);
	}

	public WizardHandler(RequestContext context, Wizard wizard) {
		super(context);
		this.wizard = wizard;
	}

	public Object add() throws ServletException {

		if (newWizardName != null) {
			new Wizard(context, newWizardName);
		}

		return new ActionDoc(context, "A new wizard has been created", new Link(WizardHandler.class));
	}

	public Object addStep() throws ServletException {

		if (wizard == null) {
			return main();
		}

		wizard.addStep();

		clearParameters();
		return edit();
	}

	public Object delete() throws ServletException {

		if (wizard == null) {
			return main();
		}

		wizard.delete();

		return new ActionDoc(context, "This wizard has been deleted", new Link(WizardHandler.class));
	}

	public Object edit() throws ServletException {

		if (wizard == null) {
			return main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit wizard: " + wizard.getName(), Tab.Extras);
		doc.setMenu(new ExtrasMenu());
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update wizard"));
				sb.append(new ButtonTag(WizardHandler.class, null, "Return to wizards menu"));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Name", "Give this wizard a name", new TextTag(context, "name", wizard.getName(), 40)));

				SelectTag itemTypeTag = new SelectTag(context, "itemType", wizard.getItemType(), ItemType.getSelectionMap(context), "-Select item type-");
				sb.append(new AdminRow("Item type", "Select the item type this wizard will operate on", itemTypeTag));

				SelectTag autoForwardTag = new SelectTag(context, "autoForward", wizard.getAutoForward(), AutoForward.values());
				sb.append(new AdminRow("Auto forward",
						"Determines if the search page should be bypassed and the wizard instead forward directly to the item page.",
						autoForwardTag));

				SelectTag categoryTag = new SelectTag(context, "category", wizard.getCategory(), Category.getCategoryOptions(context, null, 0, 500),
						"-All categories-");
				sb.append(new AdminRow("Category", "Limit items to ones in this category only.", categoryTag));

				sb.append("</table>");
			}

			private void steps() {

				sb.append(new AdminTable("Steps"));

				sb.append("<tr>");
				sb.append("<th>Position</th>");
				sb.append("<th>Description</th>");
				sb.append("<th width='10'> </th>");
				sb.append("</tr>");

				int n = 1;
				for (WizardStep step : wizard.getSteps()) {

					sb.append("<tr>");
					sb.append("<td width='60'>Step " + n + "</td>");

					// description
					sb.append("<td>" + new LinkTag(WizardStepHandler.class, null, new SpannerGif(), "step", step) + " ");

					if (step.hasDescription()) {
						sb.append(step.getDescription());
					} else {
						sb.append("No description");
					}

					sb.append("</td>");

					sb.append("<td width='10'>"
							+ new LinkTag(WizardStepHandler.class, "delete", new DeleteGif(), "step", step)
									.setConfirmation("Are you sure you want to delete this step?") + "</td>");
					sb.append("</tr>");

					n++;
				}

				sb.append("<tr><th colspan='3'>Add new step " + new ButtonTag(WizardHandler.class, "addStep", "Go", "wizard", wizard) + "</th></tr>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(WizardHandler.class, "save", "post"));
				sb.append(new HiddenTag("wizard", wizard));

                commands();
				general();

				// steps are dependant on item type
				if (wizard.hasItemType()) {
					steps();
				}

				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Wizards", Tab.Extras);
		doc.setMenu(new ExtrasMenu());
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Wizards"));

				sb.append("<tr>");
				sb.append("<th>Name</th>");
				sb.append("<th width='10'> </th>");
				sb.append("</tr>");

				for (Wizard wizard : Wizard.get(context)) {

					sb.append("<tr>");
					sb.append("<td>" + new LinkTag(WizardHandler.class, "edit", new ImageTag("files/graphics/admin/spanner.gif"), "wizard", wizard)
							+ " " + wizard.getName() + "</td>");
					sb.append("<td width='10'>"
							+ new LinkTag(WizardHandler.class, "delete", new DeleteGif(), "wizard", wizard)
									.setConfirmation("Are you sure you want to delete this wizard?") + "</td>");
					sb.append("</tr>");

				}

				sb.append(new FormTag(WizardHandler.class, "add", "post"));
				sb.append("<tr><td colspan='2'>Add a new wizard: " + new TextTag(context, "newWizardName", 20) + " " + new SubmitTag("Add")
						+ "</td></tr>");
				sb.append("</form>");

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (wizard == null) {
			return main();
		}

		wizard.setName(name);
		wizard.setItemType(itemType);
		wizard.setResults(results);
		wizard.setAutoForward(autoForward);
		wizard.setCategory(category);
		wizard.save();

		return new ActionDoc(context, "This wizard has been updated", new Link(WizardHandler.class, "edit", "wizard", wizard));
	}

}
