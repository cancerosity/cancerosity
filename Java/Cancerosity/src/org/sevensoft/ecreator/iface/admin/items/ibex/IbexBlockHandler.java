package org.sevensoft.ecreator.iface.admin.items.ibex;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;

/**
 * User: Tanya
 * Date: 06.01.2011
 */
@Path("admin-ibex-block.do")
public class IbexBlockHandler extends BlockEditHandler {

    public IbexBlockHandler(RequestContext context) {
        super(context);
    }

    protected Block getBlock() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    protected void saveSpecific() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    protected void specifics(StringBuilder sb) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
