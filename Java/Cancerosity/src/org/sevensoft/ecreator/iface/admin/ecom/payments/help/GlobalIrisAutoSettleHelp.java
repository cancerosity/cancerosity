package org.sevensoft.ecreator.iface.admin.ecom.payments.help;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.ToolTip;

/**
 * User: Tanya
 * Date: 08.01.2013
 */
public class GlobalIrisAutoSettleHelp extends ToolTip {

    protected String getHelp() {
        return "Used to signify whether or not you wish\n" +
                "the transaction to be captured in the next\n" +
                "batch or not. If set to YES and assuming\n" +
                "the transaction is authorised then it will\n" +
                "automatically be settled in the next batch.\n" +
                "If set to NO then you must use the\n" +
                "RealControl application to manually settle\n" +
                "the transaction. This option can be used\n" +
                "if you wish to delay the payment until\n" +
                "after the goods have been shipped.\n" +
                "Transactions can be settled for up to\n" +
                "115% of the original amount and must\n" +
                "be settled within 28 days of capture.";
    }
}
