package org.sevensoft.ecreator.iface.admin.system.lookandfeel;

import org.sevensoft.commons.superstrings.HtmlHelper;

/**
 * @author sks 4 Apr 2007 12:12:41
 *
 */
public abstract class ToolTip {

	protected abstract String getHelp();

	public String getId() {
		return "helpdiv" + getClass().getSimpleName();
	}

	@Override
	public String toString() {
		return HtmlHelper.nl2br(getHelp());
	}
}
