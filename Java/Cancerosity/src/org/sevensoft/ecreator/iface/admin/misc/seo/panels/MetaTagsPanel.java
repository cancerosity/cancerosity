package org.sevensoft.ecreator.iface.admin.misc.seo.panels;

import org.sevensoft.ecreator.help.seo.TitleTagHelp;
import org.sevensoft.ecreator.iface.admin.misc.seo.KeywordSuggestionsHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.misc.seo.Meta;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.JavascriptLinkTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 17 Aug 2006 15:33:22
 *
 */
public class MetaTagsPanel {

	private Meta		meta;
	private RequestContext	context;

	public MetaTagsPanel(RequestContext context, Meta meta) {
		this.context = context;
		this.meta = meta;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append(new AdminTable("Meta tags"));

		sb.append("<tr><td colspan='2'>If you want to use our keyword suggestion tool "
				+ new JavascriptLinkTag(context, new Link(KeywordSuggestionsHandler.class, "whitelabel"), "click here", 600, 400) + ".</td></tr>");

		sb.append(new AdminRow("Title tag", "Enter the title or leave blank to use default.", new TextTag(context, "titleTag", meta.getTitleTag(), 60))
				.setHelp(new TitleTagHelp()));

		sb.append(new AdminRow("Description tag", "Enter the description or leave blank to use default.", new TextTag(context, "descriptionTag", meta
				.getDescriptionTag(), 60)));

		sb.append(new AdminRow("Keywords", "Enter keywords separated by commas or leave blank to use default.", new TextTag(context, "keywords", meta
				.getKeywords(), 60)));

		sb.append("</table>");

		return sb.toString();
	}
}
