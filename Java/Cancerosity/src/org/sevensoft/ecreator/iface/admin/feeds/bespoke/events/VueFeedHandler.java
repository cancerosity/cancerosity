package org.sevensoft.ecreator.iface.admin.feeds.bespoke.events;

import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.events.VueFeed;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 3 Aug 2006 11:19:14
 *
 */
@Path("admin-feeds-vue.do")
public class VueFeedHandler extends FeedHandler {

	private VueFeed	feed;
	private Attribute	timeAttribute;
	private Attribute	bookingAttribute;
	private Attribute	classificationAttribute;
	private Attribute	dateAttribute;
	private Attribute	filmIdAttribute;
	private Attribute	venueAttribute;
	private ItemType	itemType;
	private String	venue;

	public VueFeedHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected ImportFeed getFeed() {
		return feed;
	}

	@Override
	protected void saveSpecific() {

		feed.setDateAttribute(dateAttribute);
		feed.setFilmIdAttribute(filmIdAttribute);
		feed.setClassificationAttribute(classificationAttribute);
		feed.setBookingAttribute(bookingAttribute);
		feed.setVenueAttribute(venueAttribute);
		feed.setVenue(venue);
		feed.setItemType(itemType);
	}

	/**
	 *
	 */
	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Vue specifics"));

		sb.append(new AdminRow("Item type", "Select the item type to import to.", new SelectTag(context, "itemType", feed.getItemType(), ItemType
				.getSelectionMap(context), "-None set-")));

		if (feed.hasItemType()) {

			SelectTag attributeTag = new SelectTag(context, "bookingAttribute", feed.getBookingAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append(new AdminRow("Booking attribute", null, attributeTag));

			attributeTag = new SelectTag(context, "classificationAttribute", feed.getClassificationAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append(new AdminRow("Classification attribute", null, attributeTag));

			attributeTag = new SelectTag(context, "dateAttribute", feed.getDateAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append(new AdminRow("Date attribute,", null, attributeTag));

			attributeTag = new SelectTag(context, "filmIdAttribute", feed.getFilmIdAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append(new AdminRow("Film ID attribute", null, attributeTag));

			attributeTag = new SelectTag(context, "venueAttribute", feed.getVenueAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append(new AdminRow("Venue attribute", null, attributeTag));

			sb.append(new AdminRow("Venue", "What to set as the venue value, eg, Vue Cambridge", new TextTag(context, "venue", feed.getVenue(), 40)));

			sb.append("</table>");

		}
	}
}
