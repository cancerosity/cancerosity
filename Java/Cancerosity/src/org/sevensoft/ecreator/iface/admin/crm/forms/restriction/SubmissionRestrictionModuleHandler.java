package org.sevensoft.ecreator.iface.admin.crm.forms.restriction;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.model.crm.forms.submission.restriction.SubmissionRestrictionModule;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;

import javax.servlet.ServletException;

/**
 * User: Tanya
 * Date: 29.02.2012
 */
@Path("admin-forms-submissions-restriction-module.do")
public class SubmissionRestrictionModuleHandler extends AdminHandler {

    private transient SubmissionRestrictionModule module;

    private int submissionsAllowed;
    private String customMessage;

    public SubmissionRestrictionModuleHandler(RequestContext context) {
        super(context);
        this.module = SubmissionRestrictionModule.getInstance(context);
    }

    public Object main() throws ServletException {
        AdminDoc page = new AdminDoc(context, user, "IP Form submission restrictions", Tab.Extras);
        if (miscSettings.isAdvancedMode()) {
            page.setMenu(new ExtrasMenu());
        }

        page.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update"));

                sb.append("</div>");
            }

            private void general() {

                sb.append(new AdminTable("General"));

                sb.append(new AdminRow("Submissions allowed", "Number of form submissions allwed to be sent the the IP per day ",
                        new TextTag(context, "submissionsAllowed", module.getSubmissionsAllowed(), 20)));


                sb.append(new AdminRow("Custom message", "The message a user will be shown if attempt to send more form submissions than allowed",
                        new TextAreaTag(context, "customMessage", module.getCustomMessage(), 40, 3)));


                sb.append("</table>");
            }


            @Override
            public String toString() {

                sb.append(new FormTag(SubmissionRestrictionModuleHandler.class, "save", "multi"));
                sb.append(new HiddenTag("module", module));

                commands();
                general();

                commands();

                sb.append("</form>");
                return sb.toString();
            }

        });

        return page;
    }

    public Object save() throws ServletException {

        if (module == null) {
            return index();
        }

        module.setSubmissionsAllowed(submissionsAllowed);
        module.setCustomMessage(customMessage);
        module.save();

        return new ActionDoc(context, "Module updated", new Link(SubmissionRestrictionModuleHandler.class));
    }
}
