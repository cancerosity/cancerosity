package org.sevensoft.ecreator.iface.admin.extras.calculators;

import java.util.List;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.extras.calculators.Calculator;
import org.sevensoft.ecreator.model.extras.calculators.CalculatorField;
import org.sevensoft.ecreator.model.extras.calculators.CalculatorPreset;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 07-Jul-2005 12:39:17
 * 
 */
@Path("admin-calculators.do")
public class CalculatorHandler extends AdminHandler {

	private Calculator		calculator;
	private String			name, expression;
	private CalculatorPreset	preset;
	private CalculatorField		field;
	private Category			category;
	private boolean			box;
	private String			submitButtonText;
	private String			newFields;

	public CalculatorHandler(RequestContext context) {
		super(context);
	}

	public CalculatorHandler(RequestContext context, Calculator calculator) {
		super(context);
		this.calculator = calculator;
	}

	public Object addPreset() {

		if (preset != null)
			calculator = preset.create(context);

		return edit();
	}

	public Object create() {

		calculator = new Calculator(context, "-new calculator-");
		return edit();
	}

	public Object delete() {

		if (calculator == null)
			return main();

		calculator.delete();
		addMessage("The '" + calculator.getName() + "' calculator has been deleted.");
		return main();
	}

	public Object edit() {

		if (calculator == null)
			return main();

		final List<CalculatorField> fields = calculator.getFields();

		AdminDoc page = new AdminDoc(context, user, "Edit calculator", Tab.Extras);
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update calculator"));
				sb.append(new ButtonTag(CalculatorHandler.class, "delete", "Delete calculator", "calculator", calculator)
						.setConfirmation("Are you sure you want to delete this calculator?"));
				sb.append(new ButtonTag(CalculatorHandler.class, null, "Return to menu"));
				sb.append("</div>");
			}

			private void fields() {

				sb.append(new AdminTable("Calculator fields"));

				sb.append("<tr>");
				sb.append("<th> </th>");
				sb.append("<th>Position</th>");
				sb.append("<th>Name</th>");
				sb.append("<th>Type</th>");
				sb.append("<th>Variable</th>");
				sb.append("<th>Expression</th>");
				sb.append("<th>&nbsp;</th>");
				sb.append("</tr>");

				PositionRender pr = new PositionRender(CalculatorFieldHandler.class, "field");
				int n = 1;

				MultiValueMap<String, CalculatorField> sectionsMap = CalculatorField.getSectionsMap(fields);
				for (String section : sectionsMap.keySet()) {

					sb.append("<tr><th colspan='6'>" + (section == null ? "No section" : section) + "</th></tr>");

					for (CalculatorField field : sectionsMap.list(section)) {

						sb.append("<tr>");
						sb.append("<td>" + new CheckTag(context, "fields", field, false) + "</td>");
						sb.append("<td width='120'>" + pr.render(field) + "</td>");
						sb.append("<td>" + (field.hasExtendedName() ? field.getExtendedName() : field.getName()) + "</td>");
						sb.append("<td>" + field.getType() + "</td>");
						sb.append("<td>" + (field.hasVariable() ? field.getVariable() : "") + "</td>");
						sb.append("<td>" + (field.hasExpression() ? field.getExpression() : "") + "</td>");
						sb.append("<td>"
								+ new LinkTag(CalculatorFieldHandler.class, null, "edit", "field", field)
								+ " | "
								+ new LinkTag(CalculatorFieldHandler.class, "delete", "delete", "field", field)
										.setConfirmation("If you delete this field it cannot be restored. Continue?") + "</td>");

						n++;
					}

				}

				sb.append("<tr><th colspan='7' align='right'>" + new SubmitTag("Delete selected") + "</th></tr>");
				sb.append("<tr><th colspan='7'>Add new fields</th></tr>");
				sb.append("<tr><td colspan='7'>" + new TextAreaTag(context, "newFields", 40, 4) + "</td></tr>");
				sb.append("</table>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append("<tr><th width='300'><b>Name</b></th><td>" + new TextTag(context, "name", calculator.getName(), 40) + "</td></tr>");

				sb.append("<tr><th width='300'><b>Submit button text</b><br/>Set the content of the button to submit this calculator</th><td>"
						+ new TextTag(context, "submitButtonText", calculator.getSubmitButtonText(), 20) + "</td></tr>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(CalculatorHandler.class, "save", "POST"));
				sb.append(new HiddenTag("calculator", calculator));

                commands();
				general();
				fields();
				commands();

				sb.append("</form>");
				return sb.toString();
			}
		});
		return page;
	}

	@Override
	public Object main() {

		final List<Calculator> calcs = Calculator.get(context);

		AdminDoc doc = new AdminDoc(context, user, "Calculators menu", Tab.Extras);
		doc.setMenu(new ExtrasMenu());
		doc.addBody(new Body() {

			private void forms() {

				sb.append(new AdminTable("Edit calculator"));
				sb.append(new FormTag(CalculatorHandler.class, "edit", "get"));

				if (calcs.size() > 0) {

					sb.append("<tr><th width ='300'>Choose the calculator to edit.</th>");

					SelectTag tag = new SelectTag(null, "calculator");
					tag.addOptions(calcs);

					sb.append("<td>" + tag + " " + new SubmitTag("Edit") + "</td></tr>");
				}

				sb.append("<tr><th width ='300'>Create a new calculator</th><td>"
						+ new ButtonTag(CalculatorHandler.class, "create", "Create calculator") + "</td></tr>");
				sb.append("</form>");

				if (isSuperman()) {

					SelectTag presetTag = new SelectTag(context, "preset");
					presetTag.addOptions(CalculatorPreset.values());

					sb.append(new FormTag(CalculatorHandler.class, "addPreset", "post"));
					sb.append("<tr><th>Add preset calculator</th><td>" + presetTag + " " + new SubmitTag("Add preset") + "</td></tr>");
					sb.append("</form>");

				}

				sb.append("</table>");

			}

			@Override
			public String toString() {

				forms();

				return sb.toString();
			}

		});
		return doc;
	}

	public Object removeField() {

		if (calculator == null || field == null)
			return edit();

		calculator.removeField(field);
		return edit();
	}

	public Object save() {

		if (calculator == null)
			return main();

		test(new RequiredValidator(), "name");
		if (hasErrors())
			return edit();

		calculator.setName(name);
		calculator.setSubmitButtonText(submitButtonText);
		calculator.save();

		if (newFields != null)
			for (String newField : StringHelper.explodeStrings(newFields, "\n"))
				calculator.addField(newField);

		clearParameters();
		addMessage("Changes saved.");
		return edit();
	}
}
