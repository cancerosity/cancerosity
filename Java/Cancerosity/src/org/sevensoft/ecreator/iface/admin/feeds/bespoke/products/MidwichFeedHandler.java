package org.sevensoft.ecreator.iface.admin.feeds.bespoke.products;

import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.ecom.suppliers.Supplier;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.products.midwich.MidwichFeed;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 3 Aug 2006 11:08:51
 *
 */
@Path("admin-feeds-midwich.do")
public class MidwichFeedHandler extends FeedHandler {

	private MidwichFeed	feed;
	private String		account;
	private Attribute		manufAttribute;
	private Attribute		mpnAttribute;
	private String		postcode;
	private Attribute		weightAttribute;
	private ItemType		itemType;
	private Supplier		supplier;

	public MidwichFeedHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected ImportFeed getFeed() {
		return feed;
	}

	@Override
	protected void saveSpecific() {

		feed.setAccount(account);
		feed.setManufAttribute(manufAttribute);
		feed.setMpnAttribute(mpnAttribute);
		feed.setPostcode(postcode);
		feed.setWeightAttribute(weightAttribute);
		feed.setItemType(itemType);

		if (Module.Suppliers.enabled(context)) {
			feed.setSupplier(supplier);
		}
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Midwich specifics"));

		sb.append(new AdminRow("Item type", "Select the item type to import to.", new SelectTag(context, "itemType", feed.getItemType(), ItemType
				.getSelectionMap(context), "-None set-")));

		sb.append(new AdminRow("Supplier</b><br/>Select the supplier.", new SelectTag(context, "supplier", feed.getSupplier(false), Supplier.get(context),
				"-None set-")));

		sb.append(new AdminRow("Account number", new TextTag(context, "account", feed.getAccount(), 30)));
		sb.append(new AdminRow("Image postcode", new TextTag(context, "postcode", feed.getPostcode(), 16)));

		if (feed.hasItemType()) {

			SelectTag attributeTag = new SelectTag(context, "mpnAttribute", feed.getMpnAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append("<tr><th width='300'><b>Mpn attribute</b></th><td>" + attributeTag + "</td></tr>");

			attributeTag = new SelectTag(context, "manufAttribute", feed.getManufAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append("<tr><th width='300'><b>Manufacturer attribute</b></th><td>" + attributeTag + "</td></tr>");

			attributeTag = new SelectTag(context, "weightAttribute", feed.getWeightAttribute());
			attributeTag.setAny("-None-");
			attributeTag.addOptions(feed.getItemType().getAttributes());
			sb.append("<tr><th width='300'><b>Weight attribute</b></th><td>" + attributeTag + "</td></tr>");

		}

		sb.append("</table>");
	}
}
