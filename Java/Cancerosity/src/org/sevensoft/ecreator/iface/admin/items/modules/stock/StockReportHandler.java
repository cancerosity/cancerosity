package org.sevensoft.ecreator.iface.admin.items.modules.stock;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 28 Dec 2006 13:38:54
 */
@Path("admin-stock-report.do")
public class StockReportHandler extends AdminHandler {

    private int page;
    private ItemType itemType;

    public StockReportHandler(RequestContext context) {
        super(context);
    }

    @Override
    public Object main() throws ServletException {
        context.disableCache();
        ItemSearcher searcher = new ItemSearcher(context);
        searcher.setStatus("Live");
        searcher.setStart(0);
        searcher.setItemType(itemType);
//		searcher.setLimit(1000);

        final List<Item> items = searcher.getItems();

        // generate a report for items showing their current stock level
        ReportDoc doc = new ReportDoc(context, itemType.getNamePlural() + " stock take");
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append(new TableTag("report"));

                sb.append("<tr>");
                sb.append("<th>Id</th>");
                sb.append("<th>Reference</th>");
                sb.append("<th>Name</th>");
                sb.append("<th>Price</th>");
                sb.append("<th>Stock</th>");
                sb.append("</tr>");

                for (Item item : items) {

                    sb.append("<tr>");
                    sb.append("<td>" + item.getId() + "</td>");
                    sb.append("<td>" + item.getReference() + "</td>");
                    sb.append("<td>" + item.getName() + "</td>");
                    sb.append("<td>" + item.getSellPrice() + "</td>");
                    sb.append("<td>" + item.getOurStock() + "</td>");
                    sb.append("</tr>");
                }

                return sb.toString();
            }

        });

        return doc;
    }
}
