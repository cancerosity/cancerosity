package org.sevensoft.ecreator.iface.admin.items.modules.stock;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemMenu;
import org.sevensoft.ecreator.iface.admin.items.search.ItemSearchHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.ecom.suppliers.Sku;
import org.sevensoft.ecreator.model.ecom.suppliers.Supplier;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.ecreator.model.items.stock.StockModule;
import org.sevensoft.ecreator.model.items.stock.SupplierStock;
import org.sevensoft.ecreator.model.items.stock.StockModule.StockCalculationSystem;
import org.sevensoft.ecreator.model.items.stock.StockModule.StockControl;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 20 Dec 2006 10:06:38
 *
 */
@Path("admin-items-stock.do")
public class ItemStockHandler extends AdminHandler {

	private Map<Supplier, Integer>	stocks;
	private Map<Supplier, String>		skus;
	private Item				item;
	private int					ourStock;
	private boolean				backorders;
	private String				outStockMsg;
	private boolean				booleanStock;
	private Supplier				addSupplier;
	private Supplier				addSupplierSku;
	private Supplier				supplier;

	public ItemStockHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (item == null) {
			return index();
		}

		final StockModule module = item.getItemType().getStockModule();

		setAttribute("view", item.getUrl());

		AdminDoc doc = new AdminDoc(context, user, "Edit " + item.getItemType().getName().toLowerCase() + " #" + item.getId() + " - '" + item.getName()
				+ "'", Tab.getItemTab(item));
		doc.setMenu(new EditItemMenu(item));
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				String lowerName = item.getItemType().getName().toLowerCase();
				sb.append(new SubmitTag("Update " + lowerName));
				sb.append(new ButtonTag(ItemHandler.class, "copy", "Copy " + lowerName, "item", item));
				sb.append(new ButtonTag(ItemHandler.class, "delete", "Delete " + lowerName, "item", item)
						.setConfirmation("Are you sure you want to delete this " + lowerName + "?"));

				sb.append(new ButtonTag(ItemSearchHandler.class, null, "Return to search", "itemType", item.getItemType()));

				sb.append("</div>");
			}

			private void options() {

				sb.append(new AdminTable("Availability and stock options"));

				switch (module.getStockControl()) {

				case None:
					break;

				case String:

					sb.append(new AdminRow("Availability", "The availability message showed to customers.", new TextTag(context, "outStockMsg", item
							.getOutStockMsg(), 40)));

					break;

				case Boolean:

					sb.append(new AdminRow("In stock", "Tick if this item is in stock.", new CheckTag(context, "booleanStock", true, item
							.isAvailable())));

					if (!module.hasOutStockMsgOverride()) {
						sb.append(new AdminRow("Out of stock message",
								"The message to display when this item is out of stock, eg, 'Dispatch in 2 weeks'.", new TextTag(context,
										"outStockMsg", item.getOutStockMsg(), 40)));
					}

					sb.append(new AdminRow("Backorders", "Allow customers to order when stock is zero.", new BooleanRadioTag(context, "backorders",
							item.isBackorders())));

					break;

				case Manual:
				case RealTime:

                    boolean isStockOverridden = false;
                    int overriddenStock = 0;
                    for (ItemOption option : item.getOptionSet().getOptions()) {
                        if (option.isStockOverride()) {
                            isStockOverridden = true;
                            for (ItemOptionSelection selection : option.getSelections()) {
                                overriddenStock += selection.getStock();
                            }
                        }
                    }
                    if (!isStockOverridden) {
                        sb.append(new AdminRow("Our stock", "The quantity we currently hold in stock.", new TextTag(context, "ourStock", item
                                .getOurStock(), 8)));

                        if (module.getStockSystem() != StockCalculationSystem.Internal) {

                            sb.append(new AdminRow("Available stock", "This is the stock level used for customer information.", item
                                    .getAvailableStock()));
                        }
                    }
                    else {
                        sb.append(new AdminRow("Cumulative stock", "Your stock level for this item calculated from options", new TextTag(context, "ourStock", overriddenStock, 4).setDisabled(true)));
                    }

					if (!module.hasOutStockMsgOverride()) {
						sb.append(new AdminRow("Out of stock message",
								"The message to display when this item is out of stock, eg, 'Dispatch in 2 weeks'.", new TextTag(context,
										"outStockMsg", item.getOutStockMsg(), 40)));
					}

					sb.append(new AdminRow("Backorders", "Allow customers to order when stock is zero.", new BooleanRadioTag(context, "backorders",
							item.isBackorders())));

				}

				sb.append("</table>");
			}

			private void skus() {

				List<Supplier> suppliers = Supplier.get(context);
				if (suppliers.isEmpty()) {
					return;
				}

				sb.append(new AdminTable("Skus"));

				sb.append("<tr>");
				sb.append("<td>Supplier</td>");
				sb.append("<td>Sku</td>");
				sb.append("<td>Delete</td>");
				sb.append("</tr>");

				for (Sku supplierSku : item.getSupplierSkus()) {

					sb.append("<tr>");
					sb.append("<td>" + supplierSku.getSupplier().getName() + " (#" + supplierSku.getSupplier().getId() + ")</td>");
					sb.append("<td>" + new TextTag(context, "skus~" + supplierSku.getSupplier().getId(), supplierSku.getSku(), 20) + "</td>");
					sb.append("<td>"
							+ new ButtonTag(ItemHandler.class, "removeSku", "Delete", "supplier", supplierSku.getSupplier(), "item", item)
									.setConfirmation("Are you sure you want to remove this sku?") + "</td>");
					sb.append("</tr>");

				}

				suppliers.removeAll(item.getSkuSuppliers());
				if (!suppliers.isEmpty()) {

					SelectTag supplierTag = new SelectTag(context, "addSkuSupplier");
					supplierTag.setAny("Choose supplier to add");
					supplierTag.addOptions(suppliers);

					sb.append("<tr><td colspan='3'>Add sku for supplier: " + supplierTag + "</td></tr>");

				}

				sb.append("</table>");
			}

			private void stocks() {

				List<Supplier> suppliers = Supplier.get(context);
				if (suppliers.isEmpty()) {
					return;
				}

				sb.append(new AdminTable("Supplier stocks"));

				sb.append("<tr>");
				sb.append("<td>Supplier</td>");
				sb.append("<td>Stock</td>");
				sb.append("<td>Delete</td>");
				sb.append("</tr>");

				for (SupplierStock stock : item.getSupplierStocks()) {

					sb.append("<tr>");
					sb.append("<td>" + stock.getSupplier().getName() + " (#" + stock.getSupplier().getId() + ")</td>");
					sb.append("<td>" + new TextTag(context, "stocks~" + stock.getSupplier().getId(), stock.getStock(), 6) + "</td>");
					sb.append("<td>"
							+ new ButtonTag(ItemHandler.class, "removeStock", "Delete", "supplier", stock.getSupplier(), "item", item)
									.setConfirmation("Are you sure you want to remove this stock supply?") + "</td>");
					sb.append("</tr>");

				}

				suppliers.removeAll(item.getStockSuppliers());
				if (!suppliers.isEmpty()) {

					SelectTag supplierTag = new SelectTag(context, "addSupplier");
					supplierTag.setAny("Choose supplier to add");
					supplierTag.addOptions(suppliers);

					sb.append("<tr><td colspan='3'>Add stock for supplier: " + supplierTag + "</td></tr>");

				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(ItemHandler.class, "save", "post"));
				sb.append(new HiddenTag("item", item));

                commands();
				options();

				if (Module.Suppliers.enabled(context)) {

					if (module.getStockControl() == StockControl.RealTime) {
						stocks();
					}

					skus();
				}

				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object removeStock() throws ServletException {

		if (item == null || supplier == null) {
			return main();
		}

		item.removeStock(supplier, true);
		addMessage("Stock from this supplier has been removed");
		return main();
	}

	public Object save() throws ServletException {

		if (item == null) {
			return main();
		}

		StockModule module = item.getItemType().getStockModule();

		item.setOurStock(ourStock);
		item.setOutStockMsg(outStockMsg);
		item.setBackorders(backorders);

		if (module.getStockControl() == StockControl.Boolean) {
			item.setBooleanStock(booleanStock);
		}

		for (Map.Entry<Supplier, Integer> entry : stocks.entrySet()) {
			item.setSupplierStock(entry.getKey(), entry.getValue());
		}

		for (Map.Entry<Supplier, String> entry : skus.entrySet()) {
			item.setSupplierSku(entry.getKey(), entry.getValue());
		}

		if (addSupplier != null) {
			item.setSupplierStock(addSupplier, 0);
		}

		if (addSupplierSku != null) {
			item.setSupplierSku(addSupplierSku, "-");
		}

		item.save();

		addMessage("Your changes have been saved");
		clearParameters();
		return main();
	}
}
