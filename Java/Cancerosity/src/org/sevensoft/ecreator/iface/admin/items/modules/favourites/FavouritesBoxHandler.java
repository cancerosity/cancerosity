package org.sevensoft.ecreator.iface.admin.items.modules.favourites;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.items.favourites.FavouritesGroup;
import org.sevensoft.ecreator.model.items.favourites.box.FavouritesBox;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 4 Sep 2006 12:40:04
 *
 */
@Path("admin-boxes-favourites.do")
public class FavouritesBoxHandler extends BoxHandler {

	private FavouritesBox	box;
	private FavouritesGroup	favouritesGroup;

	public FavouritesBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {
//		box.setFavouritesGroup(favouritesGroup);
	}

	@Override
	protected void specifics(StringBuilder sb) {

//		sb.append(new TableTag("form",1,0).setCaption("Favourites group"));
//
//		SelectTag selectTag = new SelectTag(context, "favouritesGroup", box.getFavouritesGroup());
//		selectTag.setAny("None selected");
//		selectTag.addOptions(FavouritesGroup.get(context));
//		sb.append("<tr><th width='300'><b>Group</b></th><td>" + selectTag + "</td></tr>");
//
//		sb.append("</table>");
	}
}
