package org.sevensoft.ecreator.iface.admin.marketing.sms;

import javax.servlet.ServletException;

import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.marketing.sms.SmsBulletin;
import org.sevensoft.ecreator.model.marketing.sms.SmsSettings;
import org.sevensoft.ecreator.model.marketing.sms.exceptions.ExistingRegistrationException;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 10 Nov 2006 11:17:45
 *
 */
@Path("admin-sms-registrations.do")
public class SmsRegistrationsHandler extends AdminHandler {

	private transient SmsSettings	smsSettings;
	private String			number;
	private String			submit;

	public SmsRegistrationsHandler(RequestContext context) {
		super(context);
		this.smsSettings = SmsSettings.getInstance(context);
	}

	public Object action() throws ServletException {

		test(new RequiredValidator(), "number");
		test(new LengthValidator(11), "number");
		if (hasErrors())
			return main();

		String title;
		if ("Add number".equals(submit)) {

			title = "Mobile number has been added";
			try {
				smsSettings.addRegistration(number, SmsBulletin.get(context));
			} catch (ExistingRegistrationException e) {
				e.printStackTrace();
				addError(e);
			}

		} else {

			title = "Mobile number has been removed";
			smsSettings.removeRegistration(number, SmsBulletin.get(context));
		}

		AdminDoc doc = new AdminDoc(context, user, title, Tab.Marketing);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new TableTag("simplelinks", "center"));
				sb.append("<tr><th align='center'>What would you like to do ?</th></tr>");

				sb.append("<tr><td align='center'>" + new LinkTag(SmsComposeHandler.class, null, "I want to send an SMS bulletin now") + "</td></tr>");

				sb.append("<tr><td align='center'>" +
						new LinkTag(SmsRegistrationsHandler.class, "main", "I want to add or remove another mobile number") + "</td></tr>");

				sb.append("</table>");

				return sb.toString();
			}
		});
		return doc;

	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Sms Registrations", Tab.Marketing);
		doc.addBody(new Body() {

            private void commands() {
                sb.append("<div align='center' class='actions'>" + new SubmitTag("submit", "Add number") + " " +
                        new SubmitTag("submit", "Remove number") + "</div>");
            }

			@Override
			public String toString() {

				sb.append(new FormTag(SmsRegistrationsHandler.class, "action", "POST"));

                commands();
                
				sb.append(new TableTag("simplebox", "center"));

				sb.append("<tr><td align='center'>Enter the mobile number you want to add or remove.</td></tr>");
				sb.append("<tr><td align='center'>" + new TextTag(context, "number", 20) + new ErrorTag(context, "number", "<br/>") +
						"<br/><br/></td></tr>");

				sb.append("</table>");

				commands();

				sb.append("</form>");
				return sb.toString();
			}

		});
		return doc;
	}

}
