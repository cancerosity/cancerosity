package org.sevensoft.ecreator.iface.admin.ecom.bookings.menu;

import org.sevensoft.ecreator.iface.admin.reminders.HolidayReminderSettingsEditor;
import org.sevensoft.ecreator.iface.admin.reminders.export.HolidayRemindersExportHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Menu;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import java.util.ArrayList;
import java.util.List;

/**
 * User: MeleshkoDN
 * Date: 07.09.2007
 * Time: 10:17:26
 */
public class BookingMenu extends Menu {

    @Override
    public List<LinkTag> getLinkTags(RequestContext context) {
        List<LinkTag> links = new ArrayList<LinkTag>();
        links.add(new LinkTag(HolidayReminderSettingsEditor.class, null, "Holiday reminders settings"));
        links.add(new LinkTag(HolidayRemindersExportHandler.class, null, "Export holiday reminders"));
        return links;
    }
}
