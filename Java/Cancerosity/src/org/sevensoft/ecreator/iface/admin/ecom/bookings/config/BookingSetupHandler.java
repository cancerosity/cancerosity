package org.sevensoft.ecreator.iface.admin.ecom.bookings.config;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.bookings.BookingDiscount;
import org.sevensoft.ecreator.model.bookings.BookingOption;
import org.sevensoft.ecreator.model.bookings.BookingSetup;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sam
 */
@Path("admin-bookings-setup.do")
public class BookingSetupHandler extends AdminHandler {

	private Item	item;
	private int		maxNights;
	private int		maxChildren;
	private int		maxAdults;
	private int		maxOccupants;
	private int		qty;
	private Money	rate;

	public BookingSetupHandler(RequestContext context) {
		this(context, null);
	}

	public BookingSetupHandler(RequestContext context, Item item) {
		super(context);
		this.item = item;
	}

	@Override
	public Object main() throws ServletException {

		if (item == null) {
			return index();
		}

		final BookingSetup setup = item.getBookingSetup();
		final List<BookingOption> options = setup.getOptions();
		final List<BookingDiscount> discounts = setup.getDiscounts();

		AdminDoc doc = new AdminDoc(context, user, "Booking details", Tab.getItemTab(item));
		doc.setMenu(new EditItemMenu(item));
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update booking details"));
				sb.append("</div>");
			}

			private void dates() {

				sb.append(new AdminTable("Dates"));
				sb.append("<tr><td>To edit the availability calendar, and set rates for each day, "
						+ new LinkTag(BookingDatesHandler.class, null, "click here", "item", item) + "</td></tr>");
				sb.append("</table>");

			}

			private void discounts() {

				sb.append(new AdminTable("Discounts"));

				sb.append("<tr>");
				sb.append("<th>Nights</th>");
				sb.append("<th>Discount</th>");
				sb.append("<th width='10'> </th>");
				sb.append("</tr>");

				for (BookingDiscount discount : discounts) {

					sb.append("<tr>");

					sb.append("<td>" + new LinkTag(BookingDiscountsHandler.class, null, new SpannerGif(), "discount", discount) + " "
							+ discount.getNights() + "</td>");

					sb.append("<td>" + discount.getAmount() + "</td>");

					sb.append("<td width='10'>"
							+ new LinkTag(BookingDiscountsHandler.class, "delete", new DeleteGif(), "discount", discount)
									.setConfirmation("Are you sure you want to delete this discount?") + "</td>");

					sb.append("</tr>");

				}

				sb.append("<tr>");
				sb.append("<td colspan='3'>Add new discount: " + new ButtonTag(BookingDiscountsHandler.class, "create", "Add", "item", item)
						+ "</td></tr>");

				sb.append("</tr>");

				sb.append("</table>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Max occupants", "The maximum number of people who can book this item.", new TextTag(context, "maxOccupants",
						setup.getMaxOccupants(), 8)));

				sb.append(new AdminRow("Max adults", "The maximum number of adults who can book this item.", new TextTag(context, "maxAdults", setup
						.getMaxAdults(), 8)));

				sb.append(new AdminRow("Max children", "The maximum number of children who can book this item.", new TextTag(context, "maxChildren",
						setup.getMaxChildren(), 8)));

				sb.append(new AdminRow("Max nights", "The maximum number of nights one can book at once.", new TextTag(context, "maxNights", setup
						.getMaxNights(), 8)));

				sb.append(new AdminRow("Qty", "The number of individual items available for booking.", new TextTag(context, "qty", setup.getQty(), 8)));

				sb.append(new AdminRow("Default rate", "This default rate is used unless overriden on a per day basis.", new TextTag(context, "rate",
						setup.getRate(), 12)));

				sb.append("</table>");
			}

			private void options() {

				sb.append(new AdminTable("Options"));

				sb.append("<tr>");
				sb.append("<th width='80'>Position</th>");
				sb.append("<th>Name</th>");
				sb.append("<th>Charge</th>");
				sb.append("<th width='10'> </th>");
				sb.append("</tr>");

				PositionRender pr = new PositionRender(BookingOptionsHandler.class, "option");
				for (BookingOption option : options) {

					sb.append("<tr>");
					sb.append("<td width='80'>" + pr.render(option) + "</td>");
					sb.append("<td>" + new LinkTag(BookingOptionsHandler.class, null, new SpannerGif(), "option", option) + " " + option.getName()
							+ "</td>");
					sb.append("<td>" + option.getChargeEx() + "</td>");
					sb.append("<td width='10'>"
							+ new LinkTag(BookingOptionsHandler.class, "delete", new DeleteGif(), "option", option)
									.setConfirmation("Are you sure you want to delete this option?") + "</td>");
					sb.append("</tr>");

				}

				sb.append("<tr>");
				sb
						.append("<td colspan='4'>Add new option: " + new ButtonTag(BookingOptionsHandler.class, "create", "Add", "item", item)
								+ "</td></tr>");
				sb.append("</tr>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(BookingSetupHandler.class, "save", "post"));
				sb.append(new HiddenTag("item", item));

                commands();
				general();
				options();
				dates();
				commands();

				sb.append("</form>");
				return sb.toString();
			}

		});
		return doc;

	}

	public Object save() throws ServletException {

		if (item == null) {
			return main();
		}

		final BookingSetup setup = item.getBookingSetup();

		setup.setMaxAdults(maxAdults);
		setup.setMaxChildren(maxChildren);
		setup.setMaxNights(maxNights);
		setup.setQty(qty);
		setup.setMaxOccupants(maxOccupants);
		setup.setRate(rate);
		setup.save();

		clearParameters();
		return new ActionDoc(context, "Booking setup updated", new Link(BookingSetupHandler.class, null, "item", item));
	}
}
