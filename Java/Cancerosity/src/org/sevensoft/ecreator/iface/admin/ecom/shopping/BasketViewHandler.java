package org.sevensoft.ecreator.iface.admin.ecom.shopping;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 2 Apr 2007 11:21:30
 * 
 * View baskets were the user has not checked out but has at least one item in the basket
 *
 */
@Path("admin-baskets.do")
public class BasketViewHandler extends AdminHandler {

	public BasketViewHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		final List<Basket> baskets = Basket.getInUse(context);

		AdminDoc doc = new AdminDoc(context, user, "Baskets", null);
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Baskets"));

				sb.append("<tr>");
				sb.append("<td>Id</td>");
				sb.append("<td>Session ID</td>");
				sb.append("<td>Name</td>");
				sb.append("<td>Email</td>");
				sb.append("<td>Lines</td>");
				sb.append("<td>Items</td>");
				sb.append("<td>Total</td>");
				sb.append("</tr>");

				for (Basket basket : baskets) {

					sb.append("<tr>");
					sb.append("<td>" + basket.getId() + "</td>");
					sb.append("<td>" + basket.getSessionId() + "</td>");
					sb.append("<td>" + basket.getName() + "</td>");
					sb.append("<td>" + basket.getEmail() + "</td>");
					sb.append("<td>" + basket.getLineCount() + "</td>");
					sb.append("<td>" + basket.getItemCount() + "</td>");
					sb.append("<td>" + basket.getTotalInc() + "</td>");
					sb.append("</tr>");

				}

				sb.append("</table>");

				return sb.toString();
			}

		});

		return doc;
	}
}
