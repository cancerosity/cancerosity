package org.sevensoft.ecreator.iface.admin.items.modules.stock;

import java.io.IOException;

import javax.servlet.ServletException;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.docs.Doc;

/**
 * @author sks 28 Dec 2006 13:39:47
 *
 */
public class ReportDoc extends Doc {

	private final String	title;

	public ReportDoc(RequestContext context, String title) {
		super(context);
		this.title = title;
	}

	@Override
	public StringBuilder output() throws ServletException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {

		StringBuilder sb = new StringBuilder();

		sb.append("<html><head>");
		sb.append("<style>");
		sb.append("* { font-family: Courier new; } ");
		sb.append("div.title { font-weight: bold; } ");
		sb.append("div.date { font-size: 13px; } ");
		sb.append("table.report { width: 630px; } ");
		sb.append("table.report td, table.report th { padding: 4px; } ");

		sb.append("</style>");
		sb.append("</head><body>");
		sb.append("<div class='title'>");
		sb.append(title);
		sb.append("</div>");
		sb.append("<div class='date'>Report generated on ");
		sb.append(new DateTime().toString("HH:mm dd-MMM-yyyy"));
		sb.append("</div><br/>");

		for (Object body : getBodies()) {
			sb.append(body);
		}

		sb.append("</body></html>");
		return sb;
	}

}
