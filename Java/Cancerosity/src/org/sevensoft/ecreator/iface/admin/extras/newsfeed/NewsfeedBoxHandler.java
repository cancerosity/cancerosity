package org.sevensoft.ecreator.iface.admin.extras.newsfeed;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.extras.rss.boxes.NewsfeedBox;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 2 Aug 2006 11:25:43
 *
 */
@Path("admin-newsfeed-box.do")
public class NewsfeedBoxHandler extends BoxHandler {

	private NewsfeedBox	box;

	public NewsfeedBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {
	}

}
