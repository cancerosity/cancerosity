package org.sevensoft.ecreator.iface.admin.system.config;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategoryItem;
import org.sevensoft.ecreator.model.ecom.shopping.Basket;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.pricing.Price;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 20 Oct 2006 12:14:44
 *
 */
@Path("admin-stats.do")
public class StatsHandler extends AdminHandler {

	public StatsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc d = new AdminDoc(context, user, "Stats", null);
		d.addBody(new Body() {

			private void database() {

				sb.append(new AdminTable("Stats "));

				sb.append(new AdminRow("Categories", "Total number of categories on the site.", SimpleQuery.count(context, CategoryItem.class)));

				sb.append(new AdminRow("Category items", "The number of item to category mappings.", SimpleQuery.count(context, Category.class)));

				for (ItemType itemType : ItemType.get(context)) {
					sb.append(new AdminRow(itemType.getNamePlural(), "Number of live " + itemType.getNamePluralLower(), SimpleQuery.count(context,
							Item.class, "itemType", itemType, "status", "LIVE")));
				}

				sb
						.append(new AdminRow("Item options", "Number of separate options created on all items.", SimpleQuery.count(context,
								ItemOption.class)));

				sb.append(new AdminRow("Image objects", "Total number of references to images (not separate image files).", SimpleQuery.count(context,
						Img.class)));

				if (Module.Pricing.enabled(context)) {
					sb.append(new AdminRow("Prices", "Number of prices set.", SimpleQuery.count(context, Price.class)));
				}

				if (Module.Shopping.enabled(context)) {
					sb.append(new AdminRow("Baskets", "The number of different baskets in the database.", SimpleQuery.count(context, Basket.class)));
				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				database();

				return sb.toString();
			}
		});

		return d;
	}

}
