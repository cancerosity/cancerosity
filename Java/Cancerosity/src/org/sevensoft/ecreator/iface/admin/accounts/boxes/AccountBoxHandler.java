package org.sevensoft.ecreator.iface.admin.accounts.boxes;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.accounts.AccountBox;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 25 Jul 2006 06:51:28
 *
 */
@Path("admin-members-account-box.do")
public class AccountBoxHandler extends BoxHandler {

	private AccountBox	box;

	public AccountBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {
	}

}
