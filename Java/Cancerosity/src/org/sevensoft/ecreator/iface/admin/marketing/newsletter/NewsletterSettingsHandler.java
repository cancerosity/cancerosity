package org.sevensoft.ecreator.iface.admin.marketing.newsletter;

import java.io.File;
import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.marketing.MarketingMenu;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.attributes.renderers.OwnerAttributesPanel;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterControl;
import org.sevensoft.ecreator.model.marketing.newsletter.Subscriber;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterOption;
import org.sevensoft.ecreator.model.marketing.newsletter.sync.NewsletterCsvExport;
import org.sevensoft.ecreator.model.marketing.newsletter.sync.NewsletterCsvImporter;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.StreamResult;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 17-Jun-2005 17:37:55
 * 
 */
@Path("admin-newsletters-settings.do")
public class NewsletterSettingsHandler extends AdminHandler {

	private boolean					recipients;
	private String					emailsToAdd;
	private boolean					registrationOptIn;
	private Newsletter				newsletter;
	private String					emailsToRemove;
	private final transient NewsletterControl	newsletterSettings;
	private String					submit;

	private Upload					upload;
	private String					email;

	private final transient MiscSettings	miscSettings;
	private String					registrationOptInText;
	private String					newAttributes;
	private String					signupContent;
	private String					newAttributesSection;
	private int						newAttributesPage;
	private boolean					checkoutOptIn;
	private boolean	validateEmails;

	public NewsletterSettingsHandler(RequestContext context) {
		super(context);
		this.newsletterSettings = NewsletterControl.getInstance(context);
		this.miscSettings = MiscSettings.getInstance(context);
	}

	/**
	 * Export subscribers from all newsletters
	 */
	public Object export() throws ServletException {

		NewsletterCsvExport csv = new NewsletterCsvExport(context);
		return new StreamResult(csv.export(), "text/csv", "subscribers.csv", StreamResult.Type.Attachment);
	}

	public Object imp() throws ServletException {

		if (upload == null) {
			addError("You must supply a file for importing");
			return main();
		}

		File file = upload.getFile();

		NewsletterCsvImporter importer = new NewsletterCsvImporter(context);
		int n = importer.run(file);

		addMessage(n + " email addresses have been imported.");
		clearParameters();
		return main();
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Newsletter settings", Tab.Marketing);
		if (miscSettings.isAdvancedMode()) {
			doc.setMenu(new MarketingMenu());
		}
		doc.addBody(new Body() {

			private void advanced() {

				sb.append(new AdminTable("Advanced"));

				sb.append(new AdminRow("Recipients", "Advanced control over who receives each newsletter.", new BooleanRadioTag(context, "recipients",
						newsletterSettings.isRecipients())));

				sb.append(new AdminRow("Validate emails", "Require users to validate their email address.", new BooleanRadioTag(context,
						"validateEmails", newsletterSettings.isValidateEmails())));

				if (Module.Accounts.enabled(context)) {

					sb.append(new AdminRow("Registration Opt In", "Show a subscribe check box on the member registration page.", new BooleanRadioTag(
							context, "registrationOptIn", newsletterSettings.isRegistrationOptIn())));

					sb.append(new AdminRow("Registration Opt In text", "This is the text that appears above the registration opt in box.",
							new TextAreaTag(context, "registrationOptInText", newsletterSettings.getRegistrationOptInText(), 50, 4)));
				}

				if (Module.Shopping.enabled(context)) {

					sb.append(new AdminRow("Checkout opt in", "Show a subscribe check box on the checkout pages.", new BooleanRadioTag(context,
							"checkoutOptIn", newsletterSettings.isCheckoutOptIn())));

					sb.append(new AdminRow("Registration Opt In text", "This is the text that appears above the registration opt in box.",
							new TextAreaTag(context, "registrationOptInText", newsletterSettings.getRegistrationOptInText(), 50, 4)));
				}

				sb.append("</table>");
			}

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update newsletter settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void content() {

				sb.append(new AdminTable("Signup content"));
				sb.append("<tr><td>This text will appear on the newsletter sign up page.</td></tr>");
				sb.append("<tr><td>" +
						new TextAreaTag(context, "signupContent", newsletterSettings.getSignupContent()).setId("signupContent").setStyle(
								"width: 100%; height: 200px;") + "</td></tr>");
				sb.append("</table>");
			}

			private void data() {

				sb.append(new AdminTable("Data"));

				sb.append(new AdminRow("Tidy subscribers",
						"Running this command will attempt to remove any corrupt email addresses from the newsletter database.", new ButtonTag(
								NewsletterSettingsHandler.class, "tidy", "Tidy")));

				sb.append("</table>");
			}

			private void newsletters() {

				/*
				 * Only show multiple newsletters if in advanced mode
				 */
				if (miscSettings.isAdvancedMode()) {

					sb.append(new AdminTable("Newsletters"));

					sb.append("<tr>");
					sb.append("<td>Name</td>");
					sb.append("<td>Delete</td>");
					sb.append("</tr>");

					for (Newsletter newsletter : Newsletter.get(context)) {

						sb.append("<tr>");
						sb.append("<td>" + new LinkTag(NewsletterHandler.class, null, new SpannerGif(), "newsletter", newsletter) + " " +
								newsletter.getName() + "</td>");
						sb.append("<td>" +
								new ButtonTag(NewsletterHandler.class, "delete", "Delete", "newsletter", newsletter)
										.setConfirmation("Are you sure you want to delete this newsletter?") + "</td>");
						sb.append("</tr>");

					}

					sb.append("<tr><td colspan='4'>Add newsletter " + new ButtonTag(NewsletterHandler.class, "create", "Add") + "</td></tr>");

					sb.append("</table>");

				}

			}

			private void subscribers() {

				sb.append(new AdminTable("Subscribers"));

				sb.append(new FormTag(NewsletterSettingsHandler.class, "subscriber", "post"));
				sb.append(new AdminRow("Add / remove subscriber", "Enter an email address to subscribe or unsubscribe that address.", new TextTag(
						context, "email", 40) +
						" " + new SubmitTag("submit", "Add") + " " + new SubmitTag("submit", "Remove")));
				sb.append("</form>");

				sb.append(new AdminRow("Export", "Export all subscribers in CSV format.", new ButtonTag(NewsletterSettingsHandler.class, "export",
						"Export")));

				sb.append(new FormTag(NewsletterSettingsHandler.class, "imp", "multi"));
				sb.append(new AdminRow("Import", "Import subscribers from text file", new FileTag("upload") + " " + new SubmitTag("submit", "Import")));
				sb.append("</form>");

				sb.append("</table>");

			}

			@Override
			public String toString() {

				if (miscSettings.isHtmlEditor()) {
                    RendererUtil.tinyMce(context, sb, "signupContent");
				}

				newsletters();
				subscribers();

				sb.append(new FormTag(NewsletterSettingsHandler.class, "save", "POST"));

                commands();
				content();

				if (miscSettings.isAdvancedMode()) {
					sb.append(new OwnerAttributesPanel(context, newsletterSettings, new Link(NewsletterSettingsHandler.class)));
				}

				if (miscSettings.isAdvancedMode()) {
					data();
					advanced();
				}

				commands();

				sb.append("</form>");

				return sb.toString();
			}
		});
		return doc;
	}

	public Object save() throws ServletException {

		newsletterSettings.setSignupContent(signupContent);
		newsletterSettings.setRecipients(recipients);
		newsletterSettings.setRegistrationOptIn(registrationOptIn);
		newsletterSettings.setRegistrationOptInText(registrationOptInText);
		newsletterSettings.setCheckoutOptIn(checkoutOptIn);
		newsletterSettings.setValidateEmails(validateEmails);
		newsletterSettings.save();

		if (emailsToAdd != null) {
			if (newsletter != null) {
				//				newsletter.subscribe(StringHelper.explodeStrings(emailsToAdd, "\n"));
			}
		}

		if (emailsToRemove != null) {

			//			if (newsletter != null)
			//				newsletter.unsubscribe(StringHelper.explodeStrings(emailsToRemove, "\n"));
		}

		if (miscSettings.isAdvancedMode()) {
			// attributes
			if (newAttributes != null) {
				newsletterSettings.addAttributes(newAttributes, newAttributesSection, newAttributesPage);
			}
		}

		addMessage("Newsletter settings updated");
		clearParameters();
		return main();
	}

	public Object subscriber() throws ServletException {

		if (email == null) {
			addError("You must enter an email");
			return main();
		}

		List<Newsletter> newsletters = Newsletter.get(context);

		if ("add".equalsIgnoreCase(submit)) {

			Subscriber sub = Subscriber.get(context, email, true);
			sub.subscribe(newsletters);
            
            if (Module.NewsletterNewItems.enabled(context))
                sub.subscribeOpt(NewsletterOption.getVisible(context));

			addMessage("'" + email + "' has been subscribed to " + newsletters.size() + " newsletters.");

		} else if ("remove".equalsIgnoreCase(submit)) {

			Subscriber sub = Subscriber.get(context, email, true);
			sub.unsubscribe();
			addMessage("'" + email + "' has been unsubscribed to " + newsletters.size() + " newsletters.");

		}

		return main();
	}

	public Object tidy() throws ServletException {

		newsletterSettings.tidy();

		addMessage("Newsletter subscribers have been tidied up.");
		clearParameters();
		return main();
	}

}
