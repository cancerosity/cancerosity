package org.sevensoft.ecreator.iface.admin.items;

/**
 * User: MeleshkoDN
 * Date: 25.09.2007
 * Time: 18:33:04
 */
public enum RecurrenceOption {
    Day, Week, Month, Year
}
