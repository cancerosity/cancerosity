package org.sevensoft.ecreator.iface.admin.ecom.orders;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.renderers.OwnerAttributesPanel;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupTag;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 03-Oct-2005 14:25:41
 * 
 */
@Path("admin-settings-order.do")
public class OrderSettingsHandler extends AdminHandler {

	private String				invoiceHeader;
	private String				invoiceFooter;
	private boolean				parcels;
	private Attribute				smsOrderAttribute;
	private boolean				salesPersons;
	private boolean				messages;
	private String				completionEmailBody;
	private String				completedStatus;
	private String				initialStatus;
	private String				awaitingStockStatus;
	private String				newAttributes;
	private String				statuses;
	private transient OrderSettings	orderSettings;
	private String				paymentQueueStatus;
	private String				authorisationQueueStatus;
	private String				orderReadyStatus;
	private boolean				automaticStatus;
	private boolean				salesCategories;
	private boolean				emailInvoiceOnCompletion;
	private String				newAttributesSection;
	private int					newAttributesPage;
	private boolean				csvExport;
	private boolean				xmlExport;
	private Markup				invoiceLinesMarkup;
	private Markup				invoiceMarkup;
	private boolean				recurringOrders;
	private boolean				items;
	private String				categories;
	private boolean				splitOrdersByBranch;
	private String				orderIdSuffix;
	private String				orderIdPrefix;
    private String              track;
    private boolean printCustomerTelepnone;

    public OrderSettingsHandler(RequestContext context) {
		super(context);
		orderSettings = OrderSettings.getInstance(context);
	}

	@Override
	public Object main() {

		AdminDoc doc = new AdminDoc(context, user, "Order settings", Tab.Orders);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update order settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Parcel tracking", "Allows you to tag parcel consignments which the customer and track his order from.",
						new BooleanRadioTag(context, "parcels", orderSettings.isParcels())));

				sb.append(new AdminRow("Items", "Show search for items box.", new BooleanRadioTag(context, "items", orderSettings.isItems())));

				sb.append(new AdminRow("Sales persons",
						"Enables orders to be tagged to an individual user and reports generated on a user's sales productivity.",
						new BooleanRadioTag(context, "salesPersons", orderSettings.isSalesPersons())));

				sb.append(new AdminRow("Order messages", "Allows messages to be left on orders by admin users.", new BooleanRadioTag(context,
						"messages", orderSettings.isMessages())));

				sb.append(new AdminRow("Recurring orders", "Ability for orders to be set to be periodic and recur automatically.", new BooleanRadioTag(
						context, "recurringOrders", orderSettings.isRecurringOrders())));

				sb.append(new AdminRow("Sales categories", "Assign each order to a category for use on reports.", new TextAreaTag(context,
						"categories", StringHelper.implode(orderSettings.getCategories(), "\n"), 40, 4)));

				sb.append(new AdminRow("Order id prefix", "Prepend all order ids with this string.", new TextTag(context, "orderIdPrefix",
						orderSettings.getOrderIdPrefix())));

				sb.append(new AdminRow("Order id suffix", "Append all order ids with this string.", new TextTag(context, "orderIdSuffix", orderSettings
						.getOrderIdSuffix())));

				if (Module.Branches.enabled(context)) {
					sb.append(new AdminRow("Split orders by branch", new BooleanRadioTag(context, "splitOrdersByBranch", orderSettings
							.isSplitOrdersByBranch())));
				}

				sb.append(new AdminRow("Export XML", "Assign each order to a category for use on reports.", new BooleanRadioTag(context, "xmlExport",
						orderSettings.isXmlExport())));

				sb.append(new AdminRow("Export CSV", "Assign each order to a category for use on reports.", new BooleanRadioTag(context, "csvExport",
						orderSettings.isCsvExport())));

				sb.append("</table>");
			}

			private void invoice() {

				sb.append(new AdminTable("Invoice setup"));

				sb.append(new AdminRow("Email invoice on completion",
						"Automatically send an invoice by email to customers when their order is changed to the completed status.",
						new BooleanRadioTag(context, "emailInvoiceOnCompletion", orderSettings.isEmailInvoiceOnCompletion())));

				if (isMarkup()) {

					sb.append(new AdminRow("Invoice markup", new MarkupTag(context, "invoiceMarkup", orderSettings.getInvoiceMarkup(), "-Default-") + " "+
                    new ButtonTag(OrderSettingsHandler.class, "createInvoiceMarkup", "Create Invoice Markup")));

					sb.append(new AdminRow("Invoice lines markup", new MarkupTag(context, "invoiceLinesMarkup",
							orderSettings.getInvoiceLinesMarkup(), "-Default-") + " "+
                    new ButtonTag(OrderSettingsHandler.class, "createInvoiceLinesMarkup", "Create Lines Markup")));

                }

                sb.append(new AdminRow(" Print Customer Telephone number on Invoice", "", new BooleanRadioTag(context, "printCustomerTelepnone", orderSettings.isPrintCustomerTelepnone())));

                sb.append(new AdminRow("Invoice header", "This is the format of the top of your invoice. If you vanish it Header will be generated automatically.", new TextAreaTag(context,
                        "invoiceHeader", orderSettings.getInvoiceHeader(), 60, 8)));

                sb.append(new AdminRow("Invoice footer", "This is the format of the bottom of your invoice. If you vanish it Footer will be generated automatically.", new TextAreaTag(context,
                        "invoiceFooter", orderSettings.getInvoiceFooter(), 60, 8)));

				sb.append("</table>");
			}

			private void notifications() {

				sb.append(new AdminTable("Notifications"));

				sb.append(new AdminRow("Completion email",
						"If you enter a message in here, it will be emailed to customers once an order is set to completed.", new TextAreaTag(
								context, "completionEmailBody", orderSettings.getCompletionEmailBody(), 60, 6)));

				sb.append("</table>");
			}

			private void status() {

				sb.append(new AdminTable("Order status"));

				sb.append(new AdminRow("Order statuses", "These are the statuses your order can take. You cannot remove the cancelled status.",
						new TextAreaTag(context, "statuses", StringHelper.implode(orderSettings.getStatuses(), "\n", true), 40, 5)));

				sb.append(new AdminRow("Initial status", "The status orders are set to when they are initially created.", new TextTag(context,
						"initialStatus", orderSettings.getInitialStatus(), 40)));

                sb.append(new AdminRow("Track website", "Website used to track order", new TextTag(context, "track", orderSettings.getTrack(), 40)));

                sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(OrderSettingsHandler.class, "save", "POST"));

                commands();
				general();
				notifications();
				invoice();

				status();

				sb.append(new OwnerAttributesPanel(context, orderSettings));

				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() {

		orderSettings.setParcels(parcels);

		/*
		 * Invoice
		 */
		orderSettings.setInvoiceFooter(invoiceFooter);

        orderSettings.setPrintCustomerTelepnone(printCustomerTelepnone);

		if (isMarkup()) {
			orderSettings.setInvoiceMarkup(invoiceMarkup);
			orderSettings.setInvoiceLinesMarkup(invoiceLinesMarkup);
		}

		orderSettings.setSmsOrderAttribute(smsOrderAttribute);
		orderSettings.setSalesPersons(salesPersons);
		orderSettings.setMessages(messages);

		// deliveries
		orderSettings.setItems(items);

		orderSettings.setOrderIdSuffix(orderIdSuffix);
		orderSettings.setOrderIdPrefix(orderIdPrefix);

		orderSettings.setSplitOrdersByBranch(splitOrdersByBranch);

		orderSettings.setCompletionEmailBody(completionEmailBody);

		orderSettings.setStatuses(StringHelper.explodeStrings(statuses, "\\n"));

		orderSettings.setCategories(StringHelper.explodeStrings(categories, "\n"));
		orderSettings.setEmailInvoiceOnCompletion(emailInvoiceOnCompletion);

		orderSettings.setInitialStatus(initialStatus);
		orderSettings.setRecurringOrders(recurringOrders);
        orderSettings.setTrack(track);

        // exports
		orderSettings.setXmlExport(xmlExport);
		orderSettings.setCsvExport(csvExport);

		orderSettings.save();

		orderSettings.addAttributes(newAttributes, newAttributesSection, newAttributesPage);

		return new ActionDoc(context, "Order settings have been updated", new Link(OrderSettingsHandler.class));
	}

    public Object createInvoiceMarkup(){
       orderSettings.createInvoiceMarkup();

       return new ActionDoc(context, "Invoice Markup has been created", new Link(OrderSettingsHandler.class));
    }

    public Object createInvoiceLinesMarkup() {
        orderSettings.createInvoiceLinesMarkup();

        return new ActionDoc(context, "Invoice Lines Markup has been created", new Link(OrderSettingsHandler.class));
    }
}
