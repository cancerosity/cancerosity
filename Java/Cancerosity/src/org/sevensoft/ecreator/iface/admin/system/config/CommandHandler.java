package org.sevensoft.ecreator.iface.admin.system.config;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import org.jdom.JDOMException;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.cron.CronDailyTasks;
import org.sevensoft.ecreator.iface.cron.CronHourlyTasks;
import org.sevensoft.ecreator.model.attachments.AttachmentUtil;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.attributes.AttributeValue;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.categories.CategoryItem;
import org.sevensoft.ecreator.model.categories.db.CategoryAncestor;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.advanced.ItemHelper;
import org.sevensoft.ecreator.model.items.highlighted.blocks.HighlightedItemsBlock;
import org.sevensoft.ecreator.model.items.pricing.Price;
import org.sevensoft.ecreator.model.media.images.ImageUtil;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.installer.Installer;
import org.sevensoft.ecreator.model.system.upgraders.Upgrade2;
import org.sevensoft.ecreator.model.stats.visits.VisitorCounterBot;
import org.sevensoft.ecreator.model.marketing.newsletter.bots.NewsletterNewItems;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.SimpleQuery;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 01-Jun-2004 21:28:42
 */
@Path("admin-commands.do")
public class CommandHandler extends AdminHandler {

	private Upload	upload;
	private String	price;
	private String	to;
	private String	from;
	private String	sitelist;
	private String	server;
	private String	ip;

	public CommandHandler(RequestContext context) {
		super(context);
	}

	public Object availabilityToAttribute() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		new Upgrade2(context).availabilityToAttribute();

		addMessage("Converted availability to attribute");
		clearParameters();
		return main();
	}

	public Object categoriesToTitleCase() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		config.categoriesToTitleCase();

		addMessage("All categories converted to title case");
		return main();
	}

	public Object clearCostPrices() throws ServletException {

		config.clearCostPrices();

		addMessage("Cleared cost prices");
		return main();
	}

	public Object clearSessions() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		config.clearSessions();
		addMessage("sessions cleared");
		return main();
	}

	public Object createAccountsFromOrders() throws ServletException {

		config.createAccountsFromOrders();
		addMessage("createAccountsFromOrders");
		return main();
	}

	public Object deleteCategories() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		Query q = new Query(context, "delete from # where parent>0");
		q.setTable(Category.class);
		q.run();

		q = new Query(context, "delete from # where category>0");
		q.setTable(Attribute.class);
		q.run();

		q = new Query(context, "delete from # where category>0");
		q.setTable(AttributeValue.class);
		q.run();

		q = new Query(context, "delete from #");
		q.setTable(CategoryItem.class);
		q.run();

		q = new Query(context, "delete from #");
		q.setTable(CategoryAncestor.class);
		q.run();

		addMessage("Categories deleted");
		return main();
	}

	public Object deleteMultipleContentBlocks() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		config.deleteMultipleContentBlocks();

		addMessage("Multiple content blocks removed");
		clearParameters();
		return main();
	}

	public Object flushHtmlCaches() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		HighlightedItemsBlock.clearHtmlCache();
		HighlightedItemsBlock.clearHtmlCache();

		return new ActionDoc(context, "Html caches have been flushed", new Link(CommandHandler.class));
	}

	public Object imageRepair() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		ImageUtil.verify(context);

		addMessage("Images repaired");
		return main();
	}

	public Object initWeights() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		config.initWeights();
		addMessage("Weights done");
		return main();
	}

	public Object install() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		if (upload == null) {
			addError("No file uploaded");
			return main();
		}

		File file = upload.getFile();
		try {

			new Installer(context).install(file);
			addMessage("Installed from: " + upload.getFilename());

		} catch (JDOMException e) {
			addError(e);

		} catch (IOException e) {
			addError(e);
		}

		return main();
	}

	public Object itemRepair() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		Item.repair(context);
		addMessage("itemRepair");
		return main();
	}

	@Override
	public Object main() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Config", null);
		doc.addBody(new Body() {

			private void categories() {

				sb.append(new AdminTable("Categories"));

				sb.append(new AdminRow("Repair categories", new ButtonTag(CommandHandler.class, "repairCategories", "Repair categories")));

				sb.append(new AdminRow("Categories to title case", new ButtonTag(CommandHandler.class, "categoriesToTitleCase", "Submit")));

				sb.append(new AdminRow("Remove multiple content blocks", null, new ButtonTag(CommandHandler.class, "deleteMultipleContentBlocks",
						"Delete multiple content blocks")));

				sb.append(new AdminRow("Recreate subcategory blocks", null, new ButtonTag(CommandHandler.class, "recreateSubcategoryBlocks",
						"Recreate subcategory blocks")));

				sb.append("</table>");
			}

			private void cron() {

				sb.append(new AdminTable("Cron"));

				sb.append(new AdminRow("Run cron daily", "Manually run cron daily tasks. It will not work if 'Cron stop' module is on.", new ButtonTag(CommandHandler.class, "runCronDaily",
						"Run Cron Daily")));

				sb.append(new AdminRow("Run cron hourly", "Manually run cron hourly tasks. It will not work if 'Cron stop' module is on", new ButtonTag(CommandHandler.class, "runCronHourly",
						"Run Cron Hourly")));

                sb.append(new AdminRow("Run visitor processor", "Manually run visitor daily task.", new ButtonTag(CommandHandler.class, "runVisitorDaily",
                        "Run Cron for Visitors")));

                sb.append(new AdminRow("Run newsletter new items", "Manually run newsletters daily task.", new ButtonTag(CommandHandler.class, "runNewslettersDaily",
                        "Run Cron for Newsletters")));

                sb.append("</table>");
			}

			private void database() {

				sb.append(new AdminTable("Database"));

				sb.append(new AdminRow("Validate database", "Check the database schema is up to date.", new ButtonTag(CommandHandler.class,
						"validateDatabase", "Validate database")));

				sb.append(new AdminRow("Reset database", "Erase the entire database and start with clean site.", new ButtonTag(CommandHandler.class,
						"resetDatabase", "Reset database")
						.setConfirmation("You might want to make sure you're not a monkey before pressing this button")));

				sb.append("</table>");
			}

			private void items() {

				sb.append(new AdminTable("Item commands"));

				sb.append(new AdminRow("Repair items", "Repair item database", new ButtonTag(CommandHandler.class, "itemRepair", "itemRepair")));

				sb.append(new AdminRow("Item sell to cost", "Move sell prices to cost prices", new ButtonTag(CommandHandler.class, "moveSellToCost",
						"moveSellToCost")));

				sb.append(new AdminRow("Delete item sell prices", "Remove all sell prices set directly on items.", new ButtonTag(CommandHandler.class,
						"removeSellPrices", "removeSellPrices")));

				sb.append(new AdminRow("Clear cached cost prices", "Clear cached cost prices", new ButtonTag(CommandHandler.class, "clearCostPrices",
						"Clear cost prices")));

				sb.append(new AdminRow("Clear cached sell prices", "Clear cached sell prices", new ButtonTag(CommandHandler.class,
						"resetSellPriceCache", "resetSellPriceCache")));

				sb.append(new AdminRow("Remove duplicated content blocks", null, new ButtonTag(CommandHandler.class, "removeDuplicatedContentBlocks",
						"removeDuplicatedContentBlocks")));

                sb.append(new AdminRow("Item RRP to Price", "Copy RRP value to Price", new ButtonTag(CommandHandler.class, "copyRrpToPrice",
                        "copyRrpToPrice")));

				sb.append("</table>");
			}

			private void misc() {

				sb.append(new AdminTable("Misc"));

				sb.append(new AdminRow("Reset balances", "Reset all account balances", new ButtonTag(CommandHandler.class, "resetBalances",
						"Reset balances")));

				sb.append(new AdminRow("createAccountsFromOrders", "createAccountsFromOrders", new ButtonTag(CommandHandler.class,
						"createAccountsFromOrders", "createAccountsFromOrders")));

				sb.append(new FormTag(CommandHandler.class, "wordReplace", "POST"));
				sb.append(new AdminRow("Word replace", "Replace " + new TextTag(context, "from", 12) + " with " + new TextTag(context, "to", 12) + " " +
						new SubmitTag("Submit->timbus")));
				sb.append("</form>");

				sb.append(new FormTag(CommandHandler.class, "install", "multi"));
				sb.append(new AdminRow("Install site", "Browse to an install.xml file to setup this site from a saved setup.", new FileTag("upload") +
						" " + new SubmitTag("Install")));
				sb.append("</form>");

				sb.append(new AdminRow("Repair attachment counts", null, new ButtonTag(CommandHandler.class, "repairAttachmentCounts",
						"repairAttachmentCounts")));
                
                sb.append(new AdminRow("Repair images counts", null, new ButtonTag(CommandHandler.class, "imageRepair",
						"Repair images")));

				sb.append(new AdminRow("Flush HTML caches", new ButtonTag(CommandHandler.class, "flushHtmlCaches", "flushHtmlCaches")));

				sb.append("</table>");

			}

			@Override
			public String toString() {

				items();
				categories();
				database();
				cron();
				misc();

				return sb.toString();
			}

		});

		return doc;
	}

	public Object massPriceChange() {

		List<Category> categories = Category.getItemContainers(context);
		for (Category category : categories) {
			category.setSellPrice(null, 1, Amount.parse(price));
		}

		return "Prices changed";
	}

	public Object moveSellToCost() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		ItemHelper.moveSellToCost(context);
		addMessage("moveSellToCost");
		return main();

	}

    public Object copyRrpToPrice() throws ServletException {

        if (!isSuperman()) {
            return index();
        }

        ItemHelper.copyRrpToPrice(context);
        addMessage("copyRrpToPrice");
        return main();

    }

	public Object reapBaskets() throws ServletException {

		config.clearBaskets();
		addMessage("Baskets reaped");
		clearParameters();
		return main();
	}

	public Object recreateSubcategoryBlocks() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		int n = config.recreateSubcategoryBlocks();

		addMessage("Created " + n + " subcategory blocks");
		return main();
	}

	public Object removeDuplicatedContentBlocks() throws ServletException {

		config.removeDuplicatedContentBlocks();

		addMessage("removed duplicated Content Blocks");
		return main();
	}

	public Object removeSellPrices() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		ItemHelper.wipeItemSellPrices(context);
		addMessage("removeSellPrices");
		return main();
	}

	public Object repairAttachmentCounts() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		AttachmentUtil.repairCounts(context);
		addMessage("Attachment counts repaired");
		return main();
	}

	public Object repairCategories() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		Category.repair(context);
		addMessage("Categories repaired");

		clearParameters();
		return main();
	}

	public Object resetBalances() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		config.resetBalances();
		addMessage("All account balances reset");

		clearParameters();
		return main();
	}

	public Object resetDatabase() throws ServletException {

		// empty database, next access will detect no database and will re-install
		SimpleQuery.emptyDb(context);

		this.config = null;
		this.captions = null;
		this.modules = null;
		this.miscSettings = null;
		this.shoppingSettings = null;
		this.user = null;

		Config.getInstance(context).init(context);

		return new ActionDoc(context, "Database has been reset", new Link(CommandHandler.class));
	}

	public Object resetSellPriceCache() throws ServletException {

		Price.resetCachedSellPrices(context);

		addMessage("Cleared sell prices");
		return main();
	}

	public Object runCronDaily() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		new CronDailyTasks(context).run();
		return new ActionDoc(context, "Cron daily tasks have been run", new Link(CommandHandler.class));
	}

	public Object runCronHourly() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		new CronHourlyTasks(context).run();
		return new ActionDoc(context, "Cron hourly tasks have been run", new Link(CommandHandler.class));
	}

    public Object runVisitorDaily() throws ServletException {

        if (!isSuperman()) {
            return index();
        }

        new VisitorCounterBot(context).run();
        return new ActionDoc(context, "Visitor daily task has been run", new Link(CommandHandler.class));
    }

    public Object runNewslettersDaily() throws ServletException {

        if (!isSuperman()) {
            return index();
        }

        new NewsletterNewItems(context).run();
        return new ActionDoc(context, "Newsletters daily task has been run", new Link(CommandHandler.class));
    }

    public Object validateDatabase() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		context.getSchemaValidator().clear();

		addMessage("Schema reset");
		clearParameters();
		return main();
	}

}