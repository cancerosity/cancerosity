package org.sevensoft.ecreator.iface.admin.marketing.newsletter;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Menu;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.RequestContext;

import java.util.List;
import java.util.ArrayList;

/**
 * User: Tanya
 * Date: 16.07.2012
 */
public class NewslettersMenu extends Menu {

    private final Newsletter newsletter;

    public NewslettersMenu(Newsletter newsletter) {
        this.newsletter = newsletter;
    }

    public List<LinkTag> getLinkTags(RequestContext context) {
        List<LinkTag> links = new ArrayList<LinkTag>();

        if (Module.Newsletter.enabled(context)) {

            links.add(new LinkTag(NewsletterComposeHandler.class, null, "Compose newsletter"));
            links.add(new LinkTag(NewsletterSettingsHandler.class, null, "Newsletter settings"));
            if (Module.NewsletterNewItems.enabled(context))
                links.add(new LinkTag(NewsletterOptionsHandler.class, null, "Newsletter options", "newsletter", newsletter));
        }

        return links;
    }
}
