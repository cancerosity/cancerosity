package org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics;

import org.sevensoft.commons.gaia.Country;
import org.sevensoft.jeezy.http.html.ImageTag;

/**
 * @author sks 14 Apr 2007 08:50:16
 *
 */
public class FlagGif {

	private final Country	country;

	public FlagGif(Country country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return new ImageTag("files/graphics/flags/" + country.getIsoAlpha2().toLowerCase() + ".gif").toString();
	}
}
