package org.sevensoft.ecreator.iface.admin.comments;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.categories.CategoryHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.TickGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.CrossGif;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.comments.Comment;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;

import javax.servlet.ServletException;
import java.util.List;

/**
 * User: Tanya
 * Date: 27.07.2010
 */
@Path("admin-comments-moderate.do")
public class CommentModerationHandler extends AdminHandler {

    private Comment comment;

    public CommentModerationHandler(RequestContext context) {
        super(context);
    }

    public Object main() throws ServletException {
        final List<Comment> commentList = Comment.get(context, true);
        AdminDoc page = new AdminDoc(context, user, "Moderation queue", null);
        page.addBody(new Body() {
            private void commands() {
                sb.append("<div align='center' class='actions'>");
                sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
                sb.append("</div>");
            }

            private void list() {
                sb.append(new AdminTable("Comments awaiting moderation"));

                sb.append("<tr>");
                sb.append("<th width='20'>Id</th>");
                sb.append("<th width='35'>Date</th>");
                sb.append("<th width='80'>Item/Category</th>");
//                sb.append("<th>Title</th>");
                sb.append("<th>Comment</th>");
                sb.append("<th width='20'>Approve</th>");
                sb.append("</tr>");

                for (Comment comment : commentList) {

                    sb.append("<tr>");
                    if (comment.getItem() != null) {

                        Item it = comment.getItem();

                        sb.append("<td>" + new LinkTag(CommentHandler.class, "edit", comment.getIdString(), "comment", comment, "item", it) + "</td>");
                        sb.append("<td>" + comment.getDateCreated().toDateString() + "</td>");
                        sb.append("<td>" + new LinkTag(ItemHandler.class, "edit", comment.getParentName(), "item", it) + "</td>");

                    } else if (comment.getCategory() != null) {

                        Category cat = comment.getCategory();

                        sb.append("<td>" + new LinkTag(CommentHandler.class, "edit", comment.getIdString(), "comment", comment, "category", cat) + "</td>");
                        sb.append("<td>" + comment.getDateCreated().toDateString() + "</td>");
                        sb.append("<td>" + new LinkTag(CategoryHandler.class, "edit", comment.getParentName(), "category", cat) + "</td>");

                    }

//                    sb.append("<td>" + comment.getTitle() + "</td>");
                    sb.append("<td>" + comment.getComment() + "</td>");

                    // approval link
                    sb.append("<td>");

                    sb.append(new LinkTag(CommentModerationHandler.class, "approve", new TickGif(), "comment", comment)
                            .setConfirmation("Are you sure you want to approve this comment"));

                    sb.append(" ");

                    sb.append(new LinkTag(CommentModerationHandler.class, "reject", new CrossGif(), "comment", comment)
                            .setConfirmation("Are you sure you want to reject this comment"));
                    sb.append("</td>");

                    sb.append("</tr>");

                }

                sb.append("</table>");
            }

            @Override
            public String toString() {

                commands();
                list();
                commands();

                return sb.toString();
            }
        });

        return page;
    }

    public Object approve() throws ServletException {

        if (comment == null) {
            return main();
        }

        try {
            comment.approved();
        } catch (EmailAddressException e) {
            addError(e);
        } catch (SmtpServerException e) {
            addError(e);
        }

        return main();
    }

    public Object reject() throws ServletException {

        if (comment == null) {
            return main();
        }

        try {
            comment.rejected();
        } catch (EmailAddressException e) {
            addError(e);
        } catch (SmtpServerException e) {
            addError(e);
        }

        return main();
    }
}
