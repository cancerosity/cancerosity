package org.sevensoft.ecreator.iface.admin.attachments;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentExistsException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentLimitException;
import org.sevensoft.ecreator.model.attachments.exceptions.AttachmentTypeException;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;

/**
 * @author sks 8 Sep 2006 15:59:09
 *
 */
@Path("admin-attachments-uploader.do")
public class AttachmentUploaderHandler extends AdminHandler {

	private Category		category;
	private List<Upload>	uploads;
	private ItemType		itemType;
	private boolean		previews;

	public AttachmentUploaderHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc page = new AdminDoc(context, user, "Attachment settings", null);
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Upload files"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Category", "Choose a category to put the newly created items in.", new SelectTag(context, "category", null,
						Category.getCategoryOptions(context), "-Choose category-")));

				if (Module.AttachmentPreviews.enabled(context)) {
					sb.append(new AdminRow("Previews", "Tick this box if you want previews to be automatically created.", new CheckTag(context,
							"previews", true, true)));
				}

				List<ItemType> itemTypes = ItemType.getAttachmentItemTypes(context);
				if (itemTypes.size() == 1) {

					sb.append(new HiddenTag("itemType", itemTypes.get(0)));

				} else {

					sb
							.append(new AdminRow("Item type", "Set the item type for the items.", new SelectTag(context, "itemType", ItemType
									.getSelectionMap(context))));

				}

				for (int n = 1; n < 21; n++) {
					sb.append("<tr><th width='300'><b>Upload file " + n + "</b><br/></th><td>" + new FileTag("uploads") + "</td></tr>");
				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(AttachmentUploaderHandler.class, "upload", "multi"));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return page;
	}

	public Object upload() throws ServletException {

		if (itemType == null) {
			addError("Item type must be set");
			return main();
		}

		int n = 0;
		for (Upload upload : uploads) {

			/*
			 * create new item based on upload filename
			 */

			// STRIP EXTENSION
			String name = SimpleFile.removeFileExtension(upload.getFilename());

			// CONVERT underscore to space
			name = name.replace("_", " ");

			// replace multple spaces
			name = name.replaceAll("\\s{2,}", " ");

			Item item = new Item(context, itemType, name, "Live", null);
			if (category != null) {
				item.addCategory(category);
			}

			n++;

			try {

				item.addAttachment(upload, previews);

			} catch (IOException e) {
				e.printStackTrace();

			} catch (AttachmentLimitException e) {
				e.printStackTrace();

			} catch (AttachmentExistsException e) {
				e.printStackTrace();

			} catch (AttachmentTypeException e) {
				e.printStackTrace();
			}
		}

		addMessage(n + " items created");
		clearParameters();
		return main();
	}
}
