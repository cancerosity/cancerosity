package org.sevensoft.ecreator.iface.admin.ecom.bookings.config;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.bookings.BookingOption;
import org.sevensoft.ecreator.model.bookings.BookingOption.Type;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sam
 */
@Path("admin-bookings-options.do")
public class BookingOptionsHandler extends AdminHandler {

	private BookingOption	option;
	private Item		item;
	private Type		type;
	private Money		chargeEx;
	private String		name;

	public BookingOptionsHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		if (item == null) {
			return main();
		}

		item.getBookingSetup().addOption("new booking option");

		return new ActionDoc(context, "A new booking option has been created", new Link(BookingSetupHandler.class, null, "item", item));
	}

	public Object delete() throws ServletException {

		if (option == null) {
			return main();
		}

		item = option.getItem();
		item.getBookingSetup().removeOption(option);

		return new ActionDoc(context, "This booking option has been deleted", new Link(BookingSetupHandler.class, null, "item", item));
	}

	@Override
	public Object main() throws ServletException {

		if (option == null) {
			return index();
		}

		item = option.getItem();

		AdminDoc doc = new AdminDoc(context, user, "Edit option", Tab.getItemTab(item));
		doc.setMenu(new EditItemMenu(item));
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update option"));
				sb.append(new ButtonTag(BookingSetupHandler.class, null, "Return to options", "item", item));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Name", "The name of this option.", new TextTag(context, "name", option.getName(), 20)));

				SelectTag typeTag = new SelectTag(context, "type", option.getType());
				typeTag.addOptions(BookingOption.Type.values());

				sb.append(new AdminRow("Type", "The type of this option.", typeTag));

				sb.append(new AdminRow("Charge", "The charge to the customer for this option.", new TextTag(context, "chargeEx", option.getChargeEx(),
						12)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(BookingOptionsHandler.class, "save", "POST"));
				sb.append(new HiddenTag("option", option));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object moveDown() throws ServletException {

		if (option == null) {
			return main();
		}

		item = option.getItem();
		List<BookingOption> options = item.getBookingSetup().getOptions();
		EntityUtil.moveDown(option, options);

		return new BookingSetupHandler(context, item).main();
	}

	public Object moveUp() throws ServletException {

		if (option == null) {
			return main();
		}

		item = option.getItem();
		List<BookingOption> options = item.getBookingSetup().getOptions();
		EntityUtil.moveUp(option, options);

		return new BookingSetupHandler(context, item).main();
	}

	public Object save() throws ServletException {

		if (option == null) {
			return main();
		}

		test(new RequiredValidator(), "name");
		test(new RequiredValidator(), "type");

		if (hasErrors()) {
			return main();
		}

		option.setName(name);
		option.setChargeEx(chargeEx);
		option.setType(type);
		option.save();

		clearParameters();

		return new ActionDoc(context, "The booking option has been updated", new Link(BookingOptionsHandler.class, null, "option", option));
	}
}
