package org.sevensoft.ecreator.iface.admin.extras.shoppingbox;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.extras.shoppingbox.ShoppingBoxSettings;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 17-Jun-2005 17:37:55
 * 
 */
@Path("admin-settings-shoppingbox.do")
public class ShoppingBoxSettingsHandler extends AdminHandler {

	private transient ShoppingBoxSettings	shoppingBoxSettings;
	private Money					seaCostPerUnit;
	private Money					airCostPerUnit;

	public ShoppingBoxSettingsHandler(RequestContext context) {
		super(context);
		this.shoppingBoxSettings = ShoppingBoxSettings.getInstance(context);
	}

	@Override
	public Object main() {

		AdminDoc doc = new AdminDoc(context, user, "Shopping Box", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update shopping box settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General settings"));

				sb.append(new AdminRow("Sea cost", "Enter the cost per weight unit for sea shipping.", new TextTag(context, "seaCostPerUnit",
						shoppingBoxSettings.getSeaCostPerUnit(), 12)));

				sb.append(new AdminRow("Air cost", "Enter the cost per weight unit for air shipping.", new TextTag(context, "airCostPerUnit",
						shoppingBoxSettings.getAirCostPerUnit(), 12)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(ShoppingBoxSettingsHandler.class, "save", "POST"));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() {

		shoppingBoxSettings.setAirCostPerUnit(airCostPerUnit);
		shoppingBoxSettings.setSeaCostPerUnit(seaCostPerUnit);
		shoppingBoxSettings.save();

		return new ActionDoc(context, "Shopping box settings have been updated", new Link(ShoppingBoxSettingsHandler.class));
	}

}
