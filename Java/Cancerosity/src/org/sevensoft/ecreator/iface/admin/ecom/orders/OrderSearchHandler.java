package org.sevensoft.ecreator.iface.admin.ecom.orders;

import java.util.List;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.ecom.ShoppingMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.ecom.orders.Order;
import org.sevensoft.ecreator.model.ecom.orders.OrderSearcher;
import org.sevensoft.ecreator.model.ecom.orders.OrderSettings;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.EmailTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 13-Jul-2005 16:24:50
 *
 */
@Path("admin-orders-overview.do")
public class OrderSearchHandler extends AdminHandler {

    private List<Order> selectedOrders;
    private String name, email, postcode, address;
    private Money totalTo;
    private Money totalFrom;
    private transient List<Order> orders;
    private String exportStatus;
    private User salesperson;
    private boolean webOrders;
    private String orderNumber;
    private Order.Sort sort;
    private String status;
    private transient OrderSettings orderSettings;
    private boolean recurring;
    private transient Results results;
    private int page;
    private OrderSearcher searcher;
    private static OrderSearchHandler instance;

    public OrderSearchHandler(RequestContext context) {
        super(context);
        orderSettings = OrderSettings.getInstance(context);
        if (instance == null) {
            instance = this;
        } else {
            if (context.getParameter("_a") != null && context.getParameter("_a").equals("search")) {
                this.sort = instance.sort;
                this.status = instance.status;
                this.salesperson = instance.salesperson;
                this.orderNumber = instance.orderNumber;
                this.name = instance.name;
                this.email = instance.email;
                this.webOrders = instance.webOrders;
                this.recurring = instance.recurring;
            }
        }
    }

    @Override
    public Object main() {

        if (orders == null) {

            searcher = new OrderSearcher(context);
            if (sort == null) {
                sort = Order.Sort.Newest;
                searcher.setSort(sort);
            } else {
                searcher.setSort(sort);
            }
            results = new Results((Integer)searcher.execute(true), page, 50);
            searcher.setLimit(50);
            searcher.setStart(results.getStartIndex());

            orders = searcher.execute();
        }

        AdminDoc doc = new AdminDoc(context, user, "Orders overview", Tab.Orders);
        doc.setMenu(new ShoppingMenu());
        doc.setIntro("Search for orders using the search form below");

        doc.addBody(new Body() {

            private void results() {

                if (orders == null) {
                    return;
                }

                sb.append(new ResultsControl(context, results, new Link(OrderSearchHandler.class, "search")));
                sb.append(new FormTag(OrderSearchHandler.class, "export", "post"));
                sb.append(new AdminTable("Viewing orders"));

                sb.append("<tr>");
                sb.append("<th>Order No</th>");
                sb.append("<th>Date placed</th>");
                sb.append("<th>Customer details</th>");
                sb.append("<th>Status</th>");
                sb.append("<th>Total</th>");
                sb.append("</tr>");

                int n = 0;
                for (Order order : orders) {

                    sb.append("<tr>");
                    sb.append("<td>" + new LinkTag(OrderHandler.class, null, order.getIdString(), "order", order) + "</td>");
                    sb.append("<td>" + order.getDatePlaced().toString("dd-MMM-yy") + "</td>");
                    sb.append("<td>" + order.getAccount().getName());
                    if (order.getAccount().hasEmail()) {
                        sb.append("<br/>");
                        sb.append(new EmailTag(order.getAccount().getEmail()));
                    }
                    sb.append("</td>");
                    sb.append("<td>" + order.getStatus() + "</td>");
                    sb.append("<td>" + order.getTotalInc() + "</td>");
                    sb.append("</tr>");

                    n++;
                }

                sb.append("</table>");
                sb.append("</form>");
            }

            private void search() {

                sb.append(new AdminTable("Order search"));
                sb.append(new FormTag(OrderSearchHandler.class, "find", "POST"));

                SelectTag statusTag = new SelectTag(context, "status", status);
                statusTag.setAny("-Any status-");
                statusTag.addOptions(orderSettings.getStatuses());

                SelectTag sortTag = new SelectTag(context, "sort", sort);
                sortTag.addOptions(Order.Sort.values());

                sb.append(new AdminRow("Status", "Set the status of orders to search for ", statusTag));
                sb.append(new AdminRow("Order No", "Search for a specific order id", new TextTag(context, "orderNumber", orderNumber, 12)));
                sb.append(new AdminRow("Name", "Search by customer name", new TextTag(context, "name", name, 40)));
                sb.append(new AdminRow("Email", "Search by customer email address", new TextTag(context, "email", email, 40)));

                if (orderSettings.isSalesPersons()) {

                    SelectTag salespersonTag = new SelectTag(context, "salesperson", salesperson);
                    salespersonTag.setAny("-Any salesperson-");
                    salespersonTag.addOptions(orderSettings.getSalesPersons());

                    sb.append(new AdminRow("Salesperson", salespersonTag));
                    sb.append(new AdminRow("Only web orders", new CheckTag(context, "webOrders", true, webOrders)));

                }

                if (orderSettings.isRecurringOrders()) {
                    sb.append(new AdminRow("Recurring orders", "Only show orders that are set to be reinvoiced automatically.", new BooleanRadioTag(
                            context, "recurring", false)));
                }

                sb.append(new AdminRow("Sort by", sortTag));
                sb.append("<tr><td colspan='2'>" + new SubmitTag("Search") + "</td></tr>");

                sb.append("</form>");
                sb.append("</table>");

            }

            @Override
            public String toString() {

                search();
                results();

                return sb.toString();
            }

        });

        return doc;
    }

    public Object search() {

        searcher = new OrderSearcher(context);

        if (sort == null) {
            sort = Order.Sort.Newest;
            searcher.setSort(sort);
        } else {
            searcher.setSort(sort);
        }
        searcher.setWebOrders(webOrders);
        searcher.setSalesperson(salesperson);
        searcher.setPostcode(postcode);
        searcher.setName(name);
        searcher.setEmail(email);
        searcher.setAddress(address);
        searcher.setTotalFrom(totalFrom);
        searcher.setTotalTo(totalTo);
        searcher.setRecurring(recurring);
        searcher.setCancelled(true);
        searcher.setOrderId(orderNumber);
        searcher.setStatus(status);
        results = new Results((Integer)searcher.execute(true), page, 50);        //todo
        searcher.setLimit(50);
        searcher.setStart(results.getStartIndex());
        orders = searcher.execute();

        return main();
    }

    public Object find() {
        instance.sort = sort;
        instance.status = status;
        instance.salesperson = salesperson;
        instance.orderNumber = orderNumber;
        instance.name = name;
        instance.email = email;
        instance.webOrders = webOrders;
        instance.recurring = recurring;
        return search();
    }
}
