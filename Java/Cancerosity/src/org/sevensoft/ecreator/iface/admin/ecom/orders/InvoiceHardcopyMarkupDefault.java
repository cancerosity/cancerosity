package org.sevensoft.ecreator.iface.admin.ecom.orders;

import org.sevensoft.ecreator.model.design.markup.MarkupDefault;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * User: Tanya
 * Date: 11.02.2011
 */
public class InvoiceHardcopyMarkupDefault implements MarkupDefault {

    public String getBetween() {
        return null;
    }

    public String getBody() {
        StringBuilder sb = new StringBuilder();

        sb.append("[invoice_header]");

        sb.append("<div class='ref'>INVOICE ");
        sb.append("[order_id]");
        sb.append("</div>");

        sb.append("<br/>\n");

        sb.append(new TableTag("addresses"));
        sb.append("<tr>");
        sb.append("<td valign='top' width='50%'><b>Billing address:</b><br/>");

        sb.append("[invoice_billing_label]");

        sb.append("</td>");
        sb.append("<td valign='top' width='50%'><b>Delivery address:</b><br/>");

        sb.append("[invoice_delivery_label]");

        sb.append("</td>");
        sb.append("</tr>");
        sb.append("</table>");

        sb.append("<br/>\n");

        sb.append(new TableTag("details"));
        sb.append("<tr>");
        sb.append("<td><b>Order / Invoice Number:</b> [id_order]</td>\n");
        sb.append("<td><b>Date:</b> [order_date_placed]</td>");
        sb.append("</tr>\n");

        sb.append("<tr>");
        sb.append("<td><b>Account No:</b> [order_account_id]</td>\n");
        sb.append("<td><b>Customer:</b> [order_account_name]</td>");
        sb.append("</tr>\n");

        sb.append("<tr>");
        sb.append("<td><b>Your reference:</b> [order_customer_ref]</td>");
        sb.append("<td></td>");
        sb.append("</tr>");

        sb.append("</table>\n");

        /*
           * get attributes defined as invoice columns
           */
        sb.append("[order_lines]\n");

        /*
           * Totals
           */
        sb.append(new TableTag("totals"));

        sb.append("<tr>");
        sb.append("<td align='right'><b>Voucher discount:</b></td>\n");
        sb.append("<td align='right' width='80'>\243[order_discount?abs=1]</td>");
        sb.append("</tr>\n");

        sb.append("<tr>");
        sb.append("<td align='right'><b>Subtotal:</b></td>\n");
        sb.append("<td align='right' width='80'>\243[order_total]</td>");
        sb.append("</tr>\n");

        sb.append("<tr>");
        sb.append("<td align='right'><b>Vat:</b></td>\n");
        sb.append("<td align='right' width='80'>\243[order_total?vat=1]</td>");
        sb.append("</tr>\n");

        sb.append("<tr>");
        sb.append("<td align='right'><b>Total:</b></td>\n");
        sb.append("<td align='right' width='80'>\243[order_total?inc=1]</td>");
        sb.append("</tr>\n");

        sb.append("</table>\n");

        sb.append("[invoice_footer]\n");

        return sb.toString();
    }

    public String getEnd() {
        return null;
    }

    public String getStart() {
        StringBuilder sb = new StringBuilder();
        sb.append("<style>\n");
        sb.append("* { font-family: Arial; margin: 0; padding: 0; font-size: 11px; }\n");
        sb.append("body { margin: 10; }\n");
        sb.append("table.company { width: 600px; margin-bottom: 15; }\n");

        sb.append("div.ref { text-align: right; width: 600px; font-size: 16px; font-weight: bold; }\n");

        sb.append("table.addresses { width: 600px; margin-top: 15px;	}\n ");
        sb.append("table.addresses td { width: 50%; }\n\n ");

        sb.append("table.details { width: 600px; border: 1px solid black; border-collapse: collapse; margin-bottom: 15;}\n ");
        sb.append("table.details td {border: 1px solid black; padding: 4; width: 50%;}\n\n ");

        sb.append("table.items { border: 1px solid black; width: 600px; border-collapse: collapse; margin-bottom: 15px; }\n");
        sb.append("table.items th { text-align: left; font-weight: bold; font-size: 12px; border: 1px solid black; padding: 4px; }\n");
        sb.append("table.items td { padding: 4px; }\n");

        sb.append("table.totals { width: 600px; margin-bottom: 15; }\n ");
        sb.append("table.totals td { padding: 4px; }\n ");

        sb.append("</style>\n");

        sb.append("<title>Order ");
        sb.append("[id_order]");
        sb.append("</title>");
        return sb.toString();
    }

    public int getTds() {
        return 0;
    }

    public String getCss() {
        return null;
    }

    public String getName() {
        return "Invoice Markup Default";
    }
}
