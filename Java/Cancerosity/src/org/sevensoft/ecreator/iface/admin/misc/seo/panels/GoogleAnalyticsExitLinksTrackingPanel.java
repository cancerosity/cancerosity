package org.sevensoft.ecreator.iface.admin.misc.seo.panels;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.misc.seo.GoogleSettings;

/**
 * User: Tanya
 * Date: 24.05.2011
 */
public class GoogleAnalyticsExitLinksTrackingPanel {

    private RequestContext context;
    private transient GoogleSettings googleSettings;

    public GoogleAnalyticsExitLinksTrackingPanel(RequestContext context) {
        this.context = context;
        this.googleSettings = GoogleSettings.getInstance(context);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        if (Module.GATrackingOutboundLinks.enabled(context)) {

            sb.append("<script type='text/javascript'>\n");
            sb.append("function recordOutboundLink(link, category, action) {\n");
            sb.append("  try {\n");
            sb.append("    var pageTracker=_gat._getTracker('" + googleSettings.getAccount() + "');\n");
            sb.append("    pageTracker._trackEvent(category, action);\n");
            sb.append("    if (link.target == '_blank') {\n");
            sb.append("         window.open(link.href);\n");
            sb.append("    } else {\n");
            sb.append("         setTimeout('document.location = \"' + link.href + '\"', 100);\n   }\n");
            sb.append("  }catch(err){}\n");
            sb.append("}\n");
            sb.append("</script>");

        }
        return sb.toString();
    }
}
