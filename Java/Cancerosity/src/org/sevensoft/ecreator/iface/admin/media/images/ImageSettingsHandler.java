package org.sevensoft.ecreator.iface.admin.media.images;

import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.ImageSettings;
import org.sevensoft.ecreator.model.media.images.ImageUtil;
import org.sevensoft.ecreator.model.media.images.Img;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

import javax.servlet.ServletException;
import java.io.File;
import java.io.IOException;

/**
 * @author sks 25 Oct 2006 09:23:22
 */
@Path("admin-images-settings.do")
public class ImageSettingsHandler extends AdminHandler {

    private boolean thumbnailAutoCrop;
    private boolean thumbnailPadding;
    private int thumbnailWidth;
    private int thumbnailHeight;
    private int maxImageHeight;
    private int maxImageWidth;
    private Upload upload;
    private Category category;
    private ItemType itemType;
    private final ImageSettings imageSettings;
    private String rejectedEmailBody;
    private String approvedEmailText;
    private boolean sendApprovalEmails;
    private String approvedEmailBody;
    private String pendingContent;
    private String formatsNote;

    public ImageSettingsHandler(RequestContext context) {
        super(context);
        this.imageSettings = ImageSettings.getInstance(context);
    }

    public Object clean() throws ServletException {

        if (!isSuperman()) {
            return main();
        }

        int n = ImageUtil.clean(context);
        addMessage(n + " images have been deleted");
        return main();
    }

    public Object stripWhitespace() throws ServletException {

        if (!isSuperman()) {
            return main();
        }

        ImageUtil.stripWhitespace(context);
        addMessage("Whitespace removed from filenames");
        return main();
    }

    public Object resetImageCounts() throws ServletException {

        if (!isSuperman()) {
            return main();
        }

        ImageUtil.resetImageCounts(context);
        return new ActionDoc(context, "Image counts have been reset", new Link(ImageSettingsHandler.class));
    }

    public Object flatten() throws ServletException {

        try {

//            File imagesDir = context.getRealFile(Config.ImagesPath);
            File imagesDir = ResourcesUtils.getRealImagesDir();
            SimpleFile.flatten(imagesDir);
            addMessage("Images flattened. They're a lot thinner now");

        } catch (IOException e) {
            e.printStackTrace();
            addError(e);
        }

        return main();
    }

    @Override
    public Object main() throws ServletException {

        AdminDoc doc = new AdminDoc(context, user, "Image control", null);
        doc.addBody(new Body() {

            private void advanced() {

                sb.append(new AdminTable("Image / Thumbnail rendering"));

                sb.append(new AdminRow("Max image size", "Set the maximum image dimensions for upgraded fullsize images", new TextTag(context,
                        "maxImageWidth", config.getMaxImageWidth(), 4) +
                        " x " + new TextTag(null, "maxImageHeight", config.getMaxImageHeight(), 4)));

                sb.append(new AdminRow("Thumbnail dimensions", "Sets the generated size of a thumbnail", new TextTag(context, "thumbnailWidth", config
                        .getThumbnailWidth(), 4) +
                        " x " + new TextTag(null, "thumbnailHeight", config.getThumbnailHeight(), 4)));

                sb.append(new AdminRow("Thumbnail padding", new BooleanRadioTag(context, "thumbnailPadding", config.isThumbnailPadding())));

                sb.append(new AdminRow("Thumbnail auto crop", "Automatically remove white space from around an image when making a thumbnail",
                        new BooleanRadioTag(context, "thumbnailAutoCrop", config.isThumbnailAutoCrop())));

                sb.append(new AdminRow("Upload image note", "", new TextAreaTag(context, "formatsNote", imageSettings.getFormatsNote(),70, 7)));
                if (isSuperman()) {

                    sb.append(new AdminRow(true, "Reset image counts",
                            "Reset all the image counts on items, galleries etc, if they have become corrupt.", new ButtonTag(
                            ImageSettingsHandler.class, "resetImageCounts", "Reset image counts")
                            .setConfirmation("Confirm - long operation")));

                    sb.append(new AdminRow(true, "Resize images", "Resize any images over the max image dimensions", new ButtonTag(
                            ImageSettingsHandler.class, "resizeImages", "Resize images")
                            .setConfirmation("This may take some time to run - are you sure?")));

                    sb.append(new AdminRow(true, "Regenerate thumbnails", "Resize all the thumbnails using the set thumbnail dimensions",
                            new ButtonTag(ImageSettingsHandler.class, "regenerateThumbnails", "Regenerate thumbnails")));

                }

                sb.append("</table>");

            }

            private void approvals() {

                sb.append(new AdminTable("Approvals"));

                sb.append(new AdminRow("Send approval emails", "When vetting user images, send an email if the image is approved or unapproved.",
                        new BooleanRadioTag(context, "sendApprovalEmails", imageSettings.isSendApprovalEmails())));

                if (imageSettings.isSendApprovalEmails()) {

                    sb.append(new AdminRow("Approved email body", "The body content of the email that is sent when an image is approved.",
                            new TextAreaTag(context, "approvedEmailBody", imageSettings.getApprovedEmailBody(), 60, 5)));

                    sb.append(new AdminRow("Rejected email body", "The body content of the email that is sent when an image is unapproved.",
                            new TextAreaTag(context, "rejectedEmailBody", imageSettings.getRejectedEmailBody(), 60, 5)));
                }

                sb.append("<tr><td colspan='2'><b>Pending page content</b><br/>"
                        + "Content that appears after an image has been uploaded and is waiting for approval.</td></tr>");
                sb.append("<tr><td colspan='2'>" +
                        new TextAreaTag(context, "pendingContent", imageSettings.getPendingContent()).setStyle("width: 100%; height: 320px;")
                                .setId("pendingContent") + "</td></tr>");

                sb.append("</table>");
            }

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update"));
                sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
                sb.append("</div>");
            }

            private void options() {

                sb.append(new AdminTable("Image options"));

                if (isSuperman()) {

                    sb
                            .append(new AdminRow("Remove multiple images", "Remove all images except first one, for all items.", new ButtonTag(
                                    ImageSettingsHandler.class, "removeMultipleImages", "removeMultipleImages")
                                    .setConfirmation("Remove multiple images")));

//					sb.append(new AdminRow("Assign random images", "Set each item a random image from uploaded zip", new FileTag("upload") + "<br/>" +
////							new SelectTag(context, "itemType", null, ItemType.get(context), "-All item types-") + " " +
//							new SelectTag(context, "category", null, Category.getCategoryOptions(context), "-All categories-") + " " +
//							new SubmitTag("Assign random images")));

                }

                sb.append(new AdminRow("Clean image folder.", "Remove all unused images from the image folder.", new ButtonTag(
                        ImageSettingsHandler.class, "clean", "Clean images").setConfirmation("This cannot be undone - continue?")));

                if (isSuperman()) {

                    sb.append(new AdminRow(true, "Strip whitespace", "This will convert all spaces in image filenames to underscores.",
                            new ButtonTag(ImageSettingsHandler.class, "stripWhitespace", "Strip white")
                                    .setConfirmation("This cannot be undone - continue?")));

                    sb.append(new AdminRow(true, "Flatten image structure",
                            "This will move all images in subdirectories of /images into /images itself.", new ButtonTag(
                            ImageSettingsHandler.class, "flatten", "Flatten").setConfirmation("This cannot be undone - continue?")));

                }

                sb.append("</table>");

            }

            @Override
            public String toString() {

                RendererUtil.tinyMce(context, sb, "pendingContent");

                sb.append(new FormTag(ImageSettingsHandler.class, "save", "post"));

                commands();
                options();
                advanced();
                approvals();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    /**
     * Assign random images from a zip to all items without images
     *
     * @throws ServletException
     */
    public Object randomImages() throws ServletException {

        logger.fine("[ImageSettingsHandler] random images, upload=" + upload);

        if (upload == null)
            return main();

        try {

            int n = ImageUtil.randomImages(upload, context, itemType, category);
            addMessage("Images assigned to " + n + " items");

        } catch (IOException e) {
            addError(e);

        } catch (ImageLimitException e) {
            addError(e);
        }
        return main();
    }

    public Object regenerateThumbnails() throws ServletException {

        if (!isSuperman()) {
            return index();
        }

        logger.fine("[ImageSettingsHandler] Regenerated thumbnails");

        Img.regenerateThumbnails(context);
        addMessage("Regenerated thumbnails");
        clearParameters();
        return main();
    }

    public Object removeMultipleImages() throws ServletException {

        if (!isSuperman()) {
            return index();
        }

        Img.removeMultipleImages(context);
        addMessage("Removed multiple images");

        clearParameters();
        return main();
    }

    public Object resizeImages() throws ServletException {

        if (!isSuperman()) {
            return index();
        }

        Img.regenerateImages(context);
        addMessage("Resized images");
        return main();
    }

    public Object save() throws ServletException {

        config.setThumbnailHeight(thumbnailHeight);
        config.setThumbnailWidth(thumbnailWidth);
        config.setMaxImageHeight(maxImageHeight);
        config.setMaxImageWidth(maxImageWidth);
        config.setThumbnailPadding(thumbnailPadding);
        config.setThumbnailAutoCrop(thumbnailAutoCrop);
        config.save();
        imageSettings.setFormatsNote(formatsNote);
        
        imageSettings.setApprovedEmailBody(approvedEmailBody);
        imageSettings.setPendingContent(pendingContent);
        imageSettings.setRejectedEmailBody(rejectedEmailBody);
        imageSettings.setSendApprovalEmails(sendApprovalEmails);
        imageSettings.save();

        addMessage("Image settings updated");
        clearParameters();

        return main();
    }

}
