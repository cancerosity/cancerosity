package org.sevensoft.ecreator.iface.admin.extras;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.crm.forms.FormHandler;
import org.sevensoft.ecreator.iface.admin.crm.forms.SubmissionHandler;
import org.sevensoft.ecreator.iface.admin.extras.newsfeed.NewsfeedsHandler;
import org.sevensoft.ecreator.iface.admin.extras.polls.PollHandler;
import org.sevensoft.ecreator.iface.admin.extras.rss.RssHandler;
import org.sevensoft.ecreator.iface.admin.media.galleries.GalleryHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.crm.forms.Form;
import org.sevensoft.ecreator.model.extras.polls.Poll;
import org.sevensoft.ecreator.model.media.images.galleries.Gallery;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 24 Jul 2006 11:46:40
 *
 */
@Path("admin-extras.do")
public final class ExtrasHandler extends AdminHandler {

	public ExtrasHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Extras", Tab.Extras);
		if (miscSettings.isAdvancedMode()) {
			doc.setMenu(new ExtrasMenu());
		}
		doc.addBody(new Body() {

			@Override
			public String toString() {

				StringBuilder sb = new StringBuilder();

				sb.append(new TableTag("simplelinks", "center"));
				sb.append("<tr><th align='center'>What would you like to do?</th></tr>");

				if (Module.Forms.enabled(context)) {

					sb.append("<tr><td align='center'>" + new LinkTag(FormHandler.class, "create", "I want to create a new form") + "</td></tr>");

					if (Form.get(context).size() > 0) {
						sb.append("<tr><td align='center'>" + new LinkTag(FormHandler.class, "list", "I want to edit an existing form")
								+ "</td></tr>");
					}

					sb.append("<tr><td align='center'>" + new LinkTag(SubmissionHandler.class, null, "I want to view or print completed forms")
							+ "</td></tr>");

					sb.append("<tr><td class='spacer'></td></tr>");

				}

				if (Module.Newsfeed.enabled(context)) {

					sb.append("<tr><td align='center'>" + new LinkTag(NewsfeedsHandler.class, null, "I want to import a newsfeed") + "</td></tr>");

					sb.append("<tr><td class='spacer'></td></tr>");
				}

				if (Module.Polls.enabled(context)) {

					sb.append("<tr><td align='center'>" + new LinkTag(PollHandler.class, "create", "I want to add a poll to my site") + "</td></tr>");

					if (Poll.get(context).size() > 0) {
						sb.append("<tr><td align='center'>" + new LinkTag(PollHandler.class, "list", "I want to edit an existing poll")
								+ "</td></tr>");
					}

					sb.append("<tr><td class='spacer'></td></tr>");
				}

				if (Module.RssExport.enabled(context)) {

					sb.append("<tr><td align='center'>" + new LinkTag(RssHandler.class, "create", "I want to edit RSS publishing settings")
							+ "</td></tr>");

					sb.append("<tr><td class='spacer'></td></tr>");
				}

				if (Module.Galleries.enabled(context)) {

					sb.append("<tr><td align='center'>" + new LinkTag(GalleryHandler.class, "create", "I want to add a photo gallery to my site")
							+ "</td></tr>");

					if (Gallery.get(context).size() > 0) {

						sb.append("<tr><td align='center'>" + new LinkTag(GalleryHandler.class, "list", "I want to edit an existing photo gallery")
								+ "</td></tr>");

					}

					sb.append("<tr><td class='spacer'></td></tr>");
				}

				sb.append("</table>");

				return sb.toString();

			}

		});
		return doc;
	}
}
