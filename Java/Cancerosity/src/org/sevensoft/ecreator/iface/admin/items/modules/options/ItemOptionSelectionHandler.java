package org.sevensoft.ecreator.iface.admin.items.modules.options;

import java.util.*;
import java.io.IOException;

import javax.servlet.ServletException;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.media.images.panels.ImageAdminPanel;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.frontend.ErrorDoc;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.options.ItemOption;
import org.sevensoft.ecreator.model.items.options.ItemOptionSelection;
import org.sevensoft.ecreator.model.items.options.renderers.RevealOptionsSelectionRenderer;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.ecreator.model.media.images.ImageUtil;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks
 */
@Path("admin-items-options-selection.do")
public class ItemOptionSelectionHandler extends AdminHandler {

	private ItemOptionSelection	selection;
	private ItemOption		option;
	private Map<Integer, String>	imageDescriptions;
	private List<Upload>		imageUploads;
	private String			text;
	private Money			sellPriceAdjustment;
	private String	imageFilename;
    private int stock;
    private String productCode;
    private List<Integer> revealIds;
//    private List<String> revealId;
    private Money optionRrp;

    public ItemOptionSelectionHandler(RequestContext context) {
		super(context);
	}

	public ItemOptionSelectionHandler(RequestContext context, ItemOptionSelection selection) {
		super(context);
		this.selection = selection;
	}

	public Object delete() throws ServletException {

		if (selection == null)
			return main();

		option = selection.getItemOption();
		option.removeSelection(selection);

		return new ItemOptionsHandler(context, option).main();
	}

	public Object main() throws ServletException {

		if (selection == null)
			return new DashboardHandler(context).main();

		AdminDoc page;
        if (selection.getItemOption().getItem() != null)
            page = new AdminDoc(context, user, "Edit option selection", Tab.getItemTab(selection.getItemOption().getItem()));
        else
            page = new AdminDoc(context, user, "Edit option selection", Tab.getItemTab(selection.getItemOption().getItemType()));

		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update selection"));

				sb.append(new ButtonTag(ItemOptionSelectionHandler.class, "delete", "Delete this selection", "selection", selection)
						.setConfirmation("Are you sure you want to delete this selection?"));

				sb.append(new ButtonTag(ItemOptionsHandler.class, null, "Return to option", "option", selection.getItemOption()));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Value", "The value of this option.", new TextTag(context, "text", selection.getText(), 40)));

                if (ItemModule.Pricing.enabled(context, selection.getItemOption().getItem()) || ItemModule.Pricing.enabled(context, selection.getItemOption().getItemType())) {

					sb.append(new AdminRow("Sell price adjustment", "Increase or decrease sell price.", new TextTag(context, "sellPriceAdjustment",
							selection.getSalePrice(), 40) + " " +
                                new ButtonTag(ItemOptionSelectionHandler.class, "savePrices", "Save Related Prices", "selection", selection, "sellPriceAdjustment", selection.getSalePrice())));

                    sb.append(new AdminRow("RRP", "This option's RRP price (calculates discount)", new TextTag(context, "optionRrp", selection.getOptionRrp(), 40) + " " +
                            new ButtonTag(ItemOptionSelectionHandler.class, "saveRrp", "Save Related Prices", "selection", selection, "optionRrp", selection.getOptionRrp())));

                }

                sb.append(new AdminRow("Stock value", "The value used to combine the total stock", new TextTag(context, "stock", selection.getStock(), 40)));

                sb.append(new AdminRow("Product code", "Code of the product to which this option applies", new TextTag(context, "productCode",
                        selection.getProductCode(), 40)));

//                sb.append(new AdminRow("Reveal Ids", "Hide these attributes until revealed by selecting this option", new TextTag(context, "revealIds",
//						StringHelper.implode(selection.getRevealIds(), ",", true))));

                sb.append("</table>");
			}

            private void revealIds(){
                sb.append(new AdminTable("General"));
                sb.append(new AdminRow("Reveal Ids", "Hide these attributes until revealed by selecting this option",
                        new RevealOptionsSelectionRenderer(context, selection.getOption(),
                                Arrays.asList(selection.getRevealIds().toArray(new Integer[selection.getRevealIds().size()])))));
                sb.append("</table>");
            }

			@Override
			public String toString() {

				sb.append(new FormTag(ItemOptionSelectionHandler.class, "save", "multi"));
				sb.append(new HiddenTag("selection", selection));

                commands();
				general();
                revealIds();

				if (selection.getOption().isImages())
					sb.append(new ImageAdminPanel(context, selection));

				commands();

				sb.append("</form>");
				return sb.toString();
			}

		});

		return page;
	}

	public Object moveDown() throws ServletException {

		if (selection == null)
			return index();

		ItemOption option = selection.getItemOption();
		EntityUtil.moveDown(selection, option.getSelections());

		return new ItemOptionsHandler(context, option).main();
	}

	public Object moveUp() throws ServletException {

		if (selection == null)
			return index();

		ItemOption option = selection.getItemOption();
		EntityUtil.moveUp(selection, option.getSelections());

		return new ItemOptionsHandler(context, option).main();
	}

	public Object save() throws ServletException {

		if (selection == null)
			return main();

        if (saveSelection(selection))
            return new ErrorDoc(context, "The image you are attempting to upload is in CYMK colour mode. " +
                    "This is unsopported currently, please use a photo editing application (e.g. Photoshop) to convert the file to RGB colour. For more information or assistance please contact support.");

        if (selection.getParent() == null) {
            List<ItemOptionSelection> related = selection.getRelated(context);
            for (ItemOptionSelection itemOptionSelection : related) {
                // stock, price, code should not be updated on the related selections
//                saveSelection(itemOptionSelection);
                itemOptionSelection.setText(text);
                itemOptionSelection.save();
            }

        }
        
        addMessage("This selection has been updated with your changes.");
        clearParameters();
		return main();
	}

    private boolean saveSelection(ItemOptionSelection selection) {
        selection.setSalePrice(sellPriceAdjustment);
        selection.setOptionRrp(optionRrp);
        selection.setText(text);
        selection.setStock(stock);
        selection.setProductCode(productCode);
        selection.setRevealIds(/*StringHelper.explodeIntegers(revealIds, "\\D")*/(revealIds));
        selection.save();

        try {
            ImageUtil.save(context, selection, imageUploads, null, imageFilename, true);
        } catch (ImageLimitException e) {
            e.printStackTrace();
        } catch (IOException e) {
            return true;
        }

        return false;
    }

    public Object savePrices() throws ServletException {
        if (selection == null) {
            return main();
        }
        selection.setSalePrice(sellPriceAdjustment);
        selection.saveSalePrice();
        for (ItemOptionSelection itemOptionSelection : selection.getRelated(context)) {
            itemOptionSelection.setSalePrice(sellPriceAdjustment);
            itemOptionSelection.saveSalePrice();
        }
        return main();
    }

    public Object saveRrp() throws ServletException {
        if (selection == null) {
            return main();
        }
        selection.setOptionRrp(optionRrp);
        selection.saveRrp();
        for (ItemOptionSelection itemOptionSelection : selection.getRelated(context)) {
            itemOptionSelection.setOptionRrp(optionRrp);
            itemOptionSelection.saveRrp();
        }
        return main();
    }

}