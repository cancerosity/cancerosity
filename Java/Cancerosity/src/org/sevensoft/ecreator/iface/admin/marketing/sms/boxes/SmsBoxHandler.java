package org.sevensoft.ecreator.iface.admin.marketing.sms.boxes;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.marketing.sms.box.SmsBox;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 24 Jul 2006 23:51:27
 *
 */
@Path("admin-sms-box.do")
public class SmsBoxHandler extends BoxHandler {

	private SmsBox	box;

	public SmsBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {
	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

}
