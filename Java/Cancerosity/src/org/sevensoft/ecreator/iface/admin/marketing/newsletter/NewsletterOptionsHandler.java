package org.sevensoft.ecreator.iface.admin.marketing.newsletter;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.misc.html.EditMarkupButton;
import org.sevensoft.ecreator.iface.admin.search.SearchEditHandler;
import org.sevensoft.ecreator.iface.admin.design.reorder.ObjectsReorderPanel;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterControl;
import org.sevensoft.ecreator.model.marketing.newsletter.NewsletterOption;
import org.sevensoft.ecreator.model.marketing.newsletter.Newsletter;
import org.sevensoft.ecreator.model.misc.MiscSettings;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;

import javax.servlet.ServletException;
import java.util.List;

/**
 * User: Tanya
 * Date: 16.07.2012
 */
@Path("admin-newsletters-options.do")
public class NewsletterOptionsHandler extends AdminHandler {

    private final transient NewsletterControl newsletterSettings;
    private final transient MiscSettings miscSettings;
    private Newsletter newsletter;
    private List<NewsletterOption> newsletterOptions;
    private NewsletterOption newsletterOption;
    private List<NewsletterOption> positions;
    private String name;
    private String subject;
    private boolean hidden;
    private String description;
    private Markup markup;

    public NewsletterOptionsHandler(RequestContext context) {
        super(context);

        this.newsletterSettings = NewsletterControl.getInstance(context);
        this.miscSettings = MiscSettings.getInstance(context);
    }

    public NewsletterOptionsHandler(RequestContext context, Newsletter newsletter) {
        this(context);
        this.newsletter = newsletter;
//        this.newsletterOptions = NewsletterOption.get(context, newsletter);
    }

    public Object main() throws ServletException {
        if (!Module.NewsletterNewItems.enabled(context))
            return index();
        this.newsletterOptions = NewsletterOption.get(context, newsletter);

        AdminDoc doc = new AdminDoc(context, user, "Newsletter settings", Tab.Marketing);
        if (miscSettings.isAdvancedMode()) {
            doc.setMenu(new NewslettersMenu(newsletter));
        }
        doc.addBody(new Body() {

            private void commands() {
                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update"));
                sb.append("</div>");
            }

            private void general() {
                sb.append(new AdminTable("Newsletter options"));

                sb.append("<tr>");
                sb.append("<th width='60'>Position</th>");
                sb.append("<th>Name</th>");
                sb.append("<th width='10'> </th>");
                sb.append("</tr>");

                ObjectsReorderPanel panel = new ObjectsReorderPanel(context, null, newsletterOptions);

                panel.setEditClazz(NewsletterOptionsHandler.class);
                panel.setEditAction("edit");
                panel.setEditParam("newsletterOption");
                panel.setEditExtraParams("newsletter", newsletter);

                panel.setDeleteClazz(NewsletterOptionsHandler.class);
                panel.setDeleteAction("delete");
                panel.setDeleteParam("newsletterOption");
                panel.setDeleteConfirmation("Are you sure you want to delete this newsletter option?");

                sb.append("<tr>");
                sb.append("<td colspan='3'>");
                sb.append(panel);
                sb.append("</td>");
                sb.append("</tr>");

                createOption();

                sb.append("</table>");

            }

            private void createOption() {
                sb.append("<tr>");
                sb.append("<td colspan='3'>");
                sb.append(new ButtonTag(NewsletterOptionsHandler.class, "create", "Add Option", "newsletter", newsletter));
                sb.append("</td>");
                sb.append("</tr>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(NewsletterOptionsHandler.class, "reorder", "POST"));
                sb.append(new HiddenTag("newsletter", newsletter));

                commands();
                general();
                commands();

                sb.append("</form>");

                return sb.toString();
            }
        });
        return doc;
    }

    public Object reorder() throws ServletException {
        reorder(positions);

        return new ActionDoc(context, "Newsletter options positions have been updated",
                new Link(NewsletterOptionsHandler.class, null, "newsletter", newsletter));
    }

    public Object create() throws ServletException {
        if (!Module.NewsletterNewItems.enabled(context))
            return index();
        if (newsletter == null) {
            return index();
        }

        newsletter.addNewsletterOption(context);
        return new NewsletterOptionsHandler(context, newsletter).main();
    }

    public Object edit() throws ServletException {
        if (!Module.NewsletterNewItems.enabled(context))
            return index();
        if (newsletterOption == null) {
            return index();
        }

        AdminDoc doc = new AdminDoc(context, user, "Newsletter option #" + newsletterOption.getId(), Tab.Marketing);
        if (miscSettings.isAdvancedMode()) {
            doc.setMenu(new NewslettersMenu(newsletter));
        }
        doc.addBody(new Body() {

            private void commands() {
                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update option"));
//                sb.append(new ButtonTag(NewsletterOptionsHandler.class, "copy", "Copy option", "newsletterOption", newsletterOption));
                sb.append(new ButtonTag(NewsletterOptionsHandler.class, "delete", "Delete option", "newsletterOption", newsletterOption, "newsletter", newsletter)
                        .setConfirmation("Are you sure you want to delete this newsletter option?"));
                sb.append(new ButtonTag(NewsletterOptionsHandler.class, null, "Return to options list", "newsletter", newsletter));

                sb.append("</div>");
            }

            private void general() {
                sb.append(new AdminTable("General"));
                sb.append(new AdminRow("Name", "", new TextTag(context, "name", newsletterOption.getName(), 80)));
                sb.append(new AdminRow("Hidden", "Hidden option won't be offered for subscription", new BooleanRadioTag(context, "hidden", newsletterOption.isHidden())));
                sb.append(new AdminRow("Description", "", new TextAreaTag(context, "description", newsletterOption.getDescription(), 80, 5)));
                sb.append(new AdminRow("Email Subject", "", new TextTag(context, "subject", newsletterOption.getSubject(), 80)));
                sb.append(new AdminRow("Search", "Edit the option's searcher", new ButtonTag(SearchEditHandler.class, null, "Submit",
                        "search", newsletterOption.getSearch())));

                sb.append(new AdminRow(!Module.UserMarkup.enabled(context), "Markup", null, new SelectTag(context, "markup", newsletterOption.getMarkup(), Markup
                        .get(context), "Use default").setId("markup") + " " + new EditMarkupButton("markup")));


                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(NewsletterOptionsHandler.class, "save", "POST"));
                sb.append(new HiddenTag("newsletter", newsletter));
                sb.append(new HiddenTag("newsletterOption", newsletterOption));

                commands();
                general();
                commands();

                sb.append("</form>");

                return sb.toString();
            }
        });
        return doc;
    }

    public Object delete() throws ServletException {
        if (!Module.NewsletterNewItems.enabled(context))
            return index();

        if (newsletterOption == null) {
            return main();
        }

        Newsletter newsletter = newsletterOption.getNewsletter();
        if (newsletter == null) {
            return index();
        }

        newsletter.removeNewletterOption(context, newsletterOption);

        return new ActionDoc(context, "This newsletter option has been deleted", new Link(NewsletterOptionsHandler.class, null, "newsletter", newsletter));
    }

    public Object save() throws ServletException {
        if (!Module.NewsletterNewItems.enabled(context))
            return index();
        if (newsletterOption == null) {
            return main();
        }
        newsletterOption.setName(name);
        newsletterOption.setHidden(hidden);
        newsletterOption.setDescription(description);
        newsletterOption.setSubject(subject);
        newsletterOption.setMarkup(markup);
        newsletterOption.save();

        return new ActionDoc(context, "This listing package has been updated", new Link(NewsletterOptionsHandler.class, "edit", "newsletterOption",
                newsletterOption, "newsletter", newsletter));

    }
}
