package org.sevensoft.ecreator.iface.admin.crm.forms.report;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.model.crm.forms.submission.report.SubmissionReportModule;
import org.sevensoft.ecreator.model.crm.forms.submission.report.SubmissionReportRunner;
import org.sevensoft.ecreator.model.crm.forms.submission.report.renderers.SubmissionFormsListRenderer;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.commons.superstrings.StringHelper;

import javax.servlet.ServletException;
import java.util.List;
import java.util.Arrays;

/**
 * User: Tanya
 * Date: 03.11.2011
 */
@Path("admin-forms-submissions-report-module.do")
public class SubmissionReportModuleHandler extends AdminHandler {

    private transient SubmissionReportModule module;

    private String emails;
    private String reportTitle;
    private List<Integer> forms;

    public SubmissionReportModuleHandler(RequestContext context) {
        super(context);
        this.module = SubmissionReportModule.getInstance(context);
    }

    public Object main() throws ServletException {

        AdminDoc page = new AdminDoc(context, user, "Form submissions Report", Tab.Extras);
        if (miscSettings.isAdvancedMode()) {
            page.setMenu(new ExtrasMenu());
        }

        page.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update selection"));
                sb.append(new ButtonTag(SubmissionReportModuleHandler.class, "sendReport", "Send Report"));

                sb.append("</div>");
            }

            private void general() {

                sb.append(new AdminTable("General"));

                sb.append(new AdminRow("Recipient emails",
                        "If you enter email addresses in here, then they will receive a copy of report "
                                + "<i>Put each email on a separate line.</i>", new TextAreaTag(context, "emails", StringHelper.implode(module
                                .getEmails(), "\n"), 60, 3)));


                sb.append(new AdminRow("Report Title", "The value used as email Subject", new TextTag(context, "reportTitle", module.getReportTitle(), 40)));


//                sb.append(new AdminRow("Reveal Ids", "Hide these attributes until revealed by selecting this option", new TextTag(context, "revealIds",
//						StringHelper.implode(selection.getRevealIds(), ",", true))));

                sb.append("</table>");
            }

            private void formsCheck() {
                sb.append(new AdminTable("Forms"));
                sb.append(new AdminRow("Form Ids", "Send reports of selected forms",
                        new SubmissionFormsListRenderer(context, Arrays.asList(module.getForms().toArray(new Integer[module.getForms().size()])))));
                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(SubmissionReportModuleHandler.class, "save", "multi"));
                sb.append(new HiddenTag("module", module));

                commands();
                general();
                formsCheck();

                commands();

                sb.append("</form>");
                return sb.toString();
            }

        });

        return page;
    }

    public Object save() throws ServletException {

        if (module == null) {
            return index();
        }

        module.setEmails(StringHelper.explodeStrings(emails, "\n"));
        module.setReportTitle(reportTitle);
        module.setForms(forms);
        module.save();

        return new ActionDoc(context, "Module updated", new Link(SubmissionReportModuleHandler.class));
    }

    public Object sendReport() {
        new SubmissionReportRunner(context).run();
        return new ActionDoc(context, "Report sent", new Link(SubmissionReportModuleHandler.class));
    }
}
