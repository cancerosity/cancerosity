package org.sevensoft.ecreator.iface.admin.feeds.ecreator;

import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.feeds.Feed;
import org.sevensoft.ecreator.model.feeds.ecreator.EcreatorExportFeed;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 27 Jan 2007 19:50:33
 *
 */
@Path("admin-feeds-ecreator-export.do")
public class EcreatorExportFeedHandler extends FeedHandler {

	private EcreatorExportFeed	feed;
	private ItemType			itemType;

	public EcreatorExportFeedHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Feed getFeed() {
		return feed;
	}

	@Override
	protected void saveSpecific() {

		feed.setItemType(itemType);
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Settings"));

		sb.append(new AdminRow("Item type", "Select the item type you want to export.", new SelectTag(context, "itemType", feed.getItemType(), ItemType
				.getSelectionMap(context), "-None set-")));

		sb.append("</table>");
	}

}
