package org.sevensoft.ecreator.iface.admin.comments;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.misc.html.EditMarkupButton;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.model.comments.CommentsSettings;
import org.sevensoft.ecreator.model.comments.blocks.CommentPostBlock;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.commons.validators.net.EmailValidator;
import org.sevensoft.commons.superstrings.StringHelper;

import javax.servlet.ServletException;
import java.util.List;
import java.util.Arrays;

/**
 * User: Tanya
 * Date: 26.07.2010
 */

@Path("admin-comments-settings.do")
public class CommentsSettingsHandler extends AdminHandler {

    private transient CommentsSettings commentsSettings;

    private boolean addToNewCategories;
    private boolean moderate;
    private Markup commentsListMarkup;
    private Markup commentsAddMarkup;
    private Markup loginMarkup;
    private String commentNotificationEmails;

    public CommentsSettingsHandler(RequestContext context) {
        super(context);
        this.commentsSettings = CommentsSettings.getInstance(context);
    }

    public Object main() throws ServletException {

        AdminDoc doc = new AdminDoc(context, user, "Comments settings", null);
        doc.addBody(new Body() {

            private void commands() {
                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update comments settings"));
                sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
                sb.append("</div>");
            }

            private void general() {
                sb.append(new AdminTable("General"));

                sb.append(new AdminRow("Add to Categories",
                        "Add the Comments block to all current categories on the site", new BooleanRadioTag(context, "addToNewCategories",
                                commentsSettings.isAddToCategories())));

                sb.append(new AdminRow("Moderation", "Comments will be moderated before showing on the site.",
                        new BooleanRadioTag(context, "moderate", commentsSettings.isModerate())));

                sb.append(new AdminRow("Third party notifications" + new ErrorTag(context, "commentNotificationEmails", "<br/>"),
                                   "Enter third party email addresses in here and they will be sent an email each time an order is placed.", new TextAreaTag(
                                           context, "commentNotificationEmails", commentsSettings.getCommentNotificationEmailsString(), 50, 4)));
              
                sb.append(new AdminRow(true, "Comments List Markup", null, new SelectTag(context, "commentsListMarkup", commentsSettings.getCommentsListMarkup(), Markup.get(context), "-Use default-")
                        .setId("commentsListMarkup") + " " + new EditMarkupButton("commentsListMarkup")));

                sb.append(new AdminRow(true, "Comments Add Markup", null, new SelectTag(context, "commentsAddMarkup", commentsSettings.getCommentsAddMarkup(), Markup.get(context), "-Use default-")
                        .setId("commentsAddMarkup") + " " + new EditMarkupButton("commentsAddMarkup")));

                sb.append(new AdminRow(true, "Login Markup", null, new SelectTag(context, "loginMarkup", commentsSettings.getLoginMarkup(), Markup.get(context), "-Use default-")
                        .setId("loginMarkup") + " " + new EditMarkupButton("loginMarkup")));

                sb.append(new AdminRow(true, "Remove blocks", "Remove the Comments block from all categories",
                        new ButtonTag(CommentsSettingsHandler.class, "removeCategoryBlocks", "Remove")
                                .setConfirmation("Are you sure want to remove all the comments blocks?")));


                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(CommentsSettingsHandler.class, "save", "POST"));

                commands();
                general();
                commands();

                sb.append("</form>");

                return sb.toString();
            }
        });

        return doc;

    }

    public Object save() throws ServletException {

        if (commentNotificationEmails != null) {
            List<String> emails = Arrays.asList(commentNotificationEmails.split("\r\n"));
            for (String email : emails) {
                String error = new EmailValidator().validate(email);
                if (error != null) {
                    context.setError("commentNotificationEmails", "Invalid email");
                    addError("Please correct the errors and click update");
                    return main();
                }
            }
        }
        commentsSettings.setCommentNotificationEmails(StringHelper.explodeStrings(commentNotificationEmails, "[,\\s\\n]"));
        commentsSettings.setAddToCategories(addToNewCategories);
        commentsSettings.setModerate(moderate);
        commentsSettings.setCommentsListMarkup(commentsListMarkup);
        commentsSettings.setCommentsAddMarkup(commentsAddMarkup);
        commentsSettings.setLoginMarkup(loginMarkup);
        commentsSettings.save();
        return main();
    }

    public Object removeCategoryBlocks() throws ServletException {
        int removed = CommentPostBlock.removeAtCategories(context);
        return new ActionDoc(context, removed + " blocks have been removed.", new Link(CommentsSettingsHandler.class));
    }
}
