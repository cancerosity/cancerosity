package org.sevensoft.ecreator.iface.admin;

import java.io.IOException;

import org.sevensoft.commons.validators.text.LengthValidator;
import org.sevensoft.ecreator.iface.EcreatorHandler;
import org.sevensoft.ecreator.iface.admin.design.template.TemplateInstaller;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Servers;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.ecreator.model.system.installer.InstallerException;
import org.sevensoft.ecreator.model.design.template.Template;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.PasswordTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.results.ExternalRedirect;
import org.sevensoft.jeezy.http.results.docs.Doc;

/**
 * @author sks 5 Aug 2006 07:43:34
 *
 */
@Path("admin-login.do")
public class AdminLoginHandler extends EcreatorHandler {

	private transient Company	company	= Company.getInstance(context);
    
    private String			password, username;
	private String			linkback;

	public AdminLoginHandler(RequestContext context) {
		super(context);
	}

	/**
	 * Automatically logs in for people with superman ip
	 */
	public Object autoLogin() {

        if (config.getUrl() != null && config.getUrl().contains("www.7soft.co.uk")) {
			return main();
		}

		if (User.isSuperIp(context)) {

			// get any admin user to login as 
			user = User.getAdmin(context);

			if (user != null) {

				user.login(context.getSessionId());
				setAttribute("user", user);

				clearParameters();
				if (linkback != null && !linkback.contains("login.do")) {
					return new ExternalRedirect(linkback);
				} else {
					return new DashboardHandler(context).main();
				}

			}
		}

		return main();
	}

	public Object login() {

		test(new LengthValidator(3), "username");
		test(new LengthValidator(3), "password");
		if (hasErrors()) {
			return main();
		}

		/*
		 * If the username is superman then check password against passwd in file
		 */
		user = User.getByLogin(context, username, password);

		/*
		 * If the user has not been set then flag error and return
		 */
		if (user == null) {

			setError("username", "Invalid login details");
			return main();
		}

		/*
		 * If we are here then we logged in successfully
		 */
        if (user.isActive()) {
            user.login(context.getSessionId());
            setAttribute("user", user);

            clearParameters();
            if (linkback != null && !linkback.contains("login.do")) {
                return new ExternalRedirect(linkback);
            } else {
                return new DashboardHandler(context).main();
            }
        }
        else {
            setError("username", "This user is inactive");
			return main();
        }
    }

	public Object logout() {

		User.logout(context, context.getSessionId());
		user = null;
		removeAttribute("user");
		return main();
	}

	@Override
	public Object main() {

		// try to get root just to ensure the installer has been run
		Category.getRoot(context);

        // if we have no template then install one

        if (Template.getDefault(context) == null) {
            try {
                TemplateInstaller.install(context, "FleetStreet3-standard.zip", null);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InstallerException e) {
                e.printStackTrace();
            }
        }

        return new Doc(context) {

			@Override
			public StringBuilder output() throws IOException {

				StringBuilder sb = new StringBuilder();

				sb.append("<style>");
				sb.append("table.login { margin-top: 100px; font-family: Verdana; color: #666666; }");
				sb.append("table.login td { padding: 4px; } ");
				sb.append("table.login td.name { font-weight: bold; font-family: Arial; font-size: 20px;  } ");
				sb.append("table.login td.key { font-size: 11px; font-weight: bold; padding-right: 20px; } ");
				sb.append("table.login td.credits {  font-size: 9px;   } ");
				sb.append("table.login td.submit { padding: 20px 0; } ");
				sb.append("table.login span.error { font-size: 11px; font-weight: bold; color: #cc0000; }");
				sb.append("</style>");

				sb.append(new FormTag(AdminLoginHandler.class, "login", "POST"));
				sb.append(new TableTag("login", "center"));
				sb.append(new HiddenTag("linkback", context.getGetRequest()));

				sb.append("<tr><td colspan='2' align='center' class='name'>" + company.getName() + "</td></tr>");
				sb.append("<tr><td colspan='2' align='center'><img src='files/graphics/admin/login.gif'/></td></tr>");

				sb.append("<tr><td class='key'>Username:</td><td>" + new TextTag(context, "username", 16) + new ErrorTag(context, "username", "<br/>") +
						"</td></tr>");
				sb.append("<tr><td class='key'>Password:</td><td>" + new PasswordTag(context, "password", 16) +
						new ErrorTag(context, "password", "<br/>") + "</td></tr>");

				sb.append("<tr><td colspan='2' align='center' class='submit'>");
				sb.append(new SubmitTag("Login"));

                if (User.isSuperIp(context) && config != null && config.getUrl() != null && !config.getUrl().contains("www.7soft.co.uk")) {
					sb.append(new ButtonTag(AdminLoginHandler.class, "autoLogin", "Auto login", "linkback", context.getGetRequest()));
				}

				sb.append("</td></tr>");

				sb.append("<tr><td colspan='2' align='center' class='credits'>" + config.getDevelopers() + "<br/>Your ip: " + getRemoteIp());

				if (User.isSuperIp(context)) {

					String server = Servers.getServerName(context.getServerIp());
					if (server != null) {
						sb.append("<br/>Server: ");
						sb.append(server);
					}
				}

				sb.append("<br/>Version: " + context.getServletAttribute("version"));

				sb.append("</td></tr>");
				sb.append("</table>");

				sb.append("</form>");

				return sb;
			}
		};
	}

	@Override
	protected boolean runSecure() {
		return false;
	}
}
