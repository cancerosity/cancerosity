package org.sevensoft.ecreator.iface.admin.system.captions;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 22-Jul-2005 10:49:00
 * 
 */
@Path("super-captions.do")
public class CaptionsHandler extends AdminHandler {

	private String	manufacturersCaption;
	private String	listingsDescriptionCaption;
	private String	listingsNameCaption;
	private String	wizardCaption;
	private String	listingCaption;
	private String	membersCaption;
	private String	accountCaption;
	private String	filesCaption;
	private String	requestsCaption;
	private String	chatCaption;
	private String	profileSearchCaption;
	private String	profileSearchSidebarCaption;
	private String	searchRegistrationCaption;
	private String	nudgesCaption;
	private String	nicknameCaption;
	private String	viewsCaption;
	private String	imagesCaption;
	private String	buddiesCaption;
	private String	messagesCaption;
	private String	deliveryOptionsCaption;
	private String	checkoutDeliveryTitle;
	private String	registrationPageCaption;
	private String	loginPageCaption;
	private String	ticketsCaption;
	private String	enotesCaption;
	private String	copyrightMessage;
	private String	poweredByMessage;
	private String	addListingCaption;
	private String	myListingsCaption;
	private String	attachmentsCaption;
	private String	creditsCaption;
	private String	categoriesCaption;
    private String  orderStatusCaption;
    private String  favouritesCaption;
    private String  listingsCaption;

	public CaptionsHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() {

		if (!isSuperman())
			return new DashboardHandler(context).main();

		AdminDoc page = new AdminDoc(context, user, "Caption settings", null);
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update captions"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void credits() {

				sb.append(new AdminTable("Credits"));

				sb.append(new AdminRow("Copyright message", new TextTag(context, "copyrightMessage", modules.getCopyrightMessage(), 80)));
				sb.append(new AdminRow("Powered by message", new TextTag(context, "poweredByMessage", modules.getPoweredByMessage(), 80)));

				sb.append("</table>");
			}

			private void members() {

				sb.append(new AdminTable("Interaction captions"));

				sb.append("<tr><th width='300'><b>Account caption</b></th><td>" + new TextTag(null, "accountCaption", captions.getAccountCaption(), 40)
						+ "</td></tr>");

				sb.append("<tr><th width='300'><b>Login page caption</b></th><td>"
						+ new TextTag(null, "loginPageCaption", captions.getLoginPageCaption(), 40) + "</td></tr>");

				sb.append("<tr><th width='300'><b>Registration page caption</b></th><td>"
						+ new TextTag(null, "registrationPageCaption", captions.getRegistrationPageCaption(), 40) + "</td></tr>");

				sb.append("<tr><th width='300'><b>Chat caption</b></th><td>" + new TextTag(null, "chatCaption", captions.getChatCaption(), 40)
						+ "</td></tr>");

				sb.append("<tr><th width='300'><b>Nudges caption</b></th><td>" + new TextTag(null, "nudgesCaption", captions.getWinksCaption(), 40)
						+ "</td></tr>");

				sb.append("<tr><th width='300'><b>Views caption</b></th><td>" + new TextTag(null, "viewsCaption", captions.getViewsCaption(), 40)
						+ "</td></tr>");

				sb.append("<tr><th width='300'><b>Messages caption</b></th><td>" + new TextTag(null, "messagesCaption", captions.getPmCaption(), 40)
						+ "</td></tr>");

				sb.append("<tr><th width='300'><b>Buddies caption</b></th><td>" + new TextTag(null, "buddiesCaption", captions.getBuddiesCaption(), 40)
						+ "</td></tr>");

				sb.append("<tr><th width='300'><b>Images caption</b></th><td>" + new TextTag(null, "imagesCaption", captions.getImagesCaption(), 40)
						+ "</td></tr>");

                sb.append("<tr><th width='300'><b>Order status caption</b></th><td>" + new TextTag(null, "orderStatusCaption", captions.getOrderStatusCaption(), 40)
						+ "</td></tr>");

                sb.append("<tr><th width='300'><b>Favourites caption</b></th><td>" + new TextTag(null, "favouritesCaption", captions.getFavouritesCaption(), 40)
						+ "</td></tr>");

                sb.append("<tr><th width='300'><b>Listings caption</b></th><td>" + new TextTag(null, "listingsCaption", captions.getListingsCaption(), 40)
						+ "</td></tr>");

				sb.append("</table>");
			}

			private void misc() {

				sb.append(new AdminTable("Misc"));

				sb.append(new AdminRow("Categories", "Change the name for categories/pages.", new TextTag(context, "categoriesCaption", captions
						.getCategoriesCaption(), 20)));

				sb.append(new AdminRow("Attachments", "Change the general name for attachments on the site.", new TextTag(context,
						"attachmentsCaption", captions.getAttachmentsCaption(), 40)));

				sb.append(new AdminRow("Credits", "Change the general name for credits on the site.", new TextTag(context, "creditsCaption", captions
						.getCreditsCaption(), 40)));

				sb.append("</table>");
			}

			private void shopping() {

				sb.append(new AdminTable("Shopping"));

				sb.append(new AdminRow("Checkout Delivery Title", new TextTag(null, "checkoutDeliveryTitle", captions.getCheckoutDeliveryTitle(), 80)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(CaptionsHandler.class, "save", "POST"));

                commands();
				shopping();
				members();

				misc();
				credits();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return page;
	}

	public Object save() {

		if (!isSuperman()) {
			return new DashboardHandler(context).main();
		}

		captions.setCategoriesCaption(categoriesCaption);

		/*
		 * Member page captions
		 */
		captions.setAccountCaption(accountCaption);
		captions.setRegistrationPageCaption(registrationPageCaption);
		captions.setLoginPageCaption(loginPageCaption);

		captions.setChatCaption(chatCaption);

		// shopping
		captions.setDeliveryOptionsCaption(deliveryOptionsCaption);
		captions.setCheckoutDeliveryTitle(checkoutDeliveryTitle);

		// members
		captions.setNudgesCaption(nudgesCaption);
		captions.setViewsCaption(viewsCaption);
		captions.setMessagesCaption(messagesCaption);
		captions.setBuddiesCaption(buddiesCaption);
		captions.setImagesCaption(imagesCaption);
        captions.setOrderStatusCaption(orderStatusCaption);
        captions.setFavouritesCaption(favouritesCaption);
        captions.setListingsCaption(listingsCaption);

		// misc
		captions.setAttachmentsCaption(attachmentsCaption);

		captions.setCreditsCaption(creditsCaption);

		captions.save();

		modules.setPoweredByMessage(poweredByMessage);
		modules.setCopyrightMessage(copyrightMessage);

		modules.save();

		addMessage("Captions updated");
		clearParameters();
		return main();
	}

}
