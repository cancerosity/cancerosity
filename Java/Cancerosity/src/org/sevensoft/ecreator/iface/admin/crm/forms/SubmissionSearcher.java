package org.sevensoft.ecreator.iface.admin.crm.forms;

import org.sevensoft.ecreator.iface.admin.search.AbstractSearcher;
import org.sevensoft.ecreator.model.crm.forms.submission.Submission;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * User: Tanya
 * Date: 15.03.2012
 */
public class SubmissionSearcher extends AbstractSearcher {

    public SubmissionSearcher(RequestContext context) {
        super(context);
    }

    public Class getTable() {
        return Submission.class;
    }

    @Override
    public String getOrderby() {
        return "id desc";
    }

    public static SubmissionSearcher getSearcher(RequestContext context) {
        return new SubmissionSearcher(context);
    }


}
