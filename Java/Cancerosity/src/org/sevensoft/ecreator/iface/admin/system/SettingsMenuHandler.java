package org.sevensoft.ecreator.iface.admin.system;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ibex.IbexSettingsHandler;
import org.sevensoft.ecreator.iface.admin.login.facebook.FacebookLoginSettingsHandler;
import org.sevensoft.ecreator.iface.admin.sitemap.SitemapSettingsHandler;
import org.sevensoft.ecreator.iface.admin.comments.CommentsSettingsHandler;
import org.sevensoft.ecreator.iface.admin.comments.CommentModerationHandler;
import org.sevensoft.ecreator.iface.admin.comments.CommentListHandler;
import org.sevensoft.ecreator.iface.admin.accounts.AccountSettingsHandler;
import org.sevensoft.ecreator.iface.admin.accounts.permissions.AccessGroupsHandler;
import org.sevensoft.ecreator.iface.admin.accounts.registration.RegistrationModerationsHandler;
import org.sevensoft.ecreator.iface.admin.accounts.registration.RegistrationSettingsHandler;
import org.sevensoft.ecreator.iface.admin.accounts.subscriptions.SubscriptionFeaturesHandler;
import org.sevensoft.ecreator.iface.admin.accounts.subscriptions.SubscriptionLevelsHandler;
import org.sevensoft.ecreator.iface.admin.attachments.AttachmentSettingsHandler;
import org.sevensoft.ecreator.iface.admin.attachments.AttachmentUploaderHandler;
import org.sevensoft.ecreator.iface.admin.attributes.AttributeHandler;
import org.sevensoft.ecreator.iface.admin.categories.CategorySettingsHandler;
import org.sevensoft.ecreator.iface.admin.categories.MultipleCategoryHandler;
import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxMenuHandler;
import org.sevensoft.ecreator.iface.admin.crm.forum.ForumSettingsHandler;
import org.sevensoft.ecreator.iface.admin.crm.jobsheets.JobsheetHandler;
import org.sevensoft.ecreator.iface.admin.crm.mailboxes.MailboxHandler;
import org.sevensoft.ecreator.iface.admin.design.markup.MarkupHandler;
import org.sevensoft.ecreator.iface.admin.design.template.StylesheetHandler;
import org.sevensoft.ecreator.iface.admin.design.template.TemplateDataHandler;
import org.sevensoft.ecreator.iface.admin.design.template.TemplateHandler;
import org.sevensoft.ecreator.iface.admin.ecom.addresses.AddressFieldsHandler;
import org.sevensoft.ecreator.iface.admin.ecom.bookings.BookingSearchHandler;
import org.sevensoft.ecreator.iface.admin.ecom.bookings.config.BookingSettingsHandler;
import org.sevensoft.ecreator.iface.admin.ecom.credits.CreditPackageHandler;
import org.sevensoft.ecreator.iface.admin.ecom.credits.CreditSettingsHandler;
import org.sevensoft.ecreator.iface.admin.ecom.currencies.CurrenciesHandler;
import org.sevensoft.ecreator.iface.admin.ecom.delivery.DeliveryOptionsHandler;
import org.sevensoft.ecreator.iface.admin.ecom.delivery.DeliverySettingsHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.OrderSettingsHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.exports.OrderExportHandler;
import org.sevensoft.ecreator.iface.admin.ecom.orders.sync.ExportOrdersHandler;
import org.sevensoft.ecreator.iface.admin.ecom.payments.CallbacksHandler;
import org.sevensoft.ecreator.iface.admin.ecom.payments.PaymentSettingsHandler;
import org.sevensoft.ecreator.iface.admin.ecom.promotions.PromotionHandler;
import org.sevensoft.ecreator.iface.admin.ecom.shopping.BasketViewHandler;
import org.sevensoft.ecreator.iface.admin.ecom.shopping.ShoppingSettingsHandler;
import org.sevensoft.ecreator.iface.admin.ecom.vouchers.VoucherHandler;
import org.sevensoft.ecreator.iface.admin.ecom.removal.OrderRemovalHandler;
import org.sevensoft.ecreator.iface.admin.extras.busclub.BusClubSettingsHandler;
import org.sevensoft.ecreator.iface.admin.extras.maps.GoogleMapSettingsHandler;
import org.sevensoft.ecreator.iface.admin.extras.newsfeed.NewsfeedsHandler;
import org.sevensoft.ecreator.iface.admin.extras.polls.PollSettingsHandler;
import org.sevensoft.ecreator.iface.admin.extras.shoppingbox.ShoppingBoxSettingsHandler;
import org.sevensoft.ecreator.iface.admin.extras.tellfriend.TellFriendSettingsHandler;
import org.sevensoft.ecreator.iface.admin.extras.cookie.CookieSettingsHandler;
import org.sevensoft.ecreator.iface.admin.extras.facebook.FacebookOpenGraphTagsHandler;
import org.sevensoft.ecreator.iface.admin.feeds.FeedMenuHandler;
import org.sevensoft.ecreator.iface.admin.feeds.bespoke.ipoints.IPointsSettingsHandler;
import org.sevensoft.ecreator.iface.admin.feeds.spiders.SpiderHandler;
import org.sevensoft.ecreator.iface.admin.interaction.InteractionSettingsHandler;
import org.sevensoft.ecreator.iface.admin.interaction.sms.SmsCreditPackageHandler;
import org.sevensoft.ecreator.iface.admin.interaction.userplane.chat.UserplaneChatSettingsHandler;
import org.sevensoft.ecreator.iface.admin.interaction.userplane.messenger.UserplaneMessengerSettingsHandler;
import org.sevensoft.ecreator.iface.admin.interaction.userplane.recorder.UserplaneRecorderAdminHandler;
import org.sevensoft.ecreator.iface.admin.interaction.userplane.recorder.UserplaneRecorderSettingsHandler;
import org.sevensoft.ecreator.iface.admin.items.GlobalItemAttributesHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.iface.admin.items.advanced.AdvItemEditorHandler;
import org.sevensoft.ecreator.iface.admin.items.bots.ExpiryBotHandler;
import org.sevensoft.ecreator.iface.admin.items.exporter.ExporterHandler;
import org.sevensoft.ecreator.iface.admin.items.listings.ListingModerationHandler;
import org.sevensoft.ecreator.iface.admin.items.listings.ListingPackageHandler;
import org.sevensoft.ecreator.iface.admin.items.listings.ListingSettingsHandler;
import org.sevensoft.ecreator.iface.admin.items.modules.prices.PriceSettingsHandler;
import org.sevensoft.ecreator.iface.admin.items.modules.favourites.FavouritesSettingsHandler;
import org.sevensoft.ecreator.iface.admin.marketing.affiliates.AffiliateReportsHandler;
import org.sevensoft.ecreator.iface.admin.marketing.newsletter.NewsletterSettingsHandler;
import org.sevensoft.ecreator.iface.admin.marketing.sms.SmsSettingsHandler;
import org.sevensoft.ecreator.iface.admin.media.images.ImageApprovalsHandler;
import org.sevensoft.ecreator.iface.admin.media.images.ImageLibraryHandler;
import org.sevensoft.ecreator.iface.admin.media.images.ImageSettingsHandler;
import org.sevensoft.ecreator.iface.admin.misc.MiscSettingsHandler;
import org.sevensoft.ecreator.iface.admin.misc.content.ContentReplacerHandler;
import org.sevensoft.ecreator.iface.admin.misc.languages.LanguageSettingsHandler;
import org.sevensoft.ecreator.iface.admin.misc.languages.TranslationHandler;
import org.sevensoft.ecreator.iface.admin.misc.seo.*;
import org.sevensoft.ecreator.iface.admin.system.captions.CaptionsHandler;
import org.sevensoft.ecreator.iface.admin.system.company.CompanyHandler;
import org.sevensoft.ecreator.iface.admin.system.config.*;
import org.sevensoft.ecreator.iface.admin.system.log.LogHandler;
import org.sevensoft.ecreator.iface.admin.system.modules.ModulesHandler;
import org.sevensoft.ecreator.iface.admin.system.users.UserHandler;
import org.sevensoft.ecreator.iface.admin.system.scripts.ScriptsHandler;
import org.sevensoft.ecreator.model.items.ItemModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.comments.CommentsSettings;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;
import org.sevensoft.jeezy.http.util.Link;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author sks 23 aug 2005 17:51:12
 */
@Path("admin-settings.do")
public class SettingsMenuHandler extends AdminHandler {

    class Setting implements Comparable<Setting> {

        private final Link link;
        private final String label;
        private final boolean superman;

        Setting(boolean superman, Link link, String label) {
            this.superman = superman;
            this.link = link;
            this.label = label;
        }

        Setting(Link link, String label) {
            this(false, link, label);
        }

        public int compareTo(Setting option) {
            return label.compareTo(option.label);
        }

        public LinkTag getLinkTag() {
            return new LinkTag(link, label);
        }

        public boolean isSuperman() {
            return superman;
        }
    }

    public SettingsMenuHandler(RequestContext context) {
        super(context);
    }

    private MultiValueMap<String, Setting> getOptions() {

        MultiValueMap<String, Setting> settings = new MultiValueMap(new TreeMap());

        settings.put("Attachments", new Setting(new Link(AttachmentSettingsHandler.class), "Attachment settings"));

        if (!ItemType.getAttachmentItemTypes(context).isEmpty()) {
            settings.put("Attachments", new Setting(new Link(AttachmentUploaderHandler.class), "Attachment uploader"));
        }

        settings.put("Attributes", new Setting(new Link(AttributeHandler.class), "Attribute editor"));
        settings.put("Attributes", new Setting(new Link(GlobalItemAttributesHandler.class), "Global item attributes"));

        settings.put("Misc", new Setting(new Link(CompanyHandler.class), "Company details"));

        settings.put("Category", new Setting(new Link(CategorySettingsHandler.class), "Category settings"));
//        links.add(new LinkTag(MultipleCategoryHandler.class, null, "Multiple categories editor"));
        if (isSuperman()) {
            settings.put("Category", new Setting(new Link(MultipleCategoryHandler.class), "Multiple categories editor")); //todo
        }

        if (Module.Accounts.enabled(context)) {
            settings.put("Accounts", new Setting(new Link(AccountSettingsHandler.class), "Account settings"));
            if (ItemType.getRegistration(context).size() > 0) {
                settings.put("Accounts", new Setting(new Link(RegistrationSettingsHandler.class), "Registration settings"));
                settings.put("Accounts", new Setting(new Link(RegistrationModerationsHandler.class), "Registration moderation queue"));
            }
        }

        if (Module.Favourites.enabled(context) && isSuperman()) {
//            settings.put("Misc", new Setting(new Link(FavouritesGroupHandler.class), "Favourites"));
            settings.put("Misc", new Setting(true, new Link(FavouritesSettingsHandler.class), "Favourites"));
        }

        if (Module.TellFriend.enabled(context)) {
            settings.put("Misc", new Setting(new Link(TellFriendSettingsHandler.class), "Tell a friend"));
        }

        if (config.getUrl().contains("businessclub") || context.isLocalhost()) {
            settings.put("Misc", new Setting(new Link(BusClubSettingsHandler.class), "Business club settings"));
        }

        if (config.getUrl().contains("shoppingbox") || context.isLocalhost()) {
            settings.put("Misc", new Setting(new Link(ShoppingBoxSettingsHandler.class), "Shopping box settings"));
        }

        if (Module.GoogleMap.enabled(context) || Module.GoogleMapV3.enabled(context)) {
            settings.put("Mapping", new Setting(new Link(GoogleMapSettingsHandler.class), "Google map"));
        }

        settings.put("Misc", new Setting(new Link(ContentReplacerHandler.class), "Content replacer"));

        if (Module.Jobsheets.enabled(context)) {
            settings.put("Support", new Setting(new Link(JobsheetHandler.class), "Jobsheets"));
        }

        if (Module.Bookings.enabled(context)) {
            settings.put("Bookings", new Setting(new Link(BookingSettingsHandler.class), "Booking settings"));
            settings.put("Bookings", new Setting(new Link(BookingSearchHandler.class), "View bookings"));
        }

        if (Module.Buddies.enabled(context) || Module.MemberViewers.enabled(context)) {
            settings.put("Interaction", new Setting(new Link(InteractionSettingsHandler.class), "Interaction settings"));
        }

        if (Module.UserplaneMessenger.enabled(context)) {
            settings.put("Interaction", new Setting(new Link(UserplaneMessengerSettingsHandler.class), "Userplane messenger"));
        }

        if (Module.SmsCredits.enabled(context)) {
            settings.put("Interaction", new Setting(new Link(SmsCreditPackageHandler.class), "Sms credits"));
        }

        if (Module.UserplaneChat.enabled(context)) {
            settings.put("Interaction", new Setting(new Link(UserplaneChatSettingsHandler.class), "Userplane chat"));
        }

        if (Module.UserplaneRecorder.enabled(context)) {
            settings.put("Interaction", new Setting(new Link(UserplaneRecorderSettingsHandler.class), "Userplane recorder"));
            settings.put("Interaction", new Setting(new Link(UserplaneRecorderAdminHandler.class), "Userplane recorder approvals"));
        }

        if (Module.Permissions.enabled(context)) {
            settings.put("Permissions", new Setting(new Link(AccessGroupsHandler.class), "Access groups"));
        }

        if (Module.CrmMailboxes.enabled(context))
            settings.put("Support", new Setting(new Link(MailboxHandler.class), "Mailboxes"));

        if (Module.Languages.enabled(context)) {
            settings.put("Languages", new Setting(new Link(LanguageSettingsHandler.class), "Language settings"));
            settings.put("Languages", new Setting(new Link(TranslationHandler.class), "Translations"));
        }

        if (isSuperman()) {
            settings.put("Import / Export", new Setting(true, new Link(SpiderHandler.class), "Spiders"));
        }

        if (isSuperman() || Module.Feeds.enabled(context) && (Module.UserFeeds.enabled(context) || isSuperman())) {
            settings.put("Import / Export", new Setting(!(Module.Feeds.enabled(context) || Module.UserFeeds.enabled(context)),
                    new Link(FeedMenuHandler.class), "Feeds"));
        }

        settings.put("Images", new Setting(new Link(ImageLibraryHandler.class), "Image library"));
        settings.put("Images", new Setting(new Link(ImageSettingsHandler.class), "Image settings"));

        settings.put("Images", new Setting(new Link(ImageApprovalsHandler.class), "Image approval queue"));

        if (Module.Listings.enabled(context)) {
            settings.put("Listings", new Setting(new Link(ListingSettingsHandler.class), "Listing settings"));
            settings.put("Listings", new Setting(new Link(ListingPackageHandler.class), "Listing packages"));

            if (Module.ListingModeration.enabled(context)) {
                settings.put("Listings", new Setting(new Link(ListingModerationHandler.class), "Moderation queue"));
            }
        }

        if (Module.Forum.enabled(context)) {
            settings.put("Forums", new Setting(new Link(ForumSettingsHandler.class), "Forum settings"));
        }

        if (isSuperman()) {

            settings.put("Superman", new Setting(true, new Link(ModulesHandler.class), "Modules"));
            settings.put("Superman", new Setting(true, new Link(ConfigHandler.class), "Config"));
            settings.put("Superman", new Setting(true, new Link(CommandHandler.class), "Commands"));

            if (isSamIp()) {
                settings.put("Superman", new Setting(true, new Link(DatabaseHandler.class), "Database"));
            }

            settings.put("Superman", new Setting(true, new Link(DeleterHandler.class), "Deleter"));
            settings.put("Superman", new Setting(true, new Link(StatsHandler.class), "Stats"));
            settings.put("Superman", new Setting(true, new Link(BotsHandler.class), "Bots"));
            settings.put("Superman", new Setting(true, new Link(UpgradeHandler.class), "Upgrades"));
            settings.put("Superman", new Setting(true, new Link(CaptionsHandler.class), "Captions"));
            settings.put("Superman", new Setting(true, new Link(ContentTablesHandler.class), "Content Blocks"));
            settings.put("Superman", new Setting(true, new Link(ScriptsHandler.class), "Scripts"));

        }

        if (Module.CookieControl.enabled(context)) {
            settings.put("Cookie Control", new Setting(new Link(CookieSettingsHandler.class), "Cookie settings"));
        }

        if (isSuperman() || Module.UserMarkup.enabled(context)) {

            boolean superonly = !Module.UserMarkup.enabled(context);

            settings.put("Template", new Setting(superonly, new Link(TemplateHandler.class), "Templates"));

            if (ResourcesUtils.getRealTemplateData("template.css").exists()) {
                settings.put("Template", new Setting(superonly, new Link(StylesheetHandler.class), "Colors and styles"));
            }

            settings.put("Template", new Setting(superonly, new Link(TemplateDataHandler.class), "Images and data"));
            settings.put("Template", new Setting(superonly, new Link(MarkupHandler.class), "Markup"));
        }

        settings.put("Misc", new Setting(new Link(MiscSettingsHandler.class), "Misc Settings"));

        if (Module.Newsfeed.enabled(context)) {
            settings.put("Extras", new Setting(new Link(NewsfeedsHandler.class), "Newsfeeds"));
        }

        if (Module.Polls.enabled(context)) {
            settings.put("Extras", new Setting(new Link(PollSettingsHandler.class), "Poll settings"));
        }

        if (Module.Newsletter.enabled(context)) {
            settings.put("Marketing", new Setting(new Link(NewsletterSettingsHandler.class), "Newsletter settings"));

            if (Module.NewsletterTemplates.enabled(context)) {
                settings.put("Marketing", new Setting(new Link(PriceSettingsHandler.class), "Newsletter templates"));
            }

        }

        if (Module.Affiliates.enabled(context)) {
            settings.put("Marketing", new Setting(new Link(AffiliateReportsHandler.class, "list"), "Affiliate click throughs"));
            if (Module.Shopping.enabled(context)) {
                settings.put("Marketing", new Setting(new Link(AffiliateReportsHandler.class, "conversions"), "Affiliate conversions"));
            }
        }

        if (Module.Subscriptions.enabled(context)) {
            settings.put("Subscriptions", new Setting(new Link(SubscriptionLevelsHandler.class), "Subscription levels"));
            settings.put("Subscriptions", new Setting(new Link(SubscriptionFeaturesHandler.class), "Subscription features"));
        }

        if (Module.Shopping.enabled(context) || Module.Listings.enabled(context) || Module.Subscriptions.enabled(context) ||
                Module.Credits.enabled(context)) {

            settings.put("Payments", new Setting(new Link(PaymentSettingsHandler.class), "Payment options"));

            if (isSuperman()) {
                settings.put("Payments", new Setting(true, new Link(CallbacksHandler.class), "Callbacks"));
            }

            if (Module.Ipoints.enabled(context)) {
                settings.put("Payments", new Setting(true, new Link(IPointsSettingsHandler.class), "Ipoints"));
            }
        }

        if (Module.Credits.enabled(context)) {
            settings.put("Credits", new Setting(new Link(CreditPackageHandler.class), "Credit packages"));
            settings.put("Credits", new Setting(new Link(CreditSettingsHandler.class), "Credit settings"));
        }

        //		boolean options = false;
        if (isSuperman() || Module.UserItemTypes.enabled(context)) {

            boolean b = !Module.UserItemTypes.enabled(context);
            for (ItemType itemType : ItemType.get(context)) {
                settings.put("Items", new Setting(b, new Link(ItemTypeHandler.class, "edit", "itemType", itemType), itemType.getName() + " settings"));

                if (ItemModule.Options.enabled(context, itemType)) {
                    //					options = true;
                }
            }
        }

        if (Module.ExpiryBots.enabled(context)) {
            settings.put("Items", new Setting(new Link(ExpiryBotHandler.class), "Expiry bots"));
        }

        if (Module.Shopping.enabled(context)) {

            settings.put("Shopping", new Setting(new Link(ShoppingSettingsHandler.class), "Shopping settings"));
            settings.put("Shopping", new Setting(new Link(OrderSettingsHandler.class), "Order settings"));
            settings.put("Addresses", new Setting(new Link(AddressFieldsHandler.class), "Address fields"));

            if (Module.OrderExport.enabled(context)) {
                settings.put("Shopping", new Setting(new Link(ExportOrdersHandler.class), "Export orders"));
                settings.put("Shopping", new Setting(new Link(OrderExportHandler.class), "Order export feeds"));
            }

            settings.put("Shopping", new Setting(new Link(BasketViewHandler.class), "View baskets"));

            //			settings.put("Shopping", new Setting(new Link(OrderExportHandler.class), "Order export"));
            //			settings.put("Shopping", new Setting(new Link(OrderExportSettingsHandler.class), "Order export settings"));

            if (Module.Vouchers.enabled(context)) {
                settings.put("Shopping", new Setting(new Link(VoucherHandler.class), "Vouchers"));
            }

            if (Module.Promotions.enabled(context)) {
                settings.put("Shopping", new Setting(new Link(PromotionHandler.class), "Promotions"));
            }

            if (Module.Delivery.enabled(context)) {
                settings.put("Shopping", new Setting(new Link(DeliverySettingsHandler.class), "Delivery settings"));
                settings.put("Shopping", new Setting(new Link(DeliveryOptionsHandler.class), "Delivery options"));
            }

            if (Module.Currencies.enabled(context)) {
                settings.put("Shopping", new Setting(new Link(CurrenciesHandler.class), "Currency"));
            }
            settings.put("Shopping", new Setting(new Link(PriceSettingsHandler.class), "Price settings"));

            if (Module.OrdersRemoval.enabled(context)) {
                settings.put("Shopping", new Setting(new Link(OrderRemovalHandler.class), "Orders removal"));
            }
        }

        settings.put("Search engine optimisation", new Setting(new Link(SeoHandler.class), "Search engine optimisation"));

        if (isSuperman() || Module.KeywordSuggestions.enabled(context)) {
            settings.put("Search engine optimisation", new Setting(!Module.KeywordSuggestions.enabled(context),
                    new Link(KeywordSuggestionsHandler.class), "Keyword suggestions"));
        }

        if (isSuperman() || Module.GoogleAnalytics.enabled(context)) {
            settings.put("Search engine optimisation", new Setting(!Module.GoogleAnalytics.enabled(context), new Link(GoogleApplicationsHandler.class),
                    "Google Applications"));
        }

        if (Module.GoogleAnalyticsTracking.enabled(context)){
             settings.put("Google Analytics", new Setting(new Link(GoogleAnalyticsEcommerceTrackingHandler.class),
                    "Ecommerce Tracking"));
        }

        if (Module.Sms.enabled(context)) {
            settings.put("Marketing", new Setting(new Link(SmsSettingsHandler.class), "Sms bulletins setup"));
        }

        // ADMIN
        {

            settings.put("Admin", new Setting(new Link(UserHandler.class), "User management"));

            settings.put("Admin", new Setting(new Link(StatsHandler.class), "Software stats"));

            settings.put("Admin", new Setting(new Link(LogHandler.class), "Log"));

        }

        settings.put("Sideboxes", new Setting(new Link(BoxMenuHandler.class), "Sideboxes"));

        // ITEMS
        if (isSuperman() || Module.UserItemTypes.enabled(context)) {

            boolean superonly = !Module.UserItemTypes.enabled(context);
            settings.put("Items", new Setting(superonly, new Link(ItemTypeHandler.class), "Item types"));
//        if (MiscSettings.getInstance(context).isAdvancedMode() || superman) {
//            links.add(new LinkTag(AdvItemEditorHandler.class, null, "Advanced editor", "itemType", itemType));
//        }
            if (isSuperman()) {
                settings.put("Items", new Setting(true, new Link(AdvItemEditorHandler.class), "Advanced editor"));
            }

        }

        settings.put("Items", new Setting(new Link(ExporterHandler.class), "Item exporters"));

        //comments
        if (Module.Comments.enabled(context)) {
            settings.put("Comments", new Setting(new Link(CommentsSettingsHandler.class), "Comments Settings"));
            settings.put("Comments", new Setting(new Link(CommentListHandler.class), "Comments List"));
            if (CommentsSettings.getInstance(context).isModerate()){
                settings.put("Comments", new Setting(new Link(CommentModerationHandler.class), "Comments Moderation"));
            }
        }

        //sitemap add on
        if (Module.SiteMap.enabled(context)){
           settings.put("Sitemap", new Setting(new Link(SitemapSettingsHandler.class), "Sitemap Settings")); 
        }

        //outer login
        if (Module.LoginOuter.enabled(context)){
            if (Module.FacebookLogin.enabled(context)){
               settings.put("Outer Login", new Setting(new Link(FacebookLoginSettingsHandler.class), "Facebook Integration"));  
            }
        }

        if (Module.Facebook.enabled(context)){
            if (Module.FacebookOpenGraphTags.enabled(context)){
               settings.put("Facebook Applications", new Setting(new Link(FacebookOpenGraphTagsHandler.class), "Open Graph Meta Tags"));  
            }
        }


        //iBex booking integration
        if (Module.IBex.enabled(context)) {
            settings.put("IBex", new Setting(new Link(IbexSettingsHandler.class), "iBex Settings"));
        }
        
        return settings;
    }

    @Override
    public Object main() {

        AdminDoc doc = new AdminDoc(context, user, "Settings menu", null);
        doc.setIntro("These are the advanced settings and configuration options for your site.<br/>"
                + "If you are unsure what a particular option does then you should email support for guideance.");
        doc.addBody(new Body() {

            @Override
            public String toString() {

                sb.append("<style>");

                sb.append("table.container { width: 100%; } ");
                sb.append("table.container td { padding: 0 6px; }");
                sb.append("table.settings { margin-bottom: 6px; background: #f7f7f7; width: 100%; "
                        + "font-family: Verdana; font-size: 11px; color: #5a5d5a; border: 1px solid #bec2c5; border-top: 0; } ");
                sb.append("table.settings caption { font-family: Verdana; padding: 6px; font-size: 12px; font-weight: bold; "
                        + "background: #f7f7f7; text-align: left; border: 1px solid #bec2c5; " + "border-bottom: 0; } ");
                sb.append("table.settings a { color: #333333; font-size: 11px; text-decoration: underline;}\n");
                sb.append("table.settings a:hover { color: orange; text-decoration: none;}\n");
                sb.append("table.settings td div { padding-bottom: 5px; } ");
                sb.append("table.settings td div.superman a { color: #cc0000; } ");

                sb.append("</style>");

                sb.append(new TableTag("container"));
                sb.append("<tr><td valign='top' width='50%'>");

                for (int n = 0; n < 2; n++) {

                    for (Map.Entry<String, List<Setting>> entry : getOptions().entryListSet()) {

                        String regex;
                        if (n == 0) {
                            regex = "^[A-M].*";
                        } else {
                            regex = "^[N-Z].*";
                        }

                        if (entry.getKey().matches(regex)) {

                            List<Setting> settings = entry.getValue();
                            Collections.sort(settings);

                            sb.append(new TableTag("settings").setCaption(entry.getKey()));
                            sb.append("<tr><td>");
                            for (Setting setting : settings) {

                                if (setting.isSuperman()) {
                                    sb.append("<div class='superman'>");
                                } else {
                                    sb.append("<div>");
                                }

                                sb.append(setting.getLinkTag());
                                sb.append("</div>");
                            }
                            sb.append("</td></tr></table>");
                        }
                    }

                    sb.append("</td><td valign='top' width='50%'>");
                }

                sb.append("</td></tr></table>");
                return sb.toString();
            }

        });
        return doc;
    }
}
