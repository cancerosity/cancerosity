package org.sevensoft.ecreator.iface.admin.categories;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * 
 * For re-ordering subcategories.
 * 
 * @author sks 18 Jul 2006 20:33:07
 *
 */
@Path("admin-categories-reorder.do")
public class CategoryReorderHandler extends AdminHandler {

	private Category		category;
	private List<Category>	subcategories;

	public CategoryReorderHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() {

		AdminDoc page = new AdminDoc(context, user, "Re-order subcategories", Tab.Categories);
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append("<style>");

				sb.append("ul#subcatorder {");
				sb.append("	list-style-type: none;");
				sb.append("	padding: 0px;");
				sb.append("	font-size: 13px;");
				sb.append("	font-family: Arial, sans-serif;");
				sb.append("}");

				sb.append("ul#subcatorder li {");
				sb.append("	cursor:move;");
				sb.append("	padding: 2px;");
				sb.append("}");

				sb.append("</style>");

				sb.append("<script language='JavaScript' type='text/javascript' src='files/js/reorder/core.js'></script>");
				sb.append("<script language='JavaScript' type='text/javascript' src='files/js/reorder/events.js'></script>");
				sb.append("<script language='JavaScript' type='text/javascript' src='files/js/reorder/css.js'></script>");
				sb.append("<script language='JavaScript' type='text/javascript' src='files/js/reorder/coordinates.js'></script>");
				sb.append("<script language='JavaScript' type='text/javascript' src='files/js/reorder/drag.js'></script>");
				sb.append("<script language='JavaScript' type='text/javascript' src='files/js/reorder/dragsort.js'></script>");
				sb.append("<script language='JavaScript' type='text/javascript' src='files/js/reorder/cookies.js'></script>");

				sb.append("<script language='JavaScript' type='text/javascript'><!--\n");
				sb.append("	var dragsort = ToolMan.dragsort()\n");
				sb.append("	var junkdrawer = ToolMan.junkdrawer()\n");
				sb.append("	window.onload = function() {\n");
				sb.append("		junkdrawer.restoreListOrder('phonetic3')\n");
				sb.append("		dragsort.makeListSortable(document.getElementById('subcatorder'),\n");
				sb.append("				verticalOnly, saveOrder)\n");
				sb.append("	}\n");
				sb.append("	function verticalOnly(item) {item.toolManDragGroup.verticalOnly() }\n");
				sb.append("	function saveOrder(item) { }\n");
				sb.append("	//-->\n");
				sb.append("</script>\n");

				sb.append(new FormTag(CategoryReorderHandler.class, "save", "post"));
				sb.append(new HiddenTag("category", category));

				sb.append(new TableTag(null, "center"));
				sb.append("<tr><td>");

				sb.append("<ul id='subcatorder'>");

				for (Category child : category.getChildren()) {

					sb.append("<li>");
					sb.append(new HiddenTag("subcategories", child));
					sb.append(new ImageTag("files/graphics/admin/drag.gif") + " " + child.getName() + "</li>");

				}

				sb.append("</ul>");

				sb.append("</td></tr>");
				sb.append("<tr><td align='center'>");

				SubmitTag tag = new SubmitTag("Save order");
				//				tag.setOnClick(" this.form.elements['order'].value = ToolMan.junkdrawer().serializeList('subcatorder'); ");
				sb.append(tag);

				//				ButtonTag tag2 = new ButtonTag("Save order");
				//				tag.setOnClick(" window.alert(ToolMan.junkdrawer().serializeList('subcatorder')); ");
				//				sb.append(tag2);

				sb.append("</form>");

				sb.append("</td></tr>");
				sb.append("</table>");

				return sb.toString();
			}

		});
		return page;
	}

	public Object save() throws ServletException {

		int n = 1;
		for (Category category : subcategories) {

			category.setPosition(n);
			category.save();

			n++;
		}

		return new CategoryHandler(context, category).edit();
		// return HttpServletResponse.SC_OK;
	}
}
