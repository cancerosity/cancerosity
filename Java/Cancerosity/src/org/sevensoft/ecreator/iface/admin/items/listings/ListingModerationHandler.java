package org.sevensoft.ecreator.iface.admin.items.listings;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.CrossGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.TickGif;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemSearcher;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 27-Feb-2006 16:28:46
 *
 */
@Path("admin-listings-moderation.do")
public class ListingModerationHandler extends AdminHandler {

	private Item	item;

	public ListingModerationHandler(RequestContext context) {
		super(context);
	}

	public Object approve() throws ServletException {

		if (item == null) {
			return main();
		}

		try {
			item.getListing().approved();
		} catch (EmailAddressException e) {
			addError(e);
		} catch (SmtpServerException e) {
			addError(e);
		}

		return main();
	}

	public Object main() throws ServletException {

		ItemSearcher searcher = new ItemSearcher(context);
		searcher.setAwaitingModeration(true);
		searcher.setVisibleOnly(false);
		searcher.setIncludeHidden(true);
        searcher.setSearchableOnly(false);

        final List<Item> items = searcher.getItems();

		if (items.isEmpty()) {

			AdminDoc doc = new AdminDoc(context, user, "Moderation queue", null);
			doc.addBody(new Body() {

				@Override
				public String toString() {

					sb.append(new TableTag("simplelinks", "center"));
					sb.append("<tr><th align='center'>You have no listings currently awaiting moderation</th></tr>");
					sb.append("</table>");

					return sb.toString();
				}
			});
			return doc;

		} else {

			AdminDoc doc = new AdminDoc(context, user, "Moderation queue", null);
			doc.addBody(new Body() {

				@Override
				public String toString() {

					sb.append(new AdminTable("Listings awaiting moderation"));

					sb.append("<tr>");
					sb.append("<th>Id</th>");
					sb.append("<th>Date</th>");
					sb.append("<th>Type</th>");
					sb.append("<th>Name</th>");
                    sb.append("<th>Payment done</th>");
					sb.append("<th>Approve</th>");
					sb.append("</tr>");

					for (Item item : items) {

						sb.append("<tr>");

						// id
						sb.append("<td>" + new LinkTag(ItemHandler.class, "edit", item.getIdString(), "item", item) + "</td>");

						// date created
						sb.append("<td>" + item.getDateCreated().toDateString() + "</td>");

						// item type
						sb.append("<td>" + item.getItemTypeName() + "</td>");

						// name and categories
						sb.append("<td>" + new LinkTag(ItemHandler.class, "edit", item.getName(), "item", item));

						if (item.hasCategories()) {
							sb.append("<br/>");
							sb.append(item.getCategoryNames());
						}

						sb.append("</td>");

                        //need to check was the listing free or not

                        // payment for the listing have been done or not
                        sb.append("<td>" + item.getListingPaymentDone() + "</td>");

						// approval link
						sb.append("<td>");
						sb.append(new LinkTag(ListingModerationHandler.class, "approve", new TickGif(), "item", item)
								.setConfirmation("Are you sure you want to approve this listing"));

						sb.append(" ");

						sb.append(new LinkTag(ListingModerationHandler.class, "reject", new CrossGif(), "item", item)
								.setConfirmation("Are you sure you want to reject this listing"));
						sb.append("</td>");

						sb.append("</tr>");
					}

					sb.append("</table>");
					return sb.toString();
				}
			});
			return doc;
		}
	}

	public Object reject() throws ServletException {

		if (item == null) {
			return main();
		}

		try {
			item.getListing().rejected();
		} catch (EmailAddressException e) {
			addError(e);
		} catch (SmtpServerException e) {
			addError(e);
		}

		return main();
	}
}
