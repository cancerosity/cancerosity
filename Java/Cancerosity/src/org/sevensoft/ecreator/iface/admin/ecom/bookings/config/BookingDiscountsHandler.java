package org.sevensoft.ecreator.iface.admin.ecom.bookings.config;

import javax.servlet.ServletException;

import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.bookings.BookingDiscount;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 18 Mar 2007 16:58:22
 *
 */
@Path("admin-bookings-discounts.do")
public class BookingDiscountsHandler extends AdminHandler {

	private Item		item;
	private BookingDiscount	discount;
	private int			nights;
	private Amount		amount;

	public BookingDiscountsHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		if (item == null) {
			return index();
		}

		return new ActionDoc(context, "A new discount has been created", new Link(BookingSetupHandler.class, null, "item", item));
	}

	@Override
	public Object main() throws ServletException {

		if (discount == null) {
			return index();
		}

		item = discount.getItem();

		AdminDoc doc = new AdminDoc(context, user, "Edit discount", Tab.getItemTab(item));
		doc.setMenu(new EditItemMenu(item));
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update discount"));
				sb.append(new ButtonTag(BookingDiscountsHandler.class, null, "Return to discounts", "item", item));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Nights", "The name of this option.", new TextTag(context, "nights", discount.getNights(), 4)));

				sb.append(new AdminRow("Discount", "Enter the discount to apply.", new TextTag(context, "amount", discount.getAmount(), 12)));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(BookingDiscountsHandler.class, "save", "POST"));
				sb.append(new HiddenTag("discount", discount));

                commands();
				general();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (discount == null) {
			return main();
		}

		test(new RequiredValidator(), "name");
		test(new RequiredValidator(), "type");

		if (hasErrors()) {
			return main();
		}

		discount.setNights(nights);
		discount.setAmount(amount);
		discount.save();

		clearParameters();

		return new ActionDoc(context, "The discount has been updated", new Link(BookingDiscountsHandler.class, null, "discount", discount));
	}

}
