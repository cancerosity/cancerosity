package org.sevensoft.ecreator.iface.admin.extras.calculators;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.DashboardHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.extras.calculators.Calculator;
import org.sevensoft.ecreator.model.extras.calculators.CalculatorField;
import org.sevensoft.ecreator.model.extras.calculators.CalculatorFieldOption;
import org.sevensoft.ecreator.model.extras.calculators.CalculatorField.Type;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.db.positionable.PositionRender;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 07-Jul-2005 12:39:17
 * 
 */
@Path("admin-calculators-fields.do")
public class CalculatorFieldHandler extends AdminHandler {

	private int			width;
	private CalculatorField	field;
	private Calculator	calculator;
	private String		name;
	private String		variable;
	private String		expression;
	private Type		type;
	private String		description;
	private String		newOptions;
	private String		section;
	private int			cellsPerRow;
	private boolean		hideLabel;
	private int			cellSpan;
	private String	defaultValue;

	public CalculatorFieldHandler(RequestContext context) {
		super(context);
	}

	public CalculatorFieldHandler(RequestContext context, CalculatorField field) {
		super(context);
		this.field = field;
	}

	//	public Object create() throws ServletException {
	//
	//		if (calculator == null)
	//			return new DashboardHandler(context).main();
	//
	//		field = calculator.addField("new field");
	//		return new CalculatorHandler(context, calculator).edit();
	//	}

	public Object delete() throws ServletException {

		if (field == null)
			return main();

		field.delete();
		addMessage("The field called '" + field.getName() + "' has been deleted.");
		return new CalculatorHandler(context, field.getCalculator()).edit();
	}

	public Object main() throws ServletException {

		if (field == null)
			return new DashboardHandler(context).main();

		AdminDoc page = new AdminDoc(context, user, "Edit calculator field", Tab.Categories);
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update field"));
				sb.append(new ButtonTag(CalculatorFieldHandler.class, "delete", "Delete field", "field", field)
						.setConfirmation("If you delete this field it cannot be restored. Continue?"));
				sb.append(new ButtonTag(CalculatorHandler.class, "edit", "Return to calculator", "calculator", field.getCalculator()));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new TableTag("form",1,0).setCaption("Field"));

				sb.append("<tr><th width='300'><b>Name</b><br/>Enter the name of this form field</th><td>"
						+ new TextTag(context, "name", field.getName(), 40) + new ErrorTag(context, "name", "<br/>") + "</td></tr>");

				sb.append("<tr><th width='300'><b>Description</b><br/>Enter an optional brief description about this field</th><td>"
						+ new TextAreaTag(context, "description", field.getDescription(), 50, 3) + new ErrorTag(context, "description", "<br/>")
						+ "</td></tr>");

				sb.append("<tr><th width='300'><b>Section</b><br/>Enter the section to group fields together</th><td>"
						+ new TextTag(context, "section", field.getSection()) + new ErrorTag(context, "section", "<br/>") + "</td></tr>");

				SelectTag typeTag = new SelectTag(context, "type", field.getType());
				typeTag.addOptions(CalculatorField.Type.values());
				sb.append("<tr><th width='300'><b>Type</b><br/>" + "The type of field.</th><td>" + typeTag + "</td></tr>");

				sb.append("<tr><th width='300'><b>Variable</b><br/>The variable name if this field is to be used in an expression.</th><td>"
						+ new TextTag(context, "variable", field.getVariable(), 40) + "</td></tr>");

				if (field.getType() == CalculatorField.Type.Expression) {

					sb.append("<tr><th width='300'><b>Expression</b><br/>The formula to calculate this field.</th><td>"
							+ new TextTag(context, "expression", field.getExpression(), 60) + "</td></tr>");
				}

				if (field.getType() == CalculatorField.Type.Numeric) {

					sb.append("<tr><th width='300'><b>Width</b><br/></th><td>" + new TextTag(context, "width", field.getWidth(), 4) + "</td></tr>");
					sb.append("<tr><th width='300'><b>Defalut value</b></th><td>" + new TextTag(context, "defaultValue", field.getDefaultValue(), 8)
							+ "</td></tr>");
				}

				sb.append("<tr><td width='300'><b>Cell span</b><br/></td>");
				sb.append("<td>" + new TextTag(context, "cellSpan", field.getCellSpan(), 4) + "</td></tr>");

				sb.append("<tr><td width='300'><b>Cells per row</b><br/></td>");
				sb.append("<td>" + new TextTag(context, "cellsPerRow", field.getCellsPerRow(), 4) + "</td></tr>");

				sb.append("<tr><td width='300'><b>Hide label</b><br/></td>");
				sb.append("<td>" + new BooleanRadioTag(context, "hideLabel", field.isHideLabel()) + "</td></tr>");

				sb.append("</table>");
			}

			private void options() {

				sb.append(new TableTag("form",1,0).setCaption("Options"));

				sb.append("<tr>");
				sb.append("<th width='120'>Position</th>");
				sb.append("<th>Text</th>");
				sb.append("<th>Value</th>");
				sb.append("<th>Expression</th>");
				sb.append("<th width='80'>Edit</th>");
				sb.append("<th width='80'>Delete</th>");
				sb.append("</tr>");

				PositionRender pr = new PositionRender(CalculatorFieldOptionHandler.class, "option");
				for (CalculatorFieldOption option : field.getOptions()) {

					sb.append("<tr>");

					sb.append("<td width='120'>" + pr.render(option) + "</td>");
					sb.append("<td>" + option.getText() + "</td>");
					sb.append("<td>" + (option.hasValue() ? option.getValue() : "") + "</td>");
					sb.append("<td>" + (option.hasExpression() ? option.getExpression() : "") + "</td>");
					sb.append("<td width='80'>" + new ButtonTag(CalculatorFieldOptionHandler.class, null, "Edit", "option", option) + "</td>");
					sb.append("<td width='80'>" + new ButtonTag(CalculatorFieldOptionHandler.class, "delete", "Delete", "option", option) + "</td>");
					sb.append("</tr>");
				}

				sb.append("<tr><td colspan='6'>Add new options<br/>");
				sb.append(new TextAreaTag(context, "newOptions", 40, 4));
				sb.append("</td></tr>");

				sb.append("</table>");

			}

			@Override
			public String toString() {

				sb.append(new FormTag(CalculatorFieldHandler.class, "save", "POST"));
				sb.append(new HiddenTag("field", field));

                commands();
				general();
				if (field.useOptions())
					options();

				commands();

				sb.append("</form>");
				return sb.toString();
			}
		});
		return page;
	}

	public Object moveDown() throws ServletException {

		if (field == null)
			return main();

		calculator = field.getCalculator();
		EntityUtil.moveDown(field, calculator.getFields());

		return new CalculatorHandler(context, calculator).edit();
	}

	public Object moveUp() throws ServletException {

		if (field == null)
			return main();

		List<CalculatorField> fields = new ArrayList<CalculatorField>(field.getCalculator().getFields());
		/*
		 * Filter out attributes from other sections
		 */
		CollectionsUtil.filter(fields, new Predicate<CalculatorField>() {

			public boolean accept(CalculatorField e) {

				if (field.hasSection())
					return field.getSection().equalsIgnoreCase(e.getSection());
				else
					return !e.hasSection();
			}
		});

		/*
		 * If attribute is at the top of the section then we shall move the entire section up, by swapping this attribute with the lead attribute of the section above
		 */
		if (fields.indexOf(field) == 0) {

			fields = CalculatorField.getLead(field.getCalculator().getFields());
			EntityUtil.moveUp(field, fields);

		} else {
			EntityUtil.moveUp(field, fields);
		}

		return new CalculatorHandler(context, field.getCalculator()).edit();
	}

	public Object save() throws ServletException {

		if (field == null)
			return main();

		test(new RequiredValidator(), "name");
		if (hasErrors())
			return main();

		field.setVariable(variable);
		field.setDefaultValue(defaultValue);
		field.setName(name);
		field.setExpression(expression);
		if (type != null)
			field.setType(type);
		field.setWidth(width);
		field.setDescription(description);
		field.setSection(section);
		field.setHideLabel(hideLabel);
		field.setCellSpan(cellSpan);
		field.setCellsPerRow(cellsPerRow);
		field.save();

		if (newOptions != null)
			field.addOptions(StringHelper.explodeStrings(newOptions, "\n"));

		clearParameters();
		addMessage("Changes saved.");
		return main();
	}
}
