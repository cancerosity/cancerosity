package org.sevensoft.ecreator.iface.admin.extras.facebook;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.media.images.panels.ImageAdminPanel;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.frontend.ErrorDoc;
import org.sevensoft.ecreator.model.extras.facebook.FacebookOpenGraphTags;
import org.sevensoft.ecreator.model.extras.facebook.FacebookApplicationSettings;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.media.images.ImageUtil;
import org.sevensoft.ecreator.model.media.images.ImageLimitException;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.List;

/**
 * User: alchemist
 * Date: 05.03.2013
 */
@Path("admin-facebook-open-graph.do")
public class FacebookOpenGraphTagsHandler extends AdminHandler {

    private transient FacebookOpenGraphTags facebookOpenGraphTags;
    private transient FacebookApplicationSettings settings;

    private String appId;
    private String type;

    // images support
    private List<Upload> imageUploads;
    private List<String> imageUrls;
    private String imageFilename;

    public FacebookOpenGraphTagsHandler(RequestContext context) {
        super(context);
        this.facebookOpenGraphTags = FacebookOpenGraphTags.getInstance(context);
        this.settings = FacebookApplicationSettings.getInstance(context);
    }

    public Object main() throws ServletException {
        if (!Module.FacebookOpenGraphTags.enabled(context)) {
            return index();
        }
        AdminDoc doc = new AdminDoc(context, user, "Facebook Open Graph Meta Tags Settings", null);
        doc.addBody(new Body() {

            @Override
            public String toString() {
                sb.append(new FormTag(FacebookOpenGraphTagsHandler.class, "save", "multi"));
                sb.append(new HiddenTag("facebookOpenGraphTags", facebookOpenGraphTags));

                commands();

                general();
                images();

                commands();

                sb.append("</form>");

                return sb.toString();
            }

            private void commands() {
                sb.append("<div align='center' class='actions'>");

                sb.append(new SubmitTag("Update"));
                sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return"));

                sb.append("</div>");
            }

            private void general() {
                sb.append(new AdminTable("General"));

                sb.append(new AdminRow("Application ID", "Firstly you�ve to setup a facebook application to get the application id. You can " + new LinkTag("http://developers.facebook.com/setup/", "setup application from here.").setTarget("_blank"),
                        new TextTag(context, "appId", settings.getAppId())));

                sb.append(new AdminRow("Built-in Object Types", "Default value",
                        new TextTag(context, "type", facebookOpenGraphTags.getType()).setDisabled(true)));

                sb.append("<table>");
            }

            private void images() {
                sb.append(new ImageAdminPanel(context, facebookOpenGraphTags));
            }


        });
        return doc;
    }

    public Object save() throws ServletException {
        if (!Module.FacebookOpenGraphTags.enabled(context)) {
            return index();
        }

        settings.setAppId(appId);
        settings.save();

        try {
            ImageUtil.save(context, facebookOpenGraphTags, imageUploads, imageUrls, imageFilename, true);
        } catch (ImageLimitException e) {
            e.printStackTrace();
        } catch (IOException e) {
            return new ErrorDoc(context, "The image you are attempting to upload is in CYMK colour mode. " +
                    "This is unsopported currently, please use a photo editing application (e.g. Photoshop) to convert the file to RGB colour. For more information or assistance please contact support.");
        }

        addMessage("Facebook Login settings have been updated");
        return main();
    }
}
