package org.sevensoft.ecreator.iface.admin.items.modules.relations;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemMenu;
import org.sevensoft.ecreator.iface.admin.items.search.ItemSearchAjaxTag;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.relations.RelationGroup;
import org.sevensoft.ecreator.model.items.relations.RelationsModule;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.DivTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 28 Mar 2007 15:58:47
 *
 */
@Path("admin-relations.do")
public class RelationHandler extends AdminHandler {

	private Item		item;
	private Item		relation;
	private RelationGroup	relationGroup;

	public RelationHandler(RequestContext context) {
		super(context);
	}

	public Object add() throws ServletException {

		if (item == null) {
			return main();
		}

		if (relationGroup == null) {
			return main();
		}

		relationGroup.addRelation(item, relation);

		return new ActionDoc(context, "This item has been added as an relation", new Link(RelationHandler.class, null, "item", item));
	}

	public Object delete() throws ServletException {

		if (item == null || relation == null || relationGroup == null) {
			return main();
		}

		relationGroup.removeRelation(item, relation);
		return new ActionDoc(context, "This relation has been removed", new Link(RelationHandler.class, null, "item", item));
	}

	@Override
	public Object main() throws ServletException {

		if (item == null) {
			return index();
		}

		setAttribute("view", item.getUrl());

		AdminDoc doc = new AdminDoc(context, user, "Relations: " + item.getName() + " #" + item.getId(), Tab.getItemTab(item));
		doc.setMenu(new EditItemMenu(item));
		doc.addBody(new Body() {

			private void group(RelationGroup relationGroup) {

				sb.append(new AdminTable(relationGroup.getName()));

				sb.append("<tr>");
				sb.append("<td>Name</td>");
				sb.append("<td width='10'> </td>");
				sb.append("</tr>");

				for (Item relation : relationGroup.getRelations(item)) {

					sb.append("<tr>");

					sb.append("<td>" + new LinkTag(ItemHandler.class, "edit", new SpannerGif(), "item", relation) + " " + relation.getName()
							+ "</td>");

					sb.append("<td width='10'>"
							+ new LinkTag(RelationHandler.class, "delete", new DeleteGif(), "item", item, "relation", relation, "relationGroup",
									relationGroup) + "</td>");

					sb.append("</tr>");

				}

				String resultsId = "relation_results_" + relationGroup.getId();

				Link link = new Link(RelationHandler.class, "add", "item", item, "relationGroup", relationGroup);
				ItemSearchAjaxTag ajaxTag = new ItemSearchAjaxTag(context, resultsId, link, "relation");

				sb.append("<tr><td colspan='2'>Search to add an relation: " + ajaxTag + new DivTag().setId(resultsId) + "</div></td></tr>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				RelationsModule module = item.getItemType().getRelationsModule();
				for (RelationGroup group : module.getGroups()) {
					group(group);
				}

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

}
