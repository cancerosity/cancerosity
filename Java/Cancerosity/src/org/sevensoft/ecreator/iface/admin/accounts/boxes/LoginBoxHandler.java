package org.sevensoft.ecreator.iface.admin.accounts.boxes;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.accounts.login.boxes.LoginBox;
import org.sevensoft.ecreator.model.accounts.login.boxes.LoginBox.DisplayType;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 24 Jul 2006 23:59:35
 *
 */
@Path("admin-members-loginbox.do")
public class LoginBoxHandler extends BoxHandler {

	private LoginBox		box;

	private DisplayType	displayType;

	public LoginBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new TableTag("form",1,0).setCaption("Login box specifics"));

		SelectTag displayTypeTag = new SelectTag(context, "displayType", box.getDisplayType());
		displayTypeTag.addOptions(DisplayType.values());

		sb.append("<tr><td width='300'><b>Display type</b><br/>When to show the login box</td>");
		sb.append("<td>" + displayTypeTag + "</td></tr>");

		sb.append("</table>");
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {

		box.setDisplayType(displayType);
	}

}
