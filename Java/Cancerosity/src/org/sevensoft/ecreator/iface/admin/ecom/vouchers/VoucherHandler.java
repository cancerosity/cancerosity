package org.sevensoft.ecreator.iface.admin.ecom.vouchers;

import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemHandler;
import org.sevensoft.ecreator.iface.admin.items.listings.ListingPackageHandler;
import org.sevensoft.ecreator.iface.admin.ecom.ShoppingMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.ecom.vouchers.Voucher;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.listings.ListingPackage;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.db.EntityObject;

import javax.servlet.ServletException;

/**
 * @author sks 13-Jul-2005 16:24:50
 * 
 */
@Path("admin-vouchers.do")
public class VoucherHandler extends AdminHandler {

	private Date	startDate, endDate;
	private Voucher	voucher;
	private int		qualifyingQty;
	private Money	qualifyingAmount;
	private String	name;
	private Amount	discount;
	private String	code;
	private boolean	freeDelivery;
	private boolean	firstOrderOnly;
	private int		maxUsesPerMember;
	private String	addItem;
	private String	addExtraItem;
	private Item	item;
	private Item	extraItem;
	private boolean	specificItemsVoucher;
	private boolean	getOneFree;
	private boolean	getExtraItem;
    private ListingPackage	listingPackage;
    private String addPackage;

	public VoucherHandler(RequestContext context) {
		super(context);
	}

	public Object create() {

		voucher = new Voucher(context, "new voucher");

		return edit();
	}

	public Object delete() {

		if (voucher == null)
			return main();

		voucher.delete();

		return main();
	}

	public Object edit() {

		if (voucher == null)
			return main();

		AdminDoc page = new AdminDoc(context, user, "Edit voucher", Tab.Orders);
		page.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update voucher"));
				sb.append(new ButtonTag(VoucherHandler.class, "delete", "Delete voucher", "voucher", voucher)
						.setConfirmation("Are you sure you want to delete this voucher?"));
				sb.append(new ButtonTag(VoucherHandler.class, null, "Return to menu"));

				sb.append("</div>");
			}

			private void details() {

				sb.append(new AdminTable("Voucher details"));
				sb.append(new AdminRow("Name", "Enter a name for this voucher, such as 'Christmas buyer discount: 10% off total order'", new TextTag(
						context, "name", voucher.getName(), 30) + " " + new ErrorTag(context, "name", "<br/>")));

				sb.append(new AdminRow("Code", "Change the code to enter for this code.", new TextTag(context, "code", voucher.getCode(), 12)));

				sb.append(new AdminRow("Discount", "How much to reduce this order by.", new TextTag(context, "discount", voucher.getDiscount())));

                String startDate = null, endDate = null;
                if (voucher.getStartDate() != null) {
                    startDate = voucher.getStartDate().toEditString();
                }
                if (voucher.getEndDate() != null) {
                    endDate = voucher.getEndDate().toEditString();
                }
                sb.append(new AdminRow("Valid dates", "Enter the dates this voucher is valid from, or leave blank if you "
                        + "want to remove it manually when it is no longer available. ",
                        new TextTag(context, "startDate", startDate, 12) + " " +
                                new TextTag(context, "endDate", endDate, 12)));

                sb.append(new AdminRow("Max uses", "If you enter a number here bigger than zero, then that " +
                        "is the maximum number of times a single customer can use this voucher.",
                        new TextTag(context, "maxUsesPerMember", voucher.getMaxUsesPerMember(), 4)));

				sb.append("</table>");
			}

            private void specItems() {
                sb.append(new AdminTable("Items"));

                sb.append(new AdminRow(
                        "Free delivery",
                        "If you tick this option then this voucher will be for free delivery only up to the maximum you specify in the discount box. "
                                + "Eg, if you set a discount of \2436 but the customer chooses delivery of \2433 then their discount will only be \2433. "
                                + "Or if their delivery was \2438 then they would still have \2432 left to pay.", new BooleanRadioTag(context,
                                "freeDelivery", voucher.isFreeDelivery())));

				sb.append(new AdminRow("Qualifying amount", "Enter the amount customers must spend for this voucher to be valid.", new TextTag(context,
						"qualifyingAmount", voucher.getQualifyingAmount())));

				sb.append(new AdminRow("Qualifying qty", "Enter the number of items a customer must order for this voucher to be valid.", new TextTag(
						context, "qualifyingQty", voucher.getQualifyingQty(), 5)));

				sb.append(new AdminRow("First order only",
						"If you set this to yes then this voucher will only be valid to a customer on their first order.", new BooleanRadioTag(
								context, "firstOrderOnly", voucher.isFirstOrderOnly())));

                sb.append(new AdminRow("Specific items voucher", "If set to 'No' it'll be possible to use this voucher for all items. "
                        + "If set to 'Yes' voucher will be used only for specific items", new BooleanRadioTag(context, "specificItemsVoucher",
                        voucher.isSpecificItemsVoucher())));

                for (Item item : voucher.getItems()) {
                    sb.append(new AdminRow("Item", "Existing valid item", new LinkTag(ItemHandler.class, "edit", new SpannerGif(), "item", item) +
                            " " + item.getName() + " "
                            + new ButtonTag(VoucherHandler.class, "removeItem", "Remove", "voucher", voucher, "item", item)));
                }

                sb.append(new AdminRow("Item", "Add item by entering the ID or reference.", new TextTag(context, "addItem", 12)));

                sb.append(new AdminRow("Buy one get one free option", "If set to 'Yes' the item with 0 price will be added with"
                        + "the voucher into the basket", new BooleanRadioTag(context, "getOneFree", voucher.isGetOneFree())));

                if (voucher.isGetOneFree()) {
                    sb.append(new AdminRow("Get extra item", "If set to 'Yes' the item with 0 price from list below will be added with "
                            + "the voucher into the basket", new BooleanRadioTag(context, "getExtraItem", voucher.isGetExtraItem())));

                    if (voucher.isGetExtraItem()) {
                        sb.append(new AdminRow("Extra item", "Add extra item by entering the ID or reference.", new TextTag(context,
                                "addExtraItem", 12)));

                        for (Item extraItem : voucher.getExtraItems()) {
                            sb.append(new AdminRow("Extra item", "Existing valid extra item", extraItem.getName()
                                    + " "
                                    + new ButtonTag(VoucherHandler.class, "removeExtraItem", "Remove", "voucher", voucher, "extraItem",
                                    extraItem)));
                        }
                    }
                }

                sb.append("</table>");
            }

            private void specListingPackages() {

                sb.append(new AdminTable("Listing packages"));

                for (ListingPackage listingPackage : voucher.getListingPackages()) {
                    sb.append(new AdminRow("Listing package", "Existing valid package",
                            new LinkTag(ListingPackageHandler.class, "edit", new SpannerGif(), "listingPackage", listingPackage) +
                                    " " + listingPackage.getName() + " "
                                    + new ButtonTag(VoucherHandler.class, "removeListingPackage", "Remove", "voucher", voucher, "listingPackage", listingPackage)));
                }

                SelectTag addPackage = new SelectTag(context, "addPackage");
                addPackage.addOptions(ListingPackage.get(context));
                addPackage.setAny("- Choose listing package -");
                sb.append(new AdminRow("Add listing package", "Add listing package by entering the ID.", addPackage));
                
                sb.append("</table>");

            }

			@Override
			public String toString() {

				sb.append(new FormTag(VoucherHandler.class, "save", "POST"));
				sb.append(new HiddenTag("voucher", voucher));

				commands();
				details();
                specItems();
                specListingPackages();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return page;
	}

	@Override
	public Object main() {

		AdminDoc page = new AdminDoc(context, user, "Vouchers", Tab.Orders);
		page.setMenu(new ShoppingMenu());
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Edit voucher"));
				sb.append(new FormTag(VoucherHandler.class, "edit", "POST"));

				List<Voucher> vouchers = Voucher.get(context);
				if (vouchers.size() > 0) {

					sb.append("<tr><th>Choose the voucher you wish to edit and click the edit button.</th></tr>");

					SelectTag tag = new SelectTag(null, "voucher");
					tag.addOptions(vouchers);

					sb.append("<tr><td>" + tag + " " + new SubmitTag("Edit") + "</td></tr>");
				}

				sb.append("<tr><td>" + new ButtonTag(VoucherHandler.class, "create", "Create a new voucher") + "</td></tr>");
				sb.append("</form>");
				sb.append("</table>");

				return sb.toString();
			}
		});
		return page;
	}

	public Object removeExtraItem() throws ServletException {

		if (voucher == null || extraItem == null) {
			return main();
		}

		voucher.removeExtraItem(extraItem);
		clearParameters();
		return edit();
	}

	public Object removeItem() throws ServletException {

		if (voucher == null || item == null) {
			return main();
		}

		voucher.removeItem(item);
		clearParameters();
		return edit();
	}

    public Object removeListingPackage() throws ServletException {

		if (voucher == null || listingPackage == null) {
			return main();
		}

		voucher.removeListingPackage(listingPackage);
		clearParameters();
		return edit();
	}

	public Object save() {

		if (voucher == null)
			return main();

		test(new RequiredValidator(), "name");
		if (hasErrors())
			return edit();

		voucher.setQualifyingAmount(qualifyingAmount);
		voucher.setQualifyingQty(qualifyingQty);
		voucher.setName(name);
		voucher.setFirstOrderOnly(firstOrderOnly);
		voucher.setStartDate(startDate);
		voucher.setEndDate(endDate);
		voucher.setDiscount(discount);
		voucher.setCode(code);
		voucher.setMaxUsesPerMember(maxUsesPerMember);
		voucher.setFreeDelivery(freeDelivery);
		voucher.setSpecificItemsVoucher(specificItemsVoucher);
		voucher.setGetOneFree(getOneFree);
		voucher.setGetExtraItem(getExtraItem);

		if (addItem != null) {
			Item item = EntityObject.getInstance(context, Item.class, addItem);

			if (item == null) {
				item = Item.getByRef(context, null, addItem);
			}

			if (item != null) {
				voucher.addItem(item);
			}
		}

		if (addExtraItem != null) {
			Item extraItem = EntityObject.getInstance(context, Item.class, addExtraItem);

			if (extraItem == null) {
				extraItem = Item.getByRef(context, null, addExtraItem);
			}

			if (extraItem != null) {
				voucher.addExtraItem(extraItem);
			}
		}

        if (addPackage != null) {
			ListingPackage listingPackage = EntityObject.getInstance(context, ListingPackage.class, addPackage);

			if (listingPackage != null) {
				voucher.addListingPackage(listingPackage);
			}
		}

		voucher.save();

		addMessage("Voucher updated");
		clearParameters();
		return edit();
	}
}
