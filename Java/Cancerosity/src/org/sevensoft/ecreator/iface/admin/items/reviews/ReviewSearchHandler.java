package org.sevensoft.ecreator.iface.admin.items.reviews;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.items.reviews.Review;
import org.sevensoft.ecreator.model.items.reviews.ReviewSearcher;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

/**
 * @author sks 24 Mar 2007 10:12:51
 *
 */
@Path("admin-reviews-search.do")
public class ReviewSearchHandler extends AdminHandler {

	private static final int		ResultsPerPage	= 40;
	private boolean				approvedOnly;
	private String				author;
	private String				name;
	private transient List<Review>	reviews;
	private transient int			total;
	private int					page;
	private Results				results;

	public ReviewSearchHandler(RequestContext context) {
		super(context);
	}

	/**
	 * 
	 */
	protected Link getLink() {
		return null;
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Reviews search", Tab.Extras);
		doc.setIntro("Here you can search for reviews on your site");
		doc.addBody(new Body() {

			private void criteria() {

				sb.append(new FormTag(ReviewSearchHandler.class, "search", "POST"));

				sb.append(new AdminTable("Advanced search"));

				sb.append(new AdminRow("Author", "Enter the author name.", new TextTag(context, "author", 33)));

				sb
						.append(new AdminRow("Approved only", "Only show reviews that have been approved.", new CheckTag(context, "noCategory",
								true, false)));

				sb.append("<tr><td class='command' colspan='2'>" + new SubmitTag("Search for reviews") + "</td></tr>");

				sb.append("</table>");
				sb.append("</form>");
			}

			private void results() {

				sb.append("<div class='pagination'>" + new ResultsControl(context, results, getLink()) + "</div>");

				sb.append(new AdminTable("Search results"));

				sb.append("</tr>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				criteria();
				if (reviews != null && reviews.size() > 0) {
					results();
				}

				return sb.toString();
			}
		});
		return doc;

	}

	public Object search() throws ServletException {

		ReviewSearcher searcher = new ReviewSearcher(context);
		searcher.setApprovedOnly(approvedOnly);
		searcher.setName(name);
		searcher.setAuthor(author);

		total = searcher.size();

		results = new Results(total, page, ResultsPerPage);

		searcher.setStart(results.getStartIndex());
		searcher.setLimit(ResultsPerPage);

		reviews = searcher.execute();

		return main();
	}

}
