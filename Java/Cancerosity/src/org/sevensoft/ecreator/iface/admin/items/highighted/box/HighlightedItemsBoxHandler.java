package org.sevensoft.ecreator.iface.admin.items.highighted.box;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.iface.admin.search.SearchEditHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.highlighted.HighlightMethod;
import org.sevensoft.ecreator.model.items.highlighted.boxes.HighlightedItemsBox;
import org.sevensoft.ecreator.model.items.highlighted.boxes.HighlightedItemsBox.Style;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 24 Jul 2006 23:04:42
 *
 */
@Path("admin-boxes-highlighted-items.do")
public class HighlightedItemsBoxHandler extends BoxHandler {

	private HighlightedItemsBox	box;
	private boolean			appletLinkToCategory;
	private HighlightMethod		method;
	private int				limit;
	private boolean			imagesOnly;
	private Markup			markup;
	private String			searchLinkText;
	private String			appletHeadlineHighlightColor;
	private String			appletHeadlineFamily;
	private String			appletHeadlineColor;
	private String			appletBackgroundColor;
	private String			appletHeadlineSize;
	private String			appletSummarySize;
	private String			appletSummaryFamily;
	private String			appletSummaryHighlightColor;
	private String			appletSummaryColor;
	private Style			style;
    private String scrollerHeight;
    private String scrollerSpeed;

	public HighlightedItemsBoxHandler(RequestContext context) {
		super(context);
	}

	private void applet(StringBuilder sb) {

		sb.append(new AdminTable("Applet options"));

		sb.append(new AdminRow("Background color", "This is the color of the background of the applet", new TextTag(context, "appletBackgroundColor", box
				.getAppletBackgroundColor(), 12)));

		sb.append(new AdminRow("Name font family", "The font used by the name field", new TextTag(context, "appletHeadlineFamily", box
				.getAppletHeadlineFamily(), 12)));

		sb.append(new AdminRow("Name font size", "The size of the font used by the name field", new TextTag(context, "appletHeadlineSize", box
				.getAppletHeadlineSize(), 5)));

		sb.append(new AdminRow("Name color", "The color of the name field", new TextTag(context, "appletHeadlineColor", box.getAppletHeadlineColor(), 12)));

		sb.append(new AdminRow("Name highlight color", null,
				new TextTag(context, "appletHeadlineHighlightColor", box.getAppletHeadlineHighlightColor(), 12)));

		sb.append(new AdminRow("Summary font family", null, new TextTag(context, "appletSummaryFamily", box.getAppletSummaryFamily(), 12)));

		sb.append(new AdminRow("Summary font size", "The size of the font used by the summary text", new TextTag(context, "appletSummarySize", box
				.getAppletSummarySize(), 5)));

		sb.append(new AdminRow("Summary color", "The color used to display the summary text", new TextTag(context, "appletSummaryColor", box
				.getAppletSummaryColor(), 12)));

		sb.append(new AdminRow("Summary highlight color", null, new TextTag(context, "appletSummaryHighlightColor", box.getAppletSummaryHighlightColor(),
				12)));

		sb.append("</table>");
	}

    private void scroller(StringBuilder sb) {
        sb.append(new AdminTable("Scroller options"));

        sb.append(new AdminRow("Height", "The height of the news scroller in pixels. You must include <em>px</em>. We recommend leaving this at the default of 150px.",
                new TextTag(context, "scrollerHeight", box.getScrollerHeight(), 8)));

        sb.append(new AdminRow("Speed", "The speed at which the news items will scroll. 1 is the default and is the slowest speed, we would not recommend any values higher than 10.",
                new TextTag(context, "scrollerSpeed", box.getScrollerSpeed(), 8)));

        sb.append("</table>");
    }

	@Override
	protected HighlightedItemsBox getBox() {
		return box;
	}

	protected void options(StringBuilder sb) {

		sb.append(new AdminTable("Highlighted box options"));

		sb.append(new AdminRow("Style", "How do you want to display the items?", new SelectTag(context, "style", box.getStyle(), HighlightedItemsBox.Style
				.values())));

        SelectTag selectTag = new SelectTag(context, "method", box.getSearch().getMethod(), HighlightMethod.getItem(context), "-None selected-");
		sb.append(new AdminRow("Method", "Select what type of highlighted items to show", selectTag));

		sb.append(new AdminRow("Limit", "Max number of results to show", new TextTag(context, "limit", box.getSearch().getLimit(), 8)));

		sb.append(new AdminRow("Images only", "Show show items with an image", new BooleanRadioTag(context, "imagesOnly", box.getSearch().isImageOnly())));

		sb.append(new AdminRow("Item type", "Limit items to a particular item type", new SelectTag(context, "itemType", box.getSearch().getItemType(),
				ItemType.getSelectionMap(context), "-Select item Type-")));

		if (miscSettings.isAdvancedMode() || isSuperman()) {

			sb.append(new AdminRow(miscSettings.isJimMode(), "Search", "The search controls how items are retrieved for this box", new ButtonTag(
					SearchEditHandler.class, null, "Edit search", "search", box.getSearch())));

		}

		sb.append("</table>");
	}

	@Override
	protected void saveSpecific() throws ServletException {

		HighlightedItemsBox box = getBox();

		box.setAppletLinkToCategory(appletLinkToCategory);
		box.getSearch().setLimit(limit);
		box.getSearch().setImageOnly(imagesOnly);
		box.getSearch().setMethod(method);
		box.getSearch().setItemType(itemType);
		box.getSearch().save();

		box.setAppletBackgroundColor(appletBackgroundColor);

		box.setAppletHeadlineColor(appletHeadlineColor);
		box.setAppletHeadlineFamily(appletHeadlineFamily);
		box.setAppletHeadlineHighlightColor(appletHeadlineHighlightColor);
		box.setAppletHeadlineSize(appletHeadlineSize);

		box.setAppletSummaryColor(appletSummaryColor);
		box.setAppletSummaryHighlightColor(appletSummaryHighlightColor);
		box.setAppletSummaryFamily(appletSummaryFamily);
		box.setAppletSummarySize(appletSummarySize);

		box.setStyle(style);

        box.setScrollerHeight(scrollerHeight);
        box.setScrollerSpeed(scrollerSpeed);

//        if (style.equals(Style.Scroller)) {
//            Markup markup = box.getMarkup();
//            if (markup != null) {
//                markup = markup.updateFrorScrollerBox(box);
//            }
//            box.setMarkup(markup);
//        }

	}

	@Override
	protected void specifics(StringBuilder sb) {

		options(sb);

		if (box.isApplet()) {
			applet(sb);
		}

        if (box.isScroller()) {
            scroller(sb);
        }
	}

}
