package org.sevensoft.ecreator.iface.admin.items.modules.alternatives;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.ItemTypeHandler;
import org.sevensoft.ecreator.iface.admin.items.menus.EditItemTypeMenu;
import org.sevensoft.ecreator.iface.admin.misc.html.EditMarkupButton;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.items.alternatives.AlternativesModule;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 23 Oct 2006 17:28:19
 *
 */
@Path("admin-alternatives.do")
public class AlternativesHandler extends AdminHandler {

	private ItemType	itemType;
	private int		alternativesQty;
	private Markup	alternativesMarkup;
	private String	alternativesCaption;
    private Attribute alternativesAttribute;

	public AlternativesHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (itemType == null || !Module.Alternatives.enabled(context)) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, itemType.getName() + " settings: Upsell", null);
		doc.setMenu(new EditItemTypeMenu(itemType));
		doc.addBody(new Body() {

			private void alternatives() {

				AlternativesModule module = itemType.getAlternativesModule();

				sb.append(new AdminTable("Alternatives"));

				sb.append(new AdminRow("Alternatives qty", "How many alternatives to show for this item.", new TextTag(context, "alternativesQty",
						module.getAlternativesQty(), 4)));

				sb.append(new AdminRow("Alternatives caption", "Rename alternatives for the front end of the site", new TextTag(context,
						"alternativesCaption", module.getAlternativesCaption())));

                SelectTag alternativesAttributes = new SelectTag(context, "alternativesAttribute", module.getAlternativesAttribute());
                alternativesAttributes.addOptions(itemType.getAttributesSelectionMap(context));
                alternativesAttributes.setAny(" -- Choose -- ");
                sb.append(new AdminRow("Alternatives attribute", "Choose attribute to find alternatives if your items are not in categories " +
                        "and categories use Search wrapper to show items.", alternativesAttributes));


                if (Module.UserMarkup.enabled(context) || isSuperman()) {

					SelectTag markupTag = new SelectTag(context, "alternativesMarkup", module.getAlternativesMarkup(), Markup.get(context), null);
					markupTag.setId("alternativesMarkup");

					sb.append(new AdminRow("Alternatives markup", null, markupTag + " " + new EditMarkupButton("alternativesMarkup")));

				}

				sb.append("</table>");
			}

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update item type"));
				sb.append(new ButtonTag(ItemTypeHandler.class, null, "Return to item types menu"));

				sb.append("</div>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(AlternativesHandler.class, "save", "post"));
				sb.append(new HiddenTag("itemType", itemType));

                commands();
				alternatives();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (itemType == null) {
			return main();
		}

		AlternativesModule m = itemType.getAlternativesModule();
		m.setAlternativesCaption(alternativesCaption);
		m.setAlternativesMarkup(alternativesMarkup);
		m.setAlternativesQty(alternativesQty);
        m.setAlternativesAttribute(alternativesAttribute);
		m.save();

		return main();
	}
}
