package org.sevensoft.ecreator.iface.admin.design.reorder;

import org.sevensoft.ecreator.iface.EcreatorRenderer;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.DeleteGif;
import org.sevensoft.ecreator.model.extras.reorder.ReorderOwner;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Link;

import java.util.List;
import java.util.Arrays;
import java.util.Iterator;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

/**
 * User: Tanya
 * Date: 11.01.2012
 */
public class ObjectsReorderPanel extends EcreatorRenderer {

    private final List<? extends ReorderOwner> reorderOwner;
    private final Link linkback;
    private Class<? extends AdminHandler> editClazz;
    private String editAction;
    private String editParam;
    private Object[] editExtraParams = new Object[0];
    private Class<? extends AdminHandler> deleteClazz;
    private String deleteAction;
    private String deleteParam;
    private String deleteConfirmation;
    private String customSpan;


    public ObjectsReorderPanel(RequestContext context) {
        this(context, null, null);
    }

    public ObjectsReorderPanel(RequestContext context, Link linkback, List<? extends ReorderOwner> reorderOwner) {
        super(context);
        this.reorderOwner = reorderOwner;
        this.linkback = linkback;
    }

    public void setEditClazz(Class<? extends AdminHandler> editClazz) {
        this.editClazz = editClazz;
    }

    public void setEditAction(String editAction) {
        this.editAction = editAction;
    }

    public void setEditParam(String editParam) {
        this.editParam = editParam;
    }

    public void setEditExtraParams(Object... editExtraParams) {
        this.editExtraParams = editExtraParams;
    }

    public void setDeleteClazz(Class<? extends AdminHandler> deleteClazz) {
        this.deleteClazz = deleteClazz;
    }

    public void setDeleteAction(String deleteAction) {
        this.deleteAction = deleteAction;
    }

    public void setDeleteParam(String deleteParam) {
        this.deleteParam = deleteParam;
    }

    public void setDeleteConfirmation(String deleteConfirmation) {
        this.deleteConfirmation = deleteConfirmation;
    }

    public void setCustomSpan(String customSpan) {
        this.customSpan = customSpan;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append
                (

                        "\t<script>\n" +
                                "\t$(function() {\n" +
                                "\t\t$( \"#sortable\" ).sortable();\n" +
                                "\t\t$( \"#sortable\" ).disableSelection();\n" +
                                "\t});\n" +
                                "\t</script>");

        sb.append("<ul id='sortable'>");

        int n = 1;
        for (ReorderOwner owner : reorderOwner) {
            sb.append("<li><span></span>");
            sb.append(new HiddenTag("positions", owner));
            double position = 2.7;
            if (deleteParam != null && deleteClazz == null) {
                sb.append("<span style='position:absolute;left:" + position + "em;'>" + new CheckTag(context, deleteParam, owner, false) + "</span>");
                position = position + 6.5;
            }

            sb.append("<span style='position:absolute;left:" + position + "em;'>" + n++ + "</span>");
            position = position + 6.5;
            LinkTag linkTag = new LinkTag(editClazz, editAction, new SpannerGif(), editParam, owner);

            Iterator iter = Arrays.asList(editExtraParams).iterator();
            while (iter.hasNext()) {
                linkTag.addParameter(String.valueOf(iter.next()), iter.next());
            }

            sb.append("<span style='position:absolute;left:" + position + "em;'>" + linkTag +
                    " " + owner.getReorderName() + "</span>");
            if (customSpan != null) {
                Object obj = null;
                try {
                    Method method = owner.getClass().getDeclaredMethod(customSpan);
                    obj = method.invoke(owner);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                } catch (InvocationTargetException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

                sb.append("<span style='position:absolute;left:53%;'>" + obj + "</span>");
            }
            if (deleteClazz != null)
                sb.append("<span style='position:absolute;right:2em;'>" + new LinkTag(deleteClazz, deleteAction, new DeleteGif(), deleteParam, owner)
                        .setConfirmation(deleteConfirmation) + "</span>");

            sb.append("</li>");

        }
        sb.append("</ul>");


        return sb.toString();
//        return "ObjectsReorderPanel{" +
//                "reorderOwner=" + reorderOwner +
//                ", linkback=" + linkback +
//                '}';
    }

}
