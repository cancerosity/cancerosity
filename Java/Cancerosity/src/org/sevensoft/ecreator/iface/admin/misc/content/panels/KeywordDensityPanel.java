package org.sevensoft.ecreator.iface.admin.misc.content.panels;

import java.util.Map;

import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.misc.seo.keywords.KeywordDensity;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 11 Jan 2007 17:14:32
 *
 */
public class KeywordDensityPanel {

	private RequestContext	context;
	private String		content;

	public KeywordDensityPanel(RequestContext context, String content) {
		this.context = context;
		this.content = content;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		KeywordDensity density = new KeywordDensity(content);

		sb.append(new AdminTable("Keyword density"));

		// intro
		sb.append("<tr><td colspan='3'>This shows the 15 most used single word keywords in your content. "
				+ "You should aim for around 5% coverage of the words you want to be found for on search engines. "
				+ "Ideally the words should be sprinkled throughout the text rather than bunched together.<br/>" + "<i>Total words used: "
				+ density.getWordCount() + "</i></td></tR>");

		// header
		sb.append("<tr><th>Keyword</th><th>Occurrences</th><th>Percentage</th></tr>");

		// keywords
		for (Map.Entry<String, Integer> entry : density.getCounts(15).entrySet()) {
			sb.append("<tr><td>" + entry.getKey() + "</td><td>" + entry.getValue() + "</td><td>" + density.getDensityFormatted(entry.getValue())
					+ "%</td></tr>");
		}

		sb.append("</table>");

		return sb.toString();
	}
}
