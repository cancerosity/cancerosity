package org.sevensoft.ecreator.iface.admin.feeds.bespoke.property;

import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.ImportFeed;
import org.sevensoft.ecreator.model.feeds.bespoke.property.kyero.KyeroFeed;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 3 Aug 2006 11:07:23
 *
 */
@Path("admin-feeds-kyero.do")
public class KyeroFeedHandler extends FeedHandler {

	private KyeroFeed	feed;
	private Attribute	bathsAttribute;
	private Attribute	bedsAttribute;
	private Attribute	typeAttribute;
	private Attribute	poolAttribute;
	private Attribute	townAttribute;
	private Attribute	provinceAttribute;
	private ItemType	itemType;
	private Attribute	priceAttribute;

	public KyeroFeedHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected ImportFeed getFeed() {
		return feed;
	}

	@Override
	protected void saveSpecific() {

		feed.setBathsAttribute(bathsAttribute);
		feed.setBedsAttribute(bedsAttribute);
		feed.setPoolAttribute(poolAttribute);
		feed.setPriceAttribute(priceAttribute);
		feed.setProvinceAttribute(provinceAttribute);
		feed.setTownAttribute(townAttribute);
		feed.setTypeAttribute(typeAttribute);
		feed.setItemType(itemType);

		feed.save();
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Kyero feed specifics"));

		sb.append(new AdminRow("Item type", "Select the item type to import to.", new SelectTag(context, "itemType", feed.getItemType(), ItemType
				.getSelectionMap(context), "-None set-")));

		if (feed.hasItemType()) {

			sb.append(new AdminRow("Price attribute", "This sets the asking price of the property.", new SelectTag(context, "priceAttribute", feed
					.getPriceAttribute(), feed.getItemType().getAttributes(), "-None-")));

			sb.append(new AdminRow("Type attribute", "This is used for the type of property, eg, 'Villa'.", new SelectTag(context, "typeAttribute", feed
					.getTypeAttribute(), feed.getItemType().getAttributes(), "-None-")));

			sb.append(new AdminRow("Town attribute", "This sets the town where the property is located.", new SelectTag(context, "townAttribute", feed
					.getTownAttribute(), feed.getItemType().getAttributes(), "-None-")));

			sb.append(new AdminRow("Province attribute", "This sets the province of the property.", new SelectTag(context, "provinceAttribute", feed
					.getProvinceAttribute(), feed.getItemType().getAttributes(), "-None-")));

			sb.append(new AdminRow("Pool attribute", "This sets if the property has a pool, yes or no.", new SelectTag(context, "poolAttribute", feed
					.getPoolAttribute(), feed.getItemType().getAttributes(), "-None-")));

			sb.append(new AdminRow("Beds attribute", "This sets the number of beds", new SelectTag(context, "bedsAttribute", feed.getBedsAttribute(),
					feed.getItemType().getAttributes(), "-None-")));

			sb.append(new AdminRow("Baths attribute", "This sets the number of baths", new SelectTag(context, "bathsAttribute", feed.getBathsAttribute(),
					feed.getItemType().getAttributes(), "-None-")));

		}

		sb.append("</table>");
	}
}
