package org.sevensoft.ecreator.iface.admin.interaction;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupTag;
import org.sevensoft.ecreator.model.interaction.InteractionModule;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;

/**
 * @author sks
 */
@Path("admin-interaction-module.do")
public class InteractionModuleHandler extends AdminHandler {

	private Markup	listMarkup;
	private Markup	profileMarkup;
	private int		days;
	private String	pmNotificationBody;
	private Markup	winksMarkup;
	private Markup	viewsMarkup;
	private boolean	hideAccount;
	private Markup	buddiesMarkup;
	private boolean	moderatingImages;
	private Markup	pmProfileMarkup;
	private ItemType	itemType;
	private boolean	pmAttachments;
	private boolean	pmNotification;
	private String	winksFooter;
	private String	winksHeader;
	private String	buddiesHeader;
	private String	buddiesFooter;
	private String	addedToBuddiesHeader;
	private String	addedToBuddiesFooter;
	private String	pmFooter;
	private String	pmHeader;
	private boolean	simpleEditor;
	private int		pmLimit;

	public InteractionModuleHandler(RequestContext context) {
		super(context);
	}

	@Override
	public Object main() throws ServletException {

		if (itemType == null) {
			return index();
		}

		final InteractionModule module = itemType.getInteractionModule();

		AdminDoc doc = new AdminDoc(context, user, "Interaction module", Tab.getItemTab(itemType));
		doc.addBody(new Body() {

			private void buddies() {

				sb.append(new AdminTable("Content"));

				sb.append("<tr><td>Buddies page header</td></tr>");
				sb.append("<tr><td>" +
						new TextAreaTag(context, "buddiesHeader", module.getBuddiesHeader()).setId("buddiesHeader").setStyle(
								"width: 100%; height: 320px;") + "</td></tr>");

				sb.append("<tr><td>Buddies page footer</td></tr>");
				sb.append("<tr><td>" +
						new TextAreaTag(context, "buddiesFooter", module.getBuddiesFooter()).setId("buddiesFooter").setStyle(
								"width: 100%; height: 320px;") + "</td></tr>");

				sb.append("<tr><td>Added to buddies page header</td></tr>");
				sb.append("<tr><td>" +
						new TextAreaTag(context, "addedToBuddiesHeader", module.getAddedToBuddiesHeader()).setId("addedToBuddiesHeader").setStyle(
								"width: 100%; height: 320px;") + "</td></tr>");

				sb.append("<tr><td>Added to buddies page footer</td></tr>");
				sb.append("<tr><td>" +
						new TextAreaTag(context, "addedToBuddiesFooter", module.getAddedToBuddiesFooter()).setId("addedToBuddiesFooter").setStyle(
								"width: 100%; height: 320px;") + "</td></tr>");

				sb.append("</table>");
			}

			private void commands() {
				sb.append("<div class='action'>" + new SubmitTag("Update") + "</div>");
			}

			private void general() {

				sb.append(new AdminTable("Content"));

				sb.append(new AdminRow("Simple editor", "Disable HTML content editor for editors on this page", new BooleanRadioTag(context,
						"simpleEditor", module.isSimpleEditor())));

				sb.append("</table>");
			}

			private void markup() {

				sb.append(new AdminTable("Markup"));

				if (Module.Nudges.enabled(context)) {

					sb.append(new AdminRow("Winks markup", "This markup is used to render the winks page", new MarkupTag(context, "winksMarkup",
							module.getWinksMarkup(), "-Default-", "winksMarkup")));

				}

				if (Module.MemberViewers.enabled(context)) {

					sb.append(new AdminRow("Views markup", "This markup is used to render the views page", new MarkupTag(context, "viewsMarkup",
							module.getViewsMarkup(), "-Default-", "viewsMarkup")));
				}

				if (Module.Buddies.enabled(context)) {

					sb.append(new AdminRow("Buddies markup", "This markup is used to render the buddies page", new MarkupTag(context,
							"buddiesMarkup", module.getBuddiesMarkup(), "-Default-", "buddiesMarkup")));
				}

				if (Module.PrivateMessages.enabled(context)) {

					sb.append(new AdminRow("Private message profile markup",
							"This markup is used to render the profile of the correspondant in private messsaging.", new MarkupTag(context,
									"pmProfileMarkup", module.getPmProfileMarkup(), "-Default-", "pmProfileMarkup")));

				}

				sb.append("</table>");

			}

			private void pm() {

				sb.append(new AdminTable("Private messaging"));

				sb.append(new AdminRow("Notification", "Send an email to the user when they receive a private message", new BooleanRadioTag(context,
						"pmNotification", module.isPmNotification())));

				if (module.isPmNotification()) {

					sb.append(new AdminRow("Notification body", "This is the content of the email that is sent to users", new TextAreaTag(context,
							"pmNotificationBody", module.getPmNotificationBody(), 60, 7)));

				}

				sb.append(new AdminRow("Private message page header", null, new TextAreaTag(context, "pmHeader", module.getPmHeader())
						.setId("pmHeader").setStyle("width: 100%; height: 320px;")));

				sb.append(new AdminRow("Private message pagefooter", null, new TextAreaTag(context, "pmFooter", module.getPmFooter()).setId("pmFooter")
						.setStyle("width: 100%; height: 320px;")));

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(InteractionModuleHandler.class, "save", "POST"));
				sb.append(new HiddenTag("itemType", itemType));

                commands();
				general();

				if (Module.UserMarkup.enabled(context) || isSuperman()) {
					markup();
				}

				if (Module.PrivateMessages.enabled(context)) {
					pm();
				}

				if (Module.Nudges.enabled(context)) {
					winks();
				}

				if (Module.Buddies.enabled(context)) {
					buddies();
				}

				commands();

				sb.append("</form>");

				return sb.toString();
			}

			private void winks() {

				sb.append(new AdminTable("Content"));

				sb.append("<tr><td>Winks page header</td></tr>");
				sb.append("<tr><td>" +
						new TextAreaTag(context, "winksHeader", module.getWinksHeader()).setId("winksHeader").setStyle(
								"width: 100%; height: 320px;") + "</td></tr>");

				sb.append("<tr><td>Winks page footer</td></tr>");
				sb.append("<tr><td>" +
						new TextAreaTag(context, "winksFooter", module.getWinksFooter()).setId("winksFooter").setStyle(
								"width: 100%; height: 320px;") + "</td></tr>");

				sb.append("</table>");
			}

		});
		return doc;
	}

	public Object save() throws ServletException {

		if (itemType == null) {
			return main();
		}

		InteractionModule module = itemType.getInteractionModule();
		module.setSimpleEditor(simpleEditor);

		module.setPmNotification(pmNotification);
		module.setPmNotificationBody(pmNotificationBody);
		module.setPmHeader(pmHeader);
		module.setPmFooter(pmFooter);

		if (Module.UserMarkup.enabled(context) || isSuperman()) {

			module.setViewsMarkup(viewsMarkup);
			module.setWinksMarkup(winksMarkup);
			module.setBuddiesMarkup(buddiesMarkup);
			module.setPmProfileMarkup(pmProfileMarkup);

		}

		module.setWinksHeader(winksHeader);
		module.setWinksFooter(winksFooter);

		module.setBuddiesFooter(buddiesFooter);
		module.setBuddiesHeader(buddiesHeader);
		module.setAddedToBuddiesFooter(addedToBuddiesFooter);
		module.setAddedToBuddiesHeader(addedToBuddiesHeader);

		module.save();

		clearParameters();
		return main();
	}

}
