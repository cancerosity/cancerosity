package org.sevensoft.ecreator.iface.admin.misc.seo.panels;

import org.sevensoft.ecreator.iface.admin.categories.CategoryKeywordsHandler;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 17-Nov-2005 18:20:36
 * 
 */
public class KeywordSearchRenderer {

	private final RequestContext	context;
	private StringBuilder		sb;
	private Category			category;

	public KeywordSearchRenderer(RequestContext context, Category category) {
		this.context = context;
		this.category = category;
		sb = new StringBuilder();
	}

	@Override
	public String toString() {

		sb.append(new FormTag(CategoryKeywordsHandler.class, "suggest", "GET"));
		sb.append(new HiddenTag("category", category));

		sb.append("<table cellspacing='0' cellpadding='0' class='form'>");
		sb.append("<caption>Keyword search term</caption>");

		sb.append("<tr>");
		sb.append("<th>Search term</th>");
		sb.append("<td>" + new TextTag(context, "term", 20) + " " + new SubmitTag("Suggest") + "</td>");
		sb.append("</tr>");

		sb.append("</table>");
		sb.append("</form>");

		return sb.toString();
	}

}
