package org.sevensoft.ecreator.iface.admin.system.config;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.misc.geoip.GeoIp;
import org.sevensoft.ecreator.model.misc.location.LocationInstaller;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.Config.SecureMode;
import org.sevensoft.ecreator.model.system.config.Config.SoftwareVersion;
import org.sevensoft.ecreator.model.system.config.Config.Status;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Upload;
import org.sevensoft.jeezy.http.html.form.FileTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.RadioTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.text.PasswordTag;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.db.annotations.Default;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * @author sks 01-Jun-2004 21:28:42
 */
@Path("admin-config.do")
public class ConfigHandler extends AdminHandler {

    private int keywordLinkingQtyLimit, keywordLinkingNameLength;
    private String smtpHostname, serverEmail;

    /**
     * Tag true if secure cert intsalled for this site
     */
    private boolean secure;

    private Upload geoipUpload;

    private String email;
    private Status status;
    private String developers;
    private String to;
    private String from;
    private SecureMode secureMode;
    private String supermanIps;
    private String mailboxEmail;

    /**
     * Enable html caching
     */
    private boolean htmlCaching;

    private SoftwareVersion version;
    private String domain;
    private String url;
    private Upload locationUpload;
    private String googleMapsApiKey;
    private Upload upload;
    private String filePath;
    private boolean useNewsContent;
    private boolean useHighlightedItemsCache;
    private boolean wwwRedirection;

    private boolean useMessages;

    private String username;
    private String password;

    public ConfigHandler(RequestContext context) {
        super(context);
    }

    public Object deleteGeoIps() throws ServletException {

        if (!isSuperman()) {
            return index();
        }

        GeoIp.delete(context);
        return new ActionDoc(context, "Geo ip data has been deleted", new Link(ConfigHandler.class));
    }

    public Object deleteLocations() throws ServletException {

        if (!isSuperman()) {
            return index();
        }

        LocationInstaller.delete(context);
        return new ActionDoc(context, "Location data has been deleted", new Link(ConfigHandler.class));
    }

    @Override
    public Object main() throws ServletException {

        if (!isSuperman()) {
            return index();
        }

        AdminDoc doc = new AdminDoc(context, user, "Config", null);
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Save configuration"));
                sb.append(new ButtonTag(ConfigHandler.class, "reinit", "Redetect settings"));
                sb.append("</div>");
            }

            private void general() {

                sb.append(new AdminTable("General"));

                sb.append(new AdminRow("Url", new TextTag(context, "url", config.getUrl(), 40)));

                sb.append(new AdminRow("Domain", "Set automatically", new TextTag(context, "domain", config.getDomain(), 40).setDisabled(true)));

                sb.append(new AdminRow("Version", "Choose the software version.", new SelectTag(context, "version", config.getSoftwareVersion(),
                        SoftwareVersion.values())));

                sb.append(new AdminRow("Developers", new TextTag(context, "developers", config.getDevelopers(), 40)));

                SelectTag statusTag = new SelectTag(context, "status", config.getStatus());
                statusTag.addOptions(Config.Status.values());

                sb.append(new AdminRow("Status", statusTag));

                sb.append(new AdminRow("Secure mode", "Enable SSL support when a certificate has been installed", new SelectTag(context, "secureMode",
                        config.getSecureMode(), SecureMode.values(), null)));

                sb.append(new AdminRow("Use highlighted items cache", "Enables highlighted items caching", new BooleanRadioTag(context, "useHighlightedItemsCache",
                        config.isHiglightedItemsCache())));

                 sb.append(new AdminRow("Use redirect non-www links to www", "Enables redirection", new BooleanRadioTag(context, "wwwRedirection",
                        config.isWwwRedirection()))); //todo

                sb.append("</table>");

            }

            private void geoip() {

                sb.append(new AdminTable("Geo IP"));

                sb.append(new AdminRow("Ip range entries", "The number of ip ranges in the database.", GeoIp.count(context)));

                sb.append(new AdminRow("Install geoip files", "Install ip ranges required for Geo Ip lookups.", new FileTag("geoipUpload", 30)));

                sb
                        .append(new AdminRow("Delete geoip files", "Delete installed ip ranges.", new ButtonTag(ConfigHandler.class,
                                "deleteGeoIps", "Delete").setConfirmation("Are you sure you want to delete ip ranges?")));

                sb.append("</table>");
            }

            private void location() {

                sb.append(new AdminTable("Location files"));

                sb.append(new AdminRow("Location entries", "The number of placename / postcodes in the database.", LocationInstaller.getSize(context)));

                sb.append(new AdminRow("Install location files", "Install location files required for postcode / placename lookups.", new FileTag(
                        "locationUpload", 30)));

                sb.append(new AdminRow("Delete location files", "Delete installed postcode and placename files.", new ButtonTag(ConfigHandler.class,
                        "deleteLocations", "Delete").setConfirmation("Are you sure you want to delete location files?")));

                sb.append("</table>");
            }

            private void mail() {

                sb.append(new AdminTable("Mail"));

                sb.append(new AdminRow("Smtp hostname", "The hostname of the SMTP server used to send mail generated by this site.", new TextTag(
                        context, "smtpHostname", config.getSmtpHostname(), 40)));

                sb.append(new AdminRow("Server email", null, new TextTag(context, "serverEmail", config.getServerEmail(), 40)));

                sb.append(new AdminRow("Mailbox email", "The from email for crm outgoing mails", new TextTag(context, "mailboxEmail", config
                        .getMailboxEmail(), 30)));

                 sb.append(new AdminRow("Mailbox username", "The from email for crm outgoing mails", new TextTag(context, "username", config
                        .getUsername(), 30)));

                 sb.append(new AdminRow("Mailbox password", "The from email for crm outgoing mails", new PasswordTag(context, "password", config
                        .getPassword(), 30)));

                ButtonTag testEmailTag = new ButtonTag("Test mail server");
                testEmailTag.setOnClick("window.location='" + new Link(ConfigHandler.class, "testMailServer")
                        + "&email=' + document.getElementById('testEmail').value;");

                sb.append(new AdminRow("Test mail server",
                        "Send an email to the email address entered here to test the mail server and mail settings are correct.", new TextTag(
                        context, "testEmail", 20).setId("testEmail")
                        + " " + testEmailTag));

                sb.append("</table>");
            }

            private void paths() {

                sb.append(new AdminTable("Paths"));

                sb.append(new AdminRow("Image path", ResourcesUtils.getRealImagesPath()));
                sb.append(new AdminRow("Thumbnails path", ResourcesUtils.getRealThumbnailsPath()));
                sb.append(new AdminRow("Files path", context.getRealPath(Config.FilesPath)));
                sb.append(new AdminRow("Template data", ResourcesUtils.getRealTemplateDataPath()));
                sb.append(new AdminRow("Attachments path", ResourcesUtils.getRealAttachmentsPath()));
                sb.append(new AdminRow("PDFs path", ResourcesUtils.getRealPdfsPath()));

                sb.append("</table>");

            }

            private void superman() {

                sb.append(new AdminTable("Superman access"));

                sb.append(new AdminRow("Superman ips", "Enter IP addresses that should receive superman on this site only.", new TextAreaTag(context,
                        "supermanIps", StringHelper.implode(config.getSupermanIps(), "\n"), 40, 4)));

                sb.append("</table>");
            }

            private void news() {
                sb.append(new AdminTable("News"));

                sb.append(new AdminRow("Use news content", "Select Yes if you want to show news on the admin home page.",
                        "Yes" + new RadioTag(context, "useNewsContent", "true", config.useNewsContent()) + "No" +
                                new RadioTag(context, "useNewsContent", "false", !config.useNewsContent())));

                sb.append(new AdminRow("File path", "The path to html page with news", new TextTag(context, "filePath", config.getFilePath(), 40)));

                sb.append("</table>");
            }

            private void systemMessages() {
                sb.append(new AdminTable("System Messages"));

                sb.append(new AdminRow("Use System Messages", "Select Yes if you want to show system messages on the admin home page.",
                        new BooleanRadioTag(context, "useMessages", config.isUseMessages())));

                sb.append("</table>");


            }

            @Override
            public String toString() {

                sb.append(new FormTag(ConfigHandler.class, "save", "multi"));

                commands();
                general();
                mail();
                paths();
                location();
                geoip();
                news();
                systemMessages();
                superman();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });

        return doc;
    }

    public Object reinit() throws ServletException {

        if (!isSuperman()) {
            return index();
        }

        config.init(context);

        clearParameters();
        return main();
    }

    public Object save() throws ServletException {

        if (!isSuperman()) {
            return index();
        }

        config.setDevelopers(developers);
        config.setUrl(url);
        config.setDomain(domain);

        config.setStatus(status);
        config.setServerEmail(serverEmail);
        config.setSmtpHostname(smtpHostname);
        config.setUsername(username);
        if (password != null && !password.equals(config.getPassword())) {
            config.setPassword(password);
        }

        config.setSecureMode(secureMode);
        config.setMailboxEmail(mailboxEmail);
        config.setSoftwareVersion(version);

        config.setFilePath(filePath);
        config.setUseNewsContent(useNewsContent);

        config.setUseMessages(useMessages);

        config.setSupermanIps(StringHelper.explodeStrings(supermanIps, "\n"));

        config.setHighlightedItemsCache(useHighlightedItemsCache);
        config.setWwwRedirection(wwwRedirection);

        config.save();

        if (geoipUpload != null) {
            try {
                GeoIp.install(context, geoipUpload.getFile());
            } catch (IOException e) {
                e.printStackTrace();
                addError(e);
            }
        }

        if (locationUpload != null) {
            try {
                LocationInstaller.install(context, locationUpload.getFile());
            } catch (IOException e) {
                e.printStackTrace();
                addError(e);
            }
        }

        clearParameters();
        addMessage("Config updated");
        return main();
    }

    public Object schemaReset() throws ServletException {

        if (!isSuperman()) {
            return index();
        }

        context.getSchemaValidator().clear();

        addMessage("Schema reset");
        clearParameters();
        return main();
    }

    /**
     * Send an email out with the administrator account (any)
     */
    public Object sendAccountEmail() throws ServletException {

        if (!isSuperman()) {
            return index();
        }

        if (email != null) {
            try {
                config.sendAccountEmail(email);
                addMessage("Account email sent to: " + email);
            } catch (EmailAddressException e) {
                e.printStackTrace();
            } catch (SmtpServerException e) {
                e.printStackTrace();
            }
        }

        clearParameters();
        return main();
    }

    public Object testMailServer() throws ServletException {

        if (!isSuperman()) {
            return index();
        }

        Email e = new Email();
        e.setTo(email);
        e.setFrom(config.getServerEmail());
        e.setSubject("test email");
        e.setBody("testing email server");
        try {

            e.send(config.getSmtpHostname(), config.getUsername(), config.getPassword());
        } catch (EmailAddressException e1) {

            e1.printStackTrace();
            addError(e1);
            return main();

        } catch (SmtpServerException e1) {
            e1.printStackTrace();
            addError(e1);
            return main();
        }

        return new ActionDoc(context, "Test email has been dispatched", new Link(ConfigHandler.class));
    }

    public final boolean isHtmlCaching() {
        return htmlCaching;
    }

    public final void setHtmlCaching(boolean htmlCaching) {
        this.htmlCaching = htmlCaching;
    }

}