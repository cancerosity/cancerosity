package org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics;

import org.sevensoft.jeezy.http.html.ImageTag;

/**
 * @author dme 05 Sep 2006 17:48:47
 */
public class SlideshowGif {

    public static final String SRC = "files/js/utils/slideshow.gif";

    @Override
    public String toString() {
        return new ImageTag(SRC).toString();
    }

}