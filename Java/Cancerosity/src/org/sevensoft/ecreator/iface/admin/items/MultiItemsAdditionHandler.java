package org.sevensoft.ecreator.iface.admin.items;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

import javax.servlet.ServletException;

/**
 * User: Tanya
 * Date: 20.10.2010
 */
@Path("admin-multiple-items.do")
public class MultiItemsAdditionHandler extends AdminHandler {

    private ItemType itemType;

    public MultiItemsAdditionHandler(RequestContext context) {
        super(context);
    }

    public Object main() throws ServletException {
        return index();  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object edit() throws ServletException {
       return new ItemMultiHandler(context).setItemType(itemType).setNewItemsNumber(5).create();
    }
}
