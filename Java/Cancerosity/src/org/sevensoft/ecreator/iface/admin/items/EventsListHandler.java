package org.sevensoft.ecreator.iface.admin.items;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Transformer;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.iface.frontend.FrontendDoc;
import org.sevensoft.ecreator.iface.frontend.FrontendHandler;
import org.sevensoft.ecreator.model.attributes.AttributeUtil;
import org.sevensoft.ecreator.model.attributes.AttributeValue;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupRenderer;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.media.images.ImageUtil;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.db.Row;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

import javax.servlet.ServletException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: MeleshkoDN
 * Date: 12.10.2007
 * Time: 15:24:45
 */
@Path("view-events.do")
public class EventsListHandler extends FrontendHandler {

    private Date date;

    public EventsListHandler(RequestContext context) {
        super(context);
    }

    public Object main() throws ServletException {
        if (date == null) {
            return index();
        }
        FrontendDoc doc = new FrontendDoc(context, "View events on date " + date.toDayMonthString());
        List<Item> events = getEventsOnDate(date);
        if (!events.isEmpty()) {
            ImageUtil.prepopulate(context, events);
            AttributeUtil.prepopulate(context, events);
            Markup markup = events.get(0).getItemType().getListMarkup();
            MarkupRenderer r = new MarkupRenderer(context, markup);
            r.setLocation(null, 0, 0);
            r.setBodyObjects(events);
            doc.addBody(r);
        } else {
            doc.addBody(new Body() {
                @Override
                public String toString() {
                    sb.append("<h2>No events found</h2>");
                    return sb.toString();
                }
            });
        }
        return doc;
    }

    private List<Item> getEventsOnDate(Date date) {
        ArrayList<Item> events = new ArrayList<Item>();
        events.addAll(getNonRecurrentEventsOnDate(date));
        events.addAll(getRecurrentEventsOnDate(date));
        return events;
    }

    private List<Item> getNonRecurrentEventsOnDate(Date date) {
        ItemType eventType = ItemType.getByName(context, "Event");
        Query q = new Query(context, "SELECT i.id FROM # i JOIN # av1 ON i.id=av1.item JOIN # av2 ON i.id=av2.item"
                + " WHERE i.id NOT IN (SELECT i.id FROM # i JOIN # av3 ON i.id=av3.item WHERE av3.attribute=? AND av3.value='true') AND" +
                "   av1.attribute=? AND av2.attribute=? AND ( av1.value<=? AND av2.value>=? )");
        q.setTable(Item.class);
        q.setTable(AttributeValue.class);
        q.setTable(AttributeValue.class);
        q.setTable(Item.class);
        q.setTable(AttributeValue.class);
        q.setParameter(eventType.getAttribute("Recurrence"));
        q.setParameter(eventType.getAttribute("Date start"));
        q.setParameter(eventType.getAttribute("Date end"));
        q.setParameter(date);
        q.setParameter(date);
        List<Row> items = q.execute();
        return CollectionsUtil.transform(items, new Transformer<Row, Item>() {
            public Item transform(Row row) {
                return row.getObject(0, Item.class);
            }

        });
    }

    private List<Item> getRecurrentEvents() {
        ItemType eventType = ItemType.getByName(context, "Event");
        Query recurrenceEventsQuery = new Query(context, "SELECT i.id FROM # i JOIN # av1 ON i.id=av1.item WHERE av1.attribute=? AND av1.value='true' ");
        recurrenceEventsQuery.setTable(Item.class);
        recurrenceEventsQuery.setTable(AttributeValue.class);
        recurrenceEventsQuery.setParameter(eventType.getAttribute("Recurrence"));
        List<Row> items = recurrenceEventsQuery.execute();
        return CollectionsUtil.transform(items, new Transformer<Row, Item>() {
            public Item transform(Row row) {
                return row.getObject(0, Item.class);
            }

        });
    }

    private List<Item> getRecurrentEventsOnDate(Date date) {
        List<Item> result = new ArrayList<Item>();
        List<Item> events = getRecurrentEvents();
        for (Item event : events) {
            List<Date> eventDates = EventDatesUtils.evaluateEventDates((DateTime) (new DateTime(date).previousMinute()), event);
            Date startDate = eventDates.get(0);
            Date endDate = eventDates.get(1);
            if (date.isAfterOrEqual(startDate) && date.isBeforeOrEqual(endDate)) {
                result.add(event);
            }
        }
        return result;
    }
}
