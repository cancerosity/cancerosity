package org.sevensoft.ecreator.iface.admin.feeds.bespoke.prices;

import org.sevensoft.ecreator.iface.admin.feeds.FeedHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attributes.Attribute;
import org.sevensoft.ecreator.model.feeds.Feed;
import org.sevensoft.ecreator.model.feeds.bespoke.prices.base.GoogleBase;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 15 Dec 2006 12:22:55
 *
 */
@Path("admin-feeds-base.do")
public class GoogleBaseHandler extends FeedHandler {

	private GoogleBase	feed;
	private ItemType		itemType;
	private Attribute		mpcAttribute;
	private String		productType;
	private Attribute		eanAttribute;
    private Attribute		isbnAttribute;
	private Attribute		brandAttribute;
    private Attribute       shipWeightAttribute;

	public GoogleBaseHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Feed getFeed() {
		return feed;
	}

	@Override
	protected void saveSpecific() {
        if (feed.hasItemType()) {
            if (brandAttribute != null && (isbnAttribute != null || eanAttribute != null)) {

            } else if (mpcAttribute != null) {

            } else {
                addError("Choose Brand + GTIN (ISBN or EAN)  or  Product code");
                return;
            }
        }
		feed.setBrandAttribute(brandAttribute);
		feed.setIsbnAttribute(isbnAttribute);
        feed.setEanAttribute(eanAttribute);
		feed.setProductType(productType);
		feed.setMpcAttribute(mpcAttribute);
        feed.setShipWeightAttribute(shipWeightAttribute);
		feed.setItemType(itemType);
		feed.save();
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Google base settings"));

		sb.append(new AdminRow("Item type", "Select the item type to import to.", new SelectTag(context, "itemType", feed.getItemType(), ItemType
				.getSelectionMap(context), "-None set-")));

		sb.append(new AdminRow("Product type", "Enter the name of the product type, eg Electronics", new TextTag(context, "productType", feed
				.getProductType(), 30)));

		if (feed.hasItemType()) {

			sb.append(new AdminRow("Brand attribute", "Select the attribute to be used for setting brands.", new SelectTag(context, "brandAttribute",
					feed.getBrandAttribute(), feed.getItemType().getAttributes(), "-None set-")));

			sb.append(new AdminRow("Product code attribute", "Select the attribute to be used for setting manufacturer product codes.", new SelectTag(
					context, "mpcAttribute", feed.getMpcAttribute(), feed.getItemType().getAttributes(), "-None set-")));

			sb.append(new AdminRow("ISBN attribute", "Select the attribute to be used for ISBN codes on books. For another types of products use EAN", new SelectTag(context, "isbnAttribute",
					feed.getIsbnAttribute(), feed.getItemType().getAttributes(), "-None set-")));

            sb.append(new AdminRow("EAN attribute", "Select the attribute to be used for EAN.", new SelectTag(context, "eanAttribute",
					feed.getEanAttribute(), feed.getItemType().getAttributes(), "-None set-")));

            sb.append(new AdminRow("Shipping Weight attribute", "Select the attribute to be used for Shiping weight.", new SelectTag(context, "shipWeightAttribute",
					feed.getShipWeightAttribute(), feed.getItemType().getAttributes(), "-None set-")));

		}

		sb.append("</table>");
	}
}
