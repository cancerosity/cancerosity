package org.sevensoft.ecreator.iface.admin.attachments;

import java.util.logging.Logger;

import javax.servlet.ServletException;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.attachments.AttachmentSettings;
import org.sevensoft.ecreator.model.design.markup.MarkupTag;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 17-Jun-2005 17:37:55
 * 
 */
@Path("admin-settings-attachments.do")
public class AttachmentSettingsHandler extends AdminHandler {

	@SuppressWarnings("hiding")
	private static Logger					logger	= Logger.getLogger(AttachmentSettingsHandler.class.getName());

	private String						attachmentDescriptionDefault;
	private final transient AttachmentSettings	attachmentSettings;
	private Money						defaultFee;
	private int							defaultCredits;
	private int							defaultValidityDays;
	private String						previewExtensions;
	private String						mp3PreviewLength;
	private boolean						overwrite;
	private boolean						regenPreviews;
	private String						extensions;
	private String						extension;
	private Money						fee;
	private String						resetFeesExtension;
	private boolean						resetFees;
	private String						resetCreditsExtension;
	private int							credits;
	private boolean						resetCredits;

	public AttachmentSettingsHandler(RequestContext context) {
		super(context);
		this.attachmentSettings = AttachmentSettings.getInstance(context);
	}

	@Override
	public Object main() throws ServletException {

		AdminDoc doc = new AdminDoc(context, user, "Attachment settings", null);
		doc.addBody(new Body() {

			private void commands() {

				sb.append("<div align='center' class='actions'>");
				sb.append(new SubmitTag("Update attachment settings"));
				sb.append(new ButtonTag(SettingsMenuHandler.class, null, "Return to settings menu"));
				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Attachment description default", "Set a generic attachment description.", new TextTag(context,
						"attachmentDescriptionDefault", attachmentSettings.getDefaultDownloadLinkText(), 40)));

				if (isSuperman() || Module.UserMarkup.enabled(context)) {

					sb.append(new AdminRow("Default markup", new MarkupTag(context, "markup", attachmentSettings.getMarkup(), null, "markup")));

				}

				if (Module.Credits.enabled(context)) {
					sb.append(new AdminRow("Default Credits", "Set the default number of credits needed for a download.", new TextTag(context,
							"defaultCredits", attachmentSettings.getDefaultCredits(), 8)));

					sb.append(new AdminRow("Default validity days", "Sets the default number of days a customer can re-download an attachment.",
							new TextTag(context, "defaultValidityDays", attachmentSettings.getDefaultValidityDays(), 4)));
				}

				if (Module.AttachmentPayment.enabled(context)) {
					sb.append(new AdminRow("Default fee", "Set the default fee to the customer for a download.", new TextTag(context, "defaultFee",
							attachmentSettings.getDefaultFee(), 8)));
				}

				if (Module.AttachmentPreviews.enabled(context)) {
					sb.append(new AdminRow("Preview extensions",
							"Enter the filetypes for which you want to generate a preview. <i>Separate each extension with a comma</i>",
							new TextAreaTag(context, "previewExtensions", StringHelper.implode(attachmentSettings.getPreviewExtensions(), "\n"),
									40, 2)));
				}

				sb.append("</table>");
			}

			private void previewSettings() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Mp3 preview length", "Set the length of the MP3 preview in the format MM:SS", new TextTag(context,
						"mp3PreviewLength", attachmentSettings.getMp3PreviewLength(), 4)));

				if (isSuperman()) {

					sb.append(new AdminRow("Regenerate previews", "Regenerate all previews for all applicable attachment file types." +
							new CheckTag(context, "regenPreviews", true, false), "Overwrite existing " +
							new CheckTag(context, "overwrite", true, false) + " Extension " + new TextTag(context, "extensions", 5)));

				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(AttachmentSettingsHandler.class, "save", "POST"));

                commands();
				general();

				if (Module.AttachmentPreviews.enabled(context)) {

					if (attachmentSettings.isPreviewing("mp3")) {
						previewSettings();
					}
				}

				utils();
				commands();

				sb.append("</form>");

				return sb.toString();
			}

			private void utils() {

				boolean b = true;

				if (Module.AttachmentPayment.enabled(context)) {

					if (b) {
						sb.append(new AdminTable("Utils"));
						b = false;
					}

					sb.append(new AdminRow("Reset all fees",
							"Change the download fee on all attachments of the specified extension to the value entered." +
									new CheckTag(context, "resetFees", true, false), "Extension " +
									new TextTag(context, "resetFeesExtension", 5) + " Fee " + new TextTag(context, "fee", 5)));

				}

				if (Module.Credits.enabled(context)) {

					if (b) {
						sb.append(new AdminTable("Utils"));
						b = false;
					}

					sb.append(new AdminRow("Reset all credits",
							"Change the download credits on all attachments of the specified extension to the value entered." +
									new CheckTag(context, "resetCredits", true, false), "Extension " +
									new TextTag(context, "resetCreditsExtension", 5) + " Credits " + new TextTag(context, "credits", 5)));

				}

				if (!b) {
					sb.append("</table>");
				}
			}

		});
		return doc;

	}

	public Object makePreviews() throws ServletException {

		return main();
	}

	public Object save() throws ServletException {

		attachmentSettings.setDefaultDownloadLinkText(attachmentDescriptionDefault);
		attachmentSettings.setDefaultCredits(defaultCredits);
		attachmentSettings.setDefaultFee(defaultFee);
		attachmentSettings.setDefaultValidityDays(defaultValidityDays);
		attachmentSettings.setPreviewExtensions(StringHelper.explodeStrings(previewExtensions, ","));
		attachmentSettings.setMp3PreviewLength(mp3PreviewLength);
		attachmentSettings.save();

		if (regenPreviews) {
			attachmentSettings.makePreviews(extension, overwrite);
			addMessage("Previews regenerated");
		}

		if (resetFees) {
			if (fee != null) {
				attachmentSettings.resetFee(resetFeesExtension, fee);
				addMessage("Fees reset");
			}
		}

		if (resetCredits) {
			attachmentSettings.resetCredits(resetCreditsExtension, credits);
			addMessage("Credits reset");
		}

		clearParameters();
		addMessage("Attachment settings updated");
		return main();
	}
}
