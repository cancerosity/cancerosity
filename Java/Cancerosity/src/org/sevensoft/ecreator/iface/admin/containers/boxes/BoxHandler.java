package org.sevensoft.ecreator.iface.admin.containers.boxes;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.misc.html.EditMarkupButton;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.categories.Category;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupDefault;
import org.sevensoft.ecreator.model.design.template.TemplateSettings;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.misc.renderers.RendererUtil;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.db.positionable.EntityUtil;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextAreaTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import javax.servlet.ServletException;
import java.util.List;
import java.util.ArrayList;

/**
 * @author sks 9 Jul 2006 09:25:43
 */
public abstract class BoxHandler extends AdminHandler {

    private boolean displayAllCategories;
    private boolean displayAllItems;
    private boolean hideAllItems;
    private boolean displayBasket;
    private boolean displayCategories;
    private boolean displayCategoriesExcluding;
    private boolean displayCheckout;
    private boolean displayGuest;
    private boolean displayHome;
    private boolean displayItemsInCategories;
    private ItemType displayItemType;
    private boolean displayItemTypes;
    private boolean displayMembers;
    private boolean displayOthers;
    private boolean border;
    private String content;
    private String css;
    private String cssId;
    private boolean subcategories;
    private boolean who;
    private boolean simpleEditor;
    private String location;
    private String name;
    private boolean where;
    private Category displayCategory;
    private boolean displayMemberAccount;
    private Category category;
    private boolean caption;
    private String cssClass;
    private Markup markup;
    private boolean displayCategoriesExcludingSubcategories;
    private String captionText;
    private boolean visible;
    protected ItemType itemType;
    private Class style;
    private boolean displaySearchResults;
    private boolean displayMemberGroups;
    private boolean displayToGuests;
    private boolean displayToAllAccounts;
    private ItemType displayToItemType;
    private ItemType displayOnItemType;
    private ItemType displayOnSearchItemType;
    private boolean searchItemType;
    private boolean excludeCategories;
    private boolean removeCss;

    public BoxHandler(RequestContext context) {
        super(context);
    }

    public Object delete() throws ServletException {

        Box box = getBox();
        if (box == null) {
            return new BoxMenuHandler(context).main();
        }

        box.delete();
        box.log(user, "Deleted");

        return new BoxMenuHandler(context).main();
    }

    protected abstract Box getBox();

    @Override
    public final Object main() throws ServletException {

        final Box box = getBox();
        if (box == null) {
            return new BoxMenuHandler(context).main();
        }

        AdminDoc page = new AdminDoc(context, user, "Edit box", Tab.Categories);
        page.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update box"));
                sb.append(new ButtonTag(BoxMenuHandler.class, null, "Return to boxes menu"));
                sb.append("</div>");
            }

            private void content() {

                sb.append(new AdminTable("Content"));

                sb.append("<tr><td>" + new TextAreaTag(context, "content", box.getContent()).setId("content").setStyle("width: 100%; height: 320px;") +
                        "</td></tr>");

                sb.append("</table>");
            }

            private void css() {

                sb.append(new AdminTable("Css"));

                sb.append(new AdminRow(true, "Css identifiers", "Set the id and classname attribute for this box for use by your css code.", "Id: " +
                        new TextTag(context, "cssId", box.getCssId()) + " Classname: " + new TextTag(context, "cssClass", box.getCssClass())));

                if (box.hasCss()) {

                    sb.append(new AdminRow(true, "Css", "Enter css code you want to appear with this box.", new TextAreaTag(context, "css", box
                            .getCss(), 60, 4).setReadOnly(true) +
                            "" + new CheckTag(context, "removeCss", true, false)));

                }

                sb.append("</table>");
            }

            private void general() {

                sb.append(new AdminTable("General"));

                sb.append(new AdminRow("Name", "Enter a name for your reference for this box", new TextTag(context, "name", box.getName(), 40)));

                if (miscSettings.isAdvancedMode()) {
                    sb
                            .append(new AdminRow(
                                    "Enabled",
                                    "You can enabled / disable this box to show it on the site. This is useful if you want to temporarily hide a box without deleting it.",
                                    new BooleanRadioTag(context, "visible", box.isVisible())));
                }

                sb.append(new AdminRow("Caption", "Show this text as a caption at the top of the box", new TextTag(context, "captionText", box
                        .getCaptionText(), 40)));

                SelectTag tag = new SelectTag(context, "location", box.getLocation());
                tag.addOptions(TemplateSettings.getInstance(context).getPanels());

                sb.append(new AdminRow("Location", "Set this box to the left or right panels", tag));

                sb.append(new AdminRow("Border", "Wrap this box in a border", new BooleanRadioTag(context, "border", box.isBorder())));

                if (miscSettings.isAdvancedMode() || isSuperman()) {

                    if (box.hasEditableContent()) {

                        sb.append(new AdminRow(miscSettings.isJimMode(), "Simple editor", "Only use simple editor for this box.",
                                new BooleanRadioTag(context, "simpleEditor", box.isSimpleEditor())));

                    }
                }

                if (isSuperman() || Module.UserMarkup.enabled(context)) {

                    SelectTag markupTag = new SelectTag(context, "markup", box.getMarkup());
                    markupTag.setId("markup");
                    markupTag.setAny("Default");
                    markupTag.addOptions(Markup.get(context));

                    sb.append(new AdminRow(!Module.UserMarkup.enabled(context), "Markup", "This markup is used to render the box", markupTag + " " +
                            new EditMarkupButton("markup")));
                }

                sb.append("</table>");

            }

            @Override
            public String toString() {

                if (box.isAdvancedEditor()) {
                    RendererUtil.tinyMce(context, sb, "content");
                }

                sb.append(new FormTag(box.getEditHandler(), "save", "multi"));
                sb.append(new HiddenTag("box", box));

                commands();
                general();
                specifics(sb);

                if (miscSettings.isAdvancedMode()) {
                    where();

                    if (Module.Accounts.enabled(context)) {
                        who();
                    }
                }

                if (box.hasEditableContent()) {
                    content();
                }

                if (isSuperman() || Module.UserMarkup.enabled(context)) {
                    css();
                }

                commands();

                sb.append("</form>");

                return sb.toString();
            }

            protected void where() {

                sb.append(new AdminTable("Where to display"));

                sb.append(new AdminRow("Enable manual control", "Lets you specify exactly which pages this box should appear on.", new BooleanRadioTag(
                        context, "where", box.isWhere())));

                if (box.isWhere()) {

                    sb.append(new AdminRow("Home page", "Show on the home / front page.", new BooleanRadioTag(context, "displayHome", box
                            .isDisplayOnHome())));

                    sb.append(new AdminRow("All categories", "Show on all category pages not including home page (which is set separately).",
                            new BooleanRadioTag(context, "displayAllCategories", box.isDisplayOnAllCategories())));

                    List<ItemType> availableItemTypes1 = new ArrayList<ItemType>();
                    
                    if (ItemType.hasItemTypes(context)) {

                        sb.append(new AdminRow("All items", "Show on all item pages.", new BooleanRadioTag(context, "displayAllItems", box
                                .isDisplayOnAllItems())));

                        sb.append(new AdminRow("All items", "Hide on all item pages.", new BooleanRadioTag(context, "hideAllItems", box
                                .isHideOnAllItems())));

                        sb.append(new AdminRow("Items in categories", "Show only on items that are part of the specified categories below.",
                                new BooleanRadioTag(context, "displayItemsInCategories", box.isDisplayOnItemsInCategories())));

                        for (ItemType itemType : box.getDisplayOnItemTypes()) {

                            sb.append(new AdminRow("Specified item type", null, itemType.getName() + " " +
                                    new ButtonTag(box.getEditHandler(), "removeDisplayOnItemType", "Remove", "box", box, "itemType", itemType)));
                        }

                        List<ItemType> availableItemTypes = ItemType.get(context);
                        availableItemTypes1 = new ArrayList<ItemType>(availableItemTypes);

                        availableItemTypes.remove(box.getDisplayOnItemTypes());

                        if (availableItemTypes.size() > 0) {

                            SelectTag tag = new SelectTag(context, "displayOnItemType");
                            tag.setAny("-Add an item type-");
                            tag.addOptions(availableItemTypes);

                            sb.append(new AdminRow("Specified item type", null, tag));

                        }

                        sb.append(new AdminRow("Exclude",
                                "Exclude from categories specified, otherwise defaults to include only on categories specified.", new CheckTag(
                                context, "excludeCategories", "true", box.isExcludeCategories())));

                        sb.append(new AdminRow("Subcategories", "Include subcategories of those selected categories.", new BooleanRadioTag(context,
                                "subcategories", box.isSubcategories())));

                    }

                    if (Module.Shopping.enabled(context)) {

                        sb.append(new AdminRow("Checkout pages", "Show on checkout pages.", new BooleanRadioTag(context, "displayCheckout", box
                                .isDisplayOnCheckout())));

                        sb.append(new AdminRow("Basket pages", "Show on basket pages.", new BooleanRadioTag(context, "displayBasket", box
                                .isDisplayOnBasket())));

                    }

                    sb.append(new AdminRow("Search results", "Show on search results pages.", new BooleanRadioTag(context, "displaySearchResults",
                            box.isDisplayOnSearchResults())));

                    if (box.isDisplayOnSearchResults()) {
                        for (ItemType itemType : box.getDisplayOnSearchItemTypes()) {

                            sb.append(new AdminRow("Specified item type", null, itemType.getName() + " " +
                                    new ButtonTag(box.getEditHandler(), "removeDisplayOnItemType", "Remove", "box", box, "itemType", itemType, "searchItemType", true)));
                        }

                        if (availableItemTypes1.size() > 0) {

                            SelectTag tag = new SelectTag(context, "displayOnSearchItemType");
                            tag.setAny("-Add an item type-");
                            tag.addOptions(availableItemTypes1);

                            sb.append(new AdminRow("Specified item type", "To show the bax on all search pages don't add any itemTypes.", tag));

                        }
                    }

                    sb.append(new AdminRow("Other pages", "Display on other generic pages.", new BooleanRadioTag(context, "displayOthers", box
                            .isDisplayOnOthers())));

                    for (Category category : box.getDisplayOnCategories()) {

                        sb.append(new AdminRow("Specified category", null, category.getFullName(" > ") + " " +
                                new ButtonTag(box.getEditHandler(), "removeDisplayOnCategory", "Remove", "box", box, "category", category)));
                    }

                    SelectTag tag = new SelectTag(context, "displayCategory");
                    tag.setAny("-Add new category-");
                    tag.addOptions(Category.getCategoryOptions(context));

                    sb.append(new AdminRow("Specified category", null, tag));

                }

                sb.append("</table>");
            }

            protected void who() {

                sb.append(new AdminTable("Who to display to"));

                sb.append(new AdminRow("Enable manual control", "Lets you specify which users can see this box.", new BooleanRadioTag(context, "who",
                        box.isWho())));

                if (box.isWho()) {

                    sb.append(new AdminRow("Guests", "Show to guests, ie, those users who are not logged in.", new BooleanRadioTag(context,
                            "displayToGuests", box.isDisplayToGuests())));

                    sb.append(new AdminRow("All accounts", "Show to all accounts regardless of account.", new BooleanRadioTag(context,
                            "displayToAllAccounts", box.isDisplayToAllAccounts())));

                    for (ItemType itemType : box.getDisplayToItemTypes()) {

                        sb.append(new AdminRow("Account", "This box is visible to users of this account type.", itemType.getName() + " " +
                                new LinkTag(box.getEditHandler(), "removeDisplayToItemType", "Remove", "box", box, "itemType", itemType)));

                    }

                    List<ItemType> itemTypes = ItemType.getAccounts(context);
                    if (itemTypes.size() > 0) {
                        SelectTag selectTag = new SelectTag(context, "displayToItemType");
                        selectTag.setAny("-Select account-");
                        selectTag.addOptions(itemTypes);
                        sb.append(new AdminRow("Add new account", null, selectTag));

                    }

                }

                sb.append("</table>");
            }

        });
        return page;

    }

    public Object moveDown() throws ServletException {

        Box box = getBox();
        if (box == null) {
            return main();
        }

        List<Box> boxes = Box.get(context);
        boxes = Box.filter(boxes, box.getLocation());

        EntityUtil.moveDown(box, boxes);

        return new BoxMenuHandler(context).main();
    }

    public Object moveUp() throws ServletException {

        Box box = getBox();
        if (box == null) {
            return main();
        }

        List<Box> boxes = Box.get(context);
        boxes = Box.filter(boxes, box.getLocation());

        EntityUtil.moveUp(box, boxes);

        return new BoxMenuHandler(context).main();
    }

    public Object removeDisplayOnCategory() throws ServletException {

        Box box = getBox();
        if (box == null || category == null) {
            return main();
        }

        box.removeDisplayOnCategory(category);
        clearParameters();
        return main();
    }

    public Object removeDisplayOnItemType() throws ServletException {

        Box box = getBox();
        if (box == null) {
            return main();
        }

        if (itemType == null) {
            return main();
        }

        box.removeDisplayOnItemType(itemType, searchItemType);
        clearParameters();
        return main();

    }

    public Object removeDisplayToItemType() throws ServletException {

        Box box = getBox();
        if (box == null) {
            return main();
        }

        if (itemType == null) {
            return main();
        }

        box.removeDisplayToItemType(itemType);
        clearParameters();
        return main();

    }

    public Object save() throws ServletException {

        Box box = getBox();
        if (box == null) {
            return main();
        }

        box.setPanel(location);

        if (!ObjectUtil.equal(box.getName(), name)) {
            box.setName(name);
            box.log(user, "Name changed to " + name);
        }

        box.setBorder(border);
        box.setCaptionText(captionText);
        box.setContent(content);

        if (miscSettings.isAdvancedMode()) {

            box.setSimpleEditor(simpleEditor);
            box.setWhere(where);

            /*
                * Where
                */
            box.setDisplayOnAllCategories(displayAllCategories);
            box.setDisplayOnCheckout(displayCheckout);
            box.setDisplayOnOthers(displayOthers);
            box.setDisplayOnSearchResults(displaySearchResults);
            box.setDisplayOnHome(displayHome);
            box.setDisplayOnAllItems(displayAllItems);
            box.setHideOnAllItems(hideAllItems);
            box.setDisplayOnBasket(displayBasket);
            box.setDisplayOnItemsInCategories(displayItemsInCategories);
            box.setSubcategories(subcategories);
            box.setExcludeCategories(excludeCategories);

            if (box.isVisible() != visible) {
                box.setVisible(visible);
                box.log(user, "Visibility set to " + visible);
            }

            if (displayOnItemType != null) {
                box.addDisplayOnItemType(displayOnItemType);
            }

            if (displayOnItemType != null) {
                box.addDisplayOnItemType(displayOnItemType);
            }

            if (displayOnSearchItemType != null) {
                box.addDisplayOnItemType(displayOnSearchItemType, true);
            }

            if (displayCategory != null) {
                box.addDisplayCategory(displayCategory);
            }

            //who
            box.setWho(who);

            if (displayToItemType != null) {
                box.addDisplayToItemType(displayToItemType);
            }

            box.setDisplayToAllAccounts(displayToAllAccounts);
            box.setDisplayToGuests(displayToGuests);
        }

        /*
           * css
           */
        if (isSuperman() || Module.UserMarkup.enabled(context)) {

            box.setCssId(cssId);
            box.setCssClass(cssClass);

            if (!ObjectUtil.equal(box.getMarkup(), markup)) {
                box.setMarkup(markup);
                if (markup == null) {
                    box.log(user, "Markup set to default");
                } else {
                    box.log(user, "Markup set to #" + markup.getId() + " " + markup.getName());
                }
            }

            if (isSamIp() && removeCss) {
                box.removeCss();
            }
        }

        if (style != null) {

            try {

                MarkupDefault md = (MarkupDefault) style.newInstance();
                Markup markup = new Markup(context, md);
                box.setMarkup(markup);

            } catch (InstantiationException e) {
                e.printStackTrace();

            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        saveSpecific();
        box.save();

        box.log(user, "Updated");

        clearParameters();
        addMessage("Your changes have been saved.");
        return main();
    }

    protected abstract void saveSpecific() throws ServletException;

    protected abstract void specifics(StringBuilder sb);

    public Object switchLocation() throws ServletException {

        Box box = getBox();
        if (box == null)
            return new BoxMenuHandler(context).main();

        box.switchPanel();
        return new BoxMenuHandler(context).main();
    }

}
