package org.sevensoft.ecreator.iface.admin.crm.forum;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.crm.forum.Forum;
import org.sevensoft.ecreator.model.crm.forum.Post;
import org.sevensoft.ecreator.model.crm.forum.PostSearcher;
import org.sevensoft.ecreator.model.crm.forum.Topic;
import org.sevensoft.ecreator.model.items.Item;
import org.sevensoft.ecreator.model.items.sorts.SortType;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

import javax.servlet.ServletException;
import java.util.List;

/**
 * @author sks 27-Feb-2006 16:28:46
 */
@Path("admin-forum-posts-search.do")
public class PostSearchHandler extends AdminHandler {

    private transient List<Post> posts;
    private String keywords;
    private String author;
    private String title;
    private Forum forum;

    public PostSearchHandler(RequestContext context) {
        super(context);
    }

    public Object main() throws ServletException {

        AdminDoc page = new AdminDoc(context, user, "Posts search", Tab.Extras);
        page.setMenu(new ExtrasMenu());
        page.addBody(new Body() {

            private void results() {

                sb.append(new AdminTable("Search results"));

                sb.append("<tr>");
                sb.append("<th>Id</th>");
                sb.append("<th>Date</th>");
                sb.append("<th>Topic</th>");
                sb.append("<th>Author</th>");
                sb.append("<th>Summary</th>");
                sb.append("</tr>");

                for (Post post : posts) {

                    sb.append("<tr>");
                    sb.append("<td>" + new LinkTag(PostHandler.class, null, post.getIdString(), "post", post) + "</td>");
                    DateTime postDate = post.getDate();
                    sb.append("<td>" + (postDate != null ? postDate.toString("dd/MM/yy HH:mm") : "") + "</td>");
                    Topic topic = post.getTopic();
                    sb.append("<td>" + (topic != null ? topic.getTitle() : "") + "</td>");
                    Item account = post.getAccount();
                    String author;
                    try {
                        author = account.getDisplayName();
                    } catch (Exception e) {
                        author = "No name";
                    }
                    sb.append("<td>" + author + "</td>");
                    sb.append("<td>" + post.getSummary(100) + "</td>");
                    sb.append("</tr>");
                }

                sb.append("</table>");
            }

            private void search() {

                sb.append(new FormTag(PostSearchHandler.class, "search", "POST"));
                sb.append(new AdminTable("Post search"));

                sb.append("<tr><td width='300'><b>Keywords</b></td><td>" + new TextTag(context, "keywords", 40) + "</td></tr>");
                sb.append("<tr><td width='300'><b>Author</b></td><td>" + new TextTag(context, "author", 40) + "</td></tr>");
                sb.append("<tr><td width='300'><b>Topic title</b></td><td>" + new TextTag(context, "title", 40) + "</td></tr>");

                sb.append("<tr><td></td><td>" + new SubmitTag("Search") + "</td></tr>");

                sb.append("</table>");
                sb.append("</form>");
            }

            @Override
            public String toString() {

                search();
                if (posts != null && posts.size() > 0)
                    results();

                return sb.toString();
            }
        });
        return page;

    }

    public Object search() throws ServletException {

        PostSearcher searcher = new PostSearcher(context);
        searcher.setKeywords(keywords);
        searcher.setAuthor(author);
        searcher.setTitle(title);
        searcher.setSortType(SortType.Newest);
        searcher.setForum(forum);
        searcher.setLimit(25);

        posts = searcher.execute();

        return main();
    }
}
