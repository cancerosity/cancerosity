package org.sevensoft.ecreator.iface.admin.misc.content;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.misc.content.clouds.TagCloudBlock;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 15 Mar 2007 08:24:22
 *
 */
@Path("admin-blocks-tagcloud.do")
public class TagCloudBlockHandler extends BlockEditHandler {

	private TagCloudBlock	block;

	public TagCloudBlockHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Block getBlock() {
		return block;
	}

	@Override
	protected void saveSpecific() {
	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

}
