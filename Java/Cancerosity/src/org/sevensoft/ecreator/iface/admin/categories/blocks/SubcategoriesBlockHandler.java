package org.sevensoft.ecreator.iface.admin.categories.blocks;

import org.sevensoft.ecreator.iface.admin.containers.blocks.BlockEditHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.categories.blocks.SubcategoriesBlock;
import org.sevensoft.ecreator.model.containers.blocks.Block;
import org.sevensoft.ecreator.model.design.markup.Markup;
import org.sevensoft.ecreator.model.design.markup.MarkupTag;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 26 Jul 2006 10:34:47
 *
 */
@Path("admin-blocks-subcategory.do")
public class SubcategoriesBlockHandler extends BlockEditHandler {

	private SubcategoriesBlock	block;
	private Markup			markup;
	private boolean			selector;
	private String			introText;

	public SubcategoriesBlockHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected Block getBlock() {
		return block;
	}

	@Override
	protected String getTitle() {
		return "Subcategories block";
	}

	@Override
	protected void saveSpecific() {

		block.setSelector(selector);
		block.save();
		
		block.setIntroText(introText);

		if (isSuperman() || Module.UserMarkup.enabled(context)) {
			block.setMarkup(markup);
		}
	}

	@Override
	protected void specifics(StringBuilder sb) {

		sb.append(new AdminTable("Subcategories block details"));

		sb.append(new AdminRow("Selector", "Set to yes to show a drop down list for selecting subcategories.", new BooleanRadioTag(context, "selector",
				block.isSelector())));

		if (block.isSelector()) {

			sb.append(new AdminRow("Intro text", "The first option in the selector which informs the user to 'select a category'.", new TextTag(context,
					"introText", block.getIntroText(), 30)));

		}

		if (isSuperman() || Module.UserMarkup.enabled(context)) {

			sb.append(new AdminRow(true, "Markup", null, new MarkupTag(context, "markup", block.getMarkup(), "-Default-")));

		}

		sb.append("</table>");
	}

}
