package org.sevensoft.ecreator.iface.admin.reports.hits.site;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.reports.StatsMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHit;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHitCounter;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHitCounterDaily;
import org.sevensoft.ecreator.model.stats.hits.site.SiteHitCounterMonthly;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 03-Oct-2005 16:12:08
 * 
 */
@Path("admin-reports-hits-site.do")
public class SiteHitsHandler extends AdminHandler {

	private Date	start;
	private Date	end;

	public SiteHitsHandler(RequestContext context) {
		super(context);
	}

	public Object daily() throws ServletException {

		if (start == null)
			start = new Date().removeDays(30);

		if (end == null)
			end = new Date();

		final List<SiteHitCounterDaily> counters = SiteHitCounterDaily.get(context, start, end);

		AdminDoc doc = new AdminDoc(context, user, "Hits daily", Tab.Stats);
		doc.setMenu(new StatsMenu());
		doc.setIntro("Listed here are your total hits and unique hits per day dating from " + start.toString("dd-MMM-yyyy") + " to "
				+ end.toString("dd-MMM-yyyy"));
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Hits daily"));

				sb.append("<tr>");
				sb.append("<th>Date</th>");
				sb.append("<th>Page views</th>");
				sb.append("<th>Unique Visits</th>");
				sb.append("</tr>");

				for (SiteHitCounter counter : counters) {

					sb.append("<tr>");
					sb.append("<td>" + counter.getDate().toString("dd-MMM-yyyy") + "</td>");
					sb.append("<td>" + counter.getTotal() + "</td>");
					sb.append("<td>" + counter.getUnique() + "</td>");
					sb.append("</tr>");

				}

				sb.append("</table>");

				return sb.toString();
			}
		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {
		return daily();

	}

	public Object monthly() {

		if (start == null)
			start = new Date().removeYear(1).beginMonth();

		if (end == null)
			end = new Date().beginMonth();

		final List<SiteHitCounterMonthly> counters = SiteHitCounterMonthly.get(context, start, end);

		AdminDoc doc = new AdminDoc(context, user, "Hits monthly", Tab.Stats);
		doc.setMenu(new StatsMenu());
		doc.setIntro("Listed here are your total hits and unique hits per month dating from " + start.toString("MMMM yy") + " to "
				+ end.toString("MMMM yy"));
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Hits monthly"));

				sb.append("<tr>");
				sb.append("<th>Date</th>");
				sb.append("<th>Page views</th>");
				sb.append("<th>Visits</th>");
				sb.append("</tr>");

				for (SiteHitCounter counter : counters) {

					sb.append("<tr>");
					sb.append("<td>" + counter.getDate().toString("MMM-yyyy") + "</td>");
					sb.append("<td>" + counter.getTotal() + "</td>");
					sb.append("<td>" + counter.getUnique() + "</td>");
					sb.append("</tr>");

				}

				sb.append("</table>");

				return sb.toString();
			}
		});
		return doc;
	}

	public Object recent() {

		int limit = 200;
		final List<SiteHit> hits = SiteHit.getCurrent(context, limit);

		AdminDoc doc = new AdminDoc(context, user, "Recent hits", Tab.Stats);
		doc.setMenu(new StatsMenu());
		doc.setIntro("This screen shows your most recent " + limit + " hits.");
		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Recent hits"));

				sb.append("<tr>");
				sb.append("<th>Date / Time</th>");
				sb.append("<th>IP Address</th>");
				sb.append("<th>Page</th>");
				sb.append("<th>Browser</th>");
				sb.append("<th>Referrer</th>");
				sb.append("</tr>");

				for (SiteHit hit : hits) {

					sb.append("<tr>");
					sb.append("<td>" + new DateTime(hit.getDate()).toString("HH:mm dd-MMM-yy") + "</td>");
					sb.append("<td>" + hit.getIpAddress() + "</td>");
					sb.append("<td>" + new LinkTag(hit.getPage(), StringHelper.toSnippet(hit.getPage().replace("http://", ""), 40, "...")) + "</td>");
					sb.append("<td></td>");
					sb.append("<td>"
							+ (hit.hasReferrer() ? new LinkTag(hit.getReferrer(), StringHelper.toSnippet(
									hit.getReferrer().replace("http://", ""), 40, "...")) : "") + "</td>");
					sb.append("</tr>");
				}

				sb.append("</table>");

				return sb.toString();
			}

		});
		return doc;
	}
}
