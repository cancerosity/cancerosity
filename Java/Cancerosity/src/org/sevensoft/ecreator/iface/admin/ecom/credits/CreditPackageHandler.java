package org.sevensoft.ecreator.iface.admin.ecom.credits;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.items.listings.ListingPackageHandler;
import org.sevensoft.ecreator.iface.admin.system.SettingsMenuHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.graphics.SpannerGif;
import org.sevensoft.ecreator.model.accounts.subscriptions.SubscriptionLevel;
import org.sevensoft.ecreator.model.ecom.credits.CreditPackage;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 22-Mar-2006 18:45:22
 *
 */
@Path("admin-credits-packages.do")
public class CreditPackageHandler extends AdminHandler {

	private CreditPackage		creditPackage;
	private Money			fee;
	private int				credits;
	private boolean			introductory;
	private boolean			onRegistration;
	private boolean			bonus;
	private Money			onSpend;
	private int				onBids;
	private SubscriptionLevel	addSubscriptionLevel;
	private ItemType			addItemType;
	private ItemType			itemType;
	private SubscriptionLevel	subscriptionLevel;
	private String			name;

	public CreditPackageHandler(RequestContext context) {
		super(context);
	}

	public Object create() throws ServletException {

		creditPackage = new CreditPackage(context, true);
		return edit();
	}

	public Object delete() throws ServletException {

		if (creditPackage == null)
			return main();

		creditPackage.delete();
		return main();
	}

	public Object edit() throws ServletException {

		if (creditPackage == null) {
			return new SettingsMenuHandler(context).main();
		}

		AdminDoc doc = new AdminDoc(context, user, "Edit credit package", null);
		doc.addBody(new Body() {

			private void access() {

				sb.append(new AdminTable("Access"));

				if (Module.Subscriptions.enabled(context)) {

					List<SubscriptionLevel> currentSubscriptions = creditPackage.getSubscriptionLevels();
					if (currentSubscriptions.size() > 0) {

						StringBuilder sb2 = new StringBuilder();
						for (SubscriptionLevel subscriptionLevel : currentSubscriptions) {

							sb2.append(subscriptionLevel.getName() +
									" " +
									new LinkTag(ListingPackageHandler.class, "removeAccess", "Remove", "creditPackage", creditPackage,
											"subscriptionLevel", subscriptionLevel) + "<br/>");
						}

						sb.append(new AdminRow("Current subscription levels",
								"These are the subscriptions this package is currently available to.", sb2));

					}

					List<SubscriptionLevel> availableSubscriptions = SubscriptionLevel.get(context);
					availableSubscriptions.removeAll(currentSubscriptions);

					if (!availableSubscriptions.isEmpty()) {

						SelectTag tag = new SelectTag(context, "addSubscriptionLevel");
						tag.setAny("-Choose subscription level-");
						tag.addOptions(availableSubscriptions);

						sb.append(new AdminRow("Add subscription level", "Choose a subscription level to add to this package.", tag + " " +
								new SubmitTag("Add")));
					}

				}

				List<ItemType> itemTypes = creditPackage.getItemTypes();
				if (itemTypes.size() > 0) {

					StringBuilder sb2 = new StringBuilder();
					for (ItemType itemType : itemTypes) {

						sb2.append(itemType.getName() +
								" " +
								new LinkTag(CreditPackageHandler.class, "removeAccess", "Remove", "creditPackage", creditPackage, "itemType",
										itemType) + "<br/>");
					}

					sb.append(new AdminRow("Current item types", "These are the account types that have access to this package.", sb2));

				}

				List<ItemType> availableItemTypes = ItemType.getAccounts(context);
				availableItemTypes.removeAll(itemTypes);

				if (!availableItemTypes.isEmpty()) {

					SelectTag tag = new SelectTag(context, "addItemType");
					tag.setAny("-Choose item type-");
					tag.addOptions(availableItemTypes);

					sb.append(new AdminRow("Add account type", "Choose an account type to give access to this package.", tag + " " +
							new SubmitTag("Add")));
				}

				sb.append("</table>");

			}

			private void commands() {

				sb.append("<div align='center' class='actions'>");

				sb.append(new SubmitTag("Update credit package"));
				sb.append(new ButtonTag(CreditPackageHandler.class, "delete", "Delete credit package", "creditPackage", creditPackage)
						.setConfirmation("Are you sure you want to delete this credit package?"));
				sb.append(new ButtonTag(CreditPackageHandler.class, null, "Exit and return"));

				sb.append("</div>");
			}

			private void general() {

				sb.append(new AdminTable("General"));

				sb.append(new AdminRow("Name", "Enter a name for this package, eg, 'Bronze credit package'.", new TextTag(context, "name",
						creditPackage.getName(), 30)));

				sb.append(new AdminRow("Credits", "Enter how many credits you will receive with this package.", new TextTag(context, "credits",
						creditPackage.getCredits(), 4)));

				sb.append(new AdminRow("Bonus", "Sets this package for bonus credits rather than normal credits.", new BooleanRadioTag(context,
						"bonus", creditPackage.isBonus())));

				sb.append(new AdminRow("Introductory", "This is an introductory package that can only be used once per member.", new BooleanRadioTag(
						context, "introductory", creditPackage.isIntroductory())));

				sb.append(new AdminRow("Fee", "The cost to the customer of this package.", new TextTag(context, "fee", creditPackage.getFee(), 8)));

				if (creditPackage.isFree()) {

					sb.append(new AdminRow("On registration", "Apply this package automatically when a user registers.", new BooleanRadioTag(context,
							"onRegistration", creditPackage.isOnRegistration())));

					sb.append(new AdminRow("On spend", "Apply this package automatically when a member spends the amount specified here.",
							new TextTag(context, "onSpend", creditPackage.getOnSpend(), 8)));

				}

				sb.append("</table>");
			}

			@Override
			public String toString() {

				sb.append(new FormTag(CreditPackageHandler.class, "save", "POST"));
				sb.append(new HiddenTag("creditPackage", creditPackage));

                commands();
				general();
				access();
				commands();

				sb.append("</form>");

				return sb.toString();
			}
		});
		return doc;
	}

	@Override
	public Object main() {

		AdminDoc page = new AdminDoc(context, user, "Credit packages", null);
		page.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new AdminTable("Credit packages"));

				sb.append("<tr>");
				sb.append("<th>Credits</th>");
				sb.append("<th>Fee</th>");
				sb.append("<th width='60'>Remove</th>");
				sb.append("</tr>");

				int n = 1;
				for (CreditPackage creditPackage : CreditPackage.get(context)) {

					sb.append("<tr>");
					sb.append("<td>" + new LinkTag(CreditPackageHandler.class, "edit", new SpannerGif(), "creditPackage", creditPackage) + " " +
							creditPackage.getCredits() + "</td>");
					sb.append("<td>" + creditPackage.getFee() + "</td>");
					sb.append("<td width='60'>" +
							new ButtonTag(CreditPackageHandler.class, "delete", "Delete", "creditPackage", creditPackage)
									.setConfirmation("Are you sure you want to delete this credit package?") + "</td>");
					sb.append("</tr>");

					n++;
				}

				sb.append("<tr><td colspan='3'>Create new credit package" + new ButtonTag(CreditPackageHandler.class, "create", "Create") +
						"</td></tr>");
				sb.append("</table>");

				return sb.toString();
			}

		});
		return page;
	}

	public Object removeAccess() throws ServletException {

		if (creditPackage == null) {
			return main();
		}

		if (itemType != null) {
			creditPackage.removeAccess(itemType);
		}

		if (subscriptionLevel != null) {
			creditPackage.delete();
		}

		return edit();
	}

	public Object removeOwner() throws ServletException {

		if (creditPackage == null)
			return main();

		clearParameters();
		return edit();
	}

	public Object save() throws ServletException {

		if (creditPackage == null)
			return main();

		creditPackage.setFee(fee);
		creditPackage.setCredits(credits);
		creditPackage.setIntroductory(introductory);
		creditPackage.setBonus(bonus);
		creditPackage.setOnRegistration(onRegistration);
		creditPackage.setOnSpend(onSpend);
		creditPackage.setOnCashBids(onBids);
		creditPackage.setName(name);
		creditPackage.save();

		if (addItemType != null) {
			creditPackage.addAccess(addItemType);
		}

		if (addSubscriptionLevel != null) {
			creditPackage.addAccess(addSubscriptionLevel);
		}

		addMessage("Listing package updated");
		clearParameters();
		return edit();
	}
}
