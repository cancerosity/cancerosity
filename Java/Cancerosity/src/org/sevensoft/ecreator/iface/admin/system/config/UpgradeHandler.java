package org.sevensoft.ecreator.iface.admin.system.config;

import java.sql.SQLException;

import javax.servlet.ServletException;

import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.model.items.ItemType;
import org.sevensoft.ecreator.model.system.upgraders.FriendyUrlMapping;
import org.sevensoft.ecreator.model.system.upgraders.PdoAttributeUpgrader;
import org.sevensoft.ecreator.model.system.upgraders.Upgrade2;
import org.sevensoft.ecreator.model.system.upgraders.Upgrade6;
import org.sevensoft.ecreator.model.system.upgraders.UpgradeColType;
import org.sevensoft.ecreator.model.system.upgraders.UpgradeGws;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.ErrorTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.bool.CheckTag;
import org.sevensoft.jeezy.http.html.form.submit.ButtonTag;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;

/**
 * @author sks 01-Jun-2004 21:28:42
 */
@Path("admin-upgrades.do")
public class UpgradeHandler extends AdminHandler {

	private String	prefix;
	private boolean	images;
	private String	url;
	private String	columnType;

	private String	runCategories, runProducts;
	private ItemType	itemType;

	public UpgradeHandler(RequestContext context) {
		super(context);
	}

	public Object pdoAttributes() throws ServletException, SQLException {

		PdoAttributeUpgrader m = new PdoAttributeUpgrader(context);
		m.setItemType(itemType);
		int k = m.upgrade();

		return "Upgraded " + k + " listings";
	}

	public Object pdoUrls() throws ServletException, SQLException {

		FriendyUrlMapping m = new FriendyUrlMapping(context);
		String string = m.generate();

		return string;

	}

	public Object gws() throws ServletException {

		test(new RequiredValidator(), "prefix");
		if (hasErrors()) {
			return main();
		}

		UpgradeGws gws = new UpgradeGws(context, prefix, url, images);
		if (runCategories != null)
			gws.runCategories();
		else if (runProducts != null)
			gws.runProducts();
		else
			gws.runOther();
		addMessage("GWS conversion complete");

		clearParameters();
		return main();
	}

	@Override
	public Object main() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		AdminDoc doc = new AdminDoc(context, user, "Upgrader", null);
		doc.addBody(new Body() {

			private void gws() {

				sb.append(new AdminTable("Update from GWS"));
				sb.append(new FormTag(UpgradeHandler.class, "gws", "post"));

				sb.append(new AdminRow("Database prefix", "Include trailing underscore if used by the prefix. Eg, for pdo_categories use pdo_",
						new TextTag(context, "prefix", 12) + "" + new ErrorTag(context, "prefix", "<br/>")));

				sb.append(new AdminRow("Site url", "Enter the url of the site and the importer will retrieve missing images from url/images",
						new TextTag(context, "url", 40)));

				sb.append(new AdminRow("Add images", "Add images to the imported products", new CheckTag(context, "images", "true", true)));

				sb.append(new AdminRow("", null, new SubmitTag("runCategories", "Run categories")));
				sb.append(new AdminRow("", null, new SubmitTag("runProducts", "Run products")));
				sb.append(new AdminRow("", null, new SubmitTag("Run Rest")));
				sb.append("</form>");
				sb.append("</table>");

				sb.append("</table>");
			}

			@Override
			public String toString() {

				// v7();
				v6();
				v2();
				gws();
				v9();
				pdo();

				return sb.toString();
			}

			private void pdo() {
				sb.append(new AdminTable("PDO Conversions"));

				sb.append(new AdminRow("Url Mapper", null, new ButtonTag(UpgradeHandler.class, "pdoUrls", "pdoUrls")));

				sb.append(new AdminRow("Attributes", "Convert old fields to attributes", new ButtonTag(UpgradeHandler.class, "pdoAttributes",
						"pdoAttributes")));

				sb.append("</table>");
			}

			private void v2() {

				sb.append(new AdminTable("Upgrade from V2"));

				sb.append(new AdminRow("Upgrade from V2", "This upgrade will run all available upgrades on V2", new ButtonTag(UpgradeHandler.class,
						"upgrade602", "Upgrade 2 to 7")));

				sb.append("</table>");

			}

			private void v6() {

				sb.append(new AdminTable("Upgrade from V6"));

				sb.append(new AdminRow("Upgrade from V6", "This upgrade will run all available upgrades on V6", new ButtonTag(UpgradeHandler.class,
						"upgrade6to7", "Upgrade 6 to 7")));

				sb.append(new AdminRow("Upgrade newsletters from V6", "This upgrade will run all available upgrades on V6", new ButtonTag(
						UpgradeHandler.class, "upgrade67Newsletter", "Upgrade newsletters 6 to 7")));

				sb.append("</table>");

			}

			private void v9() {

				sb.append(new AdminTable("Upgrade ContentBlock columns types"));

				sb.append(new AdminRow("Upgrade ContentBlock columns types to MEDIUMTEXT", "", new ButtonTag(UpgradeHandler.class, "upgradeColumnType",
						"Upgrade column type", "columnType", "MEDIUMTEXT")));

				sb.append(new AdminRow("Upgrade ContentBlock columns types to LONGTEXT", "", new ButtonTag(UpgradeHandler.class, "upgradeColumnType",
						"Upgrade column type", "columnType", "LONGTEXT")));

				sb.append("</table>");

				sb.append(new AdminTable("Upgrade Item columns types"));

				sb.append(new AdminRow("Upgrade Item columns types to MEDIUMTEXT", "", new ButtonTag(UpgradeHandler.class, "upgradeItemColumnType",
						"Upgrade column type", "columnType", "MEDIUMTEXT")));

				sb.append(new AdminRow("Upgrade Item columns types to LONGTEXT", "", new ButtonTag(UpgradeHandler.class, "upgradeItemColumnType",
						"Upgrade column type", "columnType", "LONGTEXT")));

				sb.append("</table>");

			}

		});

		return doc;
	}

	public Object upgrade602() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		new Upgrade2(context).run();

		addMessage("Upgraded from 602");
		clearParameters();
		return main();
	}

	public Object upgrade67Newsletter() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		logger.config("[ConfigHandler] upgrade67Newsletter");
		new Upgrade6(context).newsletterSubscribers();

		addMessage("upgrade67Newsletter");
		clearParameters();
		return main();
	}

	public Object upgrade6to7() throws ServletException {

		if (!isSuperman()) {
			return index();
		}

		logger.config("[ConfigHandler] upgrade v6 to v7");
		new Upgrade6(context).run();

		addMessage("Upgraded from V6 to V7");
		clearParameters();
		return main();
	}

	public Object upgradeColumnType() throws ServletException {

		if (!isSuperman() || columnType == null) {
			return index();
		}

		logger.config("[CategoryHandler] upgrade ContentBlock columns' types");

		addMessage("Upgraded ContentBlock columns' types");

		try {
			new UpgradeColType(context, columnType).run();
		} catch (RuntimeException e) {
			logger.warning("[" + UpgradeHandler.class + "] error convert column type");
			clearMessages();
			addError("Can not convert column type to " + columnType + ".  " + e.getCause().getMessage());
		}
		clearParameters();
		return main();
	}

	public Object upgradeItemColumnType() throws ServletException {

		if (!isSuperman() || columnType == null) {
			return index();
		}

		logger.config("[CategoryHandler] upgrade ContentBlock columns' types");

		addMessage("Upgraded ContentBlock columns' types");

		try {
			new UpgradeColType(context, columnType).runItem();
		} catch (RuntimeException e) {
			logger.warning("[" + UpgradeHandler.class + "] error convert column type");
			clearMessages();
			addError("Can not convert column type to " + columnType + ".  " + e.getCause().getMessage());
		}
		clearParameters();
		return main();
	}

}