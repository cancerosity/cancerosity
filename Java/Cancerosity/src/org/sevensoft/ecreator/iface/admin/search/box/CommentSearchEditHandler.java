package org.sevensoft.ecreator.iface.admin.search.box;

import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminTable;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.AdminRow;
import org.sevensoft.ecreator.model.search.comments.CommentSearch;
import org.sevensoft.ecreator.model.comments.box.CommentListBox;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.html.form.submit.SubmitTag;
import org.sevensoft.jeezy.http.html.form.bool.BooleanRadioTag;
import org.sevensoft.jeezy.http.html.form.text.TextTag;
import org.sevensoft.jeezy.http.html.form.FormTag;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;

import javax.servlet.ServletException;

/**
 * User: Tanya
 * Date: 17.11.2011
 */
@Path("admin-search-edit-comments.do")
public class CommentSearchEditHandler extends AdminHandler {

    private CommentSearch search;
    private int limit;
    private String name;
    private boolean showOnlyChildren;

    public CommentSearchEditHandler(RequestContext context) {
        this(context, null);
    }

    public CommentSearchEditHandler(RequestContext context, CommentSearch search) {
        super(context);
        this.search = search;
    }

    @Override
    public Object main() throws ServletException {

        if (search == null) {
            return index();
        }

        AdminDoc doc = new AdminDoc(context, user, "Edit search", null);
        doc.addBody(new Body() {

            private void commands() {

                sb.append("<div align='center' class='actions'>");
                sb.append(new SubmitTag("Update search"));
                sb.append("</div>");
            }

            private void general() {

                sb.append(new AdminTable("Search details"));


                sb.append(new AdminRow("Currently viewed item comments", "Show only those comments that are attached to the currently viewed item/category", new BooleanRadioTag(
                        context, "showOnlyChildren", search.isShowOnlyChildren())));

                TextTag limitTag = new TextTag(context, "limit", search.getLimit(), 4);
                sb.append(new AdminRow("Limit", "Limit search to a max number of results", limitTag));

                sb.append("</table>");
            }

            @Override
            public String toString() {

                sb.append(new FormTag(CommentSearchEditHandler.class, "save", "post"));
                sb.append(new HiddenTag("search", search));

                commands();
                general();
                commands();

                sb.append("</form>");

                return sb.toString();
            }

        });
        return doc;
    }

    /**
     * This mehod is called when a user is adding another attribute
     */
    public Object save() throws ServletException {

        if (search == null) {
            return index();
        }

        search.setShowOnlyChildren(showOnlyChildren);
        search.setLimit(limit);
        search.setName(name);
        search.save();

        // if this search is for an items box then flush cache
        if (search.hasCommentListBox()) {
            CommentListBox.clearHtmlCache();
        }

        return new ActionDoc(context, "This search has been updated", new Link(CommentSearchEditHandler.class, null, "search", search));
    }

}