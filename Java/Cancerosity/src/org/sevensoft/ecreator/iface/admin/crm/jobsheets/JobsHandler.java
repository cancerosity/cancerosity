package org.sevensoft.ecreator.iface.admin.crm.jobsheets;

import java.util.List;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.AdminDoc;
import org.sevensoft.ecreator.iface.admin.AdminHandler;
import org.sevensoft.ecreator.iface.admin.crm.jobsheets.panels.JobsPanel;
import org.sevensoft.ecreator.iface.admin.extras.ExtrasMenu;
import org.sevensoft.ecreator.iface.admin.system.lookandfeel.Tab;
import org.sevensoft.ecreator.model.crm.jobsheets.Job;
import org.sevensoft.ecreator.model.crm.jobsheets.JobSearcher;
import org.sevensoft.ecreator.model.crm.jobsheets.Jobsheet;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.http.Body;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.results.docs.ActionDoc;
import org.sevensoft.jeezy.http.util.Link;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.util.ResultsControl;

/**
 * @author sks 18 Jul 2006 16:57:43
 *
 */
@Path("admin-jobsheets-myjobs.do")
public class JobsHandler extends AdminHandler {

	private String	name;
	private int		page;
	private User	owner;
	private Jobsheet	jobsheet;
	private List<Job>	jobs;

	public JobsHandler(RequestContext context) {
		super(context);
	}

	public Object add() throws ServletException {

		if (name == null) {
			return myjobs();
		}

		Job job = new Job(context, null, user, name);
		job.setOwner(user);
		job.setJobsheet(jobsheet);

		return new ActionDoc(context, "A new job has been added", new Link(JobsHandler.class));
	}

	public Object deleteJobs() throws ServletException {

		if (jobs != null) {
			for (Job job : jobs) {
				job.delete();
			}
		}

		return myjobs();
	}

	private Object jobs(final List<Job> jobs, Results results, String title) {

		AdminDoc doc = new AdminDoc(context, user, title, Tab.Extras);
		doc.setMenu(new ExtrasMenu());

		if (results != null && results.hasMultiplePages()) {
			doc.addBody(new ResultsControl(context, results, new Link(JobsHandler.class, "unassigned")));
		}

		doc.addBody(new Body() {

			@Override
			public String toString() {

				sb.append(new JobsPanel(context, jobs));
				return sb.toString();
			}

		});
		return doc;
	}

	@Override
	public Object main() throws ServletException {
		return myjobs();
	}

	public Object myjobs() throws ServletException {

		final List<Job> myJobs = user.getJobs();
		return jobs(myJobs, null, "My jobs: " + user.getName());
	}

	public Object unassigned() {

		JobSearcher searcher = new JobSearcher(context);
		searcher.setUnassigned();

		int count = searcher.size();
		Results results = new Results(count, page, 50);

		searcher.setStart(results.getStartIndex());
		searcher.setLimit(50);

		List<Job> unassignedJobs = searcher.execute();
		return jobs(unassignedJobs, results, "New jobs");
	}
}
