package org.sevensoft.ecreator.iface.admin.extras.banners;

import javax.servlet.ServletException;

import org.sevensoft.ecreator.iface.admin.containers.boxes.BoxHandler;
import org.sevensoft.ecreator.model.advertising.banners.boxes.BannerBox;
import org.sevensoft.ecreator.model.containers.boxes.Box;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 2 Aug 2006 11:27:28
 *
 */
@Path("admin-banners-box.do")
public class BannerBoxHandler extends BoxHandler {

	private BannerBox	box;

	public BannerBoxHandler(RequestContext context) {
		super(context);
	}

	@Override
	protected void specifics(StringBuilder sb) {
	}

	@Override
	protected Box getBox() {
		return box;
	}

	@Override
	protected void saveSpecific() throws ServletException {
	}

}
