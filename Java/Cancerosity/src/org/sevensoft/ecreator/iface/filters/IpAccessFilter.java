package org.sevensoft.ecreator.iface.filters;

import org.sevensoft.commons.validators.net.IpAddressValidator;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User: Tanya Date: 28.11.2011
 */
public class IpAccessFilter implements Filter {

	public void init(FilterConfig filterConfig) throws ServletException {
	}

	public void doFilter(ServletRequest servletRequest,
			ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		// System.out.println("In IpAccessFilter");
		String domain = request.getHeader("host");

		if (new IpAddressValidator().validate(domain.split(":")[0]) == null) {
			response.sendError(HttpServletResponse.SC_MOVED_PERMANENTLY);
			// filterChain.doFilter(request, response);
			return;
		}

		filterChain.doFilter(servletRequest, servletResponse);
	}

	public void destroy() {
	}
}
