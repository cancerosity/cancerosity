package org.sevensoft.ecreator.iface.filters;

import org.apache.commons.io.IOUtils;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.JNDIUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

/**
 * User: MeleshkoDN
 * Date: 22.10.2007
 * Time: 17:32:30
 */
public class ResourcesFilter implements Filter {

    private static Logger logger = Logger.getLogger("filter");

    private static String[] resourcesDirs;
    private static String[] specificFiles;

    static {

        resourcesDirs = new String[]{Config.TemplateDataPath, Config.AttachmentsPath, Config.ImagesPath,
                Config.UserDataPath, Config.ThumbnailsPath, Config.TemplateStorePath,
                Config.MediaPath, Config.FeedDataPath, Config.TmpPath,
                Config.PostcodesPath, Config.FontsPath, Config.TemplateTitlesPath,
                Config.VideosPath, Config.CaptchaPath, Config.FlashLargePath, Config.FlashSmallPath,
                Config.SatNavPath, Config.PdfsPath, Config.ArticlesPath, Config.SitePath, Config.DownloadsPath,
                Config.StoragePath, Config.UploadsPath, Config.NewsLetterPath, Config.TinyMCETemplatesPath,
                Config.EmailLogoPath};

        specificFiles = new String[]{".swf", ".txt"};
    }

    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String servletPath = request.getServletPath();
        if (hasResourceRequest(servletPath)) {
            if (servletPath.indexOf(".css") != -1) {
                response.setContentType("text/css;charset=UTF-8");
            } /*else if (servletPath.indexOf(".ogv") != -1) {
                response.setContentType("video/ogg");        //todo this code doesn't help for IE9
            }*/
            processResourceRequest(request, response);
            return;
        } else if (hasSpecificFileRequest(servletPath)) {
            processResourceRequest(request, response);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }


    public void destroy() {

    }

    private boolean hasResourceRequest(String servletPath) {
        for (String dir : resourcesDirs) {
            if (servletPath.startsWith("/" + dir + "/")) {
                return true;
            }
        }
        return false;
    }

    private boolean hasSpecificFileRequest(String servletPath) {
        for (String extension : specificFiles) {
            if (servletPath.contains(extension)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Analyzes request & searches in resources for requested file
     *
     * @param request
     * @return found file
     */
    private File getRequestedFile(HttpServletRequest request) {
        String servletPath = request.getServletPath();
        File requestedFile = new File(JNDIUtils.getResourcesPath() + servletPath);
        return requestedFile.exists() ? requestedFile : null;
    }


    protected void processResourceRequest(HttpServletRequest request, HttpServletResponse response) {
        File requestedFile = getRequestedFile(request);
        if (requestedFile == null) {
            return;
        }
        InputStream in = null;
        ServletOutputStream out = null;
        try {
            in = new FileInputStream(requestedFile);
            out = response.getOutputStream();
            IOUtils.copy(in, out);
            out.flush();
        } catch (IOException e) {
            logger.warning("[" + ResourcesFilter.class.getName() + "]. Requested File:  " + requestedFile + ". Exception" + e);
        } finally {
            IOUtils.closeQuietly(in);
            IOUtils.closeQuietly(out);
        }

    }
}
