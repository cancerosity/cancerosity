package org.sevensoft.ecreator.iface.filters;

import java.io.IOException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.ecreator.util.log.MDC;

/**
 * @author sks 21-Dec-2005 09:34:59
 * 
 */
public class RewriteFilter implements Filter {

	private Pattern	htmlPattern;
    private Pattern	htmlPatternWrong;

	public void destroy() {
		htmlPattern = null;
	}

	public void doFilter(ServletRequest arg0, final ServletResponse arg1, FilterChain chain) throws ServletException, IOException {

		HttpServletRequest request = (HttpServletRequest) arg0;
		String path = request.getServletPath();

//        MDC.domain = request.getHeader("referer");

        Matcher matcher = htmlPattern.matcher(path);
        Matcher matcherWrong = htmlPatternWrong.matcher(path);
        //redirect non-www sites to www
//        if (request.getRequestURL().indexOf("www") == -1 && (request.getRequestURL().indexOf(".com") != -1 || request.getRequestURL().indexOf(".co.uk") != -1)) {
//            ((HttpServletResponse) arg1).sendRedirect(request.getRequestURL().toString().replace("://", "://www."));
//            return;
//        }

		if (matcher.matches() && !matcherWrong.matches()) {

			String type = matcher.group(1);
			String id = matcher.group(2);

			Map<String, String> params = StringHelper.explodeMap(matcher.group(3), "&", "=");

			StringBuilder url = new StringBuilder();

			if ("i".equals(type)) {
				url.append("/item.do?item=");
				url.append(id);
                url.append("&path=");
                url.append(path.substring(1));
                url.append("&canonical=http://");                 //use HTTP both for http and https
                url.append(request.getServerName());
                url.append(path);
			}

			else if ("c".equals(type)) {
				url.append("/category.do?category=");
				url.append(id);
                url.append("&path=");
                url.append(path.substring(1));
                url.append("&canonical=http://");
                url.append(request.getServerName());
                url.append(path);
			}

			// bc for old member accounts
			else if ("m".equals(type)) {
				url.append("/item.do?item=");
				url.append(id);
                url.append("&path=");
                url.append(path.substring(1));
                url.append("&canonical=http://");
                url.append(request.getServerName());
                url.append(path);
			}

			else if ("g".equals(type)) {
				url.append("/gallery.do?gallery=");
				url.append(id);
                url.append("&path=");
                url.append(path.substring(1));
                url.append("&canonical=http://");
                url.append(request.getServerName());
                url.append(path);
			}

			if (params.size() > 0) {
				for (Map.Entry<String, String> entry : params.entrySet()) {
					url.append("&");
					url.append(entry.getKey());
					url.append("=");
					url.append(entry.getValue());
				}
			}

			arg0.getRequestDispatcher(url.toString()).forward(arg0, arg1);
			return;

		} else {

			if (path.contains("property-to-rent.html")) {
				arg0.getRequestDispatcher("category.do?category=9116").forward(arg0, arg1);
			}

			if (path.contains("property-for-sale.html")) {
				arg0.getRequestDispatcher("category.do?category=9145").forward(arg0, arg1);
			}

			// try a match in our manual mappings
			//			SimpleQuery.get(context, Mapping.class, "mapping", path);
		}

		chain.doFilter(arg0, arg1);
	}

	public void init(FilterConfig arg0) throws ServletException {
		htmlPattern = Pattern.compile(".*-([a-z])(\\d+)\\.html\\??(.*)$");

        //to avoid links like "/smth/items-c377.html"    
        htmlPatternWrong = Pattern.compile("/.*([a-z])/.*-([a-z])(\\d+)\\.html\\??(.*)$");
	}

}
