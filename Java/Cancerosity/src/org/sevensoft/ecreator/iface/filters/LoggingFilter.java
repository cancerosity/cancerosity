package org.sevensoft.ecreator.iface.filters;

import org.sevensoft.ecreator.util.log.MDC;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
public class LoggingFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;


        String domain = request.getRequestURL().append(" Referer:").append(request.getHeader("referer")).toString();

        MDC.domain = domain;

        filterChain.doFilter(servletRequest, servletResponse);
        return;

    }

    public void destroy() {
    }
}
