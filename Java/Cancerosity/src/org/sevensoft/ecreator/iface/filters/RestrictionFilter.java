package org.sevensoft.ecreator.iface.filters;

import org.sevensoft.commons.superstrings.HtmlHelper;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.logging.Logger;

/**
 * User: Tanya
 * Date: 29.05.2012
 */
public class RestrictionFilter implements Filter {

    private Pattern htmlPattern;

    public void init(FilterConfig filterConfig) throws ServletException {
        htmlPattern = Pattern.compile(".[a-z0-9_-]*\\.do\\??(.*)$", Pattern.CASE_INSENSITIVE);
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        StringBuilder sb = new StringBuilder(request.getServletPath());
        String queryString = request.getQueryString();
        if (queryString != null) {
            sb.append("?");
            sb.append(queryString);
        }
        String path = sb.toString();
        Matcher matcher = htmlPattern.matcher(path);


        if (matcher.matches()) {
            String stripped = HtmlHelper.stripXss(path, null);
            if (path.equals(stripped)) {

                filterChain.doFilter(servletRequest, servletResponse);
            } else {
               Logger.getLogger("error").severe("[RestrictionFilter] wrong request was sent " + request.getRequestURL() + "?" + path +
                        " from IP " + request.getRemoteHost() + ", addr  " + request.getRemoteAddr());
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
//                servletRequest.getRequestDispatcher(request.getRequestURL() + "?" + stripped).forward(servletRequest, servletResponse);
//                response.sendRedirect(stripped);
//                return;
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    public void destroy() {
        htmlPattern = null;
    }
}
