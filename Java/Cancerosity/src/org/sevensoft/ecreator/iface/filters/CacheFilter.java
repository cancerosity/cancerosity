package org.sevensoft.ecreator.iface.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User: Tanya
 * Date: 01.07.2011
 */
public class CacheFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        response.setHeader("Cache-Control", "public, max-age=4536000");     // 30 days
        filterChain.doFilter(servletRequest, servletResponse);
    }

    public void destroy() {
    }
}
