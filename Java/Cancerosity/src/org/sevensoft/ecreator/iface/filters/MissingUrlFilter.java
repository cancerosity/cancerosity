package org.sevensoft.ecreator.iface.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.ecreator.model.system.config.MissingUrlUtil;

/**
 * 
 * @author Bing 2013/10/31
 */
public class MissingUrlFilter implements Filter {

	public void init(FilterConfig filterConfig) throws ServletException {
	}

	public void doFilter(ServletRequest servletRequest,
			ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		
		String context = request.getContextPath();
		if(context==null || context.trim().length()==0){
			context = "/";
		}

		String requestUrl = request.getRequestURI();
		//remove context from the url
		String prettyUrl = prettryUrl(context, requestUrl);
		//System.out.println("context : "+context);
		//System.out.println("requestUrl : "+requestUrl);
		//System.out.println("prettyUrl : "+prettyUrl);
		String redirectUrl = MissingUrlUtil.isMissingUrl(prettyUrl);
		//if the request url is a missing url,send 30E
		if (redirectUrl != null) {
			response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
			response.addHeader("Location", redirectUrl);
			// filterChain.doFilter(request, response);
			return;
		}

		filterChain.doFilter(servletRequest, servletResponse);
	}

	/**
	 * remove context from the request url /ecreator/test.do to test.do /test.do
	 * to test.do
	 * 
	 * @param input
	 */
	public static String prettryUrl(String context, String input) {
		String url = "";
		if (context.length() > 1) {
			url = input.substring(context.length() + 1);
		} else if (context.length() == 1) {
			url = input.substring(1);
		}
		return url;
	}

	public void destroy() {
	}
}
