package org.sevensoft.commons.skint;

import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.jeezy.http.RequestContext;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * @author Sam 14-Oct-2003 10:25:10
 */
public class Money implements Comparable<Money> {

    private static java.text.NumberFormat wholeFormatter, editFormatter, displayFormatter;

    static {
        Locale locale = Company.getInstance(RequestContext.getInstance()).getLocale();
        editFormatter = java.text.NumberFormat.getInstance(locale);

        editFormatter = NumberFormat.getInstance(locale);
        editFormatter.setMinimumFractionDigits(2);
        editFormatter.setMaximumFractionDigits(2);
        editFormatter.setGroupingUsed(false);

        displayFormatter = java.text.NumberFormat.getInstance(locale);

        displayFormatter = NumberFormat.getInstance(locale);
        displayFormatter.setMinimumFractionDigits(2);
        displayFormatter.setMaximumFractionDigits(2);
        displayFormatter.setGroupingUsed(true);

        wholeFormatter = java.text.NumberFormat.getInstance(locale);
        wholeFormatter.setMaximumFractionDigits(0);
        wholeFormatter.setGroupingUsed(false);
    }

    private final int amount;

    public Money(double amount) {
        this.amount = (int) Math.round(amount);
    }

    public Money(int amount) {
        this.amount = amount;
    }

    public Money(Integer amount) {
        this.amount = amount;
    }

    public Money(Money money) {
        this.amount = money.amount;
    }

    public Money(String string) {
        this(Double.parseDouble(string.replaceAll("[^\\d.-]", "").trim()) * 100);
    }

    public Money abs() {
        return new Money(Math.abs(amount));
    }

    public Money add(int i) {
        return new Money(amount + i);
    }

    public Money add(Money money) {
        return money == null ? this : new Money(this.amount + money.amount);
    }

    /*
      * @see java.lang.Comparable#compareTo(org.sevensoft.commons.util.money.Money)
      */
    public int compareTo(Money arg0) {
        if (amount == arg0.amount) {
            return 0;
        } else if (amount < arg0.amount) {
            return -1;
        }
        return 1;
    }

    public Money divide(double d) {
        return new Money((int) Math.round(amount / d));
    }

    public boolean equals(int a) {
        return amount == a;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Money) {
            return ((Money) obj).amount == amount;
        }
        return false;
    }

    public int getAmount() {
        return amount;
    }

    /**
     * Returns the amount in base units as a string, eg a string of pennies
     */
    public String getAmountString() {
        return String.valueOf(getAmount());
    }

    public int hashCode() {
        return amount;
    }

    public boolean isGreaterThan(int i) {
        return amount > i;
    }

    public boolean isGreaterThan(Money money) {
        return amount > money.amount;
    }

    public boolean isGreaterThanOrEquals(Money money) {
        return amount >= money.amount;
    }

    /**
     *
     */
    public boolean isLessThan(int pence) {
        return amount < pence;
    }

    public boolean isLessThan(Money money) {
        return amount < money.amount;
    }

    public boolean isLessThanOrEquals(Money money) {
        return amount <= money.amount;
    }

    public boolean isNegative() {
        return amount < 0;
    }

    public boolean isPositive() {
        return amount > 0;
    }

    public boolean isZero() {
        return amount == 0;
    }

    public Money minus(int i) {
        return new Money(amount - i);
    }

    public Money minus(Money money) {
        return subtract(money);
    }

    public Money multiply(double d) {
        return new Money((int) Math.round(amount * d));
    }

    public Money negative() {
        return new Money(-amount);
    }

    public Money subtract(int i) {
        return new Money(amount - i);
    }

    public Money subtract(Money money) {
        return money == null ? this : new Money(amount - money.amount);
    }

    public String toEditString() {
        return editFormatter.format(amount / 100d);
    }

    public String toString() {
        return displayFormatter.format(amount / 100d);
    }

    public String toString(int dp) {

        if (dp == 0) {
            return toWholeString();
        }

        if (dp == 2) {
            return toString();
        }

        NumberFormat format = NumberFormat.getInstance(Locale.UK);
        format.setMinimumFractionDigits(dp);
        format.setMaximumFractionDigits(dp);
        format.setGroupingUsed(true);

        return format.format(amount / 100d);
    }

    public String toWholeString() {
        return wholeFormatter.format(amount / 100d);
    }

}