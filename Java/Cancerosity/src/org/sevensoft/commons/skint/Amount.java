package org.sevensoft.commons.skint;

/**
 * @author sks 6 Aug 2006 17:14:04
 *
 */
public final class Amount {

	public static Amount parse(String string) {

		if (string == null) {
			return null;
		}

		// get rid of all characters that are not a decimal point, digit or % symbol
		string = string.replaceAll("[^%.\\d-]", "");

		// if string ends with a % then we will remove it and parse for a double
		if (string.endsWith("%")) {

			try {
				return new Amount(Double.parseDouble(string.substring(0, string.length() - 1)));
			} catch (NumberFormatException e) {
				return null;
			}

		} else {

			// otherwise we will create a money object
			try {
				return new Amount(new Money(string));
			} catch (RuntimeException e) {
				return null;
			}
		}
	}

	private final double	percentage;
	private final Money	fixed;

	public Amount(double d) {
		this.percentage = d;
		this.fixed = new Money(0);
	}

	public Amount(double d, Money fixed) {

		if (d > 0) {
			this.percentage = d;
			this.fixed = new Money(0);

		} else {

			this.fixed = fixed;
			this.percentage = 0;
		}
	}

	public Amount(int i) {
		this.percentage = 0;
		this.fixed = new Money(i);
	}

	public Amount(Money fixed) {
		this.fixed = fixed;
		this.percentage = 0;
	}

	public Money calculate(Money money) {

		if (percentage == 0) {
			return fixed;
		}

		return money.multiply(percentage / 100d);
	}

	public Money calculateMargin(Money money) {

		if (percentage == 0) {
			return fixed;
		}

		return money.divide(1d - (percentage / 100d));
	}

    public Money calculateDiscount(Amount amount) {
        if (amount.getPercentage() != 0) {
            return getFixed().multiply(1d - (amount.getPercentage() / 100d));
        } else if (amount.getFixed() != null) {
            return getFixed().multiply((10000d - amount.getFixed().getAmount()) / 10000d);
        }
        return getFixed();
    }

	public Money getFixed() {
		return fixed;
	}

	public double getPercentage() {
		return percentage;
	}

	@Override
	public String toString() {
		return percentage > 0 ? percentage + "%" : fixed.toEditString();
	}

	/**
	 * 
	 */
	public boolean isFixed() {
		return percentage == 0;
	}

    public boolean isLessThan(Amount discount) {
        if (isFixed() && discount.isFixed()) {
            return getFixed().isLessThan(discount.getFixed());
        } else if (!isFixed() && !discount.isFixed()) {
            return getPercentage() < discount.getPercentage();
        } else {
            if (!isFixed()) {
                return new Money(getPercentage()).isLessThan(discount.getFixed());
            } else if (isFixed()) {
                return getFixed().isLessThan(new Money(discount.getPercentage()));
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Amount){
            Amount amount = (Amount)obj;
            return getFixed().equals(amount.getFixed()) && getPercentage() == amount.getPercentage();
        }
        return false;
    }
}
