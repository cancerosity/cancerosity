package org.sevensoft.commons.smail;

import org.sevensoft.ecreator.model.system.company.Company;
import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.system.modules.Module;
import org.sevensoft.jeezy.http.RequestContext;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.imageio.ImageIO;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.logging.Logger;

/**
 * @author sks 14-Apr-2005 00:22:01
 */
public class Email {

    private static boolean development = false;
    protected static final Logger logger = Logger.getLogger("commons");

    public static final void setDevelopment(boolean development) {
        Email.development = development;
    }

    private Set<String> cc, bcc;
    private String body, bodyType, fromAddress, fromName, to, subject;

    private Map<File, String> files;

    public Email() {
        cc = new HashSet();
        bcc = new HashSet();
        files = new HashMap();
    }

    public Email(String from, String subject, CharSequence body) {
        this();

        setFrom(from, from);
        setSubject(subject);
        setBody(body.toString());
    }

    public void addAttachment(File file) {
        addAttachment(file, file.getName());
    }

    public void addAttachment(File file, String name) {
        files.put(file, name);
    }

    public void addBcc(String email) {
        bcc.add(email.toLowerCase().trim());
    }

    public void addBccs(List<String> emails) {
        this.bcc.addAll(emails);
    }

    public void addCc(String email) {
        cc.add(email.toLowerCase().trim());
    }

    public void addImage(RenderedImage image, String name) throws IOException {

        File file = File.createTempFile("email_image", null);
        file.deleteOnExit();

        ImageIO.write(image, "jpeg", file);
        addAttachment(file, name);
    }

    public void addRecipients(Collection<String> recipients) {

        for (String recip : recipients) {

            if (to == null) {
                setTo(recip);
            } else {
                addCc(recip);
            }
        }
    }

    public void clearBcc() {
        bcc.clear();
    }

    public void clearCc() {
        cc.clear();
    }

    public void send(String hostname) throws EmailAddressException, SmtpServerException {
        send(hostname, null, null);
    }

    public void send(String hostname, String username, String password) throws EmailAddressException, SmtpServerException {

        /*
           * If we are in localhost development mode I want to always send emails to me, from me, via eclipse
           * IE, override any other settings
           */
        /*if (development) {

            logger.fine("[Email] development mode");

            to = "sks@7soft.co.uk";
            cc.clear();
            bcc.clear();
            hostname = "mail.7soft.co.uk";
            username = "sks";
            password = "keyboard";
            fromAddress = "test@7soft.co.uk";
            fromName = "sam's development machine";
        }*/
        if (to == null) {
            logger.warning("No to address set");
            throw new EmailAddressException("No to address set");
        }

        Properties props = new Properties();
        props.put("mail.smtp.host", hostname);

        Session session;
        if (username != null && password != null) {
            props.put("mail.smtp.port", "2525");
            props.put("mail.smtp.auth", "true");
            session = Session.getInstance(props, new SMTPAuthenticator(username, password));
        } else {
            session = Session.getInstance(props);
        }
        session.setDebug(false);

        Message message = new MimeMessage(session);

        try {

            // set send date
            message.setSentDate(new Date());

            // set subject
            message.setSubject(subject);

            // set from address
            InternetAddress from = new InternetAddress(fromAddress, fromName);
            message.setFrom(from);

            // Create multipart to hold all the parts of the messages
            Multipart multipart = new MimeMultipart("related");

            // first part  (the html)
            if (Module.EmailLogo.enabled(RequestContext.getInstance())) {
                String htmlText = "<img src=\"cid:logo\">";
                body = new StringBuilder(htmlText).append("<br><br>").append(body).toString();
            }

            MimeBodyPart mbp = new MimeBodyPart();

            //  add body part to multipart

            // if body content type is set
            if (bodyType != null) {
                mbp.setContent(body, bodyType);
            } else {
                if (Module.EmailLogo.enabled(RequestContext.getInstance())) {
                    body = body.replaceAll("\r\n", "<br />").replaceAll("\n", "<br />");
                    mbp.setContent(body, "text/html; charset=UTF-8");
                } else {
                    mbp.setContent(body, "text/plain");
                }
            }
            multipart.addBodyPart(mbp);

            message.setContent(multipart);

            /*
                * Add our attachments if present
                */
            if (Module.EmailLogo.enabled(RequestContext.getInstance())) {
                mbp = new MimeBodyPart();
                DataSource fds = new FileDataSource
                        (ResourcesUtils.getRealEmailLogo(ResourcesUtils.getRealEmailLogoDir().list()[0]));
                mbp.setDataHandler(new DataHandler(fds));
                mbp.setHeader("Content-ID", "<logo>");

                // add it
                multipart.addBodyPart(mbp);
            }

            for (Map.Entry<File, String> entry : files.entrySet()) {

                // create a new body part for the attachment
                mbp = new MimeBodyPart();
                DataSource ds = new FileDataSource(entry.getKey());

                mbp.setFileName(entry.getValue());
                mbp.setText(entry.getValue());
                mbp.setDataHandler(new DataHandler(ds));

                // set the header so the html doc can find it if needed
                mbp.setHeader("Content-ID", "<" + entry.getValue() + ">");

                multipart.addBodyPart(mbp);
            }

        } catch (MessagingException e) {
            // If caught, this is a bug
            throw new RuntimeException(e);

        } catch (UnsupportedEncodingException e) {
            // If caught, this is a bug
            throw new RuntimeException(e);
        }

        try {

            InternetAddress address = new InternetAddress(to);
            message.addRecipient(Message.RecipientType.TO, address);

            for (String string : bcc) {

                address = new InternetAddress(string);
                message.addRecipient(Message.RecipientType.BCC, address);

            }

            for (String string : cc) {

                address = new InternetAddress(string);
                message.addRecipient(Message.RecipientType.CC, address);

            }

        } catch (MessagingException e) {
            throw new EmailAddressException(e);
        }

        Transport transport;

        try {

            transport = session.getTransport("smtp");

        } catch (javax.mail.NoSuchProviderException e) {
            throw new SmtpServerException("No SMTP server set", e);
        }

        try {

            transport.connect();

        } catch (AuthenticationFailedException e) {
            throw new SmtpServerAuthenticationException("SMTP Authenticiation failed", e);

        } catch (MessagingException e) {
            throw new SmtpServerConnectionException("Error connecting to SMTP server '" + hostname + "'", e);
        }

        try {

            transport.sendMessage(message, message.getAllRecipients());

        } catch (SendFailedException e) {
            throw new EmailAddressException("Send failed " + e.getMessage(), e);

        } catch (MessagingException e) {
            throw new SmtpServerException("Error sending message: " + e.getMessage(), e);
        }

        try {
            transport.close();
        } catch (MessagingException e) {
            throw new SmtpServerException("Error closing SMTP server", e);
        }
    }

    public void setBody(CharSequence body) {
        this.body = body.toString();
    }

    public void setBody(Object obj) {
        this.body = obj.toString();
    }

    public void setBody(Object obj, String contentType) {
        this.body = obj.toString();
        this.bodyType = contentType;
    }

    public void setFrom(String s) {
        setFrom(s, s);
    }

    public void setFrom(String fromName, String fromAddress) {
        this.fromAddress = fromAddress;
        this.fromName = fromName;
    }

    public void setRecipients(Collection<String> c) {

        to = null;
        cc.clear();

        addRecipients(c);
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setTo(String to) {
        this.to = to;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append("To: " + to);
        sb.append("\nFrom: " + fromName + " <" + fromAddress + ">");

        for (String email : cc)
            sb.append("\nCc: " + email);

        for (String email : bcc)
            sb.append("\nBcc: " + email);

        sb.append("\nSubject: " + subject);
        sb.append("\n\n");
        sb.append(body);

        return sb.toString();
    }

    public static Email composeEmail(RequestContext context, String subject, String emailAddressTo, CharSequence body) {
        Email email = new Email();
        email.setSubject(subject);
        email.setFrom(Company.getInstance(context).getName(), Config.getInstance(context).getServerEmail());
        email.setTo(emailAddressTo);
        email.setBody(body.toString());
        return email;
    }

    public static boolean sendEmail(RequestContext context, Email email) {
        try {
            new EmailDecorator(context, email).send(Config.getInstance(context).getSmtpHostname());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private class SMTPAuthenticator extends javax.mail.Authenticator {
        private final String username;
        private final String password;

        private SMTPAuthenticator(String username, String password) {
            this.username = username;
            this.password = password;
        }

        public PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(username, password);
        }
    }
}
