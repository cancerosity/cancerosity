package org.sevensoft.commons.smail;


public class EmailAddressException extends Exception {

	public EmailAddressException() {
		super();
	}

	public EmailAddressException(String arg0) {
		super(arg0);
	}

	public EmailAddressException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public EmailAddressException(Throwable arg0) {
		super(arg0);
	}

}
