package org.sevensoft.commons.smail;

import org.sevensoft.ecreator.model.system.config.Config;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * User: Tanya
 * Date: 06.06.2012
 */
public class EmailDecorator extends Email {

    private Email email;
    private transient Config config;

    public EmailDecorator(RequestContext context, Email email) {
        this.email = email;
        this.config = Config.getInstance(context);
    }

    @Override
    public void send(String hostname) throws EmailAddressException, SmtpServerException {
        email.send(hostname, config.getUsername(), config.getPassword());

    }

}
