package org.sevensoft.commons.smail;


/**
 * @author sks 14-Apr-2005 00:33:56
 *
 */
public class SmtpServerException extends Exception {

	public SmtpServerException() {
		super();
	}

	public SmtpServerException(String arg0) {
		super(arg0);
	}

	public SmtpServerException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public SmtpServerException(Throwable arg0) {
		super(arg0);
	}

}
