package org.sevensoft.commons.smail;

/**
 * @author sks 05-Dec-2005 00:30:07
 *
 */
public class SmtpServerConnectionException extends SmtpServerException {

	public SmtpServerConnectionException() {
		super();
	}

	public SmtpServerConnectionException(String arg0) {
		super(arg0);
	}

	public SmtpServerConnectionException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public SmtpServerConnectionException(Throwable arg0) {
		super(arg0);
	}

}
