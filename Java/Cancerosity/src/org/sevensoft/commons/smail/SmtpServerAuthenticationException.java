package org.sevensoft.commons.smail;

/**
 * @author sks 14-Apr-2005 00:34:29
 */
public class SmtpServerAuthenticationException extends SmtpServerException {

	public SmtpServerAuthenticationException() {
		super();
	}

	public SmtpServerAuthenticationException(String arg0) {
		super(arg0);
	}

	public SmtpServerAuthenticationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public SmtpServerAuthenticationException(Throwable arg0) {
		super(arg0);
	}

}
