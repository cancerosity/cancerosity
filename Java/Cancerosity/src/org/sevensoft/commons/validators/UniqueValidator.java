package org.sevensoft.commons.validators;

import org.sevensoft.jeezy.i18n.Messages;

import javax.mail.Message;
import java.util.List;

/**
 * 
 * Checks if the value is included in the list
 * 
 * @author sks 14 Oct 2006 09:19:15
 *
 * @param <E>
 */
public class UniqueValidator<E> extends Validator {

	private final List<E>	list;
	private final E		value;

	public UniqueValidator(E value, List<E> list) {
		this.value = value;
		this.list = list;
	}

	public String validate(String value) {

		if (list.contains(value)) {
			return Messages.get("commons.validators.unique");
		} else {
			return null;
		}
	}

}
