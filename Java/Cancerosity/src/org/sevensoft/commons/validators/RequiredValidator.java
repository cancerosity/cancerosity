package org.sevensoft.commons.validators;

import org.sevensoft.jeezy.i18n.Messages;

/**
 * The value must not be null or the empty string
 *
 * @author sks 04-Nov-2004 16:08:51
 */
public class RequiredValidator extends Validator {

    public String validate(String value) {
        return (value == null || value.length() == 0) ? Messages.get("commons.validators.required") : null;
    }

}
