package org.sevensoft.commons.validators.net;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.sevensoft.commons.validators.Validator;
import org.sevensoft.jeezy.i18n.Messages;

/**
 * @author Sam 30-Nov-2002 06:48:32
 */
public class EmailValidator extends Validator {

	private final boolean	allowNull;

	Pattern			pattern	= Pattern
									.compile("^([a-zA-Z0-9_\\-])+(\\.([a-zA-Z0-9_\\-])+)*@((\\[(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5]))\\]))|((([a-zA-Z0-9])+(([\\-])+([a-zA-Z0-9])+)*\\.)+([a-zA-Z])+(([\\-])+([a-zA-Z0-9])+)*))$");

	/**
	 * 
	 */
	public EmailValidator() {
		this(true);
	}

	public EmailValidator(boolean allowNull) {
		this.allowNull = allowNull;
	}

	/*
	 * Overridden Method
	 */
	public String validate(String value) {

		if (value == null || value.equals("")) {
			if (allowNull) {
				return null;
			} else {
                return Messages.get("commons.validators.net.email");
			}
		}

		Matcher m = pattern.matcher(value);
		if (!m.matches())
			return Messages.get("commons.validators.net.email");

		return null;
	}
}
