package org.sevensoft.commons.validators.net;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.sevensoft.commons.validators.Validator;
import org.sevensoft.jeezy.i18n.Messages;

/**
 * @author Sam 30-Nov-2002 06:51:59
 */
public class IpAddressValidator extends Validator {

	Pattern	p	= Pattern.compile("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}");

	public String validate(String value) {

        if (value == null)
            return Messages.get("commons.validators.net.ip");

        Matcher m = p.matcher(value);
        if (!m.matches()) {
            return Messages.get("commons.validators.net.ip");
        }

		return null;
	}
}