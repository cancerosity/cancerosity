package org.sevensoft.commons.validators.net;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.sevensoft.commons.validators.Validator;
import org.sevensoft.jeezy.i18n.Messages;

/**
 * Validates url is http url. If requirescheme is set then requires http:// or https:// otherwise just checks for valid domain really.
 * 
 * @author Sam 30-Nov-2002 06:51:59
 */
public class HttpUrlValidator extends Validator {

	Pattern			withScheme		= Pattern
										.compile("^(http://|https://)[a-zA-Z0-9\\-\\._]+(\\.[a-zA-Z0-9\\-\\._]+){2,}(\\/?)([a-zA-Z0-9\\-\\.\\?\\,\\'\\/\\\\+&%\\$#_]*)?$");
	Pattern			withoutScheme	= Pattern
										.compile("^[a-zA-Z0-9\\-\\._]+(\\.[a-zA-Z0-9\\-\\._]+){2,}(\\/?)([a-zA-Z0-9\\-\\.\\?\\,\\'\\/\\\\+&%\\$#_]*)?$");

	private final boolean	requireScheme;

	public HttpUrlValidator() {
		this(false);
	}

	public HttpUrlValidator(boolean requireScheme) {
		this.requireScheme = requireScheme;
	}

	public String validate(String value) {

		if (value == null)
			return null;

		value = value.toLowerCase().trim();

		Matcher m;
		if (requireScheme)
			m = withScheme.matcher(value);
		else
			m = withoutScheme.matcher(value);

		if (!m.matches())
			return Messages.get("commons.validators.net.http");

		return null;
	}
}