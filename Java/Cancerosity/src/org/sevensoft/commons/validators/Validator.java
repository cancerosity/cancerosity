package org.sevensoft.commons.validators;

/**
 * @author sks 04-Nov-2004 16:04:42
 */
public abstract class Validator {

	/**
	 * 
	 */
	public boolean isValid(String value) {
		return validate(value) == null;
	}

	public abstract String validate(String value);
}
