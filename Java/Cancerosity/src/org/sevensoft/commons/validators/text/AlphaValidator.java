package org.sevensoft.commons.validators.text;

import org.sevensoft.commons.validators.Validator;
import org.sevensoft.jeezy.i18n.Messages;

/**
 * @author Sam 30-Nov-2002 06:51:59
 */
public class AlphaValidator extends Validator {

	private boolean allowNull;

	/*
	 * @see org.sevensoft.commons.jwf.Validator2#validate(java.lang.String)
	 */
	public String validate(String value) {

		if (value == null)
            return Messages.get("commons.validators.text.alpha");

		char[] c = value.toCharArray();
		for (int n = 0; n < c.length; n++) {
			if (!Character.isLetter(c[n]))
                return Messages.get("commons.validators.text.alpha");
		}

		return null;
	}
}
