package org.sevensoft.commons.validators.text;

import org.sevensoft.commons.validators.Validator;
import org.sevensoft.jeezy.i18n.Messages;

import java.text.MessageFormat;

/**
 * @author sks 04-Nov-2004 16:08:51
 */
public class LengthValidator extends Validator {

	private int	min, max;

	/**
	 * @param min
	 */
	public LengthValidator(int min) {
		this.min = min;
		this.max = Integer.MAX_VALUE;
	}

	/**
	 * @param min
	 * @param max
	 */
	public LengthValidator(int min, int max) {
		this.min = min;
		this.max = max;
	}

	/*
	 * @see org.sevensoft.commons.jwf.Validator2#validate(java.lang.String)
	 */
	public String validate(String value) {

		if (value == null)
			return Messages.get("commons.validators.text.lenght");

		value = value.trim();

        if (min == max && value.length() != min)
            return MessageFormat.format(Messages.get("commons.validators.text.lenght.equals"), String.valueOf(min));

        if (value.length() < min)
            return MessageFormat.format(Messages.get("commons.validators.text.lenght.min"), String.valueOf(min));

        if (value.length() > max)
            return MessageFormat.format(Messages.get("commons.validators.text.lenght.max"), String.valueOf(max + 1));

		return null;
	}
}
