package org.sevensoft.commons.validators.text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.sevensoft.commons.validators.Validator;
import org.sevensoft.jeezy.i18n.Messages;

/**
 * @author Sam 30-Nov-2002 06:51:59
 */
public class PostcodeValidator extends Validator {

	Pattern	p	= Pattern.compile("^\\s*[a-zA-Z]{1,2}[0-9][0-9A-Za-z]{0,1} {0,1}[0-9][A-Za-z]{2}\\s*$");

	/*
	 * @see org.sevensoft.commons.jwf.Validator2#validate(java.lang.String)
	 */
	public String validate(String value) {

		if (value == null) {
			return "";
		}

		Matcher m = p.matcher(value);
		if (!m.matches()) {
            return Messages.get("commons.validators.text.postcode");
		}

		return null;
	}
}
