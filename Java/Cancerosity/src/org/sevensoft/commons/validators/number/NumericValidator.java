package org.sevensoft.commons.validators.number;

import org.sevensoft.commons.validators.Validator;
import org.sevensoft.jeezy.i18n.Messages;


/**
 * @author Sam 30-Nov-2002 06:51:59
 */
public class NumericValidator extends Validator {

	private boolean allowNull;

	/*
	 * @see org.sevensoft.commons.jwf.Validator2#validate(java.lang.String)
	 */
	public String validate(String value) {

		if (value == null)
			return null;

		char[] c = value.toCharArray();
		for (int n = 0; n < c.length; n++) {
			if (!Character.isDigit(c[n]))
				return Messages.get("commons.validators.number.numeric");
		}
		
		return null;
	}
}
