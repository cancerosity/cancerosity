package org.sevensoft.commons.validators.number;

import org.sevensoft.commons.validators.Validator;
import org.sevensoft.jeezy.i18n.Messages;

/**
 * User: MeleshkoDN
 * Date: 07.09.2007
 * Time: 11:02:21
 */
public class IntValidator extends Validator {
    public String validate(String value) {
        if (value == null || "".equals(value)) {
            return null;
        }
        try {
            Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return Messages.get("commons.validators.number.int");
        }
        return null;
    }
}
