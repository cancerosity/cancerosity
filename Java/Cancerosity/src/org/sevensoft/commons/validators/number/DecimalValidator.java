package org.sevensoft.commons.validators.number;

import org.sevensoft.commons.validators.Validator;
import org.sevensoft.jeezy.i18n.Messages;

/**
 * @author sks 04-Nov-2004 16:08:51
 */
public class DecimalValidator extends Validator {

	/*
	 * @see org.sevensoft.commons.jwf.Validator2#validate(java.lang.String)
	 */
	public String validate(String value) {

		if (value == null)
			return null;

		if (value.equals(""))
			return null;

		value = value.replace(",", "").replaceAll("\\s", "");

		try {
			Double.parseDouble(value);
			return null;
		} catch (NumberFormatException e) {
			return Messages.get("commons.validators.number.decimal");
		}

	}

}
