package org.sevensoft.commons.validators;

import org.sevensoft.jeezy.i18n.Messages;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author sks 11 Apr 2007 19:58:50
 * 
 * Validates that the string is in domain name format
 *
 */
public class DomainValidator extends Validator {

	static private Pattern	pattern;

	{
		pattern = Pattern.compile("(\\w|-)+\\.\\w+(\\.\\w+)?");
	}

	@Override
	public String validate(String value) {

		if (value == null) {
			return null;
		}

		Matcher matcher = pattern.matcher(value);
		if (matcher.matches()) {
			return null;
		}

		return Messages.get("commons.validators.domain");
	}

}
