package org.sevensoft.commons.validators;

import org.sevensoft.jeezy.i18n.Messages;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.MessageFormat;

/**
 * Checks that the value entered resembles a date in the formats ddmmyyyy or ddmmyyyy with any non digit separators between the 3 sequences,
 * ie dd/mm/yyyy
 */
public class DateValidator extends Validator {

    private String pattern;

    public DateValidator() {
    }

    public DateValidator(String pattern) {
        this.pattern = pattern;
    }

    public String validate(String value) {

        String pattern = this.pattern == null ? "dd/MM/yyyy" : this.pattern;
        DateFormat dateFormat = new SimpleDateFormat(pattern);

        if (value == null)
            return null;

        try {
            dateFormat.parse(value);
        } catch (ParseException e) {
            return MessageFormat.format(Messages.get("commons.validators.date"), pattern);
        }
        return null;
    }

}
