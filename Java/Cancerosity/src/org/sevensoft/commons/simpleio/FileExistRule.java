package org.sevensoft.commons.simpleio;


/**
 * @author sks 30 May 2007 16:25:16
 *
 */
public enum FileExistRule {
	Rename, Skip, Overwrite

}
