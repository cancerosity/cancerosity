package org.sevensoft.commons.simpleio;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

/**
 * @author sks 14 Oct 2006 10:11:32
 *
 */
public class SimpleUrl {

	public static String downloadString(String string) throws IOException {
		return downloadString(new URL(string));
	}

	public static String downloadString(URL url) throws IOException {
		return new String(downloadToByteArray(url));
	}

	public static byte[] downloadToByteArray(URL url) throws IOException {

		BufferedInputStream in = null;
		ByteArrayOutputStream out = null;

		try {

			in = new BufferedInputStream(url.openStream());
			out = new ByteArrayOutputStream();

			byte[] b = new byte[1024];
			int i = -1;
			while ((i = in.read(b, 0, 1024)) != -1) {
				out.write(b, 0, i);
			}

			return out.toByteArray();

		} catch (IOException e) {
			throw e;

		} finally {

			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 *  Downloads the data at this url and stores it in a temporary file.
	 *  Returns the temporary file where this data was stored.
	 * 
	 */
	public static File downloadToFile(URL url) throws IOException {

		File file = File.createTempFile("download", null);

		downloadToFile(url, file);

		return file;
	}

	public static void downloadToFile(URL url, File file) throws IOException {

		BufferedInputStream in = null;
		BufferedOutputStream out = null;

		try {

			// create directories for this file
			File parent = file.getParentFile();
			if (parent != null) {
				parent.mkdirs();
			}

			in = new BufferedInputStream(url.openStream());
			out = new BufferedOutputStream(new FileOutputStream(file));

			byte[] b = new byte[1024];
			int i = -1;
			while ((i = in.read(b, 0, 1024)) != -1) {
				out.write(b, 0, i);
			}

		} catch (IOException e) {
			throw e;

		} finally {

			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
