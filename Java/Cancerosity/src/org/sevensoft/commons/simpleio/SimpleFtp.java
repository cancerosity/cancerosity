package org.sevensoft.commons.simpleio;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FTPTransferType;

/**
 * @author sks 14 Oct 2006 10:11:08
 *
 */
public class SimpleFtp {

	private static Logger	logger	= Logger.getLogger("jeezy");

	public static void putFile(String ftpHostname, String ftpUsername, String ftpPassword, String ftpPath, String ftpFilename, FTPTransferType type, File file)
			throws IOException, FTPException {
		putStream(ftpHostname, ftpUsername, ftpPassword, ftpPath, ftpFilename, type, new FileInputStream(file));
	}

	/**
	 * Writes the contents of this input stream to the ftp by reading the data from the input stream
	 * @throws FTPException 
	 */
	public static void putStream(String hostname, String username, String password, String path, String filename, FTPTransferType type, InputStream stream)
			throws IOException, FTPException {

		try {

			FTPClient ftp = new FTPClient();

			ftp.setRemoteHost(hostname);
			ftp.connect();

			if (username != null) {
				ftp.user(username);
			}

			if (password != null) {
				ftp.password(password);
			}

			if (path != null) {
				ftp.chdir(path);
			}

			ftp.setType(type);

			ftp.put(stream, filename);

			ftp.quit();

		} catch (IOException e) {
			e.printStackTrace();
			throw e;

		} catch (FTPException e) {
			e.printStackTrace();
			throw e;

		} finally {

			if (stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void putString(String hostname, String username, String password, String path, String filename, FTPTransferType type, String string)
			throws IOException, FTPException {

		putStream(hostname, username, password, path, filename, type, new ByteArrayInputStream(string.getBytes()));
	}

	public static byte[] retrieveBytesFromFtp(String ftpHostname, String ftpUsername, String ftpPassword, String ftpPath, String ftpFilename)
			throws IOException {
        ByteArrayOutputStream out = null;
		try {

			FTPClient ftp = new FTPClient();

			ftp.setRemoteHost(ftpHostname);
			ftp.connect();

			if (ftpUsername != null) {
				ftp.user(ftpUsername);
			}

			if (ftpPassword != null) {
				ftp.password(ftpPassword);
			}

			if (ftpPath != null) {
				ftp.chdir(ftpPath);
			}

			ftp.setType(FTPTransferType.ASCII);

			out = new ByteArrayOutputStream();

			ftp.get(out, ftpFilename);
			ftp.quit();

			return out.toByteArray();

		} catch (FTPException e) {
			throw new IOException(e.getMessage());

		} catch (IOException e) {
			throw e;

        } finally {

            try {

                if (out != null) {
                    out.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
	}

	/**
	 * Reads the contents from an FTP server and writes them to a temp file. Returns that file
	 * @param port 
	 */
	public static File retrieveFile(String ftpHostname, int port, String ftpUsername, String ftpPassword, String ftpPath, String ftpFilename, FTPTransferType type)
			throws IOException {

		File file = null;
		BufferedOutputStream out = null;

		logger.fine("[SimpleFtp] retrieve file from ftp hostname=" + ftpHostname);

		FTPClient ftp = new FTPClient();

		try {

			ftp.setRemoteHost(ftpHostname);
			ftp.connect();

			if (ftpUsername != null) {
				logger.fine("[SimpleFtp] setting ftpUsername=" + ftpUsername);
				ftp.user(ftpUsername);
			}

			if (ftpPassword != null) {
				logger.fine("[SimpleFtp] setting ftpPassword=" + ftpPassword);
				ftp.password(ftpPassword);
			}

			if (ftpPath != null) {
				logger.fine("[SimpleFtp] setting ftpPath=" + ftpPath);
				ftp.chdir(ftpPath);
			}

			if (type == null) {
				if (ftpFilename.endsWith(".txt") || ftpFilename.endsWith(".csv") || ftpFilename.endsWith(".html")) {
					type = FTPTransferType.ASCII;
				} else {
					type = FTPTransferType.BINARY;
				}
			}

			ftp.setType(type);

			file = File.createTempFile("ftpdownload", null);
			out = new BufferedOutputStream(new FileOutputStream(file));

			logger.fine("[SimpleFtp] getting filename=" + ftpFilename);

			ftp.get(out, ftpFilename);

			logger.fine("[SimpleFtp] file downloaded to " + file);

			return file;

		} catch (FTPException e) {

			if (file != null) {
				file.delete();
			}

			throw new IOException(e.toString());

		} catch (IOException e) {

			if (file != null) {
				file.delete();
			}

			throw e;

		} finally {

			try {

				if (ftp.connected()) {
					ftp.quit();
				}

			} catch (IOException e) {
				e.printStackTrace();

			} catch (FTPException e) {
				e.printStackTrace();
			}

			try {

				if (out != null) {
					out.close();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	/**
	 * Reads the contents of the file on the FTP server and returns a string with the contents of that file
	 */
	public static String retrieveString(String ftpHostname, String ftpUsername, String ftpPassword, String ftpPath, String ftpFilename) throws IOException {
		return retrieveBytesFromFtp(ftpHostname, ftpUsername, ftpPassword, ftpPath, ftpFilename).toString();
	}

}
