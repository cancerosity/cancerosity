package org.sevensoft.commons.simpleio;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Collection;
import java.util.logging.Logger;

/**
 * 
 *  VERSION : 0.9
 *  
 *  148/10/06 - separated simple io from commons to its own project
 * 
 * @author sks 14 Oct 2006 10:11:01
 *
 */
public class SimpleFile {

	private static Logger	logger	= Logger.getLogger("commons");

	/**
	 * Adds the new suffix to the filename but before the file extension
	 */
	public static String appendFilename(String filename, String suffix) {

		if (filename.indexOf('.') < 1) {
			return null;
		}

		String[] parts = filename.split("\\.");
		if (parts.length != 2) {
			return null;
		}

		return parts[0] + suffix + "." + parts[1];
	}

	public static void copy(File src, File dest) throws FileNotFoundException, IOException {

		if (src.exists() && src.isFile()) {

			FileChannel in = null, out = null;
			try {

				in = new FileInputStream(src).getChannel();
				out = new FileOutputStream(dest).getChannel();

				long size = in.size();
				MappedByteBuffer buf = in.map(FileChannel.MapMode.READ_ONLY, 0, size);

				out.write(buf);

			} finally {

				if (in != null) {
					try {
						in.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				if (out != null) {
					try {
						out.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	public static void deleteFiles(Collection<File> files) {
		for (File file : files) {
			file.delete();
		}
	}

	/**
	 * Move all files in subdirectories of the dir param to the actual dir param. Flatten the file strucutre basically 
	 */
	public static void flatten(File base) throws IOException {

		logger.fine("[SimpleFile] flatten base= " + base);
		flatten(base, base);
	}

	private static void flatten(File base, File dir) throws IOException {

		logger.fine("[SimpleFile] flatten base= " + base + ", dir=" + dir);

		for (File file : dir.listFiles()) {

			if (file.isDirectory()) {

				flatten(base, file);

			} else {

				File dest = new File(base.getAbsolutePath() + "/" + file.getName());

				logger.fine("[SimpleFile] rename src=" + file + " dest=" + dest);

				file.renameTo(dest);
			}
		}
	}

	/**
	 * Returns the file extension used by this file excluding the dot.
	 */
	public static String getExtension(File file) {
		return file.getName().substring(file.getName().lastIndexOf('.') + 1);
	}

	/**
	 * Returns the extension excluding the dot.
	 */
	public static String getExtension(String filename) {
		return getExtension(new File(filename));
	}

	/**
	 * Returns the filename as a unique filename. It will change the filename if there are other files in this directory with the same name. 
	 * Does not include the directory on return just the ammended (possibly) filename
	 * 
	 */
	public static String getUniqueFilename(String fn, String dir) {

		if (!fn.contains(".")) {
			return null;
		}

		// get split filename on suffix.
		int dot = fn.lastIndexOf('.');
		String name = fn.substring(0, dot);
		String suffix = fn.substring(dot + 1);

		// check filename availability with 1000 timeout just in case we loop forever for some reason
		for (int n = 0; n < 1000; n++) {

			if (n > 0) {
				fn = name + "_" + n + "." + suffix;
			}

			File file = new File(dir + "/" + fn);
			if (!file.exists()) {
				return fn;
			}
		}

		return null;
	}

	public static void move(File src, File dest) throws IOException {
		copy(src, dest);
		src.delete();
	}

	public static byte[] readBytes(File file) throws FileNotFoundException, IOException {
		return readBytes(new FileInputStream(file));
	}

	public static byte[] readBytes(InputStream stream) throws IOException {

		BufferedInputStream in = null;
        ByteArrayOutputStream out = null;
		try {

			in = new BufferedInputStream(stream);
			out = new ByteArrayOutputStream();

			byte[] buf = new byte[1024];
			int i = -1;
			while ((i = in.read(buf, 0, 1024)) != -1) {
				out.write(buf, 0, i);
			}

			return out.toByteArray();

		} catch (IOException e) {
			throw e;

		} finally {

			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}

            try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	public static byte[] readBytes(String string) throws IOException {
		return readBytes(new File(string));
	}

	/**
	 * Returns the contents of this file as a string
	 */
	public static String readString(File file) throws IOException {
		return readString(new FileInputStream(file));
	}

	/**
	 * Returns the contents of this input stream as a file
	 */
	public static String readString(InputStream inputStream) throws IOException {

		BufferedReader in = null;

		try {

			in = new BufferedReader(new InputStreamReader(inputStream));

			StringBuilder sb = new StringBuilder();

			String string;
			while ((string = in.readLine()) != null) {
				sb.append(string);
				sb.append("\n");
			}

			return sb.toString();

		} catch (IOException e) {
			throw e;

		} finally {

			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}

	/**
	 * Read the contents of the path denoted by the string param as a String and return it.
	 */
	public static String readString(String string) throws IOException {
		return readString(new File(string));
	}

	/**
	 * 
	 */
	public static String removeFileExtension(String filename) {

		if (filename == null) {
			return null;
		}

		int index = filename.lastIndexOf('.');
		if (index < 1) {
			return filename;
		}

		return filename.substring(0, index);

	}

	/**
	 * 
	 */
	public static String stripPaths(String filename) {
		if (filename.contains("/")) {
			filename = filename.substring(filename.lastIndexOf('/'));
		}
		if (filename.contains("\\")) {
			filename = filename.substring(filename.lastIndexOf('\\'));
		}
		return filename;
	}

	/**
	 * Writes the contents of the byte array param to the file
	 */
	public static void writeBytes(byte[] bytes, File file) throws IOException {

		BufferedOutputStream out = null;

		try {

			out = new BufferedOutputStream(new FileOutputStream(file));
			out.write(bytes);

		} catch (IOException e) {
			throw e;

		} finally {

			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Write this stream out to the file
	 */
	public static void writeStream(InputStream source, File file) throws IOException {

		BufferedInputStream in = null;
		BufferedOutputStream out = null;

		try {

			in = new BufferedInputStream(source);
			out = new BufferedOutputStream(new FileOutputStream(file));

			byte[] b = new byte[1024];
			int i;
			while ((i = in.read(b)) > 0) {
				out.write(b, 0, i);
			}

		} finally {

			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void writeString(CharSequence data, File file) throws IOException {

		BufferedWriter out = null;

		try {

			out = new BufferedWriter(new FileWriter(file));
			out.write(data.toString());

		} catch (IOException e) {
			throw e;

		} finally {

			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * @throws IOException 
	 * 
	 */
	public static void writeString(CharSequence data, String path) throws IOException {
		writeString(data, new File(path));
	}

}
