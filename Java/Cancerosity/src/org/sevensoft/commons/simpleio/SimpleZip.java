package org.sevensoft.commons.simpleio;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * @author sks 14 Oct 2006 10:10:42
 *
 */
public class SimpleZip {

	/**
	 * Writes this file out to an already open ZIP stream using the string param as the name of the zip entry
	 */
	public static void archiveFile(File file, String string, ZipOutputStream out) throws IOException {

		FileInputStream in = null;

		try {

			out.putNextEntry(new ZipEntry(string));
			in = new FileInputStream(file);

			// Create a buffer for reading the files
			byte[] buf = new byte[1024];

			// Transfer bytes from the file to the ZIP file
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}

		} catch (FileNotFoundException e) {
			throw e;

		} catch (IOException e) {
			throw e;

		} finally {

			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			if (out != null)
				try {
					out.closeEntry();
				} catch (IOException e) {
					e.printStackTrace();
				}

		}
	}

	/**
	 *  Writes this string data out to the zip file as a new entry in the open zip
	 * 
	 */
	public static void archiveString(String data, String name, ZipOutputStream out) throws IOException {

		try {

			out.putNextEntry(new ZipEntry(name));
			ByteArrayInputStream in = new ByteArrayInputStream(data.getBytes());

			// Create a buffer for reading the files
			byte[] buf = new byte[1024];

			// Transfer bytes from the file to the ZIP file
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}

			out.closeEntry();

		} catch (IOException e) {
			throw e;

		} finally {

			if (out != null)
				try {
					out.closeEntry();
				} catch (IOException e) {
					e.printStackTrace();
				}

		}
	}

	/**
	 *  Extracts the zip entry from this zip and writes it out as a file at the path using the name of the entry as the filename
	 *  Returns a file for the new file created
	 */
	public static File extractEntryToDir(ZipFile zipFile, ZipEntry entry, String path) throws IOException {

		BufferedInputStream in = null;
		BufferedOutputStream out = null;
		File file = new File(path + "/" + entry.getName());

		try {

			in = new BufferedInputStream(zipFile.getInputStream(entry));
			out = new BufferedOutputStream(new FileOutputStream(file));

			// Create a buffer for reading the files
			byte[] buf = new byte[1024];

			// Transfer bytes from the zip to the output stream
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}

			return file;

		} catch (IOException e) {
			throw e;

		} finally {

			if (in != null)
				in.close();

			if (out != null)
				out.close();

		}
	}

	/**
	 * Extract the contents of this zip entry to a string and return it.
	 */
	public static String extractEntryToString(ZipFile zipFile, ZipEntry entry) throws IOException {

		BufferedInputStream in = new BufferedInputStream(zipFile.getInputStream(entry));
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		// Create a buffer for reading the files
		byte[] buf = new byte[1024];

		// Transfer bytes from the zip to the output stream
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}

		in.close();
		out.close();

		return out.toString();
	}

	/**
	 * Will extract the contents of the zip to the specified dir.
	 * Returns list of filenames created on system
	 * @param stripPaths 
	 * @param overwrite 
	 * @throws IOException 
	 */
	public static List<File> extractToDir(File file, File dir, boolean stripPaths, FilenameFilter filter, FileExistRule fileExistRule) throws IOException {

		ZipFile zip = null;

		try {

			zip = new ZipFile(file);
			return extractToDir(zip, dir, false, null, fileExistRule);

		} finally {

			if (zip != null) {
				try {
					zip.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static List<File> extractToDir(File file, String dir, boolean stripPaths, FilenameFilter filter, FileExistRule fileExistRule) throws IOException {
		return extractToDir(file, new File(dir), stripPaths, filter, fileExistRule);
	}

	/**
	 * @param stripPaths - will remove directory info and just extract the file
	 * @param filter - only includes files matching the filter
	 * @param overwrite - overwrites existing files
	 */
	public static List<File> extractToDir(ZipFile zipFile, File dir, boolean stripPaths, FilenameFilter filter, FileExistRule fileExistRule)
			throws IOException {

		List<File> files = new ArrayList();
		Enumeration e = zipFile.entries();
		while (e.hasMoreElements()) {

			ZipEntry entry = (ZipEntry) e.nextElement();
			String filename = entry.getName();

			// check filename in filter if not null
			if (filter != null) {
				if (!filter.accept(null, filename)) {
					continue;
				}
			}

			if (stripPaths) {
				filename = SimpleFile.stripPaths(filename);
			}

			// create file to write to
			File file;
			if (dir == null) {

				file = File.createTempFile("simplezip_temp", null);
				file.deleteOnExit();

			} else {
                if (!dir.exists()) {
                    dir.mkdir();
                }
				// strip whitespace
				filename = filename.replaceAll("\\s", "_");

				if (fileExistRule == FileExistRule.Rename) {
					filename = SimpleFile.getUniqueFilename(filename, dir.getAbsolutePath());
				}

				file = new File(dir.getAbsolutePath() + "/" + filename);

				if (fileExistRule == FileExistRule.Skip) {
					if (file.exists()) {
						continue;
					}
				}
			}

			BufferedInputStream in = null;
			BufferedOutputStream out = null;

			try {

				in = new BufferedInputStream(zipFile.getInputStream(entry));
				out = new BufferedOutputStream(new FileOutputStream(file));

				byte[] b = new byte[1024];
				int i = -1;
				while ((i = in.read(b, 0, 1024)) != -1) {
					out.write(b, 0, i);
				}

				files.add(file);

			} catch (IOException e1) {
				throw e1;

			} finally {

				try {
					if (in != null) {
						in.close();
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				try {
					if (out != null) {
						out.close();
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}

		return files;
	}

	public static List<File> extractToTempFiles(File file) throws IOException {
		return extractToDir(file, (File) null, true, null, FileExistRule.Rename);
	}

	/**
	 * Returns an iterator for this zip file
	 */
	public static Iterable<ZipEntry> iterable(final ZipFile zipFile) throws IOException {

		return new Iterable<ZipEntry>() {

			public Iterator<ZipEntry> iterator() {

				return new Iterator<ZipEntry>() {

					final Enumeration	e	= zipFile.entries();

					public boolean hasNext() {
						return e.hasMoreElements();
					}

					public ZipEntry next() {
						return (ZipEntry) e.nextElement();
					}

					public void remove() {
						throw new UnsupportedOperationException();
					}

				};
			}

		};

	}

}
