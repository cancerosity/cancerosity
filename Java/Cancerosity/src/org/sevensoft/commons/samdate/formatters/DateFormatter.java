package org.sevensoft.commons.samdate.formatters;

/**
 * @author sks 14 Oct 2006 11:59:16
 *
 */
public class DateFormatter {

	public static String daysToString(int days) {

		switch (days) {

		default:
			return days + " days";

		case 7:
			return "1 week";

		case 14:
			return "2 weeks";

		case 28:
			return "4 weeks";

		case 30:
			return "1 month";

		case 60:
			return "2 months";

		case 90:
			return "3 months";

		case 120:
			return "4 months";

		case 150:
			return "5 months";

		case 180:
			return "6 months";

		case 365:
			return "1 year";

		case 540:
			return "18 months";

		case 730:
			return "2 years";

		case 1095:
			return "3 years";

		case 1460:
			return "4 years";

		case 1825:
			return "5 years";

		case 2190:
			return "6 years";

		case 3650:
			return "10 years";
		}
	}
}
