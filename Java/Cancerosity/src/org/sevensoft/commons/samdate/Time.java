package org.sevensoft.commons.samdate;

import java.text.SimpleDateFormat;

/**
 * @author sks 28-Dec-2005 14:32:25
 * 
 */
public final class Time implements Comparable<Time> {

	private final long	timestamp;

	public Time(DateTime date) {
		this.timestamp = date.getTime().getTimestamp();
	}

	public Time(int hour, int minute, int second) {

		int time = hour * 60 * 60;
		time += minute * 60;
		time += second;
		timestamp = time * 1000;
	}

	public Time(long timestamp) {
		this.timestamp = timestamp;
	}

	public Time(String string) {

		string = string.trim();

		int minute = 0, hour = 0, second = 0;

		String[] times = string.split(":");
		if (times.length > 0)
			if (times[0].length() > 0)
				hour = Integer.parseInt(times[0]);

		if (times.length > 1)
			if (times[1].length() > 0)
				minute = Integer.parseInt(times[1]);

		if (times.length > 2)
			if (times[2].length() > 0)
				second = Integer.parseInt(times[2]);

		if (second > 59) {
			minute += second / 60;
			second = second % 60;
		}

		if (minute > 59) {
			hour += minute / 60;
			minute = minute % 60;
		}

		int time = hour * 60 * 60;
		time += minute * 60;
		time += second;
		timestamp = time * 1000;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return new Time(timestamp);
	}

	public int compareTo(Time o) {

		if (timestamp < o.timestamp)
			return -1;

		if (timestamp > o.timestamp)
			return 1;

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Time)
			return ((Time) obj).timestamp == timestamp;
		else
			return false;
	}

	//	/**
	//	 * @return
	//	 */
	//	private String getAmPmMarker() {
	//		if (getHour() < 12)
	//			return "am";
	//		else
	//			return "pm";
	//	}
	//
	//	private String getFormattedMinute() {
	//
	//		int minute = getMinute();
	//
	//		if (minute < 10)
	//			return "0" + minute;
	//		else
	//			return String.valueOf(minute);
	//	}
	//
	//	private String getFormattedSecond() {
	//
	//		int second = getSecond();
	//
	//		if (second < 10)
	//			return "0" + second;
	//		else
	//			return String.valueOf(second);
	//	}

	public int getHour() {
		return (int) (timestamp / 3600000);
	}

	public String getLabel() {
		return toString("K:mma");
	}

	public int getMinute() {

		long i = timestamp % 3600000;
		return (int) i / 60000;
	}

	public int getSecond() {

		long i = timestamp % 3600000;
		i = i % 60;
		return (int) i;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public String getValue() {
		return String.valueOf(getTimestamp());
	}

	@Override
	public int hashCode() {
		return (int) timestamp;
	}

	/**
	 * @param i
	 * @return
	 */
	public Time removeMinutes(int minutes) {
		return new Time(timestamp - (minutes * 60 * 1000));
	}

	public Time removeSeconds(int seconds) {
		return new Time(timestamp - (seconds * 1000));
	}

	@Override
	public String toString() {
		return toString("K:mm:ss");
	}

	/**
	 * 
	 * a Am/pm marker Text PM
	 * 
	 * H Hour in day (0-23) Number 0
	 * 
	 * k Hour in day (1-24) Number 24
	 * 
	 * K Hour in am/pm (0-11) Number 0
	 * 
	 * h Hour in am/pm (1-12) Number 12
	 * 
	 * m Minute in hour Number 30
	 * 
	 * s Second in minute Number 55
	 * 
	 * S Millisecond Number 978
	 * 
	 * @param string
	 * @return
	 */
	public String toString(String string) {
		// string = string.replace("m", getFormattedMinute());
		// string = string.replace("s", getFormattedSecond());
		// string = string.replace("h", String.valueOf(getHour()));
		// string = string.replace("a", getAmPmMarker());
		// return string;
		return new SimpleDateFormat(string).format(new java.util.Date(timestamp)).toLowerCase();
	}
}
