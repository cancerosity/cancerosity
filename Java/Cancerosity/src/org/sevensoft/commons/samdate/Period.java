package org.sevensoft.commons.samdate;

/**
 * @author sks 15 Sep 2006 18:31:53
 *
 */
public class Period {

	private int	minutes;
	private int	hours;
	private int	seconds;
	private int	days;
	private int	weeks;
	private int	months;

	/**
	 * Parse this string to find a period
	 */
	public Period(String string) {

		string = string.trim().toLowerCase();

        // get integer part by stripping all non digits
        String intValue = string.replaceAll("\\D", "");
        int i = 0;
        if (intValue != null && !intValue.equals("")) {
            i = Integer.parseInt(intValue);
        }
        // remove all non letters
		string = string.replaceAll("[^a-z]", "");

		if (string.equals("week") || string.equals("weeks") || string.equals("w")) {
			weeks = i;
		}

		else if (string.equals("minute") || string.equals("minutes") || string.equals("mins") || string.equals("min")) {
			minutes = i;
		}

		else if (string.equals("month") || string.equals("months") || string.equals("m")) {
			months = i;
		}

		else if (string.equals("hour") || string.equals("hours") || string.equals("h")) {
			hours = i;
		}

		else if (string.equals("second") || string.equals("seconds") || string.equals("sec") || string.equals("secs") || string.equals("s")) {
			seconds = i;
		}

		else {
			days = i;
		}
	}

	public Date add(Date date) {

		if (seconds > 0) {
			return new Date(date.getTimestamp() + seconds * 1000);
		}

		if (minutes > 0) {
			return new Date(date.getTimestamp() + minutes * 1000 * 60);
		}

		if (hours > 0) {
			return new Date(date.getTimestamp() + hours * 1000 * 60 * 60);
		}

		if (days > 0) {
			return date.addDays(days);
		}

		if (weeks > 0) {
			return date.addWeeks(weeks);
		}

		if (months > 0) {
			return date.addMonths(months);
		}

		return null;
	}

	public int getDays() {
		return days;
	}

	public int getHours() {
		return hours;
	}

	/**
	 * Returns this period as a length of millis 
	 */
	public long getMillis() {
		return (seconds + (minutes * 60) + (hours * 60 * 60) + (days * 24 * 60 * 60) + (weeks * 7 * 24 * 60 * 60)) * 1000;
	}

	public int getMinutes() {
		return minutes;
	}

	public int getSeconds() {
		return seconds;
	}

	public int getWeeks() {
		return weeks;
	}

	public Date remove(Date date) {

		if (seconds > 0) {
			return new Date(date.getTimestamp() - seconds * 1000);
		}

		if (minutes > 0) {
			return new Date(date.getTimestamp() - minutes * 1000 * 60);
		}

		if (hours > 0) {
			return new Date(date.getTimestamp() - hours * 1000 * 60 * 60);
		}

		if (days > 0) {
			return date.addDays(-days);
		}

		if (weeks > 0) {
			return date.addWeeks(-weeks);
		}

		if (months > 0) {
			return date.addMonths(-months);
		}

		return null;
	}

	public long remove(long timestamp) {
		return timestamp - getMillis();
	}

	@Override
	public String toString() {

		if (seconds > 0) {
			return seconds + " seconds";
		}

		if (minutes > 0) {
			return minutes + "minute";
		}

		if (hours > 0) {
			return hours + " hour";
		}

		if (days > 0) {
			return days + " days";
		}

		if (weeks > 0) {
			return weeks + " weeks";
		}

		if (months > 0) {
			return months + " months";
		}

		return super.toString();
	}

    public boolean isCorrect() {
        if (seconds + minutes + hours + days + weeks + months == 0) {
            return false;
        }
        return true;
    }
}
