package org.sevensoft.commons.samdate;

/**
 * @author sks 1 Mar 2007 15:44:26
 *
 */
public enum QuickDate {

	Today() {

		@Override
		public Date[] getDates() {

			// just return toda twice

			Date date = new Date();
			return new Date[] { date, date };
		}

	},
	ThisWeekend() {

		@Override
		public String toString() {
			return "This weekend";
		}

		@Override
		public Date[] getDates() {

			Date date = new Date();
			if (date.getDayOfWeek() == Date.Saturday) {
				return new Date[] { date, date.nextDay() };
			}

			else if (date.getDayOfWeek() == Date.Sunday) {
				return new Date[] { date, date };
			}

			else {

				date = date.cycleToDay(Date.Saturday);
				return new Date[] { date, date.nextDay() };
			}
		}

	},
	NextWeekend() {

		@Override
		public String toString() {
			return "Next weekend";
		}

		@Override
		public Date[] getDates() {

			Date date = new Date();
			date = date.cycleToDay(Date.Saturday);

			return new Date[] { date, date.nextDay() };

		}
	},
	Next7Days() {

		@Override
		public String toString() {
			return "Next 7 days";
		}

		@Override
		public Date[] getDates() {

			// return todays date plus the next 6
			Date date = new Date();
			return new Date[] { date, date.addDays(6) };

		}

	};

	/**
	 * Returns the date range for searching. INCLUSIVE values, 
	 * ie, weekend includes the saturday and the sunday.
	 */
	public abstract Date[] getDates();

}
