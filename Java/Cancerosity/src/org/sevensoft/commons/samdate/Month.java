package org.sevensoft.commons.samdate;

/**
 * @author sks 19 Mar 2007 11:50:44
 *
 */
public enum Month {

	January, February, March, April, May, June, July, August, September, October, November, December;

}
