package org.sevensoft.commons.samdate;

/**
 * @author sks 02-May-2006 08:35:35
 *
 */
public class DateRange {

	private final Date	start;
	private final Date	end;

	public DateRange(Date start, Date end) {

		this.start = start;
		if (end == null)
			this.end = start;
		else
			this.end = end;
	}

	public boolean contains(Date date) {
		return start.getTimestamp() <= date.getTimestamp() && date.getTimestamp() <= end.getTimestamp();
	}
}
