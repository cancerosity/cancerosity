package org.sevensoft.commons.samdate;

/**
 * @author sks 02-May-2006 08:35:35
 *
 */
public class DateTimeRange {

	private final DateTime	start;
	private final DateTime	end;

	public DateTimeRange(DateTime start, DateTime end) {

		this.start = start;
		if (end == null)
			this.end = start;
		else
			this.end = end;
	}

	/**
	 * Returns true if the date/times defined cover the day defined by this date object
	 */
	public boolean contains(Date date) {
		return start.getTimestamp() <= date.getTimestamp() && date.getTimestamp() <= end.getTimestamp();
	}

	public boolean contains(DateTime datetime) {
		return start.getTimestamp() <= datetime.getTimestamp() && datetime.getTimestamp() <= end.getTimestamp();
	}
}
