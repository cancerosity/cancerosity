package org.sevensoft.commons.samdate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Version: 0.9 14/10/06
 * <p/>
 * immutable replacement for java.util.Date
 * <p/>
 * no warranty at all if your computer explodes then tough use at your own risk
 * <p/>
 * A date instance represents a date in a gregorian calendar. It does not include any time information.
 *
 * @author sks www.sevensoft.org 11-March-2003 12:05:37
 */
public final class Date implements Comparable<Date> {

    public static final int Tuesday = java.util.Calendar.TUESDAY;
    public static final int Monday = java.util.Calendar.MONDAY;
    public static final int Wednesday = java.util.Calendar.WEDNESDAY;
    public static final int Thursday = java.util.Calendar.THURSDAY;
    public static final int Friday = java.util.Calendar.FRIDAY;
    public static final int Saturday = java.util.Calendar.SATURDAY;
    public static final int Sunday = java.util.Calendar.SUNDAY;

    private static ArrayList<String> daysOfMonth;

    private static Logger logger = Logger.getLogger("jeezy");

    static {

        daysOfMonth = new ArrayList<String>(31);
        for (int n = 1; n <= 31; n++) {
            if (n < 10) {
                daysOfMonth.add("0" + String.valueOf(n));
            } else {
                daysOfMonth.add(String.valueOf(n));
            }
        }
    }

    public static List<String> getDaysOfMonth() {
        return daysOfMonth;
    }

    public static long getTimestamp(String value) {

        long timestamp = 0;

        try {

            if (value.startsWith("d")) {

                timestamp = new Date(value).getTimestamp();

            } else if (value.startsWith("dt")) {

                timestamp = new DateTime(value).getTimestamp();

            } else {

                timestamp = Long.parseLong(value.trim());
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timestamp;
    }

    /**
     * Returns the next day after this one, looping on sats
     */
    public static int nextDay(int day) {

        if (day == Date.Saturday) {
            return Date.Sunday;
        } else {
            return day + 1;
        }
    }

    protected long timestamp;

    /**
     * Creates a new Date representing today. Internal timestamp will be set to 00:00:00:000 on the day in question
     */
    public Date() {
        this(new GregorianCalendar());
    }

    /**
     * Creates a new Date with the same timestamp as the date param
     *
     * @param date
     */
    public Date(Date date) {
        this(date.timestamp);
    }

    public Date(DateTime datetime) {
        this(datetime.getTimestamp());
    }

    protected Date(GregorianCalendar cal) {

        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MILLISECOND, 0);

        this.timestamp = cal.getTimeInMillis();
    }

    /**
     * Creates a new Date with the year, month and day set
     */
    public Date(int year, int month, int day) {
        this(new GregorianCalendar(year, month, day));
    }

    /**
     * Creates a new Date from the timestamp
     */
    public Date(long timestamp) {

        GregorianCalendar c = new GregorianCalendar();
        c.setTimeInMillis(timestamp);

        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MILLISECOND, 0);

        this.timestamp = c.getTimeInMillis();
    }

    /**
     * Creates a new string by parsing for common date formats
     * or parsing for a timestamp
     */
    public Date(String string) throws ParseException {

        string = string.trim();

        Matcher matcher = Pattern.compile("(\\d{1,2})\\D{1,}(\\d{1,2})\\D{1,}(\\d{1,4})").matcher(string);
        if (matcher.matches()) {

            String day = matcher.group(1);
            String month = matcher.group(2);
            String year = matcher.group(3);

            if (year.length() == 1) {
                year = "200" + year;
            }

            if (year.length() == 2) {
                year = "20" + year;
            }

            GregorianCalendar c = new GregorianCalendar();
            c.set(Calendar.DAY_OF_MONTH, Integer.parseInt(day));
            c.set(Calendar.MONTH, Integer.parseInt(month) - 1);
            c.set(Calendar.YEAR, Integer.parseInt(year));
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MILLISECOND, 0);

            timestamp = c.getTimeInMillis();

        } else {

            try {
                this.timestamp = Long.parseLong(string);
            } catch (NumberFormatException e) {
                throw new ParseException(e.toString(), 0);
            }

            GregorianCalendar c = new GregorianCalendar();
            c.setTimeInMillis(timestamp);

            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MILLISECOND, 0);

            timestamp = c.getTimeInMillis();

        }
    }

    /**
     * Creates a new date object and parses using the format param
     * <p/>
     * <p/>
     * G Era designator Text AD
     * <p/>
     * y Year Year 1996; 96
     * <p/>
     * M Month in year Month July; Jul; 07
     * <p/>
     * w Week in year Number 27
     * <p/>
     * W Week in month Number 2
     * <p/>
     * D Day in year Number 189
     * <p/>
     * d Day in month Number 10
     * <p/>
     * F Day of week in month Number 2
     * <p/>
     * E Day in week Text Tuesday; Tue
     * <p/>
     * a Am/pm marker Text PM
     * <p/>
     * H Hour in day (0-23) Number 0
     * <p/>
     * k Hour in day (1-24) Number 24
     * <p/>
     * K Hour in am/pm (0-11) Number 0
     * <p/>
     * h Hour in am/pm (1-12) Number 12
     * <p/>
     * m Minute in hour Number 30
     * <p/>
     * s Second in minute Number 55
     * <p/>
     * S Millisecond Number 978
     * <p/>
     * z Time zone General time zone Pacific Standard Time; PST; GMT-08:00
     * <p/>
     * Z Time zone RFC 822 time zone -0800
     */
    public Date(String string, String format) throws ParseException {
        this(new SimpleDateFormat(format).parse(string).getTime());
    }

    //	public Date add(Period period) {
    //		return new Date(timestamp + period.getMillis());
    //	}

    public Date addDays(int days) {
        Calendar c = getCalendar();
        c.add(Calendar.DAY_OF_YEAR, days);
        return new Date(c.getTimeInMillis());
    }

    public Date addMonths(int months) {

        Calendar c = getCalendar();
        c.add(Calendar.MONTH, months);
        return new Date(c.getTimeInMillis());
    }

    public Date addWeeks(int weeks) {

        Calendar calendar = getCalendar();
        calendar.add(Calendar.WEEK_OF_YEAR, weeks);

        long l = calendar.getTimeInMillis();
        return l == timestamp ? this : new Date(calendar.getTimeInMillis());
    }

    public Date addYears(int years) {

        Calendar c = getCalendar();
        c.add(Calendar.YEAR, years);
        return new Date(c.getTimeInMillis());
    }

    public Date beginMonth() {

        Calendar calendar = getCalendar();

        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        long l = calendar.getTimeInMillis();
        return l == timestamp ? this : new Date(calendar.getTimeInMillis());
    }

    public Date beginWeek() {

        Calendar calendar = getCalendar();
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        long l = calendar.getTimeInMillis();
        return l == timestamp ? this : new Date(calendar.getTimeInMillis());
    }

    /**
     * Returns a new Date with the month set to Jan and the day set to 1.
     */
    public Date beginYear() {
        Calendar calendar = getCalendar();
        calendar.set(Calendar.MONTH, Calendar.JANUARY);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return new Date(calendar.getTimeInMillis());
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Date(timestamp);
    }

    /*
      * @see java.lang.Comparable#compareTo(org.sevensoft.commons.util.DateTime)
      */
    public int compareTo(Date arg0) {
        if (timestamp > arg0.timestamp) {
            return 1;
        }
        if (timestamp < arg0.timestamp) {
            return -1;
        }
        return 0;
    }

    /**
     * Move date forward to the next day param
     */
    public Date cycleToDay(int toDay) {

        int thisDay = getDayOfWeek();
        if (toDay < thisDay) {
            return addDays(thisDay - toDay);
        } else {
            return addDays(thisDay - toDay + 7);
        }
    }

    public Date endMonth() {

        Calendar calendar = getCalendar();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return new Date(calendar.getTimeInMillis());
    }

    public Date endWeek() {

        Calendar calendar = getCalendar();
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        return new Date(calendar.getTimeInMillis());
    }

    public Date endYear() {

        Calendar calendar = getCalendar();
        calendar.set(Calendar.YEAR, Calendar.DECEMBER);
        calendar.set(Calendar.DAY_OF_MONTH, 31);

        return new Date(calendar.getTimeInMillis());
    }

    /*
      * @see java.lang.Object#equals(java.lang.Object)
      */
    public boolean equals(Object obj) {

        if (obj instanceof Date) {
            return timestamp == ((Date) obj).getTimestamp();

        } else {
            return false;
        }
    }

    private GregorianCalendar getCalendar() {
        GregorianCalendar c = new GregorianCalendar();
        c.setTimeInMillis(timestamp);
        return c;
    }

    public Date getDate() {
        return this;
    }

    public int getDateOfMonth() {
        return getCalendar().get(Calendar.DATE);
    }

    public boolean isLastDayOfMonth() {
        return getDaysInMonth() == getDateOfMonth();
    }

    /**
     *
     */
    public Day getDay() {

        switch (getDayOfWeek()) {

            case java.util.Calendar.SUNDAY:
                return Day.Sunday;

            case java.util.Calendar.MONDAY:
                return Day.Monday;

            case java.util.Calendar.TUESDAY:
                return Day.Tuesday;

            case java.util.Calendar.WEDNESDAY:
                return Day.Wednesday;

            case java.util.Calendar.THURSDAY:
                return Day.Thursday;

            case java.util.Calendar.FRIDAY:
                return Day.Friday;

            case java.util.Calendar.SATURDAY:
                return Day.Saturday;
        }

        throw new RuntimeException();
    }

    /**
     * Returns an int for the day of the month, eg 26 for a date representing 26th of a month
     */
    public int getDayOfMonth() {
        return getCalendar().get(Calendar.DAY_OF_MONTH);
    }

    /**
     * Returns the day of the week as an int
     */
    public int getDayOfWeek() {
        return getCalendar().get(Calendar.DAY_OF_WEEK);
    }

    public List<Date> getDays(int i) {

        List<Date> dates = new ArrayList<Date>(i);
        Date date = this;
        for (int n = 0; n < i; n++) {
            dates.add(date);
            date = date.nextDay();
        }
        return dates;
    }

    /**
     * Returns days between these days inclusive
     */
    public int getDaysBetween(Date from) {
        return (int) Math.abs((from.timestamp - timestamp) / (1000l * 60 * 60 * 24));
    }

    public int getDaysInMonth() {

        Calendar calendar = getCalendar();
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public int getDaysInYear() {
        Calendar calendar = getCalendar();
        return calendar.getActualMaximum(Calendar.DAY_OF_YEAR);
    }

    public int getFirstDayInMonth() {

        Calendar calendar = getCalendar();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return calendar.get(Calendar.DAY_OF_WEEK);
    }

    public int getHour() {
        return getCalendar().get(Calendar.HOUR_OF_DAY);
    }

    public Month getMonth() {

        switch (getCalendar().get(Calendar.MONTH)) {

            case java.util.Calendar.JANUARY:
                return Month.January;

            case java.util.Calendar.FEBRUARY:
                return Month.February;

            case java.util.Calendar.MARCH:
                return Month.March;

            case java.util.Calendar.APRIL:
                return Month.April;

            case java.util.Calendar.MAY:
                return Month.May;

            case java.util.Calendar.JUNE:
                return Month.June;

            case java.util.Calendar.JULY:
                return Month.July;

            case java.util.Calendar.AUGUST:
                return Month.August;

            case java.util.Calendar.SEPTEMBER:
                return Month.September;

            case java.util.Calendar.OCTOBER:
                return Month.October;

            case java.util.Calendar.NOVEMBER:
                return Month.November;

            case java.util.Calendar.DECEMBER:
                return Month.December;

        }

        throw new RuntimeException("Bug");
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getTimestampString() {
        return String.valueOf(timestamp);
    }

    public int getYear() {
        return getCalendar().get(Calendar.YEAR);
    }

    public int getYearShort() {
        return getYear() - (int) (Math.floor(getYear() / 100d) * 100);
    }

    public int hashCode() {
        return (int) timestamp;
    }

    /**
     * Returns true if this date is in the date range inclusive of start / end. If end date is null then assume end of time, and if start date is null assume epoch
     */
    public boolean inclusive(Date startDate, Date endDate) {

        if (startDate != null && startDate.isAfter(this))
            return false;

        if (endDate != null && endDate.isBefore(this))
            return false;

        return true;
    }

    public boolean isAfter(Date date) {
        return timestamp > date.timestamp;
    }

    public boolean isAfterOrEqual(Date date) {
        return timestamp >= date.timestamp;
    }

    public boolean isAfter(long l) {
        return timestamp > l;
    }

    public boolean isBefore(Date date) {
        return timestamp < date.getDate().timestamp;
    }

    public boolean isBeforeOrEqual(Date date) {
        return timestamp <= date.getDate().timestamp;
    }

    public boolean isBefore(long l) {
        return timestamp < l;
    }

    public boolean isEpoch() {
        return timestamp == 0;
    }

    /**
     * @return
     */
    public boolean isFriday() {
        return getDayOfWeek() == Friday;
    }

    /**
     * Returns true if the date represented by this object is in the future. Ignores the current time. For example if this object represented 25th
     * June 2004 then this method will return true up to 24th June 2004 11:59:59 but no later
     */
    public boolean isFuture() {
        return timestamp > new Date().timestamp;
    }

    /**
     * Returns true if this date represents a date that is a Monday.
     */
    public boolean isMonday() {
        return getDayOfWeek() == Monday;
    }

    /**
     * Returns true if the date represented by this object is in the past. Ignores the current time. For example if this object represented 25th
     * June 2004 then this method will contine to return false until 26th June 2004 00:00:00.
     *
     * @return
     */
    public boolean isPast() {
        return timestamp < new Date().timestamp;
    }

    public boolean isSaturday() {
        return getDayOfWeek() == Saturday;
    }

    public boolean isSunday() {
        return getDayOfWeek() == Sunday;
    }

    public boolean isThursday() {
        return getDayOfWeek() == Thursday;
    }

    public boolean isToday() {
        return new Date().equals(this);
    }

    public boolean isTuesday() {
        return getDayOfWeek() == Tuesday;
    }

    public boolean isWednesday() {
        return getDayOfWeek() == Wednesday;
    }

    public boolean isWeekday() {
        return !isWeekend();
    }

    public boolean isWeekend() {
        return isSaturday() || isSunday();
    }

    public boolean isYesterday() {
        return new Date().removeDays(1).equals(this);
    }

    /**
     * Returns a list of Date objects for this date and each successive day up to and including the end Date parameter.
     */
    public List<Date> listTo(Date end) {

        if (isAfter(end) || equals(end))
            return Collections.emptyList();

        List<Date> dates = new ArrayList<Date>();
        Date date = this;
        while (end.isAfter(date)) {
            dates.add(date);
            date = date.nextDay();
        }
        dates.add(end);

        return dates;
    }

    /**
     * Returns a new Date representing the next day after this.
     *
     * @return
     */
    public Date nextDay() {
        return addDays(1);
    }

    /**
     * Returns a new Date representing the next month after this. If the day is set to a number that doesn't exist in the new month, then days will
     * roll over. Ie, if this was the 31st January and you called this method you'd get 3rd March.
     *
     * @return
     */
    public Date nextMonth() {
        return addMonths(1);
    }

    public Date nextSunday() {

        if (isSunday()) {
            return this;
        }

        int days = getDayOfWeek() - Date.Sunday;
        return addDays(days);
    }

    public Date nextWeek() {
        return addWeeks(1);
    }

    public Date nextYear() {
        return addYears(1);
    }

    /**
     * @return
     */
    public Date previousDay() {
        return addDays(-1);
    }

    public Date previousMonth() {
        return addMonths(-1);
    }

    public Date previousWeek() {
        return addWeeks(-1);
    }

    public Date previousYear() {
        return addYears(-1);
    }

    //	/**
    //	 *
    //	 */
    //	public Date remove(Period period) {
    //		return new Date(timestamp - period.getMillis());
    //	}

    public Date removeDays(int i) {
        return addDays(-i);
    }

    public Date removeMonths(int i) {
        return addMonths(-i);
    }

    public Date removeWeeks(int i) {
        return addWeeks(-i);
    }

    public Date removeYear(int i) {
        return addYears(-i);
    }

    public Date setDayOfWeek(int day) {

        Calendar c = getCalendar();
        c.set(Calendar.DAY_OF_WEEK, day);
        return new Date(c.getTimeInMillis());
    }

    public Date setMonth(int month) {

        Calendar c = getCalendar();
        c.set(Calendar.MONTH, month);
        return new Date(c.getTimeInMillis());
    }

    public Date setWeekOfMonth(int week) {

        Calendar c = getCalendar();
        c.set(Calendar.WEEK_OF_MONTH, week);
        return new Date(c.getTimeInMillis());
    }

    public Date setWeekOfYear(int week) {

        Calendar c = getCalendar();
        c.set(Calendar.WEEK_OF_YEAR, week);
        return new Date(c.getTimeInMillis());
    }

    public Date setYear(int year) {

        Calendar c = getCalendar();
        c.set(Calendar.YEAR, year);
        return new Date(c.getTimeInMillis());
    }

    /**
     * 10 Jun 2004
     *
     * @return
     */
    public String toDateString() {
        return new SimpleDateFormat("dd MMM yyyy").format(new java.util.Date(getTimestamp()));
    }

    /**
     * Returns Wed Jun-10-2000
     *
     * @return
     */
    public String toDayDateString() {
        return new SimpleDateFormat("EEE MMM-dd-yyyy").format(new java.util.Date(getTimestamp()));
    }

    public String toDayMonthString() {
        return new SimpleDateFormat("dd MMM").format(new java.util.Date(timestamp));
    }

    /**
     * @return
     */
    public String toEditString() {
        return new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date(getTimestamp()));
    }

    /**
     * Eh, Thursday, October 10th 2005.
     *
     * @return
     */
    public String toFullDateString() {
        return new SimpleDateFormat("EEEEE, MMMMM dd, yyyy").format(new java.util.Date(getTimestamp()));
    }

    public String toIso8601() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date(timestamp));
    }

    public String toMonthYearString() {
        return new SimpleDateFormat("MMM yyyy").format(new java.util.Date(getTimestamp()));
    }

    public String toString() {
        return String.valueOf(getTimestamp());
    }

    public String toString(String format) {
        return new SimpleDateFormat(format).format(new java.util.Date(getTimestamp()));
    }

    public String toDefaultFormat() {
        return toString("dd/MM/yyyy");
    }

    public String toTextDateString() {
        return new SimpleDateFormat("dd MMMM yyyy").format(new java.util.Date(getTimestamp()));
    }

    public String toYear2String() {
        return new SimpleDateFormat("yy").format(new java.util.Date(getTimestamp()));
    }

    public String toYearString() {
        return new SimpleDateFormat("yyyy").format(new java.util.Date(getTimestamp()));
    }

    public static List<Date> getDaysBetween(Date start, Date end) {
        List<Date> days = new ArrayList<Date>();
        for (Date d = start; d.isBefore(end); d = d.nextDay()) {
            days.add(d);
        }
        return days;
    }
}