package org.sevensoft.commons.samdate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

/**
 * immutable replacement for java.util.Date
 * <p/>
 * no warranty at all if your computer explodes then tough use at your own risk
 *
 * @author sks www.sevensoft.org 11-March-2003 12:05:37
 */
public final class DateTime {

    private static Logger logger = Logger.getLogger("jeezy");

    private long timestamp;

    /**
     * Creates a new DateTime object with the time set to the current system time as given by System.currentTimeMillis();
     */
    public DateTime() {
        this(System.currentTimeMillis());
    }

    public DateTime(Date date) {
        this(date.timestamp);
    }

    public DateTime(Date date, Time time) {
        this(date.getTimestamp() + time.getTimestamp());
    }

    public DateTime(int year, int month, int day) {
        this(year, month, day, 0);
    }

    public DateTime(int year, int month, int day, int hour) {
        this(year, month, day, hour, 0, 0);
    }

    public DateTime(int year, int month, int day, int hour, int minute) {
        this(year, month, day, hour, minute, 0);
    }

    public DateTime(int year, int month, int day, int hour, int minute, int second) {
        this(new GregorianCalendar(year, month, day, hour, minute, second).getTimeInMillis());
    }

    public DateTime(long t) {
        this.timestamp = t;
    }

    public DateTime(String string) throws ParseException {

        logger.fine("[datetime] instantiating datetime: " + string);

        string = string.trim();

        logger.fine("[datetime] after regex: " + string);

        if (string.matches("\\d{2}:\\d{2} \\d{2}-\\d{2}-\\d{2}")) {

            timestamp = new SimpleDateFormat("HH:mm dd-MM-yy").parse(string).getTime();

        } else if (string.matches("\\d{2}:\\d{2} \\d{2}-\\d{2}-\\d{4}")) {

            timestamp = new SimpleDateFormat("HH:mm dd-MM-yyyy").parse(string).getTime();

        } else if (string.matches("\\d{2}-\\d{2}-\\d{2}")) {

            timestamp = new SimpleDateFormat("dd-MM-yy").parse(string).getTime();

        } else if (string.matches("\\d{2}-\\d{2}-\\d{4}")) {

            timestamp = new SimpleDateFormat("dd-MM-yyyy").parse(string).getTime();

        } else {

            try {
                this.timestamp = Long.parseLong(string);
            } catch (NumberFormatException e) {
                throw new ParseException(e.toString(), 0);
            }

        }
    }

    public DateTime(String string, String format) throws ParseException {
        this(new SimpleDateFormat(format).parse(string).getTime());
    }

    public DateTime addDays(int days) {
        Calendar c = getCalendar();
        c.add(Calendar.DAY_OF_YEAR, days);
        return new DateTime(c.getTimeInMillis());
    }

    public DateTime addHours(int hours) {
        return new DateTime(timestamp + (1000l * 60 * 60 * hours));
    }

    public DateTime addMillis(long l) {
        return new DateTime(timestamp + l);
    }

    public DateTime addMinutes(int minutes) {
        return new DateTime(timestamp + (1000l * 60 * minutes));
    }

    public DateTime addSeconds(int s) {
        return new DateTime(timestamp + (s * 1000));
    }

    /**
     * Returns a new instance of DateTime that represents the current day but with all time information set to zero. Ie 00:00:00:00 on the day in
     * question. This would then have an equal timestamp to that of a Date object representing the same day.
     *
     * @return DateTime
     */
    public DateTime beginDay() {

        Calendar calendar = getCalendar();

        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return new DateTime(calendar.getTimeInMillis());
    }

    /**
     * Returns a new DateTime with all time information set to the beginning of the hour, ie, sets minutes, seconds and millis to zero.
     *
     * @return
     */
    public DateTime beginHour() {

        Calendar calendar = getCalendar();

        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return new DateTime(calendar.getTimeInMillis());
    }

    public DateTime beginMinute() {

        Calendar calendar = getCalendar();

        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return new DateTime(calendar.getTimeInMillis());
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new DateTime(timestamp);
    }

    public int compareTo(DateTime date) {
        return (int) (date.timestamp - timestamp);
    }

    /**
     * Returns a new DateTime with all time information set to the end of the hour, ie, sets minutes t0 59, seconds to 59 and millis to 999.
     *
     * @return
     */
    public DateTime endHour() {

        Calendar calendar = getCalendar();

        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);

        return new DateTime(calendar.getTimeInMillis());
    }

    public DateTime endMinute() {

        Calendar calendar = getCalendar();

        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);

        return new DateTime(calendar.getTimeInMillis());
    }

    private GregorianCalendar getCalendar() {
        GregorianCalendar c = new GregorianCalendar();
        c.setTimeInMillis(timestamp);
        return c;
    }

    /**
     * Returns a date object which is this objects timestamp after discarding time information
     *
     * @return
     */
    public Date getDate() {
        return new Date(timestamp);
    }

    /**
     * @return
     */
    public int getHour() {
        return getCalendar().get(Calendar.HOUR_OF_DAY);
    }

    public int getMilli() {
        return getCalendar().get(Calendar.MILLISECOND);
    }

    public int getMinute() {
        return getCalendar().get(Calendar.MINUTE);
    }

    public int getSecond() {
        return getCalendar().get(Calendar.SECOND);
    }

    public Time getTime() {
        return new Time(timestamp);
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getTimestampString() {
        return String.valueOf(timestamp);
    }

    public String getYear() {
        return toString("yyyy");
    }

    public boolean isAfter(Date date) {
        return timestamp > date.getTimestamp();
    }

    public boolean isAm() {
        return false;
    }

    public boolean isBefore(long l) {
        return timestamp < l;
    }

    /**
     * Returns true if the time represented by this instance is in the future as compared to the current system time in millis
     */
    public boolean isFuture() {
        return timestamp > System.currentTimeMillis();
    }

    /**
     * Returns true if the time represented by this instance is in the past as compared to the current system time in millis
     */
    public boolean isPast() {
        return timestamp < System.currentTimeMillis();
    }

    public boolean isPm() {
        return false;
    }

    public Object nextHour() {
        return addHours(1);
    }

    public Object nextMinute() {
        return addMinutes(1);
    }

    public Object previousHour() {
        return addHours(-1);
    }

    public Object previousMinute() {
        return addMinutes(-1);
    }

    // public DateTime2 startHour() {
    //
    // Calendar calendar = getCalendar();
    //
    // calendar.set(Calendar.SECOND, 0);
    // calendar.set(Calendar.MINUTE, 0);
    // calendar.set(Calendar.MILLISECOND, 0);
    //
    // return new DateTime2(calendar.getTimeInMillis());
    // }
    //
    // public DateTime2 startMinute() {
    //
    // Calendar calendar = getCalendar();
    //
    // calendar.set(Calendar.SECOND, 0);
    // calendar.set(Calendar.MILLISECOND, 0);
    //
    // return new DateTime2(calendar.getTimeInMillis());
    // }
    //
    // /**
    // * Resets
    // */
    // public DateTime2 startOfDay() {
    //
    // Calendar calendar = getCalendar();
    //
    // calendar.set(Calendar.HOUR_OF_DAY, 0);
    // calendar.set(Calendar.SECOND, 0);
    // calendar.set(Calendar.MINUTE, 0);
    // calendar.set(Calendar.MILLISECOND, 0);
    //
    // long l = calendar.getTimeInMillis();
    // return l == timestamp ? this : new DateTime2(calendar.getTimeInMillis());
    // }

    public DateTime setHour(int hour) {

        Calendar c = getCalendar();
        c.set(Calendar.HOUR_OF_DAY, hour);
        return new DateTime(c.getTimeInMillis());
    }

    public DateTime setMilli(int milli) {

        Calendar c = getCalendar();
        c.set(Calendar.MILLISECOND, milli);
        return new DateTime(c.getTimeInMillis());
    }

    public DateTime setMinute(int minute) {

        Calendar c = getCalendar();
        c.set(Calendar.MINUTE, minute);
        return new DateTime(c.getTimeInMillis());
    }

    public DateTime setSecond(int second) {

        Calendar c = getCalendar();
        c.set(Calendar.SECOND, second);
        return new DateTime(c.getTimeInMillis());
    }

    public String toDateString() {
        return new SimpleDateFormat("dd.MMM.yyyy").format(new java.util.Date(getTimestamp()));
    }

    public String toDateTimeString() {
        return new SimpleDateFormat("HH:mm dd-MMM-yyyy ").format(new java.util.Date(getTimestamp()));
    }

    public String toIso8601() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date(timestamp));
    }

    public String toFullDateTimeString(){
        SimpleDateFormat sdt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
        sdt.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdt.format(new java.util.Date(getTimestamp()));
    }

    @Override
    public String toString() {
        return getTimestampString();
    }

    /**
     * G Era designator Text AD
     * <p/>
     * y Year Year 1996; 96
     * <p/>
     * M Month in year Month July; Jul; 07
     * <p/>
     * w Week in year Number 27
     * <p/>
     * W Week in month Number 2
     * <p/>
     * D Day in year Number 189
     * <p/>
     * d Day in month Number 10
     * <p/>
     * F Day of week in month Number 2
     * <p/>
     * E Day in week Text Tuesday; Tue
     * <p/>
     * a Am/pm marker Text PM
     * <p/>
     * H Hour in day (0-23) Number 0
     * <p/>
     * k Hour in day (1-24) Number 24
     * <p/>
     * K Hour in am/pm (0-11) Number 0
     * <p/>
     * h Hour in am/pm (1-12) Number 12
     * <p/>
     * m Minute in hour Number 30
     * <p/>
     * s Second in minute Number 55
     * <p/>
     * S Millisecond Number 978
     * <p/>
     * z Time zone General time zone Pacific Standard Time; PST; GMT-08:00
     * <p/>
     * Z Time zone RFC 822 time zone -0800
     *
     * @param format
     * @return
     * @throws ParseException
     * @throws ParseException
     */
    public String toString(String format) {
        return new SimpleDateFormat(format).format(new java.util.Date(getTimestamp()));
    }

    public String toTimeString() {
        return new SimpleDateFormat("HH:mm").format(new java.util.Date(getTimestamp()));
    }

    /**
     *
     */
    public java.util.Date getJdkDate() {
        return new java.util.Date(timestamp);
    }

}