package org.sevensoft.commons.samdate;

import java.util.Calendar;

/**
 * @author sks 19 Mar 2007 11:51:23
 *
 */
public enum Day {

	Sunday(Calendar.SUNDAY), Monday(Calendar.MONDAY), Tuesday(Calendar.TUESDAY), Wednesday(Calendar.WEDNESDAY), Thursday(Calendar.THURSDAY), Friday(
			Calendar.FRIDAY), Saturday(Calendar.SATURDAY);

	private int	jdkInt;

	private Day(int jdkInt) {
		this.jdkInt = jdkInt;
	}

	public int getDaysBetween(Day day) {
		return Math.abs(day.jdkInt - this.jdkInt);
	}
}
