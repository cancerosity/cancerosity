package org.sevensoft.commons.httpc;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Sam 26-Sep-2003 00:31:58
 */
public class MutableUrl {

	private Map<String, String[]>	parameters;
	private int				port;
	private String			scheme, hostname, path, anchor;

	//	public MutableUrl(HttpServletRequest request) {
	//
	//		this.path = "/";
	//		this.parameters = new HashMap<String, String[]>();
	//
	//		setScheme(request.getScheme());
	//		setHostname(request.getServerName());
	//		setPort(request.getServerPort());
	//		setPath(request.getRequestURI());
	//
	//		Enumeration e = request.getParameterNames();
	//		while (e.hasMoreElements()) {
	//
	//			String name = (String) e.nextElement();
	//			String[] values = request.getParameterValues(name);
	//			setParameters(name, values);
	//		}
	//	}

	public MutableUrl(MutableUrl url) {

		this.port = url.port;
		this.scheme = url.scheme;
		this.hostname = url.hostname;
		this.path = url.path;
		this.anchor = url.anchor;

		this.parameters = new HashMap(url.getParameters());
	}

	public MutableUrl(URL url) {

		this.port = url.getPort();
		this.scheme = url.getProtocol();
		this.hostname = url.getHost();
		this.path = url.getPath();

		this.parameters = new HashMap();

		if (url.getQuery() != null) {

			String[] params = url.getQuery().split("&");

			if (params != null) {

				for (int n = 0; n < params.length; n++) {

					String[] param = params[n].split("=");
					if (param != null && param.length == 2)
						addParameter(param[0], param[1]);
				}
			}
		}

		this.anchor = url.getRef();
	}

	public void addParameter(String name, boolean value) {
		addParameter(name, String.valueOf(value));
	}

	public void addParameter(String name, double value) {
		addParameter(name, String.valueOf(value));
	}

	public void addParameter(String name, int value) {
		addParameter(name, String.valueOf(value));
	}

	public void addParameter(String name, long value) {
		addParameter(name, String.valueOf(value));
	}

	public void addParameter(String name, String value) {
		addParameters(name, new String[] { value });
	}

	public void addParameters(String name, String[] s) {

		String[] values = parameters.get(name);
		if (values == null) {

			values = s;

		} else {

			String[] newValues = new String[values.length + s.length];
			System.arraycopy(values, 0, newValues, 0, values.length);
			System.arraycopy(s, 0, newValues, values.length - 1, s.length);
			values = newValues;
		}

		parameters.put(name, s);
	}

	public URLConnection connect() throws IOException {
		return toURL().openConnection();
	}

	public String getAnchor() {
		return anchor;
	}

	public String getHostname() {
		return hostname;
	}

	public String[] getParameter(String string) {
		return parameters.get(string);
	}

	public Map getParameters() {
		return parameters;
	}

	public String getPath() {
		return path;
	}

	public int getPort() {
		return port;
	}

	public String getScheme() {
		return scheme;
	}

	public void setAnchor(String anchor) {
		this.anchor = anchor;

		if (this.anchor != null)
			this.anchor = this.anchor.trim().toLowerCase();
	}

	public void setHostname(String hostname) {
		if (hostname.endsWith("/"))
			this.hostname = hostname.substring(0, hostname.length() - 1);
		else
			this.hostname = hostname;

		if (this.hostname != null)
			this.hostname = this.hostname.trim().toLowerCase();
	}

	public void setParameter(String name, long value) {
		setParameter(name, String.valueOf(value));
	}

	public void setParameter(String name, String value) {
		setParameters(name, new String[] { value });
	}

	public void setParameters(String name, String[] values) {
		parameters.put(name, values);
	}

	public void setPath(String newPath) {

		if (newPath == null)
			path = "/";

		else {

			newPath = newPath.trim();

			if (newPath.startsWith("/"))
				path = newPath;

			else
				path = path.substring(0, path.lastIndexOf('/') + 1) + newPath;
		}
	}

	public void setPort(int port) {
		this.port = port;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme.trim().toLowerCase();
	}

	/*
	 * @see java.lang.Object#toString()
	 */
	public String toString() {

		StringBuilder sb = new StringBuilder(40);
		sb.append(scheme);
		sb.append("://");
		sb.append(hostname);

		if ((scheme.equals("http") && port != 80) || (scheme.equals("https") && port != 443) || (scheme.equals("ftp") && port != 21)) {

			sb.append(":");
			sb.append(getPort());
		}

		if (!path.equals("/"))
			sb.append(path);

		if (parameters.size() > 0) {

			sb.append("?");

			Iterator iter = parameters.keySet().iterator();
			while (iter.hasNext()) {

				String name = (String) iter.next();
				String[] values = parameters.get(name);
				if (values != null && values.length > 0) {

					for (int n = 0; n < values.length; n++) {

						try {
							String param = URLEncoder.encode(name, "UTF-8") + '=' + URLEncoder.encode(values[n], "UTF-8");
							sb.append(param);
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						}

						if (n < values.length - 1)
							sb.append("&");
					}

					if (iter.hasNext())
						sb.append("&");
				}
			}
		}

		if (anchor != null) {
			sb.append("#");
			sb.append(anchor);
		}

		return sb.toString();
	}

	public URL toURL() throws MalformedURLException {
		return new URL(toString());
	}
}