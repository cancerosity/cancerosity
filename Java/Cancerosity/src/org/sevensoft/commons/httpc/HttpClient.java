package org.sevensoft.commons.httpc;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.superstrings.RandomHelper;

/**
 * @author sks 04-Nov-2004 21:08:00
 */
public class HttpClient {

	private static Logger			logger	= Logger.getLogger("httpclient");

	static {
		System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
	}

	private Map<String, List<String>>	inputHeaders;
	private final HttpMethod		method;
	private final MultiValueMap<String, String>	outputHeaders, parameters;
	private int							responseCode;
	private URL							url;
	private int							connectTimeout;
	private int							readTimeout;

	/**
	 * Response stored in bytes
	 */
	private byte[]						responseBytes;

	/**
	 * Send response to file
	 */
	private boolean						responseToFile;

	/**
	 * Output response stored in file
	 */
	private File						responseFile;

	private boolean						noResponse;
	private String						body;

	public HttpClient(String string) throws MalformedURLException {
		this(string, HttpMethod.Get);
	}

	public HttpClient(String string, HttpMethod method) throws MalformedURLException {

		this.url = new URL(string);
		this.method = method;
		this.parameters = new MultiValueMap(new LinkedHashMap());
		this.outputHeaders = new MultiValueMap(new LinkedHashMap());
	}

	public HttpClient(URL url) throws MalformedURLException {
		this(url, HttpMethod.Post);
	}

	public HttpClient(URL url, HttpMethod method) throws MalformedURLException {
		this(url.toString(), method);
	}

	public void addOutputHeader(String name, String value) {

		if (value == null)
			throw new NullPointerException();

		outputHeaders.put(name, value);
	}

	@SuppressWarnings("null")
	public void connect() throws IOException {

		if (method == HttpMethod.Get) {

			if (parameters.size() > 0) {

				MutableUrl mutableUrl = new MutableUrl(url);

				for (Map.Entry<String, String> entry : parameters.entrySet()) {
					mutableUrl.setParameter(entry.getKey(), entry.getValue());
				}

				url = mutableUrl.toURL();
			}
		}

		logger.fine("connecting url=" + url + ", method=" + method);

		HttpURLConnection urlConn;
		DataOutputStream printout;
		BufferedInputStream in = null;

		// URL connection channel.
		urlConn = (HttpURLConnection) url.openConnection();

		if (connectTimeout > 0) {
			logger.fine("setting connect timeout to " + connectTimeout);
			urlConn.setConnectTimeout(connectTimeout);
		}

		if (readTimeout > 0) {
			logger.fine("setting read timeout to " + readTimeout);
			urlConn.setReadTimeout(readTimeout);
		}

		// Let the run-time system (RTS) know that we want input.
		urlConn.setDoInput(true);

		if (method == HttpMethod.Post) {

			// Let the RTS know that we want to do output.
			urlConn.setDoOutput(true);

			// Specify the content type.
			urlConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

			logger.fine("[HttpClient] setting output for post method");
		}

		// No caching, we want the real thing.
		urlConn.setUseCaches(false);

		// fake user agent to bypass site checks on the java string
		urlConn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; YPC 3.0.1; .NET CLR 1.1.4322; .NET CLR 2.0." +
				RandomHelper.getRandomDigits(5) + ")");

		logger.fine("[HttpClient] ---------Start of headers-----");

		// Do output headers
		Iterator<Map.Entry<String, String>> iter = outputHeaders.entrySet().iterator();
		while (iter.hasNext()) {

			Map.Entry<String, String> entry = iter.next();
			logger.fine(entry.getKey() + "=" + entry.getValue());
			urlConn.addRequestProperty(entry.getKey(), entry.getValue());
		}

		if (method == HttpMethod.Post) {

			printout = new DataOutputStream(urlConn.getOutputStream());

			logger.fine("[HttpClient] ----Start of body---");

			StringBuilder sb = new StringBuilder();
			iter = parameters.entrySet().iterator();
			while (iter.hasNext()) {

				Map.Entry<String, String> entry = iter.next();

				logger.fine(entry.getKey() + "=" + entry.getValue());

				sb.append(entry.getKey());
				sb.append("=");
				sb.append(entry.getValue());

				if (iter.hasNext()) {
					sb.append("&");
				}
			}

			printout.writeBytes(sb.toString());

			if (body != null) {
				printout.writeBytes(body);
			}

			printout.flush();
			printout.close();

		}

		responseCode = urlConn.getResponseCode();
		logger.fine("responseCode:" + responseCode);

		inputHeaders = urlConn.getHeaderFields();
		logger.fine("Input headers:" + inputHeaders);

		// only do content if response code is 200
		if (responseCode == 200 && noResponse == false) {

			OutputStream out = null;
			try {

				// wrap incoming response in buffered stream for effeciency
				in = new BufferedInputStream(urlConn.getInputStream());

				if (responseToFile) {

					// responseFile = new File("c:/temp.txt");
					responseFile = File.createTempFile("httpc_response_file", null);
					responseFile.deleteOnExit();

					logger.fine("creating file for output:" + responseFile);
					out = new BufferedOutputStream(new FileOutputStream(responseFile));

				} else {

					logger.fine("creating byte array");
					out = new ByteArrayOutputStream();
				}

				byte[] b = new byte[1024];
				int size = 0;
				int i = -1;
				while ((i = in.read(b, 0, 1024)) != -1) {
					out.write(b, 0, i);
					size += i;
				}

				logger.fine("input complete, bytes=" + size);

			} catch (IOException e) {

				e.printStackTrace();

			} finally {

				if (in != null) {
					try {
						in.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				if (out != null) {

					if (responseToFile) {

						try {
							out.close();
						} catch (IOException e) {
							e.printStackTrace();
						}

					} else {

						responseBytes = ((ByteArrayOutputStream) out).toByteArray();

					}
				}
			}
		}
	}

	/**
	 * Returns the bytes stored in the byte array output stream
	 */
	public byte[] getBytes() {
		return responseBytes;
	}

	public List<String> getInputHeader(String name) {
		return inputHeaders.get(name);
	}

	public Map<String, List<String>> getInputHeaders() {
		return inputHeaders;
	}

	public byte[] getResponseBytes() {
		return responseBytes;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public File getResponseFile() {
		return responseFile;
	}

	/**
	 * Returns the response as a string
	 */
	public String getResponseString() {
		return responseBytes == null ? null : new String(responseBytes);
	}

	public boolean isResponseToFile() {
		return responseToFile;
	}

	public void setAcceptCharsetHeader(String value) {
		setOutputHeader("Accept-Charset", value);
	}

	public void setAcceptEncodingHeader(String value) {
		setOutputHeader("Accept-Encoding", value);
	}

	public void setAcceptLanguagesHeader(String value) {
		setOutputHeader("Accept-Language", value);
	}

	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * set timeout in millis
	 */
	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	/**
	 * 
	 */
	public void setNoResponse() {
		this.noResponse = true;
	}

	public void setOutputHeader(String name, String value) {

		if (value == null) {
			throw new NullPointerException();
		}

		outputHeaders.put(name, value);
	}

	public void setParameter(String name, Object value) {
		parameters.put(name, value.toString());
	}

	public void setReadTimeout(int readTimeout) {
		this.readTimeout = readTimeout;
	}

	public void setRefererHeader(String value) {
		setOutputHeader("Referer", value);
	}

	public void setResponseToByteArray() {
		this.responseToFile = false;
	}

	public void setResponseToFile() {
		this.responseToFile = true;
	}

	public void setUserAgentHeader(String value) {
		setOutputHeader("User-Agent", value);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append("httpclient, url=" + url + ", params=" + parameters);
		return sb.toString();
	}
}
