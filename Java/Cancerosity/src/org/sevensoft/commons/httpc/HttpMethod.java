package org.sevensoft.commons.httpc;

public enum HttpMethod {
	Post, Get
}