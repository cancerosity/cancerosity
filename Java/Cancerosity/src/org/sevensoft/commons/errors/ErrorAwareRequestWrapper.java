package org.sevensoft.commons.errors;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

/**
 * User: Tanya
 * Date: 01.03.2012
 */
public class ErrorAwareRequestWrapper extends HttpServletResponseWrapper {

    public ErrorAwareRequestWrapper(HttpServletResponse httpServletResponse) {
        super(httpServletResponse);
    }

    public void sendError(int errorCode) throws IOException {
        if (errorCode == HttpServletResponse.SC_NOT_FOUND) {
            throw new PageNotFoundException();
        }

        super.sendError(errorCode);
    }
}
