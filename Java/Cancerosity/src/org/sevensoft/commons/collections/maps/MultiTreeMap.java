package org.sevensoft.commons.collections.maps;

import java.util.TreeMap;

/**
 * @author sks 16 Jul 2006 08:48:49
 *
 */
@Deprecated
public class MultiTreeMap<K, V> extends MultiValueMap<K, V> {

	@Deprecated
	public MultiTreeMap() {
		super(new TreeMap());
	}

}
