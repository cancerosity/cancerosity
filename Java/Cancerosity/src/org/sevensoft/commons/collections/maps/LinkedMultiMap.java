package org.sevensoft.commons.collections.maps;

import java.util.LinkedHashMap;

/**
 * An implementation of map which allows multiple mappings per key. This is stored as a list internally. entrySet returns a set of all mappings so can
 * return many key to values for same key. get returns the first value for the key.
 * 
 * @author sks 11-Apr-2005 23:12:02
 */
@Deprecated
public class LinkedMultiMap<K, V> extends MultiValueMap<K, V> {

	@Deprecated
	public LinkedMultiMap() {
		super(new LinkedHashMap());
	}

}
