package org.sevensoft.commons.collections.maps;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author sks 17-Mar-2006 13:29:51
 *
 */
public class MultiValueMap<K, V> implements Map<K, V> {

	/**
	 * The underlying map type
	 */
	private Map<K, List<V>>	decorator;

	public MultiValueMap() {
		this(new HashMap());
	}

	public MultiValueMap(Map decorator) {
		this.decorator = decorator;
	}

	public void clear() {
		decorator.clear();
	}

	/**
	 * Returns true if this map contains the key to value mapping
	 * 
	 */
	public final boolean contains(K key, V value) {

		List<V> list = list(key);
		if (list == null) {
			return false;
		}

		return list.contains(value);

	}

	public boolean containsKey(Object key) {
		return decorator.containsKey(key);
	}

	public final boolean containsValue(Object value) {
		return values().contains(value);
	}

	public Set<Entry<K, List<V>>> entryListSet() {
		return decorator.entrySet();
	}

	/**
	 * Returns an entry set containing a separate entry for each key / value
	 */
	public Set<java.util.Map.Entry<K, V>> entrySet() {

		if (isEmpty()) {
			return Collections.emptySet();
		}

		Set<Map.Entry<K, V>> set = new LinkedHashSet();
		for (final Map.Entry<K, List<V>> entry : decorator.entrySet()) {

			for (final V value : entry.getValue()) {

				set.add(new Map.Entry<K, V>() {

					{
						k = entry.getKey();
						v = value;
					}
					private K	k;

					private V	v;

					public K getKey() {
						return k;
					}

					public V getValue() {
						return v;
					}

					public V setValue(V value) {
						return null;
					}

				});
			}
		}

		return set;
	}

	public V get(Object key) {
		List<V> list = list(key);
		return list == null ? null : list.get(0);
	}

	public final List<V> getAll(Object obj) {
		return list(obj);
	}

	public boolean isEmpty() {
		return decorator.isEmpty();
	}

	public Set<K> keySet() {
		return decorator.keySet();
	}

	/**
	 * Returns a list of all values for K or null if none are yet defined
	 * 
	 */
	public List<V> list(Object obj) {
		List<V> list = decorator.get(obj);
		return list;
	}

	public V put(K key, V value) {

		List<V> list = list(key);
		if (list == null) {
			list = new ArrayList<V>();
			decorator.put(key, list);
		}
		list.add(value);
		return null;
	}

	public void put(MultiValueMap map) {

		final Set<Map.Entry<K, V>> entrySet = map.entrySet();
		for (Map.Entry<K, V> entry : entrySet) {
			put(entry.getKey(), entry.getValue());
		}
	}

	public final void putAll(K key, Iterable<V> values) {
		for (V value : values) {
			put(key, value);
		}
	}

	public final void putAll(K key, V[] values) {
		for (V value : values)
			put(key, value);
	}

	public void putAll(Map<? extends K, ? extends V> m) {
		for (Map.Entry<? extends K, ? extends V> entry : m.entrySet()) {
			put(entry.getKey(), entry.getValue());
		}
	}

	/**
	 * Removes the particular key to value mapping
	 * 
	 * @param k
	 * @param v
	 */
	public void remove(K k, V v) {

		List<V> values = decorator.get(k);
		if (values != null) {

			values.remove(v);
			if (values.isEmpty())
				decorator.remove(k);
		}
	}

	/**
	 * Removes the first value for the key param
	 */
	public final V remove(Object key) {

		List<V> list = list(key);
		if (list == null)
			return null;

		V v = list.remove(0);
		if (list.isEmpty())
			decorator.remove(key);

		return v;
	}

	/**
	 * Removes all values for this key
	 * 
	 * @param key
	 */
	public void removeAll(Object key) {
		decorator.remove(key);
	}

	public final void set(K k, V v) {
		removeAll(k);
		put(k, v);
	}

	/**
	 * Returns the total size of all values
	 */
	public int size() {
		return decorator.size();
	}

	@Override
	public final String toString() {

		StringBuffer buf = new StringBuffer();
		buf.append("{");

		Iterator<Entry<K, V>> i = entrySet().iterator();
		boolean hasNext = i.hasNext();
		while (hasNext) {
			Entry<K, V> e = i.next();
			K key = e.getKey();
			V value = e.getValue();
			if (key == this)
				buf.append("(this Map)");
			else
				buf.append(key);
			buf.append("=");
			if (value == this)
				buf.append("(this Map)");
			else
				buf.append(value);
			hasNext = i.hasNext();
			if (hasNext)
				buf.append(", ");
		}

		buf.append("}");
		return buf.toString();
	}

	/**
	 * Returns a collection of all the values for all keys
	 */
	public final Collection<V> values() {

		if (isEmpty())
			return Collections.emptySet();

		List<V> list = new ArrayList<V>();
		for (Map.Entry<K, List<V>> entry : entryListSet()) {
			for (V value : entry.getValue())
				list.add(value);

		}

		return list;
	}

	public final int valuesSize() {
		return values().size();
	}

}