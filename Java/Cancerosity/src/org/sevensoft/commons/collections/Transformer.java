package org.sevensoft.commons.collections;

/**
 * @author sks 19-Oct-2005 15:31:47
 * 
 */
public interface Transformer<E, F> {

	public F transform(E obj);
}
