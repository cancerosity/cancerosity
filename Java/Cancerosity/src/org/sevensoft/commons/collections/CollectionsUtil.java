package org.sevensoft.commons.collections;


import java.util.*;

/**
 * @author sks 11-Oct-2005 10:33:29
 */
public class CollectionsUtil {

    public static <E, F> List<F> collect(E[] array, Transformer<E, F> transformer) {
        return transform(Arrays.asList(array), transformer);
    }

    /**
     * Returns the number of times this object is inside this list
     */
    public static <E> int containsCount(List<E> list, E obj) {

        int n = 0;
        for (E e : list) {
            if (e.equals(obj)) {
                n++;
            }
        }
        return n;
    }

    public static <E> Collection<E> filter(Collection<E> list, Predicate<E> predicate) {

        if (list.isEmpty()) {
            return list;
        }

        Iterator<E> iter = list.iterator();
        while (iter.hasNext()) {
            if (!predicate.accept(iter.next())) {
                iter.remove();
            }
        }

        return list;
    }

    public static <K, V> Map<K, V> filter(Map<K, V> map, Predicate<K> predicate) {

        if (map.isEmpty()) {
            return map;
        }

        Iterator<K> iter = map.keySet().iterator();
        while (iter.hasNext()) {
            if (!predicate.accept(iter.next())) {
                iter.remove();
            }
        }

        return map;
    }

    /**
     * Replaces all NULLS in this list with the object /replacement/
     */
    public static <E extends Object> void replaceNulls(List<E> list, E replacement) {

        for (int n = 0; n < list.size(); n++) {

            if (list.get(n) == null) {
                list.set(n, replacement);
            }
        }
    }

    public static void replaceNulls(String[] array, String replacement) {

        for (int n = 0; n < array.length; n++) {
            if (array[n] == null) {
                array[n] = replacement;
            }
        }
    }

    /**
     * Doens't include nulls
     */
    public static <E, F> List<F> transform(Collection<E> source, Transformer<E, F> transformer) {

        List<F> flist = new ArrayList();
        for (E e : source) {

            F f = transformer.transform(e);
            if (f != null) {
                flist.add(transformer.transform(e));
            }
        }

        return flist;
    }

    public static <E, F> Map<E, F> transformValues(Map<E, F> map, Transformer<F, F> transformer) {

        for (Map.Entry<E, F> entry : map.entrySet()) {
            map.put(entry.getKey(), transformer.transform(entry.getValue()));
        }

        return map;
    }
}
