package org.sevensoft.commons.collections.bags;

import java.util.Collection;
import java.util.List;

/**
 * @author sks 4 Nov 2006 21:35:20
 *
 */
public interface Bag<E> extends Collection<E> {

	/**
	 * Returns a list of the elements in this bag in asc order
	 */
	public List<E> listAsc();

	/**
	 * Returns a list of the elements in this bag in descending order
	 */
	public List<E> listDesc();

	public void retainLeast(int x);

	public void retainMost(int x);

	public int size(E e);

	/**
	 * Returns a list of E for which there were only 1 count of in the bag
	 */
	public List<E> uniqueList();

	/**
	 * 
	 */
	public void addAll(E[] words);
}
