package org.sevensoft.commons.collections.bags;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author sks 4 Nov 2006 21:40:06
 *
 */
public class ArrayBag<E> implements Bag<E> {

	/**
	 * Map that maintains the count information
	 */
	private Map<E, Integer>	map;

	public ArrayBag() {
		map = new HashMap<E, Integer>();
	}

	public boolean add(E e) {

		if (map.containsKey(e))
			map.put(e, map.get(e) + 1);
		else
			map.put(e, 1);

		return true;
	}

	public boolean addAll(Collection<? extends E> c) {
		for (E e : c)
			add(e);
		return true;
	}

	public void clear() {
		map.clear();
	}

	public boolean contains(Object o) {
		return map.containsKey(o);
	}

	public boolean containsAll(Collection<?> c) {
		for (Object o : c)
			if (!contains(o))
				return false;
		return true;
	}

	public boolean isEmpty() {
		return size() == 0;
	}

	public Iterator<E> iterator() {
		return map.keySet().iterator();
	}

	Map<E, Integer> getMap() {
		return map;
	}

	public List<E> listAsc() {

		List<E> list = new ArrayList(map.keySet());
		// sort list
		Collections.sort(list, new Comparator<E>() {

			public int compare(E o1, E o2) {
				return getMap().get(o1).compareTo(getMap().get(o2));
			}

		});

		return list;
	}

	/**
	 * Returns a list of elements sorted by most common first 
	 */
	public List<E> listDesc() {

		List<E> list = new ArrayList(map.keySet());
		// sort list
		Collections.sort(list, new Comparator<E>() {

			public int compare(E o1, E o2) {
				return getMap().get(o1).compareTo(getMap().get(o2));
			}
		});

		Collections.reverse(list);

		return list;
	}

	public boolean remove(Object o) {

		if (map.containsKey(o)) {

			int i = map.get(o);
			if (i == 1)
				map.remove(o);
			else
				map.put((E) o, i - 1);

			return true;
		}

		return false;
	}

	/**
	 * Remove each element in this collection by calling remove
	 */
	public void removeOnce(Collection<?> c) {

		for (Object o : c) {
			remove(o);
		}
	}

	/**
	 * Remove all elements from this bag completely if they occur in this collection param
	 */
	public boolean removeAll(Collection<?> c) {

		for (Object o : c) {
			map.remove(o);
		}

		return false;
	}

	/**
	 * Remove all elements entirely that are not in this collection
	 */
	public boolean retainAll(Collection<?> c) {

		Iterator<E> iter = map.keySet().iterator();
		while (iter.hasNext()) {
			if (!c.contains(iter.next()))
				iter.remove();
		}

		return false;
	}

	/**
	 * Only retain the least occuring X elements
	 */
	public void retainLeast(int x) {

		List<E> list = listAsc();
		if (list.size() > x)
			list = list.subList(0, x);

		retainAll(list);
	}

	public void retainMost(int x) {

		List<E> list = listDesc();
		if (list.size() > x)
			list = list.subList(0, x);

		retainAll(list);
	}

	public int size() {
		return map.size();
	}

	public int size(E e) {
		return map.containsKey(e) ? map.get(e) : 0;
	}

	public Object[] toArray() {
		return map.keySet().toArray();
	}

	public <T> T[] toArray(T[] a) {
		return map.keySet().toArray(a);
	}

	public List<E> uniqueList() {
		List<E> list = new ArrayList<E>();
		for (Map.Entry<E, Integer> entry : map.entrySet()) {
			if (entry.getValue() == 1)
				list.add(entry.getKey());
		}
		return list;
	}

	public void addAll(E[] a) {
		for (E e : a)
			add(e);
	}
}
