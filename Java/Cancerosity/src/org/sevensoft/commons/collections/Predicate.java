package org.sevensoft.commons.collections;

/**
 * @author sks 11-Oct-2005 10:33:38
 * 
 */
public interface Predicate<E> {

	public boolean accept(E e);

}
