package org.sevensoft.commons.gaia;


import java.util.ArrayList;
import java.util.List;

/**
 * @author sks 16 Sep 2006 14:29:37
 *
 */
public class UkCounty {

	private static List<String>	counties;

	static {

		counties = new ArrayList<String>();

		counties.add("Aberdeenshire");
		counties.add("Anglesey");
		counties.add("Angus");
		counties.add("Antrim");
		counties.add("Argyllshire");
		counties.add("Armagh");
		counties.add("Ayrshire");
		counties.add("Banffshire");
		counties.add("Bedfordshire");
		counties.add("Berkshire");
		counties.add("Berwickshire");
		counties.add("Brecknockshire");
		counties.add("Buckinghamshire");
		counties.add("Buteshire");
		counties.add("Caernarfonshire");
		counties.add("Caithness");
		counties.add("Cambridgeshire");
		counties.add("Cardiganshire");
		counties.add("Carmarthenshire");
		counties.add("Cheshire");
		counties.add("Clackmannanshire");
		counties.add("Cornwall");
		counties.add("Cromartyshire");
		counties.add("Cumberland");
		counties.add("Denbighshire");
		counties.add("Derbyshire");
		counties.add("Devon");
		counties.add("Dorset");
		counties.add("Down");
		counties.add("Dumfriesshire");
		counties.add("Dunbartonshire");
		counties.add("Durham");
		counties.add("East Lothian");
		counties.add("Essex");
		counties.add("Fermanagh");
		counties.add("Fife");
		counties.add("Flintshire");
		counties.add("Glamorgan");
		counties.add("Gloucestershire");
		counties.add("Hampshire");
		counties.add("Herefordshire");
		counties.add("Hertfordshire");
		counties.add("Huntingdonshire");
		counties.add("Inverness-shire");
		counties.add("Kent");
		counties.add("Kincardineshire");
		counties.add("Kinross");
		counties.add("Kirkcudbrightshire");
		counties.add("Lanarkshire");
		counties.add("Lancashire");
		counties.add("Leicestershire");
		counties.add("Lincolnshire");
		counties.add("Londonderry");
		counties.add("Merioneth");
		counties.add("Middlesex");
		counties.add("Midlothian");
		counties.add("Monmouthshire");
		counties.add("Montgomeryshire");
		counties.add("Morayshire");
		counties.add("Nairnshire");
		counties.add("Norfolk");
		counties.add("Northamptonshire");
		counties.add("Northumberland");
		counties.add("Nottinghamshire");
		counties.add("Orkney");
		counties.add("Oxfordshire");
		counties.add("Pembrokeshire");
		counties.add("Peeblesshire");
		counties.add("Perthshire");
		counties.add("Radnorshire");
		counties.add("Renfrewshire");
		counties.add("Ross-shire");
		counties.add("Roxburghshire");
		counties.add("Rutland");
		counties.add("Selkirkshire");
		counties.add("Shetland");
		counties.add("Shropshire");
		counties.add("Somerset");
		counties.add("Staffordshire");
		counties.add("Stirlingshire");
		counties.add("Suffolk");
		counties.add("Surrey");
		counties.add("Sussex");
		counties.add("Sutherland");
		counties.add("Tyrone");
		counties.add("Warwickshire");
		counties.add("West Lothian");
		counties.add("Westmorland");
		counties.add("Wigtownshire");
		counties.add("Wiltshire");
		counties.add("Worcestershire");
		counties.add("Yorkshire");

	}

	public static List<String> getCounties() {
		return counties;
	}

}
