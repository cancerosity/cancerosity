package org.sevensoft.commons.gaia;

import java.util.ArrayList;

/**
 * @author sks 17 Sep 2006 00:06:35
 *
 */
public class UkMajorTowns {

	private static ArrayList<String>	places;

	static {

		places = new ArrayList<String>();

		places.add("Aberdeen");
		places.add("Aberystwyth");
		places.add("Bath");
		places.add("Bedford");
		places.add("Birmingham");
		places.add("Blackburn");
		places.add("Blackpool");
		places.add("Bolton");
		places.add("Bournemouth");
		places.add("Bradford");
		places.add("Brighton");
		places.add("Bristol");
		places.add("Cambridge");
		places.add("Canterbury");
		places.add("Cardiff");
		places.add("Carlisle");
		places.add("Chelmsford");
		places.add("Cheltenham");
		places.add("Coventry");
		places.add("Doncaster");
		places.add("Edinburgh");
		places.add("Exeter");
		places.add("Fort William");
		places.add("Glasgow");
		places.add("Gloucester");
		places.add("Grimsby");
		places.add("Hereford");
		places.add("Hull");
		places.add("Inverness");
		places.add("Ipswich");
		places.add("Lancaster");
		places.add("Leeds");
		places.add("Leicester");
		places.add("Lincoln");
		places.add("Liverpool");
		places.add("London");
		places.add("Luton");
		places.add("Manchester");
		places.add("Milton Keynes");
		places.add("Newcastle upon Tyne");
		places.add("Northampton");
		places.add("Norwich");
		places.add("Nottingham");
		places.add("Oban");
		places.add("Oxford");
		places.add("Peterborough");
		places.add("Plymouth");
		places.add("Portsmouth");
		places.add("Reading");
		places.add("Sheffield");
		places.add("Southampton");
		places.add("Stirling");
		places.add("Stoke-on-Trent");
		places.add("Sunderland");
		places.add("Swansea");
		places.add("Swindon");
		places.add("Warwick");
		places.add("Wolverhampton");
		places.add("Worcester");
		places.add("York");

	}

	public static ArrayList<String> getPlaces() {
		return places;
	}
}
