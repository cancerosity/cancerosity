package org.sevensoft.commons.gaia;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sks 14-Sep-2004 16:50:01
 */
public enum Continent implements Comparable<Continent> {

	Africa("Africa"), Antartica("Antartica"), Asia("Asia"), Europe("Europe"), NorthAmerica("North America"), SouthAmerica("South America"), Oceania("Oceania");

	private final String	name;
	private List<Country>	countries;

	private Continent(String name) {
		this.name = name;
		countries = new ArrayList<Country>();
	}

	/**
	 * Returns a list of countries that reside in this continent.
	 */
	public List<Country> getCountries() {

		if (countries.isEmpty()) {
			for (Country country : Country.getAll()) {

				if (country.getContinent() == this)
					countries.add(country);
			}
		}
		return countries;
	}

	public String getName() {
		return name;
	}
}