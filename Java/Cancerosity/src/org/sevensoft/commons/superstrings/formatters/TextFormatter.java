package org.sevensoft.commons.superstrings.formatters;

/**
 * @author sks 30 May 2007 19:48:37
 *
 */
public class TextFormatter {

	private String	format;

	public TextFormatter(String format) {
		this.format = format;
	}

	public String format(String string) {
		if (string == null)
			return null;
		StringBuilder sb = new StringBuilder();
		int n = 0;
		char ac[];
		int j = (ac = format.toCharArray()).length;
		for (int i = 0; i < j; i++) {
			char c = ac[i];
			if (c == '#') {
				if (n < string.length())
					sb.append(string.charAt(n));
				n++;
			} else {
				sb.append(c);
			}
		}

		return sb.toString();
	}

}
