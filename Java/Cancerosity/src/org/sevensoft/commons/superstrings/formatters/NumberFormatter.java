package org.sevensoft.commons.superstrings.formatters;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * @author sks 30 May 2007 19:18:54
 *
 */
public class NumberFormatter {

	static java.text.NumberFormat	formatter1	= java.text.NumberFormat.getInstance(), formatter2 = java.text.NumberFormat.getInstance(), formatterEdit;

	static {

		formatterEdit = NumberFormat.getInstance();
		formatterEdit.setMinimumFractionDigits(2);
		formatterEdit.setMaximumFractionDigits(2);
		formatterEdit.setGroupingUsed(false);
	}

	/**
	 * Will format the number with exactly <i>2 </i> decimal places.
	 * 
	 * @param amount
	 * @return
	 */
	public static String format(double amount) {
		return format(amount, 2);

		//int i = (int) (amount * 100);
		//return String.valueOf(((double) i) / 100);
	}

	/**
	 * Will format the number with exactly <i>dp </i> decimal places.
	 * 
	 * @param amount
	 * @return
	 */
	public static String format(double amount, int dp) {
		formatter1.setMinimumFractionDigits(dp);
		formatter1.setMaximumFractionDigits(dp);
		return formatter1.format(amount);
	}

	/**
	 * Will format the number with minimum <i>min </i> decimal places, and
	 * maximum <i>max </i> decimal places.
	 * 
	 * IE, 13 would format as 13 with min=0, 13.0 with min=1, 13.00 with min=2.
	 * 12.5343 would format as 12.53 with max=2, 12.52 with max=1, etc.
	 * 
	 * @param amount
	 * @return
	 */
	public static String format(double amount, int minDp, int maxDp) {
		formatter2.setMinimumFractionDigits(minDp);
		formatter2.setMaximumFractionDigits(maxDp);
		return formatter2.format(amount);
	}

	public static String format(int number) {

		DecimalFormat formatter = new DecimalFormat();
		formatter.setGroupingUsed(true);
		formatter.setGroupingSize(3);
		formatter.setMaximumFractionDigits(0);
		return formatter.format(number);
	}

	/**
	 * @param openingBalance
	 * @return
	 */
	public static String formatEditable(int openingBalance) {
		return formatterEdit.format(openingBalance / 100d);
	}

}
