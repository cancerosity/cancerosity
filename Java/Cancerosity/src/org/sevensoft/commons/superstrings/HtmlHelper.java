package org.sevensoft.commons.superstrings;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author sks 08-Mar-2005 17:32:40
 */
public class HtmlHelper {

    /**
     * Matches any email type text, eg adasd@asdasd.com
     */
    private static Pattern email = Pattern.compile("\\b([a-zA-Z0-9\\.]+?\\@[a-zA-Z0-9\\.\\-]+)\\b", Pattern.CASE_INSENSITIVE
            | Pattern.MULTILINE);

    private static Map<String, String> entities = new HashMap<String, String>();
    private static Map<Integer, String> entitiesUTF = new HashMap<Integer, String>();

    private static Pattern http = Pattern.compile("(^|\\s|;|>)(https?://.*?)(<|$|\\s)", Pattern.CASE_INSENSITIVE
            | Pattern.MULTILINE);

    /**
     * Pattern for matching any a or img tags
     */
    private static final Pattern RemovalTagPattern = Pattern.compile("<a.+?a>|<.+?>", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

    private static final String TagHolder = "!!!!!!!TAG_HOLDER!!!!!!!";

    private static final Pattern TagHolderPattern = Pattern.compile(TagHolder, Pattern.LITERAL);

    private static final Pattern www = Pattern.compile("(^|\\s|;|>)(www\\..*?)(<|$|\\s)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

    static {

        entities.put("&yuml;", "\377");
        entities.put("&quot;", "\"");
        entities.put("&amp;", "&");
        entities.put("&lt;", "<");
        entities.put("&gt;", ">");
        entities.put("&nbsp;", " ");
        entities.put("&iexcl;", "\241");
        entities.put("&cent;", "\242");
        entities.put("&pound;", "\243");
        entities.put("&curren;", "\244");
        entities.put("&yen;", "\245");
        entities.put("&brvbar;", "|");
        entities.put("&sect;", "\247");
        entities.put("&uml;", "\250");
        entities.put("&copy;", "\251");
        entities.put("&ordf;", "\252");
        entities.put("&laquo;", "\253");
        entities.put("&not;", "\254");
        entities.put("&shy;", "-");
        entities.put("&reg;", "\256");
        entities.put("&macr;", "\257");
        entities.put("&deg;", "\260");
        entities.put("&plusmn;", "\261");
        entities.put("&sup2;", "\262");
        entities.put("&sup3;", "\263");
        entities.put("&acute;", "\264");
        entities.put("&micro;", "\265");
        entities.put("&para;", "\266");
        entities.put("&middot;", "\267");
        entities.put("&cedil;", "\270");
        entities.put("&sup1;", "\271");
        entities.put("&ordm;", "\272");
        entities.put("&raquo;", "\273");
        entities.put("&frac14;", "\274");
        entities.put("&frac12;", "\275");
        entities.put("&frac34;", "\276");
        entities.put("&iquest;", "\277");
        entities.put("&Agrave;", "\300");
        entities.put("&Aacute;", "\301");
        entities.put("&Acirc;", "\302");
        entities.put("&Atilde;", "\303");
        entities.put("&Auml;", "\304");
        entities.put("&Aring;", "\305");
        entities.put("&AElig;", "\306");
        entities.put("&Ccedil;", "\307");
        entities.put("&Egrave;", "\310");
        entities.put("&Eacute;", "\311");
        entities.put("&Ecirc;", "\312");
        entities.put("&Euml;", "\313");
        entities.put("&Igrave;", "\314");
        entities.put("&Iacute;", "\315");
        entities.put("&Icirc;", "\316");
        entities.put("&Iuml;", "\317");
        entities.put("&ETH;", "\320");
        entities.put("&Ntilde;", "\321");
        entities.put("&Ograve;", "\322");
        entities.put("&Oacute;", "\323");
        entities.put("&Ocirc;", "\324");
        entities.put("&Otilde;", "\325");
        entities.put("&Ouml;", "\326");
        entities.put("&times;", "\327");
        entities.put("&Oslash;", "\330");
        entities.put("&Ugrave;", "\331");
        entities.put("&Uacute;", "\332");
        entities.put("&Ucirc;", "\333");
        entities.put("&Uuml;", "\334");
        entities.put("&Yacute;", "\335");
        entities.put("&szlig;", "\337");
        entities.put("&agrave;", "\340");
        entities.put("&aacute;", "\341");
        entities.put("&acirc;", "\342");
        entities.put("&atilde;", "\343");
        entities.put("&auml;", "\344");
        entities.put("&aring;", "\345");
        entities.put("&aelig;", "\346");
        entities.put("&ccedil;", "\347");
        entities.put("&egrave;", "\350");
        entities.put("&eacute;", "\351");
        entities.put("&ecirc;", "\352");
        entities.put("&euml;", "\353");
        entities.put("&igrave;", "\354");
        entities.put("&iacute;", "\355");
        entities.put("&icirc;", "\356");
        entities.put("&iuml;", "\357");
        entities.put("&ntilde;", "\361");
        entities.put("&ograve;", "\362");
        entities.put("&oacute;", "\363");
        entities.put("&ocirc;", "\364");
        entities.put("&otilde;", "\365");
        entities.put("&ouml;", "\366");
        entities.put("&divide;", "\367");
        entities.put("&oslash;", "\370");
        entities.put("&ugrave;", "\371");
        entities.put("&uacute;", "\372");
        entities.put("&ucirc;", "\373");
        entities.put("&uuml;", "\374");
        entities.put("&yacute;", "\375");
        entities.put("&yuml;", "\377");

//        entitiesUTF.put(160, "&nbsp;");
//        entitiesUTF.put(38, "&amp;");
//        entitiesUTF.put(34, "&quot;");
        // finance
        entitiesUTF.put(162, "&cent;");
        entitiesUTF.put(8364, "&euro;");
        entitiesUTF.put(163, "&pound;");
        entitiesUTF.put(165, "&yen;");
        // signs
        entitiesUTF.put(169, "&copy;");
        entitiesUTF.put(174, "&reg;");
        entitiesUTF.put(8482, "&trade;");
        entitiesUTF.put(8240, "&permil;");
        entitiesUTF.put(181, "&micro;");
        entitiesUTF.put(183, "&middot;");
        entitiesUTF.put(8226, "&bull;");
        entitiesUTF.put(8230, "&hellip;");
        entitiesUTF.put(8242, "&prime;");
        entitiesUTF.put(8243, "&Prime;");
        entitiesUTF.put(167, "&sect;");
        entitiesUTF.put(182, "&para;");
        entitiesUTF.put(223, "&szlig;");
        // quotations
//        entitiesUTF.put("8249", "&lsaquo;");
//        entitiesUTF.put("8250", "&rsaquo;");
//        entitiesUTF.put("171", "&laquo;");
//        entitiesUTF.put("187", "&raquo;");
//        entitiesUTF.put("8216", "&lsquo;");
//        entitiesUTF.put("8217", "&rsquo;");
//        entitiesUTF.put("8220", "&ldquo;");
//        entitiesUTF.put("8221", "&rdquo;");
//        entitiesUTF.put("8218", "&sbquo;");
//        entitiesUTF.put("8222", "&bdquo;");
//        entitiesUTF.put("60", "&lt;");
//        entitiesUTF.put("62", "&gt;");
//        entitiesUTF.put("8804", "&le;");
//        entitiesUTF.put("8805", "&ge;");
//        entitiesUTF.put("8211", "&ndash;");
//        entitiesUTF.put("8212", "&mdash;");
//        entitiesUTF.put("175", "&macr;");
//        entitiesUTF.put("8254", "&oline;");
//        entitiesUTF.put("164", "&curren;");
//        entitiesUTF.put("166", "&brvbar;");
//        entitiesUTF.put("168", "&uml;");
//        entitiesUTF.put("161", "&iexcl;");
//        entitiesUTF.put("191", "&iquest;");
//        entitiesUTF.put("710", "&circ;");
//        entitiesUTF.put("732", "&tilde;");
//        entitiesUTF.put("176", "&deg;");
//        entitiesUTF.put("8722", "&minus;");
//        entitiesUTF.put("177", "&plusmn;");
//        entitiesUTF.put("247", "&divide;");
//        entitiesUTF.put("8260", "&frasl;");
//        entitiesUTF.put("215", "&times;");
//        entitiesUTF.put("185", "&sup1;");
//        entitiesUTF.put("178", "&sup2;");
//        entitiesUTF.put("179", "&sup3;");
//        entitiesUTF.put("188", "&frac14;");
//        entitiesUTF.put("189", "&frac12;");
//        entitiesUTF.put("190", "&frac34;");
        // math / logical
//        entitiesUTF.put("402", "&fnof;");
//        entitiesUTF.put("8747", "&int;");
//        entitiesUTF.put("8721", "&sum;");
//        entitiesUTF.put("8734", "&infin;");
//        entitiesUTF.put("8730", "&radic;");
//        entitiesUTF.put("8764", "&sim;");
//        entitiesUTF.put("8773", "&cong;");
//        entitiesUTF.put("8776", "&asymp;");
//        entitiesUTF.put("8800", "&ne;");
//        entitiesUTF.put("8801", "&equiv;");
//        entitiesUTF.put("8712", "&isin;");
//        entitiesUTF.put("8713", "&notin;");
//        entitiesUTF.put("8715", "&ni;");
//        entitiesUTF.put("8719", "&prod;");
//        entitiesUTF.put("8743", "&and;");
//        entitiesUTF.put("8744", "&or;");
//        entitiesUTF.put("172", "&not;");
//        entitiesUTF.put("8745", "&cap;");
//        entitiesUTF.put("8746", "&cup;");
//        entitiesUTF.put("8706", "&part;");
//        entitiesUTF.put("8704", "&forall;");
//        entitiesUTF.put("8707", "&exist;");
//        entitiesUTF.put("8709", "&empty;");
//        entitiesUTF.put("8711", "&nabla;");
//        entitiesUTF.put("8727", "&lowast;");
//        entitiesUTF.put("8733", "&prop;");
//        entitiesUTF.put("8736", "&ang;");
//        // undefined
//        entitiesUTF.put("180", "&acute;");
//        entitiesUTF.put("184", "&cedil;");
//        entitiesUTF.put("170", "&ordf;");
//        entitiesUTF.put("186", "&ordm;");
//        entitiesUTF.put("8224", "&dagger;");
//        entitiesUTF.put("8225", "&Dagger;");
        // alphabetical special chars
        entitiesUTF.put(192, "&Agrave;");
        entitiesUTF.put(193, "&Aacute;");
        entitiesUTF.put(194, "&Acirc;");
        entitiesUTF.put(195, "&Atilde;");
        entitiesUTF.put(196, "&Auml;");
        entitiesUTF.put(197, "&Aring;");
        entitiesUTF.put(198, "&AElig;");
        entitiesUTF.put(199, "&Ccedil;");
        entitiesUTF.put(200, "&Egrave;");
        entitiesUTF.put(201, "&Eacute;");
        entitiesUTF.put(202, "&Ecirc;");
        entitiesUTF.put(203, "&Euml;");
        entitiesUTF.put(204, "&Igrave;");
        entitiesUTF.put(205, "&Iacute;");
        entitiesUTF.put(206, "&Icirc;");
        entitiesUTF.put(207, "&Iuml;");
        entitiesUTF.put(208, "&ETH;");
        entitiesUTF.put(209, "&Ntilde;");
        entitiesUTF.put(210, "&Ograve;");
        entitiesUTF.put(211, "&Oacute;");
        entitiesUTF.put(212, "&Ocirc;");
        entitiesUTF.put(213, "&Otilde;");
        entitiesUTF.put(214, "&Ouml;");
        entitiesUTF.put(216, "&Oslash;");
        entitiesUTF.put(338, "&OElig;");
        entitiesUTF.put(352, "&Scaron;");
        entitiesUTF.put(217, "&Ugrave;");
        entitiesUTF.put(218, "&Uacute;");
        entitiesUTF.put(219, "&Ucirc;");
        entitiesUTF.put(220, "&Uuml;");
        entitiesUTF.put(221, "&Yacute;");
        entitiesUTF.put(376, "&Yuml;");
        entitiesUTF.put(222, "&THORN;");
        entitiesUTF.put(224, "&agrave;");
        entitiesUTF.put(225, "&aacute;");
        entitiesUTF.put(226, "&acirc;");
        entitiesUTF.put(227, "&atilde;");
        entitiesUTF.put(228, "&auml;");
        entitiesUTF.put(229, "&aring;");
        entitiesUTF.put(230, "&aelig;");
        entitiesUTF.put(231, "&ccedil;");
        entitiesUTF.put(232, "&egrave;");
        entitiesUTF.put(233, "&eacute;");
        entitiesUTF.put(234, "&ecirc;");
        entitiesUTF.put(235, "&euml;");
        entitiesUTF.put(236, "&igrave;");
        entitiesUTF.put(237, "&iacute;");
        entitiesUTF.put(238, "&icirc;");
        entitiesUTF.put(239, "&iuml;");
        entitiesUTF.put(240, "&eth;");
        entitiesUTF.put(241, "&ntilde;");
        entitiesUTF.put(242, "&ograve;");
        entitiesUTF.put(243, "&oacute;");
        entitiesUTF.put(244, "&ocirc;");
        entitiesUTF.put(245, "&otilde;");
        entitiesUTF.put(246, "&ouml;");
        entitiesUTF.put(248, "&oslash;");
        entitiesUTF.put(339, "&oelig;");
        entitiesUTF.put(353, "&scaron;");
        entitiesUTF.put(249, "&ugrave;");
        entitiesUTF.put(250, "&uacute;");
        entitiesUTF.put(251, "&ucirc;");
        entitiesUTF.put(252, "&uuml;");
        entitiesUTF.put(253, "&yacute;");
        entitiesUTF.put(254, "&thorn;");
        entitiesUTF.put(255, "&yuml;");
        entitiesUTF.put(913, "&Alpha;");
        entitiesUTF.put(914, "&Beta;");
        entitiesUTF.put(915, "&Gamma;");
        entitiesUTF.put(916, "&Delta;");
        entitiesUTF.put(917, "&Epsilon;");
        entitiesUTF.put(918, "&Zeta;");
        entitiesUTF.put(919, "&Eta;");
        entitiesUTF.put(920, "&Theta;");
        entitiesUTF.put(921, "&Iota;");
        entitiesUTF.put(922, "&Kappa;");
        entitiesUTF.put(923, "&Lambda;");
        entitiesUTF.put(924, "&Mu;");
        entitiesUTF.put(925, "&Nu;");
        entitiesUTF.put(926, "&Xi;");
        entitiesUTF.put(927, "&Omicron;");
        entitiesUTF.put(928, "&Pi;");
        entitiesUTF.put(929, "&Rho;");
        entitiesUTF.put(931, "&Sigma;");
        entitiesUTF.put(932, "&Tau;");
        entitiesUTF.put(933, "&Upsilon;");
        entitiesUTF.put(934, "&Phi;");
        entitiesUTF.put(935, "&Chi;");
        entitiesUTF.put(936, "&Psi;");
        entitiesUTF.put(937, "&Omega;");
        entitiesUTF.put(945, "&alpha;");
        entitiesUTF.put(946, "&beta;");
        entitiesUTF.put(947, "&gamma;");
        entitiesUTF.put(948, "&delta;");
        entitiesUTF.put(949, "&epsilon;");
        entitiesUTF.put(950, "&zeta;");
        entitiesUTF.put(951, "&eta;");
        entitiesUTF.put(952, "&theta;");
        entitiesUTF.put(953, "&iota;");
        entitiesUTF.put(954, "&kappa;");
        entitiesUTF.put(955, "&lambda;");
        entitiesUTF.put(956, "&mu;");
        entitiesUTF.put(957, "&nu;");
        entitiesUTF.put(958, "&xi;");
        entitiesUTF.put(959, "&omicron;");
        entitiesUTF.put(960, "&pi;");
        entitiesUTF.put(961, "&rho;");
        entitiesUTF.put(962, "&sigmaf;");
        entitiesUTF.put(963, "&sigma;");
        entitiesUTF.put(964, "&tau;");
        entitiesUTF.put(965, "&upsilon;");
        entitiesUTF.put(966, "&phi;");
        entitiesUTF.put(967, "&chi;");
        entitiesUTF.put(968, "&psi;");
        entitiesUTF.put(969, "&omega;");
        // symbols
        entitiesUTF.put(8501, "&alefsym;");
        entitiesUTF.put(982, "&piv;");
        entitiesUTF.put(8476, "&real;");
        entitiesUTF.put(977, "&thetasym;");
        entitiesUTF.put(978, "&upsih;");
        entitiesUTF.put(8472, "&weierp;");
        entitiesUTF.put(8465, "&image;");
        entitiesUTF.put(8592, "&larr;");
        entitiesUTF.put(8593, "&uarr;");
        entitiesUTF.put(8594, "&rarr;");
        entitiesUTF.put(8595, "&darr;");
        entitiesUTF.put(8596, "&harr;");
        entitiesUTF.put(8629, "&crarr;");
        entitiesUTF.put(8656, "&lArr;");
        entitiesUTF.put(8657, "&uArr;");
        entitiesUTF.put(8658, "&rArr;");
        entitiesUTF.put(8659, "&dArr;");
        entitiesUTF.put(8660, "&hArr;");
        entitiesUTF.put(8756, "&there4;");
        entitiesUTF.put(8834, "&sub;");
        entitiesUTF.put(8835, "&sup;");
        entitiesUTF.put(8836, "&nsub;");
        entitiesUTF.put(8838, "&sube;");
        entitiesUTF.put(8839, "&supe;");
        entitiesUTF.put(8853, "&oplus;");
        entitiesUTF.put(8855, "&otimes;");
        entitiesUTF.put(8869, "&perp;");
        entitiesUTF.put(8901, "&sdot;");
        entitiesUTF.put(8968, "&lceil;");
        entitiesUTF.put(8969, "&rceil;");
        entitiesUTF.put(8970, "&lfloor;");
        entitiesUTF.put(8971, "&rfloor;");
        entitiesUTF.put(9001, "&lang;");
        entitiesUTF.put(9002, "&rang;");
        entitiesUTF.put(9674, "&loz;");
        entitiesUTF.put(9824, "&spades;");
        entitiesUTF.put(9827, "&clubs;");
        entitiesUTF.put(9829, "&hearts;");
        entitiesUTF.put(9830, "&diams;");
        entitiesUTF.put(8194, "&ensp;");
        entitiesUTF.put(8195, "&emsp;");
        entitiesUTF.put(8201, "&thinsp;");
        entitiesUTF.put(8204, "&zwnj;");
        entitiesUTF.put(8205, "&zwj;");
        entitiesUTF.put(8206, "&lrm;");
        entitiesUTF.put(8207, "&rlm;");
        entitiesUTF.put(173, "&shy;");





    }

    /**
     * Replaces email and www. links with proper html markup
     */
    public static String convertLinks(String string) {

        if (string == null) {
            return null;
        }

        string = www.matcher(string).replaceAll("$1<a href=\"http://$2\">$2</a>$3");
        string = email.matcher(string).replaceAll("<a href=\"mailto:$1\">$1</a>");
        string = http.matcher(string).replaceAll("$1<a href=\"$2\">$2</a>$3");

        return string;
    }

    public static String decodeEntities(String string) {

        if (string == null) {
            return null;
        }

        for (Map.Entry<String, String> entry : entities.entrySet()) {
            string = string.replace(entry.getKey(), entry.getValue());
        }

        return string;
    }

    public static String encodeEntities(String string) {

        if (string == null) {
            return null;
        }

        for (Map.Entry<String, String> entry : entities.entrySet())
            string = string.replace(entry.getValue(), entry.getKey());

        return string;
    }

    public static String escape(String string) {

        if (string == null) {
            return null;
        }

        return string.replace("<", "&lt;").replace(">", "&gt;").replace("\"", "&quot;").replace("\n", "<br/>");
    }

    /**
     * Returns a List of Strings which are the URLs contained in any A tags in the string
     */
    public static List<String> getAnchorUrls(String content) {

        List<String> list = new ArrayList();

        Pattern pattern = Pattern.compile("<a\\s.*?href=(\"|')(.*?)(\"|').*?>", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()) {

            String url = matcher.group(2).toLowerCase();
            list.add(url);
        }

        return list;
    }

    public static Color getColor(String string) {

        if (string == null || string.length() < 6 || string.length() > 7) {
            return null;
        }

        Color colobj = null;

        try {
            if (string.charAt(0) == '#') {
                string = string.substring(1); // remove first character
            }
            int colvalue = Integer.parseInt(string, 16);
            colobj = new Color(colvalue);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return colobj;
    }

    /**
     * Convert Color object to HTML string rapresentation (#RRGGBB).
     */
    public static String getHTMLColor(Color c) {

        String colorR = "0" + Integer.toHexString(c.getRed());
        colorR = colorR.substring(colorR.length() - 2);

        String colorG = "0" + Integer.toHexString(c.getGreen());
        colorG = colorG.substring(colorG.length() - 2);

        String colorB = "0" + Integer.toHexString(c.getBlue());
        colorB = colorB.substring(colorB.length() - 2);

        String html_color = "#" + colorR + colorG + colorB;
        return html_color;
    }

    /**
     * Remove all tags, (anchors include inside text) and put them in tags list
     */
    public static String holdTags(String string, List<String> tags) {

        tags.clear();

        //escapes all dollar signs ($) before passing any text to the appendReplacement() method.
//        string = string.replaceAll("\\$", "\\\\\\$");

        StringBuffer sb = new StringBuffer();

        // first remove all image and anchor tags and store them away
        Matcher matcher = RemovalTagPattern.matcher(string);
        while (matcher.find()) {
            tags.add(matcher.group());
            matcher.appendReplacement(sb, TagHolder);
        }
        matcher.appendTail(sb);

        return sb.toString();
    }

    public static String nl2br(String string) {
        return string == null ? null : string.replace("\n", "<br/>");
    }

    public static String removeHtmlTags(String string) {
        return string.replaceAll("<.*?>", "");
    }

    /**
     * This method will convert strings in the form www., http://, and https:// to <a> tags
     * It will also convert apparant email addresses to a tags with mailto: protocol
     */
    public static String resolveLinks(String string) {

        if (string == null) {
            return null;
        }

        List<String> tags = new ArrayList<String>();
        string = holdTags(string, tags);

        string = www.matcher(string).replaceAll("$1<a href=\"http://$2\">$2</a>$3");
        string = email.matcher(string).replaceAll("<a href=\"mailto:$1\">$1</a>");
        string = http.matcher(string).replaceAll("$1<a href=\"$2\">$2</a>$3");

        return restoreTags(string, tags);
    }

    /**
     * replace all placeholders with the tags from param tags
     */
    public static String restoreTags(String string, List<String> tags) {

        StringBuffer sb = new StringBuffer();

        Matcher matcher = TagHolderPattern.matcher(string);
        while (matcher.find() && tags.size() > 0) {
            matcher.appendReplacement(sb, tags.remove(0));
        }
        matcher.appendTail(sb);

        return sb.toString();
    }

    /**
     * Will remove all html tags from this string.
     * Remove content inside <script> and <style> tags, other tags can just be removed in place
     * <p/>
     * tagToSpace will put a space in for each tag stripped out
     */
    public static String stripHtml(String string, String replacement) {

        if (string == null) {
            return null;
        }

        if (replacement == null) {
            replacement = "";
        }

        Pattern pattern = Pattern.compile("<script>.*?</script>", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        string = pattern.matcher(string).replaceAll(replacement);

        pattern = Pattern.compile("<style>.*?</style>", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        string = pattern.matcher(string).replaceAll(replacement);

        pattern = Pattern.compile("<\\s*br\\s*/>", Pattern.CASE_INSENSITIVE);
        string = pattern.matcher(string).replaceAll(replacement);

        string = string.replaceAll("<.*?>", replacement).replaceAll("<.*?$", replacement);

        return string;
    }
    private static String[] XSS_TAGS = new String[]{"applet", "body", "embed", "frame", "script", "frameset", "html",
            "iframe", "img", "style", "layer", "link", "ilayer", "meta", "object"};
    /**
     * Will remove all html tags from this string.
     * Remove content inside <script> and <style> tags, other tags can just be removed in place
     * <p/>
     * tagToSpace will put a space in for each tag stripped out
     */
    public static String stripXss(String string, String replacement) {

        if (string == null) {
            return null;
        }

        if (replacement == null) {
            replacement = "";
        }

        Pattern pattern;
        for (String XSS_TAG : XSS_TAGS) {
            pattern = Pattern.compile("<" + XSS_TAG + ">.*?</" + XSS_TAG + ">", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
            string = pattern.matcher(string).replaceAll(replacement);

            //in case of URLS
            pattern = Pattern.compile("%3C" + XSS_TAG + "%3E.*?%3C%2F" + XSS_TAG + "%3E", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
            string = pattern.matcher(string).replaceAll(replacement);

            pattern = Pattern.compile("%3C" + XSS_TAG + "%3E.*?%3C/" + XSS_TAG + "%3E", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
            string = pattern.matcher(string).replaceAll(replacement);
        }

        pattern = Pattern.compile("<\\s*br\\s*/>", Pattern.CASE_INSENSITIVE);
        string = pattern.matcher(string).replaceAll(replacement);

        pattern = Pattern.compile("%3C\\s*br\\s*%2F%3F", Pattern.CASE_INSENSITIVE);
        string = pattern.matcher(string).replaceAll(replacement);
        pattern = Pattern.compile("%3C\\s*br\\s*/%3F", Pattern.CASE_INSENSITIVE);
        string = pattern.matcher(string).replaceAll(replacement);

        string = string.replaceAll("<.*?>", replacement).replaceAll("<.*?$", replacement);
        string = string.replaceAll("%3C.*?%3F", replacement).replaceAll("%3C.*?$", replacement);

        return string;
    }

    public static void replaseSymbolsWithUTF(String[] array) {
        for (int n = 0; n < array.length; n++) {
            array[n] = replaseSymbolsWithUTF(array[n]);
        }

    }

    public static String replaseSymbolsWithUTF(String string) {
        if (string == null){
            return null;
        }
        StringBuilder stringUtf = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            if (entitiesUTF.containsKey(string.codePointAt(i))) {
                stringUtf.append(entitiesUTF.get(string.codePointAt(i)));
            } else {
                stringUtf.append(string.charAt(i));
            }
        }

        return stringUtf.toString();
    }
}
