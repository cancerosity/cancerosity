package org.sevensoft.commons.superstrings.comparators;

import java.util.Comparator;

/**
 * @author sks 30 May 2007 19:34:01
 *
 */
public class EnumStringComparator implements Comparator<Enum> {

	public int compare(Enum arg0, Enum arg1) {
		return arg0.toString().toLowerCase().compareTo(arg1.toString().toLowerCase());
	}

}
