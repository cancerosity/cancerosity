package org.sevensoft.commons.superstrings.comparators;

import java.io.File;
import java.util.Comparator;

/**
 * @author sks 30 May 2007 19:31:10
 *
 */
public class NaturalFileComparator implements Comparator<File> {

	public static NaturalFileComparator	instance	= new NaturalFileComparator();

	private NaturalFileComparator() {
	}

	public int compare(File o1, File o2) {
		return NaturalStringComparator.instance.compare(o1.getName(), o2.getName());
	}

}