package org.sevensoft.commons.superstrings.comparators;

import java.util.Comparator;

/**
 * @author sks 30 May 2007 19:34:49
 *
 */
public class ClassSimpleNameComparator implements Comparator<Class> {

	public static final ClassSimpleNameComparator	Instance	= new ClassSimpleNameComparator();

	public ClassSimpleNameComparator() {
	}

	public int compare(Class o1, Class o2) {
		String name1 = o1.getSimpleName().toLowerCase();
		String name2 = o2.getSimpleName().toLowerCase();
		return NaturalStringComparator.instance.compare(name1, name2);
	}

}
