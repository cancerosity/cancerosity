package org.sevensoft.commons.superstrings;

import java.util.Collection;
import java.util.Map;
import java.util.Random;

/**
 * @author sks 09-Nov-2004 14:40:30
 */
public class RandomHelper {

	static private Random	rnd	= new Random(System.currentTimeMillis());

	public static String getRandomAlpha(int length) {
		return getRandomString("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXY", length);
	}

	public static String getRandomAlphaLower(int length) {
		return getRandomString("abcdefghijklmnopqrstuvwxyz", length);
	}

	public static String getRandomAlphaUpper(int length) {
		return getRandomString("ABCDEFGHIJKLMNOPQRSTUVWXY", length);
	}

	public static <E> E getRandomArrayItem(E[] arr) {
		E obj = null;
		if (arr != null) {
			int nItems = arr.length;
			if (nItems > 0) {
				obj = arr[rnd.nextInt(nItems)];
			}
		}
		return obj;
	}

	public static <E> E getRandomCollectionItem(Collection<E> c) {
		return (E) getRandomArrayItem(c.toArray());
	}

	public static String getRandomDigits(int nDigits) {
		return getRandomString("1234567890", nDigits);
	}

	public static String getRandomHexDigits(int nDigits) {
		return getRandomString("1234567890ABCDEF", nDigits);
	}

	public static Object getRandomMapKey(Map map) {
		return getRandomCollectionItem(map.keySet());
	}

	public static Object getRandomMapValue(Map map) {
		return getRandomCollectionItem(map.values());
	}

	public static int getRandomNumber(int start, int end) {
		return rnd.nextInt() % (end - start + 1) + start;
	}

	public static String getRandomString(int length) {
		return getRandomString("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", length);
	}

	public static String getRandomString(String charset, int length) {
		StringBuffer sb = new StringBuffer(length);
		int nChars = charset.length();
		for (int i = 0; i < length; i++) {
			int rndIdx = rnd.nextInt(nChars);
			sb.append(charset.substring(rndIdx, rndIdx + 1));
		}
		return sb.toString();
	}

	public static String getRandomStringLower(int i) {
		return getRandomString("abcdefghijklmnopqrstuvwxyz0123456789", i);
	}

	public static String getRandomStringUpper(int l) {
		return getRandomString("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", l);
	}

}
