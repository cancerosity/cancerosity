package org.sevensoft.commons.superstrings.english;

import java.util.Arrays;
import java.util.List;

/**
 * @author sks 30 May 2007 19:19:11
 *
 */
public class CommonWords {

	public static final List	English	= Arrays.asList(new String[] { "and", "but", "who", "him", "her", "is", "it", "to", "when", "what", "if",
			"the", "in", "be", "with", "out", "you", "a", "on", "or", "its", "it's", "for" });

	public CommonWords() {
	}

}