package org.sevensoft.commons.superstrings.english;

/**
 * @author sks 30 May 2007 19:30:19
 *
 */
public class Pluraliser {

	public static String toPlural(String s) {
		if (s == null)
			return null;
		if (s.endsWith("s"))
			return s;
		if (s.endsWith("h") || s.endsWith("x"))
			return (new StringBuilder(String.valueOf(s))).append("es").toString();
		if (s.endsWith("y"))
			return (new StringBuilder(String.valueOf(s.substring(0, s.length() - 1)))).append("ies").toString();
		else
			return (new StringBuilder(String.valueOf(s))).append("s").toString();
	}

}
