package org.sevensoft.commons.superstrings;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.util.List;

/**
 * @author sks 5 Dec 2006 17:05:07
 *
 */
public class WordWrap {

	private final String		string;
	private final int			max;
	private final FontMetrics	metrics;
	private final List<String>	words;
	private String			line;
	private int				width;
	private Graphics			g2;
	private int				height;

	public WordWrap(Graphics g2, String string, int i) {
		this.g2 = g2;
		this.string = string;
		this.metrics = g2.getFontMetrics();
		this.max = i;
		this.words = StringHelper.explodeStrings(string, "\\s");
	}

	public final int getHeight() {
		return height;
	}

	public final String getLine() {
		return line;
	}

	public final int getWidth() {
		return width;
	}

	public boolean next() {

		if (words.isEmpty()) {
			return false;
		}

		StringBuilder sb = new StringBuilder();
		String word = words.remove(0);
		sb.append(word);

		char[] chars = word.toCharArray();
		width = (int) metrics.getStringBounds(chars, 0, chars.length, g2).getWidth();
		height = metrics.getHeight();

		// while we have more words check that the next word will fit and if so add it in the buffer
		while (words.size() > 0) {

			word = " " + words.get(0);

			// get width of next word
			chars = word.toCharArray();
			int x = (int) metrics.getStringBounds(chars, 0, chars.length, g2).getWidth();

			// check if it exceeds size
			if (width + x > max) {

				break;

			} else {

				// otherwise add this word
				sb.append(word);
				words.remove(0);
				width = width + x;
			}
		}

		this.line = sb.toString();
		return true;

	}
}
