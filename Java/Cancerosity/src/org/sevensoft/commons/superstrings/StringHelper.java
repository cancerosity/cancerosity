package org.sevensoft.commons.superstrings;

import org.jdom.Verifier;

import java.util.*;

/**
 *
 *
 *
 * @author sks 08-Mar-2005 17:34:07
 */

/**
 * @author sks 1 Apr 2007 00:08:58
 */
public class StringHelper {

    /**
     *
     */
    public static Object concat(Object... objs) {
        StringBuilder sb = new StringBuilder();
        for (Object obj : objs) {
            sb.append(obj);
        }
        return sb.toString();
    }

    /**
     *
     */
    public static boolean contains(String key, String[] strings) {

        for (String string : strings) {
            if (key.contains(string)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Splits this string up on the regex and returns each string part as an int
     */
    public static List<Integer> explodeIntegers(String strings, String regex) {

        List<Integer> list = new ArrayList<Integer>();
        if (strings == null) {
            return Collections.emptyList();
        }

        for (String string : strings.split(regex)) {

            string = string.trim();

            if (string.length() > 0)
                try {

                    Integer i = Integer.parseInt(string.trim());
                    list.add(i);

                } catch (NumberFormatException e) {
                    //e.printStackTrace();
                }
        }

        return list;
    }

    /**
     *
     */
    public static Collection<Integer> explodeIntegersRanged(String toSplit, String regex) {

        List<String> ranges = StringHelper.explodeStrings(toSplit, regex);
        return getIntegersRanged(ranges);
    }

    public static Map<String, String> explodeMap(String string, String paramSep, String keyValueSep) {

        Map<String, String> map = new HashMap();

        for (String line : string.split(paramSep)) {
            String[] record = line.split(keyValueSep);
            if (record != null && record.length == 2) {
                map.put(record[0].trim(), record[1].trim());
            }
        }

        return map;
    }

    public static List<String> explodeStrings(String strings, String regex) {
        return explodeStrings(strings, regex, true);
    }

    public static List<String> explodeStrings(String strings, String regex, boolean trim) {

        List<String> list = new ArrayList<String>();
        if (strings == null) {
            return Collections.emptyList();
        }

        if (trim)
            for (String string : strings.split(regex)) {
                string = string.trim();
                if (string.length() > 0) {
                    list.add(string);
                }
            }
        else {
            list.addAll(Arrays.asList(strings.split(regex)));
        }

        return list;
    }

    public static List<Integer> getIntegersRanged(Collection<String> ranges) {

        List<Integer> list = new ArrayList();

        for (String string : ranges) {

            try {

                if (string.contains("-")) {

                    String[] range = string.split("-");
                    if (range.length == 2) {

                        int start = Integer.parseInt(range[0]);
                        int end = Integer.parseInt(range[1]);

                        for (int n = start; n <= end; n++) {
                            list.add(start);
                        }
                    }

                } else {

                    int i = Integer.parseInt(string);
                    list.add(i);
                }

            } catch (NumberFormatException e) {
                // e.printStackTrace();
            }
        }

        return list;
    }

    public static String implode(Iterable<?> iterable, String sep) {
        return implode(iterable, sep, true);
    }

    public static String implode(Iterable<?> iterable, String separator, boolean trim) {

        StringBuilder sb = new StringBuilder();

        if (separator == null) {
            separator = ",";
        }

        if (iterable != null) {

            Iterator iter = iterable.iterator();
            while (iter.hasNext()) {

                Object temp = iter.next();
                String string = null;
                if (temp != null) {
                    string = temp.toString();
                }
                if (trim) {
                    if (string != null) {
                        string = string.trim();
                    }
                }

                sb.append(string);

                if (iter.hasNext()) {
                    sb.append(separator);
                }
            }
        }

        return sb.toString().trim();
    }

    /**
     *
     */
    public static String implode(Map<String, String> features, String keyValueSep, String entrySep) {

        StringBuilder sb = new StringBuilder();

        Iterator<Map.Entry<String, String>> iter = features.entrySet().iterator();
        while (iter.hasNext()) {

            Map.Entry<String, String> entry = iter.next();

            sb.append(entry.getKey());

            if (keyValueSep != null) {
                sb.append(keyValueSep);
            }

            sb.append(entry.getValue());

            if (keyValueSep != null) {
                if (iter.hasNext()) {
                    sb.append(entrySep);
                }
            }
        }

        return sb.toString();
    }

    public static String implode(String[] strings, String sep) {
        return implode(Arrays.asList(strings), sep, true);
    }

    /**
     * Returns a string that contains this string param repeated i param times
     */
    public static String repeat(String string, int times) {
        return repeat(string, times, "");
    }

    public static String repeat(String string, int times, String sep) {

        StringBuilder sb = new StringBuilder();
        for (int m = 0; m < times; m++) {

            if (m > 0) {
                if (sep != null) {
                    sb.append(sep);
                }
            }

            sb.append(string);
        }
        return sb.toString();
    }

    public static byte[] simpleXor(byte[] in, String key) {

        // Initialise result
        byte[] result = new byte[in.length];

        // Step through string a character at a time
        for (int i = 0; i < in.length; i++) {
            // Get ASCII code from string, get ASCII code from key (loop through with MOD), XOR the two, get the character from the result
            // % is MOD (modulus), ^ is XOR
            result[i] = (byte) (in[i] ^ key.charAt(i % key.length()));
        }
        return result;
    }

    /**
     * Removes all email addresses from this string
     */
    public static String stripEmails(String string) {

        if (string == null) {
            return null;
        }

        return string.replaceAll("\\b[a-zA-Z0-9\\.\\-_]{2,}@[a-zA-Z0-9\\-\\.]{3,}\\b", "");
    }

    /**
     * Strip new lines from the string
     */
    public static String stripNewLines(String string) {

        if (string == null) {
            return string;
        }

        return string.replaceAll("\n|\r", " ");
    }

    public static String toCamelCase(Class clazz) {
        return toCamelCase(clazz.getSimpleName());
    }

    public static String toCamelCase(Object obj) {
        return toCamelCase(obj.getClass());
    }

    public static String toCamelCase(String string) {

        if (string == null) {
            return null;
        }

        if (string.length() == 0) {
            return null;
        }

        return string.substring(0, 1).toLowerCase() + string.substring(1);
    }

    public static String toSnippet(CharSequence cs, int limit) {
        return toSnippet(cs, limit, null);
    }

    public static String toSnippet(CharSequence cs, int maxChars, String suffix) {

        if (cs == null) {
            return null;
        }

        if (maxChars == 0) {
            return cs.toString();
        }

        String string = cs.toString().trim();

        if (string.length() < maxChars) {
            return string;
        }

        string = string.substring(0, maxChars);
        int index = string.lastIndexOf(' ', maxChars);
        if (index > 0) {
            string = string.substring(0, index);
        }

        if (suffix == null) {
            return string;
        } else {
            return string + suffix;
        }
    }


    public static String toTitleCase(String name) {

        if (name == null) {
            return null;
        }

        if (name.length() == 0) {
            return name;
        }

        List<String> parts = new ArrayList();
        for (String string : explodeStrings(name, "\\s")) {

            if (string.length() == 1) {
                parts.add(string);
            } else {
                parts.add(string.substring(0, 1).toUpperCase() + string.substring(1).toLowerCase());
            }
        }

        return implode(parts, " ", true);
    }

    /**
     * Split up this string by putting words on each line so that the length of a line is never longer than the length value,
     * unless a single word is longer than that value
     */
    public static String wordWrap(String string, int length) {

        StringBuilder sb = new StringBuilder();

        for (String line : string.split("\\n")) {

            for (String word : line.split("\\s")) {

                int n = 0;

                sb.append(word);
                sb.append(" ");
                n = n + word.length();

                if (n >= length) {
                    sb.append("\n");
                    n = 0;
                }

            }

            sb.append("\n");
        }

        return sb.toString();
    }

    /**
     *
     */
    public static String yesno(boolean b) {
        return b ? "Yes" : "No";
    }

    private static String[] metas = new String[]{"?", "$", ")", "(", "+", "%", "&", "*", ".", "/", "\\", "`"};

    public static Map<String, String> stripIllegals(Map<String, String> keywords) {

        // create a new map to put keywords into
        Map<String, String> keywordsCopy = new HashMap(keywords);

        for (String key : keywords.keySet()) {

            if (StringHelper.contains(key, metas)) {
                keywordsCopy.remove(key);
            }
        }

        return keywordsCopy;
    }

    public static String verifyXml(String string) {
        StringBuilder sb = new StringBuilder();
        if (string != null) {

            for (int i = 0; i < string.length(); i++) {
                char ch = string.charAt(i);
                if (Verifier.isXMLCharacter(ch)) {
                    sb.append(ch);
                }
            }
        }

        return sb.toString();
    }

}
