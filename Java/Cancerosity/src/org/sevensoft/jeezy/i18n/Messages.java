package org.sevensoft.jeezy.i18n;

import java.util.ResourceBundle;
import java.util.Locale;

/**
 * User: Tanya
 * Date: 13.05.2011
 */
public class Messages {

    private static ResourceBundle resourceBundle;


    public static void init(String location, String locale) {
        Locale currentLocale = new Locale(location, locale);

        ResourceBundle bundle = ResourceBundle.getBundle("/Messages", currentLocale);

        resourceBundle = bundle;

    }

    public static ResourceBundle getResourceBundle() {
//        assert resourceBundle != null;
        return resourceBundle;
    }

    public static String get(String key) {
        if (getResourceBundle() == null) {
            return key;
        }
        return getResourceBundle().getString(key);
    }

}
