package org.sevensoft.jeezy.http.wrappers;

import org.sevensoft.commons.collections.maps.MultiHashMap;
import org.sevensoft.jeezy.db.EntityObject;

/**
 * @author sks 18 Jul 2006 15:49:39
 *
 */
public class MultiHashMapWrapper extends MultiHashMap {

	private final Class	valueClass;

	public MultiHashMapWrapper(Class valueClass) {
		this.valueClass = valueClass;
	}

	@Override
	public Object get(Object key) {

		Object obj;
		if (key instanceof EntityObject) {
			obj = super.get(((EntityObject) key).getId());
		} else {
			obj = super.get(key);
		}

		if (Boolean.class.equals(valueClass)) {
			return obj == null ? Boolean.FALSE : obj;
		}
		if (Integer.class.equals(valueClass)) {
			return obj == null ? Integer.valueOf(0) : obj;
		}

		if (Long.class.equals(valueClass)) {
			return obj == null ? Long.valueOf(0) : obj;
		}

		if (Double.class.equals(valueClass)) {
			return obj == null ? Double.valueOf(0) : obj;
		}

		return obj;

	}
}
