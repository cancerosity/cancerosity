package org.sevensoft.jeezy.http.results;

/**
 * @author sks 17 Oct 2006 09:46:02
 *
 */
public class HttpCode {

	private final String	message;
	private final int		responseCode;

	public HttpCode(int responseCode, String message) {
		this.responseCode = responseCode;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public int getResponseCode() {
		return responseCode;
	}
}
