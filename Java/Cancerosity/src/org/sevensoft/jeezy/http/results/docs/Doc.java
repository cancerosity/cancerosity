package org.sevensoft.jeezy.http.results.docs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 24-Nov-2005 17:27:11
 * 
 */
public abstract class Doc {

	protected final RequestContext	context;
	protected HttpServletRequest		request;
	protected List<Object>			bodys, heads;
	private final HttpServletResponse	res;

	protected Doc(RequestContext context) {

		this.context = context;
		this.request = context.getRequest();
		this.res = context.getResponse();

		this.bodys = new ArrayList();
		this.heads = new ArrayList();
	}

	public void addBody(Object obj) {
		if (obj != null) {
			bodys.add(obj);
		}
	}

	public void addHead(Object obj) {
		if (obj != null) {
			heads.add(obj);
		}
	}

	public void append(Object obj) {
		addBody(obj);
	}

	public List<Object> getBodies() {
		return bodys;
	}

	public List<Object> getHeads() {
		return heads;
	}

	protected void include(String url) throws ServletException, IOException {
		request.getRequestDispatcher(url).include(request, res);
	}

	public abstract StringBuilder output() throws ServletException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException;
}
