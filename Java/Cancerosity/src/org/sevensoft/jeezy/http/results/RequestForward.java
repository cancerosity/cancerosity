package org.sevensoft.jeezy.http.results;

/**
 * @author sks 13-Jan-2006 11:31:55
 *
 */
public class RequestForward {

	private String	string;

	public RequestForward(String string) {
		this.string = string;
	}

	@Override
	public String toString() {
		return string;
	}

}
