package org.sevensoft.jeezy.http.results.docs;

import java.io.IOException;

import javax.servlet.ServletException;

import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 21 Aug 2006 19:26:18
 *
 */
public class SimpleDoc extends Doc {

	private String	onUnload;

	public SimpleDoc(RequestContext context) {
		super(context);
	}

	public SimpleDoc(RequestContext context, StringBuilder sb) {
		super(context);
		append(sb);
	}

	@Override
	public StringBuilder output() throws ServletException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {

		StringBuilder output = new StringBuilder();
		output.append("<html><head>");

		for (Object obj : heads) {
			output.append(obj);
		}

		output.append("</head>");
		output.append("<body");
		if (onUnload != null) {
			output.append(" onUnload=\"");
			output.append(onUnload);
			output.append("\"");
		}
		output.append(">");

		for (Object obj : bodys) {
			output.append(obj);
		}

		output.append("</body></html>");
		return output;
	}

	public final void setOnUnload(String onUnload) {
		this.onUnload = onUnload;
	}

}
