package org.sevensoft.jeezy.http.results;

import org.jdom.Document;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 * @author sks 13-Sep-2004 17:29:30
 */
public class XmlResult {

	public static final int	Attachment	= 1;
	public static final int	Inline	= 2;

	private final Document	doc;
	private final int		method;
	private String		filename;
	private Format		format;

	public XmlResult(Document doc) {
		this.doc = doc;
		this.method = Inline;
	}

	public XmlResult(Document doc, Format format, String filename) {
		this(doc, filename);
		this.format = format;
	}

	public XmlResult(Document doc, String filename) {
		this.doc = doc;
		this.filename = filename;
		this.method = Attachment;
	}

	public String getFilename() {
		return filename;
	}

	public int getMethod() {
		return method;
	}

	@Override
	public String toString() {

		XMLOutputter outputter = new XMLOutputter();
		if (format != null)
			outputter.setFormat(format);

		return outputter.outputString(doc);
	}

}