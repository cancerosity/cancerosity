package org.sevensoft.jeezy.http.results;

/**
 * @author sks 31-May-2005 16:25:38
 * 
 */
public class StringResult {

	protected String	content;
	protected String	contentType;

	public StringResult(Object content, String contentType) {
		this.content = (content == null ? null : content.toString());
		this.contentType = contentType;
	}

	public String getContent() {
		return content;
	}

	public String getContentType() {
		return contentType;
	}

}
