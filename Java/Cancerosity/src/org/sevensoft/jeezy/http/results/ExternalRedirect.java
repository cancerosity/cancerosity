package org.sevensoft.jeezy.http.results;

import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 27-Sep-2005 13:04:54
 *
 */
public class ExternalRedirect {

	private final String	url;

	public ExternalRedirect(Link link) {
		this.url = link.toString();
	}

	public ExternalRedirect(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	@Override
	public String toString() {
		return url;
	}
}
