package org.sevensoft.jeezy.http.results.docs;

import java.io.IOException;

import javax.servlet.ServletException;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 4 Sep 2006 11:01:24
 *
 */
public class ActionDoc extends Doc {

	private final String	message;
	private final String	link;

	public ActionDoc(RequestContext context, String message, Link link) {
		this(context, message, link.toString());
	}

	public ActionDoc(RequestContext context, String message, String link) {
		super(context);
		this.message = message;
		this.link = link;
	}

	@Override
	public StringBuilder output() throws ServletException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {

		StringBuilder sb = new StringBuilder();
		sb.append("<html><head><title></title>");

		sb.append("<meta http-equiv=\"refresh\" content=\"1;url=" + link + "\"/>");
		sb.append("</head><body>");
		sb.append("<table width='100%' style='margin-top:180px; font-family: Verdana; font-size: 14px; font-weight: bold; color: #555555;'>"
				+ "<tr><td align='center'>");

		sb.append(message);
		sb.append("</td></tr><tr><td align='center'>Please wait to be returned</td></tr></table>");

		sb.append("</body></html>");
		return sb;
	}
}
