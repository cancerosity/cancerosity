package org.sevensoft.jeezy.http.results;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * @author sks 13-Sep-2004 17:29:30
 */
public class StreamResult {

    public enum Type {
        Attachment, Stream
    }

    private final String contentType;
    private String filename;
    private InputStream stream;
    private final Type type;
    private File file;
    private boolean deleteOnExit;

    public StreamResult(BufferedImage image) throws IOException {

        this.contentType = "image/jpeg";
        this.filename = "image";
        this.type = Type.Stream;

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ImageIO.write(image, "jpeg", out);
        out.close();

        this.stream = new ByteArrayInputStream(out.toByteArray());
    }

    /**
     * Stream bytes to browser
     */
    public StreamResult(byte[] bytes, String contentType) {
        this.contentType = contentType;
        this.filename = "image";
        this.type = Type.Stream;
        this.stream = new ByteArrayInputStream(bytes);
    }

    public StreamResult(CharSequence cs, String contentType, String filename, Type type) {
        this.contentType = contentType;
        this.filename = filename;
        this.type = type;
        this.stream = new ByteArrayInputStream(cs.toString().getBytes());
    }

    public StreamResult(File file, String contentType) throws FileNotFoundException {
        this.file = file;
        this.contentType = contentType;
        this.type = Type.Stream;
        this.stream = new FileInputStream(file);
    }

    public StreamResult(File file, String contentType, String filename, Type type) throws FileNotFoundException {
        this.file = file;
        this.contentType = contentType;
        this.filename = filename;
        this.type = type;
        this.stream = new FileInputStream(file);
    }

    public StreamResult(InputStream stream, String contentType, String filename, Type type) throws FileNotFoundException {
        this.contentType = contentType;
        this.filename = filename;
        this.type = type;
        this.stream = stream;
    }

    public void close() {

        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (file != null && deleteOnExit) {
            file.delete();
        }
    }

    public final void deleteOnExit() {
        this.deleteOnExit = true;
    }

    public String getContentType() {
        return contentType;
    }

    public String getFilename() {
        return filename;
    }

    public InputStream getStream() {
        return stream;
    }

    public Type getType() {
        return type;
    }

}