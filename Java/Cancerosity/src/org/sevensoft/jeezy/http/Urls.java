package org.sevensoft.jeezy.http;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.sevensoft.jeezy.db.EntityObject;

/**
 * @author sks 14 Aug 2006 06:51:35
 *
 */
public class Urls {

	public static URL build(HttpServletRequest request) throws MalformedURLException {

		String url = request.getRequestURL().toString();
		if (request.getQueryString() != null) {
			url = url + "?" + request.getQueryString();
		}

		return new URL(url);
	}

	/**
	 * Creates a new url from this href and params map
	 * encodes all params in UTF-8
	 */
	public static String buildUrl(String href, Map params) {

		StringBuilder sb = new StringBuilder(href);
		if (params == null)
			return sb.toString();

		Iterator<Map.Entry> iter = params.entrySet().iterator();
		if (iter.hasNext()) {
			if (href.contains("?")) {
				sb.append("&");
			} else {
				sb.append("?");
			}
		}

		while (iter.hasNext()) {

			Map.Entry entry = iter.next();

			Object obj1 = entry.getKey();
			Object obj2 = entry.getValue();

			String key = EntityObject.getEqualityString(obj1);
			String value = EntityObject.getEqualityString(obj2);

			try {

				sb.append(URLEncoder.encode(key, "UTF-8"));
				sb.append("=");
				sb.append(URLEncoder.encode(value, "UTF-8"));

			} catch (UnsupportedEncodingException e) {
				throw new RuntimeException(e);
			}

			if (iter.hasNext()) {
				sb.append("&");
			}
		}

		return sb.toString();
	}

	/**
	 * Takes this url and returns the params from the query string, decoding from UTF-8 
	 */
	public static Map<String, String> getParams(String url) {

		if (url == null) {
			return new HashMap();
		}

		// remove up to and including ? if it contains one
		if (url.contains("?")) {
			url = url.replaceFirst(".*?\\?", "");
		}

        Map map = new HashMap();

        String split = url.indexOf("&amp;") != -1 ? "&amp;" : "&";

		for (String param : url.split(split)) {

			int index = param.indexOf('=');
			if (index > 0 && (param.length() + 1) > index) {

				try {

					String key = URLDecoder.decode(param.substring(0, index), "UTF-8");
					String value = URLDecoder.decode(param.substring(index + 1), "UTF-8");

					map.put(key, value);

				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
		}

		return map;
	}

	public static Map<String, String> getParams(URL url) {
		return getParams(url.getQuery());
	}

	public static void main(String[] args) {

		System.out.println(getParams(("http://www.go-chesterfield.co.uk/s.do?itemType=2&attributeValues_48=12-04-2007")));

	}
}
