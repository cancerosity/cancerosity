package org.sevensoft.jeezy.http.util;

/**
 * @author sks 31 Oct 2006 10:18:17
 *
 */
public interface Renderer<E> {

	/**
	 * 
	 */
	Object render(E obj);

}
