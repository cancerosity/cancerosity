package org.sevensoft.jeezy.http.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 19 Jul 2006 09:56:47
 *
 */
public class Keypad {

	private int				tds;
	private String			tableClass;
	private String			caption;
	/**
	 * Pad empty cells with the padding string (&nbsp; by default)
	 */
	private boolean			padEmpty;
	private String			cellAlign;
	private String			cellVAlign;
	private String			tableId;
	private String			tableAlign;
	private int				cycleRows;
	private Renderer			renderer;
	private final List<Object[]>	objs;

	/**
	 * Wrap the keypad cells in <table> </table>
	 */
	private boolean			tableDeclaration;

	/**
	 * If the columns is greater than the number of objects, then reduce columns 
	 */
	private boolean			normaliseColumns;

	/**
	 * Cellpadding to set on the table declaration 
	 */
	private int				cellpadding;

	/**
	 * cellspacing to set on the table declaration 
	 */
	private int				cellspacing;

	private String			defaultClass;
	private int				startCol;

	public Keypad(int tds) {
		this.tds = tds;
		this.tableDeclaration = true;
		this.normaliseColumns = true;
		this.objs = new ArrayList();
	}

	public void addObject(Object obj) {
		this.objs.add(new Object[] { obj, null });
	}

	public void addObject(Object obj, String className) {
		this.objs.add(new Object[] { obj, className });
	}

	public String getDefaultClass() {
		return defaultClass;
	}

	private TableTag getTableTag() {

		TableTag tableTag;

		if (tableClass == null) {
			tableTag = new TableTag();
		} else {
			tableTag = new TableTag(tableClass, cellspacing, cellpadding);
		}

		if (tableAlign != null) {
			tableTag.setAlign(tableAlign);
		}

		if (caption != null) {
			tableTag.setCaption(caption);
		}

		if (tableId != null) {
			tableTag.setId(tableId);
		}

		return tableTag;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public void setCellAlign(String cellAlign) {
		this.cellAlign = cellAlign;
	}

	public void setCellspacing(int i) {
		cellspacing = i;
	}

	public void setCellVAlign(String cellVAlign) {
		this.cellVAlign = cellVAlign;
	}

	public void setCycleRows(int cycleRows) {
		this.cycleRows = cycleRows;
	}

	public void setDefaultClass(String defaultClass) {
		this.defaultClass = defaultClass;
	}

	public void setNormaliseColumns(boolean normaliseColumns) {
		this.normaliseColumns = normaliseColumns;
	}

	public void setObjects(Collection toAdd) {
		for (Object obj : toAdd) {
			addObject(obj);
		}
	}

	public void setPadEmpty(boolean padEmpty) {
		this.padEmpty = padEmpty;
	}

	public void setRenderer(Renderer renderer) {
		this.renderer = renderer;
	}

	/**
	 * 
	 */
	public void setStartCol(int startCol) {
		this.startCol = startCol;
	}

	public void setTableAlign(String tableAlign) {
		this.tableAlign = tableAlign;
	}

	public void setTableClass(String tableClass) {
		this.tableClass = tableClass;
	}

	public void setTableDeclaration(boolean b) {
		this.tableDeclaration = b;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		if (objs == null) {
			return null;
		}

		if (cycleRows == 0) {
			cycleRows = 1;
		}

		// if the number of tds is actually less than the number of items, then lets move tds down - no point artifically padding !
		if (tds > objs.size() && normaliseColumns) {
			tds = objs.size();
		}

		if (tds == 0) {
			tds = 1;
		}
		int width = 100 / tds;

		if (tableDeclaration) {
			sb.append(getTableTag());
		}

		int row = 0;
		Iterator<Object[]> iter = objs.iterator();
		while (iter.hasNext()) {

			sb.append("<tr class='row" + (row % cycleRows) + "'");
			if (row == 0) {
				sb.append(" id=\"first\"");
			} else if (!iter.hasNext()) {
				sb.append(" id=\"last\"");
			}
			sb.append(">\n");

			for (int n = 1; n <= tds; n++) {

				Object content;
				Object className;

				// check for padding initial
				if (row == 0 && n < startCol) {

					content = "&nbsp;";
					className = defaultClass;

				} else if (iter.hasNext()) {

					Object[] next = iter.next();

					Object obj = next[0];
					className = next[1];

					if (renderer == null) {
						content = obj;

					} else {
						content = renderer.render(obj);
					}

				} else if (padEmpty) {

					content = "&nbsp;";
					className = defaultClass;

				} else {

					content = null;
					className = defaultClass;
				}

				sb.append("<td width='" + width + "%' ");
				if (className == null) {
					sb.append(" class='cell" + n + "'");
				} else {
					sb.append(" class='" + className + "'");
				}
				if (cellVAlign != null) {
					sb.append(" valign='" + cellVAlign + "'");
				}

				if (cellAlign != null) {
					sb.append(" align='" + cellAlign + "'");
				}

				sb.append(">");
				if (content == null) {
					content = "";
				}
				sb.append(content);
				sb.append("</td>\n");
			}

			sb.append("</tr>\n");
			row++;
		}

		if (tableDeclaration) {
			sb.append("</table>\n\n");
		}

		return sb.toString();
	}
}
