package org.sevensoft.jeezy.http.util;

/**
 * @author sks 03-Sep-2005 11:19:54
 * 
 */
public interface Selectable {

	public String getLabel();

	public String getValue();
}
