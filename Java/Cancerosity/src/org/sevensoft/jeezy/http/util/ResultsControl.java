package org.sevensoft.jeezy.http.util;

import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 4 Oct 2006 08:46:16
 *
 */
public class ResultsControl {

	private final Link		link;
	private final Results		results;
	private final RequestContext	context;

	public ResultsControl(RequestContext context, Results results, Link link) {
		this.context = context;
		this.results = results;
		this.link = link;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder("<table class='ec_page_control'><tr>");
		sb.append("<td class='results'>You are viewing results <span>" + results.getStartResult() + "</span> to <span>" + results.getEndResult()
				+ "</span> of <span>" + results.getTotalResults() + "</span></td>");

		if (results.hasMultiplePages()) {

			sb.append("<td class='pages'>Pages: ");

			// show first 3 to up results - 2
			int n = 1;
			for (; n <= 3 && n < results.getCurrentPage() - 2; n++) {
				sb.append(results.getLink(link, n));
				sb.append(", ");
			}

			// only show ellipses if there are more than 5 results before the current page
			if (results.getCurrentPage() - 1 > 5) {
				sb.append(" ... ");
			}

			// show 2 before current page
			n = results.getCurrentPage() - 2;
			if (n < 1)
				n = 1;
			for (; n < results.getCurrentPage(); n++) {
				sb.append(results.getLink(link, n));
				sb.append(", ");
			}

			// show current page
			sb.append(results.getCurrentPage());
			if (results.hasNextPage()) {
				sb.append(", ");
			}

			// show 2 after current page up to last but 3
			n = results.getCurrentPage() + 1;
			for (; n <= results.getCurrentPage() + 2 && n < results.getLastPage() - 2; n++) {
				sb.append(results.getLink(link, n));
				if (n < results.getLastPage()) {
					sb.append(", ");
				}
			}

			// show ellipses if we have more than 5 results to come
			if (results.getLastPage() - results.getCurrentPage() > 5) {
				sb.append(" ... ");
			}

			// show last 3
			n = results.getLastPage() - 2;
			if (n <= results.getCurrentPage()) {
				n = results.getCurrentPage() + 1;
			}

			for (; n <= results.getLastPage(); n++) {
				sb.append(results.getLink(link, n));

				// show trailing comman if this is not the last page
				if (n < results.getLastPage()) {
					sb.append(", ");
				}

			}

			sb.append(" &nbsp; ");

			// show previous page if this is not first page
			if (results.hasPreviousPage()) {
				sb.append(results.getPreviousLink(link, "Previous"));
			} else {
				sb.append("<span class='disabled'>Previous</span>");
			}

			sb.append(" | ");

			// show next page if this is not last page
			if (results.hasNextPage()) {
				sb.append(results.getNextLink(link, "Next"));
			} else {
				sb.append("<span class='disabled'>Next</span>");
			}

			sb.append("</td>");
		}

		sb.append("</tr></table>");
		return sb.toString();
	}
}
