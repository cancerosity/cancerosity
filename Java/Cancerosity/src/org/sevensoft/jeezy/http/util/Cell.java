package org.sevensoft.jeezy.http.util;

/**
 * @author sks 1 Mar 2007 17:24:17
 *
 */
public interface Cell {

	public Object getCellRenderer();

	public int getCellSpan();

	public int getGridColumns();

	public String getSection();

}
