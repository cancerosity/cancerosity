package org.sevensoft.jeezy.http.util;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Path;
import org.sevensoft.jeezy.http.Urls;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author sks 20-Feb-2006 21:21:00
 */
public class Link {

    private MultiValueMap<String, String> params;
    private String href;

    public Link(Class<? extends Handler> clazz) {
        this(clazz, (String) null, (Object[]) null);
    }

    public Link(Class<? extends Handler> clazz, Object... array) {
        this(clazz, null, array);
    }

    public Link(Class<? extends Handler> clazz, String action, Object... array) {

        this.href = clazz.getAnnotation(Path.class).value()[0];
        this.params = new MultiValueMap(new LinkedHashMap());

        if (action != null) {
            addParameter("_a", action);
        }

        if (array != null) {
            setParameters(array);
        }
    }

    public Link(String url) {
        this.href = url;
        this.params = new MultiValueMap(new LinkedHashMap());
    }

    public void addParameter(String name, Object value) {
        if (value != null) {
            params.put(name, EntityObject.getEqualityString(value));
        }
    }

    public void addParameters(String name, Map map) {

        if (!name.endsWith("_")) {
            name = name + "_";
        }

        Set<Map.Entry> set = map.entrySet();
        for (Map.Entry entry : set) {
            addParameter(name + EntityObject.getEqualityString(entry.getKey()), entry.getValue());
        }
    }

    public MultiValueMap<String, String> getParameters() {
        return params;
    }

    protected String getUrl() {
        return Urls.buildUrl(href, params);
    }

    /**
     * Removes all values for this parameter
     */
    public Link removeParameters(String key) {
        params.removeAll(key);
        return this;
    }

    /**
     * Set parameter should replace existing parameters
     */
    public void setParameter(String key, Object obj) {
        params.remove(key);
        addParameter(key, obj);
    }

    private void setParameters(Object... array) {

        if (array.length % 2 == 1) {
            throw new RuntimeException("Unequal number of params: " + array);
        }

        for (int n = 0; n < array.length; n = n + 2) {
            if (array[n] != null && array[n + 1] != null) {
                setParameter(array[n].toString(), array[n + 1]);
            }
        }
    }

    public void setParameters(String key, Map map) {
        removeParameters(key);
        addParameters(key, map);
    }

    public String toString() {
        return getUrl();
    }

    public void addParameters(MultiValueMap<String, String> parameters) {
        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            addParameter(entry.getKey(), entry.getValue());
        }
    }

    public String getHref() {
        return href;
    }

}
