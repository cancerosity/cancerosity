package org.sevensoft.jeezy.http.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.jeezy.http.ObjectUtil;
import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 12 Mar 2007 17:06:08
 *
 */
public class Grid {

	private class CellContainer {

		private final int		span;
		private final Object	data;

		public CellContainer(Object data, int span) {
			this.data = data;
			this.span = span;
		}

		public final Object getData() {
			return data;
		}

		public final int getSpan() {
			return span;
		}
	}

	private static Logger	logger	= Logger.getLogger("ecreator");

	public static <E extends Cell> List<E> getCells(List<E> cells, final String section) {
		List<E> copy = new ArrayList(cells);
		CollectionsUtil.filter(copy, new Predicate<E>() {

			public boolean accept(Cell e) {
				return ObjectUtil.equal(e.getSection(), section);
			}
		});
		return copy;
	}

	public static List<String> getSections(List<? extends Cell> cells) {
		List<String> sections = new ArrayList();
		for (Cell cell : cells) {
			if (!sections.contains(cell.getSection())) {
				sections.add(cell.getSection());
			}
		}
		return sections;
	}

	private List<CellContainer>	cells;
	private int				columns;
	private String			tableClass;
	private String			caption;
	private boolean			padEmpty;
	private String			cellAlign;
	private String			cellVAlign;
	private String			tableId;
	private String			tableAlign;
	private int				cycleRows;
	private boolean			tableDeclaration;
	private boolean			normaliseColumns;
	private int				cellpadding;
	private int				cellspacing;
	private String			defaultClass;
    private String trIdFirst;
    private String trIdLast;

	public Grid(int columns) {
		this.columns = columns;
		this.tableDeclaration = true;
		this.normaliseColumns = true;
		this.cells = new ArrayList();
	}

	public void addCell(Object data, int span) {
		if (span < 1) {
			span = 1;
		}
		this.cells.add(new CellContainer(data, span));
	}

	public void addCells(List<? extends Cell> cells) {
		for (Cell cell : cells) {
			logger.fine("[Grid] adding cell, span=" + cell.getCellSpan());
			addCell(cell.getCellRenderer(), cell.getCellSpan());
		}
	}

	private TableTag getTableTag() {

		TableTag tableTag;

		if (tableClass == null) {
			tableTag = new TableTag();
		} else {
			tableTag = new TableTag(tableClass, cellspacing, cellpadding);
		}

		if (tableAlign != null) {
			tableTag.setAlign(tableAlign);
		}

		if (caption != null) {
			tableTag.setCaption(caption);
		}

		if (tableId != null) {
			tableTag.setId(tableId);
		}

		return tableTag;
	}

	public final void setCaption(String caption) {
		this.caption = caption;
	}

	public final void setCellAlign(String cellAlign) {
		this.cellAlign = cellAlign;
	}

	public final void setCellpadding(int cellpadding) {
		this.cellpadding = cellpadding;
	}

	public final void setCellspacing(int cellspacing) {
		this.cellspacing = cellspacing;
	}

	public final void setCellVAlign(String cellVAlign) {
		this.cellVAlign = cellVAlign;
	}

	public final void setTableAlign(String tableAlign) {
		this.tableAlign = tableAlign;
	}

	public final void setTableClass(String tableClass) {
		this.tableClass = tableClass;
	}

	public final void setTableDeclaration(boolean tableDeclaration) {
		this.tableDeclaration = tableDeclaration;
	}

	public final void setTableId(String tableId) {
		this.tableId = tableId;
	}

    public final void setTrIdFirst(String trIdFirst) {
        this.trIdFirst = trIdFirst;
    }

    public final void setTrIdLast(String trIdLast) {
        this.trIdLast = trIdLast;
    }

    @Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		if (cells.isEmpty()) {
			return null;
		}

		if (cycleRows == 0) {
			cycleRows = 1;
		}

		// if the number of tds is actually more than the number of items, then lets move tds down - no point artifically padding !
		if (columns > cells.size() && normaliseColumns) {
			columns = cells.size();
		}

		if (columns == 0) {
			columns = 1;
		}
		int width = 100 / columns;

        if (trIdFirst == null) {
            trIdFirst = "first";
        }

        if (trIdLast == null) {
            trIdFirst = "last";
        }

		if (tableDeclaration) {
			sb.append(getTableTag());
		}

		int row = 0;
		Iterator<CellContainer> iter = cells.iterator();
		while (iter.hasNext()) {

			sb.append("<tr class='row" + (row % cycleRows) + "'");
            if (row == 0) {
                sb.append(" id='" + trIdFirst + "'");
            } else if (!iter.hasNext()) {
                sb.append(" id='" + trIdLast + "'");
            }
			sb.append(">\n");

			for (int n = 1; n <= columns;) {

				Object content;
				Object className = null;
				int span;

				if (iter.hasNext()) {

					CellContainer cell = iter.next();
					content = cell.getData();
					span = cell.getSpan();

				} else if (padEmpty) {

					content = "&nbsp;";
					className = defaultClass;
					span = 1;

				} else {

					content = null;
					className = defaultClass;
					span = 1;
				}

				sb.append("<td width='" + width + "%' ");
				if (className == null) {
					sb.append(" class='cell" + n + "'");
				} else {
					sb.append(" class='" + className + "'");
				}

				if (span > 1) {
					sb.append(" colspan='" + span + "'");
				}

				if (cellVAlign != null) {
					sb.append(" valign='" + cellVAlign + "'");
				}

				if (cellAlign != null) {
					sb.append(" align='" + cellAlign + "'");
				}

				sb.append(">");
				if (content == null) {
					content = "";
				}
				sb.append(content);
				sb.append("</td>\n");

				n = n + span;
			}

			sb.append("</tr>\n");
			row++;
		}

		if (tableDeclaration) {
			sb.append("</table>\n\n");
		}

		return sb.toString();
	}
}
