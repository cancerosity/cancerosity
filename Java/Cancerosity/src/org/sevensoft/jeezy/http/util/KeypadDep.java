package org.sevensoft.jeezy.http.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.sevensoft.jeezy.http.html.table.TableTag;

/**
 * @author sks 19 Jul 2006 09:56:47
 *
 */
public class KeypadDep {

	private int				tds;
	private Collection<String>	cells;
	private String			tableClass;
	private String			caption;
	private boolean			padEmpty;
	private String			cellAlign;
	private String			cellVAlign;
	private String			tableId;
	private String			tableAlign;
	private int				cycleRows;
	private Renderer			renderer;
	private Collection		objs;

	public KeypadDep() {
		this.cells = new ArrayList<String>();
	}

	public KeypadDep(Collection<String> cells, int tds) {
		this.cells = cells;
		this.tds = tds;
		this.cellAlign = "center";
		this.cellVAlign = "top";
		this.cycleRows = 1;
	}

	public void addCell(Object obj) {
		cells.add(obj.toString());
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public void setCellAlign(String cellAlign) {
		this.cellAlign = cellAlign;
	}

	public void setCells(Collection<String> cells) {
		this.cells = cells;
	}

	public void setCellVAlign(String cellVAlign) {
		this.cellVAlign = cellVAlign;
	}

	public void setCycleRows(int cycleRows) {
		this.cycleRows = cycleRows;
	}

	public void setPadEmpty(boolean padEmpty) {
		this.padEmpty = padEmpty;
	}

	/**
	 * 
	 */
	public void setRenderer(Renderer renderer) {
		this.renderer = renderer;
	}

	public void setTableAlign(String tableAlign) {
		this.tableAlign = tableAlign;
	}

	public void setTableClass(String tableClass) {
		this.tableClass = tableClass;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}

	public void setTds(int i) {
		tds = i;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		if (tds == 0)
			tds = 1;

		if (cycleRows == 0)
			cycleRows = 1;

		int width = 100 / tds;

		if (tableClass != null) {

			final TableTag tableTag = new TableTag(tableClass);

			if (tableAlign != null)
				tableTag.setAlign(tableAlign);

			if (caption != null)
				tableTag.setCaption(caption);

			if (tableId != null)
				tableTag.setId(tableId);

			sb.append(tableTag);
		}

		int row = 0;
		Iterator<String> iter = cells.iterator();
		while (iter.hasNext()) {

			sb.append("<tr class='row" + (row % cycleRows) + "'");
			if (row == 0) {
				sb.append(" id=\"first\"");
			} else if (!iter.hasNext()) {
				sb.append(" id=\"last\"");
			}
			sb.append(">");

			for (int n = 1; n <= tds; n++) {

				sb.append("<td valign='" + cellVAlign + "' align='" + cellAlign + "' width='" + width + "%' class='cell" + n + "'>");

				if (iter.hasNext()) {

					sb.append(iter.next());

				} else if (padEmpty) {

					sb.append("&nbsp;");
				}

				sb.append("</td>");
			}

			sb.append("</tr>");
			row++;
		}

		if (tableClass != null) {
			sb.append("</table>");
		}

		return sb.toString();
	}
}
