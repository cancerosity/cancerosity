package org.sevensoft.jeezy.http.util;

import java.util.Collection;
import java.util.List;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.links.LinkTag;

/**
 * @author sks 18-Dec-2004 10:30:56
 */
public class Results {

	private int			resultsPerPage, currentPage, totalResults;

	/**
	 * Total number of pages 
	 */
	private int			pages;

	private RequestContext	context;

	public Results(Collection c, int currentPage, int rpp) {
		this(c.size(), currentPage, rpp);
	}

	public Results(int totalResults, int currentPage, int resultsPerPage) {

		this.totalResults = totalResults;
		this.resultsPerPage = resultsPerPage;

		// calculate total number of pages needed to show all results
		this.pages = totalResults / resultsPerPage;
		if (totalResults % resultsPerPage > 0) {
			this.pages++;
		}

		//		if (pages == 0)
		//			pages = 1;

		// our current page has to be between 1 and the last page
		if (currentPage < 1) {
			this.currentPage = 1;
		}

		else if (currentPage > pages) {
			this.currentPage = pages;
		}

		else {
			this.currentPage = currentPage;
		}
	}

	public Results(Object[] array, int page, int resultsPerPage) {
		this(array.length, page, resultsPerPage);
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public int getEndIndex() {
		int i = currentPage * resultsPerPage;
		return i < totalResults ? i : totalResults;
	}

	/**
	 * Returns the last search results number for this page. Ie, if this is page 3 and there are 800 records, and 30 records per page, this will return 90.
	 * 
	 * @return
	 */
	public int getEndResult() {
		int i = currentPage * resultsPerPage;
		return i < totalResults ? i : totalResults;
	}

	public LinkTag getFirstLink(Link link, String text) {

		link.setParameter("page", 1);
		return new LinkTag(link, text);

	}

	public LinkTag getLastLink(Link link, String text) {

		link.setParameter("page", getLastPage());
		return new LinkTag(link, text);

	}

	public int getLastPage() {
		return pages;
	}

	/**
	 * Returns a link to the page supplied by the page param
	 */
	public LinkTag getLink(Link link, int page) {

		link.setParameter("page", page);
		return new LinkTag(link, String.valueOf(page));
	}

	public LinkTag getNextLink(Link link, String text) {

		link.setParameter("page", getNextPage());
		return new LinkTag(link, text);

	}

	public int getNextPage() {
		return currentPage == pages ? currentPage : currentPage + 1;
	}

	/**
	 * Returns number of results on this page
	 */
	public int getPageResults() {
		return getEndResult() - getStartIndex();
	}

	public LinkTag getPreviousLink(Link link, String text) {

		link.setParameter("page", getPreviousPage());
		return new LinkTag(link, text);

	}

	public int getPreviousPage() {
		return currentPage > 1 ? currentPage - 1 : 1;
	}

	public int getResultsPerPage() {
		return resultsPerPage;
	}

	/**
	 * Returns the start index for use by searching
	 */
	public int getStartIndex() {
		return (currentPage - 1) * resultsPerPage;
	}

	public int getStartResult() {
		return ((currentPage - 1) * resultsPerPage) + 1;
	}

	/**
	 * Returns the total number of results.
	 * 
	 */
	public int getTotalResults() {
		return totalResults;
	}

	public boolean hasMultiplePages() {
		return pages > 1;
	}

	public boolean hasNextPage() {
		return pages > currentPage;
	}

	public boolean hasPreviousPage() {
		return currentPage > 1;
	}

	//	public String html(LinkTag linkTag, int distance) {
	//
	//		List<String> list = new ArrayList<String>();
	//
	//		int start = (currentPage - distance);
	//		if (start < 1)
	//			start = 1;
	//		int end = (currentPage + distance);
	//		if (end > pages)
	//			end = pages;
	//
	//		linkTag.setParameter("page", 1);
	//		linkTag.setLabel("first");
	//		list.add(linkTag.toString());
	//
	//		for (int n = start; n <= end; n++) {
	//
	//			linkTag.setParameter("page", n);
	//			linkTag.setLabel(n);
	//
	//			if (n == currentPage)
	//				linkTag.setClass("active");
	//			else
	//				linkTag.setClass(null);
	//
	//			list.add(linkTag.toString());
	//		}
	//
	//		linkTag.setClass(null);
	//		linkTag.setParameter("page", getLastPage());
	//		linkTag.setLabel("last");
	//		list.add(linkTag.toString());
	//
	//		return StringHelper.implode(list, "", true);
	//	}

	public boolean isFirstPage() {
		return currentPage == 1;
	}

	public boolean isLastPage() {
		return currentPage == getLastPage();
	}

	public boolean isSinglePage() {
		return pages == 1;
	}

	public int normalisePage(int page) {

		if (page < 1) {
			page = 1;
		}

		if (page > pages && pages > 0) {
			page = pages;
		}

		return page;
	}

	public <E extends Object> List<E> subList(List<E> list) {

		if (list.size() <= resultsPerPage)
			return list;

		return list.subList(getStartIndex(), getEndIndex());
	}

	@Override
	public String toString() {
		return "totalResults=" + totalResults + "; currentpage=" + currentPage + "; lastpage=" + pages;
	}

	/**
	 * 
	 */
	public int size() {
		return getTotalResults();
	}

}
