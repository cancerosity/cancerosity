package org.sevensoft.jeezy.http.util;

/**
 * @author sks 7 Jun 2006 15:44:12
 *
 */
public class Ajax {

	public static StringBuilder ajax() {

		StringBuilder sb = new StringBuilder();

		sb.append("<script language='javascript' type='text/javascript'>\n");

		sb.append("function createRequestObject() {");
		sb.append("	var ro;");
		sb.append("	if (window.XMLHttpRequest) { ");
		sb.append("		ro = new XMLHttpRequest();");
		sb.append("	} else if (window.ActiveXObject) {");
		sb.append("		try {");
		sb.append("			ro = new ActiveXObject('Msxml2.XMLHTTP'); ");
		sb.append("		} catch (e) {");
		sb.append("			try {");
		sb.append("				ro = new ActiveXObject('Microsoft.XMLHTTP'); ");
		sb.append("			} catch (e) {}");
		sb.append("		}");
		sb.append("	}");
		sb.append("	if (!ro) {");
		sb.append("		alert('Giving up :( Cannot create an XMLHTTP instance'); ");
		sb.append("	}");
		sb.append("	return ro; ");
		sb.append("}");

		sb.append("var http_request = createRequestObject(); ");
		sb.append("var e; ");

		sb.append("function ajaxRequest(url, elementId) {");
		sb.append("	e = elementId; ");
		sb.append("	http_request.open('GET', url, true);");
		sb.append("	http_request.onreadystatechange = ajaxResponse; ");
		sb.append("	http_request.send(null);");
		sb.append("}");

		/*
		 * Ajax response javascript function.
		 * This function will be called and will replace the HTML 
		 */
		sb.append("function ajaxResponse() {");
		sb.append("		if (http_request.readyState == 4) {");
		//		sb.append("			window.alert(http_request.responseText); ");
		//		sb.append("			window.alert(e); ");
		//		sb.append("			e.innerHTML = http_request.responseText; ");
		sb.append("			document.getElementById(e).innerHTML = http_request.responseText; ");
		sb.append("		}");
		sb.append("	} ");

		sb.append("</script>\n\n\n");

		return sb;
	}
}
