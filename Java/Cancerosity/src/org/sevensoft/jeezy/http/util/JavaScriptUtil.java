package org.sevensoft.jeezy.http.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * 
 * @author Bing
 *
 */
public class JavaScriptUtil {

	 
	/**
	 * 
	 * @param cal
	 * @param name
	 * @return
	 */
	public static String loadJsFile(Class cal,String name){
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader( cal.getResourceAsStream(name)));
			StringBuilder sb = new StringBuilder("\n");
			String line = reader.readLine();
			while(line!=null){
				sb.append(line+"\n");
				line = reader.readLine();
			}
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return "";
	}

}
