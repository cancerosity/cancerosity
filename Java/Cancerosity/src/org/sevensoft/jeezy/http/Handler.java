package org.sevensoft.jeezy.http;

import org.sevensoft.commons.collections.maps.LinkedMultiMap;
import org.sevensoft.commons.collections.maps.MultiHashMap;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.samdate.Period;
import org.sevensoft.commons.samdate.Time;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.Validator;
import org.sevensoft.ecreator.model.crm.forms.FieldType;
import org.sevensoft.ecreator.model.crm.forms.FormField;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.http.util.Results;
import org.sevensoft.jeezy.http.wrappers.*;

import javax.servlet.ServletException;
import java.io.File;
import java.lang.reflect.*;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Logger;

/**
 * This class is the base class for all handlers for web site interaction.
 *
 * @author Sam 14-Jul-2003 15:43:39
 */
public abstract class Handler {

    private static Logger logger = Logger.getLogger("jeezy");

    public static String getPath(Class<? extends Handler> clazz) {

        Path path = clazz.getAnnotation(Path.class);
        if (path == null) {
            throw new RuntimeException(clazz + " does not have a path set");
        }

        return path.value()[0];
    }

    protected RequestContext context;

    public Handler(RequestContext context) {
        this.context = context;
    }

    protected final void addError(Exception e) {
        context.addError(e);
    }

    protected final void addError(String message) {
        context.addError(message);
    }

    protected final void addHeader(String key, String value) {
        context.addHeader(key, value);
    }

    protected final void addMessage(String message) {
        context.addMessage(message);
    }

    protected final void addPopup(String message) {
        context.addPopup(message);
    }

    protected final void addWarning(String message) {
        context.addWarning(message);
    }

    protected final void clearErrors() {
        context.clearErrors();
    }

    protected final void clearMessages() {
        context.clearMessages();
    }

    protected final void clearParameters() {
        context.clearParameters();
    }

    public boolean debug() {
        return false;
    }

    protected final void doPages(int total, int page, int limit) {
        Results pages = new Results(total, page, limit);
        if (pages.getLastPage() > 1)
            setAttribute("pages", pages);
    }

    protected final Object getAttribute(String key) {
        return context.getAttribute(key);
    }

    private List<Field> getFields() {

        // List<Field> list = fields.get(getClass());
        // if (list == null) {

        List<Field> list = new ArrayList<Field>();

        Class clazz = getClass();
        while (!Handler.class.equals(clazz) && Handler.class.isAssignableFrom(clazz)) {

            Field[] fields = clazz.getDeclaredFields();
            AccessibleObject.setAccessible(fields, true);

            for (Field field : fields) {

                int modifiers = field.getModifiers();
                if (!Modifier.isStatic(modifiers) && !Modifier.isTransient(modifiers))
                    list.add(field);
            }

            clazz = clazz.getSuperclass();
        }

        // fields.put(getClass(), list);
        // }

        return list;

    }

    protected String getGetRequest() {
        return context.getGetRequest();
    }

    protected String getHeader(String name) {
        return context.getHeader(name);
    }

    protected String getParameter(String name) {
        return context.getParameter(name);
    }

    protected MultiValueMap<String, String> getParameters() {
        return context.getParameters();
    }

    protected String[] getParameters(String name) {
        return context.getParameterValues(name);
    }

    protected File getRealFile(String string) {
        return context.getRealFile(string);
    }


    protected final String getRealPath(String file) {
        return context.getRealPath(file);
    }

    protected final String getRemoteHostname() {
        return context.getRemoteHostname();
    }

    protected final String getRemoteIp() {
        return context.getRemoteIp();
    }

    public String getRequestUri() {
        return context.getRequestURI();
    }

    private Upload getUpload(String paramName, String paramValue) {

        if (paramValue == null || paramValue.length() == 0)
            return null;

        File file = new File(paramValue);
        if (!file.exists() || file.length() < 100)
            return null;

        String[] contentTypes = context.getParameterValues(paramName + ".content-type");
        String[] filenames = context.getParameterValues(paramName + ".filename");

        for (int n = 0; n < filenames.length; n++) {
            if (filenames[n] != null && filenames[n].length() > 0) {

                String contentType = contentTypes[n];
                String filename = filenames[n];

                return new Upload(paramValue, filename, contentType);
            }
        }
        return null;
    }

    protected String getUserAgent() {
        return context.getUserAgent();
    }

    protected boolean hasError(String key) {
        return context.getError(key) != null;
    }

    protected final boolean hasErrors() {
        return context.hasErrors();
    }

    protected abstract Object init() throws ServletException;

    protected final boolean isValidated() {
        return context.isValidated();
    }

    public abstract Object main() throws ServletException;

    protected void populate() throws IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchFieldException, ClassNotFoundException,
            InstantiationException {

        for (Field field : getFields()) {

            Class type = field.getType();
            logger.finest("populating field=" + field);

            if (type.isArray()) {

                populateArray(field, type);

            } else {

                if (Collection.class.isAssignableFrom(type)) {
                    populateCollection(field);

                } else if (Date.class.equals(type)) {
                    populateDate(field);

                } else if (DateTime.class.equals(type)) {
                    populateDateTime(field);

                } else if (EntityObject.class.isAssignableFrom(type)) {
                    populateEntityObject(field, type);

                } else if (Class.class.equals(type)) {
                    populateClass(field);

                } else if (Map.class.isAssignableFrom(type)) {
                    populateMap(field);

                } else if (Period.class.equals(type)) {
                    populatePeriod(field);

                } else if (Amount.class.equals(type)) {
                    populateAmount(field);

                } else if (Money.class.equals(type)) {
                    populateMoney(field);

                } else if (Upload.class.equals(type)) {
                    populateUpload(field);

                } else if (String.class.equals(type)) {
                    populateString(field);

                } else if (Enum.class.isAssignableFrom(type)) {
                    populateEnum(field);

                } else if (String.class.equals(type)) {
                    populateString(field);

                } else if (Country.class.equals(type)) {
                    populateCountry(field);

                } else if (String.class.equals(type)) {
                    populateString(field);

                } else if (type.isPrimitive()) {
                    populatePrimitive(field, type);

                } else if (Modifier.isAbstract(type.getModifiers())) {
                    // if we have an abstract object then the parameter should be the class name to instantiate
                    populateAbstract(field);
                }

            }
        }
    }

    private void populateAbstract(Field field) {

        logger.finest("populating abstract field: " + field);
        String className = context.getParameter(field.getName());
        if (className == null || className.length() == 0)
            return;

        logger.fine("abstract field className=" + className);

        try {

            Object obj = Class.forName(className).newInstance();
            if (obj == null)
                return;

            field.set(this, obj);

        } catch (InstantiationException e) {
            e.printStackTrace();

        } catch (IllegalAccessException e) {
            e.printStackTrace();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void populateAmount(Field field) throws IllegalArgumentException, IllegalAccessException {

        String value = getParameter(field.getName());
        if (value != null && value.length() > 0) {
            field.set(this, Amount.parse(value));
        }
    }

    final void populateArray(Field field, Class clazz) throws IllegalArgumentException, IllegalAccessException {

        if (debug()) {
            logger.fine("populating array");
        }

        String[] params = context.getParameterValues(field.getName());
        if (params == null)
            return;

        if (debug()) {
            logger.fine("params[]=" + params);
        }

        Class componentType = clazz.getComponentType();

        if (debug()) {
            logger.fine("componentType=" + componentType);
        }

        if (EntityObject.class.isAssignableFrom(componentType)) {

            EntityObject[] values = (EntityObject[]) Array.newInstance(componentType, params.length);

            for (int n = 0; n < params.length; n++) {
                values[n] = EntityObject.getInstance(context, componentType, params[n]);
            }

            logger.fine("Entity Objects=" + values);

            field.set(this, values);

        } else if (String.class.equals(componentType)) {

            logger.fine("Strings=" + params);

            field.set(this, params);

        } else if (Money.class.equals(componentType)) {

            Money[] moneys = new Money[params.length];
            for (int m = 0; m < moneys.length; m++) {
                moneys[m] = new Money(params[m]);
            }

            logger.fine("moneys=" + moneys);

            field.set(this, moneys);

        } else if (Double.TYPE.equals(componentType)) {

            double[] doubles = new double[params.length];
            for (int m = 0; m < doubles.length; m++) {
                doubles[m] = Double.parseDouble(params[m]);
            }

            logger.fine("doubles=" + doubles);

            field.set(this, doubles);

        } else if (Long.TYPE.equals(componentType)) {

            long[] longs = new long[params.length];
            for (int m = 0; m < longs.length; m++) {
                longs[m] = Long.parseLong(params[m]);
            }

            logger.fine("longs=" + longs);

            field.set(this, longs);

        }
    }

    /**
     *
     */
    private void populateClass(Field field) {

        String value = getParameter(field.getName());
        if (value != null && value.length() > 0)
            try {
                field.set(this, Class.forName(value));
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    final void populateCollection(Field field) throws IllegalArgumentException, IllegalAccessException {

        String name = field.getName();

        String[] values = context.getParameterValues(name);
        if (values == null || values.length == 0) {

            if (List.class.isAssignableFrom(field.getType())) {
                field.set(this, new ArrayList());

            } else if (TreeSet.class.isAssignableFrom(field.getType())) {
                field.set(this, new TreeSet());

            } else if (Set.class.isAssignableFrom(field.getType())) {
                field.set(this, new HashSet());

            } else {
                throw new RuntimeException("Cannot populate non set or list collection");
            }

        } else {

            Type type = field.getGenericType();
            if (type instanceof ParameterizedType) {

                Class clazz = (Class) ((ParameterizedType) type).getActualTypeArguments()[0];
                Collection collection = null;

                if (List.class.isAssignableFrom(field.getType())) {
                    collection = new ArrayList();
                } else if (TreeSet.class.isAssignableFrom(field.getType())) {
                    collection = new TreeSet();
                } else if (Set.class.isAssignableFrom(field.getType())) {
                    collection = new HashSet();
                } else {
                    throw new RuntimeException("Cannot populate non set or list collection");
                }

                field.set(this, collection);

                if (EntityObject.class.isAssignableFrom(clazz)) {

                    for (String value : values) {

                        EntityObject obj = EntityObject.getInstance(context, clazz, value);
                        if (obj != null)
                            collection.add(obj);
                    }

                } else if (Date.class.equals(clazz)) {

                    for (String value : values) {
                        if (value != null && value.length() > 0) {
                            collection.add(new Date(Long.valueOf(value)));
                        }
                    }

                } else if (DateTime.class.equals(clazz)) {

                    for (String value : values) {
                        if (value != null && value.length() > 0) {
                            collection.add(new DateTime(Long.valueOf(value)));
                        }
                    }

                } else if (Double.class.equals(clazz)) {

                    for (String value : values) {
                        if (value != null && value.length() > 0) {
                            collection.add(Double.valueOf(value));
                        }
                    }

                } else if (Enum.class.isAssignableFrom(clazz)) {

                    for (String value : values) {
                        if (value != null && value.length() > 0)
                            try {
                                Enum e = Enum.valueOf(clazz, value);
                                collection.add(e);
                            } catch (RuntimeException e) {
                                e.printStackTrace();
                            }
                    }

                } else if (Integer.class.equals(clazz)) {

                    for (String value : values) {
                        if (value != null && value.length() > 0)
                            collection.add(Integer.valueOf(value));
                    }

                } else if (Long.class.equals(clazz)) {

                    for (String value : values) {
                        if (value != null && value.length() > 0)
                            collection.add(Long.valueOf(value));
                    }

                } else if (Money.class.equals(clazz)) {

                    for (String value : values) {
                        if (value != null && value.length() > 0)
                            collection.add(new Money(value));
                    }

                } else if (String.class.equals(clazz)) {

                    for (String value : values) {
                        if (value != null && value.length() > 0)
                            collection.add(value);
                    }

                } else if (Upload.class.equals(clazz)) {

                    String[] contentTypes = context.getParameterValues(name + ".content-type");
                    String[] filenames = context.getParameterValues(name + ".filename");

                    for (int n = 0; n < values.length; n++) {

                        File file = new File(values[n]);
                        if (file.exists() && file.length() > 100) {

                            String path = values[n];
                            String contentType = contentTypes[n];
                            String filename = filenames[n];

                            collection.add(new Upload(path, filename, contentType));
                        }
                    }
                }
            }
        }

    }

    private void populateCountry(Field field) throws IllegalArgumentException, IllegalAccessException {

        String value = getParameter(field.getName());
        if (value != null && value.length() > 0) {
            field.set(this, Country.getInstance(value));
        }
    }

    private void populateDate(Field field) throws IllegalArgumentException, IllegalAccessException {

        String name = field.getName();
        String value = context.getParameter(name);

        if (value == null || value.length() == 0) {
            return;
        }

        logger.fine("[Handler] populateDate name=" + name + ", value=" + value);

        try {

            Date date = new Date(value);

            logger.fine("[Handler] date returned=" + date);
            field.set(this, date);

        } catch (ParseException e) {
            e.printStackTrace();

        }

    }

    private void populateDateTime(Field field) throws IllegalArgumentException, IllegalAccessException {

        String name = field.getName();
        String value = context.getParameter(name);

        if (value == null || value.length() == 0) {
            return;
        }

        logger.fine("populateDateTime name=" + name + ", value=" + value);

        try {

            DateTime dateTime = new DateTime(value);

            logger.fine("dateTime returned=" + dateTime);
            field.set(this, dateTime);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    // private void populateDate(Field field) throws IllegalAccessException {
    //
    // String name = field.getName();
    // String parameterValue = context.getParameter(name);
    //
    // if (parameterValue != null) {
    //
    // if (parameterValue.contains("/")) {
    //
    // try {
    // field.set(this, new Date(parameterValue, "dd/MM/yyyy"));
    // } catch (IllegalArgumentException e) {
    // // e.printStackTrace();
    // } catch (IllegalAccessException e) {
    // // e.printStackTrace();
    // } catch (ParseException e) {
    // // e.printStackTrace();
    // }
    //
    // } else {
    //
    // try {
    //
    // field.set(this, new Date(Long.parseLong(parameterValue)));
    //
    // } catch (NumberFormatException e) {
    // e.printStackTrace();
    // }
    // }
    //
    // } else {
    //
    // String dayParameter = context.getParameter(name + "_day");
    // String monthParameter = context.getParameter(name + "_month");
    // String yearParameter = context.getParameter(name + "_year");
    //
    // if (dayParameter != null || monthParameter != null || yearParameter != null) {
    //
    // Month month = Month.January;
    // int day = 0, year = 0;
    //
    // if (dayParameter != null)
    // try {
    // day = Integer.parseInt(dayParameter.trim());
    // } catch (NumberFormatException e) {
    // e.printStackTrace();
    // }
    //
    // if (monthParameter != null)
    // try {
    // month = Month.valueOf(monthParameter);
    // } catch (RuntimeException e2) {
    // e2.printStackTrace();
    // }
    //
    // if (yearParameter != null)
    // try {
    // year = Integer.parseInt(yearParameter.trim());
    // } catch (NumberFormatException e1) {
    // e1.printStackTrace();
    // }
    //
    // Date date = new Date(year, month, day);
    // field.set(this, date);
    // }
    // }
    // }

    // private void populateDateTime(Field field) throws IllegalAccessException {
    //
    // String name = field.getName();
    // String parameterValue = context.getParameter(name);
    //
    // if (parameterValue != null) {
    //
    // if (parameterValue.contains("/")) {
    //
    // try {
    // field.set(this, new Date(parameterValue, "dd/MM/yyyy"));
    // } catch (IllegalArgumentException e) {
    // // e.printStackTrace();
    // } catch (IllegalAccessException e) {
    // // e.printStackTrace();
    // } catch (ParseException e) {
    // // e.printStackTrace();
    // }
    //
    // } else {
    //
    // try {
    //
    // field.set(this, new DateTime(Long.parseLong(parameterValue)));
    //
    // } catch (NumberFormatException e) {
    // e.printStackTrace();
    // }
    // }
    //
    // } else {
    //
    // String dayParameter = context.getParameter(name + "_day");
    // String monthParameter = context.getParameter(name + "_month");
    // String yearParameter = context.getParameter(name + "_year");
    //
    // if (dayParameter != null || monthParameter != null || yearParameter != null) {
    //
    // Month month = Month.January;
    // int day = 0, year = 0;
    //
    // if (dayParameter != null)
    // try {
    // day = Integer.parseInt(dayParameter.trim());
    // } catch (NumberFormatException e) {
    // e.printStackTrace();
    // }
    //
    // if (monthParameter != null)
    // try {
    // month = Month.valueOf(monthParameter);
    // } catch (RuntimeException e2) {
    // e2.printStackTrace();
    // }
    //
    // if (yearParameter != null)
    // try {
    // year = Integer.parseInt(yearParameter.trim());
    // } catch (NumberFormatException e1) {
    // e1.printStackTrace();
    // }
    //
    // DateTime date = new DateTime(year, month, day);
    // field.set(this, date);
    // }
    // }
    // }

    private void populateEntityObject(Field field, Class type) throws IllegalArgumentException, IllegalAccessException, ClassNotFoundException {

        String id = context.getParameter(field.getName());
        if (id == null)
            return;

        EntityObject obj = null;

        if (Modifier.isAbstract(type.getModifiers())) {

            String[] string = id.split(";");
            if (string.length == 2) {

                type = Class.forName(string[0]);
                id = string[1];
            }
        }

        obj = EntityObject.getInstance(context, type, id);

        if (obj != null)
            field.set(this, obj);
    }

    private void populateEnum(Field field) {

        String value = getParameter(field.getName());
        if (value != null && value.length() > 0)
            try {
                field.set(this, Enum.valueOf((Class<? extends Enum>) field.getType(), value));
            } catch (IllegalArgumentException e) {
                //e.printStackTrace();
            } catch (IllegalAccessException e) {
                //e.printStackTrace();
            }

    }

    private void populateMap(Field field) throws IllegalArgumentException, IllegalAccessException {

        logger.fine("[Handler] populateMap field=" + field);

        Type type = field.getGenericType();

        if (!(type instanceof ParameterizedType)) {
            throw new RuntimeException("Map is not parameterized");
        }

        ParameterizedType pt = (ParameterizedType) type;
        Type[] types = pt.getActualTypeArguments();
        logger.fine("[Handler] pt=" + pt + ", types=" + types);

        final Class keyClass = (Class) types[0];
         Class valueClass = null;
//         ParameterizedType valueClass2 = null;
        if (types[1] instanceof Class) {
            valueClass = (Class) types[1];
        } else if (types[1] instanceof ParameterizedType) {
            valueClass = (Class) ((ParameterizedType) types[1]).getRawType();
        }

        logger.fine("[Handler] map types: " + keyClass + " " + valueClass);

        final Map map;
        final Class<?> mapType = field.getType();

        logger.fine("[Handler] map type=" + mapType);

        // get appropriate map class
        if (mapType.equals(LinkedHashMap.class)) {

            if (keyClass.equals(Integer.class)) {

                map = new LinkedHashMapWrapper(valueClass);

            } else {
                map = new LinkedHashMap();
            }

        } else if (LinkedHashMap.class.isAssignableFrom(mapType)) {

            if (keyClass.equals(Integer.class)) {

                map = new LinkedHashMapWrapper(valueClass);

            } else {

                map = new LinkedHashMap();
            }

        } else if (LinkedMultiMap.class.isAssignableFrom(mapType)) {

            if (keyClass.equals(Integer.class)) {

                map = new LinkedMultiMapWrapper(valueClass);

            } else {

                map = new LinkedMultiMap();
            }

        } else if (MultiHashMap.class.isAssignableFrom(mapType)) {

            if (keyClass.equals(Integer.class)) {

                map = new MultiHashMapWrapper(valueClass);

            } else {

                map = new MultiHashMap();
            }

        } else if (MultiValueMap.class.isAssignableFrom(mapType)) {

            if (keyClass.equals(Integer.class)) {

                map = new MultiHashMapWrapper(valueClass);

            } else {

                map = new MultiValueMap(new LinkedHashMap());
            }

        } else if (SortedMap.class.isAssignableFrom(mapType)) {

            if (keyClass.equals(Integer.class)) {

                map = new TreeMapWrapper(valueClass);

            } else {
                map = new TreeMap();
            }

        } else {

            if (keyClass.equals(Integer.class)) {

                map = new HashMapWrapper(valueClass);

            } else {

                map = new HashMap();
            }
        }

        // if our keyclass is integer then we want to override get method to turn entity objects
        // into integer keys

        logger.finest("[Handler] map class=" + map.getClass().toString());

        field.set(this, map);

        /* 080806 sks
           *	changed params to use underscore as well as tilde for splitting up map attributes, this is due to javascript not liking tildes
           */
        String name1 = field.getName() + "~";
        String name2 = field.getName() + "_";

        Enumeration en = context.getParameterNames();
//        String paramKey1 = null;
//        List paramKeys = new ArrayList();

        while (en.hasMoreElements()) {

            String paramName = (String) en.nextElement();
            logger.fine("cycling params, paramName=" + paramName);

            String paramKey = null;

            if (paramName.startsWith(name1)) {
                paramKey = paramName.substring(name1.length());
            } else if (paramName.startsWith(name2)) {
                paramKey = paramName.substring(name2.length());

            } else {

                logger.fine("param is not valid, skipping");
                continue;
            }

            /*
                * We must make sure the param name isn't one of our special upload field names
                *
                */
            if (paramName.endsWith(".filename") || paramName.endsWith(".content-type") || paramName.endsWith(".size")) {

                logger.fine("param name is special upload field, so skipping");
                continue;

            } else {

                logger.fine("param begins with map name, so using key=" + paramKey);
            }

            String[] paramValues = context.getParameterValues(paramName);
            if (paramValues == null)
                continue;

            logger.fine("[Handler] param values=" + Arrays.toString(paramValues));

            Object key = null, value = null;

            if (Double.class.equals(keyClass)) {

                key = new Double(paramKey);

            } else if (Date.class.equals(keyClass)) {
                try {
                    key = new Date(paramKey);
                } catch (ParseException e) {

                }

            } else if (DateTime.class.equals(keyClass)) {

                try {
                    key = new DateTime(paramKey);
                } catch (ParseException e) {

                }

            } else if (Integer.class.equals(keyClass)) {
                try {
                    key = new Integer(paramKey);
                } catch (NumberFormatException e) {

                }

            } else if (Long.class.equals(keyClass)) {
                try {
                    key = new Long(paramKey);
                } catch (NumberFormatException e) {

                }

            } else if (Enum.class.isAssignableFrom(keyClass)) {

                try {
                    key = Enum.valueOf(keyClass, paramKey);
                } catch (RuntimeException e1) {
                    key = null;
                }

            } else if (Short.class.equals(keyClass)) {
                key = new Short(paramKey);

            } else if (DateTime.class.equals(keyClass)) {
                key = new DateTime(Long.parseLong(paramKey));

            } else if (EntityObject.class.isAssignableFrom(keyClass)) {
                if (MultiValueMap.class.equals(valueClass)) {
                    if (paramKey.indexOf("_") != -1 && paramKey.split("_").length == 2) {
                        key = EntityObject.getInstance(context, keyClass, Integer.valueOf(paramKey.split("_")[1]));
                    } else {
                        key = EntityObject.getInstance(context, keyClass, paramKey);
                    }
                } else {
                    key = EntityObject.getInstance(context, keyClass, paramKey);
                }

            } else {

                key = paramKey;
            }

            if (key == null)
                continue;

            if (keyClass.equals(FormField.class) && ((FormField) key).getType() == FieldType.TickBoxes && paramValues.length > 1) {
                value = StringHelper.implode(paramValues, ",");
                if (value != null) {
                    map.put(key, value);
                }
                continue;
            }

            for (String paramValue : paramValues) {

                if (paramValue == null || paramValue.length() == 0)
                    continue;

                if (Boolean.class.equals(valueClass)) {
                    value = new Boolean(paramValue);

                } else if (Byte.class.equals(valueClass)) {
                    try {
                        value = new Byte(paramValue);
                    } catch (NumberFormatException e) {

                    }
                } else if (Date.class.equals(valueClass)) {

                    try {
                        value = new Date(paramValue);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if (DateTime.class.equals(valueClass)) {

                    try {
                        value = new DateTime(paramValue);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                } else if (Double.class.equals(valueClass))
                    try {
                        value = new Double(paramValue);
                    } catch (NumberFormatException e) {

                    }

                else if (Integer.class.equals(valueClass))
                    try {
                        value = new Integer(paramValue);
                    } catch (RuntimeException e) {
                        value = null;
                    }

                else if (Long.class.equals(valueClass)) {
                    try {
                        value = new Long(paramValue);
                    } catch (NumberFormatException e) {

                    }

                } else if (Money.class.equals(valueClass)) {
                    try {
                        value = new Money(paramValue);
                    } catch (NumberFormatException e) {

                    }

                } else if (Amount.class.equals(valueClass)) {
                    try {
                        value = new Amount(new Money(paramValue));
                    } catch (NumberFormatException e) {

                    }

                } else if (Short.class.equals(valueClass)) {
                    try {
                        value = new Short(paramValue);
                    } catch (NumberFormatException e) {

                    }

                } else if (Enum.class.isAssignableFrom(valueClass)) {

                    try {
                        value = Enum.valueOf(valueClass, paramValue);
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                    }

                } else if (EntityObject.class.isAssignableFrom(valueClass)) {

                    value = EntityObject.getInstance(context, valueClass, paramValue);
                } else if (Upload.class.equals(valueClass)) {

                    value = getUpload(paramName, paramValue);
                    logger.fine("getting upload for map, upload=" + value);

                } else if(MultiValueMap.class.equals(valueClass)){
                    Type[] valueTypes = ((ParameterizedType)types[1]).getActualTypeArguments();
                    final Class vKeyClass = (Class) valueTypes[0];
                    Class vValueClass = (Class) valueTypes[1];
                    Object mapKey = popKey(paramKey, vKeyClass);
                    Object mapValue = popValue(paramValue, vValueClass);
                    MultiValueMap mvm;
                    if (map.containsKey(key)) {
                        mvm = (MultiValueMap) map.get(key);
                    } else {
                        mvm = new MultiValueMap();
                    }
                    mvm.put(mapKey, mapValue);
                    
                    value = mvm;

                } else {

                    value = paramValue;
                }

                if (value != null) {

                    logger.fine("[Handler] key class=" + key.getClass() + " valueclass=" + value.getClass());
                    map.put(key, value);
                }
            }
        }
    }

    private Object popKey(String paramKey, Class keyClass) {
        Object key = null;
        if (Double.class.equals(keyClass)) {

            key = new Double(paramKey);

        } else if (Date.class.equals(keyClass)) {
            try {
                key = new Date(paramKey);
            } catch (ParseException e) {

            }

        } else if (DateTime.class.equals(keyClass)) {

            try {
                key = new DateTime(paramKey);
            } catch (ParseException e) {

            }

        } else if (Integer.class.equals(keyClass)) {
            try {
                key = new Integer(paramKey);
            } catch (NumberFormatException e) {

            }

        } else if (Long.class.equals(keyClass)) {
            try {
                key = new Long(paramKey);
            } catch (NumberFormatException e) {

            }

        } else if (Enum.class.isAssignableFrom(keyClass)) {

            try {
                key = Enum.valueOf(keyClass, paramKey);
            } catch (RuntimeException e1) {
                key = null;
            }

        } else if (Short.class.equals(keyClass)) {
            key = new Short(paramKey);

        } else if (DateTime.class.equals(keyClass)) {
            key = new DateTime(Long.parseLong(paramKey));

        } else if (EntityObject.class.isAssignableFrom(keyClass)) {
            if (paramKey.indexOf("_") != -1 && paramKey.split("_").length == 2) {
                key = EntityObject.getInstance(context, keyClass, paramKey.split("_")[0]);
            } else {
                key = EntityObject.getInstance(context, keyClass, paramKey);
            }

        } else {
            key = paramKey;
        }

        return key;
    }

    private Object popValue(String paramValue, Class valueClass) {
        Object value = null;
        if (Boolean.class.equals(valueClass)) {
            value = new Boolean(paramValue);

        } else if (Byte.class.equals(valueClass)) {
            try {
                value = new Byte(paramValue);
            } catch (NumberFormatException e) {

            }
        } else if (Date.class.equals(valueClass)) {

            try {
                value = new Date(paramValue);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if (DateTime.class.equals(valueClass)) {

            try {
                value = new DateTime(paramValue);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        } else if (Double.class.equals(valueClass))
            try {
                value = new Double(paramValue);
            } catch (NumberFormatException e) {

            }

        else if (Integer.class.equals(valueClass))
            try {
                value = new Integer(paramValue);
            } catch (RuntimeException e) {
                value = null;
            }

        else if (Long.class.equals(valueClass)) {
            try {
                value = new Long(paramValue);
            } catch (NumberFormatException e) {

            }

        } else if (Money.class.equals(valueClass)) {
            try {
                value = new Money(paramValue);
            } catch (NumberFormatException e) {

            }

        } else if (Amount.class.equals(valueClass)) {
            try {
                value = new Amount(new Money(paramValue));
            } catch (NumberFormatException e) {

            }

        } else if (Short.class.equals(valueClass)) {
            try {
                value = new Short(paramValue);
            } catch (NumberFormatException e) {

            }

        } else if (Enum.class.isAssignableFrom(valueClass)) {

            try {
                value = Enum.valueOf(valueClass, paramValue);
            } catch (RuntimeException e) {
                e.printStackTrace();
            }

        } else if (EntityObject.class.isAssignableFrom(valueClass)) {

            value = EntityObject.getInstance(context, valueClass, paramValue);
        } else {
            value = paramValue;
        }
        return value;
    }

    private void populateMoney(Field field) throws IllegalArgumentException, IllegalAccessException {

        logger.fine("populate money field=" + field);

        String parameterValue = context.getParameter(field.getName());
        if (parameterValue == null || parameterValue.length() == 0)
            return;

        logger.fine("parameter value=" + parameterValue);

        if (parameterValue.startsWith("\243")) {
            logger.fine("stripping pound sign: " + parameterValue);
            parameterValue = parameterValue.substring(1);
        }

        try {
            field.set(this, new Money(parameterValue));
        } catch (NumberFormatException e) {

        }
    }

    private void populatePeriod(Field field) throws IllegalArgumentException, IllegalAccessException {

        String value = getParameter(field.getName());
        if (value != null && value.length() > 0)
            field.set(this, new Period(value));
    }

    final void populatePrimitive(Field field, Class type) throws IllegalArgumentException, IllegalAccessException {

        logger.fine("populate primitive field=" + field);

        String parameterValue = context.getParameter(field.getName());
        if (parameterValue == null)
            return;

        logger.fine("parameterValue=" + parameterValue);

        if (Boolean.TYPE.equals(type)) {
            field.setBoolean(this, Boolean.parseBoolean(parameterValue));

        } else if (Boolean.class.equals(type)) {
            field.set(this, Boolean.valueOf(parameterValue));

        } else if (Byte.TYPE.equals(type)) {
            field.setByte(this, Byte.parseByte(parameterValue));

        } else if (Byte.class.equals(type)) {
            field.set(this, Byte.valueOf(parameterValue));

        } else if (Character.class.equals(type)) {
            if (parameterValue.length() > 0)
                field.set(this, Character.valueOf(parameterValue.toCharArray()[0]));

        } else if (Character.TYPE.equals(type)) {
            if (parameterValue.length() > 0)
                field.setChar(this, parameterValue.toCharArray()[0]);

        } else if (Double.TYPE.equals(type)) {
            try {
                field.setDouble(this, Double.parseDouble(parameterValue));
            } catch (NumberFormatException e) {
                field.setDouble(this, 0);
            }

        } else if (Double.class.equals(type)) {
            try {
                field.set(this, Double.valueOf(parameterValue));
            } catch (NumberFormatException e) {
            }

        } else if (int.class.equals(type)) {
            try {
                field.setInt(this, Integer.parseInt(parameterValue));
            } catch (NumberFormatException e) {
                field.setInt(this, 0);
            }

        } else if (Integer.class.equals(type)) {
            field.set(this, Integer.valueOf(parameterValue));

        } else if (Long.TYPE.equals(type)) {
            field.setLong(this, Long.parseLong(parameterValue));

        } else if (Long.class.equals(type)) {
            field.set(this, Long.valueOf(parameterValue));

        } else if (Short.TYPE.equals(type)) {
            field.setShort(this, Short.parseShort(parameterValue));

        } else if (Short.class.equals(type)) {
            field.set(this, Short.valueOf(parameterValue));

        }
    }

    /**
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    private void populateString(Field field) throws IllegalArgumentException, IllegalAccessException {

        String value = getParameter(field.getName());
        if (value != null && value.length() > 0) {

            value = value.replace("%3c", "");
            value = value.replace("%2f", "");

            field.set(this, value);

        }
    }

    private void populateTime(Field field) throws IllegalArgumentException, IllegalAccessException {

        String name = field.getName();
        String value = context.getParameter(name);

        if (value == null)
            return;

        value = value.trim();
        if (value.length() > 0)
            try {
                field.set(this, new Time(Long.parseLong(value)));
            } catch (NumberFormatException e) {

            }
    }

    private void populateUpload(Field field) throws IllegalArgumentException, IllegalAccessException {

        logger.fine("populate upload field=" + field);
        String name = field.getName();
        String value = context.getParameter(name);

        Upload upload = getUpload(name, value);
        field.set(this, upload);
    }

    protected final void removeAttribute(String key) {
        context.removeAttribute(key);
    }

    protected final void removeCookie(String key) {
        context.removeCookie(key);
    }

    protected final void removeParameter(String key) {
        context.removeParameter(key);
    }

    protected abstract boolean runSecure();

    /**
     *
     */
    public boolean runUnsecure() {
        return !runSecure();
    }

    protected final void setAttribute(String key, Object obj) {
        if (obj == null)
            context.removeAttribute(key);
        else
            context.setAttribute(key, obj);
    }

    protected final void setCookie(String key, Object value) {
        setCookie(key, value.toString(), false, -1);
    }

    /**
     * Sets the maximum age of the cookie in seconds.
     * <p/>
     * A positive value indicates that the cookie will expire after that many seconds have passed. Note that the value is the maximum age when the
     * cookie will expire, not the cookie's current age.
     * <p/>
     * A negative value means that the cookie is not stored persistently and will be deleted when the Web browser exits. A zero value causes the
     * cookie to be deleted.
     */
    protected final void setCookie(String key, String value, boolean secure, int age) {
        context.setCookie(key, value, secure, age);
    }

    protected final void setCookie(String key, String value, int age) {
        setCookie(key, value, false, age);
    }

    protected final void setError(String key, Exception e) {
        setError(key, e.getMessage());
    }

    protected final void setError(String key, String error) {
        context.setError(key, error);
    }

    protected void setError(String string, String string2, String string3) {
        setError(string + "_" + string2, string3);
    }

    //	protected final void test(Validator validator, String errorString, String parameterName) {
    //
    //		String value = context.getParameter(parameterName);
    //		String error = validator.validate(value);
    //
    //		if (error != null)
    //			context.setError(parameterName, errorString == null ? error : errorString);
    //	}

    protected void test(RequiredValidator validator, String string, EntityObject obj) {
        context.test(validator, string, obj);
    }

    protected final void test(Validator validator, String name) {
        context.test(validator, name);
    }

    protected final void test(Validator validator, String parameterName, String replacementParameterName) {

        String value = context.getParameter(parameterName);
        String error = validator.validate(value);

        if (error != null)
            if (replacementParameterName != null)
                context.setError(replacementParameterName, error);
            else
                context.setError(parameterName, error);
    }
}