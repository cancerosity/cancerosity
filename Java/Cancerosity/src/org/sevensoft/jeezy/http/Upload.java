package org.sevensoft.jeezy.http;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import org.sevensoft.commons.simpleio.SimpleFile;

/**
 * @author sks 09-Jan-2006 14:27:54
 *
 */
public final class Upload {

	/**
	 * The path to the temporary file
	 */
	private final String	path;

	/**
	 * The original filename on the users drive
	 */
	private final String	filename;

	private final String	contentType;

	public Upload(String path, String fn, String contentType) {

		this.path = path;
		this.contentType = contentType;

		/* 
		 * ie sends the entire path as the filename so strip out to leave just the filename
		 */
		fn = fn.replaceAll(".*/", "");
		fn = fn.replaceAll(".*\\\\", "");
		fn = fn.replaceAll("\\s", "_");

		this.filename = fn;
	}

	/**
	 * Returns the content of this upload file as a string
	 * 
	 */
	public String getContentAsString() throws IOException {
		return SimpleFile.readString(getFile());
	}

	public String getContentType() {
		return contentType;
	}

	/**
	 * Returns a file representing the temp file written in /tmp etc
	 * 
	 */
	public File getFile() {
		return new File(path);
	}

	/**
	 * Returns the filename of the original file
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * Returns the filename safe without spaces
	 */
	public String getFilenameSafe() {
		return getFilename().replaceAll("\\s", "_").replaceAll("'", "").replaceAll("\"", "").replace("(", "").replace(")", "").toLowerCase();
	}

	public FileInputStream getInputStream() throws FileNotFoundException {
		return new FileInputStream(getFile());
	}

	public String getPath() {
		return path;
	}

	/**
	 * Writes this upload to the specified file.
	 * 
	 */
	public void write(File destinationFile) throws IOException {

		FileChannel sourceChannel = null;
		FileChannel destinationChannel = null;

		try {

			sourceChannel = new FileInputStream(getFile()).getChannel();
			destinationChannel = new FileOutputStream(destinationFile).getChannel();

			sourceChannel.transferTo(0, sourceChannel.size(), destinationChannel);

		} catch (IOException e) {
			e.printStackTrace();
			throw e;

		} finally {

			try {
				if (sourceChannel != null)
					sourceChannel.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				if (destinationChannel != null)
					destinationChannel.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Writes this upload to the specified filename.
	 * 
	 */
	public File write(String destinationFilename) throws IOException {
		final File file = new File(destinationFilename);
		write(file);
		return file;
	}

	/**
	 * Will write the file to the specified directory. 
	 * It will not overwrite an existing file but instead append with _XX where _XX is the next number.
	 * 
	 */
	public String writeUnique(String dirName) throws IOException {

		String fn = SimpleFile.getUniqueFilename(getFilenameSafe(), dirName);
		if (fn == null) {
			return null;
		}

		write(new File(dirName + "/" + fn));
		return fn;
	}
}
