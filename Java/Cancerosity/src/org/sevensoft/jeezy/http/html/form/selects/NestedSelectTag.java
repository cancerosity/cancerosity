package org.sevensoft.jeezy.http.html.form.selects;

import java.util.HashMap;
import java.util.Map;

import org.sevensoft.jeezy.db.EntityObject;

/**
 * @author sks 11 Aug 2006 22:21:26
 *
 * Model data for a select
 */
public class NestedSelectTag {

	private Map<String, SelectTag>	tags;
	private SelectTag				rootTag;

	public NestedSelectTag(SelectTag rootTag) {
		this.rootTag = rootTag;
		this.tags = new HashMap<String, SelectTag>();
	}

	public void addTag(Object trigger, SelectTag tag) {
		tags.put(EntityObject.getEqualityString(trigger), tag);
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("<script>");
		sb.append("function showNestedTag(id) {");

		// hide all tags first
		for (Map.Entry<String, SelectTag> entry : tags.entrySet()) {
			sb.append("document.getElementById('" + entry.getKey() + "_nestedtag').style.display = 'none';");
		}

		sb.append("var e = document.getElementById(id + '_nestedtag');");
		sb.append("e.style.display = 'inline';");
		sb.append("}");
		sb.append("</script>");

		rootTag.setOnChange("showNestedTag(this.options[this.selectedIndex].value);");

		sb.append(rootTag);

		for (Map.Entry<String, SelectTag> entry : tags.entrySet()) {

			SelectTag tag = entry.getValue();
			tag.setId(entry.getKey() + "_nestedtag");

			// hide all non root elements
			tag.setStyle("display: none");

			sb.append(tag);
		}

		return sb.toString();
	}
}
