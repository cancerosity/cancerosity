package org.sevensoft.jeezy.http.html;

import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 10-Nov-2005 10:34:56
 * 
 */
public class ImageTag extends AbstractTag {

	public ImageTag(Link link) {
		super(null);
		setSrc(link.toString());
		setBorder(0);
	}

	public ImageTag(String src) {
		this(src, 0, 0, 0);
	}

	public ImageTag(String src, int border, int width, int height) {
		super(null);
		setSrc(src);
		setBorder(border);
		setWidth(width);
		setHeight(height);
		setAlt(SimpleFile.removeFileExtension(src));
        setTitle(getAlt());
	}

	@Override
	public String toString() {
		return super.doTag("img").toString();
	}

}
