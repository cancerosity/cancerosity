package org.sevensoft.jeezy.http.html.form.date;

import java.util.logging.Logger;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 6 Dec 2006 16:29:05
 *
 */
public class WeekDateTag {

	private Logger			logger	= Logger.getLogger("tags");

	private final RequestContext	context;
	private final String		name;
	private Date			value;
	private Date			end;
	private Date			start;
	private boolean			autoSubmit;

	public WeekDateTag(RequestContext context, String name, Date v) {
		this.context = context;
		this.name = name;

		// reset value to start of week as this is a week tag!
		if (v != null)
			this.value = v.beginWeek();

		// set end to start of the current week
		this.end = new Date().beginWeek();
		setWeeks(20);

		logger.fine("[WeekTag] created, value=" + value.toDateString() + ", name=" + name + ", end=" + end.toDateString() + ", start="
				+ start.toDateString());
	}

	public void setAutoSubmit(boolean b) {
		this.autoSubmit = b;
	}

	public WeekDateTag setWeeks(int i) {
		this.start = end.removeWeeks(i);
		return this;
	}

	@Override
	public String toString() {

		SelectTag tag = new SelectTag(context, name, value);
		tag.setAutoSubmit(autoSubmit);
		tag.setIgnoreParamValue(true);

		Date date = start;
		while (!date.isAfter(end)) {
			tag.addOption(date.getTimestampString(), date.toString("dd-MMM-yyyy"));
			date = date.nextWeek();
		}

		return tag.toString();
	}

}
