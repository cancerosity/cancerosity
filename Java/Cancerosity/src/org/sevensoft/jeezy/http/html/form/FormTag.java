package org.sevensoft.jeezy.http.html.form;

import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.html.form.hidden.HiddenTag;

/**
 * @author sks 10-Nov-2005 12:40:32
 * 
 */
public class FormTag {

	private String	url, method;
	private String	enctype;
	private String	handlerAction;
	private String	id;
    private String  clazz;
    private String	name;

	public FormTag(Class<? extends Handler> clazz, String handlerAction, String method) {

		String url = Handler.getPath(clazz);
		init(url, handlerAction, method);
	}

	public FormTag(String url, String handlerAction, String method) {

		init(url, handlerAction, method);
	}

	private void init(String url, String handlerAction, String method) {

		this.url = url;
		this.handlerAction = handlerAction;

		if (method.equalsIgnoreCase("multi")) {
			this.method = "post";
			this.enctype = "multipart/form-data";
		} else {
			this.method = method.toLowerCase();
		}
	}

    public FormTag setClass(String clazz) {
        this.clazz = clazz;
        return this;
    }

    public FormTag setId(String id) {
		this.id = id;
		return this;
	}

	public FormTag setName(String name) {
		this.name = name;
		return this;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("\n<form action=\"");
		sb.append(url);
		sb.append("\" method=\"");
		sb.append(method);

		if (name != null) {
			sb.append("\" name=\"");
			sb.append(name);
		}

		if (enctype != null) {
			sb.append("\" enctype=\"");
			sb.append(enctype);
		}

		if (id != null) {
			sb.append("\" id=\"");
			sb.append(id);
		}

        if (clazz != null) {
            sb.append("\" class=\"");
            sb.append(clazz);
        }

        sb.append("\">\n");

		if (handlerAction != null) {
			sb.append(new HiddenTag("_a", handlerAction));
		}

		return sb.toString();

	}
}
