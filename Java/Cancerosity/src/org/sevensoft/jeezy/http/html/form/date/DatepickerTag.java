package org.sevensoft.jeezy.http.html.form.date;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.commons.samdate.Date;

/**
 * User: Tanya
 * Date: 11.05.2011
 */
public class DatepickerTag extends DateTextTag {

    public DatepickerTag(RequestContext context, String name) {
        super(context, name);
    }

    public DatepickerTag(RequestContext context, String name, Date value) {
        super(context, name, value);
    }

    public DatepickerTag(RequestContext context, String name, String value) {
        super(context, name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<script>\n" +
                "\t$(function() {\n" +
                "\t\t$(\"#datepicker" + getName() + "\").datepicker({ dateFormat: 'dd/mm/yy', changeYear: true, yearRange: '1930:" + new Date().getYear() + "' });\n" +
                "\t});\n" +
                "\t</script>");

        sb.append("<input id='datepicker"+getName()+"' type=\"text\" class=\"text\" name=\"" + getName() + "\"");
        if (getValue() != null) {
            sb.append(" value='" + getValue() + "'");
        }
        sb.append("/>");

        return sb.toString();
    }
}
