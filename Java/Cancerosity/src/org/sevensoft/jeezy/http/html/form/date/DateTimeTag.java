package org.sevensoft.jeezy.http.html.form.date;

import java.util.logging.Logger;

import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 4 Aug 2006 11:09:06
 * This class uses the date picker
 * 
 */
public class DateTimeTag {

	private static Logger		logger	= Logger.getLogger("tags");

	private final RequestContext	context;

	private final DateTime		value;

	private int				endYear;

	private int				startYear;

	private int				yearRange;

	private final String		name;

	public DateTimeTag(RequestContext context, String name) {
		this(context, name, (DateTime) null);
	}

	public DateTimeTag(RequestContext context, String name, DateTime value) {

		this.name = name;
		logger.fine("created datetime tag, name=" + name + ", value=" + value);

		this.context = context;
		this.value = value;
	}

	public DateTimeTag(RequestContext context, String name, long timestamp) {
		this(context, name, new DateTime(timestamp));
	}

	public DateTimeTag(RequestContext context, String name, String value) {
		this(context, name, Long.parseLong(value));
	}

	public void setEndYear(int yearEnd) {
		this.endYear = yearEnd;
	}

	public void setStartYear(int yearStart) {
		this.startYear = yearStart;
	}

	public void setYearRange(int yearRange) {
		this.yearRange = yearRange;
	}

	@Override
	public String toString() {

		logger.fine("datetime tag toString()");

		StringBuilder sb = new StringBuilder();

		DateTag dateTag;

		if (value == null) {

			dateTag = new DateTag(context, name);

		} else {

			dateTag = new DateTag(context, name, value.getDate());

		}

		dateTag.setYearRange(5);
		dateTag.setStartYear(startYear);

		// TIME
		{

			String param = dateTag.getParam();

			SelectTag timeTag = new SelectTag(context, param);
			timeTag.setAny("--");

			if (value != null) {

				String timeValue = value.toString("HH:mm") + " ";
				timeTag.setValue(timeValue);
				logger.fine("setting time value to " + timeValue);
			}

			for (int n = 0; n < 24; n++) {

				for (int m = 0; m < 60; m += 5) {

					StringBuilder time = new StringBuilder(4);

					if (n < 10)
						time.append("0");
					time.append(n);

					time.append(":");

					if (m < 10)
						time.append("0");
					time.append(m);

					timeTag.addOption(time + " ", time);
				}
			}

			sb.append(timeTag);
		}

		sb.append(dateTag);

		return sb.toString();
	}
}
