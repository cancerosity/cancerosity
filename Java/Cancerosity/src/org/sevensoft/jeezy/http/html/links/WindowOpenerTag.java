package org.sevensoft.jeezy.http.html.links;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.AbstractTag;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sks 10 Mar 2007 10:45:32
 */
public class WindowOpenerTag extends AbstractTag {

    private final int width;

    private final int height;

    private boolean scrollbars;

    private String windowName;

    private String resize;

    private String url;
    private boolean toolbar;

    protected WindowOpenerTag(RequestContext context, String url, int width, int height) {
        super(context);
        this.url = url;
        this.width = width;
        this.height = height;

        this.windowName = "newwindow";
    }

    public List<String> getArgs() {

        List<String> args = new ArrayList();

        if (width > 0) {
            args.add("width=" + width);
        }

        if (height > 0) {
            args.add("height=" + height);
        }

        if (scrollbars) {
            args.add("scrollbars=yes");
        }

        if (toolbar) {
            args.add("toolbar=yes");
        }

        if (resize != null) {
            args.add("resize=" + resize);
        }

        return args;
    }

    public final String getUrl() {
        return url;
    }

    public final String getWindowName() {
        return windowName;
    }

    public final void setResize(String resize) {
        this.resize = resize;
    }

    public void setScrollbars(boolean scrollbars) {
        this.scrollbars = scrollbars;
    }

    public void setToolbar(boolean b) {
        this.toolbar = b;
    }

    public final void setWindowName(String windowName) {
        this.windowName = windowName;
    }
}
